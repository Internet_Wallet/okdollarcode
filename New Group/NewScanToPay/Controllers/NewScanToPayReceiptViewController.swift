//
//  NewScanToPayReceiptViewController.swift
//  OK
//
//  Created by Tushar Lama on 28/08/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class NewScanToPayReceiptViewController: OKBaseController {

    @IBOutlet var receiptTable: UITableView!
    var payModelObj: NewScanPayModel?
    var merchantData = [String]()
    let MerchantTitleInfo = ["Merchant Name".localized,"Transaction ID".localized,"Reference Number".localized,"Service Fee".localized,"Amount".localized,"Total Amount".localized, "Remarks".localized]
    
    @IBOutlet var timelabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    var yAxis : CGFloat = 0.0
    var pdfview = UIView()
    var listTitleValue : [Dictionary<String,String>]?
    var dateNew = ""
    var timeNew = ""
   
    @IBOutlet var bgView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "SCAN / PAY".localized
        self.navigationItem.hidesBackButton = true
        self.receiptTable.backgroundColor = .white
        
        pdfview.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30)
               self.pdfview.backgroundColor = #colorLiteral(red: 0.9285323024, green: 0.9334669709, blue: 0.9376094937, alpha: 1)
               self.bgView.backgroundColor = .white
        self.navigationController?.navigationItem.hidesBackButton = true
        let dateTime = payModelObj?.transactionTime ?? ""
        let value = dateTime.components(separatedBy: " ")
        if value.count>0{
            if value.indices.contains(0){
                dateLabel.text = value[0]
                dateNew = value[0]
            }
            if value.indices.contains(1){
                timelabel.text = value[1]
                timeNew = value[1]
            }
        }
       
        if let data = payModelObj{
        merchantData = NewScanToPayHelper.getMerchantFinalDetails(payModelObj: data )
        listTitleValue = NewScanToPayHelper.getDataForPdf(payModelObj: data)
        }
        self.receiptTable.register(UINib(nibName: NearByConstant.kNearByPaymentCell, bundle: nil), forCellReuseIdentifier: NearByConstant.kNearByPaymentCell)
        self.receiptTable.separatorColor = .clear
        self.receiptTable.delegate = self
        self.receiptTable.dataSource  = self
        self.receiptTable.reloadData()
       
        self.createPDF()
        DispatchQueue.main.async {
            self.saveToGallery()
        }
    }
    
    func saveToGallery() {
        bgView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: pdfview.frame.size.height + 90)
       let image = captureScreen(view: self.bgView)
       UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
           if let error = error {
//               alertViewObj.wrapAlert(title: "Error".localized, body: error.localizedDescription, img: nil)
//               alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//               })
//               alertViewObj.showAlert(controller: self)
            
            
           } else {
            
               println_debug("Your image has been saved to your photos.")
           }
       }
    
    @IBAction func onClickMore(_ sender: Any) {
        shareQRWithDetail()
    }
    
    func shareQRWithDetail() {
        bgView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: pdfview.frame.size.height + 90)
           let image = captureScreen(view: self.bgView)
           let imageToShare = [image]
           let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
           activityViewController.popoverPresentationController?.sourceView = self.view
           DispatchQueue.main.async {
               self.present(activityViewController, animated: true, completion: nil)
           }
       }
    
    
    @IBAction func onClickHome(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }

}

extension NewScanToPayReceiptViewController: UITableViewDelegate,UITableViewDataSource{
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MerchantTitleInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NearByConstant.kNearByPaymentCell) as? NearByPaymentCell else{
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor(red: 221.0/255.0, green: 221.0/255.0, blue: 221.0/255.0, alpha: 1.0)
        cell.titleLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
        let height =  heightForView(text: MerchantTitleInfo[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 13.0), width: screenWidth)
        let titleHeight = heightForView(text:MerchantTitleInfo[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 13.0), width: screenWidth)
        
        if height>titleHeight{
            if height<=19.0{
                cell.merchantLabelHeight.constant = 20.0
            }else{
                cell.merchantLabelHeight.constant = height + 10
            }
        }else{
            if titleHeight<=20.0{
                cell.merchantLabelHeight.constant = 20.0
            }else{
                cell.merchantLabelHeight.constant = titleHeight
            }
        }
        
        
        cell.titleLabel.text = MerchantTitleInfo[indexPath.row]
        
        if merchantData.indices.contains(indexPath.row){
            cell.merchantValueLabel.text = merchantData[indexPath.row]
        }
       
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
}


extension NewScanToPayReceiptViewController{
    
    func createPDF() {
        //Image Icon
        
        yAxis = 15
        let imgAppIcon = UIImageView(frame: CGRect(x: (self.view.frame.width/2) - 40, y: yAxis, width: 80, height: 80))
        imgAppIcon.image = UIImage(named: "appIcon_Ok")
        self.pdfview.addSubview(imgAppIcon)
        //Y= 120
        yAxis = 105
        let lblPaymentReceipt = UILabel(frame: CGRect(x: 0, y: yAxis, width: self.view.frame.width, height: 40))
        lblPaymentReceipt.text = "PAYMENT RECEIPT".localized
        lblPaymentReceipt.backgroundColor = #colorLiteral(red: 0.1096028909, green: 0.2039180398, blue: 0.5867625475, alpha: 1)
        lblPaymentReceipt.textAlignment = .center
        lblPaymentReceipt.textColor = .white
        lblPaymentReceipt.font = UIFont.systemFont(ofSize: 16)
        self.pdfview.addSubview(lblPaymentReceipt)
        //Y= 160
        yAxis = 145
        let dateView = UILabel(frame: CGRect(x: 0, y: yAxis, width: self.view.frame.width, height: 50))
        dateView.backgroundColor = .clear
        
        
        let imgDate = UIImageView(frame: CGRect(x: 10, y: 15, width: 20, height: 20))
        imgDate.image = UIImage(named: "merchantCalendar")
        dateView.addSubview(imgDate)
                
        
        let lblDate = UILabel()
        lblDate.text = dateNew
        lblDate.font = UIFont.systemFont(ofSize: 12)
        lblDate.textAlignment = .left
        lblDate.sizeToFit()
        lblDate.frame = CGRect(x: 35, y: 0, width: lblDate.frame.width, height: 50)
        dateView.addSubview(lblDate)
        
      
        let lblTime = UILabel()
        lblTime.text = timeNew
        lblTime.font = UIFont.systemFont(ofSize: 12)
        lblTime.textAlignment = .left
        lblTime.sizeToFit()
        lblTime.frame = CGRect(x: self.view.frame.width - 130, y: 0, width: lblTime.frame.width, height: 50)
        dateView.addSubview(lblTime)
        
        let imgTime = UIImageView(frame: CGRect(x: lblTime.frame.origin.x - 30, y: 15, width: 20, height: 20))
        imgTime.image = UIImage(named: "merchantClock")
        dateView.addSubview(imgTime)
              
        
        //Y= 210
        yAxis = 195
       
        self.pdfview.addSubview(dateView)
  
        if let list = listTitleValue {
            for (index, _) in list.enumerated() {
                
                let containerView = UIView(frame: CGRect(x: 10, y: yAxis, width: self.view.frame.width - 20, height: 0))
                let dicTitle = list[index]
                let title = dicTitle["title"]
                let lblTitle = UILabel()
                lblTitle.text = title  ?? ""
                lblTitle.textAlignment = .left
                lblTitle.font = UIFont.systemFont(ofSize: 15)
                lblTitle.numberOfLines = 0
                lblTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
                lblTitle.frame = CGRect(x: 10, y: 10, width: 140, height: heightForViewUPI(text: title ?? "", font: UIFont.systemFont(ofSize: 15), width: 140))
                containerView.addSubview(lblTitle)
                
                let lblColon = UILabel()
                lblColon.text = ":"
                lblColon.textAlignment = .center
                lblColon.font = UIFont.systemFont(ofSize: 15)
                lblColon.numberOfLines = 0
                lblColon.frame = CGRect(x: 150, y: 8, width: 10, height: heightForViewUPI(text:  ":", font: UIFont.systemFont(ofSize: 15), width: 10))
                containerView.addSubview(lblColon)
                
                let dicValue = list[index]
                let value = dicValue["value"]
                let lblValue = UILabel()
                lblValue.text = value ?? ""
                lblValue.textAlignment = .left
                lblValue.font = UIFont.systemFont(ofSize: 15)
                lblValue.numberOfLines = 0
                lblValue.lineBreakMode = NSLineBreakMode.byWordWrapping
                lblValue.frame = CGRect(x: 160, y: 10, width: containerView.frame.width - 165, height: heightForViewUPI(text: value ?? "", font: UIFont.systemFont(ofSize: 15), width: containerView.frame.width - 160))
                containerView.addSubview(lblValue)
                
                if (lblTitle.frame.height == lblValue.frame.height) || (lblTitle.frame.height > lblValue.frame.height) {
                    containerView.frame = CGRect(x: 10, y: yAxis, width: self.view.frame.width - 20, height: lblTitle.frame.height + 20)
                }else {
                    containerView.frame = CGRect(x: 10, y: yAxis, width: self.view.frame.width - 20, height: lblValue.frame.height + 20)
                }
                
                let lblSap = UILabel()
                lblSap.text = ""
                lblSap.backgroundColor = #colorLiteral(red: 0.8273780942, green: 0.8274977207, blue: 0.8273518085, alpha: 1)
                lblSap.frame = CGRect(x: 0, y: containerView.frame.height - 1, width: containerView.frame.width, height: 1)
                containerView.addSubview(lblSap)
                
                containerView.backgroundColor = #colorLiteral(red: 0.9638236165, green: 0.9687631726, blue: 0.9728998542, alpha: 1)
                pdfview.addSubview(containerView)
                
                print("Height: \(yAxis)")
                yAxis = yAxis + containerView.frame.height
                if title == "Remarks".localized {
                    let imgAppIcon = UIImageView(frame: CGRect(x: 0, y: yAxis + 30, width: self.view.frame.width, height: 30))
                    imgAppIcon.image = UIImage(named: "footerSMB")
                    self.pdfview.addSubview(imgAppIcon)
                    break
                }
            }
        }
        pdfview.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: yAxis + 90)
        self.bgView.addSubview(pdfview)
        
        
    }
    
    func heightForViewUPI(text:String, font:UIFont, width:CGFloat) -> CGFloat {
         let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
         label.numberOfLines = 0
         label.lineBreakMode = NSLineBreakMode.byWordWrapping
         label.font = font
         label.text = text
         label.sizeToFit()
         return label.frame.height
     }
    
}
