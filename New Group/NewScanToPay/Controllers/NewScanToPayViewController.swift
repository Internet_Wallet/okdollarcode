//
//  NewScanToPayViewController.swift
//  OK
//
//  Created by Tushar Lama on 28/08/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit
import AVFoundation

class NewScanToPayViewController: OKBaseController {

    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    @IBOutlet var flashView: UIView!
    @IBOutlet var flashImageView: UIImageView!
    @IBOutlet var ReportView: UIView!
    @IBOutlet var nextButton: UIButton!
    var newScanViewModel = NewScanToPayViewModel()
    var qrString = ""
    var encryptString = ""
    var ivector = ""
    var merchantID = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "SCAN / PAY".localized
         self.navigationController?.navigationBar.tintColor = UIColor.white
         self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
         loadCamera()
        NotificationCenter.default.addObserver(self, selector: #selector(self.appWillResignActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    //checking the camera session and invalidating the timer when the view gets disappers
       override func viewWillDisappear(_ animated: Bool) {
           super.viewWillDisappear(animated)
           self.flashImageView.isHighlighted = false
           if (captureSession?.isRunning == true) {
               captureSession.stopRunning()
           }
       }
    
    @IBAction func onClickNext(_ sender: Any) {
      // getQRScanDetail(encrypted: "SlN/BkTOxy4WF7UhIERJRyptj2rLkieGE6xMLNUgdHRty15VHZ71nF/FKIWJ9/QMtSJC5HDi2V8EHVpIm6AE1PLOUoRaaXgIKxrcotrGV8PPJUUWo9kP9prxvl13GnO5lZVmEL2yUBWzEaBQ0OK6DMc9PSPfV7n18fyG5hXtJkGzab+XEgCiZXCAFgZm7Z9Xg/dnl+NBO7orx5KVpzW54+AXcYJALMrkOljpdm2u980=", iVector: "0123456789123456", merchantID: "a0a2145b8791dc5")
      //  navigate()
    }
    
    
    func navigate(){
        if #available(iOS 13.0, *) {
            if let obj = self.storyboard?.instantiateViewController(identifier: NewScanToPayHelper.kNewScanQRInfoViewController) as? NewScanQRInfoViewController{
            if let objNew = self.newScanViewModel.qrScanObj{
                    obj.qrScanObj = objNew
                   // obj.qrScanObj = NewScanQRModel(data: ["MerchantName":"Tushar","RefNumber":"1234567","Amount":"1","ServiceFeeByProvider":"1","TotalAmount":"2","Remarks": "Test"] , code: 200, msg: "Success")
                    
    
              }
                self.navigationController?.pushViewController(obj, animated: true)
            }
        } else {
            if let ViewController = self.storyboard?.instantiateViewController(withIdentifier: NewScanToPayHelper.kNewScanQRInfoViewController) as? NewScanQRInfoViewController{
            if let objNew = self.newScanViewModel.qrScanObj{
                    ViewController.qrScanObj = objNew
                   //ViewController.qrScanObj = NewScanQRModel(data: ["MerchantName":"Handsome Tushar","RefNumber":"1234567","Amount":"1","ServiceFeeByProvider":"1","TotalAmount":"2","Remarks": "The best developer"] , code: 200, msg: "Success")
                    self.navigationController?.pushViewController(ViewController, animated: true)
               }
                
            }
        }
    }
    
    
    //This function will details of the user after scanning the QR
    fileprivate func getQRScanDetail(encrypted: String,iVector: String,merchantID: String) {
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            newScanViewModel.getDataForQRScan(merchantQr: encrypted,iVector: iVector,merchantID: merchantID,finished:{
                progressViewObj.removeProgressView()
                //checking if data is available
                if let obj = self.newScanViewModel.qrScanObj{
                    DispatchQueue.main.async { [weak self] in
                        if obj.code ?? 0 == 200{
                            DispatchQueue.main.async {
                                self?.navigate()
                            }
                        }else{
                            
                            self?.showErrorAlert(errMessage: obj.message ?? "")
                        }
                    }
                }else{
                    //some problem
                }
            })
        } else {
            self.noInternetAlert()
            progressViewObj.removeProgressView()
        }
    }
    
    
      @objc func appWillResignActive() {
           
            self.flashImageView.isHighlighted = false
            if let device = AVCaptureDevice.default(for: AVMediaType.video) {
                if (device.hasTorch) {
                    do {
                        try device.lockForConfiguration()
                        if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                            self.flashImageView.isHighlighted = false
                            device.torchMode = AVCaptureDevice.TorchMode.off
                        }
    //                    else {
    //                        self.flashImageView.isHighlighted = true
    //                        do {
    //                            try device.setTorchModeOn(level: 1.0)
    //                        } catch {
    //                            println_debug(error)
    //                        }
    //                    }
                        device.unlockForConfiguration()
                    } catch {
                        println_debug(error)
                    }
                }
            }
            
            
        }
    
    @IBAction func onClickFlashButton(_ sender: Any) {
        if let device = AVCaptureDevice.default(for: AVMediaType.video) {
            if (device.hasTorch) {
                do {
                    try device.lockForConfiguration()
                    if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                        self.flashImageView.isHighlighted = false
                        device.torchMode = AVCaptureDevice.TorchMode.off
                    } else {
                        self.flashImageView.isHighlighted = true
                        do {
                            try device.setTorchModeOn(level: 1.0)
                        } catch {
                            println_debug(error)
                        }
                    }
                    device.unlockForConfiguration()
                } catch {
                    println_debug(error)
                }
            }
        }
    }
    
    
    @IBAction func onClickReportButton(_ sender: Any) {
        self.showErrorAlert(errMessage: "No Data Available")
    }
    
    fileprivate func loadCamera(){
           view.backgroundColor = UIColor.black
           captureSession = AVCaptureSession()
           
           guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
           let videoInput: AVCaptureDeviceInput
           
           do {
               videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
           } catch {
               return
           }
           
           if (captureSession.canAddInput(videoInput)) {
               captureSession.addInput(videoInput)
           } else {
               failed()
               return
           }
           
           let metadataOutput = AVCaptureMetadataOutput()
           
           if (captureSession.canAddOutput(metadataOutput)) {
               captureSession.addOutput(metadataOutput)
               
               metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
               metadataOutput.metadataObjectTypes = [.qr]
           } else {
               failed()
               return
           }
           
           previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
          // previewLayer.frame = CGRect(x: 0, y: 35, width: self.view.frame.size.width, height: self.view.frame.size.height - 80)
        previewLayer.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height - 80)

           previewLayer.videoGravity = .resizeAspectFill
           view.layer.addSublayer(previewLayer)
           view.addSubview(flashView)
           view.addSubview(nextButton)
           view.addSubview(ReportView)
           view.bringSubviewToFront(flashView)
          view.bringSubviewToFront(nextButton)
           view.bringSubviewToFront(ReportView)
           captureSession.startRunning()
       }

    
    func failed() {
           let ac = UIAlertController(title: "Scanning not supported".localized, message: "Your device does not support scanning a code from an item. Please use a device with a camera.".localized, preferredStyle: .alert)
           ac.addAction(UIAlertAction(title: "OK", style: .default))
           present(ac, animated: true)
           captureSession = nil
       }
    
    func found(code: String) {
        
    }
}


extension NewScanToPayViewController: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
    }
}
