//
//  NewScanToPayViewModel.swift
//  OK
//
//  Created by Tushar Lama on 31/08/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

class NewScanToPayViewModel{
    var createConnectionObj = NearByConnection()
    var qrScanObj: NewScanQRModel?
    var qrAfterPaymentModel: NewScanPayModel?
    var cancelQRDic: NSDictionary?
    var cancelCode: Int?
    var canCelMsg: String?
    
    //this function will help us get the QR for user
    func getDataForQRScan(merchantQr: String,iVector: String,merchantID: String,finished:@escaping () -> Void){
       
        createConnectionObj.getDataFromQR(qrString: merchantQr, iVector: iVector, merchantId: merchantID, completion: { [weak self]
            (response,error) in
            if let ifThereIsError = error{
                //no error
                if !ifThereIsError{
                    self?.qrScanObj = nil
                    self?.qrScanObj = response
                    finished()
                }else{
                    finished()
                }
            }
        })
    }
    
    
    //this function will help to cancel QR
    func cancelScannedQR(referenceNumber: String,finished:@escaping () -> Void){
       
        createConnectionObj.cancelQRData(refernceNumber: referenceNumber, completion: { [weak self]
            (response,error) in
            if let ifThereIsError = error{
                //no error
                if !ifThereIsError{
                    self?.cancelQRDic = nil
                    self?.cancelQRDic = response
                    if self?.cancelQRDic?.count ?? 0 > 0{
                        self?.cancelCode = self?.cancelQRDic?.value(forKey: "Code") as? Int
                        self?.canCelMsg = self?.cancelQRDic?.value(forKey: "Msg") as? String
                    }
                    finished()
                }else{
                    finished()
                }
            }
        })
    }
    
    
    
    
    //this function will help us get the QR for user
       func sendDataForQRScan(refernceNumber: String,comments: String,finished:@escaping () -> Void){
         //  qrAfterPaymentModel = nil
           
        createConnectionObj.sendQRData(refernceNumber: refernceNumber,comments: comments, completion: { [weak self]
               (response,error) in
               if let ifThereIsError = error{
                   //no error
                   if !ifThereIsError{
                    self?.qrAfterPaymentModel = nil
                      self?.qrAfterPaymentModel = response
                       finished()
                   }else{
                       finished()
                   }
               }
           })
       }
    
}
