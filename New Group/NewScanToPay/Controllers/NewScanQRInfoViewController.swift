//
//  NewScanQRInfoViewController.swift
//  OK
//
//  Created by Tushar Lama on 28/08/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class NewScanQRInfoViewController: OKBaseController {
    
    @IBOutlet var makePaymentButton: UIButton!
    @IBOutlet var infoTable: UITableView!
    var qrScanObj: NewScanQRModel?
    var merchantData = [String]()
    var  alertMsg = ""
    var referenceNumber = ""
    var newScanViewModel = NewScanToPayViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "SCAN / PAY".localized
        self.navigationController?.navigationBar.tintColor = UIColor.white
        makePaymentButton.titleLabel?.font = UitilityClass.getZwagiFontWithSize(size: appButtonSize)
        makePaymentButton.setTitle("Make Payment".localized, for: .normal)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
        
        if let validData = qrScanObj{
            alertMsg = validData.alertMsg ?? ""
            referenceNumber = validData.referenceNumber ?? ""
            merchantData = NewScanToPayHelper.getMerchantDetails(qrScanObj: validData)
            self.infoTable.backgroundColor = .white
            self.infoTable.register(UINib(nibName: NearByConstant.kNearByQRInfoCell, bundle: nil), forCellReuseIdentifier: NearByConstant.kNearByQRInfoCell)
            self.infoTable.separatorColor = .clear
            self.infoTable.delegate = self
            self.infoTable.dataSource  = self
            self.infoTable.reloadData()
        }
        
        
        let button1 = UIBarButtonItem(image: UIImage(named: "backButton"), style: .plain, target: self, action: #selector(self.backButtonTapped))
        self.navigationItem.leftBarButtonItem  = button1
        

    }
    
    @objc func backButtonTapped() {
        
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "", body: self.alertMsg , img: #imageLiteral(resourceName: "NearMeNew"))
            alertViewObj.addAction(title: "YES".localized, style: .target , action: {
                self.cancelQRScanDetail(referenceNum: self.referenceNumber)
            })
            alertViewObj.addAction(title: "NO".localized, style: .target , action: {
                
            })
            alertViewObj.showAlert(controller: self)
        }
    }
    
  
    
    fileprivate func cancelQRScanDetail(referenceNum: String) {
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            newScanViewModel.cancelScannedQR(referenceNumber: referenceNum,finished:{
                progressViewObj.removeProgressView()
                //checking if data is available
                DispatchQueue.main.async { [weak self] in
                    if self?.newScanViewModel.cancelCode ?? 0 == 200{
                        DispatchQueue.main.async {
                            self?.navigationController?.popViewController(animated: false)
                        }
                    }else{
                        
                        DispatchQueue.main.async {
                            self?.navigationController?.popViewController(animated: false)
                        }
//                        alertViewObj.wrapAlert(title: "", body: self?.newScanViewModel.canCelMsg ?? "", img: nil)
//                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                            self?.navigationController?.popViewController(animated: false)
//                        })
//                        alertViewObj.showAlert(controller: self)
                    }
                    
                }
            })
        } else {
            self.noInternetAlert()
            progressViewObj.removeProgressView()
        }
    }
    
    
    
    @IBAction func onClickMakePayment(_ sender: Any) {
        OKPayment.main.authenticate(screenName: "Tushar", delegate: self)
    }
    
}

extension NewScanQRInfoViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return NewScanToPayHelper.kMerchantTitleInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NearByConstant.kNearByQRInfoCell) as? NearByQRInfoCell else{
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        cell.merchantInfoImageView.image = UIImage(named: NewScanToPayHelper.kScanToPayImages[indexPath.row])
        cell.imageViewWidth.constant = 30.0
        cell.imageViewLeading.constant = 15.0
        let height =  heightForView(text:merchantData[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 14.0), width: screenWidth)
        if height <= 22.0{
            cell.merchantInfoDetailLabelHeight.constant = 22.0
        }else{
            cell.merchantInfoDetailLabelHeight.constant = height
        }
        cell.merchantInfoTitleLabel.textColor = kDarkBlueColor
        cell.merchantInfoTitleLabel.text = NewScanToPayHelper.kMerchantTitleInfo[indexPath.row]
        if merchantData.indices.contains(indexPath.row){
            cell.merchantInfoDetailLabel.text = merchantData[indexPath.row]
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if merchantData.indices.contains(indexPath.row){
            let height =  heightForView(text: merchantData[indexPath.row], font: UitilityClass.getZwagiFontWithSize(size: 16.0), width: screenWidth)
            if height <= 22.0{
                return 75.0
            }else{
                let newHeight = height - 22.0
                return 75.0 + newHeight
            }
        }else{
            return 0.0
        }
        
    }
}

extension NewScanQRInfoViewController: BioMetricLoginDelegate{
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String){
        if isSuccessful{
            if #available(iOS 13.0, *) {
                if let obj = self.storyboard?.instantiateViewController(identifier: NewScanToPayHelper.kNewScanFinalViewController) as? NewScanFinalViewController{
                    obj.merchantData = merchantData
                    if let data = qrScanObj{
                        obj.qrScanObj = data
                    }
                    self.navigationController?.pushViewController(obj, animated: true)
                }
            } else {
                if let ViewController = self.storyboard?.instantiateViewController(withIdentifier: NewScanToPayHelper.kNewScanFinalViewController) as? NewScanFinalViewController{
                    ViewController.merchantData = merchantData
                    if let data = qrScanObj{
                        ViewController.qrScanObj = data
                    }
                    self.navigationController?.pushViewController(ViewController, animated: true)
                }
            }
        }
    }
}
