//
//  NewScanFinalViewController.swift
//  OK
//
//  Created by Tushar Lama on 28/08/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class NewScanFinalViewController: OKBaseController {

    @IBOutlet var finalScanTableView: UITableView!
     var isButtonTapped = false
    var merchantData = [String]()
    var newScanViewModel = NewScanToPayViewModel()
    var qrScanObj: NewScanQRModel?
    var isPayClicked = false
    @IBOutlet var paybuttonOutlet: UIButton!
    @IBOutlet var checkDetailsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        self.title = "SCAN / PAY".localized
        checkDetailsLabel.text = "Check Details".localized
        
        paybuttonOutlet.titleLabel?.font = UitilityClass.getZwagiFontWithSize(size: appButtonSize)
        paybuttonOutlet.setTitle("Pay".localized, for: .normal)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]

        
        
        self.finalScanTableView.backgroundColor = .white
        self.finalScanTableView.register(UINib(nibName: NearByConstant.kNearByPaymentCell, bundle: nil), forCellReuseIdentifier: NearByConstant.kNearByPaymentCell)
        self.finalScanTableView.separatorColor = .clear
        self.finalScanTableView.delegate = self
        self.finalScanTableView.dataSource  = self
        self.finalScanTableView.reloadData()
    }
    
    @IBAction func onClickPay(_ sender: Any) {
       payQRScanDetail()
   }
    
    func navigate(){
        if #available(iOS 13.0, *) {
            if let obj = self.storyboard?.instantiateViewController(identifier: NewScanToPayHelper.kNewScanToPayReceiptViewController) as? NewScanToPayReceiptViewController{
                if let data = self.newScanViewModel.qrAfterPaymentModel{
                   obj.payModelObj = data
                    self.navigationController?.pushViewController(obj, animated: true)
                }
            }
        } else {
            if let ViewController = self.storyboard?.instantiateViewController(withIdentifier: NewScanToPayHelper.kNewScanToPayReceiptViewController) as? NewScanToPayReceiptViewController{
                if let data = self.newScanViewModel.qrAfterPaymentModel{
                   ViewController.payModelObj = data
                    self.navigationController?.pushViewController(ViewController, animated: true)
                }
            }
        }
    }
    
    
    //This function will details of the user after scanning the QR
    fileprivate func payQRScanDetail() {
        
        if !isPayClicked{
            let balanceAmount = Double(UserLogin.shared.walletBal)
            let billAmountIntValue = Double(qrScanObj?.totalAmount ?? "0") ?? 0.0
            
            if balanceAmount ?? 0.0 >= billAmountIntValue{
                if appDelegate.checkNetworkAvail() {
                    isPayClicked = true
                    progressViewObj.showProgressView()
                    newScanViewModel.sendDataForQRScan(refernceNumber: qrScanObj?.referenceNumber ?? "",comments: qrScanObj?.remarks ?? "",finished:{
                        progressViewObj.removeProgressView()
                        //checking if data is available
                        if let obj = self.newScanViewModel.qrAfterPaymentModel{
                            self.isPayClicked = false
                            DispatchQueue.main.async { [weak self] in
                                if obj.code ?? 0 == 200{
                                    DispatchQueue.main.async {
                                        self?.navigate()
                                    }
                                }else{
                                    self?.showErrorAlert(errMessage: obj.message ?? "")
                                }
                            }
                        }else{
                            self.isPayClicked = false
                        }
                    })
                } else {
                    self.noInternetAlert()
                    progressViewObj.removeProgressView()
                }
                
            }else{
                //show alert
                self.showErrorAlert(errMessage: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized)
            }
            
            
        }
    }
    
}

extension NewScanFinalViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NewScanToPayHelper.kMerchantTitleInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: NearByConstant.kNearByQRInfoCell) as? NearByQRInfoCell else{
//            return UITableViewCell()
//        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NearByConstant.kNearByPaymentCell) as? NearByPaymentCell else{
            return UITableViewCell()
        }
        
        
//        cell.selectionStyle = .none
//        cell.merchantInfoImageView.image = UIImage(named: NewScanToPayHelper.kScanToPayImages[indexPath.row])
//        cell.imageViewWidth.constant = 30.0
//        cell.imageViewLeading.constant = 15.0
//        let height =  heightForView(text:merchantData[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 14.0), width: screenWidth)
//        if height <= 22.0{
//            cell.merchantInfoDetailLabelHeight.constant = 22.0
//        }else{
//            cell.merchantInfoDetailLabelHeight.constant = height
//        }
//        cell.merchantInfoTitleLabel.textColor = kDarkBlueColor
//        cell.merchantInfoTitleLabel.text = NewScanToPayHelper.kMerchantTitleInfo[indexPath.row]
//        cell.merchantInfoDetailLabel.text = merchantData[indexPath.row]
        
        if merchantData.indices.contains(indexPath.row){
                       let height =  heightForView(text:merchantData[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 13.0), width: screenWidth)
                       if  NewScanToPayHelper.kMerchantTitleInfo.indices.contains(indexPath.row){
                           let titleHeight = heightForView(text:NewScanToPayHelper.kMerchantTitleInfo[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 13.0), width: screenWidth)
                           if height>titleHeight{
                               if height<=19.0{
                                   cell.merchantLabelHeight.constant = 20.0
                               }else{
                                   cell.merchantLabelHeight.constant = height + 10
                               }
                           }else{
                               if titleHeight<=20.0{
                                   cell.merchantLabelHeight.constant = 20.0
                               }else{
                                   cell.merchantLabelHeight.constant = titleHeight
                               }
                           }
                           cell.titleLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
                           cell.titleLabel.text = NewScanToPayHelper.kMerchantTitleInfo[indexPath.row]
                       }
                       cell.merchantValueLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
                       cell.merchantValueLabel.text = merchantData[indexPath.row]
                   }
        
        return cell
        
    }
    
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//            if merchantData.indices.contains(indexPath.row){
//                let height =  heightForView(text: merchantData[indexPath.row], font: UitilityClass.getZwagiFontWithSize(size: 16.0), width: screenWidth)
//                if height <= 22.0{
//                    return 75.0
//                }else{
//                    let newHeight = height - 22.0
//                    return 75.0 + newHeight
//                }
//            }else{
//                return 0.0
//            }
        //}
    
    
    if merchantData.indices.contains(indexPath.row){
        let height =  heightForView(text:merchantData[indexPath.row] , font: UitilityClass.getZwagiFontWithSize(size: 16.0), width: screenWidth)
        if NewScanToPayHelper.kMerchantTitleInfo.indices.contains(indexPath.row){
            let infoHeight = heightForView(text:NewScanToPayHelper.kMerchantTitleInfo[indexPath.row] , font:UitilityClass.getZwagiFontWithSize(size: 16.0), width: screenWidth)
            
            if infoHeight>height{
                if infoHeight<=19.0{
                    return 40.0
                }else{
                    let newHeight = infoHeight - 10.0
                    return 40.0 + newHeight
                }
            }else{
                if height<=19.0{
                    return 40.0
                }else{
                    let newHeight = height - 10.0
                    return 40.0 + newHeight
                }
            }
        }
    }
        return 0.0
}

}
