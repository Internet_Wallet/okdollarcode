//
//  NewScanPayParameter.swift
//  OK
//
//  Created by Tushar Lama on 31/08/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation


struct NewScanPayParameter {
    
    //this param will help to get user data based on QR encrypted data
    static func newScanDataQRParam(qrString: String,iVector: String,merchantID: String) -> NSMutableDictionary{
       
        let userData = NSMutableDictionary()
        userData.setValue(UserModel.shared.mobileNo, forKey: "OkAccNumber")
        userData.setValue(ok_password ?? "", forKey: "Password")
        userData.setValue(UserLogin.shared.token, forKey: "SecureToken")
        userData.setValue(1, forKey: "OsType")
        userData.setValue(buildNumber, forKey: "BuildNumber")
        userData.setValue(buildVersion, forKey: "AppVersion")
        
        let merchantQRInfo = NSMutableDictionary()
        merchantQRInfo.setValue(qrString, forKey: "EncryptedString")
        merchantQRInfo.setValue(iVector, forKey: "Ivector")
        merchantQRInfo.setValue(merchantID, forKey: "MerchantId")
        
        let mainDic = NSMutableDictionary()
        mainDic.setValue(userData, forKey: "AuthInfo")
        mainDic.setValue(merchantQRInfo, forKey: "MerInfo")
                
        return mainDic
    
    }
    
    
    //this will be called when the user cancel  the QR
    static func newScanDataCancelSendQRInfo(refernceNumber: String) -> NSMutableDictionary{
        let loginDic = NSMutableDictionary()
        loginDic.setValue(UserModel.shared.mobileNo, forKey: "MobileNumber")
        loginDic.setValue(uuid, forKey: "Simid")
        loginDic.setValue(msid, forKey: "Msid")
        loginDic.setValue(1, forKey: "Ostype")
        loginDic.setValue("", forKey: "Otp")
        loginDic.setValue(UserModel.shared.appID, forKey: "AppId")
        loginDic.setValue(0, forKey: "Limit")
        loginDic.setValue(0, forKey: "Offset")
        
        let mainDic = NSMutableDictionary()
        mainDic.setValue(loginDic, forKey: "loginAuthDto")
        mainDic.setValue(refernceNumber, forKey: "RefNumber")
        
        return mainDic
    }
    
    
    //this will be called when the user will scan the QR and will pay the amount
    static func newScanDataSendQRInfo(refernceNumber: String,comments: String) -> NSMutableDictionary{
        let userData = NSMutableDictionary()
        userData.setValue(UserModel.shared.mobileNo, forKey: "OkAccNumber")
        userData.setValue(ok_password ?? "", forKey: "Password")
        userData.setValue(UserLogin.shared.token, forKey: "SecureToken")
        userData.setValue(1, forKey: "OsType")
        userData.setValue(buildNumber, forKey: "BuildNumber")
        userData.setValue(buildVersion, forKey: "AppVersion")
        
        let mainDic = NSMutableDictionary()
        mainDic.setValue(userData, forKey: "ObjLoginInfo")
        mainDic.setValue(refernceNumber, forKey: "RefNum")
        mainDic.setValue(comments, forKey: "Comments")
        
        return mainDic
    }
        
    
}

