//
//  NewScanToPayHelper.swift
//  OK
//
//  Created by Tushar Lama on 28/08/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

struct NewScanToPayHelper {
    
    static let KNewScanToPayViewController = "NewScanToPayViewController"
    static let kNewScanQRInfoViewController = "NewScanQRInfoViewController"
    static let kNewScanFinalViewController = "NewScanFinalViewController"
    static let kNewScanToPayReceiptViewController = "NewScanToPayReceiptViewController"
    static let kMerchantTitleInfo = ["Merchant Name".localized,"Reference Number".localized,"Service Fee".localized,"Amount".localized, "Total Amount".localized, "Remarks".localized]
    static let kScanToPayImages = ["merchantIcon.png","merchantReferenceNumber.png","merchantAmount.png","merchantService.png","merchantAmount.png","merchantRemarkNew.png"]
    static let scanPayTESTURL = "http://69.160.4.151:8001/RestService.svc/"
    static let scanPayProURL = "http://www.okdollar.co/RestService.svc/"
    static let scanPayFinal = scanPayProURL
    
    
    static func getUserInfoFromQR() -> String {
        if serverUrl == .productionUrl{
            return scanPayProURL + "GetDecrytDataForQrPay"
        }else{
            return scanPayTESTURL + "GetDecrytDataForQrPay"
        }
    }
    
    static func sendUserInfoFromQR() -> String {
        if serverUrl == .productionUrl{
            return scanPayProURL + "ProcessPayForQrFromUser"
        }else{
            return scanPayTESTURL + "ProcessPayForQrFromUser"
        }
    }
    
    static func UserCancelQR() -> String {
        if serverUrl == .productionUrl{
            return scanPayProURL + "CancelQrTransactionByUser"
        }else{
            return scanPayTESTURL + "CancelQrTransactionByUser"
        }
    }
    
    
    static func getMerchantDetails(qrScanObj: NewScanQRModel) -> [String]{
        var merchantData = [String]()
        merchantData.insert(qrScanObj.merchantName ?? "tushar", at: 0)
        merchantData.insert(qrScanObj.referenceNumber ?? "tushar", at: 1)
        merchantData.insert(qrScanObj.serviceFeeByProvider ?? "tushar", at: 2)
        merchantData.insert(qrScanObj.amount ?? "tushar", at: 3)
        merchantData.insert(qrScanObj.totalAmount ?? "tushar", at: 4)
        merchantData.insert(qrScanObj.remarks ?? "tushar", at: 5)
        return merchantData
    }
    
    
    static func getMerchantFinalDetails(payModelObj: NewScanPayModel) -> [String]{
        var merchantData = [String]()
        merchantData.insert(payModelObj.merchantName ?? "tushar", at: 0)
        merchantData.insert(payModelObj.transactionID ?? "tushar", at: 1)
        merchantData.insert(payModelObj.referenceNumber ?? "tushar", at: 2)
        merchantData.insert(payModelObj.serviceFeeAmount ?? "tushar", at: 3)
        merchantData.insert(payModelObj.amount ?? "tushar", at: 4)
        merchantData.insert(payModelObj.totalAmount ?? "tushar", at: 5)
        merchantData.insert(payModelObj.remarks ?? " - ", at: 6)
        return merchantData
    }
    
    static func getDataForPdf(payModelObj: NewScanPayModel) -> [Dictionary<String,String>]? {
        var listTitleValue : [Dictionary<String,String>]?
        listTitleValue = [
            ["title" : "OK$ Name".localized ,"value" : "\(UserModel.shared.name) \(UserModel.shared.phoneNumber)"],
        ["title" : "Transaction Date and Time".localized ,"value" : payModelObj.transactionTime ?? ""],
        ["title" : "Merchant Name".localized ,"value" : payModelObj.merchantName ?? ""],
        ["title" : "Transaction ID".localized ,"value" : payModelObj.transactionID ?? ""],
        ["title" : "Reference Number".localized ,"value" :  payModelObj.referenceNumber ?? ""], //self.values?[2] ??
            ["title" : "Service Fee".localized ,"value" : payModelObj.serviceFeeAmount ?? ""],
        ["title" : "Amount".localized ,"value" : payModelObj.amount ?? ""],
        ["title" : "Total Amount".localized ,"value" :payModelObj.totalAmount ?? ""],
        ["title" : "OK$ Balance".localized ,"value" : payModelObj.walletBalance ?? ""],
        ["title" : "Remarks".localized ,"value" : payModelObj.remarks ?? " - "]]
        return listTitleValue
    }
    
    
    
}
