//
//  NewScanQRModel.swift
//  OK
//
//  Created by Tushar Lama on 31/08/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

struct NewScanQRModel {
    var code: Int?
    var message: String?
    var merchantName: String?
    var referenceNumber: String?
    var amount: String?
    var serviceFeeByProvider: String?
    var totalAmount: String?
    var remarks: String?
    var alertMsg: String?
    
    public init(data: [String: Any]?,code:Int?,msg: String?){
           let codeData = code
           if let isValidCode = codeData{
               self.code = isValidCode
           }
           
           if let isValidMsg = msg{
               self.message = isValidMsg
           }
           
           if let isValidObj = data{
               if isValidObj.count>0{
                   self.merchantName = isValidObj["MerchantName"] as? String ?? ""
                   self.referenceNumber = isValidObj["RefNumber"] as? String ?? ""
                   self.amount = wrapAmountWithCommaDecimal(key: isValidObj["Amount"] as? String ?? "" ) + " " + "MMK"
                   self.serviceFeeByProvider = wrapAmountWithCommaDecimal(key: isValidObj["ServiceFeeByProvider"] as? String ?? "" ) + " " + "MMK"
                   self.totalAmount = wrapAmountWithCommaDecimal(key: isValidObj["TotalAmount"] as? String ?? "" ) + " " + "MMK"
                   self.remarks = isValidObj["Remarks"] as? String ?? ""
                  self.alertMsg = isValidObj["UserAlertMessage"] as? String ?? ""
               }
           }
       }
    
    
    
    
}


struct NewScanPayModel {
    var code: Int?
    var message: String?
   
    var merchantName: String?
    var referenceNumber: String?
    var amount: String?
    var serviceFeeAmount: String?
    var totalAmount: String?
    var remarks: String?
    var transactionID: String?
    var transactionTime: String?
    var walletBalance: String?
    var comments: String?
    
    
    public init(data: [String: Any]?,code:Int?,msg: String?){
           let codeData = code
           if let isValidCode = codeData{
               self.code = isValidCode
           }
           
           if let isValidMsg = msg{
               self.message = isValidMsg
           }
           
           if let isValidObj = data{
               if isValidObj.count>0{
                   self.transactionID = isValidObj["TransactionId"] as? String ?? ""
                   self.transactionTime = isValidObj["TransTime"] as? String ?? ""
                   self.amount =  wrapAmountWithCommaDecimal(key: isValidObj["Amount"] as? String ?? "") + " " + "MMK"
                   self.serviceFeeAmount = wrapAmountWithCommaDecimal(key: isValidObj["ServiceFeeAmount"] as? String ?? "") + " " + "MMK"
                   self.totalAmount = wrapAmountWithCommaDecimal(key: isValidObj["TotalAmount"] as? String ?? "") + " " + "MMK"
                   self.walletBalance = wrapAmountWithCommaDecimal(key: isValidObj["WalletBalance"] as? String ?? "") + " " + "MMK"
                self.merchantName = isValidObj["MerchantName"] as? String ?? ""
                self.referenceNumber = isValidObj["RefNumber"] as? String ?? ""
                self.comments = isValidObj["Comments"] as? String ?? ""
                self.remarks = isValidObj["Remarks"] as? String ?? " - "
               }
           }
       }
    
}



//{
//    Code = 200;
//    Data = "{\"TransactionId\":\"2009463832\",\"TransTime\":\"09-Sep-2020 15:15:39\",\"Amount\":1.0000,\"ServiceFeeAmount\":1.0000,\"TotalAmount\":2.0000,\"WalletBalance\":1054979.00,\"MerchantName\":\"DIAC\",\"RefNumber\":\"testref3\",\"Comments\":\"test remarks\",\"Remarks\":null}";
//    Msg = "Transaction Success...";
//}





