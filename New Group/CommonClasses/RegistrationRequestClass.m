//
//  RegistrationRequestClass.m
//  OK
//
//  Created by Subhash Arya on 02/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

#import "RegistrationRequestClass.h"

@implementation RegistrationRequestClass

-(void)callAPIwithURL:(NSURL *)url andParmaDic:(NSDictionary *)paramDic {
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration];
    
    // Setup the request with URL
   // NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
 
    // Convert POST string parameters to data using UTF8 Encoding
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:paramDic options:0 error:&err];
    NSString * inputString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    // Convert POST string parameters to data using UTF8 Encoding
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
    [urlRequest setHTTPBody:[inputString dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // Handle your response here
        NSLog(@"Responnse === %@",response.description);
        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                     options:kNilOptions
                                                                       error:&error];
        NSLog(@"json Data : %@", jsonResponse);
        
        
    }];
    
    // Fire the request
    [dataTask resume];
}

-(void)callAPIwithURL:(NSURL *)url andParmaDic:(NSDictionary *)paramDic withCompletionHandler:(void (^)(NSInteger))finishBlock {
    self.complete(1);
    }

@end

/*
 
 
 NSData * jsonData = [NSJSONSerialization dataWithJSONObject:sendtoServerDict options:0 error:&err];
 NSString * inputString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
 NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
 
 [theRequest setValue:@"application/json" forHTTPHeaderField:@"content-type"];
 [theRequest setHTTPMethod:@"POST"];
 [theRequest setHTTPBody:[inputString dataUsingEncoding:NSUTF8StringEncoding]];
 */
