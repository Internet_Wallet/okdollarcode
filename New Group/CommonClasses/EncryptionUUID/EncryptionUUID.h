//
//  EncryptionUUID.h
//  EncryptionLogic
//
//  Created by SSL on 11/7/15.
//  Copyright © 2015 CGM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EncryptionUUID : NSObject

+(id)Encryptionmanager;

-(NSString *)entryptNumber:(NSString *) number;

-(NSString *) decryptNumber:(NSString *)number;

@end
