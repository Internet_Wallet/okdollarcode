//
//  ProgressView.swift
//  OK
//
//  Created by Uma Rajendran on 10/31/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ProgressView: NSObject {
    
    //#MARK:- Alert View Methods
    func showProgressView()  {
        DispatchQueue.main.async {
            
            PTLoader.shared.show()
        }
    }
    
    func removeProgressView() {
        DispatchQueue.main.async {
            PTLoader.shared.hide()
        }
    }
    //#MARK:- End of Alert View Methods

}
