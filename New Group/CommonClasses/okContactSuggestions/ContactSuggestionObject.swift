//
//  ContactSuggestionObject.swift
//  OK
//
//  Created by Ashish on 2/8/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

let suggestedContact = ContactSuggestionObject()

class ContactSuggestionObject: NSObject {
    
   private var filteredArray = [SuggestedContacts]()

     func showContactSuggestion(havingSearchString text: String) -> [SuggestedContacts] {
        
        if okContacts.count == 0 {
            getAllContacts()
        }

        DispatchQueue.global(qos: .background).async {
            for contact in okContacts {
                for number in contact.phoneNumbers {
                    if number.value.stringValue.contains(find: text) {
                        let contactModel = SuggestedContacts.init(contact.givenName, phone: number.value.stringValue)
                        self.filteredArray.append(contactModel)
                    }
                }
            }
        }
        
        self.filteredArray = Array(Set(self.filteredArray))
        self.filteredArray = self.filteredArray.filter({ $0.phone.contains(find: text) })
        return self.filteredArray
    }
}

struct SuggestedContacts: Hashable, Equatable {
    
    var hashValue: Int
    
    static func ==(lhs: SuggestedContacts, rhs: SuggestedContacts) -> Bool {
        return lhs.phone == rhs.phone
    }
    
    var name  = ""
    var phone = ""
    
    init(_ name: String, phone: String) {
        self.name  = name
        self.phone = phone
        self.hashValue = self.phone.hashValue
    }
}

