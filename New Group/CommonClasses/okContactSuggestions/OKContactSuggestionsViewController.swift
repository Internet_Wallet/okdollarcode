//
//  OKContactSuggestionsViewController.swift
//  OK
//
//  Created by Ashish on 2/8/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Contacts

protocol ContactSugesstionDelegate {
    func selectedSuggestedContact(contact: SuggestedContacts)
}

struct SuggestedContacts: Hashable, Equatable {
    
    var hashValue: Int
    
    static func ==(lhs: SuggestedContacts, rhs: SuggestedContacts) -> Bool {
        return lhs.phone == rhs.phone
    }
    
    var name  = ""
    var phone = ""
    
    init(_ name: String, phone: String) {
        self.name  = name
        self.phone = phone
        self.hashValue = self.phone.hashValue
    }
}

class OKContactSuggestionsViewController: OKBaseController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var contactTableView: UITableView!
    
    var contactArray  = Array<CNContact>()
    var filteredArray = [SuggestedContacts]()
    var searchText = ""
    var delegate : ContactSugesstionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.contactTableView.register(UINib.init(nibName: String.init(describing: OKContactSuggestionTableViewCell.self), bundle: nil), forCellReuseIdentifier: String.init(describing: OKContactSuggestionTableViewCell.self))
        contactArray = okContacts
        self.contactTableView.delegate = self
        self.contactTableView.dataSource = self
    }
    
    func filterArray(withString string: String) {
        searchText = string
        DispatchQueue.global(qos: .background).async {
            for contact in self.contactArray {
                for number in contact.phoneNumbers {
                    if number.value.stringValue.contains(find: string) {
                        let contactModel = SuggestedContacts.init(contact.givenName, phone: number.value.stringValue)
                        self.filteredArray.append(contactModel)
                    }
                }
            }
        }

        self.filteredArray = Array(Set(self.filteredArray))
        self.filteredArray = self.filteredArray.filter({ $0.phone.contains(find: string) })
        self.contactTableView.reloadData()

        if self.filteredArray.count == 0 {
            self.view.removeFromSuperview()
            return
        }
        
    }

    //MARK:- UItableView Delegates and Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: OKContactSuggestionTableViewCell.self), for: indexPath) as? OKContactSuggestionTableViewCell
        let contact = self.filteredArray[indexPath.row]
        cell?.wrapContacts(contact: contact, serachText: searchText)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.00
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let delegate = self.delegate {
            delegate.selectedSuggestedContact(contact: self.filteredArray[indexPath.row])
        }
        self.view.removeFromSuperview()
    }
    
}



