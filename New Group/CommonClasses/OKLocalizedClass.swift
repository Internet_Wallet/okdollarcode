//
//  OKLocalizedClass.swift
//  OK
//
//  Created by Ashish on 3/10/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

// Localised class for uilabel
class LocalizedLabel : UILabel {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        NotificationCenter.default.addObserver(self, selector: #selector(myLanguageVersion), name: NSNotification.Name(rawValue: "stringLocalised"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "stringLocalised"), object: nil)
    }
    
    @objc fileprivate func myLanguageVersion(){
        self.text = appDelegate.getlocaLizationLanguage(key: self.text ?? "")
    }

}

// Localised class for UITextField
class LocalizedTextField : UITextField {
    
    override var placeholder: String? {
        didSet {
            if oldValue != self.placeholder {
                self.myLanguageVersion()
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
  fileprivate func myLanguageVersion() {
//        self.placeholder = appDelegate.getlocaLizationLanguage(key: self.placeholder ?? "")
    }
}

// Localised class for UITextField -> Restricted Language Input
class RestrictedLocalisedTextField : LocalizedTextField {
    
    override var text: String? {
        didSet {
            self.myRestrictedLanguage()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    fileprivate func myRestrictedLanguage() -> Void {
        if appDelegate.currentLanguage == "en" {
            self.text = self.text!.containsAlphabets() ?  self.text : ""
        }  else {
            self.text = self.text!.containsAlphabets() ?  self.text : ""
        }
    }
}

// Localised class for UIButton
class LocalizedUIButton : UIButton {
    
    override func setTitle(_ title: String?, for state: UIControl.State) {
        self.myLocalisedTitle(state, title: title)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    fileprivate func myLocalisedTitle(_ state: UIControl.State, title: String?) -> Void {
        let title = appDelegate.getlocaLizationLanguage(key: title ?? "")
        self.setTitle(title, for: state)
    }
}

