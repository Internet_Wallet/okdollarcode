//
//  UitilityClass.swift
//  OfferScreen
//
//  Created by CGM on 5/10/17.
//  Copyright © 2017 CGM. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration.CaptiveNetwork
import CoreTelephony
import AdSupport


class UitilityClass: NSObject {
    
    //getting current date in string as per desired format
    static func getStringDateInCustomFormat(dateFormat: String)-> String{
        let date : Date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: date)
    }

    
    static func displayAlert(title: String, message: String,view: UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: nil))
        view.present(alert, animated: true)
    }
    
    
    //Call this method to create imageview with image programatically
    static func createImageViewWithImage(imageName: String,x: Int ,y: Int,width: Int ,height: Int) -> UIImageView{
        let image = UIImage(named:imageName)?.withRenderingMode(.alwaysTemplate)
        let imageViewObj  = UIImageView(frame:CGRect(x:x, y:y, width:width, height:height));
        imageViewObj.image = image
        return imageViewObj
    }
    
    static func updateTextColor(textField: UITextField) {
        if var text = textField.text {
            text = text.replacingOccurrences(of: ",", with: "")
            if (text as NSString).floatValue > 9999999.0 {
                textField.textColor = .red
            } else if (text as NSString).floatValue > 999999.0 && (text as NSString).floatValue <= 9999999.0 {
               textField.textColor = .green
            } else {
                textField.textColor = .black
            }
        }
    }
    
    static var appVersion: String {
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
    }
    
    
    class func getStoryBoardNearBy() -> UIStoryboard {
        return UIStoryboard(name: "NearBy", bundle: nil)
    }
    
    class func getStoryBoardAddWithdraw() -> UIStoryboard {
        return UIStoryboard(name: "AddWithdraw", bundle: nil)
    }
    
    class func addChildVC(parentVC:UIViewController, childVC:UIViewController)  {
        
        parentVC.addChild(childVC)
        parentVC.view.addSubview(childVC.view)
        childVC.didMove(toParent: parentVC)
        
        
    }
    
    class func removeChildVC(childVC:UIViewController) {
        
        childVC.willMove(toParent: nil)
        childVC.view.removeFromSuperview()
        childVC.removeFromParent()
        
    }
    
    class func openContact(multiSelection : Bool,_ delegate: ContactPickerDelegate?,isComingFromPayTo: Bool,isComingFromOtherNumberTopUp: Bool) -> UINavigationController  {
        
       // let contactPickerScene = ContactPickersPicker(delegate: delegate, multiSelection:multiSelection, subtitleCellType: SubtitleCellValue.phoneNumber)
        let story = UIStoryboard(name: "PayTo", bundle: nil)
         let contactPickerScene = story.instantiateViewController(withIdentifier: "ContactPickersPicker") as! ContactPickersPicker
        contactPickerScene.contactDelegate = delegate
        contactPickerScene.isComingFromOtherNumberTopUp = isComingFromOtherNumberTopUp
        contactPickerScene.isComingFromPayTo = isComingFromPayTo
        contactPickerScene.multiSelectEnabled = multiSelection
        contactPickerScene.subtitleCellValue = SubtitleCellValue.phoneNumber
        let navigationController = story.instantiateViewController(withIdentifier: "ContactNavigationController") as! UINavigationController
        navigationController.modalPresentationStyle = .fullScreen
        navigationController.navigationBar.tintColor = UIColor.white
        navigationController.setViewControllers([contactPickerScene], animated: true)
        return navigationController
        
    }
    
    
   
    
    //EB Comment update
    class func showMobileNumberFromDesc(desc: String?) -> (sourceNumber: Bool, destinationNumber: Bool) {
        var numberDisplay = (false, false)
        if let value = desc?.components(separatedBy: "SHOW:"), value.count > 0 {
            if value.indices.contains(1){
                let name = value[1].components(separatedBy: " PAYMENTTYPE:")
                if name.indices.contains(0){
                    if name[0] == "0" {
                        numberDisplay = (true,false)
                    } else if name[0] == "1" {
                        numberDisplay = (false,true)
                    }  else if name[0] == "2" {
                        numberDisplay = (false,false)
                    }
                }
            }
        } else {
            numberDisplay = (true,true)
        }
        return numberDisplay
    }
         
    //MARK:- Payment Type Filter
       
    class func getPaymentType(rawDesc: String?, transType: String?, accTransType: String?) -> String  {
        
        var tType = "PAYTO"
        if let desc = rawDesc {
            if desc.contains("#OK-") || desc.contains("OOK"){
                if desc.contains("Top-Up") || desc.contains("TopUp") || desc.contains("TopUpPlan") || desc.contains("Bonus Point Top Up") {
                    tType = "Top-Up"
                } else if desc.contains("Data Plan") || desc.contains("DataPlan") {
                    tType = "Data Plan"
                } else if desc.contains("PAYMENTTYPE:GiftCard") {
                    tType = "GiftCard"
                } else if desc.contains("PAYMENTTYPE:DTH") {
                    tType = "DTH"
                } else if desc.contains("MPU Card") {
                    tType = "MPU"
                } else if desc.contains("Visa Card") {
                    tType = "VISA"
                } else if desc.contains("meter refund") {
                    tType = "REFUND"
                } else if desc.contains(find: "#OK-PMC"){
                    tType = "Offers"
                } else {
                    tType = "Special Offers"
                }
            } else {
                    if transType == "PAYWITHOUT ID" {
                            if accTransType == "Cr" {
                                tType = "XXXXXXXXXX"
                            } else {
                                tType = "HIDE MY NUMBER"
                            }
                        
                        if desc.contains(find: "-cashin") {
                            tType = "Cash In"
                        } else if desc.contains(find: "-cashout") {
                            tType = "Cash Out"
                        } else if desc.contains(find: "-transferto") {
                            tType = "Transfer To"
                        }  else if desc.contains(find: "#Tax Code") {
                            tType = "Tax Payment"
                        }
                    } else if transType == "TOLL" {
                        tType = "TOLL"
                    } else if transType == "TICKET" {
                        tType = "TICKET"
                    } else {
                        tType = "PAYTO"
                        if desc.contains(find: "-cashin") {
                            tType = "Cash In"
                        } else if desc.contains(find: "-cashout") {
                            tType = "Cash Out"
                        } else if desc.contains(find: "-transferto") {
                            tType = "Transfer To"
                        }
                     }
                
            }
        }
        if let desc = rawDesc, desc.contains(find: "ELECTRICITY") {
            tType = "ELECTRICITY"
        } else if let type = rawDesc, type.contains(find: "MPU") {
            tType = "MPU"
        } else if let type = rawDesc, type.contains(find: "Visa") {
            tType = "VISA"
        }
        return tType
    }
    
    //MARK:- Account Type Filter
    
    class func getAccountType(accountName: String?, businessName: String?, txType: String?, rawDesc: String?) -> String {
            var senderAccountType = ""
            var accountType = "Personal"
            
            let senderNameArray = accountName?.components(separatedBy: "-")
            let value = senderNameArray?.first ?? ""
            println_debug(txType)
            if txType == "Cr" {
                if value.contains("@accounttype"){
                    let newValue = value.components(separatedBy: "@accounttype")
                    
                    if newValue.indices.contains(1){
                        let name = newValue[1].components(separatedBy: "@receiveraccounttype")
                        if name.indices.contains(0){
                            senderAccountType = name[0]
                        }
                    }
                } else {
                    if let receiverBName = businessName, receiverBName.count > 0 {
                        senderAccountType = "2"
                    } else {
                        senderAccountType = "6"
                    }
                }
            } else {//"Dr"
                if value.contains("@receiveraccounttype"){
                    let newValue = value.components(separatedBy: "@receiveraccounttype")
                    if newValue.indices.contains(1){
                        senderAccountType = newValue[1]
                    }
                } else {
                    if let receiverBName = businessName, receiverBName.count > 0 {
                        senderAccountType = "2"
                    } else {
                        senderAccountType = "6"
                    }
                }
            }
            let isPayTypeOther = UitilityClass.ReturnTransactionType(rawDesc: rawDesc)
            if (isPayTypeOther) {
                accountType = UitilityClass.returnAccountNameFromType(type: "11")
            }
            else {
                accountType = UitilityClass.returnAccountNameFromType(type: senderAccountType)
            }
          return accountType
        }
       
    
    class func returnAccountNameFromType(type: String?) -> String {
           switch type {
           case "6":
               return "Personal"
           case "1":
               return "Agent"
           case "5":
               return "Safety Cashier"
           case "2":
               return "Merchant"
           case "4":
               return "Advance Merchant"
           case "8":
               return "One Stop Mart"
            case "11":
               return "XXXXXXXXXXXX"
           default:
               return "Personal"
           }
       }
    
    class func returnAccountTypeFromName(type: String?) -> String {
        switch type {
        case "SUBSBR":
            return "6"
        case "AGENT":
            return "1"
        case "DUMMY":
            return "5"
        case "MER":
            return "2"
        case "ADVMER":
            return "4"
        case "ONESTOP":
            return "8"
        default:
            return "6"
        }
    }
    
    class func returnAccountType(type: String?) -> String {
        switch type {
        case "SUBSBR":
            return "Personal"
        case "AGENT":
            return "Agent"
        case "DUMMY":
            return "Safety Cashier"
        case "MER":
            return "Merchant"
        case "ADVMER":
            return "Advance Merchant"
        case "ONESTOP":
            return "One Stop Mart"
        default:
            return ""
        }
    }
    
    class func ReturnTransactionType(rawDesc: String?) -> Bool {
        
        if rawDesc?.contains(find: "Top Up") ?? false || rawDesc?.contains(find: "TopUp") ?? false || rawDesc?.contains(find: "Top-Up") ?? false || rawDesc?.contains(find: "TopUpPlan") ?? false  || rawDesc?.contains(find: "DataPlan") ?? false || rawDesc?.contains(find: "Data Plan") ?? false {
            return  true
        }
        return  false
    }
    
    //Get sim network Type
    static func getNetworkType() -> String{
        var networkType = ""
        let networkInfo = CTTelephonyNetworkInfo()
        let networkString = networkInfo.currentRadioAccessTechnology
        
        if isConnectedToWifi(){
            return "Wifi"
        }else{
            if networkString == CTRadioAccessTechnologyLTE{
                networkType = "4G"
                // LTE (4G)
            }else if networkString == CTRadioAccessTechnologyWCDMA || networkString == CTRadioAccessTechnologyHSDPA{
                networkType = "3G"
                // 3G
            }else if networkString == CTRadioAccessTechnologyEdge{
                networkType = "2G"
                // EDGE (2G)
            }
            return networkType
        }
       
    }
    
    
    //Get the sim card operator name
    static func getSimName() -> String {
        var carrierName: String?
        let typeName: (Any) -> String = { String(describing: type(of: $0)) }
        let statusBar = UIApplication.shared.value(forKey: "_statusBar") as! UIView
        for statusBarForegroundView in statusBar.subviews {
            if typeName(statusBarForegroundView) == "UIStatusBarForegroundView" {
                for statusBarItem in statusBarForegroundView.subviews {
                    if typeName(statusBarItem) == "UIStatusBarServiceItemView" {
                        carrierName = (statusBarItem.value(forKey: "_serviceString") as! String)
                    }
                }
            }
        }
        return carrierName ?? ""
    }
    
    
    //Get Wifi Details
    static func getNetworkInfos() -> [String] {
        guard let interfaceNames = CNCopySupportedInterfaces() as? [String] else {
            return []
        }
        return interfaceNames.compactMap{ name in
            guard let info = CNCopyCurrentNetworkInfo(name as CFString) as? [String:AnyObject] else {
                return nil
            }
            guard let ssid = info[kCNNetworkInfoKeySSID as String] as? String else {
                return nil
            }
            guard let bccid = info[kCNNetworkInfoKeyBSSID as String] as? String else {
                return nil
            }
            return "\(ssid),\(bccid)"
        }
    }
    
    
  static func isConnectedToWifi() -> Bool{
        if getNetworkInfos().count == 0{
            return false
        }
        return true
    }
    
   static func getSignalStrength() -> String {
        let url = URL(string: "https://www.google.com/")
        let request = URLRequest(url: url!)
        let session = URLSession.shared
        let startTime = Date()
        var finalValue = ""
        let task =  session.dataTask(with: request) { (data, resp, error) in
            
            guard error == nil && data != nil else{
                print("connection error or data is nill")
                return
            }
            
            guard resp != nil else{
                print("respons is nill")
                return
            }
            
            let length  = CGFloat( (resp?.expectedContentLength)!) / 1000000.0
            let elapsed = CGFloat( Date().timeIntervalSince(startTime))
            //  print("elapsed: \(elapsed)")
            //  print("Speed: \(length/elapsed) Mb/sec")
            
            finalValue = String(format: "%.2f", (length/elapsed))
            
            
        }
        
        
        task.resume()
        
        return finalValue
    }
    
    //This will help the label to get dynamic height as per the text using autolayout
    static func heightForView(text:String,width:CGFloat,x: CGFloat,y: CGFloat,label: UILabel) -> CGFloat{
        label.frame = CGRect(x: x, y: y, width: width, height: CGFloat.greatestFiniteMagnitude)
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    //pass the application name you wish to open and also dont forget to set a key in plist under LSApplicationQueriesSchemes your application name
    static func openApplication(applicationName: String) -> Bool {
        let app = URL(string: "\(applicationName)://app")
        if let value = app{
            if (UIApplication.shared.canOpenURL(value)){
                UIApplication.shared.canOpenURL(value)
                UIApplication.shared.open(value, options: [:], completionHandler: nil)
                return true
            }else{
                return false
            }
        }
        return false
    }
    
    
    static func setFrameForContactSuggestion(arrayCount: Int, indexpath: IndexPath,onTextField: UITextField,tableView: UITableView) -> CGRect {
                       
               var height: CGFloat = CGFloat(Float(arrayCount) * 70.0)
               if height >= 200.0{
                   height = 200.0
               }
               return CGRect(x: onTextField.frame.minX, y: tableView.convert(tableView.rectForRow(at: indexpath), to: tableView).minY - height + onTextField.frame.height + 20, width: onTextField.frame.width, height: height)
        
    }
    
    static func msidNew() {
       
        var primaryNetworkCode = ""
        
        
        if #available(iOS 13.0, *) {
            if let data = CTTelephonyNetworkInfo.init().serviceSubscriberCellularProviders{
               
                if data.count == 1{
                    if data["0000000100000001"]?.mobileNetworkCode ?? "" == "" {
                        
                    }else {
                        primaryNetworkCode = data["0000000100000001"]?.mobileNetworkCode ?? ""
                        
                        
                    }
                }else{
                    if data["0000000100000001"]?.mobileNetworkCode ?? "" == "" && data["0000000100000002"]?.mobileNetworkCode ?? "" == ""{
                    }else {
                        primaryNetworkCode = data["0000000100000001"]?.mobileNetworkCode ?? ""
                    }
                }
            }
        } else {
            if #available(iOS 12.0, *) {
                if  CTTelephonyNetworkInfo.init().serviceSubscriberCellularProviders?.count ?? 0 > 0{
                    if let data = CTTelephonyNetworkInfo.init().serviceSubscriberCellularProviders{
                        print(data)
                        if data.count == 1{
                            if getMsid() == ""{
                            }else{
                                primaryNetworkCode = getMsid()
                            }
                        }else if data.count == 2{
                            if data["0000000100000001"]?.mobileNetworkCode ?? "" == "" && data["0000000100000002"]?.mobileNetworkCode ?? "" == ""{
                            }else {
                                primaryNetworkCode = data["0000000100000001"]?.mobileNetworkCode ?? ""
                            }
                        }
                    }
                }
            }else{
                let networkInfo = CTTelephonyNetworkInfo.init().subscriberCellularProvider?.mobileNetworkCode ?? ""
                primaryNetworkCode = networkInfo
                if networkInfo == ""{
                }else{
                }
            }
        }
       
    }
    
    static func checkifSimAvailableInPhone() -> Bool{
        var isSuccess = false
        var primaryNetworkCode = ""
        
        
        if #available(iOS 13.0, *) {
            if let data = CTTelephonyNetworkInfo.init().serviceSubscriberCellularProviders{
               
                if data.count == 1{
                    if data["0000000100000001"]?.mobileNetworkCode ?? "" == "" {
                        isSuccess = false
                    }else {
                        primaryNetworkCode = data["0000000100000001"]?.mobileNetworkCode ?? ""
                        
                        isSuccess = true
                    }
                }else{
                    if data["0000000100000001"]?.mobileNetworkCode ?? "" == "" && data["0000000100000002"]?.mobileNetworkCode ?? "" == ""{
                        isSuccess = false
                    }else {
                        primaryNetworkCode = data["0000000100000001"]?.mobileNetworkCode ?? ""
                        isSuccess = true
                    }
                }
            }
        } else {
            if #available(iOS 12.0, *) {
                if  CTTelephonyNetworkInfo.init().serviceSubscriberCellularProviders?.count ?? 0 > 0{
                    if let data = CTTelephonyNetworkInfo.init().serviceSubscriberCellularProviders{
                        print(data)
                        if data.count == 1{
                            if getMsid() == ""{
                                isSuccess = false
                            }else{
                                primaryNetworkCode = getMsid()
                                isSuccess = true
                            }
                        }else if data.count == 2{
                            if data["0000000100000001"]?.mobileNetworkCode ?? "" == "" && data["0000000100000002"]?.mobileNetworkCode ?? "" == ""{
                                isSuccess = false
                            }else {
                                primaryNetworkCode = data["0000000100000001"]?.mobileNetworkCode ?? ""
                                isSuccess = true
                            }
                        }
                    }
                }
            }else{
                let networkInfo = CTTelephonyNetworkInfo.init().subscriberCellularProvider?.mobileNetworkCode ?? ""
                primaryNetworkCode = networkInfo
                if networkInfo == ""{
                    isSuccess = false
                }else{
                    isSuccess = true
                }
            }
        }
        if isSuccess{
            if primaryNetworkCode != UserDefaults.standard.value(forKey: "MobileNetworkCode") as? String ?? ""{
                isSuccess = false
//                appDelegate.deleteAllObjects()
//                appDelegate.loadSplashScreen()
//                UIApplication.shared.applicationIconBadgeNumber = 0
//                UserDefaults.standard.set(0, forKey: "ReceivedCount")
            }
        }
        return isSuccess
    }
    
    static func getAccountType(accountType: String) -> String{
        if accountType == "SUBSBR"{
            return "Personal"
        }else if accountType == "AGENT"{
            return "Agent"
        }else if accountType == "DUMMY"{
            return  "Safety Cashier"
        }else{
            return "Merchant"
        }
    }
    
    //this will work when one single dic is there as i am specifying the type in return
   static func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }

    
    //this function will convert the string which contain the dictionary of array  and return you poper json format
    
   static  func convertStringToArrayOfDictionary(text: String) -> Any? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? Any
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    static  func getQRImageNew(stringQR: String, withSize rate: CGFloat,logoName: String) -> UIImage? {
     //  print(AESCrypt.encrypt(stringQR, password: "m2n1shlko@$p##d"))

         if let filter = CIFilter.init(name: "CIQRCodeGenerator") {
             filter.setDefaults()
             if let data : Data = stringQR.data(using: String.Encoding.utf8) {
                 filter.setValue(data, forKey: "inputMessage")
                 if let resultImage : CIImage = filter.outputImage {
                     let transform = CGAffineTransform(scaleX: 12, y: 12)
                     let translatedImage = resultImage.transformed(by: transform)
                     guard let logo = UIImage(named: logoName), let logoInCGImage = logo.cgImage else {
                         return nil
                     }
                     guard let qrWithLogoCI = translatedImage.combined(with:  CIImage(cgImage: logoInCGImage)) else {
                         return nil
                     }
                     let context = CIContext.init(options: nil)
                     guard let qrWithLogoCG = context.createCGImage(qrWithLogoCI, from: qrWithLogoCI.extent) else {
                         return nil
                     }
                     var image   = UIImage.init(cgImage: qrWithLogoCG, scale: 1.0, orientation: UIImage.Orientation.up)
                     let width  =  image.size.width * rate
                     let height =  image.size.height * rate
                     UIGraphicsBeginImageContext(.init(width: width, height: height))
                     let cgContext = UIGraphicsGetCurrentContext()
                     cgContext?.interpolationQuality = .none
                     image.draw(in: .init(origin: .init(x: 0, y: 0), size: .init(width: width, height: height)))
                     image = UIGraphicsGetImageFromCurrentImageContext()!
                     UIGraphicsEndImageContext()
                     return image
                 }
             }
           
         }
         return nil
     }
    
    
    //this function will return qr with image of your choice
    
    static  func getQRImage(stringQR: String, withSize rate: CGFloat,logoName: String) -> UIImage? {
      //  print(AESCrypt.encrypt(stringQR, password: "m2n1shlko@$p##d"))
     let value = AESCrypt.encrypt(stringQR, password: "m2n1shlko@$p##d")

          if let filter = CIFilter.init(name: "CIQRCodeGenerator") {
              filter.setDefaults()
            if let qrData = value{
              if let data : Data = qrData.data(using: String.Encoding.utf8) {
                  filter.setValue(data, forKey: "inputMessage")
                  if let resultImage : CIImage = filter.outputImage {
                      let transform = CGAffineTransform(scaleX: 12, y: 12)
                      let translatedImage = resultImage.transformed(by: transform)
                      guard let logo = UIImage(named: logoName), let logoInCGImage = logo.cgImage else {
                          return nil
                      }
                      guard let qrWithLogoCI = translatedImage.combined(with:  CIImage(cgImage: logoInCGImage)) else {
                          return nil
                      }
                      let context = CIContext.init(options: nil)
                      guard let qrWithLogoCG = context.createCGImage(qrWithLogoCI, from: qrWithLogoCI.extent) else {
                          return nil
                      }
                      var image   = UIImage.init(cgImage: qrWithLogoCG, scale: 1.0, orientation: UIImage.Orientation.up)
                      let width  =  image.size.width * rate
                      let height =  image.size.height * rate
                      UIGraphicsBeginImageContext(.init(width: width, height: height))
                      let cgContext = UIGraphicsGetCurrentContext()
                      cgContext?.interpolationQuality = .none
                      image.draw(in: .init(origin: .init(x: 0, y: 0), size: .init(width: width, height: height)))
                      image = UIGraphicsGetImageFromCurrentImageContext()!
                      UIGraphicsEndImageContext()
                      return image
                  }
              }
            }
          }
          return nil
      }
    
    
    //this will return zwagi always if the system  has otherwise it will system font
    static func getZwagiFontWithSize(size: CGFloat) -> UIFont{
        return UIFont.init(name: appFont, size: size) ?? UIFont.systemFont(ofSize: size, weight: UIFont.Weight.regular)
    }

    static func makeStringForQR(firstName: String,secondName: String) -> String{
        
        return firstName + "=" + secondName + ";"
    }
    
    
    static func returnUPICurrencyDetail(code: String) -> Dictionary<String, AnyObject>?{
        
        if let currencyPath = Bundle.main.path(forResource: "country_currency_code", ofType: "json")  {
            do {
                let currencyData = try Data(contentsOf: URL(fileURLWithPath: currencyPath), options: .mappedIfSafe)
                let currencyJson = try JSONSerialization.jsonObject(with: currencyData, options: .mutableLeaves)
                if let currencyJson = currencyJson as? Dictionary<String, AnyObject>, let allList = currencyJson["countries"] as? [AnyObject] {
                    let predicate = NSPredicate(format: "NumericCode == %@", "344")
                    let filter = allList.filter { predicate.evaluate(with: $0) } as [AnyObject]
                    // here create new township instance for that selected division
                    if let dic = filter[0] as? Dictionary<String, AnyObject> {
                        print( dic["AlphabeticCode"]  as! String)
                        print( dic["NumericCode"]  as! String)
                        return dic
                    }
                }
                }
             catch {
                println_debug("something went wrong when extract data from township json file")
            }
        }else{
            return nil
        }
        
        return nil
    }
    
    /// Creates a new unique user identifier or retrieves the last one created
   static func getUUID() -> String{

        // create a keychain helper instance
        let keychain = KeychainAccess()

        // this is the key we'll use to store the uuid in the keychain
        let uuidKey = "com.cgm.okDollar"

        // check if we already have a uuid stored, if so return it
        if let uuid = try? keychain.queryKeychainData(itemKey: uuidKey), uuid != nil {
            return uuid ?? ""
        }

        // generate a new id
//        guard let newId = UIDevice.current.identifierForVendor?.uuidString else {
//            return nil
//        }
    
    let newId = ASIdentifierManager.shared().advertisingIdentifier.uuidString
    

        // store new identifier in keychain
        try? keychain.addKeychainData(itemKey: uuidKey, itemValue: newId)

        // return new id
        return newId
    }
  
    
}


class KeychainAccess {

    func addKeychainData(itemKey: String, itemValue: String) throws {
        guard let valueData = itemValue.data(using: .utf8) else {
            print("Keychain: Unable to store data, invalid input - key: \(itemKey), value: \(itemValue)")
            return
        }

        //delete old value if stored first
        do {
            try deleteKeychainData(itemKey: itemKey)
        } catch {
            print("Keychain: nothing to delete...")
        }

        let queryAdd: [String: AnyObject] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: itemKey as AnyObject,
            kSecValueData as String: valueData as AnyObject,
            kSecAttrAccessible as String: kSecAttrAccessibleWhenUnlocked
        ]
        let resultCode: OSStatus = SecItemAdd(queryAdd as CFDictionary, nil)

        if resultCode != 0 {
            print("Keychain: value not added - Error: \(resultCode)")
        } else {
            print("Keychain: value added successfully")
        }
    }

    func deleteKeychainData(itemKey: String) throws {
        let queryDelete: [String: AnyObject] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: itemKey as AnyObject
        ]

        let resultCodeDelete = SecItemDelete(queryDelete as CFDictionary)

        if resultCodeDelete != 0 {
            print("Keychain: unable to delete from keychain: \(resultCodeDelete)")
        } else {
            print("Keychain: successfully deleted item")
        }
    }

    func queryKeychainData (itemKey: String) throws -> String? {
        let queryLoad: [String: AnyObject] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: itemKey as AnyObject,
            kSecReturnData as String: kCFBooleanTrue,
            kSecMatchLimit as String: kSecMatchLimitOne
        ]
        var result: AnyObject?
        let resultCodeLoad = withUnsafeMutablePointer(to: &result) {
            SecItemCopyMatching(queryLoad as CFDictionary, UnsafeMutablePointer($0))
        }

        if resultCodeLoad != 0 {
            print("Keychain: unable to load data - \(resultCodeLoad)")
            return nil
        }

        guard let resultVal = result as? NSData, let keyValue = NSString(data: resultVal as Data, encoding: String.Encoding.utf8.rawValue) as String? else {
            print("Keychain: error parsing keychain result - \(resultCodeLoad)")
            return nil
        }
        return keyValue
    }
}


