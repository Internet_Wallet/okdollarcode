//
//  LazyImageLoader.swift
//  OK
//
//  Created by T T Marshel Daniel on 09/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import SDWebImage

public final class LazyImageLoader {
    
    public class func showImage(On imageView :UIImageView, fromURL url :URL, showActivityIndicator :Bool = false, indicatorStyle :UIActivityIndicatorView.Style) {
        
        imageView.sd_setShowActivityIndicatorView(showActivityIndicator)
        imageView.sd_setIndicatorStyle(indicatorStyle)
        
        imageView.sd_setImage(with: url, completed: nil)
    }
}
