//
//  Utilities.m
//  OK
//
//  Created by Badru on 10/31/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

#import "Utilities.h"
#import "CustomAlert.h"
//#import "OK-Swift.h"

@implementation Utilities {

    NSString *dbPathString;

    NSString *lattitude,*langitude;
    NSString *csvFilePath;
    NSMutableArray *checkingFlagNames;

    //Prabu contact 7/10/2015
    NSMutableArray *contactList;
    NSMutableArray *contactListForDB;
    NSMutableArray *tableSectionTitles;
    NSMutableDictionary *contacttDictionary;
    NSMutableArray *defaultAndSerchArray;
    int selectedMonthTotalDays;
    int selectedMonthTotalWeeks;
    NSMutableArray *groupOfContacts;

    BOOL SuccessORFail;

}
static Utilities *sharedObj = nil;

+(Utilities*)sharedInstance;
{
    if(sharedObj == nil)
    {
        sharedObj = [[super allocWithZone:NULL] init];
        
    }
    return sharedObj;
}


-(void)AlertView : (NSString*)msg  SuberView:(id)suberview{
    
  //  CustomAlert *alert = [[CustomAlert alloc] initWithTitle:[AppHelper getl alloc] getlocaLizationLanguage:@"Alert"] message:msg delegate:self cancelButtonTitle:[appDelegate getlocaLizationLanguage:@"ok"] otherButtonTitle:nil];
  //  [alert showInView:(UIView *)suberview];
    
}



-(NSString*)getNumberFormat:(NSString *)str{
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString *groupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
    [formatter setGroupingSeparator:groupingSeparator];
    [formatter setGroupingSize:3];
    [formatter setAlwaysShowsDecimalSeparator:NO];
    [formatter setUsesGroupingSeparator:YES];
    
    if ([str containsString:@"."]) {
        
        [formatter setMinimumFractionDigits:2];
        
    }
    NSString *amount = [formatter stringFromNumber:[NSNumber numberWithDouble:str.doubleValue]];
    
    return amount;
    
}

@end

