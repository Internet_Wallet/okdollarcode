//
//  PaymentAuthorisationViewController.swift
//  OK
//
//  Created by Ashish on 1/15/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

 protocol AuthenticationDelegate {
    var name: String {get set}
}

class PaymentAuthorisationViewController: OKBaseController {

    @IBOutlet private var login_Button: UIButton!
    @IBOutlet private var passwordContainer: UIView!
    @IBOutlet private var patternContainerView: UIView!
    @IBOutlet private var heightConstraint_login: NSLayoutConstraint!
    
    var delegate : AuthenticationDelegate?
    
    var loginConstraint : Int = 50 {
        didSet  {
            if loginConstraint == 0 {
                self.login_Button.isHidden = true
                heightConstraint_login.constant = 0
            } else {
                self.login_Button.isHidden = false
                heightConstraint_login.constant = 50.00
            }
        }
    }
    
    private  var isLoginType : Bool = ok_password_type
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if ok_password_type {
            loginConstraint = 50
        } else {
            loginConstraint = 0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //super.viewWillAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    func loginFinalResponse(completionHandler : @escaping (Bool) -> Void) {
        dataParse { (result) in
            completionHandler(result)
        }
    }
    
    func dataParse(completionHandler : @escaping (Bool) -> Void) {
        completionHandler(true)
    }

    
    @IBAction func loginAction(_ sender: UIButton) {
//        self.loginFinalResponse(completionHandler: <#T##(Bool) -> Void#>)
    }
}
