//
//  OKPayment.swift
//  OK
//
//  Created by Ashish on 1/15/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import LocalAuthentication

class OKPayment: NSObject, BioMetricLoginDelegate {

    static let main = OKPayment()
    
    private let storyboard = UIStoryboard(name: "Login", bundle: nil)
    private let sharedApp  = UIApplication.shared
    
    var authDelegate : BioMetricLoginDelegate?

   private func deviceOwnerAuthenticate(_ completionHandler : @escaping (Bool) -> Void) {
        
        //Create Local Authentication Context
        let authenticationContext = LAContext()
        
        authenticationContext.localizedFallbackTitle = ""
        
        var error:NSError?
    
    
    
        
        guard authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            
            guard let err = error else { return }
            
            DispatchQueue.main.async {
                alertViewObj.wrapAlert(title: nil, body: err.localizedDescription, img: #imageLiteral(resourceName: "error"))
                alertViewObj.addAction(title: "Cancel", style: .cancel, action: {
                    
                })
                
                alertViewObj.addAction(title: "Unlock", style: .target, action: {
                    DispatchQueue.main.async {
                        self.passcodeAuthentication()
                    }
                    
                    
                })
                alertViewObj.showAlert(controller: UIViewController.init())
            }
            
            return
        }
        
        authenticationContext.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Use Touch ID to access OK$", reply: { (success, error) -> Void in
             completionHandler(success)
        })
    
   
    
    }
    
  fileprivate func passcodeAuthentication() {
        //Create Local Authentication Context
        let authenticationContext = LAContext()

        let policy = LAPolicy.deviceOwnerAuthentication
        
        let localisedReason : String = "Please enter your device passcode to unlock biometry"
        
        authenticationContext.evaluatePolicy(policy, localizedReason: localisedReason) { (isSuccessful, error) in
            if isSuccessful {
                DispatchQueue.main.async {
                    alertViewObj.wrapAlert(title: nil, body: "Your device biometry is unlocked now.", img: #imageLiteral(resourceName: "error"))
                    alertViewObj.addAction(title: "OK", style: .cancel, action: {
                        
                    })
                    alertViewObj.showAlert(controller: UIViewController.init())
                }
            }
        }
    }
    
    func authenticate(screenName: String, delegate: BioMetricLoginDelegate) {
        self.deviceOwnerAuthenticate { [weak self] (success) in
            if success {
                DispatchQueue.main.async {
                    let loginVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: LoginViewController.self)) as? LoginViewController
                    loginVC?.authenticateScreen = true
                    loginVC?.biometricString    = screenName
                    loginVC?.biometricDelegate  = self
                    self?.authDelegate           = delegate

                    if let keyWindow = UIApplication.shared.keyWindow {
                        loginVC?.view.frame = keyWindow.bounds
                        loginVC?.view.tag = 909786
                        keyWindow.addSubview(loginVC!.view)
                        keyWindow.makeKeyAndVisible()
                    }
                }
            } else {
                self?.authDelegate = delegate
                self?.authDelegate?.biometricAuthentication(isSuccessful: success, inScreen: screenName)
            }
        }
    }
  
    func authenticateForRegistration(screenName: String, delegate: BioMetricLoginDelegate) {
        self.deviceOwnerAuthenticate { [weak self] (success) in
            self?.authDelegate = delegate
            self?.authDelegate?.biometricAuthentication(isSuccessful: success, inScreen: screenName)
        }
    }
    
    
    //Uncomment to remove finger print access
//    func authenticate(screenName: String, delegate: BioMetricLoginDelegate) {
//
//        DispatchQueue.main.async {
//            let loginVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: LoginViewController.self)) as? LoginViewController
//            loginVC?.authenticateScreen = true
//            loginVC?.biometricString = screenName
//            loginVC?.biometricDelegate = self
//            self.authDelegate = delegate
//
//            if let keyWindow = UIApplication.shared.keyWindow {
//                loginVC?.view.frame = keyWindow.bounds
//                loginVC?.view.tag = 909786
//                keyWindow.addSubview(loginVC!.view)
//                keyWindow.makeKeyAndVisible()
//            }
//        }
//
//
//        // self.deviceOwnerAuthenticate { [weak self] (success) in
//        // if success {
//        // DispatchQueue.main.async {
//        // let loginVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: LoginViewController.self)) as? LoginViewController
//        // loginVC?.authenticateScreen = true
//        // loginVC?.biometricString = screenName
//        // loginVC?.biometricDelegate = self
//        // self?.authDelegate = delegate
//        //
//        // if let keyWindow = UIApplication.shared.keyWindow {
//        // loginVC?.view.frame = keyWindow.bounds
//        // loginVC?.view.tag = 909786
//        // keyWindow.addSubview(loginVC!.view)
//        // keyWindow.makeKeyAndVisible()
//        // }
//        // }
//        // } else {
//        // self?.authDelegate = delegate
//        // self?.authDelegate?.biometricAuthentication(isSuccessful: success, inScreen: screenName)
//        // }
//        // }
//    }
  
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if isSuccessful{
        self.removeView()
        if let delegate = self.authDelegate {
            delegate.biometricAuthentication(isSuccessful: isSuccessful, inScreen: screen)
        }
        
        }
    }

    fileprivate func removeView() {
        for views in UIApplication.shared.keyWindow!.subviews {
            if views.tag == 909786 {
                views.removeFromSuperview()
            }
        }
    }

}
