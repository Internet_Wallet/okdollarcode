//
//  PaymentAuthorizationViewController.swift
//  OK
//
//  Created by Ashish Kumar Singh on 11/8/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import LocalAuthentication

fileprivate protocol PaymentAuthorisationDelegate {
    func loginAuthorisation()
    func failedAuthorisation()
}

protocol PaymentValidationDelegate {
    func paymentAuthorisationSuccess()
}

protocol PaymentValidatonDelegateModule {
    func paymentAuthorisationSuccess(withModule moduleData : [String:Any])
}

class PaymentAuthorisation : NSObject, PaymentAuthorisationDelegate {
    
    func failedAuthorisation() {
        
    }
    
    
    let sharedApp = UIApplication.shared
    
    var delegate : PaymentValidationDelegate?
    var delegateModule : PaymentValidatonDelegateModule?
    
    var parent : UIViewController?
    var requestModuleData: [String:Any]?
    
    func authenticate() {
        authenticateUser()
    }
    
    private func authenticateUser() {
        //Create Local Authentication Context
        let authenticationContext = LAContext()
        
        var error:NSError?
        guard authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            println_debug("No Biometric Sensor Has Been Detected. This device does not support Touch Id.")
            return
        }
        
        authenticationContext.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Please enter your Touch ID", reply: { (success, error) -> Void in
            
            if( success ) {
                
                println_debug("Success")
                
                self.navigateToPaymentLogin()
                
            } else {
                
                if let errorObj = error {
                    println_debug("Error took place. \(errorObj.localizedDescription)")
                }
                
            }
        })
    }
    
    func loginAuthorisation() {
        
        DispatchQueue.main.async {
            if let window = self.sharedApp.keyWindow {
                for view in window.subviews {
                    if view.tag == 100 {
                        view.removeFromSuperview()
                    }
                }
            }
            if let delegate = self.delegate {
                delegate.paymentAuthorisationSuccess()
            }
            
            if let moduleName = self.requestModuleData {
                if let delegateModule = self.delegateModule {
                    delegateModule.paymentAuthorisationSuccess(withModule: moduleName)
                }
            }
        }
        
    }
    
    
    func navigateToPaymentLogin(){
        
        DispatchQueue.main.async {
            let vc = PaymentAuthorizationViewController(nibName: "PaymentAuthorizationViewController", bundle: nil) as PaymentAuthorizationViewController
            vc.view.tag = 100
            vc.delegate = self
            
            if let window = self.sharedApp.keyWindow {
                window.rootViewController?.addChild(vc)
                vc.view.frame = .init(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
                window.addSubview(vc.view)
                window.makeKeyAndVisible()
            }
        }
        
    }
}


class PaymentAuthorizationViewController: OKBaseController, WebServiceResponseDelegate, UITextFieldDelegate {
    
    fileprivate  var delegate : PaymentAuthorisationDelegate?
    
    @IBOutlet var showAccountNumber: UITextField!
    @IBOutlet var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hideKeyboardWhenTappedAround()
        
        
    }
    
    
    @IBAction func loginAction(_ sender: UIButton) {
        
        let password = self.password.text
        
        if password == nil { return }
        
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            let urlStr   = Url.login
            let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug(ur)
            let userModel = UserModel()
            let preObject = PreLoginModel()
            //preObject.wrapPreLoginData(agent: "00959972271860", cCode: "", fCode: "")
            preObject.wrapPreLoginData(agent: UserModel.shared.mobileNo, cCode: "", fCode: "")
            
            let typePassword = ok_password_type ? "1" : "0"
            
            let params = LoginParams.getParamsForLogin(obj: userModel, pass: password!, preObj: preObject, type: typePassword)
            println_debug(params)
            web.genericClass(url: ur, param: params as AnyObject, httpMethod: "POST", mScreen: "AshishMLogin")
        }
    }
    
    @IBAction func cancelAction(_ sender: UIButton) {
        self.view.removeFromSuperview()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        if screen == "AshishMLogin" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(dic)
                        
                        //                        UserDefaults.standard.set(password.text, forKey: "Password")
//                        DispatchQueue.main.async {
//                            self.navigateToDashboard()
//                        }
                        

                        

                        if dic["Code"] as? NSNumber == 200 {
                            if let login = dic["Data"] as? String {
                                let dic = OKBaseController.convertToDictionary(text: login)
                                if let log = dic!["AgentDetails"] as? Array<Any> {
                                    if let logi = log.first as? Dictionary<String,Any> {
                                        UserLogin.wrapUserData(dict: logi)
                                        
                                        
                                        let defaults = UserDefaults.standard
                                        DispatchQueue.main.async {
                                            defaults.set(self.password.text!, forKey: "password")
                                        }
                                        if let myString = defaults.string(forKey: "password"){
                                            println_debug("Password: \(myString)")
//                                            UserModel.shared.pass = myString
                                            
//                                            ok_password = myString
//                                            userDef.set(myString, forKey: "passwordLogin_local")
//                                          userDef.set(isPassword, forKey: "passwordLoginType")
                                            
                                        }
                                        
                                        
                                        if let delegate = self.delegate {
                                            delegate.loginAuthorisation()
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                }
            } catch {
                
            }
        }
        
        
        
        if screen == "AshishMProfile" {
            if let data = json as? Data {
                do {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dataDict = dict as? Dictionary<String,AnyObject> {
                        if let dic = dataDict["Data"] as? String{
                            
                            let profile = OKBaseController.convertToDictionary(text: dic)
                            if profile == nil { return }
                            User.shared.wrapModel(dic: profile! as Dictionary<String, AnyObject>)
                            
                        }
                    }
                } catch {
                    
                }
                
            }
        }
    }
    
    func navigateToDashboard() {
        let rootNav    = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OKTabBArController")
        self.present(rootNav, animated: true, completion: nil)
        userDef.set(true, forKey: keyLoginStatus)
    
        
    }
    
    func updateProfile() {
        if appDelegate.checkNetworkAvail() {
            
            let web      = WebApiClass()
            web.delegate = self
            let urlStr   = String.init(format: Url.updateProfile, UserModel.shared.mobileNo,uuid)
            let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
            let params = Dictionary<String,String>()

            web.genericClass(url: ur, param: params as AnyObject, httpMethod: "GET", mScreen: "AshishMProfile")
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        println_debug(string)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == showAccountNumber { return false }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
}

