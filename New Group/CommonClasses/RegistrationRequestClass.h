//
//  RegistrationRequestClass.h
//  OK
//
//  Created by Subhash Arya on 02/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^CompletionOnSuccess)(NSInteger successValue);

@interface RegistrationRequestClass : NSObject
@property (copy, nonatomic) CompletionOnSuccess complete;

-(void)callAPIwithURL:(NSURL *)url andParmaDic:(NSDictionary *)paramDic withCompletionHandler:(void (^)(NSInteger diceValue))finishBlock;

//- (void)getDiceValueAfterSpin:(void (^)(NSInteger diceValue))finishBlock;
@end
