//
//  GenericUpdate_Login.swift
//  OK
//
//  Created by Ashish on 2/27/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class GetProfile : NSObject {
    
    func callLoginApi(aCode: String, withPassword pass: String, handler: @escaping (_ success: Bool) -> Void) -> Void{
        if appDelegate.checkNetworkAvail() {
            let urlStr   = Url.login
            let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
            let params = self.loginParam(aCode: aCode, pass: pass)
           // params["PasswordType"] = "0" ?? "1"
            TopupWeb.genericClass(url: ur, param: params as AnyObject, httpMethod: "POST", handle: { (response, success) in
                DispatchQueue.main.async {
                    if let dic = response as? Dictionary<String,AnyObject> {
                        if let code = dic["Code"] as? Int{
                            if code == 200{
                                if let login = dic["Data"] as? String, login != "" {
                                    let dic = OKBaseController.convertToDictionary(text: login)
                                    if let log = dic!["AgentDetails"] as? Array<Any> {
                                        if let logi = log.first as? Dictionary<String,Any> {
                                            if logi["ResultDescription"] as? String == "Transaction Successful"{
                                                ok_password = pass
                                                UserLogin.wrapUserData(dict: logi)
                                                handler(true)
                                            }
                                        }
                                    }
                                } else {
                                    handler(false)
                                }
                            }else{
                                handler(false)
                            }
                        }
                    }
                }
            })
        }
    }
    
    
    func callLoginApiDashboard(aCode: String, withPassword pass: String) -> Void{
        if appDelegate.checkNetworkAvail() {
            let urlStr   = Url.login
            let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
            let params = self.loginParam(aCode: aCode, pass: pass)
            // params["PasswordType"] = "0" ?? "1"
            TopupWeb.genericClassLogin(url: ur, param: params as AnyObject, httpMethod: "POST", handle: { (response, success) in
                DispatchQueue.main.async {
                    if let dic = response as? Dictionary<String,AnyObject> {
                        if let login = dic["Data"] as? String, login != "" {
                            let dic = OKBaseController.convertToDictionary(text: login)
                            if let log = dic!["AgentDetails"] as? Array<Any> {
                                if let logi = log.first as? Dictionary<String,Any> {
                                    if logi["ResultDescription"] as? String == "Transaction Successful"{
                                        UserLogin.shared.walletBal = logi.safeValueForKey("WalletBalance").safelyWrappingString()
                                        UserLogin.shared.kickBack = logi.safeValueForKey("KickBack").safelyWrappingString()
                                        UserLogin.shared.token = logi.safeValueForKey("securetoken").safelyWrappingString()
                                    }
                                }
                            }
                        } else {
                            
                        }
                    }
                }
            })
        }
    }
    

  class func updateBalance() {
    
    var dictionary = Dictionary<String, Any>()
        
        if appDelegate.checkNetworkAvail() {
            var ip = ""
            if let ipAdd = OKBaseController.getIPAddress() {
                ip = ipAdd
            }
            
            let urlStr   = String.init(format: Url.balanceCheckTimeUrl, "BALANCE", UserModel.shared.mobileNo, UserModel.shared.mobileNo, ok_password ?? "", "IPAY", ip, "iOS", "GPRS", UserLogin.shared.token)
          
          guard let encodedString = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
          
          let url = getUrl(urlStr: encodedString, serverType: .serverEstel)
          
            //let url = getUrl(urlStr: urlStr, serverType: .serverEstel)
            println_debug(url)
            let pRam = Dictionary<String,String>()
            TopupWeb.genericClassXMLWithoutLoader(url: url, param: pRam as AnyObject, httpMethod: "POST") { (response, success) in
                if success {
                    DispatchQueue.main.async {
                        if let xmlString = response as? String {
                            let xml = SWXMLHash.parse(xmlString)
                            func enumerateMore(indexer: XMLIndexer) {
                                for child in indexer.children {
                                    dictionary[child.element!.name] = child.element!.text
                                    enumerateMore(indexer: child)
                                }
                            }
                            enumerateMore(indexer: xml)
                            UserLogin.shared.walletBal = dictionary.safeValueForKey("walletbalance") as? String ?? UserLogin.shared.walletBal
                        }
                    }
                }
            }
        }

    }
    
    func callUpdateProfileApi(aCode: String, handler: @escaping (_ success: Bool) -> Void) -> Void {
        if appDelegate.checkNetworkAvail() {
            let urlStr   = String.init(format: Url.updateProfile, aCode,uuid)
            let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
            let params = Dictionary<String,String>()
            TopupWeb.genericClass(url: ur, param: params as AnyObject, httpMethod: "GET", handle: { (response, success) in
                if success {
                    DispatchQueue.main.async {
                        if let dataDict = response as? Dictionary<String,AnyObject> {
                            if let dic = dataDict["Data"] as? String {
                                let profile = OKBaseController.convertToDictionary(text: dic)
                                if profile == nil { return }
                                User.shared.wrapModel(dic: profile! as Dictionary<String, AnyObject>)
                                handler(success)
                            }
                        }
                    }
                }
            })
        }
    }
    
    // MARKS:- Essential Parameters
    private func loginParam(aCode: String, pass: String) -> Dictionary<String, Any> {
        var dict = Dictionary<String, Any>()
        
        dict["MobileNumber"]          = aCode
        dict["Password"]              = pass
        dict["SimId"]                 = uuid
        dict["Msid"]                  = msid
        dict["OsType"]                = Int(1)
        dict["IosOtp"]                = uuid
        dict["DateOfBirth"]           = ""
        dict["FatherName"]            = ""
        dict["Name"]                  = ""
        dict["ReRegisterOtp"]         = uuid
        dict["AuthCode"]              = ""
        dict["AuthCodeStatus"]        = Int(truncating: false)
        dict["ProfilePic"]            = ""
        dict["AppID"]                 = LoginParams.setUniqueIDLogin()
        
        dict["PasswordType"]          = ok_password_type ? "1" : "0"
        dict["Latitude"]              = "21.9162"
        dict["Longitude"]             = "95.9560"
        dict["CellId"]                = ""
        dict["AndroidVersion"]        = ""
        dict["NetworkOperator"]       = ""
        dict["NetworkOperatorName"]   = ""
        dict["DeviceSoftwareVersion"] = ""
        dict["NetworkCountryIso"]     = ""
        dict["NetworkType"]           = ""
        dict["SIMCountryIso"]         = ""
        dict["SIMOperator"]           = ""
        dict["SIMOperatorName"]       = ""
        dict["VoiceMailNo"]           = ""
        dict["BSSID"]                 = ""
        dict["MACAddress"]            = ""
        dict["IPAddress"]             = ""
        dict["SSID"]                  = ""
        dict["BluetoothAddress"]      = ""
        dict["BluetoothName"]         = ""
        dict["ConnectedNetworkType"]  = ""
        dict["LinkSpeed"]             = Int(1)
        dict["NetworkID"]             = Int(1)
        dict["NetworkSignal"]         = Int(1)
        dict["HiddenSSID"]            = Int(truncating: true)
        dict["isNetworkRoaming"]      = Int(truncating: true)
        
        return dict
    }
}
