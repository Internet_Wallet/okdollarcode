//
//  OKActivityIndicator.swift
//  OK
//
//  Created by Ashish on 2/8/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class OKActivityIndicator: UIView {
    
    @IBOutlet var loader: InstagramActivityIndicator!
    
    fileprivate static let classAs = String.init(describing: OKActivityIndicator.self)
    
    class func updateView(_ bounds: CGRect) -> OKActivityIndicator {
        let nib   =  UINib(nibName: classAs, bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as! OKActivityIndicator
        nib.frame = bounds
        nib.layoutUpdates()
        return nib
    }

    func layoutUpdates() {
        self.layoutIfNeeded()
    }
    
    func animateLoader() {
        loader.startAnimating()
    }
    
    func stopAnimation() {
        loader.stopAnimating()
    }

}
