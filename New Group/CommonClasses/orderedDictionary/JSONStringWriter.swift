//
//  JSONObject.swift
//  OK
//
//  Created by Ashish on 2/15/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class JSONStringWriter: NSObject {
    
    func writeJsonIntoString(_ keys: [String], _ values: [String]) -> String {
        var keyValueArray = [String]()
        var json : String = ""
        
        if keys.count != values.count { return "" }
        
        for (index,key) in keys.enumerated() {
            let object = values[index]
            let keyPairString = String.init(format: "\"%@\":\"%@\"", key, object)
            keyValueArray.append(keyPairString)
        }
        
        var stringObject = ""
        for keyValue in keyValueArray {
            stringObject.append(keyValue)
            if keyValue != keyValueArray.last {
                stringObject.append(",")
            }
        }
        json = "{" + stringObject  + "}"
                
      return json
    }

}
