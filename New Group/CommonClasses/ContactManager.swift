//
//  ContactManager.swift
//  OK
//
//  Created by Ashish on 12/6/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit
import CoreData
import Contacts

public enum ContactsFetchResult {
    case success(response: [CNContact])
    case error(error: Error)
}

struct ContactManager {
    
    init() {}
    
    struct OKContactModel {
        var phone    : String?
        var name     : String?
        var imageURL : String?
    }
    
    func updateContacts() {
        self.fetchContactsOnBackgroundThread { (result) in
            switch result {
            case .success(let response):
                let allDBContacts = self.fetchAllContacts()
                response.forEach({ (contact) in
                    let phoneNumber     = self.getPhoneNumber(contact)
                    let name            = self.displayName(contact)
                    let uniqueID        = phoneNumber + name
                    
                    if let contacts = allDBContacts {
                        if let _ = contacts.first(where: {$0.id.safelyWrappingString() == uniqueID}) {} else {
                            self.store(ph: phoneNumber, name: name, id: uniqueID, img: contact.thumbnailImageData)
                        }
                    }
                })
            case .error(let error):
                println_debug(error)
            }
        }
    }
    
    var array = [OKContacts]()
    //MARK:- Core Data Manager
    private func store(ph: String, name: String, id: String, img: Data?) {
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "OKContacts", in: context)
        if let wrapEntity = entity {
            if let contactDB = NSManagedObject(entity: wrapEntity, insertInto: context) as? OKContacts {
                contactDB.imgData   = img
                contactDB.name      = name
                contactDB.phone     = ph
                contactDB.id        = id
            }
                do {
                    try context.save()
                } catch let error as NSError {
                    println_debug(error)
                }
        }
    }
    
    func fetchAllContacts() -> [OKContacts]? {
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        do {
            let fetchRequest : NSFetchRequest<OKContacts> = OKContacts.fetchRequest()
            let fetchedResults = try context.fetch(fetchRequest)
            return fetchedResults
        }
        catch {
            println_debug(error)
        }
        
        return nil
    }

    private func displayName(_ contact: CNContact) -> String {
        var name = contact.givenName + " " + contact.familyName
        if name.replacingOccurrences(of: " ", with: "") == "", name.count <= 0 {
            name = "Unknown"
        }
        return name
    }

    private func getPhoneNumber(_ contact: CNContact) -> String {

        guard let phoneNumber = contact.phoneNumbers.first else { return "" }

        if let phone    = phoneNumber.value.stringValue.addingPercentEncoding(withAllowedCharacters: .decimalDigits) {
            let phones  = phone.replacingOccurrences(of: "%20", with: "")
            let phones2 = phones.replacingOccurrences(of: "%C2%A0", with: "")
            let newFormat = phones2.removingPercentEncoding!
            let serverFormat = newFormat.digitsPhone
            let phoneAsset = PTManagerClass.decodeMobileNumber(phoneNumber: serverFormat)

            var code : String = phoneAsset.country.dialCode
                code =  code.replacingOccurrences(of: "(", with: "")
                code =  code.replacingOccurrences(of: ")", with: "")
                code =  code.replacingOccurrences(of: "+", with: "00")

            var number : String = phoneAsset.number
            
            _ = number.remove(at: String.Index.init(encodedOffset: 0))
            
            let final = code + number
            
            return final
        }
        
        return ""
    }
    
    func agentgentCodePresentinContactsLoc(number: String) -> Bool {
        if okContacts.count > 0 {
            for contact in okContacts {
                
                for phoneNumber in contact.phoneNumbers {
                    let phoneNumberStruct = phoneNumber.value
                    let phoneNumberString = phoneNumberStruct.stringValue
                    //println_debug("print phone number: \(phoneNumberString)")
                    
                    let strMobNo = phoneNumberString
                    //println_debug(strMobNo)
                    let trimmed = String(strMobNo.filter { !" ".contains($0) })
                    //println_debug(trimmed)
                    
                    var prefixString : String = number
                    if prefixString.hasPrefix("00") {
                        prefixString = String(prefixString.dropFirst(2))
                        prefixString = "+" + prefixString
                        if trimmed == prefixString {
                            return true
                        }
                    }
                    prefixString = number
                    if prefixString.hasPrefix("00") {
                        prefixString = String(prefixString.dropFirst(4))
                        if trimmed == prefixString {
                            return true
                        }
                        prefixString = "0" + prefixString
                        if trimmed == prefixString {
                            return true
                        }
                    }
                    if trimmed.contains(find: number){
                        return true
                    }
                }
            }
        }
        return false
    }
    
    //MARK:- Helper Methods
    
    func agentgentCodePresentinContacts(agent: String) -> Bool {
       
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "OKContacts")
    
        let predicate = NSPredicate(format: "phone == %@", agent)
        fetchRequest.predicate = predicate
        
        do {
            let request = try context.fetch(fetchRequest)
            if let contacts = request as? [OKContacts] {
                if contacts.count > 0 {
                    return true
                }
            }
        } catch {
            println_debug(error)
        }
        return false
    }
    
    private func modifiedContacts(name: String?, phone: String?, imageURL: String?) -> OKContactModel {
        let cnModel =  OKContactModel(phone: phone, name: name, imageURL: imageURL)
        return cnModel
    }
    
    
    private func fetchContactsOnBackgroundThread(completionHandler: @escaping (_ result: ContactsFetchResult) -> ()) {
        DispatchQueue.global(qos: .userInitiated).async(execute: { () -> () in
            let fetchRequest: CNContactFetchRequest = CNContactFetchRequest(keysToFetch: [CNContactVCardSerialization.descriptorForRequiredKeys()])
            var contacts = [CNContact]()
            CNContact.localizedString(forKey: CNLabelPhoneNumberiPhone)
            if #available(iOS 10.0, *) {
                fetchRequest.mutableObjects = false
            }
            fetchRequest.unifyResults = true
            fetchRequest.sortOrder = .userDefault
            do {
                try CNContactStore().enumerateContacts(with: fetchRequest) { (contact, _) -> () in
                    contacts.append(contact)
                }
                DispatchQueue.main.async(execute: { () -> () in
                    completionHandler(ContactsFetchResult.success(response: contacts))
                })
            } catch let error as NSError {
                completionHandler(ContactsFetchResult.error(error: error))
            }
        })
    }
}
