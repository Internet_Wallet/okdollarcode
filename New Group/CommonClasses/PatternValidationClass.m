//
//  PatternValidationClass.m
//  OK
//
//  Created by Vinod's MacBookPro on 5/21/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

#import "PatternValidationClass.h"

@implementation PatternValidationClass

-(NSString*)Encryption:(NSString *)str{
    
    NSString *encrypted;
    
    encrypted=[[NSString alloc]init];
    
    
    for (int i = 0; i < [str length]; i++) {
        
        NSString * newChar=[NSString stringWithFormat:@"%c",[str characterAtIndex:i]];
        
        int passingValue = (int)(newChar.integerValue) + i;
        
        NSString *tempChar;
        
        switch (passingValue) {
            case 0:
                tempChar =@"A";
                break;
            case 1:
                tempChar =@"B";
                break;
            case 2:
                tempChar =@"C";
                break;
            case 3:
                tempChar =@"D";
                break;
            case 4:
                tempChar =@"E";
                break;
            case 5:
                tempChar =@"F";
                break;
            case 6:
                tempChar =@"G";
                break;
            case 7:
                tempChar =@"H";
                break;
            case 8:
                tempChar =@"I";
                break;
            case 9:
                tempChar =@"J";
                break;
            case 10:
                tempChar =@"a";
                break;
            case 11:
                tempChar =@"b";
                break;
            case 12:
                tempChar =@"c";
                break;
            case 13:
                tempChar =@"d";
                break;
            case 14:
                tempChar =@"e";
                break;
            case 15:
                tempChar =@"f";
                break;
            case 16:
                tempChar =@"g";
                break;
            case 17:
                tempChar =@"h";
                break;
            case 18:
                tempChar =@"i";
                break;
            case 19:
                tempChar =@"j";
                break;
            case 20:
                tempChar =@"k";
                break;
        }
        
        NSLog(@"tempChar : %@",tempChar);
        if (!(encrypted.length > 0)) {
            
            encrypted = [NSString stringWithFormat:@"%@%@",@"",tempChar];
            
        }else{
            
            encrypted = [NSString stringWithFormat:@"%@%@",encrypted,tempChar];
            
        }
        
    }
    
    
    
    return encrypted;
}

-(NSString *)patternEncryption:(NSString *)password withnumber:(NSString *)number{
    
    
    NSString * resultCode;
    
    resultCode=[[NSString alloc]init];
    
    NSString * multiplyValue;
    
    multiplyValue=[[NSString alloc]init];
    
    
    // step 1  multiply
    
    
    long a = (number.longLongValue) * (password.longLongValue);
    
    multiplyValue = [NSString stringWithFormat:@"%ld",a];
    
    NSLog(@"%@",multiplyValue);
    
    
    // step 2  trimming & normal encrypt & reverse encrypt
    
    
    NSString * firstEncrypt =[self Encryption:password];
    
    
    // Reverse Logic
    
    int count = (int)[password length];
    
    NSString *trimmedString=[multiplyValue substringFromIndex:MAX((int)[multiplyValue length]-count, 0)];
    
    int len = (int)[trimmedString length];
    
    NSMutableString *reverseName = [[NSMutableString alloc] initWithCapacity:len];
    
    for(int i=len-1;i>=0;i--)
    {
        [reverseName appendFormat:@"%@", [NSString stringWithFormat:@"%c",[trimmedString characterAtIndex:i]]];
        
    }
    
    NSLog(@"%@",reverseName);
    
    NSString * secondEncrypt =[self Encryption:reverseName];
    
    
    // Strp 3  merging all the string
    
    NSString * Output;
    
    
    for (int i=0; i<[trimmedString length]; i++) {
        
        Output=@"";
        
        Output=[NSString stringWithFormat:@"%c%c%c",[firstEncrypt characterAtIndex:i],[trimmedString characterAtIndex:i],[secondEncrypt characterAtIndex:i]];
        
        if ([resultCode length]==0) {
            
            resultCode=[NSString stringWithFormat:@"%@%@",@"",Output];
            
        }else{
            
            resultCode=[NSString stringWithFormat:@"%@%@",resultCode,Output];
            
        }
        
        
    }
    
    NSLog(@"Passcode : %@",resultCode);
    
    
    return resultCode;
}

@end
