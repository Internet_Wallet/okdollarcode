//
//  FavoriteSelectedViewController.swift
//  OK
//
//  Created by Ashish Kumar Singh on 12/7/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
protocol UpdateFavProtocol {
    func updateFavList()
}
class FavoriteSelectedViewController: OKBaseController {
    
    //MARK: - Outlets
    @IBOutlet weak var fbtable: UITableView!
    @IBOutlet weak var constraintForTableBottom: NSLayoutConstraint!
    @IBOutlet weak var constraintViewOptionHeight: NSLayoutConstraint!
    @IBOutlet weak var imgVSortIndicator: UIImageView!
    @IBOutlet weak var imgVDateIndicator: UIImageView!
    @IBOutlet weak var imgVFilterIndicator: UIImageView!
    @IBOutlet weak var constraintHSelectedView: NSLayoutConstraint!
    @IBOutlet weak var labelSelectedNames: UILabel!
    @IBOutlet weak var btnArrow: UIButton!
    @IBOutlet weak var stackFilter: UIView!
    
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            self.dateLabel.text = "Date".localized
            self.dateLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var sortLabel: UILabel! {
        didSet {
            self.sortLabel.text = "Sort".localized
            self.sortLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var filterLabel: UILabel! {
        didSet {
            self.filterLabel.text = "Filter".localized
            self.filterLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    var isFromModules: Bool = false
    //MARK: - Properties
    var moduleDelegate : FavoriteSelectionDelegate?
    var name : String?
    var updateFavDelegate: UpdateFavProtocol?
    var model = [FavoriteContacts]()
    var selectedModel = [FavoriteContacts]()
    var images = [#imageLiteral(resourceName: "h_paySend") ,#imageLiteral(resourceName: "iCon3"), #imageLiteral(resourceName: "h_recharge"), #imageLiteral(resourceName: "h_taxi"), #imageLiteral(resourceName: "h_hotel"), #imageLiteral(resourceName: "h_hotel")]
    var count = 0
    var screenName = ""
    var timer : Timer?
    var seconds        = 0
    var deleteObject : FavoriteContacts?
    var lastListOptions = FavoriteListOptions.filter
    var payeeMaxCount = 5
    var payeeCount = 0
    weak  var delegate : FavoriteSelectionDelegate?
    private var selectedShowListBy: (date: String, filter: String, sort: String) = ("", "Default", "Default")
    private lazy var searchBar = UISearchBar()
    private var dynamicModel = [FavoriteContacts]()
    private var longPressGestureRecognizer:UILongPressGestureRecognizer!
    private var selectionMode = SelectionMode.singleSelect
    private var selectedNames = [String]()
    var singleSelection = false
    var modelPayTo : PayToUIViewController?
    var multiPay = false
    var payToCount = 0
    
    enum FavoriteListOptions {
        case filter, sort
    }
    
    enum SelectionMode {
        case singleSelect, multipleSelect
    }
    
    //MARK: - Views Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if count > images.count {
            count = 0
        }
        
        if let _ = modelPayTo {
            payeeMaxCount = payeeMaxCount - 1
        }
        if multiPay {
            payeeCount = payToCount
        }
        self.fbtable.tableFooterView = UIView.init()
        dynamicModel.removeAll()
        orderModelByDate()
        dynamicModel = model
        setupSearchBar()
        if name == "TOPUP" || name == "OVERSEASTOPUP" {
            stackFilter.isHidden = true
        }
        
        if name != "OVERSEASTOPUP" && singleSelection == false {
            configureLongPressGesture()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.setUpNavigation()
        if model.count > 1 {
            constraintViewOptionHeight.constant = 51
        } else {
            constraintViewOptionHeight.constant = 0
        }
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //MARK: - Methods
    func setUpNavigation() {
        self.navigationItem.titleView = nil
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20.0) ?? UIFont.systemFont(ofSize: 20.0), NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = screenName.localized
        self.setDismissButton()
        if model.count > 1 {
            self.setSearchButton()
        }
    }
    
    func setDismissButton() {
        let buttonIcon      = UIImage(named: "tabBarBack")
        let leftBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: self, action: #selector(FavoriteSelectedViewController.backActions))
        leftBarButton.image = buttonIcon
        self.navigationItem.leftBarButtonItem = leftBarButton
    }
    
    func setSearchButton() {
        let buttonIcon      = UIImage(named: "offer_search_icon")
        let rightBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: self, action: #selector(showSearchView))
        rightBarButton.image = buttonIcon
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    func configureLongPressGesture() {
        longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressGestureRecognizer.minimumPressDuration = 1.0
        self.fbtable.addGestureRecognizer(longPressGestureRecognizer)
    }
    
    func setupSearchBar() {
        searchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.tintColor = UIColor.blue
        searchBar.placeholder = "Search".localized
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
               //(appDel.currentLanguage == "uni") ? UIFont.systemFont(ofSize: 16) : myFont
                //searchTextField.font = (appDel.currentLanguage == "uni") ? UIFont.systemFont(ofSize: 16) : myFont
                
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.keyboardType = .asciiCapable
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
            }
        }
        let view = self.searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = UIColor.gray
            }
        }
    }
    
    func deleteObject(object: FavoriteContacts, indexPath: IndexPath) {
        favoriteManager.deleteFavoriteRecord(contact: object) { isSuccess in
            if isSuccess {
                var indx: Int?
                for (indexN, item) in self.model.enumerated() {
                    if let ph1 = item.phone, let ph2 = self.deleteObject?.phone {
                        if ph1 == ph2 {
                            indx = indexN
                            break
                        }
                    }
                }
                if let indxToDel = indx {
                    self.model.remove(at: indxToDel)
                }
                let removName = self.dynamicModel[indexPath.row].name ?? ""
                self.dynamicModel.remove(at: indexPath.row)
                DispatchQueue.main.async {
                    switch self.selectionMode {
                    case .multipleSelect:
                        if let index = self.selectedNames.index(of: removName) {
                            self.selectedNames.remove(at: index)
                        }
                        let allNames = self.selectedNames.joined(separator: ", ")
                        self.labelSelectedNames.text = allNames
                        if allNames.count < 1 {
                            self.selectionMode = .singleSelect
                            self.longPressGestureRecognizer.isEnabled = true
                            self.constraintHSelectedView.constant = 0
                            self.btnArrow.isHidden = true
                            self.view.layoutIfNeeded()
                        }
                    default:
                        break
                    }
                    if self.dynamicModel.count < 1 {
                        self.constraintViewOptionHeight.constant = 0
                        self.navigationItem.rightBarButtonItem = nil
                    } else {
                        self.constraintViewOptionHeight.constant = 51
                    }
                    self.fbtable.reloadData()
                    self.removeProgressView()
                    self.view.layoutIfNeeded()
                }
            }
            self.deleteObject = nil
        }
    }
    
    func orderModelByDate(orderBy: ComparisonResult = .orderedDescending) {
        //        model = model.filter({ (record) -> Bool in
        //            return !(record.phone?.hasPrefix("01") ?? true) || !(record.phone?.hasPrefix("02") ?? true)
        //        })
        model = model.sorted {
            guard let date1 = $0.createDate, let date2 = $1.createDate else { return false }
            return date1.compare(date2) == orderBy
        }
    }
    
    func applySearch(with searchText: String, arrayToSearch: [FavoriteContacts]) {
        dynamicModel = arrayToSearch.filter {
            if let name = $0.name {
                if name.lowercased().range(of:searchText.lowercased()) != nil {
                    return true
                }
            }
            if let phoneNumber = $0.phone {
                if phoneNumber.lowercased().range(of: searchText.lowercased()) != nil {
                    return true
                }
            }
            return false
        }
        fbtable.reloadData()
    }
    
    func navigateToNextScreen(tableView: UITableView, indexPath: IndexPath) {
        
        let favModel = dynamicModel[indexPath.row]
        let cell = tableView.cellForRow(at: indexPath) as? FavDisplayCell
        let phone = cell?.sendingNumber ?? ""
        
        if let favDelegate = self.moduleDelegate {
            let fav = FavModel.init(favModel.id.safelyWrappingString(), phone: favModel.phone.safelyWrappingString(), name: favModel.name.safelyWrappingString(), createDate: favModel.createDate.safelyWrappingString(), agentID: favModel.agentID.safelyWrappingString(), type: favModel.type.safelyWrappingString())
            
            self.navigationController?.dismiss(animated: false, completion: {
                favDelegate.selectedFavoriteObject(obj: fav)
            })
            return
        }
        
        if favModel.type.safelyWrappingString().lowercased() == "POSTPAID".lowercased(){
            
            if let vc = PPMStrings.FileNames.PPMStoryBoardObj.instantiateViewController(withIdentifier: "PPMGetBillVCSID") as? PPMGetBillVC{
                vc.favoriteTouple = ("095013355","095013355")
                let navController = PPMNavController.init(rootViewController: vc)
                navController.modalPresentationStyle = .fullScreen
                vc.modalPresentationStyle = .fullScreen
                self.present(navController, animated: true, completion: nil)
            }
        }
        
        if favModel.type.safelyWrappingString().lowercased() == "LANDLINE".lowercased(){
            
            if let vc = LLBStrings.FileNames.llbStoryBoardObj.instantiateViewController(withIdentifier: "LLBGetBillVCSID") as? LLBGetBillVC{
                vc.favoriteTouple = ("Yangon","018201365")
                let navController = LLBNavController.init(rootViewController: vc)
                navController.modalPresentationStyle = .fullScreen
                vc.modalPresentationStyle = .fullScreen
                self.present(navController, animated: true, completion: nil)
            }
        }
        
        if favModel.type.safelyWrappingString().lowercased() == "Topup".lowercased() || favModel.type.safelyWrappingString().lowercased() == "Topupother".lowercased() {
            let vc = UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "TopupNavigation"))
            
            if let navigation = vc as? UINavigationController {
                for vc in navigation.viewControllers {
                    if let tHome = vc as? TopupHomeViewController {
                        tHome.shortcutMenu = 0
                        tHome.favoriteTouple = (phone,favModel.name.safelyWrappingString())
                    }
                }
            }
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        } else if favModel.type.safelyWrappingString().lowercased() == "overseastopup".lowercased() {
            
            //            InternationalTopupManager.selectedCountry =  (countryObject.code, countryObject.name)
//            let vc = UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "InternationalTopup"))
//            if let nav = vc as? UINavigationController {
//                for controller in nav.viewControllers {
//                    if let overseas = controller as? InternationalTopupBaseViewController {
//                        overseas.userObject = (phone, favModel.name.safelyWrappingString())
//                    }
//                }
//            }
//            vc.modalPresentationStyle = .fullScreen
//            self.present(vc, animated: true, completion: nil)
            
            //New Design Nov07,2020
            var format = ""
            let numberFormate = favModel.phone.safelyWrappingString()
                   if numberFormate.hasPrefix("0095") {
                       format = favModel.phone.safelyWrappingString()
                   } else if numberFormate.hasPrefix("09") {
                       format = "0095" + favModel.phone.safelyWrappingString().dropFirst()
                   } else { // case a -> 95 993443... case b -> 993443 -> missing this
                       if (favModel.phone?.contains(find: "("))!{
                           let phoneNo = favModel.phone?.removingWhitespaces().replacingOccurrences(of: "(", with: "00").replacingOccurrences(of: ")", with: "")
                           format = phoneNo!
                       } else {
                           format = "00" + favModel.phone.safelyWrappingString()
                       }
                   }
            
            //InternationalManager.selectedCountry =  (countryObject.code, countryObject.name)
            let vc = UIStoryboard(name: "InternationalTopUp", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "InternationalMainVC")) as? InternationalMainVC
            vc?.statusScreen = "FavoriteTx"
            vc?.userObject = (format, favModel.name.safelyWrappingString())
            self.present(vc!, animated: true, completion: nil)
            
        } else {
            self.navigateToPayto(phone: phone, amount: "", name: favModel.name.safelyWrappingString(), from: PayToViewController.From.favorite)
        }
    }
    
    func updateSelectionType() {
        var isNeedToChangeSelectionType = true
        for item in model {
            if item.isSelected == true {
                isNeedToChangeSelectionType = false
                break
            }
        }
        if isNeedToChangeSelectionType {
            selectionMode = .singleSelect
            selectedNames = []
            longPressGestureRecognizer.isEnabled = true
            constraintHSelectedView.constant = 0
            btnArrow.isHidden = true
            self.view.layoutIfNeeded()
        }
    }
    
    func applyDateSortFilter(searchText: String?) {
        orderModelByDate()
        dynamicModel = model
        applyDateFilter()
        applySort()
        applyFilter()
        if let searchStr = searchText, searchStr.count > 0 {
            applySearch(with: searchStr, arrayToSearch: dynamicModel)
        } else {
            fbtable.reloadData()
        }
    }
    
    func applyDateFilter() {
        switch selectedShowListBy.date {
        case let x where x.count > 0 :
            let dateArray = selectedShowListBy.date.components(separatedBy: " to ")
            guard dateArray.count > 1 else { return }
            let fromDateStr = dateArray[0]
            let toDateStr = dateArray[1]
            println_debug(fromDateStr)
            println_debug(" - ")
            println_debug(toDateStr)
            guard let fromDate = fromDateStr.dateValue(dateFormatIs: "dd MM yyyy") else { return }
            guard let toDate = toDateStr.dateValue(dateFormatIs: "dd MM yyyy") else { return }
            dynamicModel = dynamicModel.filter {
                guard let cDate = $0.createDate else { return false }
                guard let createdDate = cDate.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SS") else { return false }
                guard let dateStart = Calendar.current.date(byAdding: Calendar.Component.day, value: -1, to: fromDate) else { return false }
                guard let dateEnd = Calendar.current.date(byAdding: Calendar.Component.day, value: 1, to: toDate) else { return false}
                return (dateStart.compare(createdDate) == .orderedAscending && createdDate.compare(dateEnd) == .orderedAscending)
            }
        default:
            break
        }
    }
    
    func applySort() {
        switch selectedShowListBy.sort {
        case "Name A to Z":
            dynamicModel = dynamicModel.sorted {
                guard let name1 = $0.name, let name2 = $1.name else { return false }
                return name1.compare(name2) == .orderedAscending
            }
        case "Name Z to A":
            dynamicModel = dynamicModel.sorted {
                guard let name1 = $0.name, let name2 = $1.name else { return false }
                return name1.compare(name2) == .orderedDescending
            }
        default:
            break
        }
    }
    
    func applyFilter() {
        switch selectedShowListBy.filter {
        case "Pay To":
            dynamicModel = dynamicModel.filter {
                guard let typeVale = $0.type else { return false }
                return typeVale.lowercased() == ("PAYTO".lowercased())
            }
        case "Hide My Number to pay":
            dynamicModel = dynamicModel.filter {
                guard let typeVale = $0.type else { return false }
                return typeVale.lowercased().contains("PAYTOID".lowercased())
            }
        case "Parking":
            dynamicModel = dynamicModel.filter {
                guard let typeVale = $0.type else { return false }
                return typeVale.lowercased().contains("Parking".lowercased())
            }
        case "Entrance":
            dynamicModel = dynamicModel.filter {
                guard let typeVale = $0.type else { return false }
                return typeVale.lowercased().contains("Entrance".lowercased())
            }
        case "Fuel":
            dynamicModel = dynamicModel.filter {
                guard let typeVale = $0.type else { return false }
                return typeVale.lowercased().contains("Fuel".lowercased())
            }
        case "Shop":
            dynamicModel = dynamicModel.filter {
                guard let typeVale = $0.type else { return false }
                return typeVale.lowercased().contains("Shop".lowercased())
            }
        case "Supermarket":
            dynamicModel = dynamicModel.filter {
                guard let typeVale = $0.type else { return false }
                return typeVale.lowercased().contains("Supermarket".lowercased())
            }
        case "Restaurant":
            dynamicModel = dynamicModel.filter {
                guard let typeVale = $0.type else { return false }
                return typeVale.lowercased().contains("Restaurant".lowercased())
            }
        default:
            break
        }
    }
    
    func showMaxCountAlert() {
        alertViewObj.wrapAlert(title: nil, body: "You can add only 5 mobile numbers".localized, img: nil)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        alertViewObj.showAlert(controller: self)
    }
    
    //MARK: - Target Methods
    @objc func backActions() {
        
        if let nav  = self.navigationController {
            if let _ = self.moduleDelegate {
                nav.dismiss(animated: true, completion: nil)
            } else {
                self.updateFavDelegate?.updateFavList()
                nav.popViewController(animated: true)
            }
            
        }
    }
    
    @objc func showSearchView() {
        searchBar.placeholder = "Search".localized
        searchBar.text = ""
        self.navigationItem.titleView = searchBar
        self.navigationItem.rightBarButtonItem = nil
        searchBar.becomeFirstResponder()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.constraintForTableBottom.constant = keyboardSize.height
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        DispatchQueue.main.async {
            self.constraintForTableBottom.constant  = 0
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .began {
            let touchPoint = gestureRecognizer.location(in: self.fbtable)
            if let indexPath = fbtable.indexPathForRow(at: touchPoint) {
                println_debug("Long press detected: indexpath - ")
                println_debug(indexPath.row)
                println_debug(" - ")
                println_debug(dynamicModel[indexPath.row])
                dynamicModel[indexPath.row].isSelected = true
                selectedNames = [dynamicModel[indexPath.row].name ?? ""]
                labelSelectedNames.text = selectedNames.joined(separator: ", ")
                fbtable.reloadRows(at: [indexPath], with: .none)
                selectionMode = .multipleSelect
                    self.constraintHSelectedView.constant = 50
                
                btnArrow.isHidden = false
                if multiPay {
                    payeeCount = payeeCount + 1
                    
                    
                    
                } else {
                    payeeCount = 1
                }
                longPressGestureRecognizer.isEnabled = false
                self.view.layoutIfNeeded()
            }
        }
    }
    
    //MARK: - Button Action Methods
    @IBAction func dateAction(_ sender: UIButton) {
        guard let reportDatePickerController = UIStoryboard(name: "ReportsA", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportDatePickerController.nameAndID) as? ReportDatePickerController else { return }
        reportDatePickerController.dateMode = .fromToDate
        reportDatePickerController.viewOrientation = .portrait
        reportDatePickerController.isResetNeeded = true
        reportDatePickerController.delegate = self
        reportDatePickerController.modalPresentationStyle = .overCurrentContext
        self.present(reportDatePickerController, animated: false, completion: nil)
    }
    
    @IBAction func sortAction(_ sender: UIButton) {
        let sortOptions = ["Default", "Name A to Z", "Name Z to A"]
        guard let favoriteViewOptionsController = UIStoryboard(name: "Favorite", bundle: Bundle.main).instantiateViewController(withIdentifier: "FavoriteViewOptionsController") as? FavoriteViewOptionsController else { return }
        favoriteViewOptionsController.listOptions = sortOptions
        favoriteViewOptionsController.optionTitle = "Sort By"
        favoriteViewOptionsController.delegate = self
        favoriteViewOptionsController.modalPresentationStyle = .fullScreen
        favoriteViewOptionsController.defaultSelectedItem = selectedShowListBy.sort
        lastListOptions = .sort
        favoriteViewOptionsController.modalPresentationStyle = .overCurrentContext
        self.present(favoriteViewOptionsController, animated: false, completion: nil)
    }
    
    @IBAction func filterAction(_ sender: UIButton) {
        let filterByOptions = ["Default", "Pay To", "Hide My Number to pay"]
        guard let favoriteViewOptionsController = UIStoryboard(name: "Favorite", bundle: Bundle.main).instantiateViewController(withIdentifier: "FavoriteViewOptionsController") as? FavoriteViewOptionsController else { return }
        favoriteViewOptionsController.listOptions = filterByOptions
        favoriteViewOptionsController.optionTitle = "Filter By"
        favoriteViewOptionsController.delegate = self
        favoriteViewOptionsController.modalPresentationStyle = .fullScreen
        favoriteViewOptionsController.defaultSelectedItem = selectedShowListBy.filter
        lastListOptions = .filter
        favoriteViewOptionsController.modalPresentationStyle = .overCurrentContext
        self.present(favoriteViewOptionsController, animated: false, completion: nil)
    }
    
    @IBAction func actionForMultiplePaytoScreen(_ sender: UIButton) {
    
        
        if payeeCount < 2 {
            let index = dynamicModel.firstIndex{$0.isSelected == true}
            if let safeIndex = index {
                navigateToNextScreen(tableView: fbtable, indexPath: IndexPath(row: safeIndex, section: 0))
            }
        } else {
            if name.safelyWrappingString().lowercased() == "TOPUP".lowercased() {
                
                let selectedList = model.filter { return $0.isSelected }
                
                var touple = [(String,String, String, Bool)]()
                
                if MultiTopupArray.main.controllers.count > 0 {
                    for list in selectedList {
                        touple.append((list.phone.safelyWrappingString(), list.name.safelyWrappingString(), "", false))
                    }
                    var dictionary = Dictionary<Int, Any>()
                    for (index, tuple) in touple.enumerated() {
                        dictionary[index] = tuple
                    }
                    self.navigationController?.dismiss(animated: true, completion: nil)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "TopupFavMultipleObjects"), object: nil, userInfo: dictionary)
                    
                } else {
                    if MultiTopupArray.main.controllers.count > 0 {
                        for (index, element) in MultiTopupArray.main.controllers.enumerated() {
                            if index >= MultiTopupArray.main.controllers.count - 1 {
                                break
                            }
                            touple.append((element.mobileNumber.text.safelyWrappingString(), element.confimMobile.text.safelyWrappingString(), element.amount.text.safelyWrappingString(), (element.confimMobile.isHidden) ? true : false))
                        }
                    }
                    
                    for list in selectedList {
                        touple.append((list.phone.safelyWrappingString(), list.name.safelyWrappingString(), "", false))
                    }
                    let vc = UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "TopupNavigation"))
                    
                    if let navigation = vc as? UINavigationController {
                        for vc in navigation.viewControllers {
                            if let tHome = vc as? TopupHomeViewController {
                                tHome.shortcutMenu = 0
                                tHome.arrayFavorite = touple
                            }
                        }
                    }
                    if touple.count > 0 {
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            } else {
                let selectedList = model.filter { return $0.isSelected }
                
                var multipayModel : PaytoObject<PayToUIViewController>?
                
                let story = PaytoConstants.global.storyboard()
                
                guard let multipay = story.instantiateViewController(withIdentifier: "MultiPaytoViewController") as? MultiPaytoViewController else { return }
                multipay.isFromFavorites = true
                guard let paytoUI = story.instantiateViewController(withIdentifier: "PayToUIViewController") as? PayToUIViewController else { return }
                paytoUI.view.frame = .zero
                multipay.view.frame = .zero
                multipayModel = PaytoObject<PayToUIViewController>.init(initial: paytoUI)
               // paytoUI.isComingFromMultipayTo = true
                if multiPay {
                    if let nav = PaytoConstants.global.navigation {
                        if let multiPayVC = nav.viewControllers.last as? MultiPaytoViewController {
                            if let firstPayToObj = multiPayVC.multipayModel?.getObjects() {
                                for (index, obj) in firstPayToObj.enumerated() {
                                    if index >= firstPayToObj.count - 1 {
                                        break
                                    }
                                    obj.view.frame = .zero
                                    obj.delegate = multipay
                                    if let nav = PaytoConstants.global.navigation {
                                        obj.favNav = nav
                                    } else {
                                        obj.favNav = self.navigationController
                                    }
                                    multipayModel?.add(object: obj)
                                }
                            }
                        }
                    }
                }
                
                if let controller = self.modelPayTo {
                    let number     = controller.mobileNumber.text
                    let name = controller.name.text
                    let amount   = controller.amount.text?.replacingOccurrences(of: ",", with: "")
                    let remarks  = controller.remarks.text
                    guard let paytoUI = story.instantiateViewController(withIdentifier: "PayToUIViewController") as? PayToUIViewController else { return }
                    paytoUI.view.frame = .zero
                    if let nav = PaytoConstants.global.navigation {
                        paytoUI.favNav = nav
                    } else {
                        paytoUI.favNav = self.navigationController
                    }
                    paytoUI.delegate = multipay
                    
                    paytoUI.contactUpgrade(number: number.safelyWrappingString(), name: name.safelyWrappingString(), checkApi: true, amount: amount ?? "", remark: remarks ?? "")
                    multipayModel?.add(object: paytoUI)
                }
                
                if selectedList.count > 0 {
                   // var i = 0
                    for list in selectedList {
                        guard let paytoUI = story.instantiateViewController(withIdentifier: "PayToUIViewController") as? PayToUIViewController else { return }
                        paytoUI.view.frame = .zero
                        if let nav = PaytoConstants.global.navigation {
                            paytoUI.favNav = nav
                           
                        } else {
                            paytoUI.favNav = self.navigationController
                        }
                        paytoUI.delegate = multipay
                        
//                        if i == 0 {
//                            paytoUI.isComingFromMultipayTo = true
//                        }else{
//                             paytoUI.isComingFromMultipayTo = false
//                        }
//                        i += 1
                        paytoUI.contactUpgrade(number: list.phone.safelyWrappingString(), name: list.name.safelyWrappingString(), checkApi: true, amount: nil)
                        multipayModel?.add(object: paytoUI)
                        
                    }
                }
                multipay.view.frame = .zero
                multipayModel?.remove(index: 0)
                multipay.multipayModel = multipayModel
                //                deSelectAllCells()
                
                //not comign from payto direct from fav
                if !isFromModules {
                   // self.navigationController?.pushViewController(multipay, animated: true)
                    if let _ = PaytoConstants.global.navigation {
                       self.navigationController?.pushViewController(multipay, animated: true)
                       //self.navigationController?.dismiss(animated: true, completion: nil)
                    }
              
                } else {
    
                    if let nav = PaytoConstants.global.navigation {
                        nav.pushViewController(multipay, animated: true)
                        self.navigationController?.dismiss(animated: true, completion: nil)
                    } else {
                        self.navigationController?.pushViewController(multipay, animated: true)
                    }
                }
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.deSelectAllCells()
            self.fbtable.reloadData()
            
            self.labelSelectedNames.text = ""
            self.constraintHSelectedView.constant = 0
            self.btnArrow.isHidden = true
            self.longPressGestureRecognizer.isEnabled = true
            self.payeeCount = 0
        }
    }
    
    private func deSelectAllCells() {
        for item in model {
            item.isSelected = false
        }
    }
    
    //MARK:- Multi Payto Action
    deinit {
        for item in model {
            item.isSelected = false
        }
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}

// MARK: - UISearchBarDelegate
extension FavoriteSelectedViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            uiButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize) ?? UIFont.systemFont(ofSize: 17)
            if appDel.currentLanguage == "my" {
                uiButton.setTitle("မလုပ်ပါ", for:.normal)
            } else if appDel.currentLanguage == "uni" {
                uiButton.setTitle("မလုပ္ပါ", for:.normal)
            } else {
                uiButton.setTitle("Cancel".localized, for:.normal)
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        if searchBar.text == "" {
//            applyDateSortFilter(searchText: "")
//        }
        if searchText.isEmpty {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.applyDateSortFilter(searchText: "")
                if self.dynamicModel.count < 1 {
                    self.constraintViewOptionHeight.constant = 0
                    self.navigationItem.rightBarButtonItem = nil
                } else {
                    self.constraintViewOptionHeight.constant = 51
                }
                searchBar.resignFirstResponder()
            }
        }
      //  self.fbtable.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == " " && range.location == 0 { return false }
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            if searchText.last == " " && text == " " { return false }
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            if updatedText.count > 50 {
                return false
            }
            if updatedText != "" && text != "\n" {
                applyDateSortFilter(searchText: updatedText)
            }
        }
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        setUpNavigation()
        applyDateSortFilter(searchText: "")
        if self.dynamicModel.count < 1 {
            self.constraintViewOptionHeight.constant = 0
            self.navigationItem.rightBarButtonItem = nil
        } else {
            self.constraintViewOptionHeight.constant = 51
        }
      //  self.fbtable.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}

//MARK: - UITableViewDelegate, UITableViewDataSource
extension FavoriteSelectedViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if dynamicModel.count == 0 {
            let noDataLabel = UILabel(frame: tableView.bounds)
            noDataLabel.font = UIFont(name:appFont, size: 18)
            noDataLabel.text = "No Records".localized
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
        } else {
            tableView.backgroundView = nil
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dynamicModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: FavDisplayCell.self), for: indexPath) as? FavDisplayCell
        cell?.wrapModel(fav: dynamicModel[indexPath.row], withImage: images[count])
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95.00
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch selectionMode {
        case .singleSelect:
            let favModel = dynamicModel[indexPath.row]
            if favModel.type.safelyWrappingString().lowercased() == "Topup".lowercased() || favModel.type.safelyWrappingString().lowercased() == "Topupother".lowercased() || favModel.type.safelyWrappingString().lowercased() == ""{
                let cell = tableView.cellForRow(at: indexPath) as? FavDisplayCell
                let phone = cell?.sendingNumber ?? ""
                if phone.hasPrefix("01") {
                    return
                }
            }
            navigateToNextScreen(tableView: tableView, indexPath: indexPath)
        case .multipleSelect:
            if dynamicModel[indexPath.row].isSelected {
                dynamicModel[indexPath.row].isSelected = false
                if let index = selectedNames.index(of: dynamicModel[indexPath.row].name ?? "") {
                    selectedNames.remove(at: index)
                }
                if payeeCount > 0 {
                    payeeCount = payeeCount - 1
                } else {
                    payeeCount = 0
                }
            } else {
                
                if payeeCount < payeeMaxCount {
                    payeeCount = payeeCount + 1
                    dynamicModel[indexPath.row].isSelected = true
                    selectedNames.append(dynamicModel[indexPath.row].name ?? "")
                    
                    if constraintHSelectedView.constant == 0{
                         self.constraintHSelectedView.constant = 50
                    }
                    
                    
                    
                    if btnArrow.isHidden == true{
                        self.btnArrow.isHidden = false
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        self.showMaxCountAlert()
                    }
                }
            }
            labelSelectedNames.text = selectedNames.joined(separator: ", ")
            updateSelectionType()
            tableView.reloadRows(at: [indexPath], with: .none)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            DispatchQueue.main.async {
                alertViewObj.wrapAlert(title: nil, body: appDel.getlocaLizationLanguage(key: "Do you want to delete?".localized), img: UIImage(named: "deleteicon"))
                
                alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
                })
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    self.showProgressView()
                    let contact = self.dynamicModel[indexPath.row]
                    self.deleteObject = contact
                    self.deleteObject(object: contact, indexPath: indexPath)
                })
                alertViewObj.showAlert(controller: self)
            }
        }
    }
}

//MARK: - FavoriteViewOptionsDelegate
extension FavoriteSelectedViewController: FavoriteViewOptionsDelegate {
    func showListBy(optionValue: String) {
        switch lastListOptions {
        case .sort:
            if optionValue == "Default" {
                imgVSortIndicator.isHidden = true
            } else {
                imgVSortIndicator.isHidden = false
            }
            selectedShowListBy.sort = optionValue
            applyDateSortFilter(searchText: searchBar.text)
        case .filter:
            if optionValue == "Default" {
                imgVFilterIndicator.isHidden = true
            } else {
                imgVFilterIndicator.isHidden = false
            }
            selectedShowListBy.filter = optionValue
            applyDateSortFilter(searchText: searchBar.text)
        }
    }
}

// MARK: - ReportDatePickerDelegate
extension FavoriteSelectedViewController: ReportDatePickerDelegate {
    func processWithDate(fromDate: Date?, toDate: Date?, dateMode: DateMode) {
        var selectedDate = ""
        switch dateMode {
        case .fromToDate:
            if let safeFDate = fromDate, let safeTDate = toDate {
                let fDateInString = safeFDate.stringValue(dateFormatIs: "dd MMM yyyy")
                let tDateInString = safeTDate.stringValue(dateFormatIs: "dd MMM yyyy")
                selectedDate = fDateInString + " to " + tDateInString
                imgVDateIndicator.isHidden = false
                selectedShowListBy.date = selectedDate
                applyDateSortFilter(searchText: searchBar.text)
            }
        default:
            break
        }
    }
    
    func resetDate() {
        imgVDateIndicator.isHidden = true
        selectedShowListBy.date = ""
        applyDateSortFilter(searchText: searchBar.text)
    }
}

//MARK: - FavDisplayCell
class FavDisplayCell : UITableViewCell {
    let validObj = PayToValidations()
    @IBOutlet var cImage: UIImageView!
    @IBOutlet var cName: UILabel!
    @IBOutlet var cNumber: UILabel!
    @IBOutlet var cDate: UILabel!
    @IBOutlet var telType: UILabel!
    @IBOutlet var typeImage: UIImageView!
    @IBOutlet weak var imgViewTick: UIImageView!
    
    var sendingNumber : String?
    
    func wrapModel(fav: FavoriteContacts, withImage img: UIImage) {
        self.cName.text  = fav.name.safelyWrappingString()
        var format = ""
        var numberFormate = fav.phone.safelyWrappingString()
        if numberFormate.hasPrefix("0095") {
            format = fav.phone.safelyWrappingString()
        } else if numberFormate.hasPrefix("09") {
            format = "0095" + fav.phone.safelyWrappingString().dropFirst()
        } else  if numberFormate.hasPrefix("00") {
            format = fav.phone.safelyWrappingString()
        }
        else { // case a -> 95 993443... case b -> 993443 -> missing this
            if (fav.phone?.contains(find: "("))!{
                let phoneNo = fav.phone?.removingWhitespaces().replacingOccurrences(of: "(", with: "(+").replacingOccurrences(of: ")", with: "")
                format = phoneNo!
            } else if numberFormate.hasPrefix("0091") {
//                _ = numberFormate.remove(at: String.Index.init(encodedOffset: 0))
//                _ = numberFormate.remove(at: String.Index.init(encodedOffset: 0))
                format = numberFormate
            }
            else {
                format = "00" + fav.phone.safelyWrappingString()
            }
        }
        sendingNumber = format
        
        // var format = fav.phone.safelyWrappingString()
        _ = format.remove(at: String.Index.init(encodedOffset: 0))
        _ = format.remove(at: String.Index.init(encodedOffset: 0))
        
        format = "+" + format
        let number = identifyCountry(withPhoneNumber: format)
        println_debug(number)
        if format.hasPrefix(number.countryCode) {
            if number.countryCode.hasPrefix("+95") {
                format = format.replacingOccurrences(of: number.countryCode, with: "0")
                self.telType.text = self.setupColorAndOperator(mobile: format)
                self.telType.textColor = MyNumberTopup.theme
            } else {
                format = format.replacingOccurrences(of: number.countryCode, with: "")
                self.telType.text = ""
            }
        }
        self.cNumber.text  = String.init(format: "(%@) %@", number.countryCode, format)
        let dateStr = fav.createDate.safelyWrappingString()
        if let cDateValue = self.getDateAndTime(timeStr: dateStr) {
            self.cDate.text = cDateValue
        }
        //self.formatDate(date: fav.createDate.safelyWrappingString())
        
        self.typeImage.image = img
        if fav.isSelected {
            imgViewTick.image = #imageLiteral(resourceName: "succ")
        } else {
            imgViewTick.image = nil
        }
        
        if fav.type == "OVERSEASTOPUP" {
            typeImage.isHidden = true
            telType.isHidden = true
        } else {
            typeImage.isHidden = false
            telType.isHidden = false
        }
    }
    
    func getDateAndTime(timeStr: String) -> String? {
//        let dateFormatter = DateFormatter()
//        dateFormatter.calendar = Calendar(identifier: .gregorian)
//        dateFormatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss.SSS"
//
//        guard let time = dateFormatter.date(from: timeStr) else { return nil }//timeStr
//        dateFormatter.dateFormat = "E, dd-MMM-yyyy HH:mm:ss"//E, d MMM yyyy HH:mm:ss"//hh:mm:ss”
//        let date = dateFormatter.string(from: time)
//        return date
        
        var date = ""
        let arrTemp = timeStr.split(separator:"T")
        if (arrTemp.count > 0) {
            let secondPart = arrTemp[1]
            let arrSSS = secondPart.split(separator:".")
            if (arrSSS.count > 0) {
                date = String(arrSSS[0])
            }
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss.SSS"
        guard let time = dateFormatter.date(from: timeStr) else { return nil }//timeStr
        dateFormatter.dateFormat = "E, dd-MMM-yyyy"//"E, d MMM yyyy hh:mm a"//HH:mm:ss
        return dateFormatter.string(from: time) + " " + date
    }
    
    fileprivate func setupColorAndOperator(mobile: String) -> String {
        //        let vc = OKBaseController()
        //        let operatorName = vc.getOparatorName(mobile)
        let operatorName = validObj.getNumberRangeValidation(mobile).operator
        if operatorName == "MPT" || operatorName == "MPT CDMA(800)" || operatorName == "MPT CDMA(450)" {
            MyNumberTopup.operatorCase = .mpt
        } else if operatorName == "Telenor" {
            MyNumberTopup.operatorCase = .telenor
        } else if operatorName == "Ooredoo" {
            MyNumberTopup.operatorCase = .ooreedo
        } else if operatorName == "MecTel" || operatorName == "MecTel CDMA" {
            MyNumberTopup.operatorCase = .mactel
        } else if operatorName == "MyTel" || operatorName == "Mytel" {
            MyNumberTopup.operatorCase = .mytel
        } else {
            MyNumberTopup.operatorCase = .okDefault
        }
        return operatorName
    }
}
