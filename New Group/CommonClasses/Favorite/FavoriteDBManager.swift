//
//  FavoriteDBManager.swift
//  OK
//
//  Created by Ashish on 6/21/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreData

class FavoriteDBManager: NSObject {
    
    //MARK:- API Response
    func sync() {
        self.syncingFavorites()
    }
    
    func count(_ selection: FavCaseSelection) -> Int {
        
        var model = fetchRecordsForFavoriteContactsEntity()
        switch selection {
        case .payto:
            model = model.filter({ $0.type.safelyWrappingString() == "PAYTO"})
            break
        case .topup, .topupother:
            model = model.filter({ $0.type.safelyWrappingString() == "TOPUPOTHER" || $0.type.safelyWrappingString() == "TOPUP" })
            break
        case .taxi:
            model = model.filter({ $0.type.safelyWrappingString() == "TAXI"})
            break
        case .hotel:
            model = model.filter({ $0.type.safelyWrappingString() == "HOTEL"})
        case .all:
            break
        case .overseas:
            model = model.filter({ $0.type.safelyWrappingString() == "OVERSEASTOPUP"})
        }
        return model.count
    }
        
    func syncingFavorites() {
        let urlString = String.init(format: Url.favorite, UserModel.shared.mobileNo, simid, getMsid(), otp)
        let ur = getUrl(urlStr: urlString, serverType: .serverApp)
        let params   = Dictionary<String,String>()
        self.genericClass(url: ur, param: params as AnyObject, httpMethod: "GET", handle: { (response, success) in
            if success {
                DispatchQueue.main.async {
                    do {
                        if let data = response as? Data {
                            let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                            if let dic = dict as? Dictionary<String,AnyObject> {
                                if let favString = dic["Data"] as? String {
                                    if let dictA = OKBaseController.convertToArrDictionary(text: favString) as? Array<Dictionary<String,Any>> {
                                        self.deleteRecords()
                                        for favorite in dictA {
                                            let favModel = FavModel.init(dictionary: favorite)
                                            println_debug(favModel.phone)
                                            self.storeFavoriteContact(model: favModel)
                                        }
                                    }
                                }
                            }
                        }
                    } catch {
                        println_debug(error)
                    }
                }
            }
        })
    }
    
    private func genericClass(url: URL, param: AnyObject, httpMethod: String, handle :@escaping (_ result: Any, _ success: Bool) -> Void ) {
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.capitalized
        
        if httpMethod == "POST" {
            let theJSONData = try? JSONSerialization.data(
                withJSONObject: param ,
                options: JSONSerialization.WritingOptions(rawValue: 0))
            let jsonString          = NSString(data: theJSONData!,
                                               encoding: String.Encoding.ascii.rawValue)
            let postLength = NSString(format:"%lu", jsonString!.length) as String
            request.setValue(postLength, forHTTPHeaderField:"Content-Length")
            request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
        }
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if((error) != nil) {
                handle(error!.localizedDescription, false)
            }else {
                handle(data as Any, true)
            }
        }
        dataTask.resume()
    }
    
    private func getContext() -> NSManagedObjectContext {
        return appDel.persistentContainer.viewContext
    }
    
    private func storeFavoriteContact(model: FavModel) {
        let context = getContext()
        let entity = NSEntityDescription.entity(forEntityName: "FavoriteContacts", in: context)
        if let wrapEntity = entity {
            if let favoriteDBObject = NSManagedObject(entity: wrapEntity, insertInto: context) as? FavoriteContacts {
                favoriteDBObject.id         = model.id
                favoriteDBObject.name       = model.name
                favoriteDBObject.phone      = model.phone
                favoriteDBObject.type       = model.type
                favoriteDBObject.createDate = model.createDate
                favoriteDBObject.agentID    = model.agentID
                favoriteDBObject.phDelete   = model.favDeleteNum
            }
            do {
                try context.save()
            } catch let error as NSError {
                println_debug(error)
            }
        }
    }
    
    func agentgentCodePresentinFavoriteLoc(number: String) -> Bool {

            let favoritesArray = favoriteManager.fetchRecordsForFavoriteContactsEntity()
            for favorite in favoritesArray {
                let numberTuple = PTManagerClass.decodeMobileNumber(phoneNumber: favorite.phone.safelyWrappingString())
                if numberTuple.number.contains(find: number) {
                    return true
                } else {
                    println_debug("Star Settings")
                }
            }
        return false
    }
    
    func fetchRecordsForFavoriteContactsEntity() -> [FavoriteContacts] {
        // Create Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FavoriteContacts")
        let context = getContext()
        var result = [FavoriteContacts]()
        
        do {
            // Execute Fetch Request
            let records = try context.fetch(fetchRequest)
            
            if let records = records as? [FavoriteContacts] {
                result = records
            }
        } catch {
            println_debug("Unable to fetch managed objects for favorite entity.")
        }
        return result
    }
    
    func insertFavoriteRecord(record: FavModel) {
        self.storeFavoriteContact(model: record)
    }
    
    private func deleteRecords() {
        let context = getContext()
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "FavoriteContacts")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            try context.execute(deleteRequest)
            try context.save()
            println_debug("Deleting all records in favorites")
        } catch {
            println_debug("Failed Deleting all records in favorites")
        }
    }
    
    func deleteFavoriteRecord(contact: FavoriteContacts, completion: @escaping (_ isSuccess: Bool) -> Void) {
        if appDelegate.checkNetworkAvail() {
            let urlString = String.init(format: Url.deleteFavorite, UserModel.shared.mobileNo, simid, getMsid(),"1", otp, contact.phDelete.safelyWrappingString(), contact.type.safelyWrappingString())
            let ur = getUrl(urlStr: urlString, serverType: .serverApp)
            let params   = Dictionary<String,String>()
            
            self.genericClass(url: ur, param: params as AnyObject, httpMethod: "GET", handle: { (response, success) in
                if success {
                    println_debug(response)
                    println_debug("favorite contact deleted")
                    completion(true)
                    
                    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FavoriteContacts")
                    let predicate = NSPredicate(format: "id == %@", contact.id.safelyWrappingString())
                    fetchRequest.predicate = predicate
                    let context = self.getContext()
                    let result = try? context.fetch(fetchRequest)
                    let resultData = result as! [FavoriteContacts]
                    
                    for object in resultData {
                        context.delete(object)
                    }
                    
                    do {
                        try context.save()
                    } catch let error as NSError  {
                        println_debug("Could not save \(error), \(error.userInfo)")
                    }
                } else {
                    completion(false)
                }
            })
        } else {
            completion(false)
        }
    }
}
