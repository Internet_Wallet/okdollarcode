//
//  FavoriteSelectionManager.swift
//  OK
//
//  Created by Ashish on 2/3/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class FavoriteSelectionManager: NSObject {
    
    static let shared = FavoriteSelectionManager()
    
    var model = [FavModel]()
    
    func loadFavoriteData(handle :@escaping (_ result: [FavModel]) -> Void) {

        let urlStr = "https://www.okdollar.co/RestService.svc/GetFavourites?MobileNumber=00959975278302&Simid=2E4F1496-DD15-4239-84BE-F9FFF1ABFE75&MSID=05&OSType=1&OTP=2E4F1496-DD15-4239-84BE-F9FFF1ABFE75"
            let ur = URL.init(string: urlStr)
            let params   = Dictionary<String,String>()
            self.genericClass(url: ur!, param: params as AnyObject, httpMethod: "GET", handle: { (response, success) in
                if success {
                    DispatchQueue.main.async {
                        do {
                            if let data = response as? Data {
                                let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                                if let dic = dict as? Dictionary<String,AnyObject> {
                                    if let favString = dic["Data"] as? String {
                                        if let dictA = OKBaseController.convertToArrDictionary(text: favString) as? Array<Dictionary<String,Any>> {
                                            for favorites in dictA {
                                                guard  let id = favorites.safeValueForKey("ID") as? String else {  return  }
                                                guard let phone = (favorites.safeValueForKey("PhoneNumber") as? String) else {
                                                    return
                                                }
                                                let name = favorites.safeValueForKey("Name") as? String
                                                let createDate  =  favorites.safeValueForKey("CreatedDate") as? String
                                                let agentID = favorites.safeValueForKey("AgentID") as? String
                                                let type = favorites.safeValueForKey("Type") as? String
                                                
                                                let model = FavModel.init(id, phone: phone, name: name!, createDate: createDate!, agentID: agentID!, type: type!)
                                                FavoriteSelectionManager.shared.model.append(model)
                                            }
                                            handle(FavoriteSelectionManager.shared.model)
                                        }
                                    }
                                }
                            }
                        } catch {
                            println_debug(error)
                        }
                    }
                }
            })
    }
    
     func genericClass(url: URL, param: AnyObject, httpMethod: String, handle :@escaping (_ result: Any, _ success: Bool) -> Void ) {
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.capitalized
        
        if httpMethod == "POST" {
            let theJSONData = try? JSONSerialization.data(
                withJSONObject: param ,
                options: JSONSerialization.WritingOptions(rawValue: 0))
            let jsonString          = NSString(data: theJSONData!,
                                               encoding: String.Encoding.ascii.rawValue)
            let postLength = NSString(format:"%lu", jsonString!.length) as String
            request.setValue(postLength, forHTTPHeaderField:"Content-Length")
            request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
        }
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if((error) != nil) {
                handle(error!.localizedDescription, false)
            }else {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                    handle(json, true)
                } catch {
                    handle(error.localizedDescription, false)
                }
            }
        }
        dataTask.resume()
    }
}
