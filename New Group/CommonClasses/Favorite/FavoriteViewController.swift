//
//  FavoriteViewController.swift
//  OK
//
//  Created by Ashish Kumar Singh on 12/5/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class FavoriteViewController: OKBaseController {
    //MARK:- Outlets
    @IBOutlet weak var favTable: UITableView!
    
    //MARK:- Properties
    fileprivate  var firstScreenData = ["Pay / Send","Top-Up Other Number","International Top-Up","Taxi","Hotel"]
    fileprivate var firstScreenImage = [#imageLiteral(resourceName: "dashboard_pay_send"),#imageLiteral(resourceName: "dashboard_other_number"),#imageLiteral(resourceName: "dashboard_overseas_recharge"),#imageLiteral(resourceName: "dashboard_toll"),#imageLiteral(resourceName: "dashboard_other_number"), #imageLiteral(resourceName: "dashboard_live_taxi"), #imageLiteral(resourceName: "dashboard_hotel"), #imageLiteral(resourceName: "dashboard_overseas_recharge")]
    fileprivate var modelFav = [FavoriteContacts]()
    fileprivate var modelFavoriteCount = [FavoriteFirstCountModel]()
    var singleSelection = false
    
    @IBOutlet weak var tableviewtopConstraint: NSLayoutConstraint!
    var directSegue : FavCaseSelection = .payto
    var isFromModules : Bool = false
    var moduleDelegate : FavoriteSelectionDelegate?
    var modelPayTo : PayToUIViewController?
    var multiPay = false
    var payToCount = 0
    var frommore = ""
    //MARK:- Views Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.updateFavoriteAccordingTo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if frommore == "More"{
         //   tableviewtopConstraint.constant = -15
        }
        else{
         //   tableviewtopConstraint.constant = 0
             self.frommore = ""
        }
   
        self.setDismissButton()
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20.0) ?? UIFont.systemFont(ofSize: 20.0), NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationItem.title = "Favorite List".localized
        self.view.setNeedsLayout()
        self.view.setNeedsDisplay()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    //MARK:- Methods
    fileprivate  func updateFavoriteAccordingTo() {
        self.modelFavoriteCount.removeAll()
        self.modelFav = favoriteManager.fetchRecordsForFavoriteContactsEntity()
        if isFromModules {
            switch directSegue {
            case .payto:
                self.moveToScreen(id: 0)
            case .topup, .topupother:
                self.moveToScreen(id: 1)
            case .overseas:
                self.moveToScreen(id: 2)
            case .taxi:
                self.moveToScreen(id: 2)
            case .hotel:
                self.moveToScreen(id: 4)
            case .all:
                self.moveToScreen(id: 5)
            }
        } else {
            let  payto = self.modelFav.filter({
                let type = $0.type.safelyWrappingString().lowercased()
                switch type {
                //case "payto", "paytoid", "parking", "entrance", "fuel", "shop", "supermarket", "restaurant":
                case "payto", "paytoid":
                    return true
                default:
                    return false
                }
            })
            self.modelFavoriteCount.append(FavoriteFirstCountModel.init(payto.count))
            
            if UserModel.shared.agentType != .advancemerchant {
                let  topup = self.modelFav.filter({ $0.type.safelyWrappingString() == "TOPUP" || $0.type.safelyWrappingString() == "TOPUPOTHER" || $0.type.safelyWrappingString() == "LANDLINE" || $0.type.safelyWrappingString() == "POSTPAID"})
                
                self.modelFavoriteCount.append(FavoriteFirstCountModel.init(topup.count))
                
                let overseas = self.modelFav.filter({ $0.type.safelyWrappingString() == "OVERSEASTOPUP"})
                self.modelFavoriteCount.append(FavoriteFirstCountModel.init(overseas.count))
                
                //                let  taxi = self.modelFav.filter({ $0.type.safelyWrappingString() == "TAXI"})
                //                self.modelFavoriteCount.append(FavoriteFirstCountModel.init(taxi.count))
                //
                //                let hotel = self.modelFav.filter({ $0.type.safelyWrappingString() == "HOTEL"})
                //                self.modelFavoriteCount.append(FavoriteFirstCountModel.init(hotel.count))
                
            }
            
            DispatchQueue.main.async {
                self.favTable.reloadData()
            }
        }
    }
    
    func setDismissButton() {
        let buttonIcon      = UIImage(named: "tabBarBack")
        let leftBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: self, action: #selector(FavoriteViewController.dismissTheViewControllerPT(_:)))
        leftBarButton.image = buttonIcon
        self.navigationItem.leftBarButtonItem = leftBarButton
    }
    
    func moveToScreen(id: Int) {
        DispatchQueue.main.async {
            var screenName = ""
            var name = "PAYTO"
            var model = [FavoriteContacts]()
            switch id {
            case 0:
                model = self.modelFav.filter({
                    let type = $0.type.safelyWrappingString().lowercased()
                    switch type {
                    //case "payto", "paytoid", "parking", "entrance", "fuel", "shop", "supermarket", "restaurant":
                    case "payto", "paytoid":
                        return true
                    default:
                        return false
                    }
                })
                name = "PAYTO"
                screenName = "Pay / Send"
                break
            case 1:
                model = self.modelFav.filter({ $0.type == "TOPUP" || $0.type == "TOPUPOTHER" || $0.type == "LANDLINE" || $0.type == "POSTPAID"} )
                name = "TOPUP"
                screenName = "Top-Up Other Number"
                break
            case 2:
                //                model = self.modelFav.filter({ $0.type == "TAXI"})
                //                name = "TAXI"
                //                screenName = "Taxi"
                //                break
                model = self.modelFav.filter({ $0.type == "OVERSEASTOPUP"})
                name = "OVERSEASTOPUP"
                screenName = "International Top-Up"
                break
            case 3:
                model = self.modelFav.filter({ $0.type == "HOTEL"})
                name = "TAXI"
                screenName = "Hotel"
                break
            case 4:
                model = self.modelFav.filter({ $0.type == "OVERSEASTOPUP"})
                name = "OVERSEASTOPUP"
                screenName = "International Top-Up"
                break
            default:
                model = self.modelFav
                break
            }
            if model.count > 0 {
                self.frommore = "More"
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: FavoriteSelectedViewController.self)) as? FavoriteSelectedViewController else { return }
                vc.updateFavDelegate = self
                vc.model = model
                vc.count = id
                vc.name = name
                vc.screenName = screenName
                vc.moduleDelegate = self.moduleDelegate
                vc.singleSelection = self.singleSelection
                vc.isFromModules = self.isFromModules
                vc.modelPayTo = self.modelPayTo
                vc.multiPay = self.multiPay
                vc.payToCount = self.payToCount
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                self.showErrorAlert(errMessage: "No Favorites Record".localized)
            }
        }
    }
    
    //MARK:- Target Methods
    @objc func dismissTheViewControllerPT(_ sender:UIBarButtonItem!) {
        if let navigation = self.navigationController {
            navigation.dismiss(animated: true, completion: nil)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

//MARK:- UITableView Datasource & Delegate
extension FavoriteViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelFavoriteCount.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: FavoriteCell.self), for: indexPath) as? FavoriteCell
        cell?.wrapFavoriteCell(firstScreenImage[indexPath.row], text: firstScreenData[indexPath.row], count: modelFavoriteCount[indexPath.row])
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.moveToScreen(id: indexPath.row)
    }
}

//MARK:- UpdateFavProtocol
extension FavoriteViewController: UpdateFavProtocol {
    func updateFavList() {
        updateFavoriteAccordingTo()
    }
}

//MARK:- UITableViewCell
class FavoriteCell: UITableViewCell {
    //MARK:- Outlets
    @IBOutlet var namesField: UILabel!
    @IBOutlet var countLabel: UILabel!
    @IBOutlet weak var imageViewl: UIImageView!
    
    //MARK:- Methods
    fileprivate func wrapFavoriteCell(_ img: UIImage, text: String, count: FavoriteFirstCountModel) {
        self.imageViewl.image = img
        self.namesField.text = text.localized
        self.countLabel.text = (count.count > 0) ? "\(count.count)" : ""
        self.namesField.font = UIFont(name: appFont, size: appFontSize)
    }
}

fileprivate struct FavoriteFirstCountModel {
    var count = 0
    init(_ count : Int) {
        self.count = count
    }
}
