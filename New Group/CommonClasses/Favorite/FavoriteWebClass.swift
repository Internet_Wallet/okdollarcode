//
//  FavoriteWebClass.swift
//  OK
//
//  Created by Ashish Kumar Singh on 12/5/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct FavoriteApi {
  static let addFav =  "http://192.168.1.13:8001/RestService.svc//AddFavourite?MobileNumber=%@&Simid=\(uuid)&MSID=%@&OSType=ios&OTP=\(uuid)&FavNum=%@&Type=%@&Name=%@&Amount=%@"
 static let AllFav = "http://192.168.1.13:8001/RestService.svc/GetFavourites?MobileNumber={lMobileNumber}&Simid={lSimid}&MSID={lMsid}&OSType={lOstype}&OTP={lOtp}"
}

class FavoriteWebClass: NSObject, WebServiceResponseDelegate {
    
    static let shared = FavoriteWebClass()
    
    var nameScreenFav = "favorite_screen"
    
    var isDataRecovered = false
    
    @IBOutlet weak var model: FavoriteModel!
    
    func callFavoriteApi() {
        DispatchQueue.global(qos: .background).async {
            let web      = WebApiClass()
            web.delegate = self
            let urlStr   = "https://www.okdollar.co/RestService.svc/GetFavourites?MobileNumber=00959975278302&Simid=2E4F1496-DD15-4239-84BE-F9FFF1ABFE75&MSID=05&OSType=1&OTP=2E4F1496-DD15-4239-84BE-F9FFF1ABFE75"

            let ur = URL.init(string: urlStr)
            
            let params   = Dictionary<String,String>()
            web.genericClass(url: ur!, param: params as AnyObject, httpMethod: "GET", mScreen: self.nameScreenFav)
        }
    }

    func webResponse(withJson json: AnyObject, screen: String) {
        if screen == nameScreenFav {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        if let favString = dic["Data"] as? String {
                            if let dictA = OKBaseController.convertToArrDictionary(text: favString) as? Array<Dictionary<String,Any>> {
                                model.modelUpdate(fav: dictA)
                            }
                        }
                    }
                }
            } catch {
                progressViewObj.removeProgressView()
            }
        }
    }
}

