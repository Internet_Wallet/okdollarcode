        //
//  FavoriteModel.swift
//  OK
//
//  Created by Ashish Kumar Singh on 12/6/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreData

enum FavScreen {
    case first
    case second
}

struct FavModel {
    var id = ""
    var phone = ""
    var name = ""
    var createDate = ""
    var agentID = ""
    var type = ""
var favDeleteNum = ""
    
    init(_ id: String,  phone: String, name: String, createDate: String, agentID: String, type: String) {
        self.id         = id
        
        if phone.hasPrefix("959") {
            self.phone = "00" + phone
        }
        else if phone.hasPrefix("09"){
            self.phone = "0095" + phone.dropFirst()
        }
        
        else if phone.hasPrefix("00"){
           self.phone      =  phone
        }
        else {
            self.phone = "00" + phone
            
        }
        
        self.name       = name
        self.createDate = createDate
        self.agentID    = agentID
        self.type       = type
        self.favDeleteNum = phone
    }
    
    init(dictionary: Dictionary<String,Any>) {
        self.id = dictionary.safeValueForKey("ID").safelyWrappingString()
        self.phone = dictionary.safeValueForKey("PhoneNumber").safelyWrappingString()
        self.name = dictionary.safeValueForKey("Name").safelyWrappingString()
        self.createDate  =  dictionary.safeValueForKey("CreatedDate").safelyWrappingString()
        self.agentID = dictionary.safeValueForKey("AgentID").safelyWrappingString()
        self.type = dictionary.safeValueForKey("Type").safelyWrappingString()
        self.favDeleteNum = dictionary.safeValueForKey("PhoneNumber").safelyWrappingString()
    }
}



