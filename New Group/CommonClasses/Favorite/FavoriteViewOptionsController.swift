//
//  FavoriteViewOptionsController.swift
//  OK
//
//  Created by E J ANTONY on 28/08/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol FavoriteViewOptionsDelegate {
    func showListBy(optionValue: String)
}

class FavoriteViewOptionsController: UIViewController {

    //MARK: - Outlets
   // @IBOutlet weak var constraintButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var tableViewList: UITableView!
    @IBOutlet weak var labelTitle: UILabel!
    
    //MARK: - Properties
    var delegate: FavoriteViewOptionsDelegate?
    private let marginGap: CGFloat = 70
    var listOptions = [String]()
    var defaultSelectedItem = "Default"
    var optionTitle = ""
    var selectedTitle = ""
    
    //MARK: - Views Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        view.isOpaque = false
    //    self.constraintViewWidth.constant = screenWidth - marginGap
      //  self.constraintButtonWidth.constant = self.marginGap
        self.view.layoutIfNeeded()
        self.tableViewList.tableFooterView = UIView()
        self.labelTitle.text = optionTitle.localized
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    //MARK: - Button Action Methods
    @IBAction func navigateBack(sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
}

//MARK: - UITableViewDelegate, UITableViewDataSource
extension FavoriteViewOptionsController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavoriteViewOptionsTableCell", for: indexPath) as? FavoriteViewOptionsTableCell
        cell?.configureWithData(optionStr: listOptions[indexPath.row])
        if defaultSelectedItem.localized == listOptions[indexPath.row].localized {
            cell?.imgView.image = #imageLiteral(resourceName: "favListOptionTick")
        } else {
            cell?.imgView.image = nil
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.showListBy(optionValue: listOptions[indexPath.row])
        self.dismiss(animated: false, completion: nil)
    }
}

//MARK: - UITableViewCell
class FavoriteViewOptionsTableCell: UITableViewCell {
    @IBOutlet weak var optionText: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    func configureWithData(optionStr: String) {
        self.optionText.text = optionStr.localized
    }
}
