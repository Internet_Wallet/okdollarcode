//
//  FavoriteContactSelectionViewController.swift
//  OK
//
//  Created by Ashish on 2/3/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol FavoriteSelectionDelegate : class {
    func selectedFavoriteObject(obj: FavModel)
}

protocol ChangeTitleForNavBar {
    func changeTitle()
}

enum FavCaseSelection {
    case payto, topup,topupother, taxi, hotel, all, overseas
}


class FavoriteContactSelectionViewController: OKBaseController, UITableViewDelegate, UITableViewDataSource {

    var model = [FavModel]()
    
    var delegate : FavoriteSelectionDelegate?
    var titleChangeDelegate: ChangeTitleForNavBar?
    
    var favoriteType : FavCaseSelection = .payto
    
    var image = UIImage.init()
    
    @IBOutlet fileprivate var favTable: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Favorites".localized
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDismissButton()
        okActivityIndicator(view: self.view)
        let urlString = String.init(format: Url.favorite, UserModel.shared.mobileNo, simid, getMsid(), otp)
        let ur = getUrl(urlStr: urlString, serverType: .serverApp)
        let params   = Dictionary<String,String>()
        self.genericClass(url: ur, param: params as AnyObject, httpMethod: "GET", handle: { (response, success) in
            DispatchQueue.main.async {
               self.removeActivityIndicator(view: self.view)
            }
            if success {
                    do {
                        if let data = response as? Data {
                            let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                            if let dic = dict as? Dictionary<String,AnyObject> {
                                if let favString = dic["Data"] as? String {
                                    if let dictA = OKBaseController.convertToArrDictionary(text: favString) as? Array<Dictionary<String,Any>> {
                                        for favorites in dictA {
                                            guard  let id = favorites.safeValueForKey("ID") as? String else {  return  }
                                            guard let phone = (favorites.safeValueForKey("PhoneNumber") as? String) else {
                                                return
                                            }
                                            let name = favorites.safeValueForKey("Name") as? String
                                            let createDate  =  favorites.safeValueForKey("CreatedDate") as? String
                                            let agentID = favorites.safeValueForKey("AgentID") as? String
                                            let type = favorites.safeValueForKey("Type") as? String
                                            
                                            let model = FavModel.init(id, phone: phone, name: name ?? "", createDate: createDate ?? "", agentID: agentID ?? "", type: type ?? "")
                                            self.model.append(model)
                                            DispatchQueue.main.async {
                                                self.updateFavoriteAccordingTo(self.favoriteType)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch {
                        println_debug(error)
                    }
                }
            })
    }
    
  fileprivate func setDismissButton() {
    
        let buttonIcon      = UIImage(named: "tabBarBack")
        let leftBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: self, action: #selector(FavoriteViewController.dismissTheViewControllerPT(_:)))
        leftBarButton.image = buttonIcon
        self.navigationItem.leftBarButtonItem = leftBarButton
        self.navigationController?.navigationBar.tintColor = .white
    }
    
    @objc func dismissTheViewControllerPT(_ sender:UIBarButtonItem!) {
        if let navigation = self.navigationController {
            navigation.dismiss(animated: true, completion: nil)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }


    func updateType(_ type:FavCaseSelection ) {
        self.favoriteType = type
    }
    
    func updateFavoriteAccordingTo(_ selection: FavCaseSelection) {
        switch selection {
        case .payto:
            model = self.model.filter({ $0.type == "PAYTO"})
            self.title = "Pay / Send"
            image = #imageLiteral(resourceName: "h_paySend")
            break
        case .topup, .topupother:
            model = self.model.filter({ $0.type == "TOPUPOTHER" || $0.type == "TOPUP" })
            self.title = "TOPUP"
            image = #imageLiteral(resourceName: "h_recharge")
            break
        case .taxi:
            model = self.model.filter({ $0.type == "TAXI"})
            self.title = "TAXI"
            image = #imageLiteral(resourceName: "h_taxi")
            break
        case .hotel:
            model = self.model.filter({ $0.type == "HOTEL"})
            self.title = "HOTEL"
            image = #imageLiteral(resourceName: "h_hotel")
        case .all:
            self.title = "Favorites"
            image = #imageLiteral(resourceName: "favorite")
            break
        case .overseas:
            model = self.model.filter({ $0.type == "OVERSEASTOPUP"})
            self.title = "OVERSEASTOPUP"
            image = #imageLiteral(resourceName: "dashboard_overseas_recharge")
        }
        
        DispatchQueue.main.async {
            self.favTable.reloadData()
        }
        
        
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: FavSelectedCell.self), for: indexPath) as? FavSelectedCell
        cell?.wrapModel(fav: self.model[indexPath.row], withImage: self.image)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95.00
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let delegate = self.delegate {
            delegate.selectedFavoriteObject(obj: self.model[indexPath.row])
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func genericClass(url: URL, param: AnyObject, httpMethod: String, handle :@escaping (_ result: Any, _ success: Bool) -> Void ) {
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.capitalized
        
        if httpMethod == "POST" {
            let theJSONData = try? JSONSerialization.data(
                withJSONObject: param ,
                options: JSONSerialization.WritingOptions(rawValue: 0))
            let jsonString          = NSString(data: theJSONData!,
                                               encoding: String.Encoding.ascii.rawValue)
            let postLength = NSString(format:"%lu", jsonString!.length) as String
            request.setValue(postLength, forHTTPHeaderField:"Content-Length")
            request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
        }
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if((error) != nil) {
                handle(error!.localizedDescription, false)
            }else {
                    handle(data as Any, true)
            }
        }
        dataTask.resume()
    }

}

class FavSelectedCell : UITableViewCell {
    @IBOutlet var cImage: UIImageView!
    @IBOutlet var cName: UILabel!
    @IBOutlet var cNumber: UILabel!
    @IBOutlet var cDate: UILabel!
    @IBOutlet var telType: UILabel!
    @IBOutlet var typeImage: UIImageView!
    
    func wrapModel(fav: FavModel, withImage img: UIImage) {
        self.cName.text  = fav.name
        
        var formattedNumber = fav.phone
        
        if formattedNumber.hasPrefix("00") {
            _ = formattedNumber.remove(at: String.Index.init(encodedOffset: 0))
            _ = formattedNumber.remove(at: String.Index.init(encodedOffset: 0))
            formattedNumber = "+" + formattedNumber
            
            let country = identifyCountry(withPhoneNumber: formattedNumber)
            
            if formattedNumber.hasPrefix(country.countryCode) {
                formattedNumber = formattedNumber.replacingOccurrences(of: country.countryCode, with: "")
                
                if country.countryCode == "+95" {
                    formattedNumber = "0" + formattedNumber
                }
                
                self.cNumber.text  = String.init(format: "(%@) %@", country.countryCode, formattedNumber)
            } else {
                self.cNumber.text = formattedNumber
            }
        } else {
            self.cNumber.text = formattedNumber
        }
        
        self.cDate.text = fav.createDate
        
        self.telType.text = self.setupColorAndOperator(mobile: fav.phone)
        self.telType.textColor = MyNumberTopup.theme
        self.typeImage.image = img
    }
    
    fileprivate func setupColorAndOperator(mobile: String) -> String {
        let vc = OKBaseController()
        let operatorName = vc.getOparatorName(mobile)
        if operatorName == "MPT" || operatorName == "MPT CDMA(800)" || operatorName == "MPT CDMA(450)" {
            MyNumberTopup.operatorCase = .mpt
        } else if operatorName == "Telenor" {
            MyNumberTopup.operatorCase = .telenor
        } else if operatorName == "Ooredoo" {
            MyNumberTopup.operatorCase = .ooreedo
        } else if operatorName == "MecTel" || operatorName == "MecTel CDMA" {
            MyNumberTopup.operatorCase = .mactel
        } else {
            MyNumberTopup.operatorCase = .okDefault
        }
        return operatorName
    }
}
