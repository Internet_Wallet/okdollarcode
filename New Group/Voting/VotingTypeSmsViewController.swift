//
//  VotingTypeSmsViewController.swift
//  VOTINGDESIGN
//
//  Created by prabu on 16/04/18.
//  Copyright © 2018 prabu. All rights reserved.
//

import Foundation
import UIKit

extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.numberStyle = .decimal
        return formatter
    }()
}

extension BinaryInteger {
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? ""
    }
}

class VotingTypeSmsViewController : OKBaseController , UITableViewDataSource , UITableViewDelegate {
    
    //Appdelegate Language Change
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var voteSmsTableView: UITableView!

    var smsAllContentArray = [[String : AnyObject]] ()
    var smsFinalArray = [[String : AnyObject]] ()
    var timelinecheck : Bool = false
    
    //TopMenu scrollview
    var navigation : UINavigationController?
    
    //MARK: MAIN METHOD
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
        self.separateAllContent()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: DATA SEPERATE
    
    func separateAllContent () {
        
        var amount = Float ()
        
        let myArraySorted = VotingUrlManager.smsAllContentArray.sorted{$1["Vote"] as! Int >= $0["Vote"] as! Int}
        
        for k in 0 ..< myArraySorted.count {
            
            let DicOne : [String : AnyObject]?
            DicOne = myArraySorted[k]

            //let DicOne = myArraySorted[k] as? [String : AnyObject]
            
            let VoteDic : NSMutableDictionary  = [:]

            VoteDic["PackageName"] = DicOne?.safeValueForKey("PackageName")
            VoteDic["IsUserCanVote"] = DicOne?.safeValueForKey("IsUserCanVote")
            VoteDic["VoteReason"] = DicOne?.safeValueForKey("VoteReason")
            VoteDic["Vote"] = DicOne?.safeValueForKey("Vote")
            VoteDic["VotePackageId"] = DicOne?.safeValueForKey("VotePackageId")
            VoteDic["AmountPerVote"] = DicOne?.safeValueForKey("AmountPerVote")

            amount = Float(DicOne?.safeValueForKey("AmountPerVote") as! Double)
            
            if DicOne?.safeValueForKey("PriceTimeLineArray") == nil {
                
                let totalamount = amount * (DicOne?.safeValueForKey("Vote") as! Float)
                
                VoteDic["TotalAmount"] = totalamount
                
                let totalvote = totalamount / amount
                
                if totalvote > 0 {
                    VoteDic["FinalTotalVote"] = totalvote
                } else {
                    VoteDic["FinalTotalVote"] = (DicOne?.safeValueForKey("Vote") as! Float)
                }
                
                VoteDic["FinalAmountPerVote"] = amount
                VoteDic["PriceTimeLineId"] = ""
                VoteDic["PriceTimeLineIdFromTime"] = ""
                VoteDic["PriceTimeLineIdToTime"] = ""
                VoteDic["PriceTimeLineArray"] = ""
                VoteDic["PriceTimeLineArrayCount"] = 0

                smsFinalArray.append(VoteDic as! [String : AnyObject])
                
            } else {
            
                let timelineArr =  DicOne?.safeValueForKey("PriceTimeLineArray") as! [Any]
            
            for m in 0 ..< timelineArr.count {
                    
                let packageDic = timelineArr[m] as? [String : Any]
                    
                    let dateValide = self.dateValidation(startdatenew: packageDic?.safeValueForKey("FromTime") as! String, Enddatenew: packageDic?.safeValueForKey("ToTime") as! String)
                    
                    if dateValide {
                        
                      //  println_debug("Timelinepart not come--------)")

                        self.timelinecheck = true
                        
                        amount = amount + (packageDic?.safeValueForKey("AdditionalAmount") as! Float)
                        
                        let totalamount = amount * (DicOne?.safeValueForKey("Vote") as! Float)
                        
                        VoteDic["FinalAmountPerVote"] = amount
                        VoteDic["TotalAmount"] = totalamount
                        VoteDic["PriceTimeLineId"] = packageDic?.safeValueForKey("PriceTimeLineId")
                        
                        let totalvote = totalamount / amount
                        
                        if totalvote > 0 {
                            VoteDic["FinalTotalVote"] = totalvote
                        } else {
                            VoteDic["FinalTotalVote"] = (DicOne?.safeValueForKey("Vote") as! Float)
                        }
                        
                        VoteDic["PriceTimeLineIdFromTime"] = packageDic?.safeValueForKey("FromTime")
                        VoteDic["PriceTimeLineIdToTime"] = packageDic?.safeValueForKey("ToTime")
                        VoteDic["PriceTimeLineArray"] = DicOne?.safeValueForKey("PriceTimeLineArray")
                        VoteDic["PriceTimeLineArrayCount"] = timelineArr.count

                        smsFinalArray.append(VoteDic as! [String : AnyObject])


                    }
                
                }
                
                 if timelinecheck == false {
                 
                    let totalamount = amount * (DicOne?.safeValueForKey("Vote") as! Float)
                    
                    VoteDic["TotalAmount"] = totalamount
                    
                    let totalvote = totalamount / amount
                    
                    if totalvote > 0 {
                        VoteDic["FinalTotalVote"] = totalvote
                    } else {
                        VoteDic["FinalTotalVote"] = (DicOne?.safeValueForKey("Vote") as! Float)
                    }
                    
                    VoteDic["FinalAmountPerVote"] = amount
                    
                    VoteDic["PriceTimeLineId"] = ""
                    VoteDic["PriceTimeLineIdFromTime"] = ""
                    
                    VoteDic["PriceTimeLineIdToTime"] = ""
                    VoteDic["PriceTimeLineArray"] = DicOne?.safeValueForKey("PriceTimeLineArray")
                    VoteDic["PriceTimeLineArrayCount"] = timelineArr.count
                    //VoteDic["PriceTimeLineArrayCount"] = 0

                    smsFinalArray.append(VoteDic as! [String : AnyObject])
                 
                 }
                
            }
            
        }
        
      //  println_debug("smsFinalArray--------\(smsFinalArray.count)")
        
        if smsFinalArray.count > 0 {

        voteSmsTableView.dataSource = self
        voteSmsTableView.delegate = self
        voteSmsTableView .reloadData()
            
        }
        
    }
    
    
    func dateValidation (startdatenew:String , Enddatenew:String) -> Bool {
        
        
        //Compare date
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.setLocale()
        let startdate = startdatenew
        let startdateString = startdate.replacingOccurrences(of: "T", with: " ")
        let finalstartDate = formatter.date(from: startdateString)
        
        let formatternew = DateFormatter()
        formatternew.calendar = Calendar(identifier: .gregorian)
        formatternew.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let enddate = Enddatenew
        let enddateString = enddate.replacingOccurrences(of: "T", with: " ")
        let finalendDate = formatter.date(from: enddateString)
        
        var checkVal = Bool()
        
        //   PTLoader.shared.hide()
        
        let currentdate = Date()
        
        if finalstartDate! <= currentdate && currentdate <= finalendDate!
        {
          //  println_debug("Equal Event----")
            
            checkVal = true
            
        } else {
            
            checkVal = false

        }
    
        return checkVal
        
    }
    
    //MARK: TABLEVIEW
    
    
    // TableView
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return (self.programArray != nil) ? self.programArray!.count : 0
        
        return smsFinalArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath as IndexPath) as! VotingTypeSmsCustomCell

        let finalDic : [String : AnyObject]?
        
        finalDic = smsFinalArray[indexPath.row]
        
        //if let finalDic = smsFinalArray[indexPath.row] as? [String : Any] {
        
            let formatter = NumberFormatter()
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = 0
            
        let nums = finalDic?.safeValueForKey("TotalAmount")
            
            let str = formatter.string(from: nums as! NSNumber)!

            let amountStr = str
        
          //  println_debug("Amount value-----\(amountStr)")
            
            cell.voteCountLbl.font = UIFont(name:appFont, size: 17.0)
        cell.voteCountLbl.text = "\(finalDic?.safeValueForKey("PackageName") as! String) - \(finalDic?.safeValueForKey("Vote") as! Float)"
            cell.voteCountLbl.sizeToFit()
            
            if amountStr == "0" {
                
                cell.amountLbl.font = UIFont(name:appFont, size: 17.0)
                cell.amountLbl.text = self.appDelegate.getlocaLizationLanguage(key: "Free")
            } else {
            cell.amountLbl.text = "\(amountStr) MMK"
            }

            cell.amountLbl.sizeToFit()
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let finalDic : [String : AnyObject]?
        
         finalDic = smsFinalArray[indexPath.row]
        
       // let finalDic = smsFinalArray[indexPath.row] as? [String : Any]

        //Check Balance
        let balanceAmount = (UserLogin.shared.walletBal as NSString).floatValue
       // println_debug("balanceAmount---------\(balanceAmount)")

        let totalAmount = finalDic?.safeValueForKey("TotalAmount") as! Float
     //   println_debug("TotalAmount---------\(totalAmount)")

        if balanceAmount < totalAmount {
            
            self.presentAddWithDrawController()
            
        } else {

        if finalDic?.safeValueForKey("IsUserCanVote") as! Int == 1 {
            
            if finalDic?.safeValueForKey("PriceTimeLineArrayCount") as! Int  == 0 {
                
                VotingUrlManager.selectedPriceTimeLineId = ""
                VotingUrlManager.selectedPriceTimeLineIdFromTime = ""
                VotingUrlManager.selectedPriceTimeLineIdToTime = ""
                VotingUrlManager.selectedPriceTimeLineArray = nil
                VotingUrlManager.selectedPriceTimeLineArrayCount = 0
                VotingUrlManager.selectedFixedAmountPerVote = Float(finalDic?.safeValueForKey("AmountPerVote") as! Double)
                VotingUrlManager.selectedFixedPerVoteCount = finalDic?.safeValueForKey("Vote") as? Float

                
            } else {
                
                if  finalDic?.safeValueForKey("PriceTimeLineIdFromTime") as! String == "" || finalDic?.safeValueForKey("PriceTimeLineIdToTime") as! String == "" {
                    
                    VotingUrlManager.selectedPriceTimeLineId = ""
                    VotingUrlManager.selectedPriceTimeLineIdFromTime = ""
                    VotingUrlManager.selectedPriceTimeLineIdToTime = ""
                    VotingUrlManager.selectedPriceTimeLineArray = finalDic?.safeValueForKey("PriceTimeLineArray") as? [Any]
                    VotingUrlManager.selectedPriceTimeLineArrayCount = finalDic?.safeValueForKey("PriceTimeLineArrayCount") as? Int
                    VotingUrlManager.selectedFixedAmountPerVote = Float(finalDic?.safeValueForKey("AmountPerVote") as! Double)
                    VotingUrlManager.selectedFixedPerVoteCount = finalDic?.safeValueForKey("Vote") as? Float
                }
                else
                {
                    
                    VotingUrlManager.selectedPriceTimeLineId = finalDic?.safeValueForKey("PriceTimeLineId") as? String
            VotingUrlManager.selectedPriceTimeLineIdFromTime = "\( finalDic?.safeValueForKey("PriceTimeLineIdFromTime") as! String)"
            VotingUrlManager.selectedPriceTimeLineIdToTime = "\( finalDic?.safeValueForKey("PriceTimeLineIdToTime") as! String)"
                    VotingUrlManager.selectedPriceTimeLineArray = finalDic?.safeValueForKey("PriceTimeLineArray") as? [Any]
                    VotingUrlManager.selectedPriceTimeLineArrayCount = finalDic?.safeValueForKey("PriceTimeLineArrayCount") as? Int
            VotingUrlManager.selectedFixedAmountPerVote = Float(finalDic?.safeValueForKey("AmountPerVote") as! Double)
                    VotingUrlManager.selectedFixedPerVoteCount = finalDic?.safeValueForKey("Vote") as? Float
                }

            }
            
            VotingUrlManager.selectedTotalAmount = finalDic?.safeValueForKey("TotalAmount") as? Float
            VotingUrlManager.selectedAmountPerVote =  finalDic?.safeValueForKey("FinalAmountPerVote") as? Float
            VotingUrlManager.selectedTotalVotes = finalDic?.safeValueForKey("FinalTotalVote") as? Float
            VotingUrlManager.selectedVotePackageID = "\( finalDic?.safeValueForKey("VotePackageId") as! String)"

            VotingUrlManager.selectedNumberofVote = finalDic?.safeValueForKey("FinalTotalVote") as? Float
            VotingUrlManager.selectedTotalVoteAmount = finalDic?.safeValueForKey("TotalAmount") as? Float
            VotingUrlManager.selectedVotePackageName = "Sms Votes"

        
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: VotingConformationViewController.self)) as? VotingConformationViewController
          //  println_debug("navigation issue------\(self.navigation)")
            if let navigation = self.navigation, let vc = vc {
                navigation.pushViewController(vc, animated: true)
            }
        }
            
        } else {
            self.showErrorAlert(errMessage:finalDic?.safeValueForKey("VoteReason") as! String)
            
            }
            
        }
           
    }
    
    
    //MARK: BALANCE CHECK
    func presentAddWithDrawController()
    {
       
            //Show Alert
            var lowBalanceAlert = ""
            let myAttribute = [NSAttributedString.Key.font: UIFont(name: appFont, size: 17) ?? UIFont.systemFont(ofSize: 17)]
            
            if self.appDelegate.getSelectedLanguage() == "en"
            {
                lowBalanceAlert = NSAttributedString(string: self.appDelegate.getlocaLizationLanguage(key: "Insufficient Balance"), attributes: myAttribute).string
                
            }
            else
            {
                lowBalanceAlert = NSAttributedString(string: self.appDelegate.getlocaLizationLanguage(key: "Insufficient Balance"), attributes: myAttribute).string
                
            }
            
            DispatchQueue.main.async {
                
                alertViewObj.wrapAlert(title: nil, body: lowBalanceAlert, img: #imageLiteral(resourceName: "phone_alert"))
                
                alertViewObj.addAction(title: "ADD MONEY".localized, style: .target , action: {
                    
                  //  println_debug("prabu smsview call ----> add money")
                
                    if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                        addWithdrawView.modalPresentationStyle = .fullScreen
                        self.navigation?.present(addWithdrawView, animated: true, completion: nil)
                    }
                    
                })
                
                alertViewObj.showAlert(controller: self)
                progressViewObj.removeProgressView()
                
                
            }
        
        
    }
    
    
}


class VotingTypeSmsCustomCell: UITableViewCell {
    
    @IBOutlet var voteCountLbl: UILabel!
    @IBOutlet var amountLbl: UILabel!

}
