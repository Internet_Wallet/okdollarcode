//
//  VotingReportViewController.swift
//  OK
//
//  Created by prabu on 11/04/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation
import UIKit


class VotingReportViewController : OKBaseController {
    
    //TopMenu scrollview
    var navigation : UINavigationController?
    
    //Appdelegate Language Change
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    //Outlet
    @IBOutlet weak var TransactionIDLbl: UILabel!
    @IBOutlet weak var programNameLbl: UILabel!
    @IBOutlet weak var eventNameLbl: UILabel!
    @IBOutlet weak var votePackageNameLbl: UILabel!
    @IBOutlet weak var numberofVoteLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var ParticipantNameLbl: UILabel!

    //Static Label
    @IBOutlet weak var staticTransactionIDLbl: UILabel!
    @IBOutlet weak var staticParticipantNameLbl: UILabel!
    @IBOutlet weak var staticParticipantIDLbl: UILabel!
    @IBOutlet weak var staticProgramNameLbl: UILabel!
    @IBOutlet weak var staticEventNameLbl: UILabel!
    @IBOutlet weak var staticVotePackageNameLbl: UILabel!
    @IBOutlet weak var staticNumberofVoteLbl: UILabel!
    @IBOutlet weak var staticAmountLbl: UILabel!
    
    @IBOutlet weak var staticThanksForVoting: UILabel!
    @IBOutlet weak var OKBtnOut: UIButton!

    
    //MARK: MAIN METHOD

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
       self.changeLanguage()
        
        if VotingUrlManager.selectedTotalAmount == 0.0 {
            self.detailView.isHidden = true
        }
        else {
            self.detailView.isHidden = false
        }
        
        self.displayAllContent ()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: LOCALIZATION
    func changeLanguage () {
        
        //Navigation Bar Title , color ,Font changes
        self.navigationItem.title = appDelegate.getlocaLizationLanguage(key: "Successful")
        navigationController?.navigationBar.titleTextAttributes  =  [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        
        ////Heading
        //let mmkString = NSMutableAttributedString(string: " MMK", attributes: mmk)
    
        staticTransactionIDLbl.font = UIFont(name:appFont, size: 17.0)
        staticTransactionIDLbl.text = appDelegate.getlocaLizationLanguage(key: "Participant ID")
        staticTransactionIDLbl.sizeToFit()
        
        staticProgramNameLbl.font = UIFont(name:appFont, size: 17.0)
        staticProgramNameLbl.text = appDelegate.getlocaLizationLanguage(key: "Programme Name")
        staticProgramNameLbl.sizeToFit()

        
        staticEventNameLbl.font = UIFont(name:appFont, size: 17.0)
        staticEventNameLbl.text = appDelegate.getlocaLizationLanguage(key: "Event Name")
        staticEventNameLbl.sizeToFit()

        staticParticipantNameLbl.font = UIFont(name:appFont, size: 17.0)
        staticParticipantNameLbl.text = appDelegate.getlocaLizationLanguage(key: "Participant Name")
        staticParticipantNameLbl.sizeToFit()

        staticVotePackageNameLbl.font = UIFont(name:appFont, size: 17.0)
        staticVotePackageNameLbl.text = appDelegate.getlocaLizationLanguage(key: "Vote Package Name")
        staticVotePackageNameLbl.sizeToFit()

        staticNumberofVoteLbl.font = UIFont(name:appFont, size: 17.0)
        staticNumberofVoteLbl.text = appDelegate.getlocaLizationLanguage(key: "Number of Votes")
        staticNumberofVoteLbl.sizeToFit()

         staticAmountLbl.font = UIFont(name:appFont, size: 17.0)
         staticAmountLbl.text = appDelegate.getlocaLizationLanguage(key: "Amount")
        staticAmountLbl.sizeToFit()

         staticThanksForVoting.font = UIFont(name:appFont, size: 17.0)
         staticThanksForVoting.text = appDelegate.getlocaLizationLanguage(key: "Thanks for Voting !")
        staticThanksForVoting.sizeToFit()

        //Button
        OKBtnOut.titleLabel?.font =  UIFont(name:appFont, size: appButtonSize)
        OKBtnOut.setTitle( appDel.getlocaLizationLanguage(key: "OK"), for: .normal)

        
    }
    
    @IBAction func OKBtn(_ sender: Any) {
        
        let viewControllers = self.navigationController!.viewControllers as [UIViewController]
        for aViewController:UIViewController in viewControllers {
            if aViewController.isKind(of: VotingParticiantsViewController.self) {
                _ = self.navigationController?.popToViewController(aViewController, animated: true)
            }
        }

    }
    
    @IBAction func backBtn(_ sender: Any) {
        
        let viewControllers = self.navigationController!.viewControllers as [UIViewController]
        for aViewController:UIViewController in viewControllers {
            if aViewController.isKind(of: VotingParticiantsViewController.self) {
                _ = self.navigationController?.popToViewController(aViewController, animated: true)
            }
        }
        
    }
    
    func displayAllContent () {
        
        TransactionIDLbl.text = VotingUrlManager.selectedParticipantID!
        
        programNameLbl.font = UIFont(name:appFont, size: 17.0)
        programNameLbl.text = VotingUrlManager.selectedProgramName
        programNameLbl.sizeToFit()

        eventNameLbl.font = UIFont(name:appFont, size: 17.0)
        eventNameLbl.text = VotingUrlManager.selectedEventName
        eventNameLbl.sizeToFit()

        votePackageNameLbl.font = UIFont(name:appFont, size: 17.0)
        votePackageNameLbl.text = VotingUrlManager.selectedVotePackageName
        votePackageNameLbl.sizeToFit()

        ParticipantNameLbl.font = UIFont(name:appFont, size: 17.0)
        ParticipantNameLbl.text = VotingUrlManager.selectedParticipantName
        ParticipantNameLbl.sizeToFit()

        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 0
        
         numberofVoteLbl.text = formatter.string(from: VotingUrlManager.selectedNumberofVote! as NSNumber)
        numberofVoteLbl.sizeToFit()
        

        let str = formatter.string(from: VotingUrlManager.selectedTotalVoteAmount! as NSNumber)
        amountLbl.text = "\(str!) MMK"
        amountLbl.sizeToFit()
        
    }
    
    
}
