//
//  VotingProgramidAPICall.swift
//  OK
//
//  Created by prabu on 08/04/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Foundation

protocol VotingProgramidAPICallDelegate {
    
    func VotingProgramidAPICallwebResponse(allData: Dictionary<String, Any>, screen: String)
    
    func showErrorAlert(errMessage: String)
    
}

class VotingProgramidAPICall : NSObject {
    
    var programdelegate : VotingProgramidAPICallDelegate?

    let delApp = UIApplication.shared.delegate as? AppDelegate

    
    func getAllProgramsMtdOne(urlStr:URL,screen:String) {
        
//        DispatchQueue.main.async {
//            PTLoader.shared.show()
//        }
        
        println_debug("Get Event url------\(urlStr)")

        
        self.getAllDetailsForInternationTopupNew(handle: { (tokenManager) in
            DispatchQueue.main.async {
            
                let paramDict = Dictionary<String,String>()
                let headerString = String.init(format: "%@ %@", tokenManager.type, tokenManager.accessToken)
                println_debug("Get All Program headerString TopupNew------\(headerString)")
                
              
                TopupWeb.genericApiWithHeader(url: urlStr, param: paramDict as AnyObject, httpMethod: "GET", header: headerString, handle: { (response, success) in
                    if success {
                        
                        DispatchQueue.main.async {
                            
                            if let dictionary = response as? Dictionary<String,Any> {
                                
                                println_debug("Get All Program dictionary TopupNew------\(dictionary)")
                                    guard(self.programdelegate?.VotingProgramidAPICallwebResponse(allData:dictionary, screen: screen) != nil) else {return}
                                    
                                       // PTLoader.shared.hide()
                            
                            }
                        }
                    }
                })
            }
        })
        
    }
    
    
     func getAllDetailsForInternationTopupNew(handle :@escaping (_ tokenManager: VotingUrlManager.accessManager) -> Void) {
        let url  = VotingUrlManager.getUrl(VotingConstants.urls.getTokens)
        println_debug("Token API url TopupNew------\(url)")
        
        let hash = VotingConstants.ApiKeys.accessKey.hmac_SHA1(key: VotingConstants.ApiKeys.secretKey)
        println_debug("Token API hash TopupNew------\(hash)")
        
        let parameters = String.init(format: "password=%@&grant_type=password", hash)
        println_debug("Token API Parameter TopupNew------\(parameters)")
        TopupWeb.genericApiWithHeader(url: url, param: parameters as AnyObject, httpMethod: "POST", header: nil) { (result, success) in
            if success {
                if let dictionary = result as? Dictionary<String,Any> {
                  //  println_debug("Token API dictionary------\(dictionary)")
                    
                    let token = dictionary.safeValueForKey("access_token") as? String
                   // println_debug("Token API token------\(token)")
                    
                    let type  = dictionary.safeValueForKey("token_type") as? String
                   // println_debug("Token API type------\(type)")
                    
                    let tokenManager = VotingUrlManager.accessManager.init(token ?? "", type: type ?? "")
                   // println_debug("Token API tokenManager------\(tokenManager)")
                    
                  //  DispatchQueue.main.async {
                        handle(tokenManager)
                   // }
                }
            }
        }
    }
    
    
    
    
    
    
}
