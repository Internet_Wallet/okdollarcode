//
//  VotingConstants.swift
//  OK
//
//  Created by prabu on 06/04/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

public struct VotingConstants {
    // stricture for api keys
    public struct ApiKeys {
        static let secretKey = "+6hVcLhU1UjQIRznqWwZRKr9O2Fycwl5djJOTW92M2xVUzdjQzZ3WkV5YXBxNFVya1pUaUpiQzJUTVhQTWdiQkJxNEZ0azQxYXpva0g2TDFoZm1PY1VjM1FFSjBpZ01oWFpuQTlQODllYklVeGN2V0JncnE0TWpxT0J6b2JFVDNtR01yU0syU2htdEFNSEd4U3d5Q3lnbkt6N3Y3WlVpYUlPcnFQRUQwb1pKUmRqZEc1QlZvUFFFR2NDMlZZNDNNRFQxOGFaaE9zOFlaV1dxUWRwQ1g4M0sxSmxPTkxzYnRmN05LcmZxZmVHNVIyb3VHVmwydDJmTktJOXl5TFVDQWdvNFpJZ2o3OWczMjgzOWUxSDFrenMxQWZiNzVkOTI1LWZlYmEtNDdkMi1hZWYwLTA2MzUxYWE3N2NjNQ=="
        static let accessKey = "2U10vAJ0oCL3mau6KAfFwg=="
    }
    private struct VotingUrl {
        static let voteProdUrl = "https://votingapi.okdollar.org/"
        static let voteTestUrl = "http://test.voting.api.okdollar.org/"
    }
    // base url formation
    public var baseUrl: String {
        #if DEBUG
        return serverUrl == .productionUrl ? VotingUrl.voteProdUrl : VotingUrl.voteTestUrl
        #else
        return VotingUrl.voteProdUrl
        #endif
    }
    
    public struct urls {
        static var getTokens    = "token"
        static var getAllprogrammes = "programmes"
        static var getAllEvent = "events"
        static var getAllsponsorships = "sponsorships"
        static var getAllorganizer = "organizer"
        static var getAllparticipants = "participants"
        static var getpaidvote = "paidvote"
        static var getfreevote = "freevote"
    }
    
    // error messages
    public struct errorMsgs {
        static let numberAlert = "Invalid Number ! Please Select Correct Number Range 4 to 13"
        static let serviceNotAvail = "Service not available in this country"
    }
    
    // acceptableCharacters
    public struct acceptableCharacters {
        static let mobileNumber = "0123456789"
    }
}

class VotingUrlManager {
    func internationalTopupAlert(title: String, body: String) {
        alertViewObj.wrapAlert(title: title, body: body, img: #imageLiteral(resourceName: "dashboard_overseas_recharge"))
        alertViewObj.addAction(title: "OK", style: .cancel) {
            
        }
        alertViewObj.showAlert(controller: UIViewController.init())
    }
    
    func validations(countryCode: String) -> (min: Int, max: Int) {
        var validation = (0,0)
        switch countryCode {
        case "+91":
            validation = (10,10)
        case "+86":
            validation = (11,11)
        case "+66":
            validation = (9,9)
        default:
            validation = (4,13)
        }
        return validation
    }
    
    public static var selectedProgram = [String: AnyObject]()
    public static var selectedEvent = [String: AnyObject]()
    public static var selectedparticipantId : String?
    
    //Conformation Pay button validation
    public static var onlineAllContentArray = [[String : AnyObject]] ()
    public static var callAllContentArray = [[String : AnyObject]] ()
    public static var smsAllContentArray = [[String : AnyObject]] ()
    
    //PaidVote
    public static var selectedTotalAmount: Float?
    public static var selectedAmountPerVote: Float?
    public static var selectedTotalVotes: Float?
    public static var selectedPriceTimeLineId: String?
    public static var selectedDestinationNumber: String?
    public static var selectedTransactionID: String?
    public static var selectedVotePackageID: String?
    public static var selectedPriceTimeLineIdFromTime: String?
    public static var selectedPriceTimeLineIdToTime: String?
    public static var selectedPriceTimeLineArray: [Any]?
    public static var selectedPriceTimeLineArrayCount: Int?
    public static var selectedFixedAmountPerVote: Float?
    public static var selectedFixedPerVoteCount: Float?
    
    //Conformation PageDisplay
    public static var conformationtopImageUrl: String?
    public static var selectedParticipantName: String?
    public static var selectedParticipantID: String?
    public static var selectedProgramName: String?
    public static var selectedEventName: String?
    public static var selectedVotePackageName: String?
    public static var selectedNumberofVote: Float?
    public static var selectedTotalVoteAmount: Float?
    
    //Event chack
    public static var eventCheck: String?
    public static var voteEndDate: String?
    public static var voteStartDate: String?
    public static var selectedCountry: (code: String, flag: String) = ("+66", "TH")
    
    struct accessManager {
        var accessToken = ""
        var type        = ""
        init(_ token: String, type: String) {
            self.accessToken = token
            self.type        = type
        }
    }
    
    static func getUrl(_ appendedUrl: String) -> URL {
        let urlString = String.init(format: "%@%@", VotingConstants.init().baseUrl, appendedUrl)
        let url = URL.init(string: urlString)
        return url!
    }
    
    static func getParticipantsUrl(_ appendedUrl: String) -> URL {
        let urlString = String.init(format: "%@%@", VotingConstants.init().baseUrl, appendedUrl)
        let urlStringSpace = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let url = URL.init(string: urlStringSpace!)
        return url!
    }
    
}
