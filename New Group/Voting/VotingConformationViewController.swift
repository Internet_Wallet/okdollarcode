//
//  VotingConformationViewController.swift
//  OK
//
//  Created by prabu on 11/04/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation
import UIKit


class VotingConformationViewController : OKBaseController , BioMetricLoginDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var transaction = Dictionary<String,Any>()

    //TopMenu scrollview
    var navigation : UINavigationController?
    @IBOutlet weak var topImageView: UIImageView!

    //Outlet
    @IBOutlet weak var participantNameLbl: UILabel!
    @IBOutlet weak var votecodeLbl: UILabel!
    @IBOutlet weak var programNameLbl: UILabel!
    @IBOutlet weak var eventNameLbl: UILabel!
    @IBOutlet weak var votePackageNameLbl: UILabel!
    @IBOutlet weak var numberofVoteLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!

    //Terms And Condition
    @IBOutlet weak var termandconditonTransparentView: UIView!
    @IBOutlet weak var termsandconditionView: UIView!
    @IBOutlet weak var termandconditionTextView: UITextView!
    @IBOutlet weak var profileTopview: UIView!
    @IBOutlet weak var termsandconditionTopView: UIView!
    @IBOutlet weak var termsandconditionBttomView: UIView!

    //DATA
    //let manager = GeoLocationManager.shared
    //Data
    var AllFreeVoteArray = [String: AnyObject] ()
    var AllPaidVoteArray = [String: AnyObject]()
    var conformationFinalArray = [[String : AnyObject]] ()
    var timelinecheck : Bool = false

    //Static Label
    @IBOutlet weak var staticParticipantNameLbl: UILabel!
    @IBOutlet weak var staticParticipantIDLbl: UILabel!
    @IBOutlet weak var staticProgramNameLbl: UILabel!
    @IBOutlet weak var staticEventNameLbl: UILabel!
    @IBOutlet weak var staticVotePackageNameLbl: UILabel!
    @IBOutlet weak var staticNumberofVoteLbl: UILabel!
    @IBOutlet weak var staticAmountLbl: UILabel!

    @IBOutlet weak var staticTermAndConditionLbl: UILabel!
    @IBOutlet weak var payBtnOut: UIButton!
    @IBOutlet weak var acceptBtnOut: UIButton!

    //SESSION EXPIRE
    var seconds        = 60
    var timer          = Timer()
   
    //MARK: MAIN METHOD
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.loadCell(imgUrl: VotingUrlManager.conformationtopImageUrl!, imageView: topImageView)
        
        self.changeLanguage()
        
        self.termsandconditionView.isHidden = true
        self.termandconditonTransparentView.isHidden = true
        
        self.setBorderShadow()
        
        self.displayAllContent()
        
        //Add Gesture
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        view.addGestureRecognizer(tap)
        view.isUserInteractionEnabled = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Session Expire
       // self.timerPay()
        
    }
    
    //MARK: GESTURE
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
        self.termsandconditionView.isHidden = true
        self.termandconditonTransparentView.isHidden = true
        
    }
    
    //MARK: IMAGE CACHE
    let imageCache = NSCache<NSString, UIImage>()
    func loadCell(imgUrl : String , imageView:UIImageView)
    {
        if let cachedImage = imageCache.object(forKey: imgUrl as NSString) {
            DispatchQueue.main.async(execute: { () -> Void in
                imageView.image = cachedImage
            })
        } else {
            URLSession.shared.dataTask(with: NSURL(string: imgUrl)! as URL, completionHandler: {
                (data, response, error) -> Void in
                if error != nil {
                    //                    println_debug(error ?? "No Error")
                    return
                }
                else
                {
                    if let data = data, let image = UIImage(data: data) {
                        self.imageCache.setObject(image, forKey: imgUrl as NSString)
                        DispatchQueue.main.async(execute: { () -> Void in
                            let image = UIImage(data: data)
                            imageView.image = image
                        })
                    }
                }
            }).resume()
        }
    }
    
    //MARK: LOCALIZATION
    
    func changeLanguage () {
        
        //Navigation Bar Title , color ,Font changes
        self.navigationItem.title = appDelegate.getlocaLizationLanguage(key: "Confirmation")
        navigationController?.navigationBar.titleTextAttributes  =  [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]

        ////Heading
       
        staticParticipantNameLbl.font = UIFont(name:appFont, size: 15.0)
        staticParticipantNameLbl.text = appDelegate.getlocaLizationLanguage(key: "Participant Name")
        staticParticipantNameLbl.sizeToFit()
        
        staticParticipantIDLbl.font = UIFont(name:appFont, size: 15.0)
        staticParticipantIDLbl.text = appDelegate.getlocaLizationLanguage(key: "Participant ID")
        staticParticipantIDLbl.sizeToFit()

        
        staticProgramNameLbl.font = UIFont(name:appFont, size: 15.0)
        staticProgramNameLbl.text = appDelegate.getlocaLizationLanguage(key: "Programme Name")
        staticProgramNameLbl.sizeToFit()

        
        staticEventNameLbl.font = UIFont(name:appFont, size: 15.0)
        staticEventNameLbl.text = appDelegate.getlocaLizationLanguage(key: "Event Name")
        staticEventNameLbl.sizeToFit()

        staticVotePackageNameLbl.font = UIFont(name:appFont, size: 15.0)
        staticVotePackageNameLbl.text = appDelegate.getlocaLizationLanguage(key: "Vote Package Name")
        staticVotePackageNameLbl.sizeToFit()

        
        staticNumberofVoteLbl.font = UIFont(name:appFont, size: 15.0)
        staticNumberofVoteLbl.text = appDelegate.getlocaLizationLanguage(key: "Number of Votes")
        staticNumberofVoteLbl.sizeToFit()

        
        staticAmountLbl.font = UIFont(name:appFont, size: 15.0)
        staticAmountLbl.text = appDelegate.getlocaLizationLanguage(key: "Amount")
        staticAmountLbl.sizeToFit()

        staticTermAndConditionLbl.font = UIFont(name:appFont, size: 18.0)
        staticTermAndConditionLbl.text = appDelegate.getlocaLizationLanguage(key: "Terms and Conditions")

        //Button
        payBtnOut.titleLabel?.font =  UIFont(name:appFont, size: appButtonSize)
        payBtnOut.setTitle( appDel.getlocaLizationLanguage(key: "Pay"), for: .normal)

        acceptBtnOut.titleLabel?.font =  UIFont(name:appFont, size: appButtonSize)
        acceptBtnOut.setTitle( appDel.getlocaLizationLanguage(key: "Accept"), for: .normal)

    }
    
    
    //MARK:VIEW SHADOW
    func setBorderShadow () {
        
        ////TopView
        profileTopview.backgroundColor = UIColor.white
        profileTopview.layer.cornerRadius = 3.0
        
        profileTopview.layer.borderWidth = 0.3
        profileTopview.layer.borderColor = UIColor.gray.cgColor
        
        profileTopview.layer.shadowColor = UIColor.gray.cgColor
        profileTopview.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        profileTopview.layer.shadowOpacity = 0.5
        profileTopview.layer.shadowRadius = 2
        
        ////Term and Condition view
        termsandconditionView.backgroundColor = UIColor.white
        termsandconditionView.layer.cornerRadius = 20.0
        
        termsandconditionView.layer.borderWidth = 0.3
        termsandconditionView.layer.borderColor = UIColor.gray.cgColor
        
        termsandconditionView.layer.shadowColor = UIColor.gray.cgColor
        termsandconditionView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        termsandconditionView.layer.shadowOpacity = 0.5
        termsandconditionView.layer.shadowRadius = 2
        
        termsandconditionView.layer.masksToBounds = true

    }
    
    @IBAction func backBtn(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func payBtn(_ sender: Any) {
        
        if VotingUrlManager.selectedPriceTimeLineArrayCount == 0 {
        
        self.termsandconditionView.isHidden = false
        self.termandconditonTransparentView.isHidden = false
        
        self.termandconditionTextView.font = UIFont(name:appFont, size: 17.0)
            self.termandconditionTextView.text = VotingUrlManager.selectedProgram.safeValueForKey("TermsAndConditions") as? String ?? ""
            
        } else {
            
            if VotingUrlManager.selectedPriceTimeLineIdFromTime != "" || VotingUrlManager.selectedPriceTimeLineIdToTime != "" {
            
            let dateValide = self.dateValidation(startdatenew: VotingUrlManager.selectedPriceTimeLineIdFromTime!, Enddatenew: VotingUrlManager.selectedPriceTimeLineIdToTime!)
            
            if dateValide {
        
                println_debug("Conformation Page validation else part called-----")
                
                self.termsandconditionView.isHidden = false
                self.termandconditonTransparentView.isHidden = false

                self.termandconditionTextView.font = UIFont(name:appFont, size: 17.0)
                self.termandconditionTextView.text = VotingUrlManager.selectedProgram.safeValueForKey("TermsAndConditions") as? String
                
            } else {
                
                alertViewObj.wrapAlert(title: nil, body: appDelegate.getlocaLizationLanguage(key: "Voting Amount Changed Based on Price Time Lines"), img: #imageLiteral(resourceName: "dashboard_pay_send"))
                alertViewObj.addAction(title: "OK", style: .cancel, action: {
                    self.replaceTimeLineCheck()
                })
                alertViewObj.showAlert(controller: self)
                
            }
                
            } else {
                
                self.newTimeLineCheck()
                
            }
            
        }
        
    }
    
 
    @IBAction func acceptBtn(_ sender: Any) {
        
        if VotingUrlManager.selectedTotalAmount == 0.0 {
            
            self.initialWebFreeVoteCall()
        }
        else {
            
        if appDelegate.checkNetworkAvail() {
            OKPayment.main.authenticate(screenName: "VotingConformation", delegate: self)
        }else {
            self.showErrorAlert(errMessage: appDelegate.getlocaLizationLanguage(key: "No Internet Connection"))
            PTLoader.shared.hide()
        }
            
        }
        
    }
    
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if isSuccessful == true {

        if screen == "VotingConformation" {
            
            self.initialWebPaidVoteCall()

        }
        }
        
    }
    
    
    func displayAllContent () {
        
        participantNameLbl.font = UIFont(name:appFont, size: 17.0)
        participantNameLbl.text = VotingUrlManager.selectedParticipantName!
        participantNameLbl.sizeToFit()

        votecodeLbl.text = VotingUrlManager.selectedParticipantID!
        votecodeLbl.sizeToFit()

        programNameLbl.font = UIFont(name:appFont, size: 17.0)
        programNameLbl.text = VotingUrlManager.selectedProgramName!
        programNameLbl.sizeToFit()

        eventNameLbl.font = UIFont(name:appFont, size: 17.0)
        eventNameLbl.text = VotingUrlManager.selectedEventName!
        eventNameLbl.sizeToFit()

        votePackageNameLbl.font = UIFont(name:appFont, size: 17.0)
        votePackageNameLbl.text = VotingUrlManager.selectedVotePackageName!
        votePackageNameLbl.sizeToFit()

        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 0
        
        numberofVoteLbl.text = formatter.string(from: VotingUrlManager.selectedNumberofVote! as NSNumber)
        numberofVoteLbl.sizeToFit()

        let str = formatter.string(from: VotingUrlManager.selectedTotalVoteAmount! as NSNumber)
        amountLbl.text = "\(str!) MMK"
        amountLbl.sizeToFit()

    }
    
    //MARK: DATA SEPERATE
    
   func replaceTimeLineCheck () {

        var amount = Float ()
        
        amount = VotingUrlManager.selectedFixedAmountPerVote!
    
        let timelineArr =  VotingUrlManager.selectedPriceTimeLineArray!
    
    for m in 0 ..< timelineArr.count {
            
            let packageDic = timelineArr[m] as? [String : Any]
            
            let dateValide = self.dateValidation(startdatenew: packageDic?.safeValueForKey("FromTime") as! String, Enddatenew: packageDic?.safeValueForKey("ToTime") as! String)
            
            if dateValide {
                
              //  println_debug("Timelinepart not come--------)")
                
                self.timelinecheck = true
                
                amount = amount + (packageDic?.safeValueForKey("AdditionalAmount") as! Float)
                let totalamount = amount * VotingUrlManager.selectedFixedPerVoteCount!
                
                VotingUrlManager.selectedTotalAmount = totalamount
                VotingUrlManager.selectedAmountPerVote = amount
                VotingUrlManager.selectedPriceTimeLineId = packageDic?.safeValueForKey("PriceTimeLineId") as? String
               
                let totalvote = totalamount / amount
                
                VotingUrlManager.selectedTotalVotes = totalvote
                
                VotingUrlManager.selectedPriceTimeLineIdFromTime = packageDic?.safeValueForKey("FromTime") as? String
                VotingUrlManager.selectedPriceTimeLineIdToTime = packageDic?.safeValueForKey("ToTime") as? String
                
            }
            
        }
        
        if timelinecheck == false {
            
            let totalamount = amount * VotingUrlManager.selectedFixedPerVoteCount!
            
            let totalvote = totalamount / amount
        
            VotingUrlManager.selectedTotalAmount = totalamount
            VotingUrlManager.selectedAmountPerVote = amount
            VotingUrlManager.selectedTotalVotes = totalvote

            VotingUrlManager.selectedPriceTimeLineId = ""
            VotingUrlManager.selectedPriceTimeLineIdFromTime = ""
            VotingUrlManager.selectedPriceTimeLineIdToTime = ""
            
        }
        
    
    //Check Balance
    let balanceAmount = (UserLogin.shared.walletBal as NSString).floatValue
    
    if balanceAmount < VotingUrlManager.selectedTotalAmount! {
        
        self.presentAddWithDrawController()
        
    } else {
        
        //Term and condition
        self.termsandconditionView.isHidden = false
        self.termandconditonTransparentView.isHidden = false
        
        self.termandconditionTextView.font = UIFont(name:appFont, size: 17.0)
        self.termandconditionTextView.text = VotingUrlManager.selectedProgram.safeValueForKey("TermsAndConditions") as? String
        
    }
        
        
    }
    
    
    func newTimeLineCheck () {
        
        
        var amount = Float ()
        
        amount = VotingUrlManager.selectedFixedAmountPerVote!
        
        let timelineArr =  VotingUrlManager.selectedPriceTimeLineArray!
        
        for m in 0 ..< timelineArr.count {
            
            let packageDic = timelineArr[m] as? [String : Any]
            
            let dateValide = self.dateValidation(startdatenew: packageDic?.safeValueForKey("FromTime") as! String, Enddatenew: packageDic?.safeValueForKey("ToTime") as! String)
            
            if dateValide {
                
                alertViewObj.wrapAlert(title: nil, body: appDelegate.getlocaLizationLanguage(key: "Voting Amount Changed Based on Price Time Lines"), img: #imageLiteral(resourceName: "dashboard_pay_send"))
                alertViewObj.addAction(title: "OK", style: .cancel, action: {
                    
                   // println_debug("Timelinepart not come--------)")
                    
                    self.timelinecheck = true
                    
                    amount = amount + (packageDic?.safeValueForKey("AdditionalAmount") as! Float)
                    
                    let totalamount = amount * VotingUrlManager.selectedFixedPerVoteCount!
                    
                    VotingUrlManager.selectedTotalAmount = totalamount
                    VotingUrlManager.selectedAmountPerVote = amount
                    VotingUrlManager.selectedPriceTimeLineId = packageDic?.safeValueForKey("PriceTimeLineId") as? String
                    
                    let totalvote = totalamount / amount
                    VotingUrlManager.selectedTotalVotes = totalvote
                    
                    VotingUrlManager.selectedPriceTimeLineIdFromTime = packageDic?.safeValueForKey("FromTime") as? String
                    VotingUrlManager.selectedPriceTimeLineIdToTime = packageDic?.safeValueForKey("ToTime") as? String
                    
                })
                alertViewObj.showAlert(controller: self)
                
            
            }
            
        }
        
        if timelinecheck == false {
            
            let totalamount = amount * VotingUrlManager.selectedFixedPerVoteCount!
            
            let totalvote = totalamount / amount
            
            VotingUrlManager.selectedTotalAmount = totalamount
            VotingUrlManager.selectedAmountPerVote = amount
            VotingUrlManager.selectedTotalVotes = totalvote
            
            VotingUrlManager.selectedPriceTimeLineId = ""
            VotingUrlManager.selectedPriceTimeLineIdFromTime = ""
            VotingUrlManager.selectedPriceTimeLineIdToTime = ""
            
          
        }
        
        //Check Balance
        let balanceAmount = (UserLogin.shared.walletBal as NSString).floatValue
        if balanceAmount < VotingUrlManager.selectedTotalAmount! {
            self.presentAddWithDrawController()
        } else {
            //Term and condition
            self.termsandconditionView.isHidden = false
            self.termandconditonTransparentView.isHidden = false
            
            self.termandconditionTextView.font = UIFont(name:appFont, size: 17.0)
            self.termandconditionTextView.text = VotingUrlManager.selectedProgram.safeValueForKey("TermsAndConditions") as? String
        }
        
    }
    
    
    func dateValidation (startdatenew:String , Enddatenew:String) -> Bool {
        
        //Compare date
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.setLocale()
        let startdate = startdatenew
        let startdateString = startdate.replacingOccurrences(of: "T", with: " ")
        let finalstartDate = formatter.date(from: startdateString)
        
        let formatternew = DateFormatter()
        formatternew.calendar = Calendar(identifier: .gregorian)
        formatternew.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let enddate = Enddatenew
        let enddateString = enddate.replacingOccurrences(of: "T", with: " ")
        let finalendDate = formatter.date(from: enddateString)
        
        var checkVal = Bool()
        
        //   PTLoader.shared.hide()
        
        let currentdate = Date()
        
        if finalstartDate! <= currentdate && currentdate <= finalendDate!
        {
          //  println_debug("Equal Event----")
            
            checkVal = true
            
        } else {
            
            checkVal = false
            
        }
        
        return checkVal
        
    }
    
    
    //MARK: BALANCE CHECK
    func presentAddWithDrawController()
    {
        
        //Show Alert
        var lowBalanceAlert = ""
        let myAttribute = [NSAttributedString.Key.font: UIFont(name: appFont, size: 17) ?? UIFont.systemFont(ofSize: 17)]
        
        if self.appDelegate.getSelectedLanguage() == "en"
        {
            lowBalanceAlert = NSAttributedString(string: self.appDelegate.getlocaLizationLanguage(key: "Insufficient Balance"), attributes: myAttribute).string
            
        }
        else
        {
            lowBalanceAlert = NSAttributedString(string: self.appDelegate.getlocaLizationLanguage(key: "Insufficient Balance"), attributes: myAttribute).string
            
        }
        
        
        DispatchQueue.main.async {
            
            alertViewObj.wrapAlert(title: nil, body: lowBalanceAlert, img: #imageLiteral(resourceName: "phone_alert"))
            
            alertViewObj.addAction(title: "ADD MONEY".localized, style: .target , action: {
                
             //   println_debug("prabu smsview call ----> add money")
                
                if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                    addWithdrawView.modalPresentationStyle = .fullScreen
                    self.navigation?.present(addWithdrawView, animated: true, completion: nil)
                }
                
            })
            
            alertViewObj.showAlert(controller: self)
            progressViewObj.removeProgressView()
            
            
        }
        
    }

    
    //MARK: API CALL
    func initialWebFreeVoteCall() {
        
        if appDelegate.checkNetworkAvail() {
           
            self.getAllFreeVoteMtd()
         
        } else {
            self.showErrorAlert(errMessage: appDelegate.getlocaLizationLanguage(key: "No Internet Connection"))
            PTLoader.shared.hide()
        }
        
    }
    
    func initialWebPaidVoteCall() {
        
        if appDelegate.checkNetworkAvail() {
            
            self.getTransactionID()
            
        } else {
            self.showErrorAlert(errMessage: appDelegate.getlocaLizationLanguage(key: "No Internet Connection"))
            PTLoader.shared.hide()
        }
        
    }
    
     
     //MARK: TRANSACTION ID API CALL
    func getTransactionID () {
        
        PTLoader.shared.show()

        let stringUrl = TopupUrl.myNumberPayment
        let url = URL.init(string: stringUrl)
        let par = self.getParametersForPaymentTopup()
        println_debug("TopupUrl.myNumberPayment-------\(stringUrl)")
        println_debug("TopupUrl.myNumberPayment parameter-------\(par)")

        
        DispatchQueue.main.async {
            TopupWeb.genericClass(url: url!, param: par as AnyObject, httpMethod: "POST") { (response, success) in
                if success {
                    
                    if let json = response as? Dictionary<String,Any> {
                        println_debug("json111-------\(json)")

                        if let code = json["Code"] as? NSNumber, code == 200 {
                            if let data = json["Data"] as? String {
                                
                                println_debug("jsonData-------\(data)")

                                let xmlString = SWXMLHash.parse(data)
                                
                                self.enumerate(indexer: xmlString)
                                if self.transaction.safeValueForKey("resultdescription") as? String == "Transaction Successful" {
                                    
                                    //println_debug("transaction Successful-------\(self.transaction.safeValueForKey("resultdescription") as? String)")
                                    
                                    VotingUrlManager.selectedTransactionID = self.transaction.safeValueForKey("transid") as? String
                                    
                                    //println_debug("transaction Successful transid-------\(VotingUrlManager.selectedTransactionID)")

                                    self.getAllPaidVoteMtd()

                                }
                            }
                        } else {
                            
                            if let code = json["Code"] as? NSNumber, code == 303 {
                                self.showErrorAlert(errMessage:self.appDelegate.getlocaLizationLanguage(key: "Advanced Merchant can\'t able to do payment !"))
                                
                                PTLoader.shared.hide()

                            }
                            else {
                                if let msg = json["Msg"] as? String {
                                self.showErrorAlert(errMessage: msg)
                                    PTLoader.shared.hide()

                                }
                            }
                        }
                    }
                }
            }
        }
        
        
    }
    
    
    
    func getParametersForPaymentTopup() -> Dictionary<String, Any> {
        
        // Transaction cell tower
        var TransactionsCellTower  = Dictionary<String,Any>()
        TransactionsCellTower["Lac"] = ""
        TransactionsCellTower["Mcc"] = mccStatus.mcc
        TransactionsCellTower["Mnc"] = mccStatus.mnc
        TransactionsCellTower["SignalStrength"] = ""
        TransactionsCellTower["NetworkType"] = UitilityClass.getNetworkType()
        TransactionsCellTower["NetworkSpeed"] = UitilityClass.getSignalStrength()
        
        
        if UitilityClass.isConnectedToWifi(){
            let wifiInfo = UitilityClass.getNetworkInfos()[0].components(separatedBy: ",")
            TransactionsCellTower["ConnectedwifiName"] = wifiInfo[0]
            TransactionsCellTower["Ssid"] = wifiInfo[0]
            TransactionsCellTower["Mac"] = wifiInfo[1]
        }else{
            TransactionsCellTower["ConnectedwifiName"] = ""
            TransactionsCellTower["Ssid"] = ""
            TransactionsCellTower["Mac"] = ""
        }
        
        // l External Reference
        var lExternalReference = Dictionary<String,Any>()
        lExternalReference["Direction"] = true
        lExternalReference["EndStation"] = ""
        lExternalReference["StartStation"] = ""
        lExternalReference["VendorID"] = ""
        
        // L geo Location
        
        var lGeoLocation = Dictionary<String,Any>()
        lGeoLocation["CellID"] = ""
        lGeoLocation["Latitude"] = geoLocManager.currentLatitude
        lGeoLocation["Longitude"] = geoLocManager.currentLongitude
        
        // L Proximity
        var lProximity = Dictionary<String,Any>()
        lProximity["BlueToothUsers"] = [Any]()
        lProximity["SelectionMode"] = false
        lProximity["WifiUsers"] = [Any]()
        
        
        var num = ""
        for index in 0 ...  UserModel.shared.mobileNo.count - 1 {
            num = num + String.init(UserModel.shared.mobileNo[index])
            if num == "0095" {
                num = "0"
            }
        }
        
        let comments = String.init(format: "#OK-voting-na#")
        
        var lTransactions = Dictionary<String, Any>()
        lTransactions["Amount"] = VotingUrlManager.selectedTotalAmount
        lTransactions["PromoCodeId"] = ""
        lTransactions["DestinationNumberAccountType"] = ""
        lTransactions["LocalTransactionType"]  = "PAYTO"
        lTransactions["Password"] = ok_password
        lTransactions["Balance"] = (UserLogin.shared.walletBal as NSString).floatValue
        lTransactions["Destination"] = VotingUrlManager.selectedDestinationNumber
        lTransactions["MobileNumber"] = UserModel.shared.mobileNo
        lTransactions["Mode"] = true
        lTransactions["ResultCode"] = 0
        lTransactions["ResultDescription"] = "Voting System"
        lTransactions["TransactionID"] = ""
        lTransactions["TransactionType"] = "PAYTO"
        lTransactions["TransactionTime"] = ""
        lTransactions["Comments"] = comments
        lTransactions["Kickback"] = "0.0"
        lTransactions["Bonus"] = "0"

        
        lTransactions["SecureToken"] = UserLogin.shared.token
        
        var finalParameterDictionaried = Dictionary<String,Any>()
        finalParameterDictionaried["TransactionsCellTower"] = TransactionsCellTower
        finalParameterDictionaried["lExternalReference"] = lExternalReference
        finalParameterDictionaried["lGeoLocation"] = lGeoLocation
        finalParameterDictionaried["lProximity"] = lProximity
        finalParameterDictionaried["lTransactions"] = lTransactions
        
      //  println_debug(finalParameterDictionaried)
        
        return finalParameterDictionaried
    }
    
    deinit {
        geoLocManager.stopUpdateLocation()
    }

    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            transaction[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    
    func getAllPaidVoteMtd() {
        
        self.getAllDetailsForInternationTopup(handle: { (tokenManager) in
            DispatchQueue.main.async {
                
                PTLoader.shared.show()

                let headerString = String.init(format: "%@ %@", tokenManager.type, tokenManager.accessToken)
               // println_debug("Get All Program headerString------\(headerString)")
                
                let paymentStatus : String = "Success"
                
                let formatter = NumberFormatter()
                formatter.minimumFractionDigits = 0
                formatter.maximumFractionDigits = 0
                
                //HASH VALUE GENERATION FOR PAID VOTE
                let pipeLineStr = "OkAccountNumber=\(UserModel.shared.mobileNo)|DestinationNumber=\(VotingUrlManager.selectedDestinationNumber!)|TotalAmount=\(VotingUrlManager.selectedTotalAmount!)|AmountPerVote=\(VotingUrlManager.selectedAmountPerVote!)|PaymentStatus=\(paymentStatus)|OkTransactionId=\(VotingUrlManager.selectedTransactionID!)|ParticipantId=\(VotingUrlManager.selectedparticipantId!)|PriceTimeLineId=\(VotingUrlManager.selectedPriceTimeLineId!)|TotalVotes=\(Int(VotingUrlManager.selectedTotalVotes!))"
                
              //  println_debug("Paidvote pipeline str :: \(pipeLineStr)")
                
                let hashValue: String = pipeLineStr.hmac_SHA1(key: VotingConstants.ApiKeys.secretKey)
               // println_debug("Paidvote actual hash value :: \(hashValue)")
                
                let keys : [String] = ["OkAccountNumber","DestinationNumber","TotalAmount","AmountPerVote","PaymentStatus","OkTransactionId","ParticipantId","PriceTimeLineId","TotalVotes","HashValue"]
                
                let values : [String] = [UserModel.shared.mobileNo,VotingUrlManager.selectedDestinationNumber!,"\(VotingUrlManager.selectedTotalAmount!)","\(VotingUrlManager.selectedAmountPerVote!)",paymentStatus,VotingUrlManager.selectedTransactionID!,VotingUrlManager.selectedparticipantId!,VotingUrlManager.selectedPriceTimeLineId!,"\(Int(VotingUrlManager.selectedTotalVotes!))",hashValue]
                
                let jsonObj = JSONStringWriter()
                let jsonText = jsonObj.writeJsonIntoString(keys, values)

                let url = VotingUrlManager.getUrl(VotingConstants.urls.getpaidvote)
                println_debug("Get All Paidvote API url------\(url)")
                println_debug("Get All Paidvote API Parameter------\(jsonText)")

                TopupWeb.genericApiWithHeader(url: url, param: jsonText as AnyObject, httpMethod: "POST", header: headerString, handle: { (response, success) in
                    if success {
                        
                        println_debug("Get All Paidvote response------\(response)")
                        
                        DispatchQueue.main.async {
                            
                            if let dictionary = response as? Dictionary<String,Any> {
                                
                                println_debug("Get All Paidvote dictionary------\(dictionary)")
                                if dictionary .safeValueForKey("Message") as! String == "Success" {
                                
                                if dictionary .safeValueForKey("Status") as! Int == 1 {
                                    
                                    self.AllPaidVoteArray = dictionary .safeValueForKey("Result") as! [String : AnyObject]
                                    
                                    self.performSegue(withIdentifier: "VotingReportPaid", sender: self)
                                    
                                    PTLoader.shared.hide()

                                } else {
                                    
                                    self.showErrorAlert(errMessage:dictionary.safeValueForKey("Message") as! String)
                                    
                                        PTLoader.shared.hide()
                                    
                                }
                                    
                                } else {
                                    
                                  self.showErrorAlert(errMessage:dictionary.safeValueForKey("Message") as! String)
                                    
                                        PTLoader.shared.hide()

                                }
                                
                            }
                        }
                    }
                })
            }
        })
        
    
    }
 
    
    
    func getAllFreeVoteMtd() {
        
        self.getAllDetailsForInternationTopup(handle: {(tokenManager) in
            DispatchQueue.main.async {
                
                
                let headerString = String.init(format: "%@ %@", tokenManager.type, tokenManager.accessToken)
               // println_debug("Get All Freevote headerString------\(headerString)")
                
                
                let formatter = NumberFormatter()
                formatter.minimumFractionDigits = 0
                formatter.maximumFractionDigits = 0
                
                let nums = VotingUrlManager.selectedTotalVotes
                
                let totalvote = formatter.string(from: nums! as NSNumber)!
               // println_debug("totalvote second nums value-----\(totalvote)")

                //HASH VALUE GENERATION FOR FREE VOTE
                let pipeLineStr = "OkAccountNumber=\(UserModel.shared.mobileNo)|ParticipantId=\(VotingUrlManager.selectedparticipantId!)|VotePackageId=\(VotingUrlManager.selectedVotePackageID!)|TotalVotes=\(totalvote)"
              //  println_debug("Freevote pipeline str :: \(pipeLineStr)")
                
                
                let hashValue: String = pipeLineStr.hmac_SHA1(key: VotingConstants.ApiKeys.secretKey)
              //  println_debug("Freevote actual hash value :: \(hashValue)")
                
                let keys : [String] = ["OkAccountNumber","ParticipantId","VotePackageId","TotalVotes","HashValue"]
                
                let values : [String] = [UserModel.shared.mobileNo,VotingUrlManager.selectedparticipantId!,VotingUrlManager.selectedVotePackageID!,"\(totalvote)",hashValue]
                
                let jsonObj = JSONStringWriter()
                let jsonText = jsonObj.writeJsonIntoString(keys, values)
                
              //  println_debug("Get All Freevote API jsonText------\(jsonText)")
                
                let url = VotingUrlManager.getUrl(VotingConstants.urls.getfreevote)
             //   println_debug("Get All Freevote url------\(url)")
                
                TopupWeb.genericApiWithHeader(url: url, param: jsonText as AnyObject, httpMethod: "POST", header: headerString, handle: { (response, success) in
                    if success {
                        
                       // println_debug("Get All Freevote response------\(response)")
                        
                        DispatchQueue.main.async {
                            
                            if let dictionary = response as? Dictionary<String,Any> {
                                
                               // println_debug("Get All Freevote dictionary------\(dictionary)")
                                
                                if dictionary .safeValueForKey("Message") as! String == "Success" {
                                    
                                    
                                    if dictionary .safeValueForKey("Status") as! Int == 1 {
                                        
                                        self.AllFreeVoteArray = dictionary .safeValueForKey("Result") as! [String : AnyObject]
                                        
                                        self.performSegue(withIdentifier: "VotingReportPaid", sender: self)
                                        
                                        PTLoader.shared.hide()

                                    } else {
                                        
                                        self.showErrorAlert(errMessage:dictionary.safeValueForKey("Message") as! String)
                                        
                                            PTLoader.shared.hide()

                                    }
                                    
                                } else {
                                    
                                    self.showErrorAlert(errMessage:dictionary.safeValueForKey("Message") as! String)
                                    
                                        PTLoader.shared.hide()

                                }
                                
                            }
                        }
                    }
                })
            }
        })
        
    }
    
    
    private func getAllDetailsForInternationTopup(handle :@escaping (_ tokenManager: VotingUrlManager.accessManager) -> Void) {
        let url  = VotingUrlManager.getUrl(VotingConstants.urls.getTokens)
      //  println_debug("Token API url------\(url)")
        
        let hash = VotingConstants.ApiKeys.accessKey.hmac_SHA1(key: VotingConstants.ApiKeys.secretKey)
        // println_debug("Token API hash------\(hash)")
        
        let parameters = String.init(format: "password=%@&grant_type=password", hash)
        // println_debug("Token API Parameter------\(parameters)")
        TopupWeb.genericApiWithHeader(url: url, param: parameters as AnyObject, httpMethod: "POST", header: nil) { (result, success) in
            if success {
                if let dictionary = result as? Dictionary<String,Any> {
                    // println_debug("Token API dictionary------\(dictionary)")
                    
                    let token = dictionary.safeValueForKey("access_token") as? String
                    //println_debug("Token API token------\(token)")
                    
                    let type  = dictionary.safeValueForKey("token_type") as? String
                    //println_debug("Token API type------\(type)")
                    
                    let tokenManager = VotingUrlManager.accessManager.init(token ?? "", type: type ?? "")
                    //println_debug("Token API tokenManager------\(tokenManager)")
                    
                    DispatchQueue.main.async {
                        handle(tokenManager)
                    }
                }
            }
        }
    }
    
    
    
    //MARK: SESSION EXPIRE
    
    func timerPay() {
        timer = Timer.scheduledTimer(timeInterval: 5, target: self,   selector: (#selector(VotingConformationViewController.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        seconds -= 1
      //  let timerString = (seconds <= 9) ? "0:" + "0" + "\(seconds)" :  "0:" + "\(seconds)"
        
        //timerView.wrapTimerView(timer: timerString)
        if seconds == 0 {
            if timer.isValid {
                timer.invalidate()
                alertViewObj.wrapAlert(title: nil, body: "Session expired".localized, img: #imageLiteral(resourceName: "dashboard_pay_send"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                    self.navigationController?.popViewController(animated: true)
                })
                alertViewObj.showAlert(controller: self)
            }
        }
    }
}


