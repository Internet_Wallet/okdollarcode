//
//  VotingParticiantsViewController.swift
//  OK
//
//  Created by prabu on 11/04/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation
import UIKit
import MapKit


class VotingParticiantsViewController : OKBaseController , UITableViewDataSource , UITableViewDelegate ,UICollectionViewDataSource , UICollectionViewDelegate , BioMetricLoginDelegate , VotingProgramidAPICallDelegate {
    
    //TopMenu scrollview
    var navigation : UINavigationController?
    
    //Appdelegate Language Change
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var votingResultCount = Int()
    var groupmemberCount : Int = 0
    var navigationTitle = String ()
    var singleParticipantsArray = [[String: AnyObject]]()
    var votingResultArray = [[String: AnyObject]]()
    var candidateProfileArray = [[String: AnyObject]]()
    var singlecandidateProfilesDic : NSMutableDictionary  = [:]
    var groupcandidateProfilesDic : NSMutableDictionary  = [:]

  
    //Constrain Outlet
    @IBOutlet weak var tableviewheightoutlet: NSLayoutConstraint!
    @IBOutlet weak var notVotingYetLblHeight: NSLayoutConstraint!
    @IBOutlet weak var groupmemberCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var groupmemberLabelHeight: NSLayoutConstraint!


    //Outlet
    @IBOutlet weak var participantScrollView: UIScrollView!
    @IBOutlet weak var votingResultTableView: UITableView!
    @IBOutlet weak var groupmemberLbl: UILabel!
    @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var groupmemberCollectionView: UICollectionView!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var payBtnOut: UIButton!

    @IBOutlet weak var profileTopview: UIView!
    
    //Outlet Contact Detail
     @IBOutlet weak var groupnameLbl: UILabel!
     @IBOutlet weak var votecodeLbl: UILabel!
     @IBOutlet weak var totalmembercountLbl: UILabel!
    
    //Contact Constrain
  //  @IBOutlet weak var PrticipantDetailViewHeightOutlet: NSLayoutConstraint!
    @IBOutlet weak var groupCandidateDetailViewHeightOutlet: NSLayoutConstraint!
    @IBOutlet weak var singlecandidateDetailViewHeightOutlet: NSLayoutConstraint!

    
    @IBOutlet weak var singlecandidateDetailView: UIView!
    @IBOutlet weak var groupCandidateDetailView: UIView!
    
    @IBOutlet weak var singleCandidateNameLbl: UILabel!
    @IBOutlet weak var singleCandidateVoteCodeLbl: UILabel!
    @IBOutlet weak var singleCandidatedateofbirthLbl: UILabel!
    @IBOutlet weak var singleCandidateEducationLbl: UILabel!
    @IBOutlet weak var singleCandidateProfileLbl: UILabel!
    @IBOutlet weak var singleCandidateOccupationLbl: UILabel!

    
    //Latitude and Longtitude
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var stateName = String()
    var currentLatitude = String()
    var currentLongtitude = String()

    var eachSection = [String]()
    var checkGroupMember = Bool()
    var voteTypeCheck : Bool = false

    //Static Labels
    @IBOutlet weak var staticvotingdetailLbl: UILabel!
    @IBOutlet weak var staticNotVotingYetLbl: UILabel!
    @IBOutlet weak var staticParticipantDetailsLbl: UILabel!
    @IBOutlet weak var staticDescriptionDetailsLbl: UILabel!

    @IBOutlet weak var staticNameLbl: UILabel!
    @IBOutlet weak var staticParticipantIDLbl: UILabel!
    @IBOutlet weak var staticDateofBirthLbl: UILabel!
    @IBOutlet weak var staticEducationLbl: UILabel!
    @IBOutlet weak var staticProfileLbl: UILabel!
    @IBOutlet weak var staticOccupationLbl: UILabel!

    @IBOutlet weak var staticGroupMemberLbl: UILabel!
    @IBOutlet weak var staticGroupNameLbl: UILabel!
    @IBOutlet weak var staticGroupParticipantIDLbl: UILabel!
    @IBOutlet weak var staticGroupCountLbl: UILabel!


    //MARK: MAIN METHOD
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.changeLanguage()
        self.setBorderShadow()
        
       /*
        
        //CollectionView
        let itemSize = UIScreen.main.bounds.width/2 - 3
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize  = CGSize(width: itemSize , height: itemSize + 20)
        
        layout.minimumInteritemSpacing = 3
        layout.minimumLineSpacing = 3
        
        groupmemberCollectionView.collectionViewLayout = layout
        
        //Refresh Control
        if #available(iOS 10.0, *) {
            let refreshControl = UIRefreshControl()
            //let title = NSLocalizedString("PullToRefresh", comment: "Pull to refresh")
            let title = "Pull to refresh"
            refreshControl.attributedTitle = NSAttributedString(string: title)
            refreshControl.addTarget(self,
                                     action: #selector(refreshOptions(sender:)),
                                     for: .valueChanged)
            participantScrollView.refreshControl = refreshControl
        } */
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        votingResultArray.removeAll()
        singleParticipantsArray.removeAll()
        
        //LocationFinding method
        self.locationFind()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    //MARK: REFRESH 

    @objc private func refreshOptions(sender: UIRefreshControl) {
        // Perform actions to refresh the content
        // ...
        // and then dismiss the control
        
        votingResultArray.removeAll()
        singleParticipantsArray.removeAll()
        
        //LocationFinding method
        self.locationFind()
        
        sender.endRefreshing()
    }
    
    
    //MARK: LOCALIZATION
    
    func changeLanguage () {
        
        //Navigation Bar Title , color ,Font changes
        print("navigationTitle-----\(navigationTitle)")
        self.navigationItem.title = navigationTitle
        navigationController?.navigationBar.titleTextAttributes  =  [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        
        ////Heading
        
        staticvotingdetailLbl.font = UIFont(name:appFont, size: 17.0)
        staticvotingdetailLbl.text = appDelegate.getlocaLizationLanguage(key: "Vote Details")
        
        staticNotVotingYetLbl.font = UIFont(name:appFont, size: 17.0)
        staticNotVotingYetLbl.text = appDelegate.getlocaLizationLanguage(key: "No Votings Yet !")
        
        staticParticipantDetailsLbl.font = UIFont(name:appFont, size: 17.0)
        staticParticipantDetailsLbl.text = appDelegate.getlocaLizationLanguage(key: "Participant Details")
        
        staticDescriptionDetailsLbl.font = UIFont(name:appFont, size: 17.0)
        staticDescriptionDetailsLbl.text = appDelegate.getlocaLizationLanguage(key: "Description")
        
        staticGroupMemberLbl.font = UIFont(name:appFont, size: 17.0)
        staticGroupMemberLbl.text = appDelegate.getlocaLizationLanguage(key: "Group Members")
        
        
        //Single candidate
        staticNameLbl.font = UIFont(name:appFont, size: 17.0)
        staticNameLbl.text = appDelegate.getlocaLizationLanguage(key: "Name")
        
         staticParticipantIDLbl.font = UIFont(name:appFont, size: 17.0)
        staticParticipantIDLbl.text = appDelegate.getlocaLizationLanguage(key: "Participant ID")
        
        
        staticDateofBirthLbl.font = UIFont(name:appFont, size: 17.0)
        staticDateofBirthLbl.text = appDelegate.getlocaLizationLanguage(key: "Date of Birth")
        
        staticEducationLbl.font = UIFont(name:appFont, size: 17.0)
        staticEducationLbl.text = appDelegate.getlocaLizationLanguage(key: "Education")
        
        staticProfileLbl.font = UIFont(name:appFont, size: 17.0)
        staticProfileLbl.text = appDelegate.getlocaLizationLanguage(key: "Profile")
        
        staticOccupationLbl.font = UIFont(name:appFont, size: 17.0)
        staticOccupationLbl.text = appDelegate.getlocaLizationLanguage(key: "Occupation")
        
        //Group
        staticGroupNameLbl.font = UIFont(name:appFont, size: 17.0)
        staticGroupNameLbl.text = appDelegate.getlocaLizationLanguage(key: "Group Name")
        
        staticGroupParticipantIDLbl.font = UIFont(name:appFont, size: 17.0)
        staticGroupParticipantIDLbl.text = appDelegate.getlocaLizationLanguage(key: "Participant ID")
        
        staticGroupCountLbl.font = UIFont(name:appFont, size: 17.0)
        staticGroupCountLbl.text = appDelegate.getlocaLizationLanguage(key: "Total members")
        
        if VotingUrlManager.eventCheck == "Running" {
            payBtnOut.titleLabel?.font = UIFont.init(name: appFont, size: appButtonSize)
            payBtnOut.setTitle( appDel.getlocaLizationLanguage(key: "Vote"), for: .normal)
            
        }
        else if VotingUrlManager.eventCheck == "Old" {
            payBtnOut.titleLabel?.font = UIFont.init(name: appFont, size: appButtonSize)
            payBtnOut.setTitle("\(appDel.getlocaLizationLanguage(key: "Voting Ended on") ) \(self.dateConverstionTime(dateStr: (VotingUrlManager.voteEndDate!)))", for: .normal)
            
            
        }
        else if VotingUrlManager.eventCheck == "Future" {
            payBtnOut.titleLabel?.font = UIFont.init(name: appFont, size: appButtonSize)
            payBtnOut.setTitle("\(appDel.getlocaLizationLanguage(key: "Voting will start on") ) \(self.dateConverstionTime(dateStr: (VotingUrlManager.voteStartDate!)))", for: .normal)
            
        }
      
    }
    
    //MARK: CONSTRAIN LAYOUT
    
     private func setupVotingResultLayout() {
        
        if votingResultCount == 0 {
        
        tableviewheightoutlet.constant = 0
        notVotingYetLblHeight.constant = 30
        votingResultTableView.layoutIfNeeded()
            
        } else  {
            
            notVotingYetLblHeight.constant = 0
            tableviewheightoutlet.constant = CGFloat(80 * votingResultCount)
            votingResultTableView.layoutIfNeeded()
            
        }
        
    }
    
    
    private func setupGroupMemberLayout() {
        
        let itemSize = ((UIScreen.main.bounds.width/2 - 3) + 25)
        
        let heightFloat = Int(itemSize)
        
        if groupmemberCount == 0 {
            
            groupmemberLabelHeight.constant = 0
            groupmemberCollectionViewHeight.constant = 0
            groupmemberCollectionView.layoutIfNeeded()
            
        } else  {
            
            if groupmemberCount <= 4 {
            
            groupmemberLabelHeight.constant = 40
            groupmemberCollectionViewHeight.constant = CGFloat(heightFloat * ((groupmemberCount / 2) + (groupmemberCount % 2)))
            groupmemberCollectionView.layoutIfNeeded()
                
            } else {
                
                groupmemberLabelHeight.constant = 40
                groupmemberCollectionViewHeight.constant = CGFloat(heightFloat * 2+10)
                groupmemberCollectionView.layoutIfNeeded()
            }
            
        }
        
    }
    
    
    //MARK:VIEW SHADOW
    func setBorderShadow () {
        
        ////TopView
        profileTopview.backgroundColor = UIColor.white
        profileTopview.layer.cornerRadius = 3.0
        
        profileTopview.layer.borderWidth = 0.3
        profileTopview.layer.borderColor = UIColor.gray.cgColor
        
        profileTopview.layer.shadowColor = UIColor.gray.cgColor
        profileTopview.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        profileTopview.layer.shadowOpacity = 0.5
        profileTopview.layer.shadowRadius = 2
    }
    
    //MARK: LOCATION
    func locationFind () {
        
        DispatchQueue.main.async {
            PTLoader.shared.show()
        }
        
        //Location and statename find
        locManager.requestWhenInUseAuthorization()
        
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways) {
            
            currentLocation = locManager.location
          //  println_debug("latitude-------\(currentLocation.coordinate.latitude)")
          //  println_debug("longitude-------\(currentLocation.coordinate.longitude)")
            
            //self.currentLatitude = String(format: "%.10f", 16.7785435)
            //self.currentLongtitude = String(format: "%.10f",96.1697678)
            
              self.currentLatitude = String(format: "%.10f", currentLocation.coordinate.latitude)
             self.currentLongtitude = String(format: "%.10f", currentLocation.coordinate.longitude)
            
            // self.currentLatitude = currentLocation.coordinate.latitude
            // self.currentLongtitude = currentLocation.coordinate.longitude
            
        }
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { placemarks, error in
            guard let addressDict = placemarks?[0].addressDictionary else {
                return
            }
            
            // Print each key-value pair in a new row
            addressDict.forEach { println_debug($0) }
            
          //  println_debug("addressDict-------\(addressDict)")
            
            // Print fully formatted address
            if (addressDict["FormattedAddressLines"] as? [String]) != nil {
               // println_debug(formattedAddress.joined(separator: ", "))
            }
            
            // Access each element manually
            if let locationName = addressDict["State"] as? String {
               // println_debug(locationName)
                self.stateName = locationName
            }
            
            //API Call
            self.singleParticipantCall()
        })
        
    }
    
    //MARK: API CALL
    //Program AllParticipants by Event
    func singleParticipantCall() {
        
        if appDelegate.checkNetworkAvail() {
            
            DispatchQueue.main.async {
                PTLoader.shared.show()
            }
            
            //Orginal
               let str = String.init(format: "%@/%@/okAccountNumber/%@/%@/%@/%@/%@/latitude/%@/longitude/%@/stateName/%@", VotingConstants.urls.getAllprogrammes, (VotingUrlManager.selectedProgram.safeValueForKey("ProgrammeId") as! String),UserModel.shared.mobileNo,VotingConstants.urls.getAllEvent,(VotingUrlManager.selectedEvent.safeValueForKey("EventId") as! String),VotingConstants.urls.getAllparticipants,VotingUrlManager.selectedparticipantId!,geoLocManager.currentLatitude,geoLocManager.currentLongitude,self.stateName)
            
            let url = VotingUrlManager.getParticipantsUrl(str)
            
           // println_debug("All participant url--------\(url)")
            let programobject = VotingProgramidAPICall()
            programobject.programdelegate = self
            programobject.getAllProgramsMtdOne(urlStr: url, screen: "SingleParticipants")
            
        } else {
            self.showErrorAlert(errMessage: appDelegate.getlocaLizationLanguage(key: "No Internet Connection"))
            PTLoader.shared.hide()
        }
    }
    
    
    //MARK: API DELEGATE
    
    func VotingProgramidAPICallwebResponse(allData: Dictionary<String,Any>, screen: String) {
        
      //  println_debug("allevent delegate called")
        
        
            
            if allData .safeValueForKey("Message") as! String == "Success" {
            
            if allData .safeValueForKey("Status") as! Int == 1 {

            if let result = allData .safeValueForKey("Result") as?  [String : Any] {
                if let participants = result["Participants"] as? [Any] {
                    
                    for i in 0 ..< participants.count {
                        
                        if let candidate = participants[i] as? [String : Any] {
                        
                            singlecandidateProfilesDic["ParticipantId"] = candidate.safeValueForKey("ParticipantId")
                            groupcandidateProfilesDic["ParticipantId"] = candidate.safeValueForKey("ParticipantId")
                           

                            singlecandidateProfilesDic["VotingCode"] = candidate.safeValueForKey("VotingCode")
                            groupcandidateProfilesDic["VotingCode"] = candidate.safeValueForKey("VotingCode")
                            
                             singlecandidateProfilesDic["Description"] = candidate.safeValueForKey("Description")
                            groupcandidateProfilesDic["Description"] = candidate.safeValueForKey("Description")
                            
                            singlecandidateProfilesDic["IsLeave"] = candidate.safeValueForKey("IsLeave")
                            groupcandidateProfilesDic["IsLeave"] = candidate.safeValueForKey("IsLeave")

                            //Group Detail
                            let group : Int = candidate.safeValueForKey("IsGroup") as! Int
                            
                            if group == 0 {
                                
                                checkGroupMember = false
                                
                                let groupmember =  candidate.safeValueForKey("Candidate") as? [String : Any]
                                
                                singlecandidateProfilesDic["GroupName"] = groupmember?.safeValueForKey("Name")
                                
                                singlecandidateProfilesDic["ProfilePicture"] = groupmember?.safeValueForKey("ProfilePicture2")
                                
                                singlecandidateProfilesDic["DateOfBirth"] = groupmember?.safeValueForKey("DateOfBirth")
                                
                                singlecandidateProfilesDic["Education"] = groupmember?.safeValueForKey("Education")

                                singlecandidateProfilesDic["Profile"] = groupmember?.safeValueForKey("Profile")

                                singlecandidateProfilesDic["Occupation"] = groupmember?.safeValueForKey("Occupation")


                            }
                            else {
                                
                                let groupmember =  candidate.safeValueForKey("ParticipantGroup") as? [String : Any]
                                
                                groupcandidateProfilesDic["GroupName"] = groupmember?.safeValueForKey("GroupName")
                                
                                groupcandidateProfilesDic["ProfilePicture"] = groupmember?.safeValueForKey("GroupProfilePicture")
                                
                                let groupmemberresult =  groupmember?.safeValueForKey("ParticipantGroupMembers") as! [Any]
                                
                                if groupmemberresult.count > 0 {
                                    
                                    checkGroupMember = true

                                    for m in 0 ..< groupmemberresult.count {
                                        
                                        let individualDic : NSMutableDictionary  = [:]

                                        if let groupcandidatenew = groupmemberresult[m] as? [String : Any] {
                                            
                                            let candidateindividual = groupcandidatenew.safeValueForKey("Candidate") as? [String : Any]
                                            
                                            
                                             individualDic["Name"] = candidateindividual?.safeValueForKey("Name")
                                            
                                             individualDic["ProfilePicture2"] = candidateindividual?.safeValueForKey("ProfilePicture2")
                                            
                                            
                                            singleParticipantsArray.append(individualDic as! [String : AnyObject])

                                            
                                        }
                                        
                                    }
                                    
                                    groupcandidateProfilesDic["GroupCount"] = "\(singleParticipantsArray.count)"
                                    
                                }
                            }
                            
                            //Voting Result
                            let votingresult =  candidate.safeValueForKey("VotingResults") as! [Any]
                            
                           // var resultcount : Int = 0
                            
                            if votingresult.count > 0 {
                                
                                for j in 0 ..< votingresult.count {
                                    
                                    let votingDic : NSMutableDictionary  = [:]

                                    if let candidatenew = votingresult[j] as? [String : Any] {
                                        
                                        votingDic["MyVoteCount"] = "\(candidatenew.safeValueForKey("MyVoteCount") as! Int)"
                                        
                                        votingDic["OkVoteCount"] = "\( candidatenew.safeValueForKey("OkVoteCount")as! Int)"
                                        
                                        votingDic["LiveVoteCount"] = "\( candidatenew.safeValueForKey("LiveVoteCount")as! Int)"
                                        
                                        
                                        let ProgrammeVotingType = candidatenew.safeValueForKey("ProgrammeVotingType") as? [String : Any]
                                        
                                        let VotingType = ProgrammeVotingType?.safeValueForKey("VotingType") as? [String : Any]
                                        
                                        votingDic["VotingTypeName"] = "\(VotingType?.safeValueForKey("VotingTypeName") as! String)"
                                        
                                        
                                        votingResultArray.append(votingDic as! [String : AnyObject])

                                    }
                                    
                                }
                                
                            }
                            
                            let allVotingType =  candidate.safeValueForKey("VotingTypes") as! [Any]
                            
                            //var resultcount : Int = 0
                            
                            if allVotingType.count > 0 {
                                
                                for j in 0 ..< allVotingType.count {
                                    
                                    if let candidatetypenew = allVotingType[j] as? [String : Any] {
                                       
                                        let votesettings =  candidatetypenew.safeValueForKey("EventVotingTypeSetting") as? [String : Any]
                                        
                                        let checkValue = votesettings?.safeValueForKey("IsUserCanVote") as! Int
                                        
                                        if checkValue == 1{
                                            
                                            voteTypeCheck = true
                                            
                                            singlecandidateProfilesDic["CheckUserCanVote"] = checkValue
                                            groupcandidateProfilesDic["CheckUserCanVote"] = checkValue
                                        
                                        } else {
                                            
                                           singlecandidateProfilesDic["VoteReason"] = votesettings?.safeValueForKey("VoteReason")
                                            groupcandidateProfilesDic["VoteReason"] = votesettings?.safeValueForKey("VoteReason")
                                       
                                        }
                                    }
                                }
                                if voteTypeCheck == false {
                                    
                                    singlecandidateProfilesDic["CheckUserCanVote"] = 0
                                    groupcandidateProfilesDic["CheckUserCanVote"] = 0
                        
                                    singlecandidateProfilesDic["VoteReason"] = "Voting Time Is Over"
                                    
                                    groupcandidateProfilesDic["VoteReason"] = "Voting Time Is Over"
                                
                                }
                                
                                
                            } else {
                                
                                singlecandidateProfilesDic["CheckUserCanVote"] = 0
                                groupcandidateProfilesDic["CheckUserCanVote"] = 0
                         
                                singlecandidateProfilesDic["VoteReason"] = appDelegate.getlocaLizationLanguage(key: "Please Try After Some Time")
                                groupcandidateProfilesDic["VoteReason"] = appDelegate.getlocaLizationLanguage(key: "Please Try After Some Time")
                                
                                
                            }
                            
                        //    println_debug("singleParticipantsArray count -----\(singleParticipantsArray.count)")
                        //    println_debug("votingResultArray count -----\(votingResultArray.count)")
                            
                        }
                        
                    }
                    
                }
            }
            
            //RELOAD ALL CONTENT
            //voting Result
            votingResultCount = votingResultArray.count
            
            self.setupVotingResultLayout()
            
            if votingResultCount > 0 {
                
                self.votingResultTableView.dataSource = self
                self.votingResultTableView.delegate = self
                self.votingResultTableView.reloadData()
                
            }
            
            //Group Member
            groupmemberCount = singleParticipantsArray.count
            
            self.setupGroupMemberLayout()
            
            if groupmemberCount > 0 {
                
                self.groupmemberCollectionView.dataSource = self
                self.groupmemberCollectionView.delegate = self
                self.groupmemberCollectionView.reloadData()
                
            }
            
            //LoadAll Values
            self.loadContent()
            PTLoader.shared.hide()

        } else {
                
                PTLoader.shared.hide()
                self.showErrorAlert(errMessage:allData.safeValueForKey("Message") as! String)
                
        }
                
        } else {
                
                PTLoader.shared.hide()
                self.showErrorAlert(errMessage:allData.safeValueForKey("Message") as! String)
                
           }
            
        
    }
    
    
    //MARK: LOAD CONTENT
    func loadContent () {
        
        if  checkGroupMember == true {
            
            self.staticDescriptionDetailsLbl.translatesAutoresizingMaskIntoConstraints = false

            self.staticDescriptionDetailsLbl.topAnchor.constraint(equalTo: groupCandidateDetailView.bottomAnchor).isActive = true
            
            let imageUrl = groupcandidateProfilesDic.value(forKey: "ProfilePicture") as! String
            
            let finalimgUrl = imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
            VotingUrlManager.conformationtopImageUrl = finalimgUrl

            self.loadCell(imgUrl: finalimgUrl! , imageView: topImageView)
            
            self.groupnameLbl.text = groupcandidateProfilesDic.value(forKey: "GroupName") as? String
            self.votecodeLbl.text = groupcandidateProfilesDic.value(forKey: "VotingCode") as? String
            self.totalmembercountLbl.text = groupcandidateProfilesDic.value(forKey: "GroupCount") as? String
           // println_debug("groupcandidateProfilesDic descriptionLbl -----\(groupcandidateProfilesDic["Description"])")

             self.descriptionLbl.font = UIFont(name:appFont, size: 17.0)
            self.descriptionLbl.text = groupcandidateProfilesDic.value(forKey: "Description") as? String
            self.descriptionLbl.sizeToFit()
            
            let IsleaveCount = groupcandidateProfilesDic.value(forKey: "IsLeave") as! Int
          //  var usercanvoteCount = groupcandidateProfilesDic.value(forKey: "CheckUserCanVote") as! Int
            
            if IsleaveCount == 0 {
                
                payBtnOut.isHidden = false
                
            } else {
                
                payBtnOut.isHidden = true

            }

            //View hide
            singlecandidateDetailView.isHidden = true
            groupCandidateDetailView.isHidden = false

            singlecandidateDetailViewHeightOutlet.constant = 0

        } else {
            
            let imageUrl = singlecandidateProfilesDic.value(forKey: "ProfilePicture") as! String
            let finalimgUrl = imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
            VotingUrlManager.conformationtopImageUrl = finalimgUrl

            self.loadCell(imgUrl: finalimgUrl!, imageView: topImageView)
            
            self.staticDescriptionDetailsLbl.translatesAutoresizingMaskIntoConstraints = false
            
            self.staticDescriptionDetailsLbl.topAnchor.constraint(equalTo: singlecandidateDetailView.bottomAnchor).isActive = true
            
            self.singleCandidateNameLbl.font = UIFont(name:appFont, size: 17.0)
            self.singleCandidateNameLbl.text = singlecandidateProfilesDic.value(forKey: "GroupName") as? String
            self.singleCandidateNameLbl.sizeToFit()

            
            self.singleCandidateVoteCodeLbl.text = singlecandidateProfilesDic.value(forKey: "VotingCode") as? String
            self.singleCandidateVoteCodeLbl.sizeToFit()
            
            self.singleCandidatedateofbirthLbl.text = self.dateConverstion(dateStr: (singlecandidateProfilesDic.value(forKey: "DateOfBirth") as! String))
            self.singleCandidatedateofbirthLbl.sizeToFit()
            
            self.singleCandidateEducationLbl.font = UIFont(name:appFont, size: 17.0)
            self.singleCandidateEducationLbl.text = singlecandidateProfilesDic.value(forKey: "Education") as? String
            self.singleCandidateEducationLbl.sizeToFit()

            self.singleCandidateProfileLbl.font = UIFont(name:appFont, size: 17.0)
            self.singleCandidateProfileLbl.text = singlecandidateProfilesDic.value(forKey: "Profile") as? String
            self.singleCandidateProfileLbl.sizeToFit()

            self.singleCandidateOccupationLbl.font = UIFont(name:appFont, size: 17.0)
            self.singleCandidateOccupationLbl.text = singlecandidateProfilesDic.value(forKey: "Occupation") as? String
            self.singleCandidateOccupationLbl.sizeToFit()

          //  println_debug("singlecandidateProfilesDic descriptionLbl -----\(singlecandidateProfilesDic.value(forKey: "Description") as? String)")

            self.descriptionLbl.font = UIFont(name:appFont, size: 17.0)

            self.descriptionLbl.text = singlecandidateProfilesDic.value(forKey: "Description") as? String
            
            self.descriptionLbl.sizeToFit()
            
            let IsleaveCount = singlecandidateProfilesDic.value(forKey: "IsLeave") as! Int
           // var usercanvoteCount = singlecandidateProfilesDic.value(forKey: "CheckUserCanVote") as! Int
            
            
            if IsleaveCount == 0  {
                
                payBtnOut.isHidden = false
                
            } else {
                
                payBtnOut.isHidden = true
                
            }

            //View hide
            singlecandidateDetailView.isHidden = false
            groupCandidateDetailView.isHidden = true
            groupCandidateDetailViewHeightOutlet.constant = 0
            
        }
        
    }
    
    //MARK: DATE CONVERSION
    
    func dateConverstion (dateStr:String) -> String {
        
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.setLocale()
        let newString = dateStr.replacingOccurrences(of: "T", with: " ")
        let yourDate = formatter.date(from: newString)
        
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MM-yyyy"
        // again convert your date to string
        let finalStr = formatter.string(from: yourDate!)
        return finalStr
    }
    
    func dateConverstionTime (dateStr:String) -> String {
        
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.setLocale()
        let newString = dateStr.replacingOccurrences(of: "T", with: " ")
        let yourDate = formatter.date(from: newString)
        
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        // again convert your date to string
        let finalStr = formatter.string(from: yourDate!)
        return finalStr
    }
    
    
    //MARK: TABLEVIEW
    
    // TableView

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
   
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return self.votingResultArray.count
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        let header = view as! UITableViewHeaderFooterView
        
        if let textlabel = header.textLabel {
            textlabel.font = UIFont(name:appFont, size: 17.0)
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "\(votingResultArray[section].safeValueForKey("VotingTypeName") as! String) \(appDelegate.getlocaLizationLanguage(key: "Voting"))"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath as IndexPath) as! VotingResultCustomCell
        
        //Static
        cell.staticmyvoteLbl.font = UIFont(name:appFont, size: 15.0)
        cell.staticmyvoteLbl.text = appDelegate.getlocaLizationLanguage(key: "My Votes")
        
        cell.staticOkVoteLbl.font = UIFont(name:appFont, size: 15.0)
        cell.staticOkVoteLbl.text = appDelegate.getlocaLizationLanguage(key: "OK$ Votes")
        
        cell.staticlivevoteLbl.font = UIFont(name:appFont, size: 15.0)
        cell.staticlivevoteLbl.text = appDelegate.getlocaLizationLanguage(key: "Live Votes")

        //Dynamic
        cell.myvoteLbl.text = (votingResultArray[indexPath.section].safeValueForKey("MyVoteCount") as! String)
        cell.OkVoteLbl.text = (votingResultArray[indexPath.section].safeValueForKey("OkVoteCount") as! String)
        cell.livevoteLbl.text = (votingResultArray[indexPath.section].safeValueForKey("LiveVoteCount") as! String)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "VotingAllEventViewController") as! VotingAllEventViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
        
    }


    //MARK: COLLECTIONVIEW

/*func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
{
    
        var size = CGSize()
        
        size = CGSize(width: 180, height: 180)
        return size
        
    
    
} */

func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
    
}

func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return singleParticipantsArray.count

}

func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "memberCell", for: indexPath as IndexPath) as! groupmemberCollectionViewCustomCell
        
        
        ////TopView
        // corner radius
        cell.topView.backgroundColor = UIColor.white
        cell.topView.layer.cornerRadius = 3.0
        
        // border
        cell.topView.layer.borderWidth = 0.1
        cell.topView.layer.borderColor = UIColor.gray.cgColor
        
        //cell.topView.clipsToBounds = true
        
        cell.topView.layer.shadowColor = UIColor.gray.cgColor
        cell.topView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        cell.topView.layer.shadowOpacity = 0.7
        cell.topView.layer.shadowRadius = 2
    
    let imageUrl = singleParticipantsArray[indexPath.row].safeValueForKey("ProfilePicture2") as! String
    
    let finalimgUrl = imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    
    self.loadCell(imgUrl: finalimgUrl! , imageView: cell.imageView)
    
    cell.nameLbl.font = UIFont(name:appFont, size: 17.0)
    cell.nameLbl.text = singleParticipantsArray[indexPath.row].safeValueForKey("Name") as? String
        
        
        return cell
    
}

func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
}
    
    //MARK: PASSWORD VERIFY
    @IBAction func payBtn(_ sender: Any) {
        
        if checkGroupMember == true {
            
            let usercanvoteCount = groupcandidateProfilesDic.value(forKey: "CheckUserCanVote") as! Int
            
            if usercanvoteCount == 1 {
                
                if appDelegate.checkNetworkAvail() {
                    OKPayment.main.authenticate(screenName: "VotingType", delegate: self)
                }else {
                    self.showErrorAlert(errMessage: appDelegate.getlocaLizationLanguage(key: "No Internet Connection"))
                    PTLoader.shared.hide()
                }
                
            } else {
                
                self.showErrorAlert(errMessage: groupcandidateProfilesDic.value(forKey: "VoteReason") as! String)

            }
            
        } else {
            
            let usercanvoteCount = singlecandidateProfilesDic.value(forKey: "CheckUserCanVote") as! Int
            
            if usercanvoteCount == 1 {
                
                if appDelegate.checkNetworkAvail() {
                    OKPayment.main.authenticate(screenName: "VotingType", delegate: self)
                }else {
                    self.showErrorAlert(errMessage: appDelegate.getlocaLizationLanguage(key: "No Internet Connection"))
                    PTLoader.shared.hide()
                }
                
            } else {
                
                self.showErrorAlert(errMessage: singlecandidateProfilesDic.value(forKey: "VoteReason") as! String)
                
            }
            
        }
        
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if isSuccessful == true {
        
        if screen == "VotingType" {
            
            let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "VotingTypeViewController") as! VotingTypeViewController
            self.navigationController?.pushViewController(secondViewController, animated: true) 
        }
            
        }
        
    }
    
    //MARK: IMAGE CACHE
    let imageCache = NSCache<NSString, UIImage>()
    func loadCell(imgUrl : String , imageView:UIImageView)
    {
        if let cachedImage = imageCache.object(forKey: imgUrl as NSString) {
            DispatchQueue.main.async(execute: { () -> Void in
                imageView.image = cachedImage
            })
        } else {
            URLSession.shared.dataTask(with: NSURL(string: imgUrl)! as URL, completionHandler: { (data, response, error) -> Void in
                if error != nil {
                    //                    println_debug(error ?? "No Error")
                    return
                }
                else
                {
                    if let data = data, let image = UIImage(data: data) {
                        self.imageCache.setObject(image, forKey: imgUrl as NSString)
                        DispatchQueue.main.async(execute: { () -> Void in
                            let image = UIImage(data: data)
                            imageView.image = image
                        })
                    }
                }
            }).resume()
        }
    }
    
    //MARK: NAVIGATION
    
    @IBAction func backBtn(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
    
class VotingResultCustomCell: UITableViewCell {
    
    @IBOutlet var myvoteLbl: UILabel!
    @IBOutlet var OkVoteLbl: UILabel!
    @IBOutlet var livevoteLbl: UILabel!
    
    @IBOutlet var staticmyvoteLbl: UILabel!
    @IBOutlet var staticOkVoteLbl: UILabel!
    @IBOutlet var staticlivevoteLbl: UILabel!


}


class groupmemberCollectionViewCustomCell: UICollectionViewCell {
    
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet weak var topView: UIView!
    
}
