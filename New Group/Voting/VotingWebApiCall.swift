//
//  VotingWebApiCall.swift
//  OK
//
//  Created by prabu on 06/04/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

extension VotingAllProgramViewController  {
    
    func initialWebCall() {
        
        if appDelegate.checkNetworkAvail() {
          
            self.getAllProgramsMtd()

        } else {
            
            self.showErrorAlert(errMessage: appDelegate.getlocaLizationLanguage(key: "No Internet Connection"))
            PTLoader.shared.hide()
        }
        
    }
    
    func getAllProgramsMtd() {
        
        DispatchQueue.main.async {
            PTLoader.shared.show()
        }
        
        self.getAllDetailsForInternationTopup(handle: { [weak self](tokenManager) in
            DispatchQueue.main.async {
                let paramDict = Dictionary<String,String>()
                let headerString = String.init(format: "%@ %@", tokenManager.type, tokenManager.accessToken)
                  println_debug("Get All Program headerString------\(headerString)")
                
                let url = VotingUrlManager.getUrl(VotingConstants.urls.getAllprogrammes)
                  println_debug("Get All Program url------\(url)")
                
                TopupWeb.genericApiWithHeader(url: url, param: paramDict as AnyObject, httpMethod: "GET", header: headerString, handle: { (response, success) in
                    if success {
                        DispatchQueue.main.async {
                            if let dictionary = response as? Dictionary<String,Any> {
                                println_debug("Get All Program dictionary------\(dictionary)")
                                if dictionary .safeValueForKey("Message") as! String == "Success" {
                                    
                                    if dictionary .safeValueForKey("Status") as! Int == 1 {
                                        if let mySelf = self, let resultValue = dictionary.safeValueForKey("Results") as? [[String : AnyObject]] {
                                            mySelf.allsortprogramArray = resultValue
                                            mySelf.currentEvent()
                                        }
                                    } else {
                                        if let mySelf = self, let message = dictionary.safeValueForKey("Message") as? String {
                                            mySelf.showErrorAlert(errMessage: message)
                                        }
                                        PTLoader.shared.hide()
                                    }
                                } else {
                                    if let mySelf = self, let message = dictionary.safeValueForKey("Message") as? String {
                                        mySelf.showErrorAlert(errMessage: message)
                                    }
                                    PTLoader.shared.hide()
                                }
                            }
                        }
                    }
                })
            }
        })
    }
    
    private func getAllDetailsForInternationTopup(handle :@escaping (_ tokenManager: VotingUrlManager.accessManager) -> Void) {
        let url  = VotingUrlManager.getUrl(VotingConstants.urls.getTokens)
    println_debug("Token API url------\(url)")

        let hash = VotingConstants.ApiKeys.accessKey.hmac_SHA1(key: VotingConstants.ApiKeys.secretKey)
        println_debug("Token API hash------\(hash)")

        let parameters = String.init(format: "password=%@&grant_type=password", hash)
        println_debug("Token API Parameter------\(parameters)")
        TopupWeb.genericApiWithHeader(url: url, param: parameters as AnyObject, httpMethod: "POST", header: nil) { (result, success) in
            if success {
                if let dictionary = result as? Dictionary<String,Any> {
                   // println_debug("Token API dictionary------\(dictionary)")

                    let token = dictionary.safeValueForKey("access_token") as? String
                    //println_debug("Token API token------\(token)")

                    let type  = dictionary.safeValueForKey("token_type") as? String
                    //println_debug("Token API type------\(type)")

                    let tokenManager = VotingUrlManager.accessManager.init(token ?? "", type: type ?? "")
                    //println_debug("Token API tokenManager------\(tokenManager)")

                    DispatchQueue.main.async {
                        handle(tokenManager)
                    }
                }
            }
        }
    }
    
   
    func currentEvent () {
        
        for i in 0..<self.allsortprogramArray.count {
            
            let operatorDetail = allsortprogramArray[i]
            
            //Compare date
            let formatter = DateFormatter()
            formatter.calendar = Calendar(identifier: .gregorian)
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            formatter.setLocale()
            let startdate = operatorDetail.safeValueForKey("StartDate") as! String
            let startdateString = startdate.replacingOccurrences(of: "T", with: " ")
            let finalstartDate = formatter.date(from: startdateString)
            
            let formatternew = DateFormatter()
            formatternew.calendar = Calendar(identifier: .gregorian)
            formatternew.dateFormat = "yyyy-MM-dd HH:mm:ss"
            formatternew.setLocale()
            let enddate = operatorDetail.safeValueForKey("EndDate") as! String
            let enddateString = enddate.replacingOccurrences(of: "T", with: " ")
            guard let finalendDate = formatter.date(from: enddateString) else { return }
            
            //   PTLoader.shared.hide()
            
            let currentdate = Date()
            println_debug(currentdate)
            println_debug(finalstartDate)
            println_debug(finalendDate)
            println_debug(operatorDetail)

            if currentdate == finalendDate
            {
                
                self.allpresentprogramArray.append(operatorDetail)
                
            }
            else if currentdate > finalendDate
            {
                
                self.allpastprogramArray.append(operatorDetail)
                
            }
            else if currentdate < finalendDate
            {
                if currentdate < finalstartDate! {
                    
                    self.allfutureprogramArray.append(operatorDetail)
                    
                } else {
                    
                    self.allpresentprogramArray.append(operatorDetail)
                    
                }
            }
            
            
        }
        
        if self.allpresentprogramArray.count > 0 {
            
            for j in 0..<self.allpresentprogramArray.count {
                
                let operatorDetailNew = allpresentprogramArray[j]
                
                self.allprogramArray.append(operatorDetailNew)
                
            }
        }
        
        if self.allfutureprogramArray.count > 0 {
            
            for j in 0..<self.allfutureprogramArray.count {
                
                let operatorDetailNew = allfutureprogramArray[j]
                
                self.allprogramArray.append(operatorDetailNew)
                
            }
            
        }
        
        if self.allpastprogramArray.count > 0 {
            
            for j in 0..<self.allpastprogramArray.count {
                
                let operatorDetailNew = allpastprogramArray[j]
                
                self.allprogramArray.append(operatorDetailNew)
                
            }
        }
        
      //  println_debug("All program count----\(self.allprogramArray.count)")
      //  println_debug("All program----\(self.allprogramArray)")
        
        if self.allprogramArray.count > 0 {

        self.allprogramTableView.dataSource = self
        self.allprogramTableView.delegate = self
        self.allprogramTableView .reloadData()
            
        }
        
        PTLoader.shared.hide()
        
    }
    
}
