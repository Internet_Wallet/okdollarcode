//
//  VotingEventViewController.swift
//  VOTING
//
//  Created by prabu on 05/04/18.
//  Copyright © 2018 prabu. All rights reserved.
//

import Foundation
import UIKit
import MapKit


class VotingEventViewController : OKBaseController ,UICollectionViewDataSource,UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout, UIScrollViewDelegate , VotingProgramidAPICallDelegate , VotingEventDelegate {
    
    //TopMenu scrollview
    var navigation : UINavigationController?
    
    //Appdelegate Language Change
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var navigationTitle = String ()

    //Constrain Outlet
    @IBOutlet weak var eventCollectionViewheightoutlet: NSLayoutConstraint!
    @IBOutlet weak var noParticipantLblHeight: NSLayoutConstraint!
    var participantResultCount = 0
    
    //////Outlet
    
    //Common
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var topView: UIView!
     @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var numberofParticipantLbl: UILabel!
    @IBOutlet weak var noSponserFoundLbl: UILabel!

    //Organizer
    @IBOutlet weak var organizerView: UIView!
    @IBOutlet weak var organizerImageView: UIImageView!
    @IBOutlet weak var organizerLbl: UILabel!
    var organizerUrl = String()

    //CollectionView
    @IBOutlet weak var eventCollectionView: UICollectionView!
    @IBOutlet weak var eventScrollView: UIScrollView!
    
    //Contact Detail
    @IBOutlet weak var personNameLbl: UILabel!
    @IBOutlet weak var mobileNumLbl: FRHyperLabel!
    @IBOutlet weak var emailLbl: FRHyperLabel!
    @IBOutlet weak var websiteLbl: FRHyperLabel!
    @IBOutlet weak var facebookLbl: FRHyperLabel!
    


    //Pagnation
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var sponserCollectionView: UICollectionView!
    let collectionMargin = CGFloat(0)
    let itemSpacing = CGFloat(0)
    let itemHeight = CGFloat(210)
    var itemWidth = CGFloat(0)
    var currentItem = 0
    
    //Data
    var allOranizerArray = [String: AnyObject] ()
    var allSponsorsArray =  [[String: AnyObject]] ()
    var allParticipantsArray = [[String: AnyObject]]()
    var allValuesDic : [Dictionary<String, Any>]?
    
    //Latitude and Longtitude
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var stateName = String()
    var currentLatitude = String()
    var currentLongtitude = String()
    
    
    //Static Label Outlet
    @IBOutlet weak var staticParticipantDetailLbl: UILabel!
    @IBOutlet weak var staticProgramDetailLbl: UILabel!
    @IBOutlet weak var staticOrganizerDetailLbl: UILabel!
    @IBOutlet weak var staticSponserDetailLbl: UILabel!
    @IBOutlet weak var staticContactDetailLbl: UILabel!
    @IBOutlet weak var staticNoParticipantLbl: UILabel!

    @IBOutlet weak var staticContactPersonNameLbl: UILabel!
    @IBOutlet weak var staticMobileNumberLbl: UILabel!
    @IBOutlet weak var staticEmailIdLbl: UILabel!
    @IBOutlet weak var staticWebsiteLbl: UILabel!
    @IBOutlet weak var staticFacebookLbl: UILabel!

    
    //MARK: MAIN METHOD
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.setUpNavigation()
        self.changeLanguage()
        
        //Topview Border
        self.setBorderShadow()
        

        //CollectionView
        let itemSize = UIScreen.main.bounds.width/2 - 3
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize  = CGSize(width: itemSize , height: 280 )
        
        layout.minimumInteritemSpacing = 3
        layout.minimumLineSpacing = 3
        
        eventCollectionView.collectionViewLayout = layout
        
        self.programOrganizerCall()

        //Setuplayout
        self.setupLayout()
        

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        allParticipantsArray.removeAll()
       // allSponsorsArray.removeAll()
        
        //LocationFinding method
        self.locationFind()
        

    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: NAVIGATION MARQUEE

    /// It set up the navigation bar
    func setUpNavigation() {
        
        self.navigationController?.navigationBar.tintColor           = kYellowColor
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30 , height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        //label.text = "Voting System".localized + " "
        label.text = navigationTitle + "   "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 20) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
        
    }
    
    //MARK: LOCALIZATION
    
    func changeLanguage () {
        
        //Navigation Bar Title , color ,Font changes
        self.navigationItem.title = navigationTitle
        navigationController?.navigationBar.titleTextAttributes  =  [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        
        ////Dynamic
        //Decription and contact load and Topview
        descriptionLbl.font = UIFont(name:appFont, size: 17.0)
        descriptionLbl.text = VotingUrlManager.selectedProgram.safeValueForKey("Description") as? String
        descriptionLbl.sizeToFit()
        
        personNameLbl.font = UIFont(name:appFont, size: 15.0)
        personNameLbl.text = VotingUrlManager.selectedProgram.safeValueForKey("ContactPersonName") as? String
        personNameLbl.sizeToFit()

        //////
        mobileNumLbl.font = UIFont(name:appFont, size: 15.0)

       // mobileNumLbl.text = VotingUrlManager.selectedProgram.safeValueForKey("ContactNumber") as? String
        
        let mobilenumber = PTManagerClass.decodeMobileNumber(phoneNumber: VotingUrlManager.selectedProgram.safeValueForKey("ContactNumber") as! String)
        let mobile = mobilenumber.number.dropFirst(1)
        
        mobileNumLbl.text = mobilenumber.country.dialCode + mobile
       // println_debug("mobilenumber.country-------\(mobilenumber.country)")
       // println_debug("mobilenumber.countrydialCode-------\(mobilenumber.country.dialCode)")
       // println_debug("number-------\(mobilenumber.number)")
       // println_debug("number-------\(mobile)")

        mobileNumLbl.sizeToFit()

        
        let handlermobile = {
            (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            
            let callURL = URL(string: "telprompt://" + self.mobileNumLbl.text!)
            UIApplication.shared.open(callURL!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            
            
        }
        
        mobileNumLbl.setLinksForSubstrings(["\(mobileNumLbl.text!)"], withLinkHandler: handlermobile)

        //////
        emailLbl.font = UIFont(name:appFont, size: 15.0)
        emailLbl.text = VotingUrlManager.selectedProgram.safeValueForKey("EmailAddress") as? String
        emailLbl.sizeToFit()
        
        let handlerEmail = {
            (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            
            if let url = URL(string: "mailto:" + self.emailLbl.text!) {
                UIApplication.shared.open(url)
            }
            else{
                alertViewObj.wrapAlert(title: "", body: "No Mail Configured".localized, img: nil)
                alertViewObj.addAction(title: "OK", style: .target , action: {
                    
                })
                
                alertViewObj.showAlert(controller: self)
                //println_debug("No Mail Configured")
            }
            
        }
        
        emailLbl.setLinksForSubstrings(["\(emailLbl.text!)"], withLinkHandler: handlerEmail)

        ////////
        let mainProgram = VotingUrlManager.selectedProgram.safeValueForKey("MainProgramme") as? [String : Any]

        facebookLbl.font = UIFont(name:appFont, size: 15.0)

        facebookLbl.text = mainProgram?.safeValueForKey("Facebook") as? String
        facebookLbl.sizeToFit()

        let handlerFacebook = {
            (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string: self.facebookLbl.text!)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:])
                    , completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: self.facebookLbl.text!)! as URL)
            }
        }
        
        facebookLbl.setLinksForSubstrings(["\(facebookLbl.text!)"], withLinkHandler: handlerFacebook)

        
        ////////
        websiteLbl.font = UIFont(name:appFont, size: 15.0)

        websiteLbl.text = mainProgram?.safeValueForKey("WebsiteUrl") as? String
        websiteLbl.sizeToFit()
        
        
        let handlerwebsite = {
            (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string: self.websiteLbl.text!)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:])
                    , completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: self.websiteLbl.text!)! as URL)
            }
        }
        
        websiteLbl.setLinksForSubstrings(["\(websiteLbl.text!)"], withLinkHandler: handlerwebsite)
        
        /////////
        let imageUrl = VotingUrlManager.selectedProgram.safeValueForKey("Logo2") as! String
        let finalimgUrl = imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        //print("change languageimage url------\(imageUrl)")
       // print("change finalimgUrl url------\(finalimgUrl)")
        self.loadCell(imgUrl:finalimgUrl! , imageView:self.topImageView)
        
        
        ////Static
        
        //let mmkString = NSMutableAttributedString(string: " MMK", attributes: mmk)
        
        staticParticipantDetailLbl.font = UIFont(name:appFont, size: 16.0)
        staticParticipantDetailLbl.text = appDelegate.getlocaLizationLanguage(key: "Participant Details")

        staticProgramDetailLbl.font = UIFont(name:appFont, size: 17.0)
        staticProgramDetailLbl.text = appDelegate.getlocaLizationLanguage(key: "Programme Details")

        staticOrganizerDetailLbl.font = UIFont(name:appFont, size: 17.0)
        staticOrganizerDetailLbl.text = appDelegate.getlocaLizationLanguage(key: "Organizer Details")

        
        staticSponserDetailLbl.font = UIFont(name:appFont, size: 17.0)
        staticSponserDetailLbl.text = appDelegate.getlocaLizationLanguage(key: "Sponsor Details")

        
        staticContactDetailLbl.font = UIFont(name:appFont, size: 17.0)
        staticContactDetailLbl.text = appDelegate.getlocaLizationLanguage(key: "Contact Details")

        
        staticNoParticipantLbl.font = UIFont(name:appFont, size: 17.0)
        staticNoParticipantLbl.text = appDelegate.getlocaLizationLanguage(key: "No Participants")
        
        
        staticContactPersonNameLbl.font = UIFont(name:appFont, size: 16.0)
        staticContactPersonNameLbl.text = appDelegate.getlocaLizationLanguage(key: "Contact Person Name")
        staticContactPersonNameLbl.sizeToFit()

        staticMobileNumberLbl.font = UIFont(name:appFont, size: 16.0)
        staticMobileNumberLbl.text = appDelegate.getlocaLizationLanguage(key: "Mobile Number")
        staticMobileNumberLbl.sizeToFit()

        staticEmailIdLbl.font = UIFont(name:appFont, size: 16.0)
        staticEmailIdLbl.text = appDelegate.getlocaLizationLanguage(key: "Email")
        staticEmailIdLbl.sizeToFit()

        staticWebsiteLbl.font = UIFont(name:appFont, size: 16.0)
        staticWebsiteLbl.text = appDelegate.getlocaLizationLanguage(key: "Website")
        staticWebsiteLbl.sizeToFit()

        staticFacebookLbl.font = UIFont(name:appFont, size: 16.0)
        staticFacebookLbl.text = appDelegate.getlocaLizationLanguage(key: "Facebook")
        staticFacebookLbl.sizeToFit()

      
    }
    
    //MARK: SETUP CONSTRAIN LAYOUT
    
    private func setupLayout() {
        
      //  let itemSize = ((UIScreen.main.bounds.width/2 - 3) + 25)

        let heightFloat = 280

        if participantResultCount == 0 {
            
            noParticipantLblHeight.constant = 40
            eventCollectionViewheightoutlet.constant = 0
            eventCollectionView.layoutIfNeeded()
            
        } else  {
            
            if participantResultCount <= 4 {
                noParticipantLblHeight.constant = 0
            eventCollectionViewheightoutlet.constant = CGFloat(heightFloat * ((participantResultCount / 2) + (participantResultCount % 2))+10)
                eventCollectionView.layoutIfNeeded()
                
            } else {
                
                noParticipantLblHeight.constant = 0
                eventCollectionViewheightoutlet.constant = CGFloat(heightFloat * 2+10)
                eventCollectionView.layoutIfNeeded()
            }
                
    
        }
        
    }
    
    //MARK: PAGINATION SETUP
    func setupPagination() {
       
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        
        itemWidth =  UIScreen.main.bounds.width - collectionMargin * 2.0
        
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        layout.headerReferenceSize = CGSize(width: collectionMargin, height: 0)
        layout.footerReferenceSize = CGSize(width: collectionMargin, height: 0)
        
        layout.minimumLineSpacing = itemSpacing
        layout.scrollDirection = .horizontal
        sponserCollectionView!.collectionViewLayout = layout
        sponserCollectionView?.decelerationRate = UIScrollView.DecelerationRate.fast
        
        
    }
    
    @objc func scrollAutomatically(_ timer1: Timer) {
        
        if let coll  = sponserCollectionView {
            for cell in coll.visibleCells {
                let indexPath: IndexPath? = coll.indexPath(for: cell)
                if ((indexPath?.row)!  < (self.allSponsorsArray.count) - 1){
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                }
                else{
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
                }
                
            }
        }
        
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if scrollView == sponserCollectionView {
            
        let pageWidth = Float(itemWidth + itemSpacing)
        let targetXContentOffset = Float(targetContentOffset.pointee.x)
        let contentWidth = Float(sponserCollectionView!.contentSize.width  )
        var newPage = Float(self.pageControl.currentPage)
        
        if velocity.x == 0 {
            newPage = floor( (targetXContentOffset - Float(pageWidth) / 2) / Float(pageWidth)) + 1.0
        } else {
            newPage = Float(velocity.x > 0 ? self.pageControl.currentPage + 1 : self.pageControl.currentPage - 1)
            if newPage < 0 {
                newPage = 0
            }
            if (newPage > contentWidth / pageWidth) {
                newPage = ceil(contentWidth / pageWidth) - 1.0
            }
        }
        
        self.pageControl.currentPage = Int(newPage)
        let point = CGPoint (x: CGFloat(newPage * pageWidth), y: targetContentOffset.pointee.y)
        targetContentOffset.pointee = point
            
        }
    }
    
    //MARK:VIEW SHADOW
    func setBorderShadow () {
        
        ////TopView
        topView.backgroundColor = UIColor.white
        topView.layer.cornerRadius = 3.0
        
        topView.layer.borderWidth = 0.3
        topView.layer.borderColor = UIColor.gray.cgColor
        
        topView.layer.shadowColor = UIColor.gray.cgColor
        topView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        topView.layer.shadowOpacity = 0.5
        topView.layer.shadowRadius = 2
        
        ////Organizer View
        organizerView.backgroundColor = UIColor.white
        organizerView.layer.cornerRadius = 3.0
        
        organizerView.layer.borderWidth = 0.3
        organizerView.layer.borderColor = UIColor.gray.cgColor
        
        organizerView.layer.shadowColor = UIColor.gray.cgColor
        organizerView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        organizerView.layer.shadowOpacity = 0.5
        organizerView.layer.shadowRadius = 2
        
        
    }
    
    //MARK: LOCATION
    func locationFind () {
        
        DispatchQueue.main.async {
            PTLoader.shared.show()
        }
        
        //Location and statename find
        locManager.requestWhenInUseAuthorization()
        
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways) {
            
            currentLocation = locManager.location
          //  println_debug("latitude-------\(currentLocation.coordinate.latitude)")
          //  println_debug("longitude-------\(currentLocation.coordinate.longitude)")
            
            //self.currentLatitude = String(format: "%.10f", 16.7785435)
            //self.currentLongtitude = String(format: "%.10f",96.1697678)
            
              self.currentLatitude = String(format: "%.10f", currentLocation.coordinate.latitude)
             self.currentLongtitude = String(format: "%.10f", currentLocation.coordinate.longitude)
            
            // self.currentLatitude = currentLocation.coordinate.latitude
            // self.currentLongtitude = currentLocation.coordinate.longitude
            
        }
        
        
        let geoCoder = CLGeocoder()
      //let location = CLLocation(latitude: Double(geoLocManager.currentLatitude) ?? 0.0, longitude:  Double(geoLocManager.currentLongitude) ?? 0.0)
        let location = CLLocation(latitude: Double(self.currentLatitude) ?? 0.0, longitude:  Double(self.currentLongtitude) ?? 0.0)

        geoCoder.reverseGeocodeLocation(location, completionHandler: { placemarks, error in
            guard let addressDict = placemarks?[0].addressDictionary else {
                return
            }
            
            // Print each key-value pair in a new row
            addressDict.forEach { println_debug($0) }
            
          //  println_debug("addressDict-------\(addressDict)")
            
            // Print fully formatted address
            //if (addressDict["FormattedAddressLines"] as? [String]) != nil {
               // println_debug(formattedAddress.joined(separator: ", "))
            //}
            
            // Access each element manually
            if let locationName = addressDict["State"] as? String {
               // println_debug(locationName)
                self.stateName = locationName
            }
            
            //API Call
            self.eventAllParticipantCall(lat: self.currentLatitude, long: self.currentLongtitude)
        })
        
    }
    
    //MARK: API CALL
    
    //Program Organizer
    func programOrganizerCall() {
        
        DispatchQueue.main.async {
            PTLoader.shared.show()
        }
        
        if appDelegate.checkNetworkAvail() {
            
            let str = String.init(format: "%@/%@/%@", VotingConstants.urls.getAllprogrammes, (VotingUrlManager.selectedProgram.safeValueForKey("ProgrammeId") as! String),VotingConstants.urls.getAllorganizer)
            
            let url = VotingUrlManager.getUrl(str)
            let programobject = VotingProgramidAPICall()
            programobject.programdelegate = self
            programobject.getAllProgramsMtdOne(urlStr: url, screen: "AllOrganizer")
            
        } else {
            self.showErrorAlert(errMessage: appDelegate.getlocaLizationLanguage(key: "No Internet Connection"))
            PTLoader.shared.hide()
        }
        
    }
    
    //Program Organizer
    func programSponsorCall() {
        
        if appDelegate.checkNetworkAvail() {
            
            let str = String.init(format: "%@/%@/%@", VotingConstants.urls.getAllprogrammes, (VotingUrlManager.selectedProgram.safeValueForKey("ProgrammeId") as! String),VotingConstants.urls.getAllsponsorships)
            
            let url = VotingUrlManager.getUrl(str)
            let programobject = VotingProgramidAPICall()
            programobject.programdelegate = self
            programobject.getAllProgramsMtdOne(urlStr: url, screen: "AllSponsors")
            
        } else {
            self.showErrorAlert(errMessage: appDelegate.getlocaLizationLanguage(key: "No Internet Connection"))
            PTLoader.shared.hide()
        }
    }
    
    //Program AllParticipants by Event
    func eventAllParticipantCall(lat:String,long:String) {
        
        if appDelegate.checkNetworkAvail() {
            
            //Orginal
               let str = String.init(format: "%@/%@/okAccountNumber/%@/%@/%@/%@/latitude/%@/longitude/%@/stateName/%@", VotingConstants.urls.getAllprogrammes, (VotingUrlManager.selectedProgram.safeValueForKey("ProgrammeId") as! String),UserModel.shared.mobileNo,VotingConstants.urls.getAllEvent,(VotingUrlManager.selectedEvent.safeValueForKey("EventId") as! String),VotingConstants.urls.getAllparticipants,lat,long,self.stateName)
            
            let url = VotingUrlManager.getParticipantsUrl(str)
           // println_debug("All participant url--------\(url)")
            
            let programobject = VotingProgramidAPICall()
            programobject.programdelegate = self
            programobject.getAllProgramsMtdOne(urlStr: url, screen: "AllParticipants")
            
        } else {
            self.showErrorAlert(errMessage: appDelegate.getlocaLizationLanguage(key: "No Internet Connection"))
            PTLoader.shared.hide()
        }
    }
    
    
    func VotingProgramidAPICallwebResponse(allData: Dictionary<String,Any>, screen: String) {
        
        
        if screen == "AllOrganizer" {
          //  println_debug("VotingAllEvent Delegate Method Called -----\(allData)")
            
            if allData .safeValueForKey("Message") as! String == "Success" {

            if allData .safeValueForKey("Status") as! Int == 1 {

            self.allOranizerArray = allData .safeValueForKey("Result") as! [String : AnyObject]

           // println_debug("Event allOranizerArray called------\(allOranizerArray)")
            
            let imageUrl = self.allOranizerArray.safeValueForKey("Logo") as! String
                
            let finalimgUrl = imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
           // print("web response image url------\(imageUrl)")
           // print("web response finalimgUrl url------\(finalimgUrl)")
            self.loadCell(imgUrl:finalimgUrl! , imageView:organizerImageView)
                
            organizerLbl.font = UIFont(name:appFont, size: 17.0)
                organizerLbl.text = self.allOranizerArray.safeValueForKey("OrganizerName") as? String
                
            organizerUrl = self.allOranizerArray.safeValueForKey("Facebook") as! String
            
            self.programSponsorCall()
                
            } else {
                
               // self.showErrorAlert(errMessage:allData.safeValueForKey("Message") as! String)
                
            }
            } else {

                //self.showErrorAlert(errMessage:allData.safeValueForKey("Message") as! String)
                
            }
            
            
        }
        
        if screen == "AllSponsors" {
            
           // println_debug("SingleprogramAPI Delegate Method Called-----\(allData)")
            
            if allData .safeValueForKey("Message") as! String == "Success" {

           if allData .safeValueForKey("Status") as! Int == 1 {
            
            self.noSponserFoundLbl.isHidden = true

            self.allSponsorsArray = allData .safeValueForKey("Results") as! [[String : AnyObject]]
            
          //  println_debug("Event allSponsorsArray called------\(self.allSponsorsArray)")


            //Pagination
            sponserCollectionView.dataSource = self
            sponserCollectionView.delegate = self
            sponserCollectionView .reloadData()
            
            setupPagination()
            
            Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
                
            } else {
            
            self.noSponserFoundLbl.isHidden = false
            self.noSponserFoundLbl.font = UIFont(name:appFont, size: 17.0)
            self.noSponserFoundLbl.text = appDelegate.getlocaLizationLanguage(key: "No Sponsor Available")
            
           // self.showErrorAlert(errMessage:allData.safeValueForKey("Message") as! String)
                
            }
            
        } else {
                
            self.noSponserFoundLbl.isHidden = false
            self.noSponserFoundLbl.font = UIFont(name:appFont, size: 17.0)
            self.noSponserFoundLbl.text = appDelegate.getlocaLizationLanguage(key: "No Sponsor Available")

          //  self.showErrorAlert(errMessage:allData.safeValueForKey("Message") as! String)
            
        }
        
        }
        
        if screen == "AllParticipants" {
            
            
                if allData .safeValueForKey("Message") as! String == "Success" {

                if allData .safeValueForKey("Status") as! Int == 1 {

                if let result = allData .safeValueForKey("Result") as?  [String : Any] {
                    
                     VotingUrlManager.selectedDestinationNumber = result["OkAccountNumber"] as? String
                    
                    if let participants = result["Participants"] as? [Any] {
                        
                        for i in 0 ..< participants.count {
                            
                            if let candidate = participants[i] as? [String : Any] {
                                
                                // let name = candidate .safeValueForKey("IsGroup")
                                
                                let Dic : NSMutableDictionary  = [:]
                                
                               // Dic?.updateValue(candidate.safeValueForKey("ParticipantId") as! String , forKey: "ParticipantId")
                                
                                Dic["ParticipantId"] = candidate.safeValueForKey("ParticipantId")
                                Dic["VotingCode"] = candidate.safeValueForKey("VotingCode")
                                Dic["IsLeave"] = candidate.safeValueForKey("IsLeave")
                                Dic["LeaveDate"] = candidate.safeValueForKey("LeaveDate")
                                Dic["WinningOrder"] = candidate.safeValueForKey("WinningOrder")
                                
                                //Group Detail
                                let group : Int = candidate.safeValueForKey("IsGroup") as! Int
                                
                                if group == 0 {
                                    
                                    let groupmember =  candidate.safeValueForKey("Candidate") as? [String : Any]
                                    Dic["GroupName"] = groupmember?.safeValueForKey("Name")
                                    Dic["ProfilePicture"] = groupmember?.safeValueForKey("ProfilePicture")

                                }
                                else {
                                    
                                    let groupmember =  candidate.safeValueForKey("ParticipantGroup") as? [String : Any]
                                    Dic["GroupName"] = groupmember?.safeValueForKey("GroupName")
                                    Dic["ProfilePicture"] = groupmember?.safeValueForKey("GroupProfilePicture")

                                  
                                }
                                
                                //Voting Result
                                let votingresult =  candidate.safeValueForKey("VotingResults") as! [Any]
                                
                                var resultcount : Int = 0
                                
                                if votingresult.count > 0 {
                                    
                                    for j in 0 ..< votingresult.count {
                                        
                                        if let candidatenew = votingresult[j] as? [String : Any] {
                                            
                                            resultcount = resultcount + (candidatenew.safeValueForKey("OkVoteCount") as? Int)!
                                            
                                        }
                                        
                                    }
                                    
                                    Dic["OkVoteCount"] = "\(resultcount)"
                                    
                                } else {
                                
                                    Dic["OkVoteCount"] = "0"

                                }
                                allParticipantsArray.append(Dic as! [String : AnyObject])
                                
                                
                            }
                            
                        }
                        
                    }
                }
                
                //RELOAD ALL CONTENT
                participantResultCount = allParticipantsArray.count
                
                numberofParticipantLbl.font = UIFont(name:appFont, size: 16.0)
                numberofParticipantLbl.text = "\(allParticipantsArray.count) \(appDelegate.getlocaLizationLanguage(key: "Participants"))"
                
                self.setupLayout()
                    
                if participantResultCount > 0 {
                
                self.eventCollectionView.dataSource = self
                self.eventCollectionView.delegate = self
                self.eventCollectionView.reloadData()
                        
                }
                    
                    PTLoader.shared.hide()
              
            } else {
                    
                   // self.showErrorAlert(errMessage:allData.safeValueForKey("Message") as! String)
                    PTLoader.shared.hide()
                }
                    
            } else {
                    
                   // self.showErrorAlert(errMessage:allData.safeValueForKey("Message") as! String)
                    PTLoader.shared.hide()
                }
                
           
        }
        

    }
        
    
    
    //MARK: COLLECTIONVIEW
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == eventCollectionView {
            return self.allParticipantsArray.count
        } else {
            return self.allSponsorsArray.count
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == eventCollectionView {

        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! VotingEventCustomCell
        
       ////TopView
        // corner radius
        cell.topView.backgroundColor = UIColor.white
        cell.topView.layer.cornerRadius = 3.0
        
        // border
        cell.topView.layer.borderWidth = 0.1
        cell.topView.layer.borderColor = UIColor.gray.cgColor
        
        //cell.topView.clipsToBounds = true

        cell.topView.layer.shadowColor = UIColor.gray.cgColor
        cell.topView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        cell.topView.layer.shadowOpacity = 0.7
        cell.topView.layer.shadowRadius = 2
            
            let ParticipantDic = self.allParticipantsArray[indexPath.row]
            
         //   println_debug("participant ProfilePicture-----\(ParticipantDic["ProfilePicture"] as! String)")
            
            let imageUrl = ParticipantDic["ProfilePicture"] as! String
            
            let finalimgUrl = imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
            self.loadCell(imgUrl:finalimgUrl!, imageView: cell.imageView)

            //Status View
            cell.winningorderLbl.isHidden = true
            cell.statusView.isHidden = true
            
            cell.nameLbl.font = UIFont(name:appFont, size: 14.0)
            cell.nameLbl.text = ParticipantDic["GroupName"] as? String
            
            cell.votecountLbl.font = UIFont(name:appFont, size: 13.0)
            cell.votecountLbl.text = "\(ParticipantDic["OkVoteCount"] as! String) - \(appDelegate.getlocaLizationLanguage(key: "OK$ Votings"))"
            
            cell.votecodeLbl.font = UIFont(name:appFont, size: 13.0)
            cell.votecodeLbl.text = ParticipantDic["VotingCode"] as? String
            
            cell.statusImageView.image = nil
            
            let isleavecount = ParticipantDic["IsLeave"] as! Int
          //  println_debug("participant isleavecount-----\(isleavecount)")
            
            //Custom cell button
            cell.delegate = self as VotingEventDelegate
            cell.viewresultBtnOut.tag = indexPath.row
            
            cell.viewresultBtnOut.titleLabel?.font = UIFont.init(name: appFont, size: appButtonSize)
            
            if VotingUrlManager.eventCheck == "Running" {
                cell.viewresultBtnOut.setTitle(appDelegate.getlocaLizationLanguage(key: "Vote Now"),for: .normal)
                
            }
            else if VotingUrlManager.eventCheck == "Old" {
                cell.viewresultBtnOut.setTitle(appDelegate.getlocaLizationLanguage(key: "View Results"),for: .normal)
                
            }
            else if VotingUrlManager.eventCheck == "Future" {
                cell.viewresultBtnOut.setTitle(appDelegate.getlocaLizationLanguage(key: "Vote Later"),for: .normal)
                
            }

            if isleavecount == 1 {
                
                cell.statusView.isHidden = false
                cell.winningorderLbl.isHidden = true
                
                cell.votecountLbl.font = UIFont(name:appFont, size: 13.0)
                cell.votecountLbl.text = appDelegate.getlocaLizationLanguage(key: "Eliminated")
              //  println_debug("participant isleavecount-----\(ParticipantDic["LeaveDate"] as! String)")

                cell.votecodeLbl.text = self.dateConverstion(dateStr: ParticipantDic["LeaveDate"] as! String)
                
                let yourImage: UIImage = UIImage(named: "eliminated")!
                cell.statusImageView.image = yourImage
               // cell.statusImageView.image = UIImage(named: "succ")!
                
                cell.viewresultBtnOut.setTitle(appDelegate.getlocaLizationLanguage(key: "View Results"),for: .normal)
                
            }
            
            let winningcount = ParticipantDic["WinningOrder"] as! Int
          //  println_debug("participant WinningOrder-----\(winningcount)")

            if winningcount > 0 {
                
                cell.statusView.isHidden = false
                cell.winningorderLbl.isHidden = false
                
                cell.winningorderLbl.text = "\(winningcount)"
                let yourImage: UIImage = UIImage(named: "votiing_success")!
                cell.statusImageView.image = yourImage
            }

        
        return cell
            
        } else {
            
            // get a reference to our storyboard cell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SponserCell", for: indexPath as IndexPath) as! VotingEventSponserCustomCell
            
            ////Sponser View
            cell.sponserView.backgroundColor = UIColor.white
            cell.sponserView.layer.cornerRadius = 3.0
            
            cell.sponserView.layer.borderWidth = 0.3
            cell.sponserView.layer.borderColor = UIColor.gray.cgColor
            
            cell.sponserView.layer.shadowColor = UIColor.gray.cgColor
            cell.sponserView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
            cell.sponserView.layer.shadowOpacity = 0.5
            cell.sponserView.layer.shadowRadius = 2
            
            //Data
            let operatorDetail = self.allSponsorsArray [indexPath.row]
                // cell?.wrapCellData(operatorDetail.operatorName.safelyWrappingString())
            
            let sponser = operatorDetail.safeValueForKey("Sponsor") as? [String : Any]

            cell.sponserLbl?.font = UIFont(name:appFont, size: 17.0)
            cell.sponserLbl.text = sponser?.safeValueForKey("CompanyName") as? String

            let imageUrl = sponser?.safeValueForKey("Logo") as! String
            
            let finalimgUrl = imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
            self.loadCell(imgUrl:finalimgUrl!, imageView: cell.sponserimageView)
                
            return cell

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == eventCollectionView {

            let ParticipantDic = self.allParticipantsArray[indexPath.row]
            
            VotingUrlManager.selectedparticipantId = ParticipantDic["ParticipantId"] as? String
            
            //Conformation Page
                VotingUrlManager.selectedParticipantName = ParticipantDic["GroupName"] as? String

            VotingUrlManager.selectedParticipantID = ParticipantDic["VotingCode"] as? String

            let secondView = self.storyboard?.instantiateViewController(withIdentifier: "VotingParticiantsViewController") as! VotingParticiantsViewController
            secondView.navigationTitle = (ParticipantDic["GroupName"] as? String)!
            self.navigationController?.pushViewController(secondView, animated: true)
            
        } else {
            
            //Data
            let operatorDetail = self.allSponsorsArray [indexPath.row]
            
            let sponser = operatorDetail.safeValueForKey("Sponsor") as? [String : Any]
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string: sponser?.safeValueForKey("WebsiteUrl") as! String)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:])
                    , completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: sponser?.safeValueForKey("WebsiteUrl") as! String)! as URL)
            }
            
            
        }
        
    
    }
    
    
    //MARK: DATE CONVERSION
    
    func dateConverstion (dateStr:String) -> String {
        
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        formatter.setLocale()
        let newString = dateStr.replacingOccurrences(of: "T", with: " ")
        let yourDate = formatter.date(from: newString)
      //  println_debug("participant WinningOrder-----\(yourDate)")

        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MM-yyyy"
        // again convert your date to string
        let finalStr = formatter.string(from: yourDate!)
        return finalStr
    }
    
    
    //MARK: IMAGE CACHE
    let imageCache = NSCache<NSString, UIImage>()
    func loadCell(imgUrl : String , imageView:UIImageView)
    {
        if let cachedImage = imageCache.object(forKey: imgUrl as NSString) {
            DispatchQueue.main.async(execute: { () -> Void in
                imageView.image = cachedImage
            })
        } else {
            URLSession.shared.dataTask(with: NSURL(string: imgUrl)! as URL, completionHandler: { (data, response, error) -> Void in
                if error != nil {
                    //                    println_debug(error ?? "No Error")
                    return
                }
                else
                {
                    if let data = data, let image = UIImage(data: data) {
                        self.imageCache.setObject(image, forKey: imgUrl as NSString)
                        DispatchQueue.main.async(execute: { () -> Void in
                            let image = UIImage(data: data)
                            imageView.image = image
                        })
                    }
                }
            }).resume()
        }
    }
    
    
    //MARK: NAVIGATION
    @IBAction func navigationBtn(_ sender: Any) {
        
       self.navgation()
        
    }
    
    @IBAction func organizerBtn(_ sender: Any) {
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(NSURL(string: organizerUrl)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:])
                , completionHandler: nil)
        } else {
            UIApplication.shared.openURL(NSURL(string: organizerUrl)! as URL)
        }
        

    }
    
    func navgation() {
        
        let secondView = self.storyboard?.instantiateViewController(withIdentifier: "VotingParticiantsViewController") as! VotingParticiantsViewController
        //secondView.votingResultCount = 1
        //secondView.eachSection = ["SMS"]
        self.navigationController?.pushViewController(secondView, animated: true)
    }
    
   
    @IBAction func backBtn(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
        
    }
    
    //Custom cell delegate
    func cellButtonTapped(cell: VotingEventCustomCell) {
        
    
        let ParticipantDic = self.allParticipantsArray[cell.viewresultBtnOut.tag]
        
        VotingUrlManager.selectedparticipantId = ParticipantDic["ParticipantId"] as? String
        
        //print("participant name----\(ParticipantDic["GroupName"] as? String)")

        //Conformation Page
        VotingUrlManager.selectedParticipantName = ParticipantDic["GroupName"] as? String
        VotingUrlManager.selectedParticipantID = ParticipantDic["VotingCode"] as? String
        

        let secondView = self.storyboard?.instantiateViewController(withIdentifier: "VotingParticiantsViewController") as! VotingParticiantsViewController
        secondView.navigationTitle = (ParticipantDic["GroupName"] as? String)!
        self.navigationController?.pushViewController(secondView, animated: true)
        
    }

}

protocol VotingEventDelegate {
    func cellButtonTapped(cell: VotingEventCustomCell)
}

class VotingEventCustomCell: UICollectionViewCell {
    
    var delegate: VotingEventDelegate?

    @IBOutlet var imageView: UIImageView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var votecountLbl: UILabel!
    @IBOutlet weak var votecodeLbl: UILabel!
    @IBOutlet var statusImageView: UIImageView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var winningorderLbl: UILabel!
    @IBOutlet weak var viewresultBtnOut: UIButton!

    
    @IBAction func viewresultBtn(_ sender: Any) {
        delegate?.cellButtonTapped(cell:self)
    }

}


class VotingEventSponserCustomCell: UICollectionViewCell {
    
    @IBOutlet weak var sponserView: UIView!
    @IBOutlet var sponserimageView: UIImageView!
    @IBOutlet weak var sponserLbl: UILabel!
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
