//
//  VotingTypeViewController.swift
//  OK
//
//  Created by prabu on 11/04/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation
import UIKit
import MapKit


class VotingTypeViewController : OKBaseController , PaytoScrollerDelegate , VotingProgramidAPICallDelegate{
    
    //TopMenu scrollview
   // var navigation : UINavigationController?
    
    //Appdelegate Language Change
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    //Outlet
    @IBOutlet weak var descriptionLbl: UILabel!
    
    
    //TopMenu ScrollView
    fileprivate var myScanMenu : PaytoScroller?
    fileprivate var controllerArray : [UIViewController] = []
    fileprivate var controllertitileArray = [[String: AnyObject]]()
    fileprivate var titileArray = [String]()

    //DATA
    var smsVoteTypeArray = [[String: AnyObject]]()
    var missedVoteTypeArray = [[String: AnyObject]]()
    var onlineVoteTypeArray = [[String: AnyObject]]()
    var checkValue = String()

    //Latitude and Longtitude
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var stateName = String()
    var currentLatitude = String()
    var currentLongtitude = String()
    
    //MARK: MAIN METHOD

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.changeLanguage()
        
        //LocationFinding method
          self.locationFind()
        
    }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
      //  println_debug("viewDidAppear called-------")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
      //  println_debug("viewWillDisappear called-------")

    }
    
    override func viewDidDisappear(_ animated: Bool) {
      //  println_debug("viewDidDisappear called-------")

    }
    
    //MARK: LOCALIZATION

    func changeLanguage () {
        
        //Navigation Bar Title , color ,Font changes
        self.navigationItem.title = appDelegate.getlocaLizationLanguage(key: "Select Vote")
        navigationController?.navigationBar.titleTextAttributes  =  [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: appFont, size: 17) ?? UIFont.systemFont(ofSize: 17)]
        
    }
    
    //MARK: LOCATION
    func locationFind () {
        
        //Location and statename find
        locManager.requestWhenInUseAuthorization()
        
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways) {
            
            currentLocation = locManager.location
          //  println_debug("latitude-------\(currentLocation.coordinate.latitude)")
         //   println_debug("longitude-------\(currentLocation.coordinate.longitude)")
            
            //self.currentLatitude = String(format: "%.10f", 16.7785435)
            //self.currentLongtitude = String(format: "%.10f",96.1697678)
            
              self.currentLatitude = String(format: "%.10f", currentLocation.coordinate.latitude)
             self.currentLongtitude = String(format: "%.10f", currentLocation.coordinate.longitude)
            
            // self.currentLatitude = currentLocation.coordinate.latitude
            // self.currentLongtitude = currentLocation.coordinate.longitude
            
        }
        
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { placemarks, error in
            guard let addressDict = placemarks?[0].addressDictionary else {
                return
            }
            
            // Print each key-value pair in a new row
            addressDict.forEach { println_debug($0) }
            
          //  println_debug("addressDict-------\(addressDict)")
            
            // Print fully formatted address
            if (addressDict["FormattedAddressLines"] as? [String]) != nil {
                //println_debug(formattedAddress.joined(separator: ", "))
            }
            
            // Access each element manually
            if let locationName = addressDict["State"] as? String {
               // println_debug(locationName)
                self.stateName = locationName
            }
            
            //API Call
            self.singleVotingtypeCall()
        })
        
    }
    
    //MARK: API CALL
    //Program AllParticipants by Event
    func singleVotingtypeCall() {
        
        if appDelegate.checkNetworkAvail() {
            
            DispatchQueue.main.async {
                PTLoader.shared.show()
            }
            
            //Orginal
               let str = String.init(format: "%@/%@/okAccountNumber/%@/%@/%@/%@/%@/latitude/%@/longitude/%@/stateName/%@", VotingConstants.urls.getAllprogrammes, (VotingUrlManager.selectedProgram.safeValueForKey("ProgrammeId") as! String),UserModel.shared.mobileNo,VotingConstants.urls.getAllEvent,(VotingUrlManager.selectedEvent.safeValueForKey("EventId") as! String),VotingConstants.urls.getAllparticipants,VotingUrlManager.selectedparticipantId!,geoLocManager.currentLatitude,geoLocManager.currentLongitude,self.stateName)
            
            
            let url = VotingUrlManager.getParticipantsUrl(str)
            
          //  println_debug("All participant url--------\(url)")
            let programobject = VotingProgramidAPICall()
            programobject.programdelegate = self
            programobject.getAllProgramsMtdOne(urlStr: url, screen: "SingleParticipants")
            
        } else {
            self.showErrorAlert(errMessage: appDelegate.getlocaLizationLanguage(key: "No Internet Connection"))
            PTLoader.shared.hide()
        }
    }
    
    
    func VotingProgramidAPICallwebResponse(allData: Dictionary<String,Any>, screen: String) {
        
       // println_debug("allevent delegate called")
        
        
            
            if allData.safeValueForKey("Message") as! String == "Success" {

            if allData .safeValueForKey("Status") as! Int == 1 {

            if let result = allData .safeValueForKey("Result") as?  [String : Any] {
                if let participants = result["Participants"] as? [Any] {
                    
                    for i in 0 ..< participants.count {
                        
                        if let candidate = participants[i] as? [String : Any] {
                            
                            //////START
                            /////////////////Voting Type
                            let allVotingType =  candidate.safeValueForKey("VotingTypes") as! [Any]
                            
                            //var resultcount : Int = 0
                            
                            if allVotingType.count > 0 {
                                
                                for j in 0 ..< allVotingType.count {
                                    
                                    let votingTypeDic : NSMutableDictionary  = [:]
                                    
                                    if let candidatenew = allVotingType[j] as? [String : Any] {
                                        
                                        votingTypeDic["VotingTypeName"] = candidatenew.safeValueForKey("VotingTypeName")
                                        
                                        checkValue = candidatenew.safeValueForKey("VotingTypeName") as! String
                                        
                                        let votesettings =  candidatenew.safeValueForKey("EventVotingTypeSetting") as? [String : Any]
                                        
                                        votingTypeDic["ProgrammeVotingTypeId"] = votesettings?.safeValueForKey("ProgrammeVotingTypeId")
                                        
                                        votingTypeDic["VotingStartTime"] = votesettings?.safeValueForKey("VotingStartTime")
                                        
                                        votingTypeDic["VotingEndTime"] = votesettings?.safeValueForKey("VotingEndTime")
                                        
                                        votingTypeDic["IsUserCanVote"] = votesettings?.safeValueForKey("IsUserCanVote") as! Int
                                        
                                        controllertitileArray.append(votingTypeDic as! [String : AnyObject])
                                        
                                        
                                        ///////////vote Package
                                        let allVotePackages =  candidatenew.safeValueForKey("VotePackages") as! [Any]
                                        
                                        //var resultcount : Int = 0
                                        
                                        if allVotePackages.count > 0 {
                                            
                                            for m in 0 ..< allVotePackages.count {
                                                
                                                let votingPackDic : NSMutableDictionary  = [:]
                                                
                                                if let Packagesnew = allVotePackages[m] as? [String : Any] {
                                                    
                                                    votingPackDic["VotePackageId"] = Packagesnew.safeValueForKey("VotePackageId")
                                                    
                                                    votingPackDic["PackageName"] = Packagesnew.safeValueForKey("PackageName")
                                                    
                                                    votingPackDic["Vote"] = Packagesnew.safeValueForKey("Vote")
                                                    
                                                    votingPackDic["AmountPerVote"] = Packagesnew.safeValueForKey("AmountPerVote")
                                                    
                                                    votingPackDic["IsUserCanVote"] = Packagesnew.safeValueForKey("IsUserCanVote")
                                                    
                                                    votingPackDic["VoteReason"] = Packagesnew.safeValueForKey("VoteReason")
                                                    
                                                    
                                                    //////////////PriceTimeLine
                                                    let allPriceTimeLines =  Packagesnew.safeValueForKey("PriceTimeLines") as! [Any]
                                                    
                                                    //var resultcount : Int = 0
                                                    
                                                    if allPriceTimeLines.count > 0 {
                                                        
                                                        var PriceTimeLinesArray = [[String: AnyObject]]()
                                                        
                                                        for n in 0 ..< allPriceTimeLines.count {
                                                            
                                                            let PriceTimeLinesDic : NSMutableDictionary  = [:]
                                                            
                                                            if let pricetime = allPriceTimeLines[n] as? [String : Any] {
                                                                
                                                                PriceTimeLinesDic["PriceTimeLineId"] = pricetime.safeValueForKey("PriceTimeLineId")
                                                                
                                                                PriceTimeLinesDic["AdditionalAmount"] = pricetime.safeValueForKey("AdditionalAmount")
                                                                
                                                                PriceTimeLinesDic["FromTime"] = pricetime.safeValueForKey("FromTime")
                                                                
                                                                PriceTimeLinesDic["ToTime"] = pricetime.safeValueForKey("ToTime")
                                                                
                                                                PriceTimeLinesArray.append(PriceTimeLinesDic as! [String : AnyObject])
                                                                
                                                            }
                                                            
                                                        }
                                                        
                                                        votingPackDic["PriceTimeLineArray"] = PriceTimeLinesArray
                                                        
                                                    } else {
                                                        
                                                        votingPackDic["PriceTimeLineArray"] = nil

                                                    }
                                                    
                                                }
                                                
                                              //  println_debug("check value-------\(checkValue)")
                                                if checkValue == "Sms" {
                                                    
                                                    smsVoteTypeArray.append(votingPackDic as! [String : AnyObject])
                                                    
                                                } else if checkValue == "Miss Call" {
                                                    missedVoteTypeArray.append(votingPackDic as! [String : AnyObject])
                                                    
                                                    
                                                } else if checkValue == "Online" {
                                                    onlineVoteTypeArray.append(votingPackDic as! [String : AnyObject])
                                                    
                                                }
                                                
                                                
                                            }
                                            
                                            
                                        }
                                        
                                        
                                    }
                                    
                                    
                                    
                                }
                                
                                
                                
                            }
                            
                            
                            
                        }
                        
                    }
                    
                }
            }
            
  
                VotingUrlManager.smsAllContentArray = smsVoteTypeArray
                VotingUrlManager.callAllContentArray = missedVoteTypeArray
                VotingUrlManager.onlineAllContentArray = onlineVoteTypeArray
                
               // println_debug("VotingUrlManager smsVoteTypeArray title array count-----\(VotingUrlManager.smsAllContentArray.count)")
              //  println_debug("VotingUrlManager missedVoteTypeArray title array count-----\(VotingUrlManager.callAllContentArray.count)")
               // println_debug("VotingUrlManager onlineVoteTypeArray title array count-----\(VotingUrlManager.onlineAllContentArray.count)")

                if controllertitileArray.count > 0 {
                    self.seperatetitleArray()
                }
                PTLoader.shared.hide()

                
            } else {
                
                PTLoader.shared.hide()

                self.showErrorAlert(errMessage:allData.safeValueForKey("Message") as! String)
                
            }
                
            } else {
                
                PTLoader.shared.hide()

                self.showErrorAlert(errMessage:allData.safeValueForKey("Message") as! String)
                
            }


        
    }
    
    //MARK: CONTENT LOAD
    func seperatetitleArray () {
        
        for k in 0 ..< controllertitileArray.count {
            
            let DicOne = controllertitileArray[k] //as? [String : Any]
            
            // let DicOne = controllertitileArray[k]
            
            let count : Int = DicOne.safeValueForKey("IsUserCanVote") as! Int

           // println_debug("IsUserCanVote count-----\(count)")

            if  count  == 1 {
                
                let dateValide = self.dateValidation(startdatenew: DicOne.safeValueForKey("VotingStartTime") as! String, Enddatenew: DicOne.safeValueForKey("VotingEndTime") as! String)
                
                if dateValide {
                    
                    titileArray.append(DicOne.safeValueForKey("VotingTypeName") as! String)
                    
                }
                
            }

        }
        
   
        if titileArray.count > 0 {

            //Load Menu View
            self.allMenuViewLoad()

        } else {

            self.showErrorAlert(errMessage: "No Vote is Valid")
        }

    }
    
    
    //MARK: MENUBAR LOAD
    func allMenuViewLoad () {
        
         for i in 0 ..< titileArray.count {
         
         if titileArray[i] == "Sms" {
        
         let smsview = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: VotingTypeSmsViewController.self)) as? VotingTypeSmsViewController
            
         smsview?.title = "SMS"
            
         VotingUrlManager.smsAllContentArray = smsVoteTypeArray
         smsview?.navigation = self.navigationController
        // self.navigationController?.pushViewController(smsview!, animated: true)
         
         controllerArray.append(smsview!)
         
         }  else if titileArray[i] == "Miss Call" {
         
         let callview = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: VotingTypeCallViewController.self)) as? VotingTypeCallViewController
         
         callview?.view.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: 548.00)
         callview?.title = "MISS CALL"
         VotingUrlManager.callAllContentArray = missedVoteTypeArray
         callview?.navigation = self.navigationController
         
         controllerArray.append(callview!)
         
         
         } else if titileArray[i] == "Online" {
         
         let onlineview = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: VotingTypeOnlineViewController.self)) as? VotingTypeOnlineViewController
         
         onlineview?.view.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: 548.00)
         onlineview?.title = "ONLINE"
         VotingUrlManager.onlineAllContentArray = onlineVoteTypeArray
         onlineview?.navigation = self.navigationController
         
         controllerArray.append(onlineview!)
         
         }
         
         
         }
        
        let parameters = [PaytoScrollerOptionMenuItemSeparatorWidth: 0,
                          PaytoScrollerOptionMenuItemSeparatorPercentageHeight: 0.1,
                          PaytoScrollerOptionScrollMenuBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectedMenuItemLabelColor: MyNumberTopup.OperatorColorCode.okDefault,
                          PaytoScrollerOptionViewBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectionIndicatorColor: MyNumberTopup.OperatorColorCode.okDefault,
                          PaytoScrollerOptionUnselectedMenuItemLabelColor:UIColor.darkText,
                          PaytoScrollerOptionMenuHeight: 50,
                          PaytoScrollerOptionMenuItemFont: "Zawgyi-One",
                          PaytoScrollerOptionMenuItemWidth: (screenWidth / CGFloat(controllerArray.count)) - 10.0] as [String : Any]
        
        myScanMenu = PaytoScroller(viewControllers: controllerArray, frame: self.view.bounds, options: parameters)
        
        myScanMenu?.delegate = self
        
        if titileArray.count == 1
        {
            myScanMenu?.scrollingEnable(false)
        }
        else {
            myScanMenu?.scrollingEnable(true)
        }
        self.view.addSubview(myScanMenu!.view)
        
    }
    
    
    func dateValidation (startdatenew:String , Enddatenew:String) -> Bool {
        
        //Compare date
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.setLocale()
        let startdate = startdatenew
        let startdateString = startdate.replacingOccurrences(of: "T", with: " ")
        let finalstartDate = formatter.date(from: startdateString)
        
        let formatternew = DateFormatter()
        formatternew.calendar = Calendar(identifier: .gregorian)
        formatternew.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let enddate = Enddatenew
        let enddateString = enddate.replacingOccurrences(of: "T", with: " ")
        let finalendDate = formatter.date(from: enddateString)
        
        var checkVal = Bool()
        
        //   PTLoader.shared.hide()
        
        let currentdate = Date()
        
        if finalstartDate! <= currentdate && currentdate <= finalendDate!
        {
          //  println_debug("Equal Event----")
            
            checkVal = true
            
        } else {
            
            checkVal = false
            
        }
        
        return checkVal
        
    }
    
    
    @IBAction func backBtn(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
}



