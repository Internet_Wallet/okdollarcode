//
//  ViewController.swift
//  VOTING
//
//  Created by prabu on 05/04/18.
//  Copyright © 2018 prabu. All rights reserved.
//

import UIKit
import MapKit

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    
    func downloadedFrom(link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}

class VotingAllProgramViewController: OKBaseController, CLLocationManagerDelegate, PaytoScrollerDelegate {
    
    //MARK: - Outlet
    @IBOutlet weak var allprogramTableView: UITableView!
    
    //MARK: - Properties
    var navigation : UINavigationController?
    var nameCheck = "Voting"
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    //Data
    var allprogramArray = [[String: AnyObject]]()
    var allsortprogramArray = [[String: AnyObject]]()
    var allpresentprogramArray = [[String: AnyObject]]()
    var allfutureprogramArray = [[String: AnyObject]]()
    var allpastprogramArray = [[String: AnyObject]]()
    
    //MARK: - Views Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.changeLanguage()

        self.initialWebCall()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Button Action Methods
    @IBAction func backBtn(_ sender: Any) {
       navigationController?.popViewController(animated: true)
    }
    
    //MARK: LOCALIZATION
    func changeLanguage () {
        
        //Navigation Bar Title , color ,Font changes
        self.navigationItem.title = appDelegate.getlocaLizationLanguage(key: "Voting System")
        navigationController?.navigationBar.titleTextAttributes  =  [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: appFont, size: 17) ?? UIFont.systemFont(ofSize: 17)]
    
    }
    
    //MARK: -  Methods
    func dateConverstion (dateStr: String) -> String {
        
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.setLocale()
        let newString = dateStr.replacingOccurrences(of: "T", with: " ")
        let yourDate = formatter.date(from: newString)
        
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        // again convert your date to string
        let finalStr = formatter.string(from: yourDate!)
        return finalStr
        
//        guard let yourDate =  dateStr.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") else { return "" }
//        let finalStr = yourDate.stringValue(dateFormatIs: "dd-MM-yyyy hh:mm a")
//        return finalStr
        
    }
}

//MARK: - Tableview Delegate and Data Source
extension VotingAllProgramViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allprogramArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath as IndexPath) as! VotingAllProgramCustomCell
        let operatorDetail = allprogramArray[indexPath.row]
        let imageUrl = operatorDetail.safeValueForKey("Logo") as! String
        let finalimgUrl = imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        cell.loadCell(imgUrl: finalimgUrl!)
        let mainProgram = operatorDetail.safeValueForKey("MainProgramme") as? [String : Any]
        cell.MainProgramName?.font = UIFont(name:appFont, size: 17.0)
        cell.MainProgramName?.text = mainProgram?.safeValueForKey("MainProgrammeName") as? String
        cell.ProgramName?.font = UIFont(name:appFont, size: 15.0)
        cell.ProgramName?.text = operatorDetail.safeValueForKey("ProgrammeName") as? String
        
        let checknew = self.dateColor(startdatenew: operatorDetail.safeValueForKey("StartDate") as! String, Enddatenew: operatorDetail.safeValueForKey("EndDate") as! String)
        
        if checknew == "Running" {
            
            cell.StartDate?.text = "\(appDelegate.getlocaLizationLanguage(key: "Since")) \(self.dateConverstion(dateStr: operatorDetail.safeValueForKey("StartDate") as! String))"
            cell.StartDate?.font = UIFont(name:appFont, size: 13.0)
            
            cell.EndDate?.text = "\(appDelegate.getlocaLizationLanguage(key: "Voting Ends on")) \( self.dateConverstion(dateStr:operatorDetail.safeValueForKey("EndDate") as! String))"
            cell.EndDate?.font = UIFont(name:appFont, size: 13.0)
            cell.EndDate?.textColor = UIColor.red
            
        }
        else if checknew == "Old" {
            
            cell.StartDate?.text = "\(appDelegate.getlocaLizationLanguage(key: "Started on")) \(self.dateConverstion(dateStr: operatorDetail.safeValueForKey("StartDate") as! String))"
            cell.StartDate?.font = UIFont(name:appFont, size: 13.0)
            
            cell.EndDate?.text = "\(appDelegate.getlocaLizationLanguage(key: "Ended on")) \( self.dateConverstion(dateStr:operatorDetail.safeValueForKey("EndDate") as! String))"
            cell.EndDate?.font = UIFont(name:appFont, size: 13.0)
            cell.EndDate?.textColor = UIColor.red
        }
        else if checknew == "Future" {
            
            cell.StartDate?.text = "\(appDelegate.getlocaLizationLanguage(key: "Starts on")) \(self.dateConverstion(dateStr: operatorDetail.safeValueForKey("StartDate") as! String))"
            cell.StartDate?.font = UIFont(name:appFont, size: 13.0)
            
            cell.EndDate?.text = "\(appDelegate.getlocaLizationLanguage(key: "Ends on")) \( self.dateConverstion(dateStr:operatorDetail.safeValueForKey("EndDate") as! String))"
            cell.EndDate?.font = UIFont(name:appFont, size: 13.0)
            cell.EndDate?.textColor = UIColor.red
        }
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let operatorDetail = allprogramArray[indexPath.row]
        let mainProgram = operatorDetail.safeValueForKey("MainProgramme") as? [String : Any]
        VotingUrlManager.selectedProgramName = "\(mainProgram!.safeValueForKey("MainProgrammeName")!)-\(operatorDetail.safeValueForKey("ProgrammeName")!)"
        VotingUrlManager.selectedProgram = allprogramArray[indexPath.row]
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "VotingAllEventViewController") as! VotingAllEventViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    
    func dateColor (startdatenew:String , Enddatenew:String) -> String {
        
        //Compare date
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.setLocale()
        let startdate = startdatenew
        let startdateString = startdate.replacingOccurrences(of: "T", with: " ")
        let finalstartDate = formatter.date(from: startdateString)
        
        let formatternew = DateFormatter()
        formatternew.calendar = Calendar(identifier: .gregorian)

        formatternew.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let enddate = Enddatenew
        formatternew.setLocale()
        let enddateString = enddate.replacingOccurrences(of: "T", with: " ")
        let finalendDate = formatter.date(from: enddateString)
        
        var checkVal = String ()
        
        //   PTLoader.shared.hide()
        
        let currentdate = Date()
        
        if currentdate == finalendDate
        {
            checkVal = "Running"
        }
        else if currentdate > finalendDate!
        {
            checkVal = "Old"
        }
        else if currentdate < finalendDate!
        {
            if currentdate < finalstartDate! {
                checkVal = "Future"
            } else {
                checkVal = "Running"
            }
        }
        return checkVal
        
    }
    
}


//MARK: - VotingAllProgramCustomCell
 class VotingAllProgramCustomCell: UITableViewCell {
    
    //MARK: - Outlet
    @IBOutlet var MainProgramName: UILabel!
    @IBOutlet var urlImg: UIImageView!
    @IBOutlet var ProgramName: UILabel!
    @IBOutlet var StartDate: UILabel!
    @IBOutlet var EndDate: UILabel!

    //MARK: - Properties
    let imageCache = NSCache<NSString, UIImage>()
    
    //MARK: - Methods
    func loadCell(imgUrl : String)
    {
        if let cachedImage = imageCache.object(forKey: imgUrl as NSString) {
            DispatchQueue.main.async(execute: { () -> Void in
                self.urlImg.image = cachedImage
            })
        } else {
            URLSession.shared.dataTask(with: NSURL(string: imgUrl)! as URL, completionHandler: { (data, response, error) -> Void in
                if error != nil {
                    return
                } else {
                    if let data = data, let image = UIImage(data: data) {
                        self.imageCache.setObject(image, forKey: imgUrl as NSString)
                        DispatchQueue.main.async(execute: { () -> Void in
                            let image = UIImage(data: data)
                            self.urlImg.image = image
                        })
                    }
                }
            }).resume()
        }
    }
}
