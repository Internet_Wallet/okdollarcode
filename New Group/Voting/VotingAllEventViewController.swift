//
//  VotingAllProgramViewController.swift
//  VOTING
//
//  Created by prabu on 05/04/18.
//  Copyright © 2018 prabu. All rights reserved.
//

import Foundation
import UIKit


class VotingAllEventViewController : OKBaseController ,UITableViewDataSource,UITableViewDelegate , VotingProgramidAPICallDelegate {
   
    //TopMenu scrollview
    var navigation : UINavigationController?
    
    //Appdelegate Language Change
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    //Constrain Outlet
    @IBOutlet weak var eventtableviewheightoutlet: NSLayoutConstraint!
    @IBOutlet weak var noSessionDetailLblHeight: NSLayoutConstraint!

    //Outlet
    @IBOutlet weak var allEventTableView: UITableView!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var topImageView: UIImageView!
    
    //Static Label Outlet
    @IBOutlet weak var staticSensionDetailLbl: UILabel!
    @IBOutlet weak var staticProgramDetailLbl: UILabel!
    @IBOutlet weak var staticeventNotFoundLbl: UILabel!

    var AllResultCount: Int = 0
    
    //Data
    var AllEventArray = [[String: AnyObject]]()
    var AllsortEventArray = [[String: AnyObject]]()
    var AllpresentEventArray = [[String: AnyObject]]()
    var AllfutureEventArray = [[String: AnyObject]]()
    var AllpastEventArray = [[String: AnyObject]]()
    
    
    //MARK: MAIN METHOD

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
         self.changeLanguage()
        
        //Constrain change
          self.setupLayout()
        
        topView.backgroundColor = UIColor.white
        topView.layer.cornerRadius = 3.0
        
        // border
        topView.layer.borderWidth = 0.3
        topView.layer.borderColor = UIColor.gray.cgColor
        
        topView.layer.shadowColor = UIColor.gray.cgColor
        topView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        topView.layer.shadowOpacity = 0.5
        topView.layer.shadowRadius = 2
        
        
        let imageUrl = VotingUrlManager.selectedProgram.safeValueForKey("Logo2") as! String
        
        let finalimgUrl = imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        self.loadCell(imgUrl:finalimgUrl!)
        
        self.allEventCall()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtn(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
        
    }
    
    //MARK: LOCALIZATION
    
    func changeLanguage () {
        
        //Navigation Bar Title , color ,Font changes
        self.navigationItem.title = VotingUrlManager.selectedProgram.safeValueForKey("ProgrammeName") as? String
        navigationController?.navigationBar.titleTextAttributes  =  [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        
        staticSensionDetailLbl.font = UIFont(name:appFont, size: 18.0)
        staticSensionDetailLbl.text = appDelegate.getlocaLizationLanguage(key: "Season Details")
        
        staticProgramDetailLbl.font = UIFont(name:appFont, size: 18.0)
        staticProgramDetailLbl.text = appDelegate.getlocaLizationLanguage(key: "Programme Details")
        
        staticeventNotFoundLbl.font = UIFont(name:appFont, size: 20.0)
        staticeventNotFoundLbl.text = appDelegate.getlocaLizationLanguage(key: "Event Not Found")
        
       
        descriptionLbl.font = UIFont(name:appFont, size: 17.0)
        descriptionLbl.text =  VotingUrlManager.selectedProgram.safeValueForKey("Description") as? String
        descriptionLbl.sizeToFit()
    }
    
    //MARK: IMAGE CACHE
    let imageCache = NSCache<NSString, UIImage>()
    func loadCell(imgUrl : String)
    {
        if let cachedImage = imageCache.object(forKey: imgUrl as NSString) {
            DispatchQueue.main.async(execute: { () -> Void in
                self.topImageView.image = cachedImage
            })
        } else {
            URLSession.shared.dataTask(with: NSURL(string: imgUrl)! as URL, completionHandler: { (data, response, error) -> Void in
                if error != nil {
                    //                    println_debug(error ?? "No Error")
                    return
                }
                else
                {
                    if let data = data, let image = UIImage(data: data) {
                        self.imageCache.setObject(image, forKey: imgUrl as NSString)
                        DispatchQueue.main.async(execute: { () -> Void in
                            let image = UIImage(data: data)
                            self.topImageView.image = image
                        })
                    }
                }
            }).resume()
        }
    }
    
    //MARK: SETUP CONSTRAIN
    private func setupLayout() {
        
        if AllResultCount == 0 {
            
            noSessionDetailLblHeight.constant = 40
            eventtableviewheightoutlet.constant = 0
            allEventTableView.layoutIfNeeded()
            
        } else  {
            
            if AllResultCount > 3 {
                
                noSessionDetailLblHeight.constant = 0
                eventtableviewheightoutlet.constant = CGFloat(85 * 3)
                allEventTableView.layoutIfNeeded()
                
            } else {
                
            noSessionDetailLblHeight.constant = 0
            eventtableviewheightoutlet.constant = CGFloat(85 * self.AllEventArray.count)
            allEventTableView.layoutIfNeeded()
                
            }
            
        }
        
    }
    
    //MARK: API CALL
    func allEventCall() {
        
        if appDelegate.checkNetworkAvail() {
            
            DispatchQueue.main.async {
                PTLoader.shared.show()
            }
            
            let str = String.init(format: "%@/%@/%@", VotingConstants.urls.getAllprogrammes, (VotingUrlManager.selectedProgram.safeValueForKey("ProgrammeId") as! String),VotingConstants.urls.getAllEvent)
            
            let url = VotingUrlManager.getUrl(str)
            
            let programobjectnew = VotingProgramidAPICall()
            programobjectnew.programdelegate = self
            programobjectnew.getAllProgramsMtdOne(urlStr: url, screen: "AllEventAPI")
            
        } else {
            self.showErrorAlert(errMessage: appDelegate.getlocaLizationLanguage(key: "No Internet Connection"))
            PTLoader.shared.hide()
        }
        
    }
    
    
    func VotingProgramidAPICallwebResponse(allData: Dictionary<String,Any>, screen: String) {
        
        if screen == "AllEventAPI" {
            
            if allData .safeValueForKey("Message") as! String == "Success" {

            if allData .safeValueForKey("Status") as! Int == 1 {

                let allContentArray = allData .safeValueForKey("Results") as! [[String : AnyObject]]
                
            self.AllsortEventArray = allContentArray.reversed()
                
            self.currentEvent()
                
         //   println_debug("VotingAllEvent Delegate Method Called -----\(AllEventArray)")
         //   println_debug("AllEventArray count Delegate Method Called -----\(AllEventArray.count)")

            } else {
                
                PTLoader.shared.hide()
                self.showErrorAlert(errMessage:allData.safeValueForKey("Message") as! String)
                
            }
                
            } else {
                
                PTLoader.shared.hide()
                self.showErrorAlert(errMessage:allData.safeValueForKey("Message") as! String)
                
            }
            
        }
        
    }
    
    //MARK: TableView
    
    //No need height previously assisged
  /*  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85.0
    } */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.AllEventArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath as IndexPath) as! AllEventTableViewCell
        
         let operatorDetail = self.AllEventArray[indexPath.row]
        
        let checknew = self.dateColor(startdatenew: operatorDetail.safeValueForKey("StartDate") as! String, Enddatenew: operatorDetail.safeValueForKey("EndDate") as! String)
                
        if checknew == "Running" {
            
            cell.startDate?.text = ""
            cell.endDate?.text = ""
            
        cell.programName?.font = UIFont(name:appFont, size: 20.0)
        cell.programName?.text = operatorDetail.safeValueForKey("EventName") as? String
            
        cell.startDate?.text = "\(appDelegate.getlocaLizationLanguage(key: "Since")) \(self.dateConverstion(dateStr: operatorDetail.safeValueForKey("StartDate") as! String))"
        cell.endDate?.text = "\(appDelegate.getlocaLizationLanguage(key: "Voting Ends on")) \( self.dateConverstion(dateStr:operatorDetail.safeValueForKey("EndDate") as! String))"
            
        cell.startDate?.textColor = UIColor.blue
        cell.endDate?.textColor = UIColor.blue
        cell.programName?.textColor = UIColor.black
            
         //Animation
            let animation = CABasicAnimation(keyPath: "transform.scale")
            animation.duration = 1
            animation.fromValue = 1
            animation.toValue = 0.9
            animation.autoreverses = true
            animation.repeatCount = .infinity
            cell.programName?.layer.add(animation, forKey: "hypnoscale")
            
        } else if checknew == "Old" {
            
            cell.startDate?.text = ""
            cell.endDate?.text = ""
            
            cell.programName?.font = UIFont(name:appFont, size: 20.0)
            cell.programName?.text = operatorDetail.safeValueForKey("EventName") as? String
            
           // cell.startDate?.text = "\(appDelegate.getlocaLizationLanguage(key: "Started on")) \(self.dateConverstion(dateStr: operatorDetail.safeValueForKey("StartDate") as! String))"
            
            cell.startDate?.text = "\(appDelegate.getlocaLizationLanguage(key: "Ended on")) \( self.dateConverstion(dateStr:operatorDetail.safeValueForKey("EndDate") as! String))"
            
            cell.startDate?.textColor = UIColor.red
            cell.endDate?.textColor = UIColor.red
            cell.programName?.textColor = UIColor.black
        
            
        } else if checknew == "Future" {
            
            cell.startDate?.text = ""
            cell.endDate?.text = ""
            
            cell.programName?.font = UIFont(name:appFont, size: 20.0)
            cell.programName?.text = operatorDetail.safeValueForKey("EventName") as? String
            
            cell.startDate?.text = "\(appDelegate.getlocaLizationLanguage(key: "Starts on")) \(self.dateConverstion(dateStr: operatorDetail.safeValueForKey("StartDate") as! String))"
            cell.endDate?.text = "\(appDelegate.getlocaLizationLanguage(key: "Ends on")) \( self.dateConverstion(dateStr:operatorDetail.safeValueForKey("EndDate") as! String))"
            
            cell.startDate?.textColor = UIColor.orange
            cell.endDate?.textColor = UIColor.orange
            cell.programName?.textColor = UIColor.black
            
        }
        cell.startDate?.font = UIFont(name:appFont, size: 15.0)
        cell.endDate?.font = UIFont(name:appFont, size: 15.0)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            let operatorDetail = self.AllEventArray[indexPath.row]
            VotingUrlManager.selectedEvent = self.AllEventArray[indexPath.row]
        VotingUrlManager.selectedEventName = operatorDetail.safeValueForKey("EventName") as? String

        let checknew = self.dateColor(startdatenew: operatorDetail.safeValueForKey("StartDate") as! String, Enddatenew: operatorDetail.safeValueForKey("EndDate") as! String)
        
        VotingUrlManager.voteEndDate = operatorDetail.safeValueForKey("EndDate") as? String
        VotingUrlManager.voteStartDate = operatorDetail.safeValueForKey("StartDate") as? String

        if checknew == "Running" {
            VotingUrlManager.eventCheck = "Running"
        }
        else if checknew == "Old" {
            VotingUrlManager.eventCheck = "Old"
            
        }
        else if checknew == "Future" {
            VotingUrlManager.eventCheck = "Future"
            
        }
        
        
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "VotingEventViewController") as! VotingEventViewController
         secondViewController.navigationTitle = "\(VotingUrlManager.selectedProgram.safeValueForKey("ProgrammeName") as! String)-\((VotingUrlManager.selectedEvent.safeValueForKey("EventName"))as! String)"
        self.navigationController?.pushViewController(secondViewController, animated: true) 

        
    }
    
    //MARK: ANIMATION LAYER
    func setTextLayer (str:String) -> CATextLayer {
        
        // Create the CATextLayer
        //Text Layer
        let textLayer = CATextLayer()
        textLayer.string = str
        textLayer.font = UIFont.systemFont(ofSize: 20.0)
        textLayer.fontSize = 20.0
        textLayer.contentsScale = UIScreen.main.scale
        textLayer.frame = view.bounds
        
        textLayer.foregroundColor = UIColor.blue.cgColor
        
        // Animation
        let duration: TimeInterval = 3
        textLayer.fontSize = 20.0
        let fontSizeAnimation = CABasicAnimation(keyPath: "fontSize")
        fontSizeAnimation.fromValue = 20.0
        fontSizeAnimation.toValue = 25.0
        fontSizeAnimation.duration = duration
        fontSizeAnimation.repeatCount = Float.infinity
        textLayer.add(fontSizeAnimation, forKey: nil)
        
        return textLayer
        
    }

    //MARK: DATE CONVERSION
    func dateConverstion (dateStr:String) -> String {
        
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.setLocale()
        let newString = dateStr.replacingOccurrences(of: "T", with: " ")
        let yourDate = formatter.date(from: newString)
        
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        // again convert your date to string
        let finalStr = formatter.string(from: yourDate!)
        return finalStr
    }
    
    func dateColor (startdatenew:String , Enddatenew:String) -> String {
        
        
        //Compare date
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.setLocale()
        let startdate = startdatenew
        let startdateString = startdate.replacingOccurrences(of: "T", with: " ")
        let finalstartDate = formatter.date(from: startdateString)
        
        let formatternew = DateFormatter()
        formatternew.calendar = Calendar(identifier: .gregorian)
        formatternew.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let enddate = Enddatenew
        let enddateString = enddate.replacingOccurrences(of: "T", with: " ")
        let finalendDate = formatter.date(from: enddateString)
        
        
        var checkVal = String ()
        
        //   PTLoader.shared.hide()
        
        let currentdate = Date()
        
        if currentdate == finalendDate
        {
            checkVal = "Running"
        }
        else if currentdate > finalendDate!
        {
            checkVal = "Old"
        }
        else if currentdate < finalendDate!
        {
            if currentdate < finalstartDate! {
              
                checkVal = "Future"

            } else {
                
                checkVal = "Running"

            }
        }
        
        return checkVal
    }
    
    func currentEvent () {
        
        for i in 0..<self.AllsortEventArray.count {
            
            let operatorDetail = AllsortEventArray[i]
            
            //Compare date
            let formatter = DateFormatter()
            formatter.calendar = Calendar(identifier: .gregorian)
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            formatter.setLocale()
            let startdate = operatorDetail.safeValueForKey("StartDate") as! String
            let startdateString = startdate.replacingOccurrences(of: "T", with: " ")
            let finalstartDate = formatter.date(from: startdateString)
            
            let formatternew = DateFormatter()
            formatternew.calendar = Calendar(identifier: .gregorian)
            formatternew.dateFormat = "yyyy-MM-dd HH:mm:ss"
            formatternew.setLocale()
            let enddate = operatorDetail.safeValueForKey("EndDate") as! String
            let enddateString = enddate.replacingOccurrences(of: "T", with: " ")
            let finalendDate = formatter.date(from: enddateString)
            
            //   PTLoader.shared.hide()
            
            let currentdate = Date()
            
            if currentdate == finalendDate
            {
                self.AllpresentEventArray.append(operatorDetail)
            }
            else if currentdate > finalendDate!
            {
                self.AllpastEventArray.append(operatorDetail)
            
            }
            else if currentdate < finalendDate!
            {
                if currentdate < finalstartDate! {
                 
                    self.AllfutureEventArray.append(operatorDetail)
                    
                } else {
                 
                    self.AllpresentEventArray.append(operatorDetail)
                    
                }
            }
            
        }
        
        if self.AllpresentEventArray.count > 0 {
            
            for j in 0..<self.AllpresentEventArray.count {
                
                let operatorDetailNew = AllpresentEventArray[j]
                
                self.AllEventArray.append(operatorDetailNew)
                
            }
        }
        
        if self.AllfutureEventArray.count > 0 {
            
            for j in 0..<self.AllfutureEventArray.count {
                
                let operatorDetailNew = AllfutureEventArray[j]
                
                self.AllEventArray.append(operatorDetailNew)
                
            }
            
        }
        
        if self.AllpastEventArray.count > 0 {
            
            self.AllpastEventArray = self.AllpastEventArray.reversed()
            
            for j in 0..<self.AllpastEventArray.count {
                
                let operatorDetailNew = AllpastEventArray[j]
                
                self.AllEventArray.append(operatorDetailNew)
                
            }
            
        }
        
      //  println_debug("AllEventArray count----\(self.AllEventArray.count)")
      //  println_debug("AllEventArray----\(self.AllEventArray)")
        
        AllResultCount = self.AllEventArray.count
        self.setupLayout()
        
        if AllResultCount > 0 {

        self.allEventTableView.dataSource = self
        self.allEventTableView.delegate = self
        self.allEventTableView.reloadData()
            
        }
        
        PTLoader.shared.hide()
        
    }

}


class AllEventTableViewCell: UITableViewCell {
    
    @IBOutlet var programName : UILabel?
    @IBOutlet var startDate : UILabel?
    @IBOutlet var endDate : UILabel?

}
