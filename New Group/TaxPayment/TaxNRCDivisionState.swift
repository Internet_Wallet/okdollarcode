//
//  TaxNRCDivisionState.swift
//  OK
//
//  Created by Shobhit Singhal on 8/22/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol NRCDivisionStateDeletage : class {
    func setDivisionAndTownship(strDivison_township : String)
    func setDivison(division : LocationDetail)
    func setDivisionDetails(stateDivCode: LocationDetail, divisionName: String, township: String)
}

extension NRCDivisionStateDeletage {
    func setDivisionAndTownship(strDivison_township : String) {}
    func setDivison(division : LocationDetail) {}
    func setDivisionDetails(stateDivCode: LocationDetail, divisionName: String, township: String) {}
}

class TaxNRCDivisionState: OKBaseController, NRCTownshipCityDeletage, UITextFieldDelegate {
    @IBOutlet weak var lblNoRecordFound: UILabel!{
        didSet{
            lblNoRecordFound.font =  UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var btnSearch: UIButton!

    weak var townShipDelegate : NRCDivisionStateDeletage?
    var allLocationsList = [LocationDetail]()
    var tfSearch = UITextField()
    var arr_city = [LocationDetail]()
    var isSearch = false
    var array = [LocationDetail]()
    var CHARSET = ""
    var index = 0
    
    var fromPaytoMap : String?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setMarqueLabelInNavigation()
        tableview.tableFooterView = UIView.init(frame: CGRect.zero)
        self.view.backgroundColor = kBackGroundGreyColor
        btnSearch.tag = 1
        loadInitialize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        isSearch = false
        tableview.reloadData()
    }

    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func searchAction(_ sender: Any) {
        if btnSearch.tag == 1 {
            self.navigationItem.titleView = nil
            self.navigationItem.title = ""
            let tfSearch = UITextField()
            tfSearch.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
            self.navigationItem.titleView = tfSearch
            tfSearch.backgroundColor = UIColor.white
            tfSearch.delegate = self
            tfSearch.layer.cornerRadius = 5.0
            tfSearch.textColor = UIColor.black
            tfSearch.font = UIFont(name: appFont, size: 17)
            tfSearch.placeholder = "Search".localized
            tfSearch.tintColor = UIColor.darkGray
            tfSearch.delegate = self
            tfSearch.clearButtonMode = .whileEditing
            
            let tfserarcView = UIView()
            tfserarcView.frame = CGRect(x: 10, y: 0, width: 30, height: 30)
            let tfserarchIcon = UIImageView()
            tfserarchIcon.frame = CGRect(x: 8, y: 8, width: 14, height: 14)
            tfserarcView.addSubview(tfserarchIcon)
            tfserarchIcon.image = #imageLiteral(resourceName: "search")
            tfSearch.leftView = tfserarcView
            tfSearch.leftViewMode = UITextField.ViewMode.always
            
            btnSearch.setImage(nil, for: .normal)
            btnSearch.setTitle("Cancel".localized, for: .normal)
            btnSearch.setTitleColor(UIColor.darkGray, for: .normal)
            tfSearch.becomeFirstResponder()
            btnSearch.tag = 2
        } else if btnSearch.tag == 2{
            btnSearch.tag = 3
            btnSearch.setTitleColor(UIColor.white, for: .normal)
            self.navigationController?.view.endEditing(false)
        } else if btnSearch.tag == 3 {
            btnSearch.tag = 2
            //  btnSearch.setTitleColor(UIColor.darkGray, for: .normal)
            self.navigationController?.view.endEditing(true)
            view.endEditing(true)
        }
    }
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: appFontSize)
        if let _ = fromPaytoMap {
            lblMarque.text = "Select State / Division".localized
        } else {
            lblMarque.text = "Select NRC Division/State Code".localized
        }
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
        lblNoRecordFound.isHidden = true
        lblNoRecordFound.text = "No Record Found".localized
    }
    
    
    func loadInitialize() {
        tableview.register(UINib(nibName: "DivisionStateCell", bundle: nil), forCellReuseIdentifier: "divisionstatecellidentifier")
        tableview.tableFooterView = UIView(frame: CGRect.zero)
        arr_city = TownshipManager.allLocationsList.sorted(by: { $0.stateOrDivitionNameEn < $1.stateOrDivitionNameEn} )
        CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 "
//        if ok_default_language == "my"{
//            arr_city = TownshipManager.allLocationsList.sorted(by: { $0.stateOrDivitionNameMy < $1.stateOrDivitionNameMy} )
//            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာ၁၂၃၄၅၆၇၈၉၀ "
//        }else {
//            arr_city = TownshipManager.allLocationsList.sorted(by: { $0.stateOrDivitionNameEn < $1.stateOrDivitionNameEn} )
//            CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 "
//        }
        DispatchQueue.main.async {
            self.tableview.reloadData()
        }
    }
    
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        tfSearch.resignFirstResponder()
        return true
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        lblNoRecordFound.isHidden = true
        tfSearch.text = ""
        btnSearch.tag = 2
        isSearch = false
        tableview.reloadData()
        return true
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            if updatedText == "" {
                isSearch = false
                lblNoRecordFound.isHidden = true
                tableview.reloadData()
            }
            else{
                btnSearch.setTitleColor(UIColor.white, for: .normal)
                getSearchArrayContains(updatedText)
            }
        }
        return true
    }
    
    func getSearchArrayContains(_ text : String) {
//        if ok_default_language == "my" {
//            array = self.arr_city.filter({$0.nrcDisplayStateDivMY.lowercased().contains(find: text.lowercased())})
//        }else{
//            array = self.arr_city.filter({$0.nrcDisplayStateDivEN.lowercased().contains(find: text.lowercased())})
//        }
        array = self.arr_city.filter({$0.nrcDisplayStateDivEN.lowercased().contains(find: text.lowercased())})
        if array.count == 0 {
            lblNoRecordFound.isHidden = false
            self.btnSearch.tag = 2
            self.btnSearch.tintColor = UIColor.lightGray
        }else {
            lblNoRecordFound.isHidden = true
        }
        isSearch = true
        tableview.reloadData()
    }
}


extension TaxNRCDivisionState : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch {
            return array.count
        }else {
            return arr_city.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "divisionstatecellidentifier", for: indexPath) as! DivisionStateCell
        if isSearch {
            let dic = array[indexPath.row]
//            if ok_default_language == "my"{
//                cell.wrapData(title: dic.nrcDisplayStateDivMY, count: dic.townShipArray.count)
//            }
//            else{
                cell.wrapData(title: dic.nrcDisplayStateDivEN, count: dic.townShipArray.count)
            
            if let _ = fromPaytoMap {
                cell.wrapData(title: dic.stateOrDivitionNameEn, count: 0, isFromPayto: true)
            }
            
//            }
        }else {
            let dic = arr_city[indexPath.row]
//            if ok_default_language == "my"{
//                cell.wrapData(title: dic.nrcDisplayStateDivMY , count: dic.townShipArray.count)
//            }
//            else{
            if let  _ = fromPaytoMap {
                cell.wrapData(title: dic.stateOrDivitionNameEn, count: 0, isFromPayto: true)
            } else {
                cell.wrapData(title: dic.nrcDisplayStateDivEN, count: dic.townShipArray.count)
            }
//            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        self.index = indexPath.row
        btnSearch.tag = 1
        self.performSegue(withIdentifier: "taxNRCDivisionToTownshipCity", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "taxNRCDivisionToTownshipCity" {
            let vc = segue.destination as! UINavigationController
            let townshipVC2 = vc.viewControllers.first as! TaxNRCTownshipCity
            townshipVC2.townshipVC2Delegate = self
            townshipVC2.fromPaytoMap = fromPaytoMap
            if isSearch {
                if let delegate = self.townShipDelegate {
                    delegate.setDivison(division: array[self.index])
                    townshipVC2.selectedDivState = array[self.index]
                }
            }else {
                if let delegate = self.townShipDelegate {
                    delegate.setDivison(division: arr_city[self.index])
                    townshipVC2.selectedDivState = arr_city[self.index]
                }
            }
        }
    }
    
    func NRCValueString(nrcSting : String) {
        self.dismiss(animated: false, completion: nil)
        guard (self.townShipDelegate?.setDivisionAndTownship(strDivison_township: nrcSting) != nil) else {
            return
        }
    }
    
    func didSelectTownshipName(name: String) {
        self.townShipDelegate?.setDivisionDetails(stateDivCode: arr_city[self.index] ,divisionName: arr_city[safe: self.index]?.stateOrDivitionNameEn ?? "", township: name)
        //self.townShipDelegate?.setStateDivisionDetails(stateDivision: arr_city[self.index])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}


