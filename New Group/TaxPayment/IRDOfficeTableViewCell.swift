//
//  IRDOfficeTableViewCell.swift
//  OK
//
//  Created by Shobhit Singhal on 8/24/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class IRDOfficeTableViewCell: UITableViewCell {
    @IBOutlet var iRDOfficeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
