//
//  TaxNRCTownshipCity.swift
//  OK
//
//  Created by Shobhit Singhal on 8/22/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol NRCTownshipCityDeletage : class {
    func NRCValueString(nrcSting : String)
    func didSelectTownshipName(name: String)
}

class TaxNRCTownshipCity: OKBaseController,UITextFieldDelegate {
    weak var townshipVC2Delegate : NRCTownshipCityDeletage?
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var lblNoRecord: UILabel!
    
    var selectedDivState: LocationDetail?
    var townshipArr : [TownShipDetail]?
    var barButton = UIBarButtonItem()
    var tfSearch = UITextField()
    var isSearch = false
    var array = [TownShipDetail]()
    var CHARSET = ""
    
    var fromPaytoMap : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSearch.tag = 1
        self.view.backgroundColor = kBackGroundGreyColor
        self.setMarqueLabelInNavigation()
        loadInitialize()
    }
    
    func loadInitialize() {
//        if ok_default_language == "my" {
//            townshipArr = selectedDivState!.townShipArray.sorted(by: { $0.townShipNameMY < $1.townShipNameMY} )
//            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာ၁၂၃၄၅၆၇၈၉၀ "
//        }else {
//            townshipArr = selectedDivState!.townShipArray.sorted(by: { $0.townShipNameEN < $1.townShipNameEN} )
//            CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 "
//        }
        townshipArr = selectedDivState!.townShipArray.sorted(by: { $0.townShipNameEN < $1.townShipNameEN} )
        CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 "
        lblNoRecord.isHidden = true
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(UINib(nibName: "TownshipCell", bundle: nil), forCellReuseIdentifier: "townshipcellidentifier")
        tableview.tableFooterView = UIView(frame: CGRect.zero)
        tableview.reloadData()
    }
    
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        if btnSearch.tag == 1 {
            self.navigationItem.titleView = nil
            self.navigationItem.title = ""
            let tfSearch = UITextField()
            tfSearch.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
            self.navigationItem.titleView = tfSearch
            tfSearch.backgroundColor = UIColor.white
            tfSearch.delegate = self
            tfSearch.layer.cornerRadius = 5.0
            tfSearch.textColor = UIColor.black
            tfSearch.font = UIFont(name: appFont, size: 17)
            tfSearch.placeholder = "Search".localized
            tfSearch.tintColor = UIColor.darkGray
            tfSearch.delegate = self
            tfSearch.clearButtonMode = .whileEditing
            
            let tfserarcView = UIView()
            tfserarcView.frame = CGRect(x: 10, y: 0, width: 30, height: 30)
            let tfserarchIcon = UIImageView()
            tfserarchIcon.frame = CGRect(x: 8, y: 8, width: 14, height: 14)
            tfserarcView.addSubview(tfserarchIcon)
            tfserarchIcon.image = #imageLiteral(resourceName: "search")
            tfSearch.leftView = tfserarcView
            tfSearch.leftViewMode = UITextField.ViewMode.always
            
            btnSearch.setImage(nil, for: .normal)
            btnSearch.setTitle("Cancel".localized, for: .normal)
            btnSearch.setTitleColor(UIColor.darkGray, for: .normal)
            tfSearch.becomeFirstResponder()
            btnSearch.tag = 2
        } else if btnSearch.tag == 2{
            btnSearch.tag = 3
            btnSearch.setTitleColor(UIColor.white, for: .normal)
            self.navigationController?.view.endEditing(false)
        } else if btnSearch.tag == 3 {
            btnSearch.tag = 2
            // btnSearch.setTitleColor(UIColor.darkGray, for: .normal)
            self.navigationController?.view.endEditing(true)
            view.endEditing(true)
        }
    }
    
    private func setMarqueLabelInNavigation() {
        lblNoRecord.isHidden = true
        lblNoRecord.text = "No Record Found".localized
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.textColor = UIColor.white
        lblMarque.font =  UIFont(name: appFont, size: 18)
        
        if let _ = fromPaytoMap {
            lblMarque.text = "Select Township / City".localized
        } else {
            lblMarque.text = "Select NRC Township code".localized
        }
        
        self.navigationItem.titleView = nil
        self.navigationItem.titleView = lblMarque
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        tfSearch.resignFirstResponder()
        return true
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        lblNoRecord.isHidden = true
        tfSearch.text = ""
        btnSearch.tag = 2
        //btnSearch.setTitleColor(UIColor.darkGray, for: .normal)
        isSearch = false
        tableview.reloadData()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
        
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            if updatedText == "" {
                isSearch = false
                lblNoRecord.isHidden = true
                //  btnSearch.setTitleColor(UIColor.darkGray, for: .normal)
                tableview.reloadData()
            }
            else{
                //  lblNoRecord.isHidden = true
                btnSearch.setTitleColor(UIColor.white, for: .normal)
                getSearchArrayContains(updatedText)
            }
        }
        return true
    }
    
    func getSearchArrayContains(_ text : String) {
//        if ok_default_language == "my" {
//            array = (self.townshipArr?.filter({$0.nrcDisplayCodeMY.lowercased().contains(find: text.lowercased())}))!
//        }else {
//            array = (self.townshipArr?.filter({$0.nrcDisplayCodeEN.lowercased().contains(find: text.lowercased())}))!
//        }
        array = (self.townshipArr?.filter({$0.nrcDisplayCodeEN.lowercased().contains(find: text.lowercased())}))!
        if array.count == 0 {
            lblNoRecord.isHidden = false
            self.btnSearch.tag = 2
            self.btnSearch.tintColor = UIColor.lightGray
        }else {
            lblNoRecord.isHidden = true
        }
        isSearch = true
        tableview.reloadData()
    }
    
}




extension TaxNRCTownshipCity : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch {
            return array.count
        }else{
            return (townshipArr?.count)!
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let townshipCell = tableView.dequeueReusableCell(withIdentifier: "townshipcellidentifier", for: indexPath) as! TownshipCell
        if isSearch {
            let model = array[indexPath.row]
//            if ok_default_language == "my" {
//                townshipCell.townshipNameLbl.text = model.nrcDisplayCodeMY
//            }
//            else {
            if let _ = fromPaytoMap {
                townshipCell.townshipNameLbl.text = model.cityNameEN
            } else {
                townshipCell.townshipNameLbl.text = model.nrcDisplayCodeEN
            }
//            }
            return townshipCell
        }else{
            let model = townshipArr![indexPath.row]
//            if ok_default_language == "my" {
//                townshipCell.townshipNameLbl.text = model.nrcDisplayCodeMY
//            }
//            else {
            
            if let _ = fromPaytoMap {
                townshipCell.townshipNameLbl.text = model.cityNameEN
            } else {
                townshipCell.townshipNameLbl.text = model.nrcDisplayCodeEN
            }
            
//            }
            return townshipCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        var nrcStr = ""
        if isSearch {
            let model = array[indexPath.row]
//            if ok_default_language == "my" {
//                nrcStr = "\(model.nrcCodeNumberMy)" + "/" + "\(model.nrcBCode)" + "(N)".localized as String
//                self.navigationController?.popViewController(animated: false)
//            }
//            else {
                nrcStr = "\(model.nrcCodeNumber)" + "/" + "\(model.nrcCode)" + "(N)".localized as String
            self.dismiss(animated: false, completion: nil)
//            }
        }else{
            let model = townshipArr![indexPath.row]
//            if ok_default_language == "my" {
//                nrcStr = "\(model.nrcCodeNumberMy)" + "/" + "\(model.nrcBCode)" + "(N)".localized as String
//                self.navigationController?.popViewController(animated: false)
//            }
//            else {
                nrcStr = "\(model.nrcCodeNumber)" + "/" + "\(model.nrcCode)" + "(N)".localized as String
            self.dismiss(animated: false, completion: nil)
            self.townshipVC2Delegate?.didSelectTownshipName(name: model.cityNameEN)
//            }
        }
        
        guard (self.townshipVC2Delegate?.NRCValueString(nrcSting: nrcStr) != nil)  else {
            return
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}


