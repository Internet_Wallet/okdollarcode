//
//  ViewTaxReceipt.swift
//  OK
//
//  Created by Shobhit Singhal on 9/1/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ViewTaxReceipt: UIViewController {
    @IBOutlet weak var receiptView: UIView!
    @IBOutlet weak var bankAccNumberLabelValue: UILabel!
    @IBOutlet weak var taxPayerNameLabelValue: UILabel!
    @IBOutlet weak var taxTransactionIDLabelValue: UILabel!
    @IBOutlet weak var addressLabelValue: UILabel!
    @IBOutlet weak var depositBankNameLabelValue: UILabel!
    @IBOutlet weak var taxTypeLabelValue: UILabel!
    @IBOutlet weak var paymentTypeLabelValue: UILabel!
    @IBOutlet weak var taxPaymentDurationLabelValue: UILabel!
    @IBOutlet weak var paymentAmountDurationLabelValue: UILabel!
    @IBOutlet weak var kyatsWithLetterLabelValue: UILabel!
    @IBOutlet weak var dateLabelValue: UILabel!
    @IBOutlet weak var officeNameLabelValue: UILabel!
    
    var dataDict = [String: String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view
        self.initLoad()
    }
    
    func initLoad() {
//        bankAccNumberLabelValue.text = ""
        taxPayerNameLabelValue.text = (UserModel.shared.name.replacingOccurrences(of: ",", with: ". "))
//        taxTransactionIDLabelValue.text = ""
        addressLabelValue.text = dataDict["address_Location"]
        depositBankNameLabelValue.text = dataDict["IRD_Office"]
        taxTypeLabelValue.text = "Income Tax(Salary Tax-Corporate)".localized
        paymentTypeLabelValue.text = "Prepaid Tax".localized
//        taxPaymentDurationLabelValue.text = ""
        paymentAmountDurationLabelValue.text = dataDict["total_Amount"]
//        kyatsWithLetterLabelValue.text = ""
        dateLabelValue.text = dataDict["date"]
        officeNameLabelValue.text = dataDict["IRD_Office"]
        
        bankAccNumberLabelValue.underline()
        taxPayerNameLabelValue.underline()
        taxTransactionIDLabelValue.underline()
        addressLabelValue.underline()
        depositBankNameLabelValue.underline()
        taxTypeLabelValue.underline()
        paymentTypeLabelValue.underline()
        taxPaymentDurationLabelValue.underline()
        paymentAmountDurationLabelValue.underline()
        kyatsWithLetterLabelValue.underline()
        dateLabelValue.underline()
        officeNameLabelValue.underline()
    }

    @IBAction func backBtnAction(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }

}

extension UILabel {
    func underline() {
        if let textString = self.text {
            let textColor: UIColor = .red
            let underLineColor: UIColor = .black
            let underLineStyle = NSUnderlineStyle.single.rawValue
            let labelAtributes:[NSAttributedString.Key : Any]  = [
                NSAttributedString.Key.foregroundColor: textColor,
                NSAttributedString.Key.underlineStyle: underLineStyle,
                NSAttributedString.Key.underlineColor: underLineColor
            ]
            let underlineAttributedString = NSAttributedString(string: textString, attributes: labelAtributes)
            attributedText = underlineAttributedString
        }
    }
}
