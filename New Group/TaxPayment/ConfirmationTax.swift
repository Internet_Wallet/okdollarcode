//
//  ConfirmationTax.swift
//  OK
//
//  Created by Shobhit Singhal on 8/20/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ConfirmationTax: TaxBaseVC, BioMetricLoginDelegate {
    @IBOutlet weak var nameLabelValue : UILabel!
    @IBOutlet weak var nrcNumberLabelValue : UILabel!
    @IBOutlet weak var incomeTaxMonthYearLabelValue : UILabel!
    @IBOutlet weak var emailIdLabelValue : UILabel!
    @IBOutlet weak var totalNumberOfEmployeesLabelValue : UILabel!
    @IBOutlet weak var totalAmountLabelValue : UILabel!
    @IBOutlet weak var irdOfficeLabelValue : UILabel!

    @IBOutlet weak var invoicePdfView : UIView!
    @IBOutlet weak var amountPaidView: UIView!

    var dataDict = Dictionary<String, String>()
    var tokenFullString = ""
    var totalAmountToPayString = ""
    let manager = GeoLocationManager.shared
    let qrCodeObj = QRCodeGenericClass()
    let web = WebApiClass()
    var transaction = Dictionary<String,Any>()
    var currency: String?

    @IBOutlet weak var nameLabel: UILabel!{
        didSet {
            nameLabel.text = nameLabel.text?.localized
        }
    }
    @IBOutlet weak var nrcNumberLabel: UILabel!{
        didSet {
            nrcNumberLabel.text = nrcNumberLabel.text?.localized
        }
    }
    @IBOutlet weak var incomeTaxMonthYearLabel: UILabel!{
        didSet {
            incomeTaxMonthYearLabel.text = incomeTaxMonthYearLabel.text?.localized
        }
    }
    @IBOutlet weak var emailIdLabel: UILabel!{
        didSet {
            emailIdLabel.text = emailIdLabel.text?.localized
        }
    }
    @IBOutlet weak var totalNumberOfEmployeesLabel: UILabel!{
        didSet {
            totalNumberOfEmployeesLabel.text = totalNumberOfEmployeesLabel.text?.localized
        }
    }
    @IBOutlet weak var totalAmountLabel: UILabel!{
        didSet {
            totalAmountLabel.text = totalAmountLabel.text?.localized
        }
    }
    @IBOutlet weak var irdOfficeLabel: UILabel!{
        didSet {
            irdOfficeLabel.text = irdOfficeLabel.text?.localized
        }
    }
    @IBOutlet var payBtn: UIButton!{
        didSet{
            payBtn.setTitle(payBtn.titleLabel?.text?.localized, for: .normal)
        }
    }

    ///////////////////////////// Pdf Invoice outlets ///////////////////////////////
    @IBOutlet weak var senderAccNameLabelValue: UILabel!
    @IBOutlet weak var senderAccNumberLabelValue: UILabel!
    @IBOutlet weak var incomeTaxMonthYearLabelValue2 : UILabel!
    @IBOutlet weak var totalNumberOfEmployeesLabelValue2 : UILabel!
    @IBOutlet weak var payToLabelValue: UILabel!
    @IBOutlet weak var categoriesLabelValue: UILabel!
    @IBOutlet weak var transactionIdLabelValue: UILabel!
    @IBOutlet weak var transactionTypeLabelValue: UILabel!
    @IBOutlet weak var dateTimeLabelValue: UILabel!
    @IBOutlet weak var totalAmountPaidLabelValue: UILabel!
    
    @IBOutlet var taxTransactionDetailsReceiptLabel: UILabel!{
        didSet
        {
            taxTransactionDetailsReceiptLabel.text = taxTransactionDetailsReceiptLabel.text?.localized
        }
    }
    @IBOutlet var senderAccNameLabel: UILabel!{
        didSet
        {
            senderAccNameLabel.text = senderAccNameLabel.text?.localized
        }
    }
    @IBOutlet var senderAccNumberLabel: UILabel!{
        didSet
        {
            senderAccNumberLabel.text = senderAccNumberLabel.text?.localized
        }
    }
    @IBOutlet var incomeTaxMonthYearLabel2: UILabel!{
        didSet
        {
            incomeTaxMonthYearLabel2.text = incomeTaxMonthYearLabel2.text?.localized
        }
    }
    @IBOutlet var totalNumberOfEmployeesLabel2: UILabel!{
        didSet
        {
            totalNumberOfEmployeesLabel2.text = totalNumberOfEmployeesLabel2.text?.localized
        }
    }
    @IBOutlet var payToLabel: UILabel!{
        didSet
        {
            payToLabel.text = payToLabel.text?.localized
        }
    }
    @IBOutlet var categoriesLabel: UILabel!{
        didSet
        {
            categoriesLabel.text = categoriesLabel.text?.localized
        }
    }
    @IBOutlet var transactionIdLabel: UILabel!{
        didSet
        {
            transactionIdLabel.text = transactionIdLabel.text?.localized
        }
    }
    @IBOutlet var transactionTypeLabel: UILabel!{
        didSet
        {
            transactionTypeLabel.text = transactionTypeLabel.text?.localized
        }
    }
    @IBOutlet var dateTimeLabel: UILabel!{
        didSet
        {
            dateTimeLabel.text = dateTimeLabel.text?.localized
        }
    }
    @IBOutlet var totalAmountPaidLabel: UILabel!{
        didSet
        {
            totalAmountPaidLabel.text = totalAmountPaidLabel.text?.localized
        }
    }
///////////////////////////////////////////////////////////////////////////////
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Confirmation".localized
        self.initLoad()
    }

    @IBAction func backBtnAction(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func initLoad() {
        let emailArr = UserModel.shared.email.components(separatedBy: ",")
        amountPaidView.layer.borderWidth = 1
        amountPaidView.layer.borderColor = UIColor.colorWithRedValue(redValue: 60, greenValue: 60, blueValue: 60, alpha: 1).cgColor
        nameLabelValue.text = UserModel.shared.name
        nrcNumberLabelValue.text = UserModel.shared.nrc
        incomeTaxMonthYearLabelValue.text = "\(self.dataDict["month"] ?? "") \(self.dataDict["year"] ?? "")"
        emailIdLabelValue.text = emailArr[0]
        totalNumberOfEmployeesLabelValue.text = dataDict["total_Employees"]
        totalAmountLabelValue.text = dataDict["total_Amount"]
        irdOfficeLabelValue.text = dataDict["IRD_Office"]
    }
    
    func updatePdfData() {
        senderAccNameLabelValue.text = (UserModel.shared.name.replacingOccurrences(of: ",", with: ". "))
        senderAccNumberLabelValue.text = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "(+95) 0")
        incomeTaxMonthYearLabelValue2.text = "\(self.dataDict["month"] ?? "") \(self.dataDict["year"] ?? "")"
        totalNumberOfEmployeesLabelValue2.text = dataDict["total_Employees"]
        payToLabelValue.text = "IRD Office \n \(dataDict["IRD_Office"] ?? "")"
        categoriesLabelValue.text = "Salary Tax".localized
        transactionIdLabelValue.text = dataDict["transactionId"]
        transactionTypeLabelValue.text = "Tax Payment".localized
        let (date,time) = qrCodeObj.currentDateAndTime()
        let dateString = date + " " + time
        dateTimeLabelValue.text = dateString
        totalAmountPaidLabelValue.text = dataDict["total_Amount"]
        dataDict["date"] = date
        dataDict["time"] = time
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if screen == "TaxPayment" {
            if isSuccessful{
                self.callingHttp()
//                self.performSegue(withIdentifier: "TaxConfirmationToReceipt", sender: self)
            }
        }
    }
    
    @IBAction func payAction(_ sender: UIButton) {
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "TaxPayment", delegate: self)
        }
        else {
            self.callingHttp()
        }
    }
    
    func callingHttp() {
//        self.passDataToReceipt()
        let param =  self.generatePaymentForTaxPayment()
        guard let url = URL.init(string: "https://www.okdollar.co/RestService.svc/'GenericPayment'") else { return }
        
        TopupWeb.genericClass(url: url, param: param as AnyObject, httpMethod: "POST") { [weak self](response, success) in
            if success {
                if let json = response as? Dictionary<String,Any> {
                    if let code = json["Code"] as? NSNumber, code == 200 {
                        if let data = json["Data"] as? String {
                            let xmlString = SWXMLHash.parse(data)
                            self?.enumerate(indexer: xmlString)
                            if self?.transaction.safeValueForKey("resultdescription") as? String == "Transaction Successful" {
                                if let jsonData = self?.transaction.jsonString().data(using: .utf8) {
                                    if let model = try? JSONDecoder().decode(PaymentFinalRecieptModel.self, from: jsonData) {
                                        DispatchQueue.main.async {
                                            println_debug(model)
                                            self?.dataDict["transactionId"] = model.transid
                                            self!.updatePdfData()

                                            guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: self!.invoicePdfView, pdfFile: "OK$ Bill_Payments Report") else {
                                                println_debug("Error - pdf not generated")
                                                return
                                            }
                                            println_debug(pdfUrl)
                                            
                                            do {
                                                let fileData = try Data(contentsOf: pdfUrl)
                                                self!.getPDFFilePathURL(filePath : pdfUrl.path , fileData : fileData, fileName: pdfUrl.lastPathComponent)
                                            } catch {
                                                println_debug("loading image file error")
                                            }
                                            
//                                            guard let vc = self?.storyboard?.instantiateViewController(withIdentifier: "TopupSingleViewRecieptController") as? TopupSingleViewRecieptController else { return }
//                                            vc.reciptModel = model
//                                            vc.currencyInt = self?.currency
//                                            vc.InternationalTopup = self?.arrDictionary.first?.safeValueForKey("internationalNumber") as? String ?? ""
//                                            if let _ = self?.isInternationl {
//                                                vc.receiverName = InternationalTopupManager.receiverName
//                                            }
//                                            vc.planType =  "Overseas Top-Up Plan"
//                                            self?.navigationController?.pushViewController(vc, animated: true)
                                        }
                                    }
                                }
                            } else {
                                self?.showErrorAlert(errMessage: self?.transaction.safeValueForKey("resultdescription") as? String ?? "Please Try Again".localized)
                            }
                        }
                    } else {
                        if let msg = json["Msg"] as? String {
                            self?.showErrorAlert(errMessage: msg)
                        }
                    }
                }
            }
        }
    }
    
    func getPDFFilePathURL(filePath : String, fileData : Data, fileName: String) {
        progressViewObj.showProgressView()
        let urlStr = String.init(format: "http://test.media.api.okdollar.org/phones/IRD/devices/\(uuid)/files")
        let url: URL? = URL.init(string: urlStr)
        //        let url = getUrl(urlStr: "phones/\(UserModel.shared.mobileNo)/devices/\(uuid)/files", serverType: .mediaUploadFilesUrl)
        println_debug(url)
        
        let request = NSMutableURLRequest(url:url as! URL)
        request.httpMethod = "POST";
        
        let boundary = "Boundary-\(uuid)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.httpBody = self.createBodyWithParameters(parameters: nil, filePathKey: filePath, imageDataKey: fileData as Data, boundary: boundary) as Data
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            (data, response, error) in
            if error != nil {
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 201 {
                println_debug("statusCode should be 200, but is \(httpStatus.statusCode)")
                if let httpResponse = response as? HTTPURLResponse {
                    if let xDemAuth = httpResponse.allHeaderFields["Location"] as? String {
                        // use X-Dem-Auth here
                        println_debug(xDemAuth)
                        DispatchQueue.main.async {
//                            self.isFileUploaded = true
//                            self.uploadFileName = fileName
                            self.dataDict["pdfFIlePath"] = xDemAuth
                            println_debug(self.dataDict)
                            progressViewObj.removeProgressView()
                            self.getTokenAndHitFinalPaymentPayAPI()
                        }
                    }
                }
            }
            else {
                progressViewObj.removeProgressView()
            }
        }
        task.resume()
    }
    
    func getTokenAndHitFinalPaymentPayAPI() {
        if appDelegate.checkNetworkAvail() {
            self.view.endEditing(true)
            getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
                guard isSuccess, let _ = tokenString else {
                    self.showErrorAlert(errMessage: "Try Again".localized)
                    return
                }
                DispatchQueue.main.async(execute: {
                    self.hitFinalPaymentPayAPI()
                })
            })
        }
        else {
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            alertViewObj.showAlert(controller: UIViewController.init())
        }
    }
    
    func getTokenFromServer(completionHandler: @escaping (Bool,Any?) -> Void) {
        let urlString = String.init(format: Url.TaxPaymentTokenAPIURL)
        let getURL = getUrl(urlStr: urlString, serverType: .taxation) as? URL
        
        guard let tokenUrl = getURL else {
            showErrorAlert(errMessage: "Something went wrong : Dev need to check")
            return
        }
        let hashValue = Url.aKey_Tax_Payment.hmac_SHA1(key: Url.sKey_Tax_Payment)
        let inputVal = "password=\(hashValue)&grant_type=password"
        
        progressViewObj.showProgressView()
        println_debug("get token from server called........")
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: kMethod_Post, contentType: kContentType_urlencoded, inputVal: inputVal, authStr: nil)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    println_debug("remove progress view called inside get token from server call")
                }
                completionHandler(false,nil)
                return
            }
            println_debug("response dict for get token from server :: \(String(describing: response))")
            let responseDic = response as! NSDictionary
            guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                completionHandler(false,nil)
                return
            }
            let authorizationString =  "\(tokentype) \(accesstoken)"
            self.tokenFullString = authorizationString
            completionHandler(true,authorizationString)
        })
    }
    
    func hitFinalPaymentPayAPI() {
        if appDelegate.checkNetworkAvail() {
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
//            let paymentPayDict = self.generatePaymentPayDict()
            let urlString   = String.init(format: Url.paymentPay)
            let getURL = getUrl(urlStr: urlString, serverType: .taxation)
            println_debug(getURL)

            let emailArr = UserModel.shared.email.components(separatedBy: ",")

            let strPipeLine = "EmployeeName=\(UserModel.shared.name)|NrcNumber=\(UserModel.shared.nrc)|EmployeeFilePath=\(dataDict["xlsxFIlePath"] ?? "")|ReceiptFilePath=\(dataDict["pdfFIlePath"] ?? "")|TotalEmployeeNumber=\(dataDict["total_Employees"] ?? "")|TotalAmount=\(dataDict["total_Amount"] ?? "")|IrdDetailId=\(dataDict["IrdDetailId"] ?? "")|OkAccountNumber=\(UserModel.shared.mobileNo)|DestinationOkNumber=\(dataDict["destinationNumber"] ?? "")|OkTransactionId=\(dataDict["transactionId"] ?? "")|PaymentStatus=success|TransactionDate=\(self.currentDate())|Email=\(emailArr[0])|Month=\(self.dataDict["month"] ?? "")|Year=\(self.dataDict["year"] ?? "")"
            let hashValue = strPipeLine.hmac_SHA1(key: Url.sKey_Tax_Payment)
            
            
            
           // let hashValue = Url.aKey_Tax_Payment.hmac_SHA1(key: Url.sKey_Tax_Payment)
            let keys : [String] = ["EmployeeName","NrcNumber","EmployeeFilePath","ReceiptFilePath", "TotalEmployeeNumber","TotalAmount","IrdDetailId","OkAccountNumber", "DestinationOkNumber","OkTransactionId","PaymentStatus","TransactionDate", "Email", "Month", "Year", "HashValue"]
            let values : [String] = [UserModel.shared.name, UserModel.shared.nrc , dataDict["xlsxFIlePath"] ?? "" , dataDict["pdfFIlePath"] ?? "", dataDict["total_Employees"] ?? "", dataDict["total_Amount"] ?? "", dataDict["IrdDetailId"] ?? "", UserModel.shared.mobileNo, dataDict["destinationNumber"] ?? "", dataDict["transactionId"] ?? "", "success", self.currentDate(), emailArr[0], self.dataDict["month"] ?? "", self.dataDict["year"] ?? "", hashValue]

            let jsonObj = JSONStringWriter()
            let jsonStr = jsonObj.writeJsonIntoString(keys, values)
            println_debug(jsonStr)

            //let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: getURL, methodType: kMethod_Post, contentType: kContentType_Json, inputVal: jsonStr, authStr: self.tokenFullString)
            
//            let params = [String: AnyObject]() as AnyObject
            //let httpData = jsonStr.data(using: .utf8)!
            let header = String.init(format: "%@", tokenFullString)

            TopupWeb.genericApiWithHeader(url: getURL, param: jsonStr as AnyObject, httpMethod: "POST", header: header, data: nil) { (response, success) in
                DispatchQueue.main.async {
                    if success {
                        let jsonData = JSON(response)
                        println_debug(jsonData)
                        if jsonData["StatusCode"].stringValue == "201" && jsonData["Status"].boolValue == true {
                            self.performSegue(withIdentifier: "TaxConfirmationToReceipt", sender: self)
                            DispatchQueue.main.async {
                            }
                        }
                        else {
                            alertViewObj.wrapAlert(title: nil, body: jsonData["Message"].stringValue, img: nil)
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                            })
                            alertViewObj.showAlert(controller: self)
                        }
                    } else {
                        alertViewObj.wrapAlert(title: nil, body:"Network error and try again".localized, img: #imageLiteral(resourceName: "alert-icon"))
                        alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        }
                        alertViewObj.showAlert(controller: UIViewController.init())
                    }
                }
            }
        }
        else {
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            alertViewObj.showAlert(controller: UIViewController.init())
        }
    }
    
    func generatePaymentPayDict() -> [String: String] {
        var paymentPayDict = [String: String]()

        paymentPayDict["EmployeeName"] = UserModel.shared.name
        paymentPayDict["NrcNumber"] = UserModel.shared.nrc
        paymentPayDict["EmployeeFilePath"] = dataDict["xlsxFIlePath"]
        paymentPayDict["ReceiptFilePath"] = dataDict["pdfFIlePath"]
        paymentPayDict["TotalEmployeeNumber"] = dataDict["total_Employees"]
        paymentPayDict["TotalAmount"] = dataDict["total_Amount"]
        paymentPayDict["IrdDetailId"] = dataDict["IrdDetailId"]
        paymentPayDict["OkAccountNumber"] = UserModel.shared.mobileNo
        paymentPayDict["DestinationOkNumber"] = dataDict["destinationNumber"]
        paymentPayDict["OkTransactionId"] = dataDict["transactionId"]
        paymentPayDict["PaymentStatus"] = "0"
        paymentPayDict["TransactionDate"] = self.currentDate()
//        paymentPayDict["HashValue"] = hashValue
      
        println_debug(paymentPayDict)
        return paymentPayDict
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            transaction[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func generatePaymentForTaxPayment() -> Dictionary<String,Any> {
//        var TransactionsCellTower  = Dictionary<String,Any>()
//        TransactionsCellTower["Lac"] = ""
//        TransactionsCellTower["Mcc"] = mccStatus.mcc
//        TransactionsCellTower["Mac"] = ""
//        TransactionsCellTower["Mnc"] = mccStatus.mnc
//        TransactionsCellTower["SignalStrength"] = ""
//        TransactionsCellTower["Ssid"] = ""
//        TransactionsCellTower["ConnectedwifiName"] = ""
//        TransactionsCellTower["NetworkType"] = ""
//        TransactionsCellTower["NetworkSpeed"] = ""
        
        // Transaction cell tower
        var TransactionsCellTower  = Dictionary<String,Any>()
        TransactionsCellTower["Lac"] = ""
        TransactionsCellTower["Mcc"] = mccStatus.mcc
        TransactionsCellTower["Mnc"] = mccStatus.mnc
        TransactionsCellTower["SignalStrength"] = ""
        TransactionsCellTower["NetworkType"] = UitilityClass.getNetworkType()
        TransactionsCellTower["NetworkSpeed"] = UitilityClass.getSignalStrength()
        
        if UitilityClass.isConnectedToWifi(){
            let wifiInfo = UitilityClass.getNetworkInfos()[0].components(separatedBy: ",")
            TransactionsCellTower["ConnectedwifiName"] = wifiInfo[0]
            TransactionsCellTower["Ssid"] = wifiInfo[0]
            TransactionsCellTower["Mac"] = wifiInfo[1]
        }else{
            TransactionsCellTower["ConnectedwifiName"] = ""
            TransactionsCellTower["Ssid"] = ""
            TransactionsCellTower["Mac"] = ""
        }
        // l External Reference
        var lExternalReference = Dictionary<String,Any>()
        lExternalReference["Direction"] = true
        lExternalReference["EndStation"] = ""
        lExternalReference["StartStation"] = ""
        lExternalReference["VendorID"] = ""
        
        // L geo Location
        var lGeoLocation = Dictionary<String,Any>()
        lGeoLocation["CellID"] = ""
        lGeoLocation["Latitude"] = manager.currentLatitude
        lGeoLocation["Longitude"] = manager.currentLongitude
        lGeoLocation["mBltDevice"] = ""
        lGeoLocation["mWifiDevice"] = ""

        // L Proximity
        var lProximity = Dictionary<String,Any>()
        lProximity["BlueToothUsers"] = [Any]()
        lProximity["SelectionMode"] = false
        lProximity["WifiUsers"] = [Any]()
        
        // Final payment Dictionary
        var lTransactions = Dictionary<String, Any>()
        lTransactions["Amount"] = dataDict["total_Amount"]
        lTransactions["Balance"] = dataDict["balance"]
        lTransactions["DestinationNumberAccountType"] = ""
        lTransactions["BonusPoint"] = 0
        lTransactions["BusinessName"] = ""
        lTransactions["CashBackFlag"] = 0
        lTransactions["Comments"] = "#tax-corporate-tax-na#"
        lTransactions["Destination"] = dataDict["destinationNumber"]
        lTransactions["DestinationNumberWalletBalance"] = ""
        lTransactions["DiscountPayTo"] = 0
        lTransactions["IsMectelTopUp"] = false
        lTransactions["IsPromotionApplicable"] = "0"
        lTransactions["KickBack"] = "0.0"
        lTransactions["KickBackMsisdn"] = ""
        lTransactions["LocalTransactionType"]  = "Corporate Tax"
        lTransactions["MerchantName"]  = ""
        lTransactions["MobileNumber"] = UserModel.shared.mobileNo
        lTransactions["Mode"] = true
        lTransactions["Password"] = ok_password ?? ""
        lTransactions["PromoActualAmount"] = ""
        lTransactions["PromoCodeId"] = ""
        lTransactions["ResultCode"] = 0
        lTransactions["ResultDescription"] = "Corporate Tax Payment"
        lTransactions["SecureToken"] = UserLogin.shared.token
        lTransactions["TransactionID"] = ""
        lTransactions["TransactionTime"] = ""
        lTransactions["TransactionType"] = "PAYTO"
        lTransactions["accounType"] = ""
        lTransactions["senderBusinessName"] = ""
        lTransactions["userName"] = "Unknown"

        var finalParameterDictionaried = Dictionary<String,Any>()
        finalParameterDictionaried["TransactionsCellTower"] = TransactionsCellTower
        finalParameterDictionaried["lExternalReference"] = lExternalReference
        finalParameterDictionaried["lGeoLocation"] = lGeoLocation
        finalParameterDictionaried["lProximity"] = lProximity
        finalParameterDictionaried["lTransactions"] = lTransactions
        
        println_debug(finalParameterDictionaried)
        return finalParameterDictionaried
    }
    
    private func passDataToReceipt () {
//        let balance : Double = (UserLogin.shared.walletBal as NSString).doubleValue + NSString(string: dataDict["total_Amount"]!).doubleValue
//        let doubleStrBillAmount = String(format: "%.2f", balance)
//        dataDict["balance"] = wrapAmountWithCommaDecimal(key: doubleStrBillAmount)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TaxConfirmationToReceipt" {
            let vc = segue.destination as! UINavigationController
            let rootViewController = vc.viewControllers.first as! TaxReceipt
            rootViewController.dataDict = self.dataDict
        }
    }
}

extension ConfirmationTax : WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        DispatchQueue.main.async {
            progressViewObj.removeProgressView()
        }
        do {
            if let data = json as? Data {
                let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                println_debug(dict)
                if let responseDic = dict as? Dictionary<String,Any> {
                    let jsonData = JSON(responseDic)
                    println_debug(jsonData)
                    if jsonData["StatusCode"].stringValue == "201" && jsonData["Status"].boolValue == true {
                        
                    }
                    else {
                        self.showMsgPopupAlert(msg: jsonData["Message"].stringValue, image: #imageLiteral(resourceName: "dashboard_taxpayment"))
                    }
                }
            }
        } catch {
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            alertViewObj.showAlert(controller: UIViewController.init())
        }
    }
}
