//
//  TaxNRCType.swift
//  OK
//
//  Created by Shobhit Singhal on 8/23/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol  TaxNRCTypeDelegate{
    func setNrcTypeName(nrcType : String)
}

class TaxNRCType: UIViewController {
    @IBOutlet weak var btnP: UIButton!
    @IBOutlet weak var btnN: UIButton!
    @IBOutlet weak var btnE: UIButton!
    @IBOutlet weak var btnT: UIButton!
    var nRCTypesVCDelegate : TaxNRCTypeDelegate?
    var nrcName:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        let first = nrcName.components(separatedBy: "(").first!
        self.btnN.setTitle("\(first)" + "(N)", for: .normal)
        self.btnP.setTitle("\(first)" + "(P)", for: .normal)
        self.btnE.setTitle("\(first)" + "(E)", for: .normal)
        self.btnT.setTitle("\(first)" + "(T)", for: .normal)
    }
    
    @IBAction func dismissViewAction(_ sender: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.15, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished){
                self.dismiss(animated: true, completion: nil)
            }
        })
    }
    
    @IBAction func btnNrcTypeAction(_ sender: UIButton) {
        UIView.animate(withDuration: 0.15, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished){
                self.dismiss(animated: true, completion: nil)
                guard (self.nRCTypesVCDelegate?.setNrcTypeName(nrcType: (sender.titleLabel?.text)!) != nil) else {
                    return
                }
            }
        })
    }
}
