//
//  IncomeTaxSalary.swift
//  OK
//
//  Created by Shobhit Singhal on 8/20/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import MobileCoreServices

class IncomeTaxSalary: TaxBaseVC {
    @IBOutlet weak var IncomeTaxTableView : UITableView!
    @IBOutlet weak var payBtn: UIButton!

    let btnnrc = UIButton()
    var tokenFullString = ""
    var uploadFileName:String = ""
    var selectedMonthArr = [String]()
    var selectedYear:String = ""

    var dataDict = Dictionary<String, String>()
    var isFileUploaded = false
    
    @IBOutlet var submitBtn: UIButton!{
        didSet{
            submitBtn.setTitle(submitBtn.titleLabel?.text?.localized, for: .normal)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Income Tax Salary".localized
        IncomeTaxTableView.estimatedRowHeight = 60
        IncomeTaxTableView.rowHeight = UITableView.automaticDimension
        self.hideSubmitBtn()
        self.dataDict["month"] = ""
        self.dataDict["year"] = ""
        uploadFileName = "Upload File".localized
        
//        self.IncomeTaxTableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.IncomeTaxTableView.bounds.size.width, height: 45))
//        self.IncomeTaxTableView.contentInset = UIEdgeInsets(top: -45, left: 0, bottom: 0, right: 0)

//        payBtn.backgroundColor = UIColor.lightGray
//        payBtn.setTitleColor(UIColor.white, for: .normal)
//        payBtn.isUserInteractionEnabled = false
//        getTokenAndHitPaymentsEnquiryAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }

    func getTokenAndHitPaymentsEnquiryAPI() {
        if appDelegate.checkNetworkAvail() {
            self.view.endEditing(true)
            getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
                guard isSuccess, let _ = tokenString else {
                    self.showErrorAlert(errMessage: "Try Again".localized)
                    return
                }
                DispatchQueue.main.async(execute: {
                    self.callingHttp()
                })
            })
        }
        else {
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            alertViewObj.showAlert(controller: UIViewController.init())
        }
    }
    
    func getTokenFromServer(completionHandler: @escaping (Bool,Any?) -> Void) {
        let urlString = String.init(format: Url.TaxPaymentTokenAPIURL)
        let tokenUrl = getUrl(urlStr: urlString, serverType: .taxation)

//        guard let tokenUrl = getURL else {
//            showErrorAlert(errMessage: "Something went wrong : Dev need to check")
//            return
//        }
        let hashValue = Url.aKey_Tax_Payment.hmac_SHA1(key: Url.sKey_Tax_Payment)
        let inputVal = "password=\(hashValue)&grant_type=password"
        
        progressViewObj.showProgressView()
        println_debug("get token from server called........")
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: kMethod_Post, contentType: kContentType_urlencoded, inputVal: inputVal, authStr: nil)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    println_debug("remove progress view called inside get token from server call")
                }
                completionHandler(false,nil)
                return
            }
            println_debug("response dict for get token from server :: \(String(describing: response))")
            let responseDic = response as! NSDictionary
            guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                completionHandler(false,nil)
                return
            }
            let authorizationString =  "\(tokentype) \(accesstoken)"
            self.tokenFullString = authorizationString
            completionHandler(true,authorizationString)
        })
    }
    
    func callingHttp() {
        if appDelegate.checkNetworkAvail() {
            let urlString   = String.init(format: Url.taxPaymentEnquiry, "\(UserModel.shared.mobileNo)", "1")
            let getURL = getUrl(urlStr: urlString, serverType: .taxation)
            println_debug(getURL)
            let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: getURL, methodType: kMethod_Get, contentType: kContentType_Json, inputVal: nil, authStr: self.tokenFullString)
            DispatchQueue.main.async {
                println_debug("progress view started")
            }
            
            JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
                guard isSuccess else {
                    DispatchQueue.main.async {
                        println_debug("remove progress view called inside cashin main api call with error")
                        progressViewObj.removeProgressView()
                    }
                    self.showErrorAlert(errMessage: "Try Again".localized)
                    return
                }
                DispatchQueue.main.async(execute: {
                    if let responseDic = response as? Dictionary<String,Any> {
                        let jsonData = JSON(responseDic)
                        println_debug(jsonData)
                        if jsonData["StatusCode"].stringValue == "302" && jsonData["Status"].boolValue == true {
                            
                        }
                        else {
                            let currentStr = UserDefaults.standard.string(forKey: "currentLanguage")
                            var displayMsg = ""
                            if currentStr == "my" {
                                displayMsg = jsonData["Result"]["MessageZawgyi"].stringValue
                            } else {
                                displayMsg = jsonData["Result"]["MessageEnglish"].stringValue
                            }
                            //                                self.showMsgPopupAlert(msg: displayMsg, image: #imageLiteral(resourceName: "dashboard_taxpayment"))
                            alertViewObj.wrapAlert(title: "".localized, body:displayMsg, img: #imageLiteral(resourceName: "tax_Logo"))
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                //                                    self.parent?.parent?.dismiss(animated: true, completion: nil)
                                self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                            }
                            alertViewObj.showAlert(controller: UIViewController.init())
                        }
                    }
                })
                progressViewObj.removeProgressView()
            })
        }
        else {
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            alertViewObj.showAlert(controller: UIViewController.init())
        }
    }
    
    @IBAction func backBtnAction(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitBtnAction(_ sender: UIButton) {
        var checkMonth = false
        for val in selectedMonthArr {
            if val == self.dataDict["month"] {
                checkMonth = true
            }
        }
        
        if checkMonth != true || self.dataDict["year"] != selectedYear {
            self.showMsgPopupAlert(msg: "Selected and given payment date in excel file mismatches.", image: #imageLiteral(resourceName: "dashboard_taxpayment"))
            return
        }
        let balance : Double = (UserLogin.shared.walletBal as NSString).doubleValue - NSString(string: dataDict["total_Amount"]!).doubleValue
        let doubleStrBillAmount = String(format: "%.2f", balance)
        dataDict["balance"] = wrapAmountWithCommaDecimal(key: doubleStrBillAmount)
        self.performSegue(withIdentifier: "IncomeTaxSalaryToConfirmation", sender: self)
    }
    
//    @IBAction func payAction(_ sender: UIButton) {
//        if let cell:UserNameTableViewCell = self.IncomeTaxTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? UserNameTableViewCell {
//            self.dataDict["name"] = cell.tfUserName.text
//        }
//        if let cell:NRCDetailsTaxTableViewCell = self.IncomeTaxTableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? NRCDetailsTaxTableViewCell {
//            self.dataDict["nrc_Number"] = "\(btnnrc.titleLabel?.text ?? "")/\(cell.nRCTextField.text ?? "")"
//        }
//        self.performSegue(withIdentifier: "corporateTaxToSelectIRDOffice", sender: self)
//    }
}

extension IncomeTaxSalary: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        if isFileUploaded {
            return 3
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 40))
        headerView.backgroundColor = UIColor.colorWithRedValue(redValue: 244, greenValue: 244, blueValue: 245, alpha: 1)
        if section == 1 {
            let headerLabel = UILabel(frame: CGRect(x: 12, y: 10, width: screenWidth-40, height: 30))
            headerLabel.text = "Income Tax Month & Year".localized
            headerLabel.font = UIFont.init(name: appFont, size: 16)
            headerLabel.textColor = UIColor.darkGray
            headerView.addSubview(headerLabel)
            return headerView
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else if section == 1 {
            return 45
        } else {
            return 25
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch (indexPath.section) {
//        case 1:
//            let cell = IncomeTaxTableView.dequeueReusableCell(withIdentifier: "NRCDetailsCell") as! NRCDetailsTaxTableViewCell
//            cell.delegate = self
//            cell.nRCTextField.myDelegate = self
//            return cell
        case 1:
            let cell = IncomeTaxTableView.dequeueReusableCell(withIdentifier: "UploadFileCell") as! UploadFileTableViewCell
            cell.delegate = self
            cell.uploadFileBtn.addTarget(self, action: #selector(self.uploadFileBtnAction(sender:)), for: .touchUpInside)
            cell.downloadSampleFileBtn.addTarget(self, action: #selector(self.downloadSampleFileBtnBtnAction(sender:)), for: .touchUpInside)
            cell.wrapData(title: self.uploadFileName)
            return cell
        case 2:
            let cell = IncomeTaxTableView.dequeueReusableCell(withIdentifier: "TaxAccountDetailsCell") as! TaxAccountXLSFileTableViewCell
            let totalEmployees = self.dataDict["total_Employees"] ?? ""
            let billAmount = String.init(format: "%@", self.dataDict["total_Amount"] ?? "")
            let myDouble = Double(billAmount)
            let doubleStrBillAmount = String(format: "%.2f", myDouble!)
            let billAmountStr = wrapAmountWithCommaDecimal(key: doubleStrBillAmount)
            self.dataDict["total_Amount"] = billAmountStr
            self.dataDict["total_Employees"] = totalEmployees
            cell.wrapData(totalEmployees: totalEmployees, totalAmount: billAmountStr)
            return cell
        default:
            if indexPath.row == 0 {
                let cell = IncomeTaxTableView.dequeueReusableCell(withIdentifier: "TaxUserNameCell", for: indexPath) as! SalaryTaxUserNameCell
                return cell
            }
            let cell = IncomeTaxTableView.dequeueReusableCell(withIdentifier: "TaxNRCDetailCell", for: indexPath) as! SalaryTaxNRCDetailCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 50
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 1 {
            if let cell:NRCDetailsTaxTableViewCell = self.IncomeTaxTableView.cellForRow(at: IndexPath(row: 0, section: indexPath.section)) as? NRCDetailsTaxTableViewCell {
                if !cell.enterNRCLabel.isHidden {
                    if let cell:UserNameTableViewCell = self.IncomeTaxTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? UserNameTableViewCell {
                        if cell.tfUserName.text?.count == 0 {
                            self.showMsgPopupAlert(msg: "Please enter name".localized, image: #imageLiteral(resourceName: "Contact Mobile Number"))
                        } else {
                            let viewController = UIStoryboard(name: "TaxPayment", bundle: Bundle.main).instantiateViewController(withIdentifier: "NRCDivisionRoot")
                            if let nav = viewController as? UINavigationController {
                                for views in nav.viewControllers {
                                    if let vc = views as? TaxNRCDivisionState {
                                        vc.townShipDelegate = self
                                    }
                                }
                            }
                            viewController.modalPresentationStyle = .fullScreen
                            self.present(viewController, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
        else if indexPath.section == 2 {
            if let cell:NRCDetailsTaxTableViewCell = self.IncomeTaxTableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? NRCDetailsTaxTableViewCell {
                if (cell.nRCTextField.text?.count)! < 6 {
                    self.showMsgPopupAlert(msg: "Please enter NRC details".localized, image: #imageLiteral(resourceName: "Contact Mobile Number"))
                } else {
                    println_debug("Upload File Action Called")
//                    self.showToastlocal(message: "Choose only .xls and .xlsx file format which is less than 6 MB".localized)
                    self.showMsgPopupAlert(msg: "Choose only .xls and .xlsx file format which is less than 7 MB".localized, image: #imageLiteral(resourceName: "dashboard_taxpayment"))

//                    payBtn.isUserInteractionEnabled = true
//                    payBtn.backgroundColor = UIColor.colorWithRedValue(redValue: 254, greenValue: 196, blueValue: 0, alpha: 1)
//                    payBtn.setTitleColor(UIColor.colorWithRedValue(redValue: 22, greenValue: 45, blueValue: 255, alpha: 1), for: .normal)
                    let importMenu = UIDocumentMenuViewController(documentTypes: [String("com.microsoft.excel.xls")], in: .import)
                    importMenu.delegate = self
//                    importMenu.addOption(withTitle: "Choose only .xls and .xlsx file format which is less than 6 MB".localized, image: nil, order: .first, handler: {
//                    })
                    importMenu.modalPresentationStyle = .fullScreen
                    self.present(importMenu, animated: true, completion: nil)
                }
            }
        }
    }
   
    @objc func uploadFileBtnAction (sender: UIButton) {
        if self.selectedMonthArr.count != 0 && self.selectedYear != "" {
            alertViewObj.wrapAlert(title: "".localized, body:"Choose only .xls and .xlsx file format which is less than 7 MB".localized, img: #imageLiteral(resourceName: "dashboard_taxpayment"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                let types = [kUTTypeSpreadsheet]
                let importMenu = UIDocumentMenuViewController(documentTypes: types as [String], in: .import)
                importMenu.delegate = self
                importMenu.modalPresentationStyle = .fullScreen
                self.present(importMenu, animated: true, completion: nil)
                
            }
            alertViewObj.showAlert(controller: UIViewController.init())
//            self.showMsgPopupAlert(msg: "Choose only .xls and .xlsx file format which is less than 7 MB".localized, image: #imageLiteral(resourceName: "dashboard_taxpayment"))
        }
        else {
            self.showMsgPopupAlert(msg: "Enter Tax Payment month and year is missing".localized, image: #imageLiteral(resourceName: "dashboard_taxpayment"))
        }
    }
    
    @objc func downloadSampleFileBtnBtnAction (sender: UIButton) {
        guard let url = URL(string: "https://s3-ap-southeast-1.amazonaws.com/okdollar/IRD/ddbc8354-6252-4c24-b4f9-969e678a1ec1.xlsx") else { return }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else { return }
//            let iCloudDocumentsURL = FileManager.default.url(forUbiquityContainerIdentifier: nil)
//            var fileManager: FileManager = FileManager()
            var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last
            docURL = docURL?.appendingPathComponent( "Tax_Sample_File")
            
            do {
                print("Document URL",docURL ?? "")
                DispatchQueue.main.async {
//                    self.showToastlocal(message: "Tax_Sample_File has been saved.".localized)
                    self.showMsgPopupAlert(msg: "Tax_Sample_File has been saved.".localized, image: #imageLiteral(resourceName: "dashboard_taxpayment"))
                }
                try data.write(to: docURL!)
            } catch {
                print(error)
            }
            }.resume()
    }
    
    private func showToastlocal(message : String) {
        let toastView = UIView(frame: CGRect(x: 20, y: (screenHeight/2)-80, width: screenWidth-40, height: 80))
        toastView.layer.cornerRadius = 20
        toastView.layer.masksToBounds = true
        let toastLabel = UILabel()
        toastLabel.frame = CGRect(x: 10, y: 10, width: toastView.frame.size.width-20, height: 60)
        toastLabel.text = message
        toastLabel.numberOfLines = 0
        toastLabel.font = UIFont(name: appFont,size: 14.0)
        toastLabel.textAlignment = .center
        toastLabel.textColor = .white
        toastView.backgroundColor = UIColor.black
        self.view.bringSubviewToFront(toastLabel)
        toastView.addSubview(toastLabel)
        self.view.addSubview(toastView)
        UIView.animate(withDuration: 4, delay: 1, options: .curveEaseOut, animations: {
            toastView.alpha = 0.0
        }, completion: {(isCompleted) in
            toastView.removeFromSuperview()
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "corporateTaxToNRCType" {
            let controller = segue.destination as! TaxNRCType
            controller.nRCTypesVCDelegate = self
            controller.nrcName = (btnnrc.titleLabel?.text)!
        }
        else if segue.identifier == "IncomeTaxSalaryToConfirmation" {
            let controller = segue.destination as! ConfirmationTax
            controller.dataDict = self.dataDict
        }
    }
}

extension IncomeTaxSalary : UIDocumentMenuDelegate, UIDocumentPickerDelegate {
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let cico = url as URL
        println_debug("The Url is : \(cico)")
        do {
            let imageData = try Data(contentsOf: cico)
            self.uploadXLSFile(filePath : cico.path , fileData : imageData, fileName: cico.lastPathComponent)
        } catch {
            println_debug("loading image file error")
        }
    }
    
    public func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        println_debug(" cancelled by user")
    }
    
    func uploadXLSFile(filePath : String, fileData : Data, fileName: String) {
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            self.validateXlsxFile(filePath : filePath, fileData : fileData, fileName: fileName)
        }
    }
    
    func validateXlsxFile(filePath : String, fileData : Data, fileName: String, _ completionHandler: @escaping (Bool, Any?) -> Void) {
        println_debug("S")
        completionHandler(false,nil)
    }
    
    func validateXlsxFile(filePath : String, fileData : Data, fileName: String) {
//        let url = getUrl(urlStr: "validate/file", serverType: .taxation)
        let urlStr = String.init(format: "http://tax-corporateapi.okdollar.org/validate/file")
        guard let url = URL.init(string: urlStr) else { return }

        println_debug(url)
        
        let request1 = NSMutableURLRequest(url:url)
        request1.httpMethod = "POST";
        
        let boundary1 = "Boundary-\(uuid)"
        request1.setValue("multipart/form-data; boundary=\(boundary1)", forHTTPHeaderField: "Content-Type")
        request1.httpBody = createBodyWithParameters(parameters: nil, filePathKey: filePath, imageDataKey: fileData as Data, boundary: boundary1) as Data
        
        JSONParser.GetApiResponseWithRequest(apiUrlReq: request1 as URLRequest, { (isSuccess, response) in
            guard isSuccess else {
                DispatchQueue.main.async {
                    println_debug("remove progress view called inside cashin main api call with error")
                    progressViewObj.removeProgressView()
                }
                self.showErrorAlert(errMessage: "Try Again".localized)
                return
            }
            DispatchQueue.main.async(execute: {
                if let responseDic = response as? Dictionary<String,Any> {
                    let jsonData = JSON(responseDic)
                    println_debug(jsonData)
                    if jsonData["StatusCode"].stringValue == "200" && jsonData["Status"].boolValue == true {
                        self.dataDict["total_Employees"] = jsonData["Result"]["EmployeeCount"].stringValue
                        self.dataDict["year"] = jsonData["Result"]["Year"].stringValue
                        self.dataDict["total_Amount"] = jsonData["Result"]["TotalTaxAmount"].stringValue
                        self.dataDict["month"] = jsonData["Result"]["Month"].stringValue
                        self.getXlsxFilePathURL(filePath : filePath, fileData : fileData, fileName: fileName)
                        DispatchQueue.main.async {
                            self.isFileUploaded = true
                            self.uploadFileName = fileName
                            self.IncomeTaxTableView.reloadData()
                            self.showSubmitBtn()
                        }
                    }
                    else {
                        self.isFileUploaded = false
                        self.uploadFileName = "Upload File".localized
                        self.IncomeTaxTableView.reloadData()
                        self.hideSubmitBtn()
                        self.showMsgPopupAlert(msg: jsonData["Message"].stringValue, image: #imageLiteral(resourceName: "dashboard_taxpayment"))
                    }
                }
            })
            progressViewObj.removeProgressView()
        })
    }
    
    func getXlsxFilePathURL(filePath : String, fileData : Data, fileName: String) {
        let urlStr = String.init(format: "http://media.api.okdollar.org/phones/IRD/devices/\(uuid)/files")
        let url: URL? = URL.init(string: urlStr)
//        let url = getUrl(urlStr: "phones/\(UserModel.shared.mobileNo)/devices/\(uuid)/files", serverType: .mediaUploadFilesUrl)
        println_debug(url)
        
        let request = NSMutableURLRequest(url:url as! URL)
        request.httpMethod = "POST";
        
        let boundary = "Boundary-\(uuid)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.httpBody = createBodyWithParameters(parameters: nil, filePathKey: filePath, imageDataKey: fileData as Data, boundary: boundary) as Data
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            (data, response, error) in
            if error != nil {
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 201 {
                println_debug("statusCode should be 200, but is \(httpStatus.statusCode)")
                if let httpResponse = response as? HTTPURLResponse {
                    if let xDemAuth = httpResponse.allHeaderFields["Location"] as? String {
                        // use X-Dem-Auth here
                        self.isFileUploaded = true
                        self.uploadFileName = fileName
                        self.dataDict["xlsxFIlePath"] = xDemAuth
                        println_debug("xxxxxxx- \(xDemAuth)")
                        DispatchQueue.main.async {
                            progressViewObj.removeProgressView()
                        }
                    }
                }
            }
            else {
                progressViewObj.removeProgressView()
            }
        }
        task.resume()
    }

    private func showSubmitBtn() {
        submitBtn.isUserInteractionEnabled = true
        submitBtn.backgroundColor = UIColor.colorWithRedValue(redValue: 254, greenValue: 196, blueValue: 0, alpha: 1)
        submitBtn.setTitleColor(UIColor.colorWithRedValue(redValue: 22, greenValue: 45, blueValue: 255, alpha: 1), for: .normal)
    }

    private func hideSubmitBtn() {
        submitBtn.backgroundColor = UIColor.lightGray
        submitBtn.setTitleColor(UIColor.white, for: .normal)
        submitBtn.isUserInteractionEnabled = false
    }
}

extension IncomeTaxSalary : NRCDivisionStateDeletage, TaxNRCTypeDelegate, TextChangeForNRCCellDelegate, MyTextFieldDelegate {
    func textFieldDidDelete() {
        println_debug("Deleteeeee")
        if let cell:NRCDetailsTaxTableViewCell = self.IncomeTaxTableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? NRCDetailsTaxTableViewCell {
            if cell.nRCTextField.text?.count == 0 {
                self.showNRCSuggestion()
            }
        }
    }
    
    func textChangedAtNRCTaxCell(_ cell: NRCDetailsTaxTableViewCell, text: String?) {
        if text == "" {
            cell.nRCTextField.text?.removeLast()
            self.showNRCSuggestion()
        }
        else if text == "000000" {
            cell.nRCTextField.text?.removeLast()
            self.showMsgPopupAlert(msg: "Invalid NRC Number".localized, image: #imageLiteral(resourceName: "Contact Mobile Number"))
        }
    }
    
    func setNrcTypeName(nrcType: String) {
        btnnrc.setTitle(nrcType, for: .normal)
        if let cell:NRCDetailsTaxTableViewCell = self.IncomeTaxTableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? NRCDetailsTaxTableViewCell {
            cell.nRCTextField.becomeFirstResponder()
        }
        //        if (tfNRC.text?.length)! < 6 {
        //            tfNRC.becomeFirstResponder()
        //        }
    }
    
    func setDivisionAndTownship(strDivison_township: String) {
        self.setNRCDetails(nrcDetails: strDivison_township)
    }
    
    func setDivison(division: LocationDetail) {
        println_debug("ssss - \(division)")
    }
    
    private func setNRCDetails(nrcDetails : String) {
        if let cell:NRCDetailsTaxTableViewCell = self.IncomeTaxTableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? NRCDetailsTaxTableViewCell {
            cell.enterNRCLabel.isHidden = true
            cell.arrowImageView.isHidden = true
            cell.nRCTextField.isHidden = false
            cell.nrcClearBtn.isHidden = false
            let myString = nrcDetails
            let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: appFont, size: 16) ?? UIFont.systemFont(ofSize: 16)])
            btnnrc.frame = CGRect(x: 0, y: -2, width:stringSize.width, height: 52)
            btnnrc.setTitleColor(.black, for: .normal)
            btnnrc.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
            btnnrc.addTarget(self, action: #selector(BtnNrcTypeAction(button:)), for: .touchUpInside)
            btnnrc.setTitle(nrcDetails, for: UIControl.State.normal)
            let btnView = UIView()
            btnView.frame = CGRect(x: 0, y: 0, width: btnnrc.frame.width , height: 52)
            btnView.addSubview(btnnrc)
            cell.nRCTextField.leftView = btnView
            cell.nRCTextField.leftViewMode = UITextField.ViewMode.always
            IncomeTaxTableView.beginUpdates()
            IncomeTaxTableView.endUpdates()
            cell.nRCTextField.becomeFirstResponder()
        }
    }
    
    @objc func BtnNrcTypeAction(button : UIButton) {
        self.showNRCSuggestion()
    }
    
    private func showNRCSuggestion() {
        if let cell:NRCDetailsTaxTableViewCell = self.IncomeTaxTableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? NRCDetailsTaxTableViewCell {
            cell.nRCTextField.resignFirstResponder()
            self.performSegue(withIdentifier: "corporateTaxToNRCType", sender: self)
        }
    }
}

extension IncomeTaxSalary : UploadFileSalaryTaxTableViewCellDelegate {
    func passMonth(month: [String], index: Int) {
        self.selectedMonthArr = month
    }
    
    func passYear(year: String, index: Int) {
        self.selectedYear = year
    }
    
//    func passMonthYear(month: [String], year: String, index: Int) {
//        self.selectedMonthArr = month
//        self.selectedYear = year
//        if isFileUploaded {
//            submitBtn.isUserInteractionEnabled = true
//            submitBtn.backgroundColor = UIColor.colorWithRedValue(redValue: 254, greenValue: 196, blueValue: 0, alpha: 1)
//            submitBtn.setTitleColor(UIColor.colorWithRedValue(redValue: 22, greenValue: 45, blueValue: 255, alpha: 1), for: .normal)
//        }
//    }
}
