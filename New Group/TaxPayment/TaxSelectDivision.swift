//
//  TaxSelectDivision.swift
//  OK
//
//  Created by Shobhit Singhal on 9/14/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class TaxSelectDivision: OKBaseController {
    @IBOutlet weak var taxSelectDivisionTableView: UITableView!

    var sectionDivisionArr = [String]()
    var divisionList = [Dictionary<String, String>]()
    var rowsTitleArr = [[Dictionary<String, String>]]()
    var divisionCode = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Select Division".localized
        sectionDivisionArr = ["Select Division"]
        println_debug("kkkkk --> \n \(divisionList)")
        self.GetDivisionListFromServer()
        println_debug("sssss --> \n \(divisionList)")
    }

    override func viewWillAppear(_ animated: Bool) {
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    @IBAction func backBtnAction(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }

    func GetDivisionListFromServer() {
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
        let customURL = URL.init(string: "http://www.okdollar.co/RestService.svc/GetDivisionListForEb")
        let agentNum = UserModel.shared.mobileNo
        let msid = UserModel.shared.msid
        let str = uuid
        let userDict = ["MobileNumber" : agentNum as Any,
                        "Msid" : msid as Any,
                        "Ostype" : 1,
                        "Otp" : "",
                        "Simid ": str] as [String : Any]
        let userData = ["LoginDeviceInfo" : userDict]
        // JSON all the things
        let auth = try? JSONSerialization.data(withJSONObject: userData, options: .prettyPrinted)
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: customURL!)
        do {
            //println_debug("Requesting Division API Now")
            
            // Set the request content type to JSON
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            // The magic...set the HTTP request method to POST
            request.httpMethod = "POST"
            
            // Add the JSON serialized login data to the body
            request.httpBody = auth
            
            // Create the task that will send our login request (asynchronously)
            let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if(data == nil) {
                    //println_debug("")
                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: nil, body: appDel.getlocaLizationLanguage(key: "Please Try Again Later."), img: nil)
                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                            
                        })
                        alertViewObj.showAlert(controller: self)
                        progressViewObj.removeProgressView()
                    }
                } else {
                    do {
                        let jsonResponse = try? JSONSerialization.jsonObject(with: data!, options:.allowFragments)
                        if let jsonResponse = jsonResponse as? [String: Any] {
                            // if let jsonResponse = jsonResponse as? [String: Any] {
                            //println_debug("\n \n \n \(jsonResponse)")
                            
                            if jsonResponse["Msg"] as! String == "Success" {
                                //println_debug(jsonResponse["Data"] as Any)
                                
                                let DataStringToJSON : [String : Any] = self.convertStringToJson(ReceivedString: jsonResponse["Data"]! as! String) as! [String : Any]
                                //println_debug(DataStringToJSON)
                                
                                let resultStringToJSON : AnyObject  = self.convertStringToJson(ReceivedString: DataStringToJSON["result"]! as! String) as AnyObject
                                
                                for element in resultStringToJSON as! Array<AnyObject> {
                                    //println_debug(element)
                                    //println_debug(element["name"])
                                    var DivName = ""
                                    if appDel.getSelectedLanguage() == "en" {
                                        DivName = element["name"]! as! String
                                    } else {
                                        DivName = element["bUnicode"]! as! String
                                    }
                                    let DivCode = element["code"]! as! String
                                    let DivID = element["id"]! as! String
                                    let DivisionInfo : [String: String] = [
                                        "name" : DivName,
                                        "code" : DivCode,
                                        "id" : DivID
                                    ]
                                    self.divisionList.append(DivisionInfo)
                                }
                                self.rowsTitleArr.append(self.divisionList)
                                DispatchQueue.main.async {
                                    progressViewObj.removeProgressView()
                                    self.taxSelectDivisionTableView.delegate = self
                                    self.taxSelectDivisionTableView.dataSource = self
                                    self.taxSelectDivisionTableView.reloadData()
                                }
                            } else {
                                //Show error
                                DispatchQueue.main.async {
                                    alertViewObj.wrapAlert(title: nil, body: appDel.getlocaLizationLanguage(key: "Please Try again in a while."), img: nil)
                                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                    })
                                    alertViewObj.showAlert(controller: self)
                                    progressViewObj.removeProgressView()
                                }
                            }
                        }
                    }
                }
            })
            // Start the task on a background thread
            task.resume()
        }
    }
    
    func convertStringToJson(ReceivedString : String) -> Any {
        let CustomStringData = ReceivedString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion:true)
        do {
            let jsonData = try? JSONSerialization.jsonObject(with: CustomStringData!, options: .allowFragments ) as Any
            return jsonData!
        }
    }
}

extension TaxSelectDivision: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionDivisionArr.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 40))
        headerView.backgroundColor = UIColor.colorWithRedValue(redValue: 244, greenValue: 244, blueValue: 245, alpha: 1)
        let headerLabel = UILabel(frame: CGRect(x: 12, y: 5, width: screenWidth-40, height: 30))
        headerLabel.text = self.sectionDivisionArr[section].localized
        headerLabel.font = UIFont.init(name: appFont, size: 17)
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.rowsTitleArr[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaxSelectDivisionCell", for: indexPath) as! TaxSelectDivisionTableViewCell
        let dict  = rowsTitleArr[indexPath.section][indexPath.row] as Dictionary
        cell.titleLabel.text = "\(String(describing: dict["name"]!))"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict  = rowsTitleArr[indexPath.section][indexPath.row] as Dictionary
        divisionCode = "\(String(describing: dict["code"]!))"
        tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: "textSelectDivisionToSelectTownship", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "textSelectDivisionToSelectTownship" {
            let controller = segue.destination as! TaxSelectTownship
            controller.divisionCode = self.divisionCode
        }
    }
    
}

class TaxSelectDivisionTableViewCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
