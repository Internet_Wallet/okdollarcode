//
//  SelectIRDOffice.swift
//  OK
//
//  Created by Shobhit Singhal on 8/20/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class SelectIRDOffice: UIViewController {
    @IBOutlet weak var iRDOfficesListTableView : UITableView!

    var tokenFullString = ""
    var whichApiCall:String = ""
    var divisionCode:String = ""
    var selectedDivision:String = ""
    var iRDOfficeViewModel:IRDOfficeViewModel!
    var dataDict = Dictionary<String, String>()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Select Tax Office Branch".localized
        iRDOfficesListTableView.estimatedRowHeight = 80
        iRDOfficesListTableView.rowHeight = UITableView.automaticDimension
        getTokenAndHitIRDDetailsAPI()
    }

    func getTokenAndHitIRDDetailsAPI() {
        if appDelegate.checkNetworkAvail() {
            self.view.endEditing(true)
            getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
                guard isSuccess, let _ = tokenString else {
                    self.showErrorAlert(errMessage: "Try Again".localized)
                    return
                }
                DispatchQueue.main.async(execute: {
                    self.whichApiCall = "IRDDetails"
                    self.callingHttp()
                })
            })
        }
        else {
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            alertViewObj.showAlert(controller: UIViewController.init())
        }
    }
    
    
    @IBAction func backBtnAction(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getTokenFromServer(completionHandler: @escaping (Bool,Any?) -> Void) {
        let urlString = String.init(format: Url.TaxPaymentTokenAPIURL)
        let getURL = getUrl(urlStr: urlString, serverType: .taxation) as? URL

        guard let tokenUrl = getURL else {
            showErrorAlert(errMessage: "Something went wrong : Dev need to check")
            return
        }
        let hashValue = Url.aKey_Tax_Payment.hmac_SHA1(key: Url.sKey_Tax_Payment)
        let inputVal = "password=\(hashValue)&grant_type=password"
        
        progressViewObj.showProgressView()
        println_debug("get token from server called........")
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: kMethod_Post, contentType: kContentType_urlencoded, inputVal: inputVal, authStr: nil)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    println_debug("remove progress view called inside get token from server call")
                }
                completionHandler(false,nil)
                return
            }
            println_debug("response dict for get token from server :: \(String(describing: response))")
            let responseDic = response as! NSDictionary
            guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                completionHandler(false,nil)
                return
            }
            let authorizationString =  "\(tokentype) \(accesstoken)"
            self.tokenFullString = authorizationString
            completionHandler(true,authorizationString)
        })
    }
    
    func callingHttp() {
        if self.whichApiCall == "IRDDetails" {
            if appDelegate.checkNetworkAvail() {
                let urlString   = String.init(format: Url.IRD_Details, self.divisionCode)
                let getURL = getUrl(urlStr: urlString, serverType: .taxation)
                println_debug(getURL)
                let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: getURL, methodType: kMethod_Get, contentType: kContentType_Json, inputVal: nil, authStr: self.tokenFullString)
                DispatchQueue.main.async {
                    println_debug("progress view started")
                }
                
                JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
                    guard isSuccess else {
                        DispatchQueue.main.async {
                            println_debug("remove progress view called inside cashin main api call with error")
                            progressViewObj.removeProgressView()
                        }
                        self.showErrorAlert(errMessage: "Try Again".localized)
                        return
                    }
                    DispatchQueue.main.async(execute: {
                        if let responseDic = response as? Dictionary<String,Any> {
                            let jsonData = JSON(responseDic)
                            println_debug(jsonData)
                            if jsonData["StatusCode"].stringValue == "302" && jsonData["Status"].boolValue == true {
                                self.iRDOfficeViewModel = IRDOfficeViewModel.init(data: jsonData)
                                self.iRDOfficesListTableView.delegate = self
                                self.iRDOfficesListTableView.dataSource = self
                                self.iRDOfficesListTableView.reloadData()
                            }
                            else {
                                self.showMsgPopupAlert(msg: jsonData["Message"].stringValue, image: #imageLiteral(resourceName: "dashboard_taxpayment"))
                            }
                        }
                    })
                    progressViewObj.removeProgressView()
                })
            }
            else {
                alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                }
                alertViewObj.showAlert(controller: UIViewController.init())
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "irdOfficeToIncomeTaxSalary" {
            let controller = segue.destination as! IncomeTaxSalary
            controller.dataDict = self.dataDict
        }
    }
}

extension SelectIRDOffice: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 72))
        headerView.backgroundColor = UIColor.colorWithRedValue(redValue: 244, greenValue: 244, blueValue: 245, alpha: 1)
        let headerLabel = UILabel(frame: CGRect(x: 12, y: 10, width: screenWidth-20, height: 25))
        headerLabel.text = "Select Tax Office in -".localized
        headerLabel.font = UIFont.init(name: appFont, size: 16)
        let headerLabel2 = UILabel(frame: CGRect(x: 12, y: 40, width: screenWidth-20, height: 25))
        headerLabel2.text = self.selectedDivision
        headerLabel2.font = UIFont.init(name: appFont, size: 17)
        headerView.addSubview(headerLabel)
        headerView.addSubview(headerLabel2)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 72
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.iRDOfficeViewModel.getIRDOfficeData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IRDOfficeCell", for: indexPath) as! IRDOfficeTableViewCell
        cell.iRDOfficeLabel.text = self.iRDOfficeViewModel.getIRDOfficeData[indexPath.row].irdOfficeName
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.dataDict["IRD_Office"] = self.iRDOfficeViewModel.getIRDOfficeData[indexPath.row].irdOfficeName
        self.dataDict["address_Location"] = self.iRDOfficeViewModel.getIRDOfficeData[indexPath.row].addressLocation
        self.dataDict["IrdDetailId"] = self.iRDOfficeViewModel.getIRDOfficeData[indexPath.row].irdDetailId
        self.dataDict["destinationNumber"] = self.iRDOfficeViewModel.getIRDOfficeData[indexPath.row].destinationNumber
        self.performSegue(withIdentifier: "irdOfficeToIncomeTaxSalary", sender: self)
    }
}

