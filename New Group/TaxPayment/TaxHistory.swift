//
//  TaxHistory.swift
//  OK
//
//  Created by Shobhit Singhal on 8/20/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class TaxHistory: UIViewController {
    @IBOutlet weak var taxHistoryTableView : UITableView!
    @IBOutlet weak var noPaymentLabel : UILabel!

    var tokenFullString = ""
    var whichApiCall:String = ""
    var taxPaymentHistoryViewModel:TaxPaymentHistoryViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Tax History".localized
        taxHistoryTableView.estimatedRowHeight = 290
        taxHistoryTableView.rowHeight = UITableView.automaticDimension
        noPaymentLabel.isHidden = true
        noPaymentLabel.text = "No payment has made yet.".localized
        getTokenAndHitTaxPaymentHistoryAPI()
    }

    @IBAction func backBtnAction(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getTokenAndHitTaxPaymentHistoryAPI() {
        if appDelegate.checkNetworkAvail() {
            self.view.endEditing(true)
            getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
                guard isSuccess, let _ = tokenString else {
                    self.showErrorAlert(errMessage: "Try Again".localized)
                    return
                }
                DispatchQueue.main.async(execute: {
                    self.whichApiCall = "TaxPaymentHistory"
                    self.callingHttp()
                })
            })
        }
        else {
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            alertViewObj.showAlert(controller: UIViewController.init())
        }
    }
    
    func getTokenFromServer(completionHandler: @escaping (Bool,Any?) -> Void) {
        let urlString = String.init(format: Url.TaxPaymentTokenAPIURL)
        let getURL = getUrl(urlStr: urlString, serverType: .taxation) as? URL
        
        guard let tokenUrl = getURL else {
            showErrorAlert(errMessage: "Something went wrong : Dev need to check")
            return
        }
        let hashValue = Url.aKey_Tax_Payment.hmac_SHA1(key: Url.sKey_Tax_Payment)
        let inputVal = "password=\(hashValue)&grant_type=password"
        
        progressViewObj.showProgressView()
        println_debug("get token from server called........")
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: kMethod_Post, contentType: kContentType_urlencoded, inputVal: inputVal, authStr: nil)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    println_debug("remove progress view called inside get token from server call")
                }
                completionHandler(false,nil)
                return
            }
            println_debug("response dict for get token from server :: \(String(describing: response))")
            let responseDic = response as! NSDictionary
            guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                completionHandler(false,nil)
                return
            }
            let authorizationString =  "\(tokentype) \(accesstoken)"
            self.tokenFullString = authorizationString
            completionHandler(true,authorizationString)
        })
    }
    
    func callingHttp() {
        if self.whichApiCall == "TaxPaymentHistory" {
            if appDelegate.checkNetworkAvail() {
                let urlString   = String.init(format: Url.Tax_Payment_History, UserModel.shared.mobileNo)
                let getURL = getUrl(urlStr: urlString, serverType: .taxation)
                println_debug(getURL)

                let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: getURL, methodType: kMethod_Get, contentType: kContentType_Json, inputVal: nil, authStr: self.tokenFullString)
                DispatchQueue.main.async {
                    println_debug("progress view started")
                }
                
                JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
                    guard isSuccess else {
                        DispatchQueue.main.async {
                            println_debug("remove progress view called inside cashin main api call with error")
                            progressViewObj.removeProgressView()
                        }
                        self.showErrorAlert(errMessage: "Try Again".localized)
                        return
                    }
                    DispatchQueue.main.async(execute: {
                        if let responseDic = response as? Dictionary<String,Any> {
                            let jsonData = JSON(responseDic)
                            println_debug(jsonData)
                            if jsonData["StatusCode"].stringValue == "302" && jsonData["Status"].boolValue == true {
                                self.taxPaymentHistoryViewModel = TaxPaymentHistoryViewModel.init(data: jsonData)
                                self.taxHistoryTableView.delegate = self
                                self.taxHistoryTableView.dataSource = self
                                self.taxHistoryTableView.reloadData()
                            }
                            else {
                                self.taxHistoryTableView.isHidden = true
                                self.noPaymentLabel.isHidden = false
//                                self.showMsgPopupAlert(msg: jsonData["Message"].stringValue, image: #imageLiteral(resourceName: "alert-icon"))
                            }
                        }
                    })
                    progressViewObj.removeProgressView()
                })
            }
            else {
                alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                }
                alertViewObj.showAlert(controller: UIViewController.init())
            }
        }
    }
}

extension TaxHistory: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taxPaymentHistoryViewModel.getTaxPaymentHistoryData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaxHistoryCell", for: indexPath) as! TaxHistoryTableViewCell
        cell.nameLabelValue.text = taxPaymentHistoryViewModel.getTaxPaymentHistoryData[indexPath.row].userName
        cell.nrcNumberLabelValue.text = taxPaymentHistoryViewModel.getTaxPaymentHistoryData[indexPath.row].nrcNumber
        cell.incomeTaxMonthYearLabelValue.text = taxPaymentHistoryViewModel.getTaxPaymentHistoryData[indexPath.row].incomeTaxMonthYear
        cell.emailIdLabelValue.text = taxPaymentHistoryViewModel.getTaxPaymentHistoryData[indexPath.row].emailID
        cell.totalNumberOfEmployeesLabelValue.text = taxPaymentHistoryViewModel.getTaxPaymentHistoryData[indexPath.row].totalEmployeeNumber
        cell.totalAmountLabelValue.text = taxPaymentHistoryViewModel.getTaxPaymentHistoryData[indexPath.row].totalAmount
        cell.irdOfficeLabelValue.text = taxPaymentHistoryViewModel.getTaxPaymentHistoryData[indexPath.row].irdOfficeName
        let dateCustom = self.getDate(timeStr: taxPaymentHistoryViewModel.getTaxPaymentHistoryData[indexPath.row].createdDate)
        cell.dateTimeLabelValue.text = "\(dateCustom.day), \(dateCustom.date)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    private func getDate(timeStr: String) -> (date: String, day: String) {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        //dateFormatter.timeZone = TimeZone.current
        guard let time = dateFormatter.date(from: timeStr) else { return ("", "") }
//        time = dateFormatter.date(from: timeStr)
        dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm:ss"
        let date = dateFormatter.string(from: time)
        let weekDay = self.dayOfWeek(date: time)
        return (date, weekDay!)
    }
    
    private func dayOfWeek(date: Date) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E"
        return dateFormatter.string(from: date).capitalized
    }
}

class TaxHistoryTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabelValue : UILabel!
    @IBOutlet weak var nrcNumberLabelValue : UILabel!
    @IBOutlet weak var incomeTaxMonthYearLabelValue : UILabel!
    @IBOutlet weak var emailIdLabelValue : UILabel!
    @IBOutlet weak var totalNumberOfEmployeesLabelValue : UILabel!
    @IBOutlet weak var totalAmountLabelValue : UILabel!
    @IBOutlet weak var irdOfficeLabelValue : UILabel!
    @IBOutlet weak var dateTimeLabelValue : UILabel!

    @IBOutlet weak var nameLabel: UILabel!{
        didSet {
            nameLabel.text = nameLabel.text?.localized
        }
    }
    @IBOutlet weak var nrcNumberLabel: UILabel!{
        didSet {
            nrcNumberLabel.text = nrcNumberLabel.text?.localized
        }
    }
    @IBOutlet weak var incomeTaxMonthYearLabel: UILabel!{
        didSet {
            incomeTaxMonthYearLabel.text = incomeTaxMonthYearLabel.text?.localized
        }
    }
    @IBOutlet weak var emailIdLabel: UILabel!{
        didSet {
            emailIdLabel.text = emailIdLabel.text?.localized
        }
    }
    @IBOutlet weak var totalNumberOfEmployeesLabel: UILabel!{
        didSet {
            totalNumberOfEmployeesLabel.text = totalNumberOfEmployeesLabel.text?.localized
        }
    }
    @IBOutlet weak var totalAmountLabel: UILabel!{
        didSet {
            totalAmountLabel.text = totalAmountLabel.text?.localized
        }
    }
    @IBOutlet weak var irdOfficeLabel: UILabel!{
        didSet {
            irdOfficeLabel.text = irdOfficeLabel.text?.localized
        }
    }
    @IBOutlet weak var dateTimeLabel: UILabel!{
        didSet {
            dateTimeLabel.text = dateTimeLabel.text?.localized
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        irdOfficeLabelValue.text = "sjdbns ahdb ahd d ah ad ajd ac bac ac ajhfve qwen cwejhdb ejwd qc ebjch ehcv evc hec hewbcewc ewc ehc ewcwhjec hwjec w"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}

