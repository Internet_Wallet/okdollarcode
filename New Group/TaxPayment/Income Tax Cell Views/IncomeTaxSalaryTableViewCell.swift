//
//  IncomeTaxSalaryTableViewCell.swift
//  OK
//
//  Created by Shobhit Singhal on 1/28/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation

protocol UploadFileSalaryTaxTableViewCellDelegate : class {
    func passMonth(month: [String], index:Int)
    func passYear(year: String, index:Int)
}


class SalaryTaxUserNameCell: UITableViewCell {
    @IBOutlet weak var userNameTF: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        userNameTF.text = UserModel.shared.name
    }
}


class SalaryTaxNRCDetailCell: UITableViewCell {
    @IBOutlet weak var nrcDetailTF: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        nrcDetailTF.text = UserModel.shared.nrc
    }
    
}


class UploadFileTableViewCell: UITableViewCell, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    @IBOutlet var uploadFileLabel: UILabel!
    @IBOutlet var downloadSampleFileLabel: UILabel!
    @IBOutlet weak var monthTF: UITextField!
    @IBOutlet weak var yearTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var uploadFileBtn: UIButton!
    @IBOutlet weak var downloadSampleFileBtn: UIButton!
    @IBOutlet weak var editEmailBtn: UIButton!

    let pickerView1 = UIPickerView()
    let pickerView2 = UIPickerView()
    var monthArr = [String]()
    var monthArr2 = [String]()
    var monthArr3 = [String]()

    var yearArr = [String]()
    var delegate : UploadFileSalaryTaxTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        monthArr = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
        monthArr2 = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        monthArr3 = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
        yearArr = ["2018", "2019"]
        let emailArr = UserModel.shared.email.components(separatedBy: ",")
        emailTF.text = emailArr[0]
        downloadSampleFileLabel.text = "Download Sample File".localized
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.emailTF.isUserInteractionEnabled = false
        self.emailTF.textColor = UIColor.darkGray
    }
    
    func wrapData(title: String){
        self.uploadFileLabel.text = title
    }
    
    @IBAction func editEmailBtnAction(_ sender: UIButton) {
        self.emailTF.isUserInteractionEnabled = true
        self.emailTF.textColor = UIColor.black
        self.emailTF.becomeFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == monthTF {
            monthTF.inputView = pickerView1
            self.pickerView1.delegate = self
            monthTF.text = monthArr[0]
            var monthData = [String]()
            monthData.append(monthArr[0])
            monthData.append(monthArr2[0])
            monthData.append(monthArr3[0])
            self.delegate?.passMonth(month: monthData, index: 0)
            return true
        }
        else if textField == yearTF {
            yearTF.inputView = pickerView2
            self.pickerView2.delegate = self
            yearTF.text = yearArr[0]
            self.delegate?.passYear(year: yearTF.text ?? "", index: 0)
            return true
        }
        return true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerView1 {
            return monthArr.count
        }
        return yearArr.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pickerView1 {
            return monthArr[row]
        }
        return yearArr[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerView1 {
            monthTF.text = monthArr[row]
            var monthData = [String]()
            monthData.append(monthArr[row])
            monthData.append(monthArr2[row])
            monthData.append(monthArr3[row])
            self.delegate?.passMonth(month: monthData, index: row)
        } else {
            yearTF.text = yearArr[row]
            self.delegate?.passYear(year: yearTF.text ?? "", index: row)
        }
    }
    
}
