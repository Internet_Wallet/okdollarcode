//
//  TaxAccountXLSFileTableViewCell.swift
//  OK
//
//  Created by Shobhit Singhal on 8/21/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class TaxAccountXLSFileTableViewCell: UITableViewCell {
    @IBOutlet var totalEmployeesLabel: UILabel!
    @IBOutlet var totalAmountLabel: UILabel!
    @IBOutlet var totalEmployeesLblCount: UILabel!
    @IBOutlet var totalAmountLblCount: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func wrapData(totalEmployees: String, totalAmount: String){
//        self.totalEmployeesLabel.text = "Total Number Of Employee(s)".localized + "  \(totalEmployees)"
//        let nsRange = NSString(string: self.totalEmployeesLabel.text!).range(of: totalEmployees, options: String.CompareOptions.caseInsensitive)
//        let myMutableString = NSMutableAttributedString(string: self.totalEmployeesLabel.text!, attributes: [NSAttributedString.Key.font:  UIFont(name: appFont, size: 15)!])
//        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: nsRange)
//        self.totalEmployeesLabel.attributedText = myMutableString
//
//        self.totalAmountLabel.text = "Total Amount".localized + "  \(totalAmount)"
//        let nsRange2 = NSString(string: self.totalAmountLabel.text!).range(of: totalAmount, options: String.CompareOptions.caseInsensitive)
//        let myMutableString2 = NSMutableAttributedString(string: self.totalAmountLabel.text!, attributes: [NSAttributedString.Key.font:  UIFont(name: appFont, size: 15)!])
//        myMutableString2.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: nsRange2)
//        self.totalAmountLabel.attributedText = myMutableString2
        
        self.totalEmployeesLabel.text = "Total Number Of Employee(s)".localized
        self.totalAmountLabel.text = "Total Amount".localized
        self.totalEmployeesLblCount.text = totalEmployees
        self.totalAmountLblCount.text = totalAmount

        
    }

}
