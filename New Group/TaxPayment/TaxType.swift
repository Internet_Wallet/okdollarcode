//
//  TaxType.swift
//  OK
//
//  Created by Shobhit Singhal on 8/20/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class TaxType: UIViewController {
    @IBOutlet weak var taxTableView : UITableView!
    @IBOutlet var taxHistoryCardView: CardDesignView!
    
    var sectionTaxArr = [String]()
    var rowsTitleArr = [JSON]()
    var tokenFullString = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Tax".localized
        self.initData()
        getTokenAndHitTaxAccountTypesAPI()

    }

    override func viewWillAppear(_ animated: Bool) {
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    func initData() {
        sectionTaxArr = ["Select Tax Type"]
    }
    
    @IBAction func backBtnAction(_ sender: Any){
//        self.dismiss(animated: true, completion: nil)
        performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
    }
    
    @IBAction func taxHistoryBtnAction(_ sender: Any) {
        self.performSegue(withIdentifier: "taxTypeToTaxHistorySegue", sender: self)
    }
    
    func getTokenAndHitTaxAccountTypesAPI() {
        if appDelegate.checkNetworkAvail() {
            self.view.endEditing(true)
            getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
                guard isSuccess, let _ = tokenString else {
                    self.showErrorAlert(errMessage: "Try Again".localized)
                    return
                }
                DispatchQueue.main.async(execute: {
                    self.callingHttp()
                })
            })
        }
        else {
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            alertViewObj.showAlert(controller: UIViewController.init())
        }
    }
    
    func getTokenFromServer(completionHandler: @escaping (Bool,Any?) -> Void) {
        let urlString = String.init(format: Url.TaxPaymentTokenAPIURL)
        let getURL = getUrl(urlStr: urlString, serverType: .taxation) as? URL
        
        guard let tokenUrl = getURL else {
            showErrorAlert(errMessage: "Something went wrong : Dev need to check")
            return
        }
        let hashValue = Url.aKey_Tax_Payment.hmac_SHA1(key: Url.sKey_Tax_Payment)
        let inputVal = "password=\(hashValue)&grant_type=password"
        
        progressViewObj.showProgressView()
        println_debug("get token from server called........")
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: kMethod_Post, contentType: kContentType_urlencoded, inputVal: inputVal, authStr: nil)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    println_debug("remove progress view called inside get token from server call")
                }
                completionHandler(false,nil)
                return
            }
            println_debug("response dict for get token from server :: \(String(describing: response))")
            let responseDic = response as! NSDictionary
            guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                completionHandler(false,nil)
                return
            }
            let authorizationString =  "\(tokentype) \(accesstoken)"
            self.tokenFullString = authorizationString
            completionHandler(true,authorizationString)
        })
    }
    
    func callingHttp() {
        if appDelegate.checkNetworkAvail() {
            let urlString   = String.init(format: Url.taxAccountTypes)
            let getURL = getUrl(urlStr: urlString, serverType: .taxation)
            println_debug(getURL)
            let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: getURL, methodType: kMethod_Get, contentType: kContentType_Json, inputVal: nil, authStr: self.tokenFullString)
            DispatchQueue.main.async {
                println_debug("progress view started")
            }
            
            JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
                guard isSuccess else {
                    DispatchQueue.main.async {
                        println_debug("remove progress view called inside cashin main api call with error")
                        progressViewObj.removeProgressView()
                    }
                    self.showErrorAlert(errMessage: "Try Again".localized)
                    return
                }
                DispatchQueue.main.async(execute: {
                    if let responseDic = response as? Dictionary<String,Any> {
                        let jsonData = JSON(responseDic)
                        println_debug(jsonData)
                        if jsonData["StatusCode"].stringValue == "302" && jsonData["Status"].boolValue == true {
                            let result = jsonData["Results"].arrayValue
                            self.rowsTitleArr = result
                            self.taxTableView.delegate = self
                            self.taxTableView.dataSource = self
                            self.taxTableView.reloadData()
                        }
                        else {
                            let currentStr = UserDefaults.standard.string(forKey: "currentLanguage")
                            var displayMsg = ""
                            if currentStr == "my" {
                                displayMsg = jsonData["Message"].stringValue
                            } else {
                                displayMsg = jsonData["Message"].stringValue
                            }
                            //                                self.showMsgPopupAlert(msg: displayMsg, image: #imageLiteral(resourceName: "dashboard_taxpayment"))
                            alertViewObj.wrapAlert(title: "".localized, body:displayMsg, img: #imageLiteral(resourceName: "tax_Logo"))
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                //                                    self.parent?.parent?.dismiss(animated: true, completion: nil)
//                                self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                            }
                            alertViewObj.showAlert(controller: UIViewController.init())
                        }
                    }
                })
                progressViewObj.removeProgressView()
            })
        }
        else {
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            alertViewObj.showAlert(controller: UIViewController.init())
        }
    }
    
}

extension TaxType: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 40))
        headerView.backgroundColor = UIColor.colorWithRedValue(redValue: 244, greenValue: 244, blueValue: 245, alpha: 1)
        let headerLabel = UILabel(frame: CGRect(x: 12, y: 5, width: screenWidth-40, height: 30))
        headerLabel.text = "Select Tax Type".localized
        headerLabel.font = UIFont.init(name: appFont, size: 17)
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.rowsTitleArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaxCell", for: indexPath) as! TaxTypeTableViewCell
        let title  = rowsTitleArr[indexPath.row]
        cell.wrapData(title: title)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 {
            self.performSegue(withIdentifier: "taxToSelectTaxDivisionState", sender: self)
        } else {
            DispatchQueue.main.async(){
                alertViewObj.wrapAlert(title: "", body: "Coming Soon".localized, img: #imageLiteral(resourceName: "dashboard_taxpayment"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel , action: {})
                alertViewObj.showAlert(controller: self)
                progressViewObj.removeProgressView()
            }
        }
    }
}

class TaxTypeTableViewCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func wrapData(title: JSON){
        self.titleLabel.text = title["AccountTypeName"].stringValue
    }
}
