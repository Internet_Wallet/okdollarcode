//
//  TaxInvoice.swift
//  OK
//
//  Created by Shobhit Singhal on 1/29/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class TaxInvoice: UIViewController {
    @IBOutlet weak var taxInvoiceView: UIView!
    @IBOutlet weak var taxInvoiceShareView: UIView!
    @IBOutlet weak var amountPaidView: UIView!

    @IBOutlet weak var senderAccNameLabelValue: UILabel!
    @IBOutlet weak var senderAccNumberLabelValue: UILabel!
    @IBOutlet weak var incomeTaxMonthYearLabelValue : UILabel!
    @IBOutlet weak var totalNumberOfEmployeesLabelValue : UILabel!
    @IBOutlet weak var payToLabelValue: UILabel!
    @IBOutlet weak var categoriesLabelValue: UILabel!
    @IBOutlet weak var transactionIdLabelValue: UILabel!
    @IBOutlet weak var transactionTypeLabelValue: UILabel!
    @IBOutlet weak var dateTimeLabelValue: UILabel!
    @IBOutlet weak var totalAmountPaidLabelValue: UILabel!

    var dataDict = Dictionary<String, String>()

    @IBOutlet var taxTransactionDetailsReceiptLabel: UILabel!{
        didSet
        {
            taxTransactionDetailsReceiptLabel.text = taxTransactionDetailsReceiptLabel.text?.localized
        }
    }
    @IBOutlet var senderAccNameLabel: UILabel!{
        didSet
        {
            senderAccNameLabel.text = senderAccNameLabel.text?.localized
        }
    }
    @IBOutlet var senderAccNumberLabel: UILabel!{
        didSet
        {
            senderAccNumberLabel.text = senderAccNumberLabel.text?.localized
        }
    }
    @IBOutlet var incomeTaxMonthYearLabel: UILabel!{
        didSet
        {
            incomeTaxMonthYearLabel.text = incomeTaxMonthYearLabel.text?.localized
        }
    }
    @IBOutlet var totalNumberOfEmployeesLabel: UILabel!{
        didSet
        {
            totalNumberOfEmployeesLabel.text = totalNumberOfEmployeesLabel.text?.localized
        }
    }
    @IBOutlet var payToLabel: UILabel!{
        didSet
        {
            payToLabel.text = payToLabel.text?.localized
        }
    }
    @IBOutlet var categoriesLabel: UILabel!{
        didSet
        {
            categoriesLabel.text = categoriesLabel.text?.localized
        }
    }
    @IBOutlet var transactionIdLabel: UILabel!{
        didSet
        {
            transactionIdLabel.text = transactionIdLabel.text?.localized
        }
    }
    @IBOutlet var transactionTypeLabel: UILabel!{
        didSet
        {
            transactionTypeLabel.text = transactionTypeLabel.text?.localized
        }
    }
    @IBOutlet var dateTimeLabel: UILabel!{
        didSet
        {
            dateTimeLabel.text = dateTimeLabel.text?.localized
        }
    }
    @IBOutlet var totalAmountPaidLabel: UILabel!{
        didSet
        {
            totalAmountPaidLabel.text = totalAmountPaidLabel.text?.localized
        }
    }
    @IBOutlet var shareBtn: UIButton!{
        didSet{
            shareBtn.setTitle(shareBtn.titleLabel?.text?.localized, for: .normal)
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Invoice Receipt".localized
        amountPaidView.layer.borderWidth = 1
        amountPaidView.layer.borderColor = UIColor.colorWithRedValue(redValue: 60, greenValue: 60, blueValue: 60, alpha: 1).cgColor
        self.initLoad()
    }
    
    func initLoad() {
        senderAccNameLabelValue.text = (UserModel.shared.name.replacingOccurrences(of: ",", with: ". "))
        senderAccNumberLabelValue.text = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "(+95)0")
        incomeTaxMonthYearLabelValue.text = "\(self.dataDict["month"] ?? "") \(self.dataDict["year"] ?? "")"
        totalNumberOfEmployeesLabelValue.text = dataDict["total_Employees"]
        payToLabelValue.text = "IRD Office \n \(dataDict["IRD_Office"] ?? "")"
        categoriesLabelValue.text = "Salary Tax".localized
        transactionIdLabelValue.text = dataDict["transactionId"]
        transactionTypeLabelValue.text = "Tax Payment".localized
        dateTimeLabelValue.text = "\(dataDict["date"] ?? "") \n \(dataDict["time"] ?? "")"
        totalAmountPaidLabelValue.text = dataDict["total_Amount"]
    }

    @IBAction func dismissControllerAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func sharePdfAction(_ sender: UIButton) {
        guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: taxInvoiceShareView, pdfFile: "OK$ Tax-Transaction Receipt") else {
            println_debug("Error - pdf not generated")
            return
        }
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
        
//        let pdfUrl = self.createPdfFromView(aView: taxInvoiceShareView, saveToDocumentsWithFileName: "Tax Transaction Receipt".localized)
//        let activityViewController = UIActivityViewController(activityItems: [pdfUrl!], applicationActivities: nil)
//        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func createPdfFromView(aView: UIView, saveToDocumentsWithFileName pdfFileName: String) -> URL? {
        let render = UIPrintPageRenderer()
        let page = CGRect(x: 0, y: 0, width: 595.2, height: 841)
        let printable = page.insetBy(dx: 0, dy: 0)
        render.setValue(NSValue(cgRect: page), forKey: "paperRect")
        render.setValue(NSValue(cgRect: printable), forKey: "printableRect")
        let priorBounds = aView.bounds
        let fittedSize = aView.sizeThatFits(CGSize(width:priorBounds.size.width, height:aView.bounds.size.height))
        aView.bounds = CGRect(x:0, y:0, width:fittedSize.width, height:fittedSize.height)
        let pdfPageBounds = CGRect(x:0, y:0, width:aView.frame.width, height:self.view.frame.height)
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds,nil)
        let pageOriginY: CGFloat = 0
        UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
        UIGraphicsGetCurrentContext()!.saveGState()
        UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
        aView.layer.render(in: UIGraphicsGetCurrentContext()!)
        UIGraphicsGetCurrentContext()!.restoreGState()
        UIGraphicsEndPDFContext()
        aView.bounds = priorBounds
        
        guard let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else  { return nil}
        let properPDFFileName = pdfFileName.replacingOccurrences(of: ".pdf", with: "") + ".pdf"
        let pdfUrl = documentDirectories + "/" + properPDFFileName
        //Delete file if alreay exists
        do {
            try FileManager.default.removeItem(atPath: pdfUrl)
        } catch let error as NSError {
            println_debug("Error: \(error.domain)")
        }
        //Write file
        if pdfData.write(toFile: pdfUrl, atomically: true) {
            println_debug("Successfully wrote to \(pdfUrl)")
            
            let pathUrl = URL(fileURLWithPath: documentDirectories)
            let fileUrl = pathUrl.appendingPathComponent(properPDFFileName)
            return fileUrl
        }
        return nil
    }
}
