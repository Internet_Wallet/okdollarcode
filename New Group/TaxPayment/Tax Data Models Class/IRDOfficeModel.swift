//
//  IRDOfficeModel.swift
//  OK
//
//  Created by Shobhit Singhal on 8/24/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class IRDOfficeModel: NSObject {
    var irdOfficeName:String = ""
    var irdOfficeBurmeseName:String = ""
    var addressLocation:String = ""
    var irdDetailId:String = ""
    var destinationNumber:String = ""

    init(data: JSON) {
//        self.irdOfficeName = "\(data["OfficeName"].stringValue) \(data["Division"]["Name"].stringValue) \(data["Township"]["Name"].stringValue)"
//        self.irdOfficeBurmeseName = "\(data["OfficeName"].stringValue) \(data["Division"]["BName"].stringValue) \(data["Township"]["BurmeseName"].stringValue)"
        self.addressLocation = "\(data["Division"]["Name"].stringValue), \(data["Township"]["Name"].stringValue)"
        
        self.irdOfficeName = "\(data["OfficeName"].stringValue)"
        self.irdOfficeBurmeseName = "\(data["OfficeBurmeseName"].stringValue)"
        self.irdDetailId = "\(data["IrdDetailId"].stringValue)"
        self.destinationNumber = "\(data["ContactNumber"].stringValue)"
    }
}


class IRDOfficeViewModel {
    var iRDOfficeModel = [IRDOfficeModel]()
    init(data:JSON) {
        if let arrayData = data["Results"].array {
            iRDOfficeModel =  arrayData.map({(value) -> IRDOfficeModel in
                return  IRDOfficeModel(data:value)
            })
        }
    }
    
    var getIRDOfficeData:Array<IRDOfficeModel>{
        return iRDOfficeModel
    }
}
