//
//  TaxPaymentHistoryModel.swift
//  OK
//
//  Created by Shobhit Singhal on 8/25/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class TaxPaymentHistoryModel: NSObject {
    var userName:String = ""
    var nrcNumber:String = ""
    var incomeTaxMonthYear:String = ""
    var emailID:String = ""
    var totalEmployeeNumber:String = ""
    var totalAmount:String = ""
    var irdOfficeName:String = ""
    var createdDate:String = ""

    init(data: JSON) {
        
        
        self.userName = data["EmployeeName"].stringValue
        self.nrcNumber = data["NrcNumber"].stringValue
        self.incomeTaxMonthYear = "\(data["Month"].stringValue) \(data["Year"].stringValue)"
        self.emailID = data["Email"].stringValue
        self.totalEmployeeNumber = data["TotalEmployeeNumber"].stringValue
        self.totalAmount = data["TotalAmount"].stringValue
        self.irdOfficeName = "\(data["IrdDetail"]["OfficeName"].stringValue) \(data["IrdDetail"]["Division"]["Name"].stringValue) \(data["IrdDetail"]["Township"]["Name"].stringValue)"
        self.createdDate = data["IrdDetail"]["CreatedDate"].stringValue
    }
}


class TaxPaymentHistoryViewModel {
    var taxPaymentHistoryModel = [TaxPaymentHistoryModel]()
    init(data:JSON) {
        if let arrayData = data["Results"].array {
            taxPaymentHistoryModel =  arrayData.map({(value) -> TaxPaymentHistoryModel in
                return  TaxPaymentHistoryModel(data:value)
            })
        }
    }
    
    var getTaxPaymentHistoryData:Array<TaxPaymentHistoryModel>{
        return taxPaymentHistoryModel
    }
}
