//
//  SelectTaxDivisionState.swift
//  OK
//
//  Created by Shobhit Singhal on 8/20/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class SelectTaxDivisionState: UIViewController {
    @IBOutlet weak var taxInfoTableView : UITableView!

    var arr_city = [LocationDetail]()
    var divisionCode:String = ""
    var selectedDivision:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setMarqueLabelInNavigation()
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 18)!]
//        self.title = "Select Tax Office Division/State".localized
        arr_city = TownshipManager.allLocationsList
        arr_city = TownshipManager.allLocationsList.sorted(by: { $0.stateOrDivitionNameEn < $1.stateOrDivitionNameEn} )
        taxInfoTableView.reloadData()
    }

    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 10, width: 200, height: 25)
        lblMarque.font =  UIFont(name: appFont, size: 18)
        lblMarque.text = "Select Tax Office Division / State".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
    }
    
    @IBAction func backBtnAction(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func myUnwindForTaxAction(segue: UIStoryboardSegue) {}
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "selectTaxDivisionToIrdOffice" {
            let controller = segue.destination as! SelectIRDOffice
            controller.divisionCode = self.divisionCode
            controller.selectedDivision = self.selectedDivision
        }
    }
    
}

extension SelectTaxDivisionState: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 40))
        headerView.backgroundColor = UIColor.colorWithRedValue(redValue: 244, greenValue: 244, blueValue: 245, alpha: 1)
        let headerLabel = UILabel(frame: CGRect(x: 12, y: 5, width: screenWidth-40, height: 30))
        headerLabel.text = "Select Tax Office Branch".localized
        headerLabel.font = UIFont.init(name: appFont, size: 17)
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arr_city.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaxInfoCell", for: indexPath) as! TaxInfoTableViewCell
        
        let dic = arr_city[indexPath.row]
        cell.wrapData(title:dic.stateOrDivitionNameEn)

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let dic = arr_city[indexPath.row]
        self.divisionCode = dic.stateOrDivitionCode
        self.selectedDivision = dic.stateOrDivitionNameEn
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectIRDOffice") as! SelectIRDOffice
//        vc.divisionCode = dic.stateOrDivitionCode
        self.performSegue(withIdentifier: "selectTaxDivisionToIrdOffice", sender: self)
    }
}

class TaxInfoTableViewCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func wrapData(title: String){
        self.titleLabel.text = title
    }
}


extension SelectTaxDivisionState : StateDivisionVCDelegate {
    func setDivisionStateName(SelectedDic: LocationDetail) {
        println_debug("setDivisionStateName")
    }
    
    func setTownshipCityNameFromDivison(Dic: TownShipDetailForAddress) {
        println_debug("setTownshipCityNameFromDivison")
    }
}
