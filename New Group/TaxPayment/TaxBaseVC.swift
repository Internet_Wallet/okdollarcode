//
//  TaxBaseVC.swift
//  OK
//
//  Created by Shobhit Singhal on 2/1/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class TaxBaseVC: OKBaseController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> NSData {
        var body = Data()
        if parameters != nil {
            for (key, value) in parameters! {
                body.append(Data("--\(boundary)\r\n".utf8))
                body.append(Data("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
                body.append(Data("\(value)\r\n".utf8))
            }
        }
        
        var filename = ""
        if filePathKey != "" {
            let fileArray = filePathKey?.components(separatedBy: "/")
            filename = (fileArray?.last)!
        }
        
        let mimetype = "multipart/form-data"
        
        body.append(Data("--\(boundary)\r\n".utf8))
        body.append(Data("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n".utf8))
        body.append(Data("Content-Type: \(mimetype)\r\n\r\n".utf8))
        body.append(imageDataKey as Data)
        body.append(Data("\r\n".utf8))
        body.append(Data("--\(boundary)--\r\n".utf8))
        return body as NSData
    }
    
    func currentDate() -> String{
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        let currentDateString: String = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "HH:mm:ss"
//        let someDateTime: String = dateFormatter.string(from: date)
        return (currentDateString)
    }
    
    func currentDate2() -> String{
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let currentDateString: String = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "HH:mm:ss"
        //        let someDateTime: String = dateFormatter.string(from: date)
        return (currentDateString)
    }
}
