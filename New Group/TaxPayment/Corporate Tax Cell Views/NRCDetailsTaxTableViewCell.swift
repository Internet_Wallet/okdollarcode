//
//  NRCDetailsTaxTableViewCell.swift
//  OK
//
//  Created by Shobhit Singhal on 8/22/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol TextChangeForNRCCellDelegate {
    func textChangedAtNRCTaxCell(_ cell: NRCDetailsTaxTableViewCell, text: String?)
}

class NRCDetailsTaxTableViewCell: UITableViewCell {
    @IBOutlet weak var nRCTextField: MyTextField!
    @IBOutlet weak var enterNRCLabel: UILabel!
    @IBOutlet weak var nrcClearBtn: UIButton!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    var delegate: TextChangeForNRCCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        nRCTextField.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    @IBAction func nrcClearAction(_ sender: Any) {
        nRCTextField.leftView = nil
        nRCTextField.resignFirstResponder()
        nRCTextField.text = ""
        nrcClearBtn.isHidden = true
        nRCTextField.isHidden = true
        arrowImageView.isHidden = false
        enterNRCLabel.isHidden = false
    }
}

extension NRCDetailsTaxTableViewCell : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let nsString = textField.text as NSString?
        let newString = nsString?.replacingCharacters(in: range, with: string)
        let length = textField.text!.count + string.count - range.length
        let NRCCHARSET = "1234567890"
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: NRCCHARSET).inverted).joined(separator: "")) { return false }
        if newString == "" {
            self.delegate?.textChangedAtNRCTaxCell(self, text: newString)
        }
//        else if newString == "" && string.isEmpty {
//            self.delegate?.textChangedAtNRCTaxCell(self, text: newString)
//        }
        else if newString == "000000" {
            self.delegate?.textChangedAtNRCTaxCell(self, text: newString)
        }
        if length > 6 {
            nRCTextField.resignFirstResponder()
            return false
        }
        return true
    }
}
