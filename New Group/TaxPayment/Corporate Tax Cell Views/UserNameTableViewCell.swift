//
//  UserNameTableViewCell.swift
//  OK
//
//  Created by Shobhit Singhal on 8/21/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class UserNameTableViewCell: UITableViewCell {
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var viewMaleFemale: UIView!
    @IBOutlet weak var viewMalePrefix: UIView!
    @IBOutlet weak var viewFemalePrifix: UIView!
    @IBOutlet weak var btnClearUserName: UIButton!
    @IBOutlet weak var topConstant: NSLayoutConstraint!
    
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnU: UIButton!
    @IBOutlet weak var btnMg: UIButton!
    @IBOutlet weak var btnMr: UIButton!
    @IBOutlet weak var btnDr: UIButton!
    @IBOutlet weak var btnDaw: UIButton!
    @IBOutlet weak var btnMa: UIButton!
    @IBOutlet weak var btnMs: UIButton!
    @IBOutlet weak var btnMrs: UIButton!
    @IBOutlet weak var btnMDr: UIButton!
    
    @IBOutlet weak var btnBFirst: UIButton!
    @IBOutlet weak var btnBSecond: UIButton!
    @IBOutlet weak var btnBThired: UIButton!
    @IBOutlet weak var viewBMaleFemaleCategory: UIView!
    
    var isYourNameKeyboardHidden = true
    var namePrefix = ""
    var currentLanguage = ""

//    weak var  delegate : UserNameVCDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
//        tfUserName.delegate = self
//        self.dHeight = 63.0
        let currentStr = UserDefaults.standard.string(forKey: "currentLanguage")
        currentLanguage = currentStr!
        viewBMaleFemaleCategory.isHidden = true
        btnClearUserName.isHidden = true
        self.tfUserName.placeholder = "Enter Your Name".localized
        self.btnMale.setTitle("Male".localized, for: .normal)
        self.btnFemale.setTitle("Female".localized, for: .normal)
        self.btnU.setTitle("U".localized, for: .normal)
        self.btnMg.setTitle("Mg".localized, for: .normal)
        self.btnMr.setTitle("Mr".localized, for: .normal)
        self.btnDr.setTitle("Dr".localized, for: .normal)
        self.btnDaw.setTitle("Daw".localized, for: .normal)
        self.btnMa.setTitle("Ma".localized, for: .normal)
        self.btnMs.setTitle("Ms".localized, for: .normal)
        self.btnMrs.setTitle("Mrs".localized, for: .normal)
        self.btnMDr.setTitle("Dr".localized, for: .normal)
        self.btnBThired.setTitle("Dr".localized, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    @IBAction func BtnClearUserNameAction(_ sender: Any) {
        tfUserName.leftView = nil
        tfUserName.text = ""
        tfUserName.resignFirstResponder()
        imgUser.image = UIImage(named: "r_user")
        tfUserName.textAlignment = .center
        btnClearUserName.isHidden = true
        isYourNameKeyboardHidden = true
    }
    
    @IBAction func btnSelectGenderAction(_ sender: UIButton) {
        viewMaleFemale.isHidden = true
        if sender.title(for: .normal) == "Male".localized{
//            if currentLanguage == "my" {
//                viewMalePrefix.isHidden = true
//                viewFemalePrifix.isHidden = true
//                viewBMaleFemaleCategory.isHidden = false
//                btnBFirst.setTitle("U".localized, for: .normal)
//                btnBSecond.setTitle("Mg".localized, for: .normal)
//            }
//            else {
                viewMalePrefix.isHidden = false
                viewFemalePrifix.isHidden = true
                viewBMaleFemaleCategory.isHidden = true
//            }
            imgUser.image = UIImage(named: "r_user")
        }else {
//            if currentLanguage == "my" {
//                viewBMaleFemaleCategory.isHidden = false
//                viewFemalePrifix.isHidden = true
//                viewMalePrefix.isHidden = true
//                btnBFirst.setTitle("Daw".localized, for: .normal)
//                btnBSecond.setTitle("Ma".localized, for: .normal)
//            }
//            else {
                viewFemalePrifix.isHidden = false
                viewMalePrefix.isHidden = true
                viewBMaleFemaleCategory.isHidden = true
//            }
            imgUser.image = UIImage(named: "r_female")
        }
    }
    
    @IBAction func btnMalePrefixAction(_ sender: UIButton) {
        setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfUserName)
        tfUserName.becomeFirstResponder()
    }
    
    @IBAction func btnFeMalePrefixAction(_ sender: UIButton) {
        setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfUserName)
        tfUserName.becomeFirstResponder()
    }
    
    @IBAction func btnBurmeseUserNameInitialAction(sender : UIButton) {
        setPrefixName(prefixName: sender.title(for: .normal)!, txtField: tfUserName)
        tfUserName.becomeFirstResponder()
    }
    
    func setPrefixName(prefixName : String, txtField : UITextField) {
        namePrefix = prefixName
        let prelabel = PreNameView.loadView() as! PreNameView
        prelabel.setPreName(name : prefixName, alignment : .left)
        txtField.leftView = prelabel
        txtField.leftViewMode = UITextField.ViewMode.always
        txtField.becomeFirstResponder()
        txtField.textAlignment = .left
        viewMalePrefix.isHidden = true
        viewFemalePrifix.isHidden = true
        viewBMaleFemaleCategory.isHidden = true
        btnClearUserName.isHidden = false
        isYourNameKeyboardHidden = false
    }
    
}

extension UserNameTableViewCell : UITextFieldDelegate {
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if tfUserName.text!.count > 2 {

        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if !isYourNameKeyboardHidden {
            viewMaleFemale.isHidden = true
            return true
        }else{
            viewMaleFemale.isHidden = false
            return false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var CHARSET = ""
//        if currentLanguage == "my"{
//            CHARSET = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာ "
//        }else {
//            CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
//        }
        CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "

        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) {
            return false
        }
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
         if text.count > 50 {
            return false
        }
        return restrictMultipleSpaces(str: string, textField: tfUserName)
    }
    
}
