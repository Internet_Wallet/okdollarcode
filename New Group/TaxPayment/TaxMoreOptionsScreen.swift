//
//  TaxMoreOptionsScreen.swift
//  OK
//
//  Created by Shobhit Singhal on 8/25/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol TaxMoreOptionsDelegate:class {
    func dismissTaxMoreOptionsOnClickRepeatPayment()
    func dismissTaxMoreOptionsOnClickShare()
    func dismissTaxMoreOptionsScreenOnClickInvoice()
}

class TaxMoreOptionsScreen: UIViewController {
    weak var delegate: TaxMoreOptionsDelegate?
    
    @IBOutlet var repeatPaymentBtn: UIButton!{
        didSet{
            repeatPaymentBtn.setTitle(repeatPaymentBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var invoiceBtn: UIButton!{
        didSet{
            invoiceBtn.setTitle(invoiceBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var shareBtn: UIButton!{
        didSet{
            shareBtn.setTitle(shareBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func dismissViewAction(_ sender: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.20, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.dismiss(animated: true, completion: nil)
            }
        });
    }
    
    @IBAction func repaymentAction(_ sender: UIButton){
        if (UserLogin.shared.walletBal as NSString).floatValue < 500.00 {
            self.dismiss(animated: false, completion: nil)
            self.delegate?.dismissTaxMoreOptionsOnClickRepeatPayment()
            return
        }
        self.performSegue(withIdentifier: "TaxRepeatPaymentToCorporateTaxSegue", sender: self)
    }
    
    @IBAction func invoiceAction(_ sender: UIButton) {
        UIView.animate(withDuration: 0.15, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished){
                self.dismiss(animated: true, completion: nil)
                self.delegate?.dismissTaxMoreOptionsScreenOnClickInvoice()
            }
        });
    }
    
    @IBAction func sharePdfAction(_ sender: UIButton){
        UIView.animate(withDuration: 0.20, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished){
                self.dismiss(animated: true, completion: nil)
                self.delegate?.dismissTaxMoreOptionsOnClickShare()
            }
        });
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "TaxReceiptToMoreOptionsScreen" {
//            let vc = segue.destination as! UINavigationController
//            let rootViewController = vc.viewControllers.first as! CorporateTax
////            rootViewController.isRepeatPayment = true
//        }
//    }
}
