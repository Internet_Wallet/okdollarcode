//
//  TaxSelectTownship.swift
//  OK
//
//  Created by Shobhit Singhal on 9/20/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class TaxSelectTownship: UIViewController {
    @IBOutlet weak var taxSelectTownshipTableView: UITableView!
    
    var sectionTownshipArr = [String]()
    var townshipList = [String]()
    var rowsTitleArr = [[String]]()
    var divisionCode = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Select Township".localized
        sectionTownshipArr = ["Select Township"]
        println_debug("kkkkk --> \n \(townshipList)")
        self.GetTownshipListFromServer()
        println_debug("sssss --> \n \(townshipList)")
    }

    @IBAction func backBtnAction(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func myUnwindForTaxAction(segue: UIStoryboardSegue) {}

    func GetTownshipListFromServer() {
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
        let customURL = URL.init(string: "http://www.okdollar.co/RestService.svc/GetTownshipsForEB")
        let agentNum = UserModel.shared.mobileNo
        let msid = UserModel.shared.msid
        let str = uuid
        let parameters = [  "DivisionCode": divisionCode ,
                            "LoginDeviceInfo": [
                                "MobileNumber": agentNum,
                                "Msid": msid,
                                "Ostype": 1,
                                "Otp": "",
                                "Simid": str
            ]] as [String : Any]
        let auth = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: customURL!)
        do {
            // Set the request content type to JSON
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            // The magic...set the HTTP request method to POST
            request.httpMethod = "POST"
            
            // Add the JSON serialized login data to the body
            request.httpBody = auth
            
            // Create the task that will send our login request (asynchronously)
            let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if(data == nil) {
                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: nil, body: appDel.getlocaLizationLanguage(key: "Please Try Again Later."), img: nil)
                        
                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        })
                        alertViewObj.showAlert(controller: self)
                        progressViewObj.removeProgressView()
                    }
                } else {
                    do {
                        let jsonResponse = try? JSONSerialization.jsonObject(with: data!, options:.allowFragments)
                        if let jsonResponse = jsonResponse as? [String: Any] {
//                            println_debug("\n \n \n \(jsonResponse)")
                            
                            if jsonResponse["Msg"] as! String == "Success" {
                                let DataStringToJSON : [String : Any] = self.convertStringToJson(ReceivedString: jsonResponse["Data"]! as! String) as! [String : Any]
                                TownshipResultStringToJSON  = self.convertStringToJson(ReceivedString: DataStringToJSON["result"]! as! String) as AnyObject
                                for element in TownshipResultStringToJSON as! Array<AnyObject> {
                                    var TownshipName = ""
                                    if appDel.getSelectedLanguage() == "en" {
                                        TownshipName = element["eName"]! as! String
                                    }
                                    else {
                                        TownshipName = element["bUnicode"]! as! String
                                    }
                                    self.townshipList.append(TownshipName)
                                }
                                self.rowsTitleArr.append(self.townshipList)
                                DispatchQueue.main.async {
                                    DispatchQueue.main.async {
                                        progressViewObj.removeProgressView()
                                        self.taxSelectTownshipTableView.delegate = self
                                        self.taxSelectTownshipTableView.dataSource = self
                                        self.taxSelectTownshipTableView.reloadData()
                                    }
                                }
                            }
                            else {
                                DispatchQueue.main.async {
                                    alertViewObj.wrapAlert(title: nil, body: appDel.getlocaLizationLanguage(key: "Please Try Again Later."), img: nil)
                                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                    })
                                    alertViewObj.showAlert(controller: self)
                                    progressViewObj.removeProgressView()
                                }
                                
                            }
                        }
                    }
                }
            })
            // Start the task on a background thread
            task.resume()
        }
    }
    
    func convertStringToJson(ReceivedString : String) -> Any {
        let CustomStringData = ReceivedString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion:true)
        do {
            let jsonData = try? JSONSerialization.jsonObject(with: CustomStringData!, options: .allowFragments ) as Any
            return jsonData!
        }
    }
}

extension TaxSelectTownship: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTownshipArr.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 40))
        headerView.backgroundColor = UIColor.colorWithRedValue(redValue: 244, greenValue: 244, blueValue: 245, alpha: 1)
        let headerLabel = UILabel(frame: CGRect(x: 12, y: 5, width: screenWidth-40, height: 30))
        headerLabel.text = self.sectionTownshipArr[section].localized
        headerLabel.font = UIFont.init(name: appFont, size: 17)
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.rowsTitleArr[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaxSelectTownshipCell", for: indexPath) as! TaxSelectTownshipTableViewCell
        cell.titleLabel.text = rowsTitleArr[indexPath.section][indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: "taxSelectTownshipToCorporateTaxSegue", sender: self)
    }
}

class TaxSelectTownshipTableViewCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

