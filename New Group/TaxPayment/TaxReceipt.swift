//
//  TaxReceipt.swift
//  OK
//
//  Created by Shobhit Singhal on 8/24/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class TaxReceipt: OKBaseController {
    @IBOutlet var home: UITabBarItem!
    @IBOutlet var more: UITabBarItem!
    @IBOutlet weak var receiptView: UIView!
    @IBOutlet weak var receiptShareView: UIView!
    @IBOutlet weak var taxReceiptView: UIView!
    @IBOutlet weak var userNameLabelValue: UILabel!
    @IBOutlet weak var okNumberLabelValue: UILabel!
    @IBOutlet weak var emailIdLabelValue : UILabel!
    @IBOutlet weak var incomeTaxMonthYearLabelValue : UILabel!
    @IBOutlet weak var totalNumberOfEmployeesLabelValue : UILabel!
    @IBOutlet weak var iRDOfficeLabelValue: UILabel!
    @IBOutlet weak var categoriesLabelValue: UILabel!
    @IBOutlet weak var transactionIdLabelValue: UILabel!
    @IBOutlet weak var transactionTypeLabelValue: UILabel!
    @IBOutlet weak var balanceRemainingLabelValue: UILabel!
    @IBOutlet weak var amountPaidLabelValue: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var viewTaxReceiptLabel: UILabel!

    var dataDict = Dictionary<String, String>()

    @IBOutlet weak var userNameLabel: UILabel!{
        didSet {
            userNameLabel.text = userNameLabel.text?.localized
        }
    }
    @IBOutlet weak var okNumberLabel: UILabel!{
        didSet {
            okNumberLabel.text = okNumberLabel.text?.localized
        }
    }
    @IBOutlet weak var emailIdLabel: UILabel!{
        didSet {
            emailIdLabel.text = emailIdLabel.text?.localized
        }
    }
    @IBOutlet weak var incomeTaxMonthYearLabel: UILabel!{
        didSet {
            incomeTaxMonthYearLabel.text = incomeTaxMonthYearLabel.text?.localized
        }
    }
    @IBOutlet weak var totalNumberOfEmployeesLabel: UILabel!{
        didSet {
            totalNumberOfEmployeesLabel.text = totalNumberOfEmployeesLabel.text?.localized
        }
    }
    @IBOutlet weak var iRDOfficeLabel: UILabel!{
        didSet {
            iRDOfficeLabel.text = iRDOfficeLabel.text?.localized
        }
    }
    @IBOutlet weak var categoriesLabel: UILabel!{
        didSet {
            categoriesLabel.text = categoriesLabel.text?.localized
        }
    }
    @IBOutlet weak var transactionIdLabel: UILabel!{
        didSet {
            transactionIdLabel.text = transactionIdLabel.text?.localized
        }
    }
    @IBOutlet weak var transactionTypeLabel: UILabel!{
        didSet {
            transactionTypeLabel.text = transactionTypeLabel.text?.localized
        }
    }
    @IBOutlet weak var balanceRemainingLabel: UILabel!{
        didSet {
            balanceRemainingLabel.text = balanceRemainingLabel.text?.localized
        }
    }

    @IBOutlet var balanceFieldViewHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Receipt".localized
        self.initLoad()
        self.receivedDataAfterTaxPayment()
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: kYellowColor, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .normal)
        UITabBar.appearance().barTintColor = .white
        UITabBar.appearance().tintColor = .darkGray
        UITabBar.appearance().unselectedItemTintColor = .lightGray
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let color = UIColor.lightGray
        let attrFont = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.0) ?? UIFont.systemFont(ofSize: 11.0), NSAttributedString.Key.foregroundColor: color]
        
        self.home.setTitleTextAttributes(attrFont, for: .normal)
        self.more.setTitleTextAttributes(attrFont, for: .normal)
        self.home.title = "Home".localized
        
        self.more.title = "More".localized
        
    }


    func receivedDataAfterTaxPayment() {
        let emailArr = UserModel.shared.email.components(separatedBy: ",")
        amountPaidLabelValue.text = self.dataDict["total_Amount"]
        userNameLabelValue.text = (UserModel.shared.name.replacingOccurrences(of: ",", with: ". "))
        let mobileNo = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "(+95)0")
        okNumberLabelValue.text = mobileNo
        emailIdLabelValue.text = emailArr[0]
        incomeTaxMonthYearLabelValue.text = "\(self.dataDict["month"] ?? "") \(self.dataDict["year"] ?? "")"
        totalNumberOfEmployeesLabelValue.text = dataDict["total_Employees"]
        iRDOfficeLabelValue.text = dataDict["IRD_Office"]
        categoriesLabelValue.text = "Salary Tax".localized
        transactionIdLabelValue.text = dataDict["transactionId"]
        transactionTypeLabelValue.text = "Tax Payment".localized
        balanceRemainingLabelValue.text = dataDict["balance"]
        dateLabel.text = dataDict["date"]
        timeLabel.text = dataDict["time"]
    }
    
    func initLoad() {
        taxReceiptView.isHidden = true
        receiptShareView.isHidden = true
        self.receiptView.layer.cornerRadius = 2
        self.receiptView.layer.shadowOpacity = 1
        self.receiptView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.receiptView.layer.shadowRadius = 2
        self.receiptView.layer.shadowColor = UIColor.black.cgColor
        self.receiptView.layer.masksToBounds = false
        
        self.taxReceiptView.layer.cornerRadius = 2
        self.taxReceiptView.layer.shadowOpacity = 1
        self.taxReceiptView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.taxReceiptView.layer.shadowRadius = 2
        self.taxReceiptView.layer.shadowColor = UIColor.black.cgColor
        self.taxReceiptView.layer.masksToBounds = false
    }
}

extension TaxReceipt : UITabBarDelegate {
    
    //MARK:- Tabbar Delegate
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch(item) {
        case home : self.btnHomeClick(item)
        case more : self.btnMoreClick(item)
        default: break
        }
    }
    
    @IBAction func btnMoreClick(_ sender: UITabBarItem){
        self.performSegue(withIdentifier: "TaxReceiptToMoreOptionsScreen", sender: self)
    }
    
    @IBAction func btnHomeClick(_ sender: UITabBarItem){
        performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
    }
    
    @IBAction func viewTaxReceiptAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "receiptToViewTaxReceiptSegue", sender: self)
    }
}

extension TaxReceipt : TaxMoreOptionsDelegate {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TaxReceiptToMoreOptionsScreen" {
            let controller = segue.destination as! TaxMoreOptionsScreen
            controller.delegate = self
        }
        else if segue.identifier == "receiptToViewTaxReceiptSegue" {
            let controller = segue.destination as! ViewTaxReceipt
            controller.dataDict = self.dataDict
        }
        else if segue.identifier == "moreOptionsToTaxInvoice" {
            let vc = segue.destination as! UINavigationController
            let rootViewController = vc.viewControllers.first as! TaxInvoice
            rootViewController.dataDict = self.dataDict
        }
    }
    
    func dismissTaxMoreOptionsOnClickRepeatPayment() {
        self.addMoney()
    }
    
    func dismissTaxMoreOptionsScreenOnClickInvoice() {
        self.performSegue(withIdentifier: "moreOptionsToTaxInvoice", sender: self)
    }

    func dismissTaxMoreOptionsOnClickShare() {
        let image = self.captureScreen()
        let imageToShare = [image!]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    
    func addMoney() {
        alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
        alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
            if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                self.modalPresentationStyle = .fullScreen
                self.present(addWithdrawView, animated: true, completion: nil)
            }
        })
        alertViewObj.showAlert()
    }
    
    func captureScreen() -> UIImage? {
        let bounds = receiptView.bounds
        UIGraphicsBeginImageContextWithOptions(bounds.size, true, 0.0)
        receiptView.drawHierarchy(in: bounds, afterScreenUpdates: false)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
