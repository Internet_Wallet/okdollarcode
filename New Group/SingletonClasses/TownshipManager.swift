//
//  TownshipManager.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/28/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import Foundation

class TownshipManager {
    static let shared = TownshipManager()
    
    static var allLocationsList = [LocationDetail]()
    static var allDivisionList = [LocationDetail]()
    static var allStateList = [LocationDetail]()
    
    init() {
        TownshipManager.getStateAndDivisionDetails()
    }
 
    class func getStateAndDivisionDetails() {
        
        if let path = Bundle.main.path(forResource: "city_list_json", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject>, let stateDivisionList = jsonResult["data"] as? [Any] {
                    
                    if let townshipPath = Bundle.main.path(forResource: "townshipJson", ofType: "json")  {
                        do {
                            let townshipData = try Data(contentsOf: URL(fileURLWithPath: townshipPath), options: .mappedIfSafe)
                            let townshipJson = try JSONSerialization.jsonObject(with: townshipData, options: .mutableLeaves)
                            if let townshipJson = townshipJson as? Dictionary<String, AnyObject>, let allTownshipList = townshipJson["data"] as? [AnyObject] {
                                
                                for (_, value) in stateDivisionList.enumerated() {
                                    // here create new instance of all locationlist for division
                                    if let dict = value as? Dictionary<String, AnyObject> {
                                        
                                        let eachLocation = LocationDetail()
                                        eachLocation.stateOrDivitionCode          = dict["State Code"]    as! String
                                        eachLocation.stateOrDivitionNameEn        = dict["State Name"]    as! String
                                        eachLocation.stateOrDivitionNameMy        = dict["State BName"]   as! String
                                        eachLocation.nrcCodeNumber                = dict["DivNrcCode"]    as! String
                                        eachLocation.nrcCodeNumberMy              = dict["DivNrcCodeMyan"]as! String
                                        eachLocation.nrcDisplayStateDivEN         =  eachLocation.nrcCodeNumber + "/(" + eachLocation.stateOrDivitionNameEn + ")"
                                        eachLocation.nrcDisplayStateDivMY         =  eachLocation.nrcCodeNumberMy + "/(" + eachLocation.stateOrDivitionNameMy + ")"

                                        
                                        var townshiplist = [TownShipDetail]()
                                        var townshipListForAddress = [TownShipDetailForAddress]()
                                        
                                        let predicate = NSPredicate(format: "StateCode == %@", dict["State Code"] as! String )
                                        var filterTownShipList = allTownshipList.filter { predicate.evaluate(with: $0) } as [AnyObject]
                                        var filterTownshipListForAddress = filterTownShipList
                                        
                                        var tempList = filterTownShipList
                                        filterTownShipList.removeAll()
                                        
                                        for i in 0..<tempList.count {
                                            let name1 = tempList[i]["NrcCode"] as! String
                                            if(i == 0){
                                                filterTownShipList.append(tempList[i])
                                            }else{
                                                var doesExist = false
                                                for j in 0..<filterTownShipList.count {
                                                    let name2: String = filterTownShipList[j]["NrcCode"]! as! String
                                                    if name1 == name2 {
                                                        doesExist = true
                                                    }
                                                }
                                                if(!doesExist){
                                                    filterTownShipList.append(tempList[i])
                                                }
                                            }
                                        }
                                        tempList.removeAll()
                                        
                                        if filterTownShipList.count > 0 {
                                            for (_, townshipValue) in filterTownShipList.enumerated() {
                                                // here create new township instance for that selected division
                                                if var dic = townshipValue as? Dictionary<String, AnyObject> {
                                                    let eachTownship = TownShipDetail()
                                                    eachTownship.cityNameEN     = dic["CityName"]       as! String
                                                    eachTownship.cityNameMY     = dic["CityBName"]      as! String
                                                    eachTownship.cityCode       = dic["CityCode"]       as! String
                                                    eachTownship.nrcCode        = dic["NrcCode"]        as! String
                                                    eachTownship.nrcBCode        = dic["NrcBCode"]        as! String
                                                    eachTownship.townShipNameEN = dic["TownshipName"]   as! String
                                                    eachTownship.townShipNameMY = dic["TownshipBName"]  as! String
                                                    eachTownship.townShipCode   = dic["TownshipCode"]   as! String
                                                    eachTownship.nrcCodeNumber  = eachLocation.nrcCodeNumber
                                                    eachTownship.nrcCodeNumberMy  = eachLocation.nrcCodeNumberMy
                                                    eachTownship.nrcDisplayCodeEN =  eachTownship.nrcCodeNumber + "/(" + eachTownship.nrcCode + ") " + eachTownship.townShipNameEN
                                                    eachTownship.nrcDisplayCodeMY =  eachTownship.nrcCodeNumberMy + "/(" + eachTownship.nrcBCode + ") " + eachTownship.townShipNameMY

                                                    if dic["isDefaultCity"] != nil {
                                                        eachTownship.isDefaultCity = dic["isDefaultCity"] as! String
                                                    }else {
                                                        eachTownship.isDefaultCity = "0"
                                                    }
                                                    
                                                    townshiplist.append(eachTownship)
                                                }
                                            }
                                        }
                                        
                                        tempList = filterTownshipListForAddress
                                        filterTownshipListForAddress.removeAll()

                                        for i in 0..<tempList.count {
                                            let name1 = tempList[i]["TownshipName"] as! String
                                            if(i == 0){
                                                filterTownshipListForAddress.append(tempList[i])
                                            }else{
                                                var doesExist = false
                                                for j in 0..<filterTownshipListForAddress.count {
                                                    let name2: String = filterTownshipListForAddress[j]["TownshipName"]! as! String
//                                                    name1.lowercased()
//                                                    name2.lowercased()
                                                    if name1.trimmingCharacters(in:.whitespaces) == name2.trimmingCharacters(in:.whitespaces) {
                                                        doesExist = true
                                                    }
                                                }
                                                if(!doesExist){
                                                    filterTownshipListForAddress.append(tempList[i])
                                                }
                                            }
                                        }
                                                tempList.removeAll()
                                        
                                        if filterTownshipListForAddress.count > 0 {
                                            for (_, townshipValue) in filterTownshipListForAddress.enumerated() {
                                                // here create new township instance for that selected division
                                                if var dic = townshipValue as? Dictionary<String, AnyObject> {
                                                    let eachTownship = TownShipDetailForAddress()
                                                    eachTownship.cityNameEN     = dic["CityName"]       as! String
                                                    eachTownship.cityNameMY     = dic["CityBName"]      as! String
                                                    eachTownship.cityCode       = dic["CityCode"]       as! String
                                                    eachTownship.nrcCode        = dic["NrcCode"]        as! String
                                                    eachTownship.townShipNameEN = dic["TownshipName"]   as! String
                                                    eachTownship.townShipNameMY = dic["TownshipBName"]  as! String
                                                    eachTownship.townShipCode   = dic["TownshipCode"]   as! String
                                                    eachTownship.nrcCodeNumber  = eachLocation.nrcCodeNumber
                                                    
                                                    if dic["isDefaultCity"] != nil {
                                                        eachTownship.isDefaultCity = dic["isDefaultCity"] as! String
                                                    }else {
                                                        eachTownship.isDefaultCity = "0"
                                                    }
                                                    townshipListForAddress.append(eachTownship)
                                                }
                                            }
                                        }
 
                                        eachLocation.townShipArray = townshiplist
                                        eachLocation.townshipArryForAddress = townshipListForAddress
                                        self.allLocationsList.append(eachLocation)
                                        
                                        if let isState = dict["isState"] as? String {
                                            if isState == "1" {
                                                self.allDivisionList.append(eachLocation)
                                            }else{
                                                self.allStateList.append(eachLocation)
                                            }
                                        }
                                        
                                    }
                                }
                            }
                        } catch {
                            println_debug("something went wrong when extract data from township json file")
                        }
                    }
                }
            } catch {
                println_debug("something went wrong when extract data from citylist json file")
            }
        }
    }
    
    class func collapseAllLocation() {
        for location in self.allLocationsList {
            if location.isExpand {
                location.isExpand = false
            }
        }
        shared.collapseAllDivision()
        shared.collapseAllState()
    }
    
    func collapseAllDivision() {
        for location in TownshipManager.allDivisionList {
            if location.isExpand {
                location.isExpand = false
            }
        }
    }
    
    func collapseAllState() {
        for location in TownshipManager.allStateList {
            if location.isExpand {
                location.isExpand = false
            }
        }
    }
}


class LocationDetail: NSObject {
    override init() {
        super.init()
    }
    var stateOrDivitionCode         :String        = ""
    var stateOrDivitionNameEn       :String        = ""
    var stateOrDivitionNameMy       :String        = ""
    var nrcCodeNumber               :String        = ""
    var nrcCodeNumberMy             :String        = ""
    var townShipArray               :Array         = [TownShipDetail]()
    var townshipArryForAddress      :Array         = [TownShipDetailForAddress]()
    var nrcDisplayStateDivEN        :String        = ""
    var nrcDisplayStateDivMY        :String        = ""
    var isExpand                    :Bool          = false
}

class TownShipDetail: NSObject {
    override init() {
        super.init()
    }
    var cityNameEN          :String   = ""
    var cityNameMY          :String   = ""
    var cityCode            :String   = ""
    var nrcCode             :String   = ""
    var nrcBCode             :String   = ""
    var townShipNameEN      :String   = ""
    var townShipNameMY      :String   = ""
    var nrcCodeNumber       :String   = ""
    var nrcCodeNumberMy     :String   = ""
    var nrcDisplayCodeEN    :String   = ""
    var nrcDisplayCodeMY    :String   = ""
    var townShipCode        :String   = ""
    var isDefaultCity       :String   = ""
}

class TownShipDetailForAddress: NSObject {
    override init() {
        super.init()
    }
    var cityNameEN          :String   = ""
    var cityNameMY          :String   = ""
    var cityCode            :String   = ""
    var nrcCode             :String   = ""
    var townShipNameEN      :String   = ""
    var townShipNameMY      :String   = ""
    var nrcCodeNumber       :String   = ""
    var townShipCode        :String   = ""
    var isDefaultCity       :String   = ""
}

