//
//  VillageManager.swift
//  OK
//
//  Created by Subhash Arya on 29/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import Foundation

class VillageManager : OKBaseController,WebServiceResponseDelegate{
    
    static let shared = VillageManager()
    var allVillageList = VillageList()
    var allStreetList =  StreetList()
    var data = getdata()
    
    var dicValue : Dictionary<String,Any> = Dictionary<String,Any>()
    
    
    class func getVillageAndStreet(townshipCode : String) {
        
        let urlString = URL(string: "https://www.okdollar.co/RestService.svc/GetVillageStreetByTownShipCode?TownshipCode=" + townshipCode)
        println_debug(urlString!)
        if let url = urlString {
            progressViewObj.showProgressView()
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    println_debug(error as Any)
                    progressViewObj.removeProgressView()
                } else {
                    progressViewObj.removeProgressView()
                    if let data = data {
                        if let stringData = String(data: data, encoding: String.Encoding.utf8) {
                            let dict = self.convertToDictionary(text: stringData)
                            let response = dict!["Data"] as Any
                            let dic = self.convertToDictionary(text: response as! String)
                            if let d = dic {
                                if let arrDic = d["Streets"] as? Array<Dictionary<String,String>> {
                                    let streetList = StreetList()
                                    streetList.isStreetList = true
                                    streetList.streetArray = arrDic
                                    VillageManager.shared.allStreetList = streetList
                                }else{
                                    let streetList = StreetList()
                                    streetList.streetArray.removeAll()
                                    streetList.isStreetList = false
                                    VillageManager.shared.allStreetList = streetList
                                }
                                
                                if let arrDic1 = d["Villages"] as? Array<Dictionary<String,String>>{
                                    let villageList = VillageList()
                                    villageList.isVillageList = true
                                    villageList.villageArray = arrDic1
                                    VillageManager.shared.allVillageList = villageList
                                }else{
                                    let villageList = VillageList()
                                    villageList.villageArray.removeAll()
                                    villageList.isVillageList = false
                                    VillageManager.shared.allVillageList = villageList
                                }
                            }
                        }
                    }
                }
            }
            task.resume()
        }
    }
    
    func webcallForGet(urlStr: String , screen : String) {
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            let url = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug(url)
            let param = [String:String]() as AnyObject
            
            web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: screen)
            
        }else{
            alertViewObj.wrapAlert(title: "", body: "Network error and try again", img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func webCallForPost(urlStr : String , screen : String ,parameter : AnyObject) {
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            let url = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug(urlStr)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "POST", mScreen: screen)
            
        }else{
            alertViewObj.wrapAlert(title: "", body: "Network error and try again", img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func webCallForPostEstel(urlStr : String , screen : String) {
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
             let url = getUrl(urlStr: urlStr, serverType: .serverEstel)
            println_debug(url)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "POST", mScreen: screen)
            
        }else{
            alertViewObj.wrapAlert(title: "", body: "Network error and try again", img:#imageLiteral(resourceName: "r_user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    

    
    
    
    func webResponse(withJson json: AnyObject, screen: String) {
        
        DispatchQueue.main.async {
          self.removeProgressView()
        }
        if screen == "SecurityQuestion"{
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(String(describing: dic))
                        if dic["Code"] as? NSInteger == 200 {
                        let StrDic = OKBaseController.convertToDictionary(text: dic["Data"] as! String)
                            VillageManager.shared.data.securityQ = StrDic! as [String : AnyObject]
                    }
                    }
                }
            } catch {
                alertViewObj.wrapAlert(title: "", body: "Network error and try again", img:#imageLiteral(resourceName: "r_user"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                DispatchQueue.main.async {
                    alertViewObj.showAlert(controller: self)
                }
            }
        }else if screen == "RegistrationStatus"{
            if let data = json as? Data {
                do {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dataDict = dict as? Dictionary<String,AnyObject> {
                        if let dic = dataDict["Data"] as? String {
                            println_debug(dic)
                            VillageManager.shared.data.registrationStatus = dic
                        }
                    }
                } catch {
                }
            }
        }else if screen == "VerifyOKDollarMobilNumber" {
            println_debug("VerifyOKDollarMobilNumber")
            let responseString = String(data: json as! Data, encoding: .utf8)
            let xml            = SWXMLHash.parse(responseString!)
            println_debug(xml)
           self.enumerate(indexer: xml)
            if dicValue.safeValueForKey("resultdescription") as? String == "Transaction Successful" {
                
            }
        }
    }
  
    func uploadImageToS3(imageBase64String : Dictionary<String,Any>, handler : @escaping (_ imageString : String,_ isSuccess : Bool) -> Void) {
        let urlStr   = "http://advertisement.api.okdollar.org/AdService.svc/GetImageUrlByBase64String"
        let url = URL.init(string:urlStr)
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url!)
        request.timeoutInterval = 120
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        let hbData:Data? = nil
    
        let theJSONData = try? JSONSerialization.data(withJSONObject: imageBase64String , options: JSONSerialization.WritingOptions(rawValue: 0))
            let jsonString          = NSString(data: theJSONData!,encoding: String.Encoding.ascii.rawValue)
            let postLength = NSString(format:"%lu", jsonString!.length) as String
            request.setValue(postLength, forHTTPHeaderField:"Content-Length")
            request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
            if let httpBodyData = hbData {
                request.httpBody = httpBodyData
            }
        
        request.setValue(nil, forHTTPHeaderField:"Authorization")
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if let errorResponse = error {
                handler(errorResponse.localizedDescription,false)
            } else {
                if let httpStatus = response as? HTTPURLResponse {
                    if httpStatus.statusCode == 200 {
                        
                        if let json = data {
                            do {
                                let dict = try JSONSerialization.jsonObject(with: json, options: .allowFragments)
                                if var dataDict = dict as? Dictionary<String,AnyObject> {
                                        println_debug(dataDict)
                                    if dataDict["Code"] as! NSNumber == 200 {
                                        handler((dataDict["Data"] as? String)! , true)
                                        return
                                    }
                                }
                            } catch {
                            }
                        }
                    }
                } else {
                    println_debug("No Response")
                }
            }
        }
        dataTask.resume()
    }
    


   fileprivate func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            dicValue[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
override class func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                println_debug(error.localizedDescription)
            }
        }
        return nil
    }
}

class VillageList: NSObject {
    var villageArray  =  [Dictionary<String,String>]()
    var isVillageList  :Bool  = false
}

class StreetList: NSObject {
    var streetArray  =  [Dictionary<String,String>]()
    var isStreetList  :Bool  = false
}

class getdata : NSObject {
    var registrationStatus = ""
    var securityQ = Dictionary<String,AnyObject>()
}


