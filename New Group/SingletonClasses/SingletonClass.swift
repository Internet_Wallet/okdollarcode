//
//  SingletonClass.swift
//  DemoUpdateProfile
//
//  Created by SHUBH on 8/29/17.
//  Copyright © 2017 SHUBH. All rights reserved.
//

import UIKit
import CoreLocation
typealias JSONDictionary = [String:Any]

class SingletonClass: NSObject {
    
    var locationManager:CLLocationManager!
    let locManager = CLLocationManager()
    var currentLocation: CLLocation!
    let authStatus = CLLocationManager.authorizationStatus()
    let inUse = CLAuthorizationStatus.authorizedWhenInUse
    let always = CLAuthorizationStatus.authorizedAlways

    var userDefauld = UserDefaults.standard
    
    static let sharedInstance: SingletonClass = {
        let instance = SingletonClass()
        // setup code
        return instance
    }()
    
    // MARK: - Initialization Method
    
    override init() {
        super.init()
    }
    
    //To save the string in UserDefalults
    func setStaticValuesForKey(keyStirng :String , valueString :String)  {
        userDefauld.set( valueString, forKey: keyStirng)
        userDefauld.synchronize()
    }
    
    //To retrieve from the key in UserDefalults
    func getStaticValuesForKey(keyStirng :String) -> String {
    let value  = userDefauld.string(forKey: keyStirng)
    return value!
    }
    
    
    
    func getAdress(completion: @escaping (_ address: JSONDictionary?, _ error: Error?) -> ()) {
        
        self.locManager.requestWhenInUseAuthorization()
        
        if self.authStatus == inUse || self.authStatus == always {
            
            self.currentLocation = locManager.location
            
            let geoCoder = CLGeocoder()
            
            geoCoder.reverseGeocodeLocation(self.currentLocation) { placemarks, error in
                
                if let e = error {
                    
                    completion(nil, e)
                    
                } else {
                    
                    let placeArray = placemarks
        
                    var placeMark: CLPlacemark!
                   
                    placeMark = placeArray?[0]
                    guard let address = placeMark.addressDictionary as? JSONDictionary else {
                        return
                    }
                    
                    completion(address, nil)
                    
                }
                
            }
            
        }
        
    }

}
