//
//  GeoLocationManager.swift
//  OK
//
//  Created by Uma Rajendran on 11/6/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class GeoLocationManager : NSObject, CLLocationManagerDelegate {
    static let shared = GeoLocationManager()
    
    var locationManager: CLLocationManager!
    var geoCoder: CLGeocoder!
    var placeMark: CLPlacemark!
    var currentLatitude: String!
    var currentLongitude: String!
    var currentLocation: CLLocation!
    
    
    override init() {
        super.init()
        self.setUpLocationManager()
    }
    
    func setUpLocationManager() {
      
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
        startUpdateLocation()
        geteCurrentLocationDetails()

    }
    
    func startUpdateLocation() {
        self.locationManager.startUpdatingLocation()
    }
    
    func stopUpdateLocation() {
        self.locationManager.stopUpdatingLocation()
    }
    
    func geteCurrentLocationDetails() {
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse || CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways {
            currentLocation = locationManager.location
        }
        
        //lGeoLocation
        if let lat = currentLocation?.coordinate.latitude, let long = currentLocation?.coordinate.longitude {
            currentLatitude = String(lat)
            currentLongitude = String(long)
        }
   
     
    }
    

    func getLatLongByName(cityname: String, completionHandler: @escaping (Bool, String?, String?) -> Void) {
        let googleUrlStr = "http://maps.google.com/maps/api/geocode/json?address=\(cityname)&sensor=false"
        let escapedUrlStr = googleUrlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if let url = URL.init(string: escapedUrlStr!) {
            getGoogleApiResponse(apiUrl: url, compilationHandler: { (isSuccess, response) in
                guard isSuccess else {
                    if let errorMsg = response {
                        println_debug("error message from google api ::::: \(errorMsg)")
                    }else{
                        println_debug("error :: something went wrong on google api :::::")
                    }
                    completionHandler(false, nil, nil)
                    return
                }
                
                if let results = response as? Array<Any> {
                    for dict in results {
                        if let dic = dict as? Dictionary<String, Any> {
                            if let geometry = dic["geometry"] as? Dictionary<String, Any> {
                                if let location = geometry["location"] as? Dictionary<String, Any> {
                                    if let locLat = location["lat"] as? Double, let locLong = location["lng"] as? Double {
                                        completionHandler(true, String(locLat), String(locLong) )
                                        return
                                    }
                                }
                            }
                        }
                    }
                }
                completionHandler(false, nil, nil)

            })
        }
        
    }
    
    func getLocationNameByLatLong(lattitude: String, longitude: String , completionHandler: @escaping (Bool, Any?) -> Void) {
        var googleUrlStr = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lattitude),\(longitude)&key=\(googleAPIKey)"
        
        if appDel.currentLanguage == "my" {
            googleUrlStr = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lattitude),\(longitude)&language=my&key=\(googleAPIKey)"
        }else {
            googleUrlStr = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lattitude),\(longitude)&key=\(googleAPIKey)"
        }
     
        let url = URL.init(string: googleUrlStr)
        getGoogleApiResponse(apiUrl: url!) { (isSuccess, response) in
            guard isSuccess else {
                if let errorMsg = response {
                    println_debug("error message from google api ::::: \(errorMsg)")
                }else{
                    println_debug("error :: something went wrong on google api :::::")
                }
                completionHandler(false, nil)
                return
            }
            
            if let results = response as? Array<Any> {
                for dict in results {
                    if let dic = dict as? Dictionary<String, Any> {
                        if let typesArr = dic["types"] as? [String] {
                            let itemExists = typesArr.contains(where: {
                                $0.range(of: "neighborhood", options: .caseInsensitive) != nil
                            })
                            if itemExists == true {
                                completionHandler(true, dic["formatted_address"])
                                return
                                /*
                                if let geometryDic = dic["geometry"] as? Dictionary<String, Any> {
                                    if let locType = geometryDic["location_type"] as? String {
                                        if locType.lowercased() == "geometric_center" {
                                            completionHandler(true, dic["formatted_address"])
                                            return
                                        }
                                    }
                                }
                                */
                            }
    
                        }
                    }
                }
            }
            completionHandler(false, nil)
        }
    }
    //language=hi
    func getAddressFrom(lattitude: String, longitude: String,language : String , completionHandler: @escaping (Bool, Any?) -> Void) {
        
     //   language = "my"
       var googleUrlStr = "https://maps.googleapis.com/maps/api/geocode/json?latlng=16.816790,96.131840&key=AIzaSyBj3-AcJhgEf7dQ91Zrwc5UjX-mpAPzQD0"
        if language == "my" {
          googleUrlStr = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lattitude),\(longitude)&language=my&key=\(googleAPIKey)"
        }else {
       // googleUrlStr = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lattitude),\(longitude)&language=my&key=\(googleAPIKey)"
    googleUrlStr = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lattitude),\(longitude)&key=\(googleAPIKey)"
        }
        println_debug("GoogleURL : \(googleUrlStr)")
        let url = URL.init(string: googleUrlStr)
        getGoogleApiResponse(apiUrl: url!) { (isSuccess, response) in
            guard isSuccess else {
                if let errorMsg = response {
                    println_debug("error message from google api ::::: \(errorMsg)")
                }else{
                    println_debug("error :: something went wrong on google api :::::")
                }
                completionHandler(false, nil)
                return
            }
            
            var finalDict = Dictionary<String,String>()
            
            if let results = response as? Array<Any> {
                for dict in results {
                    if let dic = dict as? Dictionary<String,Any> {
                        if let arr = dic["address_components"] as? Array<NSDictionary> {
                            for item in arr {
                                if let arrString = item.object(forKey: "types") as? Array<String> {
                                    for element in arrString {
                                        if element == "route" {
                                            finalDict["street"] = item.object(forKey: "long_name") as? String
                                        }
                                        
                                        if element == "sublocality_level_1" {
                                            finalDict["township"] = item.object(forKey: "long_name") as? String
                                        }
                                        
                                        if element == "administrative_area_level_1" {
                                            finalDict["region"] = item.object(forKey: "long_name") as? String
                                        }
                                        
                                        if element == "locality" {
                                            finalDict["city"] = item.object(forKey: "long_name") as? String
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                
                completionHandler(true,finalDict)
                
            }
            completionHandler(false, nil)
        }
        
        // https://maps.googleapis.com/maps/api/geocode/json?latlng=16.816790,96.131840&key=AIzaSyBj3-AcJhgEf7dQ91Zrwc5UjX-mpAPzQD0
        //https://maps.googleapis.com/maps/api/geocode/json?latlng=16.816790,96.131840&language=my&key=AIzaSyBj3-AcJhgEf7dQ91Zrwc5UjX-mpAPzQD0
    }

    
     func getGoogleApiResponse(apiUrl: URL , compilationHandler : @escaping  (Bool, Any?) -> Void){
        
        let request = URLRequest.init(url: apiUrl, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
        _ = URLSession.shared.configuration
        
        URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            do {
                
                guard data != nil || error == nil else {
                    compilationHandler(false, nil)
                    return }
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
               // println_debug(json)
                if (json["status"] as? String)?.lowercased() == "ok" {
                    if let resultArr = json["results"] as? Array<Any> {
                        if resultArr.count > 0 {
                            compilationHandler(true, resultArr)
                        }else {
                            compilationHandler(false, json["error_message"])
                        }
                    }
                    
                }else{
                    
                    compilationHandler(false, json["error_message"])
                }
            }catch {
                compilationHandler(false, nil)
            }
            }.resume()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        println_debug("GeoLocationManager ::: error in get location \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue: CLLocationCoordinate2D = (manager.location?.coordinate)!
       // println_debug("GeoLocationManager :: current lattitude \(locValue.latitude) , \(locValue.longitude)")
        currentLatitude = String(locValue.latitude)
        currentLongitude = String(locValue.longitude)
        //        mapView.centerCoordinate = CLLocationCoordinate2DMake(locValue.latitude, locValue.longitude)
        //
        //        var zoomRect: MKMapRect = MKMapRectNull
        //        for annotation in mapView.annotations {
        //            let annotationPoint = MKMapPointForCoordinate(annotation.coordinate)
        //            //            let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1)
        //            let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.0, 0.0)
        //
        //            zoomRect = MKMapRectUnion(zoomRect, pointRect)
        //
        //        }
        //        mapView.setVisibleMapRect(zoomRect, animated: true)
        //        locationManager.stopUpdatingLocation()
    }
    
}


