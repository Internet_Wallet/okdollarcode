//
//  CategoriesManager.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/28/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit
import Foundation

class CategoriesManager {
    static let shared = CategoriesManager()
    static var categoriesList = [CategoryDetail]()
    init() {
        CategoriesManager.createCategoriesModel()
    }
    class func createCategoriesModel() {
        
        if let path = Bundle.main.path(forResource: "business_category_promotion", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject>, let categoriesList = jsonResult["data"] as? [Any] {
                    
                    if let emojiPath = Bundle.main.path(forResource: "emojiString", ofType: "json")  {
                        do {
                            let emojiData = try Data(contentsOf: URL(fileURLWithPath: emojiPath), options: .mappedIfSafe)
                            let emojiJson = try JSONSerialization.jsonObject(with: emojiData, options: .mutableLeaves)
                            if let emojiJson = emojiJson as? Dictionary<String, AnyObject>, let allEmojiList = emojiJson["data"] as? [AnyObject] {
                                
                                for dicts in categoriesList {
                                    if let dict = dicts as? Dictionary<String,Any> {
                                        let category = CategoryDetail()
                                        category.categoryCode = dict["category_code"] as! String
                                        category.categoryEmoji = dict["category_emoji"] as! String
                                        category.categoryName = dict["category_name"] as! String
                                        category.category_image = dict["category_image"] as! String
                                        
                                        var subCategoryBurmese = [Any]()
                                        if let path = Bundle.main.path(forResource: "business_category_my_promotion", ofType: "json") {
                                            do {
                                                let burPromoData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                                                let burPromoJson = try JSONSerialization.jsonObject(with: burPromoData, options: .mutableLeaves)
                                                if let burPromoJson = burPromoJson as? Dictionary<String, Any>, let burPromotionList = burPromoJson["data"] as? [Any] {
                                                    for burPromodic in burPromotionList {
                                                        if let promodics = burPromodic as? Dictionary<String, Any> {
                                                            if promodics["category_code"] as! String == category.categoryCode {
                                                                category.categoryBurmeseName = promodics["category_name"] as! String
                                                                subCategoryBurmese = promodics["subcategory"] as! [Any]
                                                                break
                                                            }
                                                        }
                                                    }
                                                }
                                            } catch {
                                                println_debug("something went wrong when handle burmese promotion json file")
                                            }
                                        }
                                        
                                        
                                        let predicate = NSPredicate(format: "category_code == %@", dict["category_code"] as! String)
                                        let emojiFilter = allEmojiList.filter { predicate.evaluate(with: $0)} as [AnyObject]
                                        
                                        var subcateArr = [SubCategoryDetail]()
                                        if let array : [Dictionary<String,Any>] = dict["subcategory"] as? [Dictionary<String,Any>]  {
                                            for dics in array {
                                                    let sortedSubCategory = dics.sorted {($0.key as NSString).integerValue < ($1.key as NSString).integerValue}
 
                                                    for obj in sortedSubCategory {
                                                        let subcategory = SubCategoryDetail()
                                                        subcategory.mainCategoryCode = category.categoryCode
                                                        subcategory.subCategoryCode = obj.key
                                                        subcategory.subCategoryName = obj.value as! String
                                                        
                                                        for emoji in emojiFilter {
                                                            if let emojiArr = emoji["subcategory"] as? [Dictionary<String, Any>] {
                                                                for eachEmojiDic in emojiArr {
                                                                        let sortedSubCategoryEmoji = eachEmojiDic.sorted {($0.key as NSString).integerValue < ($1.key as NSString).integerValue}
                                                                        for emojiObj in sortedSubCategoryEmoji {
                                                                            if emojiObj.key == obj.key {
                                                                                subcategory.subCategoryEmoji = emojiObj.value as! String
                                                                                break
                                                                            }
                                                                        }
                                                                }
                                                            }
                                                        }

                                                        if let subcategBur = subCategoryBurmese as? [Dictionary<String, Any>] {
                                                            for eachCategDic in subcategBur {
                                                                  let sortedSubCategBurmese = eachCategDic.sorted {($0.key as NSString).integerValue < ($1.key as NSString).integerValue}
                                                                    for eachBurObj in sortedSubCategBurmese {
                                                                        if eachBurObj.key == obj.key {
                                                                            subcategory.subCategoryBurmeseName = eachBurObj.value as! String
                                                                            break
                                                                        }
                                                                    }
                                                            }
                                                        }
                                                      
                                                   
                                                        subcateArr.append(subcategory)
                                                    }
                                            }
                                            
                                    }
                                        category.subCategoryList = subcateArr
                                        self.categoriesList.append(category)
                                }
                                }
                                
                            }
                        } catch {
                            println_debug("something went wrong when extract data from township json file")
                        }
                    }
                }
            } catch {
                println_debug("something went wrong when extract data from citylist json file")
            }
        }
 
    }
    
    class func collapseCategories() {
        for category in self.categoriesList {
            if category.isExpand {
                category.isExpand = false
            }
        }
    }
}

class CategoryDetail: NSObject {
    override init() {
        super.init()
    }
    var categoryCode: String = ""
    var categoryName: String = ""
    var categoryBurmeseName: String = ""
    var categoryEmoji: String = ""
    var subCategoryList = [SubCategoryDetail]()
    var isExpand  : Bool = false
    var category_image = ""
}

class SubCategoryDetail: NSObject {
    override init() {
        super.init()
    }
    var mainCategoryCode: String = ""
    var subCategoryCode: String = ""
    var subCategoryName: String = ""
    var subCategoryBurmeseName: String = ""
    var subCategoryEmoji: String = ""
}
