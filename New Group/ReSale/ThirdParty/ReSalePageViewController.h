//
//  PageViewController.h
//

#import <UIKit/UIKit.h>
@class ReSalePageViewController;

@protocol ReSalePageViewControllerDelegates <NSObject>

@optional
-(void)didScrollToIndex:(NSInteger)index;

@end

@interface ReSalePageViewController : UIPageViewController

@property(weak,nonatomic) id<ReSalePageViewControllerDelegates> mDelegate;

@property(assign,nonatomic,getter=isCircular) BOOL circular;
@property(nonatomic,strong) NSArray<UIViewController*> *arrViewController;
@property(assign,nonatomic,readonly) NSInteger currentIndex;



-(void)getToIndex:(NSInteger)index animated:(BOOL)animated;

@end
