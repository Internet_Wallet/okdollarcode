//
//  ResaleMyNumViewController.swift
//  OK
//
//  Created by Vinod's MacBookPro on 12/28/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import IQKeyboardManagerSwift

class ResaleMyNumViewController: OKBaseController,UITextFieldDelegate {
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    
    @IBOutlet weak var userNumTf: SkyFloatingLabelTextField!
    {
        didSet
        {
            userNumTf.font = UIFont(name: appFont, size: appFontSize)
            userNumTf.placeholder = userNumTf.placeholder?.localized
        }
    }
    @IBOutlet weak var sellingAmountTf: SkyFloatingLabelTextField!
        {
        didSet
        {
            sellingAmountTf.font = UIFont(name: appFont, size: appFontSize)
            sellingAmountTf.placeholder = sellingAmountTf.placeholder?.localized
        }
    }
    @IBOutlet weak var sellingPriceTf: SkyFloatingLabelTextField!
        {
        didSet
        {
            sellingPriceTf.font = UIFont(name: appFont, size: appFontSize)
            sellingPriceTf.placeholder = sellingPriceTf.placeholder?.localized
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
        {
        didSet
        {
            self.submitBtn.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
            self.submitBtn.setTitle(self.submitBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    var serverDestinationNumber = ""
    var telcoName = ""
    var destinationNumber = ""
    var sellingAmt = ""
    var sellingPrice = ""
    var topUpNumber = ""
    var tokenFullString = ""
    var DisplayStr = ""
    var strEnterAmountValue = ""

    override func viewDidLoad() {
        
        super.viewDidLoad()
        sellingAmountTf.becomeFirstResponder()
        
        submitBtn.isHidden = true
        
        view1.layer.cornerRadius = 5
        view1.layer.borderWidth = 1
        view1.layer.borderColor = UIColor.clear.cgColor
        
        view2.layer.cornerRadius = 5
        view2.layer.borderWidth = 1
        view2.layer.borderColor = UIColor.clear.cgColor

        view3.layer.cornerRadius = 5
        view3.layer.borderWidth = 1
        view3.layer.borderColor = UIColor.clear.cgColor
        view3.isHidden = true

        userNumTf.isUserInteractionEnabled = false
        userNumTf.text = " 0\(UserModel.shared.mobileNo.dropFirst(4))"
        self.HideAllViews()
        
        //Manage Keyboard
        IQKeyboardManager.sharedManager().enable            = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
        sellingAmountTf.addTarget(self, action: #selector(ResaleMyNumViewController.textFieldDidChange(_:)),
        for: UIControl.Event.editingChanged)
    }    
    
    // # MARK: UITextField Delegates
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        let dataWithoutComma = self.removeCommaFromDigit(textField.text!)

        //For Text Field Amount Update
        sellingAmountTf.text = self.getDigitDisplay(dataWithoutComma)
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        if textField == sellingAmountTf  {
            
            textField.text = ""
            view3.isHidden = false
        }
        return true
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == sellingAmountTf{
            
            println_debug("should begin editing")
        }
        if textField == sellingPriceTf {
            
            sellingPriceTf.resignFirstResponder()
            self.gettingToken()
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text: String? = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        println_debug("typing string :: \(String(describing: text))")
        
        if textField == sellingAmountTf  {            
            
            if text?.count == 1 && text == "0" {
                
                return false
            }
            if (text?.count ?? 0) == 6{
                sellingPriceTf.text = ""
                
            }
            
            if (text?.count ?? 0) > 3 {
                
                view3.isHidden = false
                
            }else {
                
               view3.isHidden = true
                sellingPriceTf.text = ""
            }
            
            if (text?.count ?? 0) > 6 {
                return false
            }
        }
        
        return true
    }
    
    func gettingToken()  {
        
        println_debug("Action")
        sellingPriceTf.resignFirstResponder()
        
        let sellingAmt = Int(self.removeCommaFromDigit(self.sellingAmountTf.text!))
        
        if(sellingAmt! > 10000)
        {
            alertViewObj.wrapAlert(title: nil, body: "Cant able to sell above 10000 MMK".localized, img: #imageLiteral(resourceName: "dashboard_re_sale"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                self.submitBtn.isHidden = true
            })
            alertViewObj.showAlert(controller: self)
            sellingPriceTf.resignFirstResponder()
        }
        else
        {
        getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
            guard isSuccess, let tokenAuthStr = tokenString else {
                self.showErrorAlert(errMessage: "Try Again".localized)
                return
            }
            
            DispatchQueue.main.async(execute: {
                
                var telcoName = self.getOparatorName(UserModel.shared.mobileNo)
                
                if telcoName ==  "MPT"
                {
                    telcoName = "Mpt"
                }
                
                 if telcoName == "MyTel" {
                    telcoName = "Mytel"
                }
                if telcoName == "MecTel" {
                    telcoName = "Mectel"
                }
                    let apiUrl = URL(string: String(format: Url.AirtimeGet_PriceAPI, telcoName, self.removeCommaFromDigit(self.sellingAmountTf.text!)))
                    self.getSellingPrice(apiUrl: apiUrl!, authenticationStr: tokenAuthStr as! String)
                
            })
            
        })
        }
    }
    
    
    func getTokenFromServer(completionHandler: @escaping (Bool,Any?) -> Void){
        
        let urlStr = String.init(format: Url.airtimeTokenApi)
        let url: URL? = getUrl(urlStr: urlStr, serverType: .airtimeUrl)
        
        guard let tokenUrl = url else {
            showErrorAlert(errMessage: "Something went wrong : Dev need to check".localized)
            return
        }
        
        let hashValue = Url.aKey_airtime.hmac_SHA1(key: Url.sKey_airtime)
        let inputVal = "password=\(hashValue)&grant_type=password"
        
        progressViewObj.showProgressView()
        println_debug("get token from server called........")
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: kMethod_Post, contentType: kContentType_urlencoded, inputVal: inputVal, authStr: nil)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    println_debug("remove progress view called inside get token from server call")
                }
                
                completionHandler(false,nil)
                return
            }
            
            println_debug("response dict for get token from server :: \(String(describing: response))")
            let responseDic = response as! NSDictionary
            guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                
                completionHandler(false,nil)
                return
            }
            
            let authorizationString =  "\(tokentype) \(accesstoken)"
            self.tokenFullString = authorizationString
            completionHandler(true,authorizationString)
            
    
        })
    }
    
    func getSellingPrice(apiUrl: URL, authenticationStr: String) {
        
        let telco = self.getOparatorName(UserModel.shared.mobileNo)

        if telco == "Telenor" {
            
            let telenorAmount = self.removeCommaFromDigit(self.sellingAmountTf.text!)
            
            if Int(telenorAmount) ?? 0 <= 1999 {
                
                alertViewObj.wrapAlert(title: nil, body: "Cant able to sell below 2000.0MMK".localized, img: #imageLiteral(resourceName: "dashboard_re_sale"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    self.submitBtn.isHidden = true
                })
                alertViewObj.showAlert(controller: self)
                progressViewObj.removeProgressView()
                
                return
            }
            
        }
        
        println_debug("calling api :::: \(apiUrl)")
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: apiUrl, methodType: kMethod_Get, contentType: kContentType_Json, inputVal: nil, authStr: authenticationStr)
        
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
            println_debug("progress view started")
        }
        
        JSONParser.GetResaleApiResponse(apiUrlReq: urlRequest) { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    println_debug("remove progress view called inside cashin main api call with error")
                    progressViewObj.removeProgressView()
                }
                self.showErrorAlert(errMessage: "Try Again".localized)
                return
            }
            println_debug("response dict for selling price list :: \(String(describing: response))")
            
            DispatchQueue.main.async(execute: {
                if let responseDic = response as? Dictionary<String,Any> {
                    if  let val = responseDic["SellingAmount"] as? NSNumber {
                        
                        self.sellingPriceTf.text = val.stringValue
                        
                        DispatchQueue.main.async(){
                        
                            let formatedAlert = "Allow to selling price only ".localized + val.stringValue + " MMK"
                            
                            alertViewObj.wrapAlert(title: nil, body: formatedAlert, img: #imageLiteral(resourceName: "call"))
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                self.submitBtn.isHidden = false

                            })
                        alertViewObj.showAlert(controller: self)
                        }
                    }
                    
                    self.serverDestinationNumber = (responseDic["DestinationNumber"] as? String)!
                    self.telcoName = (responseDic["Telco"] as? String)!
                    self.destinationNumber = ""
                    self.sellingAmt = (responseDic["AirTimeAmount"] as? NSNumber)!.stringValue
                    self.sellingPrice = (responseDic["SellingAmount"] as? NSNumber)!.stringValue
                    self.topUpNumber = UserModel.shared.mobileNo
                }
            })
            
            progressViewObj.removeProgressView()
        }

    }
    
    @IBAction func submitAction(_ sender: Any) {
        
        println_debug(" submit Action")
        
        callOrderPlaceApi(completionHandler: { (isSuccess, tokenString) in
            guard isSuccess, let tokenAuthStr = tokenString else {
                self.showErrorAlert(errMessage: "Try Again".localized)
                return
            }
            DispatchQueue.main.async(execute: {
                
                let telcoName  = self.getOparatorName(UserModel.shared.mobileNo)
                let sellingAmt = self.removeCommaFromDigit(self.sellingAmountTf.text!)

                //let sellingAmt = self.sellingAmountTf.text!
                let apiUrl = URL(string: String(format: Url.AirtimeGet_PriceAPI, telcoName, sellingAmt))
                
                self.getSellingPrice(apiUrl: apiUrl!, authenticationStr: tokenAuthStr as! String)
            })
        })
    }
 
    func callOrderPlaceApi(completionHandler: @escaping (Bool,Any?) -> Void){
        
        println_debug("My Number Submit Action")
        let airtimAmt = self.removeCommaFromDigit(self.sellingAmountTf.text!)

        //let airtimAmt = self.sellingAmountTf.text!
 
        let pipeLineStr = "AirTimeAmount=\(airtimAmt)|DestinationPhoneNumber=\(serverDestinationNumber)|GcmRegistrationId=0000|OkPhoneNumber=\(UserModel.shared.mobileNo)|PhoneNumber=\(UserModel.shared.mobileNo)|TelcoName=\(telcoName)"
        
        let hashValue: String = pipeLineStr.hmac_SHA1(key: Url.sKey_airtime)
        
        println_debug("revised hash value :: \(hashValue)")
        
        let keys : [String] = ["HashValue","AirTimeAmount","DestinationPhoneNumber","GcmRegistrationId","OkPhoneNumber","PhoneNumber","TelcoName"]
        let values : [String] = [hashValue,airtimAmt,serverDestinationNumber,"0000",UserModel.shared.mobileNo,topUpNumber,telcoName]
        let jsonObj = JSONStringWriter()
        let jsonStr = jsonObj.writeJsonIntoString(keys, values)
 
        let tokenStr = self.tokenFullString
        println_debug("token string :: \(tokenStr)")
        
        let url = URL(string: String(format: Url.Airtime_OrderPlaceAPI, UserModel.shared.mobileNo))
        println_debug("selling price url :: \(String(describing: url))")
        
        var request = URLRequest.init(url: url!, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
        request.httpMethod = "POST"
        request.addValue(kContentType_Json, forHTTPHeaderField: "Content-Type")
        request.addValue(kContentType_Json, forHTTPHeaderField: "Accept")
        
        request.addValue(tokenStr, forHTTPHeaderField: "Authorization")
        request.httpBody = jsonStr.data(using: String.Encoding.utf8)!
        
        JSONParser.GetResaleApiResponse(apiUrlReq: request) { (isSuccess, response) in
           
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    println_debug("remove progress view called inside get token from server call")
                }
                completionHandler(false,nil)
                return
            }
            println_debug("response dict for get token from server :: \(String(describing: response))")
            
            let responseDic = response as! NSDictionary
            println_debug("response dict :: \(String(describing: responseDic))")
            self.gettingUSDCODE(self.tokenFullString)
        }
    }
    
    func gettingUSDCODE(_ tokenDict: String) {
        
            DispatchQueue.main.async(execute: {() -> Void in
                progressViewObj.showProgressView()
            })
                destinationNumber = UserModel.shared.mobileNo
            
        let tokenStr = tokenDict
            println_debug("tokenStr: \(tokenStr)")
        
            let url = URL(string: String(format: Url.AirtimeGetTelcoAPI, telcoName))
           println_debug("selling price url :: \(String(describing: url))")
        
        var request = URLRequest.init(url: url!, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
        request.httpMethod = "GET"
        request.addValue(kContentType_Json, forHTTPHeaderField: "Content-Type")
        request.addValue(kContentType_Json, forHTTPHeaderField: "Accept")
        
        request.addValue(tokenStr, forHTTPHeaderField: "Authorization")
        //request.httpBody = tokenStr.data(using: String.Encoding.utf8)!
        
        JSONParser.GetResaleApiResponse(apiUrlReq: request) { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    println_debug("remove progress view called inside get token from server call")
                }
                // completionHandler(false,nil)
                return
            }
            println_debug("response dict for get token from server :: \(String(describing: response))")
            
            let responseDic = response as! NSDictionary
            println_debug("response dict :: \(String(describing: responseDic))")
            
            if let response = responseDic.value(forKey: "UssdDetails") as? Array<Dictionary<String, Any>> {
                for  tempDict in response {
                    if "FundTransfer"  == tempDict["UssdName"]  as? String {
                    if let ussdCode = tempDict.safeValueForKey("UssdDialingCode") as? String {
                        DispatchQueue.main.async(){
                            
                            var formatedAlert = ""
            
                            var num = self.serverDestinationNumber
                            
                            if num.hasPrefix("0095") {
                                num = "0" + num.dropFirst(4)
                            }
                            
                            
                            if self.telcoName == "Telenor"{
                                
                            if ussdCode == "*979*4#" {
                                    
                            let code = "*979*4*1*"
                                    
                            formatedAlert = "Please Transfer Main Balance Selling Amount as below format ".localized + "*979*4*1*" + self.sellingAmt + "*" + num + "#" + "\n Copy the USSD \n Paste in Dial pad & Call".localized
                                self.DisplayStr = "\(code)\(num)*\(self.sellingAmt)#"
                                
                                }else {
                                
                         formatedAlert = "Please Transfer Main Balance Selling Amount as below format ".localized + ussdCode + self.sellingAmt + "*" + num + "#" + "\n Copy the USSD \n Paste in Dial pad & Call".localized

                        self.DisplayStr = "\(ussdCode)\(self.sellingAmt)*\(num)#"
                                }
                                
                            }else {
                            
                             formatedAlert = "Please Transfer Main Balance Selling Amount as below format ".localized + ussdCode + self.sellingAmt + "*" + num + "#" + "\n Copy the USSD \n Paste in Dial pad & Call".localized

                        self.DisplayStr = "\(ussdCode)\(self.sellingAmt)*\(num)#"
                            }
                        alertViewObj.wrapAlert(title: nil, body: formatedAlert, img: #imageLiteral(resourceName: "call"))
                        alertViewObj.addAction(title: "Copy".localized, style: .target , action: {
                            
                            let pb = UIPasteboard.general
                            pb.string = self.DisplayStr
                            self.HideAllViews()
                        })

                        alertViewObj.showAlert(controller: self)
                        progressViewObj.removeProgressView()
                        }
                    }
                        break
                    }
                }
            }
        }
    }
    
    func HideAllViews() {
        sellingAmountTf.text = ""
        sellingPriceTf.text = ""
        view3.isHidden = true
        view3.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
