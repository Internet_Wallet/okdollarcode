//
//  ResaleOtherNumViewController.swift
//  OK
//
//  Created by Vinod's MacBookPro on 12/28/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import IQKeyboardManagerSwift

class ResaleOtherNumViewController: OKBaseController,UITextFieldDelegate {
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var userNumberTf: SkyFloatingLabelTextField!
        {
        didSet
        {
            userNumberTf.font = UIFont(name: appFont, size: appFontSize)
            userNumberTf.placeholder = userNumberTf.placeholder?.localized
        }
    }
    @IBOutlet weak var enterMobileNumTf: SkyFloatingLabelTextField!
        {
        didSet
        {
            enterMobileNumTf.font = UIFont(name: appFont, size: appFontSize)
            enterMobileNumTf.placeholder = enterMobileNumTf.placeholder?.localized
        }
    }
    @IBOutlet weak var confirmMobileNumTf: SkyFloatingLabelTextField!
        {
        didSet
        {
            confirmMobileNumTf.font = UIFont(name: appFont, size: appFontSize)
            confirmMobileNumTf.placeholder = confirmMobileNumTf.placeholder?.localized
        }
    }
    @IBOutlet weak var sellingPriceTf: SkyFloatingLabelTextField!
        {
        didSet
        {
            sellingPriceTf.font = UIFont(name: appFont, size: appFontSize)
            sellingPriceTf.placeholder = sellingPriceTf.placeholder?.localized
        }
    }
    @IBOutlet weak var sellingAmountTf: SkyFloatingLabelTextField!
        {
        didSet
        {
            sellingAmountTf.font = UIFont(name: appFont, size: appFontSize)
            sellingAmountTf.placeholder = sellingAmountTf.placeholder?.localized
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
        {
        didSet
        {
            self.submitBtn.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
            self.submitBtn.setTitle(self.submitBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    let validObj  = PayToValidations()

    var serverDestinationNumber = ""
    var telcoName = ""
    var destinationNumber = ""
    var sellingAmt = ""
    var sellingPrice = ""
    var topUpNumber = ""
    var tokenFullString = ""
    var DisplayStr = ""
    var strEnterAmountValue = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        enterMobileNumTf.becomeFirstResponder()
        
        view1.layer.cornerRadius = 5
        view1.layer.borderWidth = 1
        view1.layer.borderColor = UIColor.clear.cgColor
        
        view2.layer.cornerRadius = 5
        view2.layer.borderWidth = 1
        view2.layer.borderColor = UIColor.clear.cgColor
        
        view3.layer.cornerRadius = 5
        view3.layer.borderWidth = 1
        view3.layer.borderColor = UIColor.clear.cgColor
        
        view4.layer.cornerRadius = 5
        view4.layer.borderWidth = 1
        view4.layer.borderColor = UIColor.clear.cgColor
        
        view5.layer.cornerRadius = 5
        view5.layer.borderWidth = 1
        view5.layer.borderColor = UIColor.clear.cgColor
        
        userNumberTf.isUserInteractionEnabled = false
        userNumberTf.text = "0\(UserModel.shared.mobileNo.dropFirst(4))"//"+95 \(UserModel.shared.mobileNo.dropFirst(4))"
        submitBtn.isHidden = true
        self.HideAllViews()
        
        //Manage Keyboard
        IQKeyboardManager.sharedManager().enable            = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
        sellingAmountTf.addTarget(self, action: #selector(ResaleOtherNumViewController.textFieldDidChange(_:)),
                                  for: UIControl.Event.editingChanged)
    }

    
    // MARK: UITextField Delegates
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        let dataWithoutComma = self.removeCommaFromDigit(textField.text!)
        
        //For Text Field Amount Update
        sellingAmountTf.text = self.getDigitDisplay(dataWithoutComma)
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        if textField == enterMobileNumTf  {
            textField.text = commonPrefixMobileNumber
            view3.isHidden = true
            view4.isHidden = true
            view5.isHidden = true
            enterMobileNumTf.text = commonPrefixMobileNumber
            
        } else if textField == confirmMobileNumTf {
            textField.text = commonPrefixMobileNumber
            view4.isHidden = true
            view5.isHidden = true
            confirmMobileNumTf.text = commonPrefixMobileNumber
            
        } else if textField == sellingAmountTf {
            textField.text = ""
            view5.isHidden = true
            
        }
        
        return true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == sellingPriceTf {
            
            sellingPriceTf.resignFirstResponder()
            self.gettingToken()
        }
        
        if textField == sellingAmountTf {
            println_debug("should begin editing")
            
        }
        
        if enterMobileNumTf.text?.count == 0 || confirmMobileNumTf.text?.count == 0 {
            if textField == enterMobileNumTf || textField == confirmMobileNumTf {
                textField.text = commonPrefixMobileNumber
            }
        }
    return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text: String? = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        println_debug("typing string :: \(String(describing: text))")
        
        if textField == enterMobileNumTf {
            if text?.length == 0 {
                textField.text = commonPrefixMobileNumber
                return false
            }
            
            if text?.count == 1 {
                if (string == "") {
                    return false
                }
                else {
                    textField.text = commonPrefixMobileNumber
                }
            }
            println_debug("Mobile number validation")
            
            let strTotalLength = (textField.text! + string)
            guard validObj.getNumberRangeValidation(strTotalLength).isRejected == false else {
                alertViewObj.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: UIImage(named : "AppIcon"))
                alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                    textField.text = commonPrefixMobileNumber
                })
                alertViewObj.showAlert(controller: self)
                return false
            }
            
            if validObj.getNumberRangeValidation(strTotalLength).max >= strTotalLength.count {
                if (text?.count ?? 0) > 7 {
                    
                    view3.isHidden = false
                    
                } else {
                    view3.isHidden = true
                    confirmMobileNumTf.text = ""
                    view4.isHidden = true
                    view5.isHidden = true
                }
                return true
            }
            return false
            
        } else if textField == confirmMobileNumTf {
            
            if text?.length == 0 {
                textField.text = commonPrefixMobileNumber
                return false
            }
            if text?.count == 1 {
                if (string == "") {
                    return false
                }
                else {
                    textField.text = commonPrefixMobileNumber
                }
            }
            
            if textField == confirmMobileNumTf {
                
                let enterNumberStr: String = enterMobileNumTf.text!
                let isAllow: Bool = checkConfirmNumberIsEqual(text!, checkWithNum: enterNumberStr)
                if isAllow {
                    if (text == enterNumberStr) {
                        textField.text = text
                        textField.resignFirstResponder()
                        view4.isHidden = false
                        return false
                    }
                }
                else {
                   // textField.text = "09"
                    return false
                }
            }
            
            if (text?.count ?? 0) > 7 {

                view4.isHidden = false

            } else {
                view4.isHidden = true
                sellingAmountTf.text = ""
                view5.isHidden = true
            }
            if (text?.count ?? 0) > 11 {
                return false
            }
        }
       
      else  if textField == sellingAmountTf  {
            
            if text?.count == 1 && text == "0" {
                
                return false
            }
            if (text?.count ?? 0) == 6 {
                sellingPriceTf.text = ""
            }
            
            if (text?.count ?? 0) > 3 {
                
                view5.isHidden = false
                
            } else {
                
                view5.isHidden = true
                sellingPriceTf.text = ""
            }
            
            if (text?.count ?? 0) > 6 {
                return false
            }
        }
        
        return true
    }
    
    
    func HideAllViews()   {
        
        view3.isHidden = true
        view4.isHidden = true
        view5.isHidden = true
        enterMobileNumTf.text = ""
        confirmMobileNumTf.text = ""
        sellingAmountTf.text = ""
        sellingPriceTf.text = ""
    }

    
    func checkConfirmNumberIsEqual(_ typingStr: String, checkWithNum enteredNumber: String) -> Bool {
        var isAllow = true
        if (typingStr.count ) > (enteredNumber.count) {
            isAllow = false
            return isAllow
        }
        for i in 0..<(typingStr.count) {
            if enteredNumber[enteredNumber.index(enteredNumber.startIndex, offsetBy: i)] != typingStr[typingStr.index(typingStr.startIndex, offsetBy: i)] {
                isAllow = false
                break
            }
        }
        return isAllow
    }

    // MARK: API Methods

    func gettingToken()  {
        
        println_debug("Action")
        sellingPriceTf.resignFirstResponder()
        
        let sellingAmt = Int(self.removeCommaFromDigit(self.sellingAmountTf.text!))
        
        if(sellingAmt! > 10000)
        {
            alertViewObj.wrapAlert(title: nil, body: "Cant able to sell above 10000 MMK".localized, img: #imageLiteral(resourceName: "dashboard_re_sale"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            alertViewObj.showAlert(controller: self)
            
        }
        else
        {
        getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
            guard isSuccess, let tokenAuthStr = tokenString else {
                self.showErrorAlert(errMessage: "Try Again".localized)
                return
            }
            DispatchQueue.main.async(execute: {
                
                let op = self.enterMobileNumTf.text!
                
                var telcoName = self.getOparatorName(op)
                
                if telcoName == "MPT" {
                    telcoName = "Mpt"
                }
                if telcoName == "MyTel" {
                    telcoName = "Mytel"
                }
                
                if telcoName == "MecTel" {
                    telcoName = "Mectel"
                }
                
                let sellingAmt = self.removeCommaFromDigit(self.sellingAmountTf.text!)

                let apiUrl = URL(string: String(format: Url.AirtimeGet_PriceAPI, telcoName, sellingAmt))
                
                self.getSellingPrice(apiUrl: apiUrl!, authenticationStr: tokenAuthStr as! String)
            })
        })
        }
        
    }
    
    
    func getTokenFromServer(completionHandler: @escaping (Bool,Any?) -> Void){
        
        let urlStr = String.init(format: Url.airtimeTokenApi)
        let url: URL? = getUrl(urlStr: urlStr, serverType: .airtimeUrl)
        
        guard let tokenUrl = url else {
            showErrorAlert(errMessage: "Something went wrong : Dev need to check")
            return
        }
        
        let hashValue = Url.aKey_airtime.hmac_SHA1(key: Url.sKey_airtime)
        let inputVal = "password=\(hashValue)&grant_type=password"
        
        progressViewObj.showProgressView()
        println_debug("get token from server called........")
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: kMethod_Post, contentType: kContentType_urlencoded, inputVal: inputVal, authStr: nil)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    println_debug("remove progress view called inside get token from server call")
                }
                completionHandler(false,nil)
                return
            }
            println_debug("response dict for get token from server :: \(String(describing: response))")
            let responseDic = response as! NSDictionary
            guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                
                completionHandler(false,nil)
                return
            }
            
            let authorizationString =  "\(tokentype) \(accesstoken)"
            self.tokenFullString = authorizationString
            completionHandler(true,authorizationString)
            
        })
    }
    
    func getSellingPrice(apiUrl: URL, authenticationStr: String) {
        
        
        let num = enterMobileNumTf.text ?? "0"
        let telco = self.getOparatorName(num)
        
        if telco == "Telenor" {
            
            let telenorAmount = self.removeCommaFromDigit(self.sellingAmountTf.text!)
            
            if Int(telenorAmount) ?? 0 <= 1999 {
                
                alertViewObj.wrapAlert(title: nil, body: "Cant able to sell below 2000.0MMK".localized, img: #imageLiteral(resourceName: "dashboard_re_sale"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    
                })
                alertViewObj.showAlert(controller: self)
                progressViewObj.removeProgressView()
                return
            }
            
        }
        
        println_debug("calling api :::: \(apiUrl)")
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: apiUrl, methodType: kMethod_Get, contentType: kContentType_Json, inputVal: nil, authStr: authenticationStr)
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
            println_debug("progress view started")
        }
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    println_debug("remove progress view called inside cashin main api call with error")
                    progressViewObj.removeProgressView()
                }
                self.showErrorAlert(errMessage: "Try Again".localized)
                return
            }
            println_debug("response dict for selling price list :: \(String(describing: response))")
            
            DispatchQueue.main.async(execute: {
                if let responseDic = response as? Dictionary<String,Any> {
                    if  let val = responseDic["SellingAmount"] as? NSNumber {
                        self.sellingPriceTf.text = val.stringValue
                        self.serverDestinationNumber = (responseDic["DestinationNumber"] as? String)!
                        self.telcoName = (responseDic["Telco"] as? String)!
                        // self.destinationNumber = ""
                        self.sellingPrice = (responseDic["SellingAmount"] as? NSNumber)!.stringValue
                        self.sellingAmt = (responseDic["AirTimeAmount"] as? NSNumber)!.stringValue
                         DispatchQueue.main.async(){
                            
                            let formatedAlert = "Allow to selling price only ".localized + val.stringValue + " MMK"
                            
                        alertViewObj.wrapAlert(title: nil, body: formatedAlert, img: #imageLiteral(resourceName: "phone"))
                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                            self.submitBtn.isHidden = false
                        })
                        alertViewObj.showAlert(controller: self)

                        }
                    }
                    
                    self.topUpNumber = self.enterMobileNumTf.text!
                }
            })
            
            progressViewObj.removeProgressView()
            
        })
    }
    
    
    @IBAction func submitAction(_ sender: Any) {
        
        println_debug(" submit Action")
        
        callOrderPlaceApi(completionHandler: { (isSuccess, tokenString) in
            guard isSuccess, let tokenAuthStr = tokenString else {
                self.showErrorAlert(errMessage: "Try Again".localized)
                return
            }
            DispatchQueue.main.async(execute: {
                
                let telcoName = self.getOparatorName(UserModel.shared.mobileNo)
                //let sellingAmt = self.sellingAmountTf.text!
                let sellingAmt = self.removeCommaFromDigit(self.sellingAmountTf.text!)

                let apiUrl = URL(string: String(format: Url.AirtimeGet_PriceAPI, telcoName, sellingAmt))
                
                self.getSellingPrice(apiUrl: apiUrl!, authenticationStr: tokenAuthStr as! String)
            })
        })
    }
        
    func callOrderPlaceApi(completionHandler: @escaping (Bool,Any?) -> Void){
        
        println_debug("Other Number Submit Action")
        
        var destinationNum = self.enterMobileNumTf.text!
        
        if destinationNum.hasPrefix("09"){
            destinationNum = "0095" + destinationNum.dropFirst()
        }
        
        let pipeLineStr = "DestinationPhoneNumber=\(serverDestinationNumber)|TelcoName=\(telcoName)|GcmRegistrationId=0000|OkPhoneNumber=\(destinationNum)|AirTimeAmount=\(sellingAmt)|PhoneNumber=\(UserModel.shared.mobileNo)"
        println_debug("pipeline str :: \(pipeLineStr)")
        
        let hashValue: String = pipeLineStr.hmac_SHA1(key: Url.sKey_airtime)
        println_debug("actual hash value :: \(hashValue)")
        
        let keys : [String] = ["HashValue","DestinationPhoneNumber","TelcoName","GcmRegistrationId","OkPhoneNumber","AirTimeAmount","PhoneNumber"]
        let values : [String] = [hashValue,serverDestinationNumber,telcoName,"0000",destinationNum,sellingAmt,UserModel.shared.mobileNo]

        
        let jsonObj = JSONStringWriter()
        let jsonStr = jsonObj.writeJsonIntoString(keys, values)
        
        let tokenStr = self.tokenFullString
        println_debug("token string :: \(tokenStr)")
        
        let url = URL(string: String(format: Url.Airtime_OrderPlaceAPI, UserModel.shared.mobileNo))
        println_debug("selling price url :: \(String(describing: url))")
        
        var request = URLRequest.init(url: url!, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
        request.httpMethod = "POST"
        request.addValue(kContentType_Json, forHTTPHeaderField: "Content-Type")
        request.addValue(kContentType_Json, forHTTPHeaderField: "Accept")
        
        request.addValue(tokenStr, forHTTPHeaderField: "Authorization")
        request.httpBody = jsonStr.data(using: String.Encoding.utf8)!
        
        JSONParser.GetResaleApiResponse(apiUrlReq: request) { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    println_debug("remove progress view called inside get token from server call")
                }
                completionHandler(false,nil)
                return
            }
            println_debug("response dict for get token from server :: \(String(describing: response))")
            
            let responseDic = response as! NSDictionary
            println_debug("response dict :: \(String(describing: responseDic))")
            self.gettingUSDCODE(self.tokenFullString)
        }
    }
    
    
    func gettingUSDCODE(_ tokenDict: String) {
        
        DispatchQueue.main.async(execute: {() -> Void in
            progressViewObj.showProgressView()
            self.destinationNumber = self.enterMobileNumTf.text!
        })
        let tokenStr = tokenDict
        println_debug("tokenStr: \(tokenStr)")
        
        let url = URL(string: String(format: Url.AirtimeGetTelcoAPI, telcoName))
        println_debug("selling price url :: \(String(describing: url))")
        
        var request = URLRequest.init(url: url!, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
        request.httpMethod = "GET"
        request.addValue(kContentType_Json, forHTTPHeaderField: "Content-Type")
        request.addValue(kContentType_Json, forHTTPHeaderField: "Accept")
        
        request.addValue(tokenStr, forHTTPHeaderField: "Authorization")
        //request.httpBody = tokenStr.data(using: String.Encoding.utf8)!
        
        JSONParser.GetResaleApiResponse(apiUrlReq: request) { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    println_debug("remove progress view called inside get token from server call")
                }
                // completionHandler(false,nil)
                return
            }
            println_debug("response dict for get token from server :: \(String(describing: response))")
            
            let responseDic = response as! NSDictionary
            println_debug("response dict :: \(String(describing: responseDic))")
            
            if let response = responseDic.value(forKey: "UssdDetails") as? Array<Dictionary<String, Any>> {
                for  tempDict in response {
                    if "FundTransfer"  == tempDict["UssdName"]  as? String {
                    if let ussdCode = tempDict.safeValueForKey("UssdDialingCode") as? String {
                        
                        var num = self.serverDestinationNumber
                        var formatedAlert = ""
                        
                        if num.hasPrefix("0095") {
                            num = "0" + num.dropFirst(4)
                        }
                        
                        
                         DispatchQueue.main.async(){
                            

                                if self.telcoName == "Telenor"{
                                    
                                if ussdCode == "*979*4#" {
                                        
                                let code = "*979*4*1*"
                                        
                                formatedAlert = "Please Transfer Main Balance Selling Amount as below format ".localized + "*979*4*1*" + self.sellingAmt + "*" + num + "#" + "\n Copy the USSD \n Paste in Dial pad & Call".localized
                                    self.DisplayStr = "\(code)\(num)*\(self.sellingAmt)#"
                                    
                                    }else {
                                    
                             formatedAlert = "Please Transfer Main Balance Selling Amount as below format ".localized + ussdCode + self.sellingAmt + "*" + num + "#" + "\n Copy the USSD \n Paste in Dial pad & Call".localized

                            self.DisplayStr = "\(ussdCode)\(self.sellingAmt)*\(num)#"
                                    }
                                    
                                }
                                else {
                            
                             formatedAlert = "Please Transfer Main Balance Selling Amount as below format ".localized + ussdCode + self.sellingAmt + "*" + num + "#" + "\n Copy the USSD \n Paste in Dial pad & Call".localized
                            
                        self.DisplayStr = "\(ussdCode)\(self.sellingAmt)*\(num)#"
                            }
                            
                        alertViewObj.wrapAlert(title: nil, body: formatedAlert, img: #imageLiteral(resourceName: "call"))
                        alertViewObj.addAction(title: "Copy".localized, style: .target , action: {
                            let pb = UIPasteboard.general
                            pb.string = self.DisplayStr
                            self.HideAllViews()
                            
                        })
                        
                        alertViewObj.showAlert(controller: self)
                        progressViewObj.removeProgressView()
                        }
                    }
                    break
                    }
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
