//
//  ResaleViewController.swift
//  OK
//
//  Created by Vinod's MacBookPro on 12/28/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ResaleViewController: OKBaseController,ReSalePageViewControllerDelegates {

    //Handle Page View control
    @IBOutlet weak var container: ReSalePageViewController!
    @IBOutlet weak var viewTabHolder: UIView!

    @IBOutlet weak var lblMyNumber: UILabel!
    {
        didSet
            {
               self.lblMyNumber.font = UIFont(name: appFont, size: appFontSize)
               self.lblMyNumber.text = "My Number".localized
        }
    }
    @IBOutlet weak var lblOtherNumber: UILabel!
    {
        didSet
            {
                self.lblOtherNumber.font = UIFont(name: appFont, size: appFontSize)
                self.lblOtherNumber.text = "Other Number".localized
        }
    }
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "Re-Sale Phone Bill".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]

        println_debug("My Number view")
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        
        // Do any additional setup after loading the view.
        container.getTo(0, animated: true)
        for case let view in viewTabHolder.subviews {
            for case let subData in view.subviews {
                if let button = subData as? UIButton {
                    if(button.tag==0){
                        button.isSelected=true
                        break
                    }
                }
            }
        }
        
        decrytingUDIDForServer()
        
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //
    // MARK: - Pageview Controller Delegate
    //
    
    func didScroll(to index: Int) {
        
        for case let view in viewTabHolder.subviews {
            for case let subData in view.subviews {
                if let button = subData as? UIButton {
                    
                    if(button.tag==index){
                        button.isSelected=true
                    }else{
                        button.isSelected=false
                    }
                }
            }
        }
    }
    
    @IBAction func btnActionTab(_ sender: Any) {
        
        container.getTo((sender as AnyObject).tag, animated: true)
        
        for case let view in viewTabHolder.subviews {
            for case let subData in view.subviews {
                if let button = subData as? UIButton {
                    if(button.tag==(sender as AnyObject).tag){
                        button.isSelected=true
                    }else{
                        button.isSelected=false
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ReSale_EMBEDD_TAB"
        {
            container = segue.destination as? ReSalePageViewController
            
            let commonStoryboard = UIStoryboard.init(name: "Resale", bundle: nil)
            
            let ReSaleMyObj = commonStoryboard.instantiateViewController(withIdentifier: "ResaleMyNumViewController") as! ResaleMyNumViewController

            let ReSaleOtherObj = commonStoryboard.instantiateViewController(withIdentifier: "ResaleOtherNumViewController") as! ResaleOtherNumViewController
            
            container.arrViewController=[ReSaleMyObj,ReSaleOtherObj]
            container.mDelegate = self
        }
    }
    func decrytingUDIDForServer(){
        
        guard let manager = EncryptionUUID.encryptionmanager() as? EncryptionUUID else { return }
        // for bug fixing server side
        // formate :::: "0C569471-378D-0DAD-A31D-F5786AC8B343" "16F99583-44FD-0C33-B81E-55D5AO59F05D"
        let userID1 = "FBC2F7CC-A10C-0436-AECD-171A1B8F41FA"
        let userID2 = "CEB61C71-87BB-0F2B-74A3-8EF066FB3750"
        let userID3 = "FC9EEF23-0968-09D6-376E-18608FB46365"
        
        let decryptMsg1 = manager.decryptNumber(userID1)
        let decryptMsg2 = manager.decryptNumber(userID2)
        let decryptMsg3 = manager.decryptNumber(userID3)
        println_debug("In Resale for server decrypted UDID::: \(String(describing: decryptMsg1)) and userID 2 is \(String(describing: decryptMsg2)) and userID 3 is \(String(describing: decryptMsg3))")
        //println_debug(String(describing: decryptMsg))
    }
}
