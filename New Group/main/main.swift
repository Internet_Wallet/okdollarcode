//
//  main.swift
//  OK
//
//  Created by Ashish on 2/18/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

let _ = UIApplicationMain(
    CommandLine.argc,
    CommandLine.unsafeArgv,
    NSStringFromClass(OKSessionValidation.self),
    NSStringFromClass(AppDelegate.self)
)
