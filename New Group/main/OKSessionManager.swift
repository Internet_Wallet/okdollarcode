//
//  OKSessionManager.swift
//  OK
//
//  Created by Ashish on 2/17/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class OKSessionValidation: UIApplication {
    
    private var timeout: TimeInterval {
        return 5 * 60
    }
    
    static var isMainAppSessionExpired : Bool = false
    
    private var idleTimer: Timer?
    
    // resent the timer because there was user interaction
    private func resetIdleTimer() {
        
        if let idleTimer = idleTimer {
            idleTimer.invalidate()
        }
        idleTimer = Timer.scheduledTimer(timeInterval: timeout, target: self,  selector: #selector(OKSessionValidation.timeHasExceeded), userInfo: nil, repeats: false
        )
         if userDef.bool(forKey: keyLoginStatus) {
            OKSessionValidation.isMainAppSessionExpired = false
        }
        
    }
    
    // if the timer reaches the limit as defined in timeoutInSeconds, post this notification
    @objc private func timeHasExceeded() {
        if userDef.bool(forKey: keyLoginStatus) {
            OKSessionValidation.isMainAppSessionExpired = true
            NotificationCenter.default.post(name: .appTimeout,
                                            object: nil
            )
        }
    }
        
    override func sendEvent(_ event: UIEvent) {
        super.sendEvent(event)
        if idleTimer != nil {
            self.resetIdleTimer()
        }
        if let touches = event.allTouches {
            for touch in touches where touch.phase == UITouch.Phase.began {
                self.resetIdleTimer()
            }
        }
    }
}

extension Notification.Name {
    static let appTimeout = Notification.Name("appTimeout")
    static let loginTimeOut = Notification.Name("appLoginSessionExpired")
    static let networkLost = Notification.Name("NetworkLost")
    static let networkConnected = Notification.Name("NetworkConnected")
    static let foregroundEntered = Notification.Name("NetworkConnected")
}

