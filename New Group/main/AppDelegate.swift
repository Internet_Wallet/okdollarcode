    //
    //  AppDelegate.swift
    //  OK
    //
    //  Created by Subhash Arya on 25/08/17.
    //  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
    //
    
    import UIKit
    import CoreData
    import Reachability
    import IQKeyboardManagerSwift
    import CoreTelephony
    import UserNotifications
    import UserNotificationsUI
    import GoogleMaps
    import Firebase
    import FirebaseInstanceID
    import FirebaseMessaging
    
  //  @UIApplicationMain
    class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate {
        var window: UIWindow?
        
           var currentLanguage : String = "my" {
            didSet {
                if currentLanguage == "my" || currentLanguage == "en"{
                    appFont = "Zawgyi-One"
                    appFontSize = 17.0
                    appFontSizeReport = 12.0
                    appButtonSize = 17.0
                    appTitleButton = 15.0
                }else{
                    appFont = "Myanmar3"
                    appFontSize = 12.0
                    appTitleButton = 12.0
                    appFontSizeReport = 12.0
                    appButtonSize = 15.0
                }
            }
        }
        
        
        var localBundle : Bundle?
        var reachability: Reachability?
        private var isNetworkAvail: Bool = false
        var orientationLock = UIInterfaceOrientationMask.portrait
        let networkInfo = CTTelephonyNetworkInfo()
        var floatingButtonControl: FloatingButtonController?
        var isComingFromResignActive = false
        var number = 0
        
        func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
            
          
            let currentCount = UserDefaults.standard.integer(forKey: "launchCount")
            UserDefaults.standard.set(currentCount+1, forKey:"launchCount")
            UserDefaults.standard.set(true, forKey: "isComingFromAppdelegate")
            
            if UserDefaults.standard.value(forKey: "isComingFromResignActive") == nil{
                UserDefaults.standard.set(false, forKey: "isComingFromResignActive")
            }else{
                //force wrapping because m hard coding
                let value = UserDefaults.standard.value(forKey: "isComingFromResignActive") as! Bool
                
                if !value{
                    UserDefaults.standard.set(false, forKey: "isComingFromResignActive")
                }
            }
            
           
            
            self.reachability = Reachability()!
            NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
            do {
                try reachability!.startNotifier()
            } catch {
                println_debug("Unable to start notifier")
            }
            // IQKeyboardManager.sharedManager().enable = false
            IQKeyboardManager.sharedManager().previousNextDisplayMode = .alwaysHide
            //placeholderFont
            IQKeyboardManager.sharedManager().shouldShowToolbarPlaceholder = false
            IQKeyboardManager.sharedManager().placeholderFont = UIFont(name: appFont, size: 16)
            
            //GMSPlacesClient.provideAPIKey(googleAPIKey)
            //getCurrentSimChangeStatus()
            
            self.createShortcutItemsWithIcons()
            getAllContacts()
            
            self.floatingButtonControl = FloatingButtonController()
            self.floatingButtonControl?.button.addTarget(self, action: #selector(AppDelegate.floatingButtonWasTapped), for: .touchUpInside)
            GMSServices.provideAPIKey(googleAPIKey)
            // determine whether we've launched from a shortcut item or not
            // let item = launchOptions?[UIApplicationLaunchOptionsKey.shortcutItem]
            
            self.window?.backgroundColor = UIColor.clear
            // Override point for customization after application launch.
            //Fabric.with([Crashlytics.self])
            // Reachability Handling
            
            // here initilizing the state and divisons for the whole application use - Avaneesh
            _ = TownshipManager.shared
            // here initilizing the Categories for the whole application use - Avaneesh
            _ = CategoriesManager.shared
            
            if UserDefaults.standard.value(forKey: "BuildInstalledDate") == nil {
                println_debug("Very First Time")
                let safeFDate = Date()
                let fDateInString = safeFDate.stringValue(dateFormatIs: "dd MMMM yyyy")
                UserDefaults.standard.set(fDateInString, forKey: "BuildInstalledDate")
            }
            
            if UserDefaults.standard.value(forKey: "currentLanguage") == nil {
                println_debug("Very First Time")
                currentLanguage = "en"
                UserDefaults.standard.set("en", forKey: "currentLanguage")
                if let path = Bundle.main.path(forResource: "en", ofType: "lproj") {
                    localBundle = Bundle(path: path)
                }
            }else {
                
                if let lang = UserDefaults.standard.string(forKey: "currentLanguage"), let path =  Bundle.main.path(forResource: lang, ofType: "lproj") {
                    currentLanguage = lang
                    localBundle = Bundle(path: path)
                }
                //   println_debug(currentLanguage as? String)
                if currentLanguage == "uni" {
                    appFont = "Myanmar3"
                } else {
                    appFont = "Zawgyi-One"
                }
            }
                        
            // session manager
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(AppDelegate.applicationDidTimeout(notification:)),
                                                   name: .appTimeout,
                                                   object: nil
            )
            
            self.shortcutMenuActions(item: launchOptions?[UIApplication.LaunchOptionsKey.shortcutItem])
            
            FirebaseApp.configure()
            
            if #available(iOS 10.0, *) {
                // For iOS 10 display notification (sent via APNS)
                UNUserNotificationCenter.current().delegate = self
                
                let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                UNUserNotificationCenter.current().requestAuthorization(
                    options: authOptions,
                    completionHandler: {_, _ in })
                
            } else {
                let settings: UIUserNotificationSettings =
                    UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                application.registerUserNotificationSettings(settings)
            }
            registerForNotification()
            
            Messaging.messaging().delegate = self
            Messaging.messaging().shouldEstablishDirectChannel = true
            InstanceID.instanceID().instanceID { (result, error) in
                if let error = error {
                    println_debug("Error fetching remote instange ID: \(error)")
                } else if let result = result {
                    println_debug("Remote instance ID token: \(result.token)")
                    //                    self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
                }
            }
            
            
            UIApplication.shared.registerForRemoteNotifications()
            
            let receivedCountValue = UserDefaults.standard.integer(forKey: "ReceivedCount")
            UIApplication.shared.applicationIconBadgeNumber = receivedCountValue
            
            if let option = launchOptions {
                println_debug(option.debugDescription)
                let info = option[UIApplication.LaunchOptionsKey.remoteNotification]
                if (info != nil) {
                    self.goAnotherVC()
                }}
            
            let (networkCode, carrierName) = appDel.getNetworkCode()
            
            if UserDefaults.standard.value(forKey: "MobileNetworkCode") as? String ?? "" == ""{
                UserDefaults.standard.set(networkCode, forKey: "MobileNetworkCode")
                UserDefaults.standard.set(carrierName, forKey: "MobileCarrierName")
            }else{
                notifySimChange()
            }
            
            
            
            return true
        }
        
        func goAnotherVC() {
            //NotificationCenter.default.post(name: Notification.Name("PushToCICO"), object: nil, userInfo: userInfo)
        }
        
        @objc func floatingButtonWasTapped() {
            //floatingButtonController?.button.isHidden = true
            floatingButtonControl?.window.isHidden = true
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "HelpAssistiveButtonTap"), object: nil, userInfo: nil)
        }
        
        func manageApplicationCount(){
            let currentCount = UserDefaults.standard.integer(forKey: "launchCount")
            UserDefaults.standard.set(currentCount+1, forKey:"launchCount")
            UserDefaults.standard.set(true, forKey: "isComingFromAppdelegate")
        }
        
        func applicationWillResignActive(_ application: UIApplication) {
           // isComingFromResignActive = true
            UserDefaults.standard.set(true, forKey: "isComingFromResignActive")

        }
        
        var backgroundTaskIdentifier: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier(rawValue: 0)
        var myTimer: Timer?
        
        func applicationDidEnterBackground(_ application: UIApplication) {
            if isMultitaskingSupported() == false {
                return
            }
            NotificationCenter.default.post(name: .updateTimer, object: nil)
            
            if GlobalConstants.timerValue > 0 {
                myTimer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.timerMethod)), userInfo: nil, repeats: true)
                backgroundTaskIdentifier = application.beginBackgroundTask(expirationHandler: {(_: Void) -> Void in
                    self.endBackgroundTask()
                })
            }
            
            DispatchQueue.main.async {
                PTLoader.shared.hide()
            }
        }
        
        private func shortcutMenuActions(item: Any?) {
            // Shortcut menu functions
            if item != nil {
                if (item as AnyObject).type == "com.cgm.PAYTO" {
                    UserDefaults.standard.setValue("com.cgm.PAYTO", forKey: "UIApplicationLaunchOptionsKey.shortcutItem")
                    UserDefaults.standard.synchronize()
                } else if (item as AnyObject).type == "com.cgm.MyNumber" {
                    UserDefaults.standard.setValue("com.cgm.MyNumber", forKey: "UIApplicationLaunchOptionsKey.shortcutItem")
                    UserDefaults.standard.synchronize()
                } else if (item as AnyObject).type == "com.cgm.OtherNumber" {
                    UserDefaults.standard.setValue("com.cgm.OtherNumber", forKey: "UIApplicationLaunchOptionsKey.shortcutItem")
                    UserDefaults.standard.synchronize()
                } else if (item as AnyObject).type == "com.cgm.ScanQR" {
                    UserDefaults.standard.setValue("com.cgm.ScanQR", forKey: "UIApplicationLaunchOptionsKey.shortcutItem")
                    UserDefaults.standard.synchronize()
                } else if (item as AnyObject).type == "com.cgm.FacePay" {
                    UserDefaults.standard.setValue("com.cgm.FacePay", forKey: "UIApplicationLaunchOptionsKey.shortcutItem")
                    UserDefaults.standard.synchronize()
                }
            } else {
                println_debug("We've launched properly.")
            }
        }
        
        @objc func isMultitaskingSupported() -> Bool {
            var result = false
            if UIDevice.current.responds(to: #selector(self.isMultitaskingSupported)) {
                result = UIDevice.current.isMultitaskingSupported
            }
            return result
        }
        
        @objc func timerMethod(_ paramSender: Timer?) {
            GlobalConstants.timerValue -= 1
            
            let backgroundTimeRemaining: TimeInterval = UIApplication.shared.backgroundTimeRemaining
            if backgroundTimeRemaining == Double.greatestFiniteMagnitude {
                println_debug("Background Time Remaining = Undetermined")
            } else {
                println_debug(String(format: "Background Time Remaining = %.02f Seconds", GlobalConstants.timerValue))
            }
        }

        func endBackgroundTask() {
            let mainQueue = DispatchQueue.main
            weak var weakSelf: AppDelegate? = self
            mainQueue.async(execute: {
                let strongSelf: AppDelegate? = weakSelf
                if strongSelf != nil {
                    strongSelf?.myTimer?.invalidate()
                }
            })
        }
        
        func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
            return true
        }
        
        func applicationWillEnterForeground(_ application: UIApplication) {
        }
        
        func applicationDidBecomeActive(_ application: UIApplication) {
          
            if UserDefaults.standard.bool(forKey: "isComingFromResignActive"){
                UserDefaults.standard.set(false, forKey: "isComingFromResignActive")
                self.getCurrentSimChangeStatus()
            }

        }
        
        func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
            return self.orientationLock
        }
        
        func applicationWillTerminate(_ application: UIApplication) {
            // self.setPreviousLanguage()
            //tushar
            //this is multi payto total amount i am tracking to show user insufficient balance
            UserDefaults.standard.removeObject(forKey: "MultiplePayTOAmount")
            self.saveContext()
        }
        
        func setPreviousLanguage() {
            if UserDefaults.standard.bool(forKey: "LanguageStatusChanged"){
                let preLang = UserDefaults.standard.value(forKey: "PreviousLanguage") as? String
                UserDefaults.standard.set(preLang!, forKey: "currentLanguage")
                appDel.setSeletedlocaLizationLanguage(language: preLang!)
                UserDefaults.standard.synchronize()
            }
        }
        
      
        
        //MARK:- Session task
        @objc func applicationDidTimeout(notification: NSNotification) {
            println_debug(OKSessionValidation.isMainAppSessionExpired)
            UserDefaults.standard.set(true, forKey: "isLogOut")
        }
        
        
        // MARK: - Core Data stack
        @available(iOS 10.0, *)
        lazy var persistentContainer: NSPersistentContainer = {
            /*
             The persistent container for the application. This implementation
             creates and returns a container, having loaded the store for the
             application to it. This property is optional since there are legitimate
             error conditions that could cause the creation of the store to fail.
             */
            let container = NSPersistentContainer(name: "OK")
            container.loadPersistentStores(completionHandler: { (storeDescription, error) in
                if let error = error as NSError? {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    fatalError("Unresolved error \(error), \(error.userInfo)")
                }
            })
            return container
        }()
        
        // MARK: - Core Data Saving support
        func saveContext () {
            let context = persistentContainer.viewContext
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    println_debug("Unresolved error")
                    //                    let nserror = error as NSError
                    //                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        }
        
        // MARK: - Reachability Support
        @objc func reachabilityChanged(note: Notification) {
            let reachability = note.object as! Reachability
            switch reachability.connection {
            case .wifi:
                NotificationCenter.default.post(name: .networkConnected, object: nil)
                println_debug("Reachable via WiFi")
                isNetworkAvail = true
            case .cellular:
                NotificationCenter.default.post(name: .networkConnected, object: nil)
                println_debug("Reachable via Cellular")
                isNetworkAvail = true
            case .none:
                
                NotificationCenter.default.post(name: .networkLost, object: nil)
                
                alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                }
                alertViewObj.showAlert(controller: UIViewController.init())
                println_debug("Network not reachable")
                isNetworkAvail = false
            }
        }
        
        func checkNetworkAvail() -> Bool {
            return isNetworkAvail
        }
        
        // Return IP address of WiFi interface (en0) as a String, or `nil`
        func getWiFiAddress() -> String? {
            var address : String?
            // Get list of all interfaces on the local machine:
            var ifaddr : UnsafeMutablePointer<ifaddrs>?
            guard getifaddrs(&ifaddr) == 0 else { return nil }
            guard let firstAddr = ifaddr else { return nil }
            
            // For each interface ...
            for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
                let interface = ifptr.pointee
                
                // Check for IPv4 or IPv6 interface:
                let addrFamily = interface.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    // Check interface name:
                    let name = String(cString: interface.ifa_name)
                    if  name == "en0" || name == "pdp_ip0" {
                        // Convert interface address to a human readable string:
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                                    &hostname, socklen_t(hostname.count),
                                    nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
            return address
        }
        
        func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
            self.shortcutMenuActions(item: shortcutItem)
            NotificationCenter.default.post(name: Notification.Name("ShortCutMenuAction"), object: nil, userInfo: nil)
        }
        
        
        
        // MARK: Dynamic Shortcut Items (dynamic)
        func createShortcutItemsWithIcons() {
            
            // create some icons with my own images
            let icon1 = UIApplicationShortcutIcon.init(templateImageName: "iCon1")
            let icon2 = UIApplicationShortcutIcon.init(templateImageName: "iCon2")
            let icon3 = UIApplicationShortcutIcon.init(templateImageName: "iCon3")
            let icon4 = UIApplicationShortcutIcon.init(templateImageName: "dashboard_facePay")
            
            
            
            var myNumber = ""
            if let unwrappedProfileList = UserLoginDataManager.sharedInstance.fetchUserProfileDetailsRecord() {
                myNumber = (unwrappedProfileList.mobileNo.safelyWrappingString() == "") ? "My Number" : "\(unwrappedProfileList.mobileNo.safelyWrappingString())"
            }
            
            // create dynamic shortcut items
            let item1 = UIMutableApplicationShortcutItem.init(type: "com.cgm.PAYTO", localizedTitle: "PAYTO", localizedSubtitle: "", icon: icon1, userInfo: nil)
            let item2 = UIMutableApplicationShortcutItem.init(type: "com.cgm.MyNumber", localizedTitle: "Top Up", localizedSubtitle: myNumber, icon: icon2, userInfo: nil)
            let item3 = UIMutableApplicationShortcutItem.init(type: "com.cgm.OtherNumber", localizedTitle: "Top Up", localizedSubtitle: "Other Number", icon: icon3, userInfo: nil)
            let item4 = UIMutableApplicationShortcutItem.init(type: "com.cgm.FacePay", localizedTitle: "Face ID Pay", localizedSubtitle: "", icon: icon4, userInfo: nil)
            
            // add all items to an array
            let items = [item1, item2, item3, item4] as Array
            UIApplication.shared.shortcutItems = items
        }
        
        //Notify sim change (operator change)
        func notifySimChange() {
                  if #available(iOS 12.0, *) {
                      networkInfo.serviceSubscriberCellularProvidersDidUpdateNotifier = { (carrier) in
                          self.getCurrentSimChangeStatus()
                      }
                  } else {
                      networkInfo.subscriberCellularProviderDidUpdateNotifier = { (carrier) in
                          self.getCurrentSimChangeStatus()
                      }
                  }
              }
        
        func getNetworkCode() -> (String, String) {
                   let networkInfo = CTTelephonyNetworkInfo.init()
                   var primaryNetworkCode = ""
                   var carrierName = ""
                   if #available(iOS 12.0, *) {
                       if let data = CTTelephonyNetworkInfo.init().serviceSubscriberCellularProviders{
                           
                           if data.count == 1{
                               if data["0000000100000001"]?.mobileNetworkCode ?? "" == "" {
                               }else {
                                   primaryNetworkCode = data["0000000100000001"]?.mobileNetworkCode ?? ""
                                   carrierName = data["0000000100000001"]?.carrierName ?? ""
                                   
                               }
                           }else{
                               if data["0000000100000001"]?.mobileNetworkCode ?? "" == "" && data["0000000100000002"]?.mobileNetworkCode ?? "" == ""{
                               }else {
                                   primaryNetworkCode = data["0000000100000001"]?.mobileNetworkCode ?? ""
                                   carrierName = data["0000000100000001"]?.carrierName ?? ""
                               }
                           }
                           return(primaryNetworkCode,carrierName)
                       }
                       
                   } else {
                       let carrier     = networkInfo.subscriberCellularProvider
                       let networkCode = carrier?.mobileNetworkCode ?? ""
                       let carrierName = carrier?.carrierName ?? ""
                       return (networkCode, carrierName)
                   }
                   return("","")
                  
               }
               
        
        private func getCurrentSimChangeStatus() {
            
            if userDef.bool(forKey: "Reverify_SimChange") {
                return
            }

            if !UitilityClass.checkifSimAvailableInPhone(){
                let (networkCode, carrierName) = appDel.getNetworkCode()
                UserDefaults.standard.set(networkCode, forKey: "MobileNetworkCode")
                UserDefaults.standard.set(carrierName, forKey: "MobileCarrierName")
                deleteAllObjects()
                loadSplashScreen()
                DispatchQueue.main.async {
                    UIApplication.shared.applicationIconBadgeNumber = 0
                    UserDefaults.standard.set(0, forKey: "ReceivedCount")
                }
            }
        }
        
        func deleteAllObjects(){
            userDef.removeObject(forKey: "BankRoutingapiversion")
            userDef.removeObject(forKey: "calledInLastDay")
            userDef.removeObject(forKey: "SendmoneyToBankVersion")
            userDef.removeObject(forKey: "shopID")
            userDef.removeObject(forKey: "PromotionId")
            userDef.removeObject(forKey:  "shop_name")
            userDef.removeObject(forKey:  "changeType")
            
            UserModel.clearAll()
            
            let entitesByName = appDel.persistentContainer.managedObjectModel.entitiesByName
            for (name, _) in entitesByName {
                deleteAllObjectsForEntity(entity: name)
            }
        }
        
        private func deleteAllObjectsForEntity(entity: String){
            let managedContext  =  self.persistentContainer.viewContext
            let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchReq)
            do {
                try managedContext.execute(deleteRequest)
                try managedContext.save()
            } catch {
            }
        }
        
        
        func loadSplashScreen(show: Bool = true) {
            DispatchQueue.main.async {
                UserDefaults.standard.set(false, forKey: keyLoginStatus)
                // background
                if show {
                    let content = UNMutableNotificationContent()
                    content.title = NSString.localizedUserNotificationString(forKey: "Notification", arguments: nil)
                    content.body = NSString.localizedUserNotificationString(forKey: "You have changed the sim card in your device.", arguments: nil)
                    content.sound = UNNotificationSound.default
                    content.categoryIdentifier = "simChangeaAtionCategory"
                    
                    let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 0.5, repeats: false)
                    let request = UNNotificationRequest.init(identifier: "notify-test", content: content, trigger: trigger)
                    
                    let center = UNUserNotificationCenter.current()
                    center.add(request)
                    userDef.set(false, forKey: "passKeyBoardType")
                }
                let splashScreen = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SplashScreenViewController_ID")
                let navigationController = UINavigationController(rootViewController: splashScreen)
                self.window?.rootViewController = navigationController
                self.window!.makeKeyAndVisible()
            }
        }
        
        
        //For app orientation
        struct AppUtility {
            static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
                if let delegate = UIApplication.shared.delegate as? AppDelegate {
                    delegate.orientationLock = orientation
                }
            }
            
            static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
                self.lockOrientation(orientation)
                UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
            }
        }
        // MARK: Language Localization
        
        func setSeletedlocaLizationLanguage(language : String) {
            currentLanguage = language
            if let path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj") {
                localBundle = Bundle(path: path)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "stringLocalised"), object: nil)
            }
        }
        
        func getStringAccordingToLanguage(language : String, key: String) -> String {
            if let path = Bundle.main.path(forResource: language, ofType: "lproj") {
                let bundle  = Bundle(path: path)
                return NSLocalizedString(key, tableName: "Localization", bundle: bundle!, value: "", comment: "")
            }
            return key
        }
        
        func getSelectedLanguage() -> String {
            return currentLanguage
        }
        
        func getlocaLizationLanguage(key : String) -> String {
            if let bundl = localBundle {
                return NSLocalizedString(key, tableName: "Localization", bundle: bundl, value: "", comment: "")
            } else {
                return key
            }
        }
        
        //Push notification and local notification
        
        private func registerForNotification() {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                if granted {
                    let acceptAction = UNNotificationAction(identifier: "ACCEPT_ACTION",
                                                            title: "Accept",
                                                            options: UNNotificationActionOptions(rawValue: 0))
                    let declineAction = UNNotificationAction(identifier: "DECLINE_ACTION",
                                                             title: "Decline",
                                                             options: UNNotificationActionOptions(rawValue: 0))
                    // Define the notification type
                    if #available(iOS 11.0, *) {
                        let meetingInviteCategory =
                            UNNotificationCategory(identifier: "MEETING_INVITATION",
                                                   actions: [acceptAction, declineAction],
                                                   intentIdentifiers: [],
                                                   hiddenPreviewsBodyPlaceholder: "",
                                                   options: .customDismissAction)
                        let notificationCenter = UNUserNotificationCenter.current()
                        notificationCenter.setNotificationCategories([meetingInviteCategory])
                    } else {
                        // Fallback on earlier versions
                    }
                    // Register the notification type.
                    
                    self.getNotificationSettings()
                }
                
                if error != nil {
                    println_debug("Request authorization failed!")
                } else {
                    println_debug("Request authorization succeeded!")
                }
            }
            
            let category = UNNotificationCategory(identifier: "simChangeaAtionCategory", actions: [], intentIdentifiers: [], options: [])
            UNUserNotificationCenter.current().setNotificationCategories([category])
        }
        
        func getNotificationSettings() {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                println_debug("Notification settings: \(settings)")
                guard settings.authorizationStatus == .authorized else { return }
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
        
        func application(_ application: UIApplication,
                         didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
            let tokenParts = deviceToken.map { data -> String in
                return String(format: "%02.2hhx", data)
            }
            
            let token = tokenParts.joined()
            print("token \(token)")
            println_debug("Device Token: \(token)")
            //Messaging.messaging().apnsToken = deviceToken
            UserDefaults.standard.set(token, forKey: "deviceToken")
            
            let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
            println_debug(deviceTokenString)
        }
        
        func application(_ application: UIApplication,
                         didFailToRegisterForRemoteNotificationsWithError error: Error) {
            println_debug("Failed to register: \(error)")
        }
        
        
        func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
            println_debug(messaging.debugDescription)
            println_debug(remoteMessage.appData)
            guard let data = try? JSONSerialization.data(withJSONObject: remoteMessage.appData, options: .prettyPrinted),
                let prettyPrinted = String(data: data, encoding: .utf8) else {
                    return
            }
            if let dictionary = self.convertToDictionary(text: prettyPrinted) {
                self.checkCICONotificationCases(userInfo: dictionary, actionIdentifier: nil)
            }
        }
        
        func convertToDictionary(text: String) -> [String: Any]? {
            if let data = text.data(using: .utf8) {
                do {
                    return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                } catch {
                    println_debug(error.localizedDescription)
                }
            }
            return nil
        }
        
        func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                         fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
            // If you are receiving a notification message while your app is in the background,
            // this callback will not be fired till the user taps on the notification launching the application.
            // TODO: Handle data of notification
            
            // With swizzling disabled you must let Messaging know about the message, for Analytics
            Messaging.messaging().appDidReceiveMessage(userInfo)
            
            // Print message ID.
            
            if let messageID = userInfo["gcmMessageIDKey"] {
                println_debug("Message ID: \(messageID)")
            }
            println_debug("didReceive test \(userInfo)")
            let aps = userInfo["aps"] as! [String: AnyObject]
            println_debug(aps)
            
            // Print full message.
            println_debug(userInfo)
            
            completionHandler(UIBackgroundFetchResult.newData)
            
        }
        
        fileprivate func notificationHandle(userInfo : Dictionary<String , Any>) {
            if let msg = userInfo["alert"] as? String {
                
                if msg.contains("Msg:") {
                let arrMsg = msg.components(separatedBy: "Msg:")
                let strDisplay  = arrMsg[0]
                    println_debug(strDisplay)
                    
                    // create the content and style for the local notification
                    let content = UNMutableNotificationContent()
                    
                    content.categoryIdentifier = "debitOverdraftNotification"
                    content.title = "OK$"
                    content.subtitle = "Notification"
                    content.body = strDisplay
                    content.sound = UNNotificationSound.default
                    
                    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                    
                    let uuidString = UUID().uuidString
                    let request = UNNotificationRequest(identifier: uuidString, content: content, trigger: trigger)
                    UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                }
                
                if msg.contains("You have (received)") {
                    CodeSnippets.performReceivedCount(isToAdd: true)
                }
            }
            println_debug(userInfo)
        }
        
        //MARK:  EVENT LOG FOR ANAYLITICS
        
        open func logEventWithAnaylitics(eventName : String , EventType type : String , WithUser userid : String , AndEventStatus status : String) {
            
            Analytics.logEvent(eventName , parameters: [
                "UserID": userid,
                "Event": type,
                "Status": "\(type)_\(status)"
            ])
        }
        
        //MARK: Update login api
        func updateTokentoServer(token : String) {
            let modelCashInCashOut = CashInCashOutModel()
            let request = AuthTokenCICORequest(deviceID: token, userName: UserModel.shared.mobileNo)
            modelCashInCashOut.getAuthtokenForCICO(request: request)
        }
        
        //MARK: Firebase delegate methods
        public func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
            println_debug(remoteMessage.appData)
        }
        
        public func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
            println_debug("Firebase registration token: \(fcmToken)")
            if let token = Messaging.messaging().fcmToken { // send the token to the server
                println_debug(token)
                UserDefaults.standard.set(fcmToken, forKey: "FCMToken")
                self.updateTokentoServer(token : token)
            }
        }
        
        //when app open //
        func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
            println_debug("Firebase registration token: \(fcmToken)")
            
            let dataDict = ["token": fcmToken]
            NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
            UserDefaults.standard.set(fcmToken, forKey: "FCMToken")
            let modelCashInCashOut = CashInCashOutModel()
            modelCashInCashOut.updateAuthToken(fcmToken : fcmToken)
            // TODO: If necessary send token to application server.
            // Note: This callback is fired at each app startup and whenever a new token is generated.
        }
        
        func application(application: UIApplication,
                         didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
            Messaging.messaging().apnsToken = deviceToken as Data
        }
        
        fileprivate func checkCICONotificationCases(userInfo : Dictionary<String, Any>, actionIdentifier : UNNotificationResponse?) {
            if let key = userInfo["key"] as? String {
                switch key {
                case "NewCashTransferRequest" :
                    NotificationCenter.default.post(name: Notification.Name("PushToCICOCancelAccept"), object: nil, userInfo: userInfo)
                case "CashRequestAccepted":
                    NotificationCenter.default.post(name: Notification.Name("PushToCICOPendingScreen"), object: nil, userInfo: userInfo)
                case "CancelCashTransfer":
                    NotificationCenter.default.post(name: Notification.Name("ReloadCICOAllScreens"), object: nil, userInfo: nil)
                    NotificationCenter.default.post(name: Notification.Name("ShowCICOCancelAlert"), object: nil, userInfo: nil)
                    NotificationCenter.default.post(name: Notification.Name("DismissCICOCancelAcceptScreen"), object: nil, userInfo: userInfo)
                case "StartTrip":
                    NotificationCenter.default.post(name: Notification.Name("ReloadCICOAllScreens"), object: nil, userInfo: nil)
                    NotificationCenter.default.post(name: Notification.Name("CICONotifyStartTrip"), object: nil, userInfo: userInfo)
                case "EndTrip":
                    NotificationCenter.default.post(name: Notification.Name("ReloadCICOAllScreens"), object: nil, userInfo: nil)
                    println_debug(userInfo)
                    NotificationCenter.default.post(name: Notification.Name("ShowCICOConfirmationScreen"), object: nil, userInfo: userInfo)
                case "ProcessRequestPending":
                    NotificationCenter.default.post(name: Notification.Name("ShowCICOShowProcessAlert"), object: nil, userInfo: userInfo)
                case "CashTransferPaymentCollected":
                    NotificationCenter.default.post(name: Notification.Name("PushToCICOShowRatingScreen"), object: nil, userInfo: userInfo)
                case "AgentLocationUpdated":
                    NotificationCenter.default.post(name: Notification.Name("PushToCICOMapUpdate"), object: nil, userInfo: userInfo)
                case "RejectCashTransfer":
                    NotificationCenter.default.post(name: Notification.Name("ShowCICORejectAlert"), object: nil, userInfo: nil)
                    NotificationCenter.default.post(name: Notification.Name("ReloadCICOAllScreens"), object: nil, userInfo: nil)
                    NotificationCenter.default.post(name: Notification.Name("DismissCICORequestScreen"), object: nil, userInfo: userInfo)
                    NotificationCenter.default.post(name: Notification.Name("DismissCICOCancelAcceptScreen"), object: nil, userInfo: userInfo)
                case "CashRequestAcceptedByOthers":
                    NotificationCenter.default.post(name: Notification.Name("PushToCICO"), object: nil, userInfo: userInfo)
                default:
                    break
                }
            }
        }
    }
    
    extension AppDelegate: UNUserNotificationCenterDelegate {
        
        func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
            println_debug("=======> Did receive")
            if let aps = response.notification.request.content.userInfo["aps"] as? [String: Any] {
                println_debug("=======>\(aps)")
                self.notificationHandle(userInfo: aps)
                self.checkCICONotificationCases(userInfo: aps, actionIdentifier: response)
            }
            
            completionHandler()
        }
        
        func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
            println_debug("=======> willPresent")
            if let aps = notification.request.content.userInfo["aps"] as? [String: Any] {
                self.notificationHandle(userInfo: aps)
                println_debug("=======>\(aps)")
                self.checkCICONotificationCases(userInfo: aps, actionIdentifier: nil)
            }
            completionHandler([.alert, .badge, .sound])
        }
    }
    
    
    extension Notification.Name {
        static let updateTimer = Notification.Name("goDashboardScreen")
        static let updateTimerSMB = Notification.Name("UpdateTimerSMB")
    }
    
