//
//  OKBaseController.swift
//  OK
//
//  Created by Ashish Kr Singh on 29/08/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SDWebImage
import Firebase
import ContactsUI

var preNetInfo: NetworkPreLogin?
var preLoginModel = PreLoginModel()
var arrCountry = NSArray()

class OKBaseController: UIViewController {
    //Tushar
    //This is used in only two case while coming from make payment form new map model and from map details click
    //to pop to dash board directly because it is creating loop of view controller
    var isComingFromMapMakePayment = false
    
    //MARK: - Properties
    var preCodeCountry: CodePreLogin?
    var profileImageCache = NSCache<AnyObject,AnyObject>()
    let queue = OperationQueue()
    fileprivate let imageCache = SDImageCache.init()
    

    let dateF1 = DateFormatter()
    let dateF2 = DateFormatter()
    let dateF3 = DateFormatter()
    let dateF4 = DateFormatter()
    let dateF5 = DateFormatter()
    let dateF6 = DateFormatter()
    let dateF7 = DateFormatter()
    let dateF8 = DateFormatter()
    var bgImage = UIImage()
    
    
    //MARK: - Views Method
    override func viewDidLoad() {
        super.viewDidLoad()
        dateF1.dateFormat = "dd-MM-yyyy"
        dateF2.dateFormat = "dd/MM/yyyy HH:mm:ss a"
        dateF3.dateFormat = "MM/dd/yyyy HH:mm:ss a"
        dateF4.dateFormat = "dd/MMM/yyyy"
        dateF5.dateFormat = "dd-MMM-yyyy"
        dateF6.dateFormat = "dd/MM/yyyy"
        dateF7.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS" //"yyyy-MM-dd'T'HH:mm:ssZ"
        dateF8.dateFormat = "MM/dd/yyyy HH:mm:ss a"
        //2019-07-31T13:31:42.467
        dateF1.locale = Locale(identifier: "en_US")
        dateF1.calendar = Calendar(identifier: .gregorian)
        self.helpSupportNavigationEnum = .dashboard
        ok_default_language = (ok_default_language == "") ? "en" : ok_default_language.lowercased()
        self.title = appDelegate.getlocaLizationLanguage(key: self.title ?? self.title ?? "")
    }
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if appDel.currentLanguage == "uni"{
            appDel.floatingButtonControl?.window.button?.titleLabel?.font =  UIFont(name: "Zawgyi-One", size: appFontSize)
           
            appDel.floatingButtonControl?.window.button?.setTitle("အကူအညီရယူရန္", for: .normal)
        } else {
            appDel.floatingButtonControl?.window.button?.titleLabel?.font =  UIFont(name: appFont, size: 12.0)
            appDel.floatingButtonControl?.window.button?.setTitle("Help".localized, for: .normal)
        }
       
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        appDel.floatingButtonControl?.viewDidLayoutSubviews()
        appDel.floatingButtonControl?.window.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func parseDate(strDate : String)-> Date {
        var date1 = Date()
        if let date = dateF1.date(from: strDate) {
            date1 = date
        }else if let date = dateF2.date(from: strDate){
            date1 = date
        }else if let date = dateF3.date(from: strDate){
            date1 = date
        }else if let date = dateF4.date(from: strDate){
            date1 = date
        }else if let date = dateF5.date(from: strDate){
            date1 = date
        }else if let date = dateF6.date(from: strDate){
            date1 = date
        }else if let date = dateF7.date(from: strDate){
            date1 = date
        }
        else if let date = dateF8.date(from: strDate){
            date1 = date
        }
        return date1
    }
    
    func parseDateJaoinDate(strDate : String)-> Date {
        var date1 = Date()
       if let date = dateF3.date(from: strDate) {
            date1 = date
       }else {
        
        return dateF3.date(from: getDateString(strDate)) ?? Date()
       }
        return date1
    }
    
    func getDateString(_ dateString:String) -> String {

        var strResult = ""

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "M/dd/yyyy hh:mm:ss a"

        if let date = dateFormatter.date(from: dateString) {
            dateFormatter.dateFormat = "dd MMM yyyy"
            strResult = dateFormatter.string(from: date)
            
            let dateFormatterNew = DateFormatter()
            dateFormatterNew.dateFormat = "MM/dd/yyyy HH:mm:ss a"
            strResult = dateFormatterNew.string(from: date)
        }
        
        
        return strResult

    }
    
    
    
    func parseDateJaoinDate7(strDate : String)-> Date {
          var date1 = Date()
         if let date = dateF7.date(from: strDate) {
              date1 = date
          }
          return date1
      }
    
    
    func getYearDifferace(date : String)-> String {
        let form = DateComponentsFormatter()
        form.maximumUnitCount = 2
        form.unitsStyle = .full
        form.allowedUnits = [.year, .month, .day]
        let date2 = Date()
        let diff = form.string(from: parseDateJaoinDate(strDate: date) , to: date2)
        let year = diff?.components(separatedBy: ",")
        return year?[0] ?? ""
    }
    
    func getYearDifferaceForHeader(date : String)-> String {
        let form = DateComponentsFormatter()
        form.maximumUnitCount = 2
        form.unitsStyle = .full
        form.allowedUnits = [.year, .month, .day]
        let date2 = Date()
        let diff = form.string(from: parseDate(strDate: date) , to: date2)
        let year = diff?.components(separatedBy: ",")
        return year?[0] ?? ""
    }
    
    func checkUUIDVerification() -> Bool {
        
        
        
        if uuid == "00000000-0000-0000-0000-000000000000" {
            return false
        }
        
        return true
    }
    
    //MARK: - Class Method
    class  func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
            } catch {
                println_debug(error.localizedDescription)
            }
        }
        return nil
    }
    
    class  func convertToArrDictionary(text: String) -> [AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [AnyObject]
            } catch {
                println_debug(error.localizedDescription)
            }
        }
        return nil
    }
    
    class func imageTobase64(image: UIImage) -> String {
        var base64String = ""
        let  cim = CIImage(image: image)
        if (cim != nil) {
            let imageData:Data = image.jpegData(compressionQuality: 0.2)!
            base64String = imageData.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters)
        }
        return base64String
    }
    
    //MARK: - Objc Methods
    @objc func backButtonCustomAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissControllerWhenTap() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func dismissKeyboards() {
        view.endEditing(true)
    }
    
    @objc func backAction() {
        if let nav  = self.navigationController {
            nav.popViewController(animated: true)
        }
    }
    
    //MARK: - Contact Fetch Methods
    func GetMobileNumberFromComent(coment : String ) -> String {
        var MobileNumber : String = ""
                if coment.hasPrefix("#OK-00-") {
                    let delimiter = "-"
                    let newstr = coment
                    var token = newstr.components(separatedBy: delimiter)
                    if token[6].hasPrefix("Overseas"){
                        MobileNumber = "\(token[3])"
                    } else {
                        token[3].remove(at: token[3].startIndex)
                        MobileNumber = token[3]
                    }
                } else if coment.hasPrefix("OOK-00-") {
                    let delimiter = "-"
                    let newstr = coment
                    var token = newstr.components(separatedBy: delimiter)
                    token[3].remove(at: token[3].startIndex)
                    MobileNumber = "0095".appending(token[3])
                } else {
                    
                    let value = coment
                    if value.contains(find: "ProjectId"){
                        let newValue = value.components(separatedBy: " Wallet Number :")
                        if newValue.count > 2 {
                            if newValue.indices.contains(1){
                                var name = newValue[2].components(separatedBy: "ProjectId")
                                if name.indices.contains(0){
                                    name[0].remove(at: name[0].startIndex)
                                    MobileNumber = "0095".appending(name[0])
                                }
                            }
                        }  else {
                            //OK Dollar Wallet To MAB , OK Dollar Wallet Number :  00959790681381 Account Number :09976669357ProjectIdMAB767898KUEOJBU_IEHK787UOEJDHF
                            let arrVal = value.components(separatedBy: "Account Number :")
                            if arrVal.indices.contains(1){
                                let name = arrVal[1].components(separatedBy: "ProjectId")
                                if name.indices.contains(0){
                                    MobileNumber = "0095".appending(name[0])
                                }
                            }
                        }
                    } else {
                        MobileNumber = "+950000000000"
                    }
                }
        return MobileNumber
        
    }
    
    func returnCorespondentName(value: String) -> [String:AnyObject]{
        
        if okContacts.isEmpty{
            return ["Unknown":"Unknown" as AnyObject]
        }
        else{
            return self.getDetails(Ucontacts: okContacts, number: "\(value.replacingOccurrences(of: "0095", with: ""))")
        }
    }
   
    func getDetails(Ucontacts:Array<CNContact>, number:String) -> [String:AnyObject]{
        /* Get mobile number with mobile country code */
        
        var userName = [String:AnyObject]()
        
        for i in 0...(Ucontacts.count-1){
            
            for ContctNumVar: CNLabeledValue in Ucontacts[i].phoneNumbers {
                 let FulMobNumVar  = ContctNumVar.value
                 var MobNumVar = ""
                if let numFull = FulMobNumVar.value(forKey: "digits") as? String {
                    MobNumVar = numFull
                }
//                println_debug("user Mobile Number : \(number)")
//                println_debug("Comapre Mobile Number : \(String(describing: MobNumVar))")
                if MobNumVar.contains(find: number) {
                    userName["name"] = (Ucontacts[i].givenName + " " + Ucontacts[i].familyName) as AnyObject
                    userName["pic"] = Ucontacts[i].imageData as AnyObject
                    return userName
                }else{
                    userName["name"] = "Unknown" as AnyObject
                    userName["pic"] = nil
                }
            }
        }
        return userName
    }
    
    
    //MARK: - Methods
    func showDatePicker() {
        let datePicker = UIDatePicker()
        let popoverView = UIView()
        let toolBar = UIToolbar()
        let popoverViewController = CustomDatePickerVC()
        var items = [UIBarButtonItem]()
        
        let mainViewFrame = UIScreen.main.bounds
        let datePickerHeight = mainViewFrame.size.height*0.33
        let datePickerY = mainViewFrame.size.height-datePickerHeight
        let datePickerFrame = CGRect(x: 0, y: datePickerY , width: mainViewFrame.size.width, height: datePickerHeight)
        
        let toolBarHeight = CGFloat(44.0)
        let toolBarY = datePickerY-toolBarHeight
        let toolBarFrame = CGRect(x: 0, y: toolBarY, width: mainViewFrame.size.width, height: 44)
        
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: popoverViewController, action: #selector(popoverViewController.dismissalViewController))
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: popoverViewController, action: #selector(popoverViewController.setDate(sender:)))
        let tap = UITapGestureRecognizer(target: popoverViewController, action:#selector(popoverViewController.dismissalViewController))
        
        toolBar.isTranslucent = false
        // toolBar.backgroundColor = kYellowColor
        toolBar.barTintColor = kYellowColor
        toolBar.tintColor = UIColor.white
        
        items.append(cancelButton)
        items.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil))
        items.append(doneButton)
        toolBar.items = items
        
        datePicker.frame = datePickerFrame
        datePicker.datePickerMode = .date
        datePicker.backgroundColor = UIColor.white
        if #available(iOS 13.4, *) {
                       datePicker.preferredDatePickerStyle = .wheels
                   } else {
                       // Fallback on earlier versions
                   }
        toolBar.frame = toolBarFrame
        popoverView.backgroundColor =  UIColor(red:0 , green: 0, blue: 0, alpha: 0.6)
        popoverView.addSubview(toolBar)
        popoverView.addSubview(datePicker)
        // here you can add tool bar with done and cancel buttons if required
        
        popoverViewController.view = popoverView
        popoverViewController.datePicker = datePicker
        popoverViewController.delegate = self as? RequestMoneyCustomDatePickerDelegate
        popoverViewController.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height/3)
        popoverViewController.modalPresentationStyle = .overFullScreen
        
        tap.cancelsTouchesInView = false
        popoverView.addGestureRecognizer(tap)
        popoverViewController.preferredContentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height/3)
        // if(popoverViewController.responds(to: #selector(popoverViewController.setDate(sender:)))){
        // datePicker.addTarget(popoverViewController, action: #selector(popoverViewController.setDate(sender:)), for: .valueChanged)
        // }
        // popoverViewController.popoverPresentationController?.sourceView = sender // source button
        // popoverViewController.popoverPresentationController?.sourceRect = sender.bounds // source button bounds
        self.present(popoverViewController, animated: true, completion: nil)
    }
    
    func configureBackButton(withTitle title:String) {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "tabBarBack"), for: UIControl.State.normal)
        button.tintColor = UIColor.white
        button.sizeToFit()
        button.addTarget(self, action: #selector(backButtonCustomAction), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.title = title
    }
    
    func configureBackButtonWithSearch() {
        let button1 = UIBarButtonItem(image: UIImage(named:"tabBarBack"), style: .plain, target: self, action: nil)
        button1.tintColor = UIColor.white
        navigationController?.navigationBar.topItem?.backBarButtonItem = button1
    }
    
    func requestAlertForPermissions(withBody body: String,  handler: @escaping ( _ cancelled: Bool) -> Void) {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "OK$ Permission Request", body: body, img: #imageLiteral(resourceName: "okLogo"))
            alertViewObj.addAction(title: "Setting", style: .target) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:])
                        , completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
                }
            }
            
            alertViewObj.addAction(title: "Cancel", style: .cancel) {
                println_debug("cancel")
                handler(true)
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(dismissKeyboards))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func tappTouchdismissView() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(dismissControllerWhenTap))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func jsonObject<object>(string: object) -> object {
        return string
    }
    
    func getTransactionString(_ trans: PayToType) -> String {
        switch trans.transType {
        case .payToID:
            return "PAYTOID"
        case .payTo:
            return "PAYTO"
//            switch trans.categoryType {
//            case .none:
//                return "PAYTO"
//            default:
//                return trans.categoryType.rawValue.uppercased()
//            }
        }
    }
    
    func getLocalTransactionString(_ trans: PayToType) -> String {
        switch trans.transType {
        case .payToID:
            return "PAYTOID"
        case .payTo:
            switch trans.categoryType {
            case .none:
                return "PAYTO"
            default:
                return trans.categoryType.rawValue.uppercased()
            }
        }
    }
    
    func okActivityIndicator(view: UIView) { 
        let loader = OKActivityIndicator.updateView(view.bounds)
        loader.animateLoader()
        view.addSubview(loader)
        view.layoutIfNeeded()
    }
    
    func removeActivityIndicator(view: UIView) {
        for subView in view.subviews {
            if let loader = subView as? OKActivityIndicator {
                loader.removeFromSuperview()
            }
        }
    }
    
    func navigateToPayto(phone number: String, amount: String, name: String,type: String = "payto",remark: String? = "", fromMerchant: Bool? = false, fromTranfer: Bool? = false, from: PayToViewController.From = PayToViewController.From.Recent, cashInQR: Bool? = false, referenceNumber: String = "") {
        
        let payto = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PayToNavigation")
        isPayToDashboard = false
        guard let navigation = payto as? UINavigationController else { return }
        for controller in navigation.viewControllers {
            if let paysend = controller as? PayToViewController {
                switch type {
                case "bus":
                    paysend.headerTitle = "Bus Ticket".localized
                    let payToType = PayToType(type: .payTo)
                    payToType.categoryType = .bus
                    paysend.transType = payToType
                case "toll":
                    paysend.headerTitle = "Toll".localized
                    let payToType = PayToType(type: .payTo)
                    payToType.categoryType = .toll
                    paysend.transType = payToType
                case "ferry":
                    paysend.headerTitle = "Ferry".localized
                    let payToType = PayToType(type: .payTo)
                    payToType.categoryType = .ferry
                    paysend.transType = payToType
                default:
                    break
                }
                if let transfer = fromTranfer, transfer == true {
                    paysend.transType = PayToType(type: .transferTo)
                    paysend.headerTitle = "Transfer To".localized
                }
                if let cashIn = cashInQR, cashIn == true {
                    paysend.transType = PayToType(type: .transferTo)
                    paysend.headerTitle = "Transfer To".localized
                }
                if type == "cashin" {
                    paysend.transType = PayToType(type: .transferTo)
                    paysend.headerTitle = "Transfer To".localized
                } else if type == "cashout" {
                    paysend.transType = PayToType(type: .cashOut)
                    paysend.headerTitle = "Paid Commission Agent Cash In_header".localized
                }
                paysend.fromScreen = from
                paysend.fromMerchant = fromMerchant
                paysend.otherRequirement = (number, name, amount, (remark ?? ""), (cashInQR ?? false), referenceNumber)
              
                //Tushar
                //This is added only in case of map and 
                paysend.isComingFromMapAndMakePayment = isComingFromMapMakePayment
             //   NotificationCenter.default.post(name: .popToDashboard, object: nil,userInfo: ["isComingFromMap":true])
                // NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowRightBarButton"), object: nil)
                payto.modalPresentationStyle = .fullScreen
                self.present(payto, animated: true, completion: nil)
                return
            }
        }
    }
    
    func navigatetoPayto(fromMerchant:Bool? = false, fromTranfer: Bool? = false) {
        let payto = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PayToNavigation")
        isPayToDashboard = false
        guard let navigation = payto as? UINavigationController else { return }
        for controller in navigation.viewControllers {
            if let paysend = controller as? PayToViewController {
                if let transfer = fromTranfer, transfer == true {
                    paysend.transType.type = .transferTo
                    paysend.headerTitle = "Transfer To".localized
                }
                paysend.fromMerchant = fromMerchant
                //paysend.delegateBox = self as! InBoxightBarShow
                payto.modalPresentationStyle = .fullScreen
                self.present(payto, animated: true, completion: nil)
                return
            }
        }
    }
    
    func openFavoriteFromNavigation(_ delegate: FavoriteSelectionDelegate, withFavorite type: FavCaseSelection, selectionType: Bool = false, modelPayTo : PayToUIViewController? = nil, multiPay: Bool? = false, payToCount: Int? = 0) -> UINavigationController {
        let navigation = UIStoryboard(name: "Favorite", bundle: nil).instantiateViewController(withIdentifier: "favoriteNav") as! UINavigationController
        navigation.modalPresentationStyle = .fullScreen
        for controllers in navigation.viewControllers {
            if let fav = controllers as? FavoriteViewController {
                fav.isFromModules  = true
                fav.directSegue    = type
                fav.moduleDelegate = delegate
                fav.singleSelection = selectionType
                fav.modelPayTo = modelPayTo
                fav.multiPay = multiPay ?? false
                fav.payToCount = payToCount ?? 0
            }
        }
        return navigation
    }
    
    func getDayOfWeek(_ today:String, format: String = "yyyy-MM-dd") -> Int? {
        let formatter  = DateFormatter()
        formatter.dateFormat = format
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
    }
    
    func downLoadAndCacheImage(url: URL, handler: @escaping (_ image: UIImage) -> Void)  {
        let imageView = UIImageView.init()
        imageCache.queryCacheOperation(forKey: url.absoluteString, done: { (image, data, cacheTypec) in
            if (image != nil) {
                handler(image!)
            } else {
                imageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ok"), options: SDWebImageOptions.handleCookies, completed: { (image, error, cacheType, urlExtend) in
                    if let image = image {
                        SDImageCache.shared().store(image, forKey: url.absoluteString)
                        handler(image)
                    }
                })
            }
        })
    }

    func getImageFromURL(imageURL:URL, imageView:UIImageView) {
        let image = profileImageCache.object(forKey: imageURL as AnyObject)
        if image != nil {
            imageView.image = image as? UIImage
        } else {
            DispatchQueue.global(qos: .background).async {
                let operation = BlockOperation(block: {
                    let url = imageURL
                    let data = try? Data(contentsOf:url)
                    if data != nil {
                        if let isValidData = data{
                            let img = UIImage(data: isValidData)
                            advImagesUrl.AdImageData.append(isValidData)
                            PaymentVerificationManager.insertAllRecordsToAdvertisementDatabase(imageUrl: imageURL.path, imageData: isValidData)
                            OperationQueue.main.addOperation ({
                                imageView.image = img
                                //println_debug("uuuuu",imageURL)
                                if img != nil {
                                    self.profileImageCache.setObject(img!, forKey: imageURL as AnyObject)
                                } else {
                                }
                            })
                            
                        }
                        
                    }
                })
                self.queue.addOperation(operation)
            }
        }
    }
    
    func loadNavigationBarSettings() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
    }
    
    func loadBackButton() {
        let backBtn = UIBarButtonItem(image: UIImage(named: "tabBarBack"), style: .plain, target: self, action: #selector(backAction))
        self.navigationItem.leftBarButtonItem  = backBtn
    }
    
    func addFavoriteController(withName name: String,favNum: String,type: String, amount: String) -> PTFavoriteViewController? {
        /// Basse
        let vc = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier:"PTFavoriteViewController") as? PTFavoriteViewController
        vc?.view.frame = CGRect.zero
        vc?.addFavDetails = (name,favNum,type,amount)
        vc?.modalPresentationStyle = .overCurrentContext
        return vc!
    }
    
    func overseasNavigation(countryObject: (code: String, name:String), userObject: (phone:String, name: String)) {
        InternationalManager.selectedCountry =  (countryObject.code, countryObject.name)
        let vc = UIStoryboard(name: "InternationalTopUp", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "InternationalMainVC")) as? InternationalMainVC
        vc?.statusScreen = "RecentTx"
        vc?.userObject = (userObject.phone, userObject.name)
        self.present(vc!, animated: true, completion: nil)
    }
    
    func JSONStringFromAnyObject(value: AnyObject, prettyPrinted: Bool = true) -> String {
        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : nil
        if JSONSerialization.isValidJSONObject(value) {
            do {
                let data = try JSONSerialization.data(withJSONObject: value, options: options!)
                if let string = String(data: data, encoding: String.Encoding.utf8) {
                    return string
                }
            } catch {
                println_debug(error)
            }
        }
        return ""
    }
    
    func JSONStringFromAnyObject<JSON>(value: JSON) -> String {
        if JSONSerialization.isValidJSONObject(value) {
            do {
                let data = try JSONSerialization.data(withJSONObject: value, options: JSONSerialization.WritingOptions.prettyPrinted)
                if let string = String(data: data, encoding: String.Encoding.utf8) {
                    return string
                }
            } catch {
                println_debug(error)
            }
        }
        return ""
    }
    
    func checkForContact(_ mobile: String) -> (isInContact: Bool, forNumber: String) {
        
        let isContactPresent = ContactManager().agentgentCodePresentinContactsLoc(number: mobile)
        
        return (isContactPresent, mobile)
    }
    
    func checkForFavorites(_ number: String, type: String? = "PAYTO") -> (isInFavorite: Bool, forNumber: String) {
        let decodedNumber  = PTManagerClass.decodeMobileNumber(phoneNumber: number)
        let favoritesArray = favoriteManager.fetchRecordsForFavoriteContactsEntity()
        for favorite in favoritesArray {
            let numberTuple = PTManagerClass.decodeMobileNumber(phoneNumber: favorite.phone.safelyWrappingString())
            if numberTuple.number == decodedNumber.number && favorite.type?.lowercased() == type?.lowercased() {
                return (isInFavorite: true, forNumber: number)
            } else {
                println_debug("Star Settings")
            }
        }
        return (isInFavorite: false, forNumber: number)
    }
    
    func getNameFromContact(number: String, _ completion: @escaping(_ name: String) -> Void) {
        if number.count > 7 {
            var name = ""
            let numb = String(number.dropFirst(4)) 
            if okContacts.count > 0 {
                outerLoop: for contact in okContacts {
                    for phoneNumber in contact.phoneNumbers {
                        let phoneNumberStruct = phoneNumber.value
                        let phoneNumberString = phoneNumberStruct.stringValue
                        let strMobNo = phoneNumberString
                        let trimmed = String(strMobNo.filter { !" ".contains($0) })
                        if trimmed.contains(find: numb) {
                            name = contact.givenName
                            break outerLoop
                        }
                    }
                }
            }
            completion(name)
        } else {
            completion("")
        }
    }
    
    private func getNumberforCashBackBasedOnOperator(operatorLoc: String) -> String {
        let configList = telecoTopupSubscription?.first?.cashBackConfigList?.parseJSONString as? Dictionary<String, Any>
        switch operatorLoc {
        case "mpt":
            return (configList?["Mpt"] as? String ?? "")
        case "telenor":
            return (configList?["Telenor"] as? String ?? "")
        case "ooredoo":
            return (configList?["Ooredoo"] as? String ?? "")
        case "mytel":
            return (configList?["Mytel"] as? String ?? "")
        case "mectel":
            return (configList?["Mectel"] as? String ?? "")
        case "oversastopup":
            return (configList?["OversasTopup"] as? String ?? "")
        default:
            return ""
        }
    }
    
    func getKickbackTopupByOperator(opaerator: String) -> String {
        return getNumberforCashBackBasedOnOperator(operatorLoc: opaerator.lowercased())
    }
    
    
    func returnAttributtedtextview(nameFont: String,textOne: String,textTwo: String,textThree: String,textfourth: String,sizeOne:CGFloat,sizeTwo: CGFloat,color: UIColor) -> NSMutableAttributedString {
             //theme setting
             let customAttribute1 = [ NSMutableAttributedString.Key.font: UIFont(name: nameFont, size: sizeOne) ?? UIFont.systemFont(ofSize: sizeOne),
                                      NSMutableAttributedString.Key.foregroundColor: color]
             let customAttribute2 = [ NSMutableAttributedString.Key.font: UIFont(name: nameFont, size: sizeTwo) ?? UIFont.systemFont(ofSize: sizeTwo),
                                      NSMutableAttributedString.Key.foregroundColor: color ]
             let customAttribute3 = [ NSMutableAttributedString.Key.font: UIFont(name: nameFont, size: sizeTwo) ?? UIFont.systemFont(ofSize: sizeTwo),
                                      NSMutableAttributedString.Key.foregroundColor: color ]
             let customAttribute4 = [ NSMutableAttributedString.Key.font: UIFont(name: nameFont, size: sizeOne) ?? UIFont.systemFont(ofSize: sizeOne) ,NSMutableAttributedString.Key.foregroundColor: color]
             
             
             //adding attributes and text
             let attributedAmountString = NSMutableAttributedString.init(string: textfourth, attributes: customAttribute4)

             
            // attributedAmountString.addAttribute(NSAttributedString.Key.kern, value: 1, range: NSMakeRange(0, textOne.count))

             
             let mmkString = NSMutableAttributedString.init(string: textTwo, attributes: customAttribute2)
             let mmkString3 = NSMutableAttributedString.init(string: textThree, attributes: customAttribute3)
             let mmkString4 = NSMutableAttributedString.init(string: textOne, attributes: customAttribute1)
             
             if textfourth == "OK$ Balance : ".localized{
                 mmkString4.addAttribute(NSAttributedString.Key.kern, value: 1.25, range: NSMakeRange(0, textOne.length))
             }
             
             
            //
             attributedAmountString.append(mmkString4)
             attributedAmountString.append(mmkString)
             attributedAmountString.append(mmkString3)

             return attributedAmountString
             
         }
}



//MARK: - Global Methods
func restrictMultipleSpaces(str : String, textField: UITextField) -> Bool {
   
    if str == " " && textField.text?.last == " "{
        return false
    } else {
        if characterBeforeCursor(tf: textField) == " " && str == " " {
            return false
        } else {
            if characterAfterCursor(tf: textField) == " " && str == " " {
                return false
            } else {
                return true
            }
        }
    }
}

func characterBeforeCursor(tf : UITextField) -> String? {
    if let cursorRange = tf.selectedTextRange {
        // get the position one character before the cursor start position
        if let newPosition = tf.position(from: cursorRange.start, offset: -1) {
            let range = tf.textRange(from: newPosition, to: cursorRange.start)
            return tf.text(in: range!)
        }
    }
    return nil
}

func characterAfterCursor(tf : UITextField) -> String? {
    if let cursorRange = tf.selectedTextRange {
        // get the position one character before the cursor start position
        if let newPosition = tf.position(from: cursorRange.start, offset: 1) {
            let range = tf.textRange(from: newPosition, to: cursorRange.start)
            return tf.text(in: range!)
        }
    }
    return nil
}

//MARK: - Global Methods
func restrictMultipleSpacesSearchBar(str : String , searchBar: UISearchBar , range: NSRange) -> Bool {
    if str == " " && searchBar.text?.last == " "{
        return false
    } else {
        if characterBeforeCursorInSearchBar(tf: searchBar, range: range) == " " && str == " " {
            return false
        } else {
            if characterAfterCursorInSearchBar(tf: searchBar, range: range) == " " && str == " " {
                return false
            }
            else {
                return true
            }
        }
    }
}

func characterBeforeCursorInSearchBar(tf : UISearchBar , range : NSRange) -> String? {
    if let cursorRange = range as? NSRange{
        // get the position one character before the cursor start position
//        if let newPosition = tf.searchTextPositionAdjustment(from: cursorRange.start, offset: -1) {
//            let range = tf.textRange(from: newPosition, to: cursorRange.start)
//            return tf.text(in: range!)
//        }
    }
    return nil
}

func characterAfterCursorInSearchBar(tf : UISearchBar, range : NSRange) -> String? {
    if let cursorRange = range as? NSRange{
        // get the position one character before the cursor start position
     }
    return nil
}




func trimOffLastSpace (txt : String) -> Bool {
    if txt.count > 1 {
        let last = txt.index(txt.endIndex, offsetBy: -1)
        let secondLast = txt.index(txt.endIndex, offsetBy: -2)
        if txt[last] == " "  && txt[secondLast] == " "{
            return false
        }else {
            return true
        }
    }
    return true
}

func identifyCountryByCode(withPhoneNumber prefix: String) -> (countryCode: String, countryFlag: String) {
    let countArray = revertCountryArray() as! Array<NSDictionary>
    var finalCodeString = ""
    var flagCountry = ""
    for dic in countArray {
        if let code = dic.object(forKey: "CountryCode") as? String, let flag = dic.object(forKey: "CountryFlagCode") as? String {
            if prefix == code {
                finalCodeString = code
                flagCountry  = flag
            }
        }
    }
    return (finalCodeString,flagCountry)
}

func identifyCountry(withPhoneNumber prefix: String) -> (countryCode: String, countryFlag: String) {
    let countArray = revertCountryArray() as! Array<NSDictionary>
    
    var finalCodeString = ""
    var flagCountry = ""
    
    for dic in countArray {
        if let code = dic .object(forKey: "CountryCode") as? String, let flag = dic .object(forKey: "CountryFlagCode") as? String {
            if prefix.hasPrefix(code) {
                finalCodeString = code
                flagCountry  = flag
            }
        }
    }
    return (finalCodeString,flagCountry)
}

func identifyCountryName(countryCode : String) -> (String) {
    var c_name = ""
    let CallingCodes = { () -> [[String: String]] in
        let resourceBundle = Bundle.main
        guard let path = resourceBundle.path(forResource: "CallingCodes", ofType: "plist") else {
            return []
        }
        return NSArray(contentsOfFile: path) as! [[String: String]]
    }()
    
    for item in CallingCodes {
        if item["dial_code"] == countryCode {
            if let mapFlagCode = item["name"] {
                c_name = mapFlagCode
            }
        }
    }
    return c_name
}

func replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: String) -> (mobileNo: String, counrtycode: String){
    var stringToTrim = strToTrim
    if stringToTrim.count < 5 {
        return ("","")
    } else {
        var c_Code = ""
        stringToTrim.remove(at: stringToTrim.startIndex)
        stringToTrim.remove(at: stringToTrim.startIndex)
        stringToTrim.insert("+", at: stringToTrim.startIndex)
        
        let index = stringToTrim.index(stringToTrim.startIndex, offsetBy: 3)
        if String(stringToTrim[..<index]) == "+95" {
            c_Code = String(stringToTrim[..<index])
            stringToTrim = "0" + String(stringToTrim[index...])
        } else {
            c_Code = String(stringToTrim[..<index])
            stringToTrim = String(stringToTrim[index...])
        }
        return (stringToTrim,c_Code)
    }
}

func getMobileNumberWithoutZero(strToTrim: String) -> (mobileNo: String, counrtycode: String){
    var stringToTrim = strToTrim
    if stringToTrim.count < 5 {
        return ("","")
    }else {
        var c_Code = ""
        stringToTrim.remove(at: stringToTrim.startIndex)
        stringToTrim.remove(at: stringToTrim.startIndex)
        stringToTrim.insert("+", at: stringToTrim.startIndex)
        
        let index = stringToTrim.index(stringToTrim.startIndex, offsetBy: 3)
        c_Code = String(stringToTrim[..<index])
        stringToTrim = String(stringToTrim[index...])
        return (stringToTrim,c_Code)
    }
}

func wrapFormattedNumber(_ number: String) -> String {
    var agentCode = number
    if agentCode.count > 2 {
        agentCode.remove(at: String.Index.init(encodedOffset: 0))
        agentCode.remove(at: String.Index.init(encodedOffset: 0))
        agentCode.insert("+", at: String.Index.init(encodedOffset: 0))
        let countryDetails = identifyCountry(withPhoneNumber: agentCode)
        if agentCode.hasPrefix(countryDetails.countryCode) {
            agentCode = agentCode.replacingOccurrences(of: countryDetails.countryCode, with: "0")
            return agentCode
        } else {
            return ""
        }
    } else {
        return ""
    }
}

func revertCountryArray() -> NSArray {
    var arrCountry = NSArray.init()
    do {
        if let file = Bundle.main.url(forResource: "mcc_mnc", withExtension: "txt") {
            let data = try Data(contentsOf: file)
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            if let object = json as? NSDictionary {
                if let arr = object.object(forKey: "data") as? NSArray {
                    arrCountry = arr
                    return arrCountry
                }
            } else if let object = json as? [Any] {
                println_debug(object)
            } else {
                println_debug("JSON is invalid")
            }
        } else {
            println_debug("no file")
        }
    } catch {
        println_debug(error.localizedDescription)
    }
    return arrCountry
}

func captureScreen(view:UIView) -> UIImage {
    let bounds = view.bounds
    UIGraphicsBeginImageContextWithOptions(bounds.size, true, 0.0)
    view.drawHierarchy(in: bounds, afterScreenUpdates: false)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return image!
}

func getCurrentDateForQRCode() -> String {
    let dateFormatter : DateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyyMMdd_HHmmss"
    let date = Date()
    let dateString = dateFormatter.string(from: date)
    return dateString    
}

func getCurrentDate() -> String {
    let dateFormatter : DateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let date = Date()
    let dateString = dateFormatter.string(from: date)
    return dateString
}

func getDateType(dateStr: String) -> Date? {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.locale = Locale.current
    return dateFormatter.date(from: dateStr)
}

func getDate(timeStr: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.calendar = Calendar(identifier: .gregorian)
    dateFormatter.dateFormat = "yyyyMMdd_HH:mm:ss"
    //dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    let time = dateFormatter.date(from: timeStr)//timeStr
    dateFormatter.dateFormat = "dd-MMM-yyyy"//hh:mm:ss”
    let date = dateFormatter.string(from: time!)
    return date
}

func getDateForQR(dateStr: String) -> String {
    let date = Date()
    let calender = Calendar.current
    let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
    let year = components.year
    //        let month = components.month
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "LLLL"
    let nameOfMonth = dateFormatter.string(from: date)
    let today_string = String(nameOfMonth) + "-" + String(year!)
    return today_string
}

func amountWithCommaFormat(textStr:String?) -> String {
    var amountStr:String = ""
    if let text = textStr, text.count > 0 {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        let textAfterRemovingComma = text.replacingOccurrences(of: ",", with: "")
        let formattedNum = formatter.string(from: NSNumber(value: Int(textAfterRemovingComma)!))
        amountStr = formattedNum!
    }
    return amountStr
}

func wrapAmountWithCommaDecimal(key: String) -> String {

  let number = NSDecimalNumber.init(string: key)
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
    formatter.minimumFractionDigits = 2
    formatter.maximumFractionDigits = 2

    formatter.numberStyle = .decimal
    formatter.groupingSeparator = ","
    formatter.groupingSize = 3
    formatter.usesGroupingSeparator = true
    if var num = formatter.string(from: number) {
        if num.contains(find: ".") {
            if num == "NaN" {
                return ""
            } else {
                if num.contains(find: ".00") {
                    num = num.replacingOccurrences(of: ".00", with: "")
                    return num
                }
                return num
            }
        } else {
            return (num == "NaN") ? "" : num
        }
    }
    return ""
}

func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat {
    let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
    label.numberOfLines = 0
    label.lineBreakMode = NSLineBreakMode.byWordWrapping
    label.font = font
    label.text = text
    label.sizeToFit()
    return label.frame.height
}

//MARK: - UIApplication extension
extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

//MARK: - UIImage extension
extension UIImage {
    var noir: UIImage? {
        let context = CIContext(options: nil)
        guard let currentFilter = CIFilter(name: "CIPhotoEffectNoir") else { return nil }
        currentFilter.setValue(CIImage(image: self), forKey: kCIInputImageKey)
        if let output = currentFilter.outputImage,
            let cgImage = context.createCGImage(output, from: output.extent) {
            return UIImage(cgImage: cgImage, scale: scale, orientation: imageOrientation)
        }
        return nil
    }
}

//MARK: - String extension
extension String  {
    var isNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}

extension UIViewController {
    
    func showToAlert(_ str: String, image: UIImage, simChanges: Bool)
    {
        // Please enter your valid mobile number
        alertViewObj.wrapAlert(title: "", body: str.localized, img: image)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
           
        })
        
        alertViewObj.showAlert(controller: self)
    }
    
    func showToastForProfile(message : String,handler : @escaping (_ success: Bool) -> Void) {
        
        let toastLabel = UILabel()
        
        toastLabel.frame = CGRect(x: (self.view.frame.width/2) - 125, y: (self.view.frame.height/2) - 80, width: 250, height: 60)
        toastLabel.text = message
        toastLabel.numberOfLines = 0
        toastLabel.font = UIFont(name: appFont,size: 14.0)
        toastLabel.textAlignment = .center
        toastLabel.numberOfLines = 2
        toastLabel.layer.cornerRadius = 30
        toastLabel.layer.masksToBounds = true
        toastLabel.textColor = .white
        toastLabel.backgroundColor = UIColor.black
        self.view.bringSubviewToFront(toastLabel)
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 3.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
            handler(true)
        })
    }
}


extension CNContactViewController {
    @objc func cancelHack()  {
        self.delegate?.contactViewController?(self, didCompleteWith: nil)
    }
}
