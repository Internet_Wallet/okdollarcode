//
//  BCDQRImageModel.swift
//  OK
//
//  Created by SHUBH on 8/13/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BCDQRImageModel: NSObject {

    var merchantID:String = ""
    var paymentType:String = ""
    var amount:String = ""
    var customerName:String = ""
    var registeredPhoneNumber:String = ""
    var remarks:String = ""
    var codeFull:String = ""
}
