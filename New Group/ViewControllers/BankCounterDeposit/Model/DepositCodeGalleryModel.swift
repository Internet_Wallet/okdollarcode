//
//  DepositCodeGalleryModel.swift
//  OK
//
//  Created by PC on 2/24/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class DepositCodeGalleryModel: NSObject {

    var bankId:String = ""
    var bankCode:String = ""
    var bankName:String = ""
    var amount : Int = 0
    var amountDisplay:String = ""
    var codeA:String = ""
    var codeB:String = ""
    var codeC:String = ""
    var codeD:String = ""
    var codeE:String = ""
    var codeF:String = ""
    
    //QRCode
    var codeFull:String = ""
    var merchantID:String = ""
    var paymentType:String = ""
    
    //Unused code
    var date:String = ""
    var time:String = ""
    var depositorName:String = ""
    var depositorNumber:String = ""
    //Used code
    var bankTxDate:String = ""
    var bankTxTime:String = ""
    
    var remarks:String = ""
    
    var notes:String = ""
    var notesBurmish:String = ""

    var depositID:String = ""

    var beneficiaryName:String = ""
    var beneficiaryOkAccountNumber:String = ""
    
    //Used Deposit Code details
    var bankTransactionId:String = ""
    var bankTransactionStatus:String = ""
    var transactionCode:String = ""
    var okTransactionStatus:String = ""
    var branchCode:String = ""
    var branchName:String = ""
    var branchStatus:String = ""

}
