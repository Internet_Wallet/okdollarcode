//
//  BCDBankDetailsHeaderCell.swift
//  OK
//
//  Created by SHUBH on 8/14/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BCDBankDetailsHeaderCell: UITableViewCell {

    @IBOutlet weak var btnArrow: UIButton!

    @IBOutlet weak var lblDepositDetails: UILabel!
    {
        didSet
        {
            lblDepositDetails.text = "Deposit Details".localized
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
