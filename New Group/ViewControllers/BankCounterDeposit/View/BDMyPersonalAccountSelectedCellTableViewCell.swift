//
//  BDMyPersonalAccountSelectedCellTableViewCell.swift
//  OK
//
//  Created by prabu on 29/11/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class BDMyPersonalAccountSelectedCellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var txtAccountNamePersonal: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAccountNumberPersonal: SkyFloatingLabelTextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
