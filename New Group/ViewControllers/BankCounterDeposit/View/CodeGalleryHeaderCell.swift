//
//  CodeGalleryHeaderCell.swift
//  OK
//
//  Created by PC on 2/24/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class CodeGalleryHeaderCell: UITableViewCell {

    @IBOutlet weak var lblUseBelowCode: UILabel!
    @IBOutlet weak var lblCodeF: UILabel!
    @IBOutlet weak var lblCodeE: UILabel!
    @IBOutlet weak var lblCodeD: UILabel!
    @IBOutlet weak var lblCodeC: UILabel!
    @IBOutlet weak var lblCodeB: UILabel!
    @IBOutlet weak var lblCodeA: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblCodeA.layer.borderWidth = 1.0
        lblCodeA.layer.borderColor = UIColor.black.cgColor
        lblCodeB.layer.borderWidth = 1.0
        lblCodeB.layer.borderColor = UIColor.black.cgColor
        lblCodeC.layer.borderWidth = 1.0
        lblCodeC.layer.borderColor = UIColor.black.cgColor
        lblCodeD.layer.borderWidth = 1.0
        lblCodeD.layer.borderColor = UIColor.black.cgColor
        lblCodeE.layer.borderWidth = 1.0
        lblCodeE.layer.borderColor = UIColor.black.cgColor
        lblCodeF.layer.borderWidth = 1.0
        lblCodeF.layer.borderColor = UIColor.black.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
