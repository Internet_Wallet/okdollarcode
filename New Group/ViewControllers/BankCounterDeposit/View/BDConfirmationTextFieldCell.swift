//
//  BDConfirmationTextFieldCell.swift
//  OK
//
//  Created by PC on 2/23/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class BDConfirmationTextFieldCell: UITableViewCell {
  
  @IBOutlet weak var txtFieldData: SkyFloatingLabelTextField!
  @IBOutlet weak var viewLineSeperator: UIView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
}
