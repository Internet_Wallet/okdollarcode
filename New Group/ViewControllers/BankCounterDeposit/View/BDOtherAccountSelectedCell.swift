//
//  BDOtherAccountSelectedCell.swift
//  OK
//
//  Created by PC on 2/7/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class BDOtherAccountSelectedCell: UITableViewCell {

    @IBOutlet weak var txtBusinessName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAccountName: SkyFloatingLabelTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
