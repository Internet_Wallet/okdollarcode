//
//  BDDepositByMyselfCell.swift
//  OK
//
//  Created by PC on 2/8/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class BDDepositByMyselfCell: UITableViewCell {

    @IBOutlet weak var txtAmount: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMyContact: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMyName: SkyFloatingLabelTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
