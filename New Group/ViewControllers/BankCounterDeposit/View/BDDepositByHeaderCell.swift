//
//  BDDepositByHeaderCell.swift
//  OK
//
//  Created by PC on 2/7/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class BDDepositByHeaderCell: UITableViewCell {

    @IBOutlet weak var btnSelectAccount: UIButton!
    @IBOutlet weak var imgArrowIcon: UIImageView!
    @IBOutlet weak var txtDepositBy: SkyFloatingLabelTextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
