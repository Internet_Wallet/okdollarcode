//
//  BCDNotesCell.swift
//  OK
//
//  Created by SHUBH on 8/14/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import WebKit

class BCDNotesCell: UITableViewCell, WKNavigationDelegate{

    @IBOutlet weak var webViewDisplay: WKWebView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func displayNotesWebView(notes : String)
    {
        webViewDisplay.navigationDelegate = self
        
        webViewDisplay.load(URLRequest(url: URL(string: notes)!))
        
        //webViewDisplay.loadRequest(URLRequest(url: URL(string: notes)!))
    }
  
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webViewDisplay.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
            if complete != nil {
                self.webViewDisplay.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                    println_debug(height as! CGFloat)
                    println_debug("finish")
                    progressViewObj.removeProgressView()
                })
            }
        })
    }
    
  func webViewDidStartLoad(_ webView: UIWebView) {
    println_debug("start")
    progressViewObj.showProgressView()
  }
  
  func webViewDidFinishLoad(_ webView: UIWebView) {
    println_debug("finish")
    progressViewObj.removeProgressView()
  }
  
  func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
    
    progressViewObj.removeProgressView()
  }
  
}
