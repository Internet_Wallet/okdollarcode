//
//  HorizontalCollectionViewCell.swift
//  OK
//
//  Created by PC on 3/5/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class HorizontalCollectionViewCell: UICollectionViewCell {

    var depositCodeGalleryModel = DepositCodeGalleryModel()
    var itemIndex: Int = 0

    @IBOutlet weak var tblList: UITableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        tblList.dataSource = self as UITableViewDataSource
        tblList.delegate = self as UITableViewDelegate
    }

}

extension HorizontalCollectionViewCell: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if depositCodeGalleryModel.bankCode == "KBZ"
        {
            if(indexPath.section == 0)
            {
                return 200
            }
            else if(indexPath.section == 1)
            {
                return 300
            }
            return 55
        }
        else
        {
            if(indexPath.section == 0)
            {
                return 200
            }
            return 55
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if depositCodeGalleryModel.bankCode == "KBZ"
        {
            return 3
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if depositCodeGalleryModel.bankCode == "KBZ"
        {
            if(section == 0 || section == 1)
            {
                return 1
            }
            else
            {
                if(depositCodeGalleryModel.remarks.count > 0)
                {
                    return 5
                }
                else
                {
                    return 4
                }
            }
        }
        else//Other than KBZ
        {
            if(section == 0)
            {
                return 1
            }
            else
            {
                if(depositCodeGalleryModel.remarks.count > 0)
                {
                    return 5
                }
                return 4
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0)
        {
            let identifier = "CodeGalleryHeaderCell"
            var cellView: CodeGalleryHeaderCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? CodeGalleryHeaderCell
            if cellView == nil {
                tableView.register (UINib(nibName: "CodeGalleryHeaderCell", bundle: nil), forCellReuseIdentifier: identifier)
                cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? CodeGalleryHeaderCell
            }
            
            cellView.lblCodeF.text = depositCodeGalleryModel.codeF
            cellView.lblCodeE.text = depositCodeGalleryModel.codeE
            cellView.lblCodeD.text = depositCodeGalleryModel.codeD
            cellView.lblCodeC.text = depositCodeGalleryModel.codeC
            cellView.lblCodeB.text = depositCodeGalleryModel.codeB
            cellView.lblCodeA.text = depositCodeGalleryModel.codeA
            cellView.lblAmount.text = String(depositCodeGalleryModel.amountDisplay)
            cellView.lblBankName.text = depositCodeGalleryModel.bankName
            cellView.lblCount.text = String(itemIndex)
            
            if(depositCodeGalleryModel.bankTransactionId.length == 0)
            {
                cellView.lblUseBelowCode.text = cellView.lblUseBelowCode.text?.localized
                cellView.lblUseBelowCode.isHidden = false
            }
            else
            {
                cellView.lblUseBelowCode.isHidden = true
            }
            cellView.selectionStyle = .none
            
            return cellView
        }
        else if depositCodeGalleryModel.bankCode == "KBZ"
        {
            if indexPath.section == 1
            {
                let identifier = "BCDQRImageCell"
                var cellTextField: BCDQRImageCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BCDQRImageCell
                if cellTextField == nil {
                    tableView.register (UINib(nibName: "BCDQRImageCell", bundle: nil), forCellReuseIdentifier: identifier)
                    cellTextField = tableView.dequeueReusableCell(withIdentifier: identifier) as? BCDQRImageCell
                }
                
                cellTextField.generateQRCodeImage(bcdQRImageModel : depositCodeGalleryModel)
                return cellTextField
            }
            else
            {
            let identifier = "BDConfirmationTextFieldCell"
            var cellTextField: BDConfirmationTextFieldCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationTextFieldCell
            if cellTextField == nil {
                tableView.register (UINib(nibName: "BDConfirmationTextFieldCell", bundle: nil), forCellReuseIdentifier: identifier)
                cellTextField = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationTextFieldCell
            }
            
            cellTextField.txtFieldData.isUserInteractionEnabled = false

            if(depositCodeGalleryModel.bankTransactionId.length == 0)
            {
              cellTextField.viewLineSeperator.isHidden = false
              
                if(indexPath.row == 0)
                {
                    cellTextField.txtFieldData.placeholder = "Date".localized
                    cellTextField.txtFieldData.text = depositCodeGalleryModel.date
                }
                else if(indexPath.row == 1)
                {
                    cellTextField.txtFieldData.placeholder = "Time".localized
                    cellTextField.txtFieldData.text = depositCodeGalleryModel.time
                }
                else if(indexPath.row == 2)
                {
                    cellTextField.txtFieldData.placeholder = "Register OK$ Account Name".localized
                    cellTextField.txtFieldData.text = depositCodeGalleryModel.beneficiaryName
                }
                else if(indexPath.row == 3)
                {
                    cellTextField.txtFieldData.placeholder = "OK$ Account Mobile Number".localized
                    cellTextField.txtFieldData.text = self.getActualNum(depositCodeGalleryModel.beneficiaryOkAccountNumber, withCountryCode: "+95")
                }
                else
                {
                    cellTextField.txtFieldData.placeholder = "Remarks".localized
                    cellTextField.txtFieldData.text = depositCodeGalleryModel.remarks
                  cellTextField.viewLineSeperator.isHidden = true
                }
            }
            else
            {
              cellTextField.viewLineSeperator.isHidden = false

                if(indexPath.row == 0)
                {
                    cellTextField.txtFieldData.placeholder = "Bank Transaction Date".localized
                    cellTextField.txtFieldData.text = depositCodeGalleryModel.date
                }
                else if(indexPath.row == 1)
                {
                    cellTextField.txtFieldData.placeholder = "Bank Transaction Time".localized
                    cellTextField.txtFieldData.text = depositCodeGalleryModel.time
                }
                else if(indexPath.row == 2)
                {
                    cellTextField.txtFieldData.placeholder = "Register OK$ Account Name".localized
                    cellTextField.txtFieldData.text = depositCodeGalleryModel.beneficiaryName
                }
                else if(indexPath.row == 3)
                {
                    cellTextField.txtFieldData.placeholder = "OK$ Account Mobile Number".localized
                    cellTextField.txtFieldData.text = self.getActualNum(depositCodeGalleryModel.beneficiaryOkAccountNumber, withCountryCode: "+95")
                }
                else
                {
                    cellTextField.txtFieldData.placeholder = "Remarks".localized
                    cellTextField.txtFieldData.text = depositCodeGalleryModel.remarks
                  cellTextField.viewLineSeperator.isHidden = true

                }
            }
            cellTextField.selectionStyle = .none

            return cellTextField
        }
        }
        else
        {
            let identifier = "BDConfirmationTextFieldCell"
            var cellTextField: BDConfirmationTextFieldCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationTextFieldCell
            if cellTextField == nil {
                tableView.register (UINib(nibName: "BDConfirmationTextFieldCell", bundle: nil), forCellReuseIdentifier: identifier)
                cellTextField = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationTextFieldCell
            }
            
            cellTextField.txtFieldData.isUserInteractionEnabled = false

            if(depositCodeGalleryModel.bankTransactionId.length == 0)
            {
              cellTextField.viewLineSeperator.isHidden = false
              
                if(indexPath.row == 0)
                {
                    cellTextField.txtFieldData.placeholder = "Date".localized
                    cellTextField.txtFieldData.text = depositCodeGalleryModel.date
                }
                else if(indexPath.row == 1)
                {
                    cellTextField.txtFieldData.placeholder = "Time".localized
                    cellTextField.txtFieldData.text = depositCodeGalleryModel.time
                }
                else if(indexPath.row == 2)
                {
                    cellTextField.txtFieldData.placeholder = "Register OK$ Account Name".localized
                    cellTextField.txtFieldData.text = depositCodeGalleryModel.beneficiaryName
                }
                else if(indexPath.row == 3)
                {
                    cellTextField.txtFieldData.placeholder = "OK$ Account Mobile Number".localized
                    cellTextField.txtFieldData.text = self.getActualNum(depositCodeGalleryModel.beneficiaryOkAccountNumber, withCountryCode: "+95")
                }
                else
                {
                    cellTextField.txtFieldData.placeholder = "Remarks".localized
                    cellTextField.txtFieldData.text = depositCodeGalleryModel.remarks
                  cellTextField.viewLineSeperator.isHidden = true

                }
            }
            else
            {
              cellTextField.viewLineSeperator.isHidden = false

                if(indexPath.row == 0)
                {
                    cellTextField.txtFieldData.placeholder = "Bank Transaction Date".localized
                    cellTextField.txtFieldData.text = depositCodeGalleryModel.date
                }
                else if(indexPath.row == 1)
                {
                    cellTextField.txtFieldData.placeholder = "Bank Transaction Time".localized
                    cellTextField.txtFieldData.text = depositCodeGalleryModel.time
                }
                else if(indexPath.row == 2)
                {
                    cellTextField.txtFieldData.placeholder = "Register OK$ Account Name".localized
                    cellTextField.txtFieldData.text = depositCodeGalleryModel.beneficiaryName
                }
                else if(indexPath.row == 3)
                {
                    cellTextField.txtFieldData.placeholder = "OK$ Account Mobile Number".localized
                    cellTextField.txtFieldData.text = self.getActualNum(depositCodeGalleryModel.beneficiaryOkAccountNumber, withCountryCode: "+95")
                }
                else
                {
                    cellTextField.txtFieldData.placeholder = "Remarks".localized
                    cellTextField.txtFieldData.text = depositCodeGalleryModel.remarks
                  cellTextField.viewLineSeperator.isHidden = true
                }
            }
            cellTextField.selectionStyle = .none
            
            return cellTextField
        }
    }
    
    func getActualNum(_ phoneNum: String, withCountryCode countryCode: String) -> String {
        var FormateStr: String
        if phoneNum.hasPrefix("0") {
            _ = (phoneNum).substring(from: 1)
        }
        let notAllowedChars = CharacterSet(charactersIn: "+()")
        let resultString: String = countryCode.components(separatedBy: notAllowedChars).joined(separator: "")
        if (countryCode == "+95") || (countryCode == "(+95)") {
            if phoneNum.hasPrefix("0095") {
                FormateStr = "0\((phoneNum).substring(from: 4))"
            }
            else if phoneNum.hasPrefix("0091") {
                FormateStr = "0\((phoneNum).substring(from: 4))"
            }
            else if phoneNum.hasPrefix("95") {
                FormateStr = "0\((phoneNum).substring(from: 2))"
            }
            else {
                FormateStr = "\(phoneNum)"
            }
            
        }
        else {
            FormateStr = "00\(resultString)\(phoneNum)"
        }
        return FormateStr
    }
    
}

