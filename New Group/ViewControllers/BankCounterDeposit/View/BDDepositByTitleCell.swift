//
//  BDDepositByTitleCell.swift
//  OK
//
//  Created by PC on 2/7/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BDDepositByTitleCell: UITableViewCell {

    @IBOutlet weak var btnSelectDepositBy: UIButton!
    @IBOutlet weak var lblTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
