//
//  BCDQRImageCell.swift
//  OK
//
//  Created by SHUBH on 8/13/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BCDQRImageCell: UITableViewCell {

    @IBOutlet weak var imgBankLogo: UIImageView!
    @IBOutlet weak var imgQRCode: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func generateQRCodeImage(bcdQRImageModel : DepositCodeGalleryModel)
    {
        var BenificieryNumber = bcdQRImageModel.beneficiaryOkAccountNumber
        
        if BenificieryNumber.hasPrefix("0095"){
            BenificieryNumber = "0\((BenificieryNumber).substring(from: 4))"
        }
        
        let firstPart  = "QR1:" + "\(bcdQRImageModel.merchantID)" + "||__QR2:" + "\(bcdQRImageModel.paymentType) " + "||__QR3:" + "\(bcdQRImageModel.amountDisplay)" + "||__QR4:" + "\(bcdQRImageModel.beneficiaryName)" + "||__QR5:" + "\(BenificieryNumber)" + "||__QR6:" + "||__QR7:" + "\(bcdQRImageModel.remarks)" + "||__QR8:" + "\(bcdQRImageModel.codeFull)" + "||__QR9:" + "||__QR10:"
        println_debug(firstPart)
        let finalPart = String.init(format:"%@", firstPart)
        
        let qrObject = PTQRGenerator.init()
        imgQRCode.image = qrObject.getQRImage(stringQR: finalPart, withSize: 10)
    }
    
}
