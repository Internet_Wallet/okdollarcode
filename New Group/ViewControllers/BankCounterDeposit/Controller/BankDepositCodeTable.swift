//
//  BankDepositCodeTable.swift
//  OK
//
//  Created by gauri OK$ on 1/22/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class BankDepositCodeTable: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BankDepositCodeVC: UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width:200, height: 1))
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 1.0;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if depositGalleryModel.bankCode == "KBZ" {
            if section == 0 || section == 1 || section == 2 {
                return 0
            } else {
                return 55
            }
        } else {
            if section == 0 || section == 1 {
                return 0
            } else {
                return 55
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if depositGalleryModel.bankCode == "KBZ" {
            if indexPath.section == 0 //code
            {
                return 200
            } else if indexPath.section == 1 //QRCode
            {
                return 300
            }
            else if(indexPath.section == 2)//WebView
            {
                return 1300
            }
            return 55
        }
        else
        {
            if(indexPath.section == 0)//code
            {
                return 200
            }
            else if(indexPath.section == 1)//WebView
            {
                return 1215//2000
            }
            return 55
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if depositGalleryModel.bankCode == "KBZ"
        {
            return 4
        }
        else
        {
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if depositGalleryModel.bankCode == "KBZ"
        {
            if(section == 0 || section == 1 || section == 2)
            {
                return 1
            }
            else
            {
                if isArrowStatus {
                    if(addMoneyModel.remarks.count > 0)
                    {
                        return 11
                    }
                    return 10
                }
                return 0
            }
        }
        else//Other than KBZ
        {
            if(section == 0 || section == 1)
            {
                return 1
            }
            else
            {
                if isArrowStatus {
                if(addMoneyModel.remarks.count > 0)
                {
                    return 11
                }
                return 10
                }
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "BDConfirmationTextFieldCell"
        var cellTextField: BDConfirmationTextFieldCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationTextFieldCell
        if cellTextField == nil {
            tableView.register (UINib(nibName: "BDConfirmationTextFieldCell", bundle: nil), forCellReuseIdentifier: identifier)
            cellTextField = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationTextFieldCell
        }
        
        if(indexPath.section == 0)
        {
            let identifier = "CodeGalleryHeaderCell"
            var cellView: CodeGalleryHeaderCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? CodeGalleryHeaderCell
            if cellView == nil {
                tableView.register (UINib(nibName: "CodeGalleryHeaderCell", bundle: nil), forCellReuseIdentifier: identifier)
                cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? CodeGalleryHeaderCell
            }
            
            cellView.lblCodeF.text = depositGalleryModel.codeF
            cellView.lblCodeE.text = depositGalleryModel.codeE
            cellView.lblCodeD.text = depositGalleryModel.codeD
            cellView.lblCodeC.text = depositGalleryModel.codeC
            cellView.lblCodeB.text = depositGalleryModel.codeB
            cellView.lblCodeA.text = depositGalleryModel.codeA
            cellView.lblAmount.text = String(depositGalleryModel.amountDisplay)
            cellView.lblBankName.text = depositGalleryModel.bankName
            cellView.lblCount.isHidden = true
            cellView.lblUseBelowCode.text = cellView.lblUseBelowCode.text?.localized
            
            cellView.selectionStyle = .none
            
            return cellView
        }
        else if depositGalleryModel.bankCode == "KBZ"
        {
            if indexPath.section == 1//QRCode
            {
                let identifier = "BCDQRImageCell"
                var cellQRCode: BCDQRImageCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BCDQRImageCell
                if cellQRCode == nil {
                    tableView.register (UINib(nibName: "BCDQRImageCell", bundle: nil), forCellReuseIdentifier: identifier)
                    cellQRCode = tableView.dequeueReusableCell(withIdentifier: identifier) as? BCDQRImageCell
                }
                cellQRCode.generateQRCodeImage(bcdQRImageModel : depositGalleryModel)
                cellQRCode.selectionStyle = .none
                
                return cellQRCode
            }
            else if indexPath.section == 2//Notes Display
            {
                let identifier = "BCDNotesCell"
                var cellNotes: BCDNotesCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BCDNotesCell
                if cellNotes == nil {
                    tableView.register (UINib(nibName: "BCDNotesCell", bundle: nil), forCellReuseIdentifier: identifier)
                    cellNotes = tableView.dequeueReusableCell(withIdentifier: identifier) as? BCDNotesCell
                }
                
                if appDelegate.currentLanguage == "my"
                {
                    cellNotes.displayNotesWebView( notes : urlStr)
                }
                else{
                    cellNotes.displayNotesWebView( notes : depositGalleryModel.notes)
                }
                cellNotes.selectionStyle = .none
                
                return cellNotes
            }
            else if indexPath.section == 3//Bank Details
            {
                let identifier = "BDConfirmationTextFieldCell"
                var cellTextField: BDConfirmationTextFieldCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationTextFieldCell
                if cellTextField == nil {
                    tableView.register (UINib(nibName: "BDConfirmationTextFieldCell", bundle: nil), forCellReuseIdentifier: identifier)
                    cellTextField = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationTextFieldCell
                }
                cellTextField.txtFieldData.isUserInteractionEnabled = false
                cellTextField.selectionStyle = .none
                
                let identifierLabel = "BDConfirmationLabelCell"
                var cellView: BDConfirmationLabelCell! = tableView.dequeueReusableCell(withIdentifier: identifierLabel) as? BDConfirmationLabelCell
                if cellView == nil {
                    tableView.register (UINib(nibName: "BDConfirmationLabelCell", bundle: nil), forCellReuseIdentifier: identifierLabel)
                    cellView = tableView.dequeueReusableCell(withIdentifier: identifierLabel) as? BDConfirmationLabelCell
                }
                cellView.selectionStyle = .none
                
                if(indexPath.row == 0)
                {
                    cellView.lblTitle.text = "Bank Details".localized
                    cellView.lblTitle.backgroundColor = .gray
                    return cellView
                }
                else if(indexPath.row == 1)
                {
                    cellView.lblTitle.text = depositGalleryModel.bankName
                    cellView.lblTitle.backgroundColor = .white
                    return cellView
                }
                else if(indexPath.row == 2)
                {
                    cellTextField.txtFieldData.placeholder = "Date".localized
                    cellTextField.txtFieldData.text = depositGalleryModel.date
                    return cellTextField
                }
                else if(indexPath.row == 3)
                {
                    cellTextField.txtFieldData.placeholder = "Time".localized
                    cellTextField.txtFieldData.text =  depositGalleryModel.time
                    return cellTextField
                }
                else if(indexPath.row == 4)
                {
                    if(addMoneyModel.depositeBy == "Myself")
                    {
                        cellView.lblTitle.text = "My OK$ Account Details".localized
                    }
                    else
                    {
                        cellView.lblTitle.text = "Other OK$ Account Details".localized
                    }
                    cellView.lblTitle.backgroundColor = .gray
                    return cellView
                }
                else if(indexPath.row == 5)
                {
                    cellTextField.txtFieldData.placeholder = "Beneficiary Name".localized
                    cellTextField.txtFieldData.text = depositGalleryModel.beneficiaryName
                    return cellTextField
                }
                else if(indexPath.row == 6)
                {
                    cellTextField.txtFieldData.placeholder = "Beneficiary OK$ Account Number".localized
                    cellTextField.txtFieldData.text =  self.getActualNum(depositGalleryModel.beneficiaryOkAccountNumber, withCountryCode: "+95")
                    return cellTextField
                }
                else if(indexPath.row == 7)
                {
                    cellView.lblTitle.text = "Deposit Details".localized
                    cellView.lblTitle.backgroundColor = .gray
                    return cellView
                }
                if(addMoneyModel.depositeBy == "Myself")
                {
                    if(indexPath.row == 8)
                    {
                        cellTextField.txtFieldData.placeholder = "Depositor Name".localized
                        cellTextField.txtFieldData.text =  addMoneyModel.depositorName
                        return cellTextField
                    }
                    else if(indexPath.row == 9)
                    {
                        cellTextField.txtFieldData.placeholder = "Depositor Contact Number".localized
                        cellTextField.txtFieldData.text =  self.getActualNum(addMoneyModel.depositorContactNumber, withCountryCode: "+95")
                        return cellTextField
                    }
                    else
                    {
                        cellTextField.txtFieldData.placeholder = "Remarks".localized
                        cellTextField.txtFieldData.text = addMoneyModel.remarks
                        return cellTextField
                    }
                }
                else
                {
                    if(indexPath.row == 8)
                    {
                        cellTextField.txtFieldData.placeholder = "Depositor Name".localized
                        cellTextField.txtFieldData.text =  addMoneyModel.depositorOtherContactName
                        return cellTextField
                    }
                    else if(indexPath.row == 9)
                    {
                        cellTextField.txtFieldData.placeholder = "Depositor Contact Number".localized
                        cellTextField.txtFieldData.text =  self.getActualNum(addMoneyModel.depositorOtherContactNumber, withCountryCode: "+95")
                        return cellTextField
                    }
                    else
                    {
                        cellTextField.txtFieldData.placeholder = "Remarks".localized
                        cellTextField.txtFieldData.text = addMoneyModel.remarks
                        return cellTextField
                    }
                }
            }
        }
        else//Other Bank
        {
            if indexPath.section == 1//Notes Display
            {
                let identifier = "BCDNotesCell"
                var cellNotes: BCDNotesCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BCDNotesCell
                if cellNotes == nil {
                    tableView.register (UINib(nibName: "BCDNotesCell", bundle: nil), forCellReuseIdentifier: identifier)
                    cellNotes = tableView.dequeueReusableCell(withIdentifier: identifier) as? BCDNotesCell
                }
                
                if appDelegate.currentLanguage == "my"
                {
                    cellNotes.displayNotesWebView( notes : urlStr)
                }
                else{
                    cellNotes.displayNotesWebView( notes : depositGalleryModel.notes)
                }
                cellNotes.selectionStyle = .none
                
                return cellNotes
            }
            else if indexPath.section == 2//Bank Details
            {
                let identifier = "BDConfirmationTextFieldCell"
                var cellTextField: BDConfirmationTextFieldCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationTextFieldCell
                if cellTextField == nil {
                    tableView.register (UINib(nibName: "BDConfirmationTextFieldCell", bundle: nil), forCellReuseIdentifier: identifier)
                    cellTextField = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationTextFieldCell
                }
                cellTextField.txtFieldData.isUserInteractionEnabled = false
                cellTextField.selectionStyle = .none
                
                let identifierLabel = "BDConfirmationLabelCell"
                var cellView: BDConfirmationLabelCell! = tableView.dequeueReusableCell(withIdentifier: identifierLabel) as? BDConfirmationLabelCell
                if cellView == nil {
                    tableView.register (UINib(nibName: "BDConfirmationLabelCell", bundle: nil), forCellReuseIdentifier: identifierLabel)
                    cellView = tableView.dequeueReusableCell(withIdentifier: identifierLabel) as? BDConfirmationLabelCell
                }
                cellView.selectionStyle = .none
                
                if(indexPath.row == 0)
                {
                    cellView.lblTitle.text = "Bank Details".localized
                    cellView.lblTitle.backgroundColor = .gray
                    return cellView
                }
                else if(indexPath.row == 1)
                {
                    cellView.lblTitle.text = depositGalleryModel.bankName
                    cellView.lblTitle.backgroundColor = .white
                    return cellView
                }
                else if(indexPath.row == 2)
                {
                    cellTextField.txtFieldData.placeholder = "Date".localized
                    cellTextField.txtFieldData.text = depositGalleryModel.date
                    return cellTextField
                }
                else if(indexPath.row == 3)
                {
                    cellTextField.txtFieldData.placeholder = "Time".localized
                    cellTextField.txtFieldData.text =  depositGalleryModel.time
                    return cellTextField
                }
                else if(indexPath.row == 4)
                {
                    if(addMoneyModel.depositeBy == "Myself")
                    {
                        cellView.lblTitle.text = "My OK$ Account Details".localized
                    }
                    else
                    {
                        cellView.lblTitle.text = "Other OK$ Account Details".localized
                    }
                    cellView.lblTitle.backgroundColor = .gray
                    return cellView
                }
                else if(indexPath.row == 5)
                {
                    cellTextField.txtFieldData.placeholder = "Beneficiary Name".localized
                    cellTextField.txtFieldData.text = depositGalleryModel.beneficiaryName
                    return cellTextField
                }
                else if(indexPath.row == 6)
                {
                    cellTextField.txtFieldData.placeholder = "Beneficiary OK$ Account Number".localized
                    cellTextField.txtFieldData.text =  self.getActualNum(depositGalleryModel.beneficiaryOkAccountNumber, withCountryCode: "+95")
                    return cellTextField
                }
                else if(indexPath.row == 7)
                {
                    cellView.lblTitle.text = "Deposit Details".localized
                    cellView.lblTitle.backgroundColor = .gray
                    return cellView
                }
                if(addMoneyModel.depositeBy == "Myself")
                {
                    if(indexPath.row == 8)
                    {
                        cellTextField.txtFieldData.placeholder = "Depositor Name".localized
                        cellTextField.txtFieldData.text =  addMoneyModel.depositorName
                        return cellTextField
                    }
                    else if(indexPath.row == 9)
                    {
                        cellTextField.txtFieldData.placeholder = "Depositor Contact Number".localized
                        cellTextField.txtFieldData.text =  self.getActualNum(addMoneyModel.depositorContactNumber, withCountryCode: "+95")
                        return cellTextField
                    }
                    else
                    {
                        cellTextField.txtFieldData.placeholder = "Remarks".localized
                        cellTextField.txtFieldData.text = addMoneyModel.remarks
                        return cellTextField
                    }
                }
                else
                {
                    if(indexPath.row == 8)
                    {
                        cellTextField.txtFieldData.placeholder = "Depositor Name".localized
                        cellTextField.txtFieldData.text =  addMoneyModel.depositorOtherContactName
                        return cellTextField
                    }
                    else if(indexPath.row == 9)
                    {
                        cellTextField.txtFieldData.placeholder = "Depositor Contact Number".localized
                        cellTextField.txtFieldData.text =  self.getActualNum(addMoneyModel.depositorOtherContactNumber, withCountryCode: "+95")
                        return cellTextField
                    }
                    else
                    {
                        cellTextField.txtFieldData.placeholder = "Remarks".localized
                        cellTextField.txtFieldData.text = addMoneyModel.remarks
                        return cellTextField
                    }
                }
            }
        }
        return cellTextField
    }
    
    // Header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width:200, height: 1))
        view.backgroundColor = .clear
        
        if depositGalleryModel.bankCode == "KBZ"
        {
            if(section == 3)
            {
                let identifier = "BCDBankDetailsHeaderCell"
                var cellView: BCDBankDetailsHeaderCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BCDBankDetailsHeaderCell
                if cellView == nil {
                    tableView.register (UINib(nibName: "BCDBankDetailsHeaderCell", bundle: nil), forCellReuseIdentifier: identifier)
                    cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? BCDBankDetailsHeaderCell
                }
                
                cellView.btnArrow.addTarget(self, action:#selector(self.btnArrowAction(sender:)), for: .touchUpInside)
                
                if isArrowStatus {
                    cellView.btnArrow.setImage(#imageLiteral(resourceName: "up_arrow"), for: .normal)
                } else {
                    cellView.btnArrow.setImage(#imageLiteral(resourceName: "down_arrow"), for: .normal)
                }
                
                return cellView
            }
        }
        else
        {
            if(section == 2)
            {
                let identifier = "BCDBankDetailsHeaderCell"
                var cellView: BCDBankDetailsHeaderCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BCDBankDetailsHeaderCell
                if cellView == nil {
                    tableView.register (UINib(nibName: "BCDBankDetailsHeaderCell", bundle: nil), forCellReuseIdentifier: identifier)
                    cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? BCDBankDetailsHeaderCell
                }
                
                 cellView.btnArrow.addTarget(self, action:#selector(self.btnArrowAction(sender:)), for: .touchUpInside)
                
                if isArrowStatus {
                    cellView.btnArrow.setImage(#imageLiteral(resourceName: "up_arrow"), for: .normal)
                } else {
                    cellView.btnArrow.setImage(#imageLiteral(resourceName: "down_arrow"), for: .normal)
                }
                return cellView
            }
        }
        return view
        
    }
    
    @objc func btnArrowAction(sender: UIButton!) {
        println_debug("btnArrowAction")
        if isArrowStatus {
            println_debug("YES")
            isArrowStatus = false
            tblList.reloadData()
        } else {
            println_debug("NO")
            isArrowStatus = true
            tblList.reloadData()
        }
    }
}
