//
//  BankCounterDepositConfirmationVC.swift
//  OK
//
//  Created by PC on 2/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BankCounterDepositConfirmationVC: OKBaseController {

    var dictData = ""
    var addMoneyModel = AddMoneyModel()
    var bankDetailsModel = BankCounterDepositModel()
    var depositGalleryModel = DepositCodeGalleryModel()
    var bcdMerchantModel = BCDMerchantModel()

    @IBOutlet weak var tblConfirmation: UITableView!
    
    var strHashValue = ""

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
            appDel.floatingButtonControl?.window.closeButton?.isHidden = true
            appDel.floatingButtonControl?.window.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Confirmation".localized

        self.helpSupportNavigationEnum = .Add_Withdraw

        // Do any additional setup after loading the view.
        println_debug(addMoneyModel)
    }

    @IBAction func btnBackAction(_ sender: Any) {
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    func getAccessToken()
    {
        println_debug("getAccessToken")
        
        if appDelegate.checkNetworkAvail() {
            
            showProgressView()
            
            let urlStr   = Url.AddMoney_GetBanksToken
            let tokenUrl = getUrl(urlStr: urlStr, serverType: .bankCounterDepositUrl)
            
            let hashValue = Url.aKey_AddMoneyBank.hmac_SHA1(key: Url.sKey_AddMoneyBank)
            
            let inputValue = "password=\(hashValue)&grant_type=password"
            
            let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: "POST", contentType: LLBStrings.kContentType_urlencoded, inputVal: inputValue, authStr: nil)
            JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
                
                guard isSuccess else
                {
                    println_debug("Token API Fail")
                    return
                }
                println_debug("\n\n\n Token Response dict : \(response!)")
                
                let responseDic = response as! NSDictionary
                guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                    self.removeProgressView()
                    return
                }
                
                let authorizationString =  "\(tokentype) \(accesstoken)"
                println_debug(authorizationString)
                
                if(self.bankDetailsModel.BankCode != "KBZ")
                {
                    self.callSubmitCounterDepositAPI(accesstoken: authorizationString)
                }
                else
                {
                    self.callSubmitKBZCodeAPI(accesstoken: authorizationString)
                }
            })
            
        }
    }
    
    func callSubmitKBZCodeAPI(accesstoken:String)
    {
        let urlStr = String.init(format: Url.BankCounter_BankID,bankDetailsModel.BankId)

        //let urlStr   = Url.BankCounter_BankID
        let tokenUrl = getUrl(urlStr: urlStr, serverType: .bankCounterDepositUrl)
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: kMethod_Get, contentType: kContentType_Json, inputVal: nil, authStr: accesstoken)
        
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else
            {
                println_debug(" API Fail")
                self.removeProgressView()
                return
            }
            DispatchQueue.main.async {
                self.removeProgressView()
            }
            
                let arrMerchantDetails = response as! Array<Dictionary<String,AnyObject>>
                println_debug(arrMerchantDetails)
                
                for elementStudentImg in arrMerchantDetails {
                    println_debug(elementStudentImg)
                    
                    if(elementStudentImg["ParameterName"] as! String == "Merchant ID")
                    {
                       self.bcdMerchantModel.merchantID = elementStudentImg["ParameterValue"] as? String ?? ""
                    }
                    else if(elementStudentImg["ParameterName"] as! String == "Payment Type")
                    {
                        self.bcdMerchantModel.paymentType = elementStudentImg["ParameterValue"] as? String ?? ""
                    }
                }
            
            self.callSubmitCounterDepositAPI(accesstoken: accesstoken)

        })
               
    }
    
    func callSubmitCounterDepositAPI(accesstoken:String)
    {
        
        var BenificieryNumber = ""
        var BenificieryName = ""
        var DepositorNumber = ""
        var DepositorName = ""
        
        //Benificiery Details
        if addMoneyModel.IsMyOwnAccount//Own Account Transfer
        {
            BenificieryNumber = UserModel.shared.mobileNo
            BenificieryName = UserModel.shared.name
        }
        else
        {
            BenificieryNumber = addMoneyModel.depositorContactNumber
            BenificieryName = addMoneyModel.okACOwnername
        }
        
        //Depositor Details
        if addMoneyModel.depositeBy == "Myself"
        {
            DepositorNumber = UserModel.shared.mobileNo
            DepositorName = UserModel.shared.name
        }
        else
        {
            DepositorNumber = addMoneyModel.depositorOtherContactNumber
            DepositorName = addMoneyModel.depositorOtherContactName
        }
        ///
        if BenificieryNumber.hasPrefix("09"){
            BenificieryNumber = "0095\((BenificieryNumber).substring(from: 1))"
        }
        
        if DepositorNumber.hasPrefix("09"){
            DepositorNumber = "0095\((DepositorNumber).substring(from: 1))"
        }
        
        //Generate pipepline string
        let strPipeLine = "BankId=\(bankDetailsModel.BankId)|IsMyOwnAccount=\(addMoneyModel.IsMyOwnAccount)|DepositorNrc=\(UserModel.shared.nrc)|BeneficiaryOkAccountNumber=\(BenificieryNumber)|IsBusinessAccount=\(addMoneyModel.IsMyOwnAccount)|OkAccountNumber=\(UserModel.shared.mobileNo)|BusinessName2=\(addMoneyModel.businessName)|BeneficiaryName=\(BenificieryName)|DepositorName=\(DepositorName)|CustomerName=\(UserModel.shared.name)|BusinessName1=\( UserModel.shared.businessName)|Remark=\(addMoneyModel.remarks)|Amount=\(addMoneyModel.enteredAmount)|DepositorContactNumber=\(DepositorNumber)"
        
        let hashValue = strPipeLine.hmac_SHA1(key: Url.sKey_AddMoneyBank)
       
        let urlStr   = Url.BankCounter_Deposit
        let tokenUrl = getUrl(urlStr: urlStr, serverType: .bankCounterDepositUrl)
        
        //Convert Array Data to JSON format
        let keys : [String] = ["BankId","IsMyOwnAccount","DepositorNrc","BeneficiaryOkAccountNumber","IsBusinessAccount","OkAccountNumber","BusinessName2","BeneficiaryName","DepositorName","CustomerName","BusinessName1","Remark","Amount","DepositorContactNumber","HashValue"]
        let values : [String] = [bankDetailsModel.BankId,String(addMoneyModel.IsMyOwnAccount),UserModel.shared.nrc,BenificieryNumber,String(addMoneyModel.IsMyOwnAccount),UserModel.shared.mobileNo,addMoneyModel.businessName,BenificieryName,DepositorName,UserModel.shared.name,UserModel.shared.businessName,addMoneyModel.remarks,addMoneyModel.enteredAmount,DepositorNumber,hashValue]
        
        let jsonObj = JSONStringWriter()
        let jsonStr = jsonObj.writeJsonIntoString(keys, values)
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: kMethod_Post, contentType: kContentType_Json, inputVal: jsonStr, authStr: accesstoken)
        
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else
            {
                println_debug(" API Fail")
                self.removeProgressView()
                return
            }
            DispatchQueue.main.async {
                self.removeProgressView()
            }
            if let dictResponse = response as? Dictionary<String, Any> {
                println_debug("\n\n\n Data : \(dictResponse)")
                
                //Handle crash Jun02
                if let responseData = dictResponse["Data"] as? String {
                    
                    let tempData = OKBaseController.convertToDictionary(text: responseData)
                    
                    //EB-6C-78
                    let strCode = tempData!["TransactionCode"] as! String
                    self.depositGalleryModel.transactionCode = tempData!["TransactionCode"] as? String ?? ""
                    
                    let arrTemp = strCode.split(separator: "-")
                    let firstPart = arrTemp[0]
                    let secondPart = arrTemp[1]
                    let thirdPart = arrTemp[2]
                    
                    self.depositGalleryModel.codeFull = strCode
                    self.depositGalleryModel.codeA = String(Array(firstPart)[0])
                    self.depositGalleryModel.codeB = String(Array(firstPart)[1])
                    self.depositGalleryModel.codeC = String(Array(secondPart)[0])
                    self.depositGalleryModel.codeD = String(Array(secondPart)[1])
                    self.depositGalleryModel.codeE = String(Array(thirdPart)[0])
                    self.depositGalleryModel.codeF = String(Array(thirdPart)[1])
                    
                    let strDateTime = self.getDateAndTime(tempData!["TransactionDate"] as? String ?? "")
                    let arrDateTime = strDateTime.split(separator: "&")
                    self.depositGalleryModel.date = String(arrDateTime[0])
                    self.depositGalleryModel.time = String(arrDateTime[1])
                    
                    if let tempBank = tempData!["Bank"] as? Dictionary<String, Any> {
                    self.depositGalleryModel.notes = tempBank["Notes"] as? String ?? ""
                    self.depositGalleryModel.notesBurmish = tempBank["NotesBurmese"] as? String ?? ""
                    self.depositGalleryModel.bankName = tempBank["BankName"] as? String ?? ""
                    self.depositGalleryModel.bankCode = tempBank["BankCode"] as? String ?? ""

                    }
                    self.depositGalleryModel.beneficiaryOkAccountNumber = tempData!["BeneficiaryOkAccountNumber"] as? String ?? ""
                    self.depositGalleryModel.beneficiaryName = tempData!["BeneficiaryName"] as? String ?? ""
                    
                    self.depositGalleryModel.depositorName = tempData!["DepositorName"] as? String ?? ""
                    self.depositGalleryModel.depositorNumber = tempData!["DepositorContactNumber"] as? String ?? ""
                    
                    //Use comma in digit
                    let strAmount = tempData!["Amount"] as? Int ?? 0
                    self.depositGalleryModel.amountDisplay = self.getDigitDisplay(String(strAmount))
                    self.depositGalleryModel.amount = tempData!["Amount"] as? Int ?? 0
                    self.depositGalleryModel.bankCode = self.bankDetailsModel.BankCode
                    
                    if let controller:BankDepositCodeVC = UitilityClass.getStoryBoardAddWithdraw().instantiateViewController(withIdentifier: "BankDepositCodeVC") as? BankDepositCodeVC {
                        controller.addMoneyModel = self.addMoneyModel
                        controller.depositGalleryModel = self.depositGalleryModel
                        controller.bcdMerchantModel = self.bcdMerchantModel
                        DispatchQueue.main.async {
                            self.navigationController?.pushViewController(controller, animated: true)
                        }
                    }
                }
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


