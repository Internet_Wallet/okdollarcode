//
//  BankCounterDepositDetailsVC.swift
//  OK
//
//  Created by PC on 2/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class BankCounterDepositDetailsVC: OKBaseController,UITextFieldDelegate  {
  
  @IBOutlet weak var tblAccountList: UITableView!
  @IBOutlet weak var lblBalance: UILabel!
  @IBOutlet weak var viewCodeGallery: UIView!
  //@IBOutlet weak var viewRequiredField: UIView!
  @IBOutlet weak var btnWalletBalance: UIButton!
  @IBOutlet weak var lblLogin: UILabel!
    {
    didSet
    {
       self.lblLogin.font = UIFont(name: appFont, size: appFontSize)
      self.lblLogin.text = "Login".localized
    }
  }
  
  @IBOutlet weak var lblWalletBalance: UILabel!
    {
    didSet
    {
      self.lblWalletBalance.text = self.lblWalletBalance.text?.localized
    }
  }
  
  @IBOutlet weak var btnCodeGallery: UIButton!
    {
    didSet
    {
      self.btnCodeGallery.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
      self.btnCodeGallery.setTitle(self.btnCodeGallery.titleLabel?.text?.localized, for: .normal)
    }
  }
  //Manage Contact Suggestion
  var contactSuggessionView   : ContactSuggestionVC?
  
  //Show User Balance
  var isViewBalance:Bool = false
  //Select Account Type
  var isSelectOkAccount:Bool = false
  //My Account Selection
  var isMyAccountSelected:Bool = false
  var isOtherAccountSelected:Bool = false
  //Other Account Selection
  var isContactSelected:Bool = false
  var isConfirmMobileNoShow:Bool = false
  var isOtherAccountShow:Bool = false
  
  //Select DepsoitBy
  var isSelectDepositBy:Bool = false
  var isSelectDepositByMySelf:Bool = false
  var isSelectDepositByOthers:Bool = false
  var isOtherDepsoitAccountShow:Bool = false
  var isOtherDepositContactSelected:Bool = false
  
  var isShowEnterAmountOthers:Bool = false
    
  var isPersonalAccountNumber:Bool = false

  
  var isRemark:Bool = false
  //Show&Hide More Option
  var isMoreStatus:Bool = false
  
  var emptyArray = [String.Element]()
  var strEnterAmountValue = ""
  var strMaxAmountValue = ""

  //Manage Model Data
  let addMoneyModel =  AddMoneyModel()
  var bankDetailsModel = BankCounterDepositModel()
  
  //Validation for Mobile numbers prefix
  let validObj  = PayToValidations()
  var strEnterMobileValue = ""
  var lastCountIndex = 0
  
  //MARK: View Life Cycle
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.title = "Bank Counter Deposit Add".localized
    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
    
    viewCodeGallery.isHidden = true
    
    if UserLogin.shared.loginSessionExpired {
      btnWalletBalance.isHidden = false
      lblLogin.isHidden = false
    } else{
      lblBalance.attributedText = self.walletBalance()
      btnWalletBalance.isHidden = true
      lblLogin.isHidden = true
    }
    
      appDel.floatingButtonControl?.window.closeButton?.isHidden = true
      appDel.floatingButtonControl?.window.isHidden = true
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.helpSupportNavigationEnum = .Add_Withdraw
    
    self.loadContactSuggessionView()
    
    // Do any additional setup after loading the view.
    tblAccountList.tableFooterView = UIView(frame: CGRect.zero)
    tblAccountList.sectionFooterHeight = 0.0
    
    //Manage Keyboard
    IQKeyboardManager.sharedManager().enable            = false
    IQKeyboardManager.sharedManager().enableAutoToolbar = false
    IQKeyboardManager.sharedManager().shouldShowToolbarPlaceholder = false
    IQKeyboardManager.sharedManager().placeholderFont = UIFont(name: appFont, size: 16)
    
    let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.suggestionViewHideAction(sender:)))
    self.tblAccountList.addGestureRecognizer(gesture)
    
    println_debug(bankDetailsModel.BusinessAmountLimit)
    println_debug(bankDetailsModel.PersonalAmountLimit)
    println_debug(bankDetailsModel.AdvanceMerchantLimit)
    println_debug(bankDetailsModel.AgentLimit)
    
    //Based on User type Max Amount Updated
    if UserModel.shared.agentType == .user {//Personal user
      strMaxAmountValue = String(bankDetailsModel.PersonalAmountLimit)
    } else if UserModel.shared.agentType == .advancemerchant {//Advance merchant
        strMaxAmountValue = String(bankDetailsModel.AdvanceMerchantLimit)
    } else if UserModel.shared.agentType == .agent {//Agent user
        strMaxAmountValue = String(bankDetailsModel.AgentLimit)
    } else {//Merchant user
      strMaxAmountValue = String(bankDetailsModel.BusinessAmountLimit)
    }
  }
  
  @objc func suggestionViewHideAction(sender : UITapGestureRecognizer) {
    
    self.hideContactSuggesionView()
  }
  
  @IBAction func onClickBackBtn(_ sender: Any) {
    
    self.navigationController?.popViewController(animated: true)
  }
  
  @IBAction func codeGalleryAction(_ sender: Any) {
    
    viewCodeGallery.isHidden = true
    
    let controller:DepositCodeGalleryVC = UitilityClass.getStoryBoardAddWithdraw().instantiateViewController(withIdentifier: "DepositCodeGalleryVC") as! DepositCodeGalleryVC
    controller.strHeaderAuthorization = bankDetailsModel.Bearer
    self.navigationController?.pushViewController(controller, animated: true)
  }
  
  @IBAction func onClickMoreBtn(_ sender: Any) {
    println_debug("onClickMoreBtn")
    if isMoreStatus == false
    {
      isMoreStatus = true
      viewCodeGallery.isHidden = false
    }
    else
    {
      isMoreStatus = false
      viewCodeGallery.isHidden = true
    }
  }
  
  //Hide/View Balance
  @IBAction func btnViewBalanceAction(_ sender: Any) {
    
    if UserLogin.shared.loginSessionExpired {
      OKPayment.main.authenticate(screenName: "BankCounterDepositDetailViewBalance", delegate: self)
    } else{
      lblBalance.attributedText = self.walletBalance()
      btnWalletBalance.isHidden = true
      lblLogin.isHidden = true
    }
  }
  
  //MARK:- API Methods
  func validateOkDollarNumberAPI(phoneNumber: Any) {   //Verify Mobile No
    
    if appDelegate.checkNetworkAvail() {
      let web      = WebApiClass()
      web.delegate = self
      showProgressView()
      
      let buldApi = Url.billSplitterMobileValidation + "MobileNumber=\(phoneNumber)"
      
      let url = getUrl(urlStr: buldApi, serverType: .serverApp)
      println_debug(url)
      
      let params = Dictionary<String,String>()
      
      web.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", mScreen: "mMobileValidation")
    }
  }
  
  func navigationbarController(toEnable: Bool) {
    if toEnable {
      self.navigationController?.navigationBar.layer.zPosition = 0
      self.title = "Bank Counter Deposit Add".localized
      
      self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
    } else {
      self.navigationController?.navigationBar.layer.zPosition = -64
    }
    self.navigationController?.navigationBar.isUserInteractionEnabled = toEnable
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - Text Field Delegate
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
   /*
    if(textField.tag == 60)//for Total Amount
    {
      let str = (textField.text! + string)
      if str.count <= 10 {
        
        return true
      }
      return false
    }
    else  if(textField.tag == 63)//for other user Total Amount
    {
      let str = (textField.text! + string)
      if str.count <= 10 {
        
        return true
      }
      return false
    } */
    
    if(textField.tag == 60 || textField.tag == 63)//for other user Total Amount
    {
      let str = (textField.text! + string)
      if str.count <= 10 {
        return true
      }
      alertViewObj.wrapAlert(title: nil, body:"Maximum Amount Limit".localized + "\n" + self.getDigitDisplay(strMaxAmountValue) + " MMK", img: #imageLiteral(resourceName: "phone_alert"))
      alertViewObj.addAction(title: "OK".localized, style: .cancel) {
               }
      alertViewObj.showAlert(controller: self)
      return false
    }
    if(textField.tag == 65)
    {
              println_debug("Mobile number validation")
              let strTotalLength = (textField.text! + string)
          
              guard validObj.getNumberRangeValidation(strTotalLength).isRejected == false else {
                  alertViewObj.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: UIImage(named : "AppIcon"))
                  alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                      self.hideContactSuggesionView()
                      self.tblAccountList.reloadData()
                      let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 2)) as? AMAccountMobileNoCell
                      cell?.txtAccountMobNo.becomeFirstResponder()
                      cell?.btnCancel.isHidden = true
                  })
                  alertViewObj.showAlert(controller: self)
                  return false
              }

          if validObj.getNumberRangeValidation(strTotalLength).max >= strTotalLength.count {
          
               return true
          }

       return false
        
    /*
        let strTotalLength = (textField.text! + string)
      guard validObj.getNumberRangeValidation(strTotalLength).isRejected == false else {
        alertViewObj.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: UIImage(named : "AppIcon"))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
          textField.text = commonPrefixMobileNumber
        })
        alertViewObj.showAlert(controller: self)
        return false
      }
        if validObj.getNumberRangeValidation(strTotalLength).max >= strTotalLength.count {
            return true
        }
      return false */
    }
    else if(textField.tag == 50)
    {
        if range.location == 0 || range.location == 1 {
                   return false
               }
                   println_debug("Mobile number validation")
                   let strTotalLength = (textField.text! + string)
               
                   guard validObj.getNumberRangeValidation(strTotalLength).isRejected == false else {
                       alertViewObj.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: UIImage(named : "AppIcon"))
                       alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                           self.hideContactSuggesionView()
                           self.tblAccountList.reloadData()
                           let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? AMAccountMobileNoCell
                           cell?.txtAccountMobNo.becomeFirstResponder()
                           cell?.btnCancel.isHidden = true
                       })
                       alertViewObj.showAlert(controller: self)
                       return false
                   }

               if validObj.getNumberRangeValidation(strTotalLength).max >= strTotalLength.count {
               
                    return true
               }
         return false
        
//      let strTotalLength = (textField.text! + string)
//      guard validObj.getNumberRangeValidation(strTotalLength).isRejected == false else {
//        alertViewObj.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: UIImage(named : "AppIcon"))
//        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
//          textField.text = commonPrefixMobileNumber
//        })
//        alertViewObj.showAlert(controller: self)
//        return false
//      }
//
//      return true
    }
    else if(textField.tag == 51)
    {
       if range.location == 0 || range.location == 1 {
                   return false
          }
      if validObj.checkMatchingNumber(string: text, withString: addMoneyModel.depositorContactNumber) {
        addMoneyModel.depositorConfirmContactNumber = text
        return true
      }
      else {  return false  }
      
    }
    return true
  }

  func textFieldDidEndEditing(_ textField: UITextField) {
    
    println_debug(textField.tag)
    //*
    if(textField.tag == 65)//Other Account
    {
      if(!isOtherDepositContactSelected)
      {
        isOtherDepsoitAccountShow = true
        addMoneyModel.depositorOtherContactNumber = textField.text!
        tblAccountList.reloadData()
      }
    }
    if(textField.tag == 65) {
        
          tblAccountList.reloadData()
    }
    if(textField.tag == 50)//Other Account
    {
      if(!isContactSelected)
      {
        addMoneyModel.depositorContactNumber = textField.text!
        if(addMoneyModel.depositorContactNumber.count > 7)
        {
          isConfirmMobileNoShow = true
        }
        tblAccountList.reloadData()
        let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? AMConfirmAccountMobNoCell
        cell?.txtConfirmMobileNo.becomeFirstResponder()
      }
    }
    if(textField.tag == 51)//Confirm Number
    {
      //addMoneyModel.depositorConfirmContactNumber = textField.text!
      
      if(addMoneyModel.depositorConfirmContactNumber.count > 7)
      {
        //isOtherAccountShow = true
      }
      tblAccountList.reloadData()
    }
    
    self.hideContactSuggesionView()
    
    //*/
  }
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? AMAccountMobileNoCell
    let cellconfirm = self.tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? AMConfirmAccountMobNoCell
    let cellother = self.tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 2)) as? AMAccountMobileNoCell
    if(textField.tag == 50 || textField.tag == 51  || textField.tag == 65)
    {
      if(textField.tag == 50) {
          textField.becomeFirstResponder()
          if let value = textField.text?.count{
              if value  > 5 {
                  debugPrint("Entered Value")
                cell?.btnCancel.isHidden = false
              }else{
                  textField.text = "09"
               // cell?.txtAccountMobNo.becomeFirstResponder()
                cell?.btnCancel.isHidden = true
              }
          }
      } else  if(textField.tag == 51) {
          textField.becomeFirstResponder()
          if let value = textField.text?.count{
              if value  > 5 {
                  debugPrint("Entered Value")
                cellconfirm?.btnConfirmCancel.isHidden = false
              }else{
                  textField.text = "09"
                cellconfirm?.btnConfirmCancel.isHidden = true
              }
          }
          //textField.text = "09"
          self.hideContactSuggesionView()
      }
       else  if(textField.tag == 65) {
            textField.becomeFirstResponder()
            if let value = textField.text?.count{
                if value  > 5 {
                    debugPrint("Entered Value")
                    if cellother?.btnCancel.tag == 52 {
                        cellother?.btnCancel.isHidden = false
                    }
        
                    
                }else{
                    textField.text = "09"
                    if cellother?.btnCancel.tag == 52 {
                        cellother?.btnCancel.isHidden = true
                    }
                   
                }
            }
         }
       }
   }
  
  func textFieldShouldClear(_ textField: UITextField) -> Bool {
    
    if(textField.tag == 50 || textField.tag == 51 || textField.tag == 65)
    {
      textField.text = "09"
//        let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? AMAccountMobileNoCell
//        cell?.txtAccountMobNo.becomeFirstResponder()
//        cell?.btnCancel.isHidden = true
      return false
    }
    else {
        if(textField.tag == 60) {
            isRemark = false
            tblAccountList.reloadData()
        }
    }
    return true
  }
    
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
    textField.resignFirstResponder()
    return true
  }
  
  @objc func textFieldDidChange(_ textField: UITextField) {
    //fixed text field with two prefix 09
    
    if(textField.tag == 60)//Amount Deposit by myself
    {
      let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 2)) as? BDDepositByMyselfCell
      if(textField.text?.count ?? 0 == 0)
      {
        strEnterAmountValue = ""
        isRemark = false
        tblAccountList.reloadData()
      }
      else{
        isRemark = true
        let dataWithoutComma = self.removeCommaFromDigit(textField.text!)
        if Int(dataWithoutComma)! > Int(strMaxAmountValue)!
        {
          alertViewObj.wrapAlert(title: nil, body:"Maximum Amount Limit".localized + "\n" + self.getDigitDisplay(strMaxAmountValue) + " MMK", img: #imageLiteral(resourceName: "phone_alert"))
          alertViewObj.addAction(title: "OK".localized, style: .cancel) {
          }
          alertViewObj.showAlert(controller: self)
        }
        else
        {
              addMoneyModel.enteredAmount = dataWithoutComma
              strEnterAmountValue = self.getDigitDisplay(dataWithoutComma)
              if(strEnterAmountValue.length == 9) {
                cell?.txtAmount.textColor = UIColor.green
                         } else if(strEnterAmountValue.length >= 10) {
                       cell?.txtAmount.textColor = UIColor.red
                    } else {
                    cell?.txtAmount.textColor = UIColor.black
                }
              cell?.txtAmount.text = strEnterAmountValue
            }
            if textField.text?.length == 1 {
                tblAccountList.reloadData()
                if(isSelectDepositByMySelf)
                 {
                   let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 2)) as? BDDepositByRemarkCell
                   cell?.txtRemark.text = ""
                 }
                 else
                 {
                   let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 2, section: 2)) as? BDDepositByRemarkCell
                   cell?.txtRemark.text = ""
                 }
            }
          let indexPath = IndexPath(item: 0, section: 2)
          let cell = tblAccountList.cellForRow(at: indexPath) as? BDDepositByMyselfCell
          cell?.txtAmount.becomeFirstResponder()
        /*
        isRemark = true
        
        let dataWithoutComma = self.removeCommaFromDigit(textField.text!)
        
        if Int(dataWithoutComma)! > Int(strMaxAmountValue)!
        {
            DispatchQueue.main.async {
            textField.resignFirstResponder()
            }
          alertViewObj.wrapAlert(title: nil, body:"Maximum Amount Limit".localized + "\n" + self.getDigitDisplay(strMaxAmountValue) + " MMK", img: #imageLiteral(resourceName: "phone_alert"))
          alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            
          }
          alertViewObj.showAlert(controller: self)
        }
        else
        {
          addMoneyModel.enteredAmount = dataWithoutComma
          strEnterAmountValue = self.getDigitDisplay(dataWithoutComma)
        } */
      }
//      tblAccountList.reloadData()
//      let indexPath = IndexPath(item: 0, section: 2)
//      let cell = tblAccountList.cellForRow(at: indexPath) as? BDDepositByMyselfCell
//      cell?.txtAmount.becomeFirstResponder()
    }
    
    
    /*
     let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 2, section: 1)) as? AMOtherAccountSelectedCell
    if(textField.text!.count == 0) {
        strEnterAmountValue = ""
        addMoneyModel.remarks = ""
        isRemark = false
        submitbuttonConstraint.constant = 0
    } else {
        //btnSubmit.isHidden = false
        isRemark = true
        submitbuttonConstraint.constant = 55
        //Display split amount equally on all person
        let dataWithoutComma = self.removeCommaFromDigit(textField.text!)
        addMoneyModel.enteredAmount = dataWithoutComma
        strEnterAmountValue = self.getDigitDisplay(dataWithoutComma)
      if(strEnterAmountValue.length == 9) {
        cell?.txtAmount.textColor = UIColor.green
                 } else if(strEnterAmountValue.length >= 10) {
               cell?.txtAmount.textColor = UIColor.red
            } else {
            cell?.txtAmount.textColor = UIColor.black
        }
      cell?.txtAmount.text = strEnterAmountValue
    }
    if textField.text?.length == 1 {
        tblAccountList.reloadData()
    }
    let cellX = self.tblAccountList.cellForRow(at: IndexPath.init(row: 2, section: 1)) as? AMOtherAccountSelectedCell
    cellX?.txtAmount.becomeFirstResponder()
    cellX?.txtAmount.keyboardType = .numberPad
*/
    if(textField.tag == 63)//Amount Deposit by Others
    {
      let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 2)) as? AMOtherAccountSelectedCell
      if(textField.text!.count == 0)
      {
        strEnterAmountValue = ""
        isRemark = false
      }
      else{
        isRemark = true
        let dataWithoutComma = self.removeCommaFromDigit(textField.text!)
        if Int(dataWithoutComma)! > Int(strMaxAmountValue)!
        {
          alertViewObj.wrapAlert(title: nil, body:"Maximum Amount Limit".localized + "\n" + self.getDigitDisplay(strMaxAmountValue) + " MMK", img: #imageLiteral(resourceName: "phone_alert"))
          alertViewObj.addAction(title: "OK".localized, style: .cancel) {
          }
          alertViewObj.showAlert(controller: self)
        }
        else
        {
              addMoneyModel.enteredAmount = dataWithoutComma
              strEnterAmountValue = self.getDigitDisplay(dataWithoutComma)
              if(strEnterAmountValue.length == 9) {
                cell?.txtAmount.textColor = UIColor.green
                         } else if(strEnterAmountValue.length >= 10) {
                       cell?.txtAmount.textColor = UIColor.red
                    } else {
                    cell?.txtAmount.textColor = UIColor.black
                }
              cell?.txtAmount.text = strEnterAmountValue
            }
            if textField.text?.length == 1 {
                tblAccountList.reloadData()
                if(isSelectDepositByMySelf)
                {
                  let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 2)) as? BDDepositByRemarkCell
                  cell?.txtRemark.text = ""
                }
                else
                {
                  let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 2, section: 2)) as? BDDepositByRemarkCell
                  cell?.txtRemark.text = ""
                }
            }
         }
        // tblAccountList.reloadData()
       let cellX = tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 2)) as? AMOtherAccountSelectedCell
       cellX?.txtAmount.becomeFirstResponder()
       cellX?.txtAmount.keyboardType = .numberPad
      }
    if(textField.tag == 64)
    {
      addMoneyModel.remarks = textField.text!
      //tblAccountList.reloadData()
      if(isSelectDepositByMySelf)
      {
        let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 2)) as? BDDepositByRemarkCell
        cell?.txtRemark.becomeFirstResponder()
      }
      else
      {
        let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 2, section: 2)) as? BDDepositByRemarkCell
        cell?.txtRemark.becomeFirstResponder()
      }
    }
    if(textField.tag == 65)
    {
        let startPosition: UITextPosition = textField.beginningOfDocument
        print(startPosition)
        let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 2)) as? AMAccountMobileNoCell
        
        if((textField.text?.length)! > 2)
        {
            cell?.btnCancel.isHidden = false
        }
        else {
            cell?.btnCancel.isHidden = true
        }
       // addMoneyModel.depositorOtherContactNumber = ""
        let max = validObj.getNumberRangeValidation(textField.text!).max
        //let min = validObj.getNumberRangeValidation(textField.text!).min
           if textField.text?.count == max {
             isOtherDepsoitAccountShow = true
             isOtherDepositContactSelected = false
             lastCountIndex = textField.text!.count
             tblAccountList.reloadData()
           }
           else { // textField.text!.count < max && textField.text!.count > min &&
            if (textField.text!.count < lastCountIndex) {
                isOtherDepsoitAccountShow = false
                lastCountIndex = textField.text!.count
                tblAccountList.reloadData()
                let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 2)) as? AMAccountMobileNoCell
                cell?.txtAccountMobNo.becomeFirstResponder()
               }
              lastCountIndex = textField.text!.count
            }
           if let text = textField.text, text.length <= 2 {
             textField.text = "09"
           }
//        if(textField.text!.count == 0) {
//            addMoneyModel.depositorOtherContactNumber = ""
//
//        } else {
//
//            strEnterMobileValue = textField.text ?? ""
//            addMoneyModel.depositorOtherContactNumber =  textField.text ?? ""
//
//            let rangeCheck = validObj.getNumberRangeValidation(textField.text ?? "")
//
//            if textField.text?.count ?? "" >= rangeCheck.min {
//               // if !isConfirmMobileNoShow {
//                  //  isConfirmMobileNoShow = true
//                  //  self.hideContactSuggesionView()
//                isOtherDepsoitAccountShow = true
//                isOtherDepositContactSelected = false
//                    tblAccountList.reloadData {
//                        if self.addMoneyModel.depositorOtherContactNumber.count < rangeCheck.max {
//                            let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 2)) as? AMAccountMobileNoCell
//                            cell?.txtAccountMobNo.becomeFirstResponder()
//                        }
//                    }
//                    lastCountIndex = self.addMoneyModel.depositorOtherContactNumber.count
//               // } //else {
//                    if addMoneyModel.depositorOtherContactNumber.count <= rangeCheck.max && addMoneyModel.depositorOtherContactNumber.count >= rangeCheck.min && addMoneyModel.depositorOtherContactNumber.count < lastCountIndex  {
//                          //  isConfirmMobileNoShow = true
//                         //   isRemark = false
//                         //   strEnterAmountValue = ""
//                         //   isContactSelected = false
//                        //    isRemark = false
//                            isOtherDepsoitAccountShow = false
//                            lastCountIndex = self.addMoneyModel.depositorOtherContactNumber.count
//                            tblAccountList.reloadData()
//                            let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 2)) as? AMAccountMobileNoCell
//                            cell?.txtAccountMobNo.becomeFirstResponder()
//
//                    }
////                    else {
////                        let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? AMConfirmAccountMobNoCell
////                        cell?.txtConfirmMobileNo.text = "09"
////                        lastCountIndex = self.strEnterMobileValue.count
////                    }
//              //  }
//                if addMoneyModel.depositorOtherContactNumber.count >= rangeCheck.max {
////                    let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? AMConfirmAccountMobNoCell
////                    cell?.txtConfirmMobileNo.becomeFirstResponder()
//                    isOtherDepsoitAccountShow = true
//                    isOtherDepositContactSelected = false
//                    lastCountIndex = self.strEnterMobileValue.count
//                }
//
//            } else {
//               // if isConfirmMobileNoShow || isRemark || isContactSelected {
////                    isConfirmMobileNoShow = false
////                    isRemark = false
////                    strEnterAmountValue = ""
////                    isContactSelected = false
////                    isRemark = false
////                    isOtherAccountShow = false
//                    tblAccountList.reloadData()
//                    let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 2)) as? AMAccountMobileNoCell
//                    cell?.txtAccountMobNo.becomeFirstResponder()
//              //  }
//            }
//        }
       
        
        
      }
    if(textField.tag == 61)
    {
      println_debug(textField.text!)
      addMoneyModel.depositorOtherContactName  = textField.text!
      println_debug(addMoneyModel.depositorOtherContactName)
      
      //tblAccountList.reloadData()
      let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 2)) as? AMOtherAccountSelectedCell
      cell?.txtAccountName.becomeFirstResponder()
    }
    if(textField.tag == 50)
    {
         let startPosition: UITextPosition = textField.beginningOfDocument
         print(startPosition)
         let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? AMAccountMobileNoCell
         
         if((textField.text?.length)! > 2)
         {
             cell?.btnCancel.isHidden = false
         }
         else {
             cell?.btnCancel.isHidden = true
         }
         
         let contactList = self.contactSuggesstion(textField.text!)
         if contactList.count > 0 {
             // here show the contact suggession screen
        
            let rect = UitilityClass.setFrameForContactSuggestion(arrayCount: contactList.count, indexpath: IndexPath(item: 0, section: 1),onTextField: cell!.txtAccountMobNo,tableView: tblAccountList)
            self.showContactSuggessionView(withFrame: rect, andContacts: contactList)
                         
//             let rect = self.getSuggessionviewFrame(arr: contactList.count, indexpath: IndexPath(item: 0, section: 1))
//             self.showContactSuggessionView(withFrame: rect, andContacts: contactList)
         } else {
             // here hide the contact suggession screen
               strEnterMobileValue = ""
             self.hideContactSuggesionView()
         }
         if((textField.text?.length)! <= 2) {
             textField.text = "09"
             strEnterMobileValue = textField.text ?? ""
             self.hideContactSuggesionView()
         }
         if(textField.text!.count == 0) {
             strEnterMobileValue = ""
             
         } else {
             
             strEnterMobileValue = textField.text ?? ""
             addMoneyModel.depositorContactNumber =  textField.text ?? ""
             addMoneyModel.depositorConfirmContactNumber = "09"
             isOtherAccountShow = false
             isRemark = false
             strEnterAmountValue = ""

             let rangeCheck = validObj.getNumberRangeValidation(strEnterMobileValue)
             
             if strEnterMobileValue.count >= rangeCheck.min {
                 if !isConfirmMobileNoShow {
                     isConfirmMobileNoShow = true
                     self.hideContactSuggesionView()
                     tblAccountList.reloadData {
                         if self.strEnterMobileValue.count < rangeCheck.max {
                             let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? AMAccountMobileNoCell
                             cell?.txtAccountMobNo.becomeFirstResponder()
                         }
                     }
                     lastCountIndex = self.strEnterMobileValue.count
                 } else {
                     if strEnterMobileValue.count <= rangeCheck.max && strEnterMobileValue.count >= rangeCheck.min && strEnterMobileValue.count < lastCountIndex  {
                             isConfirmMobileNoShow = true
                             isRemark = false
                             strEnterAmountValue = ""
                             isContactSelected = false
                             isRemark = false
                             lastCountIndex = self.strEnterMobileValue.count
                             tblAccountList.reloadData()
                             let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? AMAccountMobileNoCell
                             cell?.txtAccountMobNo.becomeFirstResponder()
                             let cellX = tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? AMConfirmAccountMobNoCell
                             cellX?.txtConfirmMobileNo.text = "09"
                     }
                     else {
                         let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? AMConfirmAccountMobNoCell
                         cell?.txtConfirmMobileNo.text = "09"
                         lastCountIndex = self.strEnterMobileValue.count
                     }
                 }
                 if strEnterMobileValue.count >= rangeCheck.max {
                     let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? AMConfirmAccountMobNoCell
                     cell?.txtConfirmMobileNo.becomeFirstResponder()
                     lastCountIndex = self.strEnterMobileValue.count
                 }
                 
             } else {
                 if isConfirmMobileNoShow || isRemark || isContactSelected {
                     isConfirmMobileNoShow = false
                     isRemark = false
                     strEnterAmountValue = ""
                     isContactSelected = false
                     isRemark = false
                     isOtherAccountShow = false
                     tblAccountList.reloadData()
                     let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? AMAccountMobileNoCell
                     cell?.txtAccountMobNo.becomeFirstResponder()
                 }
             }
         }

        
       /*
      let contactList = self.contactSuggesstion(textField.text!)
      if contactList.count > 0 {
        // here show the contact suggession screen
        
        let rect = self.getSuggessionviewFrame(arr: contactList.count, indexpath: IndexPath(item: 0, section: 1))
        self.showContactSuggessionView(withFrame: rect, andContacts: contactList)
        
      }else {
        // here hide the contact suggession screen
        self.hideContactSuggesionView()
        
      }
      let max = validObj.getNumberRangeValidation(textField.text!).max
      if textField.text?.count == max {
        isConfirmMobileNoShow = true
        tblAccountList.reloadData()
      }
      if let text = textField.text, text.length <= 2 {
        textField.text = "09"
      } */
    }
    if(textField.tag == 51)
    {
        let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? AMConfirmAccountMobNoCell
        if((textField.text?.length)! > 2)
        {
            cell?.btnConfirmCancel.isHidden = false
        }
        else {
            cell?.btnConfirmCancel.isHidden = true
        }
         self.hideContactSuggesionView()
        if((textField.text?.length)! <= 2) {
            textField.text = "09"
        }
        if(addMoneyModel.depositorContactNumber == textField.text) {
            addMoneyModel.depositorConfirmContactNumber = textField.text!
            addMoneyModel.depositorContactNumber = textField.text!
            isSelectDepositByMySelf = false
            isSelectDepositByOthers = false
            isSelectDepositBy = false
            self.validateOkDollarNumberAPI(phoneNumber: self.getConatctNum(textField.text!,withCountryCode: "+95"))
        }
        if isRemark || isContactSelected || isOtherAccountShow {
            isOtherAccountShow = false
            isRemark = false
            strEnterAmountValue = ""
            isContactSelected = false
            tblAccountList.reloadData()
            let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? AMConfirmAccountMobNoCell
            cell?.txtConfirmMobileNo.becomeFirstResponder()
        }
     /*
      if let text = textField.text, text.length <= 2 {
        textField.text = "09"
      }
      if(addMoneyModel.depositorContactNumber == textField.text)
      {
        addMoneyModel.depositorConfirmContactNumber = textField.text!
        addMoneyModel.depositorContactNumber = textField.text!
        self.validateOkDollarNumberAPI(phoneNumber: self.getConatctNum(textField.text!,withCountryCode: "+95"))
      } */
    }
  }
  
  //MARK: Contact Suggestion
  func loadContactSuggessionView() {
    contactSuggessionView  =  UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "ContactSugestView_ID") as? ContactSuggestionVC
    contactSuggessionView?.delegate   = self //as? ContactSuggestionDelegate
    contactSuggessionView?.view.frame = CGRect.zero
    addChild(contactSuggessionView!)
    self.view.addSubview(contactSuggessionView!.view)
    contactSuggessionView!.didMove(toParent: self)
    contactSuggessionView?.view.isHidden = true
  }
  
  fileprivate func getSuggessionviewFrame(arr: Int, indexpath: IndexPath) -> CGRect {
    
    let rect = tblAccountList.rectForRow(at: indexpath)
    var height          : CGFloat = CGFloat(Float(arr) * 70.0)
    let cellHgt         : CGFloat = 70.0
    let normalHeaderHgt : CGFloat = 40.0
    
    let maxHeight = screenHeight - 250 - cellHgt - normalHeaderHgt // - cellHgt
    if arr > 5 {
      height = maxHeight
    }
    let yPos = screenHeight - rect.origin.y - height
    
    if(screenHeight == 568)
    {
      return CGRect(x: 10, y: yPos-250 , width: screenWidth - 10, height: height)
    }
    return CGRect(x: 10, y: 0 , width: screenWidth - 10, height: 250)
    
  }
  
  func hideContactSuggesionView() {
    if let contactview = contactSuggessionView {
      contactview.view.isHidden = true
    }
  }
  
  func showContactSuggessionView(withFrame frame: CGRect, andContacts contactList: [Dictionary<String, Any>]) {
    contactSuggessionView?.view.isHidden    = false
    contactSuggessionView?.view.frame       = frame
    contactSuggessionView?.contactsList     = contactList
    contactSuggessionView?.contactsTable.reloadData()
    contactSuggessionView?.view.layoutIfNeeded()
  }
  
  func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
    contactSuggessionView?.view.isHidden = true
    self.view.endEditing(true)
  }
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
}

extension BankCounterDepositDetailsVC : ContactSuggestionDelegate{
    
    func didSelectFromContactSuggession(number: Dictionary<String, Any>)
    {
        println_debug(number)
        println_debug(number[ContactSuggessionKeys.phonenumber_backend] as! String)
        println_debug(number[ContactSuggessionKeys.contactname] as! String)
        
        let strContactNumber = self.getConatctNum(number[ContactSuggessionKeys.phonenumber_backend] as! String,withCountryCode: "+95")
        println_debug(strContactNumber)
        
        addMoneyModel.depositorContactNumber = String(self.getActualNum(strContactNumber,withCountryCode: "+95"))
        println_debug("depositorContactNumber" + addMoneyModel.depositorContactNumber)
        
        isSelectDepositByMySelf = false
        isSelectDepositByOthers = false
        isSelectDepositBy = false
        //Cal API for verification
        self.validateOkDollarNumberAPI(phoneNumber:strContactNumber)
        
        self.hideContactSuggesionView()
    }
    
}

extension BankCounterDepositDetailsVC : WebServiceResponseDelegate{
    
    //MARK:- API Response
    func webResponse(withJson json: AnyObject, screen: String) {
        
        self.removeProgressView()
        print("BankCounterDepositDetailsVC Webresponse----\(json)")
        if screen == "mMobileValidation"
        {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(dic)
                        print("BankCounterDepositDetailsVC Dic----\(dic)")

                        
                        if dic["Code"]! as! Int == 200
                        {
                            
                            DispatchQueue.main.async {
                                self.isOtherAccountShow = true
                                self.isContactSelected = true
                               
                                let dataDict = dic["Data"] as? String
                                let tempData = OKBaseController.convertToDictionary(text: dataDict!)
                                self.addMoneyModel.okACOwnername = tempData!["MercantName"]! as! String
                                self.addMoneyModel.businessName = tempData!["BusinessName"]! as! String
                                
                                if tempData!["AccountType"]! as! String == "SUBSBR" {
                                    
                                    self.isPersonalAccountNumber = true
                                } else {
                                    self.isPersonalAccountNumber = false

                                }
                                
                                self.tblAccountList.reloadData()
                            }
                        }
                        else {
                            DispatchQueue.main.async {
                                self.isConfirmMobileNoShow = false
                                self.isContactSelected = false
                                self.isOtherAccountShow = false
                                
                                self.addMoneyModel.depositorContactNumber = ""
                                self.tblAccountList.reloadData()
                                
                                alertViewObj.wrapAlert(title: nil, body:"This Number is not registered with OK$".localized, img: #imageLiteral(resourceName: "phone_alert"))
                                
                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                    
                                }
                                alertViewObj.showAlert(controller: self)
                                
                            }
                        }
                    }
                    
                }
            } catch {
                
            }
        }
    }
}

extension BankCounterDepositDetailsVC : ContactPickerDelegate{
    
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]) {
        for contact in contacts {
            decodeContact(contact: contact, isMulti: true)
        }
    }
    
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        decodeContact(contact: contact, isMulti: false)
    }
    
    func decodeContact(contact: ContactPicker, isMulti: Bool) {
        
        let phoneNumberCount = contact.phoneNumbers.count
        
        if phoneNumberCount >= 1 {
            
            println_debug(contact.firstName)
            println_debug(contact.phoneNumbers[0].phoneNumber)
            
            _ = "\(contact.phoneNumbers[0].phoneNumber)"
            
            let strMobNo = contact.phoneNumbers[0].phoneNumber
            println_debug(strMobNo)
            let trimmed = String(strMobNo.filter { !" ".contains($0) })
            println_debug(trimmed)
            
            let strContactNumber = self.getConatctNum(trimmed,withCountryCode: "+95")
            println_debug(strContactNumber)
            
            if(isOtherDepositContactSelected)//Section 2
            {
                addMoneyModel.depositorOtherContactNumber = String(self.getActualNum(strContactNumber,withCountryCode: "+95"))
                println_debug("depositorContactNumber = " + addMoneyModel.depositorOtherContactNumber)
                addMoneyModel.depositorOtherContactName = contact.firstName
                //isOtherDepsoitAccountShow = true
                addMoneyModel.IsDepositorOtherContact = true
                
                self.tblAccountList.reloadData()
            }
            else
            {
                addMoneyModel.depositorContactNumber = String(self.getActualNum(strContactNumber,withCountryCode: "+95"))
                println_debug("depositorContactNumber = " + addMoneyModel.depositorContactNumber)
                isSelectDepositByMySelf = false
                isSelectDepositByOthers = false
                isSelectDepositBy = false
                //Cal API for verification
                self.validateOkDollarNumberAPI(phoneNumber:strContactNumber)
            }
        }
        else {
            _ = ContactGlobalConstants.Strings.phoneNumberNotAvaialable
        }
        
    }
    
}

extension BankCounterDepositDetailsVC: BioMetricLoginDelegate{
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String)
    {
        println_debug("Authorised")
        
        if(isSuccessful)
        {
            if(screen == "BankCounterDepositDetailsNext")
            {
                let controller:BankCounterDepositConfirmationVC = UitilityClass.getStoryBoardAddWithdraw().instantiateViewController(withIdentifier: "BankCounterDepositConfirmationVC") as! BankCounterDepositConfirmationVC
                controller.addMoneyModel =  addMoneyModel
                controller.bankDetailsModel =  bankDetailsModel
                self.navigationController?.pushViewController(controller, animated: true)
            }
            else if(screen == "BankCounterDepositDetailViewBalance")
            {
                DispatchQueue.main.async {
                    if(self.isViewBalance)
                    {
                        self.btnWalletBalance.isHidden = false
                        self.isViewBalance = false
                        self.lblBalance.text = "xxxxxx"
                        self.lblLogin.isHidden = false
                    }
                    else
                    {
                        self.lblLogin.isHidden = true
                        self.btnWalletBalance.isHidden = true
                        self.isViewBalance = true
                        self.lblBalance.attributedText     = self.walletBalance()
                    }
                }
            }
        }
    }
}
