//
//  BankCounterDepositTable.swift
//  OK
//
//  Created by SHUBH on 10/12/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BankCounterDepositConfirmationTable: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BankCounterDepositConfirmationVC: UITableViewDataSource,UITableViewDelegate{
  
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    
    let view = UIView(frame: CGRect(x: 0, y: 0, width:200, height: 1))
    view.backgroundColor = .clear
    return view
  }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    
    return 0.0;
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    
    return 55.0
    
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if(indexPath.section == 3)
    {
      return 0.0
    }
    return 55.0
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    
    return 4
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if(section == 0)
    {
      return 1
    }
    else if(section == 1)
    {
      return 2
    }
    else if(section == 2)
    {
      if(addMoneyModel.remarks.count > 0)
      {
        return 5
      }
      return 4
    }
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if(indexPath.section == 0)
    {
      let identifier = "BDConfirmationLabelCell"
      var cellView: BDConfirmationLabelCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationLabelCell
      if cellView == nil {
        tableView.register (UINib(nibName: "BDConfirmationLabelCell", bundle: nil), forCellReuseIdentifier: identifier)
        cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationLabelCell
      }
      cellView.lblTitle.text = bankDetailsModel.BankName
      
      cellView.selectionStyle = .none
      
      return cellView
    }
    else
    {
      let identifier = "BDConfirmationTextFieldCell"
      var cellView: BDConfirmationTextFieldCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationTextFieldCell
      if cellView == nil {
        tableView.register (UINib(nibName: "BDConfirmationTextFieldCell", bundle: nil), forCellReuseIdentifier: identifier)
        cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationTextFieldCell
      }
      cellView.txtFieldData.isUserInteractionEnabled = false
      if(indexPath.section == 1)
      {
        if(indexPath.row == 0)
        {
          cellView.txtFieldData.placeholder = "Beneficiary Name".localized
          cellView.txtFieldData.text = addMoneyModel.okACOwnername
        }
        else
        {
          cellView.txtFieldData.placeholder = "Beneficiary OK$ Account Number".localized
          cellView.txtFieldData.text =  self.getActualNum(addMoneyModel.depositorContactNumber, withCountryCode: "+95")
        }
      }
      else if(indexPath.section == 2)
      {
        if(indexPath.row == 0)
        {
          cellView.txtFieldData.placeholder = "Deposit By".localized
          cellView.txtFieldData.text = addMoneyModel.depositeBy.localized
        }
        
        if(addMoneyModel.depositeBy == "Myself")
        {
          if(indexPath.row == 1)
          {
            cellView.txtFieldData.placeholder = "My Name".localized
            cellView.txtFieldData.text =  UserModel.shared.name
            addMoneyModel.depositorName = UserModel.shared.name
          }
          else if(indexPath.row == 2)
          {
            cellView.txtFieldData.placeholder = "My Contact".localized
            cellView.txtFieldData.text =  self.getActualNum(UserModel.shared.mobileNo, withCountryCode: "+95")
          }
          else if(indexPath.row == 3)
          {
            cellView.txtFieldData.placeholder = "Amount".localized
            cellView.txtFieldData.text =  self.getDigitDisplay(addMoneyModel.enteredAmount) + " MMK"//addMoneyModel.enteredAmount
          }
          else if(indexPath.row == 4)
          {
            cellView.txtFieldData.placeholder = "Remarks".localized
            cellView.txtFieldData.text =  addMoneyModel.remarks
          }
        }
        else
        {
          if(indexPath.row == 1)
          {
            cellView.txtFieldData.placeholder = "Depositor Name".localized
            cellView.txtFieldData.text =  addMoneyModel.depositorOtherContactName
          }
          else if(indexPath.row == 2)
          {
            cellView.txtFieldData.placeholder = "Depositor Contact Number".localized
            cellView.txtFieldData.text =  self.getActualNum(addMoneyModel.depositorOtherContactNumber, withCountryCode: "+95")
          }
          else if(indexPath.row == 3)
          {
            cellView.txtFieldData.placeholder = "Amount".localized
            cellView.txtFieldData.text =  self.getDigitDisplay(addMoneyModel.enteredAmount) + " MMK"//addMoneyModel.enteredAmount
          }
          else if(indexPath.row == 4)
          {
            cellView.txtFieldData.placeholder = "Remarks".localized
            cellView.txtFieldData.text =  addMoneyModel.remarks
          }
        }
        
      }
      
      cellView.selectionStyle = .none
      
      return cellView
    }
    
  }
  
  // Header
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
    if(section == 0)
    {
      let view = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 55))
      let label = UILabel(frame: CGRect(x: 15, y: 0, width: self.view.frame.width - 15, height: 55))
      label.text = "Bank Details".localized
      label.backgroundColor = UIColor.clear
      label.textAlignment = NSTextAlignment.center
      if let myFont = UIFont(name: appFont, size: 17) {
        label.font = myFont
      }
      label.textColor = UIColor.black
      view.addSubview(label)
      view.backgroundColor = UIColor.init(red: 223/255, green: 223/255, blue: 223/255, alpha: 1)
      return view
    }
    else if(section == 1)
    {
      let view = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 55))
      let label = UILabel(frame: CGRect(x: 15, y: 0, width: self.view.frame.width - 15, height: 55))
      
      if(addMoneyModel.selectedOKAC == "Myself")
      {
        label.text = "My OK$ Account Details".localized
      }
      else
      {
        label.text = "Other OK$ Account Details".localized
      }
      label.backgroundColor = UIColor.clear
      label.textAlignment = NSTextAlignment.center
      if let myFont = UIFont(name: appFont, size: 17) {
        label.font = myFont
      }
      label.textColor = UIColor.black
      view.addSubview(label)
      view.backgroundColor = UIColor.init(red: 223/255, green: 223/255, blue: 223/255, alpha: 1)
      return view
    }
    else if(section == 2)
    {
      let view = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 55))
      let label = UILabel(frame: CGRect(x: 15, y: 0, width: self.view.frame.width - 15, height: 55))
      label.text = "Deposit Details".localized
      label.backgroundColor = UIColor.clear
      label.textAlignment = NSTextAlignment.center
      if let myFont = UIFont(name: appFont, size: 17) {
        label.font = myFont
      }
      label.textColor = UIColor.black
      view.addSubview(label)
      view.backgroundColor = UIColor.init(red: 223/255, green: 223/255, blue: 223/255, alpha: 1)
      return view
    }
    else if(section == 3)
    {
      let identifier = "BDConfirmationButtonCell"
      var cellView: BDConfirmationButtonCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationButtonCell
      if cellView == nil {
        tableView.register (UINib(nibName: "BDConfirmationButtonCell", bundle: nil), forCellReuseIdentifier: identifier)
        cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationButtonCell
      }
      
      cellView.btnSubmit.setTitle(cellView.btnSubmit.titleLabel?.text?.localized, for: .normal)
      cellView.btnSubmit.addTarget(self, action:#selector(self.btnConfirmAction(sender:)), for: .touchUpInside)
      
      return cellView
    }
    else {
      let view = UIView(frame: CGRect(x: 0, y: 0, width:200, height: 1))
      view.backgroundColor = .clear
      return view
    }
  }
  
  // MARK: - Custom Methods
  @objc func btnConfirmAction(sender: UIButton!) {
    println_debug("btnConfirmAction")
    //self.getAccessToken()
    
    alertViewObj.wrapAlert(title: "", body: "Once code is generated,\n you can’t edit details.".localized, img: #imageLiteral(resourceName: "counter_bcd"))
    alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
      println_debug("cancel")
    }
    
    alertViewObj.addAction(title: "ContinueBCD".localized, style: .target) {
      println_debug("I am called")
      self.getAccessToken()
    }
    
    alertViewObj.showAlert(controller: self)
  }
}
