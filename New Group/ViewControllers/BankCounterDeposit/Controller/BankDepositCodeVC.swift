//
//  BankDepositCodeVC.swift
//  OK
//
//  Created by PC on 2/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BankDepositCodeVC: OKBaseController {

    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var btnArrow: UIButton!
    @IBOutlet weak var btnUnicode: UIButton!{
        didSet{
            btnUnicode.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    var boolUniZawg = false
    var urlStr:String = ""

    @IBOutlet weak var lblAddFav: UILabel!
    {
        didSet
        {
            lblAddFav.text = lblAddFav.text?.localized
        }
    }
    @IBOutlet weak var lblShare: UILabel!
        {
        didSet
        {
            lblShare.text = lblShare.text?.localized
        }
    }
    @IBOutlet weak var lblHome: UILabel!
        {
        didSet
        {
            lblHome.text = "HomePagePT".localized
            lblHome.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    //Show Arrow
    var isArrowStatus:Bool = false
    
    var addMoneyModel = AddMoneyModel()
    var depositGalleryModel = DepositCodeGalleryModel()
    
    //QR Code Image
    var bcdMerchantModel = BCDMerchantModel()
    
    //MARK: - View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnUnicode.setTitle("Zawgyi".localized, for: .normal)

            appDel.floatingButtonControl?.window.closeButton?.isHidden = true
            appDel.floatingButtonControl?.window.isHidden = true
        
      //Show change lang option
      btnUnicode.isHidden = true
      
      if(appDel.currentLanguage == "my")
      {
        self.navigationItem.rightBarButtonItem?.isEnabled = true
      }
      else
      {
        self.navigationItem.rightBarButtonItem = nil
      }
      
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Bank Deposit Code".localized
        self.helpSupportNavigationEnum = .Add_Withdraw
        self.title = self.title?.localized
        urlStr = depositGalleryModel.notesBurmish

        // Do any additional setup after loading the view.
        tblList.reloadData()
    }
  
      // MARK: - Custom Methods
  
  @IBAction func onClickLangChangeBtn(_ sender: Any) {
    btnUnicode.isHidden = false
  }
  
  @IBAction func selectedLanguage(_ sender: Any) {
    self.overRideWebView()
  }
  
  func overRideWebView()
  {
    btnUnicode.isHidden = true
    
    if(appDel.currentLanguage == "my")
    {
      if(boolUniZawg)
      {
        boolUniZawg = false
        
        btnUnicode.setTitle("Zawgyi".localized, for: .normal)
        urlStr = depositGalleryModel.notesBurmish
      }
      else
      {
        boolUniZawg = true
        btnUnicode.setTitle("Unicode".localized, for: .normal)
        
        if urlStr.contains(find: "MAB") || urlStr.contains(find: "UAB")
        {
          urlStr = urlStr.replacingOccurrences(of: ".html", with: "__zw.html")
        }
        else
        {
          urlStr = urlStr.replacingOccurrences(of: ".html", with: "_zw.html")
        }
        
      }
    }
    tblList.reloadData()
  }
  
    @IBAction func btnShareAction(_ sender: Any) {
        println_debug("btnShareAction")
 
          let  text = "Code for deposit :".localized + (depositGalleryModel.transactionCode) + "\n" + "Bank :".localized + (depositGalleryModel.bankName) + "\n" + "OK$ Account :".localized + (addMoneyModel.IsMyOwnAccount ? "My Account".localized : "Other Account".localized) + "\n" + "My Name :".localized + (depositGalleryModel.beneficiaryName) + "\n" + "My OK$ Account Number :".localized + (self.getActualNum(depositGalleryModel.beneficiaryOkAccountNumber, withCountryCode: "+95")) + "\n" + "Depositor Name :".localized + (depositGalleryModel.depositorName) + "\n" + "Depositor Contact Number :".localized + (self.getActualNum(depositGalleryModel.depositorNumber, withCountryCode: "+95")) + "\n" + "Date :".localized + (depositGalleryModel.date) + "\n" + "Time :".localized + (depositGalleryModel.time)
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        //activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    
    @IBAction func btnFavoriteAction(_ sender: Any) {
        println_debug("Add to Favorite")
        //addFavoriteController(withName name: String,favNum: String,type: String, amount: String)
        let vc = self.addFavoriteController(withName: depositGalleryModel.beneficiaryName,favNum: depositGalleryModel.beneficiaryOkAccountNumber,type: "PAYTO", amount: addMoneyModel.enteredAmount)
        if let vcs = vc {
            self.present(vcs, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnArrowAction(_ sender: Any) {
        
        if isArrowStatus {
            println_debug("YES")
            tblList.isHidden = true
            isArrowStatus = false
            btnArrow.setImage(#imageLiteral(resourceName: "up_arrow"), for: .normal)
        
        } else {
            println_debug("NO")
            tblList.isHidden = false
            isArrowStatus = true
            btnArrow.setImage(#imageLiteral(resourceName: "down_arrow"), for: .normal)
        }
    }
    
    @IBAction func btnBackAction(_ sender: Any) {

        //self.dismiss(animated: false, completion: nil)
        self.navigationController?.popToRootViewController(animated: true)

    }
   
    @IBAction func btnHomeAction(_ sender: Any) {
        println_debug("btnHomeAction")
        //self.dismiss(animated: false, completion: nil)
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

