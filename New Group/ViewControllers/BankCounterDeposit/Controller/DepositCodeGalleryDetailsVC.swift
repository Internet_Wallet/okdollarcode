//
//  DepositCodeGalleryDetailsVC.swift
//  OK
//
//  Created by PC on 2/24/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class DepositCodeGalleryDetailsVC: OKBaseController {

    //@IBOutlet weak var lblUseBelowCode: UILabel!
    @IBOutlet weak var lblAmount: UILabel!{
        didSet
        {
            lblAmount.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblBankName: UILabel!{
        didSet
        {
            lblBankName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblCodeA: UILabel!
    @IBOutlet weak var lblCodeB: UILabel!
    @IBOutlet weak var lblCodeC: UILabel!
    @IBOutlet weak var lblCodeD: UILabel!
    @IBOutlet weak var lblCodeE: UILabel!
    @IBOutlet weak var lblCodeF: UILabel!

    @IBOutlet weak var lblUseBelowCode: UILabel!{
        didSet
        {
            lblUseBelowCode.text = lblUseBelowCode.text?.localized
            lblUseBelowCode.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var tblList: UITableView!
    
    //To check the SCreen display
    var titleHeader = "Unused Deposit Code"

    var depositGalleryModel = DepositCodeGalleryModel()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
            appDel.floatingButtonControl?.window.closeButton?.isHidden = true
            appDel.floatingButtonControl?.window.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.helpSupportNavigationEnum = .Add_Withdraw

        if(titleHeader == "Used Deposit Code")
        {
            self.title = "Used Deposit Code".localized
        }
        else
        {
            self.title = "Unused Deposit Code".localized
        }
        // Do any additional setup after loading the view.
        
        self.loadInitalData()
    }

    func loadInitalData()
    {
        //Set border color
        lblCodeA.layer.borderWidth = 1.0
        lblCodeA.layer.borderColor = UIColor.black.cgColor
        lblCodeB.layer.borderWidth = 1.0
        lblCodeB.layer.borderColor = UIColor.black.cgColor
        lblCodeC.layer.borderWidth = 1.0
        lblCodeC.layer.borderColor = UIColor.black.cgColor
        lblCodeD.layer.borderWidth = 1.0
        lblCodeD.layer.borderColor = UIColor.black.cgColor
        lblCodeE.layer.borderWidth = 1.0
        lblCodeE.layer.borderColor = UIColor.black.cgColor
        lblCodeF.layer.borderWidth = 1.0
        lblCodeF.layer.borderColor = UIColor.black.cgColor
        
        lblAmount.text = depositGalleryModel.amountDisplay
        lblBankName.text = depositGalleryModel.bankName
        lblCodeA.text = depositGalleryModel.codeA
        lblCodeB.text = depositGalleryModel.codeB
        lblCodeC.text = depositGalleryModel.codeC
        lblCodeD.text = depositGalleryModel.codeD
        lblCodeE.text = depositGalleryModel.codeE
        lblCodeF.text = depositGalleryModel.codeF
        
        if(titleHeader == "Unused Deposit Code")
        {
            lblUseBelowCode.isHidden = false
        }
        else{
            lblUseBelowCode.isHidden = true
        }
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DepositCodeGalleryDetailsVC: UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width:200, height: 1))
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 1.0;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if depositGalleryModel.bankCode == "KBZ"
        {
            if(section == 0)
            {
                return 0
            }
            else if(section == 1)
            {
                return 0
            }
            else
            {
                return 55
            }
        }
        else
        {
            if(section == 0)
            {
                return 0
            }
            else
            {
                return 55
            }
                
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if depositGalleryModel.bankCode == "KBZ"
        {
            if(indexPath.section == 0)
            {
                return 200
            }
            else if(indexPath.section == 1)
            {
                return 300
            }
            else
            {
                return 55
            }
        }
        else
        {
            if(indexPath.section == 0)
            {
                return 200
            }
            else
            {
                return 55
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if depositGalleryModel.bankCode == "KBZ"
        {
            return 5
        }
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if depositGalleryModel.bankCode == "KBZ"
        {
            if(section == 0 || section == 1)
            {
                return 1
            }
            else if(section == 2)
            {
                if(titleHeader == "Unused Deposit Code".localized)
                {
                    return 3
                }
                return 9
            }
            else if(section == 3)
            {
                return 2
            }
            else if(section == 4)
            {
                if(depositGalleryModel.remarks.count > 0)
                {
                    return 3
                }
                return 2
            }
            return 1
        }
        else
        {
            if(section == 0)
            {
                return 1
            }
            else if(section == 1)
            {
                if(titleHeader == "Unused Deposit Code".localized)
                {
                    return 3
                }
                return 9
            }
            else if(section == 2)
            {
                return 2
            }
            else if(section == 3)
            {
                if(depositGalleryModel.remarks.count > 0)
                {
                    return 3
                }
                return 2
            }
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0)
        {
            let identifier = "CodeGalleryHeaderCell"
            var cellView: CodeGalleryHeaderCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? CodeGalleryHeaderCell
            if cellView == nil {
                tableView.register (UINib(nibName: "CodeGalleryHeaderCell", bundle: nil), forCellReuseIdentifier: identifier)
                cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? CodeGalleryHeaderCell
            }
            
            cellView.lblCodeF.text = depositGalleryModel.codeF
            cellView.lblCodeE.text = depositGalleryModel.codeE
            cellView.lblCodeD.text = depositGalleryModel.codeD
            cellView.lblCodeC.text = depositGalleryModel.codeC
            cellView.lblCodeB.text = depositGalleryModel.codeB
            cellView.lblCodeA.text = depositGalleryModel.codeA
            cellView.lblAmount.text = String(depositGalleryModel.amountDisplay)
            cellView.lblBankName.text = depositGalleryModel.bankName
            cellView.lblCount.isHidden = true

            if(depositGalleryModel.bankTransactionId.length == 0)
            {
                cellView.lblUseBelowCode.text = cellView.lblUseBelowCode.text?.localized
                cellView.lblUseBelowCode.isHidden = false
            }
            else
            {
                cellView.lblUseBelowCode.isHidden = true
            }
            cellView.selectionStyle = .none
            
            return cellView
        }
        else if depositGalleryModel.bankCode == "KBZ"
        {
            if indexPath.section == 1
            {
                let identifier = "BCDQRImageCell"
                var cellTextField: BCDQRImageCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BCDQRImageCell
                if cellTextField == nil {
                    tableView.register (UINib(nibName: "BCDQRImageCell", bundle: nil), forCellReuseIdentifier: identifier)
                    cellTextField = tableView.dequeueReusableCell(withIdentifier: identifier) as? BCDQRImageCell
                }
                
                cellTextField.generateQRCodeImage(bcdQRImageModel : depositGalleryModel)
                return cellTextField
            }
            //Old
            else if(indexPath.section == 2)
            {
                let identifier = "BDConfirmationTextFieldCell"
                var cellTextField: BDConfirmationTextFieldCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationTextFieldCell
                if cellTextField == nil {
                    tableView.register (UINib(nibName: "BDConfirmationTextFieldCell", bundle: nil), forCellReuseIdentifier: identifier)
                    cellTextField = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationTextFieldCell
                }
                cellTextField.txtFieldData.isUserInteractionEnabled = false
                
                if(indexPath.row == 0)
                {
                    let identifier = "BDConfirmationLabelCell"
                    var cellView: BDConfirmationLabelCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationLabelCell
                    if cellView == nil {
                        tableView.register (UINib(nibName: "BDConfirmationLabelCell", bundle: nil), forCellReuseIdentifier: identifier)
                        cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationLabelCell
                    }
                    
                    cellView.lblTitle.text = depositGalleryModel.bankName
                    cellView.selectionStyle = .none
                    
                    return cellView
                    
                }
                
                if(titleHeader == "Unused Deposit Code".localized)
                {
                    if(indexPath.row == 1)
                    {
                        cellTextField.txtFieldData.placeholder = "Date".localized
                        cellTextField.txtFieldData.text = depositGalleryModel.date
                    }
                    else
                    {
                        cellTextField.txtFieldData.placeholder = "Time".localized
                        cellTextField.txtFieldData.text =  depositGalleryModel.time
                    }
                }
                else
                {
                    if(indexPath.row == 1)
                    {
                        cellTextField.txtFieldData.placeholder = "Bank Transaction Code".localized
                        cellTextField.txtFieldData.text = depositGalleryModel.bankTransactionId
                    }
                    else if(indexPath.row == 2)
                    {
                        cellTextField.txtFieldData.placeholder = "Bank Transaction Status".localized
                        cellTextField.txtFieldData.text = depositGalleryModel.bankTransactionStatus
                    }
                    else if(indexPath.row == 3)
                    {
                        cellTextField.txtFieldData.placeholder = "OK$ Transaction Code".localized
                        cellTextField.txtFieldData.text = depositGalleryModel.transactionCode
                    }
                    else if(indexPath.row == 4)
                    {
                        cellTextField.txtFieldData.placeholder = "OK$ Transaction Status".localized
                        cellTextField.txtFieldData.text = depositGalleryModel.okTransactionStatus
                    }
                    else if(indexPath.row == 5)
                    {
                        cellTextField.txtFieldData.placeholder = "Branch Code".localized
                        cellTextField.txtFieldData.text = depositGalleryModel.branchCode
                    }
                    else if(indexPath.row == 6)
                    {
                        cellTextField.txtFieldData.placeholder = "Branch Name".localized
                        cellTextField.txtFieldData.text = depositGalleryModel.branchName
                    }
                    else if(indexPath.row == 7)
                    {
                        cellTextField.txtFieldData.placeholder = "Date".localized
                        cellTextField.txtFieldData.text = depositGalleryModel.date
                    }
                    else
                    {
                        cellTextField.txtFieldData.placeholder = "Time".localized
                        cellTextField.txtFieldData.text =  depositGalleryModel.time
                    }
                }
                cellTextField.selectionStyle = .none
                
                return cellTextField
            }
            else
            {
                let identifier = "BDConfirmationTextFieldCell"
                var cellView: BDConfirmationTextFieldCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationTextFieldCell
                if cellView == nil {
                    tableView.register (UINib(nibName: "BDConfirmationTextFieldCell", bundle: nil), forCellReuseIdentifier: identifier)
                    cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationTextFieldCell
                }
                cellView.txtFieldData.isUserInteractionEnabled = false
                
                if(indexPath.section == 3)
                {
                    if(indexPath.row == 0)
                    {
                        cellView.txtFieldData.placeholder = "Beneficiary Name".localized
                        cellView.txtFieldData.text = depositGalleryModel.beneficiaryName
                    }
                    else
                    {
                        cellView.txtFieldData.placeholder = "Beneficiary OK$ Account Number".localized
                        cellView.txtFieldData.text =  self.getActualNum(depositGalleryModel.beneficiaryOkAccountNumber, withCountryCode: "+95")
                    }
                }
                else if(indexPath.section == 4) {
                  cellView.viewLineSeperator.isHidden = false

                    if(indexPath.row == 0)
                    {
                        cellView.txtFieldData.placeholder = "Depositor Name".localized
                        cellView.txtFieldData.text = depositGalleryModel.depositorName
                    }
                    else if(indexPath.row == 1)
                    {
                        cellView.txtFieldData.placeholder = "Depositor Contact Number".localized
                        cellView.txtFieldData.text =  self.getActualNum(depositGalleryModel.depositorNumber, withCountryCode: "+95")
                    }
                    else if(indexPath.row == 2)
                    {
                        cellView.txtFieldData.placeholder = "Remarks".localized
                        cellView.txtFieldData.text =  depositGalleryModel.remarks
                      cellView.viewLineSeperator.isHidden = true
                    }
                }
                
                cellView.selectionStyle = .none
                
                return cellView
            }
        }
        else
        {
            //Old
            if(indexPath.section == 1)
            {
                let identifier = "BDConfirmationTextFieldCell"
                var cellTextField: BDConfirmationTextFieldCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationTextFieldCell
                if cellTextField == nil {
                    tableView.register (UINib(nibName: "BDConfirmationTextFieldCell", bundle: nil), forCellReuseIdentifier: identifier)
                    cellTextField = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationTextFieldCell
                }
                cellTextField.txtFieldData.isUserInteractionEnabled = false
                
                if(indexPath.row == 0)
                {
                    let identifier = "BDConfirmationLabelCell"
                    var cellView: BDConfirmationLabelCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationLabelCell
                    if cellView == nil {
                        tableView.register (UINib(nibName: "BDConfirmationLabelCell", bundle: nil), forCellReuseIdentifier: identifier)
                        cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationLabelCell
                    }
                    
                    cellView.lblTitle.text = depositGalleryModel.bankName
                    cellView.selectionStyle = .none
                    
                    return cellView
                    
                }
                
                if(titleHeader == "Unused Deposit Code".localized)
                {
                  cellTextField.viewLineSeperator.isHidden = false
                    if(indexPath.row == 1)
                    {
                        cellTextField.txtFieldData.placeholder = "Date".localized
                        cellTextField.txtFieldData.text = depositGalleryModel.date
                    }
                    else
                    {
                        cellTextField.txtFieldData.placeholder = "Time".localized
                        cellTextField.txtFieldData.text =  depositGalleryModel.time
                      cellTextField.viewLineSeperator.isHidden = true
                    }
                }
                else
                {
                    cellTextField.viewLineSeperator.isHidden = false

                    if(indexPath.row == 1)
                    {
                        cellTextField.txtFieldData.placeholder = "Bank Transaction Code".localized
                        cellTextField.txtFieldData.text = depositGalleryModel.bankTransactionId
                    }
                    else if(indexPath.row == 2)
                    {
                        cellTextField.txtFieldData.placeholder = "Bank Transaction Status".localized
                        cellTextField.txtFieldData.text = depositGalleryModel.bankTransactionStatus
                    }
                    else if(indexPath.row == 3)
                    {
                        cellTextField.txtFieldData.placeholder = "OK$ Transaction Code".localized
                        cellTextField.txtFieldData.text = depositGalleryModel.transactionCode
                    }
                    else if(indexPath.row == 4)
                    {
                        cellTextField.txtFieldData.placeholder = "OK$ Transaction Status".localized
                        cellTextField.txtFieldData.text = depositGalleryModel.okTransactionStatus
                    }
                    else if(indexPath.row == 5)
                    {
                        cellTextField.txtFieldData.placeholder = "Branch Code".localized
                        cellTextField.txtFieldData.text = depositGalleryModel.branchCode
                    }
                    else if(indexPath.row == 6)
                    {
                        cellTextField.txtFieldData.placeholder = "Branch Name".localized
                        cellTextField.txtFieldData.text = depositGalleryModel.branchName
                    }
                    else if(indexPath.row == 7)
                    {
                        cellTextField.txtFieldData.placeholder = "Date".localized
                        cellTextField.txtFieldData.text = depositGalleryModel.date
                    }
                    else
                    {
                        cellTextField.txtFieldData.placeholder = "Time".localized
                        cellTextField.txtFieldData.text =  depositGalleryModel.time
                        cellTextField.viewLineSeperator.isHidden = true
                    }
                }
                cellTextField.selectionStyle = .none
                
                return cellTextField
            }
            else
            {
                let identifier = "BDConfirmationTextFieldCell"
                var cellView: BDConfirmationTextFieldCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationTextFieldCell
                if cellView == nil {
                    tableView.register (UINib(nibName: "BDConfirmationTextFieldCell", bundle: nil), forCellReuseIdentifier: identifier)
                    cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDConfirmationTextFieldCell
                }
                cellView.txtFieldData.isUserInteractionEnabled = false
                
                if(indexPath.section == 2)
                {
                  cellView.viewLineSeperator.isHidden = false

                    if(indexPath.row == 0)
                    {
                        cellView.txtFieldData.placeholder = "Beneficiary Name".localized
                        cellView.txtFieldData.text = depositGalleryModel.beneficiaryName
                    }
                    else
                    {
                        cellView.txtFieldData.placeholder = "Beneficiary OK$ Account Number".localized
                        cellView.txtFieldData.text =  self.getActualNum(depositGalleryModel.beneficiaryOkAccountNumber, withCountryCode: "+95")
                      cellView.viewLineSeperator.isHidden = true
                    }
                }
                else if(indexPath.section == 3)
                {
                  cellView.viewLineSeperator.isHidden = false

                    if(indexPath.row == 0)
                    {
                        cellView.txtFieldData.placeholder = "Depositor Name".localized
                        cellView.txtFieldData.text = depositGalleryModel.depositorName
                    }
                    else if(indexPath.row == 1)
                    {
                        cellView.txtFieldData.placeholder = "Depositor Contact Number".localized
                        cellView.txtFieldData.text =  self.getActualNum(depositGalleryModel.depositorNumber, withCountryCode: "+95")
                    }
                    else if(indexPath.row == 2)
                    {
                        cellView.txtFieldData.placeholder = "Remarks".localized
                        cellView.txtFieldData.text =  depositGalleryModel.remarks
                      cellView.viewLineSeperator.isHidden = true

                    }
                }
                
                cellView.selectionStyle = .none
                
                return cellView
            }
        }
    }
    
    // Header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if depositGalleryModel.bankCode == "KBZ"
        {
            if(section == 2)
            {
                let view = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 55))
                let label = UILabel(frame: CGRect(x: 15, y: 0, width: self.view.frame.width - 15, height: 55))
                label.text = "Bank Details".localized
                label.backgroundColor = UIColor.clear
                label.textAlignment = NSTextAlignment.center
                if let myFont = UIFont(name: appFont, size: 17) {
                    label.font = myFont
                }
                label.textColor = UIColor.black
                view.addSubview(label)
                view.backgroundColor = UIColor.init(red: 223/255, green: 223/255, blue: 223/255, alpha: 1)
                return view
            }
            else if(section == 3)
            {
                let view = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 55))
                let label = UILabel(frame: CGRect(x: 15, y: 0, width: self.view.frame.width - 15, height: 55))
                
                label.text = "My OK$ Account Details".localized
                
                label.backgroundColor = UIColor.clear
                label.textAlignment = NSTextAlignment.center
                if let myFont = UIFont(name: appFont, size: 17) {
                    label.font = myFont
                }
                label.textColor = UIColor.black
                view.addSubview(label)
                view.backgroundColor = UIColor.init(red: 223/255, green: 223/255, blue: 223/255, alpha: 1)
                return view
            }
            else if(section == 4)
            {
                let view = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 55))
                let label = UILabel(frame: CGRect(x: 15, y: 0, width: self.view.frame.width - 15, height: 55))
                label.text = "Deposit Details".localized
                label.backgroundColor = UIColor.clear
                label.textAlignment = NSTextAlignment.center
                if let myFont = UIFont(name: appFont, size: 17) {
                    label.font = myFont
                }
                label.textColor = UIColor.black
                view.addSubview(label)
                view.backgroundColor = UIColor.init(red: 223/255, green: 223/255, blue: 223/255, alpha: 1)
                return view
            }
            else {
                let view = UIView(frame: CGRect(x: 0, y: 0, width:200, height: 1))
                view.backgroundColor = .clear
                return view
            }
        }
        else
        {
            if(section == 1)
            {
                let view = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 55))
                let label = UILabel(frame: CGRect(x: 15, y: 0, width: self.view.frame.width - 15, height: 55))
                label.text = "Bank Details".localized
                label.backgroundColor = UIColor.clear
                label.textAlignment = NSTextAlignment.center
                if let myFont = UIFont(name: appFont, size: 17) {
                    label.font = myFont
                }
                label.textColor = UIColor.black
                view.addSubview(label)
                view.backgroundColor = UIColor.init(red: 223/255, green: 223/255, blue: 223/255, alpha: 1)
                return view
            }
            else if(section == 2)
            {
                let view = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 55))
                let label = UILabel(frame: CGRect(x: 15, y: 0, width: self.view.frame.width - 15, height: 55))
                
                label.text = "My OK$ Account Details".localized
                
                label.backgroundColor = UIColor.clear
                label.textAlignment = NSTextAlignment.center
                if let myFont = UIFont(name: appFont, size: 17) {
                    label.font = myFont
                }
                label.textColor = UIColor.black
                view.addSubview(label)
                view.backgroundColor = UIColor.init(red: 223/255, green: 223/255, blue: 223/255, alpha: 1)
                return view
            }
            else if(section == 3)
            {
                let view = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 55))
                let label = UILabel(frame: CGRect(x: 15, y: 0, width: self.view.frame.width - 15, height: 55))
                label.text = "Deposit Details".localized
                label.backgroundColor = UIColor.clear
                label.textAlignment = NSTextAlignment.center
                if let myFont = UIFont(name: appFont, size: 17) {
                    label.font = myFont
                }
                label.textColor = UIColor.black
                view.addSubview(label)
                view.backgroundColor = UIColor.init(red: 223/255, green: 223/255, blue: 223/255, alpha: 1)
                return view
            }
            else {
                let view = UIView(frame: CGRect(x: 0, y: 0, width:200, height: 1))
                view.backgroundColor = .clear
                return view
            }
        }
    }
}
