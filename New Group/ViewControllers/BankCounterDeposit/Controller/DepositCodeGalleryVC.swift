//
//  DepositCodeGalleryVC.swift
//  OK
//
//  Created by PC on 2/24/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class DepositCodeGalleryVC: OKBaseController {

    var strHeaderAuthorization = ""
    
    @IBOutlet weak var collDepositCode: UICollectionView!
    @IBOutlet weak var btnUsed: UIButton!
    @IBOutlet weak var btnUnUsed: UIButton!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewInfo: UIView!
    @IBOutlet weak var lblUnusedCount: UILabel!
    @IBOutlet weak var lblUsedCount: UILabel!
    
    @IBOutlet weak var lblInfo: UILabel!
        {
        didSet
        {
            lblInfo.text = lblInfo.text?.localized
        }
    }
    @IBOutlet weak var lblDelete: UILabel!
        {
        didSet
        {
            lblDelete.text = lblDelete.text?.localized
        }
    }
    @IBOutlet weak var lblDeleteInfo: UILabel!
        {
        didSet
        {
            lblDeleteInfo.text = lblDeleteInfo.text?.localized
        }
    }
    
    @IBOutlet weak var lblUnused: UILabel!
    {
        didSet
            {
                lblUnused.text = lblUnused.text?.localized
                lblUnused.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblUsed: UILabel!
    {
        didSet
        {
            lblUsed.font = UIFont(name: appFont, size: appFontSize)
            lblUsed.text = lblUsed.text?.localized
        }
    }
    @IBOutlet var pageControl: UIPageControl!

    var isButtonStatus:Bool = false
    var intVisibleCell: Int = 0

    //Manage Model Data
    var arrUnUsedDepositCode = [DepositCodeGalleryModel]()
    var arrUsedDepositCode = [DepositCodeGalleryModel]()
    //QR Code Image
    var bcdMerchantModel = BCDMerchantModel()
    var bcdQRImageModel = BCDQRImageModel()

    // MARK: - View Controller Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

      pageControl.pageIndicatorTintColor = .white
      pageControl.currentPageIndicatorTintColor = .blue
      
            appDel.floatingButtonControl?.window.closeButton?.isHidden = true
            appDel.floatingButtonControl?.window.isHidden = true
        
        self.getAccessToken()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.helpSupportNavigationEnum = .Add_Withdraw
        self.title = "Deposit Code Gallery".localized

        self.loadInitialView()
        // Do any additional setup after loading the view.
        self.getCodeGalleryAPI()

    }
    
    func loadInitialView()
    {
        btnUnUsed.isSelected = true
        lblUnusedCount.isHidden = true
        lblUsedCount.isHidden = true
        viewInfo.isHidden = true
        viewBottom.isHidden = true
        pageControl.isHidden = true
        
        collDepositCode.register(UINib(nibName: "HorizontalCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HorizontalCollectionViewCell")
    }
    
    // MARK: - Custom Actions
    
    @IBAction func gotoPage(_ sender: UIPageControl) {
       
//        println_debug("gotoPage",sender.tag)
        let indexPath = IndexPath(row: sender.tag, section: 0)
        collDepositCode.reloadItems(at: [indexPath])
    }

    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnUnUsedAction(_ sender: Any) {
        println_debug("btnUnUsedAction")
        btnUsed.isSelected = false
        btnUnUsed.isSelected = true
        isButtonStatus = false
        
        if(arrUnUsedDepositCode.count > 0)
        {
            pageControl.isHidden = false
            pageControl.numberOfPages = arrUnUsedDepositCode.count
            pageControl.currentPage = 0
            viewBottom.isHidden = false
            viewInfo.isHidden = true
            collDepositCode.backgroundView = nil

        }
        else
        {
            let nib = Bundle.main.loadNibNamed("NoRecordsView", owner: self, options: nil)?[0] as! NoRecordsView
            collDepositCode.backgroundView = nib
            
            pageControl.isHidden = true
            viewBottom.isHidden = true
            viewInfo.isHidden = true
        }
        
        collDepositCode.reloadData()
    }
    
    @IBAction func btnUsedAction(_ sender: Any) {
        println_debug("btnUsedAction")
        btnUsed.isSelected = true
        btnUnUsed.isSelected = false
        isButtonStatus = true
        
        if(arrUsedDepositCode.count > 0)
        {
            pageControl.isHidden = false
            pageControl.numberOfPages = self.arrUsedDepositCode.count
            pageControl.currentPage = 0
            viewBottom.isHidden = true
            viewInfo.isHidden = false
            collDepositCode.backgroundView = nil
        }
       else
        {
            let nib = Bundle.main.loadNibNamed("NoRecordsView", owner: self, options: nil)?[0] as! NoRecordsView
            collDepositCode.backgroundView = nib
            pageControl.isHidden = true
            viewBottom.isHidden = true
            viewInfo.isHidden = true
        }
        
        collDepositCode.reloadData()
    }
    
    @IBAction func btnCodeDetailsAction(_ sender: Any) {
        let controller:DepositCodeGalleryDetailsVC = UitilityClass.getStoryBoardAddWithdraw().instantiateViewController(withIdentifier: "DepositCodeGalleryDetailsVC") as! DepositCodeGalleryDetailsVC
        println_debug(intVisibleCell)
        if(isButtonStatus)
        {
            controller.titleHeader = "Used Deposit Code".localized
            controller.depositGalleryModel = arrUsedDepositCode[intVisibleCell]
        }
        else
        {
            controller.titleHeader = "Unused Deposit Code".localized
            controller.depositGalleryModel = arrUnUsedDepositCode[intVisibleCell]
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnDeleteCodeAction(_ sender: Any) {
    
        self.deleteCodeAPI()
    }
    
    // MARK: - API Integration
    
    func getAccessToken()
    {
        println_debug("getAccessToken")
        
        if appDelegate.checkNetworkAvail() {
            
            showProgressView()
            
            let urlStr   = Url.AddMoney_GetBanksToken
            let tokenUrl = getUrl(urlStr: urlStr, serverType: .bankCounterDepositUrl)
            
            let hashValue = Url.aKey_AddMoneyBank.hmac_SHA1(key: Url.sKey_AddMoneyBank)
            
            let inputValue = "password=\(hashValue)&grant_type=password"
            
            let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: "POST", contentType: LLBStrings.kContentType_urlencoded, inputVal: inputValue, authStr: nil)
            JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
                
                guard isSuccess else
                {
                    println_debug("Token API Fail")
                    return
                }
                println_debug("\n\n\n Token Response dict : \(response!)")
                
                let responseDic = response as! NSDictionary
                guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                    self.removeProgressView()
                    return
                }
                
                let authorizationString =  "\(tokentype) \(accesstoken)"
                println_debug(authorizationString)
             
                self.callSubmitKBZCodeAPI(accesstoken: authorizationString)
                
            })
            
        }
    }
    
    func callSubmitKBZCodeAPI(accesstoken:String)
    {
        //fd310392-9c73-4899-af37-de2370ee3ad7
        //let urlStr = String.init(format: Url.BankCounter_BankID,bankDetailsModel.BankId)
        let urlStr = String.init(format: Url.BankCounter_BankID,"fd310392-9c73-4899-af37-de2370ee3ad7")
        let tokenUrl = getUrl(urlStr: urlStr, serverType: .bankCounterDepositUrl)
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: kMethod_Get, contentType: kContentType_Json, inputVal: nil, authStr: accesstoken)
        
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else
            {
                println_debug(" API Fail")
                self.removeProgressView()
                return
            }
            
            let dictTemp = response as! Dictionary<String, Any>

            if dictTemp["Message"] as? String ?? "" == "An error has occurred." {
                println_debug(" API Fail")
                self.removeProgressView()
                return
            }
            
            let arrMerchantDetails = response as! Array<Dictionary<String,AnyObject>>
            println_debug(arrMerchantDetails)
            
            for elementStudentImg in arrMerchantDetails {
                println_debug(elementStudentImg)
                
                if(elementStudentImg["ParameterName"] as! String == "Merchant ID")
                {
                    self.bcdMerchantModel.merchantID = elementStudentImg["ParameterValue"] as? String ?? ""
                }
                else if(elementStudentImg["ParameterName"] as! String == "Payment Type")
                {
                    self.bcdMerchantModel.paymentType = elementStudentImg["ParameterValue"] as? String ?? ""
                }
            }
            
            DispatchQueue.main.async {
                self.removeProgressView()
                self.collDepositCode.reloadData()

            }

        })
    }
    
    func deleteCodeAPI()
    {
        println_debug("deleteCodeAPI")
        
        alertViewObj.wrapAlert(title: "", body: "Are you sure, want to delete generated code permanently from gallery.".localized, img: #imageLiteral(resourceName: "AlertDeleteBCD"))
        alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
            println_debug("cancel")
        }
        
        alertViewObj.addAction(title: "OK".localized, style: .target) {

            let urlStr   = Url.BankCounter_DeleteDeposit + self.arrUnUsedDepositCode[self.intVisibleCell].depositID
            let tokenUrl = getUrl(urlStr: urlStr, serverType: .bankCounterDepositUrl)
            
            let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: "PUT", contentType: LLBStrings.kContentType_Json, inputVal: nil, authStr: self.strHeaderAuthorization)
            JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
                
                guard isSuccess else
                {
                    self.removeProgressView()
                    return
                }
                
                let responseDic = response as! NSDictionary
                println_debug("\n\n Response dict : \(responseDic)")
                
                self.removeProgressView()
                
                self.getCodeGalleryAPI()
            })
        }
        
        alertViewObj.showAlert(controller: self)

    }
    
    func getCodeGalleryAPI()
    {
        println_debug("getCodeGalleryAPI")
        
        if appDelegate.checkNetworkAvail() {
            
            showProgressView()
            
            let urlStr   = Url.BankCounter_GetDeposit + UserModel.shared.mobileNo
            let tokenUrl = getUrl(urlStr: urlStr, serverType: .bankCounterDepositUrl)
            
            println_debug(tokenUrl as Any)
            let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: "GET", contentType: LLBStrings.kContentType_Json, inputVal: nil, authStr: strHeaderAuthorization)
            JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
                
                guard isSuccess else
                {
                    println_debug("Token API Fail")
                    self.removeProgressView()
                    return
                }
                
                if let arrTemp = response as? [Dictionary<String, Any>] {
                println_debug("\n\n\n Array Data : \(arrTemp)")
                
                 self.arrUnUsedDepositCode.removeAll()
                 self.arrUsedDepositCode.removeAll()
                
                for element in 0..<arrTemp.count {
                    println_debug(element)
                    let depositGalleryModel =  DepositCodeGalleryModel()
                    
                    let dictData = arrTemp[element]
                    
                    let tempBank = dictData["Bank"] as! Dictionary<String, Any>
                    depositGalleryModel.bankName = tempBank["BankName"] as! String
                    depositGalleryModel.bankTransactionId = dictData["OkTransactionId"] as! String
                    let isBankTransactionStatus = tempBank["BankTransactionStatus"] as? Bool ?? true
                    depositGalleryModel.bankTransactionStatus = String(describing: isBankTransactionStatus)
                    depositGalleryModel.transactionCode = dictData["TransactionCode"] as! String
                    depositGalleryModel.okTransactionStatus = dictData["OkTransactionStatus"] as! String
                    depositGalleryModel.branchCode = dictData["BranchCode"] as! String
                    depositGalleryModel.branchName = dictData["BranchName"] as! String
                    depositGalleryModel.bankCode = tempBank["BankCode"] as! String
                    depositGalleryModel.bankId = tempBank["BankId"] as! String

                    depositGalleryModel.codeFull = dictData["TransactionCode"] as! String

                    //Use comma in digit
                    let strAmount = dictData["Amount"] as! Int
                    depositGalleryModel.amountDisplay = self.getDigitDisplay(String(strAmount))
                    depositGalleryModel.amount = dictData["Amount"] as! Int

                    
                    //EB-6C-78
                    let strCode = dictData["TransactionCode"] as! String
                    let arrTemp = strCode.split(separator: "-")
                    let firstPart = arrTemp[0]
                    let secondPart = arrTemp[1]
                    let thirdPart = arrTemp[2]

                    depositGalleryModel.codeA = String(Array(firstPart)[0])
                    depositGalleryModel.codeB = String(Array(firstPart)[1])
                    depositGalleryModel.codeC = String(Array(secondPart)[0])
                    depositGalleryModel.codeD = String(Array(secondPart)[1])
                    depositGalleryModel.codeE = String(Array(thirdPart)[0])
                    depositGalleryModel.codeF = String(Array(thirdPart)[1])
                    
                    let strDateTime = self.getDateAndTime(dictData["TransactionDate"] as! String)
                    let arrDateTime = strDateTime.split(separator: "&")
                    depositGalleryModel.date = String(arrDateTime[0])
                    depositGalleryModel.time = String(arrDateTime[1])
                    
                    depositGalleryModel.beneficiaryName = dictData["BeneficiaryName"] as! String
                    depositGalleryModel.beneficiaryOkAccountNumber = dictData["BeneficiaryOkAccountNumber"] as! String
                    
                    depositGalleryModel.depositorName = dictData["DepositorName"] as! String
                    depositGalleryModel.depositorNumber = dictData["DepositorContactNumber"] as! String
                    
                    depositGalleryModel.remarks = dictData["Remark"] as! String
                    depositGalleryModel.depositID = dictData["DepositId"] as! String
                    depositGalleryModel.bankTransactionId = dictData["BankTransactionId"] as! String
                    
                    let strBankTXID = dictData["BankTransactionId"] as! String
                    if(strBankTXID.length == 0)//Unused code Data
                    {
                        self.arrUnUsedDepositCode.append(depositGalleryModel)
                    }
                    else
                    {
                        self.arrUsedDepositCode.append(depositGalleryModel)
                    }
                  
                }
                }
                DispatchQueue.main.async {
                    if(self.arrUnUsedDepositCode.count > 0)
                    {
                        self.pageControl.isHidden = false
                        self.viewBottom.isHidden = false
                        self.pageControl.numberOfPages = self.arrUnUsedDepositCode.count
                        self.pageControl.currentPage = 0
                        self.lblUnusedCount.isHidden = false
                        self.lblUnusedCount.text = String(self.arrUnUsedDepositCode.count)
                    }
                    if(self.arrUnUsedDepositCode.count == 0)
                    {
                        let nib = Bundle.main.loadNibNamed("NoRecordsView", owner: self, options: nil)?[0] as! NoRecordsView
                        self.collDepositCode.backgroundView = nib
                        self.pageControl.isHidden = true
                        self.lblUnusedCount.isHidden = true
                        self.viewBottom.isHidden = true
                        self.viewInfo.isHidden = true
                    }
                    if(self.arrUsedDepositCode.count > 0)
                    {
                        self.lblUsedCount.isHidden = false
                        self.lblUsedCount.text = String(self.arrUsedDepositCode.count)
                    }
                    if(self.arrUsedDepositCode.count == 0)
                    {
                        self.lblUsedCount.isHidden = true
                    }
                    
                    self.collDepositCode.reloadData()

                }
                
                self.removeProgressView()
                
            })
            
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = collDepositCode.contentOffset
        visibleRect.size = collDepositCode.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        let visibleIndexPath: IndexPath = collDepositCode.indexPathForItem(at: visiblePoint)!

        println_debug(visibleIndexPath.row)
        intVisibleCell = visibleIndexPath.row
        pageControl.currentPage = intVisibleCell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DepositCodeGalleryVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        
        return 4;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        
        return 1;
    }
    
    
    //UICollectionViewDatasource methods
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {

        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if(isButtonStatus)
        {
            return arrUsedDepositCode.count
        }
        
        return arrUnUsedDepositCode.count

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let identifier = "HorizontalCollectionViewCell"
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as!HorizontalCollectionViewCell
        
        var depositGalleryModel =  DepositCodeGalleryModel()

        if(isButtonStatus)
        {
            depositGalleryModel = arrUsedDepositCode[indexPath.row]
        }
        else
        {
            depositGalleryModel = arrUnUsedDepositCode[indexPath.row]
        }
        depositGalleryModel.merchantID = bcdMerchantModel.merchantID
        depositGalleryModel.paymentType = bcdMerchantModel.paymentType
        cell.depositCodeGalleryModel = depositGalleryModel
        cell.itemIndex = indexPath.row+1
        cell.tblList.reloadData()
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize{
        let cellWidth  = collDepositCode.frame.size.width-5
        
        return CGSize(width: cellWidth, height: collDepositCode.frame.size.height - 5)
    }
    
}

