//
//  BankCounterDepositDetailsTable.swift
//  OK
//
//  Created by SHUBH on 10/20/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BankCounterDepositDetailsTable: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BankCounterDepositDetailsVC: UITableViewDataSource,UITableViewDelegate{
  
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    
    let view = UIView(frame: CGRect(x: 0, y: 0, width:200, height: 1))
    view.backgroundColor = .clear
    return view
  }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    
    return 1.0;
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    
    return 55.0
    
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
    if(indexPath.section == 1)
    {
      if(isSelectOkAccount)
      {
        if(isMyAccountSelected)
        {
          //Check Personal=1/Merchant=0
          if(UserModel.shared.accountType == "0")
          {
            println_debug("MyAccountselected")
            return 165
          }
            
         println_debug("MyAccountselected else")
          return 110
        }
        else{
          if(isContactSelected)
          {
            if(indexPath.row == 0)
            {
                println_debug("otheraccount isContactSelected\(self.isPersonalAccountNumber)")
                if !self.isPersonalAccountNumber {
                return 55
                } else {
              return 55
                }
                
            } else {
                
            println_debug("otheraccount isContactSelected else\(self.isPersonalAccountNumber)")
                if !self.isPersonalAccountNumber {
                     return 110
                    } else {
                     return 55
                }
                
            }
          }
          else if(isConfirmMobileNoShow)
          {
            if(indexPath.row == 2)
            {
                println_debug("otheraccount isConfirmMobileNoShow ")

              return 110
            }
            else
            {
                println_debug("otheraccount isConfirmMobileNoShow \(isConfirmMobileNoShow)")

              return 55
            }
            
          }
          else if(isOtherAccountSelected)
          {
            println_debug("otheraccount isOtherAccountSelected ")

            return 55
          }
        }
        
        return 55
      }
      
      return 0
    }
    else if(indexPath.section == 2)
    {
      if(isSelectDepositBy)
      {
        if(isSelectDepositByMySelf)
        {
          if(isRemark)
          {
            if(indexPath.row == 0)
            {
              return 165
            }
            return 110
          }
          return 165
        }
        else if(isSelectDepositByOthers)
        {
          if(isOtherDepsoitAccountShow)
          {
            if(indexPath.row == 0)
            {
              return 55.0
            }
            else if(indexPath.row == 1)
            {
              return 110.0
            }
            else if(indexPath.row == 2)
            {
              return 110.0
            }
            
          }
          return 55.0
        }
        return 55.0
      }
    }
    return 55.0
    
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    
    if(isMyAccountSelected)
    {
      return 3
    }
    else if(isContactSelected)
    {
      return 3
    }
    return 2
    
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if(section == 1)
    {
      if(isSelectOkAccount)
      {
        if(isMyAccountSelected)
        {
          return 1
        }
        else{
          if(isContactSelected)
          {
            return 2
          }
          else if(isConfirmMobileNoShow)
          {
            if(isOtherAccountShow)
            {
              return 3
            }
            return 2
          }
          else if(isOtherAccountSelected)
          {
            return 1
          }
        }
        
        return 2
      }
      
      return 0
    }
    else if(section == 2)
    {
      if(isSelectDepositBy)
      {
        if(isSelectDepositByMySelf)
        {
          if(isRemark)
          {
            return 2
          }
          return 1
        }
        else if(isSelectDepositByOthers)
        {
          if(isOtherDepsoitAccountShow)
          {
            if(isRemark)
            {
              return 3
            }
            return 2
          }
          return 1
        }
        return 2
      }
      return 0
    }
    return 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    println_debug("isPersonalAccountNumber----\(isPersonalAccountNumber)")
    
    if(indexPath.section == 1)
    {
      if(isMyAccountSelected)
      {
        
        if !isPersonalAccountNumber  {
            
        let identifier = "BDMyAccountSelectedCell"
        var cellView: BDMyAccountSelectedCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDMyAccountSelectedCell
        if cellView == nil {
          tableView.register (UINib(nibName: "BDMyAccountSelectedCell", bundle: nil), forCellReuseIdentifier: identifier)
          cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDMyAccountSelectedCell
        }
        cellView.txtAccountNumber.text = self.getActualNum(UserModel.shared.mobileNo,withCountryCode: "+95")
        cellView.txtAccountName.text = UserModel.shared.name
        
        addMoneyModel.depositorContactNumber = UserModel.shared.mobileNo
        addMoneyModel.okACOwnername = UserModel.shared.name
        
        cellView.txtBusinessName.text = UserModel.shared.businessName
        cellView.txtBusinessName.placeholder = cellView.txtBusinessName.placeholder?.localized
        cellView.txtAccountNumber.placeholder = cellView.txtAccountNumber.placeholder?.localized
        cellView.txtAccountName.placeholder = cellView.txtAccountName.placeholder?.localized
        
        cellView.selectionStyle = .none
        
        return cellView
            
        } else {
        
        
        let identifier = "BDMyPersonalAccountSelectedCellTableViewCell"
                              var cellView: BDMyPersonalAccountSelectedCellTableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDMyPersonalAccountSelectedCellTableViewCell
                              if cellView == nil {
                                tableView.register (UINib(nibName: "BDMyPersonalAccountSelectedCellTableViewCell", bundle: nil), forCellReuseIdentifier: identifier)
                                cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDMyPersonalAccountSelectedCellTableViewCell
                              }
                              
                cellView.txtAccountNumberPersonal.text = self.getActualNum(UserModel.shared.mobileNo,withCountryCode: "+95")
                cellView.txtAccountNamePersonal.text = UserModel.shared.name
        
                addMoneyModel.depositorContactNumber = UserModel.shared.mobileNo
                addMoneyModel.okACOwnername = UserModel.shared.name
        
        
                cellView.txtAccountNumberPersonal.placeholder = cellView.txtAccountNumberPersonal.placeholder?.localized
                cellView.txtAccountNamePersonal.placeholder = cellView.txtAccountNamePersonal.placeholder?.localized
                              
                cellView.selectionStyle = .none
                              
                return cellView
        }
                   
      }
      else if(isOtherAccountSelected)
      {
        if(indexPath.row == 0)
        {
          let identifier = "AMAccountMobileNoCell"
          var cellView: AMAccountMobileNoCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? AMAccountMobileNoCell
          if cellView == nil {
            tableView.register (UINib(nibName: "AMAccountMobileNoCell", bundle: nil), forCellReuseIdentifier: identifier)
            cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? AMAccountMobileNoCell
          }
          cellView.btnContact.tag = 73
          cellView.btnContact.addTarget(self, action:#selector(self.btnContactAction(sender:)), for: .touchUpInside)
          cellView.btnCancel.tag = 53
          cellView.btnCancel.addTarget(self, action:#selector(self.btnCancelAction(sender:)), for: .touchUpInside)
          
          cellView.txtAccountMobNo.delegate = self
          cellView.txtAccountMobNo.tag = 50
          cellView.txtAccountMobNo.becomeFirstResponder()
          cellView.txtAccountMobNo.addTarget(self, action: #selector(BankCounterDepositDetailsVC.textFieldDidChange(_:)),
                                             for: UIControl.Event.editingChanged)
          cellView.txtAccountMobNo.text = addMoneyModel.depositorContactNumber
          
          if((cellView.txtAccountMobNo.text?.count)! < 2)
          {
            cellView.txtAccountMobNo.placeholder = "Enter OK$ Account Mobile Number".localized
            cellView.btnCancel.isHidden = true
          }
          else
          {
            cellView.txtAccountMobNo.placeholder = "OK$ Account Mobile Number".localized
            cellView.btnCancel.isHidden = true
          }
          cellView.selectionStyle = .none
          
          return cellView
        }
        else if(indexPath.row == 1)
        {
          //Display data while selected from contact list
          if(isContactSelected)
          {

            if !isPersonalAccountNumber  {

                println_debug("Contactselecte NOTTTPersonal called")

            let identifier = "BDOtherAccountSelectedCell"
            var cellView: BDOtherAccountSelectedCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDOtherAccountSelectedCell
            if cellView == nil {
              tableView.register (UINib(nibName: "BDOtherAccountSelectedCell", bundle: nil), forCellReuseIdentifier: identifier)
              cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDOtherAccountSelectedCell
            }
            
            cellView.txtBusinessName.placeholder = cellView.txtBusinessName.placeholder?.localized
            cellView.txtBusinessName.text = addMoneyModel.businessName
            cellView.txtAccountName.placeholder = cellView.txtAccountName.placeholder?.localized
            cellView.txtAccountName.text = addMoneyModel.okACOwnername
            
            cellView.selectionStyle = .none
            
            return cellView
                
            } else {
                
                println_debug("Contactselecte Personal called")
            
            let identifier = "BDOtherAccountPersonalSelectedCell"
                       var cellView: BDOtherAccountPersonalSelectedCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDOtherAccountPersonalSelectedCell
                       if cellView == nil {
                         tableView.register (UINib(nibName: "BDOtherAccountPersonalSelectedCell", bundle: nil), forCellReuseIdentifier: identifier)
                         cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDOtherAccountPersonalSelectedCell
                       }
                       
                       cellView.txtAccountNamePersonal.placeholder = cellView.txtAccountNamePersonal.placeholder?.localized
                       cellView.txtAccountNamePersonal.text = addMoneyModel.okACOwnername
                      
                       
                       cellView.selectionStyle = .none
                       
                       return cellView 
            }
            
          }
          else//Display confirmation Mobile number
          {
            let identifier = "AMConfirmAccountMobNoCell"
            var cellView: AMConfirmAccountMobNoCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? AMConfirmAccountMobNoCell
            if cellView == nil {
              tableView.register (UINib(nibName: "AMConfirmAccountMobNoCell", bundle: nil), forCellReuseIdentifier: identifier)
              cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? AMConfirmAccountMobNoCell
            }
            cellView.btnConfirmCancel.tag = 54
            cellView.btnConfirmCancel.addTarget(self, action:#selector(self.btnCancelAction(sender:)), for: .touchUpInside)
            
            cellView.txtConfirmMobileNo.delegate = self
            cellView.txtConfirmMobileNo.tag = 51
            cellView.txtConfirmMobileNo.becomeFirstResponder()
            cellView.txtConfirmMobileNo.addTarget(self, action: #selector(BankCounterDepositDetailsVC.textFieldDidChange(_:)),
                                                  for: UIControl.Event.editingChanged)
            cellView.txtConfirmMobileNo.text = addMoneyModel.depositorConfirmContactNumber
            cellView.txtConfirmMobileNo.placeholder = cellView.txtConfirmMobileNo.placeholder?.localized
            cellView.btnConfirmCancel.isHidden = true
            cellView.selectionStyle = .none
            
            return cellView
          }
        }
        else
        {
            
            if !isPersonalAccountNumber  {

          let identifier = "BDOtherAccountSelectedCell"
          var cellView: BDOtherAccountSelectedCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDOtherAccountSelectedCell
          if cellView == nil {
            tableView.register (UINib(nibName: "BDOtherAccountSelectedCell", bundle: nil), forCellReuseIdentifier: identifier)
            cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDOtherAccountSelectedCell
          }
          
          cellView.txtAccountName.placeholder = cellView.txtAccountName.placeholder?.localized
          cellView.txtAccountName.text = addMoneyModel.okACOwnername
          cellView.txtBusinessName.placeholder = cellView.txtBusinessName.placeholder?.localized
          cellView.txtBusinessName.text = addMoneyModel.businessName
          
          cellView.selectionStyle = .none
          
          return cellView
                
            } else {
            
            println_debug("Contactselected else part called")
            
            let identifier = "BDOtherAccountPersonalSelectedCell"
            var cellView: BDOtherAccountPersonalSelectedCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDOtherAccountPersonalSelectedCell
            if cellView == nil {
              tableView.register (UINib(nibName: "BDOtherAccountPersonalSelectedCell", bundle: nil), forCellReuseIdentifier: identifier)
              cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDOtherAccountPersonalSelectedCell
            }
            
            cellView.txtAccountNamePersonal.placeholder = cellView.txtAccountNamePersonal.placeholder?.localized
            cellView.txtAccountNamePersonal.text = addMoneyModel.okACOwnername
            
            cellView.selectionStyle = .none
            
            return cellView
            }
        }
      }
      else
      {
        if(indexPath.row == 0)
        {
          let identifier = "MyOkAccountCell"
          var cellView: MyOkAccountCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? MyOkAccountCell
          if cellView == nil {
            tableView.register (UINib(nibName: "MyOkAccountCell", bundle: nil), forCellReuseIdentifier: identifier)
            cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? MyOkAccountCell
          }
          cellView.btnMyAccount.addTarget(self, action:#selector(self.btnMyAccountAction(sender:)), for: .touchUpInside)
          cellView.btnMyAccount.setTitle(cellView.btnMyAccount.titleLabel?.text?.localized, for: .normal)
          cellView.lblMyNo.text = self.getActualNum(UserModel.shared.mobileNo,withCountryCode: "+95")
          addMoneyModel.receiverName = UserModel.shared.name
          cellView.selectionStyle = .none
          
          return cellView
        }
        else
        {
          let identifier = "OtherOkAccountCell"
          var cellView: OtherOkAccountCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? OtherOkAccountCell
          if cellView == nil {
            tableView.register (UINib(nibName: "OtherOkAccountCell", bundle: nil), forCellReuseIdentifier: identifier)
            cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? OtherOkAccountCell
          }
          cellView.btnOtherAccount.addTarget(self, action:#selector(self.btnOtherAccountAction(sender:)), for: .touchUpInside)
          cellView.btnOtherAccount.setTitle(cellView.btnOtherAccount.titleLabel?.text?.localized, for: .normal)
          cellView.selectionStyle = .none
          
          return cellView
        }
      }
    }
    else if(indexPath.section == 2)
    {
      //Deposit by Myself
      if(isSelectDepositByMySelf)
      {
        if(indexPath.row == 0)
        {
          
          let identifier = "BDDepositByMyselfCell"
          var cellView: BDDepositByMyselfCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDDepositByMyselfCell
          if cellView == nil {
            tableView.register (UINib(nibName: "BDDepositByMyselfCell", bundle: nil), forCellReuseIdentifier: identifier)
            cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDDepositByMyselfCell
          }
          
          cellView.txtMyName.placeholder = cellView.txtMyName.placeholder?.localized
          cellView.txtMyContact.placeholder = cellView.txtMyContact.placeholder?.localized
          cellView.txtAmount.placeholder = cellView.txtAmount.placeholder?.localized
          
          cellView.txtMyName.text = UserModel.shared.name
          addMoneyModel.depositorName = UserModel.shared.name
          
          cellView.txtMyContact.text = self.getActualNum(UserModel.shared.mobileNo, withCountryCode: "+95")//UserModel.shared.mobileNo
          cellView.txtAmount.delegate = self
          cellView.txtAmount.tag = 60
          cellView.txtAmount.addTarget(self, action: #selector(BankCounterDepositDetailsVC.textFieldDidChange(_:)),
                                       for: UIControl.Event.editingChanged)
          
          cellView.txtAmount.text = strEnterAmountValue
          
          if(strEnterAmountValue.length > 0)
          {
            cellView.txtAmount.becomeFirstResponder()
            cellView.txtAmount.placeholder = "Amount".localized
            
            if(strEnterAmountValue.length == 9)
            {
              cellView.txtAmount.textColor = UIColor.green
            }
            else if(strEnterAmountValue.length == 10)
            {
              cellView.txtAmount.textColor = UIColor.red
            }
            else
            {
              cellView.txtAmount.textColor = UIColor.black
            }
          }
          else
          {
            cellView.txtAmount.placeholder = "Enter Amount".localized
          }
          
          cellView.selectionStyle = .none
          
          return cellView
        }
        else
        {
          let identifier = "BDDepositByRemarkCell"
          var cellView: BDDepositByRemarkCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDDepositByRemarkCell
          if cellView == nil {
            tableView.register (UINib(nibName: "BDDepositByRemarkCell", bundle: nil), forCellReuseIdentifier: identifier)
            cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDDepositByRemarkCell
          }
          cellView.txtRemark.delegate =  self
          cellView.txtRemark.tag = 64
          cellView.txtRemark.text = addMoneyModel.remarks
          cellView.txtRemark.addTarget(self, action: #selector(BankCounterDepositDetailsVC.textFieldDidChange(_:)),
                                       for: UIControl.Event.editingChanged)
          cellView.nextButton.addTarget(self, action:#selector(self.btnNextAction(sender:)), for: .touchUpInside)
          cellView.nextButton.setTitle(cellView.nextButton.titleLabel?.text?.localized,for: .normal)
          
          cellView.txtRemark.placeholder = cellView.txtRemark.placeholder?.localized
          cellView.selectionStyle = .none
          
          return cellView
        }
      }
      else if(isSelectDepositByOthers) //Depsoit By Others
      {
        if(indexPath.row == 0)
        {
          let identifier = "AMAccountMobileNoCell"
          var cellView: AMAccountMobileNoCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? AMAccountMobileNoCell
          if cellView == nil {
            tableView.register (UINib(nibName: "AMAccountMobileNoCell", bundle: nil), forCellReuseIdentifier: identifier)
            cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? AMAccountMobileNoCell
          }
          cellView.btnContact.tag = 72
          cellView.btnContact.addTarget(self, action:#selector(self.btnContactAction(sender:)), for: .touchUpInside)
          cellView.btnCancel.tag = 52
          cellView.btnCancel.addTarget(self, action:#selector(self.btnCancelAction(sender:)), for: .touchUpInside)
          
          cellView.txtAccountMobNo.placeholder = "Depositor Contact Number".localized
          cellView.txtAccountMobNo.delegate = self
          cellView.txtAccountMobNo.tag = 65
          cellView.txtAccountMobNo.becomeFirstResponder()
          cellView.txtAccountMobNo.addTarget(self, action: #selector(BankCounterDepositDetailsVC.textFieldDidChange(_:)),
                                             for: UIControl.Event.editingChanged)
          cellView.txtAccountMobNo.text = addMoneyModel.depositorOtherContactNumber
         // cellView.btnCancel.isHidden = true
            if((cellView.txtAccountMobNo.text?.count)! < 2)
            {

              cellView.btnCancel.isHidden = true
            }
            else
            {
              cellView.btnCancel.isHidden = true
            }
            
            
          cellView.selectionStyle = .none
          return cellView
        }
        else if(indexPath.row == 1)
        {
          let identifier = "AMOtherAccountSelectedCell"
          var cellView: AMOtherAccountSelectedCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? AMOtherAccountSelectedCell
          if cellView == nil {
            tableView.register (UINib(nibName: "AMOtherAccountSelectedCell", bundle: nil), forCellReuseIdentifier: identifier)
            cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? AMOtherAccountSelectedCell
          }
          
          cellView.txtAccountName.placeholder = "Depositor Name".localized
          cellView.txtAccountName.delegate = self
          cellView.txtAccountName.tag = 61
          cellView.txtAccountName.addTarget(self, action: #selector(BankCounterDepositDetailsVC.textFieldDidChange(_:)),
                                            for: UIControl.Event.editingChanged)
          cellView.txtAccountName.text = addMoneyModel.depositorOtherContactName
          
          cellView.txtAmount.placeholder = cellView.txtAmount.text?.localized
          cellView.txtAmount.delegate = self
          cellView.txtAmount.tag = 63
          cellView.btnOtherAmountCancel.tag = 55
          cellView.txtAmount.addTarget(self, action: #selector(BankCounterDepositDetailsVC.textFieldDidChange(_:)),
                                       for: UIControl.Event.editingChanged)
            
          cellView.btnOtherAmountCancel.addTarget(self, action:#selector(self.btnCancelAction(sender:)), for: .touchUpInside)
          
          cellView.txtAmount.text = strEnterAmountValue
          
          if(strEnterAmountValue.length > 0)
          {
            cellView.txtAmount.becomeFirstResponder()
            cellView.txtAmount.placeholder = "Amount".localized
            cellView.btnOtherAmountCancel.isHidden = false
            
            if(strEnterAmountValue.length == 9)
            {
              cellView.txtAmount.textColor = UIColor.green
            }
            else if(strEnterAmountValue.length == 10)
            {
              cellView.txtAmount.textColor = UIColor.red
            }
            else
            {
              cellView.txtAmount.textColor = UIColor.black
            }
          }
          else
          {
            cellView.txtAmount.placeholder = "Enter Amount".localized
            cellView.btnOtherAmountCancel.isHidden = true
          }
          cellView.selectionStyle = .none
          
          return cellView
        }
        else
        {
          let identifier = "BDDepositByRemarkCell"
          var cellView: BDDepositByRemarkCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDDepositByRemarkCell
          if cellView == nil {
            tableView.register (UINib(nibName: "BDDepositByRemarkCell", bundle: nil), forCellReuseIdentifier: identifier)
            cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDDepositByRemarkCell
          }
          cellView.txtRemark.delegate =  self
          cellView.txtRemark.tag = 64
          cellView.txtRemark.text = addMoneyModel.remarks
          cellView.txtRemark.addTarget(self, action: #selector(BankCounterDepositDetailsVC.textFieldDidChange(_:)),
                                       for: UIControl.Event.editingChanged)
          cellView.nextButton.addTarget(self, action:#selector(self.btnNextAction(sender:)), for: .touchUpInside)
          cellView.nextButton.setTitle(cellView.nextButton.titleLabel?.text?.localized,for: .normal)
          
          cellView.txtRemark.placeholder = cellView.txtRemark.placeholder?.localized
          cellView.selectionStyle = .none
          
          return cellView
        }
      }
      else
      {
        let identifier = "BDDepositByTitleCell"
        var cellView: BDDepositByTitleCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDDepositByTitleCell
        if cellView == nil {
          tableView.register (UINib(nibName: "BDDepositByTitleCell", bundle: nil), forCellReuseIdentifier: identifier)
          cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDDepositByTitleCell
        }
        cellView.btnSelectDepositBy.tag = indexPath.row
        cellView.btnSelectDepositBy.addTarget(self, action:#selector(self.btnDepositByUserAction(sender:)), for: .touchUpInside)
        
        if(indexPath.row == 0)
        {
          cellView.lblTitle.text = "MyselfBCD".localized
        }
        else
        {
          cellView.lblTitle.text = "OthersBCD".localized
        }
        cellView.selectionStyle = .none
        
        return cellView
      }
    }
    else
    {
      let identifier = "OtherOkAccountCell"
      var cellView: OtherOkAccountCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? OtherOkAccountCell
      if cellView == nil {
        tableView.register (UINib(nibName: "OtherOkAccountCell", bundle: nil), forCellReuseIdentifier: identifier)
        cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? OtherOkAccountCell
      }
      cellView.btnOtherAccount.setTitle(cellView.btnOtherAccount.titleLabel?.text?.localized, for: .normal)
      cellView.btnOtherAccount.addTarget(self, action:#selector(self.btnOtherAccountAction(sender:)), for: .touchUpInside)
      cellView.selectionStyle = .none
      
      return cellView
    }
    
  }
  
  // Header
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
    if(section == 0)
    {
      let identifier = "SelectionTypeHeaderCell"
      var cellView: SelectionTypeHeaderCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? SelectionTypeHeaderCell
      if cellView == nil {
        tableView.register (UINib(nibName: "SelectionTypeHeaderCell", bundle: nil), forCellReuseIdentifier: identifier)
        cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? SelectionTypeHeaderCell
      }
      cellView.imgArrow.isHidden = true
      cellView.imgIcon.image = UIImage(named: "bank")
      cellView.txtSelectionType.placeholder = "Bank".localized
      if appDel.getSelectedLanguage() == "my"
      {
        cellView.txtSelectionType.text = bankDetailsModel.BankNameBurmese
      }
      else
      {
        cellView.txtSelectionType.text = bankDetailsModel.BankName
      }
      return cellView
    }
    else if(section == 1)
    {
      let identifier = "SelectAccountHeaderCell"
      var cellView: SelectAccountHeaderCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? SelectAccountHeaderCell
      if cellView == nil {
        tableView.register (UINib(nibName: "SelectAccountHeaderCell", bundle: nil), forCellReuseIdentifier: identifier)
        cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? SelectAccountHeaderCell
      }
      
      cellView.btnSelectAccount.addTarget(self, action:#selector(self.selectAccountAction(sender:)), for: .touchUpInside)
      cellView.btnSelectAccount.setTitle(cellView.btnSelectAccount.titleLabel?.text?.localized, for: .normal)
      
      //Arrow icon
      if(isSelectOkAccount)
      {
        cellView.imgArrowIcon.image = UIImage(named: "upArrow")
      }
      else{
        cellView.imgArrowIcon.image = UIImage(named: "downArrow")
      }
      
      if(isMyAccountSelected){
        cellView.btnSelectAccount.setTitle("My OK$ Account".localized, for: .normal)
        cellView.imgArrowIcon.image = UIImage(named: "downArrow")
      }
      
      if(isOtherAccountSelected){
        cellView.btnSelectAccount.setTitle("Other OK$ Account".localized, for: .normal)
        cellView.imgArrowIcon.image = UIImage(named: "downArrow")
      }
      return cellView
    }
    else if(section == 2){
      let identifier = "BDDepositByHeaderCell"
      var cellView: BDDepositByHeaderCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDDepositByHeaderCell
      if cellView == nil {
        tableView.register (UINib(nibName: "BDDepositByHeaderCell", bundle: nil), forCellReuseIdentifier: identifier)
        cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? BDDepositByHeaderCell
      }
      
      cellView.btnSelectAccount.addTarget(self, action:#selector(self.selectDepositByAction(sender:)), for: .touchUpInside)
      cellView.txtDepositBy.placeholder = cellView.txtDepositBy.placeholder?.localized
      
      //Arrow icon
      if(isSelectDepositBy){
        cellView.imgArrowIcon.image = UIImage(named: "upArrow")
      }
      else{
        cellView.imgArrowIcon.image = UIImage(named: "downArrow")
      }
      
      if(isSelectDepositByMySelf){
        cellView.txtDepositBy.text = "MyselfBCD".localized
        cellView.imgArrowIcon.image = UIImage(named: "downArrow")
      }
      if(isSelectDepositByOthers){
        cellView.txtDepositBy.text = "OthersBCD".localized
        cellView.imgArrowIcon.image = UIImage(named: "downArrow")
      }
      return cellView
    }
    else {
      let view = UIView(frame: CGRect(x: 0, y: 0, width:200, height: 1))
      view.backgroundColor = .clear
      return view
    }
  }
  
  // MARK: - Custom Methods
  func moveToNextScreen()
  {
    //if UserLogin.shared.loginSessionExpired {
      OKPayment.main.authenticate(screenName: "BankCounterDepositDetailsNext", delegate: self)
//    } else{
//      let controller:BankCounterDepositConfirmationVC = UitilityClass.getStoryBoardAddWithdraw().instantiateViewController(withIdentifier: "BankCounterDepositConfirmationVC") as! BankCounterDepositConfirmationVC
//      controller.addMoneyModel =  addMoneyModel
//      controller.bankDetailsModel =  bankDetailsModel
//      self.navigationController?.pushViewController(controller, animated: true)
//    }
  }
  
  @objc func btnNextAction(sender: UIButton!) {
    println_debug("btnNextAction")
    if isSelectDepositByOthers{
      if addMoneyModel.depositorOtherContactName.count > 0 && addMoneyModel.depositorOtherContactNumber.count > 4
      {
        self.moveToNextScreen()
      }
      else
      {
        alertViewObj.wrapAlert(title: nil, body: "Please Fill All Fields".localized, img: UIImage(named : "AppIcon"))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
          
        })
        alertViewObj.showAlert(controller: self)
      }
    }
    else
    {
      self.moveToNextScreen()
    }
  }
  
  @objc func selectDepositByAction(sender: UIButton!) {
    println_debug("selectDepositByAction")
    
    isSelectDepositByMySelf = false
    isSelectDepositByOthers = false
    strEnterAmountValue = ""
    addMoneyModel.depositorOtherContactNumber = ""
    addMoneyModel.depositorOtherContactName = ""
    isRemark = false
    
    if(isSelectDepositBy)
    {
      isSelectDepositBy = false
    }
    else{
      isSelectDepositBy = true
    }
    tblAccountList.reloadData()
  }
  
  @objc func btnDepositByUserAction(sender: UIButton!) {
    
    println_debug("btnDepositByUserAction")
    
    if(sender.tag == 1)//Show others
    {
      addMoneyModel.depositeBy = "Others"
      isSelectDepositByOthers = true
      isSelectDepositByMySelf = false
    }
    else//Show Myself
    {
      addMoneyModel.depositeBy = "Myself"
      isSelectDepositByMySelf = true
      isSelectDepositByOthers = false
    }
    
    tblAccountList.reloadData()
  }
  
  @objc func btnContactAction(sender: UIButton!) {
    println_debug("btnContactAction")
    
    self.hideContactSuggesionView()
    
    if(sender.tag == 73)//for section 1
    {
      
    }
    else if(sender.tag == 72)//for section 2
    {
      isOtherDepositContactSelected = true
    }
    let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
    self.present(nav, animated: true, completion: nil)
  }
  
  @objc func btnCancelAction(sender: UIButton!) {
    println_debug("btnCancelAction")
    if(sender.tag == 53)//Section -1
    {
        addMoneyModel.depositorContactNumber = ""
        addMoneyModel.depositorConfirmContactNumber = ""
        isConfirmMobileNoShow = false
        isContactSelected = false
        isOtherAccountShow = false
        let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? AMAccountMobileNoCell
       // cell?.txtAccountMobNo.becomeFirstResponder()
        cell?.txtAccountMobNo.text = "09"
    }
    else if(sender.tag == 52)//Section -2
    {
      addMoneyModel.depositorOtherContactNumber = ""
      addMoneyModel.depositorOtherContactName = ""
      addMoneyModel.enteredAmount = ""
      addMoneyModel.remarks = ""
      isRemark = false
      isSelectDepositByOthers = true
      isSelectDepositByMySelf = false
      isOtherDepsoitAccountShow = false
      strEnterAmountValue = ""
     let cellother = self.tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 2)) as? AMAccountMobileNoCell
        cellother?.txtAccountMobNo.text = "09"
    }
    else if(sender.tag == 54)//confirm =54
    {
        addMoneyModel.depositorConfirmContactNumber = ""
        isContactSelected = false
        isOtherAccountShow = false
        isConfirmMobileNoShow = true
        let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? AMConfirmAccountMobNoCell
       // cell?.txtAccountMobNo.becomeFirstResponder()
        cell?.txtConfirmMobileNo.text = "09"
        
    }
    else{ // tag = 55
        strEnterAmountValue = ""
        let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 2)) as? AMOtherAccountSelectedCell
       // cell?.txtAccountMobNo.becomeFirstResponder()
        cell?.txtAmount.text = ""
    }
    tblAccountList.reloadData()
  }
  
  @objc func btnMyAccountAction(sender: UIButton!) {
    println_debug("btnMyAccountAction")
    
    isMyAccountSelected = true
    isOtherAccountSelected = false
    
    //Check Personal=1/Merchant=0
    if(UserModel.shared.accountType == "0")
    {
        self.isPersonalAccountNumber = false
    } else {
        self.isPersonalAccountNumber = true

    }
    
    addMoneyModel.selectedOKAC = "Myself"
    addMoneyModel.IsMyOwnAccount = true
    tblAccountList.reloadData()
  }
  
  @objc func btnOtherAccountAction(sender: UIButton!) {
    println_debug("btnOtherAccountAction")
    isMyAccountSelected = false
    isOtherAccountSelected = true
    
    addMoneyModel.selectedOKAC = "Others"
    addMoneyModel.IsMyOwnAccount = false
    tblAccountList.reloadData()
  }
  
  @objc func selectAccountAction(sender: UIButton!) {
    println_debug("selectAccountAction")
    
    isMyAccountSelected = false
    isOtherAccountSelected = false
    isSelectDepositBy = false
    isSelectDepositByOthers = false
    isSelectDepositByMySelf = false
    isContactSelected = false
    isRemark = false
    isOtherAccountShow = false
    isShowEnterAmountOthers = false
    isContactSelected = false
    isConfirmMobileNoShow = false
    isOtherDepsoitAccountShow = false
    
    addMoneyModel.depositorContactNumber = ""
    addMoneyModel.depositorOtherContactName = ""
    addMoneyModel.depositorOtherContactNumber = ""
    addMoneyModel.depositorConfirmContactNumber = ""
    addMoneyModel.remarks = ""
    addMoneyModel.depositorName = ""
    strEnterAmountValue = ""
    if(isSelectOkAccount)
    {
      isSelectOkAccount = false
    }
    else{
      isSelectOkAccount = true
    }
    tblAccountList.reloadData()
  }
}
