//
//  MoreItemMenuMyQrVC.swift
//  OK
//
//  Created by palnar on 19/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
 
class MoreItemMenuMyQrVC: UIViewController {

    var printableImage :UIImage?
    @IBOutlet weak var shareButton : UIButton!
    @IBOutlet weak var printButton : UIButton!
    
    override func viewDidLoad() {
        self.helpSupportNavigationEnum = .Request_Money

        super.viewDidLoad()
         self.view.backgroundColor = UIColor.init(red: 152/255, green: 161/255, blue: 167/255, alpha: 0.8)
        // Do any additional setup after loading the view.
        shareButton.setTitle("Print QR Code".localized, for: .normal)
        printButton.setTitle("Share QR Code".localized, for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func closeButtonTapped(_ sender: Any) {
        closePage()
    }
    
    @IBAction func shareButtonTapped(_ sender: Any) {
        showSharePage()
    }
    
    @IBAction func printButtonTapped(_ sender: Any) {
        showPrintPage()
    }
    
    private func closePage() {
       self.dismiss(animated: true, completion: nil)
    }
    
    private func showSharePage() {
        
        guard let image = printableImage else { return }
        
        let activityViewController = UIActivityViewController(activityItems: [image], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    
    private func showPrintPage() {
        
        ///
        guard let image = printableImage, let presentableView = view else {
            return
        }
        
        let rect = presentableView.frame
        let animation = true
        let printInfo = UIPrintInfo(dictionary:nil)
        let printController = UIPrintInteractionController.shared
        
        ///
        printInfo.outputType = UIPrintInfo.OutputType.general
        printInfo.jobName = "My Print Job"
        ///
        printController.printInfo = printInfo
        printController.printingItem = image
        
        // Do it
        printController.present(from: rect, in: presentableView, animated: animation, completionHandler: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
