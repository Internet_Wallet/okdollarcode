//
//  RequestMoneyRootViewController.swift
//  OK
//
//  Created by palnar on 29/11/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit


class RequestMoneyRootViewController: ButtonBarPagerTabStripViewController {
    
    lazy var _floatingButton: FloatingButton = {
        
        let floatingButton = FloatingButton()
        floatingButton.setImage(UIImage(named: "menu_white_payto.png"), for: UIControl.State.normal)
        floatingButton.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
//        floatingButton.contentEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
        floatingButton.addTarget(self, action: #selector(moreButtonAction(sender:)), for: UIControl.Event.touchUpInside)
        
        return floatingButton
    }()
    
    override func viewDidLoad() {
        
        self.helpSupportNavigationEnum = .Request_Money

        configureTabBarView()
        
        super.viewDidLoad()
        setLeftBarButtonItems()
        setRightBarButtonItems()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        NotificationCenter.default.addObserver(self, selector: #selector(backButtonAction), name: NSNotification.Name(rawValue: "RequestMoneyHomeTapAction"), object: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Request Money".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]

    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func backButtonAction()
    {
        //        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func moreButtonAction(sender :FloatingButton) {
        
        let settings = FloatingMenu.Settings(position: .topRight(30, 16), padingFromFloatingButton: 84, menuSpacing: 19, floatingButton: sender)
        
        FloatingMenu(settings: settings).set(menuItems: _getMenuItems()).show(On: self)
    }
    
    private func setLeftBarButtonItems() {
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let button1 = UIBarButtonItem(image: UIImage(named:"tabBarBack"), style: .plain, target: self, action:#selector(backButtonAction)) // action:#selector(Class.MethodName) for swift 3
        button1.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItems = [button1]
    }
    
    private func setRightBarButtonItems() {
        let moreButton = UIBarButtonItem(customView: self._floatingButton)
        moreButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItems = [moreButton]
    }
    
    private func _getMenuItems() ->[FloatingMenuItemView] {
        
        var menuItems = [FloatingMenuItemView]()
      
        let menus = [
            ("Close".localized, UIImage(named: "more_menu_0")),
            ("Add / Withdraw".localized, UIImage(named: "more_menu_1")),
            ("Pending Request".localized, UIImage(named: "more_menu_2")),
            ("Pending Approval Payment Request".localized, UIImage(named: "more_menu_3")),
            ("Block User Lists".localized, UIImage(named: "more_menu_4")),
            ("Get OTP".localized, UIImage(named: "more_menu_6"))
        ]
        
        let action :RightSideSlideMenuItem.Action = { [weak self] (menuVC, menu) in
            
            menuVC.dismiss({ [weak self] in
                self?._menuButtonTapped(sender: menu)
            })
        }
        
        for (index, (title, img)) in menus.enumerated() {
            
            if let menuView = RightSideSlideMenuItem.newInstance() {
                
                menuView.tag = index
                menuView.set(title: title).set(image: img).onTap = action
                menuItems.append(menuView)
            }
        }
        
        return menuItems
    }
    
    fileprivate func closeMenuVC()
    {
        self._floatingButton.defaultClose()
    }
    
    private func _menuButtonTapped(sender :RightSideSlideMenuItem) {
        
        let index = sender.tag
        //println_debug("sender.title = \(sender.label.text ?? "", "menuView.tag = \(index)")")
        
        if (index > 0 && index < 5){
            self.navigationController?.setNavigationBarHidden(false, animated: false)
        }
        
        let storyboard = UIStoryboard(name: "PendingRequestSB", bundle: nil)
        switch index {
        case 0:
            self.closeMenuVC()
        case 1:
            let addWithdrawView = UIStoryboard(name: "AddWithdrawMain", bundle: nil).instantiateViewController(withIdentifier: "AddWithdrawView_ID")
            let aObjNav = UINavigationController(rootViewController: addWithdrawView)
            //aObjNav.isNavigationBarHidden = true
            self.present(aObjNav, animated: true, completion: nil)
            //self.navigationController?.pushViewController(addWithdrawView, animated: true)

        case 2:
            let pendingRequestVC = storyboard.instantiateViewController(withIdentifier: "pending_request")
            self.navigationController?.pushViewController(pendingRequestVC, animated: true)
            
        case 3:
            let pendingApprovalPaymentRequestVC = storyboard.instantiateViewController(withIdentifier: "pending_approval_request")
            self.navigationController?.pushViewController(pendingApprovalPaymentRequestVC, animated: true)
            
        case 4:
            let blockListVC = storyboard.instantiateViewController(withIdentifier: "block_user_list")
            self.navigationController?.pushViewController(blockListVC, animated: true)
            
        case 5:
            let storyboardSetting = UIStoryboard(name: "Setting", bundle: nil)
            let VC = storyboardSetting.instantiateViewController(withIdentifier: "SMGetOTPViewController")
            self.navigationController?.pushViewController(VC, animated: true)

        default:
            println_debug("default")
        }
    }
    
    //    @objc func moreButtonAction() {
    //        let rootMoreMenuVC    = UIStoryboard(name: "PendingRequestSB", bundle: nil).instantiateViewController(withIdentifier: "more_menu_navigation_vc")
    //        self.present(rootMoreMenuVC, animated: true, completion: nil)
    //    }
    
    //MARK: - Tabbar view customisation
    func configureTabBarView(){
        settings.style.buttonBarBackgroundColor = UIColor.white
        settings.style.buttonBarItemBackgroundColor = UIColor.white
        
        settings.style.selectedBarBackgroundColor = UIColor.black//kYellowColor
        settings.style.buttonBarItemFont = UIFont(name: appFont, size: 14.0)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        //        let textColor = UIColor.init(red: 0.203, green: 0.286, blue: 0.368, alpha: 1)
        settings.style.buttonBarItemTitleColor = .black
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        changeCurrentIndexProgressive = { [] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool,_ index:Int) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .black
            newCell?.label.textColor = kSelectedTextColor//.red
            oldCell?.contentView.backgroundColor = .white
            
            newCell?.contentView.backgroundColor =  .white//UIColor.init(red:141/255, green: 141/255, blue: 141/255, alpha: 0.15)
            switch index {
            case 0:
               self.setRightBarButtonItems()
            case 1:
                self.navigationItem.rightBarButtonItems = []
            default:
                print ("default")
            }
        }
    }
    
    override func viewControllersForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let storyBoard = UIStoryboard(name: "requestmoney", bundle: nil)
        let child_1 = storyBoard.instantiateViewController(withIdentifier: "scan_qr_code")
        let stroyBoard = UIStoryboard(name: "Main", bundle: nil)
        let myQRCode = stroyBoard.instantiateViewController(withIdentifier: String.init(describing: TBMyQRCode.self)) as? TBMyQRCode
        myQRCode?.view.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: 548.00)
        myQRCode?.title = "My QR Code".localized
        //let child_2 = storyBoard.instantiateViewController(withIdentifier: "my_qr_code")
        return [child_1, myQRCode!]
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "RequestMoneyHomeTapAction"), object: nil)
    }
    //MARK: - actions
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
