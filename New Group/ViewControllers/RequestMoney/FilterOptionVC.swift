//
//  FilterOptionVC.swift
//  OK
//
//  Created by palnar on 14/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
protocol FilterCustomNavigationDelegate {
    func filterOptionSelected(withGroup group: String , option : String)
}
class FilterOptionVC: UIViewController {
    //MARK: - Properties
    fileprivate var filterItems  = [FilterItemOption]()
    fileprivate var visiblefilterItems  = [Int]()
    
    //MARK-  Outlets
     @IBOutlet weak var filterTableView: UITableView!
    
    @IBOutlet weak var remainingView: UIView!
    var delegate :FilterCustomNavigationDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.init(red:0 , green: 0, blue: 0, alpha: 0.6)
        //view.isOpaque = false
        configureFilterTableView()
        dismissWhenTappedAround()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //Filter Item Lists Configuration
    private func configureFilterTableView(){
       
        filterTableView.register(UINib(nibName : "FilterOptionCell",bundle : nil), forCellReuseIdentifier: "filterOption")
        
        let filterItem1 = FilterItemOption.init(name: "Default".localized, isExapandable: true, isExpanded: false ,isVisible: true , key: "1")
        let filterItem2 = FilterItemOption.init(name: "Type".localized, isExapandable: true, isExpanded: false , isVisible: true, key: "2")
        let filterItem3 = FilterItemOption.init(name: "PAYTO".localized, isExapandable: false, isExpanded: false , isVisible: false, key: "3")
         let filterItem7 = FilterItemOption.init(name: "Categories".localized, isExapandable: true, isExpanded: false , isVisible: true ,  key: "4")
//        let filterItem4 = FilterItemOption.init(name: "Restaurants", isExapandable: false, isExpanded: false , isVisible: false ,  key: "5")
        let filterItem5 = FilterItemOption.init(name: "Food & Drink".localized, isExapandable: false, isExpanded: false , isVisible: false ,  key: "6")
//        let filterItem6 = FilterItemOption.init(name: "Personal", isExapandable: false, isExpanded: false , isVisible: false ,  key: "7")
        filterItems.append(filterItem1!)
         filterItems.append(filterItem2!)
         filterItems.append(filterItem3!)
         filterItems.append(filterItem7!)
//         filterItems.append(filterItem4!)
         filterItems.append(filterItem5!)
//         filterItems.append(filterItem6!)
       
        updateVisiblefilterItems()
        filterTableView.dataSource  = self
        filterTableView.delegate = self
        filterTableView.sectionHeaderHeight = 50
        
    }
    
    func dismissWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(dismissController))
        tap.cancelsTouchesInView = false
        self.remainingView.addGestureRecognizer(tap)
    }
    @objc func dismissController() {
        self.dismiss(animated: true, completion:nil)
    }
    
   

}

extension FilterOptionVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return visiblefilterItems.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let filtetItemModel = filterItems [visiblefilterItems[indexPath.row]]
        let cell = tableView.dequeueReusableCell(withIdentifier: "filterOption", for: indexPath as IndexPath) as! FilterOptionCellView
        cell.downArrow.isHidden =  true
        if let secondaryTitle = filtetItemModel.secondaryTitle {
            cell.name.font = UIFont(name: appFont, size: CGFloat(FontSize))
            cell.name.text = secondaryTitle
        }else{
            cell.name.font = UIFont(name: appFont, size: CGFloat(FontSize))
           cell.name.text =  filtetItemModel.name
        }
        cell.leftLabelConstraint.constant = 15
        if filtetItemModel.isExapandable && filtetItemModel.key != "1"{
            cell.downArrow.isHidden =  false
            let  imageDownArrow : UIImage = UIImage(named : "down_arrow_filter")!
            cell.downArrow.image = imageDownArrow
            if filtetItemModel.isExpanded {
            let angle =  CGFloat(Double.pi)
               
            let tr = CGAffineTransform.identity.rotated(by: angle )
                   cell.downArrow.transform = tr
            }
            cell.leftLabelConstraint.constant = 15
        }else if (filtetItemModel.key != "1"){
            cell.leftLabelConstraint.constant = 30
        }
        if filtetItemModel.isDividerVisible!{
            cell.dividerLine.isHidden = false
        }else{
           cell.dividerLine.isHidden = true
        }
        
       
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let filtetItemModel = filterItems [visiblefilterItems[indexPath.row]]
        
        let filterIndex = getIndexFromName(key: filtetItemModel.key)
        
        if(filtetItemModel.isExapandable == false ){
            filterItems [filterIndex].isVisible = false
            let fistParentIndex = firstParent(withChildIndex: filterIndex )
             let lastChildIndex = lastChild( withParentIndex : fistParentIndex )
            if fistParentIndex > -1 {
                let parentfilterItemModel = filterItems [fistParentIndex]
               for index in stride(from: lastChildIndex, through: fistParentIndex, by: -1) {
                   if index == fistParentIndex {
                       filterItems [index].isExpanded = false
                       filterItems[index].secondaryTitle = filtetItemModel.name
                        delegate?.filterOptionSelected(withGroup: parentfilterItemModel.name, option: filtetItemModel.name)
                       
                   }else {
                     filterItems [index].isVisible = false
                     }
                
                }
            }
        }else {
             let isToBeExpanded = !filtetItemModel.isExpanded
            let lastChildIndex = lastChild( withParentIndex : filterIndex )
            if lastChildIndex > -1 {
                for index in stride(from: lastChildIndex, through: filterIndex, by: -1) {
                    if index == filterIndex {
                        filterItems [index].isExpanded = isToBeExpanded
                        filterItems[index].secondaryTitle = nil
                    }else {
                        filterItems [index].isVisible = isToBeExpanded
                    }
                    
                }
             
            }
        }
        updateVisiblefilterItems()
        filterTableView.reloadSections(NSIndexSet(index: indexPath.section) as IndexSet, with: UITableView.RowAnimation.fade)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
            let divider = UIView(frame: CGRect(x: 0, y: (headerView.frame.size.height - CGFloat(1)), width: tableView.frame.size.width, height: 1))
        divider.backgroundColor = UIColor.gray
            label.font = UIFont(name: appFont, size: appFontSize)
            label.text = "Filter By".localized
            
            label.textColor = UIColor.black
            label.font = UIFont.systemFont(ofSize: 13)
            label.textAlignment = .center
            
            headerView.backgroundColor = UIColor.white
            headerView.addSubview(label)
            headerView.addSubview(divider)
            
            return headerView
    }
    
    private  func updateVisiblefilterItems(){
      var visibleRowCount : Int = 0
        visiblefilterItems.removeAll()
      for filterItem in filterItems {
        if filterItem.isVisible {
          visiblefilterItems.append(visibleRowCount)
           
          }
        visibleRowCount += 1
      }
    
    
    }
    
    private func getIndexFromName(key name:String ) -> (Int){
        var indexOfRow : Int = -1
        for filterItem in filterItems {
            indexOfRow += 1
            if filterItem.key == name {
               break
            }
        }
        
        return indexOfRow
    }
    
    private func firstParent(withChildIndex visibleIndex : Int) -> (Int){
     var firstParentIndex : Int = -1
        for index in stride(from: visibleIndex-1, through: 0, by: -1) {
            let filtetItemModel = filterItems [index]
            if(filtetItemModel.isExapandable){
                       firstParentIndex = index
                       break
            }
            
        }
        return firstParentIndex
    }
    
    private func lastChild(withParentIndex visibleIndex : Int) -> (Int){
        var lastChildIndex : Int = -1
        for index in stride(from: visibleIndex+1, to:filterItems.count , by: 1) {
            let filtetItemModel = filterItems [index] 
            if(filtetItemModel.isExapandable){
                break
            }else {
                lastChildIndex = index ;
            }
            
        }
        return lastChildIndex
    }
     
}
