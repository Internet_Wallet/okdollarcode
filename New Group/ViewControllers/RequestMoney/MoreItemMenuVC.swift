//
//  MoreItemMenuVC.swift
//  OK
//
//  Created by palnar on 18/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class MoreItemMenuVC: UIViewController {
     fileprivate var menuItems  = [CriteriaFilterItem]()

    @IBOutlet weak var moreMenuTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.helpSupportNavigationEnum = .Request_Money

        configureMoreMenuTableView()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    //Filter Item Lists Configuration
    private func configureMoreMenuTableView(){
        
        //self.navigationItem .setLeftBarButton(nil, animated: true)
        self.view.backgroundColor = UIColor.init(red: 152/255, green: 161/255, blue: 167/255, alpha: 0.8)
        moreMenuTableView.register(UINib(nibName : "PendingRequestMenuItemCell",bundle : nil), forCellReuseIdentifier: "pendingRequestMenuItem")
       
        let menuItem1 = CriteriaFilterItem.init(name: "Add / Withdraw".localized,photo: UIImage(named:"more_menu_1") )
        let menuItem2 = CriteriaFilterItem.init(name: "Pending Request".localized,photo: UIImage(named:"more_menu_2") )
        let menuItem3 = CriteriaFilterItem.init(name: "Pending Approval Payment Request".localized,photo: UIImage(named:"more_menu_3") )
        let menuItem4 = CriteriaFilterItem.init(name: "Block User Lists".localized,photo: UIImage(named:"more_menu_4") )
        let menuItem5 = CriteriaFilterItem.init(name: "Get OTP".localized,photo: UIImage(named:"more_menu_6") )
        menuItems.append(menuItem1!)
        menuItems.append(menuItem2!)
        menuItems.append(menuItem3!)
        menuItems.append(menuItem4!)
        menuItems.append(menuItem5!)
        
        moreMenuTableView.dataSource  = self
        moreMenuTableView.delegate = self
       
       moreMenuTableView.tableFooterView = UIView(frame: CGRect.zero)
        
    }
    
    
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
    //Table View Delegate
    extension MoreItemMenuVC : UITableViewDelegate,UITableViewDataSource {
    
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return menuItems.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let menuItemModel = menuItems[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "pendingRequestMenuItem", for: indexPath as IndexPath) as! PendingRequestMenuItemCellView
           cell.menuItem.text  = menuItemModel.name
           cell.rightIcon.image = menuItemModel.photo
            cell.layer.backgroundColor = UIColor.clear.cgColor
            cell.selectionStyle = .none
            return cell
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 55
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            if(indexPath.row > 0 && indexPath.row < 4){
            self.navigationController?.setNavigationBarHidden(false, animated: false)
            }
            switch indexPath.row {
                
            case 1:
                let pendingRequestVC    = self.storyboard?.instantiateViewController(withIdentifier: "pending_request")
                self.navigationController?.pushViewController(pendingRequestVC!, animated: true)
            case 2: let pendingApprovalPaymentRequestVC    = self.storyboard?.instantiateViewController(withIdentifier: "pending_approval_request")
            println_debug("pendingApprovalPaymentRequestVC = \(pendingApprovalPaymentRequestVC!)")
            self.navigationController?.pushViewController(pendingApprovalPaymentRequestVC!, animated: true)
            case 3:
                let blockListVC    = self.storyboard?.instantiateViewController(withIdentifier: "block_user_list")
                self.navigationController?.pushViewController(blockListVC!, animated: true)
                
            default:
                println_debug("default")
            }
        }
        
        
    }


