//
//  ScanQRCodeViewController.swift
//  OK
//
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import AVFoundation


class ScanQRCodeViewController: OKBaseController,IndicatorInfoProvider,
QRCodeReaderViewControllerDelegate, OKContactsPickerDelegate,OKPhotoPickerDelegate,ContactPickerDelegate {
    
    
    //MARK: - Properties
    
    lazy var _floatingButton: FloatingButton = {
        
        let floatingButton = FloatingButton()
        floatingButton.setImage(UIImage(named: "menu_white_payto.png"), for: UIControl.State.normal)
        if device.type == .iPhoneX || device.type == .iPhoneXR || device.type == .iPhoneXS || device.type == .iPhoneXSMax  || device.type == .iPhone11 || device.type == .iPhone11Pro || device.type == .iPhone11ProMax {
            floatingButton.frame = CGRect(x: self.view.frame.width - 42, y: 54.0, width: 30, height: 30)
        } else {
            floatingButton.frame = CGRect(x: self.view.frame.width - 42, y: 30.0, width: 30, height: 30)
        }
        //        floatingButton.contentEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
        floatingButton.addTarget(self, action: #selector(moreButtonAction(sender:)), for: .touchUpInside)
        
        return floatingButton
    }()

    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var myQRCodeLbl: UILabel! {
        didSet {
            myQRCodeLbl.text = myQRCodeLbl.text?.localized
            myQRCodeLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var headerLabel: UILabel! {
        didSet {
            headerLabel.text = headerLabel.text?.localized
            headerLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnScanQRFrmGallery: UIButton!
    
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var selectedContactLabelButton: UIButton!{
        didSet{
            selectedContactLabelButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    var _canAcceptQRCode = true
    //ScanQRCodeViewController
    @IBOutlet weak var previewView: QRCodeReaderView! {
        didSet {
            previewView.setupComponents(showCancelButton: false, showSwitchCameraButton: false, showTorchButton: false, showOverlayView: false, reader: reader)
        }
    }
    lazy var reader: QRCodeReader = QRCodeReader()
    var selectedContact: Contact?
    //MARK: - Actions
    @IBOutlet weak var myQRCodeBtn: UIButton! {
        didSet {
            myQRCodeBtn.setTitle(myQRCodeBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var myQRView: UIView! {
        didSet {
            self.myQRView.layer.cornerRadius = 20.0
            self.myQRView.layer.borderWidth = 0.5
            self.myQRView.layer.borderColor = UIColor.white.cgColor
        }
    }
    @IBOutlet weak var myQRImageView: UIImageView!
    
    //MARK: - Override methods
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.helpSupportNavigationEnum = .Request_Money

        customiseViewBehaviour()
        if reader.isRunning {
            reader.stopScanning()
        }
        _setUpScanner()
        selectedContactLabelButton.setTitle("Enter Mobile Number".localized, for: .normal)
        btnScanQRFrmGallery.setTitle(btnScanQRFrmGallery.titleLabel?.text?.localized, for: .normal)
        self.myQRImageView.image = self.myQRImageView.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        self.myQRImageView.tintColor = .white
        self.setRightBarButtonItems()
        NotificationCenter.default.addObserver(self, selector: #selector(hideNavigation), name: Notification.Name("HideQRCodeNavigation"), object: nil)
        //To Hide Flash image
         NotificationCenter.default.addObserver(self, selector: #selector(ScanQRCodeViewController.appWillResignActive), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc private func hideNavigation() {
        DispatchQueue.main.async {
            self.navigationController?.isNavigationBarHidden = true
        }
    }
    
    @objc func appWillResignActive() {
        flashButton.setImage(UIImage(named:"flash_new"), for: UIControl.State.normal)
    }
    
    private func setRightBarButtonItems() {
        self._floatingButton.tintColor = UIColor.white
        self.view.addSubview(self._floatingButton)
        let image = UIImage(named: "pt_close")?.withRenderingMode(.alwaysTemplate)
        self.closeBtn.setImage(image, for: .normal)
        self.closeBtn.tintColor = .white
    }
    
    @objc func moreButtonAction(sender :FloatingButton) {
        if device.type == .iPhoneX || device.type == .iPhoneXR || device.type == .iPhoneXS || device.type == .iPhoneXSMax || device.type == .iPhone11 || device.type == .iPhone11Pro || device.type == .iPhone11ProMax {
            let settings = FloatingMenu.Settings(position: .topRight(88.0, 12.0), padingFromFloatingButton: 84, menuSpacing: 19, floatingButton: sender)
            FloatingMenu(settings: settings).set(menuItems: _getMenuItems()).show(On: self)
        } else {
            let settings = FloatingMenu.Settings(position: .topRight(30.0, 12.0), padingFromFloatingButton: 84, menuSpacing: 19, floatingButton: sender)
            FloatingMenu(settings: settings).set(menuItems: _getMenuItems()).show(On: self)
        }
    }
    
    private func _getMenuItems() ->[FloatingMenuItemView] {
        
        var menuItems = [FloatingMenuItemView]()
        
        let menus = [
            ("Close".localized, UIImage(named: "more_menu_0")),
            ("Add / Withdraw".localized, UIImage(named: "more_menu_1")),
            ("Pending Request".localized, UIImage(named: "more_menu_2")),
            ("Pending Approval Payment Request".localized, UIImage(named: "more_menu_3")),
            ("Block User Lists".localized, UIImage(named: "more_menu_4")),
            ("Get OTP".localized, UIImage(named: "more_menu_6"))
        ]
        
        let action :RightSideSlideMenuItem.Action = { [weak self] (menuVC, menu) in
            
            menuVC.dismiss({ [weak self] in
                self?._menuButtonTapped(sender: menu)
            })
        }
        
        for (index, (title, img)) in menus.enumerated() {
            
            if let menuView = RightSideSlideMenuItem.newInstance() {
                
                menuView.tag = index
                menuView.set(title: title).set(image: img).onTap = action
                menuItems.append(menuView)
            }
        }
        
        return menuItems
    }
    
    fileprivate func closeMenuVC()
    {
        self._floatingButton.defaultClose()
    }
    

    
    private func _menuButtonTapped(sender :RightSideSlideMenuItem) {
        
        let index = sender.tag
        println_debug("sender.title = \(sender.label.text ?? "", "menuView.tag = \(index)")")
        
        if (index > 0 && index < 5){
            self.navigationController?.setNavigationBarHidden(false, animated: false)
        }
        
        switch index {
        case 0:
            self.closeMenuVC()
            
        case 1:
            if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "showAddWithDrawVC", delegate: self)
            } else {
                self.showAddWithDrawVC()
            }
            
        case 2:
            if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "showPendingReqVC", delegate: self)
            } else {
                self.showPendingReqVC()
            }
        case 3:
            if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "showPendingAppReqVC", delegate: self)
            } else {
                self.showPendingAppReqVC()
            }
        case 4:
            if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "showBlockListVC", delegate: self)
            } else {
                self.showBlockListVC()
            }
        case 5:
            if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "showGetOtpVC", delegate: self)
            } else {
                self.showGetOtpVC()
            }
            
        default:
            println_debug("default")
        }
    }
    
    private func showPendingReqVC() {
        let storyboard = UIStoryboard(name: "PendingRequestSB", bundle: nil)
        let pendingRequestVC = storyboard.instantiateViewController(withIdentifier: "pending_request")
        userDef.set("FromRequestMoney", forKey: "RequestMoney")
        userDef.synchronize()
        self.navigationController?.pushViewController(pendingRequestVC, animated: true)
    }
    
    private func showPendingAppReqVC() {
        let storyboard = UIStoryboard(name: "PendingRequestSB", bundle: nil)
        let pendingApprovalPaymentRequestVC = storyboard.instantiateViewController(withIdentifier: "pending_approval_request")
        userDef.set("FromRequestMoneyApproval", forKey: "RequestMoneyApproval")
        userDef.synchronize()
        self.navigationController?.pushViewController(pendingApprovalPaymentRequestVC, animated: true)
    }
    
    private func showBlockListVC() {
        let storyboard = UIStoryboard(name: "PendingRequestSB", bundle: nil)
        let blockListVC = storyboard.instantiateViewController(withIdentifier: "block_user_list")
        self.navigationController?.pushViewController(blockListVC, animated: true)
    }
    
    private func showAddWithDrawVC() {
        
        let addMoneyStoryboard = UIStoryboard.init(name: "AddWithdraw", bundle: nil)
        guard let firstView_addMoneyVC = addMoneyStoryboard.instantiateViewController(withIdentifier: "AddWithdrawMoneyVC") as? AddWithdrawMoneyVC else { return }
        firstView_addMoneyVC.screenFrom.0 = "DashBoard"
        firstView_addMoneyVC.screenFrom.1 = "Cash In"
        self.navigationController?.pushViewController(firstView_addMoneyVC, animated: true)
        
        
//        let addWithdrawView = UIStoryboard(name: "AddWithdrawMain", bundle: nil).instantiateViewController(withIdentifier: "AddWithdrawView_ID")
//        let aObjNav = UINavigationController(rootViewController: addWithdrawView)
//        //aObjNav.isNavigationBarHidden = true
//        self.present(aObjNav, animated: true, completion: nil)
//        //self.navigationController?.pushViewController(addWithdrawView, animated: true)
    }
    
    private func showGetOtpVC() {
        let storyboardSetting = UIStoryboard(name: "Setting", bundle: nil)
        let VC = storyboardSetting.instantiateViewController(withIdentifier: "SMGetOTPViewController")
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        flashButton.setImage(UIImage(named:"flash_new"), for: UIControl.State.normal)
        
         selectedContactLabelButton.setTitle("Enter Mobile Number".localized, for: .normal)
        
        reader.startScanning()
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        reader.stopScanning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func closeBtnAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func scanQRCodeAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        guard let myQRCode = storyBoard.instantiateViewController(withIdentifier: String.init(describing:TBMyQRCode.self)) as? TBMyQRCode else { return }
        
        myQRCode.navigation = self.navigationController
        //            myQRCode?.view.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: 548.00)
        myQRCode.title = "My QR Code".localized
        myQRCode.itemNavigation = self.navigationItem
        myQRCode.fromTabBar = true
        self.navigationController?.pushViewController(myQRCode, animated: true)
    }
    
    private func _setUpScanner() {
        
        guard checkScanPermissions(), !reader.isRunning else { return }
        
        reader.didFindCode = { [weak self] result in
            
           // progressViewObj.showProgressView()
            if (self?._canAcceptQRCode ?? false) == false {
                
               // self?.reader.startScanning()
                return
            }
            
            if let scannedObject = self?.scannedString(str: result.value) {
                
                if scannedObject.number == UserModel.shared.mobileNo {
                    
                    DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: {
                        self?._show(title: "", errorMeaasge: "Not able to send request to this QRCode. It seems this is your device. Try another QRCode".localized, action: { [weak self] in
                            
                            self?.reader.startScanning()
                        })
                    }) 
                } else {
                    //self?.toggleFlash()
                    self?.navigateToRequestMoneyFieldBaseController(witInfo: scannedObject)
                }
            }
           // progressViewObj.removeProgressView()
        }
    }
    
    func scannedString(str: String ) -> ScanObject? {
        print("scannedString----\(str)")
        var scanObject : ScanObject?
        _ = AESCrypt.decrypt(str.components(separatedBy: "---")[0], password: "m2n1shlko@$p##d")
        var sttt: String = ""
        println_debug(AESCrypt.decrypt(str, password: "m2n1shlko@$p##d"))
        if let stringQR = AESCrypt.decrypt(str, password: "m2n1shlko@$p##d") {
            sttt = stringQR } else
        {
            //self.showToast(message: "Invalid QR Code")
            
                DispatchQueue.main.async {
                    alertViewObj.wrapAlert(title: nil, body: "Invalid QR Code".localized, img: #imageLiteral(resourceName: "alert-icon"))
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        self.reader.startScanning()
                    })
                    alertViewObj.showAlert(controller: self)
                }
            
        }
        /*
        if sttt != "" {
            if sttt.contains("α") {
                if sttt.contains("-1") {
                    sttt = "" }
                else {
                    
                }
                if sttt.contains("`") {
                    
                } else {
                    // sttt=@"";
                    
                }
                println_debug("contains yes") }
            else
            { sttt = "" }
            
        }*/
        if(sttt.count>0){
            // var cellId: String = ""
            var businessName: String = ""
            var businessMobile: String = ""
            var myArray: [Any] = sttt.components(separatedBy: "#")
            _ = myArray[0] as! String
            var seconArray: [Any] = (myArray[1] as AnyObject).components(separatedBy: "-")
            businessName = seconArray[0] as! String
            var thirdArray: [Any] = (seconArray[1] as AnyObject).components(separatedBy: "@")
            businessMobile = thirdArray[0] as! String
            var fourthArray: [Any] = (thirdArray[1] as AnyObject).components(separatedBy: "&")
            let amt : String = fourthArray[0] as! String
            var fifthArray : [Any] = (fourthArray[1] as AnyObject).components(separatedBy: "`,,")
            var remark : String?
            if fifthArray.count > 1 {
                remark = fifthArray[1] as? String
            }
            
            var amount : String?
            if    amt.isEqual("(null)") == false {
                amount = amt
            }
            scanObject = ScanObject.init(name: businessName, number: businessMobile, amount: amount, remark: remark)
            print("scanObject----\(scanObject)")

        } else {
         
            return nil
            
        }
        return scanObject
    }
    
    fileprivate func _show(title :String, errorMeaasge :String, iconImageName :String = "AppIcon", action: (() -> Void)? = nil) {
        
        alertViewObj.wrapAlert(title: title, body: errorMeaasge, img: UIImage(named : iconImageName))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
            
            action?()
        })
        alertViewObj.showAlert(controller: self)
    }
    
    //MARK: - IndiactorInfoProvider
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title : "Scan QR Code".localized)
    }
    
    // MARK: - QRCodeReader Delegate Methods
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        
        print("didScanResult-------\(result)")
        dismiss(animated: true) { [weak self] in
            let alert = UIAlertController(
                title: "QRCodeReader".localized,
                message: String (format:"%@ (of type %@)", result.value, result.metadataType),
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK".localized, style: .cancel, handler: nil))
            
            self?.present(alert, animated: true, completion: nil)
        }
    }
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        println_debug("Switching capturing to: \(newCaptureDevice.device.localizedName)")
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Custom methods
    func customiseViewBehaviour(){
        btnScanQRFrmGallery.centerVertically()
        
        
        
    }
    
    func toggleFlash() {
        let device = AVCaptureDevice.default(for: AVMediaType.video)
        
        if (device != nil) {
            if (device!.hasTorch) {
                do {
                    try device!.lockForConfiguration()
                    if (device!.torchMode == AVCaptureDevice.TorchMode.on) {
                        flashButton.setImage(UIImage(named:"flash_new"), for: UIControl.State.normal)
                        device!.torchMode = AVCaptureDevice.TorchMode.off
                    } else {
                        do {
                            flashButton.setImage(UIImage(named:"act_flash_new"), for: UIControl.State.normal)
                            try device!.setTorchModeOn(level: 1.0)
                        } catch {
                            
                            println_debug(error)
                        }
                    }
                    
                    device!.unlockForConfiguration()
                } catch {
                    
                    println_debug(error)
                }
            }
        }
    }
    
    private func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error".localized, message: "This app is not authorized to use Back Camera.".localized, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Setting".localized, style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:])
                                    , completionHandler: nil)
                            } else {
                                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
                            }
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil))
            default:
                alert = UIAlertController(title: "Error".localized, message: "Reader not supported by the current device".localized, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK".localized, style: .cancel, handler: nil))
            }
            
            present(alert, animated: true, completion: nil)
            
            return false
        }
    }
    
    //MARK:  - Actions
    @IBAction func mobileNumberButtonTapped(_ sender: Any) {
        ///
        progressViewObj.showProgressView()
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.01) {
            
            self.navigateToRequestMoneyFieldBaseController(witInfo: nil)
            progressViewObj.removeProgressView()
        }
    }
    
    
    @IBAction func contactButtonTapped(_ sender: Any) {
        
//        let contactPicker = OKContactsPicker(delegate: nil, multiSelection: false, showIndexBar: false, subtitleCellType: SubtitleCellValue.email)
//        let navigationController = UINavigationController(rootViewController: contactPicker)
        
        let nav = UitilityClass.openContact(multiSelection: false, self as? ContactPickerDelegate, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
       
        
//        contactPicker.didSelect = { [weak self] (picker, contact) in
//            println_debug("contact")
//
//            guard let phoneNumber = contact.phoneNumbers.first?.phoneNumber else {
//
//                self?._canAcceptQRCode = false
//
//                DispatchQueue.main.asyncAfter(deadline: .now()+0.03) {
//
//                self?.reader.stopScanning()
//
//                    self?._show(title: "Error".localized, errorMeaasge: "Contact number not found!".localized, iconImageName: "error", action: { [weak self] in
//
//                        self?._canAcceptQRCode = true
//                        self?.reader.startScanning()
//                    })
//                }
//                return
//            }
//
//            var phone = phoneNumber
//            phone = phone.replacingOccurrences(of: "%20", with: "")
//            phone = phone.replacingOccurrences(of: "%C2%A0", with: "")
//
//            let obj = ScanObject(name: "", number: phone, amount: nil, remark: nil)
//            self?.navigateToRequestMoneyFieldBaseController(witInfo: obj)
//
//        }
//
//        contactPicker.didCancel = { (picker, error) in
//
//            println_debug("error")
//        }
//
//        contactPicker.didFailed = { (picker, error) in
//
//            println_debug("error")
//        }
        
      //  self.present(navigationController, animated: true, completion: nil)
        
         self.present(nav, animated: true, completion: nil)
    }
    
    
    @IBAction func scanFromGalleryTapped(_ sender: Any) {
        scanFromGallery()
//        
//                let story = UIStoryboard.init(name: "PayTo", bundle: nil)
//                let vc = story.instantiateViewController(withIdentifier: "QRPhotoScanner") as? QRPhotoScanner
//                vc?.tbScanQRController = self
//                vc?.delegate = self
//                vc?.isFromTransferTo = isFromTransferTo ?? false
//                if let navigation = self.navigationController {
//                    navigation.pushViewController(vc!, animated: true)
//                } else {
//                    self.navigation?.pushViewController(vc!, animated: true)
//                }
    }
    
    @IBAction func toggleFlashTapped(_ sender: Any) {
        toggleFlash()
    }
    
    //Navigation To Request Money Field COntroller
    func navigateToRequestMoneyFieldBaseController(witInfo qrCodeModel : ScanObject?) {
        
        if let VC = self.storyboard?.instantiateViewController(withIdentifier: "requestmoney_field_base_controller") as? RequestMoneyFieldBaseControllerViewController {
            //print("qrCodeModel----\(qrCodeModel)")
             VC.configParam = qrCodeModel
            self.navigationController?.pushViewController(VC, animated: true)

//        let nav = UitilityClass.openContact(multiSelection: false, self as? ContactPickerDelegate)
//
//            self.present(nav, animated: true, completion: nil)
            
        }
    }
    
    
    //MARK:- Addressbook implmentaion
    func openConatctAddressBook() {
        /* let contactPicker = CNContactPickerViewController()
         contactPicker.delegate = self
         contactPicker.displayedPropertyKeys =
         [CNContactPhoneNumbersKey]
         self.present(contactPicker, animated: true, completion: nil)*/
        
        let contactPickerScene = OKContactsPicker(delegate: self, multiSelection: false,showIndexBar:false,subtitleCellType: SubtitleCellValue.phoneNumber)
        let navigationController = UINavigationController(rootViewController: contactPickerScene)
        self.present(navigationController, animated: true, completion: nil)
        
        
      
        
    }
    
//    func contactPicker(_: OKContactsPicker, didContactFetchFailed error: NSError)
//    {
//        println_debug("Failed with error \(error.description)")
//    }
//
//    func contactPicker(_: OKContactsPicker, didSelectContact contact: Contact) {
//
//        guard let number = contact.phoneNumbers.first?.phoneNumber else { return }
//
//        selectedContactLabelButton.setTitle(number, for: .normal)
//
//        self.selectedContact = contact
//        println_debug("Contact \(contact.displayName) has been selected")
//    }
//
//    func contactPicker(_: OKContactsPicker, didCancel error: NSError)
//    {
//        println_debug("User canceled the selection");
//
//    }
//
//    func contactPicker(_: OKContactsPicker, didSelectMultipleContacts contacts: [Contact]) {
//        println_debug("The following contacts are selected")
//        for contact in contacts {
//            println_debug("\(contact.displayName)")
//        }
//    }
    
   //MARK:-  NewContact Picker
    
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]) {
        for contact in contacts {
            decodeContact(contact: contact, isMulti: true)
        }
    }
    
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
     //   decodeContact(contact: contact, isMulti: false)
        
        guard let number = contact.phoneNumbers.first?.phoneNumber else { return }
        
                selectedContactLabelButton.setTitle(number, for: .normal)
        
//                contactPicker.didSelect = { [weak self] (picker, contact) in
                    println_debug("contact")
        
                    guard let phoneNumber = contact.phoneNumbers.first?.phoneNumber else {
        
                        self._canAcceptQRCode = false
        
                        DispatchQueue.main.asyncAfter(deadline: .now()+0.03) {
        
                            self.reader.stopScanning()
        
                            self._show(title: "Error".localized, errorMeaasge: "Contact number not found!".localized, iconImageName: "error", action: { [weak self] in
        
                                self?._canAcceptQRCode = true
                                self?.reader.startScanning()
                            })
                        }
                        return
                    }
        
                    var phone = phoneNumber
                    phone = phone.replacingOccurrences(of: "%20", with: "")
                    phone = phone.replacingOccurrences(of: "%C2%A0", with: "")
        
                    let obj = ScanObject(name: "", number: phone, amount: nil, remark: nil)
                    self.navigateToRequestMoneyFieldBaseController(witInfo: obj)
        
             //   }
        
        
                //self.selectedContact = contact
        println_debug("Contact \(String(describing: contact.displayName)) has been selected")
    }
    
    func decodeContact(contact: ContactPicker, isMulti: Bool) {
        
        //println_debug(intContactSelectionRow)
//

    
        
    }
    
    //MARK:-  QRCode scanning from gallery
    
    func scanFromGallery(){
        // let galleryPickerScene = QRPhotoScanner(delegate: self)
        let galleryPickerScene = OKPhotoPickerWithQRScanner.loadFromStoryboard()
        galleryPickerScene?.imgPickerDelegate = self
        //self.show(galleryPickerScene!, sender: nil)
        
        //  let navigationController = UINavigationController(rootViewController:  galleryPickerScene!)
        //self.present(galleryPickerScene!, animated: true, completion: nil)
        self.navigationController?.pushViewController(galleryPickerScene!, animated: true)
    }
    
    func imagePicker(pickedImage image: UIImage?, filteredImage: UIImage?) {
        
    }
    
    func selectedWithQrCode(qrCode code: ScanObject?) {
    
        
        UIView.animate(withDuration: 1.0, delay: 0.5,
                       usingSpringWithDamping: 0.3,
                       initialSpringVelocity: 0.5,
                       options: [], animations: {
                        self.navigationController?.popToRootViewController(animated: false)
        }, completion:{ _ in
            self.toggleFlash()
             self.navigateToRequestMoneyFieldBaseController(witInfo: code)
        })
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("HideQRCodeNavigation"), object: nil)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}


extension ScanQRCodeViewController: BioMetricLoginDelegate {
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful {
            if screen == "showAddWithDrawVC" {
                self.showAddWithDrawVC()
            } else if screen == "showPendingReqVC" {
                self.showPendingReqVC()
            } else if screen == "showPendingAppReqVC" {
                self.showPendingAppReqVC()
            } else if screen == "showBlockListVC" {
                self.showBlockListVC()
            } else if screen == "showGetOtpVC" {
                self.showGetOtpVC()
            }
        } else {
            DispatchQueue.main.async {
                self.navigationController?.isNavigationBarHidden = true
            }
        }
    }
}
