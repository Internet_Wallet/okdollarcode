//
//  AutoKeyBoardManager.swift
//  OK
//
//  Created by palnar on 14/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import IQKeyboardManagerSwift

public final class AutoKeyBoardManager {
    
    public static var enable :Bool {
        get {
            return IQKeyboardManager.sharedManager().enable
        }
        set {
            IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = newValue
            IQKeyboardManager.sharedManager().enable = newValue
            IQKeyboardManager.sharedManager().enableAutoToolbar = false
        }
    }
}
