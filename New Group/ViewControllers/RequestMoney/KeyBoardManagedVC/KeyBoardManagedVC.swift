//
//  KeyBoardManagedVC.swift
//  OK
//
//  Created by palnar on 13/01/18.
//  Copyright © 2018 T T Marshel Daniel. All rights reserved.
//

import UIKit

open class KeyBoardManagedVC: UIViewController {
    
    public var enableAutoKeyPadManage :Bool {
        get {
            return AutoKeyBoardManager.enable
        }
        set {
            if newValue == true {
                
                if AutoKeyBoardManager.enable == false {
                    AutoKeyBoardManager.enable = true
                }
            } else {
                if AutoKeyBoardManager.enable == true {
                    AutoKeyBoardManager.enable = false
                }
            }
        }
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        self.enableAutoKeyPadManage = true
    }
}
