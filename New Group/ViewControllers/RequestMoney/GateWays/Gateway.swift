//
//  Gateway.swift
//  OK
//
//  Created by palnar on 18/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

public protocol GateWayInputParams { }

public protocol GateWayError :Error {
    
    var errorMessage :String  { get }
}

public protocol GateWayResponse {
    
    var error :GateWayError? { get }
    func result()  throws -> Any
}

public protocol GateWay {
    
    func submit(_ params: GateWayInputParams, response: @escaping (GateWayResponse) -> ()) throws
}

extension GateWay {
    
    public func submit(_ params: GateWayInputParams, response: @escaping (GateWayResponse) -> ()) throws  {
        
        throw GateWayNotImplementedError.notImplemented
    }
}

enum GateWayNotImplementedError: GateWayError {
    
    case notImplemented
    
    var errorCode: Int {
        switch self {
        case .notImplemented: return 1
        }
    }
    
    var errorMessage: String {
        return "GateWayNotImplemented"
    }
}

