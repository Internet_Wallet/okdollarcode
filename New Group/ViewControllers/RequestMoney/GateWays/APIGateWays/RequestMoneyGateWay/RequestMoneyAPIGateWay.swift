//
//  RequestMoneyAPIGateWay.swift
//  OK
//
//  Created by palnar on 22/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

public class RequestMoneyAPIGateWay {
    
    public let httpClient :HTTPClientInterface
    
    public lazy var url: URL = {
        
        let url = getUrl(urlStr: Url.sendRequestForRequestMoney, serverType: .serverApp)
        return url
    }()
    
    init(httpClient :HTTPClientInterface) {
        self.httpClient = httpClient
    }
    
    func submit(_ request: InputParams, _ response: @escaping (Resposne) -> ()) {
         DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
        var params = request.serialized()
        params["AttachedImages"] = []
        
        println_debug("url = \(url)")
         println_debug("params = \(params)")
        
        httpClient.POST(ToURL: url, params: request.serialized()) { (data, res, er) in
            
            if let er = er {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                }
                println_debug(er.localizedDescription)
                response(.error(self, ResposneError.error("")))
                return
            }
            
            guard let data = data else {
                
                self._invokeResponse(callBack: response, result: .error(self, ResposneError.error("Failed")))
                return
            }
            
            println_debug(toString(data: data) as Any)
            
            guard let httpResponse = res as? HTTPURLResponse else {
                
                self._invokeResponse(callBack: response, result: .error(self, ResposneError.error("Failed")))
                return
            }
            
            
            let statusCode = httpResponse.statusCode
            guard statusCode == 200 else {
                
                self._invokeResponse(callBack: response, result: .error(self, ResposneError.error("Failed")))
                return
            }
            
            guard let dic = toDictionary(data: data) else {
                
                self._invokeResponse(callBack: response, result: .error(self, ResposneError.error("Failed")))
                return
            }
            
            guard let code = dic["Code"] as? Int, let msg = dic["Msg"] as? String else {
                
                self._invokeResponse(callBack: response, result: .error(self, ResposneError.error("Failed")))
                return
            }
            
            if code == 200 && msg == "Sucess" {
                
                let d = dic["Data"] as? String ?? ""
                self._invokeResponse(callBack: response, result: .data(self, code, d, msg))
            } else {
                
                self._invokeResponse(callBack: response, result: .error(self, ResposneError.error(msg)))
            }
        }
    }
    
    private func _invokeResponse(callBack : @escaping (Resposne) -> (), result: Resposne) {
        
        DispatchQueue.main.async {
            callBack(result)
        }
    }
}

extension RequestMoneyAPIGateWay {
    
    struct InputParams :GateWayInputParams {
        
        var mobileNumber: String
        var simId: String
        var msid: String
        var ostype: Int
        var otp: String
        var source: String
        var destination: String
        var amount: String
        var date: String
        var remarks: String
        var localTransactionType: String
        var transactionType: String
        var isSchduled: Bool
        var schduledType: RequestMoneySchduledType
        var schduledTime: String
        var requestModuleType: RequestMoneyModuleType
        var isMynumber: RequestMoneyNumberType
        var attachedImages: [UIImage]
        var attachedImagesAwsUrl : [String]
        
        func serialized() -> Dictionary<String, Any> {
            
            return [
                "MobileNumber" :mobileNumber,
                "SimId" :simId,
                "Msid" :msid,
                "Ostype" :ostype,
                "Otp" :otp,
                
                "Source" : (isMynumber == RequestMoneyNumberType.myNumber) ? source : destination,
                "Destination" :(isMynumber == RequestMoneyNumberType.myNumber) ? destination : source,
                "Amount" :amount,
                "Date" :date,
                "Remarks" :remarks,
                
                "LocalTransactionType" :localTransactionType,
                "TransactionType" :transactionType,
                "IsSchduled" :isSchduled,
                "SchduledType" :schduledType.value,
                "SchduledTime" :schduledTime,
                "RequestModuleType" :requestModuleType.value,
                "IsMynumber" :isMynumber.value,
                "AttachedImages" : [],
                "AttachedImagesAwsUrl" : attachedImagesAwsUrl
            ]
        }
        
        
        private func _base64Attachements() ->[String] {
            
            var attachements = [String]()
            
            for image in self.attachedImages {
                
                if let string = toBase64EncodedString(image: image) {
                    attachements.append(string)
                }
            }
            return attachements
        } 
    }
    
    
    enum Resposne {
        
        case error(RequestMoneyAPIGateWay, GateWayError)
        case data(RequestMoneyAPIGateWay, Int,  String, String)
        
        var gateWay :RequestMoneyAPIGateWay {
            
            switch self {
            case .error(let gateWay, _): return gateWay
            case .data(let gateWay, _, _, _): return gateWay
            }
        }
        
        func result() throws -> (code :Int, data :String, msg :String) {
            
            switch self {
            case .error(_, let er): throw er
            case .data( _, let code, let data, let msg): return (code, data, msg)
            }
        }
    }
    
    enum ResposneError :GateWayError {
        
        case error(String)
         
        var errorMessage: String {
            
            switch self {
            case .error(let er): return er;
            }
        }
    }
}

enum RequestMoneyModuleType :Int {
    
    case requestMoney = 0
    case billSplitter = 1
    
    var value :Int {
        return self.rawValue
    }
}


enum RequestMoneySchduledType :Int {
    

    case requestNow = -2
    case rechargeNow = -1
    case normal =  0
    case daily = 1
    case weekly = 2
    case monthly = 3
    case quaterly = 4
    case halfyearly = 5
    case yearly = 6
    
    var value :Int {
        return self.rawValue
    }
    
    var intValue :Int {
        return self.rawValue
    }
    
    var stringValue :String {
        
        switch self {
        case .requestNow:
            return "RequestNow"
        case .rechargeNow:
            return "TOPUP"
        case .normal:
            return "Normal"
        case .daily:
            return "Daily"
        case .weekly:
            return "Weekly"
        case .monthly:
            return "Monthly"
        case .quaterly:
            return "Quaterly"
        case .halfyearly:
            return "Half Yearly"
        case .yearly:
            return "Yearly"
        }
    }
    
   
    
  /*  var sheduledOrNormal :String {
        
        switch self {
        case .requestNow:
            return "OK $ Money"//Gauri July24
        case .rechargeNow:
            return "Recharge"
        case .normal:
            return "Normal"
        case .daily, .weekly, .monthly, .quaterly, .halfyearly, .yearly :
            return "Scheduled"
        }
        
    } */
    
    var sheduledOrNormal :String {
        
        switch self {
        case .requestNow:
            print("sheduledOrNormal OK $ Money----\(self)")

            return "OK $ Money"//Gauri July24
         case .rechargeNow:
            print("sheduledOrNormal Recharge----\(self)")

            return "Recharge"
        case .normal:
            print("sheduledOrNormal Normal----\(self)")

            return "Normal"
//        case .daily, .weekly, .monthly, .quaterly, .halfyearly, .yearly :
//            print("sheduledOrNormal Scheduled----\(self)")
//
//            return "Scheduled"
        case .daily:
            return "Daily"
        case .weekly:
        return "Weekly"
            case .monthly:
            return "Monthly"
            case .quaterly:
            return "Quaterly"
            case .halfyearly:
            return "Half-Yearly"
            case .yearly:
            return "Yearly"
        }
        

    }
    
    var iSheduled :Bool {
        
        switch self {
        case .requestNow,.rechargeNow, .normal : return false
        case .daily, .weekly, .monthly, .quaterly, .halfyearly, .yearly : return true
        }
    }
}




enum RequestMoneyNewSchduledType :Int {
    

   // case requestNow = -5
   // case rechargeNow = -6
    case normal =  -2
    case normalone =  -1
    case normaltwo =  0
    case daily = 1
    case weekly = 2
    case monthly = 3
    case quaterly = 4
    case halfyearly = 5
    case yearly = 6
    
    var value :Int {
        return self.rawValue
    }
    
    var intValue :Int {
        return self.rawValue
    }
    
    var stringValue :String {
        
        switch self {
//        case .requestNow:
//            return "RequestNow"
//        case .rechargeNow:
//            return "TOPUP"
        case .normal:
            return "Normal"
        case .normalone:
            return "Normal"
        case .normaltwo:
            return "Normal"
        case .daily:
            return "Daily"
        case .weekly:
            return "Weekly"
        case .monthly:
            return "Monthly"
        case .quaterly:
            return "Quaterly"
        case .halfyearly:
            return "Half Yearly"
        case .yearly:
            return "Yearly"
        }
    }
    
   
    
  /*  var sheduledOrNormal :String {
        
        switch self {
        case .requestNow:
            return "OK $ Money"//Gauri July24
        case .rechargeNow:
            return "Recharge"
        case .normal:
            return "Normal"
        case .daily, .weekly, .monthly, .quaterly, .halfyearly, .yearly :
            return "Scheduled"
        }
        
    } */
    
    var NewsheduledOrNormal :String {
        
        switch self {
//        case .requestNow:
//            print("sheduledOrNormal OK $ Money----\(self)")
//
//            return "OK $ Money"//Gauri July24
//         case .rechargeNow:
//            print("sheduledOrNormal Recharge----\(self)")
//
//            return "Recharge"
        case .normal, .normalone ,.normaltwo:
            print("sheduledOrNormal Normal----\(self)")

            return "Normal"
        case .daily, .weekly, .monthly, .quaterly, .halfyearly, .yearly :
            print("sheduledOrNormal Scheduled----\(self)")

            return "Scheduled"
        }
        

    } 
    
    var iSheduled :Bool {
        
        switch self {
       // case .requestNow,.rechargeNow, .normal , .normalone, .normaltwo: return false
        case  .normal , .normalone, .normaltwo: return false
        case .daily, .weekly, .monthly, .quaterly, .halfyearly, .yearly : return true
        }
    }
}



enum RequestMoneyRecharge :Int {
    

    case requestNow = -5
    case rechargeNow = -6
    
    
    var value :Int {
        return self.rawValue
    }
    
    var intValue :Int {
        return self.rawValue
    }
    
    var stringValue :String {
        
        switch self {
        case .requestNow:
            return "RequestNow"
        case .rechargeNow:
            return "TOPUP"
       
        }
    }
    
   
    
  /*  var sheduledOrNormal :String {
        
        switch self {
        case .requestNow:
            return "OK $ Money"//Gauri July24
        case .rechargeNow:
            return "Recharge"
        case .normal:
            return "Normal"
        case .daily, .weekly, .monthly, .quaterly, .halfyearly, .yearly :
            return "Scheduled"
        }
        
    } */
    
    var rechargeOrNormal :String {
        
        switch self {
        case .requestNow:
            print("sheduledOrNormal OK $ Money----\(self)")

            return "OK $ Money"//Gauri July24
         case .rechargeNow:
            print("sheduledOrNormal Recharge----\(self)")

            return "Recharge"
       
        }
        

    }
    
    
}



enum RequestMoneyNumberType {
    
    case myNumber
    case otherNumber
    
    var value :Bool {
        
        switch self {
        case .myNumber: return true
        case .otherNumber: return false
        }
    }
}


