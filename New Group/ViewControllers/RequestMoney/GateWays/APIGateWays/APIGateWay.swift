//
//  APIGateWay.swift
//  OK
//
//  Created by palnar on 18/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation



public enum APIGateWayError: GateWayError {
    
    case parameterTypeNotMatched
    
    public var errorCode: Int {
        
        switch self {
        case .parameterTypeNotMatched: return 1
        }
    }
    
    public var errorMessage: String {
        return "parameterTypeNotMatched"
    }
}

open class APIGateWayBase :GateWay {
    
    public private(set) var _domain :String = ""
    public private(set) var _responseQueue: DispatchQueue?
    
    public init(domain: String, responseQueue: DispatchQueue?) {
        
        self._domain = domain
        self._responseQueue = responseQueue
    }
    
    public func submit(_ params: GateWayInputParams, response: @escaping (GateWayResponse) -> ()) throws {
        
        throw GateWayNotImplementedError.notImplemented
    }
}

public func toString(data: Data) -> String? {
    
    return String(data: data, encoding: .utf8)
}

public func toDictionary(any : Any?) -> [String: Any]? {
    
    if any == nil { return nil }
    
    if let obj = any as? Data {
        
        return toDictionary(data: obj)
    } else if let obj = any as? String {
        
        return toDictionary(text: obj)
    } else if let obj = any as? [String: Any] {
        
        return obj
    }
    
     return nil
}

public func toDictionary(data : Data) -> [String: Any]? {
    
    do {
        let dictonary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String :Any]
        
        return dictonary
    } catch let error as NSError {
        println_debug(error)
    }
    return nil
}

public func toDictionary(text: String) -> [String: Any]? {
    
    println_debug("text = \(text)")
    if let data = text.data(using: .utf8) {
        
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            println_debug(error.localizedDescription)
        }
    }
    return nil
}

public func toData(image : UIImage) -> Data? {
    
    return image.jpegData(compressionQuality: 1)
}

public func toBase64EncodedData(image : UIImage) -> Data? {
    
    guard let data = toData(image: image) else { return nil }
    
    return toBase64EncodedData(data: data)
}

public func toBase64EncodedString(image : UIImage) -> String? {
    
    guard let data = toData(image: image) else { return nil }
    
    return toBase64EncodedString(data: data)
}

public func toBase64EncodedData(data : Data) -> Data {
    
    return data.base64EncodedData()
}

public func toBase64EncodedString(data : Data) -> String {
    
    return data.base64EncodedString()
}


