//
//  OfferReportAPIGateWayImpl.swift
//  OK
//
//  Created by palnar on 18/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

public final class OfferReportAPIGateWayImpl :APIGateWayBase {
    
    public override func submit(_ params: GateWayInputParams, response: @escaping (GateWayResponse) -> ()) throws {
        
        guard let params = params as? InputParams else {
            throw APIGateWayError.parameterTypeNotMatched
        }
        
        DispatchQueue.global(qos: .background).async {
            
            self._submit(params, response: response)
        }
    }
    
    private func _submit(_ params: InputParams, response: @escaping (Response) -> ()) {
        
        
    }
}

extension OfferReportAPIGateWayImpl {
    
    public final class InputParams :GateWayInputParams {
        
        
    }
    
    public final class Response :GateWayResponse {
        
        public var error :GateWayError? {
            return nil
        }
        
        public func result()  throws -> Any {
            
            return "x"
        }
    }
}


