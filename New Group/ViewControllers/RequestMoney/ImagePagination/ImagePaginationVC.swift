//
//  ImagePaginationVC.swift
//  OK
//
//  Created by palnar on 11/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Auk

public class ImagePaginationVC :UIViewController {
    
    @IBOutlet var scrollView :UIScrollView!
    
    fileprivate var _placeholder :UIImage?
    fileprivate var _backgroundColor = UIColor.gray.withAlphaComponent(0.3)
    fileprivate var _contentMode = UIView.ContentMode.scaleAspectFill
    fileprivate var _sources :[Image]?
    
    
    public override func viewDidLoad() {
        
        super.viewDidLoad()  
    }
    
    @discardableResult public class func newInstance() ->ImagePaginationVC? {
        
        return UIStoryboard(name: "ImagePagination", bundle: nil).instantiateInitialViewController() as? ImagePaginationVC
    }
    
    @IBAction func dismiss(_ sender :Any?) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func set(source :[Image]?) ->ImagePaginationVC {
        
        self._sources = source
        return self
    }
    
    func set(contentMode :UIView.ContentMode) ->ImagePaginationVC {
        
        self._contentMode = contentMode
        return self
    }
    
    func set(backgroundColor :UIColor) ->ImagePaginationVC {
        
        self._backgroundColor = backgroundColor
        return self
    }
    
    func set(placeholder :UIImage?) ->ImagePaginationVC {
        
        self._placeholder = placeholder
        return self
    }
    
    func show(On parentVC :UIViewController) {
        
        ///
        parentVC.present(self, animated: true) {
            
            ///
            guard let scrollView = self.scrollView  , let files = self._sources else { return }
            
            ///
            scrollView.auk.settings.contentMode = self._contentMode
            scrollView.backgroundColor = self._backgroundColor
            scrollView.auk.settings.pageControl.backgroundColor = self._backgroundColor
            scrollView.auk.settings.placeholderImage = self._placeholder
            
            for file in files {
                
                if let source = file._source as? UIImage {
                    
                    ///
                    scrollView.auk.show(image: source)
                } else if let url = file._source as? URL {
                    
                    ///
                    let source = url.absoluteString
                    scrollView.auk.show(url: source)
                } else if let source = file._source as? String {
                    
                    ///
                    scrollView.auk.show(url: source)
                }
            }
        }
    }
}

extension ImagePaginationVC {
    
    public class Image {
        
        fileprivate let _source :Any?
        
        public init(source :URL) {
            self._source = source
        }
        
        public init(source :String) {
            self._source = source
        }
        
        public init(source :UIImage) {
            self._source = source
        }
    }
}


