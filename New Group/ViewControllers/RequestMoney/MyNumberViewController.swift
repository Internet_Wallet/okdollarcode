//
//  MyNumberViewController.swift
//  OK
//
//  Created by palnar on 29/11/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import AVFoundation
import Contacts
import FileBrowser

import Photos
import SDWebImage
import IQKeyboardManagerSwift


protocol newcontactDelegate : class {
   
    func newcontactPick()
}

class MyNumberViewController: OKBaseController,IndicatorInfoProvider, WebServiceResponseDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    {
        didSet {
            self.scrollView.isScrollEnabled = false
        }
    }
    @IBOutlet weak var contentStackView: UIStackView!
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBOutlet weak var viewAllReqButton : UIButton!{
        didSet {
            viewAllReqButton.titleLabel?.font  = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var submitButton : UIButton! {
        didSet {
            submitButton.isHidden = true
            submitButton.titleLabel?.font  = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var viewAllReqview : UIView! {
        didSet {
            self.viewAllReqview.isHidden = true
        }
    }
    
    public weak var _newRequestView: RequestMoneyView!
    private var _newRequestController :MyNumberNewRequestController!
    
    @IBOutlet weak var submitButtonHeightConstraint : NSLayoutConstraint!
    
    var qrCodeModelConfig : ScanObject?
    
    var mode = RequestMoneyView.Mode.myNumber
    
    var requestMoneyItemTop : RequestMoneyItem? {
        
        didSet {
            
            if requestMoneyItemTop == nil {
                
            } else {
                
            }
        }
    }
    
    var requestMoneyItemTopOther : RequestMoneyItem? {
        
        didSet {
            
            if requestMoneyItemTopOther == nil {
                
            } else {
                
            }
        }
    }
    
  private lazy var _pendingRequstSectionHeader: UIView? = {
    
    let instance = UIView.newInstance(xibName: "PendingRequest")
    if let viewArr = instance?.subviews {
      if let label = viewArr[safe: 0]?.subviews[safe: 0] as? UILabel {
        label.font  = UIFont(name: appFont, size: appFontSize)
        label.text = label.text?.localized
      }
    }
    
    return instance
  }()
    
    override func viewWillAppear(_ animated: Bool) {
        print("Mynumber viewWillAppear----")
        //self.showToastlocal(message: "No Files Attached")

        super.viewWillAppear(animated)
        if mode == .myNumber {
            _fetchTOPResultData { [weak self] in
                
                DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
                    self?._newRequestView.focus()
                    
                }
            }
        } else {
            _fetchTOPResultData { [weak self] in
                
                DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
                    self?._newRequestView.focus()

                }
            }
        }
        
//        if mode == .myNumber, let qr = qrCodeModelConfig {
//
//            _showQRCode(qrCode: qr)
//        }
        
        viewAllReqButton.setTitle("View All Requests".localized, for: .normal)
        submitButton.setTitle("Submit".localized, for: .normal)
        let nc = NotificationCenter.default
        let myPendingImageNotification = Notification.Name(rawValue:"ImagePreviewNotification")
        nc.addObserver(forName:myPendingImageNotification, object:nil, queue:nil, using:pendingImageNotification)
        
        self.tableView.tableFooterView = UIView()
    }
    
    
    //: MARK: - Notification
    @objc func contactPick(notification:Notification) -> Void {

           if ((notification.userInfo?["MobileNo"] as? String) != nil)
           {
              // let fulldate = notification.userInfo?["MobileNo"] as? String
              // let\
            
            if let field = notification.userInfo?["textField"] as? UITextField{
               self._newRequestController.showContactView(number: notification.userInfo?["MobileNo"] as? String ?? "",textFieldNew: field)
            }
      
       
            
        }

        else
           {
             if mode == .myNumber {
            DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
                self._newRequestView.focus()

            }
        }
    }
    }
    
    override func viewDidLoad() {
        self.helpSupportNavigationEnum = .Request_Money
        print("Mynumber viewdidload----")
        submitButton.isHidden = true
        submitButtonHeightConstraint.constant = 0
        _setUpTableView()
        _insertNewRequestView()

        //Manage Keyboard
        IQKeyboardManager.sharedManager().enable            = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
       // self.showToast(message: "No Files Attached")
        //self.showToastlocal(message: "No Files Attached")
       // self.showToast(message: "No Results Found".localized, align: .top)

        super.viewDidLoad()
        
        if mode == .myNumber, let qr = qrCodeModelConfig {
            
            _showQRCode(qrCode: qr)
        }
        
        requestMoneyItemTop = nil
        requestMoneyItemTopOther = nil
        
        NotificationCenter.default.addObserver(self, selector: #selector(contactPick), name: NSNotification.Name(rawValue: "ContactPick"), object: nil)
               
    }
    
    
    func pendingImageNotification(notification:Notification) -> Void {
        
        if ((notification.userInfo?["image"] as? UIImage) != nil)
        {
            let imagePreviewVC = UIStoryboard(name: "BillSplitterView", bundle: Bundle.main).instantiateViewController(withIdentifier: "ImagePreviewVC") as! ImagePreviewVC
            imagePreviewVC.imageFile = (notification.userInfo?["image"] as? UIImage)!
            self.navigationController?.pushViewController(imagePreviewVC, animated: true)
        }
    }

    
    private func _showQRCode(qrCode :ScanObject) {
        
        guard let newRequestController = _newRequestController /*, qrCode.number != UserModel.shared.mobileNo */ else { return }
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.7) {
            newRequestController.showQRCode(qrCode: qrCode)
        }
    }
    
    private func _trimmedQRNUmber(qrCode :ScanObject) ->String {
        
        let countryCodeWithZero = UserModel.shared.countryCode.replacingOccurrences(of: "+", with: "00")
        
        if qrCode.number.contains(countryCodeWithZero) {
            return qrCode.number.replacingOccurrences(of: countryCodeWithZero, with: "0")
        }
        
        return qrCode.number
    }
    
    private func _setUpTableView() {
        
        guard let tableView = self.tableView else { return }
        
        let headerView = UIView()
        let footerView = UIView()
        let pendingRequestCell = UINib(nibName: "MyNumberRequestTVCell", bundle: Bundle.main)
         let pendingRequestCell1 = UINib(nibName: "OtherNumberRequestTVCell", bundle: Bundle.main)
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = footerView
        tableView.register(pendingRequestCell, forCellReuseIdentifier: "CellOne")
        tableView.register(pendingRequestCell1, forCellReuseIdentifier: "CellTwo")
        tableView.rowHeight = UITableView.automaticDimension;
        tableView.estimatedRowHeight = 46.0;
    }
    
    private func _cellNib() ->UINib? {
        
        if mode == .myNumber {
            return UINib(nibName: "MyNumberRequestTVCell", bundle: Bundle.main)
        } else {
            return UINib(nibName: "OtherNumberRequestTVCell", bundle: Bundle.main)
        }
    }
    
    private func _removeOldRequestView() {
        
        guard let stackView = contentStackView  else { return }
        
        var oldRequestView :RequestMoneyView?
        
        for view in stackView.subviews {
            
            if type(of: view) == RequestMoneyView.self {
                
                oldRequestView = view as? RequestMoneyView
                break
            }
        }
        
        if let oldRequestView = oldRequestView {
            
            stackView.removeArrangedSubview(oldRequestView)
            oldRequestView.removeFromSuperview()
        }
    }
    
    private func _insertNewRequestView(animation : Bool = false) {
        print("self.mode----\(self.mode)")

        guard let stackView = contentStackView, let newRequestView = RequestMoneyView.newInstance(mode: self.mode)  else {
            print("stackView else part called----")

            return }
        
        print("stackView----\(stackView)")
        print("newRequestView----\(newRequestView)")

        let controller = MyNumberNewRequestController(view: newRequestView, context: self)
        
        controller.deligate = self
        
        newRequestView.isHidden = true
        stackView.insertArrangedSubview(newRequestView, at: 0)
        
        if animation == false  {
            
            newRequestView.isHidden = false
        } else {
            
            UIView.animate(withDuration: 1.6, delay: 0.0, usingSpringWithDamping: 0.72, initialSpringVelocity: 0.7, options: [.curveEaseIn, .allowUserInteraction], animations: {
                
                newRequestView.isHidden = false
                self.view.layoutIfNeeded()
                
            }) { (flag) in
                
            }
        }
        
        self._newRequestView = newRequestView
        self._newRequestView.parentView = self
        self._newRequestController = controller
        self.view.layoutIfNeeded()
        
        if animation {
            _fetchTOPResultData { [weak self] in
                
                DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
                    self?._newRequestView.focus()
                    self?._newRequestView.endEditing(true)
                }
            }
        }
        
    }
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        
        if mode == .myNumber {
            return IndicatorInfo(title : "My Number".localized)
        } else {
            return IndicatorInfo(title : "Other Number".localized)
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func webResponse(withJson json: AnyObject, screen: String) {
        
        if screen == "mRequestMoneyMyNumber" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(dic)
                        if let request = dic["Msg"] as? String {
                            DispatchQueue.main.async(execute: {
                                // work Needs to be done
                                alertViewObj.wrapAlert(title: "Info", body: request, img: UIImage(named : "info_success"))
                                alertViewObj.addAction(title: "OK", style: AlertStyle.cancel, action: {
                                    self._fetchTOPResultData()
                                })
                                alertViewObj.showAlert(controller: self)
                            })
                        }
                    }
                }
            } catch {
                
            }
        }
    }
    
    //MARK: - getPendingTopResult
    fileprivate func _fetchTOPResultData(completion :(()->())? = nil) {
        
        requestMoneyItemTop = nil
        requestMoneyItemTopOther = nil
        self.tableView.reloadSections([0], with: .automatic)
        
        progressViewObj.showProgressView()
        guard let apiForPendingTopResult : URL = getPendingTopResultAPI(isRequestModule: true, isFromMyNumber: (mode == .myNumber) ? true : false ) else {
            return
        }
        println_debug("api \(apiForPendingTopResult.absoluteString) ")
        
        JSONParser.GetApiResponse(apiUrl: apiForPendingTopResult, type: "POST") { (status : Bool, data : Any?) in
            
            DispatchQueue.main.async {
                
                progressViewObj.removeProgressView()
                
                if status == true {
                    
                    if let dictionary = data as? Dictionary<String,Any>  {
                        
                        println_debug("dictionary = \(dictionary)")
                        if self.mode == .myNumber {
                            self.requestMoneyItemTop = RequestMoneyItem.wrapModel(dic: dictionary)
                        } else {
                            self.requestMoneyItemTopOther = RequestMoneyItem.wrapModel(dic: dictionary)
                        }
                        self.tableView.reloadSections([0], with: .automatic)
                    }
                    
                } else {
                    //println_debug("data = \(data)")
                }
                if self.mode == .myNumber {
                    if self.requestMoneyItemTop == nil {
                        self.viewAllReqview.isHidden = true
                        
                    } else {
                        self.viewAllReqview.isHidden = false
                    }
                }
                else {
                    if self.requestMoneyItemTopOther == nil {
                        self.viewAllReqview.isHidden = true
                } else {
                        self.viewAllReqview.isHidden = false
                    }
                }
                completion?()
            }
        }
    }
    
   
}

extension MyNumberViewController : UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if mode == .myNumber {
            if requestMoneyItemTop != nil {
                return 1
            }
        } else {
            if requestMoneyItemTopOther != nil {
                return 1
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if requestMoneyItemTop != nil || requestMoneyItemTopOther != nil {
          
            return _pendingRequstSectionHeader
        }
        
        return UIView(frame: CGRect.zero)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if requestMoneyItemTop != nil || requestMoneyItemTopOther != nil {
            return _pendingRequstSectionHeader?.frame.size.height ?? 0
        }
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var dataModel : RequestMoneyItem? = nil
        if mode == .myNumber
        {
            dataModel = requestMoneyItemTop
        } else {
             dataModel = requestMoneyItemTopOther
        }
        
        if dataModel?.IsMynumber == true {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellOne", for: indexPath) as! MyNumberRequestTVCell
            cell.bind(withModel: dataModel)
            
            let filesCount = dataModel?.AttachmentPath?.count ?? 0
            if filesCount > 0 {
                _addImage(On: cell.userImageView, fromURLString: dataModel?.AttachmentPath![0], forIndexPath: indexPath)
            }
            else {
                cell.userImageView.contentMode = UIView.ContentMode.scaleAspectFit
                cell.userImageView.setRounded()
                cell.userImageView.image = UIImage(named : "avatar")
            }
            
            cell.deligate = self
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellTwo", for: indexPath) as! OtherNumberRequestTVCell
            cell.nameTitleLabel.text = "Request Money Name".localized
            cell.numberTitleLabel.text = "Request Money Number".localized
            cell.bind(withModel: dataModel)
            
            let filesCount = dataModel?.AttachmentPath?.count ?? 0
            if filesCount > 0 {
                _addImage(On: cell.userImageView, fromURLString: dataModel?.AttachmentPath![0], forIndexPath: indexPath)
            }
            else {
                cell.userImageView.contentMode = UIView.ContentMode.scaleAspectFit
                cell.userImageView.setRounded()
                cell.userImageView.image = UIImage(named : "avatar")
            }
            
            cell.deligate = self
            return cell
        }
        
    }
    
//    private func _bind(view cell :MyNumberRequestTVCell, withModel model :RequestMoneyItem?) {
//
//        guard let model = model else { return }
//
//        cell.nameLabel.text  = model.AgentName
//        cell.myNumberLabel.text  = model.Destination
//        cell.dateLabel.text = model.Date
//        cell.typeLabel.text = model.CommanTransactionType
//        cell.requestTypeLabel.text = model.LocalTransactionType
//        cell.amountLabel.text = String.localizedStringWithFormat("%g", (model.Amount)!) + GlobalConstants.Strings.mmk
//        cell.catogoryLabel.text = model.CategoryName
//
//        let filesCount = model.AttachmentPath?.count ?? 0
//
//        if filesCount > 0 {
//
//            cell.attachementLabel.text = "Yes        "
//            cell.attachementLabel.isUserInteractionEnabled = true
//        } else {
//            cell.attachementLabel.text = "No        "
//            cell.attachementLabel.isUserInteractionEnabled = false
//        }
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var dataModel : RequestMoneyItem? = nil
        if mode == .myNumber
        {
            dataModel = requestMoneyItemTop!
        } else {
            dataModel = requestMoneyItemTopOther!
        }
 
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PTGalleryViewController") as? PTGalleryViewController
        vc?.arrayImages = (dataModel?.AttachmentPath)!
        vc?.reqMoney = "Request Money"
        if (dataModel?.AttachmentPath)!.count > 0 {
            if let nav = self.navigationController {
                nav.pushViewController(vc!, animated: true)
            }
            
        } else {
            showToast(message: "No Files Attached")
        }
        
    }
    
//    private func _addAttachement(On cell :CustomPendingRequestCellTableViewCell, attachements :[String]?, forIndexPath indexPath :IndexPath) {
//        
//        /// check weather label found
//        guard let label = cell.lblAttachedFile else { return }
//
//        let filesCount = attachements?.count ?? 0 // attachements count
//        let action = #selector(showAttachedFiles(_:)) // action when user tap on label
//        /// configure gester
//        if let gester = label.gestureRecognizers?.first as? TapGestureRecognizer {
//            /// if gester already added then remove the existing target, action and add new one.
//            gester.indexPath = indexPath
//        } else {
//            /// if gester not yet added then add a new gester.
//            let gester = TapGestureRecognizer(target: self, action: action)
//            gester.indexPath = indexPath
//            label.addGestureRecognizer(gester)
//        }
//        /// update the UI attachenge status on label
//        if filesCount > 0 {
//            
//            label.text = "Yes        "
//            label.isUserInteractionEnabled = true
//        } else {
//            label.text = "No        "
//            label.isUserInteractionEnabled = false
//        }
//    }
    
    private func _addImage(On imageView :UIImageView?, fromURLString urlString :String?, forIndexPath indexPath :IndexPath) {
        ///
        if let imageView = imageView, let urlString = urlString, let url = URL(string: urlString) {
            imageView.contentMode = UIView.ContentMode.scaleAspectFit
            imageView.setRounded()
            LazyImageLoader.showImage(On: imageView, fromURL: url, showActivityIndicator: true, indicatorStyle: .gray)
        } else {
            if let imageView = imageView, let urlString = urlString?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed), let url = URL(string: urlString) {
                imageView.contentMode = UIView.ContentMode.scaleAspectFit
                imageView.setRounded()
                LazyImageLoader.showImage(On: imageView, fromURL: url, showActivityIndicator: true, indicatorStyle: .gray)
            }
        }
        ///
        
    }
    
    private func _getPaginationImages(atInexPath indexPath :IndexPath) ->[ImagePaginationVC.Image]? {
        var collection = [ImagePaginationVC.Image]()

        if mode == .myNumber {
            guard let attachements = requestMoneyItemTop?.AttachmentPath, attachements.count > 0 else {  return nil }
            
            for attachement in attachements {
                
                let item = ImagePaginationVC.Image(source: attachement)
                collection.append(item)
            }
        } else {
            guard let attachements = requestMoneyItemTopOther?.AttachmentPath, attachements.count > 0 else {  return nil }
            
            for attachement in attachements {
                
                let item = ImagePaginationVC.Image(source: attachement)
                collection.append(item)
            }
        }
        return collection
    }
}

extension MyNumberViewController : NumberRequestTVCellDeligate {
    
    func numberRequestTVCell(cell :MyNumberRequestTVCell, dedTapOnCancelButton button :UIButton) {
        
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        
        cancelRequest(WithRow: indexPath.row)
    }
    
    func numberRequestTVCell(cell :MyNumberRequestTVCell, dedTapOnRemaindButton button :UIButton) {
        
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        remindRequestAgin(indexRow: indexPath.row)
    }
    
    func numberRequestTVCell(cell :MyNumberRequestTVCell, dedTapOnAttachementLabel label :UIView) {
        
//        guard let indexPath = tableView.indexPath(for: cell) else { return }
//        
//        let images = _getPaginationImages(atInexPath: indexPath)
//        ImagePaginationVC.newInstance()?.set(source: images).set(backgroundColor: UIColor.black).set(placeholder: UIImage(named: "attachement")).show(On: self)
        var dataModel : RequestMoneyItem? = nil
        if mode == .myNumber
        {
            dataModel = requestMoneyItemTop!
        } else {
            dataModel = requestMoneyItemTopOther!
        }
        
        if (dataModel?.AttachmentPath)!.count > 0 {
            let vc = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PTGalleryViewController") as? PTGalleryViewController
            vc?.arrayImages = (dataModel?.AttachmentPath)!
            vc?.reqMoney = "Request Money"
            if let nav = self.navigationController {
                nav.navigationBar.barTintColor        = kYellowColor
                nav.navigationBar.tintColor           = UIColor.white
                nav.pushViewController(vc!, animated: true)
            }
            
        } else {
            showToast(message: "No Files Attached")
        }
    }
}

//pending Request TableView Cell Delegate
extension MyNumberViewController : BioMetricLoginDelegate {
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if screen == "CancelTapped"
        {
            if isSuccessful {
               cancelRequestTap()
            }
        } else if screen == "RemindTapped"
        {
            if isSuccessful {
                remindRequestTap()
            }
        }
        
    }
    
    func updateReuestDatawith(reminderCount : Int) {
        
        DispatchQueue.main.async {
            if self.mode == .myNumber {
                if self.requestMoneyItemTop != nil {
                    let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! MyNumberRequestTVCell
                    cell.remaindCountLabel.text = String(reminderCount)
                }
            } else {
                if self.requestMoneyItemTopOther != nil {
                    let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! OtherNumberRequestTVCell
                    cell.remaindCountLabel.text = String(reminderCount)
                }
            }
        }
        self._show(title: "".localized, errorMeaasge: "Your Reminder Sent Successfully".localized, iconImageName: "info_success")
    }
    
    func remindRequestTap()
    {
        var remindReqUrl : URL?
        if mode == .myNumber
        {
            guard let url = RequestMoneyParams.getAPIForRemindRequest(pendingRequestModel: requestMoneyItemTop!)else {
                return
            }
            remindReqUrl = url
        } else {
            guard let url = RequestMoneyParams.getAPIForRemindRequest(pendingRequestModel: requestMoneyItemTopOther!)else {
                return
            }
            remindReqUrl = url
        }
        
        println_debug("api \(String(describing: remindReqUrl?.absoluteString)) ")
        
        progressViewObj.showProgressView()
        
        JSONParser.GetCancelApiResponse(apiUrl: remindReqUrl!, type : "POST") { [weak self] (status : Bool, data : Any?) in
            
            progressViewObj.removeProgressView()
            
            guard status == true else {
                var responseMsg = ""
                guard let responseMessage = data as? String else { return }
                responseMsg = responseMessage
                if responseMsg == "We are not allowing do Request Money" {
                    responseMsg = "You cannot request to same destination mobile number within 5 minutes".localized
                }
                self?._show(title: "", errorMeaasge: responseMsg.localized, iconImageName: "confirmation_icon")
                return
            }
            
            guard let dictionary = data as? NSDictionary else {
                
                if let message = data as? String {
                    
                    println_debug("message = \(message)")
                    self?._show(title: "".localized, errorMeaasge: message.localized, iconImageName: "info_success")
                }
                
                return
            }
            
            guard let msg = dictionary["Msg"] as? String  else {
                
                return
            }
            
            guard msg == "Sucess" else {
                
                self?._show(title: "".localized, errorMeaasge: msg.localized, iconImageName: "error")
                return
            }
            
            guard let data_ = toDictionary(any: dictionary["Data"]) else {
                
                return
            }
            
            guard let reminderCount = data_["RemainderCount"] as? String else {
                
                return
            }
            
            guard let count =  Int(reminderCount) else {
                
                return
            }
            if count > 0 {
                self?.updateReuestDatawith(reminderCount: count)
            }
        }
    }

    fileprivate func _show(title :String?, errorMeaasge :String, iconImageName :String = "AppIcon", action :(() ->())? = nil) {
        
        func show() {
            
            alertViewObj.wrapAlert(title: title, body: errorMeaasge, img: UIImage(named : iconImageName))
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                action?()
            })
            alertViewObj.showAlert(controller: self)
        }
        
        if Thread.isMainThread {
            show()
        } else {
            DispatchQueue.main.async {
                show()
            }
        }
    }
    
    
    //MARK: - getPendingTopResult
    func remindRequestAgin(indexRow : Int){
        
        func task() {
            remindRequestTap()
        }
        
        DispatchQueue.main.async(execute: {
            
            alertViewObj.wrapAlert(title: "", body: "Do you want to Remind Request Money Again?".localized, img: UIImage(named : "request_money_no_records"))
            alertViewObj.addAction(title: "Cancel".localized, style: AlertStyle.cancel, action: {
                
            })
            
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {
                OKPayment.main.authenticate(screenName: "RemindTapped", delegate: self)
            })
            alertViewObj.showAlert(controller: self)
        })
    }
    
    func cancelRequestTap()
    {
        progressViewObj.showProgressView()
        var topModel : RequestMoneyItem? = nil
        if mode == .myNumber {
            topModel = requestMoneyItemTop!
        } else {
            topModel = requestMoneyItemTopOther!
        }
        if let requestId = topModel?.RequestId {
            guard let apiForCancelRequest : URL = RequestMoneyParams.getAPIForChangeStausOfRequest(requestId: requestId, status: RequestedMoneyStatus.CANCELLED.rawValue)else {
                return
            }
            println_debug("api \(String(describing: apiForCancelRequest.absoluteString)) ")
            
            JSONParser.GetCancelApiResponse(apiUrl: apiForCancelRequest, type : "POST") { (status : Bool, data : Any?) in
                DispatchQueue.main.async(execute: {
                    progressViewObj.removeProgressView()
                })
                if(status){
                    
                    if let rootContent = data  as? NSDictionary {
                        if let _ =   rootContent["Msg"] as? String {
                            DispatchQueue.main.async(execute: {
                                alertViewObj.wrapAlert(title: "", body: "Your Request Cancelled Successfully".localized, img: UIImage(named : "info_success"))
                                alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                                    
                                    self._fetchTOPResultData()
                                })
                                alertViewObj.showAlert(controller: self)
                            })
                        }
                    }
                }else{
                    guard let responseMessage = data as? String else { return }
                    DispatchQueue.main.async(execute: {
                        // work Needs to be done
                        alertViewObj.wrapAlert(title: "", body: responseMessage.localized, img: UIImage(named : "AppIcon"))
                        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                            
                        })
                        alertViewObj.showAlert(controller: self)
                    })
                    
                }
            }
        }
        
    }
    
    //MARK: - Cancel the request
    func cancelRequest(WithRow indexRow : Int) {
        
        
        
        func task() {
            cancelRequestTap()
        }
        
        DispatchQueue.main.async(execute: {
            
            alertViewObj.wrapAlert(title: "", body: "Do you want to Cancel Request Money?".localized, img: UIImage(named : "request_money_no_records"))
            alertViewObj.addAction(title: "Cancel".localized, style: AlertStyle.cancel, action: {
                
            })
            
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {
                OKPayment.main.authenticate(screenName: "CancelTapped", delegate: self)
            })
            alertViewObj.showAlert(controller: self)
        })
    }
}

extension MyNumberViewController {
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        
        guard let newRequestController = _newRequestController else { return }
        
        newRequestController.submit()
    }
    
    @IBAction func viewAllRequestButtonTapped(_ sender: Any) {
        
        guard let vc = UIStoryboard(name: "PendingRequestSB", bundle: nil).instantiateViewController(withIdentifier: "pending_request") as? PendingRequestBaseVC else { return }
        vc.mode = mode
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

extension MyNumberViewController :MyNumberNewRequestControllerDeligate {
  
    func myNumberNewRequestController(_ controller: MyNumberNewRequestController, view: RequestMoneyView, requestNowOptionTapped requestSheduleView: RequestSheduleView) {
        submitButtonHeightConstraint.constant = 45
        self.submitButton.isHidden = false
    }
    
    func myNumberNewRequestController(_ controller: MyNumberNewRequestController, view: RequestMoneyView, contactButtonTapped mobileNumberView: MobileNumberView) {
         print("view calling")
    }
    
    func myNumberNewRequestController(_ controller :MyNumberNewRequestController, view :RequestMoneyView, sheduleRequestOptionTapped requestSheduleView :RequestSheduleView) {
        submitButtonHeightConstraint.constant = 0
        self.submitButton.isHidden = true
    }
    
    func myNumberNewRequestController(_ controller :MyNumberNewRequestController, view :RequestMoneyView, didPickNewDate requestSheduleView :RequestSheduleView) {
        submitButtonHeightConstraint.constant = 45
        self.submitButton.isHidden = false
    }
    
    func myNumberNewRequestController(_ controller :MyNumberNewRequestController, view :RequestMoneyView, didPickNewFrequency requestSheduleView :RequestSheduleView) {
        submitButtonHeightConstraint.constant = 45
        self.submitButton.isHidden = false
    }
    
    func myNumberNewRequestController(_ controller: MyNumberNewRequestController, willSendRequest params: RequestMoneyAPIGateWay.InputParams) {
        
    }
    
    func myNumberNewRequestController(_ controller: MyNumberNewRequestController, didReceiveSuccessResponse params: RequestMoneyAPIGateWay.InputParams, responce: RequestMoneyAPIGateWay.Resposne) {
        
        _reload()
    }
    
    func myNumberNewRequestController(_ controller: MyNumberNewRequestController, didReceiveFailoureResponse params: RequestMoneyAPIGateWay.InputParams, responce: RequestMoneyAPIGateWay.Resposne) {
        
    }
    
    func myNumberNewRequestController(_ controller: MyNumberNewRequestController, needReload view: RequestMoneyView) {
        
        _reload()
    }
    
    func myNumberNewRequestController(_ controller :MyNumberNewRequestController, scrollEnable view :AmountView){
        scrollView.isScrollEnabled = true
    }
    func myNumberNewRequestController(_ controller :MyNumberNewRequestController, scrollDisable view :MobileNumberView) {
        scrollView.isScrollEnabled = false
    }
    func myNumberNewRequestController(_ controller :MyNumberNewRequestController, scrollDisable view :RequestSheduleView) {
        scrollView.isScrollEnabled = false
    }
    
    private func _reload() {
        self.view.endEditing(true)
        submitButtonHeightConstraint.constant = 0
        self.submitButton.isHidden = true
        _removeOldRequestView()
        _insertNewRequestView(animation: true)
    }
}
 

