//
//  CustomSearchBar.swift
//  CustomSearchBar
//
//  Created by Gabriel Theodoropoulos on 8/9/15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

import UIKit

class CustomSearchBar: UISearchBar {
    
    var preferredFont: UIFont!
    
    var preferredTextColor: UIColor!
    
    var customDelegate: CustomSearchControllerDelegate!

    
    var textField: UITextField?
    var searchIcon: UIView?
    var isSearchIconShowing = false
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        // Find the index of the search field in the search bar subviews.
        if let index = indexOfSearchFieldInSubviews(ofType: UITextField.self) {
            // Access the search field
            if let searchField: UITextField = (subviews[0] ).subviews[index] as? UITextField {
                
                // Set its frame.
                searchField.frame = CGRect(x:50.0, y:0.0, width:frame.size.width-10 , height: frame.size.height-10 )
                
                // Set the font and text color of the search field.
                //searchField.font = preferredFont
              searchField.placeholder = "Search".localized
              if let myFont = UIFont(name: appFont, size: 15) {
                if searchField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                  searchField.font = myFont
//                    searchTextField.font = (appDel.currentLanguage == "uni") ? UIFont.systemFont(ofSize: 15) : myFont
//                    searchTextField.font = (appDel.currentLanguage == "uni") ? UIFont.systemFont(ofSize: 15) : myFont
//                    searchTextField.keyboardType = .asciiCapable
//
                }
              }
              
                searchField.textColor = preferredTextColor
                
                if let p = self.placeholder, let preferredTextColor = preferredTextColor {
                    
                    searchField.attributedPlaceholder  =  NSAttributedString.init(string: p, attributes: [NSAttributedString.Key.foregroundColor: preferredTextColor])
                    
                   
                }
                
        
                // Set the background color of the search field.
                searchField.backgroundColor = tintColor
                if let glassIconView: UIImageView = (searchField.leftView as? UIImageView) {
                    
                    //Magnifying glass
                    glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
                    glassIconView.tintColor = preferredTextColor
                }
            }
        }
        
        
        if let indexOFLabel = indexOfSearchFieldInSubviews(ofType: UILabel.self) {
            let labelField: UILabel = (subviews[0] ).subviews[indexOFLabel] as! UILabel
            labelField.textColor = preferredTextColor
        }
        
        let startPoint = CGPoint(x:0.0, y:frame.size.height)
        let endPoint = CGPoint(x:frame.size.width,y:frame.size.height)
        let path = UIBezierPath()
        path.move(to: startPoint)
        path.addLine(to: endPoint)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = tintColor.cgColor
        shapeLayer.lineWidth = 0.5
        
        layer.addSublayer(shapeLayer)
        super.draw(rect)
    }
    
    init(frame: CGRect, font: UIFont, textColor: UIColor) {
        super.init(frame: frame)
        
        self.frame = frame
        preferredFont = font
        preferredTextColor = textColor
        
        searchBarStyle = UISearchBar.Style.default
        isTranslucent = true
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func indexOfSearchFieldInSubviews(ofType : Swift.AnyClass) -> Int! {
        // Uncomment the next line to see the search bar subviews.
        // println(subviews[0].subviews)
        
        var index: Int!
        let searchBarView = subviews[0] 
        var i = -1
        for view in searchBarView.subviews {
            i = i+1
            if view.isKind(of: (ofType) ){
                index = i
                break
            }
        }
        
        return index
    }
    
    /// construct a search icon at right side of textField
    public func setUpSearchIconAtRightSide() {
        
        ///variable declaration
        guard let textField = self.subviews[0].subviews.last as? UITextField else { return }
        let leftView = textField.leftView
        let iconSize = (leftView != nil) ? leftView?.frame.size : CGSize(width: 20, height: 20)
        let searchIcon = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: iconSize ?? CGSize(width: 20, height: 20)))
        
        /// component configuration
        textField.rightViewMode = .always
        textField.textAlignment = .left
        searchIcon.image = UIImage(named: "search_white")
        
        /// add textField events observe to the textField
        textField.addTarget(self, action: #selector(CustomSearchBar.hideSearchIcon), for: .editingDidBegin)
        textField.addTarget(self, action: #selector(CustomSearchBar.showSearchIcon), for: .editingDidEnd)
        textField.addTarget(self, action: #selector(CustomSearchBar.textChanged), for: .editingChanged)
       // textField.addTarget(self, action: #selector(CustomSearchBar.returnKeyPressed), for: UIControlEvents.editingDidEndOnExit)
        
        /// assign local variables to globel variable
        self.textField = textField
        self.searchIcon = searchIcon
        /// remove existing leftView and rightView of searchTextField
        textField.rightView = nil
        textField.leftView = nil
    }
    
    /// show the search icon at right side of textField
    @objc func showSearchIcon() {
        
        print("CustomSearchBar showSearchIcon method called-----")

        
        if isSearchIconShowing == true { return }
        
        ///variable declaration
        guard let textField = textField, let icon = searchIcon else { return }
        
        print("CustomSearchBar showSearchIcon method called-----\(textField.text?.count ?? 0)====\(textField.text ?? "")")

        
        let count = textField.text?.count ?? 0
        if count != 0 { return }
        
        let textFieldSize = textField.frame.size
        let iconSize = icon.frame.size
        let iconX = textFieldSize.width-(2*iconSize.width)
        let iconY = (textFieldSize.height/2)-(iconSize.height/2)
        let iconOrigin = CGPoint(x: iconX, y: iconY)
        
        /// assign values to the components
        icon.alpha = 1.0
        icon.frame = CGRect(origin: iconOrigin, size: iconSize)
        icon.tintColor = UIColor.white
        
        /// add the search icon as a subView of textField
        textField.addSubview(icon)
        self.isSearchIconShowing = true
    }
    
    /// hide and remove the search icon from textField with animation
    @objc func hideSearchIcon(animation :Bool = true) {
        
        print("CustomSearchBar hideSearchIcon method called-----")

        
        if isSearchIconShowing == false { return }
        
        ///variable declaration
        guard let textField = textField, let icon = searchIcon else { return }
        
        print("CustomSearchBar hideSearchIcon method called-----\(textField.text?.count ?? 0)====\(textField.text ?? "")")

        
        let count = textField.text?.count ?? 0
        if count == 0 { return }
        
        var frame = icon.frame
        frame.origin.x = frame.origin.x - 32
        
        /// remove the search icon with animation
        if animation == true {
            UIView.animate(withDuration: 0.32, animations: {
                icon.alpha = 0.0
                icon.frame = frame
            }) { (f) in
                icon.removeFromSuperview()
            }
        } else {
            icon.removeFromSuperview()
        } 
        ///
        self.isSearchIconShowing = false
    }
    
    @objc func textChanged(sender :UITextField) {
        
        let textCount = sender.text?.count ?? 0
        
        if textCount == 0 {
            if isSearchIconShowing == false {
                showSearchIcon()
            }
        } else {
            if isSearchIconShowing == true {
                hideSearchIcon(animation: false)
            }
        }
    }
    
    
}


