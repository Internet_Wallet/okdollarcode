//
//  OKPHotoPickerCellCollectionViewCell.swift
//  OK
//
//  Created by palnar on 09/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class OKPHotoPickerCell: UICollectionViewCell {
    
    
    @IBOutlet weak  var imageV: UIImageView!
    @IBOutlet weak  var viewAnimate:CardDesignView!
    @IBOutlet weak  var yConstraint: NSLayoutConstraint!
    var image: UIImage? {
        get {
            return imageV.image
        }
        set {
            imageV.image = newValue
        }
    }
    
    func wrapCell(image: String) {
        self.yConstraint.constant = 0
        self.imageV.image = UIImage.init(named: image)
    }
    
}
