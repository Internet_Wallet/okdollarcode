//
//  QRPhotoScannerViewController.swift
//  OK
//
//  Created by Ashish Kumar Singh on 11/7/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Photos

@objc protocol OKPhotoPickerDelegate : class {
    @objc optional func imagePicker(pickedImage image: UIImage?, filteredImage: UIImage?)
    @objc optional func selectedWithQrCode(qrCode qrCodeModel : ScanObject?)
}

 class OKPhotoPickerWithQRScanner : OKBaseController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
   // var galleryImages            = [UIImage]()
    fileprivate var assets       : PHFetchResult<AnyObject>?
    @IBOutlet var collectionView : UICollectionView!
    fileprivate var sideSize     : CGFloat!
    @IBOutlet var activityIndicator : UIActivityIndicatorView!
    //var qrImageArray = [UIImage]()
    var ciDetector : CIDetector?
    var wentBackGround = false
    var mobilecheck = ""
    
    weak var imgPickerDelegate:OKPhotoPickerDelegate?
    var selectedQRCodeModel : ScanObject?
    var count = 0

    class func loadFromStoryboard() -> OKPhotoPickerWithQRScanner! {
        let _storyboard = UIStoryboard(name: "OkPhotoPickerAndScanner", bundle: Bundle.main)
        return _storyboard.instantiateViewController(withIdentifier: "OkPhotoPickerAndScannerVC") as? OKPhotoPickerWithQRScanner
   
    }
    
     override func viewDidLoad() {
        super.viewDidLoad()
        //        collectionViewLayout.itemSize = CGSize(width: sideSize, height: sideSize)
        let collectionViewLayout = UICollectionViewFlowLayout.init()
        collectionViewLayout.minimumLineSpacing = 0
        collectionViewLayout.minimumInteritemSpacing = 0
        
        ciDetector = CIDetector(ofType: CIDetectorTypeQRCode, context: nil, options: [CIDetectorAccuracy:CIDetectorAccuracyHigh])
        //****
        self.title = "Select QR Code".localized;
      self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
        let button1 = UIBarButtonItem(image: UIImage(named:"tabBarBack"), style: .plain, target: self, action:#selector(backButtonAction)) // action:#selector(Class.MethodName) for swift 3
        button1.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem  = button1
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToBackGround), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(enterForgound), name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.appWillResignActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    @objc func appWillResignActive() {
        PTLoader.shared.show()
        
        if PHPhotoLibrary.authorizationStatus() == .authorized {
            reloadAssets()
        } else {
            PHPhotoLibrary.requestAuthorization({ (status: PHAuthorizationStatus) -> Void in
                if status == .authorized {
                    self.reloadAssets()
                } else {
                    self.requestAlertForPermissions(withBody: "OK$ needs permission to Scan QR Code from your Gallery", handler: { (cancelled) in
                        if let navigation = self.navigationController {
                
                            navigation.popViewController(animated: true)
                        }
                    })
                }
            })
        }
        
        let time : DispatchTime = DispatchTime.now()
        DispatchQueue.main.asyncAfter(deadline: time) {
            PTLoader.shared.hide()
        }
        
    }
    
    
    @objc func moveToBackGround() {
        wentBackGround = true
    }
    
    @objc func enterForgound() {
        if wentBackGround {
           
            wentBackGround = false
        }
    
    }
    
    @objc func backButtonAction(){
        
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
     //   PTLoader.shared.show()
        
        if PHPhotoLibrary.authorizationStatus() == .authorized {
            reloadAssets()
        } else {
            PHPhotoLibrary.requestAuthorization({ (status: PHAuthorizationStatus) -> Void in
                if status == .authorized {
                    self.reloadAssets()
                } else {
                    self.requestAlertForPermissions(withBody: "OK$ needs permission to Scan QR Code from your Gallery", handler: { (cancelled) in
                        if let navigation = self.navigationController {
//                            if let backController = self.tbScanQRController {
//                                backController.captureSession?.startRunning()
//                            }
                            navigation.popViewController(animated: true)
                        }
                    })
                }
            })
        }
        
//        let time : DispatchTime = DispatchTime.now()
//        DispatchQueue.main.asyncAfter(deadline: time) {
//            PTLoader.shared.hide()
//        }
    }
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//          if PHPhotoLibrary.authorizationStatus() == .authorized {
//         reloadAssets()
//         } else {
//         PHPhotoLibrary.requestAuthorization({ (status: PHAuthorizationStatus) -> Void in
//         if status == .authorized {
//         self.reloadAssets()
//         } else {
//         self.showNeedAccessMessage()
//         }
//         })
//         }
//    }
   
    
    //Loading photos functions
    fileprivate func showNeedAccessMessage() {
        let alert = UIAlertController(title: "Image picker".localized, message: "App need get access to photos".localized, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: { (action: UIAlertAction) -> Void in
            self.dismiss(animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: { (action: UIAlertAction) -> Void in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: { (bool) in
                println_debug(bool)
            })
        }))
        
        show(alert, sender: nil)
    }
    
    fileprivate func reloadAssets() {
        DispatchQueue.main.async {
       self.activityIndicator.startAnimating()
        self.assets = nil
        self.collectionView.reloadData()
        self.assets = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: nil) as? PHFetchResult<AnyObject>
        self.collectionView.reloadData()
        self.activityIndicator.stopAnimating()
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? OKPHotoPickerCell
        mobilecheck = ""
        self.down(cell: cell!)
        //*
        PHImageManager.default().requestImage(for: assets?[indexPath.row] as! PHAsset, targetSize: PHImageManagerMaximumSize, contentMode: PHImageContentMode.aspectFit, options: nil) { (cellImage: UIImage?, info: [AnyHashable: Any]?) -> Void in
            if cellImage != nil {
                if let ciimg = CIImage(image: cellImage!) {
                    let features = self.ciDetector?.features(in: ciimg)
                    
                    for feature in features as! [CIQRCodeFeature] {
                        println_debug(feature.messageString!)
                        if let model = self.scannedString(str: feature.messageString!) {
                            self.selectedQRCodeModel = model
                        }
                        
                    }
                }
            }
        }
       //*/
    }
    
    func scannedString(str: String ) -> ScanObject? {
       
        var scanObject : ScanObject?
        _ = AESCrypt.decrypt(str.components(separatedBy: "---")[0], password: "m2n1shlko@$p##d")
        var sttt: String = ""
        println_debug(AESCrypt.decrypt(str, password: "m2n1shlko@$p##d"))
        if let stringQR = AESCrypt.decrypt(str, password: "m2n1shlko@$p##d") {
            sttt = stringQR } else
        {
            //self.showToast(message: "Invalid QR Code")
            
        }
        /*
        if sttt != "" {
            if sttt.contains("α") {
                if sttt.contains("-1") {
                    sttt = "" }
                else {
                    
                }
                if sttt.contains("`") {
                    
                } else {
                    // sttt=@"";
                    
                }
                println_debug("contains yes") }
            else
            { sttt = "" }
            
        }*/
        
        if(sttt.count>0){
            // var cellId: String = ""
            var businessName: String = ""
            var businessMobile: String = ""
            let myArray: [Any] = sttt.components(separatedBy: "#")
            _ = myArray[0] as! String
            let seconArray: [Any] = (myArray[1] as AnyObject).components(separatedBy: "-")
            businessName = seconArray[0] as! String
            let thirdArray: [Any] = (seconArray[1] as AnyObject).components(separatedBy: "@")
            businessMobile = thirdArray[0] as! String
            let fourthArray: [Any] = (thirdArray[1] as AnyObject).components(separatedBy: "&")
            let amt : String = fourthArray[0] as! String
            let fifthArray : [Any] = (fourthArray[1] as AnyObject).components(separatedBy: "`,,")
            var remark : String?
            if fifthArray.count > 1 {
                remark = fifthArray[1] as? String
            }
            
            var amount : String?
            if    amt.isEqual("(null)") == false {
                amount = amt
            }
            let userloginMobile = UserModel.shared.mobileNo
           
            if businessMobile != userloginMobile{
                scanObject = ScanObject.init(name: businessName, number: businessMobile, amount: amount, remark: remark)
            }
            else if businessMobile == userloginMobile {
                
                mobilecheck = businessMobile
//                alertViewObj.wrapAlert(title: nil, body: "You Can't make Payment to Your own Mobile Number".localized, img: UIImage(named : "Contact Mobile Number"))
//                alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
//
//                })
//                alertViewObj.showAlert(controller: self)
            }
          
            /* self.scanObj = ScanObject.init(amt: amt, nme: businessName, agent: businessMobile)
             if screen == .payto {
             if let delegate = self.delegate { delegate.changeScrollPage(point: 0, withScanObject: self.scanObj!)
             
             } } else if screen == .multi {
             screen = .payto
             if let delegate = mDelegate { delegate.navigateToMulti(obj: self.scanObj!)
             self.navigationController?.popViewController(animated: true) }
             
             } else if screen == .selfUpdate {
             screen = .payto
             if let delegate = mDelegate { delegate.selfUpdateScan(obj: self.scanObj!)
             self.navigationController?.popViewController(animated: true) } }*/
            
        } else {
            //captureSession?.startRunning()
            //progressViewObj.removeProgressView()
            return nil
            
        }
        // codeFrame.removeFromSuperview()
        //captureSession?.stopRunning()
        // progressViewObj.removeProgressView()
        return scanObject
    }
    
    
  /*  func up(cell: OKPHotoPickerCell) {
        UIView.animate(withDuration: 0.9, delay: 0.0, options: .curveEaseOut, animations: {() -> Void in
            cell.yConstraint.constant = 0
        }, completion: {(_ finished: Bool) -> Void in
           // self.down(cell: cell)
        })
    }*/
    
    func down(cell: OKPHotoPickerCell) {
        
        let view = UIView.init(frame: self.collectionView.frame)
        view.backgroundColor = UIColor.clear
        self.view.addSubview(view)
        
        CATransaction.begin()
        CATransaction.setCompletionBlock({
            //view.removeFromSuperview()
            if   self.selectedQRCodeModel != nil {
                self.imgPickerDelegate?.selectedWithQrCode!(qrCode: self.selectedQRCodeModel)
                //self.navigationController?.popViewController(animated: true)
            } else if self.mobilecheck == UserModel.shared.mobileNo {
                alertViewObj.wrapAlert(title: nil, body: "You Can't make Payment to own Mobile Number".localized, img: UIImage(named : "Contact Mobile Number"))
                alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                    self.navigationController?.popViewController(animated: true)
                })
                alertViewObj.showAlert(controller: self)
            }
            else {
                alertViewObj.wrapAlert(title: nil, body: "Please scan correct QR Code".localized, img: UIImage(named : "Contact Mobile Number"))
                alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                    self.navigationController?.popViewController(animated: true)
                })
                alertViewObj.showAlert(controller: self)
            }
            
        })
        let hover = CABasicAnimation(keyPath: "position")
        hover.isAdditive = true
        hover.fromValue = NSValue(cgPoint: CGPoint.zero)
        hover.toValue = NSValue(cgPoint: CGPoint(x: 0.0, y: screenWidth/3.2 - 2))
        hover.autoreverses = true
        hover.duration = 2
       // hover.repeatCount = Float.infinity
        cell.viewAnimate.layer.add(hover, forKey: "myHoverAnimation")
        //self.view.addSubview(view)

        CATransaction.commit()
    
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (assets != nil) ? assets!.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "oKImagePickerCell", for: indexPath) as? OKPHotoPickerCell
        
        let mOptiosn = PHImageRequestOptions.init()
        mOptiosn.deliveryMode = .highQualityFormat
        PHImageManager.default().requestImage(for: assets?[indexPath.row] as! PHAsset, targetSize: CGSize(width: screenWidth, height: screenWidth), contentMode: .aspectFill, options: mOptiosn) { (image: UIImage?, info: [AnyHashable: Any]?) -> Void in
            (cell!).imageV.image = image
        }
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = screenWidth/3.2
        return CGSize.init(width: size, height: size)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        let mOptiosn = PHImageRequestOptions.init()
//        mOptiosn.deliveryMode = .highQualityFormat
//        PHImageManager.default().requestImage(for: assets?[indexPath.row] as! PHAsset, targetSize: CGSize(width: screenWidth/3, height: screenWidth/3), contentMode: .aspectFit, options: mOptiosn) { (image: UIImage?, info: [AnyHashable: Any]?) -> Void in
//            (cell as! OKPHotoPickerCell).imageV.image = image
//        }
        
    }
    



    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
