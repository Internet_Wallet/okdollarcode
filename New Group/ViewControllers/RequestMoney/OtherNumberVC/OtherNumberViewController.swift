//
//  OtherNumberViewController.swift
//  OK
//
//  Created by palnar on 30/11/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import AVFoundation
import Contacts
import FileBrowser
import Photos

class OtherNumberViewController: OKBaseController,IndicatorInfoProvider, WebServiceResponseDelegate{
    
    //MARK: - Properties
    
    @IBOutlet weak var requestForTableView: UITableView!
    
    @IBOutlet weak var pendingRequestTableView : UITableView!
    
    @IBOutlet weak var btBottomButton : UIButton!
    
    private var cnContacts = [CNContact]()
    
    private var filteredContacts = [SearchTextFieldItem]()
    
    // var sections = [SectionRequestFor]()
    
    @IBOutlet weak var heightRequestForTableView: NSLayoutConstraint!
    
    @IBOutlet weak var imgSubmit: UIImageView!
    fileprivate var cellDescriptors: NSMutableArray!
    fileprivate var visibleRowsPerSection = [[Int]]()
    
    fileprivate var selectedContackCellIndexPath :IndexPath?
    fileprivate var selectedContacts = [Contact]()
    
    var otherNumberInputData = [ String : Any ]()
    var requestMoneyItemTop : RequestMoneyItem?
    var qrCodeModelConfig : ScanObject?
    
    @IBOutlet weak var bottomContainerHeightConstraint: NSLayoutConstraint!
    //@IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    
    //MARK: Override methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.helpSupportNavigationEnum = .Request_Money

        requestForAllConatct()
      //  hideKeyboardWhenTappedAround()
        configureTableView()
        
        
        //        sections = [
        //            SectionRequestFor(name: "Select Request For", items: ["OK$ Money ","Recharge"])
        //        ]
        // Auto resizing the height of the cell
        // requestForTableView.estimatedRowHeight = 44.0
        //requestForTableView.rowHeight = UITableViewAutomaticDimension
        //requestForTableView.delegate = self;
        //requestForTableView.dataSource = self;
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //loadCellDescriptors()
        //configureTableView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        //self.heightConstraint.constant = self.requestForTableView.contentSize.height
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        requestForTableView.layer.removeAllAnimations()
        heightRequestForTableView.constant = requestForTableView.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK: - Expanadable table view functions
    func loadCellDescriptors() {
        if let path = Bundle.main.path(forResource: "OtherNumberCellDescriptor", ofType: "plist") {
            cellDescriptors = NSMutableArray(contentsOfFile: path)
            if let qrCodeModel = qrCodeModelConfig {
                println_debug(qrCodeModel.name)
                if qrCodeModel.number != UserModel.shared.mobileNo {
                    let countryCodeWithZero = UserModel.shared.countryCode.replacingOccurrences(of: "+", with: "00")
                    if qrCodeModel.number.contains(countryCodeWithZero){
                        let trimmedNumber = qrCodeModel.number.replacingOccurrences(of: countryCodeWithZero, with: "0")
                        otherNumberInputData["request_mobile_number"] = trimmedNumber
                        ((cellDescriptors as! [[NSMutableDictionary]])[0][1]).setValue(true, forKey: "isVisible")
                    }
                }
            }
             getIndicesOfVisibleRows(isReloadData: true, indexPathParam: nil)
           // requestForTableView.reloadData()
            
        }
    }
    
    
    @IBAction func submitRequest(_ sender: Any) {
        alertViewObj.wrapAlert(title: nil, body: "Do you want to send Request Money?".localized, img: UIImage(named : "confirmation_icon"))
        alertViewObj.addAction(title: "Cancel".localized, style: AlertStyle.cancel, action: {
            
        })
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {
            
            OKPayment.main.authenticate(screenName: "RequestMoneyOtherNumber", delegate: self)

        })
        alertViewObj.showAlert(controller: self)
    }
    
    @IBAction func viewAllRequest(_ sender: Any) {
        
    }

    
    func configureTableView() {
        requestForTableView.delegate = self
        requestForTableView.dataSource = self
        requestForTableView.tableHeaderView = UIView(frame: CGRect.zero)
        requestForTableView.tableFooterView = UIView(frame: CGRect.zero)
        pendingRequestTableView.tableFooterView = UIView(frame: CGRect.zero)
        
        
        requestForTableView.register(UINib(nibName: "SectionCell", bundle: nil), forCellReuseIdentifier: "SectionCell")
        requestForTableView.register(UINib(nibName: "NormalItemCell", bundle: nil), forCellReuseIdentifier: "NormalItemCell")
        requestForTableView.register(UINib(nibName: "TextFieldCell", bundle: nil), forCellReuseIdentifier: "TextFieldCell")
        requestForTableView.register(UINib(nibName: "CustomButtonCell", bundle: nil), forCellReuseIdentifier: "CustomButtonCell")
        requestForTableView.register(UINib(nibName : "SectionRadioButton",bundle : nil),
                                     forCellReuseIdentifier : "SectionRadioButton")
        requestForTableView.register(UINib(nibName : "ItemRadioButton",bundle : nil),
                                     forCellReuseIdentifier : "ItemRadioButton")
        requestForTableView.register(UINib(nibName : "DatePickerCell",bundle : nil),
                                     forCellReuseIdentifier : "idCellDatePicker")
        requestForTableView.register(UINib(nibName : "SearchTextFieldCell",bundle : nil),
                                     forCellReuseIdentifier : "SearchTextField")
        
        
        pendingRequestTableView.register(UINib(nibName : "RequestCell",bundle : nil), forCellReuseIdentifier: "requestTypeCell")
        
        self.requestForTableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
        loadCellDescriptors()
        getPendingTOPResultData()
        
    }
    
    //MARK: - IndiactorInfoProvider
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title : "Other Number".localized)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func sendRequest(){
        progressViewObj.showProgressView()
        
        if appDelegate.checkNetworkAvail() {
            
            let urlStr   = Url.sendRequestForRequestMoney
            let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug(ur)
            //request_mobile_number - from plist corresponding key
            //receive_mobile_number - from plist corresponding key
            //parent_request_for - from list corres[ponding key
            //amount - corresponding plist
            //remark - corresponding plist
            //attach_file - name of the corresponding image file in asset term
            //attach_file_content - base 64 encoded image file data
            //schedule_type - -1 or 0 for 0 for schedule request
            //schedule_freequency - if  selected freequency
            //schedule_date - for selcted date
            
            let web      = WebApiClass()
            web.delegate = self
            let urlSt   = Url.sendRequestForRequestMoney
            let url = getUrl(urlStr: urlSt, serverType: .serverApp)
            println_debug(url)
            let keys  = otherNumberInputData.keys
            var sourceMobileNumber : String?
            var destinationMobileNumber : String = ""
            var amount : String = ""
            var remark  : String  = ""
            var base64EncodeImageDataString : String?
            var scheduleDate : String?
            //var selectedScheduleFreequency : String?
            var schduleType : String?
            if keys .contains("request_mobile_number"){
                destinationMobileNumber = otherNumberInputData["request_mobile_number"] as? String ?? ""
            }
            if keys.contains("receive_mobile_number"){
                sourceMobileNumber = otherNumberInputData["receive_mobile_number"] as? String ?? ""
            }
            if keys.contains("amount"){
                amount = otherNumberInputData ["amount"] as! String
            }
            if keys.contains("remark"){
                remark = otherNumberInputData["remark"] as! String
            }
            
            if keys.contains("attach_file_content"){
                base64EncodeImageDataString = otherNumberInputData["attach_file_content"] as? String
                
            }
            
            if keys.contains("schedule_type"){
                schduleType = otherNumberInputData["schedule_type"] as? String
                if schduleType == "0" {
                    if keys.contains("schedule_date") {
                        scheduleDate = otherNumberInputData["schedule_date"] as? String
                    }
                    if keys.contains("schedule_freequency") {
                        schduleType = otherNumberInputData["schedule_freequency"] as? String
                    }
                }
            }
            let params   = RequestMoneyParams.getParamsRequestForRequestMoney(obj: UserModel.shared, source: destinationMobileNumber , destination: sourceMobileNumber ?? "", amount: amount, remarks: remark, scheduledType: schduleType!, isMyNumber: false, scheduleDate: scheduleDate, base64EncodingImageString: base64EncodeImageDataString)
            println_debug(params)
            web.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", mScreen: "mRequestMoneyOtherNumber")
            
        }
    }
    
    
    func webResponse(withJson json: AnyObject, screen: String) {
        
        if screen == "mRequestMoneyOtherNumber" {
            
            do {
                
                if let data = json as? Data {
                    
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        
                        println_debug(dic)
                        if let request = dic["Msg"] as? String {
                            
                            DispatchQueue.main.async(execute: {
                                // work Needs to be done
                                alertViewObj.wrapAlert(title: "Info", body: request.localized, img: UIImage(named : "info_success"))
                                alertViewObj.addAction(title: "OK", style: AlertStyle.cancel, action: {
                                    
                                })
                                alertViewObj.showAlert(controller: self)
                            })
                        }
                    }
                }
            } catch {
                
            }
            
            DispatchQueue.main.async(execute: {
                progressViewObj.removeProgressView()
            })
        }
    }
    
    func checkOKNumberExistOrNot(withNumber : String,key : String ){
        
        var startIndex = 0
        if(withNumber.starts(with: "0")) {
            startIndex = 1
        }
        
        progressViewObj.showProgressView()
        let urlString = "\(Url.URLgetOKacDetails)" + "\(UserModel.shared.countryCode.replacingOccurrences(of: "+", with: "00"))" + "\(withNumber.substring(from: startIndex))"
        let url = getUrl(urlStr: urlString, serverType: .serverApp)
        println_debug(url)
        JSONParser.GetApiResponse(apiUrl: url, type: "POST") { (status : Bool, data : Any?) in
            DispatchQueue.main.async(execute: {
                progressViewObj.removeProgressView()
            })
            if(status){
                DispatchQueue.main.async(execute: {
                    self.changeVisibilityOfNextItemInTable(key: key)
                })
            } else {
                guard let responseMessage = data as? String else { return }
                DispatchQueue.main.async(execute: {
                    // work Needs to be done
                    alertViewObj.wrapAlert(title: "Warning".localized, body: responseMessage.localized , img: UIImage(named : "AppIcon"))
                    alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                        
                    })
                    alertViewObj.showAlert(controller: self)
                })
            }
        }
    }
    
    //MARK: - getPendingTopResult
    func getPendingTOPResultData(){
        requestMoneyItemTop = nil
        self.pendingRequestTableView.sectionHeaderHeight = 0
        self.pendingRequestTableView.reloadData()
        progressViewObj.showProgressView()
        guard let apiForPendingTopResult : URL = getPendingTopResultAPI(isRequestModule: true, isFromMyNumber: true) else {
            return
        }
        println_debug("api \(apiForPendingTopResult.absoluteString) ")
        
        JSONParser.GetApiResponse(apiUrl: apiForPendingTopResult, type: "POST") { (status : Bool, data : Any?) in
            DispatchQueue.main.async(execute: {
                progressViewObj.removeProgressView()
            })
            if(status){
                DispatchQueue.main.async(execute: {
                    self.requestMoneyItemTop = RequestMoneyItem.wrapModel(dic: (data as? Dictionary<String,Any>)!)
                    self.pendingRequestTableView.sectionHeaderHeight = 50
                    self.pendingRequestTableView.reloadData()
                    
                })
            }else{
                /* let responseMessage = data as? String
                 DispatchQueue.main.async(execute: {
                 // work Needs to be done
                 alertViewObj.wrapAlert(title: "Warning", body: responseMessage!, img: UIImage(named : "AppIcon"))
                 alertViewObj.addAction(title: "OK", style: AlertStyle.cancel, action: {
                 
                 })
                 alertViewObj.showAlert(controller: self)
                 })*/
                
            }
            
        }
    }
    
    //MARK:- Field Validation
    
    func fieldValidation() {
        /* Keys used describe the mynumberInputData Dictionary
         //request_mobile_number - from plist corresponding key
         //receive_mobile_number - from plist corresponding key
         //parent_request_for - from list corres[ponding key
         //amount - corresponding plist
         //remark - corresponding plist
         //attach_file - name of the corresponding image file in asset term
         //attach_file_content - base 64 encoded image file data
         //schedule_type - -1 or 0 for 0 for schedule request
         //schedule_freequency - if  selected freequency
         //schedule_date - for selcted date
         //schedule_date
         
         */
        var isAllFieldOk = true
        if (otherNumberInputData["receive_mobile_number"] as? NSString) == nil {
            isAllFieldOk  = false
        }
        if (otherNumberInputData["request_mobile_number"] as? NSString) == nil {
            isAllFieldOk  = false
        }
        if (otherNumberInputData["parent_request_for"] as? NSString) == nil  {
            isAllFieldOk  = false
        }
        if (otherNumberInputData["amount"] as? NSString) == nil  {
            isAllFieldOk  = false
        }else if ((otherNumberInputData["amount"] as? NSString)?.length)! < 2 {
            isAllFieldOk = false
        }
        if (otherNumberInputData["remark"] as? NSString) == nil  {
            isAllFieldOk  = false
        }
        
        if (otherNumberInputData["schedule_type"] as? NSString) == nil  {
            isAllFieldOk  = false
        }else {
            if otherNumberInputData["schedule_type"] as? String == "0" {
                
                if otherNumberInputData["schedule_freequency"] as? String == nil && otherNumberInputData["schedule_date"] as? String == nil {
                    isAllFieldOk = false
                }
            }
        }
        
        bottomContainerHeightConstraint.constant = 0
        if isAllFieldOk {
            bottomContainerHeightConstraint.constant = 50
            //submitButtonHeightConstarint.constant = 50
            btBottomButton.setTitle("Submit".localized, for: UIControl.State.normal)
            imgSubmit.isHidden = true
            btBottomButton.addTarget(self, action: #selector(submitRequest), for: .touchUpInside)
        }else{
            
            if let amount = otherNumberInputData["amount"] as? NSString {
                if amount.length > 1 {
          bottomContainerHeightConstraint.constant = 50
            btBottomButton.setTitle("View All Request".localized, for: UIControl.State.normal)
            imgSubmit.isHidden = false
            //btBottomButton.setImage(UIImage(named : "view"), for: UIControlState.normal)
            btBottomButton.addTarget(self, action: #selector(viewAllRequest), for: .touchUpInside)
                }
            }
        }
    }
    
    deinit {
        if requestForTableView != nil  {
            requestForTableView.removeObserver(self, forKeyPath: "contentSize")
        }
    }
    
}

/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destinationViewController.
 // Pass the selected object to the new view controller.
 }
 */

extension OtherNumberViewController: BioMetricLoginDelegate{
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String)
    {
        println_debug("Authorised")
        if(isSuccessful)
        {
          self.sendRequest()
        }
        else
        {
            println_debug("User not authorized")
            
        }
    }
}

extension OtherNumberViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField.isKind(of: SearchTextField.self)){
            let currentCharacterCount = textField.text?.count ?? 0
            let newLength = (currentCharacterCount + string.count - range.length)
            if (range.location<2) {
                return false
            }
            var key : String?
            if textField.tag == 150 {
                key = "request_mobile_number"
            }else{
                key = "receive_mobile_number"
            }
            if newLength == textField.maxLength {
                checkOKNumberExistOrNot(withNumber: textField.text!+string ,key: key! )
            }
            //            var  textFieldSuper : UIView = textField;
            //
            //            while (!textFieldSuper.isKind(of: UITableViewCell.self)) {
            //                textFieldSuper = textFieldSuper.superview!;
            //            }
            //            let  indexPath = (self.requestForTableView .indexPath(for: textFieldSuper as! UITableViewCell))
            //            let indexOfTappedRow = visibleRowsPerSection[(indexPath?.section)!][(indexPath?.row)!]
            //
            //            let cellKey = (cellDescriptors as! [[[String : AnyObject]]])[(indexPath?.section)!][indexOfTappedRow]["key"] as! String;
            // myNumberInputData[cellKey] = textField.text! + string
            if let newtext = textField.text {
                var startIndex = 0
                if(newtext.starts(with: "0")) {
                    startIndex = 1
                }
                
                updateSearchResultsForContacts(for: newtext.substring(from: startIndex)  + string, completion: { (Result) in
                    DispatchQueue.main.async(execute: {
                        (textField as! SearchTextField).filterItems(self.filteredContacts)
                    })
                })
            }
        }
         else if(textField.isKind(of: FloatLabelTextField.self)){
            
            print("other number amount text called----- ")
            
            let currentCharacterCount = textField.text?.count ?? 0
            
            if currentCharacterCount == 1 {
            if textField.text == "0" {
                
                return false

            }
            }
            
       /*  let currentCharacterCount = textField.text?.count ?? 0
         let newLength = (currentCharacterCount + string.count - range.length)
         //if newLength == textField.maxLength {
         //    checkOKNumberExistOrNot(withNumber: textField.text!+string)
         //}
         // if(newLength>1){
         myNumberInputData[textField.placeholder!] = textField.text!+string
         // let fieldKey =   getKeyOfLastVisibleRow()
         id textFieldSuper = textField;
         
         while (![textFieldSuper isKindOfClass:[UITableViewCell class]]) {
         textFieldSuper = [textFieldSuper superview];
         }
         
         // Get that cell's index path
         NSIndexPath *indexPath = [self.tableView indexPathForCell:(UITableViewCell *)textFieldSuper];
         self.changeVisibilityOfNextItemInTable(key: currentSelectedCellKey!)
         } */
         
         }
        return true
    }
    
    func cellFor(textField: UITextField) ->UITableViewCell? {
        
        var  textFieldSuper : UIView = textField;
        
        while (!textFieldSuper.isKind(of: UITableViewCell.self)) {
            textFieldSuper = textFieldSuper.superview!;
        }
        
        return textFieldSuper as? UITableViewCell
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        ///
        guard let cell = cellFor(textField: textField),
            let indexPath = self.requestForTableView.indexPath(for: cell) else { return }
        
        let indexOfTappedRow = visibleRowsPerSection[indexPath.section][indexPath.row]
        
        let cellKey = (cellDescriptors as! [[[String : AnyObject]]])[indexPath.section][indexOfTappedRow]["key"] as! String;
        otherNumberInputData[cellKey] = textField.text
        
        if(textField.isKind(of: SearchTextField.self)) {
            
        } else if(textField.isKind(of:FloatLabelTextField.self )){
            if let length = textField.text?.length {
                if length > 1 {
                    textField.text = textField.text?.alignWithCurrencyFormat()
                    self.changeVisibilityOfNextItemInTable(key: cellKey)
                }
            }
        }
        fieldValidation()
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    open func updateSearchResultsForContacts(for searchText: String,completion : @escaping (_ result : Bool)->())
    {
        if !searchText.isEmpty {
            filteredContacts.removeAll()
            for contact in cnContacts {
                
                let fullName = CNContactFormatter.string(from: contact, style: .fullName) ?? "No Name"
                NSLog("\(fullName): \(contact.phoneNumbers.description)")
                for phoneNumber in contact.phoneNumbers {
                    
                    if phoneNumber.value.stringValue.contains(searchText){
                        filteredContacts.append(SearchTextFieldItem(title:fullName , subtitle: phoneNumber.value.stringValue, image : UIImage(named : "AppIcon")))
                    }
                    println_debug("\(String(describing: phoneNumber.label?.description))  \(phoneNumber.value.stringValue)")
                    
                }
            }
            completion(true)
            // requestForTableView.reloadData()
            
        }
    }
    
    
    
    func requestForAllConatct(){
        let store = CNContactStore()
        store.requestAccess(for: .contacts, completionHandler: {
            granted, error in
            
            guard granted else {
                let alert = UIAlertController(title: "Can't access contact".localized, message: "Please go to Settings -> MyApp to enable contact permission".localized, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            let keysToFetch = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName), CNContactPhoneNumbersKey] as [Any]
            let request = CNContactFetchRequest(keysToFetch: keysToFetch as! [CNKeyDescriptor])
            
            
            do {
                try store.enumerateContacts(with: request){
                    (contact, cursor) -> Void in
                    self.cnContacts.append(contact)
                }
            } catch let error {
                NSLog("Fetch contact error: \(error)")
            }
            
            NSLog(">>>> Contact list:")
            for contact in self.cnContacts {
                let fullName = CNContactFormatter.string(from: contact, style: .fullName) ?? "No Name"
                NSLog("\(fullName): \(contact.phoneNumbers.description)")
            }
        })
    }
    
}

extension OtherNumberViewController : RequestMoneyCustomCellsDelegate {
    
    func dateWasSelected(selectedDateString: String) {
        otherNumberInputData["first_shedule_request"] = selectedDateString
        fieldValidation()
    }
    
    func requestMoneyCustomCell(_ cell :RequestMoneyCustomCellsTableViewCell, contactButtonTapped :Any) {
        
        self.selectedContackCellIndexPath = requestForTableView.indexPath(for: cell)
        openConatctAddressBook()
    }
}

extension OtherNumberViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if(tableView == requestForTableView) {
            if self.cellDescriptors != nil {
                return cellDescriptors.count
            }
            return 0
        }
        else {
            if requestMoneyItemTop != nil {
                return 1
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == requestForTableView) {
            if visibleRowsPerSection.count > 0 {
                return visibleRowsPerSection[section].count
            }else {
                return 0
            }
        }else if tableView == pendingRequestTableView {
            if requestMoneyItemTop != nil {
                return 1
            }
            return 0
        }else{
            return 0
        }
        
    }
    private func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if (tableView == requestForTableView ){
            switch section {
            case 0:
                return ""
                
                
            default:
                return ""
            }
        }else if tableView == pendingRequestTableView {
            if requestMoneyItemTop != nil {
                return "Pending Request"
            }
            return ""
        }else {
            return ""
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(tableView == requestForTableView ){
            let currentCellDescriptor = getCellDescriptorForIndexPath(indexPath: indexPath)
            
            switch currentCellDescriptor["cellIdentifier"] as! String {
                //            case "SectionCell":
                //                return 60.0
                
            case "idCellDatePicker":
                return 270.0
                
            default:
                return 45.0
            }
        }else {
            
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if tableView == self.requestForTableView {
            let currentCellDescriptor = getCellDescriptorForIndexPath(indexPath: indexPath)
            
            let cell = tableView.dequeueReusableCell(withIdentifier: currentCellDescriptor["cellIdentifier"] as! String, for: indexPath as IndexPath) as! RequestMoneyCustomCellsTableViewCell
            
            if currentCellDescriptor["cellIdentifier"] as! String == "SectionCell" {
              
                if let secondaryTitle = currentCellDescriptor["secondaryTitle"] {
                    cell.sectionTitle?.text = secondaryTitle as? String
                }
                
                let  imageDownArrow : UIImage = UIImage(named : "down_arrow_filter")!
                cell.sectionRightLogo.image = imageDownArrow
                
                if (currentCellDescriptor["isExpanded"] as! Bool) == true {
                    cell.separatorInset = UIEdgeInsets.init(top: 0, left: tableView.contentSize.width, bottom: 0, right: 0);
                    let angle =  CGFloat(Double.pi)
                    
                    let tr = CGAffineTransform.identity.rotated(by: angle )
                    cell.sectionRightLogo.transform = tr
                } else {
                    cell.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0);
                }
                
                if currentCellDescriptor["key"] as? String == "parent_frequency"
                {
                    cell.leftSectionLogo.isHidden = true
                }
                
            } else if currentCellDescriptor["cellIdentifier"] as! String == "idCellDatePicker" {
                
                cell.datePicker.minimumDate = Date()
                
            } else if currentCellDescriptor["cellIdentifier"] as! String == "NormalItemCell" {
                
                
                
                cell.textField.text = currentCellDescriptor["primaryTitle"] as? String
                cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
                
                
            } else if currentCellDescriptor["cellIdentifier"] as! String == "TextFieldCell" {
                let cellKey = currentCellDescriptor["key"] as! String
                if cellKey == "amount" {
                    cell.itemTextField.keyboardType = UIKeyboardType.decimalPad
                }else {
                    cell.itemTextField.keyboardType = UIKeyboardType.alphabet
                }
                cell.itemTextField.placeholder = currentCellDescriptor["secondaryTitle"] as? String
                cell.imgTextField.image = UIImage(named: (currentCellDescriptor["leftSideLogo"] as? String)!)
                cell.itemTextField.delegate = self
                if let textToDisplay = otherNumberInputData [cellKey] as? String{
                    cell.itemTextField.text = textToDisplay.alignWithCurrencyFormat()
                }
                cell.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0);
                
            }else if currentCellDescriptor["cellIdentifier"] as! String == "CustomButtonCell" {
                let cellKey = currentCellDescriptor["key"] as! String
                
                if let urlForPhoto = otherNumberInputData[cellKey] as? String {
                    getImageData(fromUrl: urlForPhoto, resultImageHandler: { (data) in
                        cell.leftSectionLogo.isHidden = false
                        cell.leftSectionLogo.image = UIImage( data : data!)
                        cell.itemButton.isHidden = true
                        self.otherNumberInputData["attach_file_content"] = data?.base64EncodedData()
                    })
                    
                }else{
                    cell.itemButton.setTitle(currentCellDescriptor["secondaryTitle"] as? String, for: UIControl.State.normal)
                    cell.itemButton.setImage(UIImage(named : (currentCellDescriptor["leftSideLogo"] as? String)!), for: UIControl.State.normal)
                    cell.leftSectionLogo.isHidden = true
                }
                
            }else if(currentCellDescriptor["cellIdentifier"] as! String == "SectionRadioButton"){
                cell.sectionRadioButton.setTitle(currentCellDescriptor["secondaryTitle"] as? String, for: UIControl.State.normal)
                cell.sectionRadioButton.isSelected = (currentCellDescriptor["isChecked"] as? Bool)!
                cell.backgroundColor = kYellowColor
                cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
                
            }else if(currentCellDescriptor["cellIdentifier"] as! String == "ItemRadioButton"){
                cell.sectionRadioButton.setTitle(currentCellDescriptor["secondaryTitle"] as? String, for: UIControl.State.normal)
                cell.sectionRadioButton.isSelected = (currentCellDescriptor["isChecked"] as? Bool)!
                cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
            } else if(currentCellDescriptor["cellIdentifier"] as! String == "SearchTextField"){
                let key = (currentCellDescriptor["key"] as? String)!
                
                cell.searchTextField.placeholder = (currentCellDescriptor["secondaryTitle"] as? String)
                cell.leftSectionLogo.image = UIImage(named: (currentCellDescriptor["leftSideLogo"] as? String)!)
                cell.searchTextField.delegate = self
                if let textToDisplay = otherNumberInputData [(currentCellDescriptor["key"] as? String)!] as? String{
                    cell.searchTextField.text = textToDisplay
                }else {
                    otherNumberInputData[key] = commonPrefixMobileNumber
                    cell.searchTextField.text = commonPrefixMobileNumber
                    
                }
                
                if key == "request_mobile_number" {
                    cell.searchTextField.tag = 150
                }else{
                    cell.searchTextField.tag = 151
                }
                cell.rightContactButton.addTarget(self, action: #selector(contactButtonTapped), for: UIControl.Event.touchUpInside)
                
                cell.separatorInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0);
                cell.rightContactButton.addTarget(self, action: #selector(contactButtonTapped), for: UIControl.Event.touchUpInside)
                
                //            let item1 = SearchTextFieldItem(title: "Anshad", subtitle: "9567192929", image: UIImage(named: "AppIcon"))
                //            let item2 = SearchTextFieldItem(title: "Vinnu", subtitle: "9976656086", image: UIImage(named: "AppIcon")); cell.searchTextField.filterItems([item1,item2])
                cell.searchTextField.filterItems(filteredContacts)
                cell.searchTextField.theme.font = UIFont.systemFont(ofSize: 13)
                cell.searchTextField.theme.bgColor = UIColor.white
                cell.searchTextField.theme.borderColor = UIColor.lightGray
                cell.searchTextField.theme.separatorColor = UIColor.white
                cell.searchTextField.theme.cellHeight = 50
                cell.searchTextField.maxNumberOfResults = 5
                cell.searchTextField.maxResultsListHeight = 200
                cell.searchTextField.highlightAttributes = [NSAttributedString.Key.backgroundColor: UIColor.yellow, NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 13)]
                cell.searchTextField.itemSelectionHandler = { filteredResults, itemPosition in
                    // Just in case you need the item position
                    let item = filteredResults[itemPosition]
                    //println_debug("Item at position \(itemPosition): \(item.subtitle)")
                    
                    // Do whatever you want with the picked item
                    cell.searchTextField.text = item.subtitle
                    
                }
                
            }
            cell.delegate = self
            return cell;
        } else {
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "requestTypeCell", for: indexPath as IndexPath) as! CustomPendingRequestCellTableViewCell
            
            cell.lblCustomerName.text  = requestMoneyItemTop?.AgentName
            cell.lblMobileNumber.text  = requestMoneyItemTop?.Destination
            cell.lblDate.text = requestMoneyItemTop?.Date
            cell.lblType.text = requestMoneyItemTop?.CommanTransactionType
            cell.lblRequestTYpe.text = requestMoneyItemTop?.LocalTransactionType
            cell.lblAmount.text = String.localizedStringWithFormat("%g", (requestMoneyItemTop?.Amount)!) + GlobalConstants.Strings.mmk
            cell.lblCategory.text = requestMoneyItemTop?.CategoryName
            
            _addAttachementDetails(On: cell, attachements: requestMoneyItemTop?.AttachmentPath, forIndexPath: indexPath)
            _addImage(On: cell.imgUserLogo, fromURLString: requestMoneyItemTop?.ProfileImage, forIndexPath: indexPath)
            
            cell.delegate = self
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == requestForTableView {
            
            let indexOfTappedRow = visibleRowsPerSection[indexPath.section][indexPath.row]
            
            let cellIdentifier = (cellDescriptors as! [[[String : AnyObject]]])[indexPath.section][indexOfTappedRow]["cellIdentifier"] as! String;
            if  (cellIdentifier == "SectionRadioButton" || cellIdentifier == "ItemRadioButton") {
                for currentSectionCells  in cellDescriptors {
                    
                    let indexFirstVisibleRow = getFirstIndicesOfGivenCellDescriptor(cellDescriptor: cellIdentifier);
                    for row in indexFirstVisibleRow...(((currentSectionCells ) as! [[String: AnyObject]]).count - 1) {
                        
                        
                        var shouldCheckedRow = false
                        if(indexOfTappedRow == row && ((currentSectionCells  as! [[String: AnyObject]])[row]["isCheckable"] )as! Bool){
                            shouldCheckedRow = true ;
                            
                            
                            
                        }
                        ((cellDescriptors as! [[NSMutableDictionary]])[indexPath.section][row]).setValue(shouldCheckedRow, forKey: "isChecked")
                        ((cellDescriptors as! [[NSMutableDictionary]])[indexPath.section][row]).setValue(shouldCheckedRow, forKey: "isExpanded")
                        
                        if(((currentSectionCells  as! [[String: AnyObject]])[row]["isExpandable"] )as! Bool == true){
                            
                            
                            let childIndex = (row + (((currentSectionCells  as! [[String: AnyObject]])[row]["childStartIndexOffset"] )as! Int) )
                            for i in (childIndex + 1)...(childIndex + ((cellDescriptors as! [[[String : AnyObject]]])[indexPath.section][row]["additionalRows"] as! Int)) {
                                ((cellDescriptors as! [[NSMutableDictionary]])[indexPath.section][i]).setValue(shouldCheckedRow, forKey: "isExpanded")
                                ((cellDescriptors as! [[NSMutableDictionary]])[indexPath.section][i]).setValue(shouldCheckedRow, forKey: "isVisible")
                                if shouldCheckedRow == false {
                                    ((cellDescriptors as! [[NSMutableDictionary]])[indexPath.section][i]).setValue(false, forKey: "isChecked")
                                }
                            }
                        }
                    }
                    
                    
                }
                let isExpandable = (cellDescriptors as! [[[String : AnyObject]]])[indexPath.section][indexOfTappedRow]["isExpandable"] as! Bool;
                if cellIdentifier == "SectionRadioButton" {
                    if !isExpandable {
                        otherNumberInputData["schedule_type"] = "-1"
                        
                    }else  {
                        otherNumberInputData["schedule_type"] = "0"
                    }
                }
                
            }
                
            else if (((cellDescriptors as! [[[String : AnyObject]]])[indexPath.section][indexOfTappedRow]["isExpandable"] as! Bool) == true){
                var shouldExpandAndShowSubRows = false
                if (cellDescriptors as! [[[String : AnyObject]]])[indexPath.section][indexOfTappedRow]["isExpanded"] as! Bool == false {
                    // In this case the cell should expand.
                    shouldExpandAndShowSubRows = true
                }
                (((cellDescriptors as! [[NSMutableDictionary]])[indexPath.section][indexOfTappedRow] ) ).setValue(shouldExpandAndShowSubRows, forKey: "isExpanded")
                for i in (indexOfTappedRow + 1)...(indexOfTappedRow + ((cellDescriptors as! [[[String : AnyObject]]])[indexPath.section][indexOfTappedRow]["additionalRows"] as! Int)) {
                    ((cellDescriptors as! [[NSMutableDictionary]])[indexPath.section][i]).setValue(shouldExpandAndShowSubRows, forKey: "isVisible")
                }
                
            }else if(cellIdentifier == "NormalItemCell"){
                let cellKey = (cellDescriptors as! [[[String : AnyObject]]])[indexPath.section][indexOfTappedRow]["key"] as! String;
                let indexFirstGroupVisibleRow = getFirstIndicesOfParentWithGivenKey(key: cellKey)
                
                
                if (((cellDescriptors as! [[[String : AnyObject]]])[indexPath.section][indexFirstGroupVisibleRow]["isExpandable"] as! Bool) == true){
                    var shouldExpandAndShowSubRows = false
                    if (cellDescriptors as! [[[String : AnyObject]]])[indexPath.section][indexFirstGroupVisibleRow]["isExpanded"] as! Bool == false {
                        // In this case the cell should expand.
                        shouldExpandAndShowSubRows = true
                    }
                    
                    
                    (((cellDescriptors as! [[NSMutableDictionary]])[indexPath.section][indexFirstGroupVisibleRow] ) ).setValue(shouldExpandAndShowSubRows, forKey: "isExpanded")
                    let selectedValue = (cellDescriptors as! [[[String : AnyObject]]])[indexPath.section][indexOfTappedRow]["primaryTitle"] as! String
                    (((cellDescriptors as! [[NSMutableDictionary]])[indexPath.section][indexFirstGroupVisibleRow] ) ).setValue(selectedValue, forKey: "secondaryTitle")
                    let parentKey = ((cellDescriptors as! [[[String : AnyObject]]])[indexPath.section][indexFirstGroupVisibleRow]["key"] as! String)
                    if parentKey == "parent_request_for"{
                        otherNumberInputData[parentKey] = selectedValue
                    }else{
                        otherNumberInputData["schedule_freequency"] = "\(indexOfTappedRow - indexFirstGroupVisibleRow)"
                    }
                    
                    for i in (indexFirstGroupVisibleRow + 1)...(indexFirstGroupVisibleRow + ((cellDescriptors as! [[[String : AnyObject]]])[indexPath.section][indexFirstGroupVisibleRow]["additionalRows"] as! Int)) {
                        ((cellDescriptors as! [[NSMutableDictionary]])[indexPath.section][i]).setValue(shouldExpandAndShowSubRows, forKey: "isVisible")
                    }
                    
                    let nextItemIndex = (indexFirstGroupVisibleRow + 1 + ((cellDescriptors as! [[[String : AnyObject]]])[indexPath.section][indexFirstGroupVisibleRow]["additionalRows"] as! Int))
                    if nextItemIndex < ((cellDescriptors as! [[[String : AnyObject]]])[indexPath.section]).count {
                        let nextItemKey = (cellDescriptors as! [[[String: AnyObject]]])[indexPath.section][nextItemIndex]["key"] as! String
                        for currentSectionCells  in cellDescriptors {
                            for nextItemIndex in nextItemIndex...(((currentSectionCells ) as! [[String: AnyObject]]).count - 1){
                                if (currentSectionCells as! [[String: AnyObject]])[nextItemIndex]["key"] as! String == nextItemKey {
                                    ((cellDescriptors as! [[NSMutableDictionary]])[indexPath.section][nextItemIndex]).setValue(true, forKey: "isVisible")
                                }
                            }
                        }
                    }
                    
                }
                
            }else if cellIdentifier == "CustomButtonCell" {
                /* let fileManager = FileManager.default
                 let documentsUrl = fileManager.urls(for: .documentationDirectory, in: .allDomainsMask)[0] as URL
                 
                 let fileBrowser = FileBrowser.init(initialPath: documentsUrl, allowEditing: false, showCancelButton: true)
                 fileBrowser.didSelectFile = { (file: FBFile) -> Void in
                 println_debug("\(file.displayName)  \(file.filePath)")
                 }
                 present(fileBrowser, animated: true, completion: nil)*/
                openPhotoLibraryButton()
            }
            getIndicesOfVisibleRows(isReloadData: true, indexPathParam: indexPath)
            //            requestForTableView.reloadSections(NSIndexSet(index: indexPath.section) as IndexSet, with: UITableViewRowAnimation.fade)
            //requestForTableView.reloadData()
            fieldValidation()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(tableView == pendingRequestTableView){
            
            if(requestMoneyItemTop != nil){
                let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
                let label = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
                
                label.text = "Pending Request".localized
                
                label.textColor = UIColor.init(red: 22/255, green: 45/255, blue: 159/255, alpha: 1.0)
                label.textAlignment = .center
                
                view.backgroundColor = UIColor.init(red: 246/255, green: 198/255, blue: 0, alpha: 1.0)
                view.addSubview(label)
                
                return view
            }else{
                return UIView(frame: CGRect.zero)
            }
        }else {
            return UIView(frame: CGRect.zero)
        }
    }
    
    private func _addAttachementDetails(On cell :CustomPendingRequestCellTableViewCell, attachements :[String]?, forIndexPath indexPath :IndexPath) {
        
        /// check weather label found
        guard let label = cell.lblAttachedFile else { return }
        
        let filesCount = attachements?.count ?? 0 // attachements count
        let action = #selector(showAttachedFiles(_:)) // action when user tap on label
        
        /// configure gester
        if let gester = label.gestureRecognizers?.first as? TapGestureRecognizer {
            
            /// if gester already added then remove the existing target, action and add new one.
            gester.indexPath = indexPath
        } else {
            
            /// if gester not yet added then add a new gester.
            let gester = TapGestureRecognizer(target: self, action: action)
            gester.indexPath = indexPath
            label.addGestureRecognizer(gester)
        }
        
        /// update the UI attachenge status on label
        if filesCount > 0 {
            
            label.text = "Yes        ".localized
            label.isUserInteractionEnabled = true
        } else {
            label.text = "No        ".localized
            label.isUserInteractionEnabled = false
        }
    }
    
    private func _addImage(On imageView :UIImageView?, fromURLString urlString :String?, forIndexPath indexPath :IndexPath) {
        
        ///
        guard let imageView = imageView,
            let urlString = urlString?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed),
            let url = URL(string: urlString) else { return }
        ///
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        imageView.setRounded()
        LazyImageLoader.showImage(On: imageView, fromURL: url, showActivityIndicator: true, indicatorStyle: .gray)
    }
    
    private func _getPaginationImages(atInexPath indexPath :IndexPath) ->[ImagePaginationVC.Image]? {
        
        guard let attachements = requestMoneyItemTop?.AttachmentPath, attachements.count > 0 else {
            return nil
        }
        
        var collection = [ImagePaginationVC.Image]()
        
        for attachement in attachements {
            
            let item = ImagePaginationVC.Image(source: attachement)
            collection.append(item)
        }
        /*
         let images = [
         ImagePaginationVC.Image(source: "https://cdn.pixabay.com/photo/2017/01/06/19/15/soap-bubble-1958650_1280.jpg"),
         ImagePaginationVC.Image(source: "https://static.pexels.com/photos/67843/splashing-splash-aqua-water-67843.jpeg"),
         ImagePaginationVC.Image(source: "https://static.pexels.com/photos/247932/pexels-photo-247932.jpeg")
         
         ]
         */
        
        return collection
    }
    
    @objc fileprivate func showAttachedFiles(_ sender: TapGestureRecognizer) {
        
        guard sender.indexPath != nil else { return }
        
//        println_debug("showAttachedFiles button Tapped at \(indexPath)")
//
//        let images = _getPaginationImages(atInexPath: indexPath)
//        ImagePaginationVC.newInstance()?.set(source: images).set(backgroundColor: UIColor.black).set(placeholder: UIImage(named: "")).show(On: self)
        
        guard let attachements = requestMoneyItemTop?.AttachmentPath, attachements.count > 0 else {
            showToast(message: "No Files Attached")
            return
        }
        
        let vc = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PTGalleryViewController") as? PTGalleryViewController
        vc?.arrayImages = attachements
        if let nav = self.navigationController {
            nav.navigationBar.barTintColor        = kYellowColor
            nav.navigationBar.tintColor           = UIColor.white
            nav.pushViewController(vc!, animated: true)
        }
    }
    
    func getKeyOfLastVisibleRow()->(String){
        let lastVisibleIndex = self.visibleRowsPerSection[0][visibleRowsPerSection[0].count-1]
        return ((self.cellDescriptors  as! [[[String: AnyObject]]])[0][lastVisibleIndex]["key"]) as! String
    }
    func getFirstIndicesOfParentWithGivenKey(key : String )->Int{
        for currentSectionCells  in self.cellDescriptors {
            
            for row in 0...(((currentSectionCells ) as! [[String: AnyObject]]).count - 1) {
                if (currentSectionCells as! [[String: AnyObject]])[row]["key"] as! String == key {
                    for reverseIndexRow in stride(from: row-1, to: 0, by: -1){
                        if (currentSectionCells as! [[String: AnyObject]])[reverseIndexRow]["key"] as! String != key {
                            return reverseIndexRow;
                        }
                    }
                    
                }
            }
        }
        return -1;
    }
    
    func changeVisibilityOfNextItemInTable(key : String ){
        for currentSectionCells  in self.cellDescriptors {
            
            for row in 0...(((currentSectionCells ) as! [[String: AnyObject]]).count - 1) {
                if (currentSectionCells as! [[String: AnyObject]])[row]["key"] as! String == key {
                    
                    var nextItemKey = (currentSectionCells as! [[String: AnyObject]])[row+1]["key"] as! String
                    for nextItemIndex in row+1...(((currentSectionCells ) as! [[String: AnyObject]]).count - 1){
                        if (currentSectionCells as! [[String: AnyObject]])[nextItemIndex]["key"] as! String == nextItemKey {
                            let isManadatory = (currentSectionCells as! [[String: AnyObject]])[nextItemIndex]["isManadatory"] as! Bool
                            if isManadatory == false {
                                nextItemKey = (currentSectionCells as! [[String: AnyObject]])[nextItemIndex+1]["key"] as! String
                            }
                            ((cellDescriptors as! [[NSMutableDictionary]])[0][nextItemIndex]).setValue(true, forKey: "isVisible")
                        }
                    }
                    
                }
            }
        }
        getIndicesOfVisibleRows(isReloadData: true, indexPathParam: IndexPath(row : 0,section : 0))
        //        self.requestForTableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableViewRowAnimation.fade)
    }
    
    func getIndicesOfVisibleRows(isReloadData : Bool,indexPathParam : IndexPath?) {
        visibleRowsPerSection.removeAll()
        
        for currentSectionCells  in cellDescriptors {
            var visibleRows = [Int]()
            for row in 0...(((currentSectionCells ) as! [[String: AnyObject]]).count - 1) {
                if (currentSectionCells as! [[String: AnyObject]])[row]["isVisible"] as! Bool == true {
                    visibleRows.append(row)
                }
            }
            
            visibleRowsPerSection.append(visibleRows)
        }
        
        
        if isReloadData == true {
            if let indxPathSection = indexPathParam?.section {
                requestForTableView.reloadSections(NSIndexSet(index: indxPathSection) as IndexSet, with: UITableView.RowAnimation.fade)
            }else {
                requestForTableView.reloadData()
            }
        }
    }
    
    func getFirstIndicesOfGivenCellDescriptor(cellDescriptor : String )->Int{
        for currentSectionCells  in cellDescriptors {
            for row in 0...(((currentSectionCells ) as! [[String: AnyObject]]).count - 1) {
                if (currentSectionCells as! [[String: AnyObject]])[row]["cellIdentifier"] as! String == cellDescriptor {
                    return row;
                }
            }
        }
        return -1;
    }
    
    func getCellDescriptorForIndexPath(indexPath: IndexPath) -> [String: AnyObject] {
        let indexOfVisibleRow = self.visibleRowsPerSection[indexPath.section][indexPath.row]
        let cellDescriptor = (self.cellDescriptors as! [[[String : AnyObject]]])[indexPath.section][indexOfVisibleRow]
        return cellDescriptor
    }
    @objc
    func contactButtonTapped(sender : UIButton ){
        openConatctAddressBook()
    }
}

extension OtherNumberViewController : OKContactsPickerDelegate {
    
    //MARK:- Addressbook implmentaion
    func openConatctAddressBook(){
        /* let contactPicker = CNContactPickerViewController()
         contactPicker.delegate = self
         contactPicker.displayedPropertyKeys =
         [CNContactPhoneNumbersKey]
         self.present(contactPicker, animated: true, completion: nil)*/
        let contactPickerScene = OKContactsPicker(delegate: self, multiSelection: false,showIndexBar:false,subtitleCellType: SubtitleCellValue.phoneNumber)
        let navigationController = UINavigationController(rootViewController: contactPickerScene)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func contactPicker(_: OKContactsPicker, didContactFetchFailed error: NSError)
    {
        println_debug("Failed with error \(error.description)")
    }
    
    func contactPicker(_: OKContactsPicker, didSelectContact contact: Contact) {
        
        if let indexPath = selectedContackCellIndexPath, let cell = requestForTableView.cellForRow(at: indexPath) as? RequestMoneyCustomCellsTableViewCell {
            
            if selectedContacts.count <= indexPath.row {
                selectedContacts.append(contact)
            } else {
                selectedContacts[indexPath.row] = contact
            }
            cell.searchTextField.text = contact.phoneNumbers.first?.phoneNumber
        }
        println_debug("Contact \(contact.displayName) has been selected")
    }
    
    func contactPicker(_: OKContactsPicker, didCancel error: NSError)
    {
        println_debug("User canceled the selection");
        
    }
    
    func contactPicker(_: OKContactsPicker, didSelectMultipleContacts contacts: [Contact]) {
        println_debug("The following contacts are selected")
        for contact in contacts {
            println_debug("\(contact.displayName)")
        }
    }
}


//Image Delegate and Functions

extension OtherNumberViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        let imageUrl = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.referenceURL)] as! URL
        println_debug(imageUrl)
        otherNumberInputData["attach_file"] =  imageUrl.absoluteString
        dismiss(animated:true, completion: nil)
        self.requestForTableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
    }
    
    func openPhotoLibraryButton() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func getImageData(fromUrl urlString:String,resultImageHandler : @escaping (Data?) -> Swift.Void)   {
        //let url = URL(string: "assets-library://asset/asset.JPG?id=DB8D91B7-DBD2-4AAE-8429-EFC6A7FF684B&ext=JPG")
        let url = URL(string : urlString)
        
        let asset = PHAsset.fetchAssets(withALAssetURLs: [url!], options: nil)
        if let result = asset.firstObject {
            //else {
            // return nil
            
            // }
            let imageManager = PHImageManager.default()
            // var imageData: Data? = nil
            
            imageManager.requestImageData(for: result, options: nil, resultHandler: { (data, string, orientation, dict) in
                resultImageHandler(data)
                
                
                //                self.btBottomButton.setImage(UIImage(data: imageData!), for: UIControlState.normal)
            })
            // let data = try? Data(contentsOf: url!)
            
            
        }
    }
}

//pending Request TableView Cell Delegate
extension OtherNumberViewController : PendingRequesttableViewCellDelegate {
    func numberRequestTVCell(_sender: CustomPendingRequestCellTableViewCell, didTapOnAttachementLabel label: UIView) {
        
    }
    
    
    func tableViewCellDidTapRemind(_ sender: CustomPendingRequestCellTableViewCell) {
        guard let tappedIndexPath = pendingRequestTableView.indexPath(for: sender) else { return }
        // println_debug("Block ", sender, tappedIndexPath)
        remindRequestAgin(indexRow: tappedIndexPath.row)
    }
    
    func tabeleViewCellDidTapChangeStauts(_ sender: CustomPendingRequestCellTableViewCell, requestMoneyStatus: RequestedMoneyStatus) {
        guard let tappedIndexPath = pendingRequestTableView.indexPath(for: sender) else { return }
        // println_debug("Block ", sender, tappedIndexPath)
        cancelRequest(WithRow: tappedIndexPath.row)
    }
    func tableViewCellDidTapBlockOrUnblockRequest(_sender: CustomPendingRequestCellTableViewCell, isNeedToBeBlock: Bool) {
        println_debug("Block Or Unblock")
    }
    
    
    //MARK: - getPendingTopResult
    func remindRequestAgin(indexRow : Int){
        progressViewObj.showProgressView()
        
        guard let apiForRemindRequest : URL = RequestMoneyParams.getAPIForRemindRequest(pendingRequestModel: requestMoneyItemTop!)else {
            return
        }
        println_debug("api \(apiForRemindRequest.absoluteString) ")
        
        JSONParser.GetCancelApiResponse(apiUrl: apiForRemindRequest, type : "POST") {(status : Bool, data : Any?) in
            DispatchQueue.main.async(execute: {
                progressViewObj.removeProgressView()
            })
            if(status){
                
                
                if let rootContent = data  as? NSDictionary {
                    if let message =   rootContent["Msg"] as? String {
                        DispatchQueue.main.async(execute: {
                            alertViewObj.wrapAlert(title: "".localized, body: message.localized, img: UIImage(named : "AppIcon"))
                            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                                
                            })
                            alertViewObj.showAlert(controller: self)
                        })
                    }
                }
                
                
                
            }else{
                var responseMessage = data as? String
                if responseMessage == "We are not allowing do Request Money" {
                    responseMessage = "You cannot request to same destination mobile number within 5 minutes".localized
                }
                DispatchQueue.main.async(execute: {
                    // work Needs to be done
                    alertViewObj.wrapAlert(title: "Warning".localized, body: responseMessage!.localized, img: UIImage(named : "AppIcon"))
                    alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                        
                    })
                    alertViewObj.showAlert(controller: self)
                })
                
            }
            
        }
    }
    
    //MARK: - Cancel the request
    func cancelRequest(WithRow indexRow : Int){
        progressViewObj.showProgressView()
        if let requestId = requestMoneyItemTop?.RequestId {
            guard let apiForCancelRequest : URL = RequestMoneyParams.getAPIForChangeStausOfRequest(requestId: requestId, status: RequestedMoneyStatus.CANCELLED.rawValue)else {
                return
            }
            println_debug("api \(apiForCancelRequest.absoluteString) ")
            
            JSONParser.GetCancelApiResponse(apiUrl: apiForCancelRequest, type : "POST") { (status : Bool, data : Any?) in
                DispatchQueue.main.async(execute: {
                    progressViewObj.removeProgressView()
                })
                if(status){
                    
                    
                    if let rootContent = data  as? NSDictionary {
                        if let message =   rootContent["Msg"] as? String {
                            DispatchQueue.main.async(execute: {
                                alertViewObj.wrapAlert(title: "".localized, body: message.localized, img: UIImage(named : "AppIcon"))
                                alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                                    self.getPendingTOPResultData()
                                })
                                alertViewObj.showAlert(controller: self)
                            })
                        }
                    }
                    
                    
                    
                }else{
                    let responseMessage = data as? String
                    DispatchQueue.main.async(execute: {
                        // work Needs to be done
                        alertViewObj.wrapAlert(title: "Warning".localized, body: responseMessage!.localized, img: UIImage(named : "AppIcon"))
                        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                            
                        })
                        alertViewObj.showAlert(controller: self)
                    })
                    
                }
            }
        }
    }
}



//MARK: - Base elments for sections
struct SectionRequestFor {
    var name: String
    var items: [String]
    var collapsed: Bool
    
    init(name: String, items: [String], collapsed: Bool = false) {
        self.name = name
        self.items = items
        self.collapsed = collapsed
    }
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
