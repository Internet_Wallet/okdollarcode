//
//  Contact.swift
//  JFContactsPicker
//

import UIKit
import Contacts

open class Contact {
  
  public let firstName: String
  public let lastName: String
  public let company: String
  public let thumbnailProfileImage: UIImage?
  public let profileImage: UIImage?
  public let birthday: Date?
  public let birthdayString: String?
  public let contactId: String?
  public let phoneNumbers: [(phoneNumber: String, phoneLabel: String)]
  public let emails: [(email: String, emailLabel: String )]
  
  public init (contact: CNContact) {
    firstName = contact.givenName
    lastName = contact.familyName
    company = contact.organizationName
    contactId = contact.identifier
    
    if let thumbnailImageData = contact.thumbnailImageData {
      thumbnailProfileImage = UIImage(data:thumbnailImageData)
      
    } else {
      thumbnailProfileImage = nil
    }
    
    if let imageData = contact.imageData {
      profileImage = UIImage(data:imageData)
    } else {
      profileImage = nil
    }
    
    if let birthdayDate = contact.birthday {
      birthday = Calendar(identifier: Calendar.Identifier.gregorian).date(from: birthdayDate)
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = GlobalConstants.Strings.birthdayDateFormat
      //Example Date Formats:  Oct 4, Sep 18, Mar 9
      birthdayString = dateFormatter.string(from: birthday!)
      
    } else {
      birthday = nil
      birthdayString = nil
    }
    
    var numbers: [(String, String)] = []
    for phoneNumber in contact.phoneNumbers {
      let phoneLabel = phoneNumber.label ?? ""
      let phone = phoneNumber.value.stringValue
      
      numbers.append((phone,phoneLabel))
    }
    phoneNumbers = numbers
    
    var emails: [(String, String)] = []
    for emailAddress in contact.emailAddresses {
      let emailLabel = emailAddress.label ?? ""
      let email = emailAddress.value as String
      
      emails.append((email,emailLabel))
    }
    self.emails = emails
    
  }
  
  open var displayName: String {
    return firstName + " " + lastName
  }
  
  open var initials: String {
    var initials = String()
    
    if let firstNameFirstChar = firstName.first {
      initials.append(firstNameFirstChar)
    }
    
    if let lastNameFirstChar = lastName.first {
      initials.append(lastNameFirstChar)
    }
    
    return initials
  }
  
}
