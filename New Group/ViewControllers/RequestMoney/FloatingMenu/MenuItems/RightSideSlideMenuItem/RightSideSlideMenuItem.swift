//
//  RightSideSlideMenuItem.swift
//  OK
//
//  Created by palnar on 13/01/18.
//  Copyright © 2018 T T Marshel Daniel. All rights reserved.
//

import UIKit

class RightSideSlideMenuItem: FloatingMenuItemView {
    
    public typealias Action = (FloatingMenu, RightSideSlideMenuItem) ->()
    
    @IBOutlet var contentView :UIView!
    @IBOutlet var imageView :UIImageView!
    @IBOutlet var label :UILabel!
    @IBOutlet var shadowView :UIView!
    @IBOutlet var cornerCurveView :UIView!
    @IBOutlet var trailingConstraint :NSLayoutConstraint!
    
    public var onTap :Action?
    
    public static func newInstance() ->RightSideSlideMenuItem? {
        
        guard let view = RightSideSlideMenuItem.newInstance(xibName: "RightSideSlideMenuItem") else { return nil }
        view.contentView.alpha = 0.0
        
        return view
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        _setUpGraphics()
    }
    
    private func _setUpGraphics() {
        
        if let shadowView = shadowView {
            
            shadowView.clipsToBounds = false
            shadowView.backgroundColor = UIColor.clear
            shadowView.layer.shadowOffset = CGSize(width: 2, height:2)
            shadowView.layer.shadowRadius = 2;
            shadowView.layer.shadowOpacity = 0.4;
        }
        
        if let cornerCurveView = cornerCurveView {
            
            cornerCurveView.clipsToBounds = true
            cornerCurveView.layer.cornerRadius = 4.0
        }
    }
    
    override func present(completion: @escaping (FloatingMenuItemView) -> ()) {
        
        let delay = TimeInterval(self.buttonIndex) * 0.06
        let trailing = trailingConstraint.constant
        
        trailingConstraint.constant = -150
        self.layoutIfNeeded()
        trailingConstraint.constant = trailing
        
        UIView.animate(withDuration: 1.2, delay: delay, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.7, options: [.curveEaseOut, .allowUserInteraction], animations: {
            //
            self.contentView.alpha = 1.0
            self.layoutIfNeeded()
        }) { (flag) in
            
            completion(self)
        }
        
        UIView.animate(withDuration: 0.2 , delay: delay, options: [.curveEaseOut], animations: {
            //
            self.imageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
        }) { (flag) in
            
            UIView.animate(withDuration: 0.3 , delay: 0, options: [.curveEaseOut], animations: {
                //
                self.imageView.transform = CGAffineTransform.identity
            })
        }
    }
    
    override func dismiss(completion: @escaping (FloatingMenuItemView) -> ()) {
        
        let delay = TimeInterval(self.buttonIndex) * 0.06
        trailingConstraint.constant = trailingConstraint.constant + 80
        
        UIView.animate(withDuration: 0.26 , delay: delay, options: [.curveEaseOut], animations: {
            //
            self.layoutIfNeeded()
        }) { (flag) in
            
            self.trailingConstraint.constant = -350
            
            UIView.animate(withDuration: 0.42, animations: {
                self.contentView.alpha = 0.0
                self.layoutIfNeeded()
            }) { (flag) in
                completion(self)
            }
        }
        
        UIView.animate(withDuration: 0.6 , delay: delay, options: [.curveEaseOut], animations: {
            //
            self.imageView.transform = CGAffineTransform(rotationAngle: -(CGFloat.pi/2))
        })
    }
    
    @IBAction func _didTapped(_ sender :Any?) {
        
        guard let floatingMenu = floatingMenu else { return }
        
        onTap?(floatingMenu, self)
    }
    
    @discardableResult public func set(image :UIImage?) ->RightSideSlideMenuItem {
        
        self.imageView.image = image
        return self
    }
    
    @discardableResult public func set(title :String?) ->RightSideSlideMenuItem {
        self.label.font = UIFont(name: appFont, size: 14.0)
        self.label.text = title?.localized
        return self
    }
}


