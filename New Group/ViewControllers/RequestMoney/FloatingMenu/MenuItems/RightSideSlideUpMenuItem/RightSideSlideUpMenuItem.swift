//
//  RightSideSlideMenuItem.swift
//  OK
//
//  Created by palnar on 13/01/18.
//  Copyright © 2018 T T Marshel Daniel. All rights reserved.
//

import UIKit

class RightSideSlideUpMenuItem: FloatingMenuItemView {
    
    public typealias Action = (FloatingMenu, RightSideSlideUpMenuItem) ->()
    
    @IBOutlet var contentView :UIView!
    @IBOutlet var imageView :UIImageView!
    @IBOutlet var label :UILabel!
    @IBOutlet var shadowView :UIView!
    @IBOutlet var cornerCurveView :UIView!
    @IBOutlet var topConstraint :NSLayoutConstraint!
    @IBOutlet var bottomConstraint :NSLayoutConstraint!
    
    public var onTap :Action?
    
    public static func newInstance() ->RightSideSlideUpMenuItem? {
        
        guard let view = RightSideSlideUpMenuItem.newInstance(xibName: "RightSideSlideUpMenuItem") else { return nil }
        view.contentView.alpha = 0.0
        
        return view
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        _setUpGraphics()
    }
    
    private func _setUpGraphics() {
        
        if let shadowView = shadowView {
            
            shadowView.clipsToBounds = false
            shadowView.backgroundColor = UIColor.clear
            shadowView.layer.shadowOffset = CGSize(width: 2, height:2)
            shadowView.layer.shadowRadius = 2;
            shadowView.layer.shadowOpacity = 0.4;
        }
        
        if let cornerCurveView = cornerCurveView {
            
            cornerCurveView.clipsToBounds = true
            cornerCurveView.layer.cornerRadius = 4.0
        }
    }
    
    override func present(completion: @escaping (FloatingMenuItemView) -> ()) {
        
        let delay = TimeInterval(self.buttonIndex) * 0.06
        
        let top = topConstraint.constant
        let bottom = bottomConstraint.constant
        
        let tempConstant = 44.0
        
        topConstraint.constant = CGFloat(tempConstant)
        bottomConstraint.constant = CGFloat(-tempConstant)
        self.layoutIfNeeded()
        topConstraint.constant = top
        bottomConstraint.constant = bottom
        
        UIView.animate(withDuration: 1.2, delay: delay, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.7, options: [.curveEaseOut, .allowUserInteraction], animations: {
            //
            self.contentView.alpha = 1.0
            self.layoutIfNeeded()
        }) { (flag) in
            
            completion(self)
        }
        
        UIView.animate(withDuration: 0.2 , delay: delay, options: [.curveEaseOut], animations: {
            //
            self.imageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
        }) { (flag) in
            
            UIView.animate(withDuration: 0.3 , delay: 0, options: [.curveEaseOut], animations: {
                //
                self.imageView.transform = CGAffineTransform.identity
            })
        }
    }
    
    override func dismiss(completion: @escaping (FloatingMenuItemView) -> ()) {
        
        let buttonIndex = self.buttonIndex
        let delay = TimeInterval(buttonIndex) * 0.06
        
        let negativeValue = CGFloat(50+(5*buttonIndex))
        let positiveValue = CGFloat(54+(50*buttonIndex))
        
        topConstraint.constant = topConstraint.constant - negativeValue
        bottomConstraint.constant = topConstraint.constant + negativeValue
        
        UIView.animate(withDuration: 0.26 , delay: delay, options: [.curveEaseOut], animations: {
            //
            self.layoutIfNeeded()
        }) { (flag) in
            
            self.topConstraint.constant = positiveValue
            self.bottomConstraint.constant = -positiveValue
            
            UIView.animate(withDuration: 0.32, animations: {
                self.contentView.alpha = 0.0
                self.layoutIfNeeded()
            }) { (flag) in
                completion(self)
            }
        }
        
        UIView.animate(withDuration: 0.6 , delay: delay, options: [.curveEaseOut], animations: {
            //
            self.imageView.transform = CGAffineTransform(rotationAngle: -(CGFloat.pi/2))
        })
    }
    
    @IBAction func _didTapped(_ sender :Any?) {
        
        guard let floatingMenu = floatingMenu else { return }
        
        onTap?(floatingMenu, self)
    }
    
    @discardableResult public func set(image :UIImage?) ->RightSideSlideUpMenuItem {
        
        self.imageView.image = image
        return self
    }
    
    @discardableResult public func set(title :String?) ->RightSideSlideUpMenuItem {
        self.label.font = UIFont(name: appFont, size: 14.0)
        self.label.text = title?.localized
        return self
    }
}


