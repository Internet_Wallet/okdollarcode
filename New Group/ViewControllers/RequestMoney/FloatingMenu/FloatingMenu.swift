//
//  FloatingMenu.swift
//  OK
//
//  Created by palnar on 13/01/18.
//  Copyright © 2018 T T Marshel Daniel. All rights reserved.
//

import UIKit

open class FloatingButton :UIButton {
    
    public typealias ActionClosure = (FloatingButton) ->()
    @IBOutlet var xConstraint :NSLayoutConstraint!
    @IBOutlet var yConstraint :NSLayoutConstraint!
    
    public var whenOpen :ActionClosure?
    public var whenClose :ActionClosure?
    
    fileprivate func opening() {
        
        guard let whenOpen = self.whenOpen else { return }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.02) {
            whenOpen(self)
        }
    }
    
    fileprivate func closing() {
        
        self.whenClose?(self)
    }
    
    public func defaultOpen() {
        
        UIView.animate(withDuration: 1.2, delay: 0, usingSpringWithDamping: 0.25, initialSpringVelocity: 2.5, options: [.curveEaseOut, .allowUserInteraction], animations: {
            //
            self.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
        })
        
        UIView.animate(withDuration: 0.12, animations: {
            
        }, completion: { (flag) in
            self.isSelected = true
        })
    }
    
    public func defaultClose() {
        
        self.isSelected = false
        
        UIView.animate(withDuration: 2.6, delay: 0, usingSpringWithDamping: 0.25, initialSpringVelocity: 2.5, options: [.curveEaseOut, .allowUserInteraction], animations: {
            
            self.transform = CGAffineTransform.identity
        })
    }
}

open class FloatingMenuItemView :UIView {
    
    public weak var floatingMenu :FloatingMenu?
    fileprivate(set) var buttonIndex :Int = 0
     
    open func present(completion: @escaping (FloatingMenuItemView) -> ()) { return }
    
    open func dismiss(completion: @escaping (FloatingMenuItemView) -> ()) { return }
}

public class FloatingMenu: KeyBoardManagedVC {
    
    @IBOutlet var overlayView :UIView!
    @IBOutlet var contentView :UIView!
    @IBOutlet var menuStackView :UIStackView!
    @IBOutlet var stackViewTopConstraint :NSLayoutConstraint!
    @IBOutlet var stackViewBottomConstraint :NSLayoutConstraint!
    
    fileprivate var _spaceFillView :UIView?
    fileprivate var _settings :Settings = Settings(position: .bottomRight(10, 10), padingFromFloatingButton: 12, menuSpacing: 12, floatingButton: FloatingButton())
    fileprivate var _menuItems :[FloatingMenuItemView]?
    
    fileprivate var _floatingButton :FloatingButton?
    
    public init(settings:Settings) {
        
        super.init(nibName: "FloationMenu", bundle: nil)
        self._settings = settings
        self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func viewDidLoad() {
        
        super.viewDidLoad()
        _addAll()
    }
    
    override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        dismiss()
    }
    
    public override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.view.alpha = 0.0
        }) { (flag) in
            
            self.view.isHidden = true
            super.dismiss(animated: flag, completion: completion)
        }
        
        UIView.animate(withDuration: 0.25, animations: {
            self._settings.floatingButton.alpha = 1.0
        })
    }
    
    func dismiss(_ completion: (() -> Void)? = nil) {
        
        _floatingButton?.isUserInteractionEnabled = false
        _floatingButton?.closing()
        
        guard let menuItems = _menuItems else {
            
            self.dismiss(animated: false, completion: completion)
            return
        }
        
        _dismisMenuItem(fromItems: menuItems.reversed(), atIndex: 0, completion) 
    }
}

extension FloatingMenu {
    
    @discardableResult public func show(On VC :UIViewController) ->FloatingMenu {
        
        VC.present(self, animated: false) { [weak self] in
            
            let overlayView = self?.overlayView
            let alpha = overlayView?.alpha ?? 0.0
            overlayView?.alpha = 0.0
            
            UIView.animate(withDuration: 0.2) {
                overlayView?.alpha = alpha
            }
            guard let menuItems = self?._menuItems else { return }
            
            self?._presentMenuItem(fromItems: menuItems)
            
        }
        return self
    }
    
    private func _presentMenuItem(fromItems items :[FloatingMenuItemView]) {
        
        for item in items {
            item.present(completion: { (item_) in
                return
            })
        }
    }
    
    private func _dismisMenuItem(fromItems items :[FloatingMenuItemView], atIndex index :Int, _ completion: (() -> Void)? = nil) {
        
        let count = items.count
        var index = 0
        
        for (index, menuItem) in items.reversed().enumerated() {
            menuItem.buttonIndex = index
        }
        
        for item in items {
            item.dismiss(completion: { [weak self] (item_) in
                
                index = index+1
                if index == count {
                    self?.dismiss(animated: false, completion: completion)
                }
            }) 
        }
    }

    @discardableResult public func set(menuItems :[FloatingMenuItemView]?) ->FloatingMenu {
        
        _removeAll()
        self._menuItems = menuItems
        _addAll()
        return self
    }
    
    private func _configureFor(bottom settings :Settings, contentView :UIView, stackView :UIStackView,  floationButton :FloatingButton, duplicateButton :FloatingButton) {
        
        switch settings.position {
            
        case .bottomRight(let right, let bottom):
            _configureFor(bottomRightSettings: settings, contentView: contentView, stackView: stackView, floationButton: floationButton, duplicateButton: duplicateButton, right: right, bottom: bottom)
            break
            
        default: break
        }
    }
    
    private func _configureFor(topRight settings :Settings, contentView :UIView, stackView :UIStackView,  floationButton :FloatingButton, duplicateButton :FloatingButton) {
        
        switch settings.position {
            
        case .topRight(let top, let right):
            _configureFor(topRightSettings: settings, contentView: contentView, stackView: stackView, floationButton: floationButton, duplicateButton: duplicateButton, top: top, right: right)
            break
            
        default: break
        }
    }
    
    private func _add(menuItems items :[FloatingMenuItemView], forSettings settings :Settings, contentView :UIView, stackView :UIStackView,  floationButton :FloatingButton, duplicateButton :FloatingButton) {
        
        var menuItems = items
        let isFromTop = _isMenuFromTop()
        let flexibleSpace = _spaceView()
        
        if isFromTop == false {
            
            _configureFor(bottom: settings, contentView: contentView, stackView: stackView, floationButton: floationButton, duplicateButton: duplicateButton)
            stackView.addArrangedSubview(flexibleSpace)
            
            for (index, menuItem) in menuItems.enumerated() {
                menuItem.buttonIndex = index
            }
            menuItems = menuItems.reversed()
        } else {
            for (index, menuItem) in menuItems.enumerated() {
                menuItem.buttonIndex = index
            }
        }
        
        for menuItem in menuItems {
            
            menuItem.floatingMenu = self
            stackView.addArrangedSubview(menuItem)
        }
        
        if isFromTop == true {
            
            _configureFor(topRight: settings, contentView: contentView, stackView: stackView, floationButton: floationButton, duplicateButton: duplicateButton)
            stackView.addArrangedSubview(flexibleSpace)
        }
        
        duplicateButton.opening()
    }
    
    private func _addAll() {
        
        guard let contentView = self.contentView, let stackView = menuStackView, let items = self._menuItems else { return }
        
        let settings = self._settings
        let floationButton = settings.floatingButton
        let duplicateButton = _duplicate(Of: floationButton)
        
        stackView.spacing = settings.menuSpacing
        floationButton.alpha = 0.0
        duplicateButton.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(duplicateButton)
        
        _add(menuItems: items, forSettings: settings, contentView: contentView, stackView: stackView, floationButton: floationButton, duplicateButton: duplicateButton)
        self._floatingButton = duplicateButton
    }
    
    private func _duplicate(Of button :FloatingButton) ->FloatingButton {
        
        let duplicate = FloatingButton()
        
        duplicate.setImage(button.image(for: .normal), for: .normal)
        duplicate.setImage(button.image(for: .selected), for: UIControl.State.selected)
        duplicate.whenOpen = button.whenOpen
        duplicate.whenClose = button.whenClose
        duplicate.addTarget(self, action: #selector(_dismiss(_:)), for: UIControl.Event.touchUpInside)
        
        return duplicate
    }
    
    @objc
    private func _dismiss(_ sender :FloatingButton) {
        dismiss()
    }
    
    private func _configureFor(topRightSettings settings :Settings, contentView :UIView, stackView :UIStackView,  floationButton :FloatingButton, duplicateButton :FloatingButton, top :CGFloat, right :CGFloat) {
        
        let size = floationButton.frame.size
        
        let topC = NSLayoutConstraint(item: duplicateButton, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1, constant: top)
        
        let trailingC = NSLayoutConstraint(item: duplicateButton, attribute: .trailing, relatedBy: .equal, toItem: contentView, attribute: .trailing, multiplier: 1, constant: -right)
        
        let widthC = NSLayoutConstraint(item: duplicateButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: size.width)
        
        let heightC = NSLayoutConstraint(item: duplicateButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: size.height)
        
        let stackViewPin = NSLayoutConstraint(item: stackView, attribute: .top, relatedBy: .equal, toItem: duplicateButton, attribute: .bottom, multiplier: 1, constant: settings.padingFromFloatingButton)
        
        stackViewTopConstraint.isActive = false
        contentView.addConstraints([topC, trailingC, widthC, heightC, stackViewPin])
        
        
    }
    
    private func _configureFor(bottomRightSettings settings :Settings, contentView :UIView, stackView :UIStackView,  floationButton :FloatingButton, duplicateButton :FloatingButton, right :CGFloat, bottom :CGFloat) {
        
        let size = floationButton.frame.size
        
        let trailingC = NSLayoutConstraint(item: duplicateButton, attribute: .trailing, relatedBy: .equal, toItem: contentView, attribute: .trailing, multiplier: 1, constant: -right)
        
        let bottomC = NSLayoutConstraint(item: duplicateButton, attribute: .bottom, relatedBy: .equal, toItem: contentView, attribute: .bottom, multiplier: 1, constant: -bottom)
        
        let widthC = NSLayoutConstraint(item: duplicateButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: size.width)
        
        let heightC = NSLayoutConstraint(item: duplicateButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: size.height)
        
        let stackViewPin = NSLayoutConstraint(item: stackView, attribute: .bottom, relatedBy: .equal, toItem: duplicateButton, attribute: .top, multiplier: 1, constant: -settings.padingFromFloatingButton)
        
        stackViewBottomConstraint.isActive = false
        contentView.addConstraints([trailingC, bottomC, widthC, heightC, stackViewPin])
    }
    
    private func _spaceView() ->FloatingMenuItemView {
        
        let view = FloatingMenuItemView()
        view.backgroundColor = UIColor.clear
        return view
    }
    
    private func _isMenuFromTop() -> Bool {
        
        var isTopAllign = true
        
        switch _settings.position {
        case .bottomRight(_, _):
            isTopAllign = false
        case .bottomLeft(_, _):
            isTopAllign = false
        default: break
        }
        return isTopAllign
    }
    
    
    private func _removeAll() {
        
        guard let menuItems = self._menuItems else { return }
        let stackView = menuStackView
        for menuItem in menuItems {
            
            stackView?.removeArrangedSubview(menuItem)
            menuItem.removeFromSuperview()
        }
    }
    
}

extension FloatingMenu {
    
    public typealias Top = CGFloat
    public typealias Right = CGFloat
    public typealias Bottom = CGFloat
    public typealias Left = CGFloat
    
    public enum MenuPosition {
        
        case topLeft(Top, Left)
        case topRight(Top, Right)
        case bottomLeft(Left, Bottom)
        case bottomRight(Right, Bottom)
    }
    
    public struct Settings {
        
        let position :MenuPosition
        let padingFromFloatingButton :CGFloat
        let menuSpacing :CGFloat
        let floatingButton :FloatingButton
    }
}
