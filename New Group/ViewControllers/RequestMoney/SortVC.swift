//
//  SortVC.swift
//  OK
//
//  Created by palnar on 18/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
protocol SortCustomNavigationDelegate {
    func sortOptionSelected(withCategory category: String , isAscending : Bool)
}
class SortVC: UIViewController {
    //MARK: - Properties
    fileprivate var sortItems  = [SortItem]()
    //MARK-  Outlets
    @IBOutlet weak var sortTableView: UITableView!
    var delegate : SortCustomNavigationDelegate?
    
    @IBOutlet weak var remainingView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.init(red:0 , green: 0, blue: 0, alpha: 0.6)
        //view.isOpaque = false
        configureSortTableView()
        dismissWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    //Sort Item Lists Configuration
    private func configureSortTableView(){
        
        let sortItem1 = SortItem.init(name: "Default")
        let sortItem2 = SortItem.init(name: "Amount High to Low")
        let sortItem3 = SortItem.init(name: "Amount Low to High")
        let sortItem4 = SortItem.init(name: "Name A to Z")
        let sortItem5 = SortItem.init(name: "Name Z to A")
       
        sortItems.append(sortItem1!)
        sortItems.append(sortItem2!)
        sortItems.append(sortItem3!)
        sortItems.append(sortItem4!)
        sortItems.append(sortItem5!)
        
        self.sortTableView.register(SortItemCellView.self as AnyClass, forCellReuseIdentifier: "sortOption")
        
        sortTableView.dataSource  = self
        sortTableView.delegate = self
        sortTableView.sectionHeaderHeight = 50
        sortTableView.tableFooterView = UIView(frame: CGRect.zero)
        
    }
    
    func dismissWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(dismissController))
        tap.cancelsTouchesInView = false
        self.remainingView.addGestureRecognizer(tap)
    }
    @objc func dismissController() {
        self.dismiss(animated: true, completion:nil)
    }
    


}
extension SortVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortItems.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let filtetItemModel = sortItems [indexPath.row]
        
        var cell: SortItemCellView? = tableView.dequeueReusableCell(withIdentifier: "sortOption", for: indexPath as IndexPath) as? SortItemCellView
        
        if cell == nil {
             cell = SortItemCellView(style: .default, reuseIdentifier: "sortOption")
        }
        cell?.textLabel?.font = UIFont(name: appFont, size: appFontSize)
        cell?.textLabel?.text = filtetItemModel.name.localized
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0: print ("Default Category ")
        case 1 : delegate?.sortOptionSelected(withCategory: "Amount", isAscending: false)
        case 2 : delegate?.sortOptionSelected(withCategory: "Amount", isAscending: true)
        case 3 : delegate?.sortOptionSelected(withCategory: "Name", isAscending: true)
        case 4 : delegate?.sortOptionSelected(withCategory: "Name", isAscending: false)
        default:
            println_debug("default")
        }
    
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        let divider = UIView(frame: CGRect(x: 0, y: (headerView.frame.size.height - CGFloat(1)), width: tableView.frame.size.width, height: 1))
        divider.backgroundColor = UIColor.gray
        label.text = "Sort By".localized
        
        label.textColor = UIColor.black
        label.font = UIFont(name: appFont, size: appFontSize)
        label.textAlignment = .center
        
        headerView.backgroundColor = UIColor.white
        headerView.addSubview(label)
        headerView.addSubview(divider)
        
        return headerView
}
}
