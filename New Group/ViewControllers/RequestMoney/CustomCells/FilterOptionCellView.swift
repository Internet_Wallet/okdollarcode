//
//  FilterOptionCell.swift
//  OK
//
//  Created by palnar on 15/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class FilterOptionCellView: UITableViewCell {

    @IBOutlet weak var dividerLine: UIView!
    @IBOutlet weak var leftLabelConstraint: NSLayoutConstraint!
    @IBOutlet weak var downArrow: UIImageView!
    @IBOutlet weak var name: UILabel!{
        didSet{
            name.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }

}
