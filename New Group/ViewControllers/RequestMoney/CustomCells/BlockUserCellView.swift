//
//  BlockUserCellViewTableViewCell.swift
//  OK
//
//  Created by palnar on 18/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
protocol BlocktableViewCellDelegate : class {
    func tableViewCellDidTapBlock(_ sender : BlockUserCellView )
}
class BlockUserCellView
: UITableViewCell {

    @IBOutlet weak var leftImage: UIImageView!
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var mobileNumber: UILabel!
    
    @IBOutlet weak var actionBlock: UIButton!
    
    weak var delegate :BlocktableViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
    
    @IBAction
    func blockButtonTapped(_ sender : UIButton ){
        delegate?.tableViewCellDidTapBlock(self)
    }

}
