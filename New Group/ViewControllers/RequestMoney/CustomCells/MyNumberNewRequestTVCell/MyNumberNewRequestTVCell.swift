//
//  MyNumberRequestTVCell.swift
//  Sample
//
//  Created by T T Marshel Daniel on 18/01/18.
//  Copyright © 2018 T T Marshel Daniel. All rights reserved.
//

import UIKit

protocol MyNumberNewRequestTVCellDeligate :class {
    
    func myNumberNewRequestTVCel(_ cell :MyNumberNewRequestTVCell, didTapOnContactButton sender :UIButton)
    func myNumberNewRequestTVCel(_ cell :MyNumberNewRequestTVCell, textDidChangedOnContactNumber text :String?)
    func myNumberNewRequestTVCel(_ cell :MyNumberNewRequestTVCell, textDidChangedOnAmount text :String?)
    func myNumberNewRequestTVCel(_ cell :MyNumberNewRequestTVCell, textDidChangedRemark text :String?)
    func myNumberNewRequestTVCel(_ cell :MyNumberNewRequestTVCell, needNewAttachementAtIndex index :Int)
    func myNumberNewRequestTVCel(_ cell :MyNumberNewRequestTVCell, didTapOnRequestNowRadioButton sender :MyNumberNewRequestRadioButton)
//    func myNumberNewRequestTVCel(_ cell :MyNumberNewRequestTVCell, didTapOnSheduleRequestNowRadioButton sender :MyNumberNewRequestRadioButton)
}
 
class MyNumberNewRequestRadioButton :UIControl {
    
    @IBOutlet weak var radioIconView :UIImageView!
    @IBOutlet weak var label :UILabel!{
        didSet{
            label.font  = UIFont(name: appFont, size: appFontSize)
        }
    }
}

class MyNumberNewRequestTVCell: UITableViewCell {
    
    @IBOutlet weak var numberContainer :UIView!
    @IBOutlet weak var requestOptionSelectionContainer :UIView!
    @IBOutlet weak var optionsContainer :UIStackView!
    @IBOutlet weak var amountContainer :UIView!
    @IBOutlet weak var remarkContainer :UIView!
    @IBOutlet weak var attachementsContainer: UIStackView!
    @IBOutlet weak var requestTypeRadioButtonContainer: UIStackView!
    @IBOutlet weak var sheduleRadioButtonContainer: UIStackView!
    @IBOutlet weak var scheduleOptionsContainer: UIView!
    @IBOutlet weak var datePickerContainer : UIView!
    @IBOutlet weak var frequencyContainer : UIView!
    @IBOutlet weak var frequencySelectionContainer : UIView!
    @IBOutlet weak var frequencyListContainer : UIStackView!
    
    @IBOutlet weak var dropDownArrow: UIImageView!
    @IBOutlet weak var contactsButton: UIButton!
    
    @IBOutlet weak var contactNumberSearchField: SearchTextField!{
        didSet{
            contactNumberSearchField.titleFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
        }
    }
    @IBOutlet weak var selectedMethodeTextField: FloatLabelTextField!{
        didSet{
            selectedMethodeTextField.titleFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
        }
    }
    @IBOutlet weak var amountTextField: FloatLabelTextField!{
        didSet{
            amountTextField.titleFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
        }
    }
    @IBOutlet weak var remarksTextField: FloatLabelTextField!{
        didSet{
            remarksTextField.titleFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
        }
    }
    
    @IBOutlet var responderChain :[UITextField]!
    
    @IBOutlet weak var currentFocus :UITextField?
    
    weak var tableView :UITableView!
    
    private var _selectedMethodeTextFieldPlaceHolder :String?
    
    weak var deligate :MyNumberNewRequestTVCellDeligate?
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.clipsToBounds = true
        _initilize()
    }
    
    private func _initilize() {
        
        _selectedMethodeTextFieldPlaceHolder = selectedMethodeTextField.placeholder
        selectedMethodeTextField.placeholder = nil
        
        requestOptionSelectionContainer.isHidden = true
        optionsContainer.isHidden = true
        optionsContainer.isHidden = true
        amountContainer.isHidden = true
        remarkContainer.isHidden = true
        attachementsContainer.isHidden = true
        
        requestTypeRadioButtonContainer.isHidden = true
        sheduleRadioButtonContainer.isHidden = true
        datePickerContainer.isHidden = true
        frequencyContainer.isHidden = true
        scheduleOptionsContainer.isHidden = true
//        frequencySelectionContainer.isHidden = true
//        frequencyListContainer.isHidden = true
    }
    
    @IBAction func optionViewTapped(_ sender :UIControl) {
        
        _updateCell ({
            self._openOrCloseOption()
        })
        
    }
    
    @IBAction func okMoneyOptionTapped(_ sender: Any) {
        
        selectedMethodeTextField.text = "OK$ Money".localized
        selectedMethodeTextField.placeholder = _selectedMethodeTextFieldPlaceHolder?.localized
        self._closeRequestForOptions()
        _updateCell ({
            
            self._showAmountView()
        })
        self.currentFocus = self.amountTextField
        self.currentFocus?.becomeFirstResponder()
    }
    
    @IBAction func rechargeOptionTapped(_ sender: Any) {
        
        selectedMethodeTextField.text = "Request Recharge".localized
        selectedMethodeTextField.placeholder = _selectedMethodeTextFieldPlaceHolder?.localized
        self._closeRequestForOptions()
        _updateCell ({
            
            self._showAmountView()
        })
        self.currentFocus = self.amountTextField
        self.currentFocus?.becomeFirstResponder()
    }
    
    private func _showAmountView() {
        
        if amountContainer.isHidden == false  { return }
        self.amountContainer.isHidden = false
    }
    
    private func _showRemarksView() {
        
        if remarkContainer.isHidden == false  { return }
        
        self.remarkContainer.isHidden = false
        self.attachementsContainer.isHidden = false
        self.requestTypeRadioButtonContainer.isHidden = false
        
    }
    
    private func _showRequesForOptionsView() {
        
        if requestOptionSelectionContainer.isHidden == false  { return }
        
        self.requestOptionSelectionContainer.isHidden = false
    }
    
    @IBAction func contactButtonTapped(_ sender: Any) {
        
    }
    
    @IBAction func requestTypeRadioButtonTapped(_ sender: MyNumberNewRequestRadioButton) {
        
        guard let views = requestTypeRadioButtonContainer.subviews as? [MyNumberNewRequestRadioButton] else { return }
        
        if sender.tag == 0 {
            if sender.radioIconView.isHighlighted == false {
                _requestNowButtonTapped(sender: sender)
            }
        } else {
            if sender.radioIconView.isHighlighted == false {
                _sheduleRequestNowButtonTapped(sender: sender)
            }
        }
        
        for view in views {
            view.radioIconView.isHighlighted = false
        }
        views[sender.tag].radioIconView.isHighlighted = true
    }
    
    private func _requestNowButtonTapped(sender: MyNumberNewRequestRadioButton) {
        
        currentFocus?.resignFirstResponder()
        currentFocus = nil
        
        _updateCell(animationDuration: 0.7) {
            
            self.scheduleOptionsContainer.isHidden = true
            self.sheduleRadioButtonContainer.isHidden = true
            self.datePickerContainer.isHidden = true
            self.frequencyContainer.isHidden = true
        }
        
        deligate?.myNumberNewRequestTVCel(self, didTapOnRequestNowRadioButton: sender)
    }
    
    private func _sheduleRequestNowButtonTapped(sender: MyNumberNewRequestRadioButton) {
        
        currentFocus?.resignFirstResponder()
        currentFocus = nil
        
        _updateCell(animationDuration: 0.7) {
            self.sheduleRadioButtonContainer.isHidden = false
        } 
    }
    
    @IBAction func sheduleRadioButtonTapped(_ sender: MyNumberNewRequestRadioButton) {
        
        guard let views = sheduleRadioButtonContainer.subviews as? [MyNumberNewRequestRadioButton] else { return }
        
        if sender.tag == 0 {
            if sender.radioIconView.isHighlighted == false {
                _selectDateButtonTapped(sender: sender)
            }
        } else {
            if sender.radioIconView.isHighlighted == false {
                _frequencyButtonTapped(sender: sender)
            }
        }
        
        for view in views {
            view.radioIconView.isHighlighted = false
        }
        views[sender.tag].radioIconView.isHighlighted = true
    }
    
    private func _selectDateButtonTapped(sender: MyNumberNewRequestRadioButton) {
        
        _showDatePicker()
    }
    
    private func _frequencyButtonTapped(sender: MyNumberNewRequestRadioButton) {
        
        _showFrequencyOptions()
    }
    
    private func _showDatePicker() {

        if scheduleOptionsContainer.isHidden == false {
            
            self.isUserInteractionEnabled = false
            
            UIView.animate(withDuration: 0.225, delay: 0.0, options: [.curveEaseIn], animations: {
                
                self.scheduleOptionsContainer.alpha = 0.0
                
            }, completion: { (flag) in
                
                self.frequencyContainer.isHidden = true
                self.frequencyListContainer.isHidden = true
                self.datePickerContainer.isHidden = false
                
                UIView.animate(withDuration: 0.225, delay: 0.0, options: [.curveEaseIn], animations: {
                    
                    self.scheduleOptionsContainer.alpha = 1.0
                    
                }, completion: { (flag) in
                    
                    self.isUserInteractionEnabled = true
                })
            })
        } else {
            
            _updateCell {
                
                self.frequencyContainer.isHidden = true
                self.frequencyListContainer.isHidden = true
                self.datePickerContainer.isHidden = false
                self.scheduleOptionsContainer.isHidden = false
            }
        }
    }
 
    private func _showFrequencyOptions() {

        if scheduleOptionsContainer.isHidden == false {
            
            self.isUserInteractionEnabled = false
            
            UIView.animate(withDuration: 0.225, delay: 0.0, options: [.curveEaseIn], animations: {
                
                self.scheduleOptionsContainer.alpha = 0.0
                
            }, completion: { (flag) in
                
                self.datePickerContainer.isHidden = true
                self.frequencyContainer.isHidden = false
                self.frequencyListContainer.isHidden = false
                
                UIView.animate(withDuration: 0.225, delay: 0.0, options: [.curveEaseIn], animations: {
                    
                    self.scheduleOptionsContainer.alpha = 1.0
                    
                }, completion: { (flag) in
                    
                    self.isUserInteractionEnabled = true
                })
            })
        } else {
            
            _updateCell {
                
                self.datePickerContainer.isHidden = true
                self.frequencyContainer.isHidden = false
                self.frequencyListContainer.isHidden = false
                self.scheduleOptionsContainer.isHidden = false
            }
        }
    }
    
    private func _openRequestForOptions() {
        
        if self.optionsContainer.isHidden == true {
            
            self.optionsContainer.isHidden = false
            self.dropDownArrow.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        }
    }
    
    private func _closeRequestForOptions() {
        
        if self.optionsContainer.isHidden == false {
            
            self.optionsContainer.isHidden = true
            self.dropDownArrow.transform = CGAffineTransform.identity
        }
    }
    
    private func _openOrCloseOption() {
        
        if self.optionsContainer.isHidden == true {
            
            _openRequestForOptions()
        } else {
            _closeRequestForOptions()
        }
    }
    
    private func _updateCell(animationDuration duration :TimeInterval = 0.4, _ animation :@escaping () ->()) {
        
        self.isUserInteractionEnabled = false
        self.tableView.beginUpdates()
        animation()
        UIView.animate(withDuration: duration, delay: 0.0, options: [.curveEaseIn], animations: {
            
           // animation()
        }, completion: { (flag) in
            
            self.isUserInteractionEnabled = true
        })
        
        self.tableView.endUpdates()
    }
    
    private func _updateCell(animationDuration duration :TimeInterval = 0.3, _ animation :@escaping () ->(), _ completion :(() ->())? = nil) {
        
        self.isUserInteractionEnabled = false
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseIn], animations: {
            
            animation()
        }, completion: { (flag) in
            
            self.isUserInteractionEnabled = true
            completion?()
        })
    }
}

extension MyNumberNewRequestTVCell {
    
    @IBAction func textDidBeginEditing(_ sender: UITextField) {
        currentFocus = sender
    }
    
    @IBAction func textDidEndEditing(_ sender: UITextField) {
        
    }
    
    @IBAction func textDidChanged(_ sender :UITextField) {
        
        if sender === contactNumberSearchField {
            
            if _isValidContactNumber(text: sender.text) {
                
                currentFocus?.resignFirstResponder()
                currentFocus = nil
                _updateCell {
                    self._showRequesForOptionsView()
                }
            }
            //deligate?.myNumberNewRequestTVCel(self, textDidChangedOnContactNumber: sender.text)
        } else if sender === amountTextField {
            
            if _isValidAmount(text: sender.text) {
                
                _updateCell {
                    self._showRemarksView()
                }
            }
        } else if sender === remarksTextField {
            
            if _isValidRemark(text: sender.text) {
                
                
            }
        }
    }
    
    
    func _isValidContactNumber(text :String?) ->Bool {
        
        let charecterCount = text?.count ?? 0
        
        if charecterCount > 9 {
            return true
        } else {
            return false
        }
    }
    
    func _isValidAmount(text :String?) ->Bool {
        
        let charecterCount = text?.count ?? 0
        
        if charecterCount == 1 {
            return true
        } else {
            return false
        }
    }
    
    func _isValidRemark(text :String?) ->Bool {
        
        let charecterCount = text?.count ?? 0
        
        if charecterCount > 4 {
            return true
        } else {
            return false
        }
    }
}

