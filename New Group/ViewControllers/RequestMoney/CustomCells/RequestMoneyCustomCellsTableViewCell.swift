//
//  RequestMoneyCustomCellsTableViewCell.swift
//  OK
//
//  Created by palnar on 01/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol RequestMoneyCustomCellsDelegate {
    func dateWasSelected( selectedDateString: String)
    func requestMoneyCustomCell(_ cell :RequestMoneyCustomCellsTableViewCell, contactButtonTapped :Any)
}

 /// make the methodes as optional
extension RequestMoneyCustomCellsDelegate {
    
    func requestMoneyCustomCell(_ cell :RequestMoneyCustomCellsTableViewCell, contactButtonTapped :Any) { return }
}

public class RequestMoneyCustomCellsTableViewCell: UITableViewCell {

    //MARK: - IBOutlet Properties
    
    @IBOutlet weak var leftSectionLogo: UIImageView!
    
    @IBOutlet weak var sectionTitle: UILabel!
    
    @IBOutlet weak var sectionRightLogo: UIImageView!
    
    @IBOutlet weak var itemTextField: UITextField!
    
    @IBOutlet weak var itemButton: UIButton!
    
    @IBOutlet weak var textField: UILabel!
    
    @IBOutlet weak var imgTextField: UIImageView!
    
    @IBOutlet weak var datePicker: UIDatePicker!{
        didSet {
            if #available(iOS 13.4, *) {
                datePicker.preferredDatePickerStyle = .wheels
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    @IBOutlet weak var searchTextField: SearchTextField!
    
    @IBOutlet weak var rightContactButton: UIButton!
    
    
    //MARK:- Variables
    var delegate: RequestMoneyCustomCellsDelegate!
  
    
    @IBOutlet weak var sectionRadioButton: OKRadioButton!
    override public func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   /* override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }*/
    
    //MARK:- custom functions
    // MARK: IBAction Functions
    
    @IBAction func setDate(sender: Any) {
        //let dateFormatter = defaultDateFormat
       // dateFormatter.dateStyle = DateFormatter.Style.long
        let dateString = stringFromDate(date: datePicker.date as NSDate, format: GlobalConstants.Strings.defaultDateFormat)
        
        if delegate != nil {
            delegate.dateWasSelected(selectedDateString: dateString)
        }
    }
    
    @IBAction func contactButtonTapped(sender: Any) {
        
        if delegate != nil {
            delegate.requestMoneyCustomCell(self, contactButtonTapped: sender)
        }
    }

}
