//
//  CustomPendingRequestCellTableViewCell.swift
//  OK
//
//  Created by palnar on 12/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
protocol PendingRequesttableViewCellDelegate : class {
    func tableViewCellDidTapRemind(_ sender : CustomPendingRequestCellTableViewCell )
    func tabeleViewCellDidTapChangeStauts(_ sender : CustomPendingRequestCellTableViewCell , requestMoneyStatus : RequestedMoneyStatus)
    func tableViewCellDidTapBlockOrUnblockRequest(_sender : CustomPendingRequestCellTableViewCell,isNeedToBeBlock : Bool)
    func numberRequestTVCell(_sender : CustomPendingRequestCellTableViewCell, didTapOnAttachementLabel label :UIView)
}
class CustomPendingRequestCellTableViewCell: UITableViewCell {
    
    //MARK: - Properties
    weak var delegate :PendingRequesttableViewCellDelegate?

    @IBOutlet weak var imgUserLogo: UIImageView!
    //Key
    @IBOutlet weak var lblCustomerNameKey: UILabel!
    {
        didSet
        {
            lblCustomerNameKey.text = lblCustomerNameKey.text?.localized
            lblCustomerNameKey.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblCategoryKey: UILabel!
        {
        didSet
        {
            lblCategoryKey.text = lblCategoryKey.text?.localized
            lblCategoryKey.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblMobileNumberKey: UILabel!
        {
        didSet
        {
            lblMobileNumberKey.text = lblMobileNumberKey.text?.localized
            lblMobileNumberKey.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblRequestTYpeKey: UILabel!
        {
        didSet
        {
            lblRequestTYpeKey.text = lblRequestTYpeKey.text?.localized
            lblRequestTYpeKey.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblAttachedFileKey: UILabel!
        {
        didSet
        {
            lblAttachedFileKey.text = lblAttachedFileKey.text?.localized
            lblAttachedFileKey.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblAmountKey: UILabel!
        {
        didSet
        {
            lblAmountKey.text = lblAmountKey.text?.localized
            lblAmountKey.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblRemarksKey: UILabel!
        {
        didSet
        {
            lblRemarksKey.text = lblRemarksKey.text?.localized
            lblRemarksKey.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblDateKey: UILabel!
        {
        didSet
        {
            lblDateKey.text = lblDateKey.text?.localized
            lblDateKey.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    //Value
    @IBOutlet weak var lblCustomerName: UILabel!{
        didSet{
            lblCustomerName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var lblCategory: UILabel!{
        didSet{
            lblCategory.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var lblRequestTYpe: UILabel!{
        didSet{
            lblRequestTYpe.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var lblType: UILabel!{
        didSet{
            lblType.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var statusLabel: UILabel!{
        didSet{
            statusLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
   
    @IBOutlet weak var lblAttachedFile: UILabel!{
        didSet{
            lblAttachedFile.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblMobileNumber: UILabel!{
        didSet{
            lblMobileNumber.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var blockUnBlock: UIButton!
    
    @IBOutlet weak var bottomStackView: UIStackView!
   
    @IBOutlet weak var remindButton: UIButton!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var lblAmount: UILabel!{
        didSet{
            lblAmount.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var lblDate: UILabel!{
        didSet{
            lblDate.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblRemarks: UILabel!{
        didSet{
            lblRemarks.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var viewGuesture: UIView!
    
    @IBOutlet weak var blockLabel: UILabel!{
        didSet{
            blockLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
     @IBOutlet weak var remarksStackView: UIStackView!
    
    @IBOutlet weak var lblReminderCount: BadgeLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        cancelButton.setTitle(cancelButton.titleLabel?.text?.localized, for: .normal)
        //remindButton.setTitle("Pay".localized, for: .normal)
        _addTapGestureOn(label: viewGuesture)
        // Initialization code
    }
    
   /* override func setSelected(_ selected: Bool, animated: Bool) {
        //super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }*/
    
    private func _addTapGestureOn(label :UIView?) {
        
        let gester = UITapGestureRecognizer(target: self, action: #selector(showAttachedFiles(_:)))
        label?.addGestureRecognizer(gester)
    }
    
    @objc fileprivate func showAttachedFiles(_ sender: UITapGestureRecognizer) {
        
        guard let label = viewGuesture else { return }
        delegate?.numberRequestTVCell(_sender: self, didTapOnAttachementLabel: label)
    }
    
    @IBAction
    func remindButtonTapped(sender : UIButton ){
        delegate?.tableViewCellDidTapRemind(self)
    }
    
    @IBAction
    func changeStatusButtonTapped(sender : UIButton ){
        delegate?.tabeleViewCellDidTapChangeStauts(self, requestMoneyStatus: RequestedMoneyStatus.CANCELLED)
    }
    
    @IBAction
    func blockUnblockButtonTapped(sender : UIButton ){
        if sender.isSelected {
            //Block state - showing UnBlock Image
           
            //sender.isSelected = false
            delegate?.tableViewCellDidTapBlockOrUnblockRequest(_sender: self, isNeedToBeBlock: false)
        }else {
            //UnBlock state - Showing Block Image
            // User want to block
            delegate?.tableViewCellDidTapBlockOrUnblockRequest(_sender: self, isNeedToBeBlock: true)
            //sender.isSelected = true
        }
    }

}
