//
//  CriteriaFilterItemCellView.swift
//  OK
//
//  Created by palnar on 14/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class CriteriaFilterItemCellView: UICollectionViewCell {
    
    
    @IBOutlet weak var leftIcon: UIImageView!
    @IBOutlet weak var selectedIcon: UIImageView!

    @IBOutlet weak var criteriaTitle: UILabel!
}
