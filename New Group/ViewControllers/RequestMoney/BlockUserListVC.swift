//
//  BlockUserListVC.swift
//  OK
//
//  Created by palnar on 18/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BlockUserListVC: OKBaseController {
    
    //MARK: - Properties
    fileprivate var blockUserList  =  [BlockUserItem]()
    
    
    //MARK: Outlets
    @IBOutlet weak var blockListTableView: UITableView!
    @IBOutlet weak var noRecordsView: UIView!
  @IBOutlet weak var lblNoRecords: UILabel!
  {
    didSet
    {
      lblNoRecords.text = "No records found!".localized
    }
  }

    //Mark : - Override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        getAllBlockContactList()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //Mark : - Configure Table View
     func configureTableView(){
        configureBackButton(withTitle: "Block User Lists".localized)
        blockListTableView.delegate = self
        blockListTableView.dataSource = self
        blockListTableView.register(UINib(nibName : "BlockUserCell" ,bundle : nil ), forCellReuseIdentifier: "block_user_list")
         blockListTableView.tableFooterView = UIView(frame: CGRect.zero)
        
    }

}

//Mark: - Table View Delegates
extension BlockUserListVC : UITableViewDelegate,UITableViewDataSource,BlocktableViewCellDelegate {
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return blockUserList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "block_user_list", for: indexPath as IndexPath) as! BlockUserCellView
        let rowData = blockUserList[indexPath.row]
       
        if let imageStringUrl = rowData.ProfilePic?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
        if let imageUrl:NSURL = NSURL(string:imageStringUrl) {
        
        DispatchQueue.global(qos: .userInitiated).async {
            do {
            let imageData:NSData = try NSData(contentsOf: imageUrl as URL)
            
            
            DispatchQueue.main.async {
                
                let image = UIImage(data: imageData as Data)
                cell.leftImage.image = image
                cell.leftImage.contentMode = UIView.ContentMode.scaleAspectFit
                cell.leftImage.setRounded()
               
            }
            }catch {
                println_debug(error)
            }
            }
        }
        }
        cell.name.text =  rowData.UserName
        cell.mobileNumber.text = rowData.DestinationNum
        cell.delegate = self
        cell.actionBlock.roundedButton()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }
    
    func tableViewCellDidTapBlock(_ sender: BlockUserCellView) {
        guard let tappedIndexPath = blockListTableView.indexPath(for: sender) else { return }
//        println_debug("Block ", sender, tappedIndexPath)
        
        
        let message = "Do you want to UnBlock Request Money from this User?".localized
    
    
    alertViewObj.wrapAlert(title: "", body: message, img: UIImage(named : "confirmation_icon"))
    
    alertViewObj.addAction(title: "Cancel".localized, style: AlertStyle.cancel, action: {
    
    })
    alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
        let selectedRowModel = self.blockUserList[tappedIndexPath.row]
        if let desNum = selectedRowModel.DestinationNum {
            self.blockOrUnblockParticular(number: desNum)
        }
    
    
    
    })
    alertViewObj.showAlert(controller: self)
        // Delete the row
        //items.remove(at: tappedIndexPath.row)
        //tableView.deleteRows(at: [tappedIndexPath], with: .automatic)
    }
}
//MARK:-  Webservice integration
extension BlockUserListVC {
    
    var hideNoRecordsView :Bool {
        get {
            return noRecordsView.isHidden
        } set {
            noRecordsView.isHidden = newValue
        }
    }
    
    func getAllBlockContactList(){
        
        progressViewObj.showProgressView()
        self.blockUserList.removeAll()
        //self.blockListTableView.reloadData()
       guard let apiForAllBlockContactList : URL = RequestMoneyParams.getAPIForAllBlockContactList(limit: nil, offset: nil, isRequestModule: true) else {
        
           hideNoRecordsView = false
            return
        }
        println_debug("api \(apiForAllBlockContactList.absoluteString) ")
        
        JSONParser.GetApiResponse(apiUrl: apiForAllBlockContactList, type: "POST") { (status : Bool, data : Any?) in
            DispatchQueue.main.async(execute: {
                progressViewObj.removeProgressView()
            })
            if(status) {
                
                if let dataArray = data as? Array<Any> {
                    for dataAnyRow in dataArray {
                        if let dataDictionaryRow = dataAnyRow as? Dictionary<String,Any> {
                            self.blockUserList.append(BlockUserItem.wrapModel(dic: dataDictionaryRow))
                        }
                    }
                }
                
                
                DispatchQueue.main.async(execute: {
                    
                    if self.blockUserList.count > 0 {
                        self.hideNoRecordsView = true
                    } else {
                        self.hideNoRecordsView = false
                    }
                    self.blockListTableView.reloadData()
                })
                
                
            } else {
                
                self.hideNoRecordsView = false
                /* let responseMessage = data as? String
                 DispatchQueue.main.async(execute: {
                 // work Needs to be done
                 alertViewObj.wrapAlert(title: "Warning", body: responseMessage!, img: UIImage(named : "AppIcon"))
                 alertViewObj.addAction(title: "OK", style: AlertStyle.cancel, action: {
                 
                 })
                 alertViewObj.showAlert(controller: self)
                 })*/
                
            }
            
        }
    }
    
    func blockOrUnblockParticular(number desNum : String ) {
        progressViewObj.showProgressView()
        guard let apiForBlockUnblockParticularNumber : URL = RequestMoneyParams.blockUnblockParticularNumber(isBlocked: false, DesNum: desNum) else {
            return
        }
        println_debug("api \(apiForBlockUnblockParticularNumber.absoluteString) ")
        
        JSONParser.GetCancelApiResponse(apiUrl: apiForBlockUnblockParticularNumber, type : "GET") {  (status : Bool, data : Any?) in
            DispatchQueue.main.async(execute: {
                progressViewObj.removeProgressView()
            })
            if(status){
                    self.getAllBlockContactList()
            }else{
                if let rootContent = data  as? NSDictionary {
                    if let message =   rootContent["Msg"] as? String {
                        DispatchQueue.main.async(execute: {
                            // work Needs to be done
                            alertViewObj.wrapAlert(title: "Warning".localized, body: message.localized, img: UIImage(named : "AppIcon"))

                            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                                
                            })
                            alertViewObj.showAlert(controller: self)
                        })
                    }
                }
                
            }
            
        }
    }
}
