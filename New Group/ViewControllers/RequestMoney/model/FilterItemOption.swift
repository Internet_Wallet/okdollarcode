//
//  FilterItemOption.swift
//  OK
//
//  Created by palnar on 15/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import Foundation

class FilterItemOption {
    
    var name: String
    var isExapandable : Bool
    var isExpanded : Bool
    var isVisible : Bool
    var secondaryTitle : String?
    var key : String
    var isDividerVisible : Bool?
    
    
    
    
    init?(name: String,isExapandable : Bool , isExpanded : Bool, isVisible : Bool , key : String) {
        self.name = name
        self.isExapandable = isExapandable
        self.isExpanded = isExpanded
        self.isVisible = isVisible
        self.key = key
        if key == "1"{
        isDividerVisible = true
        }else{
        isDividerVisible = false
        }
        
        
        
    }
    
    
    
}
