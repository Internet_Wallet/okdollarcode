//
//  CriteriaFilterItem.swift
//  OK
//
//  Created by palnar on 14/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class CriteriaFilterItem {
    
    var name: String
    var photo: UIImage?
    
    
    
    init?(name: String, photo: UIImage?) {
        self.name = name
        self.photo = photo
    }
    
    

}
