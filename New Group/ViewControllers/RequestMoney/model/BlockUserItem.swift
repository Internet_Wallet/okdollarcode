//
//  BlockUserItem.swift
//  OK
//
//  Created by palnar on 27/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import Foundation

class BlockUserItem {
    var UserName       :String?
    var ProfilePic     :String?
    var UserId         :String?
    var DestinationNum :String?
    
    private init(){
        
    }
    class func wrapModel(dic: Dictionary<String,Any>) -> BlockUserItem {
        let blockUserItem  = BlockUserItem.init()
        blockUserItem.UserName       = dic["UserName"] as? String
        blockUserItem.ProfilePic     = dic["ProfilePic"] as? String
        blockUserItem.UserId         = dic["UserId"] as? String
        blockUserItem.DestinationNum = dic["DestinationNum"] as? String
        return blockUserItem
    }
    
}
