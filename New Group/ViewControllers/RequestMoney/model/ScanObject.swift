//
//  ScanObject.swift
//  OK
//
//  Created by palnar on 09/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation
 
class ScanObject  : NSObject {
    var name : String
    var number : String
    var amount : String?
    var remark : String?
    
    init(name : String , number : String ,amount : String? ,remark : String? ) {
        self.name = name
        self.number = number
        self.amount = amount
        self.remark = remark
     }
}


extension ScanObject {
    
    override var description: String {
        return "name = \(name), number = \(number), amount = \(String(describing: amount)), remark = \(String(describing: remark))"
    }
}
