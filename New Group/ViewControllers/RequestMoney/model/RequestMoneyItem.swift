//
//  RequestMoneyItem.swift
//  OK
//
//  Created by palnar on 21/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import Foundation

class RequestMoneyItem {
    var RequestId : String?
    var AgentName : String?
    var Source    : String?
    var SourceAgentName: String?
    var Destination : String?
    var Amount : Double?
    var LocalTransactionType : String?
    var Status : Int?
    var CommanTransactionType:String?
    var Date: String?
    var IsScheduled : Bool?
    var ScheduledType :RequestMoneySchduledType?
    var Scheduledtime : String?
    var AttachmentPath : [String]?
    var CategoryName : String?
    var IsMynumber :Bool?
    var ProfileImage :String?
    var RemindCount: Int?
    var IsBlocked :Bool?
    var Remarks:String?
    var filterRecharge:RequestMoneyRecharge?
    var NewScheduledType :RequestMoneyNewSchduledType?

    
    
    
    private init() {
        
    }
    
    class func wrapModel(dic: Dictionary<String,Any>) -> RequestMoneyItem {
        
        println_debug("RequestMoneyItem = \(dic)")
        let requestMoneyItem  = RequestMoneyItem.init()
        requestMoneyItem.RequestId = dic["RequestId"] as? String
        requestMoneyItem.AgentName = dic["AgentName"] as? String
        requestMoneyItem.Source    = dic["Source"] as? String
        requestMoneyItem.SourceAgentName = dic["SourceAgentName"] as? String
        requestMoneyItem.Destination = dic["Destination"] as? String
        requestMoneyItem.Amount = dic["Amount"] as? Double
        requestMoneyItem.LocalTransactionType = dic["LocalTransactionType"] as? String
        requestMoneyItem.Status = dic["Status"] as? Int
        requestMoneyItem.CommanTransactionType = dic["CommanTransactionType"] as? String
        requestMoneyItem.Date = dic["Date"] as? String
        requestMoneyItem.IsScheduled = dic["IsScheduled"] as? Bool
        
        //(dic["ScheduledType"] as? Int
        requestMoneyItem.Scheduledtime = dic["Scheduledtime"] as? String
        requestMoneyItem.AttachmentPath = dic["AttachmentPath"] as? [String]
        requestMoneyItem.CategoryName =  dic["CategoryName"] as? String
        requestMoneyItem.IsMynumber = dic["IsMynumber"] as? Bool
        requestMoneyItem.ProfileImage = dic["ProfileImage"] as? String
        requestMoneyItem.RemindCount = dic["RemindCount"] as? Int
        requestMoneyItem.IsBlocked = dic["IsBlocked"] as? Bool
        requestMoneyItem.Remarks = dic["Remarks"] as? String
        
        if let value = dic["ScheduledType"] as? Int {
             requestMoneyItem.ScheduledType = RequestMoneySchduledType(rawValue: value)
        }
        
        if let value = dic["ScheduledType"] as? Int {
             requestMoneyItem.NewScheduledType = RequestMoneyNewSchduledType(rawValue: value)
        }
        
        if let value = dic["LocalTransactionType"] as? String {
    
            var valuenew : Int = 0
            
            if value == "REQUESTMONEY" {
                valuenew = -5
            } else {
                valuenew = -6
            }
            
             requestMoneyItem.filterRecharge = RequestMoneyRecharge(rawValue: valuenew)
        }
            
        return requestMoneyItem
        
    }
    

    
}
