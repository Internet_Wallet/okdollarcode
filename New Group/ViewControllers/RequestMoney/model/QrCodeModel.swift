//
//  QrCodeModel.swift
//  OK
//
//  Created by palnar on 29/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import Foundation


class QrCodeModel : NSObject {
    var accountName    =    ""
    var accountNumber  =    ""
    var merchantName   =    ""
    var operatorName   =    ""
    var amountOfRecharge =  ""
    var promotionBefrePayment =  ""
    var promotionAftrePayment = ""
    var remark =   ""
    
    
    private override init () {}
        static let shared = QrCodeModel ()
    
//    init(accountName : String? ,accountNumber : String?,merchantName : String?,operatorName : String?,amountofRecharge : String?,promotionBefrePayment : String?,promotionOfferAfterpayment : String?,remark : String?) {
//        self.accountName = accountName
//        self.accountNumber = accountNumber
//        self.merchantName = merchantName
//        self.operatorName = operatorName
//        self.amountOfRecharge = amountofRecharge
//        self.promotionBefrePayment = promotionBefrePayment
//        self.promotionAftrePayment = promotionOfferAfterpayment
//        self.remark = remark
//
//    }
    
    class func wrapUserData(resultContent : String?) -> Dictionary<String,Any>?{
        if  let data = resultContent?.data(using: String.Encoding.isoLatin1) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? Dictionary<String,Any>
            }catch {
                println_debug(error)
            }
        }
        return nil
    }
    class func wrapUserDataWithResult(resultContent : String?)-> QrCodeModel? {
        if let wrapData = wrapUserData(resultContent: resultContent){
            let qrCodeModel = QrCodeModel.init()
            qrCodeModel.accountNumber = wrapData["account_number"] as! String
            qrCodeModel.accountName = wrapData["account_name"] as! String
            qrCodeModel.merchantName = wrapData["merchant_name"] as! String
            qrCodeModel.operatorName = wrapData["operator_name"] as! String
            qrCodeModel.amountOfRecharge = wrapData["amount_recharge"] as! String
            qrCodeModel.promotionBefrePayment = wrapData["promo_bfr_pymnt"] as! String
            qrCodeModel.promotionAftrePayment = wrapData["promo_aftr_pymnt"] as! String
            qrCodeModel.remark = wrapData["remark"] as! String
            return qrCodeModel
        }
        return nil
    }
    
    class func wrapUserData(dict: Dictionary<String,Any>) {
        let obj = dict
        self.shared.accountNumber = obj["account_number"] as! String
        self.shared.accountName = obj["account_name"] as! String
        self.shared.merchantName = obj["merchant_name"] as! String
        self.shared.operatorName = obj["operator_name"] as! String
        self.shared.amountOfRecharge = obj["amount_recharge"] as! String
        self.shared.promotionBefrePayment = obj["promo_bfr_pymnt"] as! String
        self.shared.promotionAftrePayment = obj["promo_aftr_pymnt"] as! String
        self.shared.remark = obj["remark"] as! String
        archiveObjectWith(dict: dict, withKey: keyQRCodeModel)
    
        
    /*    var accoutNUmber : String = ""
        if let number = qrCodeModel.accountNumber  {
            accoutNUmber = number
        }
        
        var accoutNAme : String =  ""
        if let name = qrCodeModel . accountName {
            accoutNAme = name
        }
        
        var merchantNAme : String = ""
        if let mername = qrCodeModel.merchantName {
            merchantNAme = mername
        }
        
        var operatorNAme : String = ""
        if let opName = qrCodeModel.operatorName {
            operatorNAme = opName
        }
        
        var amountOfREcharge : String = ""
        if let amunt = qrCodeModel.amountOfRecharge {
            amountOfREcharge = amunt
        }
        
        var proBfrPayment : String = ""
        if let prmoBfr = qrCodeModel.promotionBefrePayment {
            proBfrPayment = prmoBfr
        }
        
        var proAftrPayment : String = ""
        if let prmoAfr = qrCodeModel.promotionAftrePayment {
            proAftrPayment = prmoAfr
        }
        
        var remark : String = ""
        if let remarkQuery = qrCodeModel.remark {
            remark = remarkQuery
        }
        
        let jsonObject: [String : Any] =  [
            "account_number"         :        accoutNUmber ,
            "account_name"           :        accoutNAme   ,
            "merchant_name"          :        merchantNAme ,
            "operator_name"          :        operatorNAme ,
            "amount_recharge"        :        amountOfREcharge ,
            "promo_bfr_pymnt"        :        proBfrPayment ,
            "promo_aftr_pymnt"       :        proAftrPayment,
            "remark"                 :        remark
            
            
            
        ]
       
        do {
        let jsonData : Data = try JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions())
            return jsonData
        }catch {
            println_debug(error)
        }*/
    }
    
    class func wrapUserData(accountName : String,accountNumber : String,merchantName : String?,operatorName : String,amountofRecharge : String,promotionBefrePayment : String,promotionOfferAfterpayment : String,remark : String, destNum: String? = "") {
        
        
        let dict = NSMutableDictionary()
        dict.setValue(accountNumber, forKey: "account_number")
        dict.setValue(accountName, forKey: "account_name")
        dict.setValue(merchantName, forKey: "merchant_name")
        dict.setValue(operatorName, forKey: "operator_name")
        dict.setValue(amountofRecharge, forKey: "amount_recharge")
        dict.setValue(promotionBefrePayment, forKey: "promo_bfr_pymnt")
        dict.setValue(promotionOfferAfterpayment, forKey: "promo_aftr_pymnt")
        dict.setValue(remark, forKey: "remark")
        if let destNo = destNum {
            dict.setValue(destNo, forKey: "destinationNumber")
        }
        if let dic = dict as? Dictionary<String, Any> {
            wrapUserData(dict: dic)
        }
        
    }
    
  /*  class func parameterForQrCodeGeneration()-> Data? {
        let dict = NSMutableDictionary()
        let rowModel = QrCodeModel.shared
        dict.setValue(rowModel.accountNumber, forKey: "account_number")
        dict.setValue(rowModel.accountName, forKey: "account_name")
        dict.setValue(rowModel.merchantName, forKey: "merchant_name")
        dict.setValue(rowModel.operatorName, forKey: "operator_name")
        dict.setValue(rowModel.amountOfRecharge, forKey: "amount_recharge")
        dict.setValue(rowModel.promotionBefrePayment, forKey: "promo_bfr_pymnt")
        dict.setValue(rowModel.promotionAftrePayment, forKey: "promo_aftr_pymnt")
        dict.setValue(rowModel.remark, forKey: "remark")
        
        if let dic = dict as? Dictionary<String, Any> {
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions()) as Data
               
                return jsonData
            } catch _ {
                print ("JSON Failure")
            }
        }
        return nil
    }*/
   
   /* class func qrCodeDataGenerateFromDefaultUserData(operatorName : String) -> Data?{
        
        let dict = NSMutableDictionary()
        let userModel = UserModel.shared
        dict.setValue(userModel.mobileNo, forKey: "account_number")
        dict.setValue(userModel.name, forKey: "account_name")
        dict.setValue(userModel.name, forKey: "merchant_name")
        dict.setValue(operatorName, forKey: "operator_name")
        dict.setValue("", forKey: "amount_recharge")
        dict.setValue("", forKey: "promo_bfr_pymnt")
        dict.setValue("", forKey: "promo_aftr_pymnt")
        dict.setValue("", forKey: "remark")
        
        if let dic = dict as? Dictionary<String, Any> {
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions()) as Data
                
                return jsonData
            } catch _ {
                print ("JSON Failure")
            }
        }
        return nil
    }*/

}
