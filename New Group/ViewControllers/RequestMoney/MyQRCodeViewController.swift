//
//  MyQRCodeViewController.swift
//  OK
//
//  Created by palnar on 29/11/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import IQKeyboardManagerSwift

class MyQRCodeViewController: OKBaseController,IndicatorInfoProvider {
    
    //MARK: Properties
    var qrcodeImage: CIImage!
    
    //MARK: - Outlets
    
    @IBOutlet weak var viewAfterPayment: UIView!
    @IBOutlet weak var viewBeforePayment: UIView!
    @IBOutlet weak var viewBusinessName: UIStackView!
    @IBOutlet weak var lblAccountName: UILabel!
    
    @IBOutlet weak var lblAccountTitleName: UILabel!
    {
        didSet
        {
            self.lblAccountTitleName.text = self.lblAccountTitleName.text?.localized
            self.lblAccountTitleName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblAccountNumber: UILabel!{
        didSet{
            self.lblAccountNumber.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblAccountTitleNumber: UILabel!
        {
        didSet
        {
            self.lblAccountTitleNumber.text = self.lblAccountTitleNumber.text?.localized
            self.lblAccountTitleNumber.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblMerchantName: UILabel!{
        didSet{
            self.lblMerchantName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblMerchantTitleName: UILabel!
        {
        didSet
        {
            self.lblMerchantTitleName.font = UIFont(name: appFont, size: appFontSize)
            self.lblMerchantTitleName.text = self.lblMerchantTitleName.text?.localized
        }
    }
    @IBOutlet weak var lblOperator: UILabel!{
        didSet
        {
            self.lblOperator.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblOperatorTitle: UILabel!
        {
        didSet
        {
            self.lblOperatorTitle.font = UIFont(name: appFont, size: appFontSize)
            self.lblOperatorTitle.text = self.lblOperatorTitle.text?.localized
        }
    }
    @IBOutlet weak var lblAmount: UILabel!{
        didSet
        {
            self.lblAmount.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblAmountTitle: UILabel!
        {
        didSet
        {
            self.lblAmountTitle.font = UIFont(name: appFont, size: appFontSize)
            self.lblAmountTitle.text = self.lblAmountTitle.text?.localized
        }
    }
    @IBOutlet weak var lblDateAndTime: UILabel!{
        didSet
        {
            self.lblDateAndTime.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblDateAndTimeTitle: UILabel!
        {
        didSet
        {
            self.lblDateAndTimeTitle.font = UIFont(name: appFont, size: appFontSize)
            self.lblDateAndTimeTitle.text = self.lblDateAndTimeTitle.text?.localized
        }
    }

    @IBOutlet weak var txtAmount: SkyFloatingLabelTextField!{
        didSet {
            
            txtAmount.titleFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            txtAmount.placeholderFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
        }
    }
    
    @IBOutlet weak var txtPromotionBfrPay: SkyFloatingLabelTextField!{
        didSet {
            
            txtPromotionBfrPay.titleFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            txtPromotionBfrPay.placeholderFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
        }
    }
    
    @IBOutlet weak var txtPromotionAftrPay: SkyFloatingLabelTextField!{
        didSet {
            
            txtPromotionAftrPay.titleFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            txtPromotionAftrPay.placeholderFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
        }
    }
    
    @IBOutlet weak var txtRemark: SkyFloatingLabelTextField!{
        didSet {
            txtRemark.font = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            txtRemark.titleFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            txtRemark.placeholderFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
        }
    }
    
    @IBOutlet weak var imgQRCode: UIImageView!
    
    @IBOutlet weak var imgOkLogo: UIButton!
    
    @IBOutlet weak var printableView: UIView!
    
    @IBOutlet weak var generateQrbtn : UIButton!
    
    var strEnterAmountValue = ""

        var timer = Timer()
    
    //MARK: - Actions
    
    
    //MARK: OVerride methods
    
    override func viewWillDisappear(_ animated: Bool)
    {
        timer.invalidate()
    }
    
    func viewWillAppear()
    {
        timer.fire()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.helpSupportNavigationEnum = .Request_Money
        configureTextFieldData()
        configureWithDefaultData(isCallFromRoot: false)
        //Manage Keyboard
        IQKeyboardManager.sharedManager().enable            = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
        if(UserModel.shared.accountType == "0")//Merchant user
        {
            viewBusinessName.isHidden = false
            viewAfterPayment.isHidden = false
            viewBeforePayment.isHidden = false
        }
        else
        {
            viewBusinessName.isHidden = true
            viewAfterPayment.isHidden = true
            viewBeforePayment.isHidden = true
        }
        
        // Do any additional setup after loading the view.
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:#selector(self.tick) , userInfo: nil, repeats: true)

        txtAmount.addTarget(self, action: #selector(MyQRCodeViewController.textFieldDidChange(_:)),
                                     for: UIControl.Event.editingChanged)
        txtPromotionBfrPay.addTarget(self, action: #selector(MyQRCodeViewController.textFieldDidChange(_:)),
                            for: UIControl.Event.editingChanged)
        txtPromotionAftrPay.addTarget(self, action: #selector(MyQRCodeViewController.textFieldDidChange(_:)),
                            for: UIControl.Event.editingChanged)
        txtRemark.addTarget(self, action: #selector(MyQRCodeViewController.textFieldDidChange(_:)),
                                      for: UIControl.Event.editingChanged)
        
        if(txtAmount.text!.count > 0)
        {
            txtAmount.placeholder = "Amount".localized
        }
        else
        {
            txtAmount.placeholder = "Enter Amount".localized
        }
        generateQrbtn.setTitle(generateQrbtn.titleLabel?.text?.localized, for: .normal)
        txtRemark.placeholder = "Enter Remarks".localized
        
        let width = UIScreen.main.bounds.width
        if width > 325 {
            
        } else {
            txtPromotionAftrPay.placeholderFont = UIFont(name: appFont, size: 13.0)
            txtPromotionBfrPay.placeholderFont = UIFont(name: appFont, size: 13.0)
        }
        txtPromotionBfrPay.placeholder = "Enter Promotion Before Payment".localized
        txtPromotionAftrPay.placeholder = "Enter Promotion After Payment".localized
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        //fixed text field with two prefix 09
        
        let strText = textField.text!

        if(textField.tag == 100)
        {
            
            let dataWithoutComma = self.removeCommaFromDigit(textField.text!)

            strEnterAmountValue = self.getDigitDisplay(dataWithoutComma)
            
            if(strEnterAmountValue.length > 0)
            {
                txtAmount.becomeFirstResponder()
                txtAmount.placeholder = "Amount".localized
                
                if(strEnterAmountValue.length == 9)
                {
                    txtAmount.textColor = UIColor.green
                }
                else if(strEnterAmountValue.length >= 10)
                {
                    txtAmount.textColor = UIColor.red
                }
                else
                {
                    txtAmount.textColor = UIColor.black
                }
            }
            else
            {
                txtAmount.placeholder = "Enter Amount".localized
            }
            
            txtAmount.text = strEnterAmountValue
        }
        else if(textField.tag == 101)
        {
            
            if(strText.count > 0)
           {
            txtPromotionBfrPay.placeholder = "Promotion Before Payment".localized
            }
            else
           {
            txtPromotionBfrPay.placeholder = "Enter Promotion Before Payment".localized
            }
        }
        else if(textField.tag == 102)
        {
            if(strText.count > 0)
            {
                txtPromotionAftrPay.placeholder = "Promotion After Payment".localized
            }
            else
            {
                txtPromotionAftrPay.placeholder = "Enter Promotion After Payment".localized
            }
        }
        else if(textField.tag == 103)
        {
            if(strText.count > 0)
            {
                txtRemark.placeholder = "Remarks".localized
            }
            else
            {
                txtRemark.placeholder = "Enter Remarks".localized
            }
        }
    }
    
    @objc func tick() {
        
        var FormateStr: String
        
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        let date: Date? = Date()
        dateFormatter.dateFormat = "EE, dd-MMM-yyy HH:mm:ss"
        FormateStr = dateFormatter.string(from: date!)
        
        lblDateAndTime.text = FormateStr
        //DateFormatter.localizedString(from: Date(), dateStyle: .short,timeStyle: .short)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        appDel.floatingButtonControl?.window.button?.isHidden = true
        appDel.floatingButtonControl?.closeButton.isHidden = true
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        //        NotificationCenter.default.removeObserver(self)
//    }
    
    
    /* @IBAction func MoreMenuTapped(_ sender: Any) {
     
     guard let rootMoreMenuVC    = UIStoryboard(name: "requestmoney", bundle: nil).instantiateViewController(withIdentifier: "more_menu_my_qrcode_navigation_vc") as? MoreItemMenuMyQrVC else {
     
     return
     }
     
     rootMoreMenuVC.printableImage = printableView.toImage()
     
     self.present(rootMoreMenuVC, animated: true, completion: nil)
     }
     */
    
    /* @IBAction func MoreMenuTapped(_ sender: Any) {
     
     let storyBoard = UIStoryboard(name: "requestmoney", bundle: nil)
     
     guard let navVc = storyBoard.instantiateViewController(withIdentifier: "more_menu_my_qrcode_navigation_vc") as? UINavigationController,
     let moreMenuVC = navVc.viewControllers.first as? MoreItemMenuMyQrVC else {
     
     return
     }
     
     moreMenuVC.printableImage = printableView.toImage()
     self.present(navVc, animated: true, completion: nil)
     }
     */
    
    @IBAction func MoreMenuTapped(_ sender :Any?) {
        
        guard let sender = sender as? FloatingButton else { return }
        
        let trailing = sender.xConstraint.constant
        let bottom = sender.yConstraint.constant
        let settings = FloatingMenu.Settings(position: .bottomRight(trailing, bottom), padingFromFloatingButton: 23, menuSpacing: 19, floatingButton: sender)
        
        sender.whenOpen = { (sender_) in
            sender_.defaultOpen()
        }
        
        sender.whenClose = { (sender_) in
            sender_.defaultClose()
        }
        
        FloatingMenu(settings: settings).set(menuItems: _getMenuItems()).show(On: self)
    }
    
    private func _getMenuItems() ->[FloatingMenuItemView] {
        
        var menuItems = [FloatingMenuItemView]()
        let menus = [
            ("Print QR Code".localized, UIImage(named: "print_qr")),
            ("Share QR Code".localized, UIImage(named: "share_qr"))
        ]
        
        let action :RightSideSlideMenuItem.Action = { [weak self] (menuVC, menu) in
            
            menuVC.dismiss({ [weak self] in
                self?._menuButtonTapped(sender: menu)
            })
        }
        
        for (index, (title, img)) in menus.enumerated()  {
            
            if let menuView = RightSideSlideMenuItem.newInstance() {
                
                menuView.tag = index
                menuView.set(title: title).set(image: img).onTap = action
                menuItems.append(menuView)
            }
        }
        
        return menuItems
    }
    
    private func _menuButtonTapped(sender :RightSideSlideMenuItem) {
        
        println_debug("sender.title = \(sender.label.text ?? "")")
        
        switch sender.tag {
        case 0:
            showPrintPage()
            break
        case 1:
            showSharePage()
            break
        default: break
        }
    }
    
    private func showSharePage() {
        
        let image = printableView.toImage()
        let activityViewController = UIActivityViewController(activityItems: [image], applicationActivities: nil)
        
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    
    private func showPrintPage() {
        
        ///
        guard let presentableView = view else { return }
        let image = printableView.toImage()
        let rect = presentableView.frame
        let animation = true
        let printInfo = UIPrintInfo(dictionary:nil)
        let printController = UIPrintInteractionController.shared
        
        ///
        printInfo.outputType = UIPrintInfo.OutputType.general
        printInfo.jobName = "My Print Job"
        ///
        printController.printInfo = printInfo
        printController.printingItem = image
        
        // Do it
        printController.present(from: rect, in: presentableView, animated: animation, completionHandler: nil)
    }
    
    @IBAction func generateQRCodeTapped(_sender : Any ) {
        //        if qrcodeImage == nil {
        //            return
        //        }
        if txtAmount?.text != nil && (txtAmount.text?.elementsEqual(""))! {
            return
        }
        
        
        var accoutNUmber : String = ""
        
        if let number = lblAccountNumber.text  {
            
            accoutNUmber = changeMobileNumberFormat(mobileNumber: number, toDisplay: false)
        }
        
        var accoutNAme : String =  ""
        if let name = lblAccountName.text {
            accoutNAme = name
        }
        
        var merchantNAme : String = ""
        if let mername = lblMerchantName.text {
            merchantNAme = mername
        }
        
        var operatorNAme : String = ""
        if let opName = lblOperator.text {
            operatorNAme = opName
        }
        
        var amountOfREcharge : String = ""
        if let amunt = txtAmount.text {
            amountOfREcharge = changeAmountNumberFormat(amount: amunt, toDisplay: false)
        }
        
        var proBfrPayment : String = ""
        if let prmoBfr = txtPromotionBfrPay.text {
            proBfrPayment = prmoBfr
        }
        
        var proAftrPayment : String = ""
        if let prmoAfr = txtPromotionAftrPay.text {
            proAftrPayment = prmoAfr
        }
        
        var remark : String = ""
        if let remarkQuery = txtRemark.text {
            remark = remarkQuery
        }
        
        QrCodeModel.wrapUserData(accountName: accoutNAme, accountNumber: accoutNUmber, merchantName: merchantNAme, operatorName: operatorNAme, amountofRecharge: amountOfREcharge, promotionBefrePayment: proBfrPayment, promotionOfferAfterpayment: proAftrPayment, remark: remark)
        
        generateQrCodeFromUserInput()
        
    }
    //MARK: - Indicator Info Provider
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title : "My QR Code".localized)
        //
    }
    
    
    
    func configureWithDefaultData(isCallFromRoot : Bool){
        
        if  QrCodeModel.shared.accountNumber != ""{
            lblAccountName.text =       QrCodeModel.shared.accountName
            lblAccountNumber.text = UserModel.shared.mobileNo//self.getActualNum(QrCodeModel.shared.accountNumber, withCountryCode: "+95")//changeMobileNumberFormat(mobileNumber: QrCodeModel.shared.accountNumber, toDisplay: true)
            
            lblMerchantName.text     =  QrCodeModel.shared.merchantName
            lblOperator.text         =  QrCodeModel.shared.operatorName
            lblAmount.text           =  changeAmountNumberFormat(amount: QrCodeModel.shared.amountOfRecharge, toDisplay: true)
            txtAmount.text           =  changeAmountNumberFormat(amount: QrCodeModel.shared.amountOfRecharge, toDisplay: false)
            txtPromotionBfrPay.text  =  QrCodeModel.shared.promotionBefrePayment
            txtPromotionAftrPay.text =  QrCodeModel.shared.promotionAftrePayment
            txtRemark.text           =  QrCodeModel.shared.remark
            // imgQRCode.isHidden = false
            // imgOkLogo.isHidden = false
            //  if isCallFromRoot{
            generateQrCodeFromUserInput()
            // }
            
        }else  {
            lblAccountName.text =       UserModel.shared.name
            lblAccountNumber.text =     UserModel.shared.mobileNo//self.getActualNum(UserModel.shared.mobileNo, withCountryCode: "+95")//changeMobileNumberFormat(mobileNumber: UserModel.shared.mobileNo, toDisplay: true)
            
            lblMerchantName.text     =  UserModel.shared.businessName
            lblOperator.text         = getOparatorName(UserModel.shared.mobileNo)
            lblAmount.text   = "0 MMK"//GlobalConstants.Strings.mmk//
            //if isCallFromRoot {
            //                generateQrCodeFromDefaultUserData(isCallFromRoot: isCallFromRoot)
            generateQrCodeFromUserInput()
            // }
            // imgQRCode.isHidden = true
            // imgOkLogo.isHidden = true
            
            
        }
        
        
    }
    
    func generateQrCodeFromUserInput(){
        //        let rowQrCodeModelData =  QrCodeModel.getWrappedDataModel(qrCodeModel:qrCodeModel )
        //        let data = txtAmount.text?.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
        // txtRemark.text.data
        //if let qrCodeData = QrCodeModel.parameterForQrCodeGeneration() {
        if let qrCodeData = encryptQrCodeData(){
            let filter = CIFilter(name: "CIQRCodeGenerator")
            
            filter?.setValue(qrCodeData, forKey: "inputMessage")
            filter?.setValue("Q", forKey: "inputCorrectionLevel")
            
            qrcodeImage = filter?.outputImage
            displayQRCodeImage()
            // imgQRCode.image = UIImage.init(ciImage: qrcodeImage)
            
            //if isCallFromRoot == false{
            // configureWithData(isCallFromRoot: false)
            // }
        }
    }
    
    /* func generateQrCodeFromDefaultUserData(isCallFromRoot : Bool){
     if let qrCodeData = QrCodeModel.qrCodeDataGenerateFromDefaultUserData(operatorName: lblOperator.text!) {
     let filter = CIFilter(name: "CIQRCodeGenerator")
     
     filter?.setValue(qrCodeData, forKey: "inputMessage")
     filter?.setValue("Q", forKey: "inputCorrectionLevel")
     
     qrcodeImage = filter?.outputImage
     // imgQRCode.image = UIImage.init(ciImage: qrcodeImage)
     displayQRCodeImage()
     if isCallFromRoot == false {
     configureWithData(isCallFromRoot: false)
     }
     }
     }*/
    
    
    
    func displayQRCodeImage() {
        let scaleX = imgQRCode.frame.size.width / qrcodeImage.extent.size.width
        let scaleY = imgQRCode.frame.size.height / qrcodeImage.extent.size.height
        
        let transformedImage = qrcodeImage.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))
        
        imgQRCode.image = UIImage.init(ciImage:transformedImage)
        
        lblAmount.text           =  changeAmountNumberFormat(amount: QrCodeModel.shared.amountOfRecharge, toDisplay: true)
    }
    
    func changeMobileNumberFormat(mobileNumber : String , toDisplay : Bool ) -> String {
        
        if toDisplay == true {
            
            if mobileNumber.starts(with: "00") {
                return mobileNumber.substring(from: 1)
            } else {
                return mobileNumber
            }
        } else {
            
            if mobileNumber.starts(with: "00"){
                return mobileNumber
            } else {
                return "0\(mobileNumber)"
            }
        }
    }
    
    func changeAmountNumberFormat(amount : String , toDisplay : Bool ) -> String {
        
        if toDisplay == true {
            
            if amount.contains(GlobalConstants.Strings.mmk) {
                return amount
            } else {
                if(amount.length == 0)
                {
                   return "0 MMK"
                }
                return "\(amount)\(GlobalConstants.Strings.mmk)"
            }
        } else {
            if amount.contains(GlobalConstants.Strings.mmk) {
                return amount.replacingOccurrences(of: GlobalConstants.Strings.mmk, with: "")
            } else {
                return amount
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func configureTextFieldData() {
        
        txtAmount.delegate = self
        txtRemark.delegate = self
        txtPromotionBfrPay.delegate = self
        txtPromotionAftrPay.delegate = self
        txtAmount.tag = 100
        txtPromotionBfrPay.tag = 101
        txtPromotionAftrPay.tag = 102
        txtRemark.tag = 103
    }
    
    
    func encryptQrCodeData() -> Data? {
        
        var encryptedData : String = ""
        encryptedData.append("00#")
        if let accoutName = lblAccountName.text {
            
            encryptedData.append(accoutName)
        } else {
            
            encryptedData.append(UserModel.shared.name)
        }
        
        encryptedData.append("-")
        if let accountNumber =  lblAccountNumber.text {
            
            encryptedData.append(changeMobileNumberFormat(mobileNumber: accountNumber, toDisplay: false))
        } else {
            encryptedData.append(UserModel.shared.mobileNo)
        }
        encryptedData.append("@")
        
        if let amount = txtAmount.text {
            
            encryptedData.append(changeAmountNumberFormat(amount: amount, toDisplay: false))
        } else {
            
            encryptedData.append("(null)")
        }
        
        encryptedData.append("&")
        encryptedData.append("0β0.0γ0.0α")
        let updatedTime = todayDateWithOutputFormat(outputFormat: GlobalConstants.Strings.qrCodeDateFormat)
        encryptedData.append(updatedTime)
        encryptedData.append("`,,")
        if let description = txtRemark.text {
            encryptedData.append(description)
        }
        encryptedData = AESCrypt.encrypt(encryptedData, password: "m2n1shlko@$p##d")
        return encryptedData.data(using: .utf8);
        
    }
    
    
    
    var qrImage : CIImage?
    
    func getQRImage(stringQR: String, withSize rate: CGFloat) -> UIImage? {
        if let filter = CIFilter.init(name: "CIQRCodeGenerator") {
            filter.setDefaults()
            if let data : Data = stringQR.data(using: String.Encoding.utf8) {
                filter.setValue(data, forKey: "inputMessage")
                if let resultImage : CIImage = filter.outputImage {
                    let transform = CGAffineTransform(scaleX: 12, y: 12)
                    let translatedImage = resultImage.transformed(by: transform)
                    guard let logo = UIImage(named: "okEmbedInQR"), let logoInCGImage = logo.cgImage else {
                        return nil
                    }
                    guard let qrWithLogoCI = translatedImage.combined(with:  CIImage(cgImage: logoInCGImage)) else {
                        return nil
                    }
                    let context = CIContext.init(options: nil)
                    guard let qrWithLogoCG = context.createCGImage(qrWithLogoCI, from: qrWithLogoCI.extent) else {
                        return nil
                    }
                    var image   = UIImage.init(cgImage: qrWithLogoCG, scale: 1.0, orientation: UIImage.Orientation.up)
                    let width  =  image.size.width * rate
                    let height =  image.size.height * rate
                    UIGraphicsBeginImageContext(.init(width: width, height: height))
                    let cgContext = UIGraphicsGetCurrentContext()
                    cgContext?.interpolationQuality = .none
                    image.draw(in: .init(origin: .init(x: 0, y: 0), size: .init(width: width, height: height)))
                    image = UIGraphicsGetImageFromCurrentImageContext()!
                    UIGraphicsEndImageContext()
                    return image
                }
            }
        }
        return nil
    }
    
    
}

extension MyQRCodeViewController : UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if(textField.tag == 100)//for Total Amount
        {
            let str = (textField.text! + string)
            if str.count <= 11 {
                
                return true
            }
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if let nextView = self.view .viewWithTag(textField.tag + 1) {
            
            if nextView.isKind(of: UITextField.self) {
                nextView.becomeFirstResponder()
            }
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //        if let nextView = self.view .viewWithTag(textField.tag + 1){
        //            if nextView.isKind(of: UITextField.self) {
        //                nextView.becomeFirstResponder()
        //            }
        //        }else {
        //            textField.resignFirstResponder()
        //        }
        
    }
}

extension CIImage {
    /// Combines the current image with the given image centered.
    func combined(with image: CIImage) -> CIImage? {
        guard let combinedFilter = CIFilter(name: "CISourceOverCompositing") else { return nil }
        let centerTransform = CGAffineTransform(translationX: extent.midX - (image.extent.size.width / 2), y: extent.midY - (image.extent.size.height / 2))
        combinedFilter.setValue(image.transformed(by: centerTransform), forKey: "inputImage")
        combinedFilter.setValue(self, forKey: "inputBackgroundImage")
        return combinedFilter.outputImage!
    }
}
