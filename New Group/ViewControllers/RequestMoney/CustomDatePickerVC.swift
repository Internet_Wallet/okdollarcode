//
//  CustomDatePickerVC.swift
//  OK
//
//  Created by palnar on 15/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
protocol RequestMoneyCustomDatePickerDelegate {
    func dateWasSelected( selectedDateString: String)
}
class CustomDatePickerVC: UIViewController {
    
     var delegate : RequestMoneyCustomDatePickerDelegate?
     var datePicker :UIDatePicker?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func dismissalViewController () {
        dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @objc
    @IBAction func setDate(sender: Any) {
        //let dateFormatter = defaultDateFormat
        // dateFormatter.dateStyle = DateFormatter.Style.long
        if let datePicker = sender as? UIDatePicker {
            
            _callDeligate(datePicker: datePicker)
        } else if let datePicker = self.datePicker {
            
            _callDeligate(datePicker: datePicker)
        }
    }
    
    private func _callDeligate(datePicker :UIDatePicker) {
        
        let dateString = stringFromDate(date: datePicker.date as NSDate, format: GlobalConstants.Strings.defaultDateFormat)
        
        if delegate != nil {
            delegate?.dateWasSelected(selectedDateString: dateString)
        }
    }
}


    

