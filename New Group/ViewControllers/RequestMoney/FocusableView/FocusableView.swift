//
//  FocusableView.swift
//  OK
//
//  Created by palnar on 20/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

open class FocusableView :UIControl {
    
    open override var isFocused: Bool {
        return (isHidden == false)
    }
    
    final func focusIfNeeded() {
        
        if isFocused == true { return }
        
        if shouldFocus() == true {
            focus()
        }
    }
    
    final func unFocusIfNeeded() {
        
        if isFocused == false { return }
        
        if shouldUnFocus() == true {
            unFocus()
        }
    }
    
    func focus() {
        
        if isHidden == false { return }
        self.isHidden = false
    }
    
    func unFocus() {
        
        if isHidden == true { return }
        self.isHidden = true
    }
    
    func shouldFocus() -> Bool { return true }
    func shouldUnFocus() -> Bool { return true }
}

