//
//  SuggestionItemCellViewTableViewCell.swift
//  OK
//
//  Created by palnar on 20/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol SuggestionItemCellViewDeligate :class {
    func didTapOn(_ cell :SuggestionItemCellView)
}

class SuggestionItemCellView: UITableViewCell {

    let imgUser = UIImageView()
    let labUerName = UILabel()
    let labMobile = UILabel()
    let mobBtn = UIButton()
    
    weak var deligate :SuggestionItemCellViewDeligate?
    var indexPath: IndexPath?
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        //imgUser.backgroundColor = UIColor.blue
        
        imgUser.translatesAutoresizingMaskIntoConstraints = false
        labUerName.translatesAutoresizingMaskIntoConstraints = false
        labMobile.translatesAutoresizingMaskIntoConstraints = false
        mobBtn.translatesAutoresizingMaskIntoConstraints = false
        
        mobBtn.setTitle("", for: .normal)
        mobBtn.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
       
        contentView.addSubview(imgUser)
        contentView.addSubview(labUerName)
        contentView.addSubview(labMobile)
        contentView.addSubview(mobBtn)

        
        let viewsDict = [
            "image" : imgUser,
            "username" : labUerName,
            "mobile" : labMobile,
            "btn" : mobBtn

            ] as [String : Any]
        
      //  contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[image(10)]", options: [], metrics: nil, views: viewsDict))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[username]-[image]-3-|", options: [], metrics: nil, views: viewsDict))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-9-[username]-8-[mobile]", options: [.alignAllLeading], metrics: nil, views: viewsDict))
        
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[btn]-0-|", options: [.alignAllCenterY], metrics: nil, views: viewsDict))
        
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[btn]-0-|", options: [.alignAllCenterY], metrics: nil, views: viewsDict))

        
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[image]-|", options: [.alignAllCenterY], metrics: nil, views: viewsDict))
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        super.touchesBegan(touches, with: event)
        //print("touchesEnded method called=====\(deligate)")
        deligate?.didTapOn(self)
    }
    
    @objc func buttonClicked() {
        //print("Button Clicked method called-------")
        deligate?.didTapOn(self)

    }
}
