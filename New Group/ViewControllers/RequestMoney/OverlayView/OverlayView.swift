//
//  OverlayView.swift
//  OK
//
//  Created by palnar on 27/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

class OverlayView :UIView {
    
    public typealias Action = (OverlayView) ->()
    
    public var didTap :Action?
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.isUserInteractionEnabled = true
        self.frame = UIScreen.main.bounds
    }
    
    public static func newInstance() ->OverlayView? {
        
        return OverlayView.newInstance(xibName: "OverlayView")
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        super.touchesEnded(touches, with: event)
        didTap?(self)
    }
}
