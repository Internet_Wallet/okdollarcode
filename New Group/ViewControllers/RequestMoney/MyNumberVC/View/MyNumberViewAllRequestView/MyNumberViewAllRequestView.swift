//
//  MyNumberViewAllRequestView.swift
//  OK
//
//  Created by palnar on 22/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class MyNumberViewAllRequestView: UIControl {
 
    static func newInstance() ->MyNumberViewAllRequestView? {
        return self.newInstance(xibName: "MyNumberViewAllRequestView")
    }
    
    @IBAction func _didTappedOnButton() {
        sendActions(for: .touchUpInside)
    }
    
    //    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//
//        super.touchesEnded(touches, with: event)
//        sendActions(for: .touchUpInside)
//    }
}
