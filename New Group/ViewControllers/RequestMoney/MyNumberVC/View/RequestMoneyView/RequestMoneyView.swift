//
//  MyNumberNewRequestView.swift
//  OK
//
//  Created by palnar on 20/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class MyNumberNewRequastViewBase :FocusableView {
    
    func reset() { return }
    
    override func focus() {
        
        if isFocused == true { return }
        
        UIView.animate(withDuration: 1.2, delay: 0.0, usingSpringWithDamping: 0.72, initialSpringVelocity: 0.7, options: [.curveEaseIn, .allowUserInteraction], animations: {
            
            self.isHidden = false
            self.layoutIfNeeded()
            
        }) { (flag) in
            
        }
    }
    
    override func unFocus() {
        
        if isFocused == false { return }
        
        UIView.animate(withDuration: 1.2, delay: 0.0, usingSpringWithDamping: 0.72, initialSpringVelocity: 0.7, options: [.curveEaseIn, .allowUserInteraction], animations: {
            
            self.isHidden = true
            self.layoutIfNeeded()
            
        }) { (flag) in
            
        }
    }
}

class MobileNumberView :MyNumberNewRequastViewBase, ContactPickerDelegate {
    
    @IBOutlet weak var clearButton: UIButton!
    {
        didSet {
            self.clearButton.isHidden = true
        }
    }
    @IBOutlet weak var operatorLbl: UILabel!
    {
        didSet {
            self.operatorLbl.text = ""
            
        }
    }

     @IBOutlet weak var mynumContact: UIButton!
     @IBOutlet weak var othernumContact: UIButton!
   
    @IBOutlet weak var leftIconImageView: UIImageView!

    @IBOutlet weak var searchTextField: SearchTextField!
    {
        didSet {
            self.searchTextField.titleFont = UIFont(name: appFont, size: appTitleButton) ?? UIFont.systemFont(ofSize: appTitleButton)
            self.searchTextField.font = UIFont(name: appFont, size: appTitleButton)
            self.searchTextField.placeholder = self.searchTextField.placeholder?.localized
        }
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        searchTextField.keyboardType = .numberPad
        let width = UIScreen.main.bounds.width
        if width > 325 {
            
        } else {
            self.searchTextField.attributedPlaceholder = NSAttributedString(string: self.searchTextField.placeholder!, attributes:[NSAttributedString.Key.font:UIFont(name: appFont, size: appTitleButton) ?? UIFont.systemFont(ofSize: appTitleButton)])
        }
        _addActions()
    }
    
    private func _addActions() {
        
        searchTextField.addTarget(self, action: #selector(editingDidBegin(_:)), for: .editingDidBegin)
        searchTextField.addTarget(self, action: #selector(editingChanged(_:)), for: .editingChanged)
        searchTextField.addTarget(self, action: #selector(editingDidEnd(_:)), for: .editingDidEnd)
        clearButton.addTarget(self, action: #selector(clearButtonTapped(_:)), for: .touchUpInside)
        
        //mynumContact.addTarget(self, action: #selector(myContactButtonTapped(_:)), for: .touchUpInside)
    }
    
    override func focus() {
        
        if isFocused == true { return }
        
        super.focus()
        searchTextField.becomeFirstResponder()
    }
    
    override func unFocus() {
        
        if isFocused == false { return }
        
        reset()
        super.unFocus()
    }
    
    override func reset() {
      //  self.operatorLbl.text = ""
        self.clearButton.isHidden = true
        if self.searchTextField.tag == 0 {
            self.searchTextField.placeholder = "Enter Request Money Number".localized
        } else {
            self.searchTextField.placeholder = "Enter Receive Money Number".localized
        }
        
        searchTextField.text = ""
        self.searchTextField.resignFirstResponder()
        
    }
    
    @objc func editingDidBegin(_ sender: SearchTextField) {
        sender.keyboardType = .numberPad
        if (sender.text?.count)! > 2 {
            
        } else {
            
            searchTextField.text = commonPrefixMobileNumber
        }
        sendActions(for: .editingDidBegin)
    }
    
    @objc func editingChanged(_ sender: SearchTextField) {
        self.clearButton.isHidden = false
        //let number = sender.text
        
        if let text = sender.text, text.count <= 2 {
            self.searchTextField.text = "09"
          //  self.operatorLbl.text = ""
            self.searchTextField.theme.cellHeight = 0
            self.searchTextField.theme.fontColor = .clear
            self.searchTextField.maxResultsListHeight = 0
            self.searchTextField.maxNumberOfResults = 0
            
            if sender.tag == 0 {
                self.searchTextField.placeholder = "Enter Request Money Number".localized
                
            } else {
                self.searchTextField.placeholder = "Enter Receive Money Number".localized
            }
            self.clearButton.isHidden = true
        } else {
            self.searchTextField.theme.fontColor = .black

              self.searchTextField.maxResultsListHeight = 200
              self.searchTextField.theme.cellHeight = 50
              self.searchTextField.maxNumberOfResults = 5
            if sender.tag == 0 {
                self.searchTextField.placeholder = "Request Money Number".localized
            } else {
                self.searchTextField.placeholder = "Receive Money Number".localized
            }
        }
        
        if let text = sender.text, text.count > 2 {
            self.clearButton.isHidden = false
        }
        
        //let validations = PayToValidations().getNumberRangeValidation(number ?? "")
      //  self.operatorLbl.text = validations.operator
        if sender.text == commonPrefixMobileNumber
        {
            if sender.tag == 0 {
                self.searchTextField.placeholder = "Enter Request Money Number".localized
            } else {
                self.searchTextField.placeholder = "Enter Receive Money Number".localized
            }
           
          //  self.operatorLbl.text = ""
            self.clearButton.isHidden = true
        } else {
            if sender.tag == 0 {
                self.searchTextField.placeholder = "Request Money Number".localized
            } else {
                self.searchTextField.placeholder = "Receive Money Number".localized
            }
        }
        sendActions(for: .editingChanged)
    }
    
    @objc func editingDidEnd(_ sender: SearchTextField) {
        sendActions(for: .editingDidEnd)
    }
    
    
    @IBAction func myContactButtonTapped(_ sender: UIButton) {
      //  print("myContactButtonTapped----")
               
        let nav = UitilityClass.openContact(multiSelection: false, self as? ContactPickerDelegate, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
        self.window?.currentViewController()?.present(nav, animated: true, completion: nil)
       
    }
    
    @IBAction func othercontactButton(_ sender : UIButton)
    {
        let nav = UitilityClass.openContact(multiSelection: false, self as? ContactPickerDelegate, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
            self.window?.currentViewController()?.present(nav, animated: true, completion: nil)
    }
    
    @objc func clearButtonTapped(_ sender: UIButton) {
        print("clearButtonTapped----")
        self.reset()
        sendActions(for: .primaryActionTriggered)
    }
 
    
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]) {
        for contact in contacts {
            decodeContact(contact: contact, isMulti: true)
        }
    }
    
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        decodeContact(contact: contact, isMulti: false)
    }
    
    func decodeContact(contact: ContactPicker, isMulti: Bool) {
        
        let phoneNumberCount = contact.phoneNumbers.count
        
        if phoneNumberCount >= 1 {
            
            println_debug(contact.firstName)
            println_debug(contact.phoneNumbers[0].phoneNumber)
            
            _ = "\(contact.phoneNumbers[0].phoneNumber)"
            
            let strMobNo = contact.phoneNumbers[0].phoneNumber
            println_debug(strMobNo)
            let trimmed = String(strMobNo.filter { !" ".contains($0) })
            println_debug(trimmed)
            
          //  let strContactNumber = self.getConatctNum(trimmed,withCountryCode: "+95")
          //  println_debug(strContactNumber)
        
            
            
            let mobileNo = self.getConatctNum(trimmed, withCountryCode: "+95")
            
              searchTextField.text = mobileNo
            
            let nc = NotificationCenter.default
                       nc.post(name:Notification.Name(rawValue:"ContactPick"),
                               object: nil, userInfo: ["MobileNo": mobileNo, "textField" : searchTextField])
            
          
            clearButton.isHidden = false
           
    
        }
        else {
            _ = ContactGlobalConstants.Strings.phoneNumberNotAvaialable
        }
        
    }
    
    func getConatctNum(_ phoneNum: String, withCountryCode countryCode: String) -> String {
          var FormateStr: String
          if phoneNum.hasPrefix("0") {
              _ = (phoneNum).substring(from: 1)
          }
          let notAllowedChars = CharacterSet(charactersIn: "+()")
          let resultString: String = countryCode.components(separatedBy: notAllowedChars).joined(separator: "")
          if (countryCode == "+95") || (countryCode == "(+95)") {
              if phoneNum.hasPrefix("0095") {
                  FormateStr = "0\((phoneNum).substring(from: 4))"
              }
              else if phoneNum.hasPrefix("0091") {
                  FormateStr = "0\((phoneNum).substring(from: 4))"
              }
              else if phoneNum.hasPrefix("95") {
                  FormateStr = "0\((phoneNum).substring(from: 2))"
              }
                else if phoneNum.hasPrefix("+95") {
                    FormateStr = "0\((phoneNum).substring(from: 3))"
                }
              else {
                  FormateStr = "\(phoneNum)"
              }
              
          }
          else {
              FormateStr = "00\(resultString)\(phoneNum)"
          }
          return FormateStr
      }
    
    // MARK: Getting Operator Name
    
    func getOparatorName(_ mobileNumber: String) -> String {
        
        let operatorArray = ["Select Operator", "Telenor", "Ooredoo", "Mpt", "MecTel", "MecTel CDMA", "Mpt CDMA(800)", "Mpt CDMA(450)"]
        
        var oparatorName: String
        
        if mobileNumber.hasPrefix("979") || mobileNumber.hasPrefix("0979") || mobileNumber.hasPrefix("0095979") || mobileNumber.hasPrefix("00950979") || mobileNumber.hasPrefix("978") || mobileNumber.hasPrefix("0978") || mobileNumber.hasPrefix("0095978") || mobileNumber.hasPrefix("00950978") || mobileNumber.hasPrefix("0976") || mobileNumber.hasPrefix("976") || mobileNumber.hasPrefix("00950976") || mobileNumber.hasPrefix("0095976") || mobileNumber.hasPrefix("977") || mobileNumber.hasPrefix("0977") || mobileNumber.hasPrefix("00950977") || mobileNumber.hasPrefix("0095977") {
            oparatorName = operatorArray[1]
            //oparatorName =@"Telenor";
            
        }else if mobileNumber.hasPrefix("0997") || mobileNumber.hasPrefix("997") || mobileNumber.hasPrefix("00950997") || mobileNumber.hasPrefix("0095997") || mobileNumber.hasPrefix("99") || mobileNumber.hasPrefix("099") || mobileNumber.hasPrefix("0095099") || mobileNumber.hasPrefix("009599") {
            oparatorName = operatorArray[2]
            //oparatorName =@"Oreedoo";
            
        }else if mobileNumber.hasPrefix("093") || mobileNumber.hasPrefix("93") || mobileNumber.hasPrefix("0095093") || mobileNumber.hasPrefix("009593") {
            oparatorName = operatorArray[4]
            //oparatorName =@"Mectel";
            
        }else if mobileNumber.hasPrefix("0931") || mobileNumber.hasPrefix("931") || mobileNumber.hasPrefix("0933") || mobileNumber.hasPrefix("933") || mobileNumber.hasPrefix("00950931") || mobileNumber.hasPrefix("0095931") || mobileNumber.hasPrefix("00950933") || mobileNumber.hasPrefix("0095933") {
            oparatorName = operatorArray[5]
            //oparatorName =@"Mectel CDMA";
            
        } else if mobileNumber.hasPrefix("0973") || mobileNumber.hasPrefix("973") || mobileNumber.hasPrefix("00950973") || mobileNumber.hasPrefix("0095973") || mobileNumber.hasPrefix("980") || mobileNumber.hasPrefix("0980") || mobileNumber.hasPrefix("00950980") || mobileNumber.hasPrefix("0095980") {
            oparatorName = operatorArray[6]
            //oparatorName =@"MPT 800";
            
            
        }else if mobileNumber.hasPrefix("0949") || mobileNumber.hasPrefix("949") || mobileNumber.hasPrefix("00950949") || mobileNumber.hasPrefix("0095949") || mobileNumber.hasPrefix("0947") || mobileNumber.hasPrefix("947") || mobileNumber.hasPrefix("0095947") || mobileNumber.hasPrefix("00950947") || mobileNumber.hasPrefix("0986") || mobileNumber.hasPrefix("986") || mobileNumber.hasPrefix("00950986") || mobileNumber.hasPrefix("0095986") {
            oparatorName = operatorArray[7]
            //oparatorName =@"MPT 400";
            
        }else {
            oparatorName = operatorArray[3]
            //oparatorName =@"MPT ";
            
        }
        
        return oparatorName
    }
    
}

class RequestForView :MyNumberNewRequastViewBase {
    
    @IBOutlet weak var headerView: UIControl!
    @IBOutlet weak var dropdownArrow: UIImageView!
    @IBOutlet weak var leftIconImageView: UIImageView!
    @IBOutlet weak var textField: FloatLabelTextField!
        {
        didSet {
            self.textField.titleFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            self.textField.placeholder = self.textField.placeholder?.localized
        }
    }
    @IBOutlet weak var listStackView: UIStackView!
    
    var selectedIndex: Int = 0
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        _initialize()
        _addActions()
        _addTags()
    }
    
    private func _initialize() {
        
        //  dropdownArrow.transform = CGAffineTransform.identity.rotated(by: CGFloat.pi)
        listStackView.isHidden = true
    }
    
    override func reset() {
        textField.text = ""
        textField.placeholder = "Select Request For".localized
        listStackView.isHidden = true
    }
    
    
    
    private func _addActions() {
        
        headerView.addTarget(self, action: #selector(didTappedOnHeader(_:)), for: .touchUpInside)
        
        for case let option as UIControl in listStackView.subviews {
            option.addTarget(self, action: #selector(didTappedOnOption(_:)), for: .touchUpInside)
            for case let label as UILabel in option.subviews {
                label.font = UIFont(name: appFont, size: appFontSize)
                label.text = label.text?.localized
            }
        }
    }
    
    private func _addTags() {
        
        for (index, view) in listStackView.subviews.enumerated() {
            view.tag = index
        }
    }
    
    @objc func didTappedOnHeader(_ sender: UIControl) {
        sendActions(for: .primaryActionTriggered)
        textField.text = ""
        textField.placeholder = "Select Request For".localized
        _showOrHide()
    }
    
    @objc func  didTappedOnOption(_ sender: UIControl) {
        
        _showOrHide()
        
        switch sender.tag {
        case 0: textField.text = "OK$ Money".localized; break;
        case 1: textField.text = "Request Recharge".localized; break;
        default: break
        }
        textField.placeholder = "Request For".localized
        selectedIndex = sender.tag
        sendActions(for: .valueChanged)
        self.listStackView.isHidden = true
        self.dropdownArrow.transform = CGAffineTransform.identity
    }
    
    private func _showOrHide() {
        
        UIView.animate(withDuration: 1.2, delay: 0.0, usingSpringWithDamping: 0.72, initialSpringVelocity: 0.7, options: [.curveEaseIn, .allowUserInteraction], animations: {
            
            if self.listStackView.isHidden == true {
                
                self.dropdownArrow.transform = CGAffineTransform.identity.rotated(by: CGFloat.pi)
                self.listStackView.isHidden = false
            } else {
                
                self.dropdownArrow.transform = CGAffineTransform.identity
                self.listStackView.isHidden = true
            }
            
            self.layoutIfNeeded()
            
        }) { (flag) in
            
        }
    }
}

class AmountView :MyNumberNewRequastViewBase {
    
    var updatedText = String()

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var textField: SkyFloatingLabelTextField!
        {
        didSet {
            self.textField.font = UIFont(name: appFont, size: appFontSize)
            //self.textField.placeholderFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            //self.textField.titleFont = UIFont(name: appFont, size: appFontSize) ??  UIFont.systemFont(ofSize: appFontSize)
            self.textField.placeholder = self.textField.placeholder?.localized
        }
    }
    
    @IBOutlet weak var rechargeTypeLbel : UILabel!
    {
        didSet {
            self.rechargeTypeLbel.font = UIFont(name: appFont, size: appFontSize) ??  UIFont.systemFont(ofSize: appFontSize)
            self.rechargeTypeLbel.isHidden = true
        }
    }
    override func awakeFromNib() {
        
        super.awakeFromNib()
        _addActions()
        textField.keyboardType = UIKeyboardType.decimalPad
    }
    
    override func focus() {
        
        if isFocused == true { return }
        
        super.focus()
    }
    
    override func unFocus() {
        
        if isFocused == false { return }
        
        reset()
        super.unFocus()
        textField.resignFirstResponder()
    }
    
    override func reset() {
        if self.rechargeTypeLbel != nil
        {
            self.rechargeTypeLbel.isHidden = true
        }
        textField.text = nil
    }
    
    private func _addActions() {
        
        textField.addTarget(self, action: #selector(editingDidBegin(_:)), for: .editingDidBegin)
        textField.addTarget(self, action: #selector(editingChanged(_:)), for: .editingChanged)
        textField.addTarget(self, action: #selector(editingDidEnd(_:)), for: .editingDidEnd)
    }
    
    @objc func editingDidBegin(_ sender: FloatLabelTextField) {
        sendActions(for: .editingDidBegin)
        if  sender.text!.count == 1 {
            
            if sender.text == "0"{
                sender.text = nil
            }
            
        }
        
    }
    
    @objc func editingChanged(_ sender: FloatLabelTextField) {
        sendActions(for: .editingChanged)
        
        var flag = true
        
        if  sender.text!.count == 1 {
            
            if sender.text == "0" {
              //  print("Amount editingChanged inner called----")
                
                sender.text = nil
                
            } else {
                
               /* if let textFieldText = textField.text, let textRange = Range(range, in: textFieldText) {
                    updatedText = textFieldText.replacingCharacters(in: textRange, with: string)
                    print(updatedText)
                    let numberCharSet = "1234567890."
                    if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: numberCharSet).inverted).joined(separator: "")) { return false }
                    flag = self.isValidAmount(text: updatedText)
                    print("mystring validation----\(flag)")
                    
                    if updatedText.count>9
                    {
                        return flag == false
                    }
                    
                } */
                
               /* let strMainTxt = sender.text?.replacingOccurrences(of: ",", with: "") ?? ""
                let amountArray = strMainTxt.components(separatedBy: ".")
                let mystring = amountArray[0]
                print("mystring without comma----\(mystring.length)")
                if(mystring.length > 9) {
                    textField.textColor = UIColor.red
                } else if(mystring.length > 8) && (mystring.length <= 9)  {
                    textField.textColor = UIColor.green
                } else {
                    textField.textColor = UIColor.black
                } */
            }
            
        }
    }
    
    @objc func editingDidEnd(_ sender: FloatLabelTextField) {
        sendActions(for: .editingDidEnd)
    }
}

class RemarkView :MyNumberNewRequastViewBase, UITextViewDelegate {
    
        @IBOutlet weak var iconImageView: UIImageView!
        @IBOutlet weak var textField: UITextView!
            {
            didSet {
                self.textField.font = UIFont(name: appFont, size: appFontSize)
                self.textField.text = self.textField.text?.localized
                self.textField.textColor = UIColor.lightGray
            }
        }
        
        @IBOutlet weak var placeHolderLabel : UILabel!
            {
            didSet {
                self.placeHolderLabel.font = UIFont(name: appFont, size: appFontSize)
                self.placeHolderLabel.text = self.placeHolderLabel.text?.localized
                self.placeHolderLabel.isHidden = true
            }
        }
        override func awakeFromNib() {
            
            super.awakeFromNib()
            _addActions()
            textField.keyboardType = UIKeyboardType.default
        }
        
        override func focus() {
            
            if isFocused == true { return }
            
            super.focus()
        }
        
        override func unFocus() {
            
            if isFocused == false { return }
            
            reset()
            super.unFocus()
            textField.resignFirstResponder()
        }
        
        override func reset() {
            placeHolderLabel.isHidden = true
            textField.text = "Enter Remarks".localized
        }
        
        private func _addActions() {
            
            //textField.addTarget(self, action: #selector(editingDidBegin(_:)), for: .editingDidBegin)
//            textField.addTarget(self, action: #selector(editingChanged(_:)), for: .editingChanged)
//            textField.addTarget(self, action: #selector(editingDidEnd(_:)), for: .editingDidEnd)
        }
    
        func textViewDidBeginEditing(_ textView: UITextView) {
            if textView.text == "Enter Remarks".localized {
                textView.text = nil
                textView.textColor = UIColor.black
                placeHolderLabel.textColor = UIColor.black
                placeHolderLabel.isHidden = false
            }
            sendActions(for: .editingDidBegin)
        }
    
        func textViewDidEndEditing(_ textView: UITextView) {
            if textView.text == "" {
                textView.text = "Enter Remarks".localized
                textView.textColor = UIColor.lightGray
                placeHolderLabel.isHidden = true
            } else {
                textView.textColor = UIColor.black
                placeHolderLabel.isHidden = false
            }
            sendActions(for: .editingDidEnd)
        }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            //let count = textView.text?.count ?? 0
            
//            if count > 100
//            {
//                textView.text?.removeLast()
//                 return text.rangeOfCharacter(from: .whitespacesAndNewlines) == nil
//                return false
//            }
//            return true
            //sendActions(for: .editingChanged)
        
        if(textView.text.count > 100 && range.length == 0) {
            print("Please summarize in 100 characters or less")
            return false;
        }
      return true
        
        }
        
        @objc func editingDidBegin(_ sender: UITextView) {
            sendActions(for: .editingDidBegin)
        }
        
        @objc func editingChanged(_ sender: UITextField) {
            if (sender.text?.count)! > 0 {
                placeHolderLabel.textColor = UIColor.black
                placeHolderLabel.isHidden = false
            } else {
                placeHolderLabel.isHidden = true
            }
            sendActions(for: .editingChanged)
        }
        
        @objc func editingDidEnd(_ sender: UITextField) {
            if (sender.text?.count)! > 0 {
                placeHolderLabel.isHidden = false
            } else {
                placeHolderLabel.isHidden = true
            }
            sendActions(for: .editingDidEnd)
        }
}

class AttachementView :MyNumberNewRequastViewBase {
    
    @IBOutlet weak var attachementIageView: UIImageView!
    
    @IBOutlet weak var button: UIButton!{
        didSet{
            button.titleLabel?.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    @IBOutlet weak var addFileButton : UIButton!
    
    @IBOutlet weak var deleteFileButton : UIButton!
    
   
    fileprivate var image :UIImage? {
        
        didSet {
            
            if image != nil {
                
                button.setTitle("", for: .normal)
                attachementIageView.image = image
                attachementIageView.isHidden = false
            } else {
                
                button.setTitle("Attach File".localized, for: .normal)
                attachementIageView.image = nil
                attachementIageView.isHidden = true
            }
        }
    }
    
    override func unFocus() {
        
        reset()
        super.unFocus()
    }
    
    override func reset() {
        
        self.image = nil
        if self.addFileButton != nil {
            self.addFileButton.isHidden = true
        }
        if self.deleteFileButton != nil {
            self.deleteFileButton.isHidden = true
        }
    }
    
    
}

protocol PhotoCameraProtocol
{
    func photoSelectionViewTapped()
}

class AttachementsView :MyNumberNewRequastViewBase {
    
    @IBOutlet weak var stackView: UIStackView!
    public private(set) var selectedIndex: Int = -1
    
    var delegate : PhotoCameraProtocol?
    
    let reuestMoneyView = RequestMoneyView()
    var currentSelectedIndex = 0
    
    var attachements :[UIImage] {
        
        var attachements = [UIImage]()
        
        guard let subViews = stackView.subviews as? [AttachementView] else { return attachements }
        
        for view in subViews {
            
            if let image = view.attachementIageView.image {
                
                attachements.append(image)
            }
        }
        
        return attachements
        
    }
    
    subscript(index: Int) -> UIImage? {
        
        get {
            guard let subViews = stackView.subviews as? [AttachementView] else { return nil }
            
            if subViews.count > index {
                return subViews[index].attachementIageView.image
            }
            return nil
        }
        
        set(newValue) {
            
            guard let subViews = stackView.subviews as? [AttachementView] else { return }
            
            if subViews.count > index {
                subViews[index].image = newValue
            }
        }
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        _initialize()
        _addActions()
        _addTags()
    }
    
    override func unFocus() {
        
        reset()
        super.unFocus()
    }
    
    override func reset() {
        
        guard let subViews = stackView.subviews as? [AttachementView] else { return }
        
        for view in subViews{
            view.reset()
            view.isHidden = true
        }
        stackView.subviews[0].isHidden = false
    }
    
    private func _initialize() {
        
        for view in stackView.subviews {
            view.isHidden = true
        }
        stackView.subviews[0].isHidden = false
    }
    
    
    
    @objc func viewTapped(_ sender : AttachementView)
    {
        print("viewTapped called----")
        sendActions(for: .primaryActionTriggered)
        UserDefaults.standard.set(true, forKey: "PhotoViewEnabled")
        currentSelectedIndex = sender.tag
        delegate?.photoSelectionViewTapped()
    }
    
 
    private func _addActions() {
        
        for case let option as UIControl in stackView.subviews {
            option.addTarget(self, action: #selector(viewTapped(_:)), for: .touchUpInside)
        }
        
        guard let subViews = stackView.subviews as? [AttachementView] else { return }
        
        for (_, view) in subViews.enumerated() {
            if view.addFileButton != nil {
                view.addFileButton.isHidden = true
                view.addFileButton.addTarget(self, action: #selector(addNewFile(_:)), for: .touchUpInside)
            }
            if view.deleteFileButton != nil {
                view.deleteFileButton.isHidden = true
                view.deleteFileButton.addTarget(self, action: #selector(deleteFile(_:)), for: .touchUpInside)
            }
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            view.attachementIageView.isUserInteractionEnabled = true
            view.attachementIageView.addGestureRecognizer(tapGestureRecognizer)
        }
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        if tappedImage.image != nil{
            let image = tappedImage.image
            let nc = NotificationCenter.default

            nc.post(name:Notification.Name(rawValue:"ImagePreviewNotification"),
                        object: nil,
                        userInfo: ["image": image ?? ""])
        }
        
        // Your action
    }
    
    @objc func addNewFile(_ sender : UIButton)
    {
        self.showNext(tag : sender.tag)
        sender.isHidden = true
        
        println_debug("Add new image")
    }
    
    func setFrameToDeleteButton(tag : Int)
    {
        guard let subViewsPrev = stackView.subviews[tag] as? AttachementView else { return }
        if let deleteButton = subViewsPrev.deleteFileButton, let addButton = subViewsPrev.addFileButton
        {
            addButton.isHidden = false
            deleteButton.frame = CGRect(x: (addButton.frame.origin.x - deleteButton.frame.size.width - 8), y: deleteButton.frame.origin.y, width: deleteButton.frame.size.width, height: deleteButton.frame.size.height)
        }
        for i in tag...2
        {
            guard let subViewsPrev = stackView.subviews[i] as? AttachementView else { return }
            if let deleteButton = subViewsPrev.deleteFileButton, let addButton = subViewsPrev.addFileButton
            {
                addButton.isHidden = false
                deleteButton.frame = CGRect(x: (addButton.frame.origin.x - deleteButton.frame.size.width - 8), y: deleteButton.frame.origin.y, width: deleteButton.frame.size.width, height: deleteButton.frame.size.height)
            }
        }
        
    }
    
    @objc func deleteFile(_ sender : UIButton)
    {
        let tag = sender.tag - 101
        if tag == 0
        {
            guard let subViews0 = stackView.subviews[0] as? AttachementView else { return }
            guard let subViews1 = stackView.subviews[1] as? AttachementView else { return }
            guard let subViews2 = stackView.subviews[2] as? AttachementView else { return }
            guard let subViews3 = stackView.subviews[3] as? AttachementView else { return }
                setFrameToDeleteButton(tag : 0)
                subViews0.addFileButton.isHidden = true
                subViews0.deleteFileButton.isHidden = true
                subViews0.reset()
            
                subViews1.reset()
                subViews1.isHidden = true
            
                subViews2.reset()
                subViews2.isHidden = true
            
                subViews3.reset()
                subViews3.isHidden = true
        }
        else if tag == 1
        {
            guard let subViews1 = stackView.subviews[1] as? AttachementView else { return }
            guard let subViews2 = stackView.subviews[2] as? AttachementView else { return }
            guard let subViews3 = stackView.subviews[3] as? AttachementView else { return }
            
            setFrameToDeleteButton(tag : 0)
                subViews1.reset()
                subViews1.isHidden = true
            
                subViews2.reset()
                subViews2.isHidden = true
           
                subViews3.reset()
                subViews3.isHidden = true
            
        }
        else if tag == 2
        {
            guard let subViews2 = stackView.subviews[2] as? AttachementView else { return }
            guard let subViews3 = stackView.subviews[3] as? AttachementView else { return }
            setFrameToDeleteButton(tag : 1)
                subViews2.reset()
                subViews2.isHidden = true
            
                subViews3.reset()
                subViews3.isHidden = true
            
        }
        else if tag == 3
        {
            setFrameToDeleteButton(tag : 2)
            guard let subViews3 = stackView.subviews[3] as? AttachementView else { return }
            subViews3.reset()
            subViews3.isHidden = true
        }
        else
        {
            
        }

        println_debug("delete  image")
    }
    
    private func _addTags() {
        
        for (index, view) in stackView.subviews.enumerated() {
            view.tag = index
        }
    }
    
    @objc func  touchUpInside() {
        UserDefaults.standard.set(false, forKey: "PhotoViewEnabled")
        selectedIndex = currentSelectedIndex
        sendActions(for: .touchUpInside)
        
    }
    
    func showButtons(tag : Int)
    {
        UserDefaults.standard.set(false, forKey: "PhotoViewEnabled")
        guard let subViews = stackView.subviews[tag] as? AttachementView else { return }
        if tag != 3
        {
            guard let subViews1 = stackView.subviews[tag+1] as? AttachementView else { return }
            if subViews1.isHidden == false {
                if subViews.addFileButton != nil {
                    subViews.addFileButton.isHidden = true
                }
                if subViews.deleteFileButton != nil {
                    subViews.deleteFileButton.isHidden = false
                }
            } else {
                if subViews.addFileButton != nil {
                    subViews.addFileButton.isHidden = false
                }
                if subViews.deleteFileButton != nil {
                    subViews.deleteFileButton.isHidden = false
                }
            }
        } else {
            if subViews.deleteFileButton != nil {
                subViews.deleteFileButton.isHidden = false
            }
        }
   
    }
    
    func showNext(tag : Int) {
        let tag = tag - 201
        guard let subViews = stackView.subviews[tag+1] as? AttachementView else { return }
        
        if subViews.addFileButton != nil {
            subViews.addFileButton.isHidden = true
        }
        if subViews.deleteFileButton != nil {
            subViews.deleteFileButton.isHidden = true
        }
        subViews.focus()
        
        guard let subViewsPrev = stackView.subviews[tag] as? AttachementView else { return }
        if subViewsPrev.deleteFileButton != nil && subViewsPrev.addFileButton != nil
        {
            subViewsPrev.addFileButton.isHidden = true
            subViewsPrev.deleteFileButton.frame = CGRect(x: subViewsPrev.addFileButton.frame.origin.x, y: subViewsPrev.deleteFileButton.frame.origin.y, width: subViewsPrev.deleteFileButton.frame.size.width, height: subViewsPrev.deleteFileButton.frame.size.height)
        }
    }
}

class FrequencyHeaderView :MyNumberNewRequastViewBase {
    
    @IBOutlet weak var label: UILabel!
    {
        didSet {
            self.label.font = UIFont(name: appFont, size: appFontSize)
            self.label.text = self.label.text?.localized
        }
    }
    @IBOutlet weak var dropDownArror: UIImageView!
}

class RequestSheduleView :MyNumberNewRequastViewBase {
    
    enum RequestType {
        case none
        case now
        case shedule(SheduleType)
        
        enum SheduleType {
            case none
            case date(OKDate)
            case frequency(Frequency)
            
            enum Frequency :Int {
                
                case none = -999
                case daily = 1
                case weekly = 2
                case monthly = 3
                case quaterly = 4
                case halfyearly = 5
                case yearly = 6
                
                var value :Int {
                    return self.rawValue
                }
                
                var toString :String {
                    
                    switch self {
                    case .daily: return "Daily".localized
                    case .weekly: return "Weekly".localized
                    case .monthly: return "Monthly".localized
                    case .quaterly: return "Quaterly".localized
                    case .halfyearly: return "Half Yearly".localized
                    case .yearly: return "Yearly".localized
                    default: return "Select Frequency".localized
                    }
                }
            }
            
            enum OKDate {
                
                case none
                case date(Date)
            }
        }
    }
    @IBOutlet weak var radioButtonStackView: UIStackView!
    @IBOutlet weak var requestTypeStackView: UIStackView!
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var frequencyView: UIView!
    
    @IBOutlet weak var optionsContainerView : UIView!
    @IBOutlet weak var frequencyHeaderView : FrequencyHeaderView!
    @IBOutlet weak var frequencyListStackView : UIStackView!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var selectedOptionLabel: UILabel! {
        didSet {
            selectedOptionLabel.font = UIFont(name: appFont, size: appFontSize)
            selectedOptionLabel.text = selectedOptionLabel.text?.localized
        }
    }
    
    @IBOutlet weak var dateView : UIView!
    @IBOutlet weak var selectedDateLbl : UILabel!{
        didSet {
            selectedDateLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    public private(set) var requestType = RequestType.none {
        didSet {
            sendActions(for: .valueChanged)
        }
    }
    
    
    @IBOutlet weak var frequencyBorderView : UIView! {
        didSet {
                self.frequencyBorderView.layer.borderWidth = 1.0
            self.frequencyBorderView.layer.borderColor = UIColor.gray.cgColor
        }
    }
    
    var selectedDate = Date()
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        _initialize()
        _addActions()
        _addTags()
    }
    
    override func reset() {
        self.requestType = RequestType.none
        for case let option as UIControl in radioButtonStackView.subviews {
            for case let imageView as UIImageView in option.subviews {
                imageView.isHighlighted = false
            }
        }
        for case let option as UIControl in requestTypeStackView.subviews {
            for case let imageView as UIImageView in option.subviews {
                imageView.isHighlighted = false
            }
        }
        dateView.isHidden = true
        selectedDateLbl.text = ""
        optionsContainerView.isHidden = true
        requestTypeStackView.isHidden = true
        datePickerView.isHidden = true
        frequencyView.isHidden  = true
        frequencyListStackView.isHidden  = true
    }
    
    @IBAction func setButtonAction(_ sender : UIButton) {
        dateView.isHidden = false
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "EEE,dd-MMM-yyyy hh:mm a"
        formatter.timeZone = TimeZone.current
        self.selectedDateLbl.text = formatter.string(from: selectedDate)
        UIView.animate(withDuration: 1.2, delay: 0.0, usingSpringWithDamping: 0.72, initialSpringVelocity: 0.7, options: [.curveEaseIn, .allowUserInteraction], animations: {
            
            self.requestTypeStackView.isHidden = false
            self.optionsContainerView.isHidden = true
            self.datePickerView.isHidden = true
            self.frequencyView.isHidden  = true
            
            self.layoutIfNeeded()
            
        }) { (flag) in
            
        }

    }
    
    @IBAction func dateChangeAction(_ sender : UIButton) {
        
        for case let option as MyNumberNewRequestRadioButton in requestTypeStackView.subviews {
            option.radioIconView.isHighlighted = false
            touchUpOnSheduleTypeRadioButton(option)
            break
        }
    }
    
    override func unFocus() {
        
        if isFocused == false { return }
        
        reset()
        UIView.animate(withDuration: 1.2, delay: 0.0, usingSpringWithDamping: 0.72, initialSpringVelocity: 0.7, options: [.curveEaseIn, .allowUserInteraction], animations: {
            
            self.isHidden = true
            self.layoutIfNeeded()
            
        }) { (flag) in
            self.requestTypeStackView.isHidden = true
            self.datePickerView.isHidden = true
            self.frequencyView.isHidden  = true
        }
    }
    
     func _initialize() {
        dateView.isHidden = true
        optionsContainerView.isHidden = true
        requestTypeStackView.isHidden = true
        datePickerView.isHidden = true
        frequencyView.isHidden  = true
        frequencyListStackView.isHidden  = true
        
        if #available(iOS 11.0, *) {
            datePicker.setValue(UIColor(named: "request_money_date_picker_text_color"), forKeyPath: "textColor")
        } else {
            // Fallback on earlier versions
        }
        
        if #available(iOS 13.4, *) {
                       datePicker.preferredDatePickerStyle = .wheels
                   } else {
                       // Fallback on earlier versions
                   }
        
        for case let button as MyNumberNewRequestRadioButton in frequencyListStackView.subviews {
            button.label.font = UIFont(name: appFont, size: appTitleButton)
            button.label.text = button.label.text?.localized
        }
    }
    
     func _addActions() {
        
        for case let option as UIControl in radioButtonStackView.subviews {
            option.addTarget(self, action: #selector(touchUpOnRequestTypeRadioButton(_:)), for: .touchUpInside)
            for case let label as UILabel in option.subviews {
                label.font = UIFont(name: appFont, size: appFontSize)
                label.text = label.text?.localized
            }
        }
        
        for case let option as UIControl in requestTypeStackView.subviews {
            option.addTarget(self, action: #selector(touchUpOnSheduleTypeRadioButton(_:)), for: .touchUpInside)
            for case let label as UILabel in option.subviews {
                label.font = UIFont(name: appFont, size: appFontSize)
                label.text = label.text?.localized
            }
        }
        
        datePicker.addTarget(self, action: #selector(valueChanged(DatePicker:)), for: .valueChanged)
        frequencyHeaderView.addTarget(self, action: #selector(touchUpInside(FrequencyHeaderView:)), for: .touchUpInside)
    }
    
     func _addTags() {
        
        for (index, view) in radioButtonStackView.subviews.enumerated() {
            view.tag = index
        }
        
        for (index, view) in requestTypeStackView.subviews.enumerated() {
            view.tag = index
        }
    }
    
    @objc func valueChanged(DatePicker sender :UIDatePicker) {
        selectedDate = sender.date
        print("valueChanged date===\(sender.date)====\(selectedDate)")
        self.requestType = RequestType.shedule(.date(.date(sender.date)))
    }
    
    @objc func touchUpInside(FrequencyHeaderView sender :FrequencyHeaderView) {
        
        if sender.tag == 0 {
            sender.tag = 1
        } else {
            sender.tag = 0
        }
        
        let isHidden = (sender.tag == 0) ? true : false


        UIView.animate(withDuration: 1.2, delay: 0.0, usingSpringWithDamping: 0.72, initialSpringVelocity: 0.7, options: [.curveEaseIn, .allowUserInteraction], animations: {
            
            //self.frequencyListStackView.alpha = 1.0
           // self.optionsContainerView.isHidden = isHidden
            self.frequencyListStackView.isHidden  = isHidden
            
            
            if isHidden == true {
                
                self.frequencyHeaderView.dropDownArror.transform = CGAffineTransform.identity
            } else {
                
                self.frequencyHeaderView.dropDownArror.transform = CGAffineTransform.identity.rotated(by: CGFloat.pi)
            }
            
            self.layoutIfNeeded()

            
        }) { (flag) in
            
            
        }
       
        
    }
    
    @IBAction func touchUpInside(Frequency sender :MyNumberNewRequestRadioButton) {
        
        let tag = sender.tag
        for case let button as MyNumberNewRequestRadioButton in frequencyListStackView.subviews {
            
            button.radioIconView.isHighlighted = false
        }
        
        sender.radioIconView.isHighlighted = true
        
        if let frequency = RequestSheduleView.RequestType.SheduleType.Frequency(rawValue: tag) {
            self.requestType = RequestType.shedule(.frequency(frequency))
            
            frequencyHeaderView.label.text = frequency.toString.localized
            frequencyHeaderView.tag = 0
            UIView.animate(withDuration: 1.2, delay: 0.0, usingSpringWithDamping: 0.72, initialSpringVelocity: 0.7, options: [.curveEaseIn, .allowUserInteraction], animations: {
                
                self.frequencyHeaderView.dropDownArror.transform = CGAffineTransform.identity
                self.frequencyListStackView.isHidden  = true
                
                self.layoutIfNeeded()
                
            }) { (flag) in
                
                
            }
        }
    }
    
    @objc func touchUpOnRequestTypeRadioButton(_ sender :MyNumberNewRequestRadioButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "HideKeyBoardNotify"), object: nil)
        if sender.radioIconView.isHighlighted == true { return }
        
        switch sender.tag {
        case 0:
            
            self.requestType = RequestType.now
            _didTappedOnRequestNow(sender)
            break
        case 1:
            self.requestType = RequestType.shedule(.none)
            _didTappedOnScheduleRequest(sender)
            break
        default: break
        }
        
        // sendActions(for: .valueChanged)
        for case let button as MyNumberNewRequestRadioButton in radioButtonStackView.subviews {
            
            button.radioIconView.isHighlighted = false
        }
        sender.radioIconView.isHighlighted = true
    }
    
    @objc func touchUpOnSheduleTypeRadioButton(_ sender: MyNumberNewRequestRadioButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "HideKeyBoardNotify"), object: nil)
        if sender.radioIconView.isHighlighted == true { return }
        
        switch sender.tag {
        case 0:
            self.requestType = RequestType.shedule(.date(.none))
            _didTappedOnDate(sender)
            break
        case 1:
            self.requestType = RequestType.shedule(.frequency(.none))
            _didTappedOnFrequency(sender)
            break
        default: break
        }
        
        //  sendActions(for: .valueChanged)
        for case let button as MyNumberNewRequestRadioButton in requestTypeStackView.subviews {
            
            button.radioIconView.isHighlighted = false
        }
        sender.radioIconView.isHighlighted = true
    }
    
    private func _didTappedOnRequestNow(_ sender: MyNumberNewRequestRadioButton) {
        
        if requestTypeStackView.isHidden == false || datePickerView.isHidden == false || frequencyView.isHidden == false {
            
            for case let button as MyNumberNewRequestRadioButton in self.requestTypeStackView.subviews {
                button.radioIconView.isHighlighted = false
            }
            
            for case let button as MyNumberNewRequestRadioButton in self.frequencyListStackView.subviews {
                button.radioIconView.isHighlighted = false
            }
            
            UIView.animate(withDuration: 1.2, delay: 0.0, usingSpringWithDamping: 0.72, initialSpringVelocity: 0.7, options: [.curveEaseIn, .allowUserInteraction], animations: {
                self.dateView.isHidden = true
                self.selectedDateLbl.text = ""
                self.requestTypeStackView.isHidden = true
                self.optionsContainerView.isHidden = true
                self.datePickerView.isHidden = true
                self.frequencyView.isHidden  = true
                self.frequencyListStackView.isHidden = true
                self.layoutIfNeeded()
                
            }) { (flag) in
                
                
            }
        }
    }
    
    private func _didTappedOnScheduleRequest(_ sender: MyNumberNewRequestRadioButton) {
        
        if requestTypeStackView.isHidden == true || datePickerView.isHidden == false || frequencyView.isHidden == false {
            
            for case let button as MyNumberNewRequestRadioButton in self.frequencyListStackView.subviews {
                button.radioIconView.isHighlighted = false
            }
            
            UIView.animate(withDuration: 1.2, delay: 0.0, usingSpringWithDamping: 0.72, initialSpringVelocity: 0.7, options: [.curveEaseIn, .allowUserInteraction], animations: {
                self.dateView.isHidden = true
                self.selectedDateLbl.text = ""
                self.requestTypeStackView.isHidden = false
                self.optionsContainerView.isHidden = true
                self.datePickerView.isHidden = true
                self.frequencyView.isHidden  = true
                
                self.layoutIfNeeded()
                
            }) { (flag) in
                
            }
        }
    }
    
    private func _didTappedOnDate(_ sender: MyNumberNewRequestRadioButton) {
        
        
        if datePickerView.isHidden == true || frequencyView.isHidden == false {
            
            self.frequencyListStackView.alpha = 1.0
            self.frequencyView.alpha = 1.0
            self.datePickerView.alpha = 0.0
            
            for case let button as MyNumberNewRequestRadioButton in self.frequencyListStackView.subviews {
                button.radioIconView.isHighlighted = false
            }
            

            
            UIView.animate(withDuration: 1.2, delay: 0.0, usingSpringWithDamping: 0.72, initialSpringVelocity: 0.7, options: [.curveEaseIn, .allowUserInteraction], animations: {
                self.datePicker.minimumDate = Date()
                self.frequencyView.isHidden  = false
                self.frequencyListStackView.isHidden = false
                self.optionsContainerView.isHidden = false
                self.datePickerView.isHidden = false
                self.datePickerView.alpha = 1.0
                self.layoutIfNeeded()
                
            }) { (flag) in
                
            }
                
         
             UIView.animate(withDuration: 1.2, delay: 0.0, usingSpringWithDamping: 0.72, initialSpringVelocity: 0.7, options: [.curveEaseIn, .allowUserInteraction], animations: {
                 self.datePicker.minimumDate = Date()
                 self.frequencyView.isHidden  = false
                 self.frequencyListStackView.isHidden = false
                 self.optionsContainerView.isHidden = false
                 self.datePickerView.isHidden = false
                 self.datePickerView.alpha = 1.0
                 self.layoutIfNeeded()
                 
             }) { (flag) in
                 
             }
                 
           
        }
    }
    
    private func _didTappedOnFrequency(_ sender: MyNumberNewRequestRadioButton) {
        
        if frequencyView.isHidden == true || datePickerView.isHidden == false {
            
            self.datePickerView.isHidden = true
            self.datePickerView.alpha = 0.0
            self.frequencyView.alpha = 1.0
            self.frequencyListStackView.alpha = 1.0
            
            for case let button as MyNumberNewRequestRadioButton in self.frequencyListStackView.subviews {
                button.radioIconView.isHighlighted = false
            }
            

            DispatchQueue.main.async {

         //   UIView.animate(withDuration: 1.2, delay: 0.0, usingSpringWithDamping: 0.72, initialSpringVelocity: 0.7, options: [.curveEaseIn, .allowUserInteraction], animations: {
                self.dateView.isHidden = true
                self.selectedDateLbl.text = ""
                self.frequencyListStackView.isHidden  = true
                self.optionsContainerView.isHidden = false
                self.frequencyView.isHidden  = false
                self.frequencyView.alpha = 1.0
                self.layoutIfNeeded()
                
          //  }) { (flag) in
                
          //  }
            
        }
                
        
        }
    }
}

protocol MyNumberNewRequestViewDeligate :class {
    
    func myNumberNewRequestView(_ view :RequestMoneyView, contactButtonTapped mobileNumberView :MobileNumberView)
    func myNumberNewRequestView(_ view :RequestMoneyView, otherContactButtonTapped mobileNumberView :MobileNumberView)
    
    func myNumberNewRequestView(_ view :RequestMoneyView, mobileNumberView :MobileNumberView, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    
    
    func myNumberNewRequestView(_ view :RequestMoneyView, otherMobileNumberView numberView :MobileNumberView, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    
    func myNumberNewRequestView(_ view :RequestMoneyView, requestNowOptionTapped requestSheduleView :RequestSheduleView)
    
    func myNumberNewRequestView(_ view :RequestMoneyView, sheduleRequestOptionTapped requestSheduleView :RequestSheduleView)
    func myNumberNewRequestView(_ view :RequestMoneyView, didPickNewDate requestSheduleView :RequestSheduleView)
    func myNumberNewRequestView(_ view :RequestMoneyView, didPickNewFrequency requestSheduleView :RequestSheduleView)
    
    func myNumberNewRequestView(_ view :RequestMoneyView, didTappedOn attachementsView :AttachementsView)
    
    func myNumberNewRequestView(_ view :RequestMoneyView, scrollEnable amountView :AmountView)
    func myNumberNewRequestView(_ view :RequestMoneyView, scrollDisable mobileView :MobileNumberView)
    func myNumberNewRequestView(_ view :RequestMoneyView, scrollDisable requestSheduleView :RequestSheduleView)
    
}


///Request Money View
class RequestMoneyView: MyNumberNewRequastViewBase, requestMoneyAmountCallback {
    
    var mynumber = MyNumberViewController()
    
    func paymentCallOtherNumberThroughDelegate() {
        
    }
    
    
    func didGetAmount(_ amount: String, withPlanType: String) {
        deligate?.myNumberNewRequestView(self, scrollEnable: amountView)
        println_debug(amount)
        amountView.textField.text = amount
        
        amountView.rechargeTypeLbel.text = withPlanType.localized
        
        remarksView.focus()
        attachementsView.focus()
        requestSheduleView.focus()
        
        print("didGetAmount---\(amount)")
        print("didGetAmount plan--\(withPlanType.localized)")

        requestSheduleView.frequencyHeaderView.tag = 0
              UIView.animate(withDuration: 1.2, delay: 0.0, usingSpringWithDamping: 0.72, initialSpringVelocity: 0.7, options: [.curveEaseIn, .allowUserInteraction], animations: {
                  
                  self.requestSheduleView.frequencyHeaderView.dropDownArror.transform = CGAffineTransform.identity
                  self.requestSheduleView.frequencyListStackView.isHidden  = true
                  
                  self.requestSheduleView.layoutIfNeeded()
                  
              }) { (flag) in
                  
                  
              }

    }
        
    var parentView : UIViewController?
    var updatedText = String()
    var myTimer: Timer?

    
    enum Mode {
        
        case myNumber
        case otherNumber
    }
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var mobileNumberView: MobileNumberView!
    @IBOutlet weak var otherMobileNumberView: MobileNumberView?
    @IBOutlet weak var requestForView: RequestForView!
    @IBOutlet weak var amountView: AmountView!
    @IBOutlet weak var remarksView: RemarkView!
    @IBOutlet weak var attachementsView: AttachementsView!
    @IBOutlet weak var requestSheduleView: RequestSheduleView!
    
    @IBOutlet weak var photoSelectionView : UIView!
        {
        didSet
        {
            self.photoSelectionView.isHidden = true
            self.photoSelectionView.layer.backgroundColor = UIColor.black.withAlphaComponent(0.5).cgColor
        }
    }
    
    weak var deligate :MyNumberNewRequestViewDeligate?
    
    private var _isShownKeyPad = false
    
    public private(set) var mode = Mode.myNumber {
        
        didSet {
            if mode == .otherNumber {
                
            } else {
                
                guard let stackView  = stackView, let view = otherMobileNumberView else { return }
                
                func remove() {
                    
                    stackView.removeArrangedSubview(view)
                    view.removeFromSuperview()
                }
                
                if Thread.isMainThread {
                    
                    remove()
                } else {
                    
                    DispatchQueue.main.async {
                        remove()
                    }
                } 
            }
        }
    }
    
    lazy var _currencyFormatter: NumberFormatter = {
        
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
       
        //formatter.locale = Locale.current
//        formatter.minimumFractionDigits = 0
//        formatter.maximumFractionDigits = 0
//        formatter.alwaysShowsDecimalSeparator = false
        //formatter.numberStyle = .currency
        //formatter.currencySymbol = ""
        
        return formatter
    }()
    
    static func newInstance(mode :Mode) ->RequestMoneyView? {
        
        let view = self.newInstance(xibName: "RequestMoneyView")
       // let view = self._newinstance

        //print("RequestMoneyView----\(view)")

        view?.mode = mode
        //print("RequestMoneyView----\(view?.mode)")
        return view
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        _initilize()
        _addActions()
        _addTags()
        NotificationCenter.default.addObserver(self, selector: #selector(hidePhotoSelectionView), name: NSNotification.Name(rawValue: "PhotoSelectionViewEnable"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleTabChange), name: NSNotification.Name(rawValue: "TabSectionChangeNotify"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyBoardHide), name: NSNotification.Name(rawValue: "HideKeyBoardNotify"), object: nil)
    }
    
    @objc func handleTabChange()
    {
       // if mode == .myNumber {
            reset()
            _initilize()
            _addActions()
            _addTags()
        deligate?.myNumberNewRequestView(self, sheduleRequestOptionTapped: requestSheduleView)       // }
    }
    
    @objc func handleKeyBoardHide() {
        self.endEditing(true)
    }
    
    @objc func hidePhotoSelectionView()
    {
        UserDefaults.standard.set(false, forKey: "PhotoViewEnabled")
        self.photoSelectionView.isHidden = true
    }
    
    override func focus() {
        
        if _isShownKeyPad == true { return }
        _isShownKeyPad = true
        
        //mobileNumberView.searchTextField.becomeFirstResponder()
    }
    
    override func reset() {
        print("Requestmoney reset called------")
//        _initilize()
        mobileNumberView.reset()
        otherMobileNumberView?.reset()
        requestForView.reset()
        amountView.reset()
        remarksView.reset()
        attachementsView.reset()
        requestSheduleView.reset()
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseIn], animations: {
            self.layoutIfNeeded()
        }) { (flag) in
            
        }
    }
    
    public func _initilize() {
        print("Requestmoney _initilize called------")

        mobileNumberView.isHidden = false
        otherMobileNumberView?.isHidden = true
        requestForView.isHidden = true
        amountView.isHidden = true
        remarksView.isHidden = true
        attachementsView.isHidden = true
        requestSheduleView.isHidden = true
        
        amountView.textField.delegate = self
        _setUpMobileNumberView(view: mobileNumberView)
        _setUpMobileNumberView(view: otherMobileNumberView)
        
        attachementsView.delegate = self
    }
    
    private func _setUpMobileNumberView(view :MobileNumberView?) {
        
        guard let view = view, let searchTextField = view.searchTextField  else { return }
        
        searchTextField.theme.font = UIFont.systemFont(ofSize: 13)
        searchTextField.theme.bgColor = UIColor.white
        searchTextField.theme.borderColor = UIColor.lightGray
        searchTextField.theme.separatorColor = UIColor.white
        searchTextField.theme.cellHeight = 50
        searchTextField.maxNumberOfResults = 5
        searchTextField.maxResultsListHeight = 200
        searchTextField.highlightAttributes = [NSAttributedString.Key.backgroundColor: UIColor.yellow, NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 13)]
        
        searchTextField.delegate = self
        //searchTextField.text = commonPrefixMobileNumber
    }
    
    private func _addActions() {
        
        mobileNumberView.addTarget(self, action: #selector(primaryActionTriggered(MobileNumberView :)), for: .primaryActionTriggered)
        otherMobileNumberView?.addTarget(self, action: #selector(primaryActionTriggered(OtherMobileNumberView:)), for: .primaryActionTriggered)
        requestForView.addTarget(self, action: #selector(valueChanged(RequestForView :)), for: .valueChanged)
        requestForView.addTarget(self, action: #selector(primaryActionTriggered(RequestForView:)), for: .primaryActionTriggered)
        
        amountView.addTarget(self, action: #selector(editingDidBegin(AmountView :)), for: .editingDidBegin)
        amountView.addTarget(self, action: #selector(editingChanged(AmountView :)), for: .editingChanged)
        amountView.addTarget(self, action: #selector(editingDidEnd(AmountView :)), for: .editingDidEnd)
        
        remarksView.addTarget(self, action: #selector(editingDidBegin(RemarkView :)), for: .editingDidBegin)
        remarksView.addTarget(self, action: #selector(editingChanged(RemarkView :)), for: .editingChanged)
        remarksView.addTarget(self, action: #selector(editingDidEnd(RemarkView :)), for: .editingDidEnd)
        
        attachementsView.addTarget(self, action: #selector(touchUpInside(AttachementsView :)), for: .touchUpInside)
        attachementsView.addTarget(self, action: #selector(primaryActionTriggered(AttachementsView :)), for: .primaryActionTriggered)
        
        requestSheduleView.addTarget(self, action: #selector(valueChanged(RequestSheduleView :)), for: .valueChanged)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTappedToClose(tapGestureRecognizer:)))
        photoSelectionView.isUserInteractionEnabled = true
        photoSelectionView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    
    @objc func viewTappedToClose(tapGestureRecognizer: UITapGestureRecognizer)
    {
        UserDefaults.standard.set(false, forKey: "PhotoViewEnabled")
        photoSelectionView.isHidden = true
    }

    private func _addTags() {
        
        if let mobileNumberView = mobileNumberView, let _ = mobileNumberView.searchTextField {
            
            // mobileNumberView.tag = 0
            // searchTextField.tag = 0
        }
        
        if let mobileNumberView = otherMobileNumberView, let _ = mobileNumberView.searchTextField {
            
            //mobileNumberView.tag = 1
            //searchTextField.tag = 1
        }
    }
    
    private func hideHelpButton()
    {
        photoSelectionView.isHidden = true
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        attachementsView.touchUpInside()
    }
    
    @IBAction func cameraButtonAction(_ sender : UIButton)
    {
        UserDefaults.standard.set(false, forKey: "PhotoButtonTapped")
        self.hideHelpButton()
    }
    
    @IBAction func photoButtonAction(_ sender : UIButton)
    {
        UserDefaults.standard.set(true, forKey: "PhotoButtonTapped")
        self.hideHelpButton()
    }
    func showOtherNumberView() {
        otherMobileNumberView?.focus()
    }
    
    func showRequestForOptions() {
        print("RequestMoneyView showRequestForOptions called====")
        requestForView.focus()
    }
    
    func showAmountView() {
        amountView.focus()
    }
    
    func showRemarksView() {
        remarksView.focus()
    }
    
    @objc func primaryActionTriggered(MobileNumberView sender: MobileNumberView) {
        print("primaryActionTriggered MobileNumberView")
        deligate?.myNumberNewRequestView(self, sheduleRequestOptionTapped: requestSheduleView)
        deligate?.myNumberNewRequestView(self, scrollDisable: mobileNumberView)
        requestForView.reset()
        amountView.reset()
        remarksView.reset()
        attachementsView.reset()
        requestSheduleView.reset()
        requestForView.dropdownArrow.transform = CGAffineTransform.identity
        requestSheduleView.frequencyHeaderView.dropDownArror.transform = CGAffineTransform.identity
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseIn], animations: {
            self.layoutIfNeeded()
        }) { (flag) in
            
        }
     //   mobileNumberView.operatorLbl.text = ""
        mobileNumberView.clearButton.isHidden = true
        _initilize()
        _addActions()
        _addTags()
        mobileNumberView.searchTextField.text = commonPrefixMobileNumber
        mobileNumberView.searchTextField.becomeFirstResponder()
        //deligate?.myNumberNewRequestView(self, contactButtonTapped: sender)
    }
    
    @objc func primaryActionTriggered(RequestForView sender: RequestForView) {
        amountView.unFocus()
    }
    
    @objc func primaryActionTriggered(OtherMobileNumberView sender: MobileNumberView) {
        deligate?.myNumberNewRequestView(self, sheduleRequestOptionTapped: requestSheduleView)
        deligate?.myNumberNewRequestView(self, scrollDisable: mobileNumberView)
     //   otherMobileNumberView?.operatorLbl.text = ""
        otherMobileNumberView?.clearButton.isHidden = true
        otherMobileNumberView?.reset()
        requestForView.reset()
        amountView.reset()
        remarksView.reset()
        attachementsView.reset()
        requestSheduleView.reset()
        requestForView.dropdownArrow.transform = CGAffineTransform.identity
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseIn], animations: {
            self.layoutIfNeeded()
        }) { (flag) in
            
        }
       
        otherMobileNumberView?.isHidden = false
        requestForView.isHidden = true
        amountView.isHidden = true
        remarksView.isHidden = true
        attachementsView.isHidden = true
        requestSheduleView.isHidden = true
        
        amountView.textField.delegate = self
        _setUpMobileNumberView(view: otherMobileNumberView)
        
        attachementsView.delegate = self
        
        _addTags()
        //deligate?.myNumberNewRequestView(self, otherContactButtonTapped: sender)
    }
    
    @objc func primaryActionTriggered(AttachementsView sender: AttachementsView) {
        print("primaryActionTriggered")
        self.parentView?.view.endEditing(true)
    }
    
    
    @objc func valueChanged(RequestForView sender: RequestForView) {
        mobileNumberView.searchTextField.resignFirstResponder()
        deligate?.myNumberNewRequestView(self, scrollDisable: requestSheduleView)
        amountView.reset()
        remarksView.reset()
        attachementsView.reset()
        requestSheduleView.reset()
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveEaseIn], animations: {
            self.layoutIfNeeded()
        }) { (flag) in
            
        }
        
        remarksView.isHidden = true
        attachementsView.isHidden = true
        requestSheduleView.isHidden = true
        
        amountView.textField.delegate = self
        attachementsView.delegate = self
        
        amountView.unFocus()
        amountView.focus()
        println_debug("value = \(sender.selectedIndex)")
        
    }
    
    @objc func editingDidBegin(AmountView sender: AmountView) {
        println_debug("value = \(sender.textField.text ?? "")")
        requestSheduleView.frequencyHeaderView.tag = 0
        UIView.animate(withDuration: 1.2, delay: 0.0, usingSpringWithDamping: 0.72, initialSpringVelocity: 0.7, options: [.curveEaseIn, .allowUserInteraction], animations: {
            
            self.requestSheduleView.frequencyHeaderView.dropDownArror.transform = CGAffineTransform.identity
            self.requestSheduleView.frequencyListStackView.isHidden  = true
            
            self.requestSheduleView.layoutIfNeeded()
            
        }) { (flag) in
            
            
        }
        
        if(requestForView.textField.text == "Request Recharge".localized)
        {
            amountView.rechargeTypeLbel.isHidden = false
            amountView.rechargeTypeLbel.text = ""
            //Commented on Mar29
            if let parent = parentView {
                if mode == .myNumber {
//                    MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: mobileNumberView?.searchTextField.text ?? "", delegate: self, present: parent, otherNumber: true)
                    
                    MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: UserModel.shared.mobileNo , delegate: self, present: parent, otherNumber: true, amountValidationCheck: true)
                    
                } else {
                    MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: otherMobileNumberView?.searchTextField.text ?? "", delegate: self, present: parent, otherNumber: true, amountValidationCheck: true)
                }
            }
        } else {
            
            amountView.rechargeTypeLbel.isHidden = true
            amountView.rechargeTypeLbel.text = ""
        }
        
    }
    
    @objc func editingChanged(AmountView sender: AmountView) {

        let count = sender.textField.text?.count ?? 0
        if sender.textField.text?.last == "."
        {
            sender.textField.text?.removeLast()
            return
        }
        if count > 0 {
            deligate?.myNumberNewRequestView(self, scrollEnable: amountView)
            
            if sender.textField.text == "0"{
                requestSheduleView.unFocus()
                attachementsView.unFocus()
                remarksView.unFocus()
                amountView.focus()
            }
            else
            {
                sender.textField.placeholder = "Amount".localized
                remarksView.focus()
                attachementsView.focus()
                requestSheduleView.focus()
                requestSheduleView._initialize()
                requestSheduleView._addActions()
                requestSheduleView._addTags()
            }
            
            guard let replaced = sender.textField.text?.replacingOccurrences(of: ",", with: "") , let value = Double(replaced) else { return }
            let amount = NSNumber(value: value)
            //let strVal = _currencyFormatter.string(from: amount)
            sender.textField.text  = _currencyFormatter.string(from: amount)//"\(amount)"//strVal?.replacingOccurrences(of: " ", with: "")
        } else {
            deligate?.myNumberNewRequestView(self, scrollDisable: mobileNumberView)
            sender.textField.placeholder = "Enter Amount".localized
            requestSheduleView.unFocus()
            attachementsView.unFocus()
            remarksView.unFocus()
            amountView.focus()
            
            requestSheduleView.frequencyHeaderView.tag = 0
            UIView.animate(withDuration: 1.2, delay: 0.0, usingSpringWithDamping: 0.72, initialSpringVelocity: 0.7, options: [.curveEaseIn, .allowUserInteraction], animations: {
                
                self.requestSheduleView.frequencyHeaderView.dropDownArror.transform = CGAffineTransform.identity
                self.requestSheduleView.frequencyListStackView.isHidden  = true
                
                self.requestSheduleView.layoutIfNeeded()
                
            }) { (flag) in
                
                
            }
        }
        
    }
  
    @objc func editingDidEnd(AmountView sender: AmountView) {
        
    }
    
    @objc func editingDidBegin(RemarkView sender: RemarkView) {
        println_debug("value = \(sender.textField.text ?? "")")
    }
    
    @objc func editingChanged(RemarkView sender: RemarkView) {
        
    }
    
    @objc func editingDidEnd(RemarkView sender: RemarkView) {
        println_debug("value = \(sender.textField.text ?? "")")
    }
    
    @objc func touchUpInside(AttachementsView sender: AttachementsView) {
        
        // _resignAllResponders()
        print("touchUpInside")

        deligate?.myNumberNewRequestView(self, didTappedOn: sender)
    }
    
    
    
    @objc func valueChanged(RequestSheduleView sender: RequestSheduleView) {
        
        _resignAllResponders()
        
        switch sender.requestType {
            
        case .none: break
        case .now:
            _requestNowOptionTapped(sender: sender)
            break
        case .shedule(let sheduleType):
            _shedulewOptionTapped(sender: sender, sheduleType: sheduleType)
            break
        }
    }
    
    private func _shedulewOptionTapped(sender: RequestSheduleView, sheduleType :RequestSheduleView.RequestType.SheduleType) {
        
        switch sheduleType {
            
        case .none:
            deligate?.myNumberNewRequestView(self, sheduleRequestOptionTapped: sender)
            break
        case .date(let option):
            
            switch option {
            case .none :
                deligate?.myNumberNewRequestView(self, sheduleRequestOptionTapped: sender)
                break
            case .date(_) :
                deligate?.myNumberNewRequestView(self, didPickNewDate: sender)
                break
            }
            break
        case .frequency(let option):
            switch option {
            case .none :
                deligate?.myNumberNewRequestView(self, sheduleRequestOptionTapped: sender)
                break
            default :
                deligate?.myNumberNewRequestView(self, didPickNewFrequency: sender)
            }
            break
        }
    }
    
    private func _requestNowOptionTapped(sender: RequestSheduleView) {
        
        deligate?.myNumberNewRequestView(self, requestNowOptionTapped: sender)
    }
    
    private func _resignAllResponders() {
        
        //        if let textField = mobileNumberView.searchTextField, textField.isFirstResponder == true {
        //            textField.resignFirstResponder()
        //        }
        //
        //        if let textField = amountView.textField, textField.isFirstResponder == true {
        //            textField.resignFirstResponder()
        //        }
        //
        //        if let textField = remarksView.textField, textField.isFirstResponder == true {
        //            textField.resignFirstResponder()
        //        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "PhotoSelectionViewEnable"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "TabSectionChangeNotify"), object: nil)
    }
    
    
    private func showToastlocal(message : String) {
        
        
       // let toastView = UIView(frame: CGRect(x: 20, y: (screenHeight/2)-80, width: screenWidth-40, height: 80))
        let toastView = UIView(frame: CGRect(x: 20, y: 100, width: screenWidth-40, height: 80))
        toastView.layer.cornerRadius = 20
        toastView.layer.masksToBounds = true
        let toastLabel = UILabel()
        toastLabel.frame = CGRect(x: 10, y: 10, width: toastView.frame.size.width-20, height: 60)
        toastLabel.text = message
        toastLabel.numberOfLines = 0
        toastLabel.font = UIFont(name: appFont,size: 14.0)
        toastLabel.textAlignment = .center
        toastLabel.textColor = .white
        toastView.backgroundColor = UIColor.black
        self.bringSubviewToFront(toastLabel)
        toastView.addSubview(toastLabel)
        self.addSubview(toastView)
        UIView.animate(withDuration: 2, delay: 0.5, options: .curveEaseOut, animations: {
            toastView.alpha = 0.0
        }, completion: {(isCompleted) in
            toastView.removeFromSuperview()
        })
        
       
    }
}

extension RequestMoneyView : PhotoCameraProtocol
{
    func photoSelectionViewTapped() {
        if photoSelectionView != nil
        {
            photoSelectionView.isHidden = false
        }
    }
}

extension RequestMoneyView : UITextFieldDelegate {
    

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
       
        if textField == amountView.textField {
            
            let flag = true
           
          /*  if let textFieldText = textField.text, let textRange = Range(range, in: textFieldText) {
                updatedText = textFieldText.replacingCharacters(in: textRange, with: string)
                print(updatedText)
                let numberCharSet = "1234567890."
                if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: numberCharSet).inverted).joined(separator: "")) { return false }
                flag = self.isValidAmount(text: updatedText)
                print("mystring validation----\(flag)")

                if updatedText.count>9
                {
                    return flag == false
                }
                
            } */
            
            let strMainTxt = textField.text?.replacingOccurrences(of: ",", with: "") ?? ""
            let amountArray = strMainTxt.components(separatedBy: ".")
            let mystring = amountArray[0]
           // print("mystring without comma----\(mystring.length)")
           // print("mystring without comma----\(mystring)")

            if(mystring.length > 6) {
                textField.textColor = UIColor.red
            } else if(mystring.length > 5) && (mystring.length <= 6)  {
                textField.textColor = UIColor.green
            } else {
                textField.textColor = UIColor.black
            }
            
            
            if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
                let endOfDocument = textField.endOfDocument
                textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
                return flag == false
            }
            
            
                
                if let text = textField.text, text.count > 0 {
                    
                     let textAfterRemovingComma = text.replacingOccurrences(of: ",", with: "")
                    
                  /*  if NSString(string: UserLogin.shared.walletBal).floatValue < NSString(string: text.replacingOccurrences(of: ",", with: "")).floatValue {
                        
                       // print("Insufficient balance float----\(UserLogin.shared.walletBal)")
                       // print("Insufficient balance float----\(NSString(string: text.replacingOccurrences(of: ",", with: "")).floatValue)")

                        self.showToastlocal(message: "Insufficient balance".localized)
                       // self.alert.showToast(msg: "Contact already added in Favorites".localized)
                        amountView.textField.text = ""
                      
                        return flag == false
                    }
                    else if NSString(string: UserLogin.shared.walletBal).doubleValue < NSString(string: text.replacingOccurrences(of: ".", with: "")).doubleValue {
                        
                        print("Insufficient balance double----")
                        self.showToastlocal(message: "Insufficient balance".localized)
                        amountView.textField.text = ""
                      
                        //mmkLabel.isHidden = true
                        
                         return flag == false
                    } */
                    
                    if textAfterRemovingComma.count > 9 {
                        textField.text = String((textField.text?.dropLast())!)
                    }
                }
                
            return flag
        }
        
    
        if mode == .myNumber {
            
            return _mobileNumberSearchFieldChangedAtMyNumberMode(textField :textField, shouldChangeCharactersIn :range, replacementString :string)
        } else {
            
            return _mobileNumberSearchFieldChangedAtOtherNumberMode(textField :textField, shouldChangeCharactersIn :range, replacementString :string)
        }
    }
    
    private func _mobileNumberSearchFieldChangedAtMyNumberMode(textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let count = textField.text?.count ?? 0
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let validations = PayToValidations().getNumberRangeValidation(text)
        if count < validations.min {
            
            secondNumberInvalid()
        }
        
        return deligate?.myNumberNewRequestView(self, mobileNumberView: mobileNumberView, shouldChangeCharactersIn: range, replacementString: string) ?? true
    }
    
    
    func isValidAmount(text :String) -> Bool {
        
        if text.contains(".") {
            let amountArray = text.components(separatedBy: ".")
            
            if amountArray.count > 2 {
                return false
            }
            
            if let last = amountArray.last {
                if last.count > 2 {
                    return false
                } else {
                    return true
                }
            }
            return true
        } else {
            let mystring = text.replacingOccurrences(of: ",", with: "")
            let number = NSDecimalNumber(string: mystring)
            
            let formatter = NumberFormatter()
            
            formatter.groupingSeparator = ","
            formatter.groupingSize = 3
            formatter.usesGroupingSeparator = true
            var num = formatter.string(from: number)
            if num == "NaN" {
                num = ""
            }
            
            amountView.textField.text = num
            return false
        }
    }
    
    func firstNumberInvalid() {
        
        requestSheduleView.unFocus()
        otherMobileNumberView?.unFocus()
        attachementsView.unFocus()
        remarksView.unFocus()
        amountView.unFocus()
        requestForView.unFocus()
    }
    
    func secondNumberInvalid() {
        
        requestSheduleView.unFocus()
        attachementsView.unFocus()
        remarksView.unFocus()
        amountView.unFocus()
        requestForView.unFocus()
    }
    
    private func _mobileNumberSearchFieldChangedAtOtherNumberMode(textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let count = textField.text?.count ?? 0
        if let otherMobileNumberView = otherMobileNumberView, let searchField = otherMobileNumberView.searchTextField, textField === searchField {
            
            if count < textField.maxLength {
                
                secondNumberInvalid()
            }
            
            return deligate?.myNumberNewRequestView(self, otherMobileNumberView: otherMobileNumberView, shouldChangeCharactersIn: range, replacementString: string) ?? true
        } else {
            
            if count < textField.maxLength {
                
                firstNumberInvalid()
            }
            
            return deligate?.myNumberNewRequestView(self, mobileNumberView: mobileNumberView, shouldChangeCharactersIn: range, replacementString: string) ?? true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField === amountView.textField {
            
            guard let replaced = textField.text?.replacingOccurrences(of: ",", with: "") , let value = Double(replaced) else { return }

            let amount = NSNumber(value: value)

            textField.text = _currencyFormatter.string(from: amount)
        }
        
    }
}

extension RequestMoneyView {
    
    var mobileNumber: String? {
        return mobileNumberView.searchTextField.text
    }
    
    var amount: String? {
        return amountView.textField.text
    }
    
    var remark: String? {
        return remarksView.textField.text
    }
}

