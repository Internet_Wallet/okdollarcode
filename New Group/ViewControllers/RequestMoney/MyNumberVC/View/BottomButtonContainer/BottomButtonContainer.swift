//
//  BottomButtonContainer.swift
//  OK
//
//  Created by palnar on 27/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

final class BottomButtonContainer :UIStackView {
    
    @IBOutlet weak var submitButtonView : UIView!
    @IBOutlet weak var viewAllRequestButtonView : UIView!
    
//    var showSubmitButton :Bool = false {
//        
//        didSet {
//            
//            self.viewAllRequestButtonView.backgroundColor = UIColor(patternImage: UIImage(named: "strip")!)
//            
//            if showSubmitButton == oldValue { return }
//            
//            if showSubmitButton == true {
//                
//                self.submitButtonView.isHidden = false
//                
//            } else {
//                
//                self.submitButtonView.isHidden = true
//                
//            }
//            
//            layoutIfNeeded()
//        }
//    }
//    
//    var hideViewAllRequestButton :Bool = false {
//        didSet {
//            self.viewAllRequestButtonView.isHidden = true
//            self.submitButtonView.isHidden = true
//            layoutIfNeeded()
//        }
//    }
//    
//    var showViewAllRequestButton :Bool = false {
//        didSet {
//            self.viewAllRequestButtonView.isHidden = false
//            self.submitButtonView.isHidden = true
//            layoutIfNeeded()
//        }
//    }
}
