//
//  MyNumberNewRequestController.swift
//  OK
//
//  Created by palnar on 21/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation
import Contacts

protocol MyNumberNewRequestControllerDeligate :class {
    
    func myNumberNewRequestController(_ controller :MyNumberNewRequestController, view :RequestMoneyView, requestNowOptionTapped requestSheduleView :RequestSheduleView)
    
    func myNumberNewRequestController(_ controller :MyNumberNewRequestController, view :RequestMoneyView, contactButtonTapped mobileNumberView :MobileNumberView)
    
    func myNumberNewRequestController(_ controller :MyNumberNewRequestController, view :RequestMoneyView, sheduleRequestOptionTapped requestSheduleView :RequestSheduleView)
    func myNumberNewRequestController(_ controller :MyNumberNewRequestController, view :RequestMoneyView, didPickNewDate requestSheduleView :RequestSheduleView)
    func myNumberNewRequestController(_ controller :MyNumberNewRequestController, view :RequestMoneyView, didPickNewFrequency requestSheduleView :RequestSheduleView)
    
    func myNumberNewRequestController(_ controller :MyNumberNewRequestController, willSendRequest params :RequestMoneyAPIGateWay.InputParams)
    
    func myNumberNewRequestController(_ controller :MyNumberNewRequestController, didReceiveSuccessResponse params :RequestMoneyAPIGateWay.InputParams, responce :RequestMoneyAPIGateWay.Resposne)
    
    func myNumberNewRequestController(_ controller :MyNumberNewRequestController, didReceiveFailoureResponse params :RequestMoneyAPIGateWay.InputParams, responce :RequestMoneyAPIGateWay.Resposne)
    
    func myNumberNewRequestController(_ controller :MyNumberNewRequestController, needReload view :RequestMoneyView)
    
    func myNumberNewRequestController(_ controller :MyNumberNewRequestController, scrollEnable view :AmountView)
    func myNumberNewRequestController(_ controller :MyNumberNewRequestController, scrollDisable view :MobileNumberView)
    func myNumberNewRequestController(_ controller :MyNumberNewRequestController, scrollDisable view :RequestSheduleView)
}

class MyNumberNewRequestController  {
    
    enum InternalError :Error {
        case error(String)
        
        var errorMessage :String? {
            
            switch self {
            case .error(let er): return er
            }
        }
    }
    let validObj                =   PayToValidations()
    private weak var _context :UIViewController!
    public private(set) var view :RequestMoneyView!
    let mobileNumberAcceptableCharacters = "0123456789"
    private var cnContacts = [CNContact]()
    private var _filteredContacts = [SearchTextFieldItem]()
    var myTimer: Timer?

    private var _selectedContact: Contact?
    
    weak var deligate :MyNumberNewRequestControllerDeligate?
    
    lazy var _requestMoneyGateWay: RequestMoneyAPIGateWay = {
        
        let gateWay = RequestMoneyAPIGateWay(httpClient: HTTPClient.shard)
        return gateWay
    }()
    
    init(view :RequestMoneyView, context :UIViewController) {
        print("init called------")

        self.view = view
        self._context = context
        view.deligate = self
        _initilize()
    }
    
    private func _initilize() {
        print("_initilize------")
        _fetchAllContacts()
        view.mobileNumberView.searchTextField.itemSelectionHandler = { [weak self] (filteredResults, itemPosition) in
            
            
            guard let _self = self else { return }
            guard let mobileNumberView = _self.view.mobileNumberView else { return }
            
            guard filteredResults.count > 0 else { return }
            
            let item = filteredResults[itemPosition]
            guard let number = item.subtitle else { return }
           // print("mobileNumberView itemSelectionHandler called------")
            _self.myNumberView(mobileNumberView, didPickNewSuggestedContactNumber: number)
        }
        
       
        view.otherMobileNumberView?.searchTextField.itemSelectionHandler = { [weak self] (filteredResults, itemPosition) in
            

            guard let _self = self else { return }
            guard let mobileNumberView = _self.view.otherMobileNumberView else { return }
            
            guard filteredResults.count > 0 else { return }
            
            let item = filteredResults[itemPosition]
            guard let number = item.subtitle else { return }
           // print("otherMobileNumberView itemSelectionHandler called------")

            _self.otherNumberView(mobileNumberView, didPickNewSuggestedContactNumber: number)
        }
 
    }
    
    private func _fetchAllContacts() {
        
        let store = CNContactStore()
        
        store.requestAccess(for: .contacts, completionHandler: { (granted, error) in
            
            guard granted == true else {
                
                let alert = UIAlertController(title: "Can't access contact".localized, message: "Please go to Settings -> MyApp to enable contact permission".localized, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
                self._context.present(alert, animated: true, completion: nil)
                return
            }
            
            let keysToFetch = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName), CNContactPhoneNumbersKey] as [Any]
            let request = CNContactFetchRequest(keysToFetch: keysToFetch as! [CNKeyDescriptor])
            
            do {
                try store.enumerateContacts(with: request){
                    (contact, cursor) -> Void in
                    self.cnContacts.append(contact)
                }
            } catch let error {
                NSLog("Fetch contact error: \(error)")
            }
            
            for contact in self.cnContacts {
                let fullName = CNContactFormatter.string(from: contact, style: .fullName) ?? "No Name"
                NSLog("\(fullName): \(contact.phoneNumbers.description)")
            }
        })
    }
    
    func showQRCode(qrCode :ScanObject) {
        
        println_debug("qrCode = \(qrCode)")
        guard let view = self.view /*, qrCode.number != UserModel.shared.mobileNo */ else { return }
        
        let phNumber = _trimmedNumber(view: view.mobileNumberView, number: qrCode.number)
        let number = phNumber.replacingOccurrences( of:"[^0-9]", with: "", options: .regularExpression)


       // view.mobileNumberView.searchTextField.text = number
        view.mobileNumberView.searchTextField.text = getActualNum(number,withCountryCode: "+95")
        view.mobileNumberView.searchTextField.placeholder = "Request Money Number"

      //  view.mobileNumberView.operatorLbl.text = view.mobileNumberView.getOparatorName(number)
        view.mobileNumberView.clearButton.isHidden = false
        
        print("showQRCode _verifyInBackEnd called----")
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.7) {
            
            self._verifyScanInBackEnd(number: number, completion: { [weak self] (isFound) in
                
                if isFound {
                    self?.view.showRequestForOptions()
                } else {
                    view.mobileNumberView.searchTextField.text = commonPrefixMobileNumber
                  //  view.mobileNumberView.operatorLbl.text = ""
                    view.mobileNumberView.clearButton.isHidden = true
                }
            })
        }
    }
    
    private func _trimmedNumber(view :MobileNumberView, number :String) ->String {
        

        let prefix = commonPrefixMobileNumber
        let numberLength = number.length
        let maxlength = view.searchTextField.maxLength

        if numberLength > maxlength {
            
            let start = (numberLength-(maxlength-1))+1
            let trimmed = prefix + number.substring(from: start)
            return trimmed
            
        }
        
        return number
    }
    
    func submit() {
        
        print("submit----\(view.mobileNumberView.searchTextField.text!)")
        
        let textstr = view.mobileNumberView.searchTextField.text!
        
        let validations = validObj.getNumberRangeValidation(textstr)
        print("validations----\(validations)")

        if validations.max != validations.min  {
        _verifyInBackEnd(number: textstr, completion: { [weak self] (isFound) in
         if isFound == true {
            
            print("validationsisFound----\(isFound)")

            guard appDelegate.checkNetworkAvail() == true else {
                
                self!._show(title: "No Internet", errorMeaasge: "")
                return
            }
            
            guard let viewController = self!._context else {
                
                assertionFailure("_context not found")
                return
            }
            
            alertViewObj.wrapAlert(title: nil, body: "Do you want to send Request Money?".localized, img: UIImage(named : "confirmation_icon"))
            alertViewObj.addAction(title: "Cancel".localized, style: AlertStyle.cancel, action: { return })
            
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {
                //Old code
                /*
                 let payment = PaymentAuthorisation()
                 let info = ["module_name" : kRequestMoney, "request" : request] as [String : Any]
                 
                 payment.delegateModule = self
                 payment.requestModuleData = info
                 payment.authenticate()
                 */
                
                //New code
                OKPayment.main.authenticate(screenName: "RequestMoneyMyNumber", delegate: self!)
                
            })
            alertViewObj.showAlert(controller: viewController)
            
            
         } else {
            
            self!.view.reset()
            self!.view._initilize()
            self!.view.mobileNumberView.focus()
            self!.view.mobileNumberView.becomeFirstResponder()
            
         return
         }
         })
            
        } else {
            
            guard appDelegate.checkNetworkAvail() == true else {
                
                self._show(title: "No Internet", errorMeaasge: "")
                return
            }
            
            guard let viewController = self._context else {
                
                assertionFailure("_context not found")
                return
            }
            
            alertViewObj.wrapAlert(title: nil, body: "Do you want to send Request Money?".localized, img: UIImage(named : "confirmation_icon"))
            alertViewObj.addAction(title: "Cancel".localized, style: AlertStyle.cancel, action: { return })
            
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {
                //Old code
                /*
                 let payment = PaymentAuthorisation()
                 let info = ["module_name" : kRequestMoney, "request" : request] as [String : Any]
                 
                 payment.delegateModule = self
                 payment.requestModuleData = info
                 payment.authenticate()
                 */
                
                //New code
                OKPayment.main.authenticate(screenName: "RequestMoneyMyNumber", delegate: self)
                
            })
            alertViewObj.showAlert(controller: viewController)
            
        }
        
       
       
    }
    
    
    private func _base64Attachements() -> [String] {
        
        var attachements = [String]()
        
        let attachedImages = view.attachementsView.attachements
        
        for image in attachedImages {
            let jpegCompressionQuality: CGFloat = 0.5 
            if let base64String = image.jpegData(compressionQuality: jpegCompressionQuality)?.base64EncodedString() {
                attachements.append(base64String)
            }
        }
        return attachements
    }
    
    

    
    func getImageURL( _ completionHandler: @escaping (_ urlArray: [String]) -> Void) {
        let imageBase64Array = _base64Attachements()
        var imageUrlArray = [String]()
        if imageBase64Array.count > 0 {
            var urlString = ""
            let paramString : [String:Any] = [
                "MobileNumber":UserModel.shared.mobileNo,
                "Base64String":imageBase64Array
            ]
            VillageManager.shared.uploadImageToS3(imageBase64String :paramString, handler: {(imageURL,success) in
                if success {
                    for imageStr in imageURL {
                        urlString = ""
                        urlString = imageStr.replacingOccurrences(of: " ", with: "%20")
                        imageUrlArray.append(urlString)
                    }
                }
                completionHandler(imageUrlArray)
            })
        } else {
            completionHandler(imageUrlArray)
        }
        
    }
    
    private func validateInputs() throws -> Bool {
        
        return false
    }
}

//func textFieldDidEndEditing(textField: UITextField) {
//
//    let validObj                =   PayToValidations()
//    var _context :UIViewController!
//
//    let text = textField.text
//
//    let validations = validObj.getNumberRangeValidation(text!)
//
//    let newLength = textField.text?.count ?? 0
//
//    if (newLength > validations.min && newLength <= validations.max)   {
//
//        let number = text
//        let agentCode = UserModel.shared.mobileNo
//        var searchString = ""
//        if(agentCode.starts(with: "0095")) {
//            searchString  = "0" + agentCode.substring(from: 4)
//        } else {
//            searchString = agentCode
//        }
//        if searchString == number {
//             let vc = _context
//            alertViewObj.wrapAlert(title: nil, body: "You cannot request money to your own number".localized, img: UIImage(named : "Contact Mobile Number"))
//            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
//                textField.text = commonPrefixMobileNumber
//               // MobileNumberView.operatorLbl.text = ""
//               // textField.clearButton.isHidden = true
//            })
//            alertViewObj.showAlert(controller: vc)
//           // return false
//        }
//
//        _verifyInBackEnd(number: number, completion: {  (isFound) in
//
//            guard let _self = self else { return }
//            searchTextField.resignFirstResponder()
//            if isFound == true {
//
//                if _self.view.mode == .myNumber {
//
//                    _self.view.showRequestForOptions()
//                } else {
//
//                    _self.view.showOtherNumberView()
//                }
//            } else {
//
//                mobileNumberView.searchTextField.text = commonPrefixMobileNumber
//                view.mobileNumberView.operatorLbl.text = ""
//                view.mobileNumberView.clearButton.isHidden = true
//            }
//        })
//    }
//}


extension MyNumberNewRequestController: BioMetricLoginDelegate{
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String)
    {
        println_debug("Authorised")
        if(isSuccessful)
        {
            progressViewObj.showProgressView()
            self.getImageURL { (urlArray) in
                DispatchQueue.main.async {
                do {
                        let request =  try self._buildInputParams(urlArray : urlArray)
                    print("biometricAuthentication check----\(request)")
                        self._sendRequest(request)
                    } catch let e {
                        progressViewObj.removeProgressView()
                        
                        guard let error = e as? InternalError, let errorMessage = error.errorMessage else { return }
                        self._show(title: "Invalid", errorMeaasge: errorMessage)
                    }
                }
            }
        }
        else
        {
            println_debug("User not authorized")
            
        }
    }
}

extension MyNumberNewRequestController : WebServiceResponseDelegate {
    
    func webResponse(withJson json: AnyObject, screen: String) {
        
        if screen == "mRequestMoneyMyNumber" {
            
        }
    }
}

extension MyNumberNewRequestController {
    /*
    func paymentAuthorisationSuccess(withModule moduleData: [String : Any]) {
        
        if let moduleName = moduleData["module_name"] as? String, moduleName == kRequestMoney,
            let request = moduleData["request"] as? RequestMoneyAPIGateWay.InputParams {
            
            _sendRequest(request)
        }
    }
 */
    
    private func _sendRequest(_ params :RequestMoneyAPIGateWay.InputParams) {
        
        deligate?.myNumberNewRequestController(self, willSendRequest: params)
        _requestMoneyGateWay.submit(params, { [weak self] (response) in
            
            progressViewObj.removeProgressView()
            self?._handleRequestMoneyResponse(params: params, response: response)
        })
    }
    
    private func _handleRequestMoneyResponse(params :RequestMoneyAPIGateWay.InputParams, response :RequestMoneyAPIGateWay.Resposne) {
        
        print("params-----\(params)")
        print("response-----\(response)")

        do {
            let result = try response.result()
            if result.msg == "Sucess", result.code == 200  {
                
                _show(title: "", errorMeaasge: "Your Request Sent Successfully".localized, iconImageName: "info_success")
                deligate?.myNumberNewRequestController(self, didReceiveSuccessResponse: params, responce: response)
            } else {
                
                
                _show(title: "", errorMeaasge: result.msg, iconImageName: "error")
                deligate?.myNumberNewRequestController(self, didReceiveFailoureResponse: params, responce: response)
            }
        } catch let e {
            
            guard let error = e as? GateWayError else { return }
            
            deligate?.myNumberNewRequestController(self, didReceiveFailoureResponse: params, responce: response)
            if error.errorMessage == "You cannot request money for same destination number and amount within 5 mins" {
                _show(title: "", errorMeaasge: "You cannot request to same destination mobile number within 5 minutes".localized, iconImageName: "confirmation_icon")
            } else {
                 _show(title: "", errorMeaasge: error.errorMessage.localized, iconImageName: "confirmation_icon")
            }
        }
    }
    
    private func _schduleDetails(requestType :RequestSheduleView.RequestType) throws -> (isSchduled :Bool, schduledType :RequestMoneySchduledType, schduledTime :String?) {
        
        var isSchduled = false
        var schduledType = RequestMoneySchduledType.requestNow
        var schduledTime :String? = nil
        
        print("_schduleDetails schduledType-----\(schduledType)")
        print("_schduleDetails RequestSheduleView.RequestType-----\(requestType)")

        switch requestType {
        case .none:
            throw InternalError.error("Please select a request type")
        case .now:
            print("_schduleDetails now called-----")

            isSchduled = false
            schduledType = .requestNow
            break
        case .shedule(let scheduleType):
            switch scheduleType {
                
            case .none :
                throw InternalError.error("Please select a date or frequency")
            case .date(let value) :
                
                switch value {
                    
                case .none :
                    throw InternalError.error("Please select a date")
                    
                case .date(let date) :
                    print("_schduleDetails date called-----\(date)")

                    isSchduled = true
                    schduledType = .normal
                    schduledTime = stringFromDate(date: date as NSDate, format: GlobalConstants.Strings.defaultDateFormat)
                    //print("_schduleDetails date schduledTime-----\(schduledTime)")

                    break
                }
                break
            case .frequency(let fr) :
                if fr == .none {
                    throw InternalError.error("Please select a frequency")
                }
                isSchduled = true
                if let st = RequestMoneySchduledType(rawValue: fr.value) {
                    schduledType = st
                    print("_schduleDetails frequency called-----\(schduledType)")

                } else {
                    throw InternalError.error("Please select a frequency")
                }
                
                break
            }
            break
        }
        
        return (isSchduled, schduledType, schduledTime)
    }
    
    private func _trim(mobileNumber number :String, countryCode :String) ->String {
        
        var startIndex = 0
        let countryCodeWithZero =  countryCode.replacingOccurrences(of: "+", with: "00")
        
        if  number.starts(with: "0")  { startIndex = 1 }
        
        return countryCodeWithZero + number.substring(from: startIndex)
    }
    
    private func _sourceMobileNumber(view :RequestMoneyView, userModel :UserModel) -> String? {
        
        if view.mode == .myNumber { return userModel.mobileNo }
        
        guard let number = view.otherMobileNumberView?.searchTextField.text else {  return nil }
        
        return _trim(mobileNumber: number, countryCode: userModel.countryCode)
    }
    
    private func _destinationMobileNumber(view :RequestMoneyView, userModel :UserModel) -> String? {
        
        guard let number = view.mobileNumberView.searchTextField.text else { return nil }
        
        return _trim(mobileNumber: number, countryCode: userModel.countryCode) 
        //
     /*   if view.mode == .myNumber {
            
            guard let number = view.mobileNumberView.searchTextField.text else { return nil }
            
            var startIndex = 0
            let countryCodeWithZero =  userModel.countryCode.replacingOccurrences(of: "+", with: "00")
            
            if  number.starts(with: "0")  { startIndex = 1 }
            
            return countryCodeWithZero + number.substring(from: startIndex)
        } else {
            
            guard let number = view.otherMobileNumberView?.searchTextField.text else {  return nil }
            
            var startIndex = 0
            let countryCodeWithZero =  userModel.countryCode.replacingOccurrences(of: "+", with: "00")
            
            if  number.starts(with: "0")  { startIndex = 1 }
            
            return countryCodeWithZero + number.substring(from: startIndex)
        }
 */
    }
    
    private func _buildInputParams(urlArray : [String]) throws ->RequestMoneyAPIGateWay.InputParams  {
        
        guard let view = self.view else { throw InternalError.error("Internal Error") }
        
        let userModel = UserModel.shared
        let thisMobileNumber = userModel.mobileNo
        
        guard let source = _sourceMobileNumber(view: view, userModel: userModel) else {
            throw InternalError.error("Please enter a valid mobile number")
        }
        
        guard let destination = _destinationMobileNumber(view: view, userModel: userModel) else {
            throw InternalError.error("Please enter a valid mobile number")
        }
        
        guard var amount = view.amountView.textField.text?.replacingOccurrences(of: ",", with: "").components(separatedBy: ".").first else {
            throw InternalError.error("Please enter a valid Amount")
        }
        
        if amount.contains(GlobalConstants.Strings.mmk) {
            amount = amount.replacingOccurrences(of: GlobalConstants.Strings.mmk, with: "")
        }
        
        if let number = amount.addingPercentEncoding(withAllowedCharacters: .decimalDigits) {
            let phones  = number.replacingOccurrences(of: "%20", with: "")
            let phones2 = phones.replacingOccurrences(of: "%C2%A0", with: "")
            amount = phones2.removingPercentEncoding!
        }
        
        var remark = view.remark ?? ""
        if remark == "", remark == "Enter Remarks", remark == "Remarks" {
            remark = ""
        }
        
        let attachedImages = view.attachementsView.attachements
        let requestType = view.requestSheduleView.requestType 
        let currentDate = Date()
        let isMyNumber = (view.mode == .myNumber) ? RequestMoneyNumberType.myNumber : RequestMoneyNumberType.otherNumber
        println_debug("Model requestType = \(view.requestSheduleView.requestType)")

        println_debug("isMyNumber = \(isMyNumber.value)")
        var isSchduled = false
        var schduledType = RequestMoneySchduledType.requestNow
        var schduledTime :String? = nil
        
        do {
            
            let schduleDetails = try _schduleDetails(requestType: requestType)
            isSchduled = schduleDetails.isSchduled
            schduledType = schduleDetails.schduledType
            schduledTime = schduleDetails.schduledTime
            
            //println_debug("Model requestType three = \(isSchduled)---\(schduledType)===\(schduledTime)")

        } catch let e {
            
            throw e
        }
        
        let sourceNumber = (view.mode == .myNumber) ? UserModel.shared.mobileNo : getConatctNum(destination, withCountryCode: "+95")
        let destinationNum = (view.mode == .myNumber) ? getConatctNum(destination, withCountryCode: "+95") : getConatctNum(source, withCountryCode: "+95")
        
        let date = stringFromDate(date: currentDate as NSDate,format: GlobalConstants.Strings.defaultDateFormat)
        
        if schduledTime == nil {
            
            print("schduledTime nil---")
            schduledTime = date

        } 
        
       /* var date = ""
        
        if schduledTime == nil {
            print("schduledTime nil---")
             date = stringFromDate(date: currentDate as NSDate,format: GlobalConstants.Strings.defaultDateFormat)
            schduledTime = date
            
        } else {
            
            print("schduledTime not nill called---")

            date = schduledTime!
        } */
        println_debug("userModel.date = \(date)")
        //println_debug("userModel.date = \(schduledTime)")

        println_debug("userModel.simID = \(userModel.simID)")
        
        if view.amountView?.rechargeTypeLbel.text == "Top-Up Plan" {
            return RequestMoneyAPIGateWay.InputParams(mobileNumber: thisMobileNumber,
                                                      simId: userModel.simID,
                                                      msid: userModel.msid,
                                                      ostype: Int(userModel.osType) ?? 0,
                                                      otp: userModel.simID,
                                                      source: sourceNumber,
                destination: destinationNum,
                amount: amount,
                date: date,
                remarks: remark,
                localTransactionType: "TOPUP",
                transactionType: "RECHARGE",
                isSchduled: isSchduled,
                schduledType: schduledType,
                schduledTime: schduledTime ?? "",
                requestModuleType: .requestMoney,
                isMynumber: isMyNumber,
                attachedImages: attachedImages,
                attachedImagesAwsUrl :urlArray
            )
        } else if view.amountView?.rechargeTypeLbel.text == "Special Offers" {
            return RequestMoneyAPIGateWay.InputParams(mobileNumber: thisMobileNumber,
                                                      simId: userModel.simID,
                                                      msid: userModel.msid,
                                                      ostype: Int(userModel.osType) ?? 0,
                                                      otp: userModel.simID,
                                                      source: sourceNumber,
                                                      destination: destinationNum,
                                                      amount: amount,
                                                      date: date,
                                                      remarks: remark,
                                                      localTransactionType: "SPECIALOFFERS",
                                                      transactionType: "RECHARGE",
                                                      isSchduled: isSchduled,
                                                      schduledType: schduledType,
                                                      schduledTime: schduledTime ?? "",
                                                      requestModuleType: .requestMoney,
                                                      isMynumber: isMyNumber,
                                                      attachedImages: attachedImages,
                                                      attachedImagesAwsUrl :urlArray
            )
        } else if view.amountView?.rechargeTypeLbel.text == "Data Plan" {
            return RequestMoneyAPIGateWay.InputParams(mobileNumber: thisMobileNumber,
                                                      simId: userModel.simID,
                                                      msid: userModel.msid,
                                                      ostype: Int(userModel.osType) ?? 0,
                                                      otp: userModel.simID,
                                                      source: sourceNumber,
                                                      destination: destinationNum,
                                                      amount: amount,
                                                      date: date,
                                                      remarks: remark,
                                                      localTransactionType: "DATAPLAN",
                                                      transactionType: "RECHARGE",
                                                      isSchduled: isSchduled,
                                                      schduledType: schduledType,
                                                      schduledTime: schduledTime ?? "",
                                                      requestModuleType: .requestMoney,
                                                      isMynumber: isMyNumber,
                                                      attachedImages: attachedImages,
                                                      attachedImagesAwsUrl :urlArray
            )
        }
        
        return RequestMoneyAPIGateWay.InputParams(mobileNumber: thisMobileNumber,
                                              simId: userModel.simID,
                                              msid: userModel.msid,
                                              ostype: Int(userModel.osType) ?? 0,
                                              otp: userModel.simID,
                                              source: sourceNumber,//getConatctNum(source, withCountryCode: "+95"),//
                                              destination: destinationNum,//destination,
                                              amount: amount,
                                              date: date,
                                              remarks: remark,
                                              localTransactionType: "RequestMoney",
                                              transactionType: "PayTo",
                                              isSchduled: isSchduled,
                                              schduledType: schduledType,
                                              schduledTime: schduledTime ?? "",
                                              requestModuleType: .requestMoney,
                                              isMynumber: isMyNumber,
                                              attachedImages: attachedImages,
                                              attachedImagesAwsUrl :urlArray
        )
    }
}

extension MyNumberNewRequestController :MyNumberNewRequestViewDeligate {
    
    func myNumberView(_ mobileNumberView :MobileNumberView, didPickNewSuggestedContactNumber phoneNumber :String) {
        
        print("myNumberView called--------")

        guard let view = self.view else { return }
        let number = _trimmedNumber(view: mobileNumberView, number: phoneNumber)
        mobileNumberView.searchTextField.text = number
        mobileNumberView.clearButton.isHidden = false
      //  mobileNumberView.operatorLbl.text = mobileNumberView.getOparatorName(number)
        _verifyInBackEnd(number: number, completion: { (isFound) in
            if isFound {
                if self.view.mode == .myNumber {
                    
                    self.view.showRequestForOptions()
                } else {
                    
                    self.view.showOtherNumberView()
                }
                
            }
//            let validations = PayToValidations().getNumberRangeValidation(number)
//
//            if view == self.mobileNumberView {
//                if validations.isRejected {
//                    PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
//                    self.mobileNumber.text = (self.mobileNumberView.country.dialCode == "+95") ? "09" : ""
//                    return false
//                }
//            }
            else {
                
                view.firstNumberInvalid()
                mobileNumberView.searchTextField.text = commonPrefixMobileNumber
              //  mobileNumberView.operatorLbl.text = ""
                mobileNumberView.clearButton.isHidden = true
            }
        })
    }
//    func response(text: String, view: UITextField) -> Bool {
//
//        let validations = PayToValidations().getNumberRangeValidation(text)
//
//        if view == self.mobileNumber {
//            if validations.isRejected {
//                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
//                self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
//                return false
//            }
//        }
//    }
    func otherNumberView(_ mobileNumberView :MobileNumberView, didPickNewSuggestedContactNumber phoneNumber :String) {
        
        print("otherNumberView called--------")

        guard let view = self.view else { return }
        let number =  _trimmedNumber(view: mobileNumberView, number: phoneNumber)
        mobileNumberView.searchTextField.text = number
        mobileNumberView.clearButton.isHidden = false
      //  mobileNumberView.operatorLbl.text = mobileNumberView.getOparatorName(number)
        _verifyInBackEnd(number: number, completion: { (isFound) in
            
            if isFound {
                
                view.showRequestForOptions()
            } else {
                
                view.secondNumberInvalid()
                mobileNumberView.searchTextField.text = commonPrefixMobileNumber
              //  mobileNumberView.operatorLbl.text = ""
                mobileNumberView.clearButton.isHidden = true
            }
        })
    }
    
    private func _openContactPicker(for view :RequestMoneyView, mobileNumberView: MobileNumberView) {
        
        guard let vc = _context else { return }
        
        let contactPicker = OKContactsPicker(delegate: nil, multiSelection: false, showIndexBar: false, subtitleCellType: SubtitleCellValue.phoneNumber)
        let navigationController = UINavigationController(rootViewController: contactPicker)
        
        contactPicker.didSelect = { [weak self] (picker, contact) in
            println_debug("contact")
            
            guard let _self = self, let phoneNumber = contact.phoneNumbers.first?.phoneNumber else { return }
            
            _self._selectedContact = contact
            let number = _self._trimmedNumber(view: mobileNumberView, number: phoneNumber)

            mobileNumberView.searchTextField.text = number
            _self._verifyInBackEnd(number: number, completion: { (isFound) in
                
                if isFound {

                    if mobileNumberView.searchTextField === view.mobileNumberView.searchTextField {
                        view.showRequestForOptions()
                    } else {
                        view.showOtherNumberView()
                    } 
                } else {
                    
                    if mobileNumberView.searchTextField === view.mobileNumberView.searchTextField {
                        view.firstNumberInvalid()
                    } else {
                        view.secondNumberInvalid()
                    }
                    
                    mobileNumberView.searchTextField.text = commonPrefixMobileNumber
                  //  view.mobileNumberView.operatorLbl.text = ""
                    view.mobileNumberView.clearButton.isHidden = true
                }
            })
        }
        
        contactPicker.didCancel = { (picker, error) in
            
            println_debug("error")
        }
        
        contactPicker.didFailed = { (picker, error) in
            
            println_debug("error")
        }
        vc.modalPresentationStyle = .fullScreen
        vc.present(navigationController, animated: true, completion: nil)
    }
    
    func myNumberNewRequestView(_ view: RequestMoneyView, contactButtonTapped mobileNumberView: MobileNumberView) {
        
        _openContactPicker(for: view, mobileNumberView: mobileNumberView)
    }
    
    func myNumberNewRequestView(_ view: RequestMoneyView, otherContactButtonTapped mobileNumberView: MobileNumberView) {
        
        _openContactPicker(for: view, mobileNumberView: mobileNumberView)
    }
    
    func myNumberNewRequestView(_ view: RequestMoneyView, mobileNumberView: MobileNumberView, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        guard let searchTextField = mobileNumberView.searchTextField else { return true }
        let text = (searchTextField.text! as NSString).replacingCharacters(in: range, with: string)

        println_debug("myNumberNewRequestVieshouldChangeCharactersIn called-----------\(text)")

        let count = searchTextField.text?.count ?? 0
        let newLength = (count + string.count - range.length)
        
        let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: mobileNumberAcceptableCharacters).inverted
        let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
        
        
        if string != filteredSet {
            return false
        }
        
        if (range.location < 2) { return false }
  
        guard validObj.getNumberRangeValidation(text).isRejected == false else {
            guard let vc = _context else { return false }
            alertViewObj.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: UIImage(named : "AppIcon"))
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
              mobileNumberView.searchTextField.text = commonPrefixMobileNumber
            //  mobileNumberView.operatorLbl.text = ""
              mobileNumberView.clearButton.isHidden = true
            })
            alertViewObj.showAlert(controller: vc)
            return false
        }
        let validations = validObj.getNumberRangeValidation(text)
        
        println_debug("validations min-----------\(validations.min)")
        println_debug("validations max-----------\(validations.max)")
        if validations.max == validations.min  {
            
        println_debug("myNumberNewRequestVieshouldChangeCharactersIn validations equal-----------\(newLength)")

        
        if newLength == validations.max {

            let number = text
            let agentCode = UserModel.shared.mobileNo
            var searchString = ""
            if(agentCode.starts(with: "0095")) {
                searchString  = "0" + agentCode.substring(from: 4)
            } else {
                searchString = agentCode
            }
            if searchString == number {
                guard let vc = _context else { return false }
                alertViewObj.wrapAlert(title: nil, body: "You cannot request money to your own number".localized, img: UIImage(named : "Contact Mobile Number"))
                alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                    mobileNumberView.searchTextField.text = commonPrefixMobileNumber
                 //   mobileNumberView.operatorLbl.text = ""
                    mobileNumberView.clearButton.isHidden = true
                })
                alertViewObj.showAlert(controller: vc)
                return false
            }
            
            _verifyInBackEnd(number: number, completion: { [weak self] (isFound) in
                
                guard let _self = self else { return }
                searchTextField.resignFirstResponder()
                if isFound == true {
                    
                    if _self.view.mode == .myNumber {
                        
                        _self.view.showRequestForOptions()
                    } else {
                        
                        _self.view.showOtherNumberView()
                    }
                } else {
                    
                    mobileNumberView.searchTextField.text = commonPrefixMobileNumber
                 //   view.mobileNumberView.operatorLbl.text = ""
                    view.mobileNumberView.clearButton.isHidden = true
                }
            })
        }else {
            
            if self.view.mode == .myNumber || self.view.mode == .otherNumber{
                
                if newLength > 11 {
                    println_debug("myNumberNewRequestVieshouldChangeCharactersIn false called-----------\(newLength)")
                    return false
                }
                
            }
        }
            
        } else {
            
            if (newLength >= validations.min && newLength <= validations.max)   {
                
                let number = text
                let agentCode = UserModel.shared.mobileNo
                var searchString = ""
                if(agentCode.starts(with: "0095")) {
                    searchString  = "0" + agentCode.substring(from: 4)
                } else {
                    searchString = agentCode
                }
                if searchString == number {
                    guard let vc = _context else { return false }
                    alertViewObj.wrapAlert(title: nil, body: "You cannot request money to your own number".localized, img: UIImage(named : "Contact Mobile Number"))
                    alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                        mobileNumberView.searchTextField.text = commonPrefixMobileNumber
                      //  mobileNumberView.operatorLbl.text = ""
                        mobileNumberView.clearButton.isHidden = true
                    })
                    alertViewObj.showAlert(controller: vc)
                    return false
                }
                
                if self.view.mode == .myNumber {
                    
                    self.view.showRequestForOptions()
                    
                } else {
                    
                    self.view.otherMobileNumberView?.isHidden = false
                }
                
              /*  _verifyInBackEnd(number: number, completion: { [weak self] (isFound) in
                    
                    guard let _self = self else { return }
                    searchTextField.resignFirstResponder()
                    if isFound == true {
                        
                        if _self.view.mode == .myNumber {
                            
                            _self.view.showRequestForOptions()
                        } else {
                            
                            _self.view.showOtherNumberView()
                        }
                    } else {
                        
                        mobileNumberView.searchTextField.text = commonPrefixMobileNumber
                        view.mobileNumberView.operatorLbl.text = ""
                        view.mobileNumberView.clearButton.isHidden = true
                    }
                }) */
            }
            else {
                
                if self.view.mode == .myNumber || self.view.mode == .otherNumber{
                    
                    if newLength > 11 {
                        println_debug("myNumberNewRequestVieshouldChangeCharactersIn false called-----------\(newLength)")
                        return false
                    }
                    
                }
            }
            
        }
        
        if let number = searchTextField.text {
            _searchInContacts(number: number, string: string, searchTextField: searchTextField)
        }
        return true
    }
    
    func myNumberNewRequestView(_ view: RequestMoneyView, otherMobileNumberView numberView: MobileNumberView, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        print("otherMobileNumberViewViewshouldChangeCharactersIn called111-----------")

        guard let searchTextField = numberView.searchTextField else { return true }
        
        let count = searchTextField.text?.count ?? 0
        let newLength = (count + string.count - range.length)
        
        let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: mobileNumberAcceptableCharacters).inverted
        let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
        
        if string != filteredSet {
            return false
        }
        

        
        if (range.location < 2) { return false }
        
        guard validObj.getNumberRangeValidation((searchTextField.text)!).isRejected == false else {
            guard let vc = _context else { return false }
            alertViewObj.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: UIImage(named : "AppIcon"))
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                numberView.searchTextField.text = commonPrefixMobileNumber
             //   numberView.operatorLbl.text = ""
                numberView.clearButton.isHidden = true
            })
            alertViewObj.showAlert(controller: vc)
            return false
        }
        let text = (searchTextField.text! as NSString).replacingCharacters(in: range, with: string)
        let validations = validObj.getNumberRangeValidation(text)
        

        if validations.max == validations.min  {

        
        if newLength == validations.max {
            
            let number = text
            
            let agentCode = UserModel.shared.mobileNo
            var searchString = ""
            if(agentCode.starts(with: "0095")) {
                searchString  = "0" + agentCode.substring(from: 4)
            } else {
                searchString = agentCode
            }
            
            if searchString == number {
                guard let vc = _context else { return false }
                alertViewObj.wrapAlert(title: nil, body: "You cannot request money to your own number".localized, img: UIImage(named : "Contact Mobile Number"))
                alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                    numberView.searchTextField.text = commonPrefixMobileNumber
                  //  numberView.operatorLbl.text = ""
                    numberView.clearButton.isHidden = true
                })
                alertViewObj.showAlert(controller: vc)
                return false
            }
            
            print("otherMobileNumberViewViewshouldChangeCharactersIn called Equel-----------")

            _verifyInBackEnd(number: number, completion: { [weak self] (isFound) in
                searchTextField.resignFirstResponder()
                if isFound == true {
                    self?.view.showRequestForOptions()
                } else {
                    numberView.searchTextField.text = commonPrefixMobileNumber
                  //  view.mobileNumberView.operatorLbl.text = ""
                    numberView.clearButton.isHidden = true
                }
            })
        }
            
        } else {
            
            if (newLength >= validations.min && newLength <= validations.max)   {
                
                let number = text
                
                let agentCode = UserModel.shared.mobileNo
                var searchString = ""
                if(agentCode.starts(with: "0095")) {
                    searchString  = "0" + agentCode.substring(from: 4)
                } else {
                    searchString = agentCode
                }
                
                if searchString == number {
                    guard let vc = _context else { return false }
                    alertViewObj.wrapAlert(title: nil, body: "You cannot request money to your own number".localized, img: UIImage(named : "Contact Mobile Number"))
                    alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                        numberView.searchTextField.text = commonPrefixMobileNumber
                     //   numberView.operatorLbl.text = ""
                        numberView.clearButton.isHidden = true
                    })
                    alertViewObj.showAlert(controller: vc)
                    return false
                }
                print("otherMobileNumberViewViewshouldChangeCharactersIn called lessthan Equel-----------")

                //searchTextField.resignFirstResponder()
                self.view.showRequestForOptions()

                /*_verifyInBackEnd(number: number, completion: { [weak self] (isFound) in
                    searchTextField.resignFirstResponder()
                    if isFound == true {
                        self?.view.showRequestForOptions()
                    } else {
                        numberView.searchTextField.text = commonPrefixMobileNumber
                        view.mobileNumberView.operatorLbl.text = ""
                        view.mobileNumberView.clearButton.isHidden = true
                    }
                }) */
            }
        }
        
        
        if let number = searchTextField.text {
            _searchInContacts(number: number, string: string, searchTextField: searchTextField)
        }
        return true
    }
    
    func myNumberNewRequestView(_ view: RequestMoneyView, didTappedOn attachementsView: AttachementsView) {
        
        
        guard let vc = _context else { return }
        
        let selectedIndex = attachementsView.selectedIndex
        if UserDefaults.standard.bool(forKey: "PhotoButtonTapped") {
        
        if ImagePickerVC.isSourceTypeAvailable(.photoLibrary) {
            
            ImagePickerVC().set(sourceType: .savedPhotosAlbum).set(allowsEditing: false).set(didPick: { (imagePicker, info) in
                
                if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
                    
                    attachementsView[selectedIndex] = image
                    attachementsView.showButtons(tag: selectedIndex)
                    //attachementsView.showNext()
                }
                
                vc.dismiss(animated:true, completion: nil)
                
            }).set(didCancel: { (imagePicker) in
                
                vc.dismiss(animated:true, completion: nil)
            }).show(On: vc)
        }
        } else {
            
            
            if ImagePickerVC.isSourceTypeAvailable(.camera) {
                
                ImagePickerVC().set(sourceType: .camera).set(allowsEditing: false).set(didPick: { (imagePicker, info) in
                    
                    if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
                        
                        attachementsView[selectedIndex] = image
                        attachementsView.showButtons(tag: selectedIndex)
                    }
                    
                    vc.dismiss(animated:true, completion: nil)
                    
                }).set(didCancel: { (imagePicker) in
                    
                    vc.dismiss(animated:true, completion: nil)
                }).show(On: vc)
            }
            
            
        }
    }
    
  
    func myNumberNewRequestView(_ view: RequestMoneyView, scrollEnable amountView: AmountView)
    {
        deligate?.myNumberNewRequestController(self, scrollEnable: amountView)
    }
    func myNumberNewRequestView(_ view: RequestMoneyView, scrollDisable mobileView: MobileNumberView)
    {
        deligate?.myNumberNewRequestController(self, scrollDisable: mobileView)
    }
    func myNumberNewRequestView(_ view: RequestMoneyView, scrollDisable requestSheduleView: RequestSheduleView)
    {
        deligate?.myNumberNewRequestController(self, scrollDisable: requestSheduleView)
    }
    
    
    func myNumberNewRequestView(_ view: RequestMoneyView, requestNowOptionTapped requestSheduleView: RequestSheduleView) {
        
        deligate?.myNumberNewRequestController(self, view: view, requestNowOptionTapped: requestSheduleView)
    }
    
    func myNumberNewRequestView(_ view :RequestMoneyView, sheduleRequestOptionTapped requestSheduleView :RequestSheduleView) {
        
        deligate?.myNumberNewRequestController(self, view: view, sheduleRequestOptionTapped: requestSheduleView)
    }
    
    func myNumberNewRequestView(_ view :RequestMoneyView, didPickNewDate requestSheduleView :RequestSheduleView) {
        
        deligate?.myNumberNewRequestController(self, view: view, didPickNewDate: requestSheduleView)
    }
    
    func myNumberNewRequestView(_ view :RequestMoneyView, didPickNewFrequency requestSheduleView :RequestSheduleView) {
        
        deligate?.myNumberNewRequestController(self, view: view, didPickNewFrequency: requestSheduleView)
    }
}

 extension MyNumberNewRequestController {
    
    
    func showContactView(number: String,textFieldNew: UITextField){
        
        if textFieldNew.tag == 2{
            
            print("showContactView tag 2 called-----")
            _verifyInBackEnd(number: number, completion: { [weak self] (isFound) in
                           textFieldNew.resignFirstResponder()
                           if isFound == true {
                               self?.view.showRequestForOptions()
                           } else {
                               textFieldNew.text = commonPrefixMobileNumber
                             //  view.mobileNumberView.operatorLbl.text = ""
                          //  self?.view.otherMobileNumberView.clearButton.isHidden = true
                            self?.view.secondNumberInvalid()
                            self?.view.otherMobileNumberView?.clearButton.isHidden  = true
                            
                           }
                       })
            
        }
        else
        {
            print("showContactView tag 1 called-----")
        _verifyInBackEnd(number: number, completion: { [weak self] (isFound) in
                       
                       guard let _self = self else { return }
                       textFieldNew.resignFirstResponder()
                       if isFound == true {
                           
                           if _self.view.mode == .myNumber {
                               
                               _self.view.showRequestForOptions()
                           } else {
                               
                               _self.view.showOtherNumberView()
                           }
                       } else {
                           
                           textFieldNew.text = commonPrefixMobileNumber
                        //   view.mobileNumberView.operatorLbl.text = ""
                        self?.view.mobileNumberView.clearButton.isHidden = true
                       }
                   })
            
        }
    }
    
    //00 Formate
    func getConatctNum(_ phoneNum: String, withCountryCode countryCode: String) -> String {
        var FormateStr: String
        if phoneNum.hasPrefix("0") {
            _ = (phoneNum).substring(from: 1)
        }
        let notAllowedChars = CharacterSet(charactersIn: "+()")
        let resultString: String = countryCode.components(separatedBy: notAllowedChars).joined(separator: "")
        if (countryCode == "+95") || (countryCode == "(+95)")  {
            if phoneNum.hasPrefix("+")  {
                FormateStr = "00\((phoneNum).substring(from: 1))"
            }
            else if phoneNum.hasPrefix("0095")
            {
                FormateStr = phoneNum
            }
            else if phoneNum.hasPrefix("0")
            {
                FormateStr = "0095\((phoneNum).substring(from: 1))"
            }
            else {
                FormateStr = "0095\(phoneNum)"
            }
            
        }
        else {
            FormateStr = "00\(resultString)\(phoneNum)"
        }
        return FormateStr
    }
    
    //00 Formate
    func getActualNum(_ phoneNum: String, withCountryCode countryCode: String) -> String {
        var FormateStr: String
        if phoneNum.hasPrefix("0") {
            _ = (phoneNum).substring(from: 1)
        }
        let notAllowedChars = CharacterSet(charactersIn: "+()")
        let resultString: String = countryCode.components(separatedBy: notAllowedChars).joined(separator: "")
        if (countryCode == "+95") || (countryCode == "(+95)") {
            if phoneNum.hasPrefix("0095") {
                FormateStr = "0\((phoneNum).substring(from: 4))"
            }
            else if phoneNum.hasPrefix("0091") {
                FormateStr = "0\((phoneNum).substring(from: 4))"
            }
            else if phoneNum.hasPrefix("95") {
                FormateStr = "0\((phoneNum).substring(from: 2))"
            }
            else {
                FormateStr = "\(phoneNum)"
            }
            
        }
        else {
            FormateStr = "00\(resultString)\(phoneNum)"
        }
        return FormateStr
    }
    
    func _verifyScanInBackEnd(number :String, completion :@escaping (Bool)->()) {
        
        //var startIndex = 0
        
        //if(number.starts(with: "0")) { startIndex = 1 }
        
        let urlString = "\(Url.URLgetOKacDetails)" + "\(getConatctNum(number,withCountryCode: "+95"))"
        
         //let urlString = "\(Url.URLgetOKacDetails)" + "\(number)"
        
        let trimmedString = urlString.components(separatedBy: .whitespaces).joined()
        
        let url = getUrl(urlStr: trimmedString, serverType: .serverApp)
        
        print("_verifyScanInBackEnd====\(url)")
        
        progressViewObj.showProgressView()
        
        JSONParser.GetApiResponse(apiUrl: url, type: "POST") { [weak self] (status : Bool, data : Any?) in
            
            DispatchQueue.main.async {
                
                if status == true {
                    
                    completion(true)
                } else {
                    
                    if let _ = data as? String  {
                        
                        completion(false)
                        self?._show(title: "", errorMeaasge: "This Number is not registered with OK$".localized, iconImageName : "Contact Mobile Number")
                    } else {
                        
                        completion(false)
                    }
                }
            }
            progressViewObj.removeProgressView()
        }
    }
    
    
    func _verifyInBackEnd(number :String, completion :@escaping (Bool)->()) {
        
        //var startIndex = 0
        
        //if(number.starts(with: "0")) { startIndex = 1 }
        
        let urlString = "\(Url.URLgetOKacDetails)" + "\(getConatctNum(number,withCountryCode: "+95"))"
        
        let trimmedString = urlString.components(separatedBy: .whitespaces).joined()
        
        let url = getUrl(urlStr: trimmedString, serverType: .serverApp)
        
        print("_verifyInBackEnd====\(url)")
        
        progressViewObj.showProgressView()
        
        JSONParser.GetApiResponse(apiUrl: url, type: "POST") { [weak self] (status : Bool, data : Any?) in
            
            DispatchQueue.main.async {
                
                if status == true {
                    
                    completion(true)
                } else {
                    
                    if let _ = data as? String  {
                        
                        completion(false)
                        self?._show(title: "", errorMeaasge: "This Number is not registered with OK$".localized, iconImageName : "Contact Mobile Number")
                    } else {
                        
                        completion(false)
                    }
                }
            }
            progressViewObj.removeProgressView()
        }
    }
    
    fileprivate func _searchInContacts(number :String, string :String, searchTextField :SearchTextField) {
        
        var startIndex = 0
        if(number.starts(with: "0")) {  startIndex = 1  }
        
        let searchString = number.substring(from: startIndex) + string
        _searchContacts(for: searchString, { [weak self] (result) in
            
            guard let _self = self else { return }
            
            _self._filteredContacts = result
            DispatchQueue.main.async(execute: {
                searchTextField.filterItems(_self._filteredContacts)
            })
        })
    }
    
    fileprivate func _searchContacts(for searchText: String, _ completion : @escaping ([SearchTextFieldItem])->()) {

        var collection = [SearchTextFieldItem]()
        
        if searchText.isEmpty == true {
            completion(collection)
            return
        }
        
        for contact in cnContacts {
            
            let fullName = CNContactFormatter.string(from: contact, style: .fullName) ?? "No Name"
            println_debug("\(fullName): \(contact.phoneNumbers.description)")
            
            for phoneNumber in contact.phoneNumbers {
                
                if phoneNumber.value.stringValue.contains(searchText) {
                    
                  //  collection.append(SearchTextFieldItem(title:fullName , subtitle: phoneNumber.value.stringValue, image : UIImage(named : "AppIcon")))
                    
                    collection.append(SearchTextFieldItem(title:fullName , subtitle: phoneNumber.value.stringValue, image : UIImage(named : "")))
                }
                println_debug("\(String(describing: phoneNumber.label?.description))  \(phoneNumber.value.stringValue)")
            }
        }
        
        completion(collection)
    }
    
    fileprivate func _isNumberExistsInBackEnd(_ number : String, _ completion : @escaping (Bool, String?)->()) {
        
        var startIndex = 0
        
        if(number.starts(with: "0")) { startIndex = 1 }
        
        let urlString = "\(Url.URLgetOKacDetails)" + "\(UserModel.shared.countryCode.replacingOccurrences(of: "+", with: "00"))" + "\(number.substring(from: startIndex))"
        let url = getUrl(urlStr: urlString, serverType: .serverApp)
        
        JSONParser.GetApiResponse(apiUrl: url, type: "POST") { (status : Bool, data : Any?) in
            
            DispatchQueue.main.async {
                
                if status == true {
                    
                    completion(true, nil)
                } else {
                    
                    if let errorMeaasge = data as? String  {
                        completion(false, errorMeaasge)
                    } else {
                        completion(false, nil)
                    }
                }
            }
        }
    }
    
    fileprivate func _show(title :String, errorMeaasge :String, iconImageName :String = "AppIcon") {
        
        guard let vc = _context else { return }
        
        alertViewObj.wrapAlert(title: title.localized, body: errorMeaasge.localized, img: UIImage(named : iconImageName))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
            
        })
        alertViewObj.showAlert(controller: vc)
    }
}




// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
