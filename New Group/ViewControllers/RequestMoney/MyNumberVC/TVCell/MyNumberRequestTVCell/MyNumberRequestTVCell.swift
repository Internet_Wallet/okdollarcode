//
//  MyNumberRequestTVCell.swift
//  OK
//
//  Created by palnar on 31/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SDWebImage


//cell.lblCustomerName.text  = model.AgentName
//cell.lblMobileNumber.text  = model.Destination
//cell.lblDate.text = model.Date
//cell.lblType.text = model.CommanTransactionType
//cell.lblRequestTYpe.text = model.LocalTransactionType
//cell.lblAmount.text = String.localizedStringWithFormat("%g", (model.Amount)!) + GlobalConstants.Strings.mmk
//cell.lblCategory.text = model.CategoryName

extension Double {
    var clean: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}

protocol NumberRequestTVCellDeligate :class {
    
    func numberRequestTVCell(cell :MyNumberRequestTVCell, dedTapOnCancelButton button :UIButton)
    func numberRequestTVCell(cell :MyNumberRequestTVCell, dedTapOnRemaindButton button :UIButton)
    func numberRequestTVCell(cell :MyNumberRequestTVCell, dedTapOnAttachementLabel label :UIView)
}

class MyNumberRequestTVCell: UITableViewCell {
    
    @IBOutlet weak var remarksStackView: UIStackView!
    @IBOutlet weak var remarksLabel: UILabel!{
        didSet{
            remarksLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var viewGuesture: UIView!
    @IBOutlet weak var userImageView :UIImageView!
    @IBOutlet weak var myNameLabel :UILabel!{
        didSet{
            myNameLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var catogoryLabel :UILabel!{
        didSet{
            catogoryLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var requestTypeLabel :UILabel!{
        didSet{
            requestTypeLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var typeLabel :UILabel!{
        didSet{
            typeLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var myNumberLabel :UILabel!{
        didSet{
            myNumberLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var attachementLabel :UILabel!{
        didSet{
            attachementLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var amountLabel :UILabel!{
        didSet{
            amountLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var dateLabel :UILabel!{
        didSet{
            dateLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var remaindCountLabel :UILabel!{
        didSet{
            remaindCountLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet weak var cancelButton :UIButton!{
        didSet{
            cancelButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var remaindButton :UIButton!{
        didSet{
            remaindButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    @IBOutlet weak var nameTitleLabel: MarqueeLabel!{
        didSet{
            self.nameTitleLabel.font = UIFont(name: appFont, size: 15.0)
            self.nameTitleLabel.text = self.nameTitleLabel.text?.localized
        }
    }
    @IBOutlet weak var categoriesTitleLabel: UILabel!{
        didSet{
            categoriesTitleLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var requestTypeTitleLabel: UILabel!{
        didSet{
            requestTypeTitleLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var numberTitleLabel: MarqueeLabel!{
        didSet{
            self.numberTitleLabel.font = UIFont(name: appFont, size: 15.0)
            self.numberTitleLabel.text = self.numberTitleLabel.text?.localized
        }
    }
    @IBOutlet weak var attachedFileTitleLabel: UILabel!{
        didSet{
            attachedFileTitleLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var amountTitleLabel: UILabel!{
        didSet{
            amountTitleLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var dateTimeTitleLabel: UILabel!{
        didSet{
            dateTimeTitleLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var remindCountTitleLabel: UILabel!{
        didSet{
            remindCountTitleLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var remarksTitleLabel: UILabel!{
        didSet{
            remarksTitleLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var typeTitleLabel: UILabel!{
        didSet{
            typeTitleLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet weak var statusLabel : UILabel!{
        didSet{
            statusLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    weak var deligate :NumberRequestTVCellDeligate?
    
    fileprivate static var _APIDateFormater: DateFormatter = {
        
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = GlobalConstants.Strings.defaultDateFormat
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter
    }()
    
    fileprivate static var _presentationDateFormater: DateFormatter = {
        //
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "EEE, dd-MMM-yyyy hh:mm a"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter
    }()
    override func awakeFromNib() {
        
        super.awakeFromNib()
        _addTapGestureOn(label: viewGuesture)
        self.cancelButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        cancelButton.setTitle("Cancel".localized, for: .normal)
        self.remaindButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        remaindButton.setTitle("Remind".localized, for: .normal)
        //self.nameTitleLabel.text = self.nameTitleLabel.text?.localized
        self.categoriesTitleLabel.text = self.categoriesTitleLabel.text?.localized
        self.requestTypeTitleLabel.text = self.requestTypeTitleLabel.text?.localized
        //self.numberTitleLabel.text = self.numberTitleLabel.text?.localized
        self.attachedFileTitleLabel.text = self.attachedFileTitleLabel.text?.localized
        self.amountTitleLabel.text = self.amountTitleLabel.text?.localized
        self.dateTimeTitleLabel.text = self.dateTimeTitleLabel.text?.localized
        self.remindCountTitleLabel.text = self.remindCountTitleLabel.text?.localized
        
        self.remarksTitleLabel.text = self.remarksTitleLabel.text?.localized
        self.typeTitleLabel.text = self.typeTitleLabel.text?.localized
    }
    
    private func _addTapGestureOn(label :UIView?) {
        
        let gester = UITapGestureRecognizer(target: self, action: #selector(showAttachedFiles(_:)))
        label?.addGestureRecognizer(gester)
    }
    
    @objc fileprivate func showAttachedFiles(_ sender: UITapGestureRecognizer) {
        
        guard let label = viewGuesture else { return }
        deligate?.numberRequestTVCell(cell: self, dedTapOnAttachementLabel: label)
    }
    
    @IBAction func cancelButtonTapped(sender :UIButton) {
        
        deligate?.numberRequestTVCell(cell: self, dedTapOnCancelButton: sender)
    }
    
    @IBAction func remaindButtonTapped(sender :UIButton) {
        
        deligate?.numberRequestTVCell(cell: self, dedTapOnRemaindButton: sender)
    }
    
    func bind(withModel model :RequestMoneyItem?) {
        
        guard let model = model else { return }
        
        let cell = self
        let remaindCount = model.RemindCount ?? 0
        let filesCount = model.AttachmentPath?.count ?? 0
        
        //cell.myNumberLabel.font = UIFont(name: appFont, size: appFontSize)
       // cell.myNameLabel.font = UIFont(name: appFont, size: appFontSize)
        
        cell.myNumberLabel.text  = self.mobileNumberFormat(number: model.Destination!).localized
        cell.myNameLabel.text  = model.AgentName?.localized
        
       cell.userImageView.image = nil
        if let imageStr = model.ProfileImage, let imageUrl = URL(string: imageStr) {
                 cell.userImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "avatar"))
        }
        
        //print("bind model-----\(model.LocalTransactionType)")
        
        if(model.LocalTransactionType == "REQUESTMONEY")
        {
            //cell.requestTypeLabel.font = UIFont(name: appFont, size: appFontSize)
            cell.requestTypeLabel.text = "OK$ Money".localized
        }
        else
        {
            //cell.requestTypeLabel.font = UIFont(name: appFont, size: appFontSize)
            cell.requestTypeLabel.text = "Request Recharge".localized//model.LocalTransactionType
        }
        
         //cell.amountLabel.text = String.localizedStringWithFormat("%g", (model.Amount)!) + ".00" + GlobalConstants.Strings.mmk
        
//        let amount = model.Amount!.clean
//        print("model.Amount----\(model.Amount!.clean)")
//        print("Amount----\(amount)")
        
       // print("model.Amount----\(model.Amount)")

        let str:String = String(format:"%f", model.Amount!)

       // print("model.Amount string----\(str)")
        
        cell.amountLabel.text = self.getDigitDisplaynew(str) + GlobalConstants.Strings.mmk

        cell.dateLabel.text = MyNumberRequestTVCell._dateString(model: model)

        //cell.amountLabel.text = String.localizedStringWithFormat(model.Amount!.clean) + GlobalConstants.Strings.mmk

        
        cell.catogoryLabel.text = model.CategoryName?.localized
        
        if(model.Remarks?.count == 0 || model.Remarks == nil || model.Remarks == "Enter Remarks")
        {
            cell.remarksStackView.isHidden = true
        }
        else
        {
            cell.remarksStackView.isHidden = false
            cell.remarksLabel.text = model.Remarks
        }
        
        //print("_dateString----\(MyNumberRequestTVCell._dateString(model: model))")
        if (model.IsScheduled!) {
            cell.typeLabel.text = "Scheduled"
        } else {
            cell.typeLabel.text = "Normal"//model.ScheduledType?.sheduledOrNormal
        }
        let schduledType = model.ScheduledType?.sheduledOrNormal
        //print("schduledType-----\(schduledType)")
        
        if(schduledType == "Normal")
        {
            cell.dateLabel.text = MyNumberRequestTVCell._dateString(model: model)
            cell.remaindButton.isHidden = false
            if remaindCount > 0 {
                cell.remaindCountLabel.text = "\(remaindCount)"
                cell.remaindCountLabel.isHidden = false
            } else {
                cell.remaindCountLabel.isHidden = true
            }
        } else  if(schduledType == "OK $ Money" || schduledType == "Recharge")
               {
                if model.ScheduledType!.rawValue == -1 {
                                   cell.dateLabel.text = MyNumberRequestTVCell._dateString(model: model)
                               }  else {
                                   cell.dateLabel.text = schduledType
                               }
                   cell.dateLabel.text = MyNumberRequestTVCell._dateString(model: model)
                   cell.remaindButton.isHidden = false
                   if remaindCount > 0 {
                       cell.remaindCountLabel.text = "\(remaindCount)"
                       cell.remaindCountLabel.isHidden = false
                   } else {
                       cell.remaindCountLabel.isHidden = true
                   }
               }
        else
        {
            cell.dateLabel.text = schduledType
            cell.remaindButton.isHidden = true
            cell.remaindCountLabel.isHidden = true
        }
        
        if filesCount > 0 {
            
            cell.attachementLabel.text = "Yes        ".localized
            cell.attachementLabel.isUserInteractionEnabled = true
        } else {
            cell.attachementLabel.text = "No        ".localized
            cell.attachementLabel.isUserInteractionEnabled = false
        }
    }
    
    func getDigitDisplaynew(_ digit: String) -> String {
        
        print("getDigitDisplaynew---\(digit)")
        var FormateStr: String
        let mystring = digit.replacingOccurrences(of: ",", with: "")
        let number = NSDecimalNumber(string: mystring)
        
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        FormateStr = formatter.string(from: number)!
        if FormateStr == "NaN"{
            FormateStr = ""
        }
        return FormateStr
    }
    
    private static func _dateString(model :RequestMoneyItem) ->String?  {
        
       /* print("_dateString----\(model.Scheduledtime)")
        guard let modelDateString = model.Scheduledtime, let date = _APIDateFormater.date(from: modelDateString) else {
            
            return nil
        } */
        
        println_debug(model.Date)
        guard let modelDateString = model.Scheduledtime, let date = _APIDateFormater.date(from: modelDateString) else {
                
             return nil
        } 
        
        return _presentationDateFormater.string(from: date)
    }
    
    func mobileNumberFormat(number : String) -> String
    {
        if(number.starts(with: "0095")) {
            return "0" + number.substring(from: 4)
        }
        return number
        
    }

} 
