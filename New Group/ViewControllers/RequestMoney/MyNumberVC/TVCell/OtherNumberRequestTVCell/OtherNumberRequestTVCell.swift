//
//  OtherNumberRequestTVCell.swift
//  OK
//
//  Created by palnar on 31/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class OtherNumberRequestTVCell: MyNumberRequestTVCell {
 
    @IBOutlet weak var otherNameLabel :MarqueeLabel!{
        didSet{
            self.otherNameLabel.font = UIFont(name: appFont, size: 15.0)
            self.otherNameLabel.text = self.otherNameLabel.text?.localized
            
        }
    }
     @IBOutlet weak var otherNumberLabel :MarqueeLabel!{
        didSet{
            self.otherNumberLabel.font = UIFont(name: appFont, size: 15.0)
            self.otherNumberLabel.text = self.otherNumberLabel.text?.localized
           
        }
    }
    
    
    @IBOutlet weak var otherNameTitleLabel :MarqueeLabel!{
        didSet{
            self.otherNameTitleLabel.font = UIFont(name: appFont, size: 15.0)
            self.otherNameTitleLabel.text = "Receive Money Name".localized
           
        }
    }
    @IBOutlet weak var otherNumberTitleLabel :MarqueeLabel!{
        didSet{
            self.otherNumberTitleLabel.font = UIFont(name: appFont, size: 15.0)
            self.otherNumberTitleLabel.text = "Receive Money Number".localized
        }
           
            
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.nameTitleLabel.text = "Request Money Name".localized
        self.numberTitleLabel.text = "Request Money Number".localized
    }
     
    override func bind(withModel model: RequestMoneyItem?) {
        
        super.bind(withModel: model)
        self.otherNameLabel.font = UIFont(name: appFont, size: 15.0)
        otherNameLabel.text = model?.SourceAgentName
        otherNumberLabel.text = self.mobileNumberFormat(number: (model?.Source)!).localized
    }
}
