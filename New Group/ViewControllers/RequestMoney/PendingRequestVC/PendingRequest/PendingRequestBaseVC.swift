//
//  PendingRequestVC.swift
//  OK
//
//  Created by palnar on 13/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PendingRequestBaseVC: ButtonBarPagerTabStripViewController {
    
    //MARK: - Properties
    
    private var childViewVCs :[PendingRequestVC]!
    private var currentVc :PendingRequestVC!
    
    var mode = RequestMoneyView.Mode.myNumber
    
    var FromScanQR = ""
    lazy var customSearchController = CustomSearchController(searchResultsController: self, searchBarFrame: CGRect(x :0.0, y :0.0, width :containerView.frame.size.width-80,height :50.0), searchBarFont: UIFont.systemFont(ofSize: 16), searchBarTextColor: UIColor.black, searchBarTintColor: UIColor.white)
    
    override func viewDidLoad() {
        
        print("PendingRequestBaseVC  called----")
        
        self.title = "Pending Request Header".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        configureTabBarView()
        super.viewDidLoad()
       // hideKeyboardWhenTappedAround()
        
        customSearchController.customSearchBar.setUpSearchIconAtRightSide()
        setLeftBarButtonItems()
        setRightBarButtonItems()
    }
    
    private func setLeftBarButtonItems() {
        let button1 = UIBarButtonItem(image: UIImage(named:"tabBarBack"), style: .plain, target: self, action:#selector(backButtonAction)) // action:#selector(Class.MethodName) for swift 3
        button1.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItems = [button1]
    }
    
    func setRightBarButtonItems() {
        let searchButton = UIBarButtonItem(image: UIImage(named:"r_search"), style: .plain, target: self, action:#selector(searchButtonAction))
        searchButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItems = [searchButton]
    }
    
    @objc func backButtonAction()
    {
        self.navigationController?.popViewController(animated: true)
        //self.dismiss(animated: true, completion: nil)
    }
    
    @objc func searchButtonAction() {
        
       configureSearchController()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //configureSearchController()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        customSearchController.customSearchBar.showSearchIcon()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Tabbar view customisation
    
    func configureTabBarView(){
        
        print("configureTabBarView---------")
        
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .white
        settings.style.selectedBarBackgroundColor = .black//kYellowColor
        settings.style.buttonBarItemFont = UIFont(name: appFont, size: 14.0)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .black
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool,_ index :Int) -> Void in
            
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .black
            newCell?.label.textColor = kSelectedTextColor//.black
            
            guard let _self = self else { return }
            
            let currentVC = _self.childViewVCs[index]
            let searchBar = _self.customSearchController.customSearchBar
            
            _self.currentVc = currentVC
            searchBar?.delegate = currentVC
            //_self.currentVc.selectedTap = true
            
             guard let searchBar_ = searchBar else { return }
            currentVC.searchBar(searchBar_, textDidChange: "")
            
            
            println_debug("index = \(index)")
        }
    }
    
    override func viewControllersForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        print("viewControllersForPagerTabStrip---------")

        
        let child_1 = UIStoryboard(name: "PendingRequestSB", bundle: nil).instantiateViewController(withIdentifier: "pending_request_result") as! PendingRequestVC
        child_1.delegate = self
        child_1.selectedItemIndex = 1
        let child_2 = UIStoryboard(name: "PendingRequestSB", bundle: nil).instantiateViewController(withIdentifier: "pending_request_result") as! PendingRequestVC
        child_2.delegate = self
        child_2.selectedItemIndex = 2
        let child_3 = UIStoryboard(name: "PendingRequestSB", bundle: nil).instantiateViewController(withIdentifier: "pending_request_result") as! PendingRequestVC
        child_3.delegate = self
        child_3.selectedItemIndex = 3
        let child_4 = UIStoryboard(name: "PendingRequestSB", bundle: nil).instantiateViewController(withIdentifier: "pending_request_result") as! PendingRequestVC
        child_4.delegate = self
        child_4.selectedItemIndex = 4
        
        child_1.searchBar = self.customSearchController.customSearchBar
        child_2.searchBar = self.customSearchController.customSearchBar
        child_3.searchBar = self.customSearchController.customSearchBar
        child_4.searchBar = self.customSearchController.customSearchBar
        
        child_1.mode = mode
        child_2.mode = mode
        child_3.mode = mode
        child_4.mode = mode
        
        childViewVCs = [
            
            child_1,
            child_2,
            child_3,
            child_4
        ]
        
        return [child_1, child_2,child_3,child_4]
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    //MARK: -Search Configation
    
    func configureSearchController() {
        /* searchController = UISearchController(searchResultsController: nil)
         searchController.searchResultsUpdater = self as? UISearchResultsUpdating
         searchController.dimsBackgroundDuringPresentation = true
         searchController.searchBar.placeholder = "Search here..."
         searchController.searchBar.delegate = self as? UISearchBarDelegate
         searchController.searchBar.sizeToFit()*/
        
        
        //searchBar.placeholder = "Pending Request"
        //searchBar.barTintColor = kYellowColor
        //searchBar.backgroundColor = kYellowColor
        // searchBar.barStyle = UISearchBarStyle.prominent
        //UISearchBarStyle.Prominent
        //searchBar.isTranslucent = false
        
        
        //customSearchController.customDelegate = self
        //customSearchController.customSearchBar.customDelegate = self
        
        print("PendingRequestBaseVC configureSearchController called----")

        
        self.navigationController?.navigationItem.title = nil
        self.title = ""
        if let searchBar = customSearchController.customSearchBar {
            
            let rightNavBarButton = UIBarButtonItem(customView: searchBar)
            
            searchBar.placeholder = "Search".localized
            searchBar.text = ""
            searchBar.showsCancelButton = true
            
            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = .blue
          
            self.navigationItem.rightBarButtonItem = rightNavBarButton
            searchBar.becomeFirstResponder()
        }
        
       //// configureBackButton(withTitle: "Pending Request".localized)
    }
}


enum RequestedMoneyStatus : String {
    case REQUESTED =  "Requested"
    case ACCEPTED = "Accepted"
    case REJECTED = "Rejected"
    case CANCELLED = "Cancelled"
    case SCHEDULED = "IsScheduled"
    case ALL = "All"
}

enum RequestType : String {
    case ALL =  "All"
    case SCHEDULED = "Scheduled"
    case NORMAL = "Normal"
}

extension PendingRequestBaseVC : ShowHideSearchButtonProtocol
{
    func showHideSearchButton(status : Bool) {
        if status {
            
            self.title = "Pending Request Header".localized
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]

            self.setRightBarButtonItems()
        } else {
            self.navigationItem.rightBarButtonItems = nil
        }
    }
    
    func HideSearchView() {
        
        //self.navigationController?.navigationItem.title = "Pending Request Header".localized
        
        self.title = "Pending Request Header".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]

        self.customSearchController.customSearchBar.setUpSearchIconAtRightSide()
        self.setLeftBarButtonItems()
        self.setRightBarButtonItems()
        
       
    }

}


