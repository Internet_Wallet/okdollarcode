//
//  PendingRequestVC.swift
//  OK
//
//  Created by palnar on 14/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class TapGestureRecognizer :UITapGestureRecognizer {
    var indexPath :IndexPath?
}


protocol ShowHideSearchButtonProtocol : class {
    func showHideSearchButton(status : Bool)
    func HideSearchView()

}

class PendingRequestVC: OKBaseController{

    //MARK: - Outlets
    
    enum Mode {
        
        case myNumber
        case otherNumber
    }
    
    private var multiButton : ActionButton?
    
  //  var delegate: InBoxightBarShow?

    @IBOutlet weak var requestItemsTableView: UITableView?
    @IBOutlet weak var noRecordsFoundView: UIView?
    @IBOutlet weak var noRecordsLabel: UILabel?
    var mode = RequestMoneyView.Mode.myNumber
    private let refreshControl = UIRefreshControl()

    var  requestModuleData =  [String : Any]()
    var agentType : String = ""

    weak var delegate : ShowHideSearchButtonProtocol?
    weak var searchBar: UISearchBar?
    
//    weak var parentVC :PendingRequestBaseVC?
    var navigationObject : UINavigationController?
    
    var attachedViewTap = false
    var selectedTap = false
    
    //prabu
    var searchClear : Bool = true

    var datadelegate: SendDataDelegate?

    open  var selectedItemIndex : Int = 1
    open  var selectedFilterIndex : Int = -1

    private var titleValue = ""
    
    var FromScanQR = ""
    
    fileprivate var _dataStore :PendingRequestData?
    fileprivate var _SearchStore :PendingRequestData?

    fileprivate var filterItems  = [CriteriaFilterItem]()
    
    fileprivate weak var _dummyTextField :UITextField!
    fileprivate weak var _overlayView :OverlayView!
    
    fileprivate lazy var _APIDateFormater: DateFormatter = {
        
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = GlobalConstants.Strings.defaultAPIDateFormat
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter
    }()
    
    fileprivate lazy var _presentationDateFormater: DateFormatter = {
        //
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "EEE, dd-MMM-yyyy"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter
    }()
    
    //MARK:   -  View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // print("PendingRequestVC viewDidLoad----------------\(self.selectedItemIndex)")
        self.title = "Pending Request Header".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        _setUpTableView()
        //_setUpCollectionView()
        _setUpDatePicker()
        
        if !attachedViewTap {
            attachedViewTap = false
            self.delegate?.showHideSearchButton(status: false)
//            _dataStore = nil
//            _fetchAll()
        } else {
            attachedViewTap = false
        }
      
        NotificationCenter.default.addObserver(self, selector: #selector(inboxslection(notification:)), name:NSNotification.Name("DashboardVCsent"), object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.showSearchText(_:)), name: NSNotification.Name(rawValue: "DashboardSearchTextChange"), object: nil)
        
        //Manage Keyboard
        IQKeyboardManager.sharedManager().enable            = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        let defaultvalue = userDef.value(forKey: "RequestMoney") as? String
        
        if defaultvalue != ""{
            FromScanQR = defaultvalue ?? ""
        }
        else
        {
            FromScanQR = ""
        }
        
       
        //prabu
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            requestItemsTableView!.refreshControl = refreshControl
        } else {
            requestItemsTableView!.addSubview(refreshControl)
        }
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshTableData), for: .valueChanged)
        
        //
       // let gesture = UITapGestureRecognizer(target: self, action: "someAction:")
       // self.noRecordsFoundView!.addGestureRecognizer(gesture)
        
        
        self.loadFloatButtons()
        self.multiButton?.hide()
       
        self.getAllData()
        
        

    }
    
    func someAction(sender:UITapGestureRecognizer){
       // do other task
        print("Some action method called..")
       // self.refreshTableData()
    }
    
    deinit {
       
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("DashboardVCsent"), object: nil)
    }
    
    @objc func refreshTableData() {
        
        print("PendingRequestVC refreshTableData----------------\(selectedItemIndex)")
        self.delegate?.HideSearchView()
        self.searchBar?.text = ""
        NotificationCenter.default.post(name: Notification.Name(rawValue: "ClearAndHideSearchBar"), object: nil)
        self.getAllData()

       /* sortEnable = false
        tableViewReceipt.reloadData()
        guard let isHidden = multiButton?.floatButton.isHidden, !isHidden else {
            self.refreshControl.endRefreshing()
            return
        }
        modelReport.getTransactionInfo(showLoader: false) */
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
       // print("PendingRequestVC viewWillAppear----------------\(self.selectedTap)")
        self.searchClear = false
        
        if self.selectedTap == true {
            self.selectedTap = false
            print("PendingRequestVC viewWillAppear----------------\(self.selectedTap)")
            self.getAllData()
        }else {
            if titleValue == "Pending"{
                selectedItemIndex = 1
            }
           else if titleValue == "Successful"{
                selectedItemIndex = 2
            }
            else if titleValue == "Reject Tab"{
                 selectedItemIndex = 3
            }
            else if titleValue == "Scheduled"{
                selectedItemIndex = 4
            }
            else
            {
               selectedItemIndex = 1
            }
            self.getAllData()
        }
//        if  self._dataStore == nil {
//            self.delegate?.showHideSearchButton(status: true)
//        }
//        else{
//             self.delegate?.showHideSearchButton(status: false)
//        }
      // self.delegate?.showInBoxRightBar(sttaus: true, getIndex: index)
        
        let count = self._dataStore?.result.count ?? 0
             if count >= 1 {

            } else {
           NotificationCenter.default.post(name: NSNotification.Name("inboxRightBarHidesent"), object: nil, userInfo: nil)
        }
        //selectedItemIndex = 0
    }
    
    private func getAllData() {
        
        //print("Get All Data Called------")
        
        //DateFilter
        userDef.set(false , forKey: "defaultDateFilter")

        //Sort
         userDef.set(true , forKey: "defaultSort")
         userDef.set(false , forKey: "amountHLSort")
         userDef.set(false , forKey: "amountLHSort")
         userDef.set(false , forKey: "nameAZSort")
         userDef.set(false , forKey: "nameZASort")
         
         //Filter
         userDef.removeObject(forKey: "defaultcategory0")
         userDef.removeObject(forKey: "defaultType")
         userDef.removeObject(forKey: "default")
         userDef.removeObject(forKey: "defaultcategory2")
         userDef.removeObject(forKey: "defaultcategory1")
         _dataStore = nil
         _fetchAll()
        
    }
    
    
//    @objc func inboxslection(notification: NSNotification) {
//        //   let sideMenu = ["All", "Pending Approval", "Request Approved", "Cancelled"]
//        let title = notification.userInfo?["Title2"] as? Int
//        selectedItemIndex = title ?? 0
//      //  DispatchQueue.main.async {
//          //  self.requestItemsTableView?.reloadData()
//            self._fetchAll()
//      //  }
//
//
//    }
    
     @objc func inboxslection(notification: NSNotification) {
     //   let sideMenu = ["All", "Pending Approval", "Request Approved", "Cancelled"]
    
        let title = notification.userInfo?["Title2"] as? Int
         selectedItemIndex = title ?? 0
         
         switch self.selectedItemIndex {
                case 1 :
                    selectedItemIndex = 1//4
                    break
                case 2 :
                    selectedItemIndex = 2//1
                    break
                case 3 :
                    selectedItemIndex = 3//2
                    break
                case 4 :
                    //cell.blockUnBlock.isHidden = false
                    selectedItemIndex = 4//3
                    break
                    
                default :
                    selectedItemIndex = 1//4
                    break
                }
         
         print("inboxslection value-----\(selectedItemIndex)")
         self._fetchAll()
     
     }
    
    @objc func showSearchText(_ notification: NSNotification) {
        if let searchText = notification.userInfo?["searchText"] as? String{
            print("showSearchText===\(searchText)")

            self._searchByName(searchText: searchText)
            //            self.filtered = self._dataStore?.result.filter({ (pair) -> Bool in
            //                guard let mallName = pair["destinationName"] as? String else { return false }
            //                return mallName.containsIgnoringCase(find: searchText)
            //            }) as! [Dictionary<String, String>]
            //            if let searchStatus = notification.userInfo?["searchStatus"] as? Bool {
            //                searchActive = searchStatus
            //                if searchActive {
            //                    DispatchQueue.main.async {
            //                        self.requestItemsTableView?.reloadData()
            //                    }
            //                }
            //            }
        }
    }
    
    private func _setUpTableView(){
        
        guard let tableView = self.requestItemsTableView else { return }
        
        let headerView = UIView()
        let footerView = UIView()
        let nib = UINib(nibName: "MyNumberRequestTVCell", bundle: Bundle.main)
        let nib1 = UINib(nibName: "OtherNumberRequestTVCell", bundle: Bundle.main)
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = footerView
        tableView.register(nib, forCellReuseIdentifier: "CellOne")
        tableView.register(nib1, forCellReuseIdentifier: "CellTwo")
        //tableView.rowHeight = UITableView.automaticDimension;
        //tableView.estimatedRowHeight = 46.0;
    }
    
    private func _cellNib() ->UINib? {
        
        if mode == .myNumber {
            return UINib(nibName: "MyNumberRequestTVCell", bundle: Bundle.main)
        } else {
            return UINib(nibName: "OtherNumberRequestTVCell", bundle: Bundle.main)
        }
    }
    
    private func _setUpDatePicker() {
        UserDefaults.standard.removeObject(forKey: "StartDate")
        UserDefaults.standard.removeObject(forKey: "EndDate")
        /*
        guard let overlayView = OverlayView.newInstance(), let datePickerView = PendingRequestDatePicker.newInstance() else { return }
        
        let textField = UITextField()
        
        textField.inputView = datePickerView
        
        overlayView.isHidden = true
        textField.isHidden = true
        textField.alpha = 0.0
        
        self.navigationController?.view.addSubview(overlayView)
        self.view.addSubview(textField) 
        
        overlayView.didTap = { [weak self] (view) in
            
            self?._hidePicker()
        }
        
        datePickerView.didPick = { [weak self] (picker, date) in
            
            self?._hidePicker()
            self?._didPickDate(date: date)
        }
        
        datePickerView.didCancel = { [weak self] (picker) in
            
            self?._hidePicker()
            self?._didPickDate(date: nil)
        }
        
        _overlayView = overlayView
        _dummyTextField = textField
 */
    }
    
    private func _showSortView() {
        
        guard let dataStore = _dataStore else { return }
        guard dataStore.result.count > 0 else { return }
        var superView = self.navigationController?.view
        guard let sortView = PendingRequestSortView.newInstance() else { return }
        if superView == nil, let nav = navigationObject {
            superView = nav.view
        }
        
        if userDef .value(forKey: "defaultSort") as! Bool == true {
            sortView.deafultImage.isHidden = false
            sortView.hightolowImage.isHidden = true
            sortView.lowtohightImage.isHidden = true
            sortView.atozImage.isHidden = true
            sortView.ztoaImage.isHidden = true
        } else if userDef .value(forKey: "amountHLSort") as! Bool == true {
            sortView.deafultImage.isHidden = true
            sortView.hightolowImage.isHidden = false
            sortView.lowtohightImage.isHidden = true
            sortView.atozImage.isHidden = true
            sortView.ztoaImage.isHidden = true
        } else if userDef .value(forKey: "amountLHSort") as! Bool == true {
            sortView.deafultImage.isHidden = true
            sortView.hightolowImage.isHidden = true
            sortView.lowtohightImage.isHidden = false
            sortView.atozImage.isHidden = true
            sortView.ztoaImage.isHidden = true
        } else if userDef .value(forKey: "nameAZSort") as! Bool == true {
            sortView.deafultImage.isHidden = true
            sortView.hightolowImage.isHidden = true
            sortView.lowtohightImage.isHidden = true
            sortView.atozImage.isHidden = false
            sortView.ztoaImage.isHidden = true
        } else if userDef .value(forKey: "nameZASort") as! Bool == true {
            sortView.deafultImage.isHidden = true
            sortView.hightolowImage.isHidden = true
            sortView.lowtohightImage.isHidden = true
            sortView.atozImage.isHidden = true
            sortView.ztoaImage.isHidden = false
        }
        
        sortView.didTapOnDefault = { [weak self] (view) in
            userDef.set(true , forKey: "defaultSort")
            userDef.set(false , forKey: "amountHLSort")
            userDef.set(false , forKey: "amountLHSort")
            userDef.set(false , forKey: "nameAZSort")
            userDef.set(false , forKey: "nameZASort")
            
            sortView.deafultImage.isHidden = false
            sortView.hightolowImage.isHidden = true
            sortView.lowtohightImage.isHidden = true
            sortView.atozImage.isHidden = true
            sortView.ztoaImage.isHidden = true
            
            view.hide()
            DispatchQueue.main.asyncAfter(deadline: .now()+0.4, execute: {
                self?._sortByDefault()
                
            })
        }
        
        sortView.didTapOnHighToLow = { [weak self] (view) in
            userDef.set(false , forKey: "defaultSort")
            userDef.set(true , forKey: "amountHLSort")
            userDef.set(false , forKey: "amountLHSort")
            userDef.set(false , forKey: "nameAZSort")
            userDef.set(false , forKey: "nameZASort")
            
            sortView.hightolowImage.isHidden = false
            sortView.deafultImage.isHidden = true
            sortView.lowtohightImage.isHidden = true
            sortView.atozImage.isHidden = true
            sortView.ztoaImage.isHidden = true
            
            view.hide()
            DispatchQueue.main.asyncAfter(deadline: .now()+0.4, execute: {
                self?._sortByAmount(assending: false)
                
            })
        }
        
        sortView.didTapOnLowToHigh = { [weak self] (view) in
            userDef.set(false , forKey: "defaultSort")
            userDef.set(false , forKey: "amountHLSort")
            userDef.set(true , forKey: "amountLHSort")
            userDef.set(false , forKey: "nameAZSort")
            userDef.set(false , forKey: "nameZASort")
            
            sortView.lowtohightImage.isHidden = false
            sortView.deafultImage.isHidden = true
            sortView.hightolowImage.isHidden = true
            sortView.atozImage.isHidden = true
            sortView.ztoaImage.isHidden = true
            
            view.hide()
            DispatchQueue.main.asyncAfter(deadline: .now()+0.4, execute: {
                self?._sortByAmount(assending: true)
                
            })
        }
        
        sortView.didTapOnAtoZ = { [weak self] (view) in
            userDef.set(false , forKey: "defaultSort")
            userDef.set(false , forKey: "amountHLSort")
            userDef.set(false , forKey: "amountLHSort")
            userDef.set(true , forKey: "nameAZSort")
            userDef.set(false , forKey: "nameZASort")
            
            sortView.atozImage.isHidden = false
            sortView.deafultImage.isHidden = true
            sortView.hightolowImage.isHidden = true
            sortView.lowtohightImage.isHidden = true
            sortView.ztoaImage.isHidden = true
            
            view.hide()
            DispatchQueue.main.asyncAfter(deadline: .now()+0.4, execute: {
                self?._sortByName(assending: true)
                
            })
        }
        
        sortView.didTapOnZtoA = { [weak self] (view) in
            userDef.set(false , forKey: "defaultSort")
            userDef.set(false , forKey: "amountHLSort")
            userDef.set(false , forKey: "amountLHSort")
            userDef.set(false , forKey: "nameAZSort")
            userDef.set(true , forKey: "nameZASort")
            
            sortView.ztoaImage.isHidden = false
            sortView.deafultImage.isHidden = true
            sortView.hightolowImage.isHidden = true
            sortView.lowtohightImage.isHidden = true
            sortView.atozImage.isHidden = true
            
            view.hide()
            DispatchQueue.main.asyncAfter(deadline: .now()+0.4, execute: {
                self?._sortByName(assending: false)
                
            })
        }
        if let superview = superView {
            sortView.show(On: superview)
        }
        
    }
    
    private func _canShowFilter() ->Bool {
        
        guard let dataStore = _dataStore else { return false }
        guard dataStore.result.count == 0 else { return true }
        
        let lastAction = dataStore.lastAction
        
        if lastAction == .filter {
            
            if dataStore.originalData.count > 0 {
                return true
            }
            return false
        }
        
        return false
    }
    
    func _showFilterView() {

        if _canShowFilter() == false { return }
        guard let dataStore = _dataStore else { return }
        var superView = self.navigationController?.view
        guard let filterView = PendingRequestFilterView.newInstance() else { return }
        
        if superView == nil, let nav = navigationObject {
            superView = nav.view
        }
        
        filterView.didTapOnDefault = { [weak self] (view) in
            
            self?._filterByDefault()
            view.hide()
        }
        
        filterView.didTapOnType = { [weak self] (view, type) in
            
            self?._filterBy(type: type)
            view.hide()
        }
        
        filterView.didTapOnCategorie = { [weak self] (view, categorie) in
            
            self?._filterBy(categorie: categorie)
            view.hide()
        }
        if let superview = superView {
            filterView.set(types: dataStore.allAvailableTypes).set(categories: dataStore.allCategoryTypes ).show(On: superview)
        }
    }
    
//    private func _didPickDate(date: Date?) {
//        
//        let isAnyChange = _dataStore?.set(fromDate: date, toDate : Date())  ?? false
//        
//        if isAnyChange == true {
//            _reloadTableViewWithSuffleAnimation()
//        } 
//    }
    
    func _showDatePicker() {
        UIView.animate(withDuration: 0.1) {
            //overlayView?.alpha = 0.3
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "RequestMoneyDateSelectViewController_ID") as? RequestMoneyDateSelectViewController {
//                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//                vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                vc.delegate = self
                if let nav = self.navigationController {
                    nav.pushViewController(vc, animated: true)
                } else {
                    if let nav = self.navigationObject {
                        nav.pushViewController(vc, animated: true)
                    }
                }
            }
            
        }
        /*
        let overlayView = _overlayView
        let textField = _dummyTextField
        
        overlayView?.alpha = 0.0
        overlayView?.isHidden = false
        textField?.becomeFirstResponder()
        
        UIView.animate(withDuration: 0.3) {
            overlayView?.alpha = 0.3
        }
 */
    }
    
    func getAllDateSortedList(startDate : Date, endDate : Date)
    {
        let isAnyChange = _dataStore?.set(fromDate :startDate, toDate : endDate)
//        print("getAllDateSortedListisAnyChange-----\(isAnyChange)")
      //  print("getAllDateSortedList_dataStore-----\(_dataStore?.result.count)")
        
        if  isAnyChange == true && (self._dataStore?.result.count)! > 0
        {
            
            self.requestItemsTableView?.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
            DispatchQueue.main.async {
                let indexPath = IndexPath(row: 0, section: 0)
             self.requestItemsTableView?.scrollToRow(at: indexPath, at: .top, animated: true)
            }
            
        } else {
        
            self.requestItemsTableView?.reloadData()
        }
        
//        if isAnyChange == true {
//            _reloadTableViewWithSuffleAnimation()
//        }
        
       
    }
    
    func _hidePicker() {
        
        let overlayView = _overlayView
        let textField = _dummyTextField
        
        textField?.resignFirstResponder()
        
        UIView.animate(withDuration: 0.3, animations: {
            overlayView?.alpha = 0.0
        }) { (flag) in
            overlayView?.isHidden = true
        } 
    }
    
    private func _reloadTableViewWithSuffleAnimation() {
        
        guard let tableView = requestItemsTableView else { return }
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [.curveEaseIn], animations: {
            
            tableView.alpha = 0.0
        }) { (flag) in
            
            if let _ = tableView.indexPathsForVisibleRows?.first {
                
                let ip = IndexPath(row: 0, section: 0)
                tableView.selectRow(at: ip, animated: false, scrollPosition: UITableView.ScrollPosition.top)
            }
            let transition = CATransition()
            
            transition.type = CATransitionType.moveIn
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            transition.fillMode = CAMediaTimingFillMode.forwards
            transition.duration = 0.5
            transition.subtype = CATransitionSubtype.fromBottom
            
            tableView.layer.add(transition, forKey: "UITableViewReloadDataAnimationKey")
            tableView.reloadData()
            
            UIView.animate(withDuration: 0.6, delay: 0.0, options: [.curveEaseIn], animations: {
                
                tableView.alpha = 1.0
            }) { (flag) in
                
                
            }
        }
    }
    
    private func _sortByDefault() {
        
        let isAnyChange = _dataStore?.set(sort: .default)  ?? false
        
        if  isAnyChange == true && (self._dataStore?.result.count)! > 0
        {
            
            self.requestItemsTableView?.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
            DispatchQueue.main.async {
                let indexPath = IndexPath(row: 0, section: 0)
             self.requestItemsTableView?.scrollToRow(at: indexPath, at: .top, animated: true)
            }
            
        } else {
        
            self.requestItemsTableView?.reloadData()
        }
        
//        if isAnyChange == true {
//            _reloadTableViewWithSuffleAnimation()
//        }
    }
    
    private func _sortByAmount(assending :Bool) {
        
        var isAnyChange = true
        
        if assending == true {
            isAnyChange = _dataStore?.set(sort: .amountLowToHigh) ?? false
        } else {
            isAnyChange = _dataStore?.set(sort: .amountHightoLow) ?? false
        }
        
        if  isAnyChange == true && (self._dataStore?.result.count)! > 0
        {
            
            self.requestItemsTableView?.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
            DispatchQueue.main.async {
                let indexPath = IndexPath(row: 0, section: 0)
             self.requestItemsTableView?.scrollToRow(at: indexPath, at: .top, animated: true)
            }
            
        } else {
        
            self.requestItemsTableView?.reloadData()
        }
        
//        if isAnyChange == true {
//            _reloadTableViewWithSuffleAnimation()
//        }
    }
    
    private func _sortByName(assending :Bool) {
        
        var isAnyChange = true
        
        if assending == true {
            isAnyChange = _dataStore?.set(sort: .nameAToZ) ?? false
        } else {
            isAnyChange = _dataStore?.set(sort: .nameZToA) ?? false
        }
        
        if  isAnyChange == true && (self._dataStore?.result.count)! > 0
        {
            
            print("sort value pending  is======\(String(describing: self._dataStore?.result.count))")

            
            self.requestItemsTableView?.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
            DispatchQueue.main.async {
                let indexPath = IndexPath(row: 0, section: 0)
             self.requestItemsTableView?.scrollToRow(at: indexPath, at: .top, animated: true)
            }
            
        } else {
        
            self.requestItemsTableView?.reloadData()
        }
        
//        if isAnyChange == true {
//            _reloadTableViewWithSuffleAnimation()
//        }
    }
    
    private func _filterByDefault() {
        
        let isAnyChange = _dataStore?.set(filter: .default) ?? false
        
        
        if  isAnyChange == true && (self._dataStore?.result.count)! > 0
        {
            
            self.requestItemsTableView?.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
            DispatchQueue.main.async {
                let indexPath = IndexPath(row: 0, section: 0)
             self.requestItemsTableView?.scrollToRow(at: indexPath, at: .top, animated: true)
            }
            
        } else {
        
            self.requestItemsTableView?.reloadData()
        }
        
//        if isAnyChange == true {
//            _reloadTableViewWithSuffleAnimation()
//        }
    }
    
    private func _filterBy(type :String) {
        
        print("_filterBy-----\(type)")
        

            let isAnyChange = _dataStore?.set(filter: .type(.string(type))) ?? false

            print("_filterByisAnyChange-----\(isAnyChange)")
            //print("_filterByis_dataStore-----\(_dataStore)")
        
        
        if  isAnyChange == true && (self._dataStore?.result.count)! > 0
        {
            
            self.requestItemsTableView?.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
            DispatchQueue.main.async {
                let indexPath = IndexPath(row: 0, section: 0)
             self.requestItemsTableView?.scrollToRow(at: indexPath, at: .top, animated: true)
            }
            
        } else {
        
            self.requestItemsTableView?.reloadData()
        }
            
//            if isAnyChange == true {
//                _reloadTableViewWithSuffleAnimation()
//            }
            
        
    }
    
    private func _filterBy(categorie :String) {
        
      //  print("_filterBycategorie-----\(categorie)")

        let isAnyChange = _dataStore?.set(filter: .categories(.string(categorie))) ?? false
       // print("_filterBycategorie-----\(isAnyChange)")
       // print("_filterBycategorie-----\(_dataStore)")
        
        if  isAnyChange == true && (self._dataStore?.result.count)! > 0
        {
            
            self.requestItemsTableView?.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
            DispatchQueue.main.async {
                let indexPath = IndexPath(row: 0, section: 0)
             self.requestItemsTableView?.scrollToRow(at: indexPath, at: .top, animated: true)
            }
            
        } else {
        
            self.requestItemsTableView?.reloadData()
        }

//        if isAnyChange == true {
//            _reloadTableViewWithSuffleAnimation()
//        }
    }
    
    //MARK:- Floating Buttons
    func loadFloatButtons() {
        var buttonItems = [ActionButtonItem]()
        let dateFilter = ActionButtonItem(title: "Date".localized, image: #imageLiteral(resourceName: "receiptSortDate"))
        dateFilter.action = { item in
            self.multiButton?.toggleMenu()
            self._showDatePicker()
            
        }
        
        let sortFilter = ActionButtonItem(title: "Sort".localized, image: #imageLiteral(resourceName: "offer_report_menu_icon_sort"))
        sortFilter.action = { item in
            self.multiButton?.toggleMenu()
            self._showSortView();
            
        }
        
        let filter = ActionButtonItem(title: "Filter".localized, image: #imageLiteral(resourceName: "offer_report_menu_icon_filter"))
        filter.action = { item in
            self.multiButton?.toggleMenu()
            self._showFilterView()
        }
        buttonItems = [dateFilter,sortFilter,filter]
        
        multiButton = ActionButton(attachedToView: self.view, items: buttonItems)
        multiButton?.action = {
            button in button.toggleMenu()
        }
        multiButton?.setImage(#imageLiteral(resourceName: "menu_white_payto"), forState: .normal)
        multiButton?.backgroundColor = kYellowColor
    }
    
    func getCategoryList(number: String) {
              
               if appDelegate.checkNetworkAvail() {
                   let web      = WebApiClass()
                   web.delegate = self
                   showProgressView()
                   
                   let buldApi = Url.billSplitterMobileValidation + "MobileNumber=\(number)"
                   let url = getUrl(urlStr: buldApi, serverType: .serverApp)
                   println_debug(url)
                   let params = Dictionary<String,String>()
                   
                   web.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", mScreen: "mMobileValidation")
                 }
          }
    
    
}


//MARK: - Web Response Delegate


extension PendingRequestVC :WebServiceResponseDelegate {
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
     if screen == "mMobileValidation"
        {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(dic)
                        print("BankCounterDepositDetailsVC Dic----\(dic)")

                        
                        if dic["Code"]! as! Int == 200
                        {
                            
                            DispatchQueue.main.async {
                               
                                let dataDict = dic["Data"] as? String
                                let tempData = OKBaseController.convertToDictionary(text: dataDict!)
                                self.agentType.append(tempData!["AccountType"]! as! String)
                             //   self.count = self.count + 1
                                
                                
//                                if self.count == self.mobileNumberArray.count{
//                                    self.tableV.delegate = self
//                                    self.tableV.dataSource = self
//
//
//                                }
                              //  DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
                                   //
                               // })
     
                            }
                        }
                        else {
                            DispatchQueue.main.async {
                                //#imageLiteral(resourceName: "alert-icon")
                                alertViewObj.wrapAlert(title: nil, body:"This Number is not registered with OK$".localized, img: UIImage(named: "phone_alert"))
                                
                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                    
                                }
                                alertViewObj.showAlert(controller: self)
                                
                            }
                        }
                    }
                    
                }
            } catch {
                
            }
        }
    }
}


//MARK: - TabStrip Provider
extension PendingRequestVC : IndicatorInfoProvider {
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        switch self.selectedItemIndex {
        case 1 :
            titleValue = "Pending"
            break
        case 2 :
            titleValue = "Successful"
            break
        case 3 :
            titleValue = "Reject Tab"
            break
        case 4 :
            titleValue =  "Scheduled"
            break
        default :
            titleValue = "Pending"
            break
        }
        print("indicatorInfoForPagerTabStrip===\(self.selectedItemIndex)")
        return IndicatorInfo(title : titleValue.localized)
    }
}

//MARK: - Table view extension
extension PendingRequestVC : UITableViewDelegate,UITableViewDataSource {
    
    private func _showNoRecordsView() {
      //  noRecordsFoundView?.isHidden = false
        self.noRecordsLabel?.isHidden = false
    }
    
    private func _hideNoRecordsView() {
        //noRecordsFoundView?.isHidden = true
        self.noRecordsLabel?.isHidden = true

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = _dataStore?.result.count ?? 0
        
        if count == 0 {
            
            _showNoRecordsView()
        } else {
            _hideNoRecordsView()
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let index = indexPath.row
        println_debug("index = \(index)")
        guard let collection = _dataStore?.result, collection.count > index else { return UITableViewCell() }
        let dataModel = collection[index]
        if dataModel.IsMynumber == true {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellOne", for: indexPath) as! MyNumberRequestTVCell
            
            cell.bind(withModel: dataModel)
            // _addUserImage(On: cell.userImageView, fromURLString: dataModel.ProfileImage, forIndexPath: indexPath)
            let filesCount = dataModel.AttachmentPath?.count ?? 0
            if filesCount > 0 {
                _addUserImage(On: cell.userImageView, fromURLString: dataModel.AttachmentPath![0], forIndexPath: indexPath)
            }
            else {
                cell.userImageView.image = nil
                if let imageStr = dataModel.ProfileImage, let imageUrl = URL(string: imageStr) {
                   cell.userImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "avatar"))
                }
                cell.userImageView.contentMode = UIView.ContentMode.scaleAspectFit
                cell.userImageView.setRounded()
               // cell.userImageView.image = UIImage(named : "avatar")
            }
            _updateVisibility(OfChildComponents: cell)
            cell.deligate = self
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellTwo", for: indexPath) as! OtherNumberRequestTVCell
            
            cell.bind(withModel: dataModel)
            // _addUserImage(On: cell.userImageView, fromURLString: dataModel.ProfileImage, forIndexPath: indexPath)
            let filesCount = dataModel.AttachmentPath?.count ?? 0
            if filesCount > 0 {
                _addUserImage(On: cell.userImageView, fromURLString: dataModel.AttachmentPath![0], forIndexPath: indexPath)
            }
            else {
                 cell.userImageView.image = nil
                 if let imageStr = dataModel.ProfileImage, let imageUrl = URL(string: imageStr) {
                    cell.userImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "avatar"))
                 }
                 cell.userImageView.contentMode = UIView.ContentMode.scaleAspectFit
                 cell.userImageView.setRounded()
                 //cell.userImageView.image = UIImage(named : "avatar")
            }
            _updateVisibility(OfChildComponents: cell)

            cell.deligate = self
            return cell
        }
    }
    
//    private func _bind(view : CustomPendingRequestCellTableViewCell, withModel model :RequestMoneyItem) {
//
//        ///
//        let APIDateFormater = _APIDateFormater
//        let presentationDateFormater = _presentationDateFormater
//
//        view.lblCustomerName.text = model.AgentName
//        view.lblCategory.text = model.CategoryName
//        view.lblRequestTYpe.text = model.CommanTransactionType
//        view.lblType.text = model.LocalTransactionType
//        view.lblMobileNumber.text = model.Destination
//        if let amount = model.Amount {
//            view.lblAmount.text = String.localizedStringWithFormat("%g", amount) + GlobalConstants.Strings.mmk
//        }
//
//        /// convert "2018-01-09T19:18:54.87" to Tue, 09-Jan-2018
//        guard let modelDateString = model.Date,
//            let date = APIDateFormater.date(from: modelDateString)  else {
//
//                view.lblDate.text = nil
//                return
//        }
//
//        view.lblDate.text = presentationDateFormater.string(from: date)
//    }
    
    private func _addUserImage(On imageView :UIImageView?, fromURLString urlString :String?, forIndexPath indexPath :IndexPath) {
        
        if let imageView = imageView, let imageStringUrl = urlString, let url = URL(string: imageStringUrl) {
            imageView.contentMode = UIView.ContentMode.scaleAspectFit
            imageView.setRounded()
            LazyImageLoader.showImage(On: imageView, fromURL: url, showActivityIndicator: true, indicatorStyle: .gray)
        } else {
            if let imageView = imageView, let imageStringUrl = urlString?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed), let url = URL(string: imageStringUrl) {
                imageView.contentMode = UIView.ContentMode.scaleAspectFit
                imageView.setRounded()
                LazyImageLoader.showImage(On: imageView, fromURL: url, showActivityIndicator: true, indicatorStyle: .gray)
            }
        }
        ///
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    /// func to add attachement status on label cell.lblAttachedFile. when user tap on cell.lblAttachedFile the selector showAttachedFiles(_:) will be triger
    ///
    /// - Parameters:
    ///   - cell: to attach the status on cell.lblAttachedFile label and add to add TapGestureRecognizer
    ///   - attachements: contains array of image url string
    ///   - indexPath: indexpath of the cell
//    private func _addAttachementDetails(On cell :CustomPendingRequestCellTableViewCell, attachements :[String]?, forIndexPath indexPath :IndexPath) {
//
//        /// check weather label found
//        guard let label = cell.lblAttachedFile else { return }
//
//        let filesCount = attachements?.count ?? 0 // attachements count
//        let action = #selector(showAttachedFiles(_:)) // action when user tap on label
//
//        /// configure gester
//        if let gester = label.gestureRecognizers?.first as? TapGestureRecognizer { /// if gester already added then remove the existing target, action and add new one.
//
//            gester.indexPath = indexPath
//        } else { /// if gester not yet added then add a new gester.
//
//            let gester = TapGestureRecognizer(target: self, action: action)
//            gester.indexPath = indexPath
//            label.addGestureRecognizer(gester)
//        }
//
//        /// update the UI attachenge status on label
//        if filesCount > 0 {
//
//            label.text = "Yes        "
//            label.isUserInteractionEnabled = true
//        } else {
//            label.text = "No        "
//            label.isUserInteractionEnabled = false
//        }
//    }
    
    private func _getPaginationImages(atInexPath indexPath :IndexPath) ->[ImagePaginationVC.Image]? {
        
        guard let attachements = _dataStore?.result[indexPath.row].AttachmentPath, attachements.count > 0 else {
            return nil
        }
        
        var collection = [ImagePaginationVC.Image]()
        
        for attachement in attachements {
            
            let item = ImagePaginationVC.Image(source: attachement)
            collection.append(item)
        }
        
        return collection
    }
    
    
    
    private func _updateVisibility(OfChildComponents view :MyNumberRequestTVCell) {
        print("_updateVisibility. function===\(self.selectedItemIndex)")

        func doForPendingTab() {
           view.cancelButton.isHidden = false
            view.remaindButton.isHidden = false
            view.statusLabel.text = ""
        }
        
        func doForSuccessTab() {

            view.cancelButton.isHidden = true
            view.remaindButton.isHidden = true
            view.remindCountTitleLabel.text = ""
            view.remindCountTitleLabel.text = ""
            view.statusLabel.text = "Success".localized
            view.statusLabel.textColor = UIColor.init(red: 63/255, green: 167/255, blue: 12/255, alpha: 1)
        }
        
        func doForRejectTab() {

            view.cancelButton.isHidden = true
            view.remaindButton.isHidden = true
            view.remindCountTitleLabel.text = ""
            view.remindCountTitleLabel.text = ""
            view.statusLabel.text = "Reject".localized
            view.statusLabel.textColor = UIColor.init(red: 232/255, green: 76/255, blue: 7/255, alpha: 1)
        }
        
        func doForScheduledTab() {

            view.cancelButton.isHidden = false
            view.remaindButton.isHidden = false
            view.statusLabel.text = ""
        }
        switch self.selectedItemIndex {
        case 1 :
            doForPendingTab()
            break
        case 2 :
            doForSuccessTab()
            break
        case 3 :
            doForRejectTab()
            break
        case 4 :
            //cell.blockUnBlock.isHidden = false
            doForScheduledTab()
            break
            
        default :
            break
        }
    }
}
//MARK: - Get all send Requests
extension PendingRequestVC {
    
    func _fetchAll() {
          self.searchBar?.text = ""
         NotificationCenter.default.post(name: Notification.Name(rawValue: "ClearAndHideSearchBar"), object: nil)
         print("PendingRequestVC_fetchAll-----")
        self.noRecordsLabel?.text = ""
        DispatchQueue.main.async {
              progressViewObj.showProgressView()
              self.requestItemsTableView?.reloadData()
        }
        
        var requestMoneyStatus  = RequestedMoneyStatus.ALL.rawValue
        var requestType = RequestType.ALL.rawValue
        
        switch selectedItemIndex {
        case 1:
            requestMoneyStatus = RequestedMoneyStatus.REQUESTED.rawValue
        case 2:
            requestMoneyStatus = RequestedMoneyStatus.ACCEPTED.rawValue
        case 3:
            requestMoneyStatus = RequestedMoneyStatus.REJECTED.rawValue
        case 4:
            //requestMoneyStatus = RequestedMoneyStatus.SCHEDULED.rawValue
           // requestMoneyStatus = RequestedMoneyStatus.REQUESTED.rawValue

            requestType = RequestType.SCHEDULED.rawValue
        default:
            requestMoneyStatus = RequestedMoneyStatus.REQUESTED.rawValue
        }
        
        guard let apiForSendAllRequests : URL = RequestMoneyParams.getAPIForAllSendRequest(requestMoneyStatus: requestMoneyStatus, requestType: requestType, limit: nil, offset: nil, isRequestModule: true) else {
            return
        }
        if FromScanQR == "FromRequestMoney" {
            //
        }
        else
        {
            if selectedItemIndex == 1{
                selectedItemIndex = 1
            }
            else if selectedItemIndex == 2{
                selectedItemIndex = 2
            }
            else if selectedItemIndex == 3{
                selectedItemIndex = 3
            }
            else if selectedItemIndex == 4{
                selectedItemIndex = 4
            }
        }
       
        println_debug("api------ \(apiForSendAllRequests.absoluteString) ")
        
        JSONParser.GetApiResponse(apiUrl: apiForSendAllRequests, type: "POST") { (status : Bool, data : Any?) in
            
            progressViewObj.removeProgressView()
            
//            guard status == true, let dataArray = data as? Array<Any> else {
//                DispatchQueue.main.async {
//
//                    if  let collection = self._dataStore?.result, collection.count <= 0
//                    {
//                        self.requestItemsTableView?.reloadData()
//
//                    } else {
//
//                        self.requestItemsTableView?.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
//                           DispatchQueue.main.async {
//                               let indexPath = IndexPath(row: 0, section: 0)
//                            self.requestItemsTableView?.scrollToRow(at: indexPath, at: .top, animated: true)
//                           }
//                    }
//                   // self.refreshControl.endRefreshing()
//
//                }
//                return }
            
            guard status == true, let dataArray = data as? Array<Any> else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    self.requestItemsTableView?.reloadData()
                     self.refreshControl.endRefreshing()
                }
                return
            }

            
            var collection = [RequestMoneyItem]()
            
            for case let dictionary as Dictionary<String,Any> in dataArray {
                
                //println_debug("dictionary = \(dictionary)") //LocalTransactionType
                print("All RequestData-----\(dictionary)")
                collection.append(RequestMoneyItem.wrapModel(dic: dictionary))
               // print("All RequestData count-----\(collection.count)")

            }
            
            if self._dataStore == nil {
                self._dataStore = PendingRequestData(datas: collection)
                self._SearchStore = PendingRequestData(datas: collection)
               
            } else {
                self._dataStore?.set(datas: collection)
                self._SearchStore?.set(datas: collection)
               
            }
             let count = self._dataStore?.result.count ?? 0
            
            
            DispatchQueue.main.async(execute: {
                
                if count >= 1 {
                    self.multiButton?.show()
                    
                    self.delegate?.showHideSearchButton(status: true)
                    
                    NotificationCenter.default.post(name: NSNotification.Name("inboxRightBarHide"), object: nil, userInfo: ["HideSearch": false])
                } else {
                    self.multiButton?.hide()
                    self.delegate?.showHideSearchButton(status: false)
                    
                    NotificationCenter.default.post(name: NSNotification.Name("inboxRightBarHide"), object: nil, userInfo: ["HideSearch": true])
                }
                
                self.refreshControl.endRefreshing()
                self.noRecordsLabel?.text = "No records found!".localized
                self.requestItemsTableView?.dataSource  = self
                self.requestItemsTableView?.delegate = self
                self._searchByName(searchText: self.searchBar?.text)
            })
        }
    }
}


//pending Request TableView Cell Delegate

extension PendingRequestVC : NumberRequestTVCellDeligate {

    func numberRequestTVCell(cell :MyNumberRequestTVCell, dedTapOnCancelButton button :UIButton) {
        
        guard let indexPath = requestItemsTableView?.indexPath(for: cell) else { return }
        // println_debug("Block ", sender, tappedIndexPath)
        
        /*
        let payment = PaymentAuthorisation()
        
        payment.delegateModule = self
        var  requestModuleData =  [String : Any]()
        requestModuleData = [
            "module_name":kChangeStatusOfRequest,
            "row_number":indexPath.row,
            "status" : RequestedMoneyStatus.CANCELLED.rawValue
        ]
        
        payment.requestModuleData = requestModuleData
        payment.authenticate()
         */
        
        requestModuleData = [
            "module_name":kChangeStatusOfRequest,
            "row_number":indexPath.row,
            "status" : RequestedMoneyStatus.CANCELLED.rawValue
        ]
        
        alertViewObj.wrapAlert(title: "", body: "Do you want to Cancel Request Money?".localized, img: UIImage(named : "request_money_no_records"))
        alertViewObj.addAction(title: "Cancel".localized, style: AlertStyle.cancel, action: {
            
        })
        
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {
            self.requestModuleData = [
                "module_name":kRemindRequest,
                "row_number":indexPath.row,
                "status" : RequestedMoneyStatus.CANCELLED.rawValue
                ] as [String : Any]
            
            OKPayment.main.authenticate(screenName: "RequestMoneyPendingRequest", delegate: self)
        })
        alertViewObj.showAlert(controller: self)
        
    }
    
    func numberRequestTVCell(cell :MyNumberRequestTVCell, dedTapOnRemaindButton button :UIButton) {
        
        //PAyment Button Action
        
        guard let indexPath = requestItemsTableView?.indexPath(for: cell) else { return }
        
        alertViewObj.wrapAlert(title: "", body: "Do you want to Remind Request Money Again?".localized, img: UIImage(named : "request_money_no_records"))
        alertViewObj.addAction(title: "Cancel".localized, style: AlertStyle.cancel, action: {
            
        })
        
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {
            self.requestModuleData = [
                "module_name":kRemindRequest,
                "row_number":indexPath.row
                ] as [String : Any]
            
            OKPayment.main.authenticate(screenName: "RequestMoneyRemindPendingRequest", delegate: self)
        })
        alertViewObj.showAlert(controller: self)
        
        /*
        _show(title: nil, errorMeaasge: "Do you want to Remind Request Money Again", iconImageName: "confirmation_icon") { [weak self] in
            
            guard let _self = self else { return }
            
            /*
            let payment = PaymentAuthorisation()
            payment.authenticate()
            payment.delegateModule = _self
            let requestModuleData = [
                "module_name":kRemindRequest,
                "row_number":indexPath.row
                ] as [String : Any]
            
            payment.requestModuleData = requestModuleData
            */
            
            self?.requestModuleData = [
                "module_name":kRemindRequest,
                "row_number":indexPath.row
                ] as [String : Any]
            
            OKPayment.main.authenticate(screenName: "RequestMoneyRemindPendingRequest", delegate: self!)

        }
 */
        
    }
    
    func numberRequestTVCell(cell :MyNumberRequestTVCell, dedTapOnAttachementLabel label :UIView) {
        
        guard let indexPath = requestItemsTableView?.indexPath(for: cell) else { return }
//
//        let images = _getPaginationImages(atInexPath: indexPath)
//        ImagePaginationVC.newInstance()?.set(source: images).set(backgroundColor: UIColor.black).set(placeholder: UIImage(named: "attachement")).show(On: self)
        
        guard let attachements = _dataStore?.result[indexPath.row].AttachmentPath, attachements.count > 0 else {
            showToast(message: "No Files Attached")
            return
        }
        self.attachedViewTap = true
        let vc = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PTGalleryViewController") as? PTGalleryViewController
        vc?.arrayImages = attachements
        vc?.reqMoney = "Request Money"
        if let nav = self.navigationController {
            nav.navigationBar.barTintColor        = kYellowColor
            nav.navigationBar.tintColor           = UIColor.white
//            vc?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//            nav.present(vc!, animated: true, completion: nil)
            nav.pushViewController(vc!, animated: true)
        } else {
            if let nav = self.navigationObject {
                nav.navigationBar.barTintColor        = kYellowColor
                nav.navigationBar.tintColor           = UIColor.white
                nav.pushViewController(vc!, animated: true)
            }
        }
        
        
    }
    
}


extension PendingRequestVC : PaymentValidatonDelegateModule {
    
    func paymentAuthorisationSuccess(withModule moduleData : [String:Any]) {
        
        if let moduleName = moduleData["module_name"] as? String {
            
            switch moduleName {
                
            case kRemindRequest:
                if let rowNumber = moduleData["row_number"] as? Int {
                    DispatchQueue.main.async(execute: {
                        self.remindRequestAgin(indexRow: rowNumber)
                    })
                }
                
            case kChangeStatusOfRequest:
                if let rowNumber = moduleData["row_number"] as? Int {
                    if let statusOFRequest = moduleData["status"] as? String {
                        DispatchQueue.main.async(execute: {
                            self.updateRequestWithStatus(indexRow: rowNumber, status: statusOFRequest)
                            
                        })
                    }
                }
                
            default:
                println_debug("default")
            }
        }
    }
}

extension PendingRequestVC: BioMetricLoginDelegate{
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String)
    {
        println_debug("Authorised")
        if(isSuccessful)
        {
            if(screen == "RequestMoneyPendingRequest")
            {
                if let rowNumber = requestModuleData["row_number"] as? Int {
                    if let statusOFRequest = requestModuleData["status"] as? String {
                        DispatchQueue.main.async(execute: {
                            self.updateRequestWithStatus(indexRow: rowNumber, status: statusOFRequest)
                            
                        })
                    }
                }
            }
            else if(screen == "RequestMoneyRemindPendingRequest")
            {
                if let rowNumber = requestModuleData["row_number"] as? Int {
                    DispatchQueue.main.async(execute: {
                        self.remindRequestAgin(indexRow: rowNumber)
                    })
                }
            }
        }
        else
        {
            println_debug("User not authorized")
            
        }
    }
}

extension PendingRequestVC {
    
    //MARK: - getPendingTopResult
    func remindRequestAgin(indexRow index :Int) {
        
        guard let item = _dataStore?.result[index],
            let url = RequestMoneyParams.getAPIForRemindRequest(pendingRequestModel: item) else { return }
        
        progressViewObj.showProgressView()
        JSONParser.GetCancelApiResponse(apiUrl: url, type : "POST") { [weak self] (status : Bool, data : Any?) in
            
            progressViewObj.removeProgressView()
            
            guard status == true else {
                var responseMsg = ""
                guard let responseMessage = data as? String else { return }
                responseMsg = responseMessage
                if responseMsg == "We are not allowing do Request Money" {
                    responseMsg = "You cannot request to same destination mobile number within 5 minutes".localized
                    
                }
                self?._show(title: "", errorMeaasge: responseMsg.localized, iconImageName: "confirmation_icon")
                return
            }
            
            guard let dictionary = data as? NSDictionary else {
                
                if let message = data as? String {
                    
                    println_debug("message = \(message)")
                    self?._show(title: "".localized, errorMeaasge: message.localized, iconImageName: "info_success")
                }
                
                return
            }
            
            guard let msg = dictionary["Msg"] as? String  else {
                
                return
            }
            
            guard msg == "Sucess" else {
                
                self?._show(title: "Info".localized, errorMeaasge: msg.localized, iconImageName: "error")
                return
            }
            
            guard let data_ = toDictionary(any: dictionary["Data"]) else {
                
                return
            }
            
            guard let requestId = data_["RequestId"] as? String else {
                
                return
            }
            
            guard let reminderCount = data_["RemainderCount"] as? String else {
                
                return
            }
            
            guard let count =  Int(reminderCount) else {
                
                return
            }
            if count > 0 {
                self?.updateReuestDatawith(ReferenceId: requestId, reminderCount: count)
            }
        }
    }
    
    
    func updateReuestDatawith(ReferenceId referenceId : String , reminderCount : Int) {
        
        guard let items = _dataStore?.result else { return }
        
        var index = -1
        for item in items {
            
            index = index+1
            if item.RequestId == referenceId {
                break
            }
        }
        
        if index != -1 {
            _dataStore?.result[index].RemindCount = reminderCount
        }
        
        
        DispatchQueue.main.async {
//            self.requestItemsTableView?.setContentOffset(.zero, animated: true)
//            self.requestItemsTableView?.reloadData()
            
            if  let collection = self._dataStore?.result, collection.count <= 0
            {
                self.requestItemsTableView?.reloadData()
                
            } else {
            
                self.requestItemsTableView?.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
                   DispatchQueue.main.async {
                       let indexPath = IndexPath(row: 0, section: 0)
                    self.requestItemsTableView?.scrollToRow(at: indexPath, at: .top, animated: true)
                   }
            }
        }
        self._show(title: "".localized, errorMeaasge: "Your Reminder Sent Successfully".localized, iconImageName: "info_success")
    }
    
    //MARK: - Cancel the request
    func updateRequestWithStatus(indexRow index :Int , status : String) {
        
        guard let items = _dataStore?.result, let requestId = items[index].RequestId else { return }
        guard let url = RequestMoneyParams.getAPIForChangeStausOfRequest(requestId: requestId, status: status) else { return }
        
        progressViewObj.showProgressView()
        
        println_debug("api \(url.absoluteString) ")
        
        JSONParser.GetCancelApiResponse(apiUrl: url, type : "POST") { [weak self] (status : Bool, data : Any?) in
            
            progressViewObj.removeProgressView()
            
            guard let _self = self else { return }
            guard status == true else {
                
                guard let errorMessage = data as? String else { return }
                _self._show(title: "Warning".localized, errorMeaasge: errorMessage.localized, iconImageName: "error")
                return
            }
            
            if let message = data as? String {
                
                _self._show(title: "", errorMeaasge: message.localized, iconImageName: "info_success")
                return
            }
            
            guard let rootContent = data as? NSDictionary, let _ = rootContent["Msg"] as? String else { return }
            
            _self._show(title: "", errorMeaasge: "Your Request Cancelled Successfully".localized, iconImageName: "info_success", action: { [weak self] in
                self?._fetchAll()
            })
        }
    }
    
    fileprivate func _show(title :String?, errorMeaasge :String, iconImageName :String = "AppIcon", action :(() ->())? = nil) {
        
        func show() {
            
            alertViewObj.wrapAlert(title: title, body: errorMeaasge, img: UIImage(named : iconImageName))
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                action?()
            })
            alertViewObj.showAlert(controller: self)
        }
        
        if Thread.isMainThread {
            show()
        } else {
            DispatchQueue.main.async {
                show()
            }
        }
    }
}

extension PendingRequestVC : UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidBeginEditing====")
        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
          //  uiButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize) ?? UIFont.systemFont(ofSize: 14)
            if appDel.currentLanguage == "my" {
                uiButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
                uiButton.setTitle("မလုပ်ပါ", for:.normal)
            } else if appDel.currentLanguage == "uni" {
                uiButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
                uiButton.setTitle("မလုပ္ပါ", for:.normal)
            } else {
                uiButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
                uiButton.setTitle("Cancel", for:.normal)
            }
        }
        searchBar.showsCancelButton = true
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidEndEditing====")

        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
          //  uiButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize) ?? UIFont.systemFont(ofSize: 14)
            if appDel.currentLanguage == "my" {
                uiButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
                uiButton.setTitle("မလုပ်ပါ", for:.normal)
            } else if appDel.currentLanguage == "uni" {
                uiButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
                uiButton.setTitle("မလုပ္ပါ", for:.normal)
            } else {
                uiButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
                uiButton.setTitle("Cancel", for:.normal)
            }
            searchBar.showsCancelButton = false
        }
    }
     
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchBar.text == "" {
            
            if searchClear == true {
                searchClear = false
                self._searchByName(searchText: searchBar.text)

            }
        }
    
//         if searchBar.text == "" {
//            print("textDidChange====\(searchBar.text)")
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
//                self._searchByName(searchText: searchBar.text)
//            });
//
//        }


    }
    
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
           if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
               let updatedText = searchText.replacingCharacters(in: textRange, with: text)
               if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: SEARCHCHARSET).inverted).joined(separator: "")) { return false }
            
            print("shouldChangeTextIn===\(updatedText)")

               
               if updatedText == " " || updatedText == "  "{
                   return false
               }
               if (updatedText.contains("  ")){
                   return false
               }
               if updatedText.count > 50 {
                   return false
               }
              // if updatedText != "" && text != "\n" {
                if  text != "\n" {
                searchClear = true
              //  let nc = NotificationCenter.default
  /*  nc.post(name:Notification.Name(rawValue:"DashboardSearchTextChange"),
                                                            object: nil, userInfo: ["searchText" : updatedText]) */
                   _searchByName(searchText: updatedText)
               }
           }
           return true
       }
       
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        //customSearchBar.resignFirstResponder()
        print("PendingRequestVC searchBarCancelButtonClicked-----")
        searchBar.text = ""
        self.delegate?.HideSearchView()
      // self.getAllData()
        self._dataStore = self._SearchStore
        self.requestItemsTableView?.reloadData()

    }
    
    fileprivate func _searchByName(searchText: String?) {
        
        _dataStore?.searchByName(name: searchText)

        DispatchQueue.main.async {
           // self.requestItemsTableView?.reloadData()
            
            if  let collection = self._dataStore?.result, collection.count <= 0
            {
                self.requestItemsTableView?.reloadData()
                
            } else {
            
                self.requestItemsTableView?.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
//                   DispatchQueue.main.async {
//                       let indexPath = IndexPath(row: 0, section: 0)
//                    self.requestItemsTableView?.scrollToRow(at: indexPath, at: .top, animated: true)
//                   }
            } 
        }
    }

    internal func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
       
        searchBar.resignFirstResponder()
    }
    
}


extension PendingRequestVC : dateSelectForSort
{
    
    func okButtonAction(fromDate: Date, toDate: Date, reset: Bool) {

        self.getAllDateSortedList(startDate : fromDate, endDate : toDate)
    }
   
}

extension PendingRequestVC: SendDataDelegate {
    func sendData(text: String) {
        print("PendingApprovalRequestVC sendData called======")

        _dataStore?.searchByName(name: text)
        
        DispatchQueue.main.async {
            self.requestItemsTableView?.reloadData()
        }
    }
  
}

/*
extension PendingRequestVC : CustomSearchControllerDelegate {
    
    func didStartSearching() {
        
        print("PendingRequestVC didTapOnSearchButton called----")
        
    }
    
    func didTapOnSearchButton() {
        print("PendingRequestVC didTapOnSearchButton called----")
        
    }
    
    func didTapOnCancelButton() {
        // customSearchController.customSearchBar.showSearchIcon()
        print("PendingRequestVC cancel button called----")
        
        
    }
    
    func didChangeSearchText(searchText: String) {
        print("PendingRequestVC didChangeSearchText called----")
        
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        print("PendingRequestVC updateSearchResults called----")
        
    }
}
*/
