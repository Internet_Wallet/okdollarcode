//
//  PendingApprovalRequestBaseVC.swift
//  OK
//
//  Created by palnar on 15/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class PendingApprovalRequestBaseVC: ButtonBarPagerTabStripViewController, NavigationSetUpProtocol {
    func setUpNavigation() {
        
    }
    
    
    //MARK: - Properties
    
    private var childViewVCs :[PendingApprovalRequestVC]!
    private var currentVc :PendingApprovalRequestVC!
    private lazy var searchBar = UISearchBar()
    lazy var customSearchController = CustomSearchController(searchResultsController: self, searchBarFrame: CGRect(x:40.0, y:0.0, width:containerView.frame.size.width-70,height:50.0), searchBarFont: UIFont.systemFont(ofSize: 13), searchBarTextColor: UIColor.white, searchBarTintColor: kYellowColor)
    
    //prabu
    var searchClear : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("PendingApprovalRequestBaseVC  called----")

        //self.title = "Pending Approval Payment Request"
        searchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.tintColor = UIColor.white
        searchBar.placeholder = "Search".localized
//        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: "Zawgyi-One", size: 18) {
//            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
//                searchTextField.font = myFont
//            }
//        }
     
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
               // searchTextField.font = (appDel.currentLanguage == "uni") ? UIFont.systemFont(ofSize: 15) : myFont
               // searchTextField.font = (appDel.currentLanguage == "uni") ? UIFont.systemFont(ofSize: 15) : myFont
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                
                searchTextField.keyboardType = .asciiCapable
                searchTextField.font = UIFont(name: appFont, size: appFontSize)
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
            }
        }
     
        
       let view = self.searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = UIColor.gray
            }
        }
        
         NotificationCenter.default.addObserver(self, selector: #selector(hideAndClearSearch), name: NSNotification.Name(rawValue: "ClearAndHideSearchBar"), object: nil)
        
        configureTabBarView()
        
       // hideKeyboardWhenTappedAround()
        //customSearchController.customSearchBar.setUpSearchIconAtRightSide()
        
        //var searchClear  = false
       
        self.setNavigation(isSearchNeeded:true)
        
        //Manage Keyboard
        IQKeyboardManager.sharedManager().enable            = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        
    }
    
    @objc func hideAndClearSearch() {
        DispatchQueue.main.async {
          self.setNavigation(isSearchNeeded:true)
        }
      }
    
  /*  private func setLeftBarButtonItems() {
        let button1 = UIBarButtonItem(image: UIImage(named:"tabBarBack"), style: .plain, target: self, action:#selector(backButtonAction)) // action:#selector(Class.MethodName) for swift 3
        button1.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItems = [button1]
    }
    
    func setRightBarButtonItems() {
        let searchButton = UIBarButtonItem(image: UIImage(named:"r_search"), style: .plain, target: self, action:#selector(searchButtonAction))
        searchButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItems = [searchButton]
    } */
    
    @objc func backButtonAction()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
//    @objc func searchButtonAction() {
//
//        configureSearchController()
//    }
    
    @objc private func showSearchOption() {
        searchBar.placeholder = "Search".localized
        searchBar.text = ""
        self.navigationItem.titleView = searchBar
        searchBar.becomeFirstResponder()
    }
    
    func setNavigation(isSearchNeeded: Bool) {
        
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        if isSearchNeeded {
            let searchButton = UIButton(type: .custom)
            searchButton.setImage(UIImage(named:"r_search"), for: .normal)
            searchButton.addTarget(self, action: #selector(showSearchOption), for: .touchUpInside)
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
            navTitleView.addSubview(searchButton)
        }
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "tabBarBack").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.font = UIFont(name: appFont, size: appFontSize)
        label.text =  "Pending Approval Payment Request".localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 17) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print("Pending Approval viewWillAppear")
        self.setNavigation(isSearchNeeded:true)

        
        //configureSearchController()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //customSearchController.customSearchBar.showSearchIcon()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Tabbar view customisation
    
    func configureTabBarView(){
        //self.navigationController?.navigationBar.backgroundColor = kYellowColor
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .white
        settings.style.selectedBarBackgroundColor = .black//kYellowColor
        settings.style.buttonBarItemFont = UIFont(name: appFont, size: 14.0)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .black
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool,_ index:Int) -> Void in
            
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .black
            newCell?.label.textColor = kSelectedTextColor//.black
            
            guard let _self = self else { return }
            
            let currentVC = _self.childViewVCs[index]
            let searchBar = _self.customSearchController.customSearchBar
            
            _self.currentVc = currentVC
            searchBar?.delegate = currentVC
            
            guard let searchBar_ = searchBar else { return }
            currentVC.searchBar(searchBar_, textDidChange: "")
        }
    }
    
    override func viewControllersForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child_1 = UIStoryboard(name: "PendingRequestSB", bundle: nil).instantiateViewController(withIdentifier: "pending_approval_request_result") as! PendingApprovalRequestVC
        child_1.delegate = self
        child_1.selectedItemIndex = 1
        let child_2 = UIStoryboard(name: "PendingRequestSB", bundle: nil).instantiateViewController(withIdentifier: "pending_approval_request_result") as! PendingApprovalRequestVC
        child_2.delegate = self
        child_2.selectedItemIndex = 2
        let child_3 = UIStoryboard(name: "PendingRequestSB", bundle: nil).instantiateViewController(withIdentifier: "pending_approval_request_result") as! PendingApprovalRequestVC
        child_3.delegate = self
        child_3.selectedItemIndex = 3
        let child_4 = UIStoryboard(name: "PendingRequestSB", bundle: nil).instantiateViewController(withIdentifier: "pending_approval_request_result") as! PendingApprovalRequestVC
        child_4.delegate = self
        child_4.selectedItemIndex = 4
        
        child_1.searchBar = self.customSearchController.customSearchBar
        child_2.searchBar = self.customSearchController.customSearchBar
        child_3.searchBar = self.customSearchController.customSearchBar
        child_4.searchBar = self.customSearchController.customSearchBar
        
        childViewVCs = [
            child_1,
            child_2,
            child_3,
            child_4
        ]
        
        return [child_4,child_1, child_2,child_3]
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    //MARK: -Search Configation
    
    func configureSearchController() {
        /* searchController = UISearchController(searchResultsController: nil)
         searchController.searchResultsUpdater = self as? UISearchResultsUpdating
         searchController.dimsBackgroundDuringPresentation = true
         searchController.searchBar.placeholder = "Search here..."
         searchController.searchBar.delegate = self as? UISearchBarDelegate
         searchController.searchBar.sizeToFit()*/
        
        
        //searchBar.placeholder = "Pending Request"
        //searchBar.barTintColor = kYellowColor
        //searchBar.backgroundColor = kYellowColor
        // searchBar.barStyle = UISearchBarStyle.prominent
        //UISearchBarStyle.Prominent
        //searchBar.isTranslucent = false
        
        
        //customSearchController.customDelegate = self
        //customSearchController.customSearchBar.customDelegate = self
        
        
    
}
    
}

// MARK: -UISearchBarDelegate
extension PendingApprovalRequestBaseVC: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("PendingApprovalRequestBaseVC searchBarTextDidBeginEditing----")

        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
           // uiButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize) ?? UIFont.systemFont(ofSize: 14)
            if appDel.currentLanguage == "my" {
                uiButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
                uiButton.setTitle("မလုပ်ပါ", for:.normal)
            } else if appDel.currentLanguage == "uni" {
                uiButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
                uiButton.setTitle("မလုပ္ပါ", for:.normal)
            } else {
                uiButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
                uiButton.setTitle("Cancel", for:.normal)
            }
        }

    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //_searchByName(searchText: searchBar.text)
        
        print("PendingApprovalRequestBaseVC textDidChange----")
        
        if searchBar.text == "" {
                        
            if searchClear == true {
                
            let nc = NotificationCenter.default
            
                nc.post(name:Notification.Name(rawValue:"DashboardSearchTextChange"),object: nil, userInfo: ["searchText" : searchBar.text ?? ""])
            }
        }


     /*  if searchBar.text == "" {
            print("textDidChange====\(searchBar.text)")
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                
                let nc = NotificationCenter.default
                        nc.post(name:Notification.Name(rawValue:"DashboardSearchTextChange"),
                                                            object: nil, userInfo: ["searchText" : searchBar.text])
            });

        } */
        
        
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        print("PendingApprovalRequestBaseVC shouldChangeTextIn----")

        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: SEARCHCHARSETOffer).inverted).joined(separator: "")) { return false }
            
            if updatedText == " " || updatedText == "  "{
                return false
            }
            if (updatedText.contains("  ")){
                return false
            }
            if updatedText.count > 50 {
                return false
            }
           // if updatedText != "" && text != "\n" {
            if  text != "\n" {
              //  _searchByName(searchText: searchBar.text)
                print("Approvalrequest====\(updatedText)")
                //var searchClear  = true

                let nc = NotificationCenter.default
                                     nc.post(name:Notification.Name(rawValue:"DashboardSearchTextChange"),
                                             object: nil, userInfo: ["searchText" : updatedText])
            
            }
        }
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("PendingApprovalRequestBaseVC searchBarCancelButtonClicked----")

//        isSearchNeed = true
//        setUpNavigation()
//        transactionRecords = initialRecords
//        buildingDataSet()
//        tableViewReceipt.reloadData()
//        multiButton?.show()
        
          NotificationCenter.default.post(name: Notification.Name(rawValue: "ClearSearchBar"), object: nil)
        
         self.setNavigation(isSearchNeeded:true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("PendingApprovalRequestBaseVC searchBarSearchButtonClicked----")

        searchBar.endEditing(true)
    }
}



/*
extension PendingApprovalRequestBaseVC : CustomSearchControllerDelegate {
    
    func didStartSearching() {
        print("PendingApprovalRequestBaseVC cancel button called----")

    }
    
    func didTapOnSearchButton() {
         print("PendingApprovalRequestBaseVC cancel button called----")
    }

    
    func didTapOnCancelButton() {
        print("PendingApprovalRequestBaseVC cancel button called----")

    }
    
    func didChangeSearchText(searchText: String) {
        print("PendingApprovalRequestBaseVC cancel button called----")

    }
    
    func updateSearchResults(for searchController: UISearchController) {
        print("PendingApprovalRequestBaseVC cancel button called----")

   }
} */



extension PendingApprovalRequestBaseVC : ShowHideSearchButtonProtocol
{
    func showHideSearchButton(status : Bool) {
       /* if status {
            self.setRightBarButtonItems()
        } else {
            self.navigationItem.rightBarButtonItems = nil
        } */
    }
    
    func HideSearchView() {
        
     /*   self.title = "Pending Approval Payment Request".localized + " "
               self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20)!]

               self.customSearchController.customSearchBar.setUpSearchIconAtRightSide()
               self.setLeftBarButtonItems()
               self.setRightBarButtonItems()
       */
    }
}

