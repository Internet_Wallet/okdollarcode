//
//  PendingApprovalCashBack.swift
//  OK
//
//  Created by E J ANTONY on 12/3/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation

 
extension PendingApprovalRequestVC {
    
     //MARK:- API Call For Multi Payment
    func multiTopupSimCall(amount: Double,number:String, planType:String) {
        guard let url = URL.init(string: TopupUrl.otherNumberTopupRetrieve) else { return }
        
        var finalDict = [Dictionary<String,Any>]()
        var subDictionary = Dictionary<String,Any>()
        //                             var amount = amount
        //                             amount = amount?.replacingOccurrences(of: ",", with: "")
        //                             amount = amount?.replacingOccurrences(of: " MMK", with: "")
        
        let str = number.replacingOccurrences(of: "0095", with: "0")
        println_debug(str)
        
        let rangeCheck = PayToValidations().getNumberRangeValidation(str)
        
        let operatorName = rangeCheck.operator.lowercased()
        
        subDictionary["Amount"] = amount
        subDictionary["ByCellID"] = "false"
        subDictionary["Customer"] = str
        subDictionary["Lat"] = "16.816666"
        subDictionary["Long"] = "96.131"
        subDictionary["MobileNumber"] = UserModel.shared.mobileNo
        subDictionary["Operator"] = operatorName
        subDictionary["balance"] = UserLogin.shared.walletBal
        println_debug(planType)
        if  planType == "DATAPLAN"{
            subDictionary["PlanType"] = "Data Plan"
            subDictionary["Type"] = "1"
        } else if planType == "TOPUP" {
            subDictionary["PlanType"] = "Top-Up Plan"
            subDictionary["Type"] = "0"
        } else  {
            subDictionary["PlanType"] = "SpecialOffer"
            subDictionary["Type"] = "2"
        }
        
        subDictionary["SubScriberName"] = ""
        
        
        
        finalDict.append(subDictionary)
        
        println_debug(finalDict)
        
        let params = finalDict//MultiTopupParameters().getMultiTopupSim(MultiTopupArray.main.controllers, plan: "Top-Up Plan", type: "0")
        
        let jsonParam = JSONStringFromAnyObject(value: params as AnyObject)
        
        print("multiTopupSimCall Agent url : \(url)")
        print("multiTopupSimCall jsonParam ===\(jsonParam)")
        
        TopupWeb.genericClass(url: url, param: jsonParam as AnyObject, httpMethod: "POST") { [weak self](response, success) in
            if success {
                DispatchQueue.main.async {
                    if let json = response as? [Dictionary<String,Any>] {
                        println_debug(json)
                        self?.cashBackdictionary.removeAll()
                        print("multiTopupSimCall response ===\(response)")

                        for (index,element) in json.enumerated() {
                            print("multiTopupSimCall element ===\(element)")
                            print("multiTopupSimCall index ===\(index)")

                            if let str = element.safeValueForKey("Data") as? String {
                                if element.safeValueForKey("Code") as? Int == 300 {
                                    if let object = MultiTopupArray.main.controllers[safe: index] {
                                        if object.operatorName.text?.lowercased() == "MecTel".lowercased() {
                                            var first = Dictionary<String,Any>()
                                            first["mobileKey"] = MultiTopupArray.main.controllers[safe: index]?.mobileNumber.text
                                            var number = MultiTopupArray.main.controllers[safe: index]?.mobileNumber.text
                                            _ = number?.removeFirst()
                                            if let num = number {
                                                first["MobileNumber"] = "0095" + num
                                            }
                                            first["amountKey"] = MultiTopupArray.main.controllers[safe: index]?.amount.text
                                            print("multiTopupSimCall first ===\(first)")

                                            self?.cashBackdictionary.append(first)
                                        }
                                    }
                                }
                                if let dict = OKBaseController.convertToArrDictionary(text: str) {
                                    if let arrDict = dict as? [Dictionary<String,Any>] {
                                        if var first = arrDict.first {
                                            first["mobileKey"] = MultiTopupArray.main.controllers[safe: index]?.mobileNumber.text
                                            first["amountKey"] = MultiTopupArray.main.controllers[safe: index]?.amount.text
                                            self?.cashBackdictionary.append(first)
                                        }
                                    }
                                }
                            }
                        }
                        println_debug(self?.cashBackdictionary)
                        guard let weakSelf = self else { return }
                        if weakSelf.cashBackdictionary.count > 0 {
                            if UserLogin.shared.loginSessionExpired {
                                OKPayment.main.authenticate(screenName: "multiCashback", delegate: weakSelf)
                            } else {
                                println_debug(weakSelf.cashBackdictionary)
                                self?.cashbackApiCallingForOtherNumber(amount: amount, number: weakSelf.cashBackdictionary.first?["MobileNumber"] as? String ?? "",planType:planType, payNumber: str, opName: operatorName)
                            }
                        } else {
                            alertViewObj.wrapAlert(title: nil, body: "Please Try Again!".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
                            alertViewObj.addAction(title: "OK", style: .cancel, action: {
                            })
                            alertViewObj.showAlert()
                        }
                    }
                }
            }
        }
    }
        
        // for multi topup
    func cashbackApiCallingForOtherNumber(amount: Double,number:String,planType:String, payNumber: String, opName: String) {
            
        print("Calling cashbackApiCallingForOtherNumber====")
            let url = URL.init(string: TopupUrl.otherNumberMultiCashback)
       //     let param = JSONStringFromAnyObject(value: MultiTopupParameters().cashbackMultiApiParameters(mpModel: MultiTopupArray.main.controllers, amount: "1000", dictCashback: self.cashBackdictionary) as AnyObject)
            
            var finalObject =  Dictionary<String,AnyObject>()
            var dict        = [Dictionary<String,String>]()
               
            var cashbackobj = Dictionary<String,String>()
            cashbackobj["Agentcode"]   = UserModel.shared.mobileNo
            let mobile = number
          //  let customerMobile = dictionaryCashback.safeValueForKey("mobileKey") as? String
            let amountMobile   = amount
                       
//            var vc : TopupViewController?
//                       for element in mpModel {
//                           if element.mobileNumber.text == customerMobile, element.amount.text == amountMobile {
//                               vc = element
//                           }
//                       }
//                       var amountA = (vc == nil) ? "" : vc!.amount.text
//                       amountA = amountA?.replacingOccurrences(of: ",", with: "")
//                       amountA = amountA?.replacingOccurrences(of: " MMK", with: "")
            
            
             let str = number.replacingOccurrences(of: "0095", with: "0")
                                                println_debug(str)
          
              let operatorName = opName.lowercased()
            
            
                       cashbackobj["Amount"]      = "\(amountMobile)"
                       
                       cashbackobj["Clientip"]    = OKBaseController.getIPAddress()
                       cashbackobj["Clientos"]    = "iOS"
                       cashbackobj["Clienttype"]  = "GPRS"
                       
                       cashbackobj["Destination"]    = mobile
                       cashbackobj["Pin"]            = ok_password ?? ""
                       cashbackobj["Requesttype"]    = "FEELIMITKICKBACKINFO"
                       cashbackobj["Securetoken"]    = UserLogin.shared.token
                       
                       if operatorName.lowercased() == "MecTel".lowercased() {
                        print()
                           cashbackobj["Transtype"]      =  "TOPUP"
                       } else {
                           cashbackobj["Transtype"]      =  "PAYTO"
                       }
            
                     //  cashbackobj["Transtype"]      =  "PAYTO"
                       cashbackobj["Vendorcode"]     = "IPAY"
                       dict.append(cashbackobj)
                 
               
               var log = Dictionary<String,Any>()
               log["MobileNumber"] = UserModel.shared.mobileNo
               log["Msid"]         = UserModel.shared.msid
               log["Ostype"]       = 1
               log["Otp"]          = ""
               log["SimId"]        = uuid
               
               finalObject["CashBackRequestList"] = dict as AnyObject
               finalObject["Login"] = log as AnyObject
               print("cashbackApiCallingForOtherNumber finalObject====\(finalObject)")
            
            
            
            print("Check Agent url : \(String(describing: url))")
          //  println_debug(param)

            
            TopupWeb.genericClass(url: url!, param: finalObject as AnyObject, httpMethod: "POST") { [weak self](response, successfull) in
                if successfull {
                    DispatchQueue.main.async {
                        if let value = response as? [Dictionary<String,AnyObject>] {
                            guard let weakSelf = self else { return }
                            self?.arrayKickBackInfo.removeAll()
                            print("cashbackApiCallingForOtherNumber response===\(response)")
                            for (index,item) in value.enumerated() {
                                let obj = item["Data"] as? String ?? ""
                                let xml = SWXMLHash.parse(obj)
                                //print("cashbackApiCallingForOtherNumber item===\(item["Data"])")

                                self?.kickBackInfoDictionary.removeAll()
                                self?.enumerate1(indexer: xml)

                                if let transaction = weakSelf.kickBackInfoDictionary.safeValueForKey("resultdescription") as? String, transaction == "Transaction Successful" {
                                    //let number1 = (weakSelf.kickBackInfoDictionary.safeValueForKey("destination") as? String) == nil ? "" : (weakSelf.kickBackInfoDictionary.safeValueForKey("destination") as? String)
                                    //let numberMobile = self?.cashBackdictionary[safe: index]?.safeValueForKey("mobileKey") as? String ?? ""
                                    let amountCheck = self?.cashBackdictionary[safe: index]?.safeValueForKey("amountKey") as? String ?? ""
                                    
                                    let str = number.replacingOccurrences(of: "0095", with: "0")
                                    println_debug(str)
//                                    var elementScan: TopupViewController?
//                                    for element in MultiTopupArray.main.controllers {
//                                        if element.mobileNumber.text == num, element.amount.text == amountCheck {
//                                            elementScan = element
//                                        }
//                                    }
                                    
                            
//                                     let rangeCheck = PayToValidations().getNumberRangeValidation(str)
//
//                                    let operatorName = rangeCheck.operator.lowercased() //(elementScan == nil) ? "" : elementScan!.opName
//                                    println_debug(operatorName)

                                    var type  = "" //(elementScan == nil) ? "" : elementScan!.plantype
                                    println_debug(planType)
                                    
                                    if  planType == "DATAPLAN"{
                                        type = "Data Plan"
                                        
                                    } else if  planType == "TOPUP"{
                                        type = "Top-Up Plan"
                                    }
                                    else {
                                        type = "SpecialOffer"
                                    }
                                     
                                    
                                   
                                   //operatorName = (rangeCheck.operator != "Mpt") ? rangeCheck.operator : rangeCheck.operator.uppercased()
                                   // operatorName = rangeCheck.operator
                                    
                                  //  operatorName = PayToValidations().getColorForOperator(operatorName: rangeCheck.operator)
                           
                                    let cashback = (weakSelf.kickBackInfoDictionary.safeValueForKey("kickback") as? String) == nil ? "" : (weakSelf.kickBackInfoDictionary.safeValueForKey("kickback") as? String)
                                    let amount   = (weakSelf.kickBackInfoDictionary.safeValueForKey("amount") as? String) == nil ? "" : (weakSelf.kickBackInfoDictionary.safeValueForKey("amount") as? String)

                                    //let nameUser = "Unknown"
//                                    if let obj = elementScan, obj.confimMobile.text!.containsAlphabets() {
//                                        nameUser = obj.confimMobile.text ?? nameUser
//                                    }

                                    if MultiTopupArray.main.controllers.count == 1 && !PaymentVerificationManager.isValidPaymentTransactionsForOperator(operatorName , amountCheck, number ) {
                                        //self?.cashbackApiCallingForOtherNumber()
                                        return
                                    }

    //                                //Remove alert for TopupHomeExtensionViewController
    //                                DispatchQueue.main.async {
    //                                    alertViewObj.wrapAlert(title: "Backend Number", body: number ?? "".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
    //                                    alertViewObj.addAction(title: "OK", style: .cancel, action: {
    //                                    })
    //                                    alertViewObj.showAlert()
    //                                }

                                    //let payNum = str//elementScan?.mobileNumber.text ?? ""

//                                    let model = TopupConfirmationModel.init(nameUser, number: number ?? "", opeName: operatorName ?? "", type: type , cashbk: cashback ?? "", topAmt: amount ?? "", payNumber: payNum ?? "")
                                    
                                    let model = TopupConfirmationModel.init("Unknown", number: number , opeName: operatorName, type: type, cashbk: cashback ?? "", topAmt: amount ?? "", payNumber: payNumber )
                                  //  if elementScan != nil {
                                        self?.arrayKickBackInfo.append(model)
                                   // }
                                } else {
                                    alertViewObj.wrapAlert(title: nil, body: weakSelf.kickBackInfoDictionary.safeValueForKey("resultdescription") as? String ?? "Please Try Again!".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
                                    alertViewObj.addAction(title: "OK", style: .cancel, action: {
                                    })
                                    alertViewObj.showAlert()
                                }
                            }
//                            if weakSelf.arrayKickBackInfo.count > 0 {
//
//                                guard let vc = weakSelf.storyboard?.instantiateViewController(withIdentifier: "CommonTopupTransactionViewController") as? CommonTopupTransactionViewController else { return }
//                                vc.arrDictionary = [weakSelf.kickBackInfoDictionary]
//                                vc.delegate = self as? CommonTopupTransactionViewControllerDelegate
//                                vc.modelArray = weakSelf.arrayKickBackInfo
//                                vc.cashBackMerchantNumber = weakSelf.cashBackdictionary
//                                weakSelf.navigationController?.pushViewController(vc, animated: true)
//                            }
                            
//                            guard  let vc = self?.storyboard?.instantiateViewController(withIdentifier: String.init(describing: CommonTopupTransactionViewController.self)) as? CommonTopupTransactionViewController else { return }
//                            guard self != nil else { return }
                            
                             let vc =  UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: "CommonTopupTransactionViewController") as? CommonTopupTransactionViewController
                           vc?.arrDictionary = [weakSelf.kickBackInfoDictionary]
                           vc?.delegate = self as? CommonTopupTransactionViewControllerDelegate
                           vc?.modelArray = weakSelf.arrayKickBackInfo
                           vc?.cashBackMerchantNumber = weakSelf.cashBackdictionary
                           vc?.strnumber = payNumber
                           vc?.fromPendingApproval = "pendingRequestMoney"
                            
                            //prabu
                            vc?.displayResponse = self!.processedTransaction
                            vc?.requestMoney = true
                            
                            if let nav = self?.navigationController {
                                nav.pushViewController(vc!, animated: true)
                                return
                             }
                             if let navBack = self?.navigationObject {
                                navBack.pushViewController(vc!, animated: true)
                             return
                             }
                            
                        }
                    }
                }

            }
        }
        
        func enumerate1(indexer: XMLIndexer) {
            for child in indexer.children {
                kickBackInfoDictionary[child.element!.name] = child.element!.text
                enumerate1(indexer: child)
            }
        }
}

