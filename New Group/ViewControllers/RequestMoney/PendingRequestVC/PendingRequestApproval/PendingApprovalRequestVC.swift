//
//  PendingApprovalRequestVC.swift
//  OK
//
//  Created by palnar on 15/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit


class PendingApprovalRequestVC: OKBaseController {
    
    //MARK: Properties
    @IBOutlet weak var requestItemsTableView: UITableView?
    var vcTitle = "Pending Approval Payment Request"
    @IBOutlet weak var noRecordsFoundView: UIView?
    @IBOutlet weak var noRecordsLabel: UILabel?
    
    private var multiButton : ActionButton?
    
    var agentType : String = ""
    var businessNameNew = ""
    
    private let refreshControl = UIRefreshControl()
    
    var navigationObject : UINavigationController?
    
      var datadelegate: SendDataDelegate?
    
    var filtered = [Dictionary<String, String>]()
    var searchActive = false
    
    var comparstr = String()
    
    var cancelStr = ""
    
    open  var selectedItemIndex : Int = 1
    open  var selectedFilterIndex : Int = -1
    fileprivate var filterItems  = [CriteriaFilterItem]()
    
    fileprivate var _dataStore :PendingRequestData?
     fileprivate var _SearchStore :PendingRequestData?
    fileprivate weak var _dummyTextField :UITextField!
    fileprivate weak var _overlayView :OverlayView!
    
    var  requestModuleData =  [String : Any]()
    var requestCancelData = [String : Any]()
    var  requestModuleDataToPay =  [String : Any]()
    weak var searchBar: UISearchBar?
    
    var selectedTap = false
    var FromScanQR = ""
 //   private var titleValue = ""
    
    var transaction      = Dictionary<String,Any>()
    var processedTransaction = [Dictionary<String,Any>]()
     var kickBackInfoDictionary = Dictionary<String,Any>()
     var arrayKickBackInfo = [TopupConfirmationModel]()
    //MARK: - Outlets
    weak var delegate : ShowHideSearchButtonProtocol?
    var attachmentViewTap = false
    fileprivate lazy var _APIDateFormater: DateFormatter = {
        
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = GlobalConstants.Strings.defaultAPIDateFormat
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter
    }()
    
    fileprivate lazy var _presentationDateFormater: DateFormatter = {
        //
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "EEE, dd-MMM-yyyy hh:mm a"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter
    }()
    
    //For cashback
    var cashBackdictionary = [Dictionary<String,Any>]()

    
    //MARK:   -  Override functions
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        print("PendingApprovalRequestVC viewDidLoad  called----")

        self.title = vcTitle.localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        
         comparstr = "0"

        configureRequestTypesTableView()
        _setUpDatePicker()
        if !attachmentViewTap {
            attachmentViewTap = false
            self.noRecordsLabel?.text = ""
            self.delegate?.showHideSearchButton(status: false)
//            _dataStore = nil
//            _fetchAll()
        } else {
            attachmentViewTap = false
        }
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTableView), name: NSNotification.Name("ReloadNotificationVC"), object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(inboxslection(notification:)), name:NSNotification.Name("DashboardVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSearchText(_:)), name: NSNotification.Name(rawValue: "DashboardSearchTextChange"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(clearreloadTableView), name: NSNotification.Name("ClearSearchBar"), object: nil)
        let defaultvalue = userDef.value(forKey: "RequestMoneyApproval") as? String
               
        if defaultvalue != ""{
            FromScanQR = defaultvalue ?? ""
        }
        else
        {
            FromScanQR = ""
        }
        
        if #available(iOS 10.0, *) {
            requestItemsTableView!.refreshControl = refreshControl
        } else {
            requestItemsTableView!.addSubview(refreshControl)
        }
        
     
      //  DispatchQueue.main.async(execute: {

            self.refreshControl.addTarget(self, action: #selector(self.refreshTableData), for: .valueChanged)
        
      //  })
        
        self.loadFloatButtons()
        self.multiButton?.hide()
        
        _dataStore = nil
        _fetchAll()
     
    }
    
    //MARK:- Floating Buttons
    func loadFloatButtons() {
        var buttonItems = [ActionButtonItem]()
        let dateFilter = ActionButtonItem(title: "Date".localized, image: #imageLiteral(resourceName: "receiptSortDate"))
        dateFilter.action = { item in
            self.multiButton?.toggleMenu()
           self._showDatePicker()
            
        }
        
        let sortFilter = ActionButtonItem(title: "Sort".localized, image: #imageLiteral(resourceName: "offer_report_menu_icon_sort"))
        sortFilter.action = { item in
            self.multiButton?.toggleMenu()
           self._showSortView();
            
        }
        
        let filter = ActionButtonItem(title: "Filter".localized, image: #imageLiteral(resourceName: "offer_report_menu_icon_filter"))
        filter.action = { item in
            self.multiButton?.toggleMenu()
           self._showFilterView()
        }
        buttonItems = [dateFilter,sortFilter,filter]
        
        multiButton = ActionButton(attachedToView: self.view, items: buttonItems)
        multiButton?.action = {
            button in button.toggleMenu()
        }
        multiButton?.setImage(#imageLiteral(resourceName: "menu_white_payto"), forState: .normal)
        multiButton?.backgroundColor = kYellowColor
    }
    
    @objc func reloadTableView() {
        
            _dataStore = nil
            _fetchAll()
       
    }
    
    @objc func clearreloadTableView() {
        self._dataStore = self._SearchStore
        self.requestItemsTableView?.reloadData()
     }
    
    deinit {
        //NotificationCenter.default.removeObserver(self, name: NSNotification.Name("ReloadNotificationVC"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("DashboardVC"), object: nil)
    }
    @objc func refreshTableData() {
         
        print("PendingApprovalRequestVC refreshTableData  called----\(selectedItemIndex)")

        NotificationCenter.default.post(name: Notification.Name(rawValue: "ClearAndHideSearchBar"), object: nil)
        _dataStore = nil
        _fetchAll()
     }
    
    override func viewWillDisappear(_ animated: Bool) {
        //Show icon default
//        userDef.set(false , forKey: "defaultSort")
//        userDef.set(false , forKey: "amountHLSort")
//        userDef.set(false , forKey: "amountLHSort")
//        userDef.set(false , forKey: "nameAZSort")
//        userDef.set(false , forKey: "nameZASort")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print("PendingApprovalRequestVC viewWillAppear  called----\( self.selectedTap)")

        userDef.set(true , forKey: "defaultSort")
        userDef.set(false , forKey: "amountHLSort")
        userDef.set(false , forKey: "amountLHSort")
        userDef.set(false , forKey: "nameAZSort")
        userDef.set(false , forKey: "nameZASort")
        
      //  self.datadelegate?.sendData(text: "vamsi")
        
        userDef.removeObject(forKey: "defaultcategory0")
        userDef.removeObject(forKey: "defaultType")
        userDef.removeObject(forKey: "default")
        userDef.removeObject(forKey: "defaultcategory2")
        userDef.removeObject(forKey: "defaultcategory1")
        
        
        if self.selectedTap == true {
            self.selectedTap = false
            print("PendingApprovalRequestVC viewWillAppear  selectedTap----\(selectedTap)")
            reloadTableView()
        }else {
//            if titleValue == "All"{
//                selectedItemIndex = 1
//            }
//            if titleValue == "Pending Approval"{
//                selectedItemIndex = 2
//            }
//            else if titleValue == "Request Approved"{
//                selectedItemIndex = 3
//            }
//            else if titleValue == "Cancelled"{
//                selectedItemIndex = 4
//            }
//            else
//            {
//                selectedItemIndex = 1
//            }
            

             reloadTableView()
        }
        
        let count = self._dataStore?.result.count ?? 0
        if count >= 1 {
        }else {
            NotificationCenter.default.post(name: NSNotification.Name("inboxRightBarHide"), object: nil, userInfo: nil)
        }
      
     /*   DispatchQueue.main.async(execute: {
            self.selectedItemIndex = 4
            self._dataStore = nil
            self._fetchAll()
        }) */
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func showSearchText(_ notification: NSNotification) {
        if let searchText = notification.userInfo?["searchText"] as? String{
            print("showSearchText===\(searchText)")

            self._searchByName(searchText: searchText)
//            self.filtered = self._dataStore?.result.filter({ (pair) -> Bool in
//                guard let mallName = pair["destinationName"] as? String else { return false }
//                return mallName.containsIgnoringCase(find: searchText)
//            }) as! [Dictionary<String, String>]
//            if let searchStatus = notification.userInfo?["searchStatus"] as? Bool {
//                searchActive = searchStatus
//                if searchActive {
//                    DispatchQueue.main.async {
//                        self.requestItemsTableView?.reloadData()
//                    }
//                }
//            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //Pending Item Lists Configuration
    private func configureRequestTypesTableView(){
        
        requestItemsTableView?.register(UINib(nibName : "RequestCell",bundle : nil), forCellReuseIdentifier: "requestTypeCell")
    }
    
    private func _setUpDatePicker() {
        UserDefaults.standard.removeObject(forKey: "StartDate")
        UserDefaults.standard.removeObject(forKey: "EndDate")
         /*
        guard let overlayView = OverlayView.newInstance() else { return }
        self.navigationController?.view.addSubview(overlayView)
        _overlayView = overlayView
        overlayView.didTap = { [weak self] (view) in
            
            self?._hidePicker()
        }
       
        guard let overlayView = OverlayView.newInstance(), let datePickerView = PendingRequestDatePicker.newInstance() else { return }
        
        let textField = UITextField()
        
        textField.inputView = datePickerView
        
        overlayView.isHidden = true
        textField.isHidden = true
        textField.alpha = 0.0
        
        self.navigationController?.view.addSubview(overlayView)
        self.view.addSubview(textField)
        
        overlayView.didTap = { [weak self] (view) in
            
            self?._hidePicker()
        }
        
        datePickerView.fromDidPick = { [weak self] (picker, date) in
            self?._fromDidPickDate(date: date)
        }
        
        datePickerView.fromDidCancel = { [weak self] (picker) in
            self?._fromDidPickDate(date: nil)
        }
        
        
        datePickerView.didPick = { [weak self] (picker, date) in
            
            self?._hidePicker()
            self?._didPickDate(date: date)
        }
        
        datePickerView.didCancel = { [weak self] (picker) in
            
            self?._hidePicker()
            self?._didPickDate(date: nil)
        }
        
        _overlayView = overlayView
        _dummyTextField = textField
 */
    }
    
    func getCategoryList(number: String) {
              
               if appDelegate.checkNetworkAvail() {
                   let web      = WebApiClass()
                   web.delegate = self
                   showProgressView()
                   
                   let buldApi = Url.billSplitterMobileValidation + "MobileNumber=\(number)"
                   let url = getUrl(urlStr: buldApi, serverType: .serverApp)
                   println_debug(url)
                   let params = Dictionary<String,String>()
                   
                   web.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", mScreen: "mMobileValidation")
                 }
          }
    
}
//MARK: - Web Response Delegate


extension PendingApprovalRequestVC :WebServiceResponseDelegate {
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
      if screen == "mMobileValidation"
        {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(dic)
                        print("BankCounterDepositDetailsVC Dic----\(dic)")

                        
                        if dic["Code"]! as! Int == 200
                        {
                            
                            DispatchQueue.main.async {
                               
                                let dataDict = dic["Data"] as? String
                                let tempData = OKBaseController.convertToDictionary(text: dataDict!)
                               self.agentType = tempData?["AccountType"] as! String
                                self.businessNameNew = tempData?["BusinessName"] as! String
                            // self.count = self.count + 1
                                
                                
//                                if self.count == self.mobileNumberArray.count{
//                                    self.tableV.delegate = self
//                                    self.tableV.dataSource = self
//
//
//                                }
                              //  DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
                                   //
                               // })
     
                            }
                        }
                        else {
                            DispatchQueue.main.async {
                                
                                alertViewObj.wrapAlert(title: nil, body:"This Number is not registered with OK$".localized, img:  UIImage(named: "phone_alert") )
                                
                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                    
                                }
                                alertViewObj.showAlert(controller: self)
                                
                            }
                        }
                    }
                    
                }
            } catch {
                
            }
        }
    }
}

//MARK: - TabStrip Provider
extension PendingApprovalRequestVC : IndicatorInfoProvider {
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        
        var titleValue = ""
        switch self.selectedItemIndex {
        case 1 :
             titleValue =  "Pending Approval"
         break
        case 2 :
             titleValue = "Request Approved"
           
            break
        case 3 :
             titleValue = "Cancelled"
            break
        case 4 :
           titleValue = "All"
            break
            
        default :
            titleValue = "All"
            break
        }
        return IndicatorInfo(title : titleValue.localized)
    }
}

//Mark : -  View Extensions
extension PendingApprovalRequestVC {
   
    private func _showSortView() {
        
        guard let dataStore = _dataStore else { return }
        guard dataStore.result.count > 0 else { return }
        var superView = self.navigationController?.view
        guard let sortView = PendingRequestSortView.newInstance() else { return }
        if superView == nil, let nav = navigationObject {
            superView = nav.view
        }
        
        if userDef .value(forKey: "defaultSort") as! Bool == true {
            sortView.deafultImage.isHidden = false
            sortView.hightolowImage.isHidden = true
            sortView.lowtohightImage.isHidden = true
            sortView.atozImage.isHidden = true
            sortView.ztoaImage.isHidden = true
        } else if userDef .value(forKey: "amountHLSort") as! Bool == true {
            sortView.deafultImage.isHidden = true
            sortView.hightolowImage.isHidden = false
            sortView.lowtohightImage.isHidden = true
            sortView.atozImage.isHidden = true
            sortView.ztoaImage.isHidden = true
        } else if userDef .value(forKey: "amountLHSort") as! Bool == true {
            sortView.deafultImage.isHidden = true
            sortView.hightolowImage.isHidden = true
            sortView.lowtohightImage.isHidden = false
            sortView.atozImage.isHidden = true
            sortView.ztoaImage.isHidden = true
        } else if userDef .value(forKey: "nameAZSort") as! Bool == true {
            sortView.deafultImage.isHidden = true
            sortView.hightolowImage.isHidden = true
            sortView.lowtohightImage.isHidden = true
            sortView.atozImage.isHidden = false
            sortView.ztoaImage.isHidden = true
        } else if userDef .value(forKey: "nameZASort") as! Bool == true {
            sortView.deafultImage.isHidden = true
            sortView.hightolowImage.isHidden = true
            sortView.lowtohightImage.isHidden = true
            sortView.atozImage.isHidden = true
            sortView.ztoaImage.isHidden = false
        }
        
        sortView.didTapOnDefault = { [weak self] (view) in
            userDef.set(true , forKey: "defaultSort")
            userDef.set(false , forKey: "amountHLSort")
            userDef.set(false , forKey: "amountLHSort")
            userDef.set(false , forKey: "nameAZSort")
            userDef.set(false , forKey: "nameZASort")
                
            sortView.deafultImage.isHidden = false
            sortView.hightolowImage.isHidden = true
            sortView.lowtohightImage.isHidden = true
            sortView.atozImage.isHidden = true
            sortView.ztoaImage.isHidden = true
            
            view.hide()
            DispatchQueue.main.asyncAfter(deadline: .now()+0.4, execute: {
                self?._sortByDefault()
               
            })
        }
        
        sortView.didTapOnHighToLow = { [weak self] (view) in
            userDef.set(false , forKey: "defaultSort")
            userDef.set(true , forKey: "amountHLSort")
            userDef.set(false , forKey: "amountLHSort")
            userDef.set(false , forKey: "nameAZSort")
            userDef.set(false , forKey: "nameZASort")
            
            sortView.hightolowImage.isHidden = false
            sortView.deafultImage.isHidden = true
            sortView.lowtohightImage.isHidden = true
            sortView.atozImage.isHidden = true
            sortView.ztoaImage.isHidden = true
            
            view.hide()
            DispatchQueue.main.asyncAfter(deadline: .now()+0.4, execute: {
                self?._sortByAmount(assending: false)
               
            })
        }
        
        sortView.didTapOnLowToHigh = { [weak self] (view) in
            userDef.set(false , forKey: "defaultSort")
            userDef.set(false , forKey: "amountHLSort")
            userDef.set(true , forKey: "amountLHSort")
            userDef.set(false , forKey: "nameAZSort")
            userDef.set(false , forKey: "nameZASort")
            
            sortView.lowtohightImage.isHidden = false
            sortView.deafultImage.isHidden = true
            sortView.hightolowImage.isHidden = true
            sortView.atozImage.isHidden = true
            sortView.ztoaImage.isHidden = true
            
            view.hide()
            DispatchQueue.main.asyncAfter(deadline: .now()+0.4, execute: {
                self?._sortByAmount(assending: true)
              
            })
        }
        
        sortView.didTapOnAtoZ = { [weak self] (view) in
            userDef.set(false , forKey: "defaultSort")
            userDef.set(false , forKey: "amountHLSort")
            userDef.set(false , forKey: "amountLHSort")
            userDef.set(true , forKey: "nameAZSort")
            userDef.set(false , forKey: "nameZASort")
            
            sortView.atozImage.isHidden = false
            sortView.deafultImage.isHidden = true
            sortView.hightolowImage.isHidden = true
            sortView.lowtohightImage.isHidden = true
            sortView.ztoaImage.isHidden = true
            
            view.hide()
            DispatchQueue.main.asyncAfter(deadline: .now()+0.4, execute: {
                self?._sortByName(assending: true)
              
            })
        }
        
        sortView.didTapOnZtoA = { [weak self] (view) in
            userDef.set(false , forKey: "defaultSort")
            userDef.set(false , forKey: "amountHLSort")
            userDef.set(false , forKey: "amountLHSort")
            userDef.set(false , forKey: "nameAZSort")
            userDef.set(true , forKey: "nameZASort")
            
            sortView.ztoaImage.isHidden = false
            sortView.deafultImage.isHidden = true
            sortView.hightolowImage.isHidden = true
            sortView.lowtohightImage.isHidden = true
            sortView.atozImage.isHidden = true
            
            view.hide()
            DispatchQueue.main.asyncAfter(deadline: .now()+0.4, execute: {
                self?._sortByName(assending: false)
               
            })
        }
        if let superview = superView {
            sortView.show(On: superview)
        }
         
    } 
    
    private func _canShowFilter() ->Bool {
        
        guard let dataStore = _dataStore else { return false }
        guard dataStore.result.count == 0 else { return true }
        
        let lastAction = dataStore.lastAction
       // #imageLiteral(resourceName: "dashboard_other_number.png")
        if lastAction == .filter {
            
            if dataStore.originalData.count > 0 {
                return true
            }
            return false
        }
        
        return false
    }
    
    func _showFilterView() {        
        if _canShowFilter() == false { return }
        guard let dataStore = _dataStore else { return }
        var superView = self.navigationController?.view
        
        guard let filterView = PendingRequestFilterView.newInstance() else { return }
        if superView == nil, let nav = navigationObject {
            superView = nav.view
        }
        
        filterView.didTapOnDefault = { [weak self] (view) in
            filterView.defaultImageView.isHidden = false
        
            self?._filterByDefault()
            view.hide()
        }
        
        filterView.didTapOnType = { [weak self] (view, type) in
           
            self?._filterBy(type: type)
            view.hide()
        }
        
        filterView.didTapOnCategorie = { [weak self] (view, categorie) in
            
            self?._filterBy(categorie: categorie)
            view.hide()
        }
      
            if let superview = superView {
                filterView.set(types: dataStore.allAvailableTypes).set(categories: dataStore.allCategoryTypes).show(On: superview)
            }
    }
    
    private func _filterByDefault() {
        
        let isAnyChange = _dataStore?.set(filter: .default) ?? false
        
        if  isAnyChange == true && (self._dataStore?.result.count)! > 0
       {
                          DispatchQueue.main.async {

            self.requestItemsTableView?.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
                let indexPath = IndexPath(row: 0, section: 0)
                self.requestItemsTableView?.scrollToRow(at: indexPath, at: .top, animated: true)
            }
                          
        } else {
                      
            self.requestItemsTableView?.reloadData()
        }
        
//        if isAnyChange == true {
//            _reloadTableViewWithSuffleAnimation()
//        }
    }
    
    private func _filterBy(type :String) {
        
        let isAnyChange = _dataStore?.set(filter: .type(.string(type))) ?? false
        
        if  isAnyChange == true && (self._dataStore?.result.count)! > 0
              {
                                 DispatchQueue.main.async {

                   self.requestItemsTableView?.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
                       let indexPath = IndexPath(row: 0, section: 0)
                       self.requestItemsTableView?.scrollToRow(at: indexPath, at: .top, animated: true)
                   }
                                 
               } else {
                             
                   self.requestItemsTableView?.reloadData()
               }
        
//        if isAnyChange == true {
//            _reloadTableViewWithSuffleAnimation()
//        }
    }
    
    private func _filterBy(categorie :String) {
        
        let isAnyChange = _dataStore?.set(filter: .categories(.string(categorie))) ?? false
        
        if  isAnyChange == true && (self._dataStore?.result.count)! > 0
              {
                                 DispatchQueue.main.async {

                   self.requestItemsTableView?.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
                       let indexPath = IndexPath(row: 0, section: 0)
                       self.requestItemsTableView?.scrollToRow(at: indexPath, at: .top, animated: true)
                   }
                                 
               } else {
                             
                   self.requestItemsTableView?.reloadData()
               }
        
//        if isAnyChange == true {
//            _reloadTableViewWithSuffleAnimation()
//        }
    }
    
    private func _didPickDate(date: Date?) {
        
//        let isAnyChange = _dataStore?.set(date: date)  ?? false
//
//        if isAnyChange == true {
//            _reloadTableViewWithSuffleAnimation()
//        }
    }
    
    private func _fromDidPickDate(date: Date?) {
        
//        let isAnyChange = _dataStore?.set(date: date)  ?? false
//
//        if isAnyChange == true {
            //_reloadTableViewWithSuffleAnimation()
//        }
    }
    
    
    func _showDatePicker() {
        
//        let overlayView = _overlayView
//       // let textField = _dummyTextField
//
//        overlayView?.alpha = 0.0
//        overlayView?.isHidden = false
       // textField?.becomeFirstResponder()
        
        UIView.animate(withDuration: 0.1) {
            //overlayView?.alpha = 0.3
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "RequestMoneyDateSelectViewController_ID") as? RequestMoneyDateSelectViewController {
//                vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//                vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                vc.delegate = self
                if let navigation = self.navigationController {
                    navigation.pushViewController(vc, animated: true)
                } else {
                    if let nav = self.navigationObject {
                        nav.pushViewController(vc, animated: true)
                    }
                }
                //self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    func _hidePicker() {
        
        let overlayView = _overlayView
//        let textField = _dummyTextField
//        
//        textField?.resignFirstResponder()
        
        UIView.animate(withDuration: 0.3, animations: {
            overlayView?.alpha = 0.0
        }) { (flag) in
            overlayView?.isHidden = true
        }
    }
    
    private func _reloadTableViewWithSuffleAnimation() {
        
        guard let tableView = requestItemsTableView else { return }
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [.curveEaseIn], animations: {
            
            tableView.alpha = 0.0
        }) { (flag) in
            
            if let _ = tableView.indexPathsForVisibleRows?.first {
                
                let ip = IndexPath(row: 0, section: 0)
                tableView.selectRow(at: ip, animated: false, scrollPosition: UITableView.ScrollPosition.top)
            }
            let transition = CATransition()
            
            transition.type = CATransitionType.moveIn
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            transition.fillMode = CAMediaTimingFillMode.forwards
            transition.duration = 0.5
            transition.subtype = CATransitionSubtype.fromBottom
            
            tableView.layer.add(transition, forKey: "UITableViewReloadDataAnimationKey")
            tableView.reloadData()
            
            UIView.animate(withDuration: 0.6, delay: 0.0, options: [.curveEaseIn], animations: {
                
                tableView.alpha = 1.0
            }) { (flag) in
                
                
            }
        }
    }
    
    private func _sortByDefault() {
        
        let isAnyChange = _dataStore?.set(sort: .default)  ?? false
        
        if  isAnyChange == true && (self._dataStore?.result.count)! > 0
                     {
                                        DispatchQueue.main.async {

                          self.requestItemsTableView?.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
                              let indexPath = IndexPath(row: 0, section: 0)
                              self.requestItemsTableView?.scrollToRow(at: indexPath, at: .top, animated: true)
                          }
                                        
                      } else {
                                    
                          self.requestItemsTableView?.reloadData()
                      }
        
//        if isAnyChange == true {
//            _reloadTableViewWithSuffleAnimation()
//        }
    }
    
    private func _sortByAmount(assending :Bool) {
        
        var isAnyChange = true
        
        if assending == true {
            isAnyChange = _dataStore?.set(sort: .amountLowToHigh) ?? false
        } else {
            isAnyChange = _dataStore?.set(sort: .amountHightoLow) ?? false
        }
        
        if  isAnyChange == true && (self._dataStore?.result.count)! > 0
                     {
                                        DispatchQueue.main.async {

                          self.requestItemsTableView?.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
                              let indexPath = IndexPath(row: 0, section: 0)
                              self.requestItemsTableView?.scrollToRow(at: indexPath, at: .top, animated: true)
                          }
                                        
                      } else {
                                    
                          self.requestItemsTableView?.reloadData()
                      }
        
//        if isAnyChange == true {
//            _reloadTableViewWithSuffleAnimation()
//        }
    }
    
    private func _sortByName(assending :Bool) {
        
        var isAnyChange = true
        
        if assending == true {
            isAnyChange = _dataStore?.set(sort: .nameAToZ) ?? false
        } else {
            isAnyChange = _dataStore?.set(sort: .nameZToA) ?? false
        }
        
        
        if  isAnyChange == true && (self._dataStore?.result.count)! > 0
                  
        {
                
               // print("sort value pending Approval is======\(String(describing: self._dataStore?.result.count))")

                DispatchQueue.main.async {

                self.requestItemsTableView?.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
                       // let indexPath = IndexPath(row: 0, section: 0)
                       // self.requestItemsTableView?.scrollToRow(at: indexPath, at: .top, animated: true)
                }
                                        
            } else {
                                    
                self.requestItemsTableView?.reloadData()
            }
        
//        if isAnyChange == true {
//            _reloadTableViewWithSuffleAnimation()
//        }
    }
    
    
}

//MARK: - Table view extension
extension PendingApprovalRequestVC : UITableViewDelegate,UITableViewDataSource {
   
    private func _showNoRecordsView() {
        
       // noRecordsFoundView?.isHidden = false
        self.noRecordsLabel?.isHidden = false

    }
    
    private func _hideNoRecordsView() {
        
       // noRecordsFoundView?.isHidden = true
        self.noRecordsLabel?.isHidden = true

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = _dataStore?.result.count ?? 0
        
        if count == 0 {
            
            _showNoRecordsView()
        } else {
            _hideNoRecordsView()
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let collection = _dataStore?.result else { return UITableViewCell() }
        
        ///
        let cell = tableView.dequeueReusableCell(withIdentifier: "requestTypeCell", for: indexPath as IndexPath) as! CustomPendingRequestCellTableViewCell
        let dataModel = collection[indexPath.row]
        
        ///
        _bind(view: cell, withModel: dataModel)
        _addAttachementDetails(On: cell.lblAttachedFile, attachements: dataModel.AttachmentPath, forIndexPath: indexPath)
        if let status = dataModel.Status, status != 0 {
            cell.imgUserLogo.contentMode = UIView.ContentMode.scaleAspectFill
            cell.imgUserLogo.setRounded()
            cell.imgUserLogo.image = UIImage(named : "avatar")
        } else {
      
            if let profileImageUrl = dataModel.ProfileImage {
                _addImage(On: cell.imgUserLogo, fromURLString: profileImageUrl, forIndexPath: indexPath)
                      println_debug("Display Images \(profileImageUrl)")
            } else {
                cell.imgUserLogo.contentMode = UIView.ContentMode.scaleAspectFill
                cell.imgUserLogo.setRounded()
                cell.imgUserLogo.image = UIImage(named : "avatar")
                //println_debug("Not Display Images \(dataModel.ProfileImage)")
            }
        }
        //_addImage(On: cell.imgUserLogo, fromURLString: dataModel.ProfileImage, forIndexPath: indexPath)
        _updateVisibility(OfChildComponents: cell, withModel: dataModel)
        ///
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    private func _bind(view : CustomPendingRequestCellTableViewCell, withModel model :RequestMoneyItem) {
        
        ///
        let APIDateFormater = _APIDateFormater
        let presentationDateFormater = _presentationDateFormater
        
        view.lblCustomerName.text = model.AgentName?.localized
        
        if model.CategoryName?.localized == "Merchant".localized{
            //view.lblCategory.font = UIFont(name: appFont, size: appFontSize)
            view.lblCategory.text = "Safety Casheir".localized
        }
        else{
            //view.lblCategory.font = UIFont(name: appFont, size: appFontSize)
            view.lblCategory.text = model.CategoryName?.localized
        }
       
        if(model.LocalTransactionType == "REQUESTMONEY")
        {
            //view.lblRequestTYpe.font = UIFont(name: appFont, size: appFontSize)
            view.lblRequestTYpe.text = "OK$ Money".localized //model.CommanTransactionType?.localized
            view.remindButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            view.remindButton.setTitle("Pay".localized, for: .normal)
        }
       else  if(model.LocalTransactionType == "TOPUP")
        {
            //view.lblRequestTYpe.font = UIFont(name: appFont, size: appFontSize)
            view.lblRequestTYpe.text = "Top-Up Plan".localized //model.CommanTransactionType?.localized
            view.remindButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            view.remindButton.setTitle("Top-Up Plan".localized,for: .normal)
        }
      else  if(model.LocalTransactionType == "DATAPLAN")
        {
            //view.lblRequestTYpe.font = UIFont(name: appFont, size: appFontSize)
            view.lblRequestTYpe.text = "Data Plan".localized //model.CommanTransactionType?.localized
            view.remindButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            view.remindButton.setTitle("Data Plan".localized,for: .normal)
        }
        else
        {
            //view.lblRequestTYpe.font = UIFont(name: appFont, size: appFontSize)
            view.lblRequestTYpe.text = "Special Offer".localized//model.LocalTransactionType
            view.remindButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            view.remindButton.setTitle("Special Offer".localized,for: .normal)
        }
        
        //view.lblRequestTYpe.text = "OK$ Money".localized //model.CommanTransactionType?.localized
        //view.lblType.text = model.LocalTransactionType?.localized
        if let sourceNo = model.Source {
            if sourceNo.starts(with: "0095") {
                view.lblMobileNumber.text = "0" + sourceNo.substring(from: 4)
            } else {
                view.lblMobileNumber.text = sourceNo
            }
        } else {
            view.lblMobileNumber.text = model.Source
        }
        
        if let amount = model.Amount {
            
           // view.lblAmount.text = String.localizedStringWithFormat("%g", amount) + ".00" + GlobalConstants.Strings.mmk
            
            print("model.Amount----\(amount)")
            
            let str:String = String(format:"%f", amount)
            
            print("model.Amount string----\(str)")
            
            view.lblAmount.text = self.getDigitDisplaynew(str) + GlobalConstants.Strings.mmk
            
           // view.lblAmount.text = String.localizedStringWithFormat("%g", amount) + GlobalConstants.Strings.mmk
        }
        
        /// convert "2018-01-09T19:18:54.87" to Tue, 09-Jan-2018
        guard let modelDateString = model.Date,
            let date = APIDateFormater.date(from: modelDateString)  else {
                
                view.lblDate.text = nil
                return
        }
        
        view.lblDate.text = presentationDateFormater.string(from: date)
    }
    
    func getDigitDisplaynew(_ digit: String) -> String {
        
        print("getDigitDisplaynew---\(digit)")
        var FormateStr: String
        let mystring = digit.replacingOccurrences(of: ",", with: "")
        let number = NSDecimalNumber(string: mystring)
        
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        FormateStr = formatter.string(from: number)!
        if FormateStr == "NaN"{
            FormateStr = ""
        }
        return FormateStr
    }
    
    private func _addImage(On imageView :UIImageView?, fromURLString urlString :String?, forIndexPath indexPath :IndexPath) {
        
        if let imageView = imageView, let imageStringUrl = urlString, let url = URL(string: imageStringUrl) {
            imageView.contentMode = UIView.ContentMode.scaleAspectFill
            imageView.setRounded()
            LazyImageLoader.showImage(On: imageView, fromURL: url, showActivityIndicator: true, indicatorStyle: .gray)
        } else {
            if let imageView = imageView, let imageStringUrl = urlString?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed), let url = URL(string: imageStringUrl) {
                imageView.contentMode = UIView.ContentMode.scaleAspectFill
                imageView.setRounded()
                LazyImageLoader.showImage(On: imageView, fromURL: url, showActivityIndicator: true, indicatorStyle: .gray)
            }
        }
    }
    
    private func _addAttachementDetails(On label :UILabel?, attachements :[String]?, forIndexPath indexPath :IndexPath) {
        
        /// check weather label is nil
        guard let label = label else { return }
        
        let filesCount = attachements?.count ?? 0 // attachements count
        let action = #selector(showAttachedFiles(_:)) // action for when user tap on label
        
        /// configure gester
        if let gester = label.gestureRecognizers?.first as? TapGestureRecognizer { /// if gester already added then remove the existing target, action and add new one.
            
            gester.indexPath = indexPath
        } else { /// if gester not yet added then add a new gester.
            
            let gester = TapGestureRecognizer(target: self, action: action)
            gester.indexPath = indexPath
            label.addGestureRecognizer(gester)
        }
        
        /// update the UI attachenge status on label
        if filesCount > 0 {
            //label.font = UIFont(name: appFont, size: appFontSize)
            label.text = "Yes        ".localized
            label.isUserInteractionEnabled = true
        } else {
            //label.font = UIFont(name: appFont, size: appFontSize)
            label.text = "No        ".localized
            label.isUserInteractionEnabled = false
        }
    }
    
    private func _updateVisibility(OfChildComponents view :CustomPendingRequestCellTableViewCell, withModel model :RequestMoneyItem) {
        
        ///
        func doForPendingApprovelTab() {
            view.bottomStackView.isHidden = false
            view.blockUnBlock.isHidden = false
            view.statusLabel.isHidden = true
            view.blockLabel.isHidden = false
            
            print("model.IsBlocked!-----\(model.IsBlocked!)")
            if model.IsBlocked! {

                //Now its Block stage // Show image of Unblock
                view.blockLabel.font = UIFont(name: appFont, size: appFontSize)
                view.blockLabel.text = "UnBlock".localized
                view.blockLabel.textAlignment = .left
                view.blockLabel.textColor = UIColor.white
                view.blockUnBlock.isSelected = true
                
                
                if let remarks = model.Remarks, remarks.count > 0 {
                    view.lblRemarks.font = UIFont(name: appFont, size: appFontSize)
                    view.lblRemarks.text = remarks
                } else {
                    view.remarksStackView.isHidden = true
                }
                view.cancelButton.isHidden = true
                view.cancelButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
                view.cancelButton.setTitle("Cancel".localized, for: UIControl.State.normal)
                
                
                     if(model.LocalTransactionType == "REQUESTMONEY")
                      {
                         view.remindButton.isHidden = true
                         view.remindButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
                         view.remindButton.setTitle("Pay".localized, for: UIControl.State.normal)
                      }
                     else  if(model.LocalTransactionType == "TOPUP")
                      {
                          view.remindButton.isHidden = true
                           view.remindButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
                          view.remindButton.setTitle("Top-Up Plan".localized, for: UIControl.State.normal)
                      }
                     else  if(model.LocalTransactionType == "DATAPLAN")
                      {
                          view.remindButton.isHidden = true
                          view.remindButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
                          view.remindButton.setTitle("Data Plan".localized, for: UIControl.State.normal)
                      }
                      else
                      {
                          view.remindButton.isHidden = true
                          view.remindButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
                          view.remindButton.setTitle("Special Offer".localized, for: UIControl.State.normal)
                         
                      }
                
                
                
            } else {
                //view.blockLabel.font = UIFont(name: appFont, size: appFontSize)

                view.blockLabel.text = "Block".localized
                view.blockLabel.textAlignment = .right
                view.blockLabel.textColor = UIColor.black
                view.blockUnBlock.isSelected = false
                
                
                if let remarks = model.Remarks, remarks.count > 0 {
                    view.lblRemarks.font = UIFont(name: appFont, size: appFontSize)
                    view.lblRemarks.text = remarks
                } else {
                    view.remarksStackView.isHidden = true
                }
                view.cancelButton.isHidden = false
                view.cancelButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
                view.cancelButton.setTitle("Cancel".localized, for: UIControl.State.normal)
//                view.remindButton.isHidden = false
//                view.remindButton.setTitle("Pay".localized, for: UIControl.State.normal)
                
                if(model.LocalTransactionType == "REQUESTMONEY")
                {
                     view.remindButton.isHidden = false
                     view.remindButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
                     view.remindButton.setTitle("Pay".localized, for: UIControl.State.normal)
                 }
                 else  if(model.LocalTransactionType == "TOPUP")
                  {
                    view.remindButton.isHidden = false
                    view.remindButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
                    view.remindButton.setTitle("Top-Up Plan".localized, for: UIControl.State.normal)
                  }
                 else  if(model.LocalTransactionType == "DATAPLAN")
                  {
                   view.remindButton.isHidden = false
                   view.remindButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
                   view.remindButton.setTitle("Data Plan".localized, for: UIControl.State.normal)
                   }
                  else
                   {
                      view.remindButton.isHidden = false
                      view.remindButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
                      view.remindButton.setTitle("Special Offer".localized, for: UIControl.State.normal)
                   }
                
            }
            
        }
        
        func doForRequestApprovedTab() {
            if let remarks = model.Remarks, remarks.count > 0 {
                view.lblRemarks.font = UIFont(name: appFont, size: appFontSize)
                view.lblRemarks.text = remarks
            } else {
                view.remarksStackView.isHidden = true
            }
            view.blockLabel.isHidden = true
            view.blockUnBlock.isHidden = true
            view.statusLabel.isHidden = false
            //view.statusLabel.font = UIFont(name: appFont, size: appFontSize)
            view.statusLabel.text = "Success".localized
            view.remindButton.isHidden = true
            view.bottomStackView.isHidden = true
            view.lblReminderCount.text = ""
            view.statusLabel.textColor = UIColor.init(red: 63/255, green: 167/255, blue: 12/255, alpha: 1)
        }
        
        func doForCancelledTab() {
            if let remarks = model.Remarks, remarks.count > 0 {
                view.lblRemarks.text = remarks
            } else {
                view.remarksStackView.isHidden = true
            }
            view.blockLabel.isHidden = true
            view.blockUnBlock.isHidden = true
            view.statusLabel.isHidden = false
            //view.statusLabel.font = UIFont(name: appFont, size: appFontSize)
            view.statusLabel.text = "Cancelled".localized
            view.remindButton.isHidden = true
            view.lblReminderCount.text = ""
            view.bottomStackView.isHidden = false
            view.cancelButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            view.cancelButton.setTitle("Undo Payment Request".localized, for: UIControl.State.normal)
            view.statusLabel.textColor = UIColor.init(red: 232/255, green: 76/255, blue: 7/255, alpha: 1)
        }
        
        func doForAllTab() {
            if model.Status == 0 {
               doForPendingApprovelTab()
            } else if model.Status == 1 {
                doForRequestApprovedTab()
            } else {
                doForCancelledTab()
            }
        }
       
        switch self.selectedItemIndex {
        case 1 :
             doForPendingApprovelTab()
            
            break
        case 2 :
             doForRequestApprovedTab()
            break
        case 3 :
             doForCancelledTab()
            break
        case 4 :
            //cell.blockUnBlock.isHidden = false
             doForAllTab()
            break
            
        default :
            doForAllTab()
            break
        }
    }
    
    
    @objc func inboxslection(notification: NSNotification) {
    //   let sideMenu = ["All", "Pending Approval", "Request Approved", "Cancelled"]
   
       let title = notification.userInfo?["Title"] as? Int
        selectedItemIndex = title ?? 0
        
        switch self.selectedItemIndex {
               case 1 :
                   selectedItemIndex = 4
                   break
               case 2 :
                   selectedItemIndex = 1
                   break
               case 3 :
                   selectedItemIndex = 2
                   break
               case 4 :
                   //cell.blockUnBlock.isHidden = false
                   selectedItemIndex = 3
                   break
                   
               default :
                   selectedItemIndex = 4
                   break
               }
        
        print("inboxslection value-----\(selectedItemIndex)")
        self._fetchAll()
    
    }
   
    
    private func _getPaginationImages(atInexPath indexPath :IndexPath) ->[ImagePaginationVC.Image]? {
        
        /// check weather the attached image paths array contains data
        guard let attachements = _dataStore?.result[indexPath.row].AttachmentPath, attachements.count > 0 else {
            return nil
        }
        
        var collection = [ImagePaginationVC.Image]()
        
        /// create array of attachable images from image paths
        for attachement in attachements {
            
            let item = ImagePaginationVC.Image(source: attachement)
            collection.append(item)
        }
        
        return collection
    }
    
    /// This methode will call when the user tap on the attachment label if the label showing "Yes".
    ///
    /// - Parameter sender: TapGestureRecognizer
    @objc fileprivate func showAttachedFiles(_ sender: TapGestureRecognizer) {
        attachmentViewTap = true
        /// check weather the indexPath is nil
        guard let indexPath = sender.indexPath else { return }
        
//        /// get constucted params for ImagePaginationVC
//        let images = _getPaginationImages(atInexPath: indexPath)
//
//        /// create new instance of ImagePaginationVC then set the params, pre-configurations and show on self.
//        ImagePaginationVC.newInstance()?.set(source: images).set(backgroundColor: UIColor.black).set(placeholder: UIImage(named: "")).show(On: self)

        
        guard let attachements = _dataStore?.result[indexPath.row].AttachmentPath, attachements.count > 0 else {
            showToast(message: "No Files Attached")
            return
        }
        let vc = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PTGalleryViewController") as? PTGalleryViewController
        vc?.arrayImages = attachements
        vc?.reqMoney = "Request Money"
        if let nav = self.navigationController {
            nav.navigationBar.barTintColor        = kYellowColor
            nav.navigationBar.tintColor           = UIColor.white
            nav.pushViewController(vc!, animated: true)
        } else {
            if let nav = self.navigationObject {
                nav.navigationBar.barTintColor        = kYellowColor
                nav.navigationBar.tintColor           = UIColor.white
                nav.pushViewController(vc!, animated: true)
            }
        }
    }
}

func paymentAuthorisationSuccess(withModule moduleData: [String : Any]) {
    
    
}




extension PendingApprovalRequestVC: BioMetricLoginDelegate, PTWebResponseDelegate{
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String)
    {
        println_debug("Authorised")
        if(isSuccessful)
        {
            if screen == "PayToRequestMoney"
            {
                if appDelegate.checkNetworkAvail() {
                    println_debug("Single Payment Initiated")
                    let web = PayToWebApi()
                    web.delegate = self
                    
                    //guard let moduleName = requestModuleDataToPay["module_name"] as? String,
                        let cellIndex = (requestModuleDataToPay["row_number"] as? IndexPath)?.row
                        //moduleName == "PayTo"  else { moduleName = "PayTo" }
                    
                    
                    guard let dataModel = _dataStore?.result[cellIndex ?? 0] else { return }
                    print("biometricAuthentication PayToRequestMoney====\(dataModel)")
                    let source =  dataModel.Source!
                    var ip = ""
                    if let ipAdd = OKBaseController.getIPAddress() {
                        ip = ipAdd
                    }
                    
                    var amount = ""
                    if let amountVal = dataModel.Amount
                    {
                        amount = String(describing: amountVal).replacingOccurrences(of: ",", with: "")
                    }
                    
                    let urlString = String.init(format: PTHelper.confirmPay, UserModel.shared.mobileNo,source,amount,ok_password ?? "","IPAY",ip,"ios","PAYTO","GPRS",UserLogin.shared.token)
                    print("ALL URL tableViewCellDidTapRemind===== \(urlString)")
                    let url = URL.init(string: urlString)
                    let pRam = Dictionary<String,String>()
                    web.genericClassXML(url: url!, param: pRam as AnyObject, httpMethod: "POST", mScreen: "ApproveRequestMoney")
                    
                }
            }
            else if screen == "CancelPaymentReq"
            {
                guard let moduleName = requestCancelData["module_name"] as? String,
                    let statusValue = requestCancelData["status"] as? String,
                    let tappedIndexPath = (requestCancelData["row_number"] as? IndexPath)?.row,
                    moduleName == "CancelPaymentReq"  else { return }
                self.updateRequestWithStatus(indexRow: tappedIndexPath, status: statusValue)
            }
            else {
                guard let moduleName = requestModuleData["module_name"] as? String,
                    let isNeedToBeBlock = requestModuleData["isNeedToBeBlock"] as? Bool,
                    let cellIndex = (requestModuleData["row_number"] as? IndexPath)?.row,
                    moduleName == kBlockOrUnblock  else { return }
                
                ///
                blockUnblockRequest(indexRow: cellIndex, isNeedToBeBlock: isNeedToBeBlock)
            }
        }
        else
        {
            println_debug("User not authorized")
            
        }
    }
    
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            transaction[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }

    
    func webFailureResult(screen: String) {
        
    }
    
    
    func webSuccessResult(data: Any, screen: String) {
        
        //MARK:- ConfirmationMovement
        if screen == "ApproveRequestMoney"
        {
            DispatchQueue.main.async {
                if let xmlString = data as? String {
//                    guard let moduleName = self.requestModuleDataToPay["module_name"] as? String,
                        let cellIndex = (self.requestModuleDataToPay["row_number"] as? IndexPath)?.row
//                        moduleName == "PayTo"  else { return }
                    
                    guard let dataModel = self._dataStore?.result[cellIndex ?? 0] else { return }
                    
                   // print("webSuccessResult ApproveRequestMoney===\(dataModel)")
                        // print("webSuccessResult ApproveRequestMoneyCommanTransactionType===\(dataModel.CommanTransactionType)")

                    let xml = SWXMLHash.parse(xmlString)
                    self.transaction.removeAll()
                    self.enumerate(indexer: xml)
                    self.processedTransaction.removeAll()
                    
                   if self.agentType == "SUBSBR"{
                        self.transaction["businessName"] = ""
                    }else{
                        self.transaction["businessName"] = self.businessNameNew
                    }
                    
                   // self.transaction["businessName"] = ""
                    self.transaction["destinationName"]  = dataModel.AgentName ?? ""
                    self.transaction["localRemark"] = "Request Money"
                    self.transaction["requestId"] = dataModel.RequestId ?? ""
                    self.processedTransaction.append(self.transaction)
                    if (self.transaction["resultdescription"] as? String)?.lowercased() == "Transaction Successful".lowercased() {
                        DispatchQueue.main.async {
                            self.attachmentViewTap = true
                            
                            let transactiontype = dataModel.CommanTransactionType
                            
                            if transactiontype == "PAYTO"
                            {
                                let vc =  UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PayToTransactionConfirmationVC") as? PayToTransactionConfirmationVC
                                    vc?.displayResponse = self.processedTransaction
                                    vc?.agentTypeNew = self.agentType
                                    vc?.requestMoney = true
                                
                                if let nav = self.navigationController {
                                    nav.pushViewController(vc!, animated: true)
                                    return
                                }
                                if let navBack = self.navigationObject {
                                    navBack.pushViewController(vc!, animated: true)
                                    return
                                }
                            }
                            else
                            {
                                print("Recharge confirmation======")

                           /*     let number = dataModel.Source ?? ""
                                    let operatorName = MyNumberTopup.opName
                                    let type     = RechargePlanType.recharge
                                let cashback = (self.kickBackInfoDictionary.safeValueForKey("kickback") as? String) == nil ? "" : (self.kickBackInfoDictionary.safeValueForKey("kickback") as? String)
                                let amount   = dataModel.Amount ?? 0.0

                                let model = TopupConfirmationModel.init("Unknown", number: number, opeName: operatorName, type: type, cashbk: cashback ?? "", topAmt: "\(amount)", payNumber: number)
                                    self.arrayKickBackInfo.append(model)
                                    // PaymentVerificationManager.storeTransactions(model: PaymentTransactions.init(number ?? "", type: "Top-Up Plan", name: "", amount: amount ?? "", opName: operatorName))
                                //  let vc = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TopupConfirmationViewController.self)) as? TopupConfirmationViewController

                                let vc =  UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: "TopupConfirmationViewController") as? TopupConfirmationViewController

                                    vc?.confirmResponseModel = self.arrayKickBackInfo
                                    vc?.type = "Top-Up Plan"
                                    vc?.screenType = ConfirmScreenType.mynumber

                                if let nav = self.navigationController {
                                  nav.pushViewController(vc!, animated: true)
                                  return
                                }
                                 if let navBack = self.navigationObject {
                                    navBack.pushViewController(vc!, animated: true)
                                     return
                                } */
                                
                                self.multiTopupSimCall(amount:dataModel.Amount ?? 0.0, number:dataModel.Source ?? "", planType: dataModel.LocalTransactionType ?? "")
                            
                            }
                          

                        }
                    }
                }
            }
            println_debug(data)
        }
    }
}


//MARK: - Get all received Requests
extension PendingApprovalRequestVC {
    
    func getAllDateSortedList(startDate : Date, endDate : Date)
    {
        //2019-10-10 09:54:29 +0000
        //9/9/2019 5:57:06 PM
        
        let isAnyChange = _dataStore?.set(fromDate :startDate, toDate : endDate)
        
        if  isAnyChange == true && (self._dataStore?.result.count)! > 0
               {
                   DispatchQueue.main.async {

                   self.requestItemsTableView?.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
                       let indexPath = IndexPath(row: 0, section: 0)
                    self.requestItemsTableView?.scrollToRow(at: indexPath, at: .top, animated: true)
                   }
                   
        } else {
               
                   self.requestItemsTableView?.reloadData()
        }
        
//        if isAnyChange == true {
//            _reloadTableViewWithSuffleAnimation()
//        }
    }
    
    func _fetchAll() {
      //  self.refreshTableData()
         NotificationCenter.default.post(name: Notification.Name(rawValue: "ClearAndHideSearchBar"), object: nil)
        print("_fetchAll called-----\(selectedItemIndex)")
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
            self.requestItemsTableView?.reloadData()
        }
        
        var requestMoneyStatus  = RequestedMoneyStatus.ALL.rawValue
        let requestType = RequestType.ALL.rawValue
        switch selectedItemIndex {
        case 1:
            requestMoneyStatus = RequestedMoneyStatus.REQUESTED.rawValue
           
        case 2:
            requestMoneyStatus = RequestedMoneyStatus.ACCEPTED.rawValue
        case 3:
            
            requestMoneyStatus = RequestedMoneyStatus.REJECTED.rawValue
            
        case 4:
           requestMoneyStatus = RequestedMoneyStatus.ALL.rawValue
            
        default:
            requestMoneyStatus = RequestedMoneyStatus.ALL.rawValue
        }
        guard let apiForAllReceivedRequests : URL = RequestMoneyParams.getAPIForAllReceivedRequest(requestMoneyStatus: requestMoneyStatus, requestType: requestType, limit: nil, offset: nil, isRequestModule: true) else {
            return
        }
       if FromScanQR == "FromRequestMoneyApproval" {
          //
       }
       else
       {
       
        selectedItemIndex = 0
       }
        println_debug("api \(apiForAllReceivedRequests.absoluteString) ")
        
        JSONParser.GetApiResponse(apiUrl: apiForAllReceivedRequests, type: "POST") { (status : Bool, data : Any?) in
            guard status == true, let dataArray = data as? Array<Any> else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    self.requestItemsTableView?.reloadData()
                     self.refreshControl.endRefreshing()
                }
                return
            }
            
            print("dataArray------\(dataArray)")
            var collection = [RequestMoneyItem]()
            
            for case let dictionary as Dictionary<String,Any> in dataArray {
                
                collection.append(RequestMoneyItem.wrapModel(dic: dictionary))
            }
            
            if self._dataStore == nil {
                self._dataStore = PendingRequestData(datas: collection)
                self._SearchStore = PendingRequestData(datas: collection)
            } else {
                self._dataStore?.set(datas: collection)
                self._SearchStore?.set(datas: collection)
            }
            let count = self._dataStore?.result.count ?? 0
            
            DispatchQueue.main.async(execute: {
                if count >= 1 {
                   self.multiButton?.show()
                    self.delegate?.showHideSearchButton(status: true)
                    NotificationCenter.default.post(name: NSNotification.Name("inboxRightBarHide"), object: nil, userInfo: ["HideSearch": false])

                } else {
                     self.multiButton?.hide()
                    self.delegate?.showHideSearchButton(status: false)
                    NotificationCenter.default.post(name: NSNotification.Name("inboxRightBarHide"), object: nil, userInfo: ["HideSearch": true])
                    
                }
                 self.refreshControl.endRefreshing()
                self.noRecordsLabel?.font = UIFont(name: appFont, size: appFontSize)
                self.noRecordsLabel?.text = "No records found!".localized
                self.requestItemsTableView?.dataSource  = self
                self.requestItemsTableView?.delegate = self
                self._searchByName(searchText: self.searchBar?.text)
                progressViewObj.removeProgressView()
            })
        }
    } 
}

extension PendingApprovalRequestVC : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
     
      
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: SEARCHCHARSET).inverted).joined(separator: "")) { return false }
            
            if updatedText == " " || updatedText == "  "{
                return false
            }
            if (updatedText.contains("  ")){
                return false
            }
            if updatedText.count > 50 {
                return false
            }
            if updatedText != "" && text != "\n" {
                print("searchBar===\(searchText)")
                print("updatedText===\(updatedText)")
                _searchByName(searchText: searchBar.text)
            }
        }
        return true
    }
    
    
    fileprivate func _searchByName(searchText: String?) {
        print("PendingApprovalRequestVC searchBarSearchName-----")

        _dataStore?.searchByName(name: searchText)
        
//        DispatchQueue.main.async {
//            self.requestItemsTableView?.reloadData()
//        }
        
        if  let collection = self._dataStore?.result, collection.count <= 0
        {
            self.requestItemsTableView?.reloadData()
            
        } else {
            DispatchQueue.main.async {

            self.requestItemsTableView?.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
//                   let indexPath = IndexPath(row: 0, section: 0)
//                self.requestItemsTableView?.scrollToRow(at: indexPath, at: .top, animated: true)
        
        
               }
        }
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        //customSearchBar.resignFirstResponder()
        print("PendingApprovalRequestVC searchBarCancelButtonClicked-----")
        searchBar.text = ""
        self.delegate?.HideSearchView()
       // _fetchAll()
       
    }
    
    internal func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

//pending Request TableView Cell Delegate
extension PendingApprovalRequestVC : PendingRequesttableViewCellDelegate {
    
    
    func tableViewCellDidTapRemind(_ sender: CustomPendingRequestCellTableViewCell) {
        
        
        // PaymentBUtton Action
      
        guard let cell = requestItemsTableView?.indexPath(for: sender) else { return }
        
        guard let dataModel = _dataStore?.result[cell.row] else { return }
        
        var amount = ""
        if let amountVal = dataModel.Amount
        {
            amount = String(describing: amountVal).replacingOccurrences(of: ",", with: "")
        }
        
        let transactionType = dataModel.CommanTransactionType
        let walletAmount = self.walletAmount()
        let amountDouble = Double(amount) ?? 0.0
       if amountDouble > walletAmount {
           alertViewObj.wrapAlert(title: "", body: "Insufficient Balance".localized, img: UIImage(named : "confirmation_icon"))

           alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {

         })
        alertViewObj.showAlert(controller: self)
        } else {
        
        print ("Pay action required . redirect to new view controller ")
        // println_debug("Block ", sender, tappedIndexPath)
        // remindRequestAgin(indexRow: tappedIndexPath.row)
        
        //Add pay code here : Kethan
        
        alertViewObj.wrapAlert(title: "", body: "Do you want to Pay the Request?".localized, img: UIImage(named : "confirmation_icon"))
        
        alertViewObj.addAction(title: "Cancel".localized, style: AlertStyle.cancel, action: {
            
        })
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: { [weak self] in
        
            if transactionType == "PAYTO"{
             self?.requestModuleDataToPay = [
                       "module_name":"PayTo",
                       "row_number":cell,
                       ] as [String : Any]
            }
            else
            {
                self?.requestModuleDataToPay = [
                                      "module_name":"Recharge",
                                      "row_number":cell,
                                      ] as [String : Any]
            }
            
            OKPayment.main.authenticate(screenName: "PayToRequestMoney", delegate: self!)
            var mobileNo : String? = dataModel.Source
            if mobileNo!.hasPrefix("09"){
                mobileNo = "0095\((mobileNo)!.substring(from: 1))"
            }
            self?.getCategoryList(number: mobileNo!)
            //self?._showAuthentication(info: info)
        })
        alertViewObj.showAlert(controller: self)
       }
    }
    
    /// Pending Approval cancel button action
    func tabeleViewCellDidTapChangeStauts(_ sender: CustomPendingRequestCellTableViewCell, requestMoneyStatus: RequestedMoneyStatus) {
        guard let tappedIndexPath = requestItemsTableView?.indexPath(for: sender) else { return }
        // println_debug("Block ", sender, tappedIndexPath)
        let cancelMessage = sender.cancelButton.titleLabel?.text

        var statusValue : String = RequestedMoneyStatus.REQUESTED.rawValue
        if cancelMessage == "Cancel".localized || cancelMessage == "Reject".localized {
            statusValue = RequestedMoneyStatus.REJECTED.rawValue
        }
        
        var messag = ""
        
        if cancelMessage == "Cancel".localized || cancelMessage == "Reject".localized {
            messag = "Do you want to Cancel Payment Request?"
        } else {
            messag = "Are you sure you want to undo payment request?"
        }
        
        alertViewObj.wrapAlert(title: "", body: messag.localized, img: UIImage(named : "confirmation_icon"))
        
        alertViewObj.addAction(title: "Cancel".localized, style: AlertStyle.cancel, action: {
            
        })
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: { [weak self] in
            
            self?.requestCancelData = [
                "module_name":"CancelPaymentReq",
                "row_number":tappedIndexPath,
                "status" :statusValue
                ] as [String : Any]
            
            OKPayment.main.authenticate(screenName: "CancelPaymentReq", delegate: self!)
            
            //self?._showAuthentication(info: info)
        })
        alertViewObj.showAlert(controller: self)
    }
    
    func tableViewCellDidTapBlockOrUnblockRequest(_sender: CustomPendingRequestCellTableViewCell, isNeedToBeBlock: Bool) {
        /// on working
        guard let tappedIndexPath = requestItemsTableView?.indexPath(for: _sender) else { return }
        var message : String = ""
        println_debug("block or unblock")
        
        if isNeedToBeBlock {
            message = "Do you want to Block Request Money from this User?"
            //Current is UnBlock State - Default   - Showing Block Image
            //Next Action - Need to Block - Ask confirmation
        } else {
            message = "Do you want to UnBlock Request Money from this User?"
        }
        
        alertViewObj.wrapAlert(title: "", body: message.localized, img: UIImage(named : "block"))
        
        alertViewObj.addAction(title: "Cancel".localized, style: AlertStyle.cancel, action: {
            
        })
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: { [weak self] in
            
            self?.requestModuleData = [
                "module_name":kBlockOrUnblock,
                "row_number":tappedIndexPath,
                "isNeedToBeBlock" :isNeedToBeBlock
                ] as [String : Any]
            
            OKPayment.main.authenticate(screenName: kBlockOrUnblock, delegate: self!)

            //self?._showAuthentication(info: info)
        })
        alertViewObj.showAlert(controller: self)
    }
    
    private func _showAuthentication(info :[String :Any]) {
        
        /*
        let payment = PaymentAuthorisation()
        payment.delegateModule = self
        payment.requestModuleData = info
        payment.authenticate()
         */
        
    }
    
   
    
    //    func tableViewCellDidTapBlockOrUnblockRequest(_sender: CustomPendingRequestCellTableViewCell, isNeedToBeBlock: Bool) {
    //
    //        guard let tappedIndexPath = requestItemsTableView.indexPath(for: _sender) else { return }
    //        var message : String = ""
    //        println_debug("block or unblock")
    //        if isNeedToBeBlock {
    //            message = "Do you want to Block Request Money from this User?"
    //            //Current is UnBlock State - Default   - Showing Block Image
    //            //Next Action - Need to Block - Ask confirmation
    //        }else  {
    //            message = "Do you want to UnBlock Request Money from this User?"
    //        }
    //
    //        alertViewObj.wrapAlert(title: "", body: message, img: UIImage(named : "confirmation_icon"))
    //
    //        alertViewObj.addAction(title: "Cancel", style: AlertStyle.cancel, action: {
    //
    //        })
    //        alertViewObj.addAction(title: "OK", style: AlertStyle.cancel, action: {
    //
    //            self.blockUnblockRequest(indexRow: tappedIndexPath.row,isNeedToBeBlock: isNeedToBeBlock)
    //
    //
    //        })
    //        alertViewObj.showAlert(controller: self)
    //    }
    
    //MARK: - getPendingTopResult
    func blockUnblockRequest(indexRow index :Int, isNeedToBeBlock : Bool) {
        
        guard let item = _dataStore?.result[index] else { return }
        ///
        progressViewObj.showProgressView()
        guard let source = item.Source,
            let apiForBlockUnblockRequest = RequestMoneyParams.blockUnblockParticularNumber(isBlocked: isNeedToBeBlock, DesNum: source) else { return }
        
        println_debug("PendingRequestVC api \(apiForBlockUnblockRequest.absoluteString) ")
        
        JSONParser.GetCancelApiResponse(apiUrl: apiForBlockUnblockRequest, type : "GET") {  (isSuccess : Bool, data : Any?) in
            
            ///
            progressViewObj.removeProgressView()
            
            if isSuccess == true {
                DispatchQueue.main.async {
                    self._fetchAll()
                }
            } else {
                ///
                if let message = data as? String {
                    
                    DispatchQueue.main.async(execute: {
                        // work Needs to be done
                        alertViewObj.wrapAlert(title: "", body: message.localized, img: UIImage(named : "AppIcon"))
                        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                            
                        })
                        alertViewObj.showAlert(controller: self)
                    })
                }
            }
        }
    }
    
    //MARK: - Cancel the request 
    func updateRequestWithStatus(indexRow index :Int , status : String) {
        
        guard let item = _dataStore?.result[index] else { return }
        
        
        let urlStr   = Url.changeStatusOfRequest
        let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
        var urlComponents = URLComponents.init(url: ur, resolvingAgainstBaseURL: false)
        let userData = UserModel.shared
        // add params
        
        let requestId = item.RequestId ?? ""
        
        let mobileNUmberQuery       = URLQueryItem(name:"MobileNumber",value : userData.mobileNo)
        let simIdQuery              = URLQueryItem(name:"Simid",value :userData.simID)
        let msIDQuery               = URLQueryItem(name:"MSID" , value:userData.msid)
        let osTypeQuery             = URLQueryItem(name:"OSType", value: userData.osType)
        let otpQuery                = URLQueryItem(name:"OTP", value: userData.simID)
        let requestIDQuery          = URLQueryItem(name:"RequestId", value: "\(requestId)")
        let statusQuery             = URLQueryItem(name:"Status", value: "\(status)")
        
        urlComponents?.queryItems  = [mobileNUmberQuery,simIdQuery,msIDQuery,osTypeQuery,otpQuery,requestIDQuery,statusQuery]
        guard let apiForCancelRequest = urlComponents?.url else { return }

        
        println_debug("updateRequestWithStatus api \(apiForCancelRequest.absoluteString) ")
        
        progressViewObj.showProgressView()
        
        JSONParser.GetCancelApiResponse(apiUrl: apiForCancelRequest, type: "GET") { (isSuccess : Bool, data : Any?) in
            
            progressViewObj.removeProgressView()
            
            println_debug("indexRow \(index) ")
            println_debug("status \(status) ")
            println_debug("requestId \(requestId) ")
            println_debug("isSuccess \(isSuccess) ")
            println_debug("data \(data ?? "") ")
            
            if isSuccess == true {
                ///
                if let rootContent = data as? NSDictionary, let _ = rootContent["Msg"] as? String {
                    if status != "Rejected" {
                        DispatchQueue.main.async(execute: {
                            alertViewObj.wrapAlert(title: "".localized, body: "Your Payment Request Undo Successfully".localized, img: UIImage(named : "info_success"))
                            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                                self._fetchAll()
                            })
                            alertViewObj.showAlert(controller: self)
                        })
                    } else {
                    DispatchQueue.main.async(execute: {
                        alertViewObj.wrapAlert(title: "".localized, body: "Your Request Cancelled Successfully".localized, img: UIImage(named : "info_success"))
                        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                            self._fetchAll()
                        })
                        alertViewObj.showAlert(controller: self)
                    })
                    }
                }
            } else {
                ///
                if let message = data as? String {
                    
                    DispatchQueue.main.async(execute: {
                        
                        alertViewObj.wrapAlert(title: "", body: message.localized, img: UIImage(named : "AppIcon"))
                        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                            
                        })
                        alertViewObj.showAlert(controller: self)
                    })
                }
            }
        }
    }
    
    func numberRequestTVCell(_sender : CustomPendingRequestCellTableViewCell, didTapOnAttachementLabel label :UIView) {
        
        guard let indexPath = requestItemsTableView?.indexPath(for: _sender) else { return }
        //
        //        let images = _getPaginationImages(atInexPath: indexPath)
        //        ImagePaginationVC.newInstance()?.set(source: images).set(backgroundColor: UIColor.black).set(placeholder: UIImage(named: "attachement")).show(On: self)
        
        guard let attachements = _dataStore?.result[indexPath.row].AttachmentPath, attachements.count > 0 else {
            showToast(message: "No Files Attached")
            return
        }
        self.attachmentViewTap = true
        let vc = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PTGalleryViewController") as? PTGalleryViewController
        vc?.arrayImages = attachements
        vc?.reqMoney = "Request Money"
        if let nav = self.navigationController {
            nav.navigationBar.barTintColor        = kYellowColor
            nav.navigationBar.tintColor           = UIColor.white
            nav.pushViewController(vc!, animated: true)
        } else {
            if let nav = self.navigationObject {
                nav.navigationBar.barTintColor        = kYellowColor
                nav.navigationBar.tintColor           = UIColor.white
                nav.pushViewController(vc!, animated: true)
            }
        }
        
        
    }
}


extension PendingApprovalRequestVC : dateSelectForSort
{
    func okButtonAction(fromDate: Date, toDate: Date, reset: Bool) {
        self.getAllDateSortedList(startDate : fromDate, endDate : toDate)
    }
    
}


extension PendingApprovalRequestVC: SendDataDelegate {
    func sendData(text: String) {
        print("PendingApprovalRequestVC sendData called======")
        _dataStore?.searchByName(name: text)
        
        DispatchQueue.main.async {
            self.requestItemsTableView?.reloadData()
        }
    }
  
}

