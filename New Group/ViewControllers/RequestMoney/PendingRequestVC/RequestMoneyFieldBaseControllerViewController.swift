//
//  RequestMoneyFieldBaseControllerViewController.swift
//  OK
//
//  Created by palnar on 06/12/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class RequestMoneyFieldBaseControllerViewController : ButtonBarPagerTabStripViewController {
    //Properties
    //map from previous controller to this paramter
    //var configParam   =  Dictionary<String,Any>()
    var configParam : ScanObject?
    
    override func viewDidLoad() {
        
        configureTabBarView()
        super.viewDidLoad()
        
        let button1 = UIBarButtonItem(image: UIImage(named:"tabBarBack"), style: .plain, target: self, action:#selector(backButtonAction)) // action:#selector(Class.MethodName) for swift 3
        button1.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem  = button1
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "Request Money".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func backButtonAction(){
        if UserDefaults.standard.bool(forKey: "PhotoViewEnabled") {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PhotoSelectionViewEnable"), object: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
 }
    
    //MARK: - Tabbar view customisation
    
    func configureTabBarView(){
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .white
        settings.style.selectedBarBackgroundColor = .black//kYellowColor
        settings.style.buttonBarItemFont = UIFont(name: appFont, size: 14.0)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .black
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        changeCurrentIndexProgressive = { [] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool,_ index:Int) -> Void in
            
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .black
            newCell?.label.textColor = kSelectedTextColor//.red
            oldCell?.contentView.backgroundColor = .white
            
            newCell?.contentView.backgroundColor =  .white//UIColor.init(red:141/255, green: 141/255, blue: 141/255, alpha: 0.15)
        }
    } 
  
    override func viewControllersForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let storyboard = UIStoryboard(name: "requestmoney", bundle: nil)
        var controllers = [UIViewController]()
        
        if let myNumberVC = storyboard.instantiateViewController(withIdentifier: "request_money_my_number") as? MyNumberViewController {
            print("Mynumber------")
            myNumberVC.mode = .myNumber
            myNumberVC.qrCodeModelConfig = configParam
            
            controllers.append(myNumberVC)
        }
        
        if let otherNumberVC = storyboard.instantiateViewController(withIdentifier: "request_money_my_number") as? MyNumberViewController {
            print("Othernumber------")
            otherNumberVC.mode = .otherNumber
            // otherNumberVC.qrCodeModelConfig = configParam
            controllers.append(otherNumberVC)
        }
        return controllers
    }
    
//    override func viewControllersForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
//        var controllers   = [UIViewController]()
//        if let child_1 = UIStoryboard(name: "requestmoney", bundle: nil).instantiateViewController(withIdentifier: "request_money_my_number") as? MyNumberViewController{
//             child_1.qrCodeModelConfig = configParam
//            controllers.append(child_1)
//        }
//        if let child_2 = UIStoryboard(name: "requestmoney", bundle: nil).instantiateViewController(withIdentifier: "request_money_other_number") as? OtherNumberViewController {
//            child_2.qrCodeModelConfig = configParam
//            controllers.append(child_2)
//        }
//        return controllers
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
