//
//  PendingRequestFilterView.swift
//  OK
//
//  Created by palnar on 27/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class FilterViewButtonItem :UIControl {
    
    @IBOutlet weak var button :UIButton?
    
    static func newInstance() ->FilterViewButtonItem? {
        
        return FilterViewButtonItem.newInstance(xibName: "FilterButtonItemView")
    }
}

class FilterViewButtonItemGroup :UIControl {
    
    @IBOutlet weak var headerButton :UIButton!
        {
        didSet
        {
            self.headerButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.headerButton.setTitle(self.headerButton.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var dropDownArrow :UIImageView!
    @IBOutlet weak var itemListStackView :UIStackView!
    
    @IBAction func didTapperOnHeaderButton(_ sender :UIButton) {
        
        let isHidden = (sender.tag == 1) ? false : true
        let alpha :CGFloat = (isHidden == true) ? 0.0 : 1.0
        let transform = (isHidden == true) ? CGAffineTransform.identity.rotated(by: CGFloat.pi) : CGAffineTransform.identity
        
        sender.tag = (sender.tag == 0) ? 1 : 0
        
        UIView.animate(withDuration: 1.1, delay: 0.0, usingSpringWithDamping: 0.72, initialSpringVelocity: 0.7, options: [.curveEaseIn, .allowUserInteraction], animations: {
            
            for view in self.itemListStackView.subviews {
                
                view.alpha = alpha
                if view.tag != -1 {
                    view.isHidden = isHidden
                }
            }
            
            self.dropDownArrow.transform = transform
            self.layoutIfNeeded()
            
        }) { (flag) in
            
        }
    }
}

public class PendingRequestFilterView: RightSideSlidableView {
    
    public typealias SelectionAction = (PendingRequestFilterView, String) ->()
    public typealias Action = (PendingRequestFilterView) ->()
    
    @IBOutlet weak var defaultItem :FilterViewButtonItem!
    {
        didSet
        {
            for case let button as UIButton in defaultItem.subviews
            {
                button.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
                button.setTitle(button.titleLabel?.text?.localized, for: .normal)
            }
        }
    }
    @IBOutlet weak var defaultImageView: UIImageView!
    @IBOutlet weak var byTypeGroup :FilterViewButtonItemGroup!
    @IBOutlet weak var byCategoriesGroup :FilterViewButtonItemGroup!
    
    public var didTapOnDefault :Action?
    public var didTapOnType :SelectionAction?
    public var didTapOnCategorie :SelectionAction?
    
    private var _types :[String] = []
    private var _categories :[String] = []
    
    var comparestr = String()
    
    public static func newInstance() ->PendingRequestFilterView? {
        return PendingRequestFilterView.newInstance(xibName: "PendingRequestFilterView")
    }
     
    @discardableResult func set(types :[String]?) ->PendingRequestFilterView {
        
        if let types = types {
           self._types = types
        }
        
        return self
    }
    
    @discardableResult func set(categories :[String]?) ->PendingRequestFilterView {
        
        
        if let categories = categories {
            self._categories = categories
            
        }
        return self
    }
    
    public override func show(On view: UIView) {
        
        _addTypes()
        _addCategories()
     //   comparestr = "0"
     
        if userDef.object(forKey: "default") != nil {
            let boolMobileStatus = userDef .value(forKey: "default") as! Bool
            
            if boolMobileStatus == true {
                defaultImageView.isHidden = false
               
            }    else    {
                  defaultImageView.isHidden = true
               
            }
        }
        
        for  (index, type) in _types.enumerated(){
            
            let value = index * 42
           // print("index value------\(value)")
            
            if userDef.object(forKey: "defaultType\(type)") != nil {
                let boolMobileStatus = userDef .value(forKey: "defaultType\(type)") as! Bool
                
                if boolMobileStatus == true {
                    //print(" True value for assign====\(userDef.object(forKey: "defaultType\(type)"))")
                    
                    guard let stackView = byTypeGroup.itemListStackView else { return }
                    let commentImageView = UIImageView(frame: CGRect(x: Int(stackView.frame.width-110), y: value, width: 40, height: 40))
                    commentImageView.image = UIImage(named: "check_bill")!.withRenderingMode(.alwaysOriginal)
                    stackView.addSubview(commentImageView)
                    
                }    else    {
                    
                    
                }
            }
            
        
        }
        
        
        for  (index, type) in _categories.enumerated(){
            
            
            let value = index * 42
           // print("index value------\(value)")
            
            if userDef.object(forKey: "defaultcategory\(type)") != nil {
                let boolMobileStatus = userDef .value(forKey: "defaultcategory\(type)") as! Bool
                
                if boolMobileStatus == true {
                   // print(" True value for assign====\(userDef.object(forKey: "defaultcategory\(type)"))")
                    
                    
                    guard let stackView = byCategoriesGroup.itemListStackView else { return }
                    let commentImageView = UIImageView(frame: CGRect(x: Int(stackView.frame.width-110), y: value, width: 35, height: 35))
                    commentImageView.image = UIImage(named: "check_bill")!.withRenderingMode(.alwaysOriginal)
                    stackView.addSubview(commentImageView)
                    
                }    else    {
                    
                    
                }
            }
            
            
        }
        
        
     
      /*  if userDef.object(forKey: "defaultcategory0") != nil {
            let boolMobileStatus = userDef .value(forKey: "defaultcategory0") as! Bool
            
            if boolMobileStatus == true {
                guard let stackView = byCategoriesGroup.itemListStackView else { return }
                let commentImageView = UIImageView(frame: CGRect(x: stackView.frame.width-110, y: 2, width: 40, height: 40))
                commentImageView.image = UIImage(named: "check_bill")!.withRenderingMode(.alwaysOriginal)
                stackView.addSubview(commentImageView)
                
            }    else    {
                
            }
        }
        if userDef.object(forKey: "defaultcategory1") != nil {
            let boolMobileStatus = userDef .value(forKey: "defaultcategory1") as! Bool
            
            if boolMobileStatus == true {
                guard let stackView = byCategoriesGroup.itemListStackView else { return }
                let commentImageView = UIImageView(frame: CGRect(x: stackView.frame.width-110, y: 44, width: 40, height: 40))
                commentImageView.image = UIImage(named: "check_bill")!.withRenderingMode(.alwaysOriginal)
                stackView.addSubview(commentImageView)
            }    else    {
                
            }
        }
        if userDef.object(forKey: "defaultcategory2") != nil {
            let boolMobileStatus = userDef .value(forKey: "defaultcategory2") as! Bool
            
            if boolMobileStatus == true {
                guard let stackView = byCategoriesGroup.itemListStackView else { return }
                let commentImageView = UIImageView(frame: CGRect(x: stackView.frame.width-110, y: 86, width: 86, height: 40))
                commentImageView.image = UIImage(named: "check_bill")!.withRenderingMode(.alwaysOriginal)
                stackView.addSubview(commentImageView)
                
            }    else    {
                
            }
        } */
        
      
        super.show(On: view)
    }
    
    @IBAction func defaultItemTapped(_ sender :Any) {
        
        userDef.set(true , forKey: "default")

        for  name in _types{
            

            //print("name display===defaultType\(name)")

                userDef.set(false , forKey: "defaultType\(name)")
            
        }
     
     //userDef.set(false , forKey: "defaultType")
        
        for  namenew in _categories{
            
            userDef.set(false , forKey: "defaultcategory\(namenew)")
            
        }
        
        
//     userDef.set(false , forKey: "defaultcategory2")
//     userDef.set(false , forKey: "defaultcategory0")
//     userDef.set(false , forKey: "defaultcategory1")
        
        didTapOnDefault?(self)
    }
    
    @objc func didTapOnType(sender: UIButton) {

        let index = sender.tag
        let value = _types[index];
    
        userDef.set(false , forKey: "default")
        

        for  name in _types{
            

            if name == _types[index] {
                
                userDef.set(true , forKey: "defaultType\(name)")

            } else {
                
                userDef.set(false , forKey: "defaultType\(name)")

            }
        }
        
        for  namenew in _categories{
            
            userDef.set(false , forKey: "defaultcategory\(namenew)")
            
        }
        
        
//        userDef.set(false , forKey: "defaultcategory2")
//        userDef.set(false , forKey: "defaultcategory0")
//        userDef.set(false , forKey: "defaultcategory1")
     
        didTapOnType?(self, value)
    }
    
    @objc func didTapOnCategorie(sender: UIButton) {
        let index = sender.tag
        let value = _categories[index];
        
        userDef.set(false , forKey: "default")

        for  name in _categories{
            
            if name == _categories[index] {
                
                userDef.set(true , forKey: "defaultcategory\(name)")
                // print("didtapontype value----\(userDef.value(forKey: "defaultType\(name)"))")
                
            } else {
                
                userDef.set(false , forKey: "defaultcategory\(name)")
                
            }
        }
        

        for  namenew in _types{
            
            
                userDef.set(false , forKey: "defaultType\(namenew)")
            
        }
        
       /* if index == 0 {
          
         
                userDef.set(true , forKey: "defaultcategory0")
                userDef.set(false , forKey: "defaultType")
                userDef.set(false , forKey: "default")
                userDef.set(false , forKey: "defaultcategory2")
                userDef.set(false , forKey: "defaultcategory1")
        }
        else if index == 1
        {
          
            userDef.set(true , forKey: "defaultcategory1")
             userDef.set(false , forKey: "defaultcategory0")
             userDef.set(false , forKey: "defaultcategory2")
            userDef.set(false , forKey: "defaultType")
            userDef.set(false , forKey: "default")
        }
       
        else{
           
            userDef.set(true , forKey: "defaultcategory2")
            userDef.set(false , forKey: "defaultcategory0")
             userDef.set(false , forKey: "defaultcategory1")
            userDef.set(false , forKey: "defaultType")
            userDef.set(false , forKey: "default")
        } */
        
       
        didTapOnCategorie?(self, value)
    }
    
    private func _addTypes() {
        
        guard let stackView = byTypeGroup.itemListStackView else { return }
        
        print("_addTypes before sorted-----\(_types)")
        _types = _types.sorted(by: {$0 < $1})
        print("_addTypes after sorted-----\(_types)")

        
        for (index, type) in _types.enumerated() {
            
            if let item = FilterViewButtonItem.newInstance() {
             
                item.button?.tag = index
                item.button?.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
                item.button?.setTitle(type.localized, for: .normal)
                item.button?.addTarget(self, action: #selector(didTapOnType(sender:)), for: .touchUpInside)
                
                stackView.addArrangedSubview(item)
                
      
            }
        }
    }
    
    private func _addCategories() {
        
        
    guard let stackView = byCategoriesGroup.itemListStackView else { return }
        
        _categories = _categories.sorted(by: {$0 < $1})

        for (index, type) in _categories.enumerated() {
            
            if let item = FilterViewButtonItem.newInstance() {
                
                item.button?.tag = index
                item.button?.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
                item.button?.setTitle(type.localized, for: .normal)
                item.button?.addTarget(self, action: #selector(didTapOnCategorie(sender:)), for: .touchUpInside)
                stackView.addArrangedSubview(item)
            }
        }
    }
}



