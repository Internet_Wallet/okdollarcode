//
//  RightSideSlidableView.swift
//  OK
//
//  Created by palnar on 27/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

public class RightSideSlidableView: UIView {
     
    @IBOutlet weak var shadowView :UIView!
    private weak var _overlayView :UIView?
   
    private let _xVelocity = CGFloat(1000.0)
    public override func awakeFromNib() {
        
        super.awakeFromNib()
        _setupLocalization()
        _applyShadow()
        _setUpGesture()
    }
    
    public override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        super.touchesEnded(touches, with: event)
        hide()
    }
    
    private func _setupLocalization()
    {
        for case let scrollView as UIScrollView in shadowView.subviews
        {
            for case let stackView as UIStackView in scrollView.subviews
            {
                for case let sortViewButton as SortViewButtonItem in stackView.subviews
                {
                    for case let button as UIButton in sortViewButton.subviews
                    {
                        button.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
                        button.setTitle(button.titleLabel?.text?.localized, for: .normal)
                    }
                }
            }
        }
        for case let viewLocal as UIView in shadowView.subviews
        {
            for case let label as UILabel in viewLocal.subviews
            {
                label.font = UIFont(name: appFont, size: appFontSize)
                label.text = label.text?.localized
            }
        }
    }
    
    private func _applyShadow() {
        
        guard let view = shadowView else { return }
        view.clipsToBounds = false
        view.layer.shadowOffset = CGSize(width: -10, height : 0)
        view.layer.shadowRadius = 8;
        view.layer.shadowOpacity = 0.2;
    }
     
    private func _setUpGesture() {
        
        let view = self
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(PendingRequestFilterView._panning(_:)))
        
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 1
        
        view.addGestureRecognizer(panGesture)
    }
    
    @objc func _panning(_ sender :UIPanGestureRecognizer) {
        
        let xTranslation = sender.translation(in: self).x
        let velocity = sender.velocity(in: self).x
        sender.setTranslation(CGPoint.zero, in: self)
        
        switch sender.state {
        case .began:
            _panningBegan(On: self, xTranslation: xTranslation)
            
            break
            
        case .changed:
            _panningChanged(On: self, xTranslation: xTranslation)
            break
            
        case .ended:
            _panningEnded(On: self, xTranslation: xTranslation, xVelocity :velocity)
            break
        default: break
        }
    }
    
    private func _panningBegan(On view :UIView, xTranslation :CGFloat) {
        
        var frame = view.frame
        
        frame.origin = CGPoint(x: frame.origin.x + xTranslation, y: frame.origin.y)
        view.frame = frame
        
        println_debug("began")
    }
    
    private func _panningChanged(On view :UIView, xTranslation :CGFloat) {
        
        var frame = view.frame
        let minX = CGFloat(0.0)
        let maxX = frame.size.width
        let currentX = frame.origin.x
        let newX = currentX + xTranslation
        
        if newX > minX && newX < maxX {
            
            frame.origin = CGPoint(x: newX, y: frame.origin.y)
            view.frame = frame
        }
    }
    
    private func _panningEnded(On view :UIView, xTranslation :CGFloat, xVelocity :CGFloat) {
        
        var frame = view.frame
        let minX = 0
        let maxX = frame.size.width
        let currentX = frame.origin.x
        let newX = currentX + xTranslation
        
        let midX = maxX/2
        
        var moveToX = newX
        println_debug("xVelocity = \(xVelocity)")
        
         var _isClosed = false
        if xVelocity > _xVelocity {
            
            _isClosed = true
            moveToX = maxX
        } else {
            
            if newX >= midX {
                
                _isClosed = true
                moveToX = maxX
            } else {
                
                moveToX = CGFloat(minX)
            }
        }
        
        frame.origin = CGPoint(x: moveToX, y: frame.origin.y)
        _panAnimation(view: view, frame: frame, isClosed :_isClosed) { [weak self] in
            
            if _isClosed == true {
                self?.removeFromSuperview()
            }
        }
    }
    
    private func _panAnimation(view :UIView, frame :CGRect, isClosed :Bool, _ completion :(() ->())? = nil) {
        
        let speed = (isClosed == true) ? 0.8 : 1.2
        
        UIView.animate(withDuration: speed, delay: 0.0, usingSpringWithDamping: 0.72, initialSpringVelocity: 0.7, options: [.curveEaseOut, .allowUserInteraction], animations: {
            
            if isClosed == true {
                view.alpha  = 0.0
                self._overlayView?.alpha = 0.0
            }
            view.frame = frame
            
        }) { (flag) in
            completion?()
        }
    }
    
    public func show(On view :UIView) {
        
        let finalFrame = view.bounds
        let initialFrame = CGRect(x: finalFrame.size.width, y: finalFrame.origin.y, width: finalFrame.size.width, height: finalFrame.size.height)
        let overlayView = UIView(frame: finalFrame)
        
        overlayView.backgroundColor = UIColor.black
        overlayView.alpha = 0.0
        view.addSubview(overlayView)
        
        self.alpha = 0.6
        self.frame = initialFrame
        view.addSubview(self)
        
        UIView.animate(withDuration: 1.2, delay: 0.0, usingSpringWithDamping: 0.72, initialSpringVelocity: 0.7, options: [.curveEaseIn, .allowUserInteraction], animations: {
            
            self.alpha = 1.0
            overlayView.alpha  = 0.3
            self.frame = finalFrame
            
        }) { (flag) in
            
        }
        
        self._overlayView = overlayView
    }
    
    public func hide() {
        
        let initialFrame = self.frame
        let finalFrame = CGRect(x: initialFrame.size.width, y: initialFrame.origin.y, width: initialFrame.size.width, height: initialFrame.size.height)
        let overlayView = _overlayView
        
        UIView.animate(withDuration: 0.3, animations: {
            
            var frame = initialFrame
            frame.origin.x = frame.origin.x - 40
            self.frame = frame
        }) { (flag) in
            
            UIView.animate(withDuration: 0.6, delay: 0.0, options: [.curveEaseOut], animations: {
                
                self.alpha = 6.0
                overlayView?.alpha  = 0.0
                self.frame = finalFrame
            }, completion: { (flag) in
                
                self.removeFromSuperview()
            })
        }
    }
    
    public override func removeFromSuperview() {
        
        _overlayView?.removeFromSuperview()
        super.removeFromSuperview()
    }
}

