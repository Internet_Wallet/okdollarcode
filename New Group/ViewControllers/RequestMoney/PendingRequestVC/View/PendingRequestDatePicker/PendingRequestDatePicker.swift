//
//  PendingRequestDatePicker.swift
//  OK
//
//  Created by palnar on 27/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PendingRequestDatePicker :UIView {
    
    public typealias CancelAction = (PendingRequestDatePicker) ->()
    public typealias DoneAction = (PendingRequestDatePicker, Date) ->()
    
    public typealias FromCancelAction = (PendingRequestDatePicker) ->()
    public typealias FromDoneAction = (PendingRequestDatePicker, Date) ->()
    
    @IBOutlet weak var shadowView :UIView!
    @IBOutlet weak var picker :UIDatePicker!
    @IBOutlet weak var fromPicker :UIDatePicker!
    
    public var didCancel :CancelAction?
    public var didPick :DoneAction?
    
    public var fromDidCancel :FromCancelAction?
    public var fromDidPick :FromDoneAction?
    
    public static func newInstance() ->PendingRequestDatePicker? {
        
        return PendingRequestDatePicker.newInstance(xibName: "PendingRequestDatePicker")
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        _setUpDatePicker()
        _applyShadow()
    }
    
    private func _setUpDatePicker() {
        
        if #available(iOS 11.0, *) {
            picker.setValue(UIColor(named: "request_money_date_picker_text_color"), forKeyPath: "textColor")
        } else {
            // Fallback on earlier versions
        }
        
        if #available(iOS 13.4, *) {
            picker.preferredDatePickerStyle = .wheels
            fromPicker.preferredDatePickerStyle = .wheels

                   } else {
                       // Fallback on earlier versions
                   }
    }
    
    private func _applyShadow() {
        
        let view = self
        view.clipsToBounds = false
        view.layer.shadowOffset = CGSize(width: 0, height : -10)
        view.layer.shadowRadius = 8;
        view.layer.shadowOpacity = 0.2;
    }
    
    @IBAction func _cancelButtonTapped(_ sender :Any?) {
        
        didCancel?(self)
    }
    
    @IBAction func _doneButtonTapped(_ sender :Any?) {
        
        didPick?(self, picker.date)
    }
    
    @IBAction func _fromCancelButtonTapped(_ sender :Any?) {
        
        fromDidCancel?(self)
    }
    
    @IBAction func _fromDoneButtonTapped(_ sender :Any?) {
        
        fromDidPick?(self, fromPicker.date)
    }
}
