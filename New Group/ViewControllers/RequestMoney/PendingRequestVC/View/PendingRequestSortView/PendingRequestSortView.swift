//
//  PendingRequestSortView.swift
//  OK
//
//  Created by palnar on 27/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class SortViewButtonItem :UIControl {
    
    @IBOutlet weak var button :UIButton!{
        didSet{
            button.titleLabel?.font = UIFont(name: appFont, size: 8)
        }
    }
}

public class PendingRequestSortView: RightSideSlidableView {
    @IBOutlet weak var defultButton: UIButton!{
        didSet{
            defultButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var amountHighlowButton: UIButton!{
        didSet{
            amountHighlowButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var amountLowhighButton: UIButton!{
        didSet{
            amountLowhighButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var nameAtoZButton: UIButton!{
        didSet{
            nameAtoZButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var nameZtoAButton: UIButton!{
        didSet{
            nameZtoAButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    public typealias Action = (PendingRequestSortView) ->()
    
    public var didTapOnDefault :Action?
    public var didTapOnHighToLow :Action?
    public var didTapOnLowToHigh :Action?
    public var didTapOnAtoZ :Action?
    public var didTapOnZtoA :Action?
    
    @IBOutlet weak var deafultImage: UIImageView!
    
    @IBOutlet weak var hightolowImage: UIImageView!
    
    @IBOutlet weak var lowtohightImage: UIImageView!
    
    @IBOutlet weak var atozImage: UIImageView!
    
    @IBOutlet weak var ztoaImage: UIImageView!
    
    public static func newInstance() ->PendingRequestSortView? {
        return PendingRequestSortView.newInstance(xibName: "PendingRequestSortView")
    }
    
   
    @IBAction func defaultItemTapped(_ sender :Any) {
        didTapOnDefault?(self)
     
    }
    
    @IBAction func higtToLowItemTapped(_ sender :Any) {
        didTapOnHighToLow?(self)
    }
    
    @IBAction func lowToHighItemTapped(_ sender :Any) {
        didTapOnLowToHigh?(self)
    }
    
    @IBAction func AtoZItemTapped(_ sender :Any) {
        didTapOnAtoZ?(self)
    }
    
    @IBAction func ZtoAItemTapped(_ sender :Any) {
        didTapOnZtoA?(self)
    }
    
    
}


