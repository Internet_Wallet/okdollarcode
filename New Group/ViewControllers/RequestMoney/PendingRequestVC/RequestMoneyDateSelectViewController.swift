//
//  RequestMoneyDateSelectViewController.swift
//  OK
//
//  Created by PC on 5/18/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol dateSelectForSort : class {
    func okButtonAction(fromDate : Date, toDate : Date, reset: Bool)
}

class RequestMoneyDateSelectViewController: OKBaseController {
    
    @IBOutlet weak var resetButton : UIBarButtonItem!
    @IBOutlet weak var lblFrom : UILabel! {
        didSet {
            lblFrom.font = UIFont(name: appFont, size: appFontSize)
            lblFrom.text = "From".localized
        }
    }
    @IBOutlet weak var lblTo : UILabel! {
        didSet {
            lblTo.font = UIFont(name: appFont, size: appFontSize)
            lblTo.text = "To".localized
        }
    }
    
    @IBOutlet weak var cancelButton : UIButton! {
        didSet {
            cancelButton.backgroundColor = kYellowColor
            cancelButton.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
            cancelButton.setTitle(cancelButton.titleLabel?.text?.localized, for: .normal)
        }
    }
    var screenFrom = "Request Money"
    
    @IBOutlet weak var okButton : UIButton! {
        didSet {
            okButton.backgroundColor = kYellowColor
            okButton.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
            okButton.setTitle(okButton.titleLabel?.text?.localized, for: .normal)
        }
    }
    

    @IBOutlet weak var fromDatePicker : UIDatePicker! {
        didSet {
           self.fromDatePicker.maximumDate = Date()
        }
    }
    @IBOutlet weak var toDatePicker : UIDatePicker! {
        didSet {
            self.toDatePicker.maximumDate = Date()
        }
    }
    
    @IBOutlet weak var durationLabel : UILabel!{
        didSet{
            durationLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    var fromDate = Date()
    var toDate = Date()
    var dateFormatter = DateFormatter()
    weak var delegate : dateSelectForSort?

    override func viewDidLoad() {
        super.viewDidLoad()
//        resetButton = UIBarButtonItem(title:  appDelegate.getlocaLizationLanguage(key: "Reset"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.resetAction(_:)))
//        resetButton.setTitleTextAttributes([ NSAttributedStringKey.font: UIFont(name: "Zawgyi-One", size: 20)!,
//                                            NSAttributedStringKey.foregroundColor: UIColor.white ], for: .normal)
      
      resetButton.title = "Reset".localized
      resetButton.setTitleTextAttributes([ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20),
                                        NSAttributedString.Key.foregroundColor: UIColor.white ], for: .normal)
      
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        if screenFrom == "Request Money" {
            self.title = "Request Money".localized
        } else {
            self.title = "Transaction Receipt".localized
        }
        
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white ]
        self.navigationController?.navigationBar.tintColor = UIColor.white
        //self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        fromDatePicker.backgroundColor = .white
        toDatePicker.backgroundColor = .white
        if #available(iOS 13.4, *) {
            toDatePicker.preferredDatePickerStyle = .wheels
            fromDatePicker.preferredDatePickerStyle = .wheels

                   } else {
                       // Fallback on earlier versions
                   }
       // fromDatePicker.timeZone = TimeZone(identifier: "UTC")
        //toDatePicker.timeZone = TimeZone(identifier: "UTC")
        self.calculateDay()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func okButtonTapped(_ sender : UIButton) {
        if fromDate > toDate {
            self.showErrorAlert(errMessage: "From date should be less than To date".localized)
        } else {
            print("okButtonTapped----\(fromDate)---to---\(toDate)")
            delegate?.okButtonAction(fromDate: fromDate, toDate: toDate, reset: false)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender : UIButton) {
         self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func fromDatePickerChange(_ sender: Any) {
        if fromDatePicker.date > toDate {
            fromDatePicker.setDate(fromDate, animated: true)
            self.showErrorAlert(errMessage: "From date should be less than To date".localized)
        } else {
            fromDate = fromDatePicker.date
            self.calculateDay()
        }
    }
    
    @IBAction func toDatePickerChange(_ sender: Any) {
        if toDatePicker.date < fromDate {
            toDatePicker.setDate(toDate, animated: true)
           self.showErrorAlert(errMessage: "To date should be greater than From date".localized)
        } else {
            toDate = toDatePicker.date
            self.calculateDay()
        }
    }
    
    func calculateDay()
    {
        let form = DateComponentsFormatter()
        form.maximumUnitCount = 2
        form.unitsStyle = .full
        form.allowedUnits = [.day]
        let yearValue = form.string(from: fromDatePicker.date, to: toDatePicker.date)
        println_debug(yearValue as Any)
        durationLabel.text = "Duration".localized + ": "+(yearValue)!
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func resetAction(_ sender: Any) {
        if let date = dateFormatter.date(from: "01-Jan-1990") {
            delegate?.okButtonAction(fromDate: date, toDate: Date(), reset: true)
        }
        self.navigationController?.popViewController(animated: false)
    }

}
