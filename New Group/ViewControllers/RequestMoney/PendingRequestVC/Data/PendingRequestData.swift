//
//  PendingCriteria.swift
//  OK
//
//  Created by palnar on 28/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

struct PendingRequestData {
    
    var originalData :[RequestMoneyItem]
    fileprivate var _result :[RequestMoneyItem] = []
    
    
    enum LastAction {
    
        case none
        case date
        case sort
        case filter
        case search
    }
    
    var sort :Sort = .default
    var filter :Filter = .default
    var fromDate :Date?
    var toDate : Date?
    var searchString :String?
    
    var lastAction = LastAction.none
    
    init(datas :[RequestMoneyItem]) {
        
        self.originalData = datas
        self._result = datas
    }
    
    mutating func set(datas :[RequestMoneyItem]) {
        
        self.originalData = datas
        _refresh()
    }
    
    var result :[RequestMoneyItem] {
        
        return _result
    }
    
    var x :Bool {
        
        if let _ = searchString {
            return true
        }
        
        return false
    }
    
    var allAvailableTypes :[String] {
        
        var types = [String]()
        print("allAvailableTypes.originalData=====\(self.originalData.count)")
        for obj in self.originalData {
            //print("allAvailableTypes.sheduledOrNormal=====\(obj.NewScheduledType?.NewsheduledOrNormal)")

            if let type = obj.NewScheduledType?.NewsheduledOrNormal {
                
                types.append(type)

              /*  if  obj.LocalTransactionType == "TOPUP" || obj.LocalTransactionType == "DATAPLAN" || obj.LocalTransactionType == "SPECIALOFFER" {
                    types.append("Recharge")
                   // print("allAvailableTypes.types=====\(types)")
                 }
                else {
                types.append(type)
                } */
            }
            
            //print("allAvailableTypes.rechargeOrNormal=====\(obj.filterRecharge?.rechargeOrNormal)")

            if let type = obj.filterRecharge?.rechargeOrNormal {
                if  obj.LocalTransactionType == "TOPUP" || obj.LocalTransactionType == "DATAPLAN" || obj.LocalTransactionType == "SPECIALOFFER" {
                    types.append("Recharge")
                   // print("allAvailableTypes.types=====\(types)")
                 }
                else {
                types.append(type)
                }
            }
        }
        
        if types.count == 0 { return types }
        print("allAvailableTypes.alltypes=====\(types)----\(types.count)")

        return Array(Set(types))
    }
    
    var availableTypes :[String] {
         
        var types = [String]()
        
        for obj in self.result {

            if let type = obj.ScheduledType?.sheduledOrNormal {
                types.append(type)
            }
        }
        
        if types.count == 0 { return types }
        
        return Array(Set(types))
    }
    
    var allCategoryTypes : [String] {
        var categories = [String]()
        
        for obj in self.originalData {
            
            if let categorie = obj.CategoryName {
                categories.append(categorie)
            }
        }
        
        if categories.count == 0 { return categories }
        
        return Array(Set(categories))
    }
    
    var availableCategories :[String] {
        
        var categories = [String]()
        
        for obj in self.result {
            
            if let categorie = obj.CategoryName {
                categories.append(categorie)
            }
        }
        
        if categories.count == 0 { return categories }
        
        return Array(Set(categories))
    }
}

extension PendingRequestData {
    
    fileprivate mutating func _refresh() {
    
        self._result = _sorted(items: _filtered(items: _filteredByDate(items: originalData, fromDate: self.fromDate, toDate: self.toDate), filter: self.filter), sort: sort)
    }
}

extension PendingRequestData {
    
    mutating func searchByName(name string :String?)  {
        
        println_debug("string = \(string ?? "")")
        
        guard let string = string, string != "" else {
            
            self.searchString = nil
            _refresh()
            return
        }
        
        if _result.count > 0 {
            self.lastAction = .search
        }
        
        _refresh()
        self.searchString = string
        self._result = _searchIn(items: _result, byName: string)
    }
    
    private func _searchIn(items :[RequestMoneyItem], byName searchString :String) ->[RequestMoneyItem] {
        
        let searchString_ = searchString.lowercased()
        
        
     //   let value  = items
//        return items.filter({ (item) -> Bool in
//
//            if let name = item.AgentName, let number = item.Destination {
//
//
//                return ((name.lowercased().range(of: searchString_) != nil) || (number.lowercased().range(of: searchString_) != nil))
//            }
//            return false
//        })
        
//        for i in 0..<items.count{
//            print("****************")
//            print(value[i].Source ?? "")
//            print("****************")
//        }
        
        let itemArray =  items.filter({ (item) -> Bool in

                   guard let name = item.AgentName, let number = item.Source else { return false }

             

                 return ((name.lowercased().range(of: searchString_) != nil) || (number.lowercased().range(of: searchString_) != nil))
               })
        
        return itemArray
       // return value
    }
}

extension PendingRequestData {
    
    /*
    @discardableResult mutating func set(date :Date?) ->Bool {
        
        if self.fromDate == nil && date == nil { return false }
        
        if let dateA = self.fromDate, let dateB = date {
            
            switch dateA.compare(dateB) {
            case .orderedSame : return false
            default :break
            }
        }
        
        if _result.count > 0 {
           self.lastAction = .date
        }
        
        self.fromDate = date
        _refresh()
        return true
    } */
    
    
    @discardableResult mutating func set(fromDate :Date?, toDate : Date?) ->Bool {
        
        if fromDate == nil && toDate == nil { return false }
        
//        if let dateA = fromDate, let dateB = toDate {
//            
//            switch dateA.compare(dateB) {
//            case .orderedSame : return false
//            default :break
//            }
//        }
        if _result.count > 0 {
            self.lastAction = .date
        }
        
        self.fromDate = fromDate
        self.toDate = toDate
        _refresh()
        return true
    }
    
    fileprivate func _filteredByDate(items :[RequestMoneyItem], fromDate :Date?, toDate : Date?) ->[RequestMoneyItem] {

        guard let fromDateL = fromDate, let toDateL = toDate else { return items }
        
        let inputFormat = GlobalConstants.Strings.defaultDateFormat
        let outputFormat = GlobalConstants.Strings.filterDateFormat

        let dateString = stringFromDate(date: fromDateL as NSDate, format: inputFormat)
        let toDateString = stringFromDate(date: toDateL as NSDate, format: inputFormat)
        //print("_filteredByDatetoDateString----\(toDateString)")

        guard let lastSelectedDate = _formattedDateString(dateString, inputFormat: inputFormat, outputFormat: outputFormat), let currentSelectedDate = _formattedDateString(toDateString, inputFormat: inputFormat, outputFormat: outputFormat) else {

            return items
        }
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = GlobalConstants.Strings.filterDateFormat
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        guard let date1 = dateFormatter.date(from:  lastSelectedDate), let date2 = dateFormatter.date(from:  currentSelectedDate)  else {

            return items }
        
        dateFormatter.dateFormat = GlobalConstants.Strings.defaultAPIDateFormat
        let collected =  items.filter { (tempReuestMoneyItem) -> Bool in
            
            if let dateFromModel = tempReuestMoneyItem.Date {
                
                if let filteredModelDateString = _formattedDateString(dateFromModel, inputFormat: GlobalConstants.Strings.defaultAPIDateFormat, outputFormat: outputFormat) {
                    dateFormatter.dateFormat = GlobalConstants.Strings.filterDateFormat
                    if let dateModel = dateFormatter.date(from:  filteredModelDateString) {
                        let fallsBetween = (date1 ... date2).contains(dateModel)
                        //print("_filteredByDatetofallsBetween----\(fallsBetween)")

                        return fallsBetween
                    }
                }
                return false
                
            } else {
                return false
            }
        }
        
        return collected
    }
    
    private func _formattedDateString(_ date : String ,inputFormat : String , outputFormat: String ) -> String? {

        var formateStr : String
        let dateString = date
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = inputFormat
        
        guard let date = dateFormatter.date(from: dateString) else { return nil }
        
        dateFormatter.dateFormat = outputFormat
        formateStr = dateFormatter.string(from: date)
        
        return formateStr
    }
}

extension PendingRequestData {
    
    @discardableResult mutating func set(sort :Sort) ->Bool {
        
        if self.sort == sort { return false }
        
        if _result.count > 0 {
            self.lastAction = .sort
        }
        
        self.sort = sort
        _refresh()
        
        return true
    }
    
    fileprivate func _sorted(items :[RequestMoneyItem], sort :Sort) ->[RequestMoneyItem] {
        
        switch sort {
        case .default : return items
        case .amountHightoLow : return items.sorted(by: _hightoLow)
        case .amountLowToHigh : return items.sorted(by: _lowToHigh)
        case .nameAToZ : return items.sorted(by: _AtoZ)
        case .nameZToA : return items.sorted(by: _ZtoA)
        }
    }
    
    private func _hightoLow(item1 :RequestMoneyItem, item2 :RequestMoneyItem) -> Bool {
        
        guard let amount1 = item1.Amount, let amount2 = item2.Amount else { return false }
        return amount1 > amount2
    }
    
    private func _lowToHigh(item1 :RequestMoneyItem, item2 :RequestMoneyItem) -> Bool {
        
        guard let amount1 = item1.Amount, let amount2 = item2.Amount else { return false }
        return amount1 < amount2
    }
    
    private func _AtoZ(item1 :RequestMoneyItem, item2 :RequestMoneyItem) -> Bool {
        
        guard let name1 = item1.AgentName, let name2 = item2.AgentName else { return false }
        return name1 < name2
    }
    
    private func _ZtoA(item1 :RequestMoneyItem, item2 :RequestMoneyItem) -> Bool {
        
        guard let name1 = item1.AgentName, let name2 = item2.AgentName else { return false }
        return name1 > name2
    }
}

extension PendingRequestData {
    
    @discardableResult mutating func set(filter :Filter) ->Bool {
     print("set------\(filter)----\(self.filter)")
       // print("set_result------\(_result)")

        if self.filter == filter { return false }
        
        if _result.count > 0 {
            self.lastAction = .filter
        }
        if filter == .default {
           
        } else {
            self.sort = .nameAToZ
        }
        self.filter = filter
        _refresh()
        return true
    }
    
    fileprivate func _filtered(items :[RequestMoneyItem], filter :Filter) ->[RequestMoneyItem] {
        
        switch filter {
        case .default : return _filterDefault(items: items)
        case .type(let t) : return _filterTypes(items: items, type: t)
        case .categories(let c) : return _filterCategories(items: items, categories: c)
        }
    }
    
    private func _filterDefault(items :[RequestMoneyItem]) ->[RequestMoneyItem] {
        
        return items
    }
    
    private func _filterCategories(items :[RequestMoneyItem], categories :Filter.ByCategories) ->[RequestMoneyItem] {
        
        let value = categories.value
        let methode = _filterMethodeForCategorie(value: value)
        
        return items.filter(methode)
    }
    
    private func _filterTypes(items :[RequestMoneyItem], type :Filter.ByType) ->[RequestMoneyItem] {
        
        
        let value = type.value
        print("_filterTypesvalue---\(value)")
        let methode = _filterMethodeForType(value: value)
        
        return items.filter(methode)
    }
    
    private func _filterMethodeForType(value :String) -> ((RequestMoneyItem) -> Bool) {
        
        func innerFunction(item1 :RequestMoneyItem) -> Bool {
            
            let value_ = value

            if value_ == "Scheduled" || value_ == "Normal" {
                print("innerFunction value --\(value_)")

                guard let type = item1.NewScheduledType?.NewsheduledOrNormal else { return false }
                print("innerFunction type --\(type)")
                return type == value_

            } else {
                print("innerFunction else value --\(value_)")

                guard let type = item1.filterRecharge?.rechargeOrNormal else { return false }
                print("innerFunction else type --\(type)")
                return type == value_
            }
           

        }
        return innerFunction
    }
    
    private func _filterMethodeForCategorie(value :String) -> ((RequestMoneyItem) -> Bool) {
        
        func innerFunction(item1 :RequestMoneyItem) -> Bool {
            
            let value_ = value
            guard let category = item1.CategoryName else { return false }
            
            return category == value_
        }
        return innerFunction
    }
}


extension PendingRequestData {
    
    enum Sort {
        
        case `default`
        case amountHightoLow
        case amountLowToHigh
        case nameAToZ
        case nameZToA
    }
    
    enum Filter {
        
        case `default`
        case type(ByType)
        case categories(ByCategories)
        
        enum ByType {
            case string(String)
            
            var value :String {
                switch self {
                case .string(let s): return s
                }
            }
        }
        
        enum ByCategories {
            
            case string(String)
//            case restaurants = "Restaurants"
//            case foodNDrink = "Food & Drink"
//            case personal = "Personal"
            
            var value :String {
                switch self {
                case .string(let s): return s
                }
            }
        }
    }
}

func == (lhs: PendingRequestData.Sort, rhs: PendingRequestData.Sort) -> Bool {
    
    switch (lhs, rhs) {
    case (.default, .default) : return true
    case (.amountHightoLow, .amountHightoLow) : return true
    case (.amountLowToHigh, .amountLowToHigh) : return true
    case (.nameAToZ, .nameAToZ) : return true
    case (.nameZToA, .nameZToA) : return true
        
    default : return false
    }
}

func == (lhs: PendingRequestData.Filter, rhs: PendingRequestData.Filter) -> Bool {
    
    switch (lhs, rhs) {
    case (.default, .default) : return true
    case (.type(let t1), .type(let t2)) : return (t1 == t2)
    case (.categories(let c1), .categories(let c2)) : return (c1 == c2)
        
    default : return false
    }
}

func == (lhs: PendingRequestData.Filter.ByType, rhs: PendingRequestData.Filter.ByType) -> Bool {
    
    if lhs.value == rhs.value {
        return true
    }
    return false
}

func == (lhs: PendingRequestData.Filter.ByCategories, rhs: PendingRequestData.Filter.ByCategories) -> Bool {
    
    if lhs.value == rhs.value {
        return true
    }
    return false
}



