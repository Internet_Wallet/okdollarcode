//
//  HTTPUtilities.swift
//  OK
//
//  Created by palnar on 24/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

public class HTTPUtilities {
    
    public static func toString(data: Data) -> String? {
        
        return String(data: data, encoding: .utf8)
    }
    
    public static func toDictionary(text: String) -> [String: Any]? {
        
        println_debug("text = \(text)")
        if let data = text.data(using: .utf8) {
            
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                println_debug(error.localizedDescription)
            }
        }
        return nil
    }
    
    public static func toDictionary(data : Data) -> [String: Any]? {
        
        do {
            let dictonary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String :Any]
            
            return dictonary
        } catch let error as NSError {
            println_debug(error)
        }
        return nil
    }
    
    public static func toData(image : UIImage) -> Data? {
        
        return image.jpegData(compressionQuality: 1)
    }
    
    public static func toBase64EncodedData(image : UIImage) -> Data? {
        
        guard let data = toData(image: image) else { return nil }
        
        return toBase64EncodedData(data: data)
    }
    
    public static func toBase64EncodedString(image : UIImage) -> String? {
        
        guard let data = toData(image: image) else { return nil }
        
        return toBase64EncodedString(data: data)
    }
    
    public static func toBase64EncodedData(data : Data) -> Data {
        
        return data.base64EncodedData()
    }
    
    public static func toBase64EncodedString(data : Data) -> String {
        
        return data.base64EncodedString()
    }
}


