//
//  GET.swift
//  OK
//
//  Created by palnar on 23/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

public final class GETSubmitter {
    
    let configuration :HTTPClientConfiguration
    
    init(configuration :HTTPClientConfiguration) {
        self.configuration = configuration
    }
    
    @discardableResult func submit(ToURL url: URL, params: [String : Any]? = nil, _ completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) ->URLSessionDataTask {
        
        let configuration = self.configuration
        let session = configuration.session
        let timeoutInterval = configuration.timeoutInterval
        // let requetCachePolicy = configuration.cachePolicy
        
        var convertedParams :String? = nil
        var urlString = url.absoluteString
        if let params = params {
            convertedParams = _convertToURLParams(params)
        }
        //
        if let keyValues = convertedParams {
            urlString = urlString+"?"+keyValues
        }
        if let c = urlString.last, c == "&" {
            urlString.remove(at: urlString.index(before: urlString.endIndex))
        }
        let url_ = URL(string: urlString)!
        //
        println_debug("url = \(url_)")
        var request = URLRequest(url: url_)
        //
        request.httpMethod = "GET"
        request.timeoutInterval = timeoutInterval
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.setValue("application/json", forHTTPHeaderField:"Accept")
        //
        let task = session.dataTask(with: request, completionHandler: completionHandler)
        //
        task.resume()
        
        return task
    }
    
    private func _convertToURLParams(_ params :[String : Any]) ->String? {
        //
        var urlString = ""
        //
        for (key, value) in params {
            //
            urlString = urlString.appending("\(key)=\(value)&")
        }
        if urlString == "" { return nil }
        return urlString
    }
}


