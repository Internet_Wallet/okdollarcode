//
//  POST.swift
//  OK
//
//  Created by palnar on 23/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

public final class POSTSubmitter {
    
    let configuration :HTTPClientConfiguration
    let httpMethod :String
    
    init(configuration :HTTPClientConfiguration, httpMethod :String) {
        
        self.configuration = configuration
        self.httpMethod = httpMethod
    }
    
    @discardableResult func submit(ToURL url: URL, params: [String : Any]? = nil, images: [String : UIImage]? = nil, _ completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) ->URLSessionDataTask {
        
        if let images = images, images.keys.count > 0 {
            return _submitMultiPart(ToURL: url, params: params, images: images, completionHandler)
        } else {
            return _submitPOST(ToURL: url, params: params, completionHandler)
        }
    }
    
    private func _submitMultiPart(ToURL url: URL, params: [String : Any]? = nil, images :[String :UIImage], _ completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) ->URLSessionDataTask {
        
        let configuration = self.configuration
        let httpMethod = self.httpMethod
        let session = configuration.session
        let timeoutInterval = configuration.timeoutInterval
        let boundary = _generateBoundaryString()
        var request = URLRequest(url: url) 
        
        request.httpMethod = httpMethod
        request.timeoutInterval = timeoutInterval
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.httpBody = _requestBody(with: images, params: params, boundary: boundary)
        
        let task = session.dataTask(with: request, completionHandler: completionHandler)
        //
        task.resume()
        
        return task
    }
    
    private func _submitPOST(ToURL url: URL, params: [String : Any]? = nil, _ completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) ->URLSessionDataTask {
        
        let configuration = self.configuration
        let httpMethod = self.httpMethod
        let session = configuration.session
        let timeoutInterval = configuration.timeoutInterval
        var request = URLRequest(url: url)
        
        request.httpMethod = httpMethod
        request.timeoutInterval = timeoutInterval
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.setValue("application/json", forHTTPHeaderField:"Accept")
        //
        if let data = _datafrom(params) {
            
            request.setValue("\(data.count)", forHTTPHeaderField:"Content-Length")
            request.httpBody = data
        }
        
        let task = session.dataTask(with: request, completionHandler: completionHandler)
        //
        task.resume()
        
        return task
    }
    
    private func _requestBody(with images :[String :UIImage], params :[String : Any]?, boundary: String) -> Data {
        //
        var body = Data()
        var tempData = Data()
        //
        if let params = params {
            //
            for (key, value) in params {
                //
                tempData = "--\(boundary)\r\n".data(using: .utf8)!
                body.append(tempData)
                tempData = "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: .utf8)!
                body.append(tempData)
                tempData = "\(value)\r\n".data(using: .utf8)!
                body.append(tempData)
            }
        }
        
        for (filename, image) in images {
            //
            let data = image.jpegData(compressionQuality: 1)
            let mimetype = "image/jpeg"
            //
            tempData = "--\(boundary)\r\n".data(using: .utf8)!
            body.append(tempData)
            tempData = "Content-Disposition: form-data; name=\"\(filename)\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!
            body.append(tempData)
            tempData = "Content-Type: \(mimetype)\r\n\r\n".data(using: .utf8)!
            body.append(tempData)
            body.append(data!)
            tempData = "\r\n".data(using: .utf8)!
            body.append(tempData)
            //
            tempData = "--\(boundary)\r\n".data(using: .utf8)!
            body.append(tempData)
            tempData = "Content-Disposition: form-data; name=\"\(filename)\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!
            body.append(tempData)
            tempData = "Content-Type: \(mimetype)\r\n\r\n".data(using: .utf8)!
            body.append(tempData)
            tempData = "\r\n".data(using: .utf8)!
            body.append(tempData)
        }
        //
        tempData = "--\(boundary)--\r\n".data(using: .utf8)!
        body.append(tempData)
        //
        return body
    }
    
    private func _generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    private func _datafrom(_ params :[String : Any]?) -> Data? {
        //
        guard let params = params else { return nil }
        do {
            
            let data = try JSONSerialization.data(withJSONObject: params)
            let string = String(data: data, encoding: .utf8)
            
            return string?.data(using: .utf8)
        } catch let e {
            
            println_debug(e.localizedDescription)
            return nil
        }
    }
}

