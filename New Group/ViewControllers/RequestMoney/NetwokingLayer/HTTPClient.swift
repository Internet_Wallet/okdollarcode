//
//  HTTPClient.swift
//  OK
//
//  Created by palnar on 24/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

public class HTTPClient {
    
    public let configuration :HTTPClientConfiguration
    fileprivate let GET :GETSubmitter
    fileprivate let POST :POSTSubmitter
    fileprivate let PUT :POSTSubmitter
    fileprivate let DELETE :POSTSubmitter
    
    public private(set) static var shard: HTTPClient = {
        
        let session = HTTPClientConfiguration.default
        return HTTPClient(configuration: session)
    }()
    
    init(configuration :HTTPClientConfiguration) {
        
        self.configuration = configuration
        self.GET = GETSubmitter(configuration: configuration)
        self.POST = POSTSubmitter(configuration: configuration, httpMethod: "POST")
        self.PUT = POSTSubmitter(configuration: configuration, httpMethod: "PUT")
        self.DELETE = POSTSubmitter(configuration: configuration, httpMethod: "DELETE")
    }
}

extension HTTPClient :HTTPClientInterface {
    
    @discardableResult public func GET(ToURL url: URL, _ completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) ->URLSessionDataTask {
        
        return GET.submit(ToURL: url, params: nil, completionHandler)
    }
    
    @discardableResult public func GET(ToURL url: URL, params: [String : Any]? = nil, _ completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) ->URLSessionDataTask {
        
        return GET.submit(ToURL: url, params: params, completionHandler)
    }
    
    @discardableResult public func POST(ToURL url: URL, params: [String : Any]?, _ completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) ->URLSessionDataTask {
        
        return POST.submit(ToURL :url, params :params, images :nil, completionHandler)
    }
    
    @discardableResult public func POST(ToURL url: URL, params: [String : Any]? = nil, images: [String : UIImage]? = nil, _ completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) ->URLSessionDataTask {
        
        return POST.submit(ToURL :url, params :params, images :images, completionHandler)
    }
    
    @discardableResult public func PUT(ToURL url: URL, params: [String : Any]? = nil, images: [String : UIImage]? = nil, _ completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) ->URLSessionDataTask {
        
        return PUT.submit(ToURL :url, params :params, images :images, completionHandler)
    }
    
    @discardableResult public func DELETE(ToURL url: URL, params: [String : Any]? = nil, _ completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) ->URLSessionDataTask {
        
        return DELETE.submit(ToURL :url, params :params, completionHandler)
    }
}

