//
//  HTTPClientInterface.swift
//  OK
//
//  Created by palnar on 24/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

//public typealias JSONDictionary = [String:Any]

public protocol HTTPClientInterface {
    
    @discardableResult func GET(ToURL url: URL, _ completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) ->URLSessionDataTask
    
    @discardableResult func GET(ToURL url: URL, params: [String : Any]? , _ completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) ->URLSessionDataTask
    
    @discardableResult func POST(ToURL url: URL, params: [String : Any]?, _ completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) ->URLSessionDataTask
    
    @discardableResult func POST(ToURL url: URL, params: [String : Any]? , images: [String : UIImage]? , _ completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) ->URLSessionDataTask
    
    @discardableResult func PUT(ToURL url: URL, params: [String : Any]? , images: [String : UIImage]? , _ completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) ->URLSessionDataTask
    
    @discardableResult func DELETE(ToURL url: URL, params: [String : Any]?, _ completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) ->URLSessionDataTask
}

