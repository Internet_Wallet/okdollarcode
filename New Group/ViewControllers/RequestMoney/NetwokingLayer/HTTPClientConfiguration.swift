//
//  HTTPClientConfiguration.swift
//  OK
//
//  Created by palnar on 24/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

public class HTTPClientConfiguration {
    
    public private(set) static var `default`: HTTPClientConfiguration = {
        
        let session = HTTPClientConfiguration(session: URLSession.shared, timeoutInterval: 60, cachePolicy: .reloadIgnoringLocalCacheData)
        return session
    }()
    
    let session :URLSession
    let timeoutInterval :TimeInterval
    let cachePolicy :NSURLRequest.CachePolicy
    
    init(session :URLSession, timeoutInterval :TimeInterval, cachePolicy :NSURLRequest.CachePolicy) {
        
        self.session = session
        self.timeoutInterval = timeoutInterval
        self.cachePolicy = cachePolicy
    }
}

