//
//  ImagePickerVC.swift
//  OK
//
//  Created by palnar on 23/01/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

public class ImagePickerVC: UIImagePickerController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
 
    public typealias PickAction = (ImagePickerVC, [String : Any]) ->()
    public typealias CancelAction = (ImagePickerVC) ->()
    
    public var didPick :PickAction?
    public var didCancel :CancelAction?
    
    @discardableResult func set(sourceType :UIImagePickerController.SourceType) ->ImagePickerVC {
        
        self.sourceType = sourceType
        return self
    }
    @discardableResult func set(allowsEditing :Bool) ->ImagePickerVC {
        
        self.allowsEditing = allowsEditing
        return self
    } 
    
    @discardableResult func set(didPick :PickAction?) ->ImagePickerVC {
        
        self.didPick = didPick
        return self
    }
    
    @discardableResult func set(didCancel :CancelAction?) ->ImagePickerVC {
        
        self.didCancel = didCancel
        return self
    }
    
    @discardableResult func show(On VC :UIViewController, completion: (() -> Swift.Void)? = nil) ->ImagePickerVC {
        
        self.delegate = self
        VC.present(self, animated: true, completion: nil)
        return self
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
        didPick?(self, info)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        didCancel?(self)
    }
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}
