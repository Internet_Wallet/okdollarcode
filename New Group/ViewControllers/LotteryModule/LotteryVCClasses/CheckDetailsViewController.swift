//
//  CheckDetailsViewController.swift
//  LotteryModule
//
//  Created by iMac on 21/09/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CheckDetailsViewController: OKBaseController {
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var navigationTitleLabel: UILabel!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var historyButton: UIButton!
    @IBOutlet var cartButton: UIButton!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var payButton: UIButton!
    @IBOutlet var timerdisplayView: UIView!
    @IBOutlet var minutesLabel: UILabel!
    @IBOutlet var secsLabel: UILabel!
    @IBOutlet var ticketcountLabel: UILabel!
    @IBOutlet var detailsView: UIView!
    @IBOutlet var detailsCollectionView: UICollectionView!
    var singlePrice: String?
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var totalamountLabel: UILabel!
    var confirmationticketListArray = [LotteryCouponDetails]()
    var ticketMainArray = [[String : Any]]()
    var lotteryViewModel = LotteryViewModel()
    var finalModelObj: LotteryFinalModel?
    var otherPersonDetails: OtherPersonDetails?
    var isButtonTapped = false
    var lotteryTicketImage: String?
    
    var timerQR: Timer?
    var totalTime = 0
    var timeAddedFromServer = 0
    var previousTime = Date()
    var currentTime = Date()
    var previoushour = 0
    var previousMinute = 0
    var previousSecond = 0
    var currentHour = 0
    var currentMinute = 0
    var currentSecond = 0
    var wentBackGround = false
    var formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTimer()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "hh:mm:ss"
        NotificationCenter.default.addObserver(self, selector: #selector(moveToBackGround), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(enterForgound), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        
        if let validData = otherPersonDetails{
            nameLabel.text = validData.name ?? ""
            numberLabel.text = validData.phoneNumer ?? ""
        }else{
            nameLabel.text = UserModel.shared.name
            numberLabel.text = UserModel.shared.mobileNo
        }
        
        timerdisplayView.layer.cornerRadius = 10
        detailsView.layer.cornerRadius = 10
        minutesLabel.layer.cornerRadius = 4.0
        minutesLabel.layer.masksToBounds = true
        minutesLabel.layer.borderColor = UIColor.lightGray.cgColor
        minutesLabel.layer.borderWidth = 0.5
        
        secsLabel.layer.cornerRadius = 4.0
        secsLabel.layer.masksToBounds = true
        secsLabel.layer.borderColor = UIColor.lightGray.cgColor
        secsLabel.layer.borderWidth = 0.5
        
        
        let strcount = confirmationticketListArray.count
        
        
        
        ticketcountLabel.text = "\(strcount)" + " Nos."
        
        if let value = singlePrice{
//            let singleAmount = Int(value)
//            if let final = singleAmount{
//                let amount = final * confirmationticketListArray.count
//                let total = String(amount)
                totalamountLabel.text = "\(value)" + ".00" + " MMK "
          //  }
        }
        
        
        
        detailsCollectionView.reloadData()
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let timer = self.timerQR {
            timer.invalidate()
        }
    }
    
    
    @objc func moveToBackGround() {
        let date = Date()
        previousTime = date
        let str = formatter.string(from: previousTime)
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: formatter.date(from: str) ?? Date())
        let minutes = calendar.component(.minute, from: formatter.date(from: str) ?? Date())
        let second = calendar.component(.second, from: formatter.date(from: str) ?? Date())
        previoushour = hour
        previousMinute = minutes
        previousSecond = second
        wentBackGround = true
    }
    
    @objc func enterForgound() {
        if wentBackGround {
            diffTime()
            wentBackGround = false
        }
    }
    
    func diffTime() {
        let str = formatter.string(from: Date())
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: formatter.date(from: str) ?? Date())
        let minutes = calendar.component(.minute, from: formatter.date(from: str) ?? Date())
        let second = calendar.component(.second, from: formatter.date(from: str) ?? Date())
        currentHour = hour
        currentMinute = minutes
        currentSecond = second
        
        if currentHour < previoushour {
            currentMinute = currentMinute + 60
        }
        if currentMinute < previousMinute {
            currentSecond = currentSecond + 60
        }
        let finalMin = currentMinute - previousMinute
        let finalSecond = currentSecond - previousSecond
        totalTime =  totalTime - (finalMin * 60 + finalSecond)
    }
    
    
    fileprivate func setTimer(){
        let value = totalTime/60

        if value<10{
            minutesLabel.text = "\(0)\(value):00"
        }else{
            minutesLabel.text = "\(value):00"
        }

       // timeLabel.text  = "05:00"

        self.startOtpTimer(time: totalTime - 1)
    }
    
    private func startOtpTimer(time: Int) {
        //5 min 300 sec
        self.totalTime = time
        self.timerQR = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        self.minutesLabel.text = self.timeFormatted(self.totalTime)
        
        if totalTime > 0 {
            totalTime -= 1
        }else {
            if let timer = self.timerQR {
                timer.invalidate()
                alertViewObj.wrapAlert(title: "", body: "Session Expired.Do You want to book more".localized, img: #imageLiteral(resourceName: "NearMeNew"))
                alertViewObj.addAction(title: "YES".localized, style: .target , action: {
                   // self.totalTime = self.timeAddedFromServer
                    DispatchQueue.main.async {
                        if let navigationController = self.navigationController{
                            for controller in navigationController.viewControllers as Array {
                                if #available(iOS 13.0, *) {
                                    if controller.isKind(of: LotteryListViewController.self) {
                                        navigationController.popToViewController(controller, animated: true)
                                        break
                                    }
                                } else {
                                    // Fallback on earlier versions
                                }
                            }
                        }
                    }
                })
                alertViewObj.addAction(title: "NO".localized, style: .target , action: {
                    self.navigationController?.popToRootViewController(animated: false)
                })
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%01d:%02d", minutes, seconds)
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
            alertViewObj.wrapAlert(title: "", body: "Since you have selected Ticket you cannot go back.If you press Ok your session will expire".localized, img: #imageLiteral(resourceName: "NearMeNew"))
            alertViewObj.addAction(title: "YES".localized, style: .target , action: {
                DispatchQueue.main.async {
                    if let navigationController = self.navigationController{
                        for controller in navigationController.viewControllers as Array {
                            if controller.isKind(of: LotteryListViewController.self) {
                                navigationController.popToViewController(controller, animated: true)
                                break
                            }
                        }
                    }
                }
            })
            alertViewObj.addAction(title: "NO".localized, style: .target , action: {
                
            })
            alertViewObj.showAlert(controller: self)
    }
    @IBAction func historyButtonClick(_ sender: Any) {
    }
    @IBAction func cancelButtonClick(_ sender: Any) {
        if let navigationController = self.navigationController{
            for controller in navigationController.viewControllers as Array {
                if controller.isKind(of: LotteryListViewController.self) {
                    navigationController.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    
    @IBAction func payButtonClick(_ sender: Any) {
        
        
        if let value = singlePrice{
            let singleAmount = Int(value)
            if let final = singleAmount{
                let amount = final * confirmationticketListArray.count
                let total = String(amount)
                if !isButtonTapped {
                    
                    let balanceAmount = (UserLogin.shared.walletBal as NSString).floatValue
                    let billAmountIntValue = (total as NSString).floatValue
                    
                    if balanceAmount>=billAmountIntValue{
                        //then only deduct the money
                        if appDelegate.checkNetworkAvail() {
                            progressViewObj.showProgressView()
                            self.isButtonTapped = true
                           //call service here
                            lotteryViewModel.receiptDel = self
                            lotteryViewModel.luckyPay(ticketList: confirmationticketListArray)
                        } else {
                            self.isButtonTapped = false
                            self.noInternetAlert()
                            progressViewObj.removeProgressView()
                        }
                   }else{
                        //show alert
                        self.showErrorAlert(errMessage: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized)
                    }
                }
                
            }
            
        }
        
    }
    
    
}

class HomeConstant {
    static let totalItem: CGFloat = 2
    static let column: CGFloat = 1
    static let minLineSpacing: CGFloat = 10.0
    static let minItemSpacing: CGFloat = 10.0
    static let offset: CGFloat = 1.0 // TODO: for each side, define its offset
    
    static func getItemWidth(boundWidth: CGFloat) -> CGFloat {
        let totalWidth = boundWidth - (offset + offset) - ((column - 1) * minItemSpacing)
        return totalWidth / column
    }
}

extension CheckDetailsViewController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return confirmationticketListArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ticketCell", for: indexPath) as! CheckDetailsCollectionViewCell
        
        
        
        let ticketnum = confirmationticketListArray[indexPath.row].couponNumber ?? ""
        cell.ticketImageView.sd_setImage(with: URL(string: lotteryTicketImage ?? ""), placeholderImage: nil)
        cell.ticketnumberLabel.text = ticketnum
     
        return cell
    
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ticketCell", for: indexPath) as! CheckDetailsCollectionViewCell

        return cell.contentView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)


    }
    
    
    // Layout: Set Edges
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0) // top, left, bottom, right
    }
  
}

extension CheckDetailsViewController: LotteryReceiptModel {
    
    func receipt(response: LotteryFinalModel){
        finalModelObj = nil
        finalModelObj = response
        progressViewObj.removeProgressView()
        
        if !response.isTransactionSuccess{
            //failed
            DispatchQueue.main.async {
                self.showErrorAlert(errMessage: response.msg ?? "")
            }
        }else{
            DispatchQueue.main.async {
                var homeVC: LotteryReceiptViewController?
                homeVC = UIStoryboard(name: LotteryConstant.kLotteryStoryBoard, bundle: nil).instantiateViewController(withIdentifier: LotteryConstant.kLotteryReceiptViewController) as? LotteryReceiptViewController
                if let homeVC = homeVC {
                    if let validValue = self.otherPersonDetails{
                        homeVC.otherPersonDetails = validValue
                    }
                    homeVC.finalLotteryModel = self.finalModelObj
                    homeVC.lotteryTicketImage = self.lotteryTicketImage
                    self.navigationController?.pushViewController(homeVC, animated: true)
                }
            }
        }
        
    }
    
    
    
}


//extension CheckDetailsViewController : ApiRequestProtocol{
//    func ObjLoginAuthDto() ->NSMutableDictionary {
//
//        let dictObjLoginAuthDto = NSMutableDictionary()
//        dictObjLoginAuthDto["MobileNumber"] = "00959790681381"
//        dictObjLoginAuthDto["Simid"] = "1"
//        dictObjLoginAuthDto["Msid"] = "1"
//        dictObjLoginAuthDto["Ostype"] = 0
//        dictObjLoginAuthDto["Otp"] = ""
//        dictObjLoginAuthDto["AppId"] = "ngjg"
//        dictObjLoginAuthDto["Limit"] = 0
//        dictObjLoginAuthDto["Offset"] = 0
//
//
//
//        return dictObjLoginAuthDto
//    }
//
//    fileprivate func callAPIpayment(){
//
//
//
//
//
//
//
//        let apiReqObj = ApiRequestClass()
//        apiReqObj.customDelegate = self
//
//
//        for valTicket in confirmationticketListArray {
//            var dictTemp : [String:Any] = [:]
//            dictTemp["LuckyNumber"] = valTicket["CouponNumber"]
//            dictTemp["ProductId"] = valTicket["ProductId"]
//            ticketMainArray.append(dictTemp)
//        }
//
//
//        let childDicts = [
//            "LuckyNumberList": ticketMainArray,
//            "ObjLoginAuthDto" : self.ObjLoginAuthDto(),
//            "Password":"123456",
//            "SecureToken":"fgegeger"
//            ] as [String: Any]
//
//        print ("@@@@@",childDicts)
//
//        DispatchQueue.main.async{
//            apiReqObj.sendHttpRequest(requestUrl:URL(string:"http://69.160.4.151:8001/RestService.svc/ProcessLuckyNumPayment")!, requestData: childDicts, requestType: RequestTypeObj.RequestTypePayment, httpMethodName: "POST")
//        }
//
//    }
//
//    func httpResponse(responseObj: Any, reqType: RequestTypeObj) {
//        DispatchQueue.main.async{
//
//         // progressViewObj.removeProgressView()
//
//            if let respMsg = responseObj as? String
//            {
//
//            //  alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//
//            //      })
//            //  alertViewObj.showAlert(controller: self)
//
//            }
//            else
//            {
//
//              var respDict = [String:Any]()
//              if let resposneDataFromServer = responseObj as? [String : Any] {
//                  respDict = resposneDataFromServer
//              }
//
//              if reqType == RequestTypeObj.RequestTypePayment
//                {
//                    if respDict.count > 0
//                    {
//                    let code = respDict["Code"] as? Int
//                     print("#####",code as Any)
//                     if let isValidCode = code {
//                        if isValidCode == 200 {
//                            var homeVC: LotteryReceiptViewController?
//                            homeVC = UIStoryboard(name: LotteryConstant.kLotteryStoryBoard, bundle: nil).instantiateViewController(withIdentifier: LotteryConstant.kLotteryReceiptViewController) as? LotteryReceiptViewController
//                            if let homeVC = homeVC {
//                                self.navigationController?.pushViewController(homeVC, animated: true)
//                            }
//                        }
//                      else
//                      {
////                          alertViewObj.wrapAlert(title: "", body: (respDict["message"] as? String) ?? "", img: #imageLiteral(resourceName: "alert-icon"))
////                          alertViewObj.addAction(title: "OK".localized, style: .target , action: {
////                              })
////                          alertViewObj.showAlert(controller: self)
//                        let alert = UIAlertController(title: "Alert", message: respDict["Msg"] as? String, preferredStyle: UIAlertController.Style.alert)
//                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//                        self.present(alert, animated: true, completion: nil)
//
//                        }
//                      }
//                      else
//                       {
//                        let alert = UIAlertController(title: "Alert", message: respDict["Msg"] as? String, preferredStyle: UIAlertController.Style.alert)
//                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//                        self.present(alert, animated: true, completion: nil)
//                      }
//
//
//                    }
//                    else
//                    {
//                        let alert = UIAlertController(title: "Alert", message: respDict["Msg"] as? String, preferredStyle: UIAlertController.Style.alert)
//                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//                        self.present(alert, animated: true, completion: nil)
//                    }
//              }
//            }
//        }
//    }
//
//
//
//}
