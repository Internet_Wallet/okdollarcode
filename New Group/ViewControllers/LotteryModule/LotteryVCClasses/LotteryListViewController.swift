//
//  LotteryListViewController.swift
//  LotteryModule
//
//  Created by iMac on 18/09/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import Photos

class LotteryListViewController: OKBaseController {
    @IBOutlet var navigationView: UIView!
    @IBOutlet var navigationTitleLabel: UILabel!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var lotteryTableView: UITableView!
    var lotteryViewModel = LotteryViewModel()
    var lotteryModel =  [LotteryCompanyDetails]()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.lotteryTableView.tableFooterView = UIView()
        self.apicall()
    
    }

    func apicall(){
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            lotteryViewModel.companyListDel = self
            lotteryViewModel.getLotteryCompanyList(param: LotteryParam.getParamForCompanyList())
        }else {
            self.noInternetAlert()
            progressViewObj.removeProgressView()
        }
    }

    
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}


extension LotteryListViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lotteryModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = lotteryTableView.dequeueReusableCell(withIdentifier: "Cell") as? LotteryListTableViewCell else{
            return UITableViewCell()
        }
        
        cell.selectionStyle = .none
        cell.wrapDataInCell(value: lotteryModel[indexPath.row].companyName ?? "")
        cell.lottryLogoImageView.sd_setImage(with: URL(string: lotteryModel[indexPath.row].imageUrl ?? ""), placeholderImage: nil)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if #available(iOS 13.0, *) {
            let obj = self.storyboard?.instantiateViewController(identifier: LotteryConstant.kReadMeViewController) as? ReadMeViewController
            if let isValid = obj{
                isValid.lotterydict = lotteryModel[indexPath.row]
                self.navigationController?.pushViewController(isValid, animated: true)
            }
        } else {
            if let obj = self.storyboard?.instantiateViewController(withIdentifier: LotteryConstant.kReadMeViewController) as? ReadMeViewController{
                obj.lotterydict = lotteryModel[indexPath.row]
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }
        
    }
    
}

extension LotteryListViewController: GetCompanyList{
    
    func companyList(model: LotteryCompanyModel){
        lotteryModel.removeAll()
        progressViewObj.removeProgressView()
        
        if !model.isTransactionSuccess{
                //failed
                DispatchQueue.main.async {
                    self.showErrorAlert(errMessage: model.msg ?? "")
                }
        }else{
            if model.lotteryCompanyDetails.count>0{
                lotteryModel = model.lotteryCompanyDetails
                DispatchQueue.main.async {
                    self.lotteryTableView.delegate = self
                    self.lotteryTableView.dataSource = self
                    self.lotteryTableView.reloadData()
                }
            }else{
                DispatchQueue.main.async {
                  self.showErrorAlert(errMessage: "Some error happen please try again".localized)
                }
            }
        }
    }
    
    
}
