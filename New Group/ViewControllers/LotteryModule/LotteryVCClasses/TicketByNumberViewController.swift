//
//  TicketByNumberViewController.swift
//  LotteryModule
//
//  Created by iMac on 18/09/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class TicketByNumberViewController: OKBaseController {
   
    @IBOutlet var fourthLabel: UILabel!
    @IBOutlet var thirdLabel: UILabel!
    @IBOutlet var firstLetterLabel: UILabel!
    
    @IBOutlet var secondLetterLabel: UILabel!
    @IBOutlet var navigationView: UIView!
    @IBOutlet var navigationTitleLabel: UILabel!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var historyButton: UIButton!
    @IBOutlet var cartButton: UIButton!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var resetButton: UIButton!
    @IBOutlet var applyButton: UIButton!
    @IBOutlet var selectionTypeLabel: UILabel!
    
    @IBOutlet var alphaNumberView: UIView!
    @IBOutlet var timerdisplayView: UIView!
    @IBOutlet var minutesLabel: UILabel!
    @IBOutlet var secsLabel: UILabel!
    
    @IBOutlet var lotteryTicketsTableView: UITableView!
    
    @IBOutlet var timerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var infolabel: UILabel!
    
    @IBOutlet var cartcountlabel: UILabel!
    
    var companyDic: LotteryCompanyDetails?
    var productid = ""
    var singlePrice: String?
    
    
    var reservedIndex: Int?
    
    var otherPersonDetails: OtherPersonDetails?
    var cartListArray = [LotteryCouponDetails]()
    
    var selectedPaths=Set<IndexPath>()
    var lotteryViewModel = LotteryViewModel()
    var lotteryCouponDetails = [LotteryCouponDetails]()
    var lotteryTicketImage: String?
    
    var timerQR: Timer?
    var totalTime = 0
    var timeAddedFromServer = 0
    var previousTime = Date()
    var currentTime = Date()
    var previoushour = 0
    var previousMinute = 0
    var previousSecond = 0
    var currentHour = 0
    var currentMinute = 0
    var currentSecond = 0
    var wentBackGround = false
    var formatter = DateFormatter()
    var tickArray = [String]()
    var isTicketSelected = 0
    
    weak var Ticketdelegate : TicketByAlphabetDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "hh:mm:ss"
        NotificationCenter.default.addObserver(self, selector: #selector(moveToBackGround), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(enterForgound), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        navigationTitleLabel.text = companyDic?.companyName ?? ""
        minutesLabel.layer.cornerRadius = 4.0
        minutesLabel.layer.masksToBounds = true
        minutesLabel.layer.borderColor = UIColor.lightGray.cgColor
        minutesLabel.layer.borderWidth = 0.5
        
        secsLabel.layer.cornerRadius = 4.0
        secsLabel.layer.masksToBounds = true
        secsLabel.layer.borderColor = UIColor.lightGray.cgColor
        secsLabel.layer.borderWidth = 0.5
        
        
       
        
        
        if cartListArray.count < 1{
            cartcountlabel.isHidden = true
            if timeAddedFromServer>0{
                self.totalTime = timeAddedFromServer
            }
            timerdisplayView.isHidden = true
            timerHeightConstraint.constant = 0
            infolabel.isHidden = true
        }
        else{
            infolabel.isHidden = false
            cartcountlabel.isHidden = false
            cartcountlabel.text = "\(cartListArray.count)"
            self.totalTime = timeAddedFromServer
            timerdisplayView.isHidden = false
            timerHeightConstraint.constant = 60
            setTimer()
        }
       
        cartcountlabel.layer.cornerRadius = 10
        cartcountlabel.layer.masksToBounds = true
        
        switch tickArray.count {
        case 1:
            firstLetterLabel.text = "\(tickArray[0])"
            firstLetterLabel.isHidden = false
            secondLetterLabel.isHidden = true
            thirdLabel.isHidden = true
            fourthLabel.isHidden = true
        case 2:
            firstLetterLabel.text = "\(tickArray[0])"
            secondLetterLabel.text = "\(tickArray[1])"
            firstLetterLabel.isHidden = false
            secondLetterLabel.isHidden = false
            thirdLabel.isHidden = true
            fourthLabel.isHidden = true
        case 3:
            firstLetterLabel.text = "\(tickArray[0])"
            secondLetterLabel.text = "\(tickArray[1])"
            thirdLabel.text = "\(tickArray[2])"
            firstLetterLabel.isHidden = false
            secondLetterLabel.isHidden = false
            thirdLabel.isHidden = false
            fourthLabel.isHidden = true
        case 4:
            firstLetterLabel.text = "\(tickArray[0])"
            secondLetterLabel.text = "\(tickArray[1])"
            thirdLabel.text = "\(tickArray[2])"
            fourthLabel.text = "\(tickArray[3])"
            firstLetterLabel.isHidden = false
            secondLetterLabel.isHidden = false
            thirdLabel.isHidden = false
            fourthLabel.isHidden = false
        default:
            break
        }
        
        apicall()
    //    self.setTimer()
    }
    
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        if let timer = self.timerQR {
//            timer.invalidate()
//        }
    }
    
    
    @objc func moveToBackGround() {
        let date = Date()
        previousTime = date
        let str = formatter.string(from: previousTime)
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: formatter.date(from: str) ?? Date())
        let minutes = calendar.component(.minute, from: formatter.date(from: str) ?? Date())
        let second = calendar.component(.second, from: formatter.date(from: str) ?? Date())
        previoushour = hour
        previousMinute = minutes
        previousSecond = second
        wentBackGround = true
    }
    
    @objc func enterForgound() {
        if wentBackGround {
            diffTime()
            wentBackGround = false
        }
    }
    
    func diffTime() {
        let str = formatter.string(from: Date())
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: formatter.date(from: str) ?? Date())
        let minutes = calendar.component(.minute, from: formatter.date(from: str) ?? Date())
        let second = calendar.component(.second, from: formatter.date(from: str) ?? Date())
        currentHour = hour
        currentMinute = minutes
        currentSecond = second
        
        if currentHour < previoushour {
            currentMinute = currentMinute + 60
        }
        if currentMinute < previousMinute {
            currentSecond = currentSecond + 60
        }
        let finalMin = currentMinute - previousMinute
        let finalSecond = currentSecond - previousSecond
        totalTime =  totalTime - (finalMin * 60 + finalSecond)
    }
    
    
    fileprivate func setTimer(){
        let value = totalTime/60

        if value<10{
            minutesLabel.text = "\(0)\(value):00"
        }else{
            minutesLabel.text = "\(value):00"
        }

       // timeLabel.text  = "05:00"

        self.startOtpTimer(time: totalTime - 1)
    }
    
    private func startOtpTimer(time: Int) {
        //5 min 300 sec
        self.totalTime = time
        self.timerQR = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        self.minutesLabel.text = self.timeFormatted(self.totalTime)
        
        if totalTime > 0 {
            totalTime -= 1
        }else {
            if let timer = self.timerQR {
                timer.invalidate()
                alertViewObj.wrapAlert(title: "", body: "Session Expired.Do You want to book more".localized, img: #imageLiteral(resourceName: "NearMeNew"))
                alertViewObj.addAction(title: "YES".localized, style: .target , action: {
                    self.totalTime = self.timeAddedFromServer
                    DispatchQueue.main.async {
                        if let navigationController = self.navigationController{
                            for controller in navigationController.viewControllers as Array {
                                if controller.isKind(of: LotteryListViewController.self) {
                                    navigationController.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                        }
                    }
                })
                alertViewObj.addAction(title: "NO".localized, style: .target , action: {
                    self.navigationController?.navigationBar.isHidden = false
                    self.navigationController?.popToRootViewController(animated: false)
                })
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%01d:%02d", minutes, seconds)
    }
    
    
    func apicall(){
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            lotteryViewModel.couponListDel = self
            lotteryViewModel.getNumberList(productId: productid, searchString: tickArray.joined(separator: ""))
        }else {
            self.noInternetAlert()
            progressViewObj.removeProgressView()
        }
    }
    
    
    @IBAction func backButtonClick(_ sender: Any) {
        
        if timerdisplayView.isHidden || cartcountlabel.isHidden{
           
            self.navigationController?.popViewController(animated: true)
        }else{
//
//            alertViewObj.wrapAlert(title: "", body: "Since you have selected Ticket you cannot go back.If you press Ok your session will expire".localized, img: #imageLiteral(resourceName: "NearMeNew"))
//            alertViewObj.addAction(title: "YES".localized, style: .target , action: {
//                DispatchQueue.main.async {
//                    if let navigationController = self.navigationController{
//                        for controller in navigationController.viewControllers as Array {
//                            if controller.isKind(of: LotteryListViewController.self) {
//                                navigationController.popToViewController(controller, animated: true)
//                                break
//                            }
//                        }
//                    }
                    
                    self.Ticketdelegate?.cartAdded(Countdowntimer: self.totalTime, FromScreen: "TicketByNumber", cartCount: self.cartListArray)
                    self.navigationController?.popViewController(animated: true)

//                }
//            })
//            alertViewObj.addAction(title: "NO".localized, style: .target , action: {
//
//            })
//            alertViewObj.showAlert(controller: self)

        }
        

    }
    @IBAction func historyButtonClick(_ sender: Any) {
    }
    
    @IBAction func cartButtonClick(_ sender: Any) {
        
        if cartcountlabel.isHidden{
            let alert = UIAlertController(title: "Alert", message: "Please add atleast one ticket to  Cart", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            var homeVC: CartViewController?
            homeVC = UIStoryboard(name: LotteryConstant.kLotteryStoryBoard, bundle: nil).instantiateViewController(withIdentifier: LotteryConstant.kCartViewController) as? CartViewController
            if let homeVC = homeVC {
                if let validValue = otherPersonDetails{
                    homeVC.otherPersonDetails = validValue
                }
                homeVC.totalTime = totalTime
                homeVC.ticketListArray = cartListArray
                homeVC.singlePrice = singlePrice
                homeVC.lotteryTicketImage = lotteryTicketImage
                navigationController?.pushViewController(homeVC, animated: true)
            }
        }
        
    }
    
    @IBAction func restButtonClick(_ sender: Any) {
        lotteryCouponDetails.removeAll()
        lotteryTicketsTableView.reloadData()
        timerdisplayView.isHidden = true
        timerHeightConstraint.constant = 0
        infolabel.isHidden = true
        cartcountlabel.isHidden = true
        self.navigationController?.popViewController(animated: false)
    }
    
    
    @IBAction func applyButtonClick(_ sender: Any) {

            if cartListArray.count < 1{
                let alert = UIAlertController(title: "Alert", message: "Please Select atleast one ticket to add cart", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else{
                var homeVC: CartViewController?
                homeVC = UIStoryboard(name: LotteryConstant.kLotteryStoryBoard, bundle: nil).instantiateViewController(withIdentifier: LotteryConstant.kCartViewController) as? CartViewController
                if let homeVC = homeVC {
                    if let validValue = otherPersonDetails{
                        homeVC.otherPersonDetails = validValue
                    }
                    homeVC.totalTime = totalTime
                    homeVC.ticketListArray = cartListArray
                    homeVC.singlePrice = singlePrice
                    homeVC.lotteryTicketImage = lotteryTicketImage
                    navigationController?.pushViewController(homeVC, animated: true)
                }
            }
    }
    
    @objc func checkButtonClicked(_ checkButton: UIButton?){
        
        if cartListArray.count >= 10{
           print("You can select max 10 ticket")
        }else{
            showHideCell(isFromAPI: false,index: checkButton!.tag)
        }
        
    }
    
    func showHideCell(isFromAPI: Bool,index: Int){
        
        let g : NSIndexPath = NSIndexPath(row: index, section: 0)
        
        let cell: LotteryTicketsTableViewCell = lotteryTicketsTableView.cellForRow(at: g as IndexPath) as! LotteryTicketsTableViewCell
        print(cell)
       
        
        //this will work once you uncheck the ticket
    //    if let indexPath = self.lotteryTicketsTableView.indexPath(for: cell) {
            //removing from list the selected ticket
            
            if cell.checkButton.currentImage == UIImage(named: "checkmark"){
            //if self.selectedPaths.contains(indexPath) {
                if self.lotteryCouponDetails.indices.contains(index){
                    
                    //once  uncheck the cell removing it from model and custom obj
                    let coupon = self.lotteryCouponDetails[index].couponNumber
                    
                    //removing it from custom obj
                    for i in 0..<cartListArray.count{
                        if cartListArray[i].couponNumber == coupon{
                            if cartListArray.indices.contains(index){
                                cartListArray.remove(at: i)
                                break
                            }
                        }
                    }
                    
                    //this is commented
                    self.lotteryCouponDetails.remove(at: index)
                  //  self.cartcountlabel.isHidden = false
                    if cartListArray.count > 0 {
                        self.cartcountlabel.text = "\(self.cartListArray.count)"
                    }
                    else{
                        self.cartcountlabel.isHidden = true
                    }
                    
                    
                    self.reservedIndex = nil
                    self.lotteryTicketsTableView.reloadData()
                }
               // self.selectedPaths.remove(indexPath)
            }
            else {
                
                //this case will work once you reserve the ticket and once api will success i am saving the obj
                if isFromAPI{
                 //   self.selectedPaths.insert(indexPath)
                    if let data = reservedIndex{
                        if lotteryCouponDetails.indices.contains(data){
                            //checking if duplicate data is there or not
                            let result = cartListArray.filter({$0.couponNumber == lotteryCouponDetails[data].couponNumber})
                            if result.count == 0{
                                //if duplicate data is not there will save the ticket
                                cartListArray.append(lotteryCouponDetails[data])
                                DispatchQueue.main.async {
                                    if self.isTicketSelected == 1{
                                        self.timerdisplayView.isHidden = false
                                        self.setTimer()
                                    }
                                }
                            }
                        }
                    }
                    
                    let image = UIImage(named: "checkmark")
                    cell.checkButton.setImage(image, for: .normal)
                    reservedIndex = nil
                }else{
                    //this will call to reserve the ticket
                    reservedIndex = index
                    self.reserveTicket(index: index)
                }
            }
      //  }
    }
    
    
    
    
    
    fileprivate func reserveTicket(index: Int){
        
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            lotteryViewModel.reserveTicketDel = self
            lotteryViewModel.reserveTicket(productId: productid,luckyNum: lotteryCouponDetails[index].couponNumber ?? "")
        }else {
            self.noInternetAlert()
            progressViewObj.removeProgressView()
        }
        
    }
    
}

extension TicketByNumberViewController : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lotteryCouponDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = lotteryTicketsTableView.dequeueReusableCell(withIdentifier: "Cell") as? LotteryTicketsTableViewCell else{
            return UITableViewCell()
        }
        
        cell.selectionStyle = .none
        cell.wrapData(coupon: lotteryCouponDetails[indexPath.row].couponNumber ?? "")
        
        var image = UIImage(named: "emptycheck")
//        if self.selectedPaths.contains(indexPath) {
//            image = UIImage(named: "checkmark")
//        }
//        if self.selectedPaths.count >= 9{
//            cell.checkButton.isUserInteractionEnabled = false
//        }
//        else{
//            cell.checkButton.isUserInteractionEnabled = true
//        }
//
        if lotteryCouponDetails.indices.contains(indexPath.row){
            
            if cartListArray.contains(where: {$0.couponNumber ?? "" == lotteryCouponDetails[indexPath.row].couponNumber ?? ""}){
                image = UIImage(named: "checkmark")
            }else{
                
                
            }
            
        }
        
        
        cell.checkButton.setImage(image, for: .normal)
        cell.checkButton.tag = indexPath.row
        cell.checkButton.addTarget(self, action: #selector(self.checkButtonClicked(_:)), for: .touchUpInside)
        
        return cell
    }
    
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         
    }
    
}
    

extension TicketByNumberViewController: GetCouponList {
   
    func coupponList(model: LotteryTicketListModel){
        lotteryCouponDetails.removeAll()
        progressViewObj.removeProgressView()
        
        if !model.isTransactionSuccess{
                //failed
                DispatchQueue.main.async {
                    self.showErrorAlert(errMessage: model.msg ?? "")
                    self.applyButton.isUserInteractionEnabled = false
                }
        }else{
            if model.lotteryCouponDetails.count>0{
                lotteryCouponDetails = model.lotteryCouponDetails
               // selectedPaths.removeAll()
                DispatchQueue.main.async {
                    self.applyButton.isUserInteractionEnabled = true
                    self.lotteryTicketsTableView.delegate = self
                    self.lotteryTicketsTableView.dataSource = self
                    self.lotteryTicketsTableView.reloadData()
                }
            }else{
                DispatchQueue.main.async {
                  self.showErrorAlert(errMessage: "Some error happen please try again".localized)
                }
            }
        }
    }
}


extension TicketByNumberViewController: LotteryReserveTicket{
    
    func reserveTicket(model: NSDictionary?){
        progressViewObj.removeProgressView()
        let value = model
        if let hasValid = value{
            if hasValid.count>0{
                let code = hasValid.value(forKey: "Code") as? Int
                let msg = hasValid.value(forKey: "Msg") as? String
                
                if let isValidCode = code{
                    if isValidCode == 200{
                        DispatchQueue.main.async {
                            if let validData = self.reservedIndex{
                                self.showHideCell(isFromAPI: true,index: validData)
                              //  self.applyButton.setTitle("ADD TO CART", for: .normal)
                                self.timerdisplayView.isHidden = false
                                self.timerHeightConstraint.constant = 60
                                self.infolabel.isHidden = false
                                self.cartcountlabel.isHidden = false
                                self.cartcountlabel.text = "\(self.cartListArray.count)"
                                self.isTicketSelected = self.isTicketSelected + 1
                            }
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.showErrorAlert(errMessage: msg ?? "")
                        }
                    }
                }
            }else{
                DispatchQueue.main.async {
                    self.showErrorAlert(errMessage: "Some error happen please try again".localized)
                }
            }
        }
    }
    
}



