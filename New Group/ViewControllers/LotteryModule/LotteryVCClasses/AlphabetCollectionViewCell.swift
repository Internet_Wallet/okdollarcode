//
//  AlphabetCollectionViewCell.swift
//  LotteryModule
//
//  Created by iMac on 21/09/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class AlphabetCollectionViewCell: UICollectionViewCell {
    @IBOutlet var alphabetLabel: UILabel!
}
