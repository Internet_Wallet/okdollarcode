//
//  CartViewController.swift
//  LotteryModule
//
//  Created by iMac on 19/09/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CartViewController: OKBaseController {
    
      @IBOutlet var navigationView: UIView!
      
      @IBOutlet var navigationTitleLabel: UILabel!
      
      @IBOutlet var backButton: UIButton!
      
      @IBOutlet var historyButton: UIButton!
      
      @IBOutlet var cartButton: UIButton!
      @IBOutlet var bottomView: UIView!
      
      @IBOutlet var buymoreButton: UIButton!
      @IBOutlet var payButton: UIButton!

      @IBOutlet var timerdisplayView: UIView!
      @IBOutlet var minutesLabel: UILabel!
      @IBOutlet var secsLabel: UILabel!
    var singlePrice: String?
    @IBOutlet var cartcountlabel: UILabel!
    var isButtonTapped = false
    @IBOutlet var deleteButton: UIButton!
    
    @IBOutlet var checkButton: UIButton!
    var otherPersonDetails: OtherPersonDetails?
    @IBOutlet var cartTableView: UITableView!
    var ticketListArray = [LotteryCouponDetails]()
    var ticketMainArray = [[String : Any]]()
    var productid = ""
    var parameters:[String : Any] = [:]
    var selectedTicket = NSMutableDictionary()
    var isAllSelectedTicket = false
    var lotteryViewModel = LotteryViewModel()
    var lotteryTicketImage: String?
    var timerQR: Timer?
    var totalTime = 0
    var timeAddedFromServer = 0
    var previousTime = Date()
    var currentTime = Date()
    var previoushour = 0
    var previousMinute = 0
    var previousSecond = 0
    var currentHour = 0
    var currentMinute = 0
    var currentSecond = 0
    var wentBackGround = false
    var formatter = DateFormatter()
    

    override func viewDidLoad() {
        super.viewDidLoad()
              
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "hh:mm:ss"
        NotificationCenter.default.addObserver(self, selector: #selector(moveToBackGround), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(enterForgound), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        timerdisplayView.layer.cornerRadius = 10
        minutesLabel.layer.cornerRadius = 4.0
        minutesLabel.layer.masksToBounds = true
        minutesLabel.layer.borderColor = UIColor.lightGray.cgColor
        minutesLabel.layer.borderWidth = 0.5
        
        secsLabel.layer.cornerRadius = 4.0
        secsLabel.layer.masksToBounds = true
        secsLabel.layer.borderColor = UIColor.lightGray.cgColor
        secsLabel.layer.borderWidth = 0.5
        
        cartcountlabel.layer.cornerRadius = 10
        cartcountlabel.layer.masksToBounds = true
        
        cartcountlabel.text = "\(ticketListArray.count)"
    
        cartTableView.reloadData()
        setTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        if let timer = self.timerQR {
//            timer.invalidate()
//        }
    }
    
    
    
    @objc func moveToBackGround() {
        let date = Date()
        previousTime = date
        let str = formatter.string(from: previousTime)
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: formatter.date(from: str) ?? Date())
        let minutes = calendar.component(.minute, from: formatter.date(from: str) ?? Date())
        let second = calendar.component(.second, from: formatter.date(from: str) ?? Date())
        previoushour = hour
        previousMinute = minutes
        previousSecond = second
        wentBackGround = true
    }
    
    @objc func enterForgound() {
        if wentBackGround {
            diffTime()
            wentBackGround = false
        }
    }
    
    func diffTime() {
        let str = formatter.string(from: Date())
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: formatter.date(from: str) ?? Date())
        let minutes = calendar.component(.minute, from: formatter.date(from: str) ?? Date())
        let second = calendar.component(.second, from: formatter.date(from: str) ?? Date())
        currentHour = hour
        currentMinute = minutes
        currentSecond = second
        
        if currentHour < previoushour {
            currentMinute = currentMinute + 60
        }
        if currentMinute < previousMinute {
            currentSecond = currentSecond + 60
        }
        let finalMin = currentMinute - previousMinute
        let finalSecond = currentSecond - previousSecond
        totalTime =  totalTime - (finalMin * 60 + finalSecond)
    }
    
    
    fileprivate func setTimer(){
        let value = totalTime/60

        if value<10{
            minutesLabel.text = "\(0)\(value):00"
        }else{
            minutesLabel.text = "\(value):00"
        }

        self.startOtpTimer(time: totalTime - 1)
    }
    
    private func startOtpTimer(time: Int) {
        //5 min 300 sec
        self.totalTime = time
        self.timerQR = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        self.minutesLabel.text = self.timeFormatted(self.totalTime)
        
        if totalTime > 0 {
            totalTime -= 1
        }else {
            if let timer = self.timerQR {
                timer.invalidate()
                alertViewObj.wrapAlert(title: "", body: "Session Expired.Do You want to book more".localized, img: #imageLiteral(resourceName: "NearMeNew"))
                alertViewObj.addAction(title: "YES".localized, style: .target , action: {
                    self.totalTime = self.timeAddedFromServer
                    DispatchQueue.main.async {
                        if let navigationController = self.navigationController{
                            for controller in navigationController.viewControllers as Array {
                                    if controller.isKind(of: LotteryListViewController.self) {
                                        navigationController.popToViewController(controller, animated: true)
                                        break
                                    }
                            }
                        }
                    }
                })
                alertViewObj.addAction(title: "NO".localized, style: .target , action: {
                    self.navigationController?.navigationBar.isHidden = false
                    self.navigationController?.popToRootViewController(animated: false)
                })
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%01d:%02d", minutes, seconds)
    }
    
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

//
//            alertViewObj.wrapAlert(title: "", body: "Since you have selected Ticket you cannot go back.If you press Ok your session will expire".localized, img: #imageLiteral(resourceName: "NearMeNew"))
//            alertViewObj.addAction(title: "YES".localized, style: .target , action: {
//                DispatchQueue.main.async {
//                    if let navigationController = self.navigationController{
//                        for controller in navigationController.viewControllers as Array {
//                            if controller.isKind(of: LotteryListViewController.self) {
//                                navigationController.popToViewController(controller, animated: true)
//                                break
//                            }
//                        }
//                    }
//                }
//            })
//            alertViewObj.addAction(title: "NO".localized, style: .target , action: {
//
//            })
//            alertViewObj.showAlert(controller: self)

        
    }
    @IBAction func historyButtonClick(_ sender: Any) {
    }
    @IBAction func checkButtonClick(_ sender: Any) {
        
        if !isAllSelectedTicket{
            isAllSelectedTicket = true
            checkButton.setImage(UIImage(named: "checkmark"), for: .normal)
        }else{
            isAllSelectedTicket = false
            checkButton.setImage(UIImage(named: "emptycheck"), for: .normal)
        }
        
        cartTableView.reloadData()
        
    }
    
    @IBAction func deleteButtonClick(_ sender: Any) {
                
        
        if isAllSelectedTicket {
            ticketListArray.removeAll()
        }else{
            
            let dicKey = selectedTicket.allKeys
            
            print(dicKey)

            for i in 0..<dicKey.count{
                let index = dicKey[i] as? String
                if let indexNew  = index{
                    if let validIndex = Int(indexNew){
                        if ticketListArray.indices.contains(validIndex){
                            ticketListArray.remove(at: validIndex)
                        }
                    }
                }
            }
        }
        
        selectedTicket.removeAllObjects()
        
        if ticketListArray.count == 0{
                      
            alertViewObj.wrapAlert(title:"", body: "You Do not have any reserve ticket.Do You want to purchase new tickets".localized, img: UIImage(named: "alert-icon"))
            alertViewObj.addAction(title: "Ok", style: .target , action: {
                if let navigationController = self.navigationController{
                    for controller in navigationController.viewControllers as Array {
                        if controller.isKind(of: LotteryListViewController.self) {
                            navigationController.popToViewController(controller, animated: true)
                            break
                        }
                    }
                }
            })
            alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
                self.navigationController?.navigationBar.isHidden = false
                self.navigationController?.popToRootViewController(animated: true)
            }
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
            
        }else{
            cartTableView.reloadData()
        }
        
    }
    
    @IBAction func buymoreButtonClick(_ sender: Any) {
    }
    @IBAction func payButtonClick(_ sender: Any) {
    
        
        if let value = singlePrice{
            let singleAmount = Int(value)
            if let final = singleAmount{
                let amount = final * ticketListArray.count
                let total = String(amount)
                if !isButtonTapped {
                    
                    let balanceAmount = (UserLogin.shared.walletBal as NSString).floatValue
                    let billAmountIntValue = (total as NSString).floatValue
                    
                    if balanceAmount>=billAmountIntValue{
                        //then only deduct the money
                        if appDelegate.checkNetworkAvail() {
                            progressViewObj.showProgressView()
                            self.isButtonTapped = true
                           //call service here
                            OKPayment.main.authenticate(screenName: "Tushar", delegate: self)
                        } else {
                            self.isButtonTapped = false
                            self.noInternetAlert()
                            progressViewObj.removeProgressView()
                        }
                   }else{
                        //show alert
                        self.showErrorAlert(errMessage: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized)
                    }
                }
                
            }
        }
    }

  
}


extension CartViewController: BioMetricLoginDelegate{
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String){
        if isSuccessful{
            if appDelegate.checkNetworkAvail() {
                progressViewObj.showProgressView()
                lotteryViewModel.reserveTicketDel = self
                lotteryViewModel.verifyAllNumber(ticketList: ticketListArray, amount: singlePrice ?? "")
            }else {
                self.noInternetAlert()
                progressViewObj.removeProgressView()
            }
        }
    }
}


extension CartViewController : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return ticketListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier: String = "Cell"
        
        var cell:CartTableViewCell? = self.cartTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CartTableViewCell
        
        
        if cell == nil {
            cell = CartTableViewCell(style: .default, reuseIdentifier: cellIdentifier)
        }
        cell?.selectionStyle = .none
        
        cell?.delegate = self
        cell?.checkButton.tag = indexPath.row
        
        let ticketnum = ticketListArray[indexPath.row].couponNumber ?? ""
        
        let ansTest = ticketnum.enumerated().compactMap({ ($0 > 0) && ($0 % 1 == 0) ? "  \($1)" : "\($1)" }).joined()
        
        cell?.ticketNumberLabel.text = ansTest
        
        cell?.layer.cornerRadius = 7.0;
        cell?.layer.masksToBounds = true
        
         
        
          cell?.checkButton.tag = indexPath.row
        
        
        if isAllSelectedTicket{
            cell?.checkButton.setImage(UIImage(named: "checkmark"), for: .normal)
        }else{
            if selectedTicket.count>0{
                let value = selectedTicket.value(forKey: "\(indexPath.row)") as? Bool
                if let isValidData = value{
                    if isValidData{
                        cell?.checkButton.setImage(UIImage(named: "checkmark"), for: .normal)
                    }else{
                        cell?.checkButton.setImage(UIImage(named: "emptycheck"), for: .normal)
                    }
                }else{
                    cell?.checkButton.setImage(UIImage(named: "emptycheck"), for: .normal)
                }
                
            }else{
                cell?.checkButton.setImage(UIImage(named: "emptycheck"), for: .normal)
            }
        }
       
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       
    }
    
}


extension CartViewController: DeleteSelectedTicket {
  
    func getIndexToDelete(index: Int){
                
        if selectedTicket.count>0{
            let value = selectedTicket.value(forKey: "\(index)") as? Bool
            if let isValidData = value{
                if isValidData{
                    selectedTicket.setValue(false, forKey: "\(index)")
                }else{
                    selectedTicket.setValue(true, forKey: "\(index)")
                }
            }else{
                selectedTicket.setValue(true, forKey: "\(index)")
            }
        }else{
            //this will work for the first time
            selectedTicket.setValue(true, forKey: "\(index)")
            
        }
        
        cartTableView.reloadData()
        
    }
}


extension CartViewController: LotteryReserveTicket{
    
    func reserveTicket(model: NSDictionary?){
        progressViewObj.removeProgressView()
        let value = model
        if let hasValid = value{
            if hasValid.count>0{
                let code = hasValid.value(forKey: "Code") as? Int
                 let msg = hasValid.value(forKey: "Msg") as? String
                
                if let isValidCode = code{
                    if isValidCode == 200{
                        DispatchQueue.main.async {
                                var homeVC: CheckDetailsViewController?
                                homeVC = UIStoryboard(name: LotteryConstant.kLotteryStoryBoard, bundle: nil).instantiateViewController(withIdentifier: LotteryConstant.kCheckDetailsViewController) as? CheckDetailsViewController
                                if let homeVC = homeVC {
                                    if let validValue = self.otherPersonDetails{
                                        homeVC.otherPersonDetails = validValue
                                    }
                                    
                                    //didnot make model because just taking single Value
                                   
                                    let data = hasValid.value(forKey: "Data") as? String
                                    if let validObj = data{
                                        if let list = UitilityClass.convertStringToArrayOfDictionary(text: validObj) as? NSDictionary {
                                            if list.count>0{
                                                homeVC.singlePrice = list.value(forKey: "TotalAmount") as? String ?? ""
                                            }
                                        }
                                    }
                                    
                                    
                                    
                                    
                                    homeVC.totalTime =  self.totalTime
                                    homeVC.confirmationticketListArray = self.ticketListArray
                                    homeVC.lotteryTicketImage = self.lotteryTicketImage
                                    self.navigationController?.pushViewController(homeVC, animated: true)
                                }
                        }
                    }else{
                        DispatchQueue.main.async {
                          self.showErrorAlert(errMessage: msg ?? "")
                        }
                    }
                }
        }else{
            DispatchQueue.main.async {
              self.showErrorAlert(errMessage: "Some error happen please try again".localized)
            }
        }
        
        }
    }
    
}
