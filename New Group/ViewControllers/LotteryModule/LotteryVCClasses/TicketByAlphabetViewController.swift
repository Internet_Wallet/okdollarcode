//
//  TicketByAlphabetViewController.swift
//  LotteryModule
//
//  Created by iMac on 21/09/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit


protocol TicketByAlphabetDelegate : class {
    func cartAdded(Countdowntimer: Int, FromScreen: String, cartCount: [LotteryCouponDetails])
}

class TicketByAlphabetViewController: OKBaseController, TicketByAlphabetDelegate {
    
    @IBOutlet var navigationView: UIView!
    
    @IBOutlet var ticketNumberCollectionView: UICollectionView!
    @IBOutlet var navigationTitleLabel: UILabel!
    
    @IBOutlet var backButton: UIButton!
    
    @IBOutlet var historyButton: UIButton!
    
    @IBOutlet var cartButton: UIButton!
    @IBOutlet var bottomView: UIView!
    
    @IBOutlet var resetButton: UIButton!
    @IBOutlet var applyButton: UIButton!
    
    @IBOutlet var fourthLabel: UILabel!
    @IBOutlet var selectionletterView: UIView!
    @IBOutlet var firstalphabetLabel: UILabel!
    
    @IBOutlet var thirdLabel: UILabel!
    @IBOutlet var secondalphabetLabel: UILabel!
    @IBOutlet var alphabetCollectionView: UICollectionView!
    
    @IBOutlet var crossButton: UIButton!
    
    @IBOutlet var timerdisplayView: UIView!
    @IBOutlet var minutesdisplayLabel: UILabel!
    @IBOutlet var secsdisplayLabel: UILabel!
    @IBOutlet var timerViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var cartCountLabel: UILabel!
    
    var cartListArray = [LotteryCouponDetails]()
    
    var ticketNumArray = [String]()
    var timeFromServer = 0
    var companyDic: LotteryCompanyDetails?
    var productid = ""
    var singlePrice: String?
    var lotteryTicketImage: String?
    
    var timerQR: Timer?
    var totalTime = 0
    var timeAddedFromServer = 0
    var previousTime = Date()
    var currentTime = Date()
    var previoushour = 0
    var previousMinute = 0
    var previousSecond = 0
    var currentHour = 0
    var currentMinute = 0
    var currentSecond = 0
    var wentBackGround = false
    var formatter = DateFormatter()
    
    
    
   // let alphabetarray = [ "က", "ခ" ,"ဂ", "ဃ", "င", "စ", "ဆ", "ဇ", "ဈ" ,"ည", "ဋ", "ဌ", "ဍ", "ဎ", "ဏ", "တ", "ထ", "ဒ", "ဓ", "န", "ပ", "ဖ", "ဗ", "ဘ", "မ", "ယ", "ရ", "လ", "ဝ", "သ", "ဟ", "ဠ", "အ"]
    
    
    let alphabetarray = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    
    
  //  let numberArray = [ "၀", "၁"," ၂"," ၃"," ၄"," ၅"," ၆", "၇" ,"၈"," ၉"]
    
    let numberArray = [ "0", "1","2","3","4","5","6", "7" ,"8","9"]
    
    
    var inSelectedRow : Int?
    var otherPersonDetails: OtherPersonDetails?
    
    var arrSelectedData = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inSelectedRow = -1
        bottomView.isHidden = true
        alphabetCollectionView.allowsMultipleSelection =  true
        
        firstalphabetLabel.layer.cornerRadius = 4.0
        firstalphabetLabel.layer.masksToBounds = true
        
        secondalphabetLabel.layer.cornerRadius = 4.0
        secondalphabetLabel.layer.masksToBounds = true
      
        bottomView.layer.shadowColor = UIColor.gray.cgColor
        bottomView.layer.shadowOffset = .zero
        bottomView.layer.shadowOpacity = 0.8
        bottomView.layer.shadowRadius = 10
        selectionletterView.isHidden = true
        crossButton.isHidden = true
        
        minutesdisplayLabel.layer.cornerRadius = 4.0
        minutesdisplayLabel.layer.masksToBounds = true
        minutesdisplayLabel.layer.borderColor = UIColor.lightGray.cgColor
        minutesdisplayLabel.layer.borderWidth = 0.5
        
        secsdisplayLabel.layer.cornerRadius = 4.0
        secsdisplayLabel.layer.masksToBounds = true
        secsdisplayLabel.layer.borderColor = UIColor.lightGray.cgColor
        secsdisplayLabel.layer.borderWidth = 0.5
        
        timerdisplayView.isHidden = true
        timerViewHeightConstraint.constant = 0
        
        if cartListArray.count < 1 {
            self.cartCountLabel.isHidden = true
        }
        else{
            self.cartCountLabel.isHidden = false
        }
       
        cartCountLabel.layer.cornerRadius = 10
        cartCountLabel.layer.masksToBounds = true
        
        
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "hh:mm:ss"
        NotificationCenter.default.addObserver(self, selector: #selector(moveToBackGround), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(enterForgound), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        if let timer = self.timerQR {
//            timer.invalidate()
//        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.bottomView.isHidden = true
        UIView.transition(with: selectionletterView, duration: 0.4,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.selectionletterView.isHidden = true
                            self.crossButton.isHidden = true
                            
                          })
        ticketNumArray.removeAll()
        alphabetCollectionView.reloadData()
        ticketNumberCollectionView.reloadData()
    }
    
    @objc func moveToBackGround() {
        let date = Date()
        previousTime = date
        let str = formatter.string(from: previousTime)
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: formatter.date(from: str) ?? Date())
        let minutes = calendar.component(.minute, from: formatter.date(from: str) ?? Date())
        let second = calendar.component(.second, from: formatter.date(from: str) ?? Date())
        previoushour = hour
        previousMinute = minutes
        previousSecond = second
        wentBackGround = true
    }
    
    @objc func enterForgound() {
        if wentBackGround {
            diffTime()
            wentBackGround = false
        }
    }
    
    func diffTime() {
        let str = formatter.string(from: Date())
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: formatter.date(from: str) ?? Date())
        let minutes = calendar.component(.minute, from: formatter.date(from: str) ?? Date())
        let second = calendar.component(.second, from: formatter.date(from: str) ?? Date())
        currentHour = hour
        currentMinute = minutes
        currentSecond = second
        
        if currentHour < previoushour {
            currentMinute = currentMinute + 60
        }
        if currentMinute < previousMinute {
            currentSecond = currentSecond + 60
        }
        let finalMin = currentMinute - previousMinute
        let finalSecond = currentSecond - previousSecond
        totalTime =  totalTime - (finalMin * 60 + finalSecond)
    }
    
    
    fileprivate func setTimer(){
        let value = totalTime/60

        if value<10{
            minutesdisplayLabel.text = "\(0)\(value):00"
        }else{
            minutesdisplayLabel.text = "\(value):00"
        }

       // timeLabel.text  = "05:00"

        self.startOtpTimer(time: totalTime - 1)
    }
    
    private func startOtpTimer(time: Int) {
        //5 min 300 sec
        self.totalTime = time
        self.timerQR = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        self.minutesdisplayLabel.text = self.timeFormatted(self.totalTime)
        
        if totalTime > 0 {
            totalTime -= 1
        }else {
            if let timer = self.timerQR {
                timer.invalidate()
                alertViewObj.wrapAlert(title: "", body: "Session Expired.Do You want to book more".localized, img: #imageLiteral(resourceName: "NearMeNew"))
                alertViewObj.addAction(title: "YES".localized, style: .target , action: {
                    self.totalTime = self.timeAddedFromServer
                    DispatchQueue.main.async {
                        if let navigationController = self.navigationController{
                            for controller in navigationController.viewControllers as Array {
                                if controller.isKind(of: LotteryListViewController.self) {
                                    navigationController.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                        }
                    }
                })
                alertViewObj.addAction(title: "NO".localized, style: .target , action: {                    self.navigationController?.navigationBar.isHidden = false
                    self.navigationController?.popToRootViewController(animated: false)
                })
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%01d:%02d", minutes, seconds)
    }
    
    func cartAdded(Countdowntimer: Int, FromScreen: String, cartCount: [LotteryCouponDetails]) {
        if cartCount.count > 0 {
            cartListArray.removeAll()
            cartCountLabel.isHidden = false
            cartCountLabel.text = "\(cartCount.count)"
            cartListArray.append(contentsOf: cartCount)
            totalTime = Countdowntimer
            self.setTimer()
            timerdisplayView.isHidden = false
            timerViewHeightConstraint.constant = 45
        }
        else{
            cartCountLabel.isHidden = true
            timerdisplayView.isHidden = true
            timerViewHeightConstraint.constant = 0
        }
        
        
    }
    
    
    @IBAction func backButtonClick(_ sender: Any) {
        if let timer = self.timerQR {
            timer.invalidate()
        }
         self.navigationController?.popViewController(animated: true)

     }
    
    @IBAction func cartButtonClick(_ sender: Any) {
        
        if cartCountLabel.isHidden{
            let alert = UIAlertController(title: "Alert", message: "Please add atleast one ticket to Cart", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            var homeVC: CartViewController?
            homeVC = UIStoryboard(name: LotteryConstant.kLotteryStoryBoard, bundle: nil).instantiateViewController(withIdentifier: LotteryConstant.kCartViewController) as? CartViewController
            if let homeVC = homeVC {
                if let validValue = otherPersonDetails{
                    homeVC.otherPersonDetails = validValue
                }
                homeVC.totalTime = totalTime
                homeVC.ticketListArray = cartListArray
                homeVC.singlePrice = singlePrice
                homeVC.lotteryTicketImage = lotteryTicketImage
                navigationController?.pushViewController(homeVC, animated: true)
            }
        }
        
    }
    
     @IBAction func historyButtonClick(_ sender: Any) {
        
     }
    @IBAction func crossbuttonClick(_ sender: Any) {
        switch ticketNumArray.count {
        case 1:
            ticketNumArray.remove(at: 0)
            firstalphabetLabel.text = ""
            firstalphabetLabel.isHidden = true
            self.crossButton.isHidden = true
            self.selectionletterView.isHidden = true
        case 2:
            ticketNumArray.remove(at: 1)
            secondalphabetLabel.text = ""
            firstalphabetLabel.isHidden = false
            secondalphabetLabel.isHidden = true
           
        case 3:
            ticketNumArray.remove(at: 2)
            thirdLabel.text = ""
            firstalphabetLabel.isHidden = false
            secondalphabetLabel.isHidden = false
            thirdLabel.isHidden = true
        case 4:
            ticketNumArray.remove(at: 3)
            fourthLabel.text = ""
            firstalphabetLabel.isHidden = false
            secondalphabetLabel.isHidden = false
            thirdLabel.isHidden = false
            fourthLabel.isHidden = true
            
        default:
            break
        }
        
        
    }
    
    //copied from internet
    func findIntersection (firstArray : [String], secondArray : [String]) -> [String]
    {
        return [String](Set<String>(firstArray).intersection(secondArray))
    }
    
    
    func appendDataInLabel(stringArr: [String]){
        firstalphabetLabel.isHidden = true
        secondalphabetLabel.isHidden = true
        thirdLabel.isHidden = true
        fourthLabel.isHidden = true
        
        if stringArr.count>0{
            
            UIView.transition(with: selectionletterView, duration: 0.4,
                              options: .transitionCrossDissolve,
                              animations: {
                                self.selectionletterView.isHidden = false
                                self.crossButton.isHidden = false
                          })
            
            switch ticketNumArray.count {
            case 1:
                firstalphabetLabel.text = "\(stringArr[0])"
                firstalphabetLabel.isHidden = false
            case 2:
                
                secondalphabetLabel.text = "\(stringArr[1])"
                firstalphabetLabel.isHidden = false
                secondalphabetLabel.isHidden = false
            case 3:
                thirdLabel.text = "\(stringArr[2])"
                firstalphabetLabel.isHidden = false
                secondalphabetLabel.isHidden = false
                thirdLabel.isHidden = false
            case 4:
                fourthLabel.text = "\(stringArr[3])"
                firstalphabetLabel.isHidden = false
                secondalphabetLabel.isHidden = false
                thirdLabel.isHidden = false
                fourthLabel.isHidden = false
            default:
                break
            }
            
        }
        
    }
   
    @IBAction func restButtonClick(_ sender: Any) {
        self.bottomView.isHidden = true
        UIView.transition(with: selectionletterView, duration: 0.4,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.selectionletterView.isHidden = true
                            self.crossButton.isHidden = true
                          })
        ticketNumArray.removeAll()
        ticketNumberCollectionView.reloadData()
        alphabetCollectionView.reloadData()
    }
    
    
     @IBAction func applyButtonClick(_ sender: Any) {
        
        if ticketNumArray.count>0{
            var homeVC: TicketByNumberViewController?
            homeVC = UIStoryboard(name: LotteryConstant.kLotteryStoryBoard, bundle: nil).instantiateViewController(withIdentifier: LotteryConstant.kTicketByNumberViewController) as? TicketByNumberViewController
            if let homeVC = homeVC {
                
                if let validValue = otherPersonDetails{
                    homeVC.otherPersonDetails = validValue
                }
                homeVC.productid = productid
                homeVC.companyDic = companyDic
                homeVC.singlePrice = singlePrice
                homeVC.tickArray = ticketNumArray
                if cartListArray.count < 1{
                    homeVC.timeAddedFromServer = timeFromServer
                }
                else{
                    homeVC.timeAddedFromServer = totalTime
                }
                homeVC.lotteryTicketImage = lotteryTicketImage
                homeVC.cartListArray = cartListArray
                homeVC.Ticketdelegate = self
                navigationController?.pushViewController(homeVC, animated: true)
                
                
            }
        }
        else{
            alertViewObj.wrapAlert(title: "Lottery", body: "Please Select atleast one Alphabet/Number".localized, img: #imageLiteral(resourceName: "exclaim"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                DispatchQueue.main.async {
                    
                }
            })
            alertViewObj.showAlert(controller: self)
        }
        
     }
    
    func validateArray(arrayToValidate: String,msg: String,collection: UICollectionView){
       
        var localArray = [String]()
        
        
        if collection == alphabetCollectionView{
            localArray = alphabetarray
        }else{
            localArray = numberArray
        }
        
        
        if ticketNumArray.filter({$0 == arrayToValidate}).count == 0{
                    
            if findIntersection(firstArray: ticketNumArray, secondArray: localArray).count == 2 || ticketNumArray.filter(localArray.contains).count == 2{
                showErrorAlert(errMessage: msg)
            }else{
                ticketNumArray.append(arrayToValidate)
                ticketNumberCollectionView.reloadData()
                appendDataInLabel(stringArr: ticketNumArray)
            }
            
        }else if ticketNumArray.filter({$0 == arrayToValidate}).count == 1{
            if findIntersection(firstArray: ticketNumArray, secondArray: localArray).count == 2{
                showErrorAlert(errMessage: msg)
            }else{
                ticketNumArray.append(arrayToValidate)
                ticketNumberCollectionView.reloadData()
                appendDataInLabel(stringArr: ticketNumArray)
            }
        }else if ticketNumArray.filter({$0 == arrayToValidate}).count == 2{
            if findIntersection(firstArray: ticketNumArray, secondArray: localArray).count == 1{
                showErrorAlert(errMessage: msg)
            }else{
                ticketNumArray.append(arrayToValidate)
                ticketNumberCollectionView.reloadData()
                appendDataInLabel(stringArr: ticketNumArray)
            }
        }
        
        if ticketNumArray.count>0{
            UIView.transition(with: bottomView, duration: 0.4,
                              options: .transitionCrossDissolve,
                              animations: {
                                self.bottomView.isHidden = false
                              })
        }
    }
 
}
extension TicketByAlphabetViewController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == ticketNumberCollectionView{
            return numberArray.count
        }else{
            return alphabetarray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "alphabetcell", for: indexPath) as? AlphabetCollectionViewCell
        else {
            return UICollectionViewCell()
        }
        
        
        cell.contentView.layer.cornerRadius = 7.0
        cell.contentView.layer.masksToBounds = true
        
        cell.layer.cornerRadius = 7.0
        cell.clipsToBounds = true
        cell.layer.masksToBounds = false
        
        cell.contentView.sizeToFit()
        cell.autoresizesSubviews = true
        cell.alphabetLabel.textAlignment = .center
        if collectionView == ticketNumberCollectionView{
            cell.alphabetLabel.text = numberArray[indexPath.row]
           
            if ticketNumArray.contains(numberArray[indexPath.row]){
                cell.alphabetLabel.textColor = .white
                cell.contentView.backgroundColor = UIColor(red: 99/256, green: 85/256, blue: 181/256, alpha: 1)
                
            }else{
                cell.alphabetLabel.textColor = .black
                cell.contentView.backgroundColor = UIColor(red: 242/256, green: 242/256, blue: 242/256, alpha: 1)
            }
            
        }else{
            cell.alphabetLabel.text = alphabetarray[indexPath.row]
            
            if ticketNumArray.contains(alphabetarray[indexPath.row]){
                cell.alphabetLabel.textColor = .white
                cell.contentView.backgroundColor = UIColor(red: 99/256, green: 85/256, blue: 181/256, alpha: 1)
                
            }else{
                cell.alphabetLabel.textColor = .black
                cell.contentView.backgroundColor = UIColor(red: 242/256, green: 242/256, blue: 242/256, alpha: 1)
            }
        }
        
        
        return cell
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
        return CGSize(width: (UIScreen.main.bounds.width / 10)-3, height: (UIScreen.main.bounds.width / 10)-3 )
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    // Layout: Set Edges
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0) // top, left, bottom, right
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        inSelectedRow = indexPath.row
        
        if collectionView == alphabetCollectionView{
            validateArray(arrayToValidate: alphabetarray[indexPath.item], msg: "You cannot add more than 2 alphabets",collection: alphabetCollectionView)
        }else{
            validateArray(arrayToValidate: numberArray[indexPath.item], msg: "You cannot add more than 2 numbers",collection: ticketNumberCollectionView)
        }
        
        
        
    }
}
