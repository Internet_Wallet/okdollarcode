//
//  LotteryReceiptViewController.swift
//  LotteryModule
//
//  Created by iMac on 21/09/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import SDWebImage

class LotteryReceiptViewController: OKBaseController, NearByPaymentFeedbackDelegate {
 
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var timeLabel: UILabel!
    
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var transactionId: UILabel!
    @IBOutlet var navigationTitleLabel: UILabel!
    
    @IBOutlet var backButton: UIButton!
    
    @IBOutlet var bottomView: UIView!
    
    @IBOutlet var buymoreButton: UIButton!
    @IBOutlet var homeButton: UIButton!
    @IBOutlet var shareButton: UIButton!
    
    @IBOutlet var okName: UILabel!
    @IBOutlet var finalAmountLabel: UILabel!
    
    @IBOutlet var companyLogoImageView: UIImageView!
    @IBOutlet var companyNameLabel: UILabel!
    
    @IBOutlet var okdollaraccountdetailsLabel: UILabel!
    @IBOutlet var lotteryticketsLabel: UILabel!
    @IBOutlet var totlaamountLabel: UILabel!
    
    @IBOutlet var detailsView: UIView!
    @IBOutlet var receiptCollectionView: UICollectionView!
    @IBOutlet var okNumber: UILabel!
    var otherPersonDetails: OtherPersonDetails?
    var finalLotteryModel: LotteryFinalModel?
    var lotteryTicketImage: String?
    
    
    @IBOutlet var lotteryTicketNumber: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationItem.hidesBackButton = true
        
        
        if let validData = otherPersonDetails{
            okName.text = validData.name ?? ""
            okNumber.text = validData.phoneNumer ?? ""
        }else{
            okName.text = UserModel.shared.name
            okNumber.text = UserModel.shared.mobileNo
        }
        
        
        var amout = 0
        if let obj = finalLotteryModel{
            transactionId.text = obj.transactionID
            lotteryTicketNumber.text = obj.totalSuccessCount
            let date = obj.transactionTime?.components(separatedBy: " ")
            dateLabel.text = date?[0]
            
            if ((date?.indices.contains(1)) != nil){
                timeLabel.text = date?[1]
            }
            
            for i in 0..<obj.lotteryFinalPayObj.count{
                if let value = obj.lotteryFinalPayObj[i].amount{
                    let price = Int(value)
                    amout = amout + price!
                }
                
            }
        }
        
        finalAmountLabel.text = "\(amout)  MMK"
        detailsView.layer.cornerRadius = 10
        backButton.isHidden = true
      
//        bottomView.layer.shadowColor = UIColor.gray.cgColor
//        bottomView.layer.shadowOffset = .zero
//        bottomView.layer.shadowOpacity = 0.8
//        bottomView.layer.shadowRadius = 10
//
//        homeButton.layer.cornerRadius = 20
//        buymoreButton.layer.cornerRadius = 20
//
    }
    @IBAction func backButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareButtonClick(_ sender: Any) {
    }
    
    @IBAction func buymoreButtonClick(_ sender: Any) {
        if let navigationController = self.navigationController{
            for controller in navigationController.viewControllers as Array {
                if controller.isKind(of: LotteryListViewController.self) {
                    navigationController.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    
    @IBAction func homeButtonClick(_ sender: Any) {
        self.navigationController?.navigationBar.isUserInteractionEnabled = false
        guard  let vc = UIStoryboard.init(name: "NearMePaymentGeneric", bundle: nil).instantiateViewController(withIdentifier: "NearByPaymentFeedbackVC") as? NearByPaymentFeedbackVC else {return}
        vc.delegate = self
        addChild(vc)
        vc.view.frame = self.view.bounds
        vc.didMove(toParent: self)
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.reveal
        vc.view.layer.add(transition, forKey: nil)
        view.addSubview(vc.view)
        
     //   self.navigationController?.popToRootViewController(animated: false)
    }
    
    func returnFeedback(feedback: String) {
        println_debug("returnFeedback submitted")
        self.navigationController?.navigationBar.isUserInteractionEnabled = true
        if UserModel.shared.agentType == .user {
            if NSString(string: UserLogin.shared.walletBal).floatValue < 200000.0{
                self.addMoney()
            }
            else{
             self.navigationController?.navigationBar.isHidden = false
             self.navigationController?.popToRootViewController(animated: false)
            }
        } else if UserModel.shared.agentType == .merchant {
           if NSString(string: UserLogin.shared.walletBal).floatValue < 500000.0{
                self.addMoney()
            }
           else{
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.popToRootViewController(animated: false)
           }
        } else if UserModel.shared.agentType == .agent {
            if NSString(string: UserLogin.shared.walletBal).floatValue < 1000000.0{
                self.addMoney()
            }
            else{
            self.navigationController?.navigationBar.isHidden = false
             self.navigationController?.popToRootViewController(animated: false)
            }
        }else{
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.popToRootViewController(animated: false)
            print("navigate to dashboard")
        }
    }
    
    // wallet balance if zero
   func addMoney() {
       alertViewObj.wrapAlert(title: nil, body: "Your OK$ balance is low. Please recharge.".localized, img: #imageLiteral(resourceName: "error"))
       alertViewObj.addAction(title: "OK".localized, style: .target, action: {
           self.navigationController?.navigationBar.isHidden = false
           self.navigationController?.popToRootViewController(animated: true)
       })
       alertViewObj.addAction(title: "CASH IN".localized, style: .target, action: {
           
           if let objCashIn = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
               objCashIn.modalPresentationStyle = .fullScreen
               self.navigationController?.present(objCashIn, animated: true, completion: nil)
           }
       })
       alertViewObj.showAlert()
   }

  
}
extension LotteryReceiptViewController: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let obj = finalLotteryModel{
            return obj.lotteryFinalPayObj.count
        }else{
            return 0
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ticketreceiptCell", for: indexPath) as! LotteryReceiptCollectionViewCell
        
        if let obj = finalLotteryModel{
            cell.ticketNumberLabel.text = obj.lotteryFinalPayObj[indexPath.row].luckyNumber
            cell.receiptTicketImageView.sd_setImage(with: URL(string: lotteryTicketImage ?? ""), placeholderImage: nil)
        }
        
        
     
        return cell
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        // CGSize mElementSize = CGSizeMake(self.view.frame.size.width/3-10, self.view.frame.size.width/3-10);
        //  if ([[UIDevice currentDevice] userInterfaceIdiom] ==UIUserInterfaceIdiomPhone) {
        
        //    let collectionWidth = collectionView.bounds.width
        
        
        //    if collectionView == gridCollectionView {
        
        return CGSize(width: (collectionView.frame.size.width / 3)-2, height: (collectionView.frame.size.height / 1)-2 )
        
       
        
//        return CGSize(width: (UIScreen.main.bounds.width-10)/2.2,height: (UIScreen.main.bounds.width-10)/2.2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    
    // Layout: Set Edges
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0) // top, left, bottom, right
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    }
}
