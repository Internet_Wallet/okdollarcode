//
//  LotteryPriceViewController.swift
//  LotteryModule
//
//  Created by iMac on 30/09/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit


class LotteryPriceViewController: OKBaseController {
    
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var navigationTitleLabel: UILabel!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var lotterypriceTableView: UITableView!
    
    var priceListArray = [[String : Any]]()
    var parameters:[String : Any] = [:]
    var companyiddict: LotteryCompanyDetails?
    var priceListModel = [LotteryCompanyPriceDetails]()
        
    var lotteryViewModel = LotteryViewModel()
    

    override func viewDidLoad() {
        super.viewDidLoad()
       navigationTitleLabel.text = companyiddict?.companyName ?? ""
        self.apicall()
        
    }
    
    
    func apicall(){
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            lotteryViewModel.companyPriceListDel = self
            lotteryViewModel.getProductList(companyId: companyiddict?.companyID ?? "")
        }else {
            self.noInternetAlert()
            progressViewObj.removeProgressView()
        }
    }
    
    
    @IBAction func backButtonClick(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }
  
}

extension LotteryPriceViewController : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return priceListModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = lotterypriceTableView.dequeueReusableCell(withIdentifier: "Cell") as? LotteryPriceTableViewCell else{
            return UITableViewCell()
        }
               
        cell.selectionStyle = .none
        cell.wrapDataInCell(priceValue: priceListModel[indexPath.row].price ?? "")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//
//        var homeVC: TicketByAlphabetViewController?
//        homeVC = UIStoryboard(name:  LotteryConstant.kLotteryStoryBoard, bundle: nil).instantiateViewController(withIdentifier: LotteryConstant.kTicketByAlphabetViewController) as? TicketByAlphabetViewController
//        if let homeVC = homeVC {
//            homeVC.productid = priceListModel[indexPath.row].productId ?? ""
//            homeVC.timeFromServer = Int(priceListModel[indexPath.row].sessionTimeOut ?? "0") ?? 0
//            homeVC.companyDic = companyiddict
//            homeVC.singlePrice = priceListModel[indexPath.row].price ?? ""
//            homeVC.lotteryTicketImage = priceListModel[indexPath.row].lotteryticketImage ?? ""
//            navigationController?.pushViewController(homeVC, animated: true)
//        }
        
        
        var homeVC: ChooseOptionViewController?
        homeVC = UIStoryboard(name: LotteryConstant.kLotteryStoryBoard, bundle: nil).instantiateViewController(withIdentifier: "ChooseOptionViewController") as? ChooseOptionViewController
        if let homeVC = homeVC {
            homeVC.productid = priceListModel[indexPath.row].productId ?? ""
            homeVC.timeFromServer = Int(priceListModel[indexPath.row].sessionTimeOut ?? "0") ?? 0
            homeVC.companyDic = companyiddict
            homeVC.singlePrice = priceListModel[indexPath.row].price ?? ""
            homeVC.lotteryTicketImage = priceListModel[indexPath.row].lotteryticketImage ?? ""
            navigationController?.pushViewController(homeVC, animated: true)
        }
        
        
        
//        var homeVC: SelectedPersonViewController?
//        homeVC = UIStoryboard(name: LotteryConstant.kLotteryStoryBoard, bundle: nil).instantiateViewController(withIdentifier: LotteryConstant.kSelectedPersonViewController) as? SelectedPersonViewController
//
//              if let homeVC = homeVC {
//                homeVC.productid = priceListModel[indexPath.row].productId ?? ""
//                homeVC.companyDic = companyiddict
//                homeVC.singlePrice = priceListModel[indexPath.row].price ?? ""
//                  navigationController?.pushViewController(homeVC, animated: true)
//              }

    }
    
}

extension LotteryPriceViewController: GetCompanyPriceList{
    
    func companyPriceList(model: LotteryPriceListModel){
        priceListModel.removeAll()
        progressViewObj.removeProgressView()
        
        if !model.isTransactionSuccess{
                //failed
                DispatchQueue.main.async {
                    self.showErrorAlert(errMessage: model.msg ?? "")
                }
        }else{
            if model.lotteryPriceDetails.count>0{
                
                priceListModel = model.lotteryPriceDetails
    
                DispatchQueue.main.async {
                    self.lotterypriceTableView.delegate = self
                    self.lotterypriceTableView.dataSource = self
                    self.lotterypriceTableView.reloadData()
                }
            }else{
                DispatchQueue.main.async {
                  self.showErrorAlert(errMessage: "Some error happen please try again".localized)
                }
            }
        }
        
    }
    
}


