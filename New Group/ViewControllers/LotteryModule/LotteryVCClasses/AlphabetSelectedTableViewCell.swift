//
//  AlphabetSelectedTableViewCell.swift
//  LotteryModule
//
//  Created by iMac on 24/09/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class AlphabetSelectedTableViewCell: UITableViewCell {
    
    @IBOutlet var cellView: UIView!
    @IBOutlet var checkButton: UIButton!
    @IBOutlet var ticketNumberLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
