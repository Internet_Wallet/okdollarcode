//
//  LotteryViewModel.swift
//  OK
//
//  Created by Tushar Lama on 10/11/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation



class LotteryViewModel: NSObject{
    
    var connectionClassObj = LotteryConnectionClass()
    let apiReqObj = ApiRequestClass()
    
    //model
    var companyList: LotteryCompanyModel?
    var companyPriceList: LotteryPriceListModel?
    var couponListData: LotteryTicketListModel?
    var lotteryFinalPaymentObj: LotteryFinalModel?
    var verifywinningsheet: LotterywinningsheetModel? 
    
   
    //protocol
    var companyListDel: GetCompanyList?
    var companyPriceListDel: GetCompanyPriceList?
    var couponListDel: GetCouponList?
    var reserveTicketDel: LotteryReserveTicket?
    var receiptDel: LotteryReceiptModel?
    var verifywinningsheetDel: LotteryVerifyWinningSheet?
    var url = ""
    
    
    //get all the company list
    func getLotteryCompanyList(param: Any){
        apiReqObj.customDelegate = self
        url = LotteryConstant.getAllLotterycompany()
        apiReqObj.sendHttpRequest(requestUrl:URL(string:LotteryConstant.getAllLotterycompany())!, requestData: param, requestType: .RequestTypeGetCompanyList, httpMethodName: LotteryConstant.LOTTERY_POST)
    }
    
    //get price list by id
    func getProductList(companyId: String){
        apiReqObj.customDelegate = self
        url = LotteryConstant.getProductListById()
       
        apiReqObj.sendHttpRequest(requestUrl:URL(string:LotteryConstant.getProductListById())!, requestData: LotteryParam.getParamFromCompanyId(companyId: companyId), requestType: .RequestTypeGetCompanyPrice, httpMethodName: LotteryConstant.LOTTERY_POST)
    }
    
    //get number list
    func getNumberList(productId: String,searchString: String){
        apiReqObj.customDelegate = self
        url = LotteryConstant.getProductNumberList()
       
        apiReqObj.sendHttpRequest(requestUrl:URL(string:LotteryConstant.getProductNumberList())!, requestData: LotteryParam.getListByNumber(productId: productId,searchString: searchString), requestType: .RequestTypeTicketByNumber, httpMethodName: LotteryConstant.LOTTERY_POST)
    }
    
    
    //get number list
    func reserveTicket(productId: String,luckyNum: String){
        apiReqObj.customDelegate = self
        
        url = LotteryConstant.reserveTicket()
        apiReqObj.sendHttpRequest(requestUrl:URL(string:LotteryConstant.reserveTicket())!, requestData: LotteryParam.reserveTicket(productId: productId,luckyNumber: luckyNum), requestType: .ReserverLuckyNumByUser, httpMethodName: LotteryConstant.LOTTERY_POST)
    }
    
    
    //verify all number
    func verifyAllNumber(ticketList: [LotteryCouponDetails],amount:String){
        apiReqObj.customDelegate = self
        
        url = LotteryConstant.verifyAllNumber()
        apiReqObj.sendHttpRequest(requestUrl:URL(string:LotteryConstant.verifyAllNumber())!, requestData: LotteryParam.verifyAllNumber(ticketList: ticketList,amount: amount), requestType: .VerifyallNumbers, httpMethodName: LotteryConstant.LOTTERY_POST)
    }
    
    
    //Lucky number payment
    func luckyPay(ticketList: [LotteryCouponDetails]){
        apiReqObj.customDelegate = self
        url = LotteryConstant.luckyDrawPay()
        
        apiReqObj.sendHttpRequest(requestUrl:URL(string:LotteryConstant.luckyDrawPay())!, requestData: LotteryParam.luckyNumPaymentParam(ticketList: ticketList), requestType: .RequestTypePayment, httpMethodName: LotteryConstant.LOTTERY_POST)
    }
    
    // verify winnig Sheet
    
    func Verifywinningsheet(productId: String){
        apiReqObj.customDelegate = self
        url = LotteryConstant.verifywinningsheet()
        apiReqObj.sendHttpRequest(requestUrl:URL(string:LotteryConstant.verifywinningsheet())!, requestData:LotteryParam.Verifywinningsheet(productId: productId), requestType: .RequestTypeVerifyWinningSheet, httpMethodName: LotteryConstant.LOTTERY_POST)
        
    }
    
    
}



extension LotteryViewModel:  ApiRequestProtocol {
  
    
    func httpResponse(responseObj:Any,reqType:RequestTypeObj) -> Void{
        
        print("******************************************")
        print(url)
        print(responseObj)
        print("******************************************")
        //for lotteryCompanyList
        switch reqType {
        case .RequestTypeGetCompanyList:
            companyList = nil
            companyList = LotteryCompanyModel(data: responseObj as? NSDictionary)
            if let data = companyList{
                companyListDel?.companyList(model: data)
            }
        
        case .RequestTypeGetCompanyPrice:
            companyPriceList = nil
            companyPriceList = LotteryPriceListModel(data: responseObj as? NSDictionary)
            if let data = companyPriceList{
                companyPriceListDel?.companyPriceList(model: data)
            }
        
        case .RequestTypeTicketByNumber:
            couponListData = nil
            couponListData = LotteryTicketListModel(data: responseObj as? NSDictionary)
            if let data = couponListData{
                couponListDel?.coupponList(model: data)
            }
            
        case .ReserverLuckyNumByUser, .VerifyallNumbers:
            
            if let hasValue = responseObj as? NSDictionary{
                reserveTicketDel?.reserveTicket(model: hasValue)
            }else{
                reserveTicketDel?.reserveTicket(model: nil)
            }
            
       
            
        case .RequestTypePayment :
            lotteryFinalPaymentObj = nil
            lotteryFinalPaymentObj = LotteryFinalModel(data: responseObj as? NSDictionary)
            if let data = lotteryFinalPaymentObj{
                receiptDel?.receipt(response: data)
            }
            // *******
        case.RequestTypeVerifyWinningSheet:
            verifywinningsheet = nil
            verifywinningsheet = LotterywinningsheetModel(data: responseObj as? NSDictionary)
            if let data = verifywinningsheet{
                verifywinningsheetDel?.winningSheet(response: data)
            }
       
        default:
            break
        }
        
        
    }
}
