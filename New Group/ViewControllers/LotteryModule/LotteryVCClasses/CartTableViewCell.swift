//
//  CartTableViewCell.swift
//  LotteryModule
//
//  Created by iMac on 19/09/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

protocol DeleteSelectedTicket {
    func getIndexToDelete(index: Int)
}

import UIKit



class CartTableViewCell: UITableViewCell {

    
    @IBOutlet var cellView: UIView!
    @IBOutlet var checkButton: UIButton!
    @IBOutlet var ticketNumberLabel: UILabel!
    var delegate: DeleteSelectedTicket?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func onClickCheckButton(_ sender: Any) {
        delegate?.getIndexToDelete(index: checkButton.tag)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
