//
//  MyTicketsTableViewCell.swift
//  OK
//
//  Created by iMac on 10/12/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class MyTicketsTableViewCell: UITableViewCell {

    @IBOutlet var ticketCountLabel: UILabel!
    @IBOutlet var monthLabel: UILabel!
    @IBOutlet var CellView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        ticketCountLabel.layer.cornerRadius = 40/2
        ticketCountLabel.layer.masksToBounds = true
        ticketCountLabel.layer.borderWidth = 0.5
        
        layer.cornerRadius = 7.0
        layer.masksToBounds = true
        CellView.layer.cornerRadius = 7.0
        CellView.layer.borderColor = UIColor.white.cgColor
        CellView.layer.borderWidth = 0.5
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
