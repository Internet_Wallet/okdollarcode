//
//  CheckDetailsCollectionViewCell.swift
//  LotteryModule
//
//  Created by iMac on 21/09/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CheckDetailsCollectionViewCell: UICollectionViewCell {
    @IBOutlet var ticketImageView: UIImageView!
    @IBOutlet var ticketnumberLabel: UILabel!
}
