//
//  LotteryTicketsTableViewCell.swift
//  LotteryModule
//
//  Created by iMac on 19/09/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class LotteryTicketsTableViewCell: UITableViewCell {

    @IBOutlet var cellView: UIView!
    @IBOutlet var checkButton: UIButton!
    @IBOutlet var ticketNumberLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func wrapData(coupon: String){
        
        self.layer.cornerRadius = 7.0;
        self.layer.masksToBounds = true
        
        let ansTest = coupon.enumerated().compactMap({ ($0 > 0) && ($0 % 1 == 0) ? "  \($1)" : "\($1)" }).joined()
        ticketNumberLabel.text = ansTest
        
    }

}
