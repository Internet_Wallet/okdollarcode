//
//  LotteryConstant.swift
//  OK
//
//  Created by Tushar Lama on 10/11/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation


struct LotteryConstant {
    
    static let kLotteryStoryBoard = "Lottery"
    
    //all the controller names
    static let kAlphabetSelectedViewController = "AlphabetSelectedViewController"
    static let kCartViewController = "CartViewController"
    static let kCheckDetailsViewController = "CheckDetailsViewController"
    static let kCountDownTimerViewController = "CountDownTimerViewController"
    static let kLotteryListViewController = "LotteryListViewController"
    static let kLotteryPriceViewController = "LotteryPriceViewController"
    static let kLotteryReceiptViewController = "LotteryReceiptViewController"
    static let kReadMeViewController = "ReadMeViewController"
    static let kSelectedPersonViewController = "SelectedPersonViewController"
    static let kTicketByAlphabetViewController = "TicketByAlphabetViewController"
    static let kTicketByNumberViewController = "TicketByNumberViewController"
    static let kWinningSheetViewController = "WinningSheetViewController"
    
    
    
    //cell classes
    static let kAlphabetCollectionViewCell = "AlphabetCollectionViewCell"
    static let kAlphabetSelectedTableViewCell = "AlphabetSelectedTableViewCell"
    static let kCartTableViewCell = "CartTableViewCell"
    static let kCheckDetailsCollectionViewCell = "CheckDetailsCollectionViewCell"
    static let kLotteryListTableViewCell = "LotteryListTableViewCell"
    static let kLotteryPriceTableViewCell = "LotteryPriceTableViewCell"
    static let kLotteryReceiptCollectionViewCell = "LotteryReceiptCollectionViewCell"
    static let kLotteryTicketsTableViewCell = "LotteryTicketsTableViewCell"
    
    
    //API Type
    static let LOTTERY_POST = "POST"
    static let LOTTERY_GET = "GET"
    
    // baseURL
   static func getBaseUrl() -> String{
        var finalURL = ""
        if serverUrl == .productionUrl{
            finalURL = "http://www.okdollar.co/RestService.svc/"
        }else{
            finalURL = "http://69.160.4.151:8001/RestService.svc/"
        }
        return finalURL
    }
    
    
    
    
    //get all companyList
    static func getAllLotterycompany() -> String{
       return getBaseUrl() + "GetCompanyList"
    }
    
    //get product list by company id
    static func getProductListById() -> String{
        return getBaseUrl() + "GetProductListByCompanyId"
    }
    
    //get product list by company id
    static func getProductNumberList() -> String{
        return getBaseUrl() + "GetLuckyNumbersByProductId"
    }
    
    //reserve ticket
    static func reserveTicket() -> String{
        return getBaseUrl() + "ReserverLuckyNumByUser"
    }
    
    //verify all number
    
    static func verifyAllNumber() -> String{
        return getBaseUrl() + "VerifyallNumbers"
    }
    
    //lucky draw pay final
    
    static func luckyDrawPay() -> String{
        return getBaseUrl() + "ProcessLuckyNumPayment"
    }
    
    // verifywinning Sheet
    static func verifywinningsheet() -> String{
        return getBaseUrl() + "GetWinningSheetByProductId"
    }
    
}
