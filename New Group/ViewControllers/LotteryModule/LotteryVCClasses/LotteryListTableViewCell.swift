//
//  LotteryListTableViewCell.swift
//  LotteryModule
//
//  Created by iMac on 18/09/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class LotteryListTableViewCell: UITableViewCell {

    @IBOutlet var cellView: UIView!
    @IBOutlet var lottryLogoImageView: UIImageView!
    @IBOutlet var lottryNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func wrapDataInCell(value: String){
        lottryNameLabel.text =  value
        
        layer.cornerRadius = 7.0
        layer.masksToBounds = true
        cellView.layer.cornerRadius = 7.0
        cellView.layer.borderColor = UIColor.white.cgColor
        cellView.layer.borderWidth = 0.5
    }

}
