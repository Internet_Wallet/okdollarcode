//
//  MyTicketsViewController.swift
//  OK
//
//  Created by iMac on 10/12/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class MyTicketsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var navigationView: UIView!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var historyButton: UIButton!
    @IBOutlet var navigationTitleLabel: UILabel!
    
    @IBOutlet var ticketmonthsTableView: UITableView!
    
    var monthsArr = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        monthsArr = ["January","February","March","Aprial"]
        ticketmonthsTableView.delegate = self
        ticketmonthsTableView.dataSource = self
        ticketmonthsTableView.reloadData()
        
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func historyButtonClick(_ sender: Any) {
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return monthsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = ticketmonthsTableView.dequeueReusableCell(withIdentifier: "Cell") as? MyTicketsTableViewCell else{
            return UITableViewCell()
        }
        
        cell.selectionStyle = .none
        cell.monthLabel.text = monthsArr[indexPath.row]
        
        return cell
        
    }


}
