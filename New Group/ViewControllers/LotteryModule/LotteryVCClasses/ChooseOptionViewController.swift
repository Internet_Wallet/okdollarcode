//
//  ChooseOptionViewController.swift
//  OK
//
//  Created by iMac on 13/11/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class ChooseOptionViewController: OKBaseController, LotteryVerifyWinningSheet {
    @IBOutlet var navigationView: UIView!
    @IBOutlet var navigationTitelLabel: UILabel!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var buyTicketsView: CardDesignView!
    @IBOutlet var myTicketsView: CardDesignView!
    @IBOutlet var winningSheetView: CardDesignView!
    
    @IBOutlet var buyTicketLabel: UILabel!{
        didSet {
            self.buyTicketLabel.text = "Buy Ticket".localized
        }
        
    } 
    
    @IBOutlet var myTicketLabel: UILabel!{
        didSet {
            self.myTicketLabel.text = "My Tickett".localized
        }
    }
    
    @IBOutlet var winningSheetLabel: UILabel!{
        didSet {
            self.winningSheetLabel.text = "Winning Sheet".localized
        }
    }
    var timeFromServer = 0
    var companyDic: LotteryCompanyDetails?
    var productid = ""
    var singlePrice: String?
    var lotteryTicketImage: String?
  
    var lotterywinningsheet = LotteryViewModel()
    var lotterywinningsheetModel =  [Lotterywinningsheetdetails]()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("@@@@@",timeFromServer)

        let tapBuyTicket = UITapGestureRecognizer(target: self, action: #selector(self.handleTapOnBuyTicket(_:)))
        buyTicketsView.addGestureRecognizer(tapBuyTicket)
        
        let tapMyTicket = UITapGestureRecognizer(target: self, action: #selector(self.handleTapOnMyTicket(_:)))
        myTicketsView.addGestureRecognizer(tapMyTicket)
        
        
        let tapWinningSheet = UITapGestureRecognizer(target: self, action: #selector(self.handleTapOnWinningSheet(_:)))
        winningSheetView.addGestureRecognizer(tapWinningSheet)
    
        
    }
    
    
    @objc func handleTapOnBuyTicket(_ sender: UITapGestureRecognizer? = nil) {
//
//        var homeVC: LotteryPriceViewController?
//        homeVC = UIStoryboard(name: LotteryConstant.kLotteryStoryBoard, bundle: nil).instantiateViewController(withIdentifier: LotteryConstant.kLotteryPriceViewController) as? LotteryPriceViewController
//        if let homeVC = homeVC {
//            homeVC.companyiddict = companyDic
//            navigationController?.pushViewController(homeVC, animated: true)
//        }
        
        
        var homeVC: TicketByAlphabetViewController?
        homeVC = UIStoryboard(name:  LotteryConstant.kLotteryStoryBoard, bundle: nil).instantiateViewController(withIdentifier: LotteryConstant.kTicketByAlphabetViewController) as? TicketByAlphabetViewController
        if let homeVC = homeVC {
            homeVC.productid = productid
            homeVC.timeFromServer = timeFromServer
            homeVC.companyDic = companyDic
            homeVC.singlePrice = singlePrice
            homeVC.lotteryTicketImage = lotteryTicketImage
            navigationController?.pushViewController(homeVC, animated: true)
        }
     
    }
    
    @objc func handleTapOnMyTicket(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        var homeVC: MyTicketsViewController?
        homeVC = UIStoryboard(name:  LotteryConstant.kLotteryStoryBoard, bundle: nil).instantiateViewController(withIdentifier: "MyTicketsViewController") as? MyTicketsViewController
        if let homeVC = homeVC {
            navigationController?.pushViewController(homeVC, animated: true)
        }
        
        
    }
    
    @objc func handleTapOnWinningSheet(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        
        
        var homeVC: WinningSheetViewController?
        homeVC = UIStoryboard(name:  LotteryConstant.kLotteryStoryBoard, bundle: nil).instantiateViewController(withIdentifier: LotteryConstant.kWinningSheetViewController) as? WinningSheetViewController
        if let homeVC = homeVC {
            homeVC.productid = productid
            navigationController?.pushViewController(homeVC, animated: true)
        }
    
      //  self.apicall()
        
    }
    
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func apicall(){
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            lotterywinningsheet.verifywinningsheetDel = self
            lotterywinningsheet.Verifywinningsheet(productId: productid)
            
        }else {
            self.noInternetAlert()
            progressViewObj.removeProgressView()
        }
    }
    
    func winningSheet(response:LotterywinningsheetModel){
        lotterywinningsheetModel.removeAll()
        progressViewObj.removeProgressView()
        
        if !response.isTransactionSuccess{
            //failed
            DispatchQueue.main.async {
                self.showErrorAlert(errMessage: response.msg ?? "")
            }
        }
        else if response.lotterywinningsheetDetails.count>0{
            
            lotterywinningsheetModel = response.lotterywinningsheetDetails
            
        }
        
        else{
            DispatchQueue.main.async {
                self.showErrorAlert(errMessage: "Some error happen please try again".localized)
            }
        }
    }
    
    
}
