//
//  CountDownTimerViewController.swift
//  LotteryModule
//
//  Created by iMac on 18/09/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CountDownTimerViewController: OKBaseController {
    @IBOutlet var navigationView: UIView!
    @IBOutlet var navigationTitleLabel: UILabel!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var historyButton: UIButton!
    @IBOutlet var cartButton: UIButton!
    
    @IBOutlet var countDownView: UIView!
    
    @IBOutlet var buttonsView: UIView!
    
    @IBOutlet var bottomView: UIView!
    
    @IBOutlet var ticketbyNumberButton: UIButton!
    
    @IBOutlet var ticketbyAlphabetButton: UIButton!
    
    @IBOutlet var daysLabel: UILabel!
    @IBOutlet var houroneLabel: UILabel!
    @IBOutlet var hourtwoLabel: UILabel!
    
    @IBOutlet var minsoneLabel: UILabel!
    @IBOutlet var minstwoLabel: UILabel!
    
    @IBOutlet var secsoneLabel: UILabel!
    @IBOutlet var secstwoLabel: UILabel!
    var otherPersonDetails: OtherPersonDetails?
    
    var companyDic: LotteryCompanyDetails?
    
    var productid = ""
    var singlePrice: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        navigationTitleLabel.text = companyDic?.companyName ?? ""
        self.setUI()
    }
    
    func setUI(){
        
        daysLabel.layer.cornerRadius = 4.0
        daysLabel.layer.masksToBounds = true
        daysLabel.layer.borderColor = UIColor.lightGray.cgColor
        daysLabel.layer.borderWidth = 0.5
        
        houroneLabel.layer.cornerRadius = 4.0
        houroneLabel.layer.masksToBounds = true
        houroneLabel.layer.borderColor = UIColor.lightGray.cgColor
        houroneLabel.layer.borderWidth = 0.5
        
        hourtwoLabel.layer.cornerRadius = 4.0
        hourtwoLabel.layer.masksToBounds = true
        hourtwoLabel.layer.borderColor = UIColor.lightGray.cgColor
        hourtwoLabel.layer.borderWidth = 0.5
        
        minsoneLabel.layer.cornerRadius = 4.0
        minsoneLabel.layer.masksToBounds = true
        minsoneLabel.layer.borderColor = UIColor.lightGray.cgColor
        minsoneLabel.layer.borderWidth = 0.5
        
        minstwoLabel.layer.cornerRadius = 4.0
        minstwoLabel.layer.masksToBounds = true
        minstwoLabel.layer.borderColor = UIColor.lightGray.cgColor
        minstwoLabel.layer.borderWidth = 0.5
        
        secsoneLabel.layer.cornerRadius = 4.0
        secsoneLabel.layer.masksToBounds = true
        secsoneLabel.layer.borderColor = UIColor.lightGray.cgColor
        secsoneLabel.layer.borderWidth = 0.5
        
        secstwoLabel.layer.cornerRadius = 4.0
        secstwoLabel.layer.masksToBounds = true
        secstwoLabel.layer.borderColor = UIColor.lightGray.cgColor
        secstwoLabel.layer.borderWidth = 0.5
        
        let dcf = DateComponentsFormatter()
        dcf.allowedUnits = [.day, .hour, .minute, .second]
        dcf.unitsStyle = .full
        
        let df = ISO8601DateFormatter()
        df.formatOptions = [.withFullDate, .withDashSeparatorInDate]
        if let future = df.date(from: "2021-01-15"), let diff = dcf.string(from: Date(), to: future) {
            print(diff)
        }
        
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    @IBAction func historyButtonClick(_ sender: Any) {
    }
    @IBAction func cartButtonClick(_ sender: Any) {
    }
    
    @IBAction func ticketbynumberClick(_ sender: Any) {
        var homeVC: TicketByNumberViewController?
        homeVC = UIStoryboard(name: LotteryConstant.kLotteryStoryBoard, bundle: nil).instantiateViewController(withIdentifier: LotteryConstant.kTicketByNumberViewController) as? TicketByNumberViewController
        if let homeVC = homeVC {
            
            if let validValue = otherPersonDetails{
                homeVC.otherPersonDetails = validValue
            }
            homeVC.productid = productid
            homeVC.companyDic = companyDic
            homeVC.singlePrice = singlePrice
            navigationController?.pushViewController(homeVC, animated: true)
        }
        
    }
    
    @IBAction func ticketbyalbhabetClick(_ sender: Any) {
        var homeVC: TicketByAlphabetViewController?
        homeVC = UIStoryboard(name:  LotteryConstant.kLotteryStoryBoard, bundle: nil).instantiateViewController(withIdentifier: LotteryConstant.kTicketByAlphabetViewController) as? TicketByAlphabetViewController
        if let homeVC = homeVC {
            homeVC.productid = productid
            homeVC.companyDic = companyDic
            homeVC.singlePrice = singlePrice
            navigationController?.pushViewController(homeVC, animated: true)
        }
        
        
    }
    
}
