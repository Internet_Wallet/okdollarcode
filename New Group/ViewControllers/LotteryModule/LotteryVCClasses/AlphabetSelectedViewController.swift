//
//  AlphabetSelectedViewController.swift
//  LotteryModule
//
//  Created by iMac on 24/09/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class AlphabetSelectedViewController: OKBaseController {
    
    @IBOutlet var fourthLabel: UILabel!
    @IBOutlet var thirdLabel: UILabel!
    @IBOutlet var navigationView: UIView!
    @IBOutlet var navigationTitleLabel: UILabel!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var historyButton: UIButton!
    @IBOutlet var cartButton: UIButton!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var resetButton: UIButton!
    @IBOutlet var addcartButton: UIButton!
    
    
    @IBOutlet var checkmarkButton: UIButton!
    @IBOutlet var selectionView: UIView!
    
    @IBOutlet var firstalphabetLabel: UILabel!
    
    @IBOutlet var secondalphabetLabel: UILabel!
    @IBOutlet var timerdisplayView: UIView!
    @IBOutlet var minutesLabel: UILabel!
    @IBOutlet var secsLabel: UILabel!
    
    @IBOutlet var TicketsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timerdisplayView.layer.cornerRadius = 10
        minutesLabel.layer.cornerRadius = 4.0
        minutesLabel.layer.masksToBounds = true
        minutesLabel.layer.borderColor = UIColor.lightGray.cgColor
        minutesLabel.layer.borderWidth = 0.5
        
        secsLabel.layer.cornerRadius = 4.0
        secsLabel.layer.masksToBounds = true
        secsLabel.layer.borderColor = UIColor.lightGray.cgColor
        secsLabel.layer.borderWidth = 0.5
        
        bottomView.layer.shadowColor = UIColor.gray.cgColor
        bottomView.layer.shadowOffset = .zero
        bottomView.layer.shadowOpacity = 0.8
        bottomView.layer.shadowRadius = 10
        
        addcartButton.layer.cornerRadius = 20
        resetButton.layer.cornerRadius = 20
        
        
    }
    @IBAction func backButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func historyButtonClick(_ sender: Any) {
    }
    
    @IBAction func cartButtonClick(_ sender: Any) {
        var homeVC: CartViewController?
        homeVC = UIStoryboard(name: LotteryConstant.kLotteryStoryBoard, bundle: nil).instantiateViewController(withIdentifier: LotteryConstant.kCartViewController) as? CartViewController
        if let homeVC = homeVC {
            navigationController?.pushViewController(homeVC, animated: true)
        }
        
        
    }
    @IBAction func restButtonClick(_ sender: Any) {
    }
    @IBAction func addcartButtonClick(_ sender: Any) {
    }
    
    @IBAction func checkmarkClick(_ sender: Any) {
    }
    
    
}
