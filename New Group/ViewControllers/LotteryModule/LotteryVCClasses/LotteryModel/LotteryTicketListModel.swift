//
//  LotteryTicketListModel.swift
//  OK
//
//  Created by Tushar Lama on 10/11/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

//["Code": 200, "Msg": , "Data": [{"CouponNumber":"143456789","ProductId":"6274e224-4559-4ae8-82e6-854ee2908688"},{"CouponNumber":"183456789","ProductId":"6274e224-4559-4ae8-82e6-854ee2908688"},{"CouponNumber":"163456789","ProductId":"6274e224-4559-4ae8-82e6-854ee2908688"},{"CouponNumber":"133456789","ProductId":"6274e224-4559-4ae8-82e6-854ee2908688"},{"CouponNumber":"153456789","ProductId":"6274e224-4559-4ae8-82e6-854ee2908688"},{"CouponNumber":"193456789","ProductId":"6274e224-4559-4ae8-82e6-854ee2908688"},{"CouponNumber":"173456789","ProductId":"6274e224-4559-4ae8-82e6-854ee2908688"},{"CouponNumber":"123456789","ProductId":"6274e224-4559-4ae8-82e6-854ee2908688"}]]

import Foundation



open class LotteryTicketListModel{
    var msg: String?
    var data: String?
    var isTransactionSuccess = false
    var lotteryCouponDetails = [LotteryCouponDetails]()
    
    public init(data: NSDictionary?){
        if let response = data{
            if response.count>0{
                let code = response.value(forKey: "Code") as? Int
                self.msg = response.value(forKey: "Msg") as? String
                if let isValidCode = code{
                    if isValidCode == 200{
                        self.isTransactionSuccess = true
                        let data = response.value(forKey: "Data") as? String
                        if let validObj = data{
                            if let list = UitilityClass.convertStringToArrayOfDictionary(text: validObj) as? [AnyObject] {
                                lotteryCouponDetails = list.compactMap({LotteryCouponDetails(responseData: $0 as! NSDictionary)})
                            }
                        }
                    }else{
                        self.isTransactionSuccess = false
                    }
                }
            }
        }
    }
}


open class LotteryCouponDetails{
    var couponNumber: String?
    var productId: String?
    
    
    public init(responseData: NSDictionary){
        if responseData.count>0{
            self.couponNumber = responseData.value(forKey: "CouponNumber") as? String ?? ""
            self.productId = responseData.value(forKey: "ProductId") as? String ?? ""
        }
    }
    
}





