//
//  LotteryFinalModel.swift
//  OK
//
//  Created by Tushar Lama on 11/11/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

//["Data":
//
//{"TransactionTime":"11-Nov-2020 12:41:59","TransactionId":"2009475049","TotalReqCount":6,"TotalSuccessCount":6,"TotalFailCount":0,"LuckyNumResultSummaries":[{"Luckynum":"163456789","Amount":1000.0000,"Status":"Success"},{"Luckynum":"133456789","Amount":1000.0000,"Status":"Success"},{"Luckynum":"153456789","Amount":1000.0000,"Status":"Success"},{"Luckynum":"193456789","Amount":1000.0000,"Status":"Success"},{"Luckynum":"173456789","Amount":1000.0000,"Status":"Success"},{"Luckynum":"123456789","Amount":1000.0000,"Status":"Success"}]}, "Code": 200, "Msg": Success]

import Foundation

open class LotteryFinalModel{
    var msg: String?
    var data: String?
    var isTransactionSuccess = false
    var lotteryFinalPayObj = [LotteryFinalPayDetails]()
    var totalFailCount: String?
    var totalReqCount: String?
    var totalSuccessCount: String?
    var transactionID: String?
    var transactionTime: String?
  
    
    public init(data: NSDictionary?){
        if let response = data{
            if response.count>0{
                let code = response.value(forKey: "Code") as? Int
                self.msg = response.value(forKey: "Msg") as? String
                if let isValidCode = code{
                    if isValidCode == 200{
                        self.isTransactionSuccess = true
                        let data = response.value(forKey: "Data") as? String
                        if let validObj = data{
                            
                            if let list = UitilityClass.convertToDictionary(text: validObj) {
                                
                                let valueFinal =  list["LuckyNumResultSummaries"] as? Array<Any>
                                
                                self.totalFailCount = "\(list["TotalFailCount"] ?? 0)"
                                self.totalReqCount = "\(list["TotalReqCount"] ?? 0)"
                                self.totalSuccessCount = "\(list["TotalSuccessCount"] ?? 0)"
                                self.transactionID = "\(list["TransactionId"] ?? 0)"
                                self.transactionTime = list["TransactionTime"] as? String
                                                            
                                
                                if let valid = valueFinal{
                                    if valid.count>0{
                                        lotteryFinalPayObj = valid.compactMap({LotteryFinalPayDetails(responseData: $0 as! NSDictionary)})
                                    }
                                }
                                
                                
                            }
                        }
                    }else{
                        self.isTransactionSuccess = false
                    }
                }
            }
        }
    }
}


open class LotteryFinalPayDetails{
    var amount: String?
    var luckyNumber: String?
    var status: String?
    
    
    public init(responseData: NSDictionary){
        if responseData.count>0{
            self.amount = "\(responseData.value(forKey: "Amount") as? Int ?? 0)"
            self.luckyNumber = "\(responseData.value(forKey: "Luckynum") as? String ?? "")"
            self.status = responseData.value(forKey: "Status") as? String ?? ""
        }
    }
    
}
