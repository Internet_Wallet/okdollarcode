//
//  LotteryCompanyModel.swift
//  OK
//
//  Created by Tushar Lama on 10/11/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

open class LotteryCompanyModel{
        
    var msg: String?
    var data: String?
    var isTransactionSuccess = false
    var lotteryCompanyDetails = [LotteryCompanyDetails]()
    
    public init(data: NSDictionary?){
        if let response = data{
            if response.count>0{
                let code = response.value(forKey: "Code") as? Int
                self.msg = response.value(forKey: "Msg") as? String
                if let isValidCode = code{
                    if isValidCode == 200{
                        self.isTransactionSuccess = true
                        let data = response.value(forKey: "Data") as? String
                        if let validObj = data{
                            if let list = UitilityClass.convertStringToArrayOfDictionary(text: validObj) as? [AnyObject] {
                                lotteryCompanyDetails = list.compactMap({LotteryCompanyDetails(responseData: $0 as! NSDictionary)})
                            }
                        }
                    }else{
                        self.isTransactionSuccess = false
                    }
                }
            }
        }
    }
}


open class LotteryCompanyDetails{
    var imageUrl: String?
    var companyName: String?
    var comments: String?
    var companyID: String?
    
    public init(responseData: NSDictionary){
        if responseData.count>0{
            self.imageUrl = responseData.value(forKey: "ImageUrl") as? String ?? ""
            self.companyName = responseData.value(forKey: "CompanyName") as? String ?? ""
            self.comments = responseData.value(forKey: "Comments") as? String ?? ""
            self.companyID = responseData.value(forKey: "CompanyId") as? String ?? ""
            
        }
    }
    
}
