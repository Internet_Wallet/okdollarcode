//
//  LotteryPriceListModel.swift
//  OK
//
//  Created by Tushar Lama on 10/11/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//



import Foundation


open class LotteryPriceListModel{
        
    var msg: String?
    var data: String?
    var isTransactionSuccess = false
    var lotteryPriceDetails = [LotteryCompanyPriceDetails]()
    
    public init(data: NSDictionary?){
        if let response = data{
            if response.count>0{
                let code = response.value(forKey: "Code") as? Int
                self.msg = response.value(forKey: "Msg") as? String
                if let isValidCode = code{
                    if isValidCode == 200{
                        self.isTransactionSuccess = true
                        let data = response.value(forKey: "Data") as? String
                        if let validObj = data{
                            if let list = UitilityClass.convertStringToArrayOfDictionary(text: validObj) as? [AnyObject] {
                                lotteryPriceDetails = list.compactMap({LotteryCompanyPriceDetails(responseData: $0 as! NSDictionary)})
                            }
                        }
                    }else{
                        self.isTransactionSuccess = false
                    }
                }
            }
        }
    }
}



open class LotteryCompanyPriceDetails{
    var price: String?
    var productId: String?
    var sessionTimeOut: String?
    var lotteryticketImage: String?
    
    public init(responseData: NSDictionary){
        if responseData.count>0{
            self.price = String(responseData.value(forKey: "Price") as? Int ?? 0)
            self.productId = responseData.value(forKey: "ProductId") as? String ?? ""
            self.sessionTimeOut = responseData.value(forKey: "Sessiontimeout") as? String ?? ""
            self.lotteryticketImage = responseData.value(forKey: "ImageUrl") as? String ?? ""
        }
    }
    
}
