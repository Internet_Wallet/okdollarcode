//
//  LotteryWinningSheetModel.swift
//  OK
//
//  Created by iMac on 25/11/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

open class LotterywinningsheetModel{
        
    var msg: String?
    var data: String?
    var isTransactionSuccess = false
    var lotterywinningsheetDetails = [Lotterywinningsheetdetails]()
    
    public init(data: NSDictionary?){
        if let response = data{
            if response.count>0{
                let code = response.value(forKey: "Code") as? Int
                self.msg = response.value(forKey: "Msg") as? String
                if let isValidCode = code{
                    if isValidCode == 200{
                        self.isTransactionSuccess = true
                        let data = response.value(forKey: "Data") as? String
                        if let validObj = data{
                            if let list = UitilityClass.convertStringToArrayOfDictionary(text: validObj) as? [AnyObject] {
                                lotterywinningsheetDetails = list.compactMap({Lotterywinningsheetdetails(responseData: $0 as! NSDictionary)})
                               // let sorted = responseData.sorted {$0.key < $1.key}
                              
                            }
                        }
                    }else{
                        self.isTransactionSuccess = false
                    }
                }
            }
        }
    }
}


open class Lotterywinningsheetdetails{
    var imageUrl: String?
    var winningNumber: String?

    public init(responseData: NSDictionary){
        if responseData.count>0{
            self.imageUrl = responseData.value(forKey: "WinningsheetImage") as? String ?? ""
            self.winningNumber = responseData.value(forKey: "WinningNumber") as? String ?? ""
        }
    }
    
}
