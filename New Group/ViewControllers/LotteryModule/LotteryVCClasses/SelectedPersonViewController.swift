//
//  SelectedPersonViewController.swift
//  LotteryModule
//
//  Created by iMac on 29/10/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Contacts

struct OtherPersonDetails {
    var name: String?
    var phoneNumer: String?
}

class SelectedPersonViewController: OKBaseController{

    @IBOutlet var slectionView: UIView!
    @IBOutlet var navigationView: UIView!
    @IBOutlet var navigationTitleLabel: UILabel!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var accountSegment: UISegmentedControl!
    @IBInspectable var primaryColor: UIColor! = .lightGray
    private let mobileNumberAcceptableCharacters = "0123456789"
    var companyDic: LotteryCompanyDetails?
    var productid = ""
    var singlePrice: String?
    var contactsArray = [CNContact]()
    
    @IBOutlet var otherPersonView: CardDesignView!
    @IBOutlet var otherPersonNameLabel: UILabel!
    @IBOutlet var otherPersonMobileLabel: UILabel!
    @IBOutlet var buyerNameTxt: UITextField!
    
    @IBOutlet var mobileNumTxt: UITextField!
    
    @IBOutlet var closeButton: CurvedButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        otherPersonNameLabel.attributedText = returnAttributtedAmount(value: "Name")
        otherPersonMobileLabel.attributedText = returnAttributtedAmount(value: "Mobile Number")
        
        otherPersonView.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
        navigationTitleLabel.text = companyDic?.companyName ?? ""
        slectionView.layer.cornerRadius = 10
        accountSegment?.setTitle("My Self", forSegmentAt: 0)
        accountSegment?.setTitle("Other Person", forSegmentAt: 1)
        
        accountSegment.setTitleTextAttributes( [NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        nextButton.isHidden = true
        closeButton.isHidden = true
        self.setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
      
        
        self.accountSegment.selectedSegmentIndex = UISegmentedControl.noSegment
    }
  
    
    @IBAction func closeButtonClick(_ sender: Any) {
        self.mobileNumTxt.text = "09"
       
        self.mobileNumTxt.becomeFirstResponder()
        
        closeButton.isHidden = true
        
    }
    @IBAction func onClickConact(_ sender: Any) {
        self.newActionSheet()
    }
    func setUI(){
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let image = UIImage(named: "calender")
        imageView.image = image
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func accounsegmentClick(_ sender: Any) {
        
        if accountSegment.selectedSegmentIndex == 0{
            nextButton.isHidden = true
            otherPersonView.isHidden = true
            var homeVC: CountDownTimerViewController?
            homeVC = UIStoryboard(name: LotteryConstant.kLotteryStoryBoard, bundle: nil).instantiateViewController(withIdentifier: LotteryConstant.kCountDownTimerViewController) as? CountDownTimerViewController
             if let homeVC = homeVC {
                homeVC.productid = productid
                homeVC.companyDic = companyDic
                homeVC.singlePrice = singlePrice
                 navigationController?.pushViewController(homeVC, animated: true)
             }
        }
        else{
            otherPersonView.isHidden = false
            nextButton.isHidden = false
        }
    }
    
    
    
    @IBAction func nextButtonClick(_ sender: Any) {
        
        if buyerNameTxt.text == ""{
            alertViewObj.wrapAlert(title: "OK$", body: "Enter Buyer Name".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        }
       else if mobileNumTxt.text == "09" || mobileNumTxt.text == "" {
            alertViewObj.wrapAlert(title: "OK$", body: "Enter Mobile Number".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        }
       
        else{
            var homeVC: CountDownTimerViewController?
            homeVC = UIStoryboard(name: LotteryConstant.kLotteryStoryBoard, bundle: nil).instantiateViewController(withIdentifier: LotteryConstant.kCountDownTimerViewController) as? CountDownTimerViewController
            if let homeVC = homeVC {
                
                if !otherPersonView.isHidden{
                    homeVC.otherPersonDetails =  OtherPersonDetails(name: buyerNameTxt.text, phoneNumer: mobileNumTxt.text)
                }
                homeVC.singlePrice = singlePrice
                homeVC.productid = productid
                homeVC.companyDic = companyDic
                navigationController?.pushViewController(homeVC, animated: true)
            }
        }
    }
    
    func newActionSheet(){
        
//
//        let alert = UIAlertController(
//            title: "Choose From".localized,
//            message: "",
//            preferredStyle: .actionSheet)
        let alert:UIAlertController = UIAlertController(title: nil ,message: nil, preferredStyle: .actionSheet)
        
        let title = UIAlertAction(
            title: "Chose From".localized,
            style: .default,
            handler: { action in
                //Do some thing here
            })
        
        let online = UIAlertAction(
            title: "Favourite".localized,
            style: .default,
            handler: { action in
                //Do some thing here
                
                let nav = self.openFavoriteFromNavigation(self, withFavorite: .payto, selectionType: false,  multiPay: true, payToCount: MultiTopupArray.main.controllers.count - 1)
                self.navigationController?.present(nav, animated: true, completion: nil)
             
                alert.dismiss(animated: true)
            })
        let offline = UIAlertAction(
            title: "Contacts".localized,
            style: .default,
            handler: { action in
                let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
                nav.modalPresentationStyle = .fullScreen
                self.navigationController?.present(nav, animated: true, completion: nil)
           
                alert.dismiss(animated: true)
            })
        let cancel = UIAlertAction(
            title: "Cancel".localized,
            style: .cancel,
            handler: { action in
            })
        
     //   alert.setValue(NSAttributedString(string: "Chose From", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15),NSAttributedString.Key.foregroundColor : UIColor.red]), forKey: "attributedTitle")
        
        online.setValue(UIImage(named: "unionpayFav")?.withRenderingMode(.alwaysOriginal), forKey: "image")
        online.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        offline.setValue(UIImage(named: "unionpaycontact")?.withRenderingMode(.alwaysOriginal), forKey: "image")
        offline.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        
        
       
       // alert.setTitlet(font: UIFont.boldSystemFont(ofSize: 25), color: UIColor.red)
       
        
        alert.view.tintColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
        
      //  alert.applyContacts()
      //  alert.applyFavorites()
        alert.addAction(title)
        alert.addAction(online)
        alert.addAction(offline)
        alert.addAction(cancel)
        
      

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    
    func contact(contact: ContactPicker) {
       
        self.mobileNumTxt.text = "09"
        self.mobileNumTxt.becomeFirstResponder()
        
    }
    func decodeContact(ph: String,name: String, amount: String = "", hide: Bool = false) {
      
        var str = ""
        str = ph
        if str.count > 0 {
            str = str.replacingOccurrences(of: "(", with: "")
            str = str.replacingOccurrences(of: ")", with: "")
            str = str.trimmingCharacters(in: .whitespaces)
        }
        if str.hasPrefix("00") {
            _ = str.remove(at: String.Index.init(encodedOffset: 0))
            _ = str.remove(at: String.Index.init(encodedOffset: 0))
            str = "+" + str
        }
        let country = self.identifyCountry(withPhoneNumber: str)
        if country.0 == "+95" ||  country.1 == "myanmar" ||  country.0 == "" ||  country.1 == "" {
            let countryObject = Country.init(name: "", code: "myanmar", dialCode: "+95")
            if str.hasPrefix(countryObject.dialCode) {
                str = str.replacingOccurrences(of: countryObject.dialCode, with: "")
                str = "0" + str
            } else {
                if str.hasPrefix("0") {
                    
                } else {
                    str = "0" + str
                }
            }
           
            self.mobileNumTxt.text = str
          
            closeButton.isHidden = false
          //  self.mobileNumberView.hideClearButton(hide: false)
            let rangeCheck = PayToValidations().getNumberRangeValidation(str)
        } else {
         //   self.delegate?.showAddObjectButton(show: false)
            guard let keyWinndow = UIApplication.shared.keyWindow else { return }
            keyWinndow.makeToast("You Selected Other Country Number".localized, point: keyWinndow.center, title: nil, image: nil, completion: nil)
        }
    }
    
    func identifyCountry(withPhoneNumber prefix: String) -> (String, String) {
           let countArray = revertCountryArray() as! Array<NSDictionary>
           var finalCodeString = ""
           var flagCountry = ""
           for dic in countArray {
               if let code = dic .object(forKey: "CountryCode") as? String, let flag = dic.object(forKey: "CountryFlagCode") as? String{
                   if prefix.hasPrefix(code) {
                       finalCodeString = code
                       flagCountry = flag
                   }
               }
           }
           return (finalCodeString,flagCountry)
       }
    func revertCountryArray() -> NSArray {
        do {
            if let file = Bundle.main.url(forResource: "mcc_mnc", withExtension: "txt") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? NSDictionary {
                    if let arr = object.object(forKey: "data") as? NSArray {
                        arrCountry = arr
                        return arrCountry
                    }
                } else if let object = json as? [Any] {
                    println_debug(object)
                } else {
                    println_debug("JSON is invalid")
                }
            } else {
                println_debug("no file")
            }
        } catch {
            println_debug(error.localizedDescription)
        }
        return arrCountry
    }
  
}

extension SelectedPersonViewController : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        if textField == self.buyerNameTxt{
            
            
            let updatedText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: NAME_CHAR_SET_En).inverted).joined(separator: "")) { return false }
            
            if updatedText == " " || updatedText == "  "{
                return false
            }
            if (updatedText.contains("  ")){
                return false
            }
            if updatedText.count > 30 {
                return false
            }
        }
        else{
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let rangeCheck = PayToValidations().getNumberRangeValidation(text)
            if !text.hasPrefix("09") {
                return false
            }
            if rangeCheck.isRejected {
                textField.text = "09"
                closeButton.isHidden = true
             //   mobileNumberView.hideClearButton(hide: true)
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                return false
            }
            
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                   // self.delegate?.showAddObjectButton(show: false)
                    if textField == self.mobileNumTxt {
                        if textField.text == "09" {
                            println_debug(textField.text)
                            return false
                        }
                        if text == "09" {
                           
                      //      mobileNumberView.hideClearButton(hide: true)
                            closeButton.isHidden = true
                          
                        }else{
                            
                         
                        }
                        if text.count < rangeCheck.min {
                           
                        }
                        if text.count < 3 {
                            
                        }
                        
                        
                        self.mobileNumTxt.text = text
                        return false
                    }
                }
            }
            
           
            
            if textField == self.mobileNumTxt {
            

                
                
                if text.count > 2 {
                 //   mobileNumberView.hideClearButton(hide: false)
                    closeButton.isHidden = false
                }else {
                  
                }
                
                let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: mobileNumberAcceptableCharacters).inverted
                let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
                
                if string != filteredSet { return false }
                
                if text.count > 3 {
                   
                } else {
                   
                }
                
              
                if text.count > rangeCheck.max {
                    self.mobileNumTxt.resignFirstResponder()
                   
                    return false
                }
                
                if text.count >= rangeCheck.min {

                    
                  //  self.delegate?.showAddObjectButton(show: true)
                  

                    if text.count == rangeCheck.max {
                     
                        self.mobileNumTxt.text = text
                        self.mobileNumTxt.resignFirstResponder()
                        return false
                    } else if text.count > rangeCheck.max {
                        self.mobileNumTxt.resignFirstResponder()
                        return false
                    }
                    self.mobileNumTxt.text = text
                  
                    return false
                } else {

                    
                   // self.delegate?.showAddObjectButton(show: false)
                }
            }
        }
        
        
        return true
    }

func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    
    if textField == self.buyerNameTxt{
        buyerNameTxt.text = textField.text
    }
    
    if textField == self.mobileNumTxt {
        if textField == self.mobileNumTxt {
            if let text = self.mobileNumTxt.text, text.count < 3 {
                textField.text = "09"
            //    mobileNumberView.hideClearButton(hide: true)
                closeButton.isHidden = true
            }
            else{
             //   mobileNumberView.hideClearButton(hide: false)
                closeButton.isHidden = false
            }
        }
    }
    return true
}

func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    
    if textField == self.buyerNameTxt{
        //
    }
    if textField == self.mobileNumTxt {
       // mobileNumberView.hideClearButton(hide: true)
        closeButton.isHidden = true
    }
    return true
}
}


//MARK: - FavoriteSelectionDelegate
extension SelectedPersonViewController: FavoriteSelectionDelegate {
    func selectedFavoriteObject(obj: FavModel) {
        let contact = ContactPicker.init(object: obj)
        self.contact(ContactPickersPicker(), didSelectContact: contact)
    }
}



extension SelectedPersonViewController{
    func returnAttributtedAmount(value: String) -> NSAttributedString {

    let customAttribute1 = [ NSAttributedString.Key.font: UIFont(name: "", size: 16) ?? UIFont.systemFont(ofSize: 16),
                             NSAttributedString.Key.foregroundColor: UIColor.black]
        let attributedAmountString = NSMutableAttributedString.init(string: value, attributes: customAttribute1)

    let customAttribute2 = [ NSAttributedString.Key.font: UIFont(name: "", size: 15) ?? UIFont.systemFont(ofSize: 15),
    NSAttributedString.Key.foregroundColor: UIColor.red ]
    let mmkString = NSMutableAttributedString.init(string: " * ", attributes: customAttribute2)
    attributedAmountString.append(mmkString)
   
    return attributedAmountString

    //self.amountTF.attributedPlaceholder = attributedAmountString
    }
}



//MARK: - ContactPickerDelegate
extension SelectedPersonViewController: ContactPickerDelegate {
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        let phoneCount = contact.phoneNumbers.count
        var phoneNumber = ""
        if phoneCount > 0 {
            phoneNumber = contact.phoneNumbers[0].phoneNumber
        }
        
        if phoneNumber.count <= 0 {
           
            self.mobileNumTxt.text = "09"
            closeButton.isHidden = true
         //   mobileNumberView.hideClearButton(hide: true)
            PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
         //   self.delegate?.showAddObjectButton(show: false)
            return
        }
        var str = ""
        str = phoneNumber
        if str.count > 0 {
            str = str.replacingOccurrences(of: "(", with: "")
            str = str.replacingOccurrences(of: ")", with: "")
            str = str.trimmingCharacters(in: .whitespaces)
        }
        if str.hasPrefix("00") {
            _ = str.remove(at: String.Index.init(encodedOffset: 0))
            _ = str.remove(at: String.Index.init(encodedOffset: 0))
            str = "+" + str
        }
        
        let country = self.identifyCountry(withPhoneNumber: str)
        if country.0 == "+95" ||  country.1 == "myanmar" ||  country.0 == "" ||  country.1 == "" {
            let countryObject = Country.init(name: "", code: "myanmar", dialCode: "+95")
            if str.hasPrefix(countryObject.dialCode) {
                str = str.replacingOccurrences(of: countryObject.dialCode, with: "")
                str = "0" + str
            } else {
                if str.hasPrefix("0") {
                    
                } else {
                    str = "0" + str
                }
            }
            let number = PayToValidations().getNumberRangeValidation(str)

            if str.count > number.max || str.count < number.min || number.isRejected {
             //   mobileNumberView.hideClearButton(hide: true)
                closeButton.isHidden = true
                self.mobileNumTxt.text = "09"
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
             //   self.delegate?.showAddObjectButton(show: false)
             //   self.clearActionType()
                return
            }
         
            
            self.mobileNumTxt.text = str
           
            self.buyerNameTxt.text = contact.displayName()
            closeButton.isHidden = true

           
          //  self.mobileNumberView.hideClearButton(hide: true)
        } else {
          //  self.delegate?.showAddObjectButton(show: false)
            guard let keyWinndow = UIApplication.shared.keyWindow else { return }
            keyWinndow.makeToast("You Selected Other Country Number".localized, point: keyWinndow.center, title: nil, image: nil, completion: nil)
        }
    }
}
