//
//  LotteryProtocol.swift
//  OK
//
//  Created by Tushar Lama on 10/11/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

//this will return all company list
protocol GetCompanyList {
    func companyList(model: LotteryCompanyModel)
}

//this will return price for particular company

protocol GetCompanyPriceList {
    func companyPriceList(model: LotteryPriceListModel)
}


//this will return coupon as per number search

protocol GetCouponList {
    func coupponList(model: LotteryTicketListModel)
}


//this will reserve Ticker

protocol LotteryReserveTicket {
    func reserveTicket(model: NSDictionary?)
}


//lottery final model

protocol LotteryReceiptModel {
    func receipt(response: LotteryFinalModel)
}

protocol LotteryVerifyWinningSheet {
    func winningSheet(response:LotterywinningsheetModel)
}
