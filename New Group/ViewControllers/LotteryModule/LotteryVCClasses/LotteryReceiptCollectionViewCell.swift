//
//  LotteryReceiptCollectionViewCell.swift
//  LotteryModule
//
//  Created by iMac on 21/09/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class LotteryReceiptCollectionViewCell: UICollectionViewCell {
    @IBOutlet var receiptTicketImageView: UIImageView!
    
    @IBOutlet var ticketNumberLabel: UILabel!
}
