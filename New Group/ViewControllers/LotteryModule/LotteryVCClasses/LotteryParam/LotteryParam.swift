//
//  LotteryParam.swift
//  OK
//
//  Created by Tushar Lama on 10/11/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

struct LotteryParam {
    
    static func getParamForCompanyList() -> [String : Any] {
        var parameters:[String : Any] = [:]
        parameters = ["ObjLoginAuthDto": [
            "MobileNumber":UserModel.shared.mobileNo,
            "Simid":uuid,
            "Msid":msid,
            "Ostype":1,
            "Otp":"",
            "AppId":UserModel.shared.appID,
            "Limit":0,
            "Offset":0
        ]
        ]
        return parameters
    }
    
    static func getParamFromCompanyId(companyId: String) -> [String: Any] {
        var parameters:[String : Any] = [:]
        
        parameters = ["ObjLoginAuthDto": [
            "MobileNumber":UserModel.shared.mobileNo,
            "Simid":uuid,
            "Msid":msid,
            "Ostype":1,
            "Otp":"",
            "AppId":UserModel.shared.appID,
            "Limit":0,
            "Offset":0
        ],
        "CompanyId": companyId
        ]
        
        return parameters
        
    }
    
    //param to get list by number
    
    static func getListByNumber(productId: String,searchString: String) -> [String: Any] {
        var parameters:[String : Any] = [:]
        parameters = ["ObjLoginAuthDto": [
            "MobileNumber":UserModel.shared.mobileNo,
            "Simid":uuid,
            "Msid":msid,
            "Ostype":1,
            "Otp":"",
            "AppId":UserModel.shared.appID,
            "Limit":0,
            "Offset":0
        ],
            "ProductId":productId,
            "SearchString":searchString
        ]
        
        return parameters
    }
    
    
    //reserve ticket param
    
    static func reserveTicket(productId: String,luckyNumber: String)  -> [String: Any]{
        var parameters:[String : Any] = [:]
        parameters = ["ObjLoginAuthDto": [
            "MobileNumber":UserModel.shared.mobileNo,
            "Simid":uuid,
            "Msid":msid,
            "Ostype":1,
            "Otp":"",
            "AppId":UserModel.shared.appID,
            "Limit":0,
            "Offset":0
        ],
            "ProductId":productId,
            "LuckyNum":luckyNumber
        ]
        return parameters
    }
    
    
   
    
  
    
    
    //verify all number
    static func verifyAllNumber(ticketList: [LotteryCouponDetails],amount: String) -> [String: Any]{
        
        let list = ticketList
        var ticketMainArray = [[String : Any]]()
            
        for valTicket in list {
            var dictTemp : [String:Any] = [:]
            dictTemp["LuckyNumber"] = valTicket.couponNumber
            dictTemp["ProductId"] = valTicket.productId
            dictTemp["Amount"] = amount
            ticketMainArray.append(dictTemp)
        }
        
        let childDicts = [
            "LuckyNumberList": ticketMainArray,
            "ObjLoginAuthDto" : [
                "MobileNumber":UserModel.shared.mobileNo,
                "Simid":uuid,
                "Msid":msid,
                "Ostype":1,
                "Otp":"",
                "AppId":UserModel.shared.appID,
                "Limit":0,
                "Offset":0
            ]
            ] as [String: Any]
        
        
        return childDicts
        
    }
    
    
    static func luckyNumPaymentParam(ticketList: [LotteryCouponDetails]) -> [String: Any]{
        
        let list = ticketList
        var ticketMainArray = [[String : Any]]()
            
        for valTicket in list {
            var dictTemp : [String:Any] = [:]
            dictTemp["LuckyNumber"] = valTicket.couponNumber
            dictTemp["ProductId"] = valTicket.productId
            
            ticketMainArray.append(dictTemp)
        }
        
        let childDicts = [
            "LuckyNumberList": ticketMainArray,
            "ObjLoginAuthDto" : [
                "MobileNumber":UserModel.shared.mobileNo,
                "Simid":uuid,
                "Msid":msid,
                "Ostype":1,
                "Otp":"",
                "AppId":UserModel.shared.appID,
                "Limit":0,
                "Offset":0
            ],
            "Password" : ok_password ?? "",
            "SecureToken": UserLogin.shared.token
            ] as [String: Any]
        
        
        return childDicts
        
    }
    
    //Verify Winning ticket param
    
    static func Verifywinningsheet(productId: String)  -> [String: Any]{
        var parameters:[String : Any] = [:]
        parameters = ["ObjLoginAuthDto": [
            "MobileNumber":UserModel.shared.mobileNo,
            "Simid":uuid,
            "Msid":msid,
            "Ostype":1,
            "Otp":"",
            "AppId":UserModel.shared.appID,
            "Limit":0,
            "Offset":0
        ],
            "ProductId":productId
        ]
        return parameters
    }
  
}
