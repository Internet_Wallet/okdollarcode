//
//  WinningSheetViewController.swift
//  OK
//
//  Created by iMac on 01/12/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit
import SDWebImage

class WinningSheetViewController: OKBaseController,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,LotteryVerifyWinningSheet {

    @IBOutlet var navigationView: UIView!
    @IBOutlet var navigationTitleLabel: UILabel!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var timeSelectTF: UITextField!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var lotterySheetImageView: UIImageView!
    
    var productid = ""
    var lotterywinningsheet = LotteryViewModel()
    var lotterywinningsheetModel =  [Lotterywinningsheetdetails]()

    var sortarray : [Lotterywinningsheetdetails] = []
   
    
    var  pickerTextView = UITextView()
    var strTF = String()
    var timePicker = UIPickerView()
    var timearr = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        timeSelectTF.rightViewMode = .always
        timeSelectTF.rightView = UIImageView(image: UIImage(named: "ic_keyboard_arrow_down"))
        
        timePicker.delegate = self
        timePicker.dataSource = self
     
        self.apicall()

    }
    
    
    func apicall(){
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            lotterywinningsheet.verifywinningsheetDel = self
            lotterywinningsheet.Verifywinningsheet(productId: productid)
            
        }else {
            self.noInternetAlert()
            progressViewObj.removeProgressView()
        }
    }
    
    func winningSheet(response:LotterywinningsheetModel){
        lotterywinningsheetModel.removeAll()
        progressViewObj.removeProgressView()
        
        if !response.isTransactionSuccess{
            //failed
            DispatchQueue.main.async {
                self.showErrorAlert(errMessage: response.msg ?? "")
            }
        }
        else if response.lotterywinningsheetDetails.count>0{
            
            lotterywinningsheetModel = response.lotterywinningsheetDetails
            
            lotterywinningsheetModel = lotterywinningsheetModel.sorted(by: { $0.winningNumber ?? "" < $1.winningNumber ?? "" })
            
            DispatchQueue.main.async {
                self.timeSelectTF.text = self.lotterywinningsheetModel[0].winningNumber
                self.lotterySheetImageView.sd_setImage(with: URL(string: self.lotterywinningsheetModel[0].imageUrl ?? ""), placeholderImage: nil)
            }
        }
        
        else{
            DispatchQueue.main.async {
                self.showErrorAlert(errMessage: "Some error happen please try again".localized)
            }
        }
    }
    
    
    
    
  
    @IBAction func backButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //delegate method
        if textField == timeSelectTF
        {
            timeSelectTF.inputView = timePicker
            timeSelectTF.tintColor = UIColor.clear
        }
    }
}

extension WinningSheetViewController{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
         return 1
     }
     
     func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
         
        return lotterywinningsheetModel.count
     }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       
        return lotterywinningsheetModel[row].winningNumber
      
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        timeSelectTF.text =  lotterywinningsheetModel[row].winningNumber
        timeSelectTF.textColor = .red
        lotterySheetImageView.sd_setImage(with: URL(string: lotterywinningsheetModel[row].imageUrl ?? ""), placeholderImage: nil)
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
  
        let pickerLabel = UILabel()
        
        pickerLabel.textAlignment = .center
        
        pickerLabel.text = lotterywinningsheetModel[row].winningNumber
        
        return pickerLabel
    }
}
