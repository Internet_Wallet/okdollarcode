//
//  LotteryPriceTableViewCell.swift
//  LotteryModule
//
//  Created by iMac on 30/09/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class LotteryPriceTableViewCell: UITableViewCell {
    
       @IBOutlet var cellView: UIView!
       @IBOutlet var priceLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func wrapDataInCell(priceValue: String){
        self.layer.cornerRadius = 7.0;
        self.layer.masksToBounds = true
        self.cellView.layer.cornerRadius = 7.0
        self.cellView.layer.borderColor = UIColor.white.cgColor
        self.cellView.layer.borderWidth = 0.5
        priceLabel.text = "Price : \(priceValue)"
    }

}
