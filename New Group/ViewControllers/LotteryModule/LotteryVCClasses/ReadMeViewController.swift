//
//  ReadMeViewController.swift
//  LotteryModule
//
//  Created by iMac on 18/09/2020.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class ReadMeViewController: OKBaseController {

    @IBOutlet var navigationView: UIView!
    @IBOutlet var navigationTitleLable: UILabel!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var agreeButton: UIButton!
    @IBOutlet var commentsLabel: UILabel!
    var lotterydict: LotteryCompanyDetails?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationController?.navigationBar.isHidden = true
        commentsLabel.text = lotterydict?.comments ?? ""
        navigationTitleLable.text = lotterydict?.companyName ?? ""

    }

    @IBAction func backButtonClick(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func agreeButtonClick(_ sender: Any) {
        var homeVC: LotteryPriceViewController?
        homeVC = UIStoryboard(name: LotteryConstant.kLotteryStoryBoard, bundle: nil).instantiateViewController(withIdentifier: LotteryConstant.kLotteryPriceViewController) as? LotteryPriceViewController
        if let homeVC = homeVC {
            homeVC.companyiddict = lotterydict
            navigationController?.pushViewController(homeVC, animated: true)
        }
        
//        var homeVC: ChooseOptionViewController?
//        homeVC = UIStoryboard(name: LotteryConstant.kLotteryStoryBoard, bundle: nil).instantiateViewController(withIdentifier: "ChooseOptionViewController") as? ChooseOptionViewController
//        if let homeVC = homeVC {
//            homeVC.companyiddict = lotterydict
//            navigationController?.pushViewController(homeVC, animated: true)
//        }
        
    }
}
