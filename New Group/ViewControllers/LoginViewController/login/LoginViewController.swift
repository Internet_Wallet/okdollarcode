//
//  LoginViewController.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/20/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import AppTrackingTransparency
import AdSupport

protocol SubmitButtonDelegate {
    func didTapSubmitButton()
}

protocol BioMetricLoginDelegate: class {
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String)
}

class LoginViewController: OKBaseController, WebServiceResponseDelegate, SubmitButtonDelegate {
    //MARK: - Outlets
    @IBOutlet weak var passwordContainer: UIView!
    @IBOutlet weak var patternContainer: UIView!
    @IBOutlet weak var forgotPasswordBtn: UIButton!{
        didSet{
            self.forgotPasswordBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var otherIssuesBtn: UIButton!{
        didSet{
            self.otherIssuesBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var changeMobNumButton: UIButton!{
        didSet{
            self.changeMobNumButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var passwordPatternBtn: UIButton! {
        didSet {
            self.passwordPatternBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            passwordPatternBtn.isHidden = true
            self.passwordPatternBtn.setTitle("Click here to Login with Pattern".localized, for: .normal)
        }
    }
    @IBOutlet weak var loginSubmit: UIButton!{
        didSet{
            self.loginSubmit.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var leftButton: UIButton!{
        didSet{
            self.leftButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var passwordPatternBtnHeight: NSLayoutConstraint! {
        didSet {
            passwordPatternBtnHeight.constant = 0.0
        }
    }

    //FailureCount Managed on Jan12,2021
    var failureCountTrxFailed = 0
    
    //MARK: - Properties
    var ApnsTransaction = Dictionary<String,Any>()
    // biometric login scenarios
    var authenticateScreen = false
    weak var biometricDelegate : BioMetricLoginDelegate?
    var biometricString = ""
    var forgotPasswordTapped = false
    // login other ui updates
    var passVC    : lPasswordViewController!
    var patternVC : lPatternViewController!
    var screenInitialFrom = false
    var agentCode : String!
    var paramsFailed = Dictionary<String,Any>()
    var containerType : UIView! {
        didSet {
            if containerType == passwordContainer {
                self.passwordContainer.isHidden = false
                self.patternContainer.isHidden  = true
            } else {
                self.patternContainer.isHidden  = false
                self.passwordContainer.isHidden = true
            }
        }
    }
    var psswordType = ""
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - Views life cycle methods
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
        
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = kYellowColor
        statusBarView.backgroundColor = statusBarColor
        self.view.addSubview(statusBarView)
        
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        
        //Reset Failure Value
        failureCountTrxFailed = 0
    }
    
    
    //NEWLY ADDED PERMISSIONS FOR iOS 14
    func requestPermission() {
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization { status in
                switch status {
                case .authorized:
                    // Tracking authorization dialog was shown
                    // and we are authorized
                    print("Authorized")
                    
                    // Now that we are authorized we can get the IDFA
                    print(ASIdentifierManager.shared().advertisingIdentifier)
                case .denied:
                    // Tracking authorization dialog was
                    // shown and permission is denied
                    print("Denied")
                case .notDetermined:
                    // Tracking authorization dialog has not been shown
                    print("Not Determined")
                case .restricted:
                    print("Restricted")
                @unknown default:
                    print("Unknown")
                }
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        println_debug("Login viewDidLoad")
        
        self.navigationItem.setHidesBackButton(true, animated: false)
        if screenInitialFrom {
            let agentCodeArray = agentCode.components(separatedBy: ",")
            if agentCodeArray.count > 1 {
                agentCode = getAgentCode(numberWithPrefix: agentCodeArray.first ?? "", havingCountryPrefix: agentCodeArray[1] )
                preLoginModel.wrapPreLoginData(agent: agentCodeArray.first ?? "", cCode: agentCodeArray[1] , fCode: agentCodeArray.last ?? "")
                passVC.updatePasswordView()
            }
        }
        containerType = passwordContainer
        patternVC.autoFireSubmit = self
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = self.navigationItem.title?.localized
        if UserDefaults.standard.integer(forKey: "App_Password_Type") == 0 {
            containerType = (ok_password_type) ?  patternContainer : passwordContainer
            leftButton.isHidden = true
            let senderString = (containerType == passwordContainer) ? "Pattern" : "Password"
            if containerType == patternContainer {
                self.bottomView.isHidden = true
            }
            passwordPatternBtn.isHidden = false
            passwordPatternBtnHeight.constant = 35.0
            leftButton.setTitle(senderString, for: .normal)
        } else {
            passwordPatternBtnHeight.constant = 0.0
            leftButton.isHidden = false
            leftButton.setTitle("Cancel".localized, for: .normal)
        }
        if authenticateScreen {
            containerType = (ok_password_type) ?  patternContainer : passwordContainer
            leftButton.setTitle("Cancel".localized, for: .normal)
        }
        if containerType == patternContainer {
            self.passwordPatternBtn.setTitle("Click here to Login with Password".localized, for: .normal)
            loginSubmit.isHidden = true
        } else {
            self.passwordPatternBtn.setTitle("Click here to Login with Pattern".localized, for: .normal)
            loginSubmit.isHidden = false
        }
        leftButton.setTitle("Cancel".localized, for: .normal)
        loginSubmit.setTitle(loginSubmit.titleLabel?.text?.localized, for: .normal)
        forgotPasswordBtn.setTitle(forgotPasswordBtn.titleLabel?.text?.localized, for: .normal)
        otherIssuesBtn.setTitle(otherIssuesBtn.titleLabel?.text?.localized, for: .normal)
        DispatchQueue.main.async {
            self.checkOldNewRegistration()
            self.getSecurityQuestion()
        }
        changeMobNumButton.setTitle(changeMobNumButton.titleLabel?.text?.localized, for: .normal)
        NotificationCenter.default.addObserver(self, selector: #selector(enableBothLoginOptions), name: NSNotification.Name(rawValue: "ShowLoginBothViews"), object: nil)        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        forgotPasswordTapped = false
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
    //MARK: - Methods
    private  func setPOPButton() -> Void {
        let buttonIcon      = UIImage(named: "tabBarBack")
        let leftBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: self, action: #selector(LoginViewController.popTheViewControllerPT(_:)))
        leftBarButton.image = buttonIcon
        self.navigationItem.leftBarButtonItem = leftBarButton
    }
    
    // MARK: Subhash Api called for re-verify
    func checkOldNewRegistration() {
        
        self.webCall(url: Url.URL_CheckRegistrationStatus, Screen: "RegistrationStatus")
    }
    
    func getSecurityQuestion() {
        self.webCall(url: Url.URL_ViewSecurityQuestions, Screen: "SecurityQuestion")
    }
    
   
    
    private func webCall(url:String , Screen : String) {
        
        if appDelegate.checkNetworkAvail() {
            var mobNum = ""
            if preLoginModel.agentCode == "" {
                mobNum =  UserModel.shared.mobileNo
            } else {
                mobNum =  preLoginModel.agentCode
            }
            var urlString   = url + "MobileNumber=" + mobNum + "&Simid=" + uuid + "&MSID=" + msid + "&OSType=" + "1" + "&OTP=" + uuid
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            VillageManager.shared.webcallForGet(urlStr: urlString, screen: Screen)
        } else {
           // self.noInternetAlert()
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            alertViewObj.showAlert(controller:self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? lPasswordViewController {
            passVC = vc
        }
        if let vc = segue.destination as? lPatternViewController {
            patternVC = vc
        }
    }
    
    func didTapSubmitButton() {
        self.loginSubmit.sendActions(for: .touchUpInside)
    }
    
    fileprivate func extractedFunc(_ dic: [String : AnyObject]) {
        
        if let login = dic["Data"] as? String, login != "" {
            if  let dic = OKBaseController.convertToDictionary(text: login) {
                println_debug("Login Response: \(dic)")
                if let log = dic["AgentDetails"] as? Array<Any> {
                    if let logi = log.first as? Dictionary<String,Any> {
                        if logi["ResultDescription"] as? String == "Transaction Successful"{
                            if UserDefaults.standard.integer(forKey: "App_Password_Type") == 0 {
                                UserDefaults.standard.set(-1, forKey: "App_Password_Type")
                            }
                            // change on//8006932264
                            if UserModel.shared.accountType != "" {
                            if logi["agenttype"] as? String == "SUBSBR" && UserModel.shared.agentType == .user {
                                // UserModel.shared.agentType = .user
                            }else if logi["agenttype"] as? String == "MER" && UserModel.shared.agentType == .merchant {
                                //UserModel.shared.agentType = .merchant
                            }else if logi["agenttype"] as? String == "AGENT" && UserModel.shared.agentType == .agent{
                                // UserModel.shared.agentType = .agent
                            }else if logi["agenttype"] as? String == "ADVMER" && UserModel.shared.agentType == .advancemerchant{
                                //UserModel.shared.agentType = .advancemerchant
                            }else if logi["agenttype"] as? String == "DUMMY" && UserModel.shared.agentType == .dummymerchant{
                                // UserModel.shared.agentType = .dummymerchant
                            }else {
                                var mobNum = ""
                                   if preLoginModel.agentCode == "" {
                                       mobNum =  UserModel.shared.mobileNo
                                   } else {
                                       mobNum =  preLoginModel.agentCode
                                   }
                                profileObj.callUpdateProfileApi(aCode: mobNum, handler: { (success) in
                                    if success {
                                        dashboard?.reloadDashBoard()
                                    }else {
                                        println_debug("callUpdateProfileApi Failed")
                                    }
                                })
                                userDef.set(logi["agenttype"] as? String, forKey: "changeType")
                                NotificationCenter.default.post(name: .changeUserType, object: nil, userInfo: nil)
                                
                                }
                            }
                            
                            userDef.removeObject(forKey: "ReverifyClicked")
                            UserLogin.shared.sessionTimerStart()
                            ok_password_type = userDef.bool(forKey: "UpdatePassword_Type")
                            userDef.removeObject(forKey: "UpdatePassword_Type")
                       
                            
                            UserLogin.wrapUserData(dict: logi)
                            
                            let defaults = UserDefaults.standard
                            defaults.set("", forKey: "password")
                            
                            if let myString = defaults.string(forKey: "password"){
                                println_debug("Password: \(myString)")
                            }
                            //RE_REGITER
                            let passDetails = (self.containerType == self.patternContainer) ? self.patternVC.capturePasswordWithDetails(self.agentCode) : self.passVC.capturePasswordWithDetails()
                            let passwordTypeString = passDetails.type ? "1" : "0"
                            ReRegistrationModel.shared.SecureToken = UserLogin.shared.token
                            ReRegistrationModel.shared.PasswordType = passwordTypeString
                            self.psswordType = passwordTypeString
                            ReRegistrationModel.shared.Password = passDetails.password
                            if logi["RegistrationStatus"] as? NSNumber == -1 {
                                
                                DispatchQueue.main.async {
                                    if !UserDefaults.standard.bool(forKey: "Re_Registration_Presented") {
                                        if UserModel.shared.language == "" {
                                            ReRegistrationModel.shared.Language = userDef.value(forKey: "AppRegisteredLanguage") as? String ?? ""
                                            if ReRegistrationModel.shared.Language == "" {
                                                ReRegistrationModel.shared.Language = ok_default_language
                                            }
                                        }else {
                                            ReRegistrationModel.shared.Language = UserModel.shared.language.lowercased()
                                        }
                                        ReRegistrationModel.shared.RegistrationStatus = 1
                                        ReRegistrationModel.shared.lType = logi["agenttype"] as? String ?? ""
                                        if preLoginModel.agentCode == "" {
                                            ReRegistrationModel.shared.MobileNumber =  UserModel.shared.mobileNo
                                        } else {
                                            ReRegistrationModel.shared.MobileNumber =  preLoginModel.agentCode
                                        }
                                        
                                        userDef.set(ReRegistrationModel.shared.Language, forKey: "AppRegisteredLanguage")
                                        userDef.synchronize()
                                        
                                        UserDefaults.standard.set(true, forKey: "RE_REGITRATION")
                                        ReRegistrationModel.shared.clearValues()
                                        let sb = UIStoryboard(name: "Registration", bundle: nil)
                                        let re_registraion = sb.instantiateViewController(withIdentifier: "ReRegitrationAuthVC") as! ReRegitrationAuthVC
                                        re_registraion.modalPresentationStyle = .fullScreen
                                        self.present(re_registraion, animated: false, completion: {
                                            if let keyWindow = UIApplication.shared.keyWindow {
                                                for view in keyWindow.subviews {
                                                    if view.tag == 909786 {
                                                        view.removeFromSuperview()
                                                        //OKSessionValidation.isMainAppSessionExpired = false
                                                    }
                                                }
                                            }
                                        })
                                    }else {
                                        if let keyWindow = UIApplication.shared.keyWindow {
                                            for view in keyWindow.subviews {
                                                if view.tag == 909786 {
                                                    if UserDefaults.standard.bool(forKey: "Re_Registration_Presented") {
                                                        NotificationCenter.default.post(name: Notification.Name("HIT_RE_REGISTRATION_API"), object: nil)
                                                    }
                                                    view.removeFromSuperview()
                                                }
                                            }
                                        }
                                    }
                                }
                            }else {
                                
                               // "caskbackPayto"{
                                //    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PAYTO_LOGIN"), object: nil)
                               // }
                                
                                _ = (authenticateScreen) ? self.callbackForBiometricsLogin(resultStatus: true, mScreen: biometricString) : self.updateProfile()
                            }
                        } else {
                            if authenticateScreen { self.callbackForBiometricsLogin(resultStatus: false, mScreen: biometricString) }
                            failureCountTrxFailed = failureCountTrxFailed + 1
                            if failureCountTrxFailed == 3 {
                                let failureViewModel = FailureViewModel()
                                failureViewModel.sendDataToFailureApi(request: paramsFailed as Any, response: dic, type: FailureHelper.FailureType.LOGIN.rawValue, finished: {
                                })
                            }
                        }
                    }
                } else {
                   // NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PAYTO_LOGIN"), object: nil)
                    if authenticateScreen {
                        
                        self.callbackForBiometricsLogin(resultStatus: false, mScreen: biometricString) }
                }
            }
        } else  {
            if let code = dic["Code"] as? NSNumber, code == 300 {
                
                DispatchQueue.main.async {
                    self.failureCountTrxFailed = self.failureCountTrxFailed + 1
                    if self.failureCountTrxFailed == 3 {
                        let failureViewModel = FailureViewModel()
                        failureViewModel.sendDataToFailureApi(request: self.paramsFailed as Any, response: dic, type: FailureHelper.FailureType.LOGIN.rawValue, finished: {
                        })
                    }
                }
                
                alertViewObj.wrapAlert(title: nil, body: "Your device/sim changed. Please do Re-Verify".localized, img: #imageLiteral(resourceName: "mobile"))
                alertViewObj.addAction(title: "Cancel".localized, style: .target, action: {
                    
                })
                alertViewObj.addAction(title: "OK".localized.localized, style: .target , action: {
                    userDef.set(true, forKey: "ReverifyClicked")
                    self.navigateToReVerify()
                })
                alertViewObj.showAlert(controller: self)
            }else if let code = dic["Code"] as? NSNumber, code == 306 {
                
                DispatchQueue.main.async {
                    
                    self.failureCountTrxFailed = self.failureCountTrxFailed + 1
                    if self.failureCountTrxFailed == 3 {
                        let failureViewModel = FailureViewModel()
                        failureViewModel.sendDataToFailureApi(request: self.paramsFailed as Any, response: dic, type: FailureHelper.FailureType.LOGIN.rawValue, finished: {
                        })
                    }
                }
                
                alertViewObj.wrapAlert(title: nil, body: "Login Not Match with OK$".localized, img: nil)
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                alertViewObj.showAlert(controller: self)
            }else if let code = dic["Code"] as? NSNumber, code == 307 {
                
                DispatchQueue.main.async {
                    self.failureCountTrxFailed = self.failureCountTrxFailed + 1
                    if self.failureCountTrxFailed == 3 {
                        let failureViewModel = FailureViewModel()
                        failureViewModel.sendDataToFailureApi(request: self.paramsFailed as Any, response: dic, type: FailureHelper.FailureType.LOGIN.rawValue, finished: {
                        })
                    }
                }
                alertViewObj.wrapAlert(title: nil, body: "Safety Cashier can not able login in OK$".localized, img: nil)
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                alertViewObj.showAlert(controller: self)
            }else if let code = dic["Code"] as? NSNumber, code == 308 {
                
                DispatchQueue.main.async {
                    self.failureCountTrxFailed = self.failureCountTrxFailed + 1
                    if self.failureCountTrxFailed == 3 {
                        let failureViewModel = FailureViewModel()
                        failureViewModel.sendDataToFailureApi(request: self.paramsFailed as Any, response: dic, type: FailureHelper.FailureType.LOGIN.rawValue, finished: {
                        })
                    }
                }
                alertViewObj.wrapAlert(title: nil, body: dic["Msg"] as! String, img: nil)
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                alertViewObj.showAlert(controller: self)
            }else if let code = dic["Code"] as? NSNumber, code == 60 {
                DispatchQueue.main.async {
                    self.failureCountTrxFailed = self.failureCountTrxFailed + 1
                    if self.failureCountTrxFailed == 3 {
                        let failureViewModel = FailureViewModel()
                        failureViewModel.sendDataToFailureApi(request: self.paramsFailed as Any, response: dic, type: FailureHelper.FailureType.LOGIN.rawValue, finished: {
                        })
                    }
                }
                if containerType == patternContainer {
                    alertViewObj.wrapAlert(title: nil, body: "You have Drawn Incorrect Pattern. Please try again".localized, img: nil)
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                      
                    })
                    alertViewObj.showAlert(controller: self)
                    
                } else {
                alertViewObj.wrapAlert(title: nil, body: "You entered Incorrect Password. Please try again".localized, img: nil)
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                alertViewObj.showAlert(controller: self)
                }
            }else {
                
                DispatchQueue.main.async {
                    
                    self.failureCountTrxFailed = self.failureCountTrxFailed + 1
                    if self.failureCountTrxFailed == 3 {
                        let failureViewModel = FailureViewModel()
                        failureViewModel.sendDataToFailureApi(request: self.paramsFailed as Any, response: dic, type: FailureHelper.FailureType.LOGIN.rawValue, finished: {
                        })
                    }
                }
                alertViewObj.wrapAlert(title: nil, body: dic["Msg"] as! String, img: nil)
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        DispatchQueue.main.async {
            progressViewObj.removeProgressView()
        }
  
        if screen == "mLogin" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug("OK$LOGIN RESPONSE")
                        println_debug(dic)
                        println_debug("*************************")
                        UserDefaults.standard.set(false, forKey: "isLogOut")
                        DispatchQueue.main.async {
                            self.extractedFunc(dic)
                        }
                    }
                }
            } catch {
                
            }
        }
        if screen == "mProfile" {
            if let data = json as? Data {
                do {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dataDict = dict as? Dictionary<String,AnyObject> {
                        if let dic = dataDict["Data"] as? String {
                            println_debug(dic)
                            let profile = OKBaseController.convertToDictionary(text: dic)
                            if profile == nil { return }
                            User.shared.wrapModel(dic: profile! as Dictionary<String, AnyObject>)
                            DispatchQueue.main.async {
                                self.navigateToDashboard()
                            }
                        }
                    }
                } catch {
                }
            }
        }
        if screen == "ShopStatusApi" {
            // Shop Status Api
            if let data = json as? Data {
                do {
                    let dict = try JSONSerialization.jsonObject(with: data, options: [])
                    
                    if let dataDict = dict as? Dictionary<String,Any> {
                        self.parseJsonData(dataDict)
                    }
                } catch {
                }
            }
        }
        if screen == "PUSHNOTIFICATION" {
            // /*
            // Appserver API
            if let data = json as? Data {
                do {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dataDict = dict as? Dictionary<String,AnyObject> {
                        
                        print("*********************\(dataDict)****************************")
                        if let str = dataDict["Data"] as? String {
                            println_debug("Push Notification API Response::: \(str)")
                        }
                        
                        
                    }
                } catch {
                }
            }
        }
    }
    
    func parseJsonData(_ jsonWrapped: Dictionary<String,Any>) {
        if let stringData = jsonWrapped["Data"] as? String, (jsonWrapped["Code"] as? NSNumber)?.intValue == 200 {
            let dictionaryObj = OKBaseController.convertToDictionary(text: stringData)
            println_debug(dictionaryObj!)
            if dictionaryObj == nil {return}
            
            if let arrayObjects = dictionaryObj?.safeValueForKey("ShopStatus") as? [Any] {
                let dictionaryArray = OKBaseController.convertToArrDictionary(text: arrayObjects.jsonString())
                //                    let dictionary = OKBaseController.convertToDictionary(text: arrayObjects.jsonString())
                //                    println_debug(dictionary!)
                var lat = ""
                var long = ""
                for element in (dictionaryArray![0] as? Dictionary<String,String>)! {
                    println_debug(element)
                    if element.key == "PromotionId"{
                        println_debug(element.value)
                        if element.value == "<null>"{
                            UserDefaults.standard.set(uuid, forKey: "PromotionId")
                        } else{
                            UserDefaults.standard.set(element.value, forKey: "PromotionId")
                        }
                        UserDefaults.standard.synchronize()
                    } else if element.key == "ShopId"{
                        println_debug(element.value)
                        UserDefaults.standard.set(element.value, forKey: "shopID")
                        UserDefaults.standard.synchronize()
                    } else if element.key == "Latitude"{
                        println_debug(element.value)
                        lat = element.value
                        UserDefaults.standard.set(element.value, forKey: "lat")
                        UserDefaults.standard.synchronize()
                    } else if element.key == "Longitude"{
                        println_debug(element.value)
                        long = element.value
                        UserDefaults.standard.set(element.value, forKey: "long")
                        UserDefaults.standard.synchronize()
                    } else if element.key == "PromotionName"{
                        println_debug(element.value)
                        UserDefaults.standard.set(element.value, forKey: "PromotionName")
                        UserDefaults.standard.synchronize()
                    } else if element.key == "ShopName"{
                        println_debug(element.value)
                        UserDefaults.standard.set(element.value, forKey: "txtValue")
                        UserDefaults.standard.synchronize()
                    } else if element.key == "AgentName"{
                        println_debug(element.value)
                        UserDefaults.standard.set(element.value, forKey: "AgentName")
                        UserDefaults.standard.synchronize()
                    } else if element.key == "PromotionUpdatedDate"{
                        UserDefaults.standard.set(self.convertDateFormater(dateValue: element.value), forKey: "dateAndTime")
                        UserDefaults.standard.synchronize()
                    }
                }
                let userLocation = ["lat": Double(lat), "long": Double(long)]
                UserDefaults.standard.set(userLocation, forKey: "BussinessDoneCurrentLocation")
                UserDefaults.standard.synchronize()
            }
            self.updateCurrentLocationToUI()
        }
    }
    
    func convertDateFormater(dateValue: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        let dateChange = dateFormatter.date(from: dateValue)
        dateFormatter.dateFormat = "MMM dd,yyyy HH:mm:ss"
        if let dateChng = dateChange {
            return  dateFormatter.string(from: dateChng)
        }
        return ""
    }
    
    func updateCurrentLocationToUI() {
        geoLocManager.startUpdateLocation()
        guard appDelegate.checkNetworkAvail() else{
            self.showErrorAlert(errMessage: "Network Not Available")
            return
        }
        
        if let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude {
            self.showProgressView()
            geoLocManager.getLocationNameByLatLong(lattitude: lat, longitude: long) {(isSuccess, currentAddress) in
                if isSuccess {
                    DispatchQueue.main.async {
                        if let curAddress = currentAddress as? String {
                            UserDefaults.standard.set(curAddress, forKey: "curAddress")
                            UserDefaults.standard.synchronize()
                        } else{
                            println_debug("Loading location.....")
                        }
                        self.removeProgressView()
                        geoLocManager.stopUpdateLocation()
                    }
                }
                return
            }
        }
    }
    
    func navigateToOldFirstPassword () {
        forgotPasswordTapped = true
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: {
            let sb = UIStoryboard(name: "ForgotPassword" , bundle: nil)
            let VC  = sb.instantiateViewController(withIdentifier: "OldForgotPassword")
            VC.modalPresentationStyle = .fullScreen
            self.present(VC, animated: true, completion: nil)
        })
    }
    
    func getParamsForAPN() -> Dictionary<String, Any> {
        var dict = Dictionary<String, Any>()
        
        print(UserModel.shared.appID)
        dict["AppId"] =  buildNumber
       // dict["AppId"] = LoginParams.setUniqueIDLogin()
        dict["AppVersion"] = buildVersion
        dict["GcmId"] = UserDefaults.standard.value(forKey: "deviceToken") as? String
        dict["MSID"] = UserModel.shared.msid
        dict["MobileNumber"] = UserModel.shared.mobileNo
        dict["OsType"] = 1
        dict["OSVersion"] =  UIDevice.current.systemVersion
        dict["Password"] = ok_password ?? ""
        dict["PhoneBrand"] = "iPhone"
        dict["PhoneModel"] = String(UIDevice().type.rawValue)
        dict["SecureToken"] = UserLogin.shared.token
        dict["SimId"] = uuid
        
        
        
        
        
        
        return dict
    }
    
    func navigateToOldSecondPassword() {
        forgotPasswordTapped = true
        if preLoginModel.agentCode == "" {
            ForgotPasswordModel.share.MobileNumber = UserModel.shared.mobileNo
        } else {
            ForgotPasswordModel.share.MobileNumber = preLoginModel.agentCode
        }
        
        let sb = UIStoryboard(name: "ForgotPassword" , bundle: nil)
        let vcForogtPassword  = sb.instantiateViewController(withIdentifier: "NewForgotPassword")
        vcForogtPassword.modalPresentationStyle = .fullScreen
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topController.present(vcForogtPassword, animated: false, completion: nil)
        }
    }
    
    func callbackForBiometricsLogin(resultStatus stat: Bool, mScreen: String) {
        if psswordType == "0" {
          self.setKeyoboardType()
        }
        if let delegate = biometricDelegate {
         delegate.biometricAuthentication(isSuccessful: stat, inScreen: mScreen)
        }
    }
    
    func updateProfile() {
        if appDelegate.checkNetworkAvail() {
            let web  = WebApiClass()
            web.delegate = self
            let urlStr   = String.init(format: Url.updateProfile, preLoginModel.agentCode,uuid)
            let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
            let params = Dictionary<String,String>()
            web.genericClass(url: ur, param: params as AnyObject, httpMethod: "GET", mScreen: "mProfile")
        }
    }
    
    func getShopStatus() {
        if appDelegate.checkNetworkAvail() {
            let web  = WebApiClass()
            let urlStr   = String.init(format: Url.ShopStatusApi, UserModel.shared.mobileNo)
            let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
            let params = Dictionary<String,String>()
            web.genericClass(url: ur, param: params as AnyObject, httpMethod: "GET", mScreen: "ShopStatusApi")
        }
    }
    
    func navigateToDashboard() {
    
        if authenticateScreen {
            if let keyWindow = UIApplication.shared.keyWindow {
                for view in keyWindow.subviews {
                    if view.tag == 909786 {
                        view.removeFromSuperview()
                        OKSessionValidation.isMainAppSessionExpired = false
                    }
                }
            }
        } else {
            // /*
            // call shop status api
            self.getShopStatus()
            //
            // call Appserver notification api
            
            let web      = WebApiClass()
            web.delegate = self
            let urlStr   = Url.pushNotificationApi
            let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug(ur)
            let params   = getParamsForAPN()
            println_debug(params)
            PTLoader.shared.show()
            web.genericClass(url: ur, param: params as AnyObject, httpMethod: "POST", mScreen: "PUSHNOTIFICATION")
            
            // */
            // Direct Estel API
            //            showProgressView()
            //            let web = WebApiClass()
            //            web.delegate = self
            //            var urlS = String()
            //
            //            let deviceTok = UserDefaults.standard.value(forKey: "deviceToken") as? String
            //            let pas = ok_password ?? ""
            //
            //            urlS = String.init(format:Url.pushNotificationApi,UserModel.shared.mobileNo
            //                ,"",pas,deviceTok!,UserLogin.shared.token)
            //            let url =   getUrl(urlStr: urlS, serverType: .serverEstel)
            //            println_debug("notication final URL::: \(url)")
            //            let param = [String:String]() as AnyObject
            //            web.genericClass(url: url, param: param, httpMethod: "POST", mScreen: "PUSHNOTIFICATION")
            
            let rootNav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navigationMain")
            rootNav.modalPresentationStyle = .fullScreen
            self.present(rootNav, animated: true, completion: nil)
        }
        // from this we are setting the keyboard type - Avaneesh
        self.setKeyoboardType()
        userDef.set(true, forKey: keyLoginStatus)
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            ApnsTransaction[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    private func setKeyoboardType() {
        // check if the passwrod is alphanumeric or not
        if ReRegistrationModel.shared.Password.isNumeric {
                 // set the type here as numberpad
                 userDef.set(true, forKey: "passKeyBoardType")
             }else {
                // set the type here as aplphanuemeric
                 userDef.set(false, forKey: "passKeyBoardType")
             }
    }
    
    func navigateToReVerify() {
        userDef.set(true, forKey: "Reverify_SimChange")
        showProgressView()
        VerifyUPModel.share.MobileNumber = preLoginModel.agentCode
        if !(VillageManager.shared.data.registrationStatus == "") {
            DispatchQueue.main.async {
                let sb = UIStoryboard(name: "VeryfyUser" , bundle: nil)
                let veryfyUserVC  = sb.instantiateViewController(withIdentifier: "verifyUserNav")
                veryfyUserVC.modalPresentationStyle = .fullScreen
                if var topController = UIApplication.shared.keyWindow?.rootViewController {
                    while let presentedViewController = topController.presentedViewController {
                        topController = presentedViewController
                    }
                    topController.present(veryfyUserVC, animated: false, completion: nil)
                }
            }
        } else {
            DispatchQueue.main.async {
                self.checkOldNewRegistration()
            }
            DispatchQueue.main.async {
                self.self.showAlert(alertTitle: "", alertBody: "Please wait, Fetching some info", alertImage: #imageLiteral(resourceName: "alert-icon"))}
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody.localized, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ShowLoginBothViews"), object: nil)
    }
    
    //MARK: - Target Methods
    @objc func enableBothLoginOptions() {
       // authenticateScreen = true
        forgotPasswordTapped = true
//        appDel.floatingButtonControl?.window.isHidden = true
//        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        if forgotPasswordTapped {
            let backButton = UIButton()
            backButton.frame = CGRect(x: self.view.frame.size.width * 0.05, y: self.view.frame.size.height * 0.08, width: 30.0, height: 30.0)
            backButton.setImage(UIImage(named: "tabBarBack"), for: .normal)
            backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
            self.view.addSubview(backButton)
            authenticateScreen = true
            containerType = (ok_password_type) ?  patternContainer : passwordContainer
            if UserDefaults.standard.integer(forKey: "App_Password_Type") != 0 {
                leftButton.isHidden = false
                leftButton.setTitle("Cancel".localized, for: .normal)
            }
            if containerType == passwordContainer {
                bottomView.isHidden = false
                loginSubmit.isHidden = false
            } else {
                if UserDefaults.standard.integer(forKey: "App_Password_Type") != 0 {
                    bottomView.isHidden = false
                } else {
                    bottomView.isHidden = true
                }
                loginSubmit.isHidden = true
            }
            loginSubmit.setTitle(loginSubmit.titleLabel?.text?.localized, for: .normal)
            passwordPatternBtnHeight.constant = 35.0
            passwordPatternBtn.isHidden = false
        }
    }
    
    @objc func backButtonAction() {
        if let keyWindow = UIApplication.shared.keyWindow {
            for view in keyWindow.subviews {
                if view.tag == 909786 {
                    keyWindow.rootViewController?.navigationController?.navigationBar.isHidden = false
                    view.removeFromSuperview()
                }
            }
        }
    }
    
    @objc private func popTheViewControllerPT(_ sender:UIBarButtonItem!) {
        if let navigation = self.navigationController {
            navigation.popViewController(animated: true)
        }
    }
    
    //MARK: - Button action methods
    @IBAction func passwordPatternAction(_ sender: UIButton) {
        userDef.set(false, forKey: "passKeyBoardType")
        if !forgotPasswordTapped {            
            if containerType == patternContainer {
                containerType = passwordContainer
                self.passwordPatternBtn.setTitle("Click here to Login with Pattern".localized, for: .normal)
                loginSubmit.isHidden = false
                self.bottomView.isHidden = false
            } else {
                containerType = patternContainer
                self.passwordPatternBtn.setTitle("Click here to Login with Password".localized, for: .normal)
                loginSubmit.isHidden = true
                if !authenticateScreen {
                    self.bottomView.isHidden = true
                }
            }
        } else {
            if UserDefaults.standard.integer(forKey: "App_Password_Type") != 0 {
                leftButton.setTitle("Cancel".localized, for: .normal)
                leftButton.isHidden = false
                self.bottomView.isHidden = false
            } else {
                leftButton.isHidden = true
            }
            if containerType == patternContainer {
                containerType = passwordContainer
                self.passwordPatternBtn.setTitle("Click here to Login with Pattern".localized, for: .normal)
                self.bottomView.isHidden = false
                loginSubmit.isHidden = false
            } else {
                containerType = patternContainer
                self.passwordPatternBtn.setTitle("Click here to Login with Password".localized, for: .normal)
                if UserDefaults.standard.integer(forKey: "App_Password_Type") != 0 {
                    bottomView.isHidden = false
                } else {
                    bottomView.isHidden = true
                }
                loginSubmit.isHidden = true
            }
        }
    }
    
    @IBAction func changeMobileNumberAction(_ sender : UIButton) {
        alertViewObj.wrapAlert(title: nil, body: "If you want to change the mobile number, your data will be reset and again need to verify the number.".localized, img: UIImage(named : "Contact Mobile Number"))
        alertViewObj.addAction(title: "Cancel".localized, style: AlertStyle.cancel, action: {
            
        })
        alertViewObj.addAction(title: "OK".localized, style: .target, action: {
            userDef.removeObject(forKey: "PreloginSuccess")
            appDel.deleteAllObjects()
            appDel.loadSplashScreen()
            UserDefaults.standard.set(false, forKey: "is_QR_Sugesstion_Read")
        })
        alertViewObj.showAlert(controller: self)
    }
    
    @IBAction func ForgotPasswordAction(_ sender: UIButton) {
        //VillageManager.shared.data.registrationStatus == "0" || VillageManager.shared.data.registrationStatus == "-1"
        if VillageManager.shared.data.registrationStatus == "0" || VillageManager.shared.data.registrationStatus == "1" || VillageManager.shared.data.registrationStatus == "-1"{
            //self.navigateToOldFirstPassword()
             self.navigateToOldSecondPassword()
        } else if VillageManager.shared.data.registrationStatus == "1"{
            self.navigateToOldSecondPassword()
        } else if VillageManager.shared.data.registrationStatus == "" {
            DispatchQueue.main.async {
                self.showAlert(alertTitle: "", alertBody: "Please wait, Fetching some info", alertImage: #imageLiteral(resourceName: "alert-icon"))
            }
            self.checkOldNewRegistration()
        }
    }
    
    @IBAction func otherLoginIssueAction(_ sender: Any) {
        self.navigateToOldFirstPassword()
        
    }
    
    //MARK:---------------------------------------------------------------------------------------------------------------
    @IBAction func LoginTypeAction(_ sender: UIButton) {
        if authenticateScreen || forgotPasswordTapped {
            if let keyWindow = UIApplication.shared.keyWindow {
                for view in keyWindow.subviews {
                    if view.tag == 909786 {
                        view.removeFromSuperview()
                        //Request money issue oct23
                        NotificationCenter.default.post(name: Notification.Name("HideQRCodeNavigation"), object: nil, userInfo: nil)
                    }
                }
            }
        } else {
            containerType    = (containerType == passwordContainer) ? patternContainer : passwordContainer
            let senderString = (containerType == passwordContainer) ? "Pattern" : "Password"
            sender.setTitle(senderString.localized, for: .normal)
        }
    }
    
    //MARK: Login Api Calling and After Functions
    @IBAction func submit(_ sender: UIButton) {
        
        
        agentCode = userDef.bool(forKey: keyLoginStatus) ? UserModel.shared.mobileNo : preLoginModel.agentCode
        
        if !checkUUIDVerification() {
            
            if #available(iOS 14, *) {
                ATTrackingManager.requestTrackingAuthorization { status in
                    switch status {
                    case .authorized:
                        // Tracking authorization dialog was shown
                        // and we are authorized
                        print("Authorized")
                        
                        // Now that we are authorized we can get the IDFA
                        print(ASIdentifierManager.shared().advertisingIdentifier)
                    case .denied:
                        self.showToAlert("Please Allow Tracking to Register with OK$ \n Settings -> OK$ -> Allow Tracking -> Allow Apps to Request to Track", image: #imageLiteral(resourceName: "alert-icon"), simChanges: false)
                    case .notDetermined:
                        // Tracking authorization dialog has not been shown
                        print("Not Determined")
                    case .restricted:
                        print("Restricted")
                    @unknown default:
                        print("Unknown")
                    }
                }
            }else{
                showToAlert("Please enable Advertising to register with UUID in OK$ \n #iOS 13 \n Settings -> Privacy -> Advertising -> Limit Ad Tracking \n #iOS 14\n Settings -> Privacy -> Tracking -> Allow Apps to Request to Track", image: #imageLiteral(resourceName: "alert-icon"), simChanges: false)
            }
//            showToAlert("Please enable Advertising to register with UUID in OK$ \n #iOS 13 \n Settings -> Privacy -> Advertising -> Limit Ad Tracking \n #iOS 14\n Settings -> Privacy -> Tracking -> Allow Apps to Request to Track", image: #imageLiteral(resourceName: "alert-icon"), simChanges: false)
            return
        }
        
        let passDetails = (containerType == patternContainer) ? patternVC.capturePasswordWithDetails(agentCode) : passVC.capturePasswordWithDetails()
        println_debug(passDetails.password)
        
        _ =  LoginPasswordType.init(passDetails.password, withType: passDetails.type)
        
        if passDetails.password == "" {
            alertViewObj.wrapAlert(title: "", body: "Please enter the password".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                println_debug("Cancel")
            })
            alertViewObj.showAlert(controller: self)
            return
        }
        if appDelegate.checkNetworkAvail() {
            
            let web      = WebApiClass()
            web.delegate = self
            let urlStr   = Url.login
            let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug(ur)
            preLoginModel.agentCode = (userDef.bool(forKey: keyLoginStatus)) ? UserModel.shared.mobileNo : preLoginModel.agentCode
            
            let passwordTypeString = passDetails.type ? "1" : "0"
            
            let params   = LoginParams.getParamsForLogin(obj: UserModel.shared, pass: passDetails.password, preObj: preLoginModel, type: passwordTypeString)
            ok_password = passDetails.password
//            VerifyUPModel.share.LoginPassword = passDetails.password
//            VerifyUPModel.share.LoginPasswordType = passwordTypeString
            println_debug(params)
            paramsFailed = params
            PTLoader.shared.show()
            userDef.set(passDetails.type, forKey: "UpdatePassword_Type")
            web.genericClass(url: ur, param: params as AnyObject, httpMethod: "POST", mScreen: "mLogin")
        } else {
           // self.noInternetAlert()
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            alertViewObj.showAlert(controller:self)
        }
    }
}

//MARK: - UITextFieldDelegate
extension LoginViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
