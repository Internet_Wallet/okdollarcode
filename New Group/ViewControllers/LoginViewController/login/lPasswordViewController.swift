//
//  lPasswordViewController.swift
//  OK
//
//  Created by Ashish on 1/5/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

struct LoginPasswordType {
    
    init(_ password: String, withType type: Bool) {
       self.update(password, withType: type)
    }
    
    mutating func update(_ password: String, withType type: Bool) {
        self.password   = password
        self.isPassword = type
    }
    
    var password : String = "" {
        didSet {
            userDef.set(password, forKey: "passwordLogin_local")
            userDef.synchronize()
        }
    }
    var isPassword : Bool = true {
        didSet {
            userDef.set(isPassword, forKey: "passwordLoginType")
        }
    }
}

class lPasswordViewController: OKBaseController {
    
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var accountView: UIView!
    @IBOutlet weak var passwordField: FloatLabelTextField!{
        didSet{
            passwordField.font = UIFont(name: appFont, size: 17.0)
//            passwordField.titleFont = UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: 17.0)
//            passwordField.placeholderFont = UIFont(name: appFont, size: 17.0)
        }
    }
    @IBOutlet weak var accountField: FloatLabelTextField!{
        didSet{
            accountField.font = UIFont(name: appFont, size: 17.0)
        }
    }
    @IBOutlet weak var bacgroundLoginImage : UIImageView!
    
    @IBOutlet weak var headerLabel1 : UILabel!{
        didSet {
            headerLabel1.font = UIFont(name : appFont, size : appFontSize)
        }
    }
    @IBOutlet weak var headerLabel2 : UILabel!{
        didSet {
            headerLabel2.font = UIFont(name : appFont, size : appFontSize)
        }
    }

    
    fileprivate let hidenAccount = "XXXXXXXXXXXXXXX"
    
    var leftViewShowAccount = LeftViewTextField.updateView(fr: CGRect(x: 0, y: 0, width: 95, height: 50)) as! LeftViewTextField
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.passwordField.leftView = DefaultIconView.updateView(icon: "password_blue")
        self.passwordField.leftView?.tintColor = UIColor.init(hex: "0321AA")
        self.passwordField.leftViewMode  = .always

        self.updatePasswordView()
        leftViewShowAccount.lblCountryCode.textColor = UIColor.init(hex: "0321AA")
        self.accountField.leftView = leftViewShowAccount
        self.accountField.leftViewMode = .always
        
        passwordField.attributedPlaceholder = NSAttributedString(string: "Enter OK$ Password".localized,
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(hex: "0321AA")])
        
        headerLabel1.text = headerLabel1.text?.localized
        headerLabel2.text = headerLabel2.text?.localized
        self.accountField.text = "Show Account Number".localized
        loadAdvertisement()
    }
    
    func updatePasswordView() {
        var code = userDef.bool(forKey: keyLoginStatus) ? UserModel.shared.mobileNo : preLoginModel.agentCode
        if code.count > 1 {
            code.remove(at: String.Index.init(encodedOffset: 0))
            code.remove(at: String.Index.init(encodedOffset: 0))
            code.insert("+", at: String.Index.init(encodedOffset: 0))
        }
        let countryDetails =  identifyCountry(withPhoneNumber: code)
        leftViewShowAccount.updateViewItems(cImg: countryDetails.countryFlag, txt: "(\(countryDetails.countryCode))")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    @IBAction func touchDown(_ sender: UIButton) {
        if userDef.bool(forKey: keyLoginStatus) {
            self.accountField.text = UserModel.shared.formattedNumber
        } else {
            if preLoginModel.agentCode.hasPrefix("0095") {
                self.accountField.text = "0\(preLoginModel.agentCode.dropFirst(4))"
            } else {
                self.accountField.text = "\(preLoginModel.agentCode.dropFirst(4))"
            }
        }
    }
    
    @IBAction func releasingActionAccount(_ sender: UIButton) {
        self.accountField.text = "Show Account Number".localized
    }
    
    @IBOutlet var enableImageView: UIImageView! {
        didSet {
            enableImageView.tintColor = UIColor.init(hex: "0321AA")
        }
    }
    @IBAction func showPasswordAction(_ sender: UIButton) {
        self.passwordField.isSecureTextEntry =  !(self.passwordField.isSecureTextEntry)
        self.enableImageView.isHighlighted = self.passwordField.isSecureTextEntry
    }
    
    func capturePasswordWithDetails() -> (password: String,type: Bool) {
        
        if let password = self.passwordField.text {
            if password.count > 0 {
                return (password,false)
            }
        }
        return ("",true)
    }
    
    //MARK:- Advertisement Views
    func loadAdvertisement() {
        let randomIndex = Int(arc4random_uniform(UInt32(advImagesUrl.loginImageArray.count)))
        if let urlString = advImagesUrl.loginImageArray[safe: randomIndex] {
            DispatchQueue.main.async {
                self.bacgroundLoginImage.setImage(url: urlString, contentMode: UIView.ContentMode.scaleAspectFill, placeholderImage: nil)
            }
        }
    }

}

//MARK:- Delegate
extension lPasswordViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    // set the value for Keybord Type - Avaneesh
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == passwordField {
            textField.placeholder = "Password".localized
            if userDef.bool(forKey: "passKeyBoardType") {
                textField.keyboardType = .numberPad
            }else {
                 textField.keyboardType = .default
            }
        }
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}
