//
//  lPatternViewController.swift
//  OK
//
//  Created by Ashish on 1/5/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class lPatternViewController: UIViewController {

    @IBOutlet weak var lock: GesturePatternLock!
    
    var autoFireSubmit : SubmitButtonDelegate?
    
    @IBOutlet weak var headerLabel1 : UILabel!{
        didSet {
            headerLabel1.font = UIFont(name : appFont, size : appFontSize)
        }
    }
    @IBOutlet weak var headerLabel2 : UILabel!{
        didSet {
            headerLabel2.font = UIFont(name : appFont, size : appFontSize)
        }
    }
    @IBOutlet weak var backGroundLogin : UIImageView!
    
    private var passwordString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setuplock()
        headerLabel1.text = headerLabel1.text?.localized
        headerLabel2.text = headerLabel2.text?.localized
        loadAdvertisement()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.endEditing(true)
     }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    
    private func setuplock() {
        
        // Set number of sensors
        lock.lockSize = (3, 3)
        
        // Sensor grid customisations
        lock.edgeInsets = UIEdgeInsets.init(top: 50, left: 7, bottom: 0, right: 7)
        
        // Sensor point customisation (normal)
        lock.setSensorAppearance(
            type: .inner,
            radius: 4,
            width: 8,
            color: UIColor.init(hex: "0321AA"),
            forState: .normal
        )
        lock.setSensorAppearance(
            type: .outer,
            color: UIColor.init(hex: "0321AA"),
            forState: .normal
        )
        
        // Sensor point customisation (selected)
        lock.setSensorAppearance(
            type: .inner,
            radius: 4,
            width: 8,
            color: UIColor.init(hex: "0321AA"),
            forState: .selected
        )
        lock.setSensorAppearance(
            type: .outer,
            radius: 0,
            width: 1,
            color: .green,
            forState: .selected
        )
        
        // Sensor point customisation (wrong password)
        lock.setSensorAppearance(
            type: .inner,
            radius: 03,
            width: 15,
            color: .red,
            forState: .error
        )
        lock.setSensorAppearance(
            type: .outer,
            radius: 0,
            width: 1,
            color: .red,
            forState: .error
        )
        
        // Line connecting sensor points (normal/selected)
        [GesturePatternLock.GestureLockState.normal, GesturePatternLock.GestureLockState.selected].forEach { (state) in
            lock.setLineAppearance(
                width: 5.5,
                color: UIColor.init(hex: "FFFFFF"), //0321AA// UIColor.yellow.withAlphaComponent(0.5)
                forState: state
            )
        }
        
        // Line connection sensor points (wrong password)
        lock.setLineAppearance(
            width: 5.5,
            color: UIColor.red.withAlphaComponent(0.5),
            forState: .error
        )
        
        lock.addTarget(
            self,
            action: #selector(gestureComplete),
            for: .gestureComplete
        )
        
    }
    
    func capturePasswordWithDetails(_ code: String) -> (password: String,type: Bool) {
        return (retainingEncryptedPassword(code),true)
    }
    
    private func retainingEncryptedPassword(_ aCode: String) -> String {
        
        if passwordString == "" {
            alertViewObj.wrapAlert(title: "", body: "Draw the Pattern to Login", img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK", style: .cancel, action: {
                
            })
            
            alertViewObj.showAlert(controller: self)
        }
        
        println_debug("Password pattern string to check --------------> \(passwordString)")

        let object = PatternValidationClass()
        return object.patternEncryption(passwordString, withnumber: aCode)
    }

    
    @objc func gestureComplete(gestureLock: GesturePatternLock) {
        
        if gestureLock.lockSequence.count <= 3 {
            gestureLock.gestureLockState = .normal
            self.showErrorAlert(errMessage: "More Than 3 counts needed".localized)
            return
        }
        
        passwordString = ""
        
        for element in gestureLock.lockSequence {
            passwordString.append(element.stringValue)
        }
        
        println_debug(passwordString)
        
        if let delegate = self.autoFireSubmit {
            delegate.didTapSubmitButton()
        }
        
        gestureLock.gestureLockState = .normal
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  private  func patternEncryption(_ password: String, withnumber number: String) -> String {
        var resultCode: String
        resultCode = String()
        var multiplyValue: NSString = ""
    
        // step 1  multiply
    
    if password == "" {
        alertViewObj.wrapAlert(title: "", body: "Draw the Pattern to Login", img: #imageLiteral(resourceName: "alert-icon"))
        alertViewObj.addAction(title: "OK", style: .cancel, action: {
            
        })
        
        alertViewObj.showAlert(controller: self)
        return ""
    }
    
        let strNumber = (number as NSString).longLongValue
        let strPassword = (password as NSString).longLongValue
    
        multiplyValue = NSString.init(format: "%l", strNumber * strPassword)

    
        // step 2  trimming & normal encrypt & reverse encrypt
        let firstEncrypt: String = encryption(password)
        // Reverse Logic
        let count: Int = (password .count )
        let trimmedString: String = ((multiplyValue as NSString).substring(from: max(Int(multiplyValue.length) - count, 0)))
        let len: Int = (trimmedString .count )
        
        var  reverseName = ""
        
        var i = len - 1
        while i >= 0 {
            reverseName += "\(trimmedString[i])"
            i -= 1
        }
        
        let secondEncrypt: String = encryption(reverseName as String)
        // Strp 3  merging all the string
        var Output: String
        for i in 0..<(trimmedString .count) {
            Output = ""
            Output = "\(firstEncrypt[i] as Character)\(trimmedString[i] as Character)\(secondEncrypt[i] as Character)"
            if (resultCode .count ) == 0 {
                resultCode = "\("")\(Output)"
            }
            else {
                resultCode = "\(resultCode)\(Output)"
            }
        }
        return resultCode
    }
    
 private func encryption(_ str: String) -> String {
        var encrypted: String
        // let data = str as NSString
        encrypted = String()
        for i in 0..<(str .count) {
            let newChar: String = "\(str[i] as Character)"
            let passingValue: Int = Int(newChar)! + i
            var tempChar: String
            switch passingValue {
            case 0:
                tempChar = "A"
            case 1:
                tempChar = "B"
            case 2:
                tempChar = "C"
            case 3:
                tempChar = "D"
            case 4:
                tempChar = "E"
            case 5:
                tempChar = "F"
            case 6:
                tempChar = "G"
            case 7:
                tempChar = "H"
            case 8:
                tempChar = "I"
            case 9:
                tempChar = "J"
            case 10:
                tempChar = "a"
            case 11:
                tempChar = "b"
            case 12:
                tempChar = "c"
            case 13:
                tempChar = "d"
            case 14:
                tempChar = "e"
            case 15:
                tempChar = "f"
            case 16:
                tempChar = "g"
            case 17:
                tempChar = "h"
            case 18:
                tempChar = "i"
            case 19:
                tempChar = "j"
            case 20:
                tempChar = "k"
            default:
                tempChar = ""
            }
            if !(encrypted .count > 0) {
                encrypted = "\("")\(tempChar)"
            }
            else {
                encrypted = "\(encrypted)\(tempChar)"
            }
        }
        return encrypted
    }
    //MARK:- Advertisement Views
    func loadAdvertisement() {
        let randomIndex = Int(arc4random_uniform(UInt32(advImagesUrl.loginImageArray.count)))
        if let urlString = advImagesUrl.loginImageArray[safe: randomIndex] {
            DispatchQueue.main.async {
                self.backGroundLogin.setImage(url: urlString, contentMode: UIView.ContentMode.scaleAspectFill, placeholderImage: nil)
            }
        }
    }
    
}
