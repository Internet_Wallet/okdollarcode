//
//  UserLoginDataManager.swift
//  OK
//
//  Created by PC on 3/10/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreData

class UserLoginDataManager {
    
    static let sharedInstance = UserLoginDataManager()
    
    let managedContext  =  appDelegate.persistentContainer.viewContext
    
    func insertUserLoginDetailsIntoDB(dict: Dictionary<String,Any>) {

        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "UserLoginDetails")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchReq)
        
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
        }
        
        let transactionInfo = UserLoginDetails(context: managedContext)
        self.insertIntoUserLoginDB(dataToInsert: dict, insertDataTo: transactionInfo)
    }
    
    func insertUserProfileDetailsIntoDB(dict: Dictionary<String,Any>) {
        /*
         */
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "UserProfileDetails")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchReq)
        
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
        }
        
        let transactionInfo = UserProfileDetails(context: managedContext)
        self.insertIntoUserProfileDB(dataToInsert: dict, insertDataTo: transactionInfo)
    }
    
    func insertCloseDaysIntoDB(closeStr : String){
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "ProfileDay")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchReq)
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
        }
        let transactionInfo = ProfileDay(context: managedContext)
        self.insertIntoProfileDaysDB(dataToInsert: closeStr, insertDataTo: transactionInfo)
    }
    
    func insertIntoProfileDaysDB(dataToInsert: String, insertDataTo: ProfileDay) {
        insertDataTo.closedDays = dataToInsert
        self.saveContext()
        closeDayModel.wrapFromDB()
    }
    
    func fetchUProfileDaysRecord() -> ProfileDay? {
        let fetchReq = NSFetchRequest<NSManagedObject>(entityName: "ProfileDay")
        do {
            guard let fetchedObjects = try managedContext.fetch(fetchReq) as? [ProfileDay] else { return nil }
            if fetchedObjects.count > 0 {
                println_debug(fetchedObjects[0])
                return fetchedObjects[0]
            }
        }catch {
            //            println_debug("Fetching operation failed", error)
        }
        return nil
    }
    
    
    //MARK: Insert User Profile Data
    
    func insertIntoUserProfileDB(dataToInsert: Dictionary<String,Any>, insertDataTo: UserProfileDetails) {
        
        insertDataTo.accountID    = dataToInsert.safeValueForKey("AccountId").safelyWrappingString()
        insertDataTo.mobileNo     = dataToInsert.safeValueForKey("Mobilenumber").safelyWrappingString()
        insertDataTo.name         = dataToInsert.safeValueForKey("Name").safelyWrappingString()
        insertDataTo.simID        = dataToInsert.safeValueForKey("SimID").safelyWrappingString()
        insertDataTo.fatherName   = dataToInsert.safeValueForKey("FatherName").safelyWrappingString()
        insertDataTo.dob          = dataToInsert.safeValueForKey("DOB").safelyWrappingString()
        insertDataTo.nrc          = dataToInsert.safeValueForKey("NRC").safelyWrappingString()
        insertDataTo.idType       = dataToInsert.safeValueForKey("IDType").safelyWrappingString()
        insertDataTo.businessType = dataToInsert.safeValueForKey("BusinessType").safelyWrappingString()
        insertDataTo.businessCategory = dataToInsert.safeValueForKey("BusinessCategory").safelyWrappingString()
        insertDataTo.latitude         = dataToInsert.safeValueForKey("Latitude").safelyWrappingString()
        insertDataTo.longitude     = dataToInsert.safeValueForKey("Longitude").safelyWrappingString()
        insertDataTo.profilePic    = dataToInsert.safeValueForKey("ProfilePic").safelyWrappingString()
        insertDataTo.signaturePic   = dataToInsert.safeValueForKey("SignaturePic").safelyWrappingString()
        insertDataTo.emailId = dataToInsert.safeValueForKey("EmailId").safelyWrappingString()
        insertDataTo.addressType  = dataToInsert.safeValueForKey("AddressType").safelyWrappingString()
        insertDataTo.businessName = dataToInsert.safeValueForKey("BusinessName").safelyWrappingString()
        insertDataTo.businessOutlet = dataToInsert.safeValueForKey("BusinessOutlet").safelyWrappingString()
        insertDataTo.country      = dataToInsert.safeValueForKey("Country").safelyWrappingString()
        insertDataTo.countryCode  = dataToInsert.safeValueForKey("CountryCode").safelyWrappingString()
        
        insertDataTo.codeRecommended  = dataToInsert.safeValueForKey("CodeRecommended").safelyWrappingString()
        insertDataTo.deviceId     = dataToInsert.safeValueForKey("DeviceId").safelyWrappingString()
        insertDataTo.accountType      = dataToInsert.safeValueForKey("AccountType").safelyWrappingString()//(dataToInsert.safeValueForKey("AccountType") as! NSNumber).stringValue
        insertDataTo.address1         = dataToInsert.safeValueForKey("Address1").safelyWrappingString()
        insertDataTo.address2         = dataToInsert.safeValueForKey("Address2").safelyWrappingString()
        insertDataTo.appID            = dataToInsert.safeValueForKey("AppId").safelyWrappingString()
        insertDataTo.authCode         = dataToInsert.safeValueForKey("AuthCode").safelyWrappingString()
        insertDataTo.authCodeStatus   = dataToInsert.safeValueForKey("AuthCodeStatus").safelyWrappingString()//self.covertBoolToString(value: dataToInsert.safeValueForKey("AuthCodeStatus") as! Bool)
        insertDataTo.businessIdPhoto1 = dataToInsert.safeValueForKey("BusinessIdPhoto1").safelyWrappingString()
        insertDataTo.businessIdPhoto2 = dataToInsert.safeValueForKey("BusinessIdPhoto2").safelyWrappingString()
        insertDataTo.car              = dataToInsert.safeValueForKey("Car").safelyWrappingString()
        insertDataTo.carType          = dataToInsert.safeValueForKey("CarType").safelyWrappingString()//(dataToInsert.safeValueForKey("CarType") as! NSNumber).stringValue
        insertDataTo.cellTowerID      = dataToInsert.safeValueForKey("CellTowerID").safelyWrappingString()
        insertDataTo.closeTime        = dataToInsert.safeValueForKey("CloseTime").safelyWrappingString()
        insertDataTo.codeAlternate    = dataToInsert.safeValueForKey("CodeAlternate").safelyWrappingString()
        insertDataTo.codeRecommended  = dataToInsert.safeValueForKey("CodeRecommended").safelyWrappingString()
        insertDataTo.countryOfCitizen = dataToInsert.safeValueForKey("CountryOfCitizen").safelyWrappingString()
        insertDataTo.createdDate      = dataToInsert.safeValueForKey("CreatedDate").safelyWrappingString()
        insertDataTo.encryptedKey     = dataToInsert.safeValueForKey("EncryptedKey").safelyWrappingString()
        insertDataTo.fbEmailId        = dataToInsert.safeValueForKey("FBEmailId").safelyWrappingString()
        insertDataTo.floorNumber      = dataToInsert.safeValueForKey("FloorNumber").safelyWrappingString()
        insertDataTo.gcMID            = dataToInsert.safeValueForKey("GCMID").safelyWrappingString()
        
        insertDataTo.gender           = dataToInsert.safeValueForKey("Gender").safelyWrappingString()//self.covertBoolToString(value: dataToInsert.safeValueForKey("Gender") as! Bool)
        
        insertDataTo.houseBlockNo     = dataToInsert.safeValueForKey("HouseBlockNo").safelyWrappingString()
        insertDataTo.houseName        = dataToInsert.safeValueForKey("HouseName").safelyWrappingString()
        insertDataTo.imei             = dataToInsert.safeValueForKey("IMEI").safelyWrappingString()
        insertDataTo.idExpDate        = dataToInsert.safeValueForKey("IdExpDate").safelyWrappingString()
        
        insertDataTo.idPhoto          = dataToInsert.safeValueForKey("IdPhoto").safelyWrappingString()
        insertDataTo.idPhoto1         = dataToInsert.safeValueForKey("IdPhoto1").safelyWrappingString()
        
        insertDataTo.isNewStreet      = dataToInsert.safeValueForKey("IsNewStreet").safelyWrappingString()//self.covertBoolToString(value: dataToInsert.safeValueForKey("IsNewStreet") as! Bool)
        //Check Registered Language
        let fontType = dataToInsert.safeValueForKey("FontType").safelyWrappingString()
        if dataToInsert.safeValueForKey("Language").safelyWrappingString().lowercased() == "en" {
          insertDataTo.language         = dataToInsert.safeValueForKey("Language").safelyWrappingString()
        }else if fontType == "1" {
           insertDataTo.language         = "uni"
        }else {
           insertDataTo.language         = "my"
        }
       
        insertDataTo.msid             = dataToInsert.safeValueForKey("MSID").safelyWrappingString()
        
        insertDataTo.osType           = dataToInsert.safeValueForKey("OSType").safelyWrappingString()//(dataToInsert.safeValueForKey("OSType") as! NSNumber).stringValue
        
        insertDataTo.openTime         = dataToInsert.safeValueForKey("OpenTime").safelyWrappingString()
        insertDataTo.osVersion        = dataToInsert.safeValueForKey("OsVersion").safelyWrappingString()
        insertDataTo.parentAccount    = dataToInsert.safeValueForKey("ParentAccount").safelyWrappingString()
        
        insertDataTo.passwordType     = dataToInsert.safeValueForKey("PasswordType").safelyWrappingString()//(dataToInsert.safeValueForKey("PasswordType") as! NSNumber).stringValue
        
        insertDataTo.phoneModel       = dataToInsert.safeValueForKey("PhoneModel").safelyWrappingString()
        insertDataTo.phoneNumber      = dataToInsert.safeValueForKey("PhoneNumber").safelyWrappingString()
        insertDataTo.phoneBrand       = dataToInsert.safeValueForKey("Phonebrand").safelyWrappingString()
        if let addressDict = dataToInsert.safeValueForKey("BusinessAddressInfo") as? Dictionary<String, Any> {
            insertDataTo.b_Division      = addressDict.safeValueForKey("Division") as? String ?? ""
            insertDataTo.b_Township      = addressDict.safeValueForKey("Township") as? String ?? ""
            insertDataTo.b_City    = addressDict.safeValueForKey("City") as? String ?? ""
            insertDataTo.b_Village      = addressDict.safeValueForKey("Village") as? String ?? ""
            insertDataTo.b_Street      = addressDict.safeValueForKey("Street") as? String ?? ""
            insertDataTo.b_HouseNumber      = addressDict.safeValueForKey("HouseNumber ") as? String ?? ""
            insertDataTo.b_FloorNumber     = addressDict.safeValueForKey("FloorNumber") as? String ?? ""
            insertDataTo.b_RoomNumber      = addressDict.safeValueForKey("RoomNumber") as? String ?? ""
            insertDataTo.b_HouseBlockNumber      = addressDict.safeValueForKey("HouseBlockNumber") as? String ?? ""
        }
        insertDataTo.ownershipType = dataToInsert.safeValueForKey("OwnershipType").safelyWrappingString()
        insertDataTo.logoImages = dataToInsert.safeValueForKey("LogoImages").safelyWrappingString()
        
        if dataToInsert.safeValueForKey("Recommendedby")  != nil {
            insertDataTo.recommendedBy    = dataToInsert.safeValueForKey("Recommendedby").safelyWrappingString()
        } else {
            insertDataTo.recommendedBy    = ""
        }
        
        insertDataTo.registrationStatus = dataToInsert.safeValueForKey("RegistrationStatus").safelyWrappingString()//(dataToInsert.safeValueForKey("RegistrationStatus") as! NSNumber).stringValue
        insertDataTo.roomNumber       = dataToInsert.safeValueForKey("RoomNumber").safelyWrappingString()//dataToInsert.safeValueForKey("RoomNumber") as! String
        insertDataTo.state            = dataToInsert.safeValueForKey("State").safelyWrappingString()//dataToInsert.safeValueForKey("State") as! String
        insertDataTo.township         = dataToInsert.safeValueForKey("Township").safelyWrappingString()//dataToInsert.safeValueForKey("Township") as! String
        insertDataTo.villageName      = dataToInsert.safeValueForKey("VillageName").safelyWrappingString()//dataToInsert.safeValueForKey("VillageName") as! String
        
        insertDataTo.ageNow   = self.calculateCurrentAge(dataToInsert.safeValueForKey("DOB") as! String) ?? ""
        
        insertDataTo.agentType = dataToInsert.safeValueForKey("AgentType").safelyWrappingString()
        insertDataTo.kickBackNumber = dataToInsert.safeValueForKey("KickBackNumber").safelyWrappingString()
        self.saveContext()
        
        UserModel.wrapFromDatabase()
    }
    
    func covertBoolToString(value: Bool) -> String {
        return value ? "1" : "0"
    }
    
    func calculateCurrentAge(_ dob: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "dd-MMM-yyyy" //Your date format
        let date = dateFormatter.date(from: dob) //according to date format your date string
        let ageValue = date?.age
        let stringAgeValue : String? = "\(ageValue ?? 0)"
        return stringAgeValue
    }
    
    //MARK: Fetch User Profile Data
    
    func fetchUserProfileDetailsRecord() -> UserProfileDetails? {
        let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "UserProfileDetails")
        do {
            guard let fetchedObjects    = try managedContext.fetch(fetchReq) as? [UserProfileDetails] else { return nil }
            if fetchedObjects.count > 0 {
                println_debug(fetchedObjects[0])
                return fetchedObjects[0]
            }
        }catch {
//            println_debug("Fetching operation failed", error)
        }
        return nil
    }
    
    //MARK: Insert User Login Data

    func insertIntoUserLoginDB(dataToInsert: Dictionary<String,Any>, insertDataTo: UserLoginDetails) {
        
        insertDataTo.authCode = dataToInsert.safeValueForKey("AuthCode").safelyWrappingString()//(dataToInsert.safeValueForKey("AuthCode") as! String)
        insertDataTo.transactionTime = dataToInsert.safeValueForKey("TransactionTime").safelyWrappingString()
        insertDataTo.agentName = dataToInsert.safeValueForKey("agentname").safelyWrappingString()//(dataToInsert.safeValueForKey("agentname") as! String)
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss.S"
        let date = dateFormatterGet.string(from: Date())
        insertDataTo.lastLogin =  date
        
      //  insertDataTo.lastLogin = dataToInsert.safeValueForKey("LastLogin").safelyWrappingString()//(dataToInsert.safeValueForKey("LastLogin") as! String)
        insertDataTo.resultDescription = dataToInsert.safeValueForKey("ResultDescription").safelyWrappingString()//(dataToInsert.safeValueForKey("ResultDescription") as! String)
        insertDataTo.needToUpdateProfile = (dataToInsert.safeValueForKey("NeedToUpdateProfile") != nil)
        insertDataTo.resultCode = (dataToInsert.safeValueForKey("ResultCode") != nil)
        insertDataTo.telecoNumber = dataToInsert.safeValueForKey("TelecoNumber").safelyWrappingString()//(dataToInsert.safeValueForKey("TelecoNumber") as! String)
        insertDataTo.buinessCategory = dataToInsert.safeValueForKey("BuinessCategory").safelyWrappingString()//(dataToInsert.safeValueForKey("BuinessCategory") as! String)
        insertDataTo.bonusPoint = dataToInsert.safeValueForKey("BonusPoint").safelyWrappingString()//dataToInsert.safeValueForKey("BonusPoint").safelyWrappingString()
        insertDataTo.agentLevel = dataToInsert.safeValueForKey("agentLevel").safelyWrappingString()//(dataToInsert.safeValueForKey("agentLevel") as! String)
        insertDataTo.kickBack = (dataToInsert.safeValueForKey("KickBack") != nil)
        insertDataTo.authCodeStatus = (dataToInsert.safeValueForKey("AuthCodeStatus") != nil)
        insertDataTo.secureToken = dataToInsert.safeValueForKey("securetoken").safelyWrappingString()//(dataToInsert.safeValueForKey("securetoken") as! String)
        insertDataTo.transactionId = dataToInsert.safeValueForKey("TransactionId").safelyWrappingString()//(dataToInsert.safeValueForKey("TransactionId") as! String)
        insertDataTo.walletBalance = dataToInsert.safeValueForKey("WalletBalance").safelyWrappingString()//(dataToInsert.safeValueForKey("WalletBalance") as! String)
        insertDataTo.agentType = dataToInsert.safeValueForKey("agenttype").safelyWrappingString()//(dataToInsert.safeValueForKey("agenttype") as! String)
        insertDataTo.registrationStatus = (dataToInsert.safeValueForKey("RegistrationStatus") != nil)

        self.saveContext()
        
        UserLogin.wrapLoginFromDB()
    }
    
    //MARK: Fetch User Login Data

    func fetchUserLoginDetailsRecord() -> UserLoginDetails? {
        let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "UserLoginDetails")
        do {
            guard let fetchedObjects = try managedContext.fetch(fetchReq) as? [UserLoginDetails] else { return nil }
            if fetchedObjects.count > 0 {
                println_debug(fetchedObjects[0])
                return fetchedObjects.last
            }
        }catch {
//            println_debug("Fetching operation failed", error)
        }
        return nil
    }
    
    func deleteAllObjects()
    {
        userDef.removeObject(forKey: "calledInLastDay")
        let entitesByName = appDel.persistentContainer.managedObjectModel.entitiesByName
        for (name, _) in entitesByName {
            deleteAllObjectsForEntity(entity: name)
        }
    }
    
    func deleteAllObjectsForEntity(entity: String)
    {
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchReq)
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
        }
    }
    
    //MARK: Save In Core DataBase
    
    func saveContext() {
        let managedContext  =  appDelegate.persistentContainer.viewContext
        do {
            try managedContext.save()
        } catch let error as NSError {
            println_debug("could not save :::: \(error)  ,,, \(error.description) ,,, \(error.userInfo)")
        }
    }
}
