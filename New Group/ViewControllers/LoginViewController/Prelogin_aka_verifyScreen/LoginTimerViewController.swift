//
//  LoginTimerViewController.swift
//  OK
//
//  Created by Ashish on 1/8/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreGraphics

class LoginTimerViewController: UIViewController {

    var timer          = Timer()
    var seconds        = 20
    @IBOutlet var timerLabel: StrokedLabel!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.timerLabel.strockedText = ""
        timer.fire()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(LoginTimerViewController.updateTimer)), userInfo: nil, repeats: true)        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    @objc func updateTimer() {
        seconds -= 1
        timerLabel.strockedText = "\(seconds)"
        if seconds == 0 {
            if timer.isValid {
                self.view.removeFromSuperview()
                PTLoader.shared.hide()
                DispatchQueue.main.async {
                    if let keyWindow = UIApplication.shared.keyWindow {
                        for view in keyWindow.subviews {
                            if view.tag == 1473 {
                                view.removeFromSuperview()
                            }
                        }
                    }
                }
            }
        }
    }
}

//MARK:- Helper class to mark stroke in text
class StrokedLabel: UILabel {
    
    var strockedText: String = "" {
        willSet(newValue) {
            let strokeTextAttributes = [
                NSAttributedString.Key.strokeColor : UIColor.white,
                NSAttributedString.Key.foregroundColor : self.textColor,
                NSAttributedString.Key.strokeWidth : -1.0,
                NSAttributedString.Key.font : self.font
                ] as [NSAttributedString.Key : Any]
            
            let customizedText = NSMutableAttributedString(string: newValue,
                                                           attributes: strokeTextAttributes)
            attributedText = customizedText
        }
    }
}



