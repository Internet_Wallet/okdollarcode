//
//  PreLoginViewController.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/19/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreTelephony
import MessageUI
import IQKeyboardManagerSwift
import Rabbit_Swift
import ContactsUI
import CoreLocation
import AppTrackingTransparency
import AdSupport

struct CodePreLogin {
    var country = "Myanmar"
    var code    = "+95"
    var flag    = "myanmar"
    
    init(cName: String, codes: String, flag: String) {
        self.country = cName
        self.code = codes
        self.flag  = flag
    }
}

struct NetworkMCCMNC {
    var mnc = ""
    var mcc = ""
    
    init(_ mnc: String, _ mcc: String) {
        self.mnc = mnc
        self.mcc = mcc
    }
}

struct NetworkPreLogin {
    
    var status       = ""
    var networkName  = ""
    var operatorName = ""

    init(status: String, netName: String,opeName: String) {
        self.status       = status
        self.networkName  = netName
        self.operatorName = opeName
     }
}


class PreLoginViewController: OKBaseController, PhValidationProtocol, TapCountryBtnDelegate, CountryViewControllerDelegate,WebServiceResponseDelegate,MFMessageComposeViewControllerDelegate, SendSMSDelegate {
    
    
    enum countrySelected {
        case indiaCountry, thaiCountry, chinaCountry, myanmarCountry, other
    }
    
    let validObj = PayToValidations.init()
    
    private var availableSIM: Bool {
        let networkInfo = CTTelephonyNetworkInfo.init()
        if #available(iOS 12.0, *) {
            if let ct = networkInfo.serviceSubscriberCellularProviders {
                for carr in ct {
                    println_debug(carr.value.mobileNetworkCode)
                    println_debug(carr.value.mobileCountryCode)
                    println_debug(carr.value.carrierName)
                    if carr.value.mobileNetworkCode != nil {
                        return true
                    }
                }
            }
        }
        return (CTTelephonyNetworkInfo().subscriberCellularProvider?.mobileNetworkCode == nil ) ? false : true // sim detection
    }
    
    var preloginResponseModel : PreloginResponse?

    @IBOutlet weak var logoImageViewHeight: NSLayoutConstraint!
    
    @IBOutlet var preloginTextField: RestrictedCursorMovement! {
        didSet {
            self.preloginTextField.layer.cornerRadius = 12.0
            self.preloginTextField.layer.borderWidth = 1.0
            self.preloginTextField.layer.borderColor = UIColor.init(hex: "0321AA").cgColor
        }
    }
    @IBOutlet var submitBtn: UIButton! {
        didSet {
            self.submitBtn.layer.cornerRadius = 15.0
            self.submitBtn.backgroundColor = .lightGray
            self.submitBtn.isEnabled = false
        }
    }
    @IBOutlet weak var copyrightLabel: UILabel! {
        didSet {
            self.copyrightLabel.textColor = UIColor.init(hex: "0321AA")
        }
    }
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var preLoginContentLbl: UILabel!
    @IBOutlet weak var copyrightHeightConst: NSLayoutConstraint!
    @IBOutlet weak var viewHolderViewHeight: NSLayoutConstraint!
    @IBOutlet weak var clearButton: UIButton!
     {
        didSet {
            self.clearButton.isHidden = true
        }
    }
    
    @IBOutlet weak var englishView: UIView! {
        didSet {
           self.englishView.backgroundColor = UIColor.lightGray
            self.englishView.layer.cornerRadius = 15.0
        }
    }
    @IBOutlet weak var burmeseView: UIView! {
        didSet {
            self.burmeseView.backgroundColor = kYellowColor
            self.burmeseView.layer.cornerRadius = 15.0
        }
    }
    
    @IBOutlet weak var unicodeView: UIView! {
        didSet {
            self.unicodeView.backgroundColor = .lightGray
            self.unicodeView.layer.cornerRadius = 15.0
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    var preLoginDict = Dictionary<String, Any>()
    var preLoginAlertDict = Dictionary<String, Any>()
    
    @IBOutlet var btnLanguageFirst: UIButton!
    @IBOutlet var viewLanguage: UIView!
    @IBOutlet var btnLanguageSecond: UIButton!
    @IBOutlet weak var trainlingConstraints: NSLayoutConstraint!
    @IBOutlet weak var imgEngFlag: UIImageView!
    @IBOutlet weak var imgMyFlag: UIImageView!
    @IBOutlet var imguniFlag: UIImageView!
    
    var lang : laguageFlag?
    let tranView = UIView()
    let titleLabel = UILabel()
    var codeObj = CodePreLogin(cName: "Myanmar", codes: "+95", flag: "myanmar")
    
    let countryFlagView = LeftViewTextField.updateView(fr: CGRect(x: 0, y: 0, width: 95, height: 44)) as! LeftViewTextField
    var currentSelectedCountry : countrySelected = .myanmarCountry
    @IBOutlet weak var viewHolder: UIView!
    
    let notificationCenter = NotificationCenter.default
    
    var loader = false
    var failureCount = 0

    override func viewDidLoad() {
        super.viewDidLoad()
       // markLaunchCount()
        appFont = "Zawgyi-One"

        //this function will work to show advertise in dashboard
        UserDefaults.standard.set(0, forKey:"launchCount")
        manageApplicationCount()
        imgMyFlag.layer.borderWidth = 1.0
        imgMyFlag.layer.borderColor = UIColor.white.cgColor
        imguniFlag.layer.borderWidth = 1.0
        imguniFlag.layer.borderColor = UIColor.white.cgColor
        imgEngFlag.layer.borderWidth = 1.0
        imgEngFlag.layer.borderColor = UIColor.white.cgColor
        phNumValidationsFile = getDataFromJSONFile() ?? []
        submitBtn.layer.cornerRadius = 15.0
        submitBtn.isEnabled = false
        self.setCurrentLanguage(lang: "my")
        self.updateLocalizations()
        self.countryFlagView.delegate = self
        self.countryFlagView.lblCountryCode.textColor = UIColor.init(hex: "0321AA")
        self.updateTextfieldView(cImg: "myanmar", txt: "(+95)")
        titleLabel.text = "Verify Account".localized
        titleLabel.textColor = UIColor.init(hex: "162D9F")
        
        if let font = UIFont(name :appFont, size : 20.0) {
            titleLabel.font = font
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.init(hex: "0321AA") ,NSAttributedString.Key.font: font]
        } else {
            titleLabel.font = UIFont.systemFont(ofSize: 20.0)
        }

        self.title = titleLabel.text
        if device.type == .iPhone6plus || device.type == .iPhone6Splus || device.type == .iPhone7plus || device.type == .iPhone8plus {
            trainlingConstraints.constant = 20
        }
        
        if let lang = lang {
            btnLanguageFirst.stringTag = lang.flag1
            btnLanguageFirst.setImage(UIImage(named: lang.flag1), for: .normal)
            btnLanguageSecond.stringTag = lang.falg2
            btnLanguageSecond.setImage(UIImage(named: lang.falg2), for: .normal)
        }
        
        viewLanguage.isHidden = true
        self.preloginTextField.text = "09"
        
        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyBoard), name: UIResponder.keyboardDidHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyBoard), name: UIResponder.keyboardDidShowNotification, object: nil)
        
        decrytingUDIDForServer()

        if device.type == .iPhone4 || device.type == .iPhone4S || device.type == .iPhone5 || device.type == .iPhone5S || device.type == .iPhoneSE {
            logoImageViewHeight.constant = 80.0
            //viewHolderViewHeight.constant = self.view.frame.height
        }
        
        self.versionLabel.text = "Version \(buildVersion), Build (\(buildNumber)) \( UserDefaults.standard.string(forKey: "BuildInstalledDate") ?? "")"
        submitBtn.isEnabled = false
        submitBtn.backgroundColor = .lightGray
        self.clearButton.isHidden = true
        
        if !CLLocationManager.locationServicesEnabled() {
            self.view.endEditing(true)
            alertViewObj.wrapAlert(title: "", body: "Please allow OK$ to use your location to offer location specific deals. It's completely private and secure.".localized, img: nil)
            alertViewObj.addAction(title: "Setting".localized, style: .target , action: {
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        println_debug("Settings opened: \(success)")
                    })
                }
            })
            alertViewObj.addAction(title: "Cancel".localized, style: .cancel , action: {
            })
            alertViewObj.showAlert(controller: self)
        } else {
            self.preloginTextField.becomeFirstResponder()
        }
        
        notificationCenter.addObserver(self, selector:#selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc func appMovedToForeground() {
      print("App moved to foreground!")
        if loader == true{
            PTLoader.shared.show()
           
        }

      }
    
    func manageApplicationCount(){
        let currentCount = UserDefaults.standard.integer(forKey: "launchCount")
        UserDefaults.standard.set(currentCount+1, forKey:"launchCount")
        UserDefaults.standard.set(true, forKey: "isComingFromAppdelegate")
    }
    
    
    @objc func showKeyBoard() {
        //self.scrollView.isScrollEnabled = false
    }
    
    @objc func hideKeyBoard() {
       //self.scrollView.isScrollEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        failureCount = 0
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    fileprivate func showAlert(_ str: String, image: UIImage, simChanges: Bool)
    {
        // Please enter your valid mobile number
        alertViewObj.wrapAlert(title: "", body: str.localized, img: image)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            if !simChanges {
                self.preloginTextField.text = (self.codeObj.code == "+95") ? "09" : ""
                self.preloginTextField.becomeFirstResponder()
            }
        })
        
        alertViewObj.showAlert(controller: self)
    }
    
    @IBAction func englishBtnAction(_ sender: UIButton) {
        englishView.backgroundColor = kYellowColor
        burmeseView.backgroundColor = .lightGray
        unicodeView.backgroundColor = .lightGray
        self.updateLanguage(languageF: "ukF")
        
        self.changeLaguageOfApplication()
    }
    
    @IBAction func burmeseBtnAction(_ sender: UIButton) {
        englishView.backgroundColor = .lightGray
        burmeseView.backgroundColor = kYellowColor
        unicodeView.backgroundColor = .lightGray
        self.updateLanguage(languageF: "myanmarF")
        
        self.changeLaguageOfApplication()
    }
    
    @IBAction func unicodeBtnAction(_ sender: UIButton) {
        englishView.backgroundColor = .lightGray
        burmeseView.backgroundColor = .lightGray
        unicodeView.backgroundColor = kYellowColor
        self.updateLanguage(languageF: "unicodeF")
        
        self.changeLaguageOfApplication()
    }
    
    func decrytingUDIDForServer(){
        
        guard let manager = EncryptionUUID.encryptionmanager() as? EncryptionUUID else { return }
        // for bug fixing server side
        // formate :::: "0C569471-378D-0DAD-A31D-F5786AC8B343"
        let userID = "07596809-0951-8402-6264-0724-07075968"
        let decryptMsg = manager.decryptNumber(userID)
        println_debug("In Prelogin for server decrypted UDID::: \(String(describing: decryptMsg))")
        //println_debug(String(describing: decryptMsg))
    }
 
    @IBAction func preLoginAction(_ sender: UIButton) {
        
        //For AppStore Testing
       number = self.preloginTextField.text ?? ""
        
        if !checkUUIDVerification() {
            
            if #available(iOS 14, *) {
                ATTrackingManager.requestTrackingAuthorization { status in
                    switch status {
                    case .authorized:
                        
                        print(ASIdentifierManager.shared().advertisingIdentifier.uuidString)
                        
                    case .denied:
                        // Tracking authorization dialog was
                        // shown and permission is denied
                        print("Denied")
                        
                        //self.showAlert("Please Allow Tracking to Register with OK$ \n Settings -> OK$ -> Allow Tracking -> Allow Apps to Request to Track", image: #imageLiteral(resourceName: "alert-icon"), simChanges: false)
                        self.showAlert("Please enable Advertising to register with UUID in OK$ \n #iOS 13 \n Settings -> Privacy -> Advertising -> Limit Ad Tracking \n #iOS 14\n Settings -> Privacy -> Tracking -> Allow Apps to Request to Track", image: #imageLiteral(resourceName: "alert-icon"), simChanges: false)

                    case .notDetermined:
                        // Tracking authorization dialog has not been shown
                        print("Not Determined")
                    case .restricted:
                        print("Restricted")
                        self.showAlert("Please enable Advertising to register with UUID in OK$ \n #iOS 13 \n Settings -> Privacy -> Advertising -> Limit Ad Tracking \n #iOS 14\n Settings -> Privacy -> Tracking -> Allow Apps to Request to Track", image: #imageLiteral(resourceName: "alert-icon"), simChanges: false)

                    @unknown default:
                        print("Unknown")
                    }
                }
            }else{
                self.showAlert("Please enable Advertising to register with UUID in OK$ \n #iOS 13 \n Settings -> Privacy -> Advertising -> Limit Ad Tracking \n #iOS 14\n Settings -> Privacy -> Tracking -> Allow Apps to Request to Track", image: #imageLiteral(resourceName: "alert-icon"), simChanges: false)
            }
            
            
            
            return
        }
        
        if number == "09782763424" {
            
            let count = self.preloginTextField.text?.count
            switch currentSelectedCountry {
                
            case .indiaCountry:
                if count! < 10 {
                    showAlert("Please enter your valid mobile number", image: #imageLiteral(resourceName: "mobile"), simChanges: false)
                    submitBtn.isEnabled = false
                    return
                }
                //            if (count! < 4) || (count! > 13) {
                //                showAlert("Please enter your valid mobile number", image: #imageLiteral(resourceName: "mobile"), simChanges: false)
                //                submitBtn.isEnabled = false
                //                return
            //            }
            case .myanmarCountry:
                if count! < 9 {
                    showAlert("Please enter your valid mobile number", image: #imageLiteral(resourceName: "mobile"), simChanges: false)
                    submitBtn.isEnabled = false
                    return
                }
            case .chinaCountry:
                if count! < 11 {
                    showAlert("Please enter your valid mobile number", image: #imageLiteral(resourceName: "mobile"), simChanges: false)
                    submitBtn.isEnabled = false
                    return
                }
                //            if (count! < 4) || (count! > 13) {
                //                showAlert("Please enter your valid mobile number", image: #imageLiteral(resourceName: "mobile"), simChanges: false)
                //                submitBtn.isEnabled = false
                //                return
            //            }
            case .thaiCountry:
                if count! < 9 {
                    showAlert("Please enter your valid mobile number", image: #imageLiteral(resourceName: "mobile"), simChanges: false)
                    submitBtn.isEnabled = false
                    return
                }
                //            if (count! < 4) || (count! > 13) {
                //                showAlert("Please enter your valid mobile number", image: #imageLiteral(resourceName: "mobile"), simChanges: false)
                //                submitBtn.isEnabled = false
                //                return
            //            }
            case .other:
                if (count! < 4) || (count! > 13) {
                    showAlert("Please enter your valid mobile number", image: #imageLiteral(resourceName: "mobile"), simChanges: false)
                    submitBtn.isEnabled = false
                    return
                }
            }
            
            if appDelegate.checkNetworkAvail() {
                
                if let agentString = self.preloginTextField.text {
                    let agentCode  = getAgentCode(numberWithPrefix: agentString, havingCountryPrefix: codeObj.code)
                    RegistrationModel.shared.MobileNumber = agentCode
                    ReRegistrationModel.shared.MobileNumber = agentCode
                    
                    if appDelegate.checkNetworkAvail() {
                        PTLoader.shared.show()
                        let web = WebApiClass()
                        web.delegate = self
                        
                        let url = getUrl(urlStr: Url.preLogin, serverType: .serverApp)
                        
                        let params = self.JSONStringFromAnyObject(value: self.agentOtpParameterBinding(agentCode) as AnyObject)
                        println_debug(params)
                        TopupWeb.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", handle: { [weak self](resp, success) in
                            DispatchQueue.main.async {
                                guard let weakSelf = self else { return }
                                guard let responseDic = resp as? Dictionary<String,Any> else { return }
                                let stringJson = weakSelf.JSONStringFromAnyObject(value: responseDic as AnyObject)
                                if let modelData = stringJson.data(using: .utf8) {
                                    if let preloginResponseModel = try? JSONDecoder().decode(PreloginResponse.self, from: modelData) {
                                        if preloginResponseModel.code == 200 {
                                            weakSelf.preloginResponseModel = preloginResponseModel
                                            weakSelf.locationAddress()
                                            weakSelf.preLoginParsing()
                                        } else {
                                            if let jsonString = preloginResponseModel.data {
                                                self?.xmlParesingAlertFunction(jsonString)
                                                //self?.showErrorAlert(errMessage: errorMessage)
                                                
                                                self?.failureCount = self?.failureCount ?? 0 + 1
                                                if self?.failureCount == 3 {
                                                    let failureViewModel = FailureViewModel()
                                                    failureViewModel.sendDataToFailureApi(request: params as Any, response: responseDic as Any, type: FailureHelper.FailureType.Verification.rawValue, finished: {
                                                    })
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        })
                    }
                } else {
                    println_debug("Alert to show")
                }
            } else {
                alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK", style: .cancel) {
                }
                alertViewObj.showAlert(controller: UIViewController.init())
            }
        

        }
        
        else {
        
        if !availableSIM {
            self.showAlert("Sim is not available", image: #imageLiteral(resourceName: "alert-icon"), simChanges: true)
            return
        } 
        
        let count = self.preloginTextField.text?.count
        switch currentSelectedCountry {
            
        case .indiaCountry:
            if count! < 10 {
                showAlert("Please enter your valid mobile number", image: #imageLiteral(resourceName: "mobile"), simChanges: false)
                submitBtn.isEnabled = false
                return
            }
//            if (count! < 4) || (count! > 13) {
//                showAlert("Please enter your valid mobile number", image: #imageLiteral(resourceName: "mobile"), simChanges: false)
//                submitBtn.isEnabled = false
//                return
//            }
        case .myanmarCountry:
            if count! < 9 {
               showAlert("Please enter your valid mobile number", image: #imageLiteral(resourceName: "mobile"), simChanges: false)
                submitBtn.isEnabled = false
                return
            }
        case .chinaCountry:
            if count! < 11 {
                showAlert("Please enter your valid mobile number", image: #imageLiteral(resourceName: "mobile"), simChanges: false)
                submitBtn.isEnabled = false
                return
            }
//            if (count! < 4) || (count! > 13) {
//                showAlert("Please enter your valid mobile number", image: #imageLiteral(resourceName: "mobile"), simChanges: false)
//                submitBtn.isEnabled = false
//                return
//            }
        case .thaiCountry:
            if count! < 9 {
                showAlert("Please enter your valid mobile number", image: #imageLiteral(resourceName: "mobile"), simChanges: false)
                submitBtn.isEnabled = false
                return
            }
//            if (count! < 4) || (count! > 13) {
//                showAlert("Please enter your valid mobile number", image: #imageLiteral(resourceName: "mobile"), simChanges: false)
//                submitBtn.isEnabled = false
//                return
//            }
        case .other:
            if (count! < 4) || (count! > 13) {
                showAlert("Please enter your valid mobile number", image: #imageLiteral(resourceName: "mobile"), simChanges: false)
                submitBtn.isEnabled = false
                return
            }
        }
         
        if appDelegate.checkNetworkAvail() {
            
            if let agentString = self.preloginTextField.text {
                let agentCode  = getAgentCode(numberWithPrefix: agentString, havingCountryPrefix: codeObj.code)
                RegistrationModel.shared.MobileNumber = agentCode
                ReRegistrationModel.shared.MobileNumber = agentCode
                UserModel.shared.mobileNo = agentCode
                
                if appDelegate.checkNetworkAvail() {
                    PTLoader.shared.show()
                    let web = WebApiClass()
                    web.delegate = self
                    
                    let url = getUrl(urlStr: Url.preLogin, serverType: .serverApp)
                    println_debug(url)
                    let params = self.JSONStringFromAnyObject(value: self.agentOtpParameterBinding(agentCode) as AnyObject)
                    println_debug(params)
                    TopupWeb.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", handle: { [weak self](resp, success) in
                        DispatchQueue.main.async {
                            guard let weakSelf = self else { return }
                            guard let responseDic = resp as? Dictionary<String,Any> else { return }
                            let stringJson = weakSelf.JSONStringFromAnyObject(value: responseDic as AnyObject)
                            if let modelData = stringJson.data(using: .utf8) {
                                if let preloginResponseModel = try? JSONDecoder().decode(PreloginResponse.self, from: modelData) {
                                    if preloginResponseModel.code == 200 {
                                        weakSelf.preloginResponseModel = preloginResponseModel
                                        weakSelf.locationAddress()
                                        DispatchQueue.main.async {
                                            alertViewObj.wrapAlert(title: "", body:"Please make sure you have balance to send sms and send sms from primary sim that you have entered as in OK$ Number".localized, img:#imageLiteral(resourceName: "mobile_bcd"))
                                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                            weakSelf.openSMS()
                                        })
                                         alertViewObj.showAlert(controller: self)
                                        }
                                     // weakSelf.preLoginParsing()
                                    } else {
                                        if let jsonString = preloginResponseModel.data {
                                            self?.xmlParesingAlertFunction(jsonString)
                                            //self?.showErrorAlert(errMessage: errorMessage)
                                        }
                                    }
                                }
                            }
                        }
                    })
                }
            } else {
                println_debug("Alert to show")
            }
        } else {
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK", style: .cancel) {
            }
            alertViewObj.showAlert(controller: UIViewController.init())
        }
        }
    }
 
    
    fileprivate func agentOtpParameterBinding(_ agentCode: String) -> Dictionary<String,Any> {
        var parameters = Dictionary<String,Any>()
        
        var agentOtpRequest = Dictionary<String,Any>()
        agentOtpRequest["AgentCode"]   = agentCode
        agentOtpRequest["ClientIp"]    = OKBaseController.getIPAddress() ?? ""
        agentOtpRequest["ClientOs"]    = "0"
        agentOtpRequest["RequestType"] = "AUTH"
        
        var loginRequest = Dictionary<String,Any>()
        loginRequest["MobileNumber"] = agentCode
        loginRequest["Msid"]         = getMsid()
        loginRequest["Ostype"]       = "1"
        loginRequest["Otp"]          = ""
        loginRequest["Simid"]        = simid
        
        parameters["AgentOtpRequest"] = agentOtpRequest
        parameters["AppId"]           = "1029" // default set for android but need to check again with server they need to update with iOS build version
        parameters["IsSendOtp"]       = false
        parameters["Login"]           = loginRequest
        parameters["OsVersion"]       = UIDevice.current.systemVersion

        return parameters
    }
    
    @IBAction func clearButtonAction(_ sender : UIButton)
    {
        self.preloginTextField.text = (codeObj.code == "+95") ? "09" : ""
        self.clearButton.isHidden = true
        submitBtn.isEnabled = false
        submitBtn.backgroundColor = .lightGray
    }
    
    func btnCountryTapped() {
        self.navigationController?.present(countryViewController(delegate: self), animated: true, completion: nil)
    }

    func countryViewController(_ list: CountryViewController, country: Country) {
        list.dismiss(animated: true, completion: nil)
        codeObj.code    = country.dialCode
        codeObj.country = country.name
        codeObj.flag    = country.dialCode
        
        switch codeObj.code {
        case "+91":
            currentSelectedCountry = .indiaCountry
        case "+95":
            currentSelectedCountry = .myanmarCountry
        case "+66":
            currentSelectedCountry = .thaiCountry
        case "+86":
            currentSelectedCountry = .chinaCountry
        default:
            currentSelectedCountry  = .other
        }
        
        let stringCode = String.init(format: "(%@)", country.dialCode)
        self.updateTextfieldView(cImg: country.code, txt: stringCode)
        
        self.preloginTextField.text = (country.dialCode == "+95") ? "09" : ""
        self.preloginTextField.becomeFirstResponder()
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
         self.navigationController?.dismiss(animated: true, completion: nil)
    }

    func updateTextfieldView(cImg: String, txt: String) {
        self.countryFlagView.updateViewItems(cImg: cImg, txt: txt)
        self.preloginTextField.leftView = nil
        countryFlagView.updateViewItems(cImg: cImg, txt: txt)
        self.preloginTextField.leftView = countryFlagView
        self.preloginTextField.leftViewMode  = UITextField.ViewMode.always
        submitBtn.isEnabled = false
        submitBtn.backgroundColor = .lightGray
        self.clearButton.isHidden = true
    }
    
    func updateLocalizations() {
        self.title = "Verify Account".localized
        
        if UserDefaults.standard.bool(forKey: "DUAL_SIM") {
            preLoginContentLbl.text = "Mobile have dual Sim Slot please insert Active Sim Card in Sim Slot-1 for OK $ Account Registration. If you have OK $ Account please enter mobile number for Verify Account.".localized
        }else {
            preLoginContentLbl.text = "Please Insert Active Sim card for OK$ Account Registration. If you have OK$ Account please enter mobile number for Verify Account".localized
        }
        
        submitBtn.setTitle("Submit".localized, for: .normal)
        self.copyrightLabel.text = "Copyright @ 2021 Internet Wallet Myanmar Ltd. All rights reserved".localized
        if device.type == .iPhoneSE || device.type == .iPhone5S || device.type == .iPhone5 {
            self.copyrightLabel.font = UIFont(name: appFont, size: 13.0)
        }
        self.view.layoutIfNeeded()
    }
    
    
    func addview() {
        // to Hide Language view on touch on screen
        tranView.frame = CGRect(x: 0, y: 64, width: self.view.frame.width, height: self.view.frame.height - 64)
        tranView.backgroundColor = UIColor.clear
        self.view.addSubview(tranView)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(nextStepTap))
        tapGestureRecognizer.numberOfTapsRequired = 1
        tranView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc public func nextStepTap(sender: UITapGestureRecognizer) {
        viewLanguage.isHidden = true
        tranView.removeFromSuperview()
    }
    
    // Language Changes
    @IBAction func btnLanguageFirstAction(_ sender: Any) {
        viewLanguage.isHidden = !viewLanguage.isHidden
        if !(viewLanguage.isHidden) {
            addview()
        }
    }
    
    @IBAction func btnLanguageSecondAction(_ sender: Any) {
        
        self.viewLanguage.isHidden = true
        if (self.btnLanguageSecond.currentImage as UIImage?) != nil{
            let image = self.btnLanguageFirst.currentImage
            let strTag = self.btnLanguageFirst.stringTag
            self.btnLanguageFirst.setImage(self.btnLanguageSecond.currentImage, for: UIControl.State.normal)
            self.btnLanguageSecond.setImage(image, for: UIControl.State.normal)
            self.btnLanguageFirst.stringTag = self.btnLanguageSecond.stringTag
            self.btnLanguageSecond.stringTag = strTag
            
            self.updateLanguage(languageF: self.btnLanguageFirst.stringTag!)
            
            self.changeLaguageOfApplication()
        }
        
    }
    
    
    func updateLanguage(languageF : String) {
        
        var strLanguage = ""
        if languageF == "myanmarF"{
            strLanguage = "my"
        }else if languageF == "ukF"{
            strLanguage = "en"
        } else if languageF == "chinaF" {
            strLanguage = "zh-Hans"
        } else if languageF == "unicodeF" {
            strLanguage = "uni"
        }
        UserDefaults.standard.set(strLanguage, forKey:"currentLanguage")
        appDel.setSeletedlocaLizationLanguage(language: strLanguage)
        UserDefaults.standard.synchronize()
        viewLanguage.isHidden = true
        tranView.removeFromSuperview()
    }
    
    func changeLaguageOfApplication() {
          if appDel.currentLanguage == "uni" {
            preLoginContentLbl.font = UIFont(name : "Myanmar3", size : 12.0)
        } else {
            preLoginContentLbl.font = UIFont(name : "Zawgyi-One", size :15.0)
        }
        
        titleLabel.font = UIFont(name : appFont, size :appFontSize)
        //preLoginContentLbl.font = UIFont(name : appFont, size :appFontSizeReport)
        submitBtn.titleLabel?.font = UIFont(name : appFont, size : appButtonSize)
        updateLocalizations()
        //self.title = titleLabel.text
        if UserDefaults.standard.bool(forKey: "DUAL_SIM") {
            preLoginContentLbl.text = "Mobile have dual Sim Slot please insert Active Sim Card in Sim Slot-1 for OK $ Account Registration. If you have OK $ Account please enter mobile number for Verify Account.".localized
        } else {
            preLoginContentLbl.text = "Please Insert Active Sim card for OK$ Account Registration. If you have OK$ Account please enter mobile number for Verify Account".localized
        }
        submitBtn.setTitle("Submit".localized, for: UIControl.State.normal)
        self.copyrightLabel.text = "Copyright @ 2019 Internet Wallet Myanmar Ltd. All rights reserved".localized
        self.view.layoutIfNeeded()

    }
    
    fileprivate func setCurrentLanguage(lang : String) {
        UserDefaults.standard.set(lang, forKey: "currentLanguage")
        appDel.setSeletedlocaLizationLanguage(language: "my")
        UserDefaults.standard.synchronize()
    }
    
    fileprivate func clearButtonShowHide(count : Int)
    {
        if count > 0 {
            self.clearButton.isHidden = false
        } else {
            self.submitBtn.backgroundColor = .lightGray
            self.submitBtn.isEnabled = false
            self.clearButton.isHidden = true
        }
    }
    
    fileprivate func xmlParesingAlertFunction(_ xmlString: String) {
        var string = xmlString.replacingOccurrences(of: "\\", with: "")
        string = string.replacingOccurrences(of: "\"", with: "")
        let xml = SWXMLHash.parse(string)
        self.enumerateAlert(indexer: xml)
        if let rawData = self.preLoginAlertDict.jsonString().data(using: .utf8) {
            if let estelModel = try? JSONDecoder().decode(PreloginEstelResponse.self, from: rawData) {
                let errMessage = estelModel.resultdescription.safelyWrappingString()
                if errMessage != "" {
                    self.showErrorAlert(errMessage: errMessage.localized)
                } else {
                    self.showErrorAlert(errMessage: "Please Try Again!".localized)
                }
            }
        }
        
    }
    
    func enumerateAlert(indexer: XMLIndexer) {
        for child in indexer.children {
            for child1 in child.children {
                if child1.element?.name == "response" {
                    for child2 in child1.children {
                        if child2.element?.name == "resultdescription" {
                            self.preLoginAlertDict[child2.element!.name] = child2.element!.text
                        }
                    }
                }
            }
        }
    }

}

extension PreLoginViewController{
    
    func webResponse(withJson json: AnyObject, screen: String) {
        
        DispatchQueue.main.async {
            
            PTLoader.shared.hide()
            
            if screen == "verifyOtp" {
                guard let jsonData = json as? Data else { return }
                if let otpModel = try? JSONDecoder().decode(OTPVerificationResponse.self, from: jsonData) {
                    if otpModel.code == 200 {
                        self.preLoginParsing()
                    } else if otpModel.code == 300 {
                        self.showErrorAlert(errMessage: otpModel.msg.safelyWrappingString())
                    }
                }
            }
        }
    }
    //MARK: Fetch location Address
    func locationAddress() {
        let lat  = GeoLocationManager.shared.currentLatitude
        let long = GeoLocationManager.shared.currentLongitude
        
        if let lattitude = lat, let longitude = long {
            
            geoLocManager.getAddressFrom(lattitude: lattitude, longitude: longitude, language: appDel.currentLanguage, completionHandler: { (success, address) in
                DispatchQueue.main.async {
                    if success {
                        if let adrs = address {
                            if let dictionary = adrs as? Dictionary<String,Any> {
                                let street = dictionary.safeValueForKey("street") as? String ?? ""
                                let township = dictionary.safeValueForKey("township") as? String ?? ""
                                let city = dictionary.safeValueForKey("city") as? String ?? ""
                                if appDel.currentLanguage == "uni" {
                                    let currentAdd = String.init(format: "%@ ,%@ ,%@", street , township, city)
                                    userDef.set(currentAdd, forKey: "lastLoginKey")
                                } else {
                                    let currentAdd = Rabbit.uni2zg(String.init(format: "%@ ,%@ ,%@", street , township, city))
                                    userDef.set(currentAdd, forKey: "lastLoginKey")
                                }
                                
                            }
                        }
                    } else {
                        //self.location.text = self.lastLoginAddress
                    }
                }
            })
        } else {
            //self.location.text = lastLoginAddress
        }
    }
    
    func preLoginParsing() {
        
        guard let model = preloginResponseModel else { return  }
        
        guard let jsonString = model.data else { return }
        
        if let jsonData = jsonString.data(using: .utf8) {
            if let otpModel = try? JSONDecoder().decode(PreloginFinalResponse.self, from: jsonData) {
                self.xmlParesingFunction(otpModel)
            }
        }
    }
    
    fileprivate func xmlParesingFunction(_ xmlModel: PreloginFinalResponse) {
        let tempLanguage = ok_default_language
        if xmlModel.language.safelyWrappingString() == "MY" || xmlModel.language.safelyWrappingString() == "my" || xmlModel.language.safelyWrappingString() == "MYA" || xmlModel.language.safelyWrappingString() == "mya" {
            if let type = xmlModel.fontType, type == "1"  {
                ok_default_language = "uni"
                registered_Language = "uni"
            } else {
                ok_default_language = "my"
                registered_Language = "my"
            }
        } else {
            ok_default_language = (xmlModel.language.safelyWrappingString().lowercased() == "") ? "en" : xmlModel.language.safelyWrappingString().lowercased()
            registered_Language = "en"
        }
        
        userDef.set(registered_Language, forKey: "AppRegisteredLanguage")
        userDef.synchronize()
        println_debug("RegisteredLanguage : \(ok_default_language)")
        
//        UserDefaults.standard.set(ok_default_language, forKey: "currentLanguage")
//        appDel.setSeletedlocaLizationLanguage(language: ok_default_language)
//        UserDefaults.standard.synchronize()
        
        UserDefaults.standard.set(0, forKey: "App_Password_Type")
        
        if xmlModel.passwordType.safelyWrappingString() == "0" {
            userDef.set(false, forKey: "passwordLoginType")
            userDef.synchronize()
            ok_password         = userDef.string(forKey: "passwordLogin_local")
            ok_password_type    = userDef.bool(forKey: "passwordLoginType")
        } else if xmlModel.passwordType.safelyWrappingString() == "1" {
            userDef.set(true, forKey: "passwordLoginType")
            userDef.synchronize()
            ok_password         = userDef.string(forKey: "passwordLogin_local")
            ok_password_type    = userDef.bool(forKey: "passwordLoginType")
        } else {
            UserDefaults.standard.set(-1, forKey: "App_Password_Type")
            println_debug("enable both password and pattern")
        }
        
        if ok_default_language == "na" || ok_default_language == "NA" {
            ok_default_language = tempLanguage
            registered_Language = ok_default_language
        }
        
        guard let xmlString = xmlModel.estelResponse else { return }
        
        guard let _ = xmlModel.passwordType else { return }
        
        let xml = SWXMLHash.parse(xmlString)
        
        self.enumerate(indexer: xml)
        
        if let rawData = self.preLoginDict.jsonString().data(using: .utf8) {
            
            if let estelModel = try? JSONDecoder().decode(PreloginEstelResponse.self, from: rawData) {
                if estelModel.resultcode.safelyWrappingString() == "0" {
                    let number = "\(estelModel.agentcode.safelyWrappingString()),\(self.codeObj.code),\(self.codeObj.country)"
                    let value: [Any] = [number, ok_default_language, true]
                    userDef.set(value, forKey: "PreloginSuccess")
                    
                    preLoginModel.wrapPreLoginData(agent: estelModel.agentcode.safelyWrappingString(), cCode: self.codeObj.code, fCode: self.codeObj.country)
                    self.performSegue(withIdentifier: "login", sender: self)
                } else if estelModel.resultcode.safelyWrappingString() == "14" {
                    self.showErrorAlert(errMessage: estelModel.resultdescription.safelyWrappingString())
                } else if estelModel.resultcode.safelyWrappingString() == "28" {
                    self.showErrorAlert(errMessage: estelModel.resultdescription.safelyWrappingString())
                } else if estelModel.resultcode.safelyWrappingString() == "9" {
                    let number = "\(self.preloginTextField.text ?? ""),\(self.codeObj.code),\(self.codeObj.country)"
                    let value: [Any] = [number, ok_default_language, false]
                    userDef.set(value, forKey: "PreloginSuccess")
                    self.navigateToRegistration()
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "login" {
            guard  let vc = segue.destination as? LoginViewController else { return }
            DispatchQueue.main.async {
                if let text = self.preloginTextField.text {
                    vc.agentCode = self.getAgentCode(numberWithPrefix: text, havingCountryPrefix: self.codeObj.code)
                }
            }
        }
    }

    private func navigateToRegistration() -> Void {
        DispatchQueue.main.async(execute: {
            let sb = UIStoryboard(name: "Registration" , bundle: nil)
            let VC  = sb.instantiateViewController(withIdentifier: "Regi_HelpVC")
            self.navigationController?.pushViewController(VC, animated: true)
        })
    }
    
    @IBAction func sendSms(_ sender: UIButton) {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "", body:"Please make sure you have balance to send sms and send sms from primary sim that you have entered as in OK$ Number".localized, img:#imageLiteral(resourceName: "mobile_bcd"))
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            self.openSMS()
        })
         alertViewObj.showAlert(controller: self)
        }
        
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            self.preLoginDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func markLaunchCount(){
        UserDefaults.standard.set(0, forKey: "launchCount")
        let currentCount = UserDefaults.standard.integer(forKey: "launchCount")
        UserDefaults.standard.set(currentCount+1, forKey:"launchCount")
        UserDefaults.standard.set(true, forKey: "isComingFromDashboard")
        UserDefaults.standard.set(true, forKey: "isComingFromReVerify")
    }
}

// MARK: - Open SMS
extension PreLoginViewController: UITextFieldDelegate {
    
    func openSmsPreview(_ control: SendSmsViewController) {
        control.dismiss(animated: true, completion: nil)
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "", body:"Please make sure you have balance to send sms and send sms from primary sim that you have entered as in OK$ Number".localized, img:#imageLiteral(resourceName: "mobile_bcd"))
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            self.openSMS()
        })
         alertViewObj.showAlert(controller: self)
        }
       
    }
    
    func openSMS() {
        var defaultNumber = [String]()
        if codeObj.code == "+95" {
            defaultNumber.append("+95930000044")
        } else {
            defaultNumber.append("+14809009123")
        }
        guard let manager = EncryptionUUID.encryptionmanager() as? EncryptionUUID else { return }
        let encryptMsg = manager.entryptNumber(uuid)
        
        // for bug fixing server side
        // formate :::: "0C569471-378D-0DAD-A31D-F5786AC8B343"
        //        let userID = "FBB66A57-D079-03AB-A3CF-B5C621EBBDD6"
        //        let decryptMsg = manager.decryptNumber(userID)
        //        println_debug("for server decrypted UDID::: \(String(describing: decryptMsg))")
        
        #if DEBUG
        let msg = serverUrl == .productionUrl ? String.init(format: "Registration Security #%@", encryptMsg ?? "") : String.init(format: "Registration Security #%@#!TEST!#", encryptMsg ?? "")
        #else
        let msg = String.init(format: "Registration Security #%@", encryptMsg ?? "")
        #endif
        
        if (MFMessageComposeViewController.canSendText()) {
            let controller        = MFMessageComposeViewController()
            controller.body       = msg
            controller.recipients = defaultNumber
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult){
        
        switch result {
            
        case .sent:
            controller.dismiss(animated: true, completion: nil)
            
            let time : DispatchTime = DispatchTime.now() + 19.0
            
            PTLoader.shared.show()
            loader = true
            DispatchQueue.main.async {
                guard let loginController = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: LoginTimerViewController.self)) as? LoginTimerViewController else { return }
                if let keyWindow = UIApplication.shared.keyWindow {
                    loginController.view.frame = .init(x: screenWidth/2 - 64, y: screenHeight - 148, width: 128.00, height: 128.00)
                    loginController.view.tag = 1473
                    keyWindow.addSubview(loginController.view)
                    keyWindow.makeKeyAndVisible()
                    keyWindow.bringSubviewToFront(loginController.view)
                }
            }
            DispatchQueue.main.asyncAfter(deadline: time, execute: {
                self.loader = false
                self.callingApiTocheckOTP()
            })
            break
        case .cancelled:
            controller.dismiss(animated: true, completion: nil)
            println_debug("remove window")
            self.submitBtn.isEnabled = true
            self.submitBtn.backgroundColor = UIColor.init(hex: "E9C551")
            
            break
        case .failed:
            controller.dismiss(animated: true, completion: nil)
            println_debug("popup remove, dismiss")
            break
        }
    }
    
    func callingApiTocheckOTP(){
        let web = WebApiClass()
        web.delegate = self
        
        DispatchQueue.main.async {
            if let text = self.preloginTextField.text {
                PTLoader.shared.show()
                let agentCode  = self.getAgentCode(numberWithPrefix: text, havingCountryPrefix: self.codeObj.code)
                let urlstring = String.init(format: Url.verifyOtp,agentCode,uuid)
                let url =   getUrl(urlStr: urlstring, serverType: .serverApp)
                println_debug(url)
                let param = [String:String]() as AnyObject
                web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "verifyOtp")
            }
        }
    }
    
    override var textInputMode: UITextInputMode? {
        let language = "en-US" //getKeyboardLanguage()
        if language.isEmpty {
            return super.textInputMode
            
        } else {
            for tim in UITextInputMode.activeInputModes {
                if tim.primaryLanguage!.contains(language) {
                    return tim
                }
            }
            return super.textInputMode
        }
    }
    
    func getKeyboardLanguage() -> String {
        return "en"
        //        switch self {
        //        case .one:
        //            return "en"
        //        case .two:
        //            return "my"
    }
    
    //MARK: Textfield Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //textField.text = (codeObj.code == "+95") ? "09" : ""
        //scrollView.isScrollEnabled = false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //scrollView.isScrollEnabled = true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        if codeObj.code == "+95" {
            textField.text = "09"
        } else {
            textField.text = ""
        }
        
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
            let endOfDocument = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
            return false
        }
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "0123456789").inverted).joined(separator: "")) { return false }
        switch currentSelectedCountry {
        case .indiaCountry:
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
            clearButtonShowHide(count: textCount)
            if textCount == 10 {
                self.preloginTextField.text = text
                self.submitBtn.isEnabled = true
                self.submitBtn.backgroundColor = UIColor.init(hex: "E9C551")
                self.submitBtn.sendActions(for: .touchUpInside)
                return false
            }
            else {
                return true
            }
            
            //            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            //            let textCount  = text.count
            //            clearButtonShowHide(count: textCount)
            //
            //            if textCount > 13 {
            //                return false
            //            }
            //            if textCount > 3  {
            //                self.submitBtn.isEnabled = true
            //                self.submitBtn.backgroundColor = UIColor.init(hex: "E9C551")
            //                //self.submitBtn.sendActions(for: .touchUpInside)
            //                return true
            //            }
            //            else {
            //                self.submitBtn.isEnabled = false
            //                self.submitBtn.backgroundColor = .lightGray
            //
            //                return true
        //            }
        case .chinaCountry:
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
            clearButtonShowHide(count: textCount)
            if textCount == 11 {
                self.preloginTextField.text = text
                self.submitBtn.isEnabled = true
                self.submitBtn.backgroundColor = UIColor.init(hex: "E9C551")
                self.submitBtn.sendActions(for: .touchUpInside)
                return false
            }
            else {
                return true
            }
            
            //            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            //            let textCount  = text.count
            //            clearButtonShowHide(count: textCount)
            //
            //            if textCount > 13 {
            //                return false
            //            }
            //            if textCount > 3  {
            //                self.submitBtn.isEnabled = true
            //                self.submitBtn.backgroundColor = UIColor.init(hex: "E9C551")
            //                //self.submitBtn.sendActions(for: .touchUpInside)
            //                return true
            //            }
            //            else {
            //                self.submitBtn.isEnabled = false
            //                self.submitBtn.backgroundColor = .lightGray
            //
            //                return true
        //            }
        case .myanmarCountry:
            if (range.location == 0 && range.length == 0) || (range.location == 1 && range.length == 0)
            {
                return false
            }
            
            if range.location == 0 && range.length > 1 {
                textField.text = "09"
                self.clearButton.isHidden = true
                return false
            }
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
            if textCount > 2
            {
                self.clearButton.isHidden = false
            } else {
                self.clearButton.isHidden = true
            }
            
            let object = validObj.getNumberRangeValidation(text)
            
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            
            if (isBackSpace == -92) {
                if textField.text == "09" {
                    textField.text = "09"
                    self.clearButton.isHidden = true
                    return false
                }
            }
            
            if object.isRejected == true {
                textField.text = "09"
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                self.clearButton.isHidden = true
                return false
            }
            
            if textCount < object.min {
                self.submitBtn.isEnabled = false
                self.submitBtn.backgroundColor = .lightGray
            } else if (textCount >= object.min && textCount < object.max) {
                self.submitBtn.isEnabled = true
                self.submitBtn.backgroundColor = UIColor.init(hex: "E9C551")
            } else if (textCount == object.max) {
                textField.text = text
                self.submitBtn.backgroundColor = UIColor.init(hex: "E9C551")
                self.submitBtn.sendActions(for: .touchUpInside)
                self.submitBtn.isEnabled = true
                return false
            } else if textCount > object.max {
                return false
            }
            
        case .thaiCountry:
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
            clearButtonShowHide(count: textCount)
            if textCount == 9 {
                self.preloginTextField.text = text
                self.submitBtn.isEnabled = true
                self.submitBtn.backgroundColor = UIColor.init(hex: "E9C551")
                self.submitBtn.sendActions(for: .touchUpInside)
                return false
            }
            else {
                return true
            }
            
            
            //            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            //            let textCount  = text.count
            //            clearButtonShowHide(count: textCount)
            //
            //            if textCount > 13 {
            //                return false
            //            }
            //            if textCount > 3  {
            //                self.submitBtn.isEnabled = true
            //                self.submitBtn.backgroundColor = UIColor.init(hex: "E9C551")
            //                //self.submitBtn.sendActions(for: .touchUpInside)
            //                return true
            //            }
            //            else {
            //                self.submitBtn.isEnabled = false
            //                self.submitBtn.backgroundColor = .lightGray
            //
            //                return true
        //            }
        case .other:
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount  = text.count
            clearButtonShowHide(count: textCount)
            
            if textCount == 13 {
                self.preloginTextField.text = text
                self.submitBtn.sendActions(for: .touchUpInside)
                //self.submitBtn.sendActions(for: .touchUpInside)
                return false
            }
            
            if textCount > 13 {
                self.submitBtn.sendActions(for: .touchUpInside)
                //self.submitBtn.sendActions(for: .touchUpInside)
                return false
            }
            
            if textCount > 3  {
                self.preloginTextField.text = text
                self.submitBtn.isEnabled = true
                self.submitBtn.backgroundColor = UIColor.init(hex: "E9C551")
                //self.submitBtn.sendActions(for: .touchUpInside)
                return false
            }
            else {
                self.submitBtn.isEnabled = false
                self.submitBtn.backgroundColor = .lightGray
                return true
            }
        }
        
        return true
    }
}

