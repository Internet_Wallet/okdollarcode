//
//  SendSmsViewController.swift
//  OK
//
//  Created by Ashish on 1/8/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol SendSMSDelegate {
    func openSmsPreview(_ control: SendSmsViewController)
}

class SendSmsViewController: OKBaseController {

    @IBOutlet var circleLogo: UIView!
    
    var delegate : SendSMSDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewLayouting(onView: self.circleLogo)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //super.viewWillAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   private  func viewLayouting(onView v : UIView) {
        v.layer.cornerRadius = v.frame.width / 2
        // border
        v.layer.borderColor = UIColor.white.cgColor
        v.layer.borderWidth = 1.5
        // drop shadow
        v.layer.shadowColor = UIColor.lightGray.cgColor
        v.layer.shadowOpacity = 0.8
        v.layer.shadowRadius = 2.0
        v.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
    }
    
    // MARK: - IBAction
    @IBAction func closeAction(_ sender: UIButton) {
        println_debug("closeAction")
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func sendSmsAction(_ sender: UIButton) {
        if let delegate = delegate {
            println_debug("openSmsPreview")
            delegate.openSmsPreview(self)
        }
    }
}
