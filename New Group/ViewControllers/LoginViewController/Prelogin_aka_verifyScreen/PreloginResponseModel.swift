
//
//  PreloginResponseModel.swift
//  OK
//
//  Created by Ashish on 4/21/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

// Prelogin initial response model
struct PreloginResponse : Codable {
    
    let code : Int?
    let data : String?
    let msg : String?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case msg = "Msg"
    }
}

// OTP verification response message
struct OTPVerificationResponse : Codable {
    let code : Int?
    let data : String?
    let msg : String?

    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case msg = "Msg"
    }
}

// prelogin response model for language and password type
struct PreloginFinalResponse : Codable {
    
    let estelResponse : String?
    let language : String?
    let passwordType : String?
    let fontType: String?
    
    enum CodingKeys: String, CodingKey {
        case estelResponse = "EstelResponse"
        case language = "Language"
        case passwordType = "PasswordType"
        case fontType = "FontType"
    }
}

// estel prelogin response model
struct PreloginEstelResponse : Codable {
    
    let agentcode : String?
    let clienttype : String?
    let comments : String?
    let estel : String?
    let header : String?
    let otp : String?
    let requestcts : String?
    let response : String?
    let responsects : String?
    let responsetype : String?
    let resultcode : String?
    let resultdescription : String?
    let source : String?
    let transid : String?
    let udv1 : String?
    let vendorcode : String?
    
    enum CodingKeys: String, CodingKey {
        case agentcode = "agentcode"
        case clienttype = "clienttype"
        case comments = "comments"
        case estel = "estel"
        case header = "header"
        case otp = "otp"
        case requestcts = "requestcts"
        case response = "response"
        case responsects = "responsects"
        case responsetype = "responsetype"
        case resultcode = "resultcode"
        case resultdescription = "resultdescription"
        case source = "source"
        case transid = "transid"
        case udv1 = "udv1"
        case vendorcode = "vendorcode"
    }
}
