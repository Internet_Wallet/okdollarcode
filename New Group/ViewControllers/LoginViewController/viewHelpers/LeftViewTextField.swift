//
//  LeftViewTextField.swift
//  OK
//
//  Created by Ashish Kumar Singh on 7/17/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit


protocol TapCountryBtnDelegate {
    func btnCountryTapped()
}



public class LeftViewTextField: UIView {

    @IBOutlet weak var imgCountry: UIImageView!
    @IBOutlet weak var lblCountryCode: UILabel!
    
    var delegate: TapCountryBtnDelegate?
    
    public class func updateView(fr: CGRect) -> UIView {
      let nib =  UINib(nibName: "View", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as! LeftViewTextField
        nib.frame = fr
        nib.layoutUpdates()
        return nib
    }
    
     func layoutUpdates() {
        self.layoutIfNeeded()
    }
    
    func updateViewItems(cImg: String, txt: String) {
        self.imgCountry.image    = UIImage(named: cImg)
        self.lblCountryCode.text = txt
        
        let size   = self.lblCountryCode.text?.size(withAttributes: [.font: self.lblCountryCode.font]) ?? .zero
        let vWidth = 50 + size.width
        self.frame = .init(x: 0, y: 0, width: vWidth, height: 50.00)
        self.layoutIfNeeded()
    }

    @IBAction func buttonAction(_ sender: UIButton) {
        guard ((self.delegate?.btnCountryTapped()) != nil) else { return }
    }
    
}
