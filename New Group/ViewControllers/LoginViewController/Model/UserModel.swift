//
//  UserModel.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/20/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit


class PreLoginModel: NSObject {
    
    var agentCode       = ""
    var countryCode     = ""
    var flagCode        = ""
    var patternPassword = ""
    var pCount          = 0
    var networkName     = Array<Any>()
    var operatorName    = Array<Any>()
    
    func wrapPreLoginData(agent: String, cCode: String, fCode: String) {
        
        agentCode   = agent
        countryCode = cCode
        flagCode    = fCode
        
    }
    
    func wrapPatternPass(passKey: String, count: Int) {
        self.patternPassword = passKey
        self.pCount = count
    }
    
}

class LoginParams {
    
    class func getParamsForLogin(obj: UserModel, pass: String, preObj: PreLoginModel, type: String) -> Dictionary<String, Any> {
        
        var dict = Dictionary<String, Any>()
        
        dict["MobileNumber"]          = preObj.agentCode
        dict["Password"]              = pass
        dict["SimId"]                 = uuid
        dict["Msid"]                  = ""
        dict["OsType"]                = Int(1)
        dict["IosOtp"]                = uuid
        dict["DateOfBirth"]           = ""
        dict["FatherName"]            = ""
        dict["Name"]                  = obj.name
        dict["ReRegisterOtp"]         = uuid
        dict["AuthCode"]              = ""
        dict["AuthCodeStatus"]        = Int(truncating: false)
        dict["ProfilePic"]            = ""
        dict["AppId"]                 = LoginParams.setUniqueIDLogin()
        dict["AppVersion"]            = buildVersion
        dict["PasswordType"]          = type
        dict["Latitude"]              = "21.9162" //obj.lat
        dict["Longitude"]             = "95.9560" //obj.long
        dict["CellId"]                = ""
        dict["AndroidVersion"]        = ""
        dict["NetworkOperator"]       = ""
        dict["NetworkOperatorName"]   = ""
        dict["DeviceSoftwareVersion"] = ""
        dict["NetworkCountryIso"]     = ""
        dict["NetworkType"]           = ""
        dict["SIMCountryIso"]         = ""
        dict["SIMOperator"]           = ""
        dict["SIMOperatorName"]       = ""
        dict["VoiceMailNo"]           = ""
        dict["BSSID"]                 = ""
        dict["MACAddress"]            = ""
        dict["IPAddress"]             = ""
        dict["SSID"]                  = ""
        dict["BluetoothAddress"]      = ""
        dict["BluetoothName"]         = ""
        dict["ConnectedNetworkType"]  = ""
        dict["LinkSpeed"]             = Int(1)
        dict["NetworkID"]             = Int(1)
        dict["NetworkSignal"]         = Int(1)
        dict["HiddenSSID"]            = Int(truncating: true)
        dict["isNetworkRoaming"]      = Int(truncating: true)
        
        var cellTowerDict = Dictionary<String, Any>()
        cellTowerDict["Mcc"] = mccStatus.mcc
        cellTowerDict["Mnc"] = mccStatus.mnc
        cellTowerDict["Lac"] = ""
        cellTowerDict["SignalStrength"] = ""
        cellTowerDict["NetworkType"] = UitilityClass.getNetworkType()
        cellTowerDict["NetworkSpeed"] = UitilityClass.getSignalStrength()
        
        if UitilityClass.isConnectedToWifi(){
            let wifiInfo = UitilityClass.getNetworkInfos()[0].components(separatedBy: ",")
            cellTowerDict["ConnectedwifiName"] = wifiInfo[0]
            cellTowerDict["Ssid"] = wifiInfo[0]
            cellTowerDict["Mac"] = wifiInfo[1]
        }else{
            cellTowerDict["ConnectedwifiName"] = ""
            cellTowerDict["Ssid"] = ""
            cellTowerDict["Mac"] = ""
        }
        
        dict["transactionsCellTower"] = cellTowerDict
        return dict
    }
    
    class func setUniqueIDLogin() -> String {
        
//        var appId: String = "1,"
//        appId = appId + buildNumber
//        appId = appId + (UIDevice.current.identifierForVendor?.uuidString)!
//        appId = appId + (",")
//        appId = appId + (UIDevice.current.model)
//        appId = appId + (",")
//        appId = appId + (UIDevice.current.model)
//        appId = appId + (",")
//        appId = appId + (UIDevice.current.systemVersion)
//        appId = appId + (",")
//        appId = appId + ("0")
//        appId = appId + (",")
//        appId = appId + ("0")
//        println_debug("full \(appId)")
        
//        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String //"1.20"
//        let deviceUDID = UIDevice.current.identifierForVendor!.uuidString as String
        _ = UIDevice.current.name as String
        let deviceModel = UIDevice.current.model
        let systemVersion = UIDevice.current.systemVersion
        //let appIDstr = "OK_ID," + appVersion + ","  + deviceUDID + "," + deviceName + "," + deviceModel + "," + systemVersion + "," + "0,0"
        
        let appID = "0," + buildNumber + "," + uuid + "," + deviceModel + "," + deviceModel + "," + systemVersion + "," + "0" + "," + "0" + "," + uuid
        
        return appID
        
//        var appID = "1," + buildNumber + "," + uuid + "," + deviceModel + "," + deviceName + "," + systemVersion + "0" + "," + "0" + "," + uuid
//        return appId
        
        //let appID = "1," + buildNumber + "," + uuid + "," + deviceModel + "," + deviceName + "," + systemVersion + "0" + "," + "0" + "," + uuid

    }
    
    
    class func getParamsCheckInOut(inamount: String,outamount: String,inrateamount: String,outrateamount: String,  preObj: NSString) -> Dictionary<String, Any> {
        
        var dict = Dictionary<String, Any>()
        
        dict["MobileNumber"]      = preObj
        dict["MaxCashInLimit"]     = inamount
        dict["MaxCashOutLimit"]    = outamount
        dict["CashInRate"]        = inrateamount
        dict["CashOutRate"]       = outrateamount
        
        return dict
    }
}

class RequestMoneyParams{
    class func getParamsRequestForRequestMoney(obj: UserModel,source : String?,destination : String ,amount :String,remarks : String ,scheduledType : String,isMyNumber : Bool, scheduleDate : String?,base64EncodingImageString : String?)
        ->Dictionary<String,Any> {
            
            
            let countryCodeWithZero =  obj.countryCode.replacingOccurrences(of: "+", with: "00")
            var dict = Dictionary<String, Any>()
            dict["MobileNumber"]           =   obj.mobileNo
            dict["SimId"]                  =   obj.simID
            dict["Msid"]                   =   obj.msid
            dict["Ostype"]                 =   obj.osType
            dict["Otp"]                    =   obj.osType
            
            println_debug(dict["Source"])
            /*
            if source != nil {
                var startIndex = 0
                if(source!.starts(with: "0")) {
                    startIndex = 1
                }
                dict["Source"]               =   countryCodeWithZero + source!.substring(from: startIndex)
                
            } else {
                dict["Source"]            =       obj.mobileNo
            }
            */
            
            var startIndex1 = 0
            if(source!.starts(with: "0")) {
                startIndex1 = 1
            }
            dict["Source"]               =   countryCodeWithZero + source!.substring(from: startIndex1)
            
            var startIndex = 0
            if(destination.starts(with: "0")) {
                startIndex = 1
            }
            dict["Destination"]           = countryCodeWithZero + destination.substring(from: startIndex)
            
            
            dict["Amount"]                 =   amount
            let currentDate = Date()
            dict["Date"]                   =   stringFromDate(date: currentDate as NSDate,format: GlobalConstants.Strings.defaultDateFormat)
            dict["Remarks"]                =   remarks
            dict["LocalTransactionType"]   =   "RequestMoney"
            dict["TransactionType"]        =   "PayTo"
            if scheduledType == "-1" {
                dict["IsSchduled"]             =    false
                dict["SchduledType"]           =    scheduledType
                dict["SchduledTime"]           =     dict["Date"]
            }else {
                dict["IsSchduled"]             =    true
                if scheduleDate != nil {
                    dict["SchduledTime"]           =    scheduleDate
                    dict["SchduledType"]           =    0
                }else {
                    dict["SchduledType"]        = scheduledType
                }
                
                
            }
            dict["RequestModuleType"]       =     0
            dict["IsMynumber"]              =     isMyNumber
            if base64EncodingImageString != nil{
                dict["AttachedImages"]          =      [base64EncodingImageString]
            }else{
                dict["AttachedImages"]                      =   []
            }
            return dict
    }
    
    //MARK: Params for get all send requests
    class func getAPIForAllSendRequest(requestMoneyStatus : String , requestType : String,limit : Int?,offset : Int? ,isRequestModule : Bool ) -> URL?{
        let urlStr   = Url.getAllSendRequest
        let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
        var urlComponents = URLComponents.init(url: ur, resolvingAgainstBaseURL: false)
        let userData = UserModel.shared
        // add params
        
        //  RequestMoney = 0,BillSplitter = 1
        var requestNoduleValue : Int = 0
        if isRequestModule == false{
            requestNoduleValue = 1
        }
        
        var offsetParam : Int = 0
        if offset != nil {
            offsetParam = offset!
        }
        
        var limitParam : Int = 15
        if limit != nil {
            limitParam = limit!
        }
        
        
        let mobileNUmberQuery       = URLQueryItem(name:"MobileNumber", value:userData.mobileNo)
        let simIdQuery              = URLQueryItem(name:"Simid",value :userData.simID)
        let msIDQuery               = URLQueryItem(name:"MSID" , value:userData.msid)
        let osTypeQuery             = URLQueryItem(name:"OSType", value: userData.osType)
        let requestMoneyStatusQuery = URLQueryItem(name:"RequestedMoneyStatus", value: "\(requestMoneyStatus)")
        let requestTypeQuery        = URLQueryItem(name:"RequestType", value: "\(requestType)")
        let limitQuery              = URLQueryItem(name:"Limit", value: "\(limitParam)")
        let offsetQuery             = URLQueryItem(name:"OffSet", value: "\(offsetParam)")
        let requestModuleQuery      = URLQueryItem(name:"RequestModuleType" , value :"\(requestNoduleValue)" )
        
        
        urlComponents?.queryItems  = [mobileNUmberQuery,simIdQuery,msIDQuery,osTypeQuery,requestMoneyStatusQuery,requestTypeQuery,limitQuery,offsetQuery,requestModuleQuery]
        return  urlComponents?.url
        
    }
    
    //MARK: Params for get all received requests
    class func getAPIForAllReceivedRequest(requestMoneyStatus : String , requestType : String,limit : Int?,offset : Int? ,isRequestModule : Bool ) -> URL?{
        
        let urlStr   = Url.getAllReceivedRequest
        let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
        var urlComponents = URLComponents.init(url: ur, resolvingAgainstBaseURL: false)
        let userData = UserModel.shared
        // add params
        
        //  RequestMoney = 0,BillSplitter = 1
        var requestNoduleValue : Int = 0
        if isRequestModule == false{
            requestNoduleValue = 1
        }
        
        var offsetParam : Int = 0
        if offset != nil {
            offsetParam = offset!
        }
        
        var limitParam : Int = 15
        if limit != nil {
            limitParam = limit!
        }
        
        
        let mobileNUmberQuery       = URLQueryItem(name:"MobileNumber", value:userData.mobileNo)
        let simIdQuery              = URLQueryItem(name:"Simid",value :userData.simID)
        let msIDQuery               = URLQueryItem(name:"MSID" , value:userData.msid)
        let osTypeQuery             = URLQueryItem(name:"OSType", value: userData.osType)
        let otpQuery                = URLQueryItem(name:"OTP", value: userData.simID)
        let requestMoneyStatusQuery = URLQueryItem(name:"Status", value: "\(requestMoneyStatus)")
        let requestTypeQuery        = URLQueryItem(name:"LocalTransType", value: "\(requestType.uppercased())")
        let limitQuery              = URLQueryItem(name:"Limit", value: "\(limitParam)")
        let offsetQuery             = URLQueryItem(name:"OffSet", value: "\(offsetParam)")
        let requestModuleQuery      = URLQueryItem(name:"RequestModuleType" , value :"\(requestNoduleValue)" )
        
        
        urlComponents?.queryItems  = [mobileNUmberQuery,simIdQuery,msIDQuery,osTypeQuery,otpQuery,requestMoneyStatusQuery,requestTypeQuery,limitQuery,offsetQuery,requestModuleQuery]
        return  urlComponents?.url
        
    }
    
    //MARK: Params for get all block contact lists
    class func getAPIForAllBlockContactList(limit : Int?,offset : Int? ,isRequestModule : Bool ) -> URL?{
        let urlStr   = Url.getAllBlockContactList
        let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
        var urlComponents = URLComponents.init(url: ur, resolvingAgainstBaseURL: false)
        let userData = UserModel.shared
        // add params
        
        //  RequestMoney = 0,BillSplitter = 1
        var requestNoduleValue : Int = 0
        if isRequestModule == false{
            requestNoduleValue = 1
        }
        
        var offsetParam : Int = 0
        if offset != nil {
            offsetParam = offset!
        }
        
        var limitParam : Int = 15
        if limit != nil {
            limitParam = limit!
        }
        
        
        let mobileNUmberQuery       = URLQueryItem(name:"MobileNumber", value:userData.mobileNo)
        let simIdQuery              = URLQueryItem(name:"Simid",value :userData.simID)
        let msIDQuery               = URLQueryItem(name:"MSID" , value:userData.msid)
        let osTypeQuery             = URLQueryItem(name:"OSType", value: userData.osType)
        let limitQuery              = URLQueryItem(name:"Limit", value: "\(limitParam)")
        let offsetQuery             = URLQueryItem(name:"OffSet", value: "\(offsetParam)")
        let requestModuleQuery      = URLQueryItem(name:"RequestModuleType" , value :"\(requestNoduleValue)" )
        
        
        urlComponents?.queryItems  = [mobileNUmberQuery,simIdQuery,msIDQuery,osTypeQuery,limitQuery,offsetQuery,requestModuleQuery]
        return  urlComponents?.url
        
    }
    
    
    //MARK: Params for Block or Unblock particular number
    class func blockUnblockParticularNumber(isBlocked : Bool,DesNum : String) -> URL?{
        let urlStr   = Url.blockOrUnblockParticularNumber
        let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
        var urlComponents = URLComponents.init(url: ur, resolvingAgainstBaseURL: false)
        let userData = UserModel.shared
        // add params
        
        
        let mobileNUmberQuery       = URLQueryItem(name:"MobileNumber", value:userData.mobileNo)
        let simIdQuery              = URLQueryItem(name:"Simid",value :userData.simID)
        let msIDQuery               = URLQueryItem(name:"MSID" , value:userData.msid)
        let osTypeQuery             = URLQueryItem(name:"OSType", value: userData.osType)
        let otpQuery                = URLQueryItem(name:"OTP", value: userData.simID)
        let isBlockedQuery          = URLQueryItem(name:"IsBlocked", value: "\(isBlocked)")
        let initiatorNumberQuery    = URLQueryItem(name:"InitiatorNum" , value :"\(userData.mobileNo)" )
        let desNumberQuery          = URLQueryItem(name:"DesNum" , value :"\(DesNum)" )
        
        
        urlComponents?.queryItems  = [mobileNUmberQuery,simIdQuery,msIDQuery,osTypeQuery,otpQuery,isBlockedQuery,initiatorNumberQuery,desNumberQuery]
        return  urlComponents?.url
        
        
        
    }
    
    //MARK: Params for change the status of request
    class func getAPIForChangeStausOfRequest(requestId : String  , status : String) -> URL?{
        
        let urlStr   = Url.changeStatusOfRequest
        let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
        var urlComponents = URLComponents.init(url: ur, resolvingAgainstBaseURL: false)
        let userData = UserModel.shared
        // add params
        
        
        
        
        let mobileNUmberQuery       = URLQueryItem(name:"MobileNumber", value:userData.mobileNo)
        let simIdQuery              = URLQueryItem(name:"Simid", value :userData.simID)
        let msIDQuery               = URLQueryItem(name:"MSID", value:userData.msid)
        let osTypeQuery             = URLQueryItem(name:"OSType", value: userData.osType)
        let otpQuery                = URLQueryItem(name:"OTP", value: userData.simID)
        let requestIDQuery          = URLQueryItem(name:"RequestId", value: "\(requestId)")
        let statusQuery             = URLQueryItem(name:"Status", value: "\(status)")
        
        
        
        urlComponents?.queryItems  = [mobileNUmberQuery,simIdQuery,msIDQuery,osTypeQuery,otpQuery,requestIDQuery,statusQuery]
        return  urlComponents?.url
        
    }
    
    //MARK: Params for remind the request again
    class func getAPIForRemindRequest(pendingRequestModel : RequestMoneyItem) -> URL?{
        
        let urlStr   = Url.remindRequest
        let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
        var urlComponents = URLComponents.init(url: ur, resolvingAgainstBaseURL: false)
        let userData = UserModel.shared
        // add params

        let mobileNUmberQuery       = URLQueryItem(name:"MobileNumber", value:userData.mobileNo)
        let simIdQuery              = URLQueryItem(name:"Simid",value :userData.simID)
        let msIDQuery               = URLQueryItem(name:"MSID" , value:userData.msid)
        let osTypeQuery             = URLQueryItem(name:"OSType", value: userData.osType)
        let otpQuery                = URLQueryItem(name:"OTP", value: userData.simID)
        let sourceQuery          = URLQueryItem(name:"Source", value: "\(pendingRequestModel.Source ?? "")")
        let destinationQuery             = URLQueryItem(name:"Destination", value: "\(pendingRequestModel.Destination ?? "")")
        let amountQuery             = URLQueryItem(name:"Amount", value: "\( pendingRequestModel.Amount ?? 0.0)")
        let dateQuery             = URLQueryItem(name:"Date", value: "\(pendingRequestModel.Date ?? "")")
        let localTransactionQuery             = URLQueryItem(name:"LocalTransactionType", value: "\(pendingRequestModel.LocalTransactionType ?? "")")
        let transactionTypeQuery             = URLQueryItem(name:"TransactionType", value: "\(pendingRequestModel.CommanTransactionType ?? "" )")
        let requestIdQuery = URLQueryItem(name:"id", value:"\(pendingRequestModel.RequestId ?? "" )")
        
        
        
        urlComponents?.queryItems  = [mobileNUmberQuery,simIdQuery,msIDQuery,osTypeQuery,otpQuery,sourceQuery,destinationQuery,amountQuery,dateQuery,localTransactionQuery,transactionTypeQuery,requestIdQuery]
        return  urlComponents?.url
        
    }
    
}


