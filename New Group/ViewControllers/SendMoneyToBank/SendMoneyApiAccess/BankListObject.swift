//
//  BankListObject.swift
//  OK
//
//  Created by Uma Rajendran on 12/23/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import Foundation
import UIKit

class BankModel: NSObject {
    var bankId                  : Int    = 0
    var bankNameEN              : String = ""
    var bankNameMY              : String = ""
    var bankNameUN              : String = ""
    var accountFormat           : String = ""
    var multiAccountFormat      : String = ""
    var isActiveBank            : Bool   = false
    var totalBranches           : Int    = 0
    var branchListByDivision    = [BankBranchByDivision]()
    var accountValidationRule   : String = ""
    
    init(dictionary: Dictionary<String, Any>) {
        super.init()
        if let title = dictionary["bankId"] as? Int {
            self.bankId = title
        }
        if let title = dictionary["bankName"] as? String {
            self.bankNameEN = title
        }
        if let title = dictionary["burmeseName"] as? String {
            self.bankNameMY = title
        }
        if let title = dictionary["accountFormat"] as? String {
            self.accountFormat = title
        }
        if let title = dictionary["isActive"] as? Bool {
            self.isActiveBank = title
        }
        if let title = dictionary["totalBranches"] as? Int {
            self.totalBranches = title
        }
        if let title = dictionary["multiAccountFormat"] as? String {
            self.multiAccountFormat = title
        }
        if let title = dictionary["burmeseNameUnicode"] as? String {
            self.bankNameUN = title
        }
        if let title = dictionary["bankBranchCountListByDivision"] as? [Any] {
            var value: Int = 0
            while value < title.count {
                let branchByDivision = BankBranchByDivision.init(dictionary: title[value] as! Dictionary)
                self.branchListByDivision.append(branchByDivision)
                value += 1
            }
        }
        if let title = dictionary["validationRule"] as? String {
            self.accountValidationRule = title
        }
    }
}

class BankRoutingModel: NSObject {
    var bankId                : Int    = 0
    var bankName              : String = ""
    var projectId             : String = ""
    var rountingType          : String = ""
    
    init(dictionary: Dictionary<String, Any>) {
        super.init()
        if let title = dictionary["Bankid"] as? String {
            self.bankId = Int(title) ?? 0
        }
        if let title = dictionary["BankName"] as? String {
            self.bankName = title
        }
        if let title = dictionary["ProjectId"] as? String {
            self.projectId = title
        }
        if let title = dictionary["RoutingType"] as? String {
            self.rountingType = title
        }
    }
}

class BankBranchByDivision: NSObject {
    var divisionNameEN          : String = ""
    var divisionNameMY          : String = ""
    var divisionNameUN          : String = ""
    var divisionCode            : String = ""
    var branchCount             : String = ""
    var branchListByTownship    = [BankBranchByTownship]()
    
    init(dictionary: Dictionary<String, Any>) {
        super.init()
        if let title = dictionary["divisionEName"] as? String {
            self.divisionNameEN = title
        }
        if let title = dictionary["divisionBName"] as? String {
            self.divisionNameMY = title
        }
        if let title = dictionary["divisionBNameUnicode"] as? String {
            self.divisionNameUN = title
        }
        if let title = dictionary["divisionCode"] as? String {
            self.divisionCode = title
        }
        if let title = dictionary["branchCount"] as? String {
            self.branchCount = title
        }
        if let title = dictionary["bankBranchCountListByTownship"] as? [Any] {
            var value: Int = 0
            while value < title.count {
                let branchByTownship = BankBranchByTownship.init(dictionary: title[value] as! Dictionary)
                self.branchListByTownship.append(branchByTownship)
                value += 1
            }
        }
    }
}

class BankBranchByTownship: NSObject {
    var townshipNameEN          : String = ""
    var townshipNameMY          : String = ""
    var townshipNameUN          : String = ""
    var townshipCode            : String = ""
    var branchCount             : String = ""
    var branchList              = [BankBranch]()
    
    init(dictionary: Dictionary<String, Any>) {
        super.init()
        if let title = dictionary["townshipEName"] as? String {
            self.townshipNameEN = title
        }
        if let title = dictionary["townshipBName"] as? String {
            self.townshipNameMY = title
        }
        if let title = dictionary["townshipBNameUnicode"] as? String {
            self.townshipNameUN = title
        }
        if let title = dictionary["townshipCode"] as? String {
            self.townshipCode = title
        }
        if let title = dictionary["branchCount"] as? String {
            self.branchCount = title
        }
        if let title = dictionary["bankBranchDetails"] as? [Any] {
            var value: Int = 0
            while value < title.count {
                let branchlist = BankBranch.init(dictionary: title[value] as! Dictionary)
                self.branchList.append(branchlist)
                value += 1
            }
        }
    }
}

class BankBranch: NSObject {
    var branchId                : Int    = 0
    var branchNameEN            : String = ""
    var branchNameMY            : String = ""
    var branchNameUN            : String = ""
    var branchAddressEN         : String = ""
    var branchAddressMY         : String = ""
    var branchAddressUN         : String = ""
    var branchPhoneNumber       : String = ""
    var branchFaxNumber         : String = ""
    
    init(dictionary: Dictionary<String, Any>) {
        super.init()
        if let title = dictionary["branchId"] as? String {
            self.branchId = Int(title)!
        }
        if let title = dictionary["branchEName"] as? String {
            self.branchNameEN = title
        }
        if let title = dictionary["branchBName"] as? String {
            self.branchNameMY = title
        }
        if let title = dictionary["branchBNameUnicode"] as? String {
            self.branchNameUN = title
        }
        if let title = dictionary["branchEAddress"] as? String {
            self.branchAddressEN = title
        }
        if let title = dictionary["branchBAddress"] as? String {
            self.branchAddressMY = title
        }
        if let title = dictionary["branchEAddressUnicode"] as? String {
            self.branchAddressUN = title
        }
        if let title = dictionary["phonenumber"] as? String {
            self.branchPhoneNumber = title
        }
        if let title = dictionary["faxnumber"] as? String {
            self.branchFaxNumber = title
        }
        
    }
    
}
