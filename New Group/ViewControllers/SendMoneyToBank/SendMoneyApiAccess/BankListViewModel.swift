//
//  BankListViewModel.swift
//  OK
//  It contains the model for the bank list
//  Created by Uma Rajendran on 12/23/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import Foundation
import UIKit

class BankViewModel {
    static let shared       = BankViewModel()
    var bankList            = [BankModel]()
    var selectedBank            : BankModel?
    var selectedDivision        : BankBranchByDivision?
    var selectedTownship        : BankBranchByTownship?
    var selectedBranch          : BankBranch?
    var selectedAccountType     : String?
    var selectedNRCDivLocation  : LocationDetail?
    var selectedNRCTownship     : TownShipDetail?
    var currentAccView          : AccountView = .myacc
    var currentMyAccView : MyAccountView = .myacc_addbank
    var currentOtherAccView : OtherAccountView = .otheracc_addbank
    var bankAccDetailsInfoDict = [String: BankAccountModel]()
    
    var bankListDB            = [Bank]()
    var selectedBankDB            : Bank?
    var selectedDivisionDB        : BankByDivision?
    var selectedTownshipDB        : BankByTownship?
    var selectedBranchDB          : BankBranchAddress?
     
    static var apiCallInProgress : Bool = false
    init() {
        //BankViewModel.hitBankListApi()
    }
    
    class func hitBankListApi() {
        println_debug("hitBankListApi")
        self.apiCallInProgress = true
        guard appDelegate.checkNetworkAvail() else {
            self.apiCallInProgress = false
            return
        }
        self.apiCallInProgress = false
        PaymentVerificationManager.deleteAllRecordsFromDatabase { (status) in
            if status {
                BankApiClient.fetchBanksList { (isSuccess, banklist) in
                    guard isSuccess else {
                        self.hitBankListApi()
                        return
                    }
                    guard let list = banklist else { return }
                    PaymentVerificationManager.insertAllRecordsToBankDatabase(model: list)
                }
            }
        }
        
//        BankApiClient.fetchBanksList { (isSuccess, banklist) in
//            guard isSuccess else {
//                self.hitBankListApi()
//                return
//            }
//            self.apiCallInProgress = false
//            DispatchQueue.main.async {
//                BankViewModel.shared.bankList = banklist!
//            }
//        }
    }
    
    class func refreshBank() {
        self.shared.selectedBank            = nil
        self.shared.selectedDivision        = nil
        self.shared.selectedTownship        = nil
        self.shared.selectedBranch          = nil
        self.shared.selectedAccountType     = nil
    }
    
    class func refreshNRC() {
        self.shared.selectedNRCDivLocation  = nil
        self.shared.selectedNRCTownship     = nil
    }
    
}
