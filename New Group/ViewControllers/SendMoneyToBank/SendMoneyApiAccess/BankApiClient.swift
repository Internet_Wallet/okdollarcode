//
//  BankApiClient.swift
//  OK
//  This class handles the api hit in send money to bank module
//  Created by Uma Rajendran on 1/11/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Url constants
/// This structure holds the the url constants
struct SMHelper {
    static let bankList             = "AdService.svc/GetBankListWithCount"
    static let bankRoutingList      = "RestService.svc/GetBankPaymentRoutingInfo "
    static let addNewBank           = "BankPayments.Svc/AddBankDetails?PhoneNumber=%@&AccountNumber=%@&IdType=%@&NrcNumber=%@&BankPhone=%@&AccountType=%@&AccountName=%@&bankId=%@&bankBranchId=%@&AccountModel=%@&BranchAddress=%@&State=%@&Township=%@&SimId=%@&MsId=%@&OsType=%@&Otp=%@&BeneficiaryMobNumber=%@&Remarks=%@&EmailId=%@"
    static let bankAmountTransfer   = "BankPayments.Svc/BankTransfer"
    static let getBankDetails       = "RestService.svc/GetBankDetails?MobileNumber=%@&SimId=%@&MsId=%@&OsType=%@&OTP=%@"
    static let deleteBank           = "BankPayments.Svc/DeleteBankDetails?PhoneNumber=%@&BankDetailId=%@&SimId=%@&MsId=%@&OsType=%@&Otp=%@"
    static let updateBank           = "BankPayments.svc/UpdateBankAccDetails"
    static let kinleyPaymentNumber  = "RestService.svc/RetrieveBankPaymentNumber?MobileNumber=%@&Simid=%@&MSID=%@&OSType=%@&OTP=%@"
}

// MARK: - Api hit methods
/// This class contains method for the api hit
class BankApiClient: NSObject {
    /// It fetches the bank list
    ///
    /// - Parameter completionHandler: It returns the boolean value and array of bank list
    class func fetchBanksList(completionHandler: @escaping (Bool,[BankModel]?) -> Void) {
        let url = getUrl(urlStr: SMHelper.bankList, serverType: .SMBBankList)
        let jsonDic = ["AppId" : "", "MobileNumber" : UserModel.shared.mobileNo, "Msid" : msid, "Ostype": "1", "Otp" : otp, "Simid": simid]
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url, methodType: kMethod_Post, contentType: kContentType_Json,
                                                       inputVal: jsonDic, authStr: "")

      //  println_debug(url)
       // println_debug(jsonDic)

        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            guard isSuccess else {
                completionHandler(false, nil)
                return
            }
            do {
                if let json = response as? NSDictionary {
                    if (json["Msg"] as? String)?.lowercased() == "success" || (json["Msg"] as? String)?.lowercased() == "sucess" {
                        let resData : Data? = (json[kJSONData] as? String)?.data(using: String.Encoding.utf8)!
                        guard resData != nil else {
                            completionHandler(false, nil)
                            return
                        }
                        let jsondic = try JSONSerialization.jsonObject(with: resData!, options: .allowFragments)
                        let bankList = ParserAttributeClass.parseBankListJSON(anyValue: jsondic as AnyObject) as [BankModel]
                        completionHandler(true, bankList)
                    }
                }
            } catch {
                
            }
        })
    }
    
    class func fetchBanksRoutingList(completionHandler: @escaping (Bool,[BankRoutingModel]?) -> Void) {
        let url = getUrl(urlStr: SMHelper.bankRoutingList, serverType: .serverApp)
        let jsonDic = ["AppId" : "","Limit":0, "MobileNumber" : UserModel.shared.mobileNo, "Msid" : msid, "Offset":0, "Ostype": "1", "Otp" : otp ?? "", "Simid": simid ?? ""] as [String : Any]
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url, methodType: kMethod_Post, contentType: kContentType_Json,
                                                       inputVal: jsonDic, authStr: "")
        //println_debug("bankRoutingList: \(url)")
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            guard isSuccess else {
                completionHandler(false, nil)
                return
            }
            do {
                if let json = response as? NSDictionary {
                    if (json["Msg"] as? String)?.lowercased() == "success" || (json["Msg"] as? String)?.lowercased() == "sucess" {
                        let resData : Data? = (json[kJSONData] as? String)?.data(using: String.Encoding.utf8)!
                        guard resData != nil else {
                            completionHandler(false, nil)
                            return
                        }
                        let jsondic = try JSONSerialization.jsonObject(with: resData!, options: .allowFragments)
                        let bankList = ParserAttributeClass.parseBankRoutingListJSON(anyValue: jsondic as AnyObject) as [BankRoutingModel]
                        completionHandler(true, bankList)
                    }
                }
            } catch {
                
            }
        })
    }
    
    
    /// It add the bank like beneficiary
    ///
    /// - Parameters:
    ///   - accDetails: account details of which need to be added like beneficiary
    ///   - completionHandler: It contains a bool, string and any data
    class func addBankApi(accDetails: BankAccountModel, completionHandler: @escaping (Bool, String?, Any?) -> Void) {
        let idType = accDetails.selectedIdProofType == .id_nrc ? "NRC" : "Passport"
        let accModel = accDetails.isMyAccount ? "MyAccount" : "OtherAccount"
        let urlStr = String.init(format: SMHelper.addNewBank, UserModel.shared.mobileNo, accDetails.accountNumber, idType,
                                 accDetails.accountIDProofNumber,accDetails.bankPhoneNumber, accDetails.accountType,
                                 accDetails.accountHolderName, String(accDetails.bankId), String(accDetails.branchId),
                                 accModel, accDetails.branchAddress, accDetails.stateName, accDetails.townshipName, simid ?? "",
                                 msid, "1", uuid ?? "", accDetails.contactMobileNumber, accDetails.remarks, accDetails.emailid)
        let url = getUrl(urlStr: urlStr.validUrl(), serverType: .serverApp)
        //println_debug("BankApiClient: \(url)")
        let request = URLRequest.init(url: url, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
        _ = URLSession.shared.configuration
        URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            do {
                guard data != nil || error == nil else {
                    completionHandler(false, nil, nil)
                    return
                }
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                if let dic = json as? Dictionary<String, Any> {
                    if let code = dic["Code"] as? Int {
                        guard code == 200 else {
                            var errMsg = ""
                            if let msg = dic["Msg"] as? String {
                                errMsg = (msg.count > 0) ? msg : "Something went wrong".localized
                            }
                            completionHandler(false, errMsg, nil)
                            return
                        }
                        completionHandler(true, nil, dic)
                        return
                    }
                }
                completionHandler(false, nil, nil)
            } catch {
                completionHandler(false, nil, nil)
            }
            }.resume()
    }
    
    /// It pays the amount in the account
    ///
    /// - Parameters:
    ///   - accDetail: account detail where the payment has to be made
    ///   - cashOutDetail: cash detail of payment
    ///   - cashOutApiHandler: handler contains the boolean, string and any data type
    class func bankCashOutAmountApi(accDetail: BankAccountModel, cashOutDetail: BankCashOutModel, cashOutApiHandler: @escaping (Bool, String?, Any?) -> Void) {
        let amount =  cashOutDetail.cashOutAmount.components(separatedBy: ",").joined(separator: "") 
        let destinationNumber = (UserModel.shared.agentType == .advancemerchant) ? UserModel.shared.paymentNumber_AdvanceMerchant : ""
        var comments = "SEND MONEY TO BANK: \(accDetail.bankName ), \(accDetail.townshipName ), \(accDetail.accountNumber ), \(accDetail.accountHolderName ), \(accDetail.accountType ), \(accDetail.branchAddress )"
        comments = comments.replacingOccurrences(of: "$", with: " ").replacingOccurrences(of: "&", with: "And").replacingOccurrences(of: "/", with: " ").replacingOccurrences(of: "<", with: " ").replacingOccurrences(of: ">", with: " ")
        
        let inputDict = [ "MobileNumber"    : UserModel.shared.mobileNo,
                          "Simid"           : simid,
                          "Msid"            : msid,
                          "Ostype"          : 1,
                          "Otp"             : uuid,
                          "Destination"     : destinationNumber,
                          "AgentCode"       : UserModel.shared.mobileNo,
                          "Pin"             : ok_password ?? "",
                          "BankAccNum"      : accDetail.accountNumber,
                          "Amount"          : amount,
                          "Password"        : ok_password ?? "",
                          "Remarks"         : accDetail.remarks,
                          "Comments"        : comments,
                          "SecureToken"     : UserLogin.shared.token] as [String: Any]
        let url = getUrl(urlStr: SMHelper.bankAmountTransfer, serverType: .serverApp)
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url,methodType: kMethod_Post, contentType: kContentType_Json, inputVal: inputDict, authStr: nil)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (issuccess, response) in
            guard issuccess else {
                cashOutApiHandler(false, "Something went wrong".localized, nil)
                return
            }
            guard let dict = response as? Dictionary<String, Any> else {
                cashOutApiHandler(false, "Something went wrong".localized, nil)
                return
            }
            if let code = dict["Code"] as? Int {
                guard code == 200 else {
                    var errMsg = ""
                    if code == 79 {
                        errMsg = "You can't pay the same amount within 5 minutes".localized
                    } else {
                        if let msg = dict["Msg"] as? String {
                            errMsg = (msg.count > 0) ? msg : "Check the inputs".localized
                        }
                    }
                    cashOutApiHandler(false,errMsg, nil)
                    return
                }
                if let xmlData = dict["Data"] as? String {
                    guard let xmlDict = self.parseXmlData(data: xmlData) else {
                        cashOutApiHandler(false, "Something went wrong".localized, nil)
                        return
                    }
                    cashOutApiHandler(true, nil, xmlDict)
                    return
                }
            }
            cashOutApiHandler(false,"Something went wrong".localized, nil)
        })
    }
    
    /// It delete the added bank (beneficiary)
    ///
    /// - Parameters:
    ///   - bankDetail: detail of the account
    ///   - deleteApiHanlder: handler contains the boolean value, string value and any data value
    class func deleteBank(bankDetail: SendMoneyToBank, deleteApiHanlder: @escaping (Bool, String?, Any?) -> Void) {
        let urlStr = String.init(format: SMHelper.deleteBank, UserModel.shared.mobileNo, bankDetail.serverBankId!, simid, msid, "1", uuid)
        let url = getUrl(urlStr: urlStr.validUrl(), serverType: .serverApp)
        let request = URLRequest.init(url: url, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
        _ = URLSession.shared.configuration
        URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            do {
                guard data != nil || error == nil else {
                    deleteApiHanlder(false, nil, nil)
                    return
                }
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                if let dic = json as? Dictionary<String, Any> {
                    if let code = dic["Code"] as? Int {
                        guard code == 200 else {
                            var errMsg = ""
                            if let msg = dic["Msg"] as? String {
                                errMsg = (msg.count > 0) ? msg : "Something went wrong".localized
                            }
                            deleteApiHanlder(false, errMsg, nil)
                            return
                        }
                        deleteApiHanlder(true,nil,nil)
                        return
                    }
                }
                deleteApiHanlder(false, nil, nil)
            } catch {
                deleteApiHanlder(false, nil, nil)
            }
            }.resume()
    }
    
    /// It fetch the all saved banks (beneficiary)
    ///
    /// - Parameter completionHandler: handler contains the boolean value and any data type value
    class func getAllSavedBanks(completionHandler: @escaping (Bool, Any?) -> Void) {
        let urlStr = String.init(format: SMHelper.getBankDetails, UserModel.shared.mobileNo, simid, msid, "1", uuid)
        let url = getUrl(urlStr: urlStr.validUrl(), serverType: .serverApp)
        let request = URLRequest.init(url: url, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
        _ = URLSession.shared.configuration
        println_debug("getAllSavedBanks: \(url)")
        URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            do {
                guard data != nil || error == nil else {
                    completionHandler(false, nil)
                    return
                }
                let json = try JSONSerialization.jsonObject(with: data!,
                                                            options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                if let dic = json as? Dictionary<String, Any> {
                    if let code = dic["Code"] as? Int {
                        guard code == 200 else {
                            completionHandler(false, nil)
                            return
                        }
                        if let resString = dic["Msg"] as? String {
                            let resData: Data? = resString.data(using: String.Encoding.utf8)!
                            if let responseArr = try JSONSerialization.jsonObject(with: resData!,
                                                                                  options: JSONSerialization.ReadingOptions.allowFragments) as? [Dictionary<String,Any>] {
                                if responseArr.count > 0 {
                                    completionHandler(true, responseArr)
                                    return
                                }
                            }
                        }
                        completionHandler(true, nil)
                        return
                    }
                }
                completionHandler(false, nil)
            } catch {
                completionHandler(false, nil)
            }
            }.resume()
    }
    
    /// It updates the bank detail
    ///
    /// - Parameters:
    ///   - accDetail: account detail to update
    ///   - updateApiHandler: handler contains the boolean value and string and any data type value
    class func updateBankApi(accDetail: BankAccountModel, updateApiHandler: @escaping (Bool, String?, Any?) -> Void) {
        let inputDict = [
            "Login": [
                "AppId": "",
                "MobileNumber": UserModel.shared.mobileNo,
                "SimId": simid,
                "Msid": msid,
                "Ostype": "1",
                "Otp": uuid,
                "Limit": 0,
                "Offset": 0
            ],
            "BankId": accDetail.serverBankId,
            "EmailId": accDetail.emailid,
            "Remarks": accDetail.remarks
            ] as [String : Any]
        let url = getUrl(urlStr: SMHelper.updateBank, serverType: .serverApp)
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url,methodType: kMethod_Post,
                                                       contentType: kContentType_Json, inputVal: inputDict, authStr: nil)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (issuccess, response) in
            guard issuccess else {
                updateApiHandler(false,"Something went wrong".localized,nil)
                return
            }
            if let dic = response as? Dictionary<String, Any> {
                if let code = dic["Code"] as? Int {
                    guard code == 200 else {
                        updateApiHandler(false,"Something went wrong".localized, nil)
                        return
                    }
                    updateApiHandler(true, nil, dic)
                    return
                }
            }
            updateApiHandler(false,"Something went wrong".localized, nil)
            return
        })
    }
    
    /// It retrives the bank payment number
    ///
    /// - Parameter apiHandler: handler contains the boolean value and any data type value
    class func kinleyNum_AdvanceMerchant(apiHandler: @escaping (Bool, Any?) -> Void) {
        let urlStr = String.init(format: SMHelper.kinleyPaymentNumber, UserModel.shared.mobileNo, simid, msid, "1", uuid)
        let url = getUrl(urlStr: urlStr.validUrl(), serverType: .serverApp)
        JSONParser.GetApiResponse(apiUrl: url, type: "POST") { (isSuccess, response) in
            guard isSuccess else {
                apiHandler(false,nil)
                return
            }
            if let dic = response as? Dictionary<String, Any> {
                if let kinleyNum = dic["PhoneNumber"] as? String {
                    apiHandler(true, kinleyNum)
                }
            } else {
                apiHandler(false,nil)
            }
        }
    }
    
    /// It adds the five number
    ///
    /// - Parameters:
    ///   - param: parameter for this request
    ///   - apiHandler: hanlder contains the boolean, string, and any data type value
    class func addOk$FiveNumbers(param: Any, apiHandler: @escaping (Bool, String?, Any?) -> Void) {
        let urlStr =   String.init(format: Url.addFiveNumber)
        let url  =   getUrl(urlStr: urlStr, serverType: .serverApp)
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url, methodType: kMethod_Post, contentType: kContentType_Json,
                                                       inputVal: param, authStr: nil)
        //println_debug("addOk$FiveNumbers: \(urlRequest)")
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (issuccess, response) in
            guard issuccess else {
                apiHandler(false, nil, nil)
                return
            }
            if let dict = response as? Dictionary<String, Any> {
                if let code = dict["Code"] as? Int {
                    guard code == 200 else {
                        var errMsg = ""
                        if let msg = dict["Msg"] as? String {
                            errMsg = (msg.count > 0) ? msg : "Something went wrong".localized
                        }
                        apiHandler(false, errMsg, nil)
                        return
                    }
                    apiHandler(true,nil, dict)
                }
            } else {
                apiHandler(false,nil, nil)
            }
        })
    }
    
    // MARK: - XML Parser
    static var xmlResponseDict = Dictionary<String, Any>()
    
    /// Parsing xml
    ///
    /// - Parameter data: data to parse
    /// - Returns: return dictionary value
    class func parseXmlData(data: Any) -> Dictionary<String,Any>? {
        let xmlString = SWXMLHash.parse(data as! String)
        self.enumerate(indexer: xmlString)
        if xmlResponseDict["resultdescription"] as? String == "Transaction Successful" {
            let dic: Dictionary<String,Any> = xmlResponseDict
            xmlResponseDict.removeAll()
            return dic
        }
        return nil
    }
    
    class func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            xmlResponseDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
}

