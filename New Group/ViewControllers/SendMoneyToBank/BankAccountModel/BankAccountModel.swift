//
//  BankAccountModel.swift
//  OK
//
//  Created by Uma Rajendran on 12/21/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol BankAccountModelUIUpdates  {
    func didChangeBankAccountModel()
}

class BankAccountModel: NSObject {
    var delegate : BankAccountModelUIUpdates?
    var accountHolderName                    : String  = "" {
        didSet {
            if let delegate = self.delegate {
                delegate.didChangeBankAccountModel()
            }
        }
    }
    var accountIDProofNumber                 : String  = "" {
        didSet {
            if let delegate = self.delegate {
                delegate.didChangeBankAccountModel()
            }
        }
    }
    var amount                               : String  = "" {
        didSet {
            if let delegate = self.delegate {
                delegate.didChangeBankAccountModel()
            }
        }
    }
    var accountType                          : String  = "" {
        didSet {
            if let delegate = self.delegate {
                delegate.didChangeBankAccountModel()
            }
        }
    }
    var accountNumber                        : String  = ""  {
        didSet {
            if let delegate = self.delegate {
                delegate.didChangeBankAccountModel()
            }
        }
    }
    var bankName                             : String  = "" {
        didSet {
            if let delegate = self.delegate {
                delegate.didChangeBankAccountModel()
            }
        }
    }
    var bankBurmeseName                      : String  = ""  {
        didSet {
            if let delegate = self.delegate {
                delegate.didChangeBankAccountModel()
            }
        }
    }
    var bankBurmeseNameUnicode              : String  = ""  {
        didSet {
            if let delegate = self.delegate {
                delegate.didChangeBankAccountModel()
            }
        }
    }
    
    var bankId                               : Int     = 0
    var branchId                             : Int     = 0
    var branchName                           : String  = ""
    var branchBurmeseName                    : String  = ""
    var townshipCode                         : String  = ""
    var townshipName                         : String  = ""
    var townshipBurmeseName                  : String  = ""
    var stateCode                            : String  = ""
    var stateName                            : String  = ""
    var stateBurmeseName                     : String  = ""
    var branchAddress                        : String  = ""
    var branchBurmeseAddress                 : String  = ""
    var bankPhoneNumber                      : String  = ""
    var bankFaxNumber                        : String  = ""
    var accountFormat                        : String  = ""
    var multiAccountFormat                   : String  = ""
    var nrcCodeNumber                        : String  = ""
    var nrcCode                              : String  = ""
    var nrcFullCode                          : String  = ""
    var branchBurmeseNameUnicode             : String  = ""
    var townshipBurmeseNameUnicode           : String  = ""
    var stateBurmeseNameUnicode              : String  = ""
    var branchBurmeseAddressUnicode          : String  = ""
    var contactMobileNumber                  : String  = "09" {
        didSet {
            if let delegate = self.delegate {
                delegate.didChangeBankAccountModel()
            }
        }
    }
    var remarks                              : String  = "" {
        didSet {
            if let delegate = self.delegate {
                delegate.didChangeBankAccountModel()
            }
        }
    }
    var emailid                              : String  = "" {
        didSet {
            if let delegate = self.delegate {
                delegate.didChangeBankAccountModel()
            }
        }
    }
    var isNrcCodeSelected                    : Bool    =  false
    var isMyAccount                          : Bool    =  false
    var isExistingBank                       : Bool    =  false
    var isBankActive                         : Bool    =  false
    var serverBankId                         : String  =  ""
    var selectedIdProofType                  : IdProofType  = .id_nrc
    var isBankAddressExpanded                : Bool    =  false
    var isLoadFromExistingBank               : Bool    =  false
    
    override init() {
        super.init()
    }
}

class BankCashOutModel: NSObject {
    override init() {
        super.init()
    }
    var accountNumber                       : String         = ""
    var amountTransferTime                  : Date           = Date()
    var bankId                              : Int            = 0
    var branchId                            : Int            = 0
    var cashOutAmount                       : String         = ""
    var cashOutStatus                       : Bool           = false
    var isActiveBank                        : Bool           = false
    var transactionId                       : String         = ""
    var balanceAmount                       : String         = ""
}


var minAcNumCount = 0
var maxAcNumCount = 0
