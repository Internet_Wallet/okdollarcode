//
//  SendMoneyMainCell.swift
//  OK
//  Main cell of send money
//  Created by Uma Rajendran on 12/19/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class SendMoneyMainCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var accImageView         : UIImageView!
    @IBOutlet weak var contentTxtField      : RestrictedCursorMovementForSMB!
    @IBOutlet weak var rowActionBtn         : UIButton!
    @IBOutlet weak var separatorLabel       : UILabel!
    @IBOutlet weak var contactNumberCloseBtn: UIButton!
    @IBOutlet weak var contactNumWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var operatorLabel: UILabel!
    @IBOutlet weak var operatorLblWidthConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    var cellindex       : Int = 0
    var cellname        : BankItems?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textFieldSettings()
        separatorLabel.backgroundColor         =  UIColor.blue
        operatorLabel.font                     =  UIFont(name: appFont, size: 14)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Methods
    /// It set up the text field
    func textFieldSettings() {
        contentTxtField.font                    = UIFont(name: appFont, size: 15) ?? UIFont.systemFont(ofSize: 15)
        contentTxtField.titleFont               = UIFont(name: appFont, size: 13) ?? UIFont.systemFont(ofSize: 13)
        contentTxtField.placeholderFont         = UIFont(name: appFont, size: 15) ?? UIFont.systemFont(ofSize: 15)
        contentTxtField.placeholderColor        = UIColor(red: 25.0/255.0, green: 50.0/255.0, blue: 156.0/255.0, alpha: 1.0)
        contentTxtField.textColor               = UIColor.black
        contentTxtField.lineColor               = UIColor.clear
        contentTxtField.selectedLineColor       = UIColor.clear
        contentTxtField.selectedTitleColor      = UIColor(red: 25.0/255.0, green: 50.0/255.0, blue: 156.0/255.0, alpha: 1.0)
        contentTxtField.titleColor              = UIColor(red: 25.0/255.0, green: 50.0/255.0, blue: 156.0/255.0, alpha: 1.0)
    }
    
    /// It unwraps the each and every data separately
    ///
    /// - Parameters:
    ///   - accDetails: account details where the data is fetched and
    ///   - cellIndex: cell index to get the type of data to be unwrapped
    func wrap_CellData(accDetails: BankAccountModel, cellIndex: Int) {
        switch cellindex {
        case 0:
            self.wrap_accountHolderName(accDetails: accDetails)
        case 1:
            self.wrap_NRCPassport(accDetails: accDetails)
        case 2:
            self.wrap_amount(accDetails: accDetails)
        case 3:
            self.wrap_bankName(accDetails: accDetails)
        case 4:
            self.wrap_accountType(accDetails: accDetails)
        case 5:
            self.wrap_accountNumber(accDetails: accDetails)
        case 6:
            self.wrap_contactNumber(accDetails: accDetails)
        case 7:
            self.wrap_remarks(accDetails: accDetails)
        case 8:
            self.wrap_email(accDetails: accDetails)
        default:
            break
        }
    }
    
    /// It unwraps the account holder name
    ///
    /// - Parameter accDetails: account details to get the account holder name
    func wrap_accountHolderName(accDetails: BankAccountModel, offSetValue: Int? = nil) {
        self.contentTxtField.text = accDetails.accountHolderName
        if let offSet = offSetValue {
            if let newPosition = self.contentTxtField.position(from: self.contentTxtField.beginningOfDocument, offset: offSet) {
                self.contentTxtField.selectedTextRange = self.contentTxtField.textRange(from: newPosition, to: newPosition)
            }
        }
        if accDetails.accountHolderName.count > 0 {
            self.contentTxtField.title          = accDetails.isMyAccount ? "OK$ Account Name".localized : "Account Holder Name".localized
        } else {
            self.contentTxtField.placeholder    = "Enter Account Holder Name".localized
        }
        if accDetails.isLoadFromExistingBank {
            self.contentTxtField.isUserInteractionEnabled = false
        } else {
            self.contentTxtField.isUserInteractionEnabled = (accDetails.isMyAccount == true) ? false : true
        }
        self.rowActionBtn.isHidden = true
        self.rowActionBtn.setImage(nil, for: .normal)
        self.accImageView.image = UIImage.init(named : "sm_nameNew")
        self.showHideContactNumberCloseBtn(isHide: true)
        self.showHideOperatorLabel(isHide: true, accDetail: accDetails)
    }
    
    /// It unwraps the nrc or passport detail
    ///
    /// - Parameter accDetails: account details to get the nrc or passport detail
    func wrap_NRCPassport(accDetails: BankAccountModel) {
        self.contentTxtField.text               = accDetails.accountIDProofNumber
        if accDetails.accountIDProofNumber.count > 0 {
            self.contentTxtField.title          = UserModel.shared.idType == "04" ? "Passport Number".localized : "NRC".localized
        } else {
            self.contentTxtField.placeholder    = "Enter Account NRC Number".localized
        }
        if accDetails.isLoadFromExistingBank {
            self.contentTxtField.isUserInteractionEnabled =  false

        } else {
            self.contentTxtField.isUserInteractionEnabled = (accDetails.isMyAccount == true) ? false : true
        }
        self.rowActionBtn.isHidden = true
        self.rowActionBtn.setImage(nil, for: .normal)
        let imgName = (accDetails.selectedIdProofType == .id_nrc) ? "sm_nrc" : "sm_passport"
        self.accImageView.image = UIImage.init(named : imgName)
        self.showHideContactNumberCloseBtn(isHide: true)
        self.showHideOperatorLabel(isHide: true, accDetail: accDetails)
    }
    
    /// It unwraps the amount
    ///
    /// - Parameters:
    ///   - accDetails: account details to get the amount to deposit
    ///   - offSetValue: offset value to fix the cursor position
    func wrap_amount(accDetails: BankAccountModel, offSetValue: Int? = nil) {
        self.contentTxtField.text               = accDetails.amount
        if let offSet = offSetValue {
            if let newPosition = self.contentTxtField.position(from: self.contentTxtField.beginningOfDocument, offset: offSet) {
                self.contentTxtField.selectedTextRange = self.contentTxtField.textRange(from: newPosition, to: newPosition)
            }
        }
        if accDetails.amount.count  >  0 {
            self.contentTxtField.title          = "Sending Amount".localized
        } else {
            self.contentTxtField.placeholder    = "Enter Sending Amount".localized
        }
        self.contentTxtField.isUserInteractionEnabled = true
        self.rowActionBtn.isHidden = true
        self.rowActionBtn.setImage(nil, for: .normal)
        self.accImageView.image = UIImage.init(named : "sm_amount")
 
        self.showHideContactNumberCloseBtn(isHide: true)
        self.showHideOperatorLabel(isHide: true, accDetail: accDetails)
    }
    
    /// It unwraps the bank name
    ///
    /// - Parameter accDetails: account details to get the bank name
    func wrap_bankName(accDetails: BankAccountModel) {
        self.contentTxtField.text = ""
        if let curLanguage = UserDefaults.standard.value(forKey: "currentLanguage") as? String {
            switch curLanguage {
            case "en":
                self.contentTxtField.text = accDetails.bankName
            case "my":
                self.contentTxtField.text = accDetails.bankBurmeseName
            default:
                self.contentTxtField.text = accDetails.bankName
            }
        }
        if accDetails.bankName.count > 0 {
            self.contentTxtField.title          = "Bank".localized
        } else {
            self.contentTxtField.placeholder    = "Select Bank".localized
        }
        self.contentTxtField.isUserInteractionEnabled = false
        
        if accDetails.bankName.count > 0 {
            self.rowActionBtn.isHidden = false
            let imgName = accDetails.isBankAddressExpanded ? "up_arrow_small" : "down_arrow_small"
            self.rowActionBtn.setImage(UIImage.init(named: imgName), for: .normal)
        } else {
            self.rowActionBtn.isHidden = true
        }
        self.accImageView.image = UIImage.init(named : "sm_bank")
        self.showHideContactNumberCloseBtn(isHide: true)
        self.showHideOperatorLabel(isHide: true, accDetail: accDetails)
    }
    
    /// It unwraps the account type
    ///
    /// - Parameter accDetails: account details to get the account type
    func wrap_accountType(accDetails: BankAccountModel) {
        let accountTypes = ["CDA" : "Call Deposit Account", "SAV" : "Savings Account", "CUR" : "Current Account", "ACA": "ATM Card Account"]
        if accDetails.accountType.count > 0 {
            if let accType = accountTypes[accDetails.accountType] {
                self.contentTxtField.text           = accType.localized
            }
            self.contentTxtField.title          = "Account Type".localized
        } else {
            self.contentTxtField.text           = ""
            self.contentTxtField.placeholder    = "Select Account Type".localized
        }
        self.contentTxtField.isUserInteractionEnabled = false
        self.rowActionBtn.isHidden = true
        self.rowActionBtn.setImage(nil, for: .normal)
        self.accImageView.image = UIImage.init(named : "sm_account_type")
        self.showHideContactNumberCloseBtn(isHide: true)
        self.showHideOperatorLabel(isHide: true, accDetail: accDetails)
    }
    
    /// It unwraps the account number
    ///
    /// - Parameter accDetails: account details to get the account number
    func wrap_accountNumber(accDetails: BankAccountModel) {
        self.contentTxtField.text              = accDetails.accountNumber
        if accDetails.accountNumber.count > 0 {
            self.contentTxtField.title         = "Bank Account Number".localized
        } else {
            self.contentTxtField.placeholder   = "Enter Bank Account Number".localized
        }
        if accDetails.isLoadFromExistingBank {
            self.contentTxtField.isUserInteractionEnabled = false
            self.rowActionBtn.isHidden = true

        } else {
            self.contentTxtField.isUserInteractionEnabled = true
            if accDetails.accountNumber.count > 0 {
                self.rowActionBtn.isHidden = false
            } else {
                self.rowActionBtn.isHidden = true
            }
        }
        self.rowActionBtn.setImage(UIImage.init(named: "closeBlue"), for: .normal)
        self.accImageView.image = UIImage.init(named : "sm_account_number")
        self.showHideContactNumberCloseBtn(isHide: true)
        self.showHideOperatorLabel(isHide: true, accDetail: accDetails)
    }
    
    /// It unwraps the contact number
    ///
    /// - Parameter accDetails: account details to get the contact number
    func wrap_contactNumber(accDetails: BankAccountModel) {
        self.accImageView.image = UIImage.init(named : "sm_mobile_number")
        self.contentTxtField.text              = accDetails.contactMobileNumber
        if accDetails.contactMobileNumber.count > 0 {
            self.contentTxtField.title         = "Contact Mobile Number".localized
        } else {
            self.contentTxtField.placeholder   = "Enter Contact Mobile Number".localized
        }
        if accDetails.isLoadFromExistingBank {
            self.contentTxtField.isUserInteractionEnabled = false
            self.rowActionBtn.isHidden = true
            self.showHideContactNumberCloseBtn(isHide: true)
            self.showHideOperatorLabel(isHide: false, accDetail: accDetails)
        } else {
            self.contentTxtField.isUserInteractionEnabled = true
            self.rowActionBtn.isHidden = false
            self.rowActionBtn.setImage(UIImage.init(named: "contact"), for: .normal)
            if accDetails.contactMobileNumber.count > 2 {
                self.showHideContactNumberCloseBtn(isHide: false)
                self.showHideOperatorLabel(isHide: false, accDetail: accDetails)
            } else {
                self.showHideContactNumberCloseBtn(isHide: true)
                self.showHideOperatorLabel(isHide: true, accDetail: accDetails)
            }
        }
    }
    
    /// It unwraps the remarks
    ///
    /// - Parameters:
    ///   - accDetails: accDetails to get the remarks
    ///   - offSetValue: offset value to fix the cursor position
    func wrap_remarks(accDetails: BankAccountModel, offSetValue: Int? = nil) {
        self.contentTxtField.text = accDetails.remarks
        if let offSet = offSetValue {
            if let newPosition = self.contentTxtField.position(from: self.contentTxtField.beginningOfDocument, offset: offSet) {
                self.contentTxtField.selectedTextRange = self.contentTxtField.textRange(from: newPosition, to: newPosition)
            }
        }
        if accDetails.remarks.count > 0 {
            self.contentTxtField.title = "Remarks".localized
        } else {
            self.contentTxtField.placeholder = "Enter Remarks".localized
        }
        self.contentTxtField.isUserInteractionEnabled = true
        self.rowActionBtn.isHidden = false
        self.rowActionBtn.setImage(nil, for: .normal)
        self.accImageView.image = UIImage.init(named : "sm_remark")
        self.showHideContactNumberCloseBtn(isHide: true)
        self.showHideOperatorLabel(isHide: true, accDetail: accDetails)
    }
    
    /// It unwraps the email data
    ///
    /// - Parameters:
    ///   - accDetails: accDetails to get the email data
    ///   - offSetValue: offset value to fix the cursor position
    func wrap_email(accDetails: BankAccountModel, offSetValue: Int? = nil) {
        self.contentTxtField.text = ""
        let emailArray = accDetails.emailid.components(separatedBy: ",")
        if emailArray.count > 0 {
            self.contentTxtField.text = emailArray[0]
        }
        if let offSet = offSetValue {
            if let newPosition = self.contentTxtField.position(from: self.contentTxtField.beginningOfDocument, offset: offSet) {
                self.contentTxtField.selectedTextRange = self.contentTxtField.textRange(from: newPosition, to: newPosition)
            }
        }
        if accDetails.emailid.count > 0 {
            self.contentTxtField.title       = "OK$ Account Holder Email".localized
        } else {
            self.contentTxtField.placeholder = "Enter OK$ Account Holder Email".localized
        }
        self.contentTxtField.isUserInteractionEnabled = true
        self.rowActionBtn.isHidden = false
        self.rowActionBtn.setImage(nil, for: .normal)
        self.accImageView.image = UIImage.init(named : "sm_email")
        self.showHideContactNumberCloseBtn(isHide: true)
        self.showHideOperatorLabel(isHide: true, accDetail: accDetails)
    }
    
    /// It shows or hides the operator label button and fill the operator name
    ///
    /// - Parameters:
    ///   - isHide: boolean value to show or hide
    ///   - accDetail: data from which the operator name is fetched
    func showHideOperatorLabel(isHide: Bool, accDetail: BankAccountModel) {
        self.operatorLabel.text = PayToValidations().getNumberRangeValidation(accDetail.contactMobileNumber).operator
        self.operatorLabel.isHidden = isHide
        self.operatorLblWidthConstraint.constant = isHide ? 0 : 75
    }

    /// It shows or hides the contact number close button
    ///
    /// - Parameter isHide: boolean value to show or hide
    func showHideContactNumberCloseBtn(isHide: Bool) {
        self.contactNumberCloseBtn.isHidden = isHide
        self.contactNumWidthConstraint.constant = isHide ? 0 : 50
    }
    
    /// Getting Operator Name
    ///
    /// - Parameter mobileNumber: mobile number to check the operator
    /// - Returns: return the result in string
    private func getOparatorName(_ mobileNumber: String) -> String {
        guard mobileNumber.count > 0 else {
            return ""
        }
        let operatorArray = ["Select Operator", "Telenor", "Ooredoo", "MPT", "MecTel", "MecTel CDMA", "MPT CDMA(800)", "MPT CDMA(450)"]
        var oparatorName: String
        if mobileNumber.hasPrefix("979") || mobileNumber.hasPrefix("0979") || mobileNumber.hasPrefix("0095979") || mobileNumber.hasPrefix("00950979") || mobileNumber.hasPrefix("978") || mobileNumber.hasPrefix("0978") || mobileNumber.hasPrefix("0095978") || mobileNumber.hasPrefix("00950978") || mobileNumber.hasPrefix("0976") || mobileNumber.hasPrefix("976") || mobileNumber.hasPrefix("00950976") || mobileNumber.hasPrefix("0095976") || mobileNumber.hasPrefix("977") || mobileNumber.hasPrefix("0977") || mobileNumber.hasPrefix("00950977") || mobileNumber.hasPrefix("0095977") {
            oparatorName = operatorArray[1]
            //oparatorName =@"Telenor";
        } else if mobileNumber.hasPrefix("0997") || mobileNumber.hasPrefix("997") || mobileNumber.hasPrefix("00950997") || mobileNumber.hasPrefix("0095997") || mobileNumber.hasPrefix("99") || mobileNumber.hasPrefix("099") || mobileNumber.hasPrefix("0095099") || mobileNumber.hasPrefix("009599") {
            oparatorName = operatorArray[2]
            //oparatorName =@"Oreedoo";
        } else if mobileNumber.hasPrefix("093") || mobileNumber.hasPrefix("93") || mobileNumber.hasPrefix("0095093") || mobileNumber.hasPrefix("009593") {
            oparatorName = operatorArray[4]
            //oparatorName =@"Mectel";
        } else if mobileNumber.hasPrefix("0931") || mobileNumber.hasPrefix("931") || mobileNumber.hasPrefix("0933") || mobileNumber.hasPrefix("933") || mobileNumber.hasPrefix("00950931") || mobileNumber.hasPrefix("0095931") || mobileNumber.hasPrefix("00950933") || mobileNumber.hasPrefix("0095933") {
            oparatorName = operatorArray[5]
            //oparatorName =@"Mectel CDMA";
        } else if mobileNumber.hasPrefix("0973") || mobileNumber.hasPrefix("973") || mobileNumber.hasPrefix("00950973") || mobileNumber.hasPrefix("0095973") || mobileNumber.hasPrefix("980") || mobileNumber.hasPrefix("0980") || mobileNumber.hasPrefix("00950980") || mobileNumber.hasPrefix("0095980") {
            oparatorName = operatorArray[6]
            //oparatorName =@"MPT 800";
        } else if mobileNumber.hasPrefix("0949") || mobileNumber.hasPrefix("949") || mobileNumber.hasPrefix("00950949") || mobileNumber.hasPrefix("0095949") || mobileNumber.hasPrefix("0947") || mobileNumber.hasPrefix("947") || mobileNumber.hasPrefix("0095947") || mobileNumber.hasPrefix("00950947") || mobileNumber.hasPrefix("0986") || mobileNumber.hasPrefix("986") || mobileNumber.hasPrefix("00950986") || mobileNumber.hasPrefix("0095986") {
            oparatorName = operatorArray[7]
            //oparatorName =@"MPT 400";
        } else {
            oparatorName = operatorArray[3]
            //oparatorName =@"MPT ";
        }
        return oparatorName
    }
}


class RestrictedCursorMovementForSMB : SkyFloatingLabelTextField {
    override func closestPosition(to point: CGPoint) -> UITextPosition? {
        let beginning = self.beginningOfDocument
        let end = self.position(from: beginning, offset: self.text?.count ?? 0)
        return end
    }
}
