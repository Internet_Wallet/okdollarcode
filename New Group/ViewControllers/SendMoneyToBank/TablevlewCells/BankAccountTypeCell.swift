//
//  BankAccountTypeCell.swift
//  OK
//  This cell is for the account type of bank
//  Created by Uma Rajendran on 12/23/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BankAccountTypeCell: UITableViewCell {
    // MARK: - Outlet
    @IBOutlet weak var accountTypeNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        accountTypeNameLbl.font           = UIFont.init(name: appFont, size: 15)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    /// It wraps the data
    ///
    /// - Parameter title: title which need to be displayed
    func wrapData(title: String) {
        self.accountTypeNameLbl.text = title
    }
}
