//
//  ExistingBankCell.swift
//  OK
//  This is showing the beneficiary account detail added
//  Created by Uma Rajendran on 12/20/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ExistingBankCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var display_accountHolderName: UILabel!
    @IBOutlet weak var display_passportNumber: UILabel!    
    @IBOutlet weak var display_BankName: UILabel!
    @IBOutlet weak var display_accountType: UILabel!
    @IBOutlet weak var display_accountNumber: UILabel!
    @IBOutlet weak var display_contactNumber: UILabel!
    @IBOutlet weak var display_township: UILabel!
    @IBOutlet weak var display_divisionState: UILabel!
    @IBOutlet weak var display_bankPhNumber: UILabel!
    @IBOutlet weak var display_email: UILabel!
    @IBOutlet weak var display_remarks: UILabel!
    @IBOutlet weak var accountHolderNameContentLbl: UILabel!
    @IBOutlet weak var nrcPassportContentLbl: UILabel!
    @IBOutlet weak var bankNameContentLbl: UILabel!
    @IBOutlet weak var accountTypeContentLbl: UILabel!
    @IBOutlet weak var accountNumberContentLbl: UILabel!
    @IBOutlet weak var contactMobileNumberContentLbl: UILabel!
    @IBOutlet weak var townshipContentLbl: UILabel!
    @IBOutlet weak var divisionStateContentLbl: UILabel!
    @IBOutlet weak var bankPhNumber: UILabel!
    @IBOutlet weak var emailContentLbl: UILabel!
    @IBOutlet weak var remarksContentLbl: UILabel!
    @IBOutlet weak var remarkSeparator: UILabel!
    @IBOutlet weak var advanceMerchantActionView: UIView!
    @IBOutlet weak var advanceMerSendBtn: UIButton!
    @IBOutlet weak var normalUserActionView: UIView!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var constraintTopActionBtn: NSLayoutConstraint!
    @IBOutlet weak var constraintTopEmail: NSLayoutConstraint!
    @IBOutlet weak var separatorLbl: UILabel!
    @IBOutlet weak var bankPhSeparator: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if let myFont = UIFont(name: "Zawgyi-One", size: 15) {
            display_accountHolderName.font              = myFont
            display_passportNumber.font                 = myFont
            display_BankName.font                       = myFont
            display_accountType.font                    = myFont
            display_accountNumber.font                  = myFont
            display_contactNumber.font                  = myFont
            display_bankPhNumber.font                   = myFont
            display_township.font                       = myFont
            display_divisionState.font                  = myFont
            display_email.font                          = myFont
            display_remarks.font                        = myFont
            accountHolderNameContentLbl.font            = myFont
            nrcPassportContentLbl.font                  = myFont
            bankNameContentLbl.font                     = myFont
            accountTypeContentLbl.font                  = myFont
            accountNumberContentLbl.font                = myFont
            contactMobileNumberContentLbl.font          = myFont
            bankPhNumber.font                           = myFont
            townshipContentLbl.font                     = myFont
            divisionStateContentLbl.font                = myFont
            emailContentLbl.font                        = myFont
            remarksContentLbl.font                      = myFont
        }
        
        self.normalUserActionView.backgroundColor      = kYellowColor
        self.advanceMerchantActionView.backgroundColor = kYellowColor
        sendBtn.titleLabel?.font            = UIFont(name: appFont, size: appButtonSize)
        deleteBtn.titleLabel?.font          = UIFont(name: appFont, size: appButtonSize)
        advanceMerSendBtn.titleLabel?.font  = UIFont(name: appFont, size: appButtonSize)
        sendBtn.backgroundColor             = kYellowColor
        deleteBtn.backgroundColor           = kYellowColor
        advanceMerSendBtn.backgroundColor   = kYellowColor
        sendBtn.setTitle("Send".localized, for: .normal)
        deleteBtn.setTitle("Delete".localized, for: .normal)
        advanceMerSendBtn.setTitle("Send".localized, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    /// It wraps the data
    ///
    /// - Parameters:
    ///   - bank: It is the bank data which will be showed
    ///   - isAgent: It is the boolean value of whethere this is agent or not
    func wrap_Data(bank: SendMoneyToBank, isAgent: Bool) {
        let accountTypes = ["CDA" : "Call Deposit Account", "SAV" : "Savings Account", "CUR" : "Current Account", "ACA": "ATM Card Account"]
        display_accountHolderName.text      = bank.isMyAccount ? "OK$ Account Name".localized : "Account Holder Name".localized
        display_passportNumber.text         = bank.isIdProofNRC ? "NRC".localized : "Passport Number".localized
        display_BankName.text               = "Bank Name".localized
        display_accountType.text            = "Account Type".localized
        display_accountNumber.text          = "Account Number".localized
        display_contactNumber.text          = "Contact Mobile Number".localized
        display_bankPhNumber.text           = "Bank Contact Number".localized
        display_township.text               = "Township".localized
        display_divisionState.text          = "Division / State".localized
        display_email.text                  = "Email".localized
        display_remarks.text                = "Remarks".localized
        var bankname        = ""
        var townshipname    = ""
        var statename       = ""
        
        if let curLanguage = UserDefaults.standard.value(forKey: "currentLanguage") as? String {
            switch curLanguage {
            case "en":
                bankname = bank.bankNameEN ?? ""
                statename = bank.stateNameEN ?? ""
                townshipname = bank.townshipNameEN ?? ""
            case "my":
                bankname = bank.bankNameMY ?? ""
                statename = bank.stateNameMY ?? ""
                townshipname = bank.townshipNameMY ?? ""
            default:
                bankname = bank.bankNameEN ?? ""
                statename = bank.stateNameEN ?? ""
                townshipname = bank.townshipNameEN ?? ""
            }
        }
        let acctype         = setLocStr(accountTypes[bank.accountType!]!)
        
        accountHolderNameContentLbl.text        =  bank.accountHolderName
        nrcPassportContentLbl.text              =  bank.accountIDProofNumber
        bankNameContentLbl.text                 =  bankname
        accountTypeContentLbl.text              =  acctype
        accountNumberContentLbl.text            =  bank.accountNumber
        contactMobileNumberContentLbl.text      =  bank.contactMobileNumber
        townshipContentLbl.text                 =  townshipname
        divisionStateContentLbl.text            =  statename
        emailContentLbl.text                    =  bank.emailId
        remarksContentLbl.text                  =  ""
        bankPhNumber.text                       =  ""
        if let bnkPh = bank.bankPhoneNumber, bnkPh.count > 0 {
            bankPhNumber.text =  bank.bankPhoneNumber
            display_bankPhNumber.isHidden = false
            bankPhNumber.isHidden = false
            bankPhSeparator.isHidden = false
            constraintTopEmail.constant = 10
            self.layer.layoutIfNeeded()
        } else {
            display_bankPhNumber.isHidden = true
            bankPhNumber.isHidden = true
            bankPhSeparator.isHidden = true
            constraintTopEmail.constant = -22
            self.layer.layoutIfNeeded()
        }
        if let remarks = bank.remarks, remarks.count > 0 {
            remarksContentLbl.text = remarks
            remarkSeparator.isHidden = false
            display_remarks.isHidden = false
            remarksContentLbl.isHidden = false
            constraintTopActionBtn.constant = 20
            self.layer.layoutIfNeeded()
        } else {
            remarkSeparator.isHidden = true
            display_remarks.isHidden = true
            remarksContentLbl.isHidden = true
            constraintTopActionBtn.constant = 0
            self.layer.layoutIfNeeded()
        }
        self.validate_AdvanceMerchant(isAgent: isAgent)
    }
    
    func validate_AdvanceMerchant(isAgent: Bool) {
        if isAgent {
            advanceMerchantActionView.isHidden      = false
            normalUserActionView.isHidden           = true
        } else {
            advanceMerchantActionView.isHidden      = true
            normalUserActionView.isHidden           = false
        }
    }
}
