//
//  BankBranchCell.swift
//  OK
//  This cell show the bank branch
//  Created by Uma Rajendran on 12/22/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BankBranchCell: UITableViewCell {
    // MARK: - Outlet
    @IBOutlet weak var bankNameLabel    : UILabel!
    @IBOutlet weak var branchNameLabel  : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bankNameLabel.font           = UIFont.init(name: appFont, size: 16)
        branchNameLabel.font         = UIFont.init(name: appFont, size: 15)
        bankNameLabel.textColor      = UIColor.black
        branchNameLabel.textColor    = UIColor.black
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    /// It wraps the data
    ///
    /// - Parameters:
    ///   - bankName: bank name which is going to show
    ///   - branchAddress: branch address which is goind to show
    func wrapData(bankName: String, branchAddress: String) {
        self.bankNameLabel.text     =  bankName
        self.branchNameLabel.text   =  branchAddress
    }
}
