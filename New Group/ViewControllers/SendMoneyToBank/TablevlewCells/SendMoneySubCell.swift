//
//  SendMoneySubCell.swift
//  OK
//  Sub cell of send money
//  Created by Uma Rajendran on 12/19/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class SendMoneySubCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var bankAddressLabel: UILabel!
    @IBOutlet weak var constraintLeadingLabel: NSLayoutConstraint!
    @IBOutlet weak var constraintTopLabel: NSLayoutConstraint!
    @IBOutlet weak var labelSeparator: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        bankAddressLabel.font = UIFont(name: appFont, size: 15)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Methods
    /// It unwraps the data
    func wrapData(accDetails: BankAccountModel) {
        self.bankAddressLabel.text = ""
        if let curLanguage = UserDefaults.standard.value(forKey: "currentLanguage") as? String {
            switch curLanguage {
            case "en":
                getEnglishAddress(accDetails: accDetails)
            case "my":
                getBurmeseAddress(accDetails: accDetails)
            default:
                getEnglishAddress(accDetails: accDetails)
            }
        }
    }
    
    func getEnglishAddress(accDetails: BankAccountModel) {
        var address = ""
        if accDetails.branchAddress.count > 5 {
            address = "\(accDetails.branchName)"
            if accDetails.branchAddress.count > 0 {
                address += ",\n\(accDetails.branchAddress)"
            }
            if accDetails.townshipName.count > 0 {
                address += ",\n\(accDetails.townshipName)"
            }
            if accDetails.stateName.count > 0 {
                address += ", \(accDetails.stateName)"
            }
            if accDetails.bankPhoneNumber.count > 0 {
                address += ",\n\(accDetails.bankPhoneNumber)"
            }
        } else {
            address = "\(accDetails.branchName)"
            if accDetails.townshipName.count > 0 {
                address += ",\n\(accDetails.townshipName)"
            }
            if accDetails.stateName.count > 0 {
                address += ", \(accDetails.stateName)"
            }
            if accDetails.bankPhoneNumber.count > 0 {
                address += ",\n\(accDetails.bankPhoneNumber)"
            }
        }
        self.bankAddressLabel.text = address
        self.bankAddressLabel.textColor = UIColor.black
        if let myFont = UIFont(name: appFont, size: 15) {
            self.bankAddressLabel.font = myFont
        }
        self.constraintLeadingLabel.constant = 13
        self.constraintTopLabel.constant = 20
        //self.labelSeparator.isHidden = true
    }
    
    func getBurmeseAddress(accDetails: BankAccountModel) {
        var address = ""
        if accDetails.branchBurmeseAddress.count > 5 {
            address = "\(accDetails.branchBurmeseName)"
            if accDetails.branchBurmeseAddress.count > 0 {
                address += ",\n\(accDetails.branchBurmeseAddress)"
            }
            if accDetails.townshipBurmeseName.count > 0 {
                address += ",\n\(accDetails.townshipBurmeseName)"
            }
            if accDetails.stateBurmeseName.count > 0 {
                address += ", \(accDetails.stateBurmeseName)"
            }
            if accDetails.bankPhoneNumber.count > 0 {
                address += ",\n\(accDetails.bankPhoneNumber)"
            }
        } else {
            address = "\(accDetails.branchBurmeseName)"
            if accDetails.townshipBurmeseName.count > 0 {
                address += ",\n\(accDetails.townshipBurmeseName)"
            }
            if accDetails.stateBurmeseName.count > 0 {
                address += ", \(accDetails.stateBurmeseName)"
            }
            if accDetails.bankPhoneNumber.count > 0 {
                address += ",\n\(accDetails.bankPhoneNumber)"
            }
        }
        self.bankAddressLabel.text = address
        self.bankAddressLabel.textColor = UIColor.black
        if let myFont = UIFont(name: appFont, size: 15) {
            self.bankAddressLabel.font = myFont
        }
        self.constraintLeadingLabel.constant = 13
        self.constraintTopLabel.constant = 20
        //self.labelSeparator.isHidden = true
    }
    
    func add(address2: String, to address1: String) -> String {
        var finalAddress = address1
        if address2.count > 0 {
            finalAddress += address2
        }
        return finalAddress
    }
}
