//
//  SMBInfoCell.swift
//  OK
//
//  Created by Sam on 29/06/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class SMBInfoCell: UITableViewCell {
    @IBOutlet weak var lblInfo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblInfo.layer.cornerRadius = 5.0
        lblInfo.layer.masksToBounds = true
        lblInfo.layer.borderWidth = 1.5
        lblInfo.layer.borderColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        lblInfo.font = UIFont(name: appFont, size: 14)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
