//
//  ConfirmationCell.swift
//  OK
//  Confirmation cell before we add or send amount to account
//  Created by Uma Rajendran on 1/12/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ConfirmationCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        labelTitle.font =  UIFont(name: appFont, size: 14)
        labelValue.font =  UIFont(name: appFont, size: 14)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    /// It returns the amount in attributed string
    ///
    /// - Parameter amount: amount string to convert
    /// - Returns: returns the attributes string which is specified
    func attributedString(amount: String) -> NSMutableAttributedString? {
        guard let myAmountFont = UIFont(name: appFont, size: 15.0), let mmkFont = UIFont(name: appFont, size: 11.0) else {
            return nil
        }
        let mutableStr = NSMutableAttributedString(string: amount, attributes: [NSAttributedString.Key.font : myAmountFont])
        mutableStr.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], range: NSRange(location: 0, length: amount.count))
        let mmk = " " + "MMK".localized
        let mmkStr = NSMutableAttributedString(string: mmk, attributes: [NSAttributedString.Key.font : mmkFont])
        mmkStr.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], range: NSRange(location: 0, length: mmk.count))
        mutableStr.append(mmkStr)
        return mutableStr
    }
}
