//
//  OtherAccNrcPassportCell.swift
//  OK
//  This passport or nrc cell
//  Created by Uma Rajendran on 12/21/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class OtherAccNrcPassportCell: UITableViewCell {
    
    // MARK: - Properties
    var cellindex       : Int = 0
    var cellname        : BankItems?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
