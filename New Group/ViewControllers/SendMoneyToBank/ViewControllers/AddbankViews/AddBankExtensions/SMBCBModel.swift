//
//  SMBCBModel.swift
//  OK
//
//  Created by Kethan Kumar on 27/02/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation


// MARK: - CBBankValidation
struct CBBankValidation: Codable {
    let code: Int
    let message: String
    let data: DataClass
}

// MARK: - DataClass
struct DataClass: Codable {
    let statusCode: Int
    let status: Bool
    let message: String?
    let result: Result
    enum CodingKeys: String, CodingKey {
        case statusCode = "StatusCode"
        case status = "Status"
        case message = "Message"
        case result = "Result"
    }
}

// MARK: - Result
struct Result: Codable {
    let userDetail: UserDetail?
    let bankAccountNumber, bankPhoneNumber, otp, otpExpiryDate: String?
    let okData: OKData
    enum CodingKeys: String, CodingKey {
        case okData = "OKData"
        case userDetail = "UserDetail"
        case bankAccountNumber = "BankAccountNumber"
        case bankPhoneNumber = "BankPhoneNumber"
        case otp = "Otp"
        case otpExpiryDate = "OtpExpiryDate"
    }
}

// MARK: - OKData
struct OKData: Codable {
    let demoURL: String
    let okCustomercare: String
    let cbMultiplebank: Bool
    enum CodingKeys: String, CodingKey {
        case demoURL = "DemoUrl"
        case okCustomercare = "OKCustomercare"
        case cbMultiplebank = "CBMultiplebank"
    }
}

// MARK: - UserDetail
struct UserDetail: Codable {
    let userDetailID, walletReferenceID, userName, contactPhoneNumber: String
    let nrc, userLevel, status: String
    let transactionLimit, minimumLimit: Int
    let bankAccountNumber, createdDate: String

    enum CodingKeys: String, CodingKey {
        case userDetailID = "UserDetailId"
        case walletReferenceID = "WalletReferenceId"
        case userName = "UserName"
        case contactPhoneNumber = "ContactPhoneNumber"
        case nrc = "Nrc"
        case userLevel = "UserLevel"
        case status = "Status"
        case transactionLimit = "TransactionLimit"
        case minimumLimit = "MinimumLimit"
        case bankAccountNumber = "BankAccountNumber"
        case createdDate = "CreatedDate"
    }
}
