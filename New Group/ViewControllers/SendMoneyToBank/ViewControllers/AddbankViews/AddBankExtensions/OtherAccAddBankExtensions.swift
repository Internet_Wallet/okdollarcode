//
//  OtherAccAddBankExtensions.swift
//  OK
//
//  Created by Uma Rajendran on 1/19/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation
import UIKit

extension OtherAccAddBankViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return accountFieldsList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount : Int = 0
        if section == BankItems.bank_bankname.rawValue {
            rowCount  =  isBranchExpand ? 2 : 1
        } else if section == BankItems.bank_amount.rawValue {
            rowCount  =  2
        } else {
            rowCount  =  1
        }
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let accountDetails = getAccountDetailsDic()
        if indexPath.section == BankItems.bank_bankname.rawValue && indexPath.row == 1 {
            let branchCell = tableView.dequeueReusableCell(withIdentifier: "subcellidentifier", for: indexPath) as! SendMoneySubCell
            branchCell.wrapData(accDetails: accountDetails)
            return branchCell
        } else if indexPath.section == BankItems.bank_amount.rawValue && indexPath.row == 1 {
            let branchCell = tableView.dequeueReusableCell(withIdentifier: "subcellidentifier", for: indexPath) as! SendMoneySubCell
            branchCell.bankAddressLabel.text = "Amount must be (30,000) MMK & Above.".localized
            branchCell.bankAddressLabel.textColor = UIColor.red
            if let myFont = UIFont(name: appFont, size: 12) {
                branchCell.bankAddressLabel.font = myFont
            }
            branchCell.constraintLeadingLabel.constant = 58
            branchCell.constraintTopLabel.constant = 0
            branchCell.labelSeparator.isHidden = true
            return branchCell
        } else {
            if let bankmainCell = otherAcc_TableCells[indexPath.section] as? SendMoneyMainCell {
                bankmainCell.wrap_CellData(accDetails: accountDetails, cellIndex: indexPath.section)
                bankmainCell.rowActionBtn.addTarget(self, action: #selector(cellAction(_:)), for: .touchUpInside)
                bankmainCell.contactNumberCloseBtn.addTarget(self, action: #selector(contactNumberCloseBtnAction(_ :)), for: .touchUpInside)
                return bankmainCell
            } else if let idProofCell = otherAcc_TableCells[indexPath.section] as? OtherAccNrcPassportCell {
                return idProofCell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == BankItems.bank_bankname.rawValue && indexPath.row == 0 {
            let accountDetails = getAccountDetailsDic()
            if !accountDetails.isLoadFromExistingBank {
                self.showBanksView()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if indexPath.section == BankItems.bank_accountidproof.rawValue {
            if let height = self.dHeight {
                return height
            }
            return 0
        } else if indexPath.section == BankItems.bank_bankname.rawValue && indexPath.row == 1 {
            tableView.estimatedRowHeight = 250
            tableView.rowHeight          = UITableView.automaticDimension
            return tableView.rowHeight
            
        } else if indexPath.section == BankItems.bank_amount.rawValue && indexPath.row == 1 {
            return 50
        } else {
            return 70
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.hideContactSuggesionView()
    }
}

extension OtherAccAddBankViewController: UITextFieldDelegate  {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.currentTextField   = textField
        let accountDetails      = self.getAccountDetailsDic()
        
        let textfieldPosition:CGPoint   = textField.superview!.convert(CGPoint.zero, to: self.otherAcc_AddBankTable)
        if let indexPath                = self.otherAcc_AddBankTable.indexPathForRow(at: textfieldPosition) {
            if let cell                 = self.otherAcc_TableCells[indexPath.section] as? SendMoneyMainCell {
                if cell.cellindex == BankItems.bank_amount.rawValue {  // amount field
                    cell.contentTxtField.keyboardType = UIKeyboardType.numberPad
                } else if cell.cellindex == BankItems.bank_accountnumber.rawValue { // account number field
                    if accountDetails.accountType == AccountTypes.atmcardAcc {
                        cell.contentTxtField.keyboardType = UIKeyboardType.numberPad
                    } else {
                        if self.isDiffAccFormat(accDetails: accountDetails) {
                            if accountDetails.accountNumber.count == 0 {
                                cell.contentTxtField.keyboardType = UIKeyboardType.default
                                
                            } else {
                                accNumberByDiffFormat(typingStr: accountDetails.accountNumber, accDetails: accountDetails, cell: cell)
                            }
                            if accountDetails.accountType == AccountTypes.atmcardAcc {
                                cell.contentTxtField.keyboardType = UIKeyboardType.numberPad
                            }
                        } else {
                            cell.contentTxtField.keyboardType = UIKeyboardType.numberPad
                        }
                    }
                } else if cell.cellindex == BankItems.bank_contactnumber.rawValue {  // mobile number field
                    cell.contentTxtField.keyboardType = UIKeyboardType.numberPad
                    
                } else if cell.cellindex == BankItems.bank_emailid.rawValue {  // email id field
                    cell.contentTxtField.keyboardType = UIKeyboardType.emailAddress
                    
                } else {  // for other fields
                    cell.contentTxtField.keyboardType = UIKeyboardType.default
                }
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let textfieldPosition:CGPoint = textField.superview!.convert(CGPoint.zero, to: self.otherAcc_AddBankTable)
        if let indexPath              = self.otherAcc_AddBankTable.indexPathForRow(at: textfieldPosition) {
            if let bankCell           = self.otherAcc_TableCells[indexPath.section] as? SendMoneyMainCell {
                let accountDetails = self.getAccountDetailsDic()
                accountDetails.delegate = self
                if bankCell.cellindex == BankItems.bank_accountholdername.rawValue {
                    var name = accountDetails.accountHolderName
                    name = name.trimmingCharacters(in: .whitespacesAndNewlines)
                        accountDetails.accountHolderName = name
                        bankCell.wrap_accountHolderName(accDetails: accountDetails, offSetValue: nil)
                }
            }
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        println_debug("TextField should begin editing method called")
        return true;
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        println_debug("TextField should clear method called")
        return true;
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        println_debug("TextField should end editing method called")
        return true;
    }
    func isValidChar(string: String, language: String = appDel.currentLanguage) -> Bool {
        var allowedChars = ""
        switch language {
        case "en":
            allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 "
            let cs = NSCharacterSet(charactersIn: allowedChars).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            if string != filtered {
                return false
            }
        case "my":
            allowedChars = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာ၁၂၃၄၅၆၇၈၉၀ "
            let cs = NSCharacterSet(charactersIn: allowedChars).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            if string != filtered {
                return false
            }
        case "th":
            break
        case "zh-Hans":
            break
        default:
            break
        }
        return true
    }
    func isValidCharWithSpecialChar(string: String, language: String = appDel.currentLanguage) -> Bool {
        var allowedChars = ""
        switch language {
        case "en":
            allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 -_!@#$%^&*()+:<>?,./;'\"|][}{`~"
            let cs = NSCharacterSet(charactersIn: allowedChars).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            if string != filtered {
                return false
            }
        case "my":
            allowedChars = "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာ၁၂၃၄၅၆၇၈၉၀ 1234567890-_!@#$%^&*()+:<>?,./;'\"|][}{`~"
            let cs = NSCharacterSet(charactersIn: allowedChars).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            if string != filtered {
                return false
            }
        case "th":
            break
        case "zh-Hans":
            break
        default:
            break
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var text        = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        let textCount   = text.count
        println_debug("typing text :::: \(text)")
//        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
//        let filteredStr = string.components(separatedBy: cs).joined(separator: "")
//        guard string == filteredStr else {
//            return false
//        }
        let textfieldPosition:CGPoint = textField.superview!.convert(CGPoint.zero, to: self.otherAcc_AddBankTable)
        if let indexPath              = self.otherAcc_AddBankTable.indexPathForRow(at: textfieldPosition) {
            if let bankCell           = self.otherAcc_TableCells[indexPath.section] as? SendMoneyMainCell {
                let accountDetails = self.getAccountDetailsDic()
                accountDetails.delegate = self
                if bankCell.cellindex == BankItems.bank_accountholdername.rawValue {
                    func updateAccountName(nameText: String, offSetVal: Int? = nil) -> Bool {
                        if !isValidChar(string: string) {
                            return false
                        }
                        guard nameText.count <= 30 else { return false }
                        accountDetails.accountHolderName = nameText
                        bankCell.wrap_accountHolderName(accDetails: accountDetails, offSetValue: offSetVal)
                      
                        if nameText.count == 3 {
                            if nameText[2] != " " {
                                if !accountFieldsList.contains(.bank_accountidproof) {
                                    insertRow(curRow: .bank_accountholdername)
                                    self.refreshIDProofBaseView()
                                }
                            } else {
                                deleteRow(curRow: .bank_accountholdername)
                                self.refreshNRCDetails(accountDetails: accountDetails)
                            }
                        } else  if nameText.count >= 3 {
                            if !accountFieldsList.contains(.bank_accountidproof) {
                                insertRow(curRow: .bank_accountholdername)
                                self.refreshIDProofBaseView()
                            }
                        } else {
                            if accountFieldsList.contains(.bank_accountidproof) {
                                deleteRow(curRow: .bank_accountholdername)
                                self.refreshNRCDetails(accountDetails: accountDetails)
                            }
                        }
                        if textCount == 3 {
                            if let cell = otherAcc_AddBankTable.cellForRow(at: IndexPath(row: 0, section: 0)) as? SendMoneyMainCell {
                                cell.contentTxtField.becomeFirstResponder()
                            }
                        }
                        return false
                    }
                    guard let textFieldText = textField.text else { return false }
                    if range.lowerBound == 0 && range.upperBound == 0 && string == " " {
                        return false
                    }
                    // Typing letter is backspace
                    if string == "" {
                        //Without Range
                        if range.lowerBound == range.upperBound - 1 {
                            let textArray = Array(textFieldText)
                            if range.lowerBound > 0 && range.lowerBound + 1 <= textArray.count - 1 {
                                if textArray[range.lowerBound - 1] == " " && textArray[range.lowerBound + 1] == " " {
                                    text = text.replacingOccurrences(of: "  ", with: " ")
                                    return updateAccountName(nameText: text, offSetVal: range.upperBound - range.length)
                                }
                            } else {
                                if range.lowerBound + 1 <= textArray.count - 1 {
                                    if textArray[range.lowerBound + 1] == " " {
                                        text = String(text.dropFirst())
                                        return updateAccountName(nameText: text, offSetVal: 0)
                                    }
                                }
                            }
                        }
                            // With Range
                        else {
                            //From first
                            let textArray = Array(textFieldText)
                            if range.lowerBound > 0 && range.upperBound < textArray.count {
                                if textArray[range.lowerBound - 1] == " " && textArray[range.upperBound] == " " {
                                    text = text.replacingOccurrences(of: "  ", with: " ")
                                    return updateAccountName(nameText: text, offSetVal: range.upperBound - range.length)
                                }
                            }
                        }
                    }
                        // Typing letter is not backspace
                    else {
                        // Without range
                        if range.lowerBound == range.upperBound {
                            //Starting - space never happen already condition and if it is normal letter it will written
                            //Middle and Last
                            //Space
                            if string == " " {
                                let textArray = Array(textFieldText)
                                if textArray[textArray.count - 1] == " " {
                                    return false
                                } else {
                                    return true
                                }
                            }
                        }
                            // With Range
                        else {
                            if string == " " {
                                let textArray = Array(textFieldText)
                                // Selected text from start posit
                                // From First
                                if range.lowerBound == 0 {
                                    // Till Last
                                    if textFieldText.count == range.upperBound {
                                        return false
                                    }
                                        // Till Middle
                                    else {
                                        if textArray[range.upperBound] == " " { //trim space and set focus
                                            text = text.replacingOccurrences(of: "  ", with: "")
                                            return updateAccountName(nameText: text, offSetVal: range.upperBound - range.length)
                                        }
                                    }
                                }
                                    // From Middle
                                else {
                                    // Till Last
                                    if textFieldText.count == range.upperBound {
                                        if textArray[range.lowerBound - 1] == " " {
                                            text = text.replacingOccurrences(of: "  ", with: " ")
                                            return updateAccountName(nameText: text, offSetVal: range.upperBound - range.length)
                                        }
                                    }
                                        // Till Middle
                                    else {
                                        if textArray[range.lowerBound - 1] == " " {
                                            text = text.replacingOccurrences(of: "  ", with: " ")
                                            if textArray[range.upperBound] == " " {
                                                text = text.replacingOccurrences(of: "  ", with: "")
                                            }
                                            return updateAccountName(nameText: text, offSetVal: range.upperBound - range.length)
                                        } else if textArray[range.upperBound] == " " {
                                            text = text.replacingOccurrences(of: "  ", with: " ")
                                            return updateAccountName(nameText: text, offSetVal: range.upperBound - range.length + 1)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return updateAccountName(nameText: text, offSetVal: range.upperBound - range.length + 1)
                } else if bankCell.cellindex == BankItems.bank_accountidproof.rawValue {
                    if !isValidChar(string: string, language: "en") {
                        return false
                    }
                    bankCell.contentTxtField.autocapitalizationType = .allCharacters
                } else if bankCell.cellindex == BankItems.bank_amount.rawValue {
                    println_debug("amount field typing....")
                    if self.walletAmount() < 30000 {
                        self.showInsufficientBalanceAlert()
                        return false
                    }
                    let amountVal = text.components(separatedBy: ",").joined(separator: "")
                    guard amountVal.count <= 9 else {
                        return false
                    }
                    guard (amountVal as NSString).doubleValue <= self.walletAmount() else {
                        DispatchQueue.main.async {
                            alertViewObj.wrapAlert(title: nil, body: "Your Entered Amount has exceeded available OK$ Wallet balance".localized, img: #imageLiteral(resourceName: "smToBank"))
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                bankCell.contentTxtField.becomeFirstResponder()
                            })
                            alertViewObj.showAlert(controller: self)
                        }
                        accountDetails.amount = ""
                        bankCell.wrap_amount(accDetails: accountDetails)
                        if accountFieldsList.contains(.bank_bankname) {
                            deleteRow(curRow: .bank_amount)
                        }
                        return false
                    }
                    if amountVal.count < 7 {
                        bankCell.contentTxtField.textColor = UIColor.black
                    } else if amountVal.count == 7 {
                        bankCell.contentTxtField.textColor = UIColor.green
                    } else if amountVal.count > 7 {
                        bankCell.contentTxtField.textColor = UIColor.red
                    }
                    accountDetails.amount  = (amountVal.count > 0) ? self.amountInFormatForValidation(amountVal) : amountVal
                    var offSetVal = range.upperBound - range.length + 1
                    if string == "" {
                        offSetVal = range.upperBound - range.length
                        if textCount == accountDetails.amount.count + 1 {
                            offSetVal -= 1
                        }
                    } else {
                        if textCount == accountDetails.amount.count - 1 {
                            offSetVal += 1
                        }
                    }
                    bankCell.wrap_amount(accDetails: accountDetails, offSetValue: offSetVal)
                    if accountDetails.isLoadFromExistingBank {
                        if (amountVal.count >= 5) &&  (amountVal as NSString).integerValue >= 30000  {
                            if !accountFieldsList.contains(.bank_bankname) {
                                insertRow(curRow: .bank_amount)
                                insertRow(curRow: .bank_bankname)
                                insertRow(curRow: .bank_accounttype)
                                insertRow(curRow: .bank_accountnumber)
                                insertRow(curRow: .bank_contactnumber)
                                insertRow(curRow: .bank_remarks)
                                self.refreshCells(curRow: .bank_amount)
                            }
                        } else {
                            if accountFieldsList.contains(.bank_bankname) {
                                deleteRow(curRow: .bank_amount)
                            }
                        }
                    } else {
                        if (amountVal.count >= 5) &&  (amountVal as NSString).integerValue >= 30000  {
                            if !accountFieldsList.contains(.bank_bankname) {
                                insertRow(curRow: .bank_amount)
                            }
                        } else {
                            if accountFieldsList.contains(.bank_bankname) {
                                deleteRow(curRow: .bank_amount)
                            }
                        }
                    }
                    return false
                } else if bankCell.cellindex  == BankItems.bank_bankname.rawValue {
                    println_debug("bankname field typing....")
                    
                } else if bankCell.cellindex == BankItems.bank_accounttype.rawValue {
                    println_debug("account type field typing....")
                    
                } else if bankCell.cellindex == BankItems.bank_accountnumber.rawValue {
                    guard let encoded = string.cString(using: String.Encoding.utf8) else { return false }
                    let isBackSpace = strcmp(encoded, "\\b")
                    
                    if (isBackSpace == -92) {
                        if textCount < minAcNumCount {
                            if accountFieldsList.contains(.bank_contactnumber) {
                                deleteRow(curRow: .bank_accountnumber)
                            }
                        }
                        return true
                    }
                    
                    if !isValidChar(string: string, language: "en") {
                        return false
                    }
                    bankCell.contentTxtField.autocapitalizationType = .allCharacters
                    println_debug("account number field typing....")
                    guard string != "" else {
                        return false
                    }
                    if accountDetails.accountType == AccountTypes.atmcardAcc {
                        // need to check the account number has min 16 to max 19
                        guard textCount <= AtmAccount.maxAccNumber else {
                            becomeKeypad(row: .bank_contactnumber)
                            return false
                        }
                        accountDetails.accountNumber = text
                        bankCell.wrap_accountNumber(accDetails: accountDetails)
                        
                        // do the validation is , check the account number is similar to expected account number means show the next field
                        if textCount >= AtmAccount.minAccNumber  {
                            if !accountFieldsList.contains(.bank_contactnumber) {
                                insertRow(curRow: .bank_accountnumber)
                            }
                        } else {
                            if accountFieldsList.contains(.bank_contactnumber) {
                                deleteRow(curRow: .bank_accountnumber)
                            }
                        }
                        if textCount == AtmAccount.maxAccNumber {
                            becomeKeypad(row: .bank_contactnumber)
                        }
                        return false
                    } else {
                        if self.isDiffAccFormat(accDetails: accountDetails) {
                            // here we need to do different validation for account number
                            
                            let diffAccFormat: String = bankDiffAccFormatDic[accountDetails.bankId]!
                            guard textCount <= maxAcNumCount else {
                                becomeKeypad(row: .bank_contactnumber)
                                return false
                            }
                            accNumberByDiffFormat(typingStr: text, accDetails: accountDetails, cell: bankCell)
                            if accountDetails.bankId == 9 {
                                // this is myanmar citizens bank ... need to do different validation for this bank.. min 2 digit after hifen max 4 digit will come after hifen
                                // this bank format is c-dd or c-ddd or c-dddd
                                if accountDetails.accountNumber.count >= 4 {
                                    if !accountFieldsList.contains(.bank_contactnumber) {
                                        insertRow(curRow: .bank_accountnumber)
                                    }
                                } else {
                                    if accountFieldsList.contains(.bank_contactnumber) {
                                        deleteRow(curRow: .bank_accountnumber)
                                    }
                                }
                                if accountDetails.accountNumber.count == diffAccFormat.count {
                                    becomeKeypad(row: .bank_contactnumber)
                                }
                            } else {
                                // for other banks have to insert next row when enter last number in account number
                                if accountDetails.accountNumber.count >= minAcNumCount {
                                    if !accountFieldsList.contains(.bank_contactnumber) {
                                        insertRow(curRow: .bank_accountnumber)
                                    }
                                } else {
                                    if accountFieldsList.contains(.bank_contactnumber) {
                                        deleteRow(curRow: .bank_accountnumber)
                                    }
                                }
                                if accountDetails.accountNumber.count == maxAcNumCount  {
                                    becomeKeypad(row: .bank_contactnumber)
                                }
                            }
                            return false
                        } else{
                            let accFormatCount                = accountDetails.multiAccountFormat.count
                            let formattedAccNumber            = frameAccNumberByFormat(typingStr: text, accDetails: accountDetails)
                            let formattedAccNumberCount       = formattedAccNumber.count
                            guard formattedAccNumberCount <= maxAcNumCount else {
                                return false
                            }
                            accountDetails.accountNumber = formattedAccNumber
                            bankCell.wrap_accountNumber(accDetails: accountDetails)
                            if formattedAccNumber.count >= minAcNumCount {
                                if !accountFieldsList.contains(.bank_contactnumber) {
                                    insertRow(curRow: .bank_accountnumber)
                                }
                            } else {
                                if accountFieldsList.contains(.bank_contactnumber) {
                                    deleteRow(curRow: .bank_accountnumber)
                                }
                            }
                            if formattedAccNumber.count == maxAcNumCount {
                                becomeKeypad(row: .bank_contactnumber)
                            }
                            return false
                        }
                    }
                } else if bankCell.cellindex == BankItems.bank_contactnumber.rawValue {
                    println_debug("contact number field typing....")
                    let cs = NSCharacterSet(charactersIn: "0123456789").inverted
                    let filtered = string.components(separatedBy: cs).joined(separator: "")
                    if string != filtered {
                        return false
                    }
                    if textCount == 0 {
                        accountDetails.contactMobileNumber = "09"
                        bankCell.wrap_contactNumber(accDetails: accountDetails)
                        return false
                    }
                    let validCount = validObj.getNumberRangeValidation(text)
                    guard validCount.isRejected == false else {
                        alertViewObj.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: #imageLiteral(resourceName: "Contact Mobile Number"))
                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        })
                        alertViewObj.showAlert(controller: self)
                        accountDetails.contactMobileNumber = "09"
                        bankCell.wrap_contactNumber(accDetails: accountDetails)
                        return false
                    }
                    guard textCount >= 2 else {
                        self.hideContactSuggesionView()
                        return false
                    }
                    
                    println_debug("valid count ::: \(validCount)")
                    
                    guard textCount <= validCount.max else {
                        return false
                    }
                    if textCount >= validCount.max {
                        self.hideContactSuggesionView()
                    }
                    accountDetails.contactMobileNumber = text
                    bankCell.wrap_contactNumber(accDetails: accountDetails)
                   
                    let contactList = self.contactSuggesstion(text)
                    if contactList.count > 0 {
                        // here show the contact suggession screen
                        let rect = self.getSuggessionviewFrame(arr: contactList.count, indexpath: indexPath)
                        self.showContactSuggessionView(withFrame: rect, andContacts: contactList)
                        
                    } else {
                        // here hide the contact suggession screen
                        self.hideContactSuggesionView()
                    }
                    if textCount == validCount.min {
                        self.hideContactSuggesionView()
                        if !accountFieldsList.contains(.bank_remarks) {
                            insertRow(curRow: .bank_contactnumber)
                            insertRow(curRow: .bank_remarks)
                        }
                    }else if textCount < validCount.min {
                        if accountFieldsList.contains(.bank_remarks) {
                            deleteRow(curRow: .bank_contactnumber)
                        }
                    }
                    if textCount == validCount.max {
                        becomeKeypad(row: .bank_remarks)
                    }
                    return false
                } else if bankCell.cellindex == BankItems.bank_remarks.rawValue {
                    func updateRemarks(remarksText: String, offSetVal: Int? = nil) -> Bool {
                        if !isValidCharWithSpecialChar(string: string) {
                            return false
                        }
                        accountDetails.remarks = remarksText
                        bankCell.wrap_remarks(accDetails: accountDetails, offSetValue: offSetVal)
                        return false
                    }
                    guard let textFieldText = textField.text else { return false }
                    if range.lowerBound == 0 && range.upperBound == 0 && string == " " {
                        return false
                    }
                    // Typing letter is backspace
                    if string == "" {
                        //Without Range
                        if range.lowerBound == range.upperBound - 1 {
                            let textArray = Array(textFieldText)
                            if range.lowerBound > 0 && range.lowerBound + 1 <= textArray.count - 1 {
                                if textArray[range.lowerBound - 1] == " " && textArray[range.lowerBound + 1] == " " {
                                    text = text.replacingOccurrences(of: "  ", with: " ")
                                    return updateRemarks(remarksText: text, offSetVal: range.upperBound - range.length)
                                }
                            } else {
                                if range.lowerBound + 1 <= textArray.count - 1 {
                                    if textArray[range.lowerBound + 1] == " " {
                                        text = String(text.dropFirst())
                                        return updateRemarks(remarksText: text, offSetVal: 0)
                                    }
                                }
                            }
                        }
                            // With Range
                        else {
                            //From first
                            let textArray = Array(textFieldText)
                            if range.lowerBound > 0 && range.upperBound < textArray.count {
                                if textArray[range.lowerBound - 1] == " " && textArray[range.upperBound] == " " {
                                    text = text.replacingOccurrences(of: "  ", with: " ")
                                    return updateRemarks(remarksText: text, offSetVal: range.upperBound - range.length)
                                }
                            }
                        }
                    }
                        // Typing letter is not backspace
                    else {
                        // Without range
                        if range.lowerBound == range.upperBound {
                            //Starting - space never happen already condition and if it is normal letter it will written
                            //Middle and Last
                            //Space
                            if string == " " {
                                let textArray = Array(textFieldText)
                                if textArray[textArray.count - 1] == " " {
                                    return false
                                } else {
                                    return true
                                }
                            }
                        }
                            // With Range
                        else {
                            if string == " " {
                                let textArray = Array(textFieldText)
                                // Selected text from start posit
                                // From First
                                if range.lowerBound == 0 {
                                    // Till Last
                                    if textFieldText.count == range.upperBound {
                                        return false
                                    }
                                        // Till Middle
                                    else {
                                        if textArray[range.upperBound] == " " { //trim space and set focus
                                            text = text.replacingOccurrences(of: "  ", with: "")
                                            return updateRemarks(remarksText: text, offSetVal: range.upperBound - range.length)
                                        }
                                    }
                                }
                                    // From Middle
                                else {
                                    // Till Last
                                    if textFieldText.count == range.upperBound {
                                        if textArray[range.lowerBound - 1] == " " {
                                            text = text.replacingOccurrences(of: "  ", with: " ")
                                            return updateRemarks(remarksText: text, offSetVal: range.upperBound - range.length)
                                        }
                                    }
                                        // Till Middle
                                    else {
                                        if textArray[range.lowerBound - 1] == " " {
                                            text = text.replacingOccurrences(of: "  ", with: " ")
                                            if textArray[range.upperBound] == " " {
                                                text = text.replacingOccurrences(of: "  ", with: "")
                                            }
                                            return updateRemarks(remarksText: text, offSetVal: range.upperBound - range.length)
                                        } else if textArray[range.upperBound] == " " {
                                            text = text.replacingOccurrences(of: "  ", with: " ")
                                            return updateRemarks(remarksText: text, offSetVal: range.upperBound - range.length + 1)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return updateRemarks(remarksText: text, offSetVal: range.upperBound - range.length + 1)
                } else if bankCell.cellindex == BankItems.bank_emailid.rawValue {
                    println_debug("email field typing....")
                    let cs = NSCharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890-_!@#$%^&*()+:<>?,./;'\"|][}{`~").inverted
                    let filtered = string.components(separatedBy: cs).joined(separator: "")
                    if string != filtered {
                        return false
                    }
                    accountDetails.emailid = text
                    var offSetVal = range.upperBound - range.length + 1
                    if string == "" {
                        offSetVal = range.upperBound - range.length
                    }
                    bankCell.wrap_email(accDetails: accountDetails, offSetValue: offSetVal)
                    return false
                }
                return true
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        println_debug("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    
    fileprivate func getSuggessionviewFrame(arr: Int, indexpath: IndexPath) -> CGRect {
        
        let rect = self.otherAcc_AddBankTable.rectForRow(at: indexpath)
        var height          : CGFloat = CGFloat(Float(arr) * 70.0)
        let cellHgt         : CGFloat = 70.0
        let normalHeaderHgt : CGFloat = isAdvanceMerchant() ? 80.0 : 40.0
        var keypadHgt: CGFloat = 0.0
        if let hgt = keyboardHeight {
            keypadHgt = hgt
        }
        let maxHeight = screenHeight - keypadHgt - cellHgt - normalHeaderHgt
        if arr > 5 {
            height = maxHeight
        }
        var yPos = screenHeight - rect.origin.y - height - cellHgt
        if yPos < 0 {
            height = height + yPos - 5
            yPos = 5
        }
        return CGRect(x: 50, y: yPos , width: screenWidth - 60, height: height)
    }
    
    func hideContactSuggesionView() {
        if let contactview = contactSuggessionView {
            contactview.view.isHidden = true
        }
    }
    
    func showContactSuggessionView(withFrame frame: CGRect, andContacts contactList: [Dictionary<String, Any>]) {
        contactSuggessionView?.view.isHidden    = false
        contactSuggessionView?.view.frame       = frame
        contactSuggessionView?.contactsList     = contactList
        contactSuggessionView?.contactsTable.reloadData()
        contactSuggessionView?.view.layoutIfNeeded()
    }
}
