//
//  OtherAccAddBankViewController.swift
//  OK
//
//  Created by Uma Rajendran on 12/29/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

struct IDProofDynamicHeight {
    static let basicHeight     : CGFloat   = 200.0
    static let selectedHeight  : CGFloat   = 100.0
    static let existingHeight  : CGFloat   = 70.0
}

class OtherAccAddBankViewController: OKBaseController, BankAccountModelUIUpdates {

    @IBOutlet weak var otherAcc_AddBankTable: UITableView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var tableBottomConstraint: NSLayoutConstraint!
    
    var navc       : UINavigationController?
    var delegate   : SubViewDidSelectActionDelegate?
    
    var otherAcc_TableCells     =   [Any]()
    var accountFieldsList       =   [BankItems]()
    
    var isBranchExpand          =   false
    let validObj                =   PayToValidations()
    var currentTextField        :   UITextField?
    
    var idProofView             : NRCPassportViewController!
    var contactSuggessionView   : ContactSuggestionVC?
    var keyboardHeight : CGFloat?
    
    var minAcNumCount = 0
    var maxAcNumCount = 0
    var cbBankTokenModel: CBBankTokenModel?
    
    var submitView : UIView {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 50))
        let btn = UIButton.init(frame: view.frame)
        btn.backgroundColor = kYellowColor
        btn.setTitle("Submit".localized, for: .normal)
        if let myFont = UIFont(name: appFont, size: appButtonSize) {
            btn.titleLabel?.font =  myFont
        }
        btn.addTarget(self, action: #selector(accessorySubmitBtnAction(_ :)), for: UIControl.Event.touchUpInside)
        view.addSubview(btn)
        return view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.sharedManager().enable            = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
        loadUI()
        loadInitialization()
        updateLocalization()
        validateBottomSubmitBtn()
        loadContactSuggessionView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func customViewDidAppear() {
        self.otherAcc_AddBankTable.reloadData()
        let insertIndexPath = IndexPath(row: 0, section: 0)
        self.otherAcc_AddBankTable.scrollToRow(at: insertIndexPath, at: .top, animated: false)
        self.otherAcc_AddBankTable.layoutIfNeeded()
        validateBottomSubmitBtn()
        self.becomeKeypad(row: .bank_accountholdername)
    }
    
    func loadUI() {
        submitBtn.backgroundColor           = kYellowColor
        submitBtn.setTitleColor(UIColor.white, for: .normal)
    }
    
    func loadInitialization() {
        otherAcc_AddBankTable.register(UINib(nibName: "SendMoneyMainCell", bundle: nil), forCellReuseIdentifier: "maincellidentifier")
        otherAcc_AddBankTable.register(UINib(nibName: "SendMoneySubCell", bundle: nil), forCellReuseIdentifier: "subcellidentifier")
        otherAcc_AddBankTable.register(UINib(nibName: "OtherAccNrcPassportCell", bundle: nil), forCellReuseIdentifier: "nrcpassportcellidentifier")
        otherAcc_AddBankTable.tableFooterView = UIView(frame: CGRect.zero)
        loadTableCells()
        loadBasicCells()
    }
    
    func updateLocalization() {
        submitBtn.setTitle("Submit".localized, for: .normal)
    }
    
    func loadTableCells() {
        for i in 0 ... 8 {
            if i == 1 {
                let cell = self.otherAcc_AddBankTable.dequeueReusableCell(withIdentifier: "nrcpassportcellidentifier") as! OtherAccNrcPassportCell
                cell.cellindex       = i
                cell.cellname        = accountFields_BackUp[i]
                idProofView   = UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "NrcPassportView_ID") as? NRCPassportViewController
                idProofView.delegate = self
                idProofView.view.frame = cell.bounds
                idProofView.view.layoutIfNeeded()
                cell.addSubview(idProofView.view)
                cell.layoutIfNeeded()
                self.setIDProofCellHeight(height: IDProofDynamicHeight.basicHeight)
                otherAcc_TableCells.append(cell)

            } else {
                let cell = self.otherAcc_AddBankTable.dequeueReusableCell(withIdentifier: "maincellidentifier") as! SendMoneyMainCell
                cell.cellindex                                      = i
                cell.cellname                                       = accountFields_BackUp[i]
                cell.contentTxtField.delegate                       = self
                cell.contentTxtField.inputAccessoryView             = self.submitView
                cell.contentTxtField.inputAccessoryView?.isHidden   = true
                otherAcc_TableCells.append(cell)
            }
        }
    }
    
    func loadBasicCells() {
        accountFieldsList.removeAll()
        let accDetails = getAccountDetailsDic()
        if accDetails.isLoadFromExistingBank {
            for i in 0 ... 2 {
                accountFieldsList.append(accountFields_BackUp[i])
                if i == 1 {
                    loadIdProofCellFromExistingBank()
                }
            }
        } else {
            if accountFields_BackUp.count > 0 {
                accountFieldsList.append(accountFields_BackUp.first!)
            }
        }
        otherAcc_AddBankTable.reloadData()
    }
    
    func loadContactSuggessionView() {
        contactSuggessionView  =  UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "ContactSugestView_ID") as? ContactSuggestionVC
        contactSuggessionView?.delegate   = self
        contactSuggessionView?.view.frame = CGRect.zero
        addChild(contactSuggessionView!)
        self.view.addSubview(contactSuggessionView!.view)
        contactSuggessionView!.didMove(toParent: self)
        contactSuggessionView?.view.isHidden = true
    }
    
    // MARK:- Insert and Delete the Cells -///////////////////
    func insertRow(curRow: BankItems) {
        let touchIndex = curRow.rawValue
        let insertRowIndex = touchIndex + 1
        if insertRowIndex <= accountFields_BackUp.count - 1 {
            let inserVal = accountFields_BackUp[insertRowIndex]
            if !accountFieldsList.contains(inserVal) {
                accountFieldsList.append(accountFields_BackUp[insertRowIndex])
                otherAcc_AddBankTable.beginUpdates()
                otherAcc_AddBankTable.insertSections(IndexSet(integer: insertRowIndex), with: .fade)
                otherAcc_AddBankTable.layoutIfNeeded()
                otherAcc_AddBankTable.endUpdates()
                
                if curRow != .bank_accountholdername {
                    otherAcc_AddBankTable.beginUpdates()
                    let insertIndexPath = IndexPath(row: 0, section: insertRowIndex)
                    otherAcc_AddBankTable.scrollToRow(at: insertIndexPath, at: .bottom, animated: true)
                    otherAcc_AddBankTable.layoutIfNeeded()
                    otherAcc_AddBankTable.endUpdates()
                }
                
            }else {
                println_debug("this item already inserted ... but u r trying to insert again ... pl check")
            }
        } else {
            println_debug("selected last index to insert so dont do anything")
        }
    }
    
    func deleteRow(curRow: BankItems)  {
        
        let touchIndex = curRow.rawValue
        println_debug("touch index value is ::: \(touchIndex)")
        println_debug("before remove from items array :::: \(accountFieldsList)")
        
        let fromindextoremove = touchIndex + 1
        if fromindextoremove <= accountFieldsList.count - 1 {
            for eachindex in touchIndex + 1 ... accountFieldsList.count - 1 {
                if accountFieldsList.count - 1 >= 3 {
                    if isBranchExpand {
                        isBranchExpand = false
                        let accDetails = getAccountDetailsDic()
                        accDetails.isBankAddressExpanded = false
                        otherAcc_AddBankTable.reloadSections(IndexSet(integer: 3), with: .fade)
                    }
                }
                
                println_debug("current removing index :::: \(eachindex)")
                accountFieldsList.remove(at: fromindextoremove)
                //                otherAcc_AddBankTable.beginUpdates()
                //                otherAcc_AddBankTable.deleteSections(IndexSet(integer: fromindextoremove), with: .fade)
                //                otherAcc_AddBankTable.endUpdates()
                otherAcc_AddBankTable.reloadData()
                if touchIndex == 0 {
                    if let cell = otherAcc_AddBankTable.cellForRow(at: IndexPath(row: 0, section: 0)) as? SendMoneyMainCell {
                        cell.contentTxtField.becomeFirstResponder()
                    }
                }else if touchIndex == 1 {
                    if let _ = otherAcc_AddBankTable.cellForRow(at: IndexPath(row: 0, section: 1)) as? OtherAccNrcPassportCell {
                        idProofView.idProofNumberField.becomeFirstResponder()
                    }
                }else if touchIndex == 2 {
                    if let cell = otherAcc_AddBankTable.cellForRow(at: IndexPath(row: 0, section: 2)) as? SendMoneyMainCell {
                        cell.contentTxtField.becomeFirstResponder()
                    }
                }
            }
            println_debug("after remove from items array :::: \(accountFieldsList)")
            refreshAccountDetails(curRow: curRow)
        } else {
            println_debug("selected last index so dont do anything")
        }
    }
    
    // MARK:- Show Banks List View -///////////////////
    func showBanksView() {
        if BankViewModel.shared.bankList.count == 0 {
            self.view.endEditing(true)
        }
        self.delegate?.showBanksListView()
    }
    
    // MARK:- Account Type Selection Delegate -/////////////////////
    func didSelectBankAccountType() {
        let accDetails = getAccountDetailsDic()
        guard accDetails.accountType.count > 0 else {
            self.showError(errMsg: "Something went wrong".localized)
            return
        }
        refreshAccountDetails(curRow: .bank_accounttype)
        if !accountFieldsList.contains(.bank_accounttype) {
            
            insertRow(curRow: .bank_bankname)
            insertRow(curRow: .bank_accounttype)
            refreshSingleCell(refreshRow: .bank_bankname)
        } else {
            if accountFieldsList.contains(.bank_contactnumber) {
                deleteRow(curRow: .bank_accountnumber)
            }
            refreshCells(curRow: .bank_bankname)
            
        }
        
        let accountFormate = BankViewModel.shared.selectedBankDB?.multiAccountFormat ?? ""
         let countAcNum = accountFormate.count
         let multiAccountFormat = BankViewModel.shared.selectedBankDB?.multiAccountFormat ?? ""
         if multiAccountFormat.contains(find: ",") {
             let maxCountArr = multiAccountFormat.components(separatedBy: ",")
             if maxCountArr.count > 0 {
                 let array = maxCountArr.sorted { (str1, str2) -> Bool in
                     return str1.count > str2.count
                 }
                 let min = array.last?.count ?? 0
                 let max = array.first?.count ?? 0
                 if min == 0 {
                     minAcNumCount = countAcNum
                 } else {
                     if countAcNum > min {
                         minAcNumCount = min
                     } else {
                         minAcNumCount = countAcNum
                     }
                 }
                 if max == 0 {
                     maxAcNumCount = countAcNum
                 } else {
                     if countAcNum > max {
                         maxAcNumCount = max
                     } else {
                         maxAcNumCount = countAcNum
                     }
                 }
             }
         }else {
             minAcNumCount = countAcNum
             maxAcNumCount = countAcNum
             
         }
        
        
//        let countAcNum = accDetails.accountFormat.count
//        let maxCountArr = accDetails.multiAccountFormat.components(separatedBy: ",")
//        if maxCountArr.count > 0 {
//            let array = maxCountArr.sorted { (str1, str2) -> Bool in
//                return str1.count > str2.count
//            }
//            let min = array.last?.count ?? 0
//            let max = array.first?.count ?? 0
//            if min == 0 {
//                minAcNumCount = countAcNum
//            } else {
//                if countAcNum > min {
//                    minAcNumCount = min
//                } else {
//                    minAcNumCount = countAcNum
//                }
//            }
//            if max == 0 {
//                maxAcNumCount = countAcNum
//            } else {
//                if countAcNum > max {
//                    maxAcNumCount = max
//                } else {
//                    maxAcNumCount = countAcNum
//                }
//            }
//        }
        DispatchQueue.main.async {
            self.becomeKeypad(row: .bank_accountnumber)
        }
    }
    
    func becomeKeypad(row: BankItems) {
        for tableCell in otherAcc_TableCells {
            if let cell = tableCell as? SendMoneyMainCell {
                if cell.cellindex == row.rawValue {
                    self.otherAcc_AddBankTable.beginUpdates()
                    cell.contentTxtField.becomeFirstResponder()
                    self.otherAcc_AddBankTable.layoutIfNeeded()
                    self.otherAcc_AddBankTable.endUpdates()
                    break
                }
            }
        }
    }
    
    func refreshCells(curRow : BankItems) {
        let accDetails = getAccountDetailsDic()
        for index in curRow.rawValue ... otherAcc_TableCells.count - 1 {
            if let cell = otherAcc_TableCells[index] as? SendMoneyMainCell {
                if cell.contentTxtField.text?.count == 0 {
                    continue
                }
                cell.wrap_CellData(accDetails: accDetails, cellIndex: cell.cellindex)
            } else {
                self.refreshIDProofView(accDetails: accDetails)
            }
        }
        otherAcc_AddBankTable.reloadData()
    }
    
    func refreshSingleCell(refreshRow: BankItems) {
        let accDetails = getAccountDetailsDic()
        for tableCell in otherAcc_TableCells {
            if let cell = tableCell as? SendMoneyMainCell {
                if cell.cellindex == refreshRow.rawValue {
                    cell.wrap_CellData(accDetails: accDetails, cellIndex: cell.cellindex)
                    break
                }
            }
        }
    }
    
    func showError(errMsg: String) {
        self.showErrorAlert(errMessage: errMsg)
        self.view.endEditing(true)
    }
    
    // MARK :- Keyboard show and hide
    @objc private func myAcckeyboardWillAppear(notification: Notification) {
        adjustKeyboardShow(true, notification: notification )
    }
    
    @objc private func myAcckeyboardWillDisappear(notification: Notification) {
        self.hideContactSuggesionView()
        adjustKeyboardShow(false, notification: notification)
    }
    
    func adjustKeyboardShow(_ open: Bool, notification: Notification) {
        let userInfo = notification.userInfo ?? [:]
        let keyboardFrame1 = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        if open  {
            keyboardHeight = keyboardFrame1.height
        } else {
            keyboardHeight = CGFloat(0)
        }
        self.validateSubmitBtn()
        self.view.layoutIfNeeded()
        self.otherAcc_AddBankTable.layoutIfNeeded()
    }
    
    // MARK:- Set Notifications for keyboard -//////////////////
    func setKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.myAcckeyboardWillDisappear(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.myAcckeyboardWillAppear(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    func removeKeyboardNotifications() {
//        NotificationCenter.default.removeObserver(self)
        println_debug("removing the notifications from other account  ////////////////////// ")
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)

    }
  
    // MARK :- IDProof View functions
    func didSelectOtherAccNRCCode() {
        let accDetails = getAccountDetailsDic()
        self.idProofView.showNRCView(accDetails: accDetails)
    }

    func refreshIDProofView(accDetails: BankAccountModel) {
        self.idProofView.refreshData(accDetails: accDetails)
    }
    
    func refreshIDProofBaseView() {
        self.idProofView.loadBasicView()
    }
    
    func loadIdProofCellFromExistingBank() {
        let accDetails = getAccountDetailsDic()
        if accDetails.selectedIdProofType == .id_nrc {
            self.idProofView.showNRCView(accDetails: accDetails)
        }else {
            self.idProofView.showPassportView(accDetails: accDetails)
        }
    }
    
    // MARK :- Adjust Idproof view cell height
    func setIDProofCellHeight(height: CGFloat) {
        self.dHeight = height
    }
    
    func adjustIDProofCellHeight(height: CGFloat) {
        self.setIDProofCellHeight(height: height)
        guard let hgt = self.dHeight else {
            return
        }
        
        let accDetails = getAccountDetailsDic()
        if accDetails.isLoadFromExistingBank {
            return
        }
        
        for index in 0 ... otherAcc_TableCells.count - 1 {
            if let cell = otherAcc_TableCells[index] as? OtherAccNrcPassportCell {
                cell.frame = CGRect.init(x: 0, y: 0, width: cell.bounds.width, height: hgt)
                cell.layoutIfNeeded()
                cell.layoutSubviews()
                UIView.setAnimationsEnabled(false)
                self.otherAcc_AddBankTable.beginUpdates()
                self.otherAcc_AddBankTable.layoutIfNeeded()
                self.otherAcc_AddBankTable.layoutSubviews()
                self.otherAcc_AddBankTable.endUpdates()
                UIView.setAnimationsEnabled(true)
                break
            }
        }
    }
    
    // MARK :- Cell Button Actions
    @objc func contactNumberCloseBtnAction(_ sender: UIButton) {
        let accDetails = getAccountDetailsDic()
        let textfieldPosition:CGPoint   = (sender.superview!.convert(CGPoint.zero, to: self.otherAcc_AddBankTable))
        if let indexPath                = self.otherAcc_AddBankTable.indexPathForRow(at: textfieldPosition) {
            if let cell                 = self.otherAcc_TableCells[indexPath.section] as? SendMoneyMainCell {
                if cell.cellindex == BankItems.bank_contactnumber.rawValue {
                    accDetails.contactMobileNumber = "09"
                    cell.wrap_contactNumber(accDetails: accDetails)
                    deleteRow(curRow: BankItems.bank_contactnumber)
                    cell.contentTxtField.becomeFirstResponder()
                }
            }
        }
    }
    
    @objc func cellAction(_ sender: UIButton) {
        println_debug("cell row action btn pressed.....")
        let accDetails = getAccountDetailsDic()
        let textfieldPosition:CGPoint   = (sender.superview!.convert(CGPoint.zero, to: self.otherAcc_AddBankTable))
        if let indexPath                = self.otherAcc_AddBankTable.indexPathForRow(at: textfieldPosition) {
            if let cell                 = self.otherAcc_TableCells[indexPath.section] as? SendMoneyMainCell {
                if cell.cellindex == BankItems.bank_bankname.rawValue {
                    if accountFieldsList.count - 1 >= 3 {
                        isBranchExpand = !isBranchExpand
                        accDetails.isBankAddressExpanded = !accDetails.isBankAddressExpanded
                        otherAcc_AddBankTable.reloadData()
                        let insertIndexPath = IndexPath(row: 0, section: 0)
                        otherAcc_AddBankTable.scrollToRow(at: insertIndexPath, at: .top, animated: true)
                        self.otherAcc_AddBankTable.layoutIfNeeded()
                    }
                } else if cell.cellindex == BankItems.bank_accountnumber.rawValue {
                    accDetails.accountNumber = ""
                    cell.wrap_accountNumber(accDetails: accDetails)
                    deleteRow(curRow: BankItems.bank_accountnumber)
                    if self.isDiffAccFormat(accDetails: accDetails) {
                        if accDetails.accountNumber.count == 0 {
                            cell.contentTxtField.keyboardType = UIKeyboardType.default
                        }
                    } else {
                        cell.contentTxtField.keyboardType = UIKeyboardType.numberPad
                    }
                    if accDetails.accountType == AccountTypes.atmcardAcc {
                        cell.contentTxtField.keyboardType = UIKeyboardType.numberPad
                    }
                    cell.contentTxtField.becomeFirstResponder()
                } else if cell.cellindex == BankItems.bank_contactnumber.rawValue {
                    // here we navigate to contact selection screen
                    let nav =  UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
                    if let nv = self.navc {
                        nv.present(nav, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    func wrapContactNumberFromContactView(_ number: String) {
        let accountDetails = self.getAccountDetailsDic()
        accountDetails.contactMobileNumber = number
        self.refreshSingleCell(refreshRow: .bank_contactnumber)
        if !accountFieldsList.contains(.bank_remarks) {
            insertRow(curRow: .bank_contactnumber)
            insertRow(curRow: .bank_remarks)
            becomeKeypad(row: .bank_remarks)
        }
    }
    
    func wrapContactNumberFromSuggessionView(_ number: String) {
//        DispatchQueue.main.async {
//            let accountDetails = self.getAccountDetailsDic()
//            if number.count > 0 {
//                accountDetails.contactMobileNumber = number
//                //            self.refreshSingleCell(refreshRow: .bank_contactnumber)
//
//                if !self.accountFieldsList.contains(.bank_remarks) {
//                    self.insertRow(curRow: .bank_contactnumber)
//                    self.insertRow(curRow: .bank_remarks)
//                    self.becomeKeypad(row: .bank_remarks)
//                }
//            }else {
//                accountDetails.contactMobileNumber = "09"
//                //            self.refreshSingleCell(refreshRow: .bank_contactnumber)
//
//                if self.accountFieldsList.contains(.bank_remarks) {
//                    self.deleteRow(curRow: .bank_contactnumber)
//                }
//            }
//
//        }
        let accountDetails = self.getAccountDetailsDic()
        if number.count > 0 {
            accountDetails.contactMobileNumber = number
            if !accountFieldsList.contains(.bank_remarks) {
                insertRow(curRow: .bank_contactnumber)
                insertRow(curRow: .bank_remarks)
                self.otherAcc_AddBankTable.reloadData()
                becomeKeypad(row: .bank_remarks)
            }
        } else {
            accountDetails.contactMobileNumber = "09"
            if accountFieldsList.contains(.bank_remarks) {
                deleteRow(curRow: .bank_contactnumber)
            }
        }
    }
    
    // MARK :- Validate Submit Button
    func didChangeBankAccountModel() {
        self.validateSubmitBtn()
    }
    
    func validateSubmitBtn() {
        let accountDetails = self.getAccountDetailsDic()
        var isHide = false
        
        if canShowSendBtn(accDetails: accountDetails, withValidateObj: validObj) {
            isHide = false
            DispatchQueue.main.async {
                self.submitBtn.isHidden = false  // to show the submit button
                self.tableBottomConstraint.constant = 50
            }
        } else {
            isHide = true
            DispatchQueue.main.async {
                self.submitBtn.isHidden = true  // to hide the submit button
                self.tableBottomConstraint.constant = 0
            }
        }
        
        for (index, _) in self.otherAcc_TableCells.enumerated() {
            if let cell = self.otherAcc_TableCells[index] as? SendMoneyMainCell {
                if isHide {
                    DispatchQueue.main.async {
                        cell.contentTxtField.inputAccessoryView?.isHidden = true
                    }
                } else {
                    DispatchQueue.main.async {
                        cell.contentTxtField.inputAccessoryView?.isHidden = false
                    }
                }
            } else {
                self.idProofView.showHideKeyboardAccessoryView(isShow: !isHide)
            }
        }
    }
    
    func validateBottomSubmitBtn() {
        let accountDetails = self.getAccountDetailsDic()
        
        if canShowSendBtn(accDetails: accountDetails, withValidateObj: validObj) {
            DispatchQueue.main.async {
                self.submitBtn.isHidden = false  // to show the submit button
                self.tableBottomConstraint.constant = 50
            }
        } else {
            DispatchQueue.main.async {
                self.submitBtn.isHidden = true  // to hide the submit button
                self.tableBottomConstraint.constant = 0
            }
        }
    }
    
    // MARK :- Submit Button Actions
    @IBAction func bottomSubmitAction(_ sender: Any) {
        self.submitTapped()
    }
    
    @objc func accessorySubmitBtnAction(_ sender: UIButton) {
        println_debug("accessorySubmitBtnAction/////////////////////////")
        self.submitTapped()
    }
    
//    func navigateToUpdateProfile() {
//        DispatchQueue.main.async {
//            let story = UIStoryboard.init(name: "MyProfile", bundle: nil)
//            let vc = story.instantiateViewController(withIdentifier: "UpdateProfile") as! UpdateProfile
//            vc.navigation = self.navigationController
//            let navController = UpdateProfileNavController.init(rootViewController: vc)
//            navController.modalPresentationStyle = .fullScreen
//            self.present(navController, animated: true, completion: nil)
//        }
//    }
    
    func navigateToConfirmationScreen() {
        DispatchQueue.main.async {
            if let vc = UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "BankConfirmation_ViewID") as? BankConfirmationViewController {
                vc.cbBankTokenModel = self.cbBankTokenModel
                self.navc?.pushViewController(vc, animated: true)
            }
        }
    }
    
    fileprivate func nRCMismatchAlert(message: String, number: String = "") {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: nil, body: "\(message.localized) \(number)", img: #imageLiteral(resourceName: "Contact Mobile Number"))
            alertViewObj.addAction(title: "Cancel".localized, style: .cancel , action: {
                
            })
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                self.cbBankTokenModel = nil
                self.navigateToConfirmationScreen()
            })
            alertViewObj.showAlert(controller: self)
        }
    }
    
    fileprivate func showSMBCBOtpVC(validation: CBBankValidation) {
        DispatchQueue.main.async {
            guard let cbPayVC = UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: SMBCBPayOTPViewController.self)) as? SMBCBPayOTPViewController else {
                print("noting")
                return }
            cbPayVC.validation = validation.data.result
            self.navc?.pushViewController(cbPayVC, animated: true)
        }
    }
    
    private func getCBBankParams() -> Dictionary<String, Any> {
        let bankAccountModel = getAccountDetailsDic()
        var params1 = Dictionary<String, Any>()
        let phNum = bankAccountModel.contactMobileNumber.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "")
        let phNo = "0095\(String(phNum.dropFirst()))"
        params1["ContactPhoneNumber"] = phNo
        params1["Nrc"] = UserModel.shared.nrc.replacingOccurrences(of: "@", with: "/")
        params1["UserName"] = bankAccountModel.accountHolderName
        params1["BankAccountNumber"] = bankAccountModel.accountNumber
        params1["UserLevel"]  =  "Level-1"
        params1["WalletReferenceId"]  = UserModel.shared.mobileNo
        return params1
    }
    
    
    func wrapDataModel() -> Data? {
         let bankAccountModel = getAccountDetailsDic()
         let phNum = bankAccountModel.contactMobileNumber.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "")
         let phNo = "0095\(String(phNum.dropFirst()))"
         let nrc = UserModel.shared.nrc.replacingOccurrences(of: "@", with: "/")
         
         let model = BackAssociationModel.init(bankAccountNumber: bankAccountModel.accountNumber, contactPhoneNumber: phNo, nrc: nrc, userLevel: "Level-1", userName: UserModel.shared.name, walletReferenceID: UserModel.shared.mobileNo)
         println_debug(model)
         do {
             let data  =  try JSONEncoder().encode(model)
             println_debug(String(data: data, encoding: .utf8))
             return data
         } catch {
             println_debug(error)
         }
         return Data.init()
     }
    
    private func generateToken() {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlString = String.init(format: Url.cbGenerateToken)
            let urlStr = getUrl(urlStr: urlString, serverType: .CBBankPay)
            
            let TICKS_AT_EPOCH: Int64 = 621355968000000000
            let TICKS_PER_MILLISECOND: Int64 = 10000
            var timeInterval = DateComponents()
            timeInterval.hour = 6
            timeInterval.minute = 30
            let futureDate = Calendar.current.date(byAdding: timeInterval, to: Date())!
            let currentTime = futureDate.millisecondsSince1970
            let getUTCTicks =  (currentTime * TICKS_PER_MILLISECOND) + TICKS_AT_EPOCH
            let aKey = Url.cbBank_aKey + "\(getUTCTicks)"
            do {
                let hashKey = try Encryption.encryptData(plainText: aKey, hexKey: Url.cbBank_sKey)
                let params = ["password": hashKey]
                web.genericClass(url: urlStr, param: params as AnyObject, httpMethod: "POST", mScreen: "CBBankTokenGenerate")
            } catch {
                
            }
        } else {
            println_debug("No Internet")
        }
    }
    
    private func cbBankUpDate(model: CBBankTokenModel) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlString = String.init(format: Url.cbBankAssociation)
            let urlStr = getUrl(urlStr: urlString, serverType: .CBBankPay)
            let data = wrapDataModel()
            println_debug(String(data: data ?? Data(), encoding: .utf8))
            let dic = Dictionary<String,Any>()
            web.genericClass(url: urlStr, param: dic as AnyObject, httpMethod: "POST", mScreen: "CBBankValidation", hbData: data, authToken:model.data?.token , authTokenKey: "X-access-token")
        } else {
            println_debug("No Internet")
        }
    }
    
    
    private func payFromBackend(accountDetails: BankAccountModel) {
        if !canShowSendBtn(accDetails: accountDetails, withValidateObj: validObj) {
            self.showError(errMsg: "Fill All the fields".localized)
            return
        }
        if UserLogin.shared.loginSessionExpired {
            self.view.endEditing(true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                OKPayment.main.authenticate(screenName: "MyBankToConfirmation", delegate: self)
            }
        } else {
            self.navigateToConfirmationScreen()
        }
    }
    
    func submitTapped() {
        let accountDetails = getAccountDetailsDic()
        if (kycAppControlInfo.first?.OkToBankType ?? 1) == 1 {
            self.payFromBackend(accountDetails: accountDetails)
        } else {
            PaymentVerificationManager.getRoutingBankStatus(bankId: Int16(accountDetails.bankId)) { (status, modelArray) in
                if let modelFirst = modelArray?.first {
                    routingModel = modelFirst
                    if modelFirst.routingStatus == "1" {
                        self.generateToken()
                    } else {
                        self.payFromBackend(accountDetails: accountDetails)
                    }
                }
            }
        }
    }
    
}

extension OtherAccAddBankViewController: NRCPassportViewDelegate {
    func adjustCellHeight(height: CGFloat) {
        self.adjustIDProofCellHeight(height: height)
        self.view.endEditing(true)
    }
    
    func insertNextRow() {
        if !accountFieldsList.contains(.bank_amount) {
            insertRow(curRow: .bank_accountidproof)
            let accDetails = getAccountDetailsDic()
            if accDetails.selectedIdProofType == .id_nrc {
                DispatchQueue.main.async {
                    self.becomeKeypad(row: .bank_amount)
                }
            }
        }
    }
    
    func removeNextRow() {
        if accountFieldsList.contains(.bank_amount) {
            deleteRow(curRow: .bank_accountidproof)
        }
    }
    
    func becomeNextRow() {
        DispatchQueue.main.async {
            self.becomeKeypad(row: .bank_amount)
        }
    }
    
    func navigateToNRCListView() {
        if let nav = self.navc {
            guard let vc = UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "NRCDivisionView_ID") as? NRCDivisionViewController else { return }
            nav.pushViewController(vc, animated: true)
        }
    }
    
    func showNRCCodeTypesView() {
        self.view.endEditing(true)
        let vc = UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "NRCThreeCodeTypesView_ID") as? NRCCodeSelectionViewController
        vc?.delegate = self
        vc?.modalPresentationStyle = .overCurrentContext
//        self.present(vc!, animated: true, completion: nil)
        
        DispatchQueue.main.async {
            if let nav = self.navc {
                for controller in nav.viewControllers as Array {
                    if controller.isKind(of: SendMoneyToBankViewController.self) {
                        controller.present(vc!, animated: true, completion: nil)
                        break
                    }
                }
            }
        }
    }
    
    func submitTappedFromAccessoryView() {
        self.submitTapped()
    }
    
    func clearAndMakeContactFieldFocus() {
        let accDetails = getAccountDetailsDic()
        accDetails.contactMobileNumber = "09"
        for cell in otherAcc_TableCells {
            if let safeCell = cell as? SendMoneyMainCell {
                if safeCell.cellindex == BankItems.bank_contactnumber.rawValue {
                    safeCell.wrap_contactNumber(accDetails: accDetails)
                    safeCell.contentTxtField.becomeFirstResponder()
                    break
                }
            }
        }
    }
}

extension OtherAccAddBankViewController: NRCTypesSelectionDelegate {
    func didSelectNRCType(accDetails: BankAccountModel) {
        self.idProofView.refreshData(accDetails: accDetails)
        self.idProofView.showKeyboard()
    }
}

extension OtherAccAddBankViewController: ContactPickerDelegate {
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]) {
        for contact in contacts {
            decodeContact(contact: contact, isMulti: true)
        }
    }
    
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        decodeContact(contact: contact, isMulti: false)
    }
    
    func decodeContact(contact: ContactPicker, isMulti: Bool) {
        let phoneNumberCount = contact.phoneNumbers.count
        if phoneNumberCount >= 1 {
            println_debug(contact.firstName)
            println_debug(contact.phoneNumbers[0].phoneNumber)
            _ = "\(contact.phoneNumbers[0].phoneNumber)"
            let strMobNo = contact.phoneNumbers[0].phoneNumber
            println_debug(strMobNo)
            let trimmed = String(strMobNo.filter { !" ".contains($0) })
            println_debug(trimmed)
            let strContactNumber = self.getConatctNum(trimmed,withCountryCode: "+95")
            println_debug(strContactNumber)
            
            var validNumber = ""
            if strContactNumber.hasPrefix("0095") {
                validNumber = "0\(strContactNumber.substring(from: 4))"
                let validCount = validObj.getNumberRangeValidation(validNumber)
                if validCount.isRejected {
                    alertViewObj.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: #imageLiteral(resourceName: "Contact Mobile Number"))
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        self.clearAndMakeContactFieldFocus()
                    })
                    alertViewObj.showAlert(controller: self)
                } else {
                    if validNumber.count >= validCount.min {
                        self.wrapContactNumberFromContactView(validNumber)
                    } else {
                        alertViewObj.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: #imageLiteral(resourceName: "Contact Mobile Number"))
                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                            self.clearAndMakeContactFieldFocus()
                        })
                        alertViewObj.showAlert(controller: self)
                    }
                }
            } else {
                let alert = UIAlertController(title: nil, message: "Other country number is selected".localized, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK".localized, style: .default, handler: {  action in
                    self.becomeKeypad(row: BankItems.bank_contactnumber)
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
        else {
            _ = ContactGlobalConstants.Strings.phoneNumberNotAvaialable
            self.showError(errMsg: "PhoneNumber Not Available")
        }
    }
}

extension OtherAccAddBankViewController: ContactSuggestionDelegate {
    func didSelectFromContactSuggession(number:  Dictionary<String, Any>) {
        self.hideContactSuggesionView()
        let number = number[ContactSuggessionKeys.phonenumber_backend] as! String
        let validCount = validObj.getNumberRangeValidation(number)
        guard validCount.isRejected == false else{
            alertViewObj.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: #imageLiteral(resourceName: "Contact Mobile Number"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
            self.wrapContactNumberFromSuggessionView("")
            return
        }
        println_debug("valid count ::: \(validCount)")
        
        if number.count >= validCount.min {
            // allow
            self.wrapContactNumberFromSuggessionView(number)
        } else {
            // not allowed
            self.wrapContactNumberFromSuggessionView("")
            alertViewObj.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: #imageLiteral(resourceName: "Contact Mobile Number"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        }
    }
}

extension OtherAccAddBankViewController: BioMetricLoginDelegate {
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if screen == "OtherBankToConfirmation" {
            if isSuccessful{
                navigateToConfirmationScreen()
            }
        }
    }
}


extension OtherAccAddBankViewController: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        if screen == "CBBankValidation" {
            guard let castedData = json as? Data else {
                PTLoader.shared.hide()
                println_debug("Data error")
                return
            }
            do {
                PTLoader.shared.hide()
                let decoder = JSONDecoder()
                let validation = try decoder.decode(CBBankValidation.self, from: castedData)
                println_debug(validation)
                if validation.code == 200 {
                    if validation.data.statusCode == 400 && (validation.data.message ?? "").contains(find: "User already associated with bank account") {
                        self.navigateToConfirmationScreen()
                    } else if validation.data.statusCode == 400 && (validation.data.message ?? "").contains(find: "Bank account is already associated with other OkDallar pay wallet account") {
                        self.nRCMismatchAlert(message: "Bank Account number is already \nassociated with other OK$ Account. \nCan not associate with \nany more OK$ Account. \nIf you wish, you can proceed \nbut it will take 24 banking hours to Credit.\nFor assistant, please call to".localized, number: "\(validation.data.result.okData.okCustomercare)")
                     } else if validation.data.statusCode == 400 && (validation.data.message ?? "").contains(find: "User already associated with other bank account") {
                        if validation.data.result.okData.cbMultiplebank {
                            self.nRCMismatchAlert(message: "This OK$ Account already associated \nwith a CB Bank Account.\nCan not associate \n with any more CB Bank Account.\nIf you wish, you can proceed \nbut it will take 24 banking hours to Credit.\nFor assistant, please call to".localized, number: "\(validation.data.result.okData.okCustomercare)")
                        } else {
                            self.nRCMismatchAlert(message: "OK$ Account number is not match \nwith CB Pay mobile number. \nFor assistant, please call to".localized, number: "\(validation.data.result.okData.okCustomercare)")
                        }
                     } else if validation.data.statusCode == 400 && (validation.data.message ?? "").contains(find: "Customer not using mobile banking") {
                        self.nRCMismatchAlert(message: "Customer not using mobile banking. \nIf you wish, you can proceed \nbut it will take 24 banking hours to Credit.\nFor assistant, please call to".localized, number: "\(validation.data.result.okData.okCustomercare)")
                     } else {
                        if validation.data.statusCode == 200 && validation.data.status == true {
                            DispatchQueue.main.async {
                                self.showSMBCBOtpVC(validation: validation)
                            }
                        } else {
                            self.nRCMismatchAlert(message: "Quick Transfer is not \navailable at this moment. \nIf you wish, you can proceed \nbut it will take 24 banking hours /n to Credit.\nFor assistant, please call to ".localized, number: "\(validation.data.result.okData.okCustomercare)")
                        }
                    }
                } else {
                    self.showError(errMsg: "Something went wrong. Please try again!".localized)
                }
            } catch {
                
            }
        } else if screen == "CBBankTokenGenerate" {
            guard let castedData = json as? Data else {
                PTLoader.shared.hide()
                println_debug("Data error")
                return
            }
            do {
                PTLoader.shared.hide()
                let decoder = JSONDecoder()
                let validation = try decoder.decode(CBBankTokenModel.self, from: castedData)
                cbBankTokenModel = validation
                if validation.code == 200 {
                    self.cbBankUpDate(model: validation)
                } else {
                    showErrorAlert(errMessage: "Please try after some time".localized)
                }
            } catch {
                
            }
            
        }
    }
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        PTLoader.shared.hide()
        if screenName == "CBBankValidation" {
            println_debug("Api Failure in PhNoValidation response")
        }
    }
}
