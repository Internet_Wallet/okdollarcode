//
//  MyAccExistingBankViewController.swift
//  OK
//
//  Created by Uma Rajendran on 12/29/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol MyAccExistingBankDelegate {
    func didSelectSendFrom_ExistingBank()
    func didSelectAddNewBank_MyAcc()
}

class MyAccExistingBankViewController: UIViewController {

    @IBOutlet weak var myAcc_ExistingBankTable: UITableView!
    @IBOutlet weak var addBtn: UIButton!
    
    var navc       : UINavigationController?
    var delegate   : MyAccExistingBankDelegate?
    var myAccBanksList = [SendMoneyToBank]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadInitialization()
        loadUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func customViewDidAppear() {
        self.myAcc_ExistingBankTable.reloadData()
        self.showHideAddBtn()
    }
    
    func loadUI() {
        self.view.setShadowToButton(view: addBtn, color: UIColor.darkGray)
        self.myAcc_ExistingBankTable.backgroundColor = kBackGroundGreyColor
        self.myAcc_ExistingBankTable.register(UINib(nibName: "ExistingBankCell", bundle: nil), forCellReuseIdentifier: "existingbankcellidentifier")
        self.myAcc_ExistingBankTable.rowHeight             = UITableView.automaticDimension
        self.myAcc_ExistingBankTable.estimatedRowHeight    = 440
        self.myAcc_ExistingBankTable.tableFooterView       = UIView(frame: CGRect.zero)
    }
    
    func loadInitialization() {
        if let list = CoreDataHelper.shared.myAcc_BanksFromDB() {
            self.myAccBanksList = list
        }
        self.showHideAddBtn()
    }

    fileprivate func showHideAddBtn() {
        addBtn.isHidden = canShowAddBtn() ? false : true
        if canShowAddBtn() {
            DispatchQueue.main.async {
                self.addBtn.center.x += 100
                UIView.animate(withDuration: 0.5, delay: 0.5,
                               usingSpringWithDamping: 0.3,
                               initialSpringVelocity: 0.5,
                               options: [], animations: {
                                self.addBtn.center.x = screenArea.size.width - 50
                }, completion:{ _ in })
            }
        }
    }
    
    fileprivate func canShowAddBtn() -> Bool {
        if self.isAdvanceMerchant() {
            if CoreDataHelper.shared.myAccBanks_DBCount() >= 2 {
                return false
            } else {
                return true
            }
        } else {
            if CoreDataHelper.shared.myAccBanks_DBCount() >= 4 {
                return false
            } else {
                return true
                
            }
        }
    }
    
    @IBAction func addBtnAction(_ sender: Any) {
        guard self.walletAmount() >= 300 else {
            self.showInsufficientBalanceAlert()
            return
        }
        if let del = self.delegate {
            del.didSelectAddNewBank_MyAcc()
        }
    }
    
    @objc func bankSendBtnAction(_ sender: UIButton) {
        guard self.walletAmount() >= 300 else {
            self.showInsufficientBalanceAlert()
            return
        }
        
        let btnPosition:CGPoint     = (sender.superview!.convert(CGPoint.zero, to: self.myAcc_ExistingBankTable))
        if let indexPath            = self.myAcc_ExistingBankTable.indexPathForRow(at: btnPosition) {
           let bankDetails          = self.myAccBanksList[indexPath.row]
//           BankViewModel.shared.currentBankView  = .myacc_addbank
            BankViewModel.shared.currentAccView = .myacc
            BankViewModel.shared.currentMyAccView = .myacc_addbank
            
           self.wrap_AccountDetails(bank: bankDetails)
            if let del = self.delegate {
                del.didSelectSendFrom_ExistingBank()
            }
        }
    }
    
    @objc func bankDeleteBtnAction(_ sender: UIButton) {
        alertViewObj.wrapAlert(title: nil, body: "Do you want to delete Bank details?".localized, img: #imageLiteral(resourceName: "smToBank"))
        alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
        })
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            guard appDelegate.checkNetworkAvail() else {
                return
            }
            let btnPosition:CGPoint     = (sender.superview!.convert(CGPoint.zero, to: self.myAcc_ExistingBankTable))
            if let indexPath            = self.myAcc_ExistingBankTable.indexPathForRow(at: btnPosition) {
                let bankDetails         = self.myAccBanksList[indexPath.row]
                self.deleteBankApi(bank: bankDetails, completionHandler: { [weak self] (isSuccess, response) in
                    guard isSuccess else {
                        return
                    }
                    DispatchQueue.main.async {
                        self?.showSuccessAlert()
                        if CoreDataHelper.shared.myAccBanks_DBCount() > 0 {
                            if let list = CoreDataHelper.shared.myAcc_BanksFromDB() {
                                self?.myAccBanksList = list
                                self?.myAcc_ExistingBankTable.reloadData()
                                self?.showHideAddBtn()
                            }
                        } else {
                            if let del = self?.delegate {
                                del.didSelectSendFrom_ExistingBank()
                            }
                        }
                    }
                })
            }
        })
        alertViewObj.showAlert(controller: self)
    }
    
    func showSuccessAlert() {
        alertViewObj.wrapAlert(title: nil, body: "Bank Details Deleted Successfully.".localized, img: #imageLiteral(resourceName: "bank_success"))
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        alertViewObj.showAlert(controller: self)
    }
    
    func wrap_AccountDetails(bank: SendMoneyToBank) {
        let accDetail       = self.getAccountDetailsDic()
        accDetail.isLoadFromExistingBank    =   true
        accDetail.accountHolderName         =   bank.accountHolderName!
        accDetail.accountIDProofNumber      =   bank.accountIDProofNumber!
        accDetail.accountFormat             =   bank.accountFormat!
        accDetail.accountNumber             =   bank.accountNumber!
        accDetail.accountType               =   bank.accountType!
        accDetail.amount                    =   ""
        accDetail.branchAddress             =   bank.bankAddressEN!
        accDetail.branchBurmeseAddress      =   bank.bankAddressMY!
        accDetail.bankId                    =   Int(bank.bankId)
        accDetail.bankName                  =   bank.bankNameEN!
        accDetail.bankBurmeseName           =   bank.bankNameMY!
        accDetail.branchId                  =   Int(bank.branchId)
        accDetail.branchName                =   bank.branchNameEN!
        accDetail.branchBurmeseName         =   bank.branchNameMY!
        accDetail.bankPhoneNumber           =   bank.bankPhoneNumber!
        accDetail.bankFaxNumber             =   bank.bankFaxNumber!
        accDetail.contactMobileNumber       =   bank.contactMobileNumber!
        accDetail.emailid                   =   bank.emailId!
        accDetail.isBankActive              =   bank.isBankActive
        accDetail.isMyAccount               =   true
        accDetail.remarks                   =   bank.remarks!
        accDetail.serverBankId              =   bank.serverBankId!
        accDetail.stateCode                 =   bank.stateCode!
        accDetail.stateName                 =   bank.stateNameEN!
        accDetail.stateBurmeseName          =   bank.stateNameMY!
        accDetail.townshipCode              =   bank.townshipCode!
        accDetail.townshipName              =   bank.townshipNameEN!
        accDetail.townshipBurmeseName       =   bank.townshipNameMY!
        accDetail.selectedIdProofType       =   bank.isIdProofNRC ? .id_nrc : .id_passport
        accDetail.bankBurmeseNameUnicode    =   bank.bankNameUN!
        accDetail.branchBurmeseNameUnicode  =   bank.branchNameUN ?? ""
        accDetail.branchBurmeseAddressUnicode = bank.bankAddressUN ?? ""
        accDetail.townshipBurmeseNameUnicode = bank.townshipNameUN ?? ""
        accDetail.stateBurmeseNameUnicode   = bank.stateNameUN ?? ""
    }
}

extension MyAccExistingBankViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        println_debug(myAccBanksList.count)
        return self.myAccBanksList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sendMoney   = self.myAccBanksList[indexPath.row]
        let cell        =  tableView.dequeueReusableCell(withIdentifier: "existingbankcellidentifier", for: indexPath) as! ExistingBankCell
        cell.wrap_Data(bank: sendMoney, isAgent: self.isAdvanceMerchant())
        cell.sendBtn.addTarget(self, action: #selector(bankSendBtnAction(_:)), for: .touchUpInside)
        cell.deleteBtn.addTarget(self, action: #selector(bankDeleteBtnAction(_:)), for: .touchUpInside)
        cell.advanceMerSendBtn.addTarget(self, action: #selector(bankSendBtnAction(_ :)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight    = 440
        tableView.rowHeight             = UITableView.automaticDimension
        return tableView.rowHeight
    }
}
