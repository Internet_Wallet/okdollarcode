//
//  MyAccAddBankViewController.swift
//  OK
//
//  Created by Uma Rajendran on 12/29/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Contacts

var routingModel: BankRoutingStatus?

class MyAccAddBankViewController: OKBaseController , BankAccountModelUIUpdates {

    @IBOutlet weak var myAcc_AddBankTable: UITableView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var tableBottomConstraint: NSLayoutConstraint!
    
    var myAcc_TableCells = [SendMoneyMainCell]()
    var accountFieldsList = [BankItems]()
    
    var isBranchExpand =   false
    var navc:   UINavigationController?
    var delegate:   SubViewDidSelectActionDelegate?
    let validObj = PayToValidations()
    var currentTextField: UITextField?
    
    var contactSuggessionView: ContactSuggestionVC?
    var keyboardHeight: CGFloat?
    
    var cbBankTokenModel: CBBankTokenModel?
    var isSuggestionShow = true
    var submitView: UIView {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 50))
        let btn = UIButton.init(frame: view.frame)
        btn.backgroundColor = kYellowColor
        btn.setTitle("Submit".localized, for: .normal)
        if let myFont = UIFont(name: appFont, size: appButtonSize) {
            btn.titleLabel?.font =  myFont
        }
        btn.addTarget(self, action: #selector(accessorySubmitBtnAction(_ :)), for: UIControl.Event.touchUpInside)
        view.addSubview(btn)
        return view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.sharedManager().enable            = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        loadInitialization()
        loadUI()
        updateLocalization()
        validateBottomSubmitBtn()
        loadContactSuggessionView()
        getAllContacts()
    }
    
    func customViewDidAppear() {
        self.myAcc_AddBankTable.reloadData()
        let insertIndexPath = IndexPath(row: 0, section: 0)
        myAcc_AddBankTable.scrollToRow(at: insertIndexPath, at: .top, animated: false)
        self.myAcc_AddBankTable.layoutIfNeeded()
        validateBottomSubmitBtn()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadUI() {
        submitBtn.backgroundColor           = kYellowColor
        submitBtn.setTitleColor(UIColor.white, for: .normal)
    }
    
    func loadInitialization() {
        myAcc_AddBankTable.register(UINib(nibName: "SendMoneyMainCell", bundle: nil), forCellReuseIdentifier: "maincellidentifier")
        myAcc_AddBankTable.register(UINib(nibName: "SendMoneySubCell", bundle: nil), forCellReuseIdentifier: "subcellidentifier")
        myAcc_AddBankTable.tableFooterView = UIView(frame: CGRect.zero)
        
        DispatchQueue.main.async {
            self.loadTableCells()
            self.loadBasicCells()
        }
    }
    
    func updateLocalization() {
        submitBtn.setTitle("Submit".localized, for: .normal)
    }
    
    func loadTableCells() {
        for i in 0 ... 8 {
            let cell = self.myAcc_AddBankTable.dequeueReusableCell(withIdentifier: "maincellidentifier") as! SendMoneyMainCell
            cell.cellindex                                      =  i
            cell.cellname                                       =  accountFields_BackUp[i]
            cell.contentTxtField.delegate                       =  self
            cell.contentTxtField.inputAccessoryView             =  self.submitView
            cell.contentTxtField.inputAccessoryView?.isHidden   =  true
            myAcc_TableCells.append(cell)
        }
    }
    
    func loadBasicCells() {
        accountFieldsList.removeAll()
        for i in 0 ... 2 {
            accountFieldsList.append(accountFields_BackUp[i])
        }
        myAcc_AddBankTable.reloadData()
        if let cell = self.myAcc_AddBankTable.cellForRow(at: IndexPath(row: 0, section: 2)) as? SendMoneyMainCell {
            if cell.cellname == BankItems.bank_amount {
                cell.contentTxtField.becomeFirstResponder()
            }
        }
    }
    
    func loadContactSuggessionView() {
        contactSuggessionView  =  UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "ContactSugestView_ID") as? ContactSuggestionVC
        contactSuggessionView?.delegate   = self
        contactSuggessionView?.view.frame = CGRect.zero
        addChild(contactSuggessionView!)
        self.view.addSubview(contactSuggessionView!.view)
        contactSuggessionView!.didMove(toParent: self)
        contactSuggessionView?.view.isHidden = true
    }
    
    func insertRow(curRow: BankItems) {
        let touchIndex = curRow.rawValue
        let insertRowIndex = touchIndex + 1
        if insertRowIndex <= accountFields_BackUp.count - 1 {
            let inserVal = accountFields_BackUp[insertRowIndex]
            if !accountFieldsList.contains(inserVal) {
                accountFieldsList.append(accountFields_BackUp[insertRowIndex])
                myAcc_AddBankTable.beginUpdates()
                myAcc_AddBankTable.insertSections(IndexSet(integer: insertRowIndex), with: .fade)
                myAcc_AddBankTable.layoutIfNeeded()
                myAcc_AddBankTable.endUpdates()
                
                myAcc_AddBankTable.beginUpdates()
                let insertIndexPath = IndexPath(row: 0, section: insertRowIndex)
                myAcc_AddBankTable.scrollToRow(at: insertIndexPath, at: .bottom, animated: true)
                myAcc_AddBankTable.layoutIfNeeded()
                myAcc_AddBankTable.endUpdates()
            } else {
                println_debug("this item already inserted ... but u r trying to insert again ... pl check")
            }
        } else {
            println_debug("selected last index to insert so dont do anything")
        }
    }
    
    func deleteRow(curRow: BankItems)  {
        let touchIndex = curRow.rawValue
        println_debug("touch index value is ::: \(touchIndex)")
        println_debug("before remove from items array :::: \(accountFieldsList)")
        
        let fromindextoremove = touchIndex + 1
        if fromindextoremove <= accountFieldsList.count - 1 {
            for eachindex in touchIndex + 1 ... accountFieldsList.count - 1 {
                if accountFieldsList.count - 1 >= 3 {
                    if isBranchExpand {
                        isBranchExpand = false
                        let accDetails = getAccountDetailsDic()
                        accDetails.isBankAddressExpanded = false
                        myAcc_AddBankTable.reloadSections(IndexSet(integer: 3), with: .fade)
                    }
                }
                println_debug("current removing index :::: \(eachindex)")
                accountFieldsList.remove(at: fromindextoremove)
                myAcc_AddBankTable.beginUpdates()
                myAcc_AddBankTable.deleteSections(IndexSet(integer: fromindextoremove), with: .fade)
                myAcc_AddBankTable.endUpdates()
            }
            println_debug("after remove from items array :::: \(accountFieldsList)")
            refreshAccountDetails(curRow: curRow)
        } else {
            println_debug("selected last index so dont do anything")
        }
    }
    
    func showBanksView() {
        if BankViewModel.shared.bankList.count == 0 {
            self.view.endEditing(true)
        }
        self.delegate?.showBanksListView()
    }
    
    func didSelectBankAccountType() {
        let accDetails = getAccountDetailsDic()
        guard accDetails.accountType.count > 0 else{
            self.showError(errMsg: "Something went wrong".localized)
            return
        }
        refreshAccountDetails(curRow: .bank_accounttype)
        if !accountFieldsList.contains(.bank_accounttype) {
            insertRow(curRow: .bank_bankname)
            insertRow(curRow: .bank_accounttype)
            refreshSingleCell(refreshRow: .bank_bankname)
        } else {
            if accountFieldsList.contains(.bank_contactnumber) {
                deleteRow(curRow: .bank_accountnumber)
            }
            refreshCells(curRow: .bank_bankname)
        }
    
        // My New code for check bank validation
        let accountFormate = BankViewModel.shared.selectedBankDB?.multiAccountFormat ?? ""
        let countAcNum = accountFormate.count
        let multiAccountFormat = BankViewModel.shared.selectedBankDB?.multiAccountFormat ?? ""
        if multiAccountFormat.contains(find: ",") {
            let maxCountArr = multiAccountFormat.components(separatedBy: ",")
            if maxCountArr.count > 0 {
                let array = maxCountArr.sorted { (str1, str2) -> Bool in
                    return str1.count > str2.count
                }
                let min = array.last?.count ?? 0
                let max = array.first?.count ?? 0
                if min == 0 {
                    minAcNumCount = countAcNum
                } else {
                    if countAcNum > min {
                        minAcNumCount = min
                    } else {
                        minAcNumCount = countAcNum
                    }
                }
                if max == 0 {
                    maxAcNumCount = countAcNum
                } else {
                    if countAcNum > max {
                        maxAcNumCount = max
                    } else {
                        maxAcNumCount = countAcNum
                    }
                }
            }
        }else {
            minAcNumCount = countAcNum
            maxAcNumCount = countAcNum
            
        }
        
       //  let countAcNum = accDetails.accountFormat.count
//        let maxCountArr = accDetails.multiAccountFormat.components(separatedBy: ",")
//        if maxCountArr.count > 0 {
//            let array = maxCountArr.sorted { (str1, str2) -> Bool in
//                return str1.count > str2.count
//            }
//            let min = array.last?.count ?? 0
//            let max = array.first?.count ?? 0
//            if min == 0 {
//                minAcNumCount = countAcNum
//            } else {
//                if countAcNum > min {
//                    minAcNumCount = min
//                } else {
//                    minAcNumCount = countAcNum
//                }
//            }
//            if max == 0 {
//                maxAcNumCount = countAcNum
//            } else {
//                if countAcNum > max {
//                    maxAcNumCount = max
//                } else {
//                    maxAcNumCount = countAcNum
//                }
//            }
//        }
        println_debug("multiAccountFormat: \(accDetails.multiAccountFormat)")
        println_debug("accountFormat: \(accDetails.accountFormat)")
        println_debug("Min Account No: \(minAcNumCount)")
        println_debug("Max Account No: \(maxAcNumCount)")
        DispatchQueue.main.async {
            self.becomeKeypad(row: .bank_accountnumber)
        }
    }
    
    func becomeKeypad(row: BankItems) {
        for cell in myAcc_TableCells {
            if cell.cellindex == row.rawValue {
                cell.contentTxtField.becomeFirstResponder()
                break
            }
        }
    }
    
    func refreshCells(curRow : BankItems) {
        let accDetails = getAccountDetailsDic()
        for index in curRow.rawValue ... myAcc_TableCells.count - 1 {
            let cell = myAcc_TableCells[index]
            if cell.contentTxtField.text?.count == 0 {
                continue
            }
            cell.wrap_CellData(accDetails: accDetails, cellIndex: cell.cellindex)
        }
        myAcc_AddBankTable.reloadData()
    }
    
    func refreshSingleCell(refreshRow: BankItems) {
        let accDetails = getAccountDetailsDic()
        for cell in myAcc_TableCells {
            if cell.cellindex == refreshRow.rawValue {
                cell.wrap_CellData(accDetails: accDetails, cellIndex: cell.cellindex)
                break
            }
        }
    }
    
    func showError(errMsg: String) {
        self.showErrorAlert(errMessage: errMsg)
        self.view.endEditing(true)
    }
    
    @objc func cellAction(_ sender: UIButton) {
        println_debug("cell row action btn pressed.....")
        let accDetails = getAccountDetailsDic()
    
        let textfieldPosition: CGPoint   = (sender.superview!.convert(CGPoint.zero, to: self.myAcc_AddBankTable))
        if let indexPath                = self.myAcc_AddBankTable.indexPathForRow(at: textfieldPosition) {
            let cell                    = self.myAcc_TableCells[indexPath.section] as SendMoneyMainCell
            if cell.cellindex == BankItems.bank_bankname.rawValue {
                if accountFieldsList.count - 1 >= 3 {
                    isBranchExpand = !isBranchExpand
                    accDetails.isBankAddressExpanded = !accDetails.isBankAddressExpanded
                    myAcc_AddBankTable.reloadData()
              
                    let insertIndexPath = IndexPath(row: 0, section: 0)
                    myAcc_AddBankTable.scrollToRow(at: insertIndexPath, at: .top, animated: true)
                    self.myAcc_AddBankTable.layoutIfNeeded()
                }
                
            } else if cell.cellindex == BankItems.bank_accountnumber.rawValue {
                accDetails.accountNumber = ""
                cell.wrap_accountNumber(accDetails: accDetails)
                deleteRow(curRow: BankItems.bank_accountnumber)
                if self.isDiffAccFormat(accDetails: accDetails) {
                    if accDetails.accountNumber.count == 0 {
                        cell.contentTxtField.keyboardType = UIKeyboardType.default
                    }
                } else {
                    cell.contentTxtField.keyboardType = UIKeyboardType.numberPad
                }
                if accDetails.accountType == AccountTypes.atmcardAcc {
                    cell.contentTxtField.keyboardType = UIKeyboardType.numberPad
                }
                cell.contentTxtField.becomeFirstResponder()
            } else if cell.cellindex == BankItems.bank_contactnumber.rawValue {
                // here we navigate to contact selection screen
                let nav =  UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
                if let nv = self.navc {
                    nv.present(nav, animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc func contactNumberCloseBtnAction(_ sender: UIButton) {
        self.hideContactSuggesionView()
        let accDetails = getAccountDetailsDic()
        let textfieldPosition:CGPoint   = (sender.superview!.convert(CGPoint.zero, to: self.myAcc_AddBankTable))
        if let indexPath                = self.myAcc_AddBankTable.indexPathForRow(at: textfieldPosition) {
            let cell                    = self.myAcc_TableCells[indexPath.section] as SendMoneyMainCell
            if cell.cellindex == BankItems.bank_contactnumber.rawValue {
                accDetails.contactMobileNumber = "09"
                cell.wrap_contactNumber(accDetails: accDetails)
                deleteRow(curRow: BankItems.bank_contactnumber)
                cell.contentTxtField.becomeFirstResponder()
            }
        }
    }
    
    func wrapContactNumberFromContactView(_ number: String) {
        let accountDetails = self.getAccountDetailsDic()
        accountDetails.contactMobileNumber = number
        self.refreshSingleCell(refreshRow: .bank_contactnumber)
        if !accountFieldsList.contains(.bank_remarks) {
            insertRow(curRow: .bank_contactnumber)
            insertRow(curRow: .bank_remarks)
            becomeKeypad(row: .bank_remarks)
        } else {
            accountDetails.remarks = ""
            self.refreshSingleCell(refreshRow: .bank_remarks)
        }
    }
    
    func wrapContactNumberFromSuggessionView(_ number: String) {
        let accountDetails = self.getAccountDetailsDic()
        if number.count > 0 {
            accountDetails.contactMobileNumber = number
            if !accountFieldsList.contains(.bank_remarks) {
                insertRow(curRow: .bank_contactnumber)
                insertRow(curRow: .bank_remarks)
                becomeKeypad(row: .bank_remarks)
            }
        } else {
            accountDetails.contactMobileNumber = "09"
            if accountFieldsList.contains(.bank_remarks) {
                deleteRow(curRow: .bank_contactnumber)
            }
        }
        self.refreshSingleCell(refreshRow: .bank_contactnumber)
    }
    
    // MARK :- Keyboard show and hide
    @objc private func otherAcckeyboardWillAppear(notification: Notification) {
        adjustKeyboardShow(true, notification: notification )
    }
    
    @objc private func OtherAcckeyboardWillDisappear(notification: Notification) {
        self.hideContactSuggesionView()
        adjustKeyboardShow(false, notification: notification)
    }
    
    func adjustKeyboardShow(_ open: Bool, notification: Notification) {
        let userInfo = notification.userInfo ?? [:]
        let keyboardFrame1 = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        if open  {
            keyboardHeight = keyboardFrame1.height
        } else {
            keyboardHeight = CGFloat(0)
        }
        //        let height1 = (keyboardFrame1.height ) * (open ? 1 : 0)
        self.validateSubmitBtn()
        self.view.layoutIfNeeded()
        self.myAcc_AddBankTable.layoutIfNeeded()
    }
 
    // MARK:- Set Notifications for keyboard
    func setKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.OtherAcckeyboardWillDisappear(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.otherAcckeyboardWillAppear(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    func removeKeyboardNotifications() {
//        NotificationCenter.default.removeObserver(self)
        println_debug("removing the notifications from my account  ////////////////////// ")
        println_debug(self)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)

    }
    
    // MARK:- Submit Action
    @IBAction func submitAction(_ sender: Any) {
        self.submitTapped()
    }
    
    @objc func accessorySubmitBtnAction(_ sender: UIButton) {
        println_debug("accessorySubmitBtnAction/////////////////////////")
        self.submitTapped()
    }
    
//    func navigateToUpdateProfile() {
//        DispatchQueue.main.async {
//            let story = UIStoryboard.init(name: "MyProfile", bundle: nil)
//            let vc = story.instantiateViewController(withIdentifier: "UpdateProfile") as! UpdateProfile
//            vc.navigation = self.navigationController
//            let navController = UpdateProfileNavController.init(rootViewController: vc)
//            navController.modalPresentationStyle = .fullScreen
//            self.present(navController, animated: true, completion: nil)
//        }
//    }
    
    func navigateToConfirmationScreen() {
        DispatchQueue.main.async {
            if let vc = UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "BankConfirmation_ViewID") as? BankConfirmationViewController {
                vc.cbBankTokenModel = self.cbBankTokenModel
                self.navc?.pushViewController(vc, animated: true)
            }
        }
    }
    
    fileprivate func nRCMismatchAlert(message: String, number: String = "") {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: nil, body: "\(message.localized) \(number)", img: #imageLiteral(resourceName: "smToBank"))
            alertViewObj.addAction(title: "Cancel".localized, style: .cancel , action: {
                
            })
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                self.cbBankTokenModel = nil
                self.navigateToConfirmationScreen()
            })
            alertViewObj.showAlert(controller: self)
        }
    }
    
    fileprivate func showSMBCBOtpVC(validation: CBBankValidation) {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: nil, body: "If you have CB Pay account \nClick \"Quick\" to proceed \nwith \"OTP\" ( One Time Password ).\nTo link CB & OK$ Account \nyou must enter \"OTP\" \nin CB Pay App \n (OR)\nClick \"24 hour\" to transfer normally \nwhich will take 24 banking hours \nto Credit.".localized, img: #imageLiteral(resourceName: "smToBank"))
            
            alertViewObj.addAction(title: "24 hours".localized, style: .cancel , action: {
                self.cbBankTokenModel = nil
                self.navigateToConfirmationScreen()
            })
            alertViewObj.addAction(title: "Quick".localized, style: .target , action: {
                guard let cbPayVC = UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: SMBCBPayOTPViewController.self)) as? SMBCBPayOTPViewController else {
                    print("noting")
                    return }
                cbPayVC.validation = validation.data.result
                self.navc?.pushViewController(cbPayVC, animated: true)
            })
            alertViewObj.showAlert(controller: self)
        }
    }
    
    private func getCBBankParams() -> Dictionary<String, Any> {
        let bankAccountModel = getAccountDetailsDic()
        var params1 = Dictionary<String, Any>()
        let phNum = bankAccountModel.contactMobileNumber.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "")
        let phNo = "0095\(String(phNum.dropFirst()))"
        params1["ContactPhoneNumber"] = phNo
        params1["Nrc"] = UserModel.shared.nrc.replacingOccurrences(of: "@", with: "/")
        params1["UserName"] = bankAccountModel.accountHolderName
        params1["BankAccountNumber"] = bankAccountModel.accountNumber
        params1["UserLevel"]  =  "Level-1"
        params1["WalletReferenceId"]  = UserModel.shared.mobileNo
        return params1
    }
    
    func wrapDataModel() -> Data? {
        let bankAccountModel = getAccountDetailsDic()
        let phNum = bankAccountModel.contactMobileNumber.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "")
        let phNo = "0095\(String(phNum.dropFirst()))"
        let nrc = UserModel.shared.nrc //"၁၂@ဥကတ(ႏိုင္)၁၅၆၃၆၃".replacingOccurrences(of: "@", with: "/")
        let model = BackAssociationModel.init(bankAccountNumber: bankAccountModel.accountNumber, contactPhoneNumber: phNo, nrc: nrc, userLevel: "Level-1", userName: UserModel.shared.name, walletReferenceID: UserModel.shared.mobileNo)
        println_debug(model)
        do {
            let data  =  try JSONEncoder().encode(model)
            println_debug(String(data: data, encoding: .utf8))
            return data
        } catch {
            println_debug(error)
        }
        return Data.init()
    }
    
    
    private func generateToken() {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            
            let urlString = String.init(format: Url.cbGenerateToken)
            let urlStr = getUrl(urlStr: urlString, serverType: .CBBankPay)
            
            let TICKS_AT_EPOCH: Int64 = 621355968000000000
            let TICKS_PER_MILLISECOND: Int64 = 10000
            var timeInterval = DateComponents()
            timeInterval.hour = 6
            timeInterval.minute = 30
            let futureDate = Calendar.current.date(byAdding: timeInterval, to: Date())!
            let currentTime = futureDate.millisecondsSince1970
            let getUTCTicks =  (currentTime * TICKS_PER_MILLISECOND) + TICKS_AT_EPOCH
            let aKey = Url.cbBank_aKey + "\(getUTCTicks)"
            do {
                let hashKey = try Encryption.encryptData(plainText: aKey, hexKey: Url.cbBank_sKey)
                let params = ["password": hashKey]
                web.genericClass(url: urlStr, param: params as AnyObject, httpMethod: "POST", mScreen: "CBBankTokenGenerate")
            } catch {
                
            }
        } else {
            println_debug("No Internet")
        }
    }
    
    private func cbBankUpDate(model: CBBankTokenModel) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            
            let web      = WebApiClass()
            web.delegate = self
            let urlString = String.init(format: Url.cbBankAssociation)
            let urlStr = getUrl(urlStr: urlString, serverType: .CBBankPay)
            println_debug("cbBankAssociation : \(urlStr)")
            let data = wrapDataModel()
            println_debug(String(data: data ?? Data(), encoding: .utf8))
            let dic = Dictionary<String,Any>()
            //println_debug("Token \(model.data?.token)")
            
            web.genericClass(url: urlStr, param: dic as AnyObject, httpMethod: "POST", mScreen: "CBBankValidation", hbData: data, authToken:model.data?.token , authTokenKey: "X-access-token")
        } else {
            println_debug("No Internet")
        }
    }
    
    private func payFromBackend(accountDetails: BankAccountModel) {
        if !canShowSendBtn(accDetails: accountDetails, withValidateObj: validObj) {
            self.showError(errMsg: "Fill All the fields".localized)
            return
        }
        if UserLogin.shared.loginSessionExpired {
            self.view.endEditing(true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                OKPayment.main.authenticate(screenName: "MyBankToConfirmation", delegate: self)
            }
        } else {
            self.navigateToConfirmationScreen()
        }
    }
    
    func submitTapped() {
        let accountDetails = getAccountDetailsDic()
        if (kycAppControlInfo.first?.OkToBankType ?? 1) == 1 {
            self.payFromBackend(accountDetails: accountDetails)
        } else {
            PaymentVerificationManager.getRoutingBankStatus(bankId: Int16(accountDetails.bankId)) { (status, modelArray) in
                if let modelFirst = modelArray?.first {
                    routingModel = modelFirst
                    if modelFirst.routingStatus == "1" {
                       self.generateToken()
                    } else {
                        self.payFromBackend(accountDetails: accountDetails)
                    }
                }
            }
            
        }
    }
    
    // MARK :- Validate Bottom Submit Button
    func didChangeBankAccountModel() {
        self.validateSubmitBtn()
    }
    
    func validateSubmitBtn() {
        let accountDetails = self.getAccountDetailsDic()
        var isHide = false
        
        if canShowSendBtn(accDetails: accountDetails, withValidateObj: validObj) {
            isHide = false
            DispatchQueue.main.async {
                self.submitBtn.isHidden = false  // to show the submit button
                self.tableBottomConstraint.constant = 50
            }
        } else {
            isHide = true
            DispatchQueue.main.async {
                self.submitBtn.isHidden = true  // to hide the submit button
                self.tableBottomConstraint.constant = 0
            }
        }
        
        for cell in self.myAcc_TableCells {
            if isHide {
                DispatchQueue.main.async {
                    cell.contentTxtField.inputAccessoryView?.isHidden = true
                }
            } else {
                DispatchQueue.main.async {
                    cell.contentTxtField.inputAccessoryView?.isHidden = false
                }
            }
        }
        
        if !isHide {
            println_debug("Showing submit button")
            let bottomOffset = myAcc_AddBankTable.contentSize.height - myAcc_AddBankTable.bounds.size.height
            if !myAcc_AddBankTable.isDragging && bottomOffset < 0 {
                let point: CGPoint = CGPoint(x: 0, y: bottomOffset + (keyboardHeight ?? 0.0))
                println_debug("Scrolling Down")
                myAcc_AddBankTable.setContentOffset(point, animated: true)
            } else if !myAcc_AddBankTable.isDragging && bottomOffset > 0 {
                let point: CGPoint = CGPoint(x: 0, y: bottomOffset + (keyboardHeight ?? 0.0))
                println_debug("Scrolling Up")
                myAcc_AddBankTable.setContentOffset(point, animated: true)
            }
        }
    }
    
    func validateBottomSubmitBtn() {
        let accountDetails = self.getAccountDetailsDic()
        if canShowSendBtn(accDetails: accountDetails, withValidateObj: validObj) {
            DispatchQueue.main.async {
                self.submitBtn.isHidden = false  // to show the submit button
                self.tableBottomConstraint.constant = 50
            }
        } else {
            DispatchQueue.main.async {
                self.submitBtn.isHidden = true  // to hide the submit button
                self.tableBottomConstraint.constant = 0
            }
        }
    }
    
    func clearAndMakeContactFieldFocus() {
        let accDetails = getAccountDetailsDic()
        accDetails.contactMobileNumber = "09"
        for cell in myAcc_TableCells {
            if cell.cellindex == BankItems.bank_contactnumber.rawValue {
                cell.wrap_contactNumber(accDetails: accDetails)
                cell.contentTxtField.becomeFirstResponder()
                break
            }
        }
    }
}

extension MyAccAddBankViewController: ContactPickerDelegate {
    
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]) {
        for contact in contacts {
            decodeContact(contact: contact, isMulti: true)
        }
    }
    
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        decodeContact(contact: contact, isMulti: false)
    }
    
    func decodeContact(contact: ContactPicker, isMulti: Bool) {
        let phoneNumberCount = contact.phoneNumbers.count
        if phoneNumberCount >= 1 {
            println_debug(contact.firstName)
            println_debug(contact.phoneNumbers[0].phoneNumber)
            _ = "\(contact.phoneNumbers[0].phoneNumber)"
            let strMobNo = contact.phoneNumbers[0].phoneNumber
            println_debug(strMobNo)
            let trimmed = String(strMobNo.filter { !" ".contains($0) })
            println_debug(trimmed)
            
            let strContactNumber = self.getConatctNum(trimmed,withCountryCode: "+95")
            println_debug(strContactNumber)
            
            var validNumber = ""
            if strContactNumber.hasPrefix("0095") {
                validNumber = "0\(strContactNumber.substring(from: 4))"
                let validCount = validObj.getNumberRangeValidation(validNumber)
                if validCount.isRejected {
                    alertViewObj.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: #imageLiteral(resourceName: "Contact Mobile Number"))
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        self.clearAndMakeContactFieldFocus()
                    })
                    alertViewObj.showAlert(controller: self)
                } else {
                    if validNumber.count >= validCount.min {
                        self.wrapContactNumberFromContactView(validNumber)
                    } else {
                        alertViewObj.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: #imageLiteral(resourceName: "Contact Mobile Number"))
                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                            self.clearAndMakeContactFieldFocus()
                        })
                        alertViewObj.showAlert(controller: self)
                    }
                }
            } else {
                alertViewObj.wrapAlert(title: nil, body: "Other country number is selected".localized, img: #imageLiteral(resourceName: "Contact Mobile Number"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    self.becomeKeypad(row: BankItems.bank_contactnumber)
                })
                
                alertViewObj.showAlert(controller: self)
            }
        }
        else {
            _ = ContactGlobalConstants.Strings.phoneNumberNotAvaialable
            self.showError(errMsg: "PhoneNumber Not Available".localized)
        }
    }
}

extension MyAccAddBankViewController: ContactSuggestionDelegate {
    func didSelectFromContactSuggession(number: Dictionary<String, Any>) {
        self.hideContactSuggesionView()
        let number = number[ContactSuggessionKeys.phonenumber_backend] as! String
        let validCount = validObj.getNumberRangeValidation(number)
        guard validCount.isRejected == false else {
            alertViewObj.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: #imageLiteral(resourceName: "smToBank"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
            self.wrapContactNumberFromSuggessionView("")
            return
        }
        println_debug("valid count ::: \(validCount)")
        
        if number.count >= validCount.min {
            self.wrapContactNumberFromSuggessionView(number)
        } else {
            // not allowed
            self.wrapContactNumberFromSuggessionView("")
            alertViewObj.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: #imageLiteral(resourceName: "smToBank"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        }
    }
}

extension MyAccAddBankViewController: BioMetricLoginDelegate {
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if screen == "MyBankToConfirmation" {
            if isSuccessful{
                navigateToConfirmationScreen()
            }
        }
    }
}

//alert messages


extension MyAccAddBankViewController: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        if screen == "CBBankValidation" {
            guard let castedData = json as? Data else {
                PTLoader.shared.hide()
                println_debug("Data error")
                return
            }
            do {
                PTLoader.shared.hide()
                // C:Subh -: c If status is 200 than only go down
                let json = dataToJSON(data: castedData)
                if let js = json as? Dictionary<String,Any> {
                    if let code = js["code"] as? Int {
                        if code == 400 {
                            if let message = js["message"] as? String {
                               showErrorAlert(errMessage: message)
                                return
                            }
                        }
                    }
                }
                
                let decoder = JSONDecoder()
                let validation = try decoder.decode(CBBankValidation.self, from: castedData)
                println_debug(validation)
                if validation.code == 200 {
                    if validation.data.statusCode == 400 && (validation.data.message ?? "").contains(find: "User already associated with bank account") {
                        self.navigateToConfirmationScreen()
                    } else if validation.data.statusCode == 400 && (validation.data.message ?? "").contains(find: "Bank account is already associated with other OkDallar pay wallet account") {
                        self.nRCMismatchAlert(message: "Bank Account number is already \nassociated with other OK$ Account. \nCan not associate with \nany more OK$ Account. \nIf you wish, you can proceed \nbut it will take 24 banking hours to Credit.\nFor assistant, please call to".localized, number: "\(validation.data.result.okData.okCustomercare)")
                     } else if validation.data.statusCode == 400 && (validation.data.message ?? "").contains(find: "User already associated with other bank account") {
                        if validation.data.result.okData.cbMultiplebank {
                            self.nRCMismatchAlert(message: "This OK$ Account already associated \nwith a CB Bank Account.\nCan not associate \n with any more CB Bank Account.\nIf you wish, you can proceed \nbut it will take 24 banking hours to Credit.\nFor assistant, please call to".localized, number: "\(validation.data.result.okData.okCustomercare)")
                        } else {
                            self.nRCMismatchAlert(message: "OK$ Account number is not match \nwith CB Pay mobile number. \nFor assistant, please call to".localized, number: "\(validation.data.result.okData.okCustomercare)")
                        }
                    } else if validation.data.statusCode == 400 && (validation.data.message ?? "").lowercased().contains(find: "customer not using mobile banking") {
                      //  self.nRCMismatchAlert(message: "Customer not using mobile banking. \nIf you wish, you can proceed \nbut it will take 24 banking hours to Credit.\nFor assistant, please call to", number: "\(validation.data.result.okData.okCustomercare)")
                        DispatchQueue.main.async {
                            alertViewObj.wrapAlert(title: nil, body: "Invalid CB Bank account number \n(OR) \nNon CB Pay App user. \nIt will take 24 banking hours to credit".localized, img: #imageLiteral(resourceName: "smToBank"))
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                self.cbBankTokenModel = nil
                                self.navigateToConfirmationScreen()
                            })
                            alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
                            })
                            alertViewObj.showAlert(controller: self)
                            return
                        }
                    }else {
                        if validation.data.statusCode == 200 || validation.data.statusCode == 201 && validation.data.status == true {
                                self.showSMBCBOtpVC(validation: validation)
                        }else {
                            if validation.data.message?.lowercased().contains(find: "customer not using mobile banking") ?? false {
                                DispatchQueue.main.async {
                                    alertViewObj.wrapAlert(title: nil, body: "Invalid CB Bank account number \n(OR) \nNon CB Pay App user. \nIt will take 24 banking hours to credit".localized, img: #imageLiteral(resourceName: "smToBank"))
                                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                    })
                                    alertViewObj.showAlert(controller: self)
                                    return
                                }
                            }else {
                                DispatchQueue.main.async {
                                    alertViewObj.wrapAlert(title: nil, body: validation.data.message ?? "", img: #imageLiteral(resourceName: "smToBank"))
                                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                    })
                                    alertViewObj.showAlert(controller: self)
                                    return
                                }
                            }
                            // if server messge is null than show this
                            self.nRCMismatchAlert(message: "Quick Transfer is not \navailable at this moment. \nIf you wish, you can proceed \nbut it will take 24 banking hours /n to Credit.\nFor assistant, please call to ".localized, number: "\(validation.data.result.okData.okCustomercare)")
                        }
                    }
                }else {
                    self.showError(errMsg: "Something went wrong. Please try again!".localized)
                }
            } catch {
                
            }
        } else if screen == "CBBankTokenGenerate" {
            guard let castedData = json as? Data else {
                PTLoader.shared.hide()
                println_debug("Data error")
                return
            }
            do {
                PTLoader.shared.hide()
                let decoder = JSONDecoder()
                let validation = try decoder.decode(CBBankTokenModel.self, from: castedData)
                cbBankTokenModel = validation
                if validation.code == 200 {
                    self.cbBankUpDate(model: validation)
                } else {
                    showErrorAlert(errMessage: "Please try after some time".localized)
                }
            } catch {
                
            }
            
        }
    }
    
    func dataToJSON(data: Data) -> Any? {
       do {
           return try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
       } catch let myJSONError {
           print(myJSONError)
       }
       return nil
    }
    
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        PTLoader.shared.hide()
        if screenName == "CBBankValidation" {
            println_debug("Api Failure in PhNoValidation response")
        } else if screenName == "CBBankTokenGenerate" {
            println_debug("Please try after some time")
        }
    }
}


struct BackAssociationModel: Codable {
    let bankAccountNumber, contactPhoneNumber, nrc, userLevel: String
    let userName, walletReferenceID: String

    enum CodingKeys: String, CodingKey {
        case bankAccountNumber = "BankAccountNumber"
        case contactPhoneNumber = "ContactPhoneNumber"
        case nrc = "Nrc"
        case userLevel = "UserLevel"
        case userName = "UserName"
        case walletReferenceID = "WalletReferenceId"
    }
}


