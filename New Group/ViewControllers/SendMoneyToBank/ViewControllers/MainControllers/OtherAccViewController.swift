//
//  OtherAccViewController.swift
//  OK
//
//  Created by Uma Rajendran on 12/27/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class OtherAccViewController: UIViewController {
    
    var navController                   : UINavigationController?
    var otherAcc_AddBankView            : OtherAccAddBankViewController?
    var otherAcc_ExistingBankView       : OtherAccExistingBankViewController?
    var delegate                        : SubViewDidSelectActionDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refresh_OtherAccountViews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.addNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeNotifications()
    }
    
    
    func addNotifications() {
        if let thisView = self.otherAcc_AddBankView {
            thisView.setKeyboardNotifications()
        }
    }
    
    func removeNotifications() {
        if let thisView = self.otherAcc_AddBankView {
            thisView.removeKeyboardNotifications()
        }
    }
    
    func switchOtherAccViews() {
        BankViewModel.shared.currentAccView = .otheracc
        if BankViewModel.shared.currentMyAccView == .myacc_addbank {
            if checkExistingBanks() > 0 {
                if BankViewModel.shared.currentOtherAccView != .otheracc_existingbank {
                    refreshAccountDetailsDic(isRefreshMyAcc: false)
                    loadExistingBankView()
                } else {
                    self.otherAcc_ExistingBankView?.customViewDidAppear()
                }
            }else {
                if BankViewModel.shared.currentOtherAccView != .otheracc_addbank {
                    loadAddBankView()
                } else {
                    self.otherAcc_AddBankView?.customViewDidAppear()
                }
            }
        }
    }
    
    func refresh_OtherAccountViews() {
        loadBaseView()
    }

    func checkExistingBanks() -> Int {
        // if existing banks are there load the other account existing bank view here
        
        // else load the other account add bank view here
        return CoreDataHelper.shared.otherAccBanks_DBCount()
    }
    
    func loadBaseView() {
        if checkExistingBanks() > 0 {
            loadExistingBankView()
        } else {
            loadAddBankView()
        }
    }
    
    func loadAddBankView() {
        BankViewModel.shared.currentOtherAccView      = .otheracc_addbank
        if let lastView = self.otherAcc_ExistingBankView {
            lastView.view.removeFromSuperview()
        }
        self.otherAcc_AddBankView                      = OtherAccAddBankViewController()
        self.otherAcc_AddBankView?.delegate            = self.delegate
        self.otherAcc_AddBankView!.view.frame          = self.view.bounds
        if let nav = self.navController {
            self.otherAcc_AddBankView?.navc  = nav
        }
        self.view.addSubview(self.otherAcc_AddBankView!.view)
    }
    
    func loadExistingBankView() {
        BankViewModel.shared.currentOtherAccView            = .otheracc_existingbank
        if let lastView = self.otherAcc_AddBankView {
            lastView.view.removeFromSuperview()
        }
        self.otherAcc_ExistingBankView                      = OtherAccExistingBankViewController()
        self.otherAcc_ExistingBankView?.delegate            = self
        self.otherAcc_ExistingBankView!.view.frame          = self.view.bounds
        if let nav = self.navController {
            self.otherAcc_ExistingBankView?.navc  = nav
        }
        self.view.addSubview(self.otherAcc_ExistingBankView!.view)
    }
    
    // MARK:- Delegate Methods
    func didSelectBankAccountType() {
        if BankViewModel.shared.currentOtherAccView == .otheracc_addbank {
            if let addbankview = self.otherAcc_AddBankView {
                addbankview.didSelectBankAccountType()
            }
        }
    }
    
    func didSelect_OtherAccNRCCode() {
        if BankViewModel.shared.currentOtherAccView == .otheracc_addbank {
            if let addbankview = self.otherAcc_AddBankView {
                addbankview.didSelectOtherAccNRCCode()
            }
        }
    }
}

extension OtherAccViewController: OtherAccExistingBankDelegate {
    
    func didSelectSendFrom_ExistingBank() {
        self.loadAddBankView()
    }
    
    func didSelectAddNewBank_OtherAcc() {
        self.refreshAccountDetailsDic(isRefreshMyAcc: false)
        self.loadAddBankView()
    }
}
