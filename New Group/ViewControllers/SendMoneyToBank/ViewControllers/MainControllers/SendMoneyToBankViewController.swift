//
//  SendMoneyToBankViewController.swift
//  OK
//
//  Created by Uma Rajendran on 12/19/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol SubViewDidSelectActionDelegate {
    func showBanksListView()
}

protocol AccountTypeSelectionDelegate {
    func didSelectBankAccountType()
}

struct sendBtnDynamicHeight {
    static let isHideHeight  =  50
    static let isShowHeight  =  50
}

struct sendBtnBottomConstraint {
    static let isKeyboardShown   =  kAppKeyboardHeight
    static let isKeyboardHide    =  0
}

var kAppKeyboardHeight = 0

struct AccountTypes {
    static let calldepositAcc     = "CDA"
    static let savingsAcc         = "SAV"
    static let currentAcc         = "CUR"
    static let atmcardAcc         = "ACA"
}

enum AccountTypesOfBank: String {
    case callDeposit     = "CDA"
    case savings         = "SAV"
    case current         = "CUR"
    case atmCard         = "ACA"
    
    var stringDescription: String {
        switch self {
        case .callDeposit:
            return "Call Deposit Account"
        case .savings:
            return "Savings Account"
        case .current:
            return "Current Account"
        case .atmCard:
            return "ATM Card Account"
        }
    }
}

struct AtmAccount {
    static let minAccNumber       = 16
    static let maxAccNumber       = 19
}

class SendMoneyToBankViewController: SendMoneyBaseViewController {

    @IBOutlet weak var walletBalanceBackView: UIView!
    @IBOutlet weak var walletNameLabel: UILabel!{
        didSet{
            walletNameLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var walletValueLabel: UILabel!
    @IBOutlet weak var switchAccountTabsView: UIView!
    @IBOutlet weak var myAccountBtn: UIButton!
    @IBOutlet weak var otherAccountBtn: UIButton!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var switchTabsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var constraintWidthSeparator: NSLayoutConstraint!
    @IBOutlet weak var constraintLeadingSeparator: NSLayoutConstraint!
    var pageViewController: UIPageViewController?
    var allSourceViews = [UIViewController]()
    weak var myAccView: MyAccViewController?
    weak var otherAccView: OtherAccViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadInitialization()
        loadUI()
        if self.isAdvanceMerchant() {
            self.callKinleyNumAPI()
        }
        updateLocalization()
        checkAndCallGetAllBanksAPI()
        setUpNavigation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func showAddBankInfoToUser(isLoadInOkAction: Bool = false) {
        // image need to change
        alertViewObj.wrapAlert(title: nil, body:  "Transfer money from your OK$ Wallet to your Bank Account processing time minimum one working day.".localized, img: #imageLiteral(resourceName: "smToBank"))
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            if isLoadInOkAction {
                self.loadBaseView()
            }
        })
        alertViewObj.showAlert(controller: self)
    }
    
    func checkAndCallGetAllBanksAPI() {
        guard CoreDataHelper.shared.totalActiveBanksCount() == 0 else {
            showAddBankInfoToUser()
            self.loadBaseView()
            return
        }
        guard appDelegate.checkNetworkAvail() else {                                                                                              
            return
        }
        self.getAllBanksFromServer { [weak self] (isSuccess, response, isAnyBank) in
            guard isSuccess else {
                return
            }
            DispatchQueue.main.async {
                self?.showAddBankInfoToUser(isLoadInOkAction: true)
            }
        }
    }
    
    func loadBaseView() {
        if BankViewModel.shared.currentAccView == .myacc {
            self.myAccView?.refresh_MyAccountViews()
        } else {
            self.otherAccView?.refresh_OtherAccountViews()
        }
    }
    
    func refreshViews() {
        if BankViewModel.shared.currentAccView == .myacc {
            refreshAccountDetailsDic(isRefreshMyAcc: true)
        } else if BankViewModel.shared.currentAccView == .otheracc {
            refreshAccountDetailsDic(isRefreshMyAcc: false)
        }
        DispatchQueue.main.async {
            self.loadBaseView()
        }
    }
    
    func loadUI() {
        switchAccountTabsView.backgroundColor                   = UIColor.white
        myAccountBtn.backgroundColor                            = UIColor.clear
        otherAccountBtn.backgroundColor                         = UIColor.clear
        myAccountBtn.setTitleColor(UIColor.orange, for: .normal)
        otherAccountBtn.setTitleColor(UIColor.black, for: .normal)
        self.view.setShadowToView(view: walletBalanceBackView, color: kBackGroundGreyColor)
        self.view.setShadowToView(view: baseView, color: kBackGroundGreyColor)
    }
    
    @objc func backBtnAction() {
        var isPopSelfView = true
        if BankViewModel.shared.currentAccView == .myacc {
            if BankViewModel.shared.currentMyAccView  == .myacc_addbank {
                if CoreDataHelper.shared.myAccBanks_DBCount() > 0 {
                    refreshAccountDetailsDic(isRefreshMyAcc: true)
                    
                    BankViewModel.shared.currentMyAccView = .myacc_existingbank
                    self.myAccView?.refresh_MyAccountViews()
                    isPopSelfView = false
                }
            }
        } else if BankViewModel.shared.currentAccView == .otheracc {
            
            if BankViewModel.shared.currentOtherAccView  == .otheracc_addbank {
                if CoreDataHelper.shared.otherAccBanks_DBCount() > 0 {
                    refreshAccountDetailsDic(isRefreshMyAcc: false)
                    
                    BankViewModel.shared.currentOtherAccView = .otheracc_existingbank
                    self.otherAccView?.refresh_OtherAccountViews()
                    isPopSelfView = false
                }
            }
        }
    
        if isPopSelfView {
            if let nav  = self.navigationController {
                clear_BankAccountDetailsDic()
                BankViewModel.refreshBank()
                self.removeAllNotifications()
                nav.popViewController(animated: true)
            }
        }
    }
    
    func removeAllNotifications() {
        self.myAccView?.removeNotifications()
        self.otherAccView?.removeNotifications()
    }
    
    func loadInitialization() {
        getBankDetails()
        BankViewModel.shared.currentAccView   = .myacc
        checkAndCreate_BankAccountDetailsInfoDict()
        if self.isAdvanceMerchant() {
            self.loadViewControllers_ForAdvanceMerchant()
        } else {
            self.loadViewControllers_ForOthers()
        }
        checkAdvanceMerchant()
        myAccTab_HighLighted()
    }
    
    func updateLocalization() {
        walletNameLabel.text                = "OK$ Wallet Balance".localized
        walletValueLabel.attributedText     = self.walletBalance()
        myAccountBtn.setTitle("My Account".localized, for: .normal)
        otherAccountBtn.setTitle("Other Account".localized, for: .normal)
    }
    
    @objc func customBackAction() {
        //if self.isAdvanceMerchant() {
            if BankViewModel.shared.currentAccView == .myacc {
                if BankViewModel.shared.currentMyAccView == .myacc_addbank {
                    if let myAccVC = myAccView {
                        if myAccVC.checkExistingBanks() > 0 {
                            refreshAccountDetailsDic(isRefreshMyAcc: true)
                            myAccVC.loadExistingBankView()
                            return
                        }
                    }
                }
            } else {
                if BankViewModel.shared.currentOtherAccView == .otheracc_addbank {
                    if let otherVC = otherAccView {
                        if otherVC.checkExistingBanks() > 0 {
                            refreshAccountDetailsDic(isRefreshMyAcc: false)
                            otherVC.loadExistingBankView()
                            return
                        }
                    }
                }
            }
      //  }
        refreshAccountDetailsDic(isRefreshMyAcc: true)
        refreshAccountDetailsDic(isRefreshMyAcc: false)
        if UserModel.shared.agentType == .advancemerchant {
            self.navigationController?.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(customBackAction), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "Send Money To Bank".localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21.0) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
    
    func myAccTab_HighLighted() {
        DispatchQueue.main.async {
            self.myAccountBtn.setTitleColor(UIColor.orange, for: .normal)
            self.otherAccountBtn.setTitleColor(UIColor.black, for: .normal)
            UIView.animate(withDuration: 0.2) {
                self.constraintLeadingSeparator.constant = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func otherAccTab_HighLighted() {
        DispatchQueue.main.async {
            self.myAccountBtn.setTitleColor(UIColor.black, for: .normal)
            self.otherAccountBtn.setTitleColor(UIColor.orange, for: .normal)
            UIView.animate(withDuration: 0.2) {
                self.constraintLeadingSeparator.constant = UIScreen.main.bounds.size.width / 2
                self.view.layoutIfNeeded()
            }
        }
    }

    func checkAdvanceMerchant() {
        self.switchTabsViewHeightConstraint.constant = self.isAdvanceMerchant() ? 50 : 0
        self.constraintWidthSeparator.constant = UIScreen.main.bounds.size.width / 2
        self.switchAccountTabsView.isHidden = self.isAdvanceMerchant() ? false : true
    }
 
    @IBAction func tabAction(_ sender: Any) {
        let btn =  sender as! UIButton
        if btn.tag == 0 {
            if let view = viewControllerAtIndex(index: 0) {
                DispatchQueue.main.async {
                    self.myAccTab_HighLighted()
                    self.myAccView?.switchMyAccViews()
                }
                pageViewController!.setViewControllers([view], direction: .forward, animated: false, completion: nil)
            }
        } else {
            if let view = viewControllerAtIndex(index: 1) {
                DispatchQueue.main.async {
                    self.otherAccTab_HighLighted()
                    self.otherAccView?.switchOtherAccViews()
                }
                pageViewController!.setViewControllers([view], direction: .forward, animated: false, completion: nil)
            }
        }
    }
    
    func getBankDetails() {
        hitBankListIfBankEmpty()
    }
    
    func hitBankListIfBankEmpty() {
        println_debug("hitBankListIfBankEmpty")
        guard appDelegate.checkNetworkAvail() else {
            return
        }
        PaymentVerificationManager.getAllBanksFromDB { (status, banks) in
            if status, let bankList = banks {
                if bankList.count > 0 {
                    BankViewModel.shared.bankListDB = bankList
                } else {
                    BankViewModel.hitBankListApi()
                }
            } else {
                
            }
        }
//        if  BankViewModel.shared.bankListDB.isEmpty {
//         if !BankViewModel.apiCallInProgress {
//             BankViewModel.hitBankListApi()
//         } else {
//             println_debug("api calling is on progress")
//            }
//        }
    }
    
    func loadViewControllers_ForAdvanceMerchant() {
        myAccView       = UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "MyAccView_ID") as? MyAccViewController
        otherAccView    = UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "OtherAccView_ID") as? OtherAccViewController
        allSourceViews.append(myAccView!)
        allSourceViews.append(otherAccView!)
        
        pageViewController  = UIPageViewController.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController!.delegate    = self
        pageViewController!.dataSource  = self
        pageViewController!.view.frame  = baseView.bounds
        pageViewController!.view.backgroundColor = UIColor.clear
        
        myAccView?.delegate            = self
        otherAccView?.delegate         = self
        myAccView?.navController       = self.navigationController
        otherAccView?.navController    = self.navigationController
        
        pageViewController!.setViewControllers([allSourceViews[0]], direction: .forward, animated: true, completion: nil)
        addChild(pageViewController!)
        baseView.addSubview(pageViewController!.view)
        pageViewController!.didMove(toParent: self)
        
        for view in self.view.subviews {
            view.layoutIfNeeded()
        }
    }
    
    func loadViewControllers_ForOthers() {
        myAccView       = UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "MyAccView_ID") as? MyAccViewController
        myAccView?.delegate            = self
        myAccView?.navController       = self.navigationController
        myAccView?.view.frame          = baseView.bounds
        addChild(myAccView!)
        baseView.addSubview((myAccView?.view)!)
        myAccView?.didMove(toParent: self)
        
        for view in self.view.subviews {
            view.layoutIfNeeded()
        }
    }
    
    deinit {
        myAccView      = nil
        otherAccView    = nil
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
}

extension SendMoneyToBankViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index : Int = self.indexOf(viewController)
        if index == 0 {
            return nil
        }
        index -= 1
        return allSourceViews[index]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index : Int = self.indexOf(viewController)
        index += 1
        if index == allSourceViews.count {
            return nil
        }
        return allSourceViews[index]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        self.setPageIndex()
    }
    
    func indexOf(_ viewController: UIViewController) -> Int {
        return (allSourceViews as NSArray).index(of: viewController)
    }
    
    func viewControllerAtIndex(index: Int ) -> UIViewController? {
        if allSourceViews.count == 0 || index >= allSourceViews.count {
            return nil
        }
        return allSourceViews[index]
    }
    
    func setPageIndex() {
        let currentViewController: UIViewController = pageViewController!.viewControllers![0]
        let currentIndex: Int = self.indexOf(currentViewController)
        
        if currentIndex == 0 {
            highLight_myAccView()
        }else if currentIndex == 1 {
            highLight_otherAccView()
        }
    }
    
    func highLight_myAccView() {
        DispatchQueue.main.async {
             UIView.animate(withDuration: 0.5) {
                self.myAccTab_HighLighted()
                self.myAccView?.switchMyAccViews()
             }
        }
    }
    
    func highLight_otherAccView() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5) {
                self.otherAccTab_HighLighted()
                self.otherAccView?.switchOtherAccViews()
            }
        }
    }
}

extension SendMoneyToBankViewController: SubViewDidSelectActionDelegate {
    func showBanksListView() {
        if BankViewModel.shared.bankListDB.count > 0 {
            let vc = UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "BanksListView_ID") as! BanksListViewController
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            self.showErrorAlert(errMessage: "Sorry! Banks yet to loaded. Please try again".localized)
            hitBankListIfBankEmpty()
        }
    }
}

extension SendMoneyToBankViewController: AccountTypeSelectionDelegate {
    func didSelectBankAccountType() {
        let accountDetails = getAccountDetailsDic()
        if accountDetails.isMyAccount {
            myAccView?.didSelectBankAccountType()
        }else {
            otherAccView?.didSelectBankAccountType()
        }
    }
}

extension SendMoneyToBankViewController: ConfirmationDelegate {
    func removeConfirmationScreen() {
        self.navigationController?.popViewController(animated: true)
        self.refreshViews()
    }
}

extension SendMoneyToBankViewController : NRCSelectionDelegate {
    func didSelectNRCCode() {
        otherAccView?.didSelect_OtherAccNRCCode()
    }
}
