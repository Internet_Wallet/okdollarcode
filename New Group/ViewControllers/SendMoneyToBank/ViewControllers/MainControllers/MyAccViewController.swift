//
//  MyAccViewController.swift
//  OK
//
//  Created by Uma Rajendran on 12/27/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class MyAccViewController: UIViewController {
    
    var navController                   : UINavigationController?
    var myAcc_AddBankView               : MyAccAddBankViewController?
    var myAcc_ExistingBankView          : MyAccExistingBankViewController?
    var delegate                        : SubViewDidSelectActionDelegate?
    
//    @IBOutlet weak var baseview: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //refresh_MyAccountViews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.addNotifications()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.removeNotifications()
    }
    
    func addNotifications() {
        
        if let thisView = self.myAcc_AddBankView {
            thisView.setKeyboardNotifications()
        }
    }
    
    func removeNotifications() {
        if let thisView = self.myAcc_AddBankView {
            println_debug(thisView)
            thisView.removeKeyboardNotifications()
        }
    }
    
    func switchMyAccViews() {

        BankViewModel.shared.currentAccView = .myacc
        
        if BankViewModel.shared.currentOtherAccView == .otheracc_addbank {
            if checkExistingBanks() > 0 {
                if BankViewModel.shared.currentMyAccView != .myacc_existingbank {
                    refreshAccountDetailsDic(isRefreshMyAcc: true)
                    loadExistingBankView()
                }else {
                    self.myAcc_ExistingBankView?.customViewDidAppear()
                }
            }else {
                if BankViewModel.shared.currentMyAccView != .myacc_addbank {
                     loadAddBankView()
                }else {
                     self.myAcc_AddBankView?.customViewDidAppear()
                }
            }
        }
    }
    
    func refresh_MyAccountViews() {
        loadBaseView()
    }
    
    func checkExistingBanks() -> Int {
        // if existing banks are there load the my account existing bank view here
        
        // else load the my account add bank view here
        return CoreDataHelper.shared.myAccBanks_DBCount()
    }
    
    func loadBaseView() {
        if checkExistingBanks() > 0 {
            loadExistingBankView()
        } else {
            loadAddBankView()
        }
    }
    
    func loadAddBankView() {
        
        BankViewModel.shared.currentMyAccView       = .myacc_addbank
        
        if let lastView = self.myAcc_ExistingBankView {
            lastView.view.removeFromSuperview()
        }
       
        self.myAcc_AddBankView                      = MyAccAddBankViewController()
        self.myAcc_AddBankView?.delegate            = self.delegate
        self.myAcc_AddBankView!.view.frame          = self.view.bounds
        if let nav = self.navController {
            self.myAcc_AddBankView?.navc  = nav
        }
        self.view.addSubview(self.myAcc_AddBankView!.view)
    }
    
    func loadExistingBankView() {
        
        BankViewModel.shared.currentMyAccView             = .myacc_existingbank

        if let lastView = self.myAcc_AddBankView {
            lastView.view.removeFromSuperview()
        }
        self.myAcc_ExistingBankView                      = MyAccExistingBankViewController()
        self.myAcc_ExistingBankView?.delegate            = self
        self.myAcc_ExistingBankView!.view.frame          = self.view.bounds
        if let nav = self.navController {
            self.myAcc_ExistingBankView?.navc  = nav
        }
        self.view.addSubview(self.myAcc_ExistingBankView!.view)
    }
    
    // MARK:- Delegate Methods
    func didSelectBankAccountType() {

        if BankViewModel.shared.currentMyAccView == .myacc_addbank {
            if let addbankview = self.myAcc_AddBankView {
                addbankview.didSelectBankAccountType()
            }
        }
    }
}

extension MyAccViewController: MyAccExistingBankDelegate {
    
    func didSelectSendFrom_ExistingBank()  {
        self.loadAddBankView()
    }

    func didSelectAddNewBank_MyAcc() {
        self.refreshAccountDetailsDic(isRefreshMyAcc: true)
        self.loadAddBankView()
    }
}
