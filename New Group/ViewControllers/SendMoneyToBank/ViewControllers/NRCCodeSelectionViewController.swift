//
//  NRCCodeSelectionViewController.swift
//  OK
//
//  Created by Uma Rajendran on 1/24/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol NRCTypesSelectionDelegate {
    func didSelectNRCType(accDetails: BankAccountModel)
}

class NRCCodeSelectionViewController: UIViewController {

    @IBOutlet weak var backView: UIView!
    @IBOutlet var nrcCodeBtnCollection: [UIButton]!
    
    let codes = [NRCCodes.N_Code, NRCCodes.P_Code, NRCCodes.E_Code, NRCCodes.T_Code]
    var delegate: NRCTypesSelectionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = kGradientGreyColor
        self.loadUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadUI() {
        let accountDetails          =   getAccountDetailsDic()
        for (index, _) in self.nrcCodeBtnCollection.enumerated() {
            let btn = self.nrcCodeBtnCollection[index]
            let str = "\(accountDetails.nrcCodeNumber)/\(accountDetails.nrcCode)\(codes[index])"
            btn.setTitle(str, for: .normal)
            btn.titleLabel?.font    =  UIFont(name: appFont, size: 14)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if touch.view != self.backView {
                self.resignTheSelfView()
            }
        }
        super.touchesBegan(touches, with: event)
    }
    
    func resignTheSelfView() {
        if let presentingView = self.presentingViewController {
            presentingView.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func nrcCodeBtnAction(_ sender: Any) {
        let btn = sender as? UIButton
        if let tag = btn?.tag {
            let code                    =   codes[tag]
            let accountDetails          =   getAccountDetailsDic()
            accountDetails.nrcFullCode  =   "\(accountDetails.nrcCodeNumber)/\(accountDetails.nrcCode)\(code)"
            accountDetails.accountIDProofNumber  =  accountDetails.nrcFullCode
            if let del = self.delegate {
                del.didSelectNRCType(accDetails: accountDetails)
            }
        }
        self.resignTheSelfView()
    }
}
