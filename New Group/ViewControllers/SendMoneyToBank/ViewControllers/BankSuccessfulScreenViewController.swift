//
//  BankSuccessfulScreenViewController.swift
//  OK
//
//  Created by Uma Rajendran on 12/21/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BankSuccessfulScreenViewController: SendMoneyBaseViewController {
    
    @IBOutlet weak var backView                 : CardDesignView!
    @IBOutlet weak var receiptNameLbl           : UILabel!
    @IBOutlet weak var logoImgView              : UIImageView!
    @IBOutlet weak var separatorImgView         : UIImageView!
    @IBOutlet weak var accHolderName            : UILabel!
    @IBOutlet weak var transferAmountLbl        : UILabel!
    @IBOutlet weak var bankLogoImgView          : UIImageView!
    @IBOutlet weak var bank_NameLbl             : UILabel!
    @IBOutlet weak var branch_NameLbl           : UILabel!
    @IBOutlet weak var accType_NameLbl          : UILabel!
    @IBOutlet weak var accNum_NameLbl           : UILabel!
    @IBOutlet weak var balance_NameLbl          : UILabel!
    @IBOutlet weak var bank_ContentLbl          : UILabel!
    @IBOutlet weak var branch_ContentLbl        : UILabel!
    @IBOutlet weak var accType_ContentLbl       : UILabel!
    @IBOutlet weak var accNum_ContentLbl        : UILabel!
    @IBOutlet weak var balance_ContentLbl       : UILabel!
    @IBOutlet weak var transaction_NameLbl: UILabel!
    @IBOutlet weak var transaction_ContentLbl: UILabel!
    @IBOutlet weak var dateLbl                  : UILabel!
    @IBOutlet weak var timeLbl                  : UILabel!
    @IBOutlet weak var homeBtn                  : UIButton!

    var accountDetails: BankAccountModel?
    var transactionDict = [String: Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        println_debug("trans dic : \(transactionDict)")
        loadUI()
        updateLocalization()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clear_BankAccountDetailsDic()
        BankViewModel.refreshBank()
        BankViewModel.refreshNRC()
        self.loadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let img = self.backView.snapshotOfCustomeView
        img.saveImageToGallery()
    }
    
    func loadUI() {
        self.view.backgroundColor = kBackGroundGreyColor
        loadNavigationBarSettings()
        self.backButtonSettings()
        self.navigationController?.title        = "Payment Successful".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 21) ?? UIFont.systemFont(ofSize: 21), NSAttributedString.Key.foregroundColor: UIColor.white]

//        if let navFont = UIFont(name: "Zawgyi-One", size: 21) {
//            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: navFont]
//        }
    }
    
    fileprivate func backButtonSettings() {
        let backBtn = UIBarButtonItem(image: UIImage(named: ""), style: .plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItem  = backBtn
    }
    
    @objc private func backBtnAction() {
       //self.popSelfView()
    }
    
    private func popSelfView() {
        DispatchQueue.main.async {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: AddWithdrawViewController.self) {
                    
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    
    func updateLocalization() {
        receiptNameLbl.font             = UIFont(name: appFont, size: 22)
        accHolderName.font              = UIFont(name: appFont, size: 18)
        
        bank_NameLbl.font               = UIFont(name: appFont, size: 15)
        branch_NameLbl.font             = UIFont(name: appFont, size: 15)
        accType_NameLbl.font            = UIFont(name: appFont, size: 15)
        transaction_NameLbl.font            = UIFont(name: appFont, size: 15)
        accNum_NameLbl.font             = UIFont(name: appFont, size: 15)
        balance_NameLbl.font            = UIFont(name: appFont, size: 15)
        
        bank_ContentLbl.font            = UIFont(name: appFont, size: 15)
        branch_ContentLbl.font          = UIFont(name: appFont, size: 15)
        accType_ContentLbl.font         = UIFont(name: appFont, size: 15)
        transaction_ContentLbl.font     = UIFont(name: appFont, size: 15)
        accNum_ContentLbl.font          = UIFont(name: appFont, size: 15)
 
        dateLbl.font                    = UIFont(name: appFont, size: 13)
        timeLbl.font                    = UIFont(name: appFont, size: 13)
        
        homeBtn.titleLabel?.font        = UIFont(name: appFont, size: appButtonSize)

        receiptNameLbl.textColor        = kYellowColor
        accHolderName.textColor         = UIColor.black
        bank_NameLbl.textColor          = UIColor.black
        branch_NameLbl.textColor        = UIColor.black
        accType_NameLbl.textColor       = UIColor.black
        transaction_NameLbl.textColor   = UIColor.black
        accNum_NameLbl.textColor        = UIColor.black
        balance_NameLbl.textColor       = UIColor.black
        
        bank_ContentLbl.textColor       = UIColor.black
        branch_ContentLbl.textColor     = UIColor.black
        accType_ContentLbl.textColor    = UIColor.black
        transaction_ContentLbl.textColor = UIColor.black
        accNum_ContentLbl.textColor     = UIColor.black
        dateLbl.textColor               = UIColor.black
        timeLbl.textColor               = UIColor.black
 
        homeBtn.setTitleColor(UIColor.darkGray, for: .normal)
        homeBtn.setTitle(setLocStr("Home Page"), for: .normal)
        
        receiptNameLbl.text             = "Receipt".localized
        bank_NameLbl.text               = "Bank Name".localized
        branch_NameLbl.text             = "Branch Name".localized
        accType_NameLbl.text            = "Account Type".localized
        transaction_NameLbl.text        = "Transaction ID".localized
        accNum_NameLbl.text             = "Account Number".localized
        balance_NameLbl.text            = "Balance".localized
        dateLbl.text = self.getDate()
        timeLbl.text = self.getTime()
    }
    
    func loadData() {
        let accountTypes = ["CDA" : "Call Deposit Account", "SAV" : "Savings Account", "CUR" : "Current Account", "ACA": "ATM Card Account"]
        if let accDetail = self.accountDetails {
            var bankname = ""
            var branchname = ""
            if let curLanguage = UserDefaults.standard.value(forKey: "currentLanguage") as? String {
                switch curLanguage {
                case "en":
                    bankname = accDetail.bankName
                    branchname = accDetail.branchName
                case "my":
                    bankname = accDetail.bankBurmeseName
                    branchname = accDetail.branchBurmeseName
                case "uni":
                    bankname = accDetail.bankBurmeseNameUnicode
                    branchname = accDetail.branchBurmeseNameUnicode
                default:
                    bankname = accDetail.bankName
                    branchname = accDetail.branchName
                }
            }
            let acctype      = setLocStr(accountTypes[accDetail.accountType]!)
            accHolderName.text                  = accDetail.accountHolderName
            bank_ContentLbl.text                = bankname
            branch_ContentLbl.text              = branchname
            transaction_ContentLbl.text         = transactionDict["transid"] as? String ?? ""
            accType_ContentLbl.text             = acctype
            accNum_ContentLbl.text              = accDetail.accountNumber
            let amount = accDetail.amount.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: ".00", with: "")
            transferAmountLbl.attributedText    = self.attributedStringWithFont(amount: self.amountInFormat(amount), fontSize: 22)
        }
        if let bal = self.transactionDict["walletbalance"] as? String {
            balance_ContentLbl.attributedText   = self.attributedStringWithFont(amount: self.amountInFormat(bal) , fontSize: 15.0)
        }
    }
    
    func getDate() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "dd-MMM-yyyy"
        return formatter.string(from: date)
    }
    
    func getTime() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "HH:mm:ss"
        return formatter.string(from: date)
    }
  
    @IBAction func homeBtnAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
      //self.popSelfView()
    }
}
