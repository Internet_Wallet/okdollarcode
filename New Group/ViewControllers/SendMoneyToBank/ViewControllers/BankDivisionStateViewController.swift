//
//  BankDivisionStateViewController.swift
//  OK
//
//  Created by Uma Rajendran on 12/21/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BankDivisionStateViewController: SendMoneyBaseViewController {

    @IBOutlet weak var divisionStateTable: UITableView!
    
    private lazy var searchBar = UISearchBar()
    var divListUI           = [BankBranchByDivision]()
    var searchList: [BankBranchByDivision] = []
    
    var divListUIDB           = [BankByDivision]()
    var searchListDB: [BankByDivision] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadInitialization()
        loadUI()
        setUpNavigation()
        setUpSearchBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.divisionStateTable.isUserInteractionEnabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
    func loadUI() {
        self.view.backgroundColor               = kBackGroundGreyColor
        self.divisionStateTable.backgroundColor = kBackGroundGreyColor
        self.divisionStateTable.register(UINib(nibName: "BankListCell", bundle: nil), forCellReuseIdentifier: "listcellidentifier")
        self.divisionStateTable.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    func loadInitialization() {
        PaymentVerificationManager.getAllDivisionsFromDB(bankId: BankViewModel.shared.selectedBankDB?.bankId) { (status, divisionList) in
            DispatchQueue.main.async {
                if status, let divList = divisionList {
                    self.divListUIDB = divList.sorted(by: {($0.divisionEName ?? "") < ($1.divisionEName ?? "")})
                    self.searchListDB = self.divListUIDB
                } else {
                    self.divListUIDB = []
                }
                self.divisionStateTable.reloadData()
                self.divisionStateTable.isUserInteractionEnabled = true
            }
        }
        //        if let myList = BankViewModel.shared.selectedBank?.branchListByDivision {
        //            divListUI = myList.sorted(by: {$0.divisionNameEN < $1.divisionNameEN})
        //            searchList = divListUI
        //        } else {
        //            divListUI = []
        //        }
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func setUpSearchBar() {
        searchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44)
        if #available(iOS 11.0, *) {
            searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.tintColor = UIColor.white
        searchBar.placeholder = "Search".localized
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
            searchTextField.keyboardType = .asciiCapable
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
                searchTextField.keyboardType = .asciiCapable
            }
        }
        let view = self.searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = UIColor.gray
            }
        }
    }
    
    func resetDivisionScreen() {
        setUpNavigation()
        searchListDB = divListUIDB
        divisionStateTable.reloadData()
    }
    
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        if divListUI.count > 1 {
            let searchButton = UIButton(type: .custom)
            searchButton.setImage(#imageLiteral(resourceName: "search_white").withRenderingMode(.alwaysOriginal), for: .normal)
            searchButton.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
            if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
                searchButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
            }
            else{
                searchButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
            }
            navTitleView.addSubview(searchButton)
        }
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "Select Division / State".localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21.0) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }

    @objc func searchAction() {
        searchBar.placeholder = "Search".localized
        searchBar.text = ""
        self.navigationItem.titleView = searchBar
        searchBar.becomeFirstResponder()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInset = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
            self.divisionStateTable.contentInset = contentInset
            self.divisionStateTable.scrollIndicatorInsets = contentInset
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.divisionStateTable.contentInset = UIEdgeInsets.zero
        self.divisionStateTable.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}

extension BankDivisionStateViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if searchListDB.count == 0 {
            let noDataLabel = UILabel(frame: tableView.bounds)
            if let navFont = UIFont(name: appFont, size: 21) {
                noDataLabel.font = navFont
            }
            noDataLabel.text = "No records found!".localized
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
        } else {
            tableView.backgroundView = nil
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchListDB.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let bankdivisioncell = tableView.dequeueReusableCell(withIdentifier: "listcellidentifier", for: indexPath) as! BankListCell
        let bankDivision = searchListDB[indexPath.row] as BankByDivision
        var divName = ""
        if let curLanguage = UserDefaults.standard.value(forKey: "currentLanguage") as? String {
            switch curLanguage {
            case "en":
                divName = bankDivision.divisionEName ?? ""
            case "my":
                divName = bankDivision.divisionBName ?? ""
            case "uni":
                divName = bankDivision.divisionUName ?? ""
            default:
                divName = bankDivision.divisionEName ?? ""
            }
        }
        bankdivisioncell.wrapData(title: divName, count: bankDivision.branchCount ?? "0")
        return bankdivisioncell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let bankdivision = searchListDB[indexPath.row] as BankByDivision
        func pushToTownship() {
            let story = UIStoryboard(name: "SendMoney", bundle: nil)
            let townshipview = story.instantiateViewController(withIdentifier: "BankTownshipListView_ID") as! BankTownshipViewController
            BankViewModel.shared.selectedDivisionDB = bankdivision
            self.navigationController?.pushViewController(townshipview, animated: true)
        }
        if searchBar.isFirstResponder {
            DispatchQueue.main.async {
                self.searchBar.endEditing(true)
                self.resetDivisionScreen()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.60) {
                    pushToTownship()
                }
            }
        } else {
            pushToTownship()
        }
        divisionStateTable.isUserInteractionEnabled = false
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}

// MARK: -UISearchBarDelegate
extension BankDivisionStateViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            uiButton.setTitle("Cancel".localized, for:.normal)

            if let navFont = UIFont(name: appFont, size: 18) {
                uiButton.titleLabel?.font = navFont
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            searchListDB = divListUIDB
            divisionStateTable.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            if updatedText.count > 30 {
                return false
            }
            if updatedText != "" && text != "\n" {
                searchListDB = []
                if let curLanguage = UserDefaults.standard.value(forKey: "currentLanguage") as? String {
                    switch curLanguage {
                    case "en":
                        searchListDB = divListUIDB.filter {
                            guard let _ = ($0.divisionEName ?? "").localized.localizedStandardRange(of: updatedText) else { return false }
                            return true
                        }
                    case "my":
                        searchListDB = divListUIDB.filter {
                            guard let _ = ($0.divisionBName ?? "").localized.localizedStandardRange(of: updatedText) else { return false }
                            return true
                        }
                    case "uni":
                        searchListDB = divListUIDB.filter {
                            guard let _ = ($0.divisionUName ?? "").localized.localizedStandardRange(of: updatedText) else { return false }
                            return true
                        }
                    default:
                        searchListDB = divListUIDB.filter {
                            guard let _ = ($0.divisionEName ?? "").localized.localizedStandardRange(of: updatedText) else { return false }
                            return true
                        }
                    }
                } else {
                    return false
                }
                divisionStateTable.reloadData()
            }
        }
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        resetDivisionScreen()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}
