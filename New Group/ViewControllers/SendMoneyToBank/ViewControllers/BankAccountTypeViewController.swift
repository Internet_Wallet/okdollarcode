//
//  BankAccountTypeViewController.swift
//  OK
//
//  Created by Uma Rajendran on 12/21/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BankAccountTypeViewController: SendMoneyBaseViewController {

    @IBOutlet weak var accountTypeTable: UITableView! 
    
    let accountTypes        = ["Call Deposit Account","Savings Account","Current Account","ATM Card Account"]
    let accountTypesVal     = ["CDA", "SAV", "CUR", "ACA"]
    var delegate: AccountTypeSelectionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadUI()
        setUpNavigation()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        accountTypeTable.isUserInteractionEnabled = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadUI() {
        self.view.backgroundColor               = kBackGroundGreyColor
        self.accountTypeTable.backgroundColor   = kBackGroundGreyColor
        self.accountTypeTable.tableFooterView   = UIView.init(frame: CGRect.zero)
    }
    
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "Select Account Type".localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21.0) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}

extension BankAccountTypeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accountTypes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let accounttypecell = tableView.dequeueReusableCell(withIdentifier: "accounttypecellidentifier", for: indexPath) as! BankAccountTypeCell
        let accountType = setLocStr(accountTypes[indexPath.row]) 
        accounttypecell.wrapData(title: accountType)
        return accounttypecell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let accountTypeVal  = accountTypesVal[indexPath.row]
        BankViewModel.shared.selectedAccountType  = accountTypeVal
        
        let accountDetails = getAccountDetailsDic()
        accountDetails.bankId                   = Int(BankViewModel.shared.selectedBankDB?.bankId ?? 0)
        accountDetails.bankName                 = (BankViewModel.shared.selectedBankDB?.bankName ?? "")
        accountDetails.bankBurmeseName          = (BankViewModel.shared.selectedBankDB?.burmeseName ?? "")
        accountDetails.branchId                 = Int(BankViewModel.shared.selectedBranchDB?.branchId ?? 0)
        accountDetails.branchName               = (BankViewModel.shared.selectedBranchDB?.branchEName ?? "")
        accountDetails.branchBurmeseName        = (BankViewModel.shared.selectedBranchDB?.branchBName ?? "")
        accountDetails.stateCode                = (BankViewModel.shared.selectedDivisionDB?.divisionCode ?? "")
        accountDetails.stateName                = (BankViewModel.shared.selectedDivisionDB?.divisionEName ?? "")
        accountDetails.stateBurmeseName         = (BankViewModel.shared.selectedDivisionDB?.divisionBName ?? "")
        accountDetails.townshipCode             = (BankViewModel.shared.selectedTownshipDB?.townshipCode ?? "")
        accountDetails.townshipName             = (BankViewModel.shared.selectedTownshipDB?.townshipEName ?? "")
        accountDetails.townshipBurmeseName      = (BankViewModel.shared.selectedTownshipDB?.townshipBName ?? "")
        accountDetails.branchAddress            = (BankViewModel.shared.selectedBranchDB?.branchEAddress ?? "")
        accountDetails.branchBurmeseAddress     = (BankViewModel.shared.selectedBranchDB?.branchBAddress ?? "")
        accountDetails.accountType              = accountTypeVal
        accountDetails.accountNumber            = ""
        accountDetails.accountFormat            = (BankViewModel.shared.selectedBankDB?.accountFormat ?? "")
        accountDetails.bankPhoneNumber          = (BankViewModel.shared.selectedBranchDB?.phonenumber ?? "")
        accountDetails.bankFaxNumber            = (BankViewModel.shared.selectedBranchDB?.faxnumber ?? "")
        accountDetails.multiAccountFormat       = (BankViewModel.shared.selectedBankDB?.multiAccountFormat ?? "")
        println_debug("Bank Account selected: \(BankViewModel.shared.selectedBankDB?.multiAccountFormat ?? "")")
        accountDetails.bankBurmeseNameUnicode = (BankViewModel.shared.selectedBankDB?.unicodeName ?? "")
        accountDetails.branchBurmeseNameUnicode = (BankViewModel.shared.selectedBranchDB?.branchUName ?? "")
        accountDetails.townshipBurmeseNameUnicode = (BankViewModel.shared.selectedTownshipDB?.townshipUName ?? "")
        accountDetails.stateBurmeseNameUnicode = (BankViewModel.shared.selectedDivisionDB?.divisionUName ?? "")
        accountDetails.branchBurmeseAddressUnicode = (BankViewModel.shared.selectedBranchDB?.branchUAddress ?? "")
        
        DispatchQueue.main.async {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: SendMoneyToBankViewController.self) {
                    self.delegate = (controller as! AccountTypeSelectionDelegate)
                    self.delegate?.didSelectBankAccountType()

                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        accountTypeTable.isUserInteractionEnabled = false
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
