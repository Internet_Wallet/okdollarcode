//
//  NRCDivisionViewController.swift
//  OK
//
//  Created by Uma Rajendran on 1/23/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class NRCDivisionViewController: SendMoneyBaseViewController {
    
    @IBOutlet weak var nrcTable: UITableView!
    private lazy var searchBar = UISearchBar()
    var allLocationsList = [LocationDetail]()
    private var searchList = [LocationDetail]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        allLocationsList = TownshipManager.allLocationsList
        self.sort(arr: allLocationsList)
        searchList = allLocationsList
        self.loadUI()
        setUpSearchBar()
        setUpNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        nrcTable.isUserInteractionEnabled = true
        BankViewModel.refreshNRC()
        nrcTable.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadUI() {
        self.view.backgroundColor = kBackGroundGreyColor
        self.nrcTable.backgroundColor = kBackGroundGreyColor
        nrcTable.tableFooterView = UIView()
    }
    
    func sort(arr: [LocationDetail]) {
        allLocationsList = arr.sorted(by: {$0.stateOrDivitionNameEn < $1.stateOrDivitionNameEn})
    }
    
    func setUpSearchBar() {
        searchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44)
        if #available(iOS 11.0, *) {
            searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.tintColor = UIColor.white
        searchBar.placeholder = "Search".localized
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
            searchTextField.keyboardType = .asciiCapable
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
                searchTextField.keyboardType = .asciiCapable
            }
        }
        let view = self.searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = UIColor.gray
            }
        }
    }
    
    @objc func searchAction() {
        searchBar.placeholder = "Search".localized
        searchBar.text = ""
        self.navigationItem.titleView = searchBar
        searchBar.becomeFirstResponder()
    }
    
    func resetNRCDivisionScreen() {
        setUpNavigation()
        searchList = allLocationsList
        nrcTable.reloadData()
    }
    
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        if allLocationsList.count > 1 {
            let searchButton = UIButton(type: .custom)
            searchButton.setImage(#imageLiteral(resourceName: "search_white").withRenderingMode(.alwaysOriginal), for: .normal)
            searchButton.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
            if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
                searchButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
            }
            else{
               searchButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
            }
            navTitleView.addSubview(searchButton)
        }
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "Select NRC".localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21.0) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}

extension NRCDivisionViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if searchList.count == 0 {
            let noDataLabel = UILabel(frame: tableView.bounds)
            if let navFont = UIFont(name: appFont, size: 21) {
                noDataLabel.font = navFont
            }
            noDataLabel.text = "No records found!".localized
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
        } else {
            tableView.backgroundView = nil
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "nrcdivisioncell", for: indexPath) as! NRCDivisionCell
        let nrcLoc = self.searchList[indexPath.row]
        var title = ""
        if let curLanguage = UserDefaults.standard.value(forKey: "currentLanguage") as? String {
            switch curLanguage {
            case "en":
                title = nrcLoc.nrcDisplayStateDivEN
            case "my":
                title = nrcLoc.nrcDisplayStateDivMY
            default:
                title = nrcLoc.nrcDisplayStateDivEN
            }
        }
        cell.wrapData(title: title, count: String(nrcLoc.townShipArray.count))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nrcLoc   =  self.searchList[indexPath.row]
        func pushToNRCTownship() {
            let story = UIStoryboard(name: "SendMoney", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "NRCTownshipView_ID") as! NRCTownshipViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if searchBar.isFirstResponder {
            DispatchQueue.main.async {
                self.searchBar.endEditing(true)
                self.resetNRCDivisionScreen()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.60) {
                    pushToNRCTownship()
                }
            }
        } else {
            pushToNRCTownship()
        }
        BankViewModel.shared.selectedNRCDivLocation = nrcLoc
        nrcTable.isUserInteractionEnabled = false
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

class NRCDivisionCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameLabel.font = UIFont.init(name: appFont, size: 15)
        countLabel.font = UIFont.init(name: appFont, size: 13)
        countLabel.layer.cornerRadius = 15
        countLabel.layer.masksToBounds = true
        countLabel.layer.borderWidth = 1.0
        countLabel.layer.borderColor = UIColor.white.cgColor //as! CGColor
        countLabel.backgroundColor = UIColor.red
        countLabel.textColor = UIColor.white
        self.countLabel.center.x += 30
        UIView.animate(withDuration: 0.5, delay: 0.5,
                       usingSpringWithDamping: 0.3,
                       initialSpringVelocity: 0.5,
                       options: [], animations: {
                        self.countLabel.center.x = screenArea.size.width - 30
        }, completion:{ _ in })
    }
    
    func wrapData(title: String, count: String) {
        self.nameLabel.text = title
        self.countLabel.text = count
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

// MARK: -UISearchBarDelegate
extension NRCDivisionViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            uiButton.setTitle("Cancel".localized, for:.normal)

            if let navFont = UIFont(name: appFont, size: 18) {
                uiButton.titleLabel?.font = navFont
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            searchList = allLocationsList
            nrcTable.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            if updatedText.count > 30 {
                return false
            }
            if updatedText != "" && text != "\n" {
                searchList = []
                if let curLanguage = UserDefaults.standard.value(forKey: "currentLanguage") as? String {
                    switch curLanguage {
                    case "en":
                        searchList = allLocationsList.filter {
                            guard let _ = ($0.stateOrDivitionNameEn).localized.localizedStandardRange(of: updatedText) else { return false }
                            return true
                        }
                    case "my":
                        searchList = allLocationsList.filter {
                            guard let _ = ($0.stateOrDivitionNameMy).localized.localizedStandardRange(of: updatedText) else { return false }
                            return true
                        }
                    default:
                        searchList = allLocationsList.filter {
                            guard let _ = ($0.stateOrDivitionNameEn).localized.localizedStandardRange(of: updatedText) else { return false }
                            return true
                        }
                    }
                } else {
                    return false
                }
                nrcTable.reloadData()
            }
        }
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        resetNRCDivisionScreen()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}
