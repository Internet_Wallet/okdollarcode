//
//  NRCPassportViewController.swift
//  OK
//
//  Created by Uma Rajendran on 12/21/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol NRCPassportViewDelegate {
    func insertNextRow()
    func removeNextRow()
    func adjustCellHeight(height: CGFloat)
    func navigateToNRCListView()
    func showNRCCodeTypesView()
    func becomeNextRow()
    func submitTappedFromAccessoryView()
}

struct PassportNumberCount {
    static let maxCount = 12
    static let minCount = 5
}

struct TitleViewHeight {
    static let whenShow: CGFloat = 30
    static let whenHide: CGFloat = 0
}

class NRCPassportViewController: UIViewController {
    
    @IBOutlet weak var backView             : UIView!
    @IBOutlet weak var mainOptionView       : UIView!
    @IBOutlet weak var numberEnteringView   : UIView!
    @IBOutlet weak var idProofNumberField   : SkyFloatingLabelTextField!
    @IBOutlet weak var idProofImgView       : UIImageView!
    var delegate: NRCPassportViewDelegate?
    
    @IBOutlet weak var titleView             : UIView!
    @IBOutlet weak var titleViewHgtConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var titleLbl             : UILabel!
    @IBOutlet var separatorLblCollection    : [UILabel]!
    @IBOutlet weak var nrcTypeBtn           : UIButton!
    @IBOutlet weak var passportTypeBtn      : UIButton!
    @IBOutlet weak var orLabel              : UILabel!
    
   fileprivate var submitView : UIView {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 50))
        let btn = UIButton.init(frame: view.frame)
        btn.backgroundColor = kYellowColor
        btn.setTitle("Submit".localized, for: .normal)
        btn.addTarget(self, action: #selector(accessorySubmitBtnAction(_ :)), for: UIControl.Event.touchUpInside)
        view.addSubview(btn)
        return view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textFieldSettings()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func loadBasicView() {
        mainOptionView.isHidden = false
        numberEnteringView.isHidden = true

        let accountDetails = self.getAccountDetailsDic()
        self.refreshNRCDetails(accountDetails: accountDetails)
        if let del = self.delegate {
            del.adjustCellHeight(height: IDProofDynamicHeight.basicHeight)
        }
    }
    
    func showNumberEnteringView(accDetails: BankAccountModel) {
        mainOptionView.isHidden = true
        numberEnteringView.isHidden = false
        var cellHight: CGFloat = 0.0
        if accDetails.isLoadFromExistingBank {
            titleView.isHidden = true
            titleViewHgtConstraint.constant = TitleViewHeight.whenHide
            cellHight = IDProofDynamicHeight.existingHeight
        } else {
            titleView.isHidden = false
            titleViewHgtConstraint.constant = TitleViewHeight.whenShow
            cellHight = IDProofDynamicHeight.selectedHeight
        }
        
        if let del = self.delegate {
            del.adjustCellHeight(height: cellHight)
        }
    }
    
    func refreshData(accDetails: BankAccountModel) {
        if accDetails.selectedIdProofType == .id_nrc {
            self.wrap_NRCData(accDetails: accDetails)
        } else {
            self.wrap_PassportData(accDetails: accDetails)
        }
    }
    
    func showNRCView(accDetails: BankAccountModel) {
        self.showNumberEnteringView(accDetails: accDetails)
        self.refreshData(accDetails: accDetails)
        self.showKeyboard()
    }
    
    func showPassportView(accDetails: BankAccountModel) {
        self.showNumberEnteringView(accDetails: accDetails)
        self.refreshData(accDetails: accDetails)
        self.showKeyboard()
    }
    
    func wrap_NRCData(accDetails: BankAccountModel) {
 
        self.idProofNumberField.text               = accDetails.accountIDProofNumber
        if accDetails.accountIDProofNumber.count > 0 {
            self.idProofNumberField.title          = "NRC Number".localized
        } else {
            self.idProofNumberField.placeholder    = "Select NRC".localized
        }
        
        self.idProofNumberField.isUserInteractionEnabled = (accDetails.isLoadFromExistingBank) ? false : true
        self.idProofImgView.image = UIImage.init(named : "sm_nrc")
    }
    
    func wrap_PassportData(accDetails: BankAccountModel) {
        self.idProofNumberField.text               = accDetails.accountIDProofNumber
        if accDetails.accountIDProofNumber.count > 0 {
            self.idProofNumberField.title          = "Passport Number".localized
        } else {
            self.idProofNumberField.placeholder    = "Enter Passport Number".localized
        }
        self.idProofNumberField.isUserInteractionEnabled = (accDetails.isLoadFromExistingBank) ? false : true
        self.idProofImgView.image = UIImage.init(named : "sm_passport")
    }
    
    func textFieldSettings() {
        
        idProofNumberField.font                    = UIFont(name: appFont, size: 15)
        idProofNumberField.titleFont               = UIFont(name: appFont, size: 13)!
        idProofNumberField.placeholderFont         = UIFont(name: appFont, size: 15)
        idProofNumberField.placeholderColor        = UIColor(red: 25.0/255.0, green: 50.0/255.0, blue: 156.0/255.0, alpha: 1.0)
        idProofNumberField.textColor               = UIColor.black
        idProofNumberField.lineColor               = UIColor.clear
        idProofNumberField.selectedLineColor       = UIColor.clear
        idProofNumberField.selectedTitleColor      = UIColor(red: 25.0/255.0, green: 50.0/255.0, blue: 156.0/255.0, alpha: 1.0)
        idProofNumberField.titleColor              = UIColor(red: 25.0/255.0, green: 50.0/255.0, blue: 156.0/255.0, alpha: 1.0)
        idProofNumberField.inputAccessoryView             =  self.submitView
        idProofNumberField.inputAccessoryView?.isHidden   =  true
        
        nrcTypeBtn.titleLabel?.font                =  UIFont(name: appFont, size: appButtonSize)
        passportTypeBtn.titleLabel?.font           =  UIFont(name: appFont, size: appButtonSize)
        titleLbl.font                              =  UIFont(name: appFont, size: appButtonSize)
        nrcTypeBtn.setTitleColor(UIColor(red: 25.0/255.0, green: 50.0/255.0, blue: 156.0/255.0, alpha: 1.0), for: .normal)
        passportTypeBtn.setTitleColor(UIColor(red: 25.0/255.0, green: 50.0/255.0, blue: 156.0/255.0, alpha: 1.0), for: .normal)
        titleLbl.textColor                         =  UIColor(red: 25.0/255.0, green: 50.0/255.0, blue: 156.0/255.0, alpha: 1.0)
        orLabel.font                               =  UIFont(name: appFont, size: 14)
        orLabel.textColor                          =  UIColor(red: 25.0/255.0, green: 50.0/255.0, blue: 156.0/255.0, alpha: 1.0)
        
        for lbl in separatorLblCollection {
            lbl.backgroundColor         =  UIColor.blue
        }
        
        titleLbl.text = "Select ID Type".localized
        nrcTypeBtn.setTitle("Select NRC".localized, for: .normal)
        passportTypeBtn.setTitle("Enter Passport Number".localized, for: .normal)
    }
    
    func showKeyboard() {
        DispatchQueue.main.async {
            self.idProofNumberField.becomeFirstResponder()
        }
    }
    
    @objc func accessorySubmitBtnAction(_ sender: UIButton) {
        println_debug("////////////// accessorySubmitBtnAction from nrc passport view ///////////")
        if let del = self.delegate {
            del.submitTappedFromAccessoryView()
        }
    }
    
    func showHideKeyboardAccessoryView(isShow: Bool) {
        idProofNumberField.inputAccessoryView?.isHidden   =  isShow ? false : true
    }
    
    @IBAction func resetBtnAction(_ sender: Any) {
        if let del = self.delegate {
            del.removeNextRow()
        }
        loadBasicView()
    }
    
    @IBAction func idProofSelection(_ sender: Any) {
        let btn = sender as! UIButton
        if btn.tag == 0 {
            // here comes when select nrc type
            // here we navigate to select nrc view
            if let del = self.delegate {
                del.navigateToNRCListView()
            }
        } else {
            // here comes when select passport number type
            // here allow the user to enter passport number in the field
            let accountDetails = self.getAccountDetailsDic()
            accountDetails.selectedIdProofType = .id_passport
            self.showPassportView(accDetails: accountDetails)
        }
    }
}

//MARK: - TextField Delegates
extension NRCPassportViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        println_debug("textFieldDidBeginEditing")
        let accountDetails = self.getAccountDetailsDic()
        if accountDetails.selectedIdProofType == .id_nrc {
            textField.keyboardType = UIKeyboardType.numberPad
        } else{
            textField.keyboardType = UIKeyboardType.default
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        println_debug("textFieldDidEndEditing")
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        println_debug("textFieldShouldBeginEditing")
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        println_debug("textFieldShouldClear")
        textField.text = ""
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        println_debug("textFieldShouldEndEditing")
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        println_debug("shouldChangeCharactersIn")
        
        let text = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        println_debug("text entering in nrc :::: \(text)")
    
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filteredStr = string.components(separatedBy: cs).joined(separator: "")
        guard string == filteredStr else {
            return false
        }
    
        let textCount = text.count
        let accountDetails = self.getAccountDetailsDic()
        
        guard !accountDetails.isLoadFromExistingBank else {
            return false
        }
        
        if accountDetails.selectedIdProofType == .id_nrc {
            if accountDetails.nrcFullCode == textField.text && string == "" {
                // show the nrc types view
                if let del = self.delegate  {
                    del.showNRCCodeTypesView()
                }
                return false
            } else {
                let remainStr = text.substring(from: accountDetails.nrcFullCode.count)
                guard remainStr.count <= 6 else {
                    return false
                }
                accountDetails.accountIDProofNumber = text
                self.wrap_NRCData(accDetails: accountDetails)
                if remainStr.count == 6 {
                    if let del = self.delegate {
                        del.insertNextRow()
                    }
                } else {
                    if let del = self.delegate {
                        del.removeNextRow()
                    }
                }
                return false
            }
        } else {
            guard textCount <= PassportNumberCount.maxCount else {
                if let del = self.delegate {
                    del.becomeNextRow()
                }
                return false
            }
            accountDetails.accountIDProofNumber = text.uppercased()
            self.wrap_PassportData(accDetails: accountDetails)
            
            if text.count >= PassportNumberCount.minCount {
                
                if let del = self.delegate {
                    del.insertNextRow()
                }
            } else {
                if let del = self.delegate {
                    del.removeNextRow()
                }
            }
            return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        println_debug("textFieldShouldReturn")
        textField.resignFirstResponder();
        return true;
    }
}
