//
//  NRCTownshipViewController.swift
//  OK
//
//  Created by Uma Rajendran on 1/23/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol NRCSelectionDelegate {
    func didSelectNRCCode()
}

struct NRCCodes {
    static let N_Code = "(N)"
    static let P_Code = "(P)"
    static let E_Code = "(E)"
    static let T_Code = "(T)"
}

class NRCTownshipViewController: SendMoneyBaseViewController {
    
    @IBOutlet weak var nrcTableView: UITableView!
    
    var allNRCTownshipList = [TownShipDetail]()
    private lazy var searchBar = UISearchBar()
    var delegate : NRCSelectionDelegate?
    private var searchList = [TownShipDetail]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        allNRCTownshipList = (BankViewModel.shared.selectedNRCDivLocation?.townShipArray)!
        self.sort(arr: allNRCTownshipList)
        searchList = allNRCTownshipList
        nrcTableView.reloadData()
        self.loadUI()
        setUpSearchBar()
        setUpNavigation()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        nrcTableView.isUserInteractionEnabled = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func sort(arr: [TownShipDetail]) {
        allNRCTownshipList = arr.sorted(by: {$0.townShipNameEN < $1.townShipNameEN})
    }

    func loadUI() {
        self.view.backgroundColor = kBackGroundGreyColor
        self.nrcTableView.backgroundColor = kBackGroundGreyColor
        nrcTableView.tableFooterView = UIView()
    }
    
    func setUpSearchBar() {
        searchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44)
        if #available(iOS 11.0, *) {
            searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.tintColor = UIColor.white
        searchBar.placeholder = "Search".localized
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
            searchTextField.keyboardType = .asciiCapable
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
                searchTextField.keyboardType = .asciiCapable
            }
        }
        let view = self.searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = UIColor.gray
            }
        }
    }
    
    @objc func searchAction() {
        searchBar.placeholder = "Search".localized
        searchBar.text = ""
        self.navigationItem.titleView = searchBar
        searchBar.becomeFirstResponder()
    }
    
    func resetNRCTownshipScreen() {
        setUpNavigation()
        searchList = allNRCTownshipList
        nrcTableView.reloadData()
    }
    
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        if allNRCTownshipList.count > 1 {
            let searchButton = UIButton(type: .custom)
            searchButton.setImage(#imageLiteral(resourceName: "search_white").withRenderingMode(.alwaysOriginal), for: .normal)
            searchButton.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
            if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
                searchButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
            }
            else{
               searchButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
            }
            navTitleView.addSubview(searchButton)
        }
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        if let navFont = UIFont(name: appFont, size: 21.0) {
            label.font = navFont
        }
        label.text =  "Select NRC Code".localized + " "
        label.textColor = UIColor.white
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}

extension NRCTownshipViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if searchList.count == 0 {
            let noDataLabel = UILabel(frame: tableView.bounds)
            if let navFont = UIFont(name: appFont, size: 21) {
                noDataLabel.font = navFont
            }
            noDataLabel.text = "No records found!".localized
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
        } else {
            tableView.backgroundView = nil
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "nrctownshipcell", for: indexPath) as! NRCTownshipCell
        let nrcTownship = self.searchList[indexPath.row]
        var code = ""
        if let curLanguage = UserDefaults.standard.value(forKey: "currentLanguage") as? String {
            switch curLanguage {
            case "en":
                code = nrcTownship.nrcDisplayCodeEN
            case "my":
                code = nrcTownship.nrcDisplayCodeMY
            default:
                code = nrcTownship.nrcDisplayCodeEN
            }
        }
        cell.wrapData(title: code)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nrcTownship   =  self.searchList[indexPath.row]
        func getDataAndPopUp() {
            BankViewModel.shared.selectedNRCTownship = nrcTownship
            let accountDetails = getAccountDetailsDic()
            accountDetails.selectedIdProofType      = .id_nrc
            accountDetails.nrcCodeNumber            = nrcTownship.nrcCodeNumber
            accountDetails.nrcCode                  = nrcTownship.nrcCode
            accountDetails.nrcFullCode              = "\(nrcTownship.nrcCodeNumber)/\(nrcTownship.nrcCode)\(NRCCodes.N_Code)"
            accountDetails.accountIDProofNumber     = accountDetails.nrcFullCode
            
            DispatchQueue.main.async {
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: SendMoneyToBankViewController.self) {
                        self.delegate = controller as? NRCSelectionDelegate
                        self.delegate?.didSelectNRCCode()
                        self.navigationController?.popToViewController(controller, animated: false)
                        break
                    }
                }
            }
        }
        if searchBar.isFirstResponder {
            DispatchQueue.main.async {
                self.searchBar.endEditing(true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.60) {
                    getDataAndPopUp()
                }
            }
        } else {
            getDataAndPopUp()
        }
        nrcTableView.isUserInteractionEnabled = false
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

class NRCTownshipCell : UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameLabel.font = UIFont.init(name: appFont, size: 15)
    }

    func wrapData(title: String) {
        self.nameLabel.text = title
     }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

// MARK: - UISearchBarDelegate
extension NRCTownshipViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            uiButton.setTitle("Cancel".localized, for:.normal)

            if let navFont = UIFont(name: appFont, size: appButtonSize) {
                uiButton.titleLabel?.font = navFont
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            searchList = allNRCTownshipList
            nrcTableView.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            if updatedText.count > 30 {
                return false
            }
            if updatedText != "" && text != "\n" {
                searchList = []
                if let curLanguage = UserDefaults.standard.value(forKey: "currentLanguage") as? String {
                    switch curLanguage {
                    case "en":
                        searchList = allNRCTownshipList.filter {
                            guard let _ = ($0.townShipNameEN).localized.localizedStandardRange(of: updatedText) else { return false }
                            return true
                        }
                    case "my":
                        searchList = allNRCTownshipList.filter {
                            guard let _ = ($0.townShipNameMY).localized.localizedStandardRange(of: updatedText) else { return false }
                            return true
                        }
                    default:
                        searchList = allNRCTownshipList.filter {
                            guard let _ = ($0.townShipNameEN).localized.localizedStandardRange(of: updatedText) else { return false }
                            return true
                        }
                    }
                } else {
                    return false
                }
                nrcTableView.reloadData()
            }
        }
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        resetNRCTownshipScreen()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}
