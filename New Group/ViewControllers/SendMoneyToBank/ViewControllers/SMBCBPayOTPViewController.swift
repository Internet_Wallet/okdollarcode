//
//  SMBCBPayOTPViewController.swift
//  OK
//
//  Created by Kethan Kumar on 17/02/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class SMBCBPayOTPViewController: OKBaseController {
    var validation: Result?
    var timer = Timer()
    var seconds = 299
    var cbBankTokenModel: CBBankTokenModel?
    
    @IBOutlet weak var backGroundImageView: UIImageView!
    
    @IBOutlet weak var otpStackView: UIStackView!
    @IBOutlet weak var otpLabel: UILabel! {
        didSet {
            self.otpLabel.text = "Enter above \"OTP\" \nOne Time Password \n in CB Pay App within".localized
        }
    }
    @IBOutlet weak var minutesLabel: UILabel!
    @IBOutlet weak var playLabel: UILabel! {
        didSet {
            self.playLabel.text = "Watch below video to learn \nhow to enter \"OTP\" in CB Pay App".localized
        }
    }
    @IBOutlet weak var minLabel: UILabel!
    @IBOutlet weak var continueBtn: UIButton!
    
        var previousTime = Date()
       var currentTime = Date()
       var previoushour = 0
       var previousMinute = 0
       var previousSecond = 0
       var currentHour = 0
       var currentMinute = 0
       var currentSecond = 0
       var wentBackGround = false
       var formatter = DateFormatter()
    var showAlert = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Send Money to Bank".localized
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "hh:mm:ss"
        NotificationCenter.default.addObserver(self, selector: #selector(moveToBackGround), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(enterForgound), name: UIApplication.willEnterForegroundNotification, object: nil)
        self.updateUI()
    }
    
    
    @objc func moveToBackGround() {
        let date = Date()
        previousTime = date
        
        let str = formatter.string(from: previousTime)
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: formatter.date(from: str) ?? Date())
        let minutes = calendar.component(.minute, from: formatter.date(from: str) ?? Date())
        let second = calendar.component(.second, from: formatter.date(from: str) ?? Date())
        previoushour = hour
        previousMinute = minutes
        previousSecond = second
        wentBackGround = true
    }
    
    @objc func enterForgound() {
        if wentBackGround {
            diffTime()
            wentBackGround = false
        }
    }
    
    
    func diffTime() {
          let str = formatter.string(from: Date())
          let calendar = Calendar.current
          let hour = calendar.component(.hour, from: formatter.date(from: str) ?? Date())
          let minutes = calendar.component(.minute, from: formatter.date(from: str) ?? Date())
          let second = calendar.component(.second, from: formatter.date(from: str) ?? Date())
          currentHour = hour
          currentMinute = minutes
          currentSecond = second
          
          if currentHour < previoushour {
              currentMinute = currentMinute + 60
          }
          if currentMinute < previousMinute {
              currentSecond = currentSecond + 60
          }
          let finalMin = currentMinute - previousMinute
          let finalSecond = currentSecond - previousSecond
          seconds =  seconds - (finalMin * 60 + finalSecond)
      }
    
    
    @IBAction func youTubePlayAction(_ sender: UIButton) {
        if let youtubeurl = validation?.okData.demoURL {
            guard let url = URL(string: youtubeurl) else { return }
            UIApplication.shared.open(url)
        }
    }

    @IBAction func continueAction(_ sender: UIButton) {
     
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: nil, body: "Did you enter OTP in CB Pay App?".localized, img: #imageLiteral(resourceName: "smToBank"))
            alertViewObj.addAction(title: "Yes".localized, style: .target , action: {
                self.timer.invalidate()
                self.seconds = 299
                self.generateToken()
            })
            alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func runTimer() {
         timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        seconds -= 1
        let min = seconds / 60 % 60
        let sec = seconds % 60
        if sec < 10 {
            if seconds < 0 {
                minutesLabel.text = "00:00"
            }else {
                minutesLabel.text = "\(min):0\(sec)"
            }
        } else {
            minutesLabel.text = "\(min):\(sec)"
        }
         
        if seconds == 0 || seconds < 0{
            self.timer.invalidate()
            self.seconds = 299
            DispatchQueue.main.async {
                alertViewObj.wrapAlert(title: nil, body: "OTP already expired".localized, img: #imageLiteral(resourceName: "smToBank"))
                alertViewObj.addAction(title: "Request New OTP".localized, style: .target , action: {
                   // self.runTimer()
                    self.showAlert = false
                    self.continueBtn.isEnabled = true
                    self.generateToken()
                })
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.timer.invalidate()
        self.seconds = 299
    }
    
    func navigateToConfirmationScreen() {
        DispatchQueue.main.async {
            if let vc = UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "BankConfirmation_ViewID") as? BankConfirmationViewController {
                vc.cbBankTokenModel = self.cbBankTokenModel
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    fileprivate func updateUI() {
        minutesLabel.text = "\(4):\(59)"
        self.runTimer()
        let id = validation?.okData.demoURL.components(separatedBy: "v=")
        let youTId = id?.last?.components(separatedBy: "&").first ?? ""
        self.backGroundImageView.downloadedFrom(link: "http://img.youtube.com/vi/\(youTId)/0.jpg", contentMode: .scaleAspectFill)
        let subviews = self.otpStackView.subviews
        var count = 0
        guard let otp = validation?.otp else { return }
        for view in subviews {
            if let label = view as? UILabel {
                if otp.count > count {
                    label.text = "\(otp[count])"
                    count += 1
                }
            }
        }
        self.continueBtn.isEnabled = true
    }
    
    //MARK:- CB Bank token and auth API
    private func generateToken() {
        
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            
            let urlString = String.init(format: Url.cbGenerateToken)
            let urlStr = getUrl(urlStr: urlString, serverType: .CBBankPay)
            
            let TICKS_AT_EPOCH: Int64 = 621355968000000000
            let TICKS_PER_MILLISECOND: Int64 = 10000
            var timeInterval = DateComponents()
            timeInterval.hour = 6
            timeInterval.minute = 30
            let futureDate = Calendar.current.date(byAdding: timeInterval, to: Date())!
            let currentTime = futureDate.millisecondsSince1970
            let getUTCTicks =  (currentTime * TICKS_PER_MILLISECOND) + TICKS_AT_EPOCH
            let aKey = Url.cbBank_aKey + "\(getUTCTicks)"
            do {
                let hashKey = try Encryption.encryptData(plainText: aKey, hexKey: Url.cbBank_sKey)
                let params = ["password": hashKey]
                web.genericClass(url: urlStr, param: params as AnyObject, httpMethod: "POST", mScreen: "CBBankTokenGenerate")
            } catch {
                
            }
        } else {
            println_debug("No Internet")
        }
    }
    
    private func getCBBankParams() -> Dictionary<String, Any> {
            let bankAccountModel = getAccountDetailsDic()
            var params1 = Dictionary<String, Any>()
            let phNum = bankAccountModel.contactMobileNumber.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "")
            let phNo = "0095\(String(phNum.dropFirst()))"
            params1["ContactPhoneNumber"] =  phNo
            params1["Nrc"] = UserModel.shared.nrc.replacingOccurrences(of: "@", with: "/")
            params1["UserName"] = bankAccountModel.accountHolderName
            params1["BankAccountNumber"] = bankAccountModel.accountNumber
            params1["UserLevel"]  =  "Level-1"
            params1["WalletReferenceId"]  =  UserModel.shared.mobileNo
            return params1
        }
    
    func wrapDataModel() -> Data? {
        let bankAccountModel = getAccountDetailsDic()
        let phNum = bankAccountModel.contactMobileNumber.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "")
        let phNo = "0095\(String(phNum.dropFirst()))"
        let nrc = UserModel.shared.nrc.replacingOccurrences(of: "@", with: "/")
        
        let model = BackAssociationModel.init(bankAccountNumber: bankAccountModel.accountNumber, contactPhoneNumber: phNo, nrc: nrc, userLevel: "Level-1", userName: UserModel.shared.name, walletReferenceID: UserModel.shared.mobileNo)
        println_debug(model)
        do {
            let data  =  try JSONEncoder().encode(model)
            println_debug(String(data: data, encoding: .utf8))
            return data
        } catch {
            println_debug(error)
        }
        return Data.init()
    }
    
    private func cbBankUpDate(model: CBBankTokenModel) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            // hit here association API
            let urlString = String.init(format: Url.cbBankAssociation)
            let urlStr = getUrl(urlStr: urlString, serverType: .CBBankPay)
            println_debug("cbBankAssociation : \(urlStr)")
            let data = wrapDataModel()
            println_debug(String(data: data ?? Data(), encoding: .utf8))
            let dic = Dictionary<String,Any>()
            web.genericClass(url: urlStr, param: dic as AnyObject, httpMethod: "POST", mScreen: "CBBankValidation", hbData: data, authToken:model.data?.token , authTokenKey: "X-access-token")
        } else {
            println_debug("No Internet")
        }
    }

    fileprivate func nRCMismatchAlert(message: String) {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: nil, body: message.localized, img: #imageLiteral(resourceName: "smToBank"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                self.cbBankTokenModel = nil
                self.navigateToConfirmationScreen()
            })
            alertViewObj.addAction(title: "Cancel".localized.localized, style: .cancel , action: {
                
            })
            alertViewObj.showAlert(controller: self)
        }
    }
}


extension SMBCBPayOTPViewController: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        if screen == "CBBankValidation" {
            guard let castedData = json as? Data else {
                PTLoader.shared.hide()
                println_debug("Data error")
                return
            }
            do {
                PTLoader.shared.hide()
                let decoder = JSONDecoder()
                let validation = try decoder.decode(CBBankValidation.self, from: castedData)
                println_debug(validation)
                if validation.code == 200 {
                    if validation.data.statusCode == 400 && validation.data.message ?? "" == "User already associated with bank account." {
                        self.navigateToConfirmationScreen()
                    }else {
                        if validation.data.statusCode == 201 && validation.data.status == true {
                            if self.showAlert {
                                DispatchQueue.main.async {
                                    alertViewObj.wrapAlert(title: nil, body: "\"OTP\" still not verified. \nNew \"OTP\" already generated \nPlease check and enter in \nCB Pay App".localized, img: #imageLiteral(resourceName: "smToBank"))
                                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                        self.validation = validation.data.result
                                        self.updateUI()
                                    })
                                    alertViewObj.showAlert(controller: self)
                                }
                            }else {
                                self.showAlert = true
                                DispatchQueue.main.async {
                                    self.validation = validation.data.result
                                    self.updateUI()
                                }
                            }
                        } else {
                            if validation.data.message?.lowercased().contains(find: "nrc") ?? false {
                                showErrorAlert(errMessage: "Please update your NRC detail in OK$ Update Profile".localized)
                            } else if validation.data.message?.lowercased().contains(find: "invalid bank account") ?? false {
                                showErrorAlert(errMessage: "Your bank account number is not valid please check your account number and try again".localized)
                            }else {
                                showErrorAlert(errMessage: validation.data.message ?? "")
                            }
                            
                        }
                    }
                }else {
                    //show server messgae
                }
            } catch {
                
            }
        } else if screen == "CBBankTokenGenerate" {
            guard let castedData = json as? Data else {
                PTLoader.shared.hide()
                println_debug("Data error")
                return
            }
            do {
                PTLoader.shared.hide()
                let decoder = JSONDecoder()
                let validation1 = try decoder.decode(CBBankTokenModel.self, from: castedData)
                cbBankTokenModel = validation1
                if validation1.code == 200 {
                    self.cbBankUpDate(model: validation1)
                } else {
                    showErrorAlert(errMessage: "Please try after some time".localized)
                }
            } catch {
                
            }
            
        }
    }
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        PTLoader.shared.hide()
        if screenName == "CBBankValidation" {
            println_debug("Api Failure in PhNoValidation response")
        } else if screenName == "CBBankTokenGenerate" {
            println_debug("Please try after some time")
        }
    }
}
