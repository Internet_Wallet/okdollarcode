//
//  ContactSuggestionVC.swift
//  OK
//
//  Created by Uma Rajendran on 2/3/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Contacts

protocol ContactSuggestionDelegate {
    func didSelectFromContactSuggession(number: Dictionary<String, Any>)
}

struct ContactSuggessionKeys {
    static let contactname          = "name"
    static let phonenumber_backend  = "phonenumber"
    static let phonenumber_onscreen = "phonenumberUI"
    static let operatorname         = "operatorname"
}

class ContactSuggestionVC: UIViewController {

    @IBOutlet weak var contactsTable   : UITableView!
    @IBOutlet weak var backView        : CardDesignView!
    var contactsList                   : [Dictionary<String, Any>]?
    var delegate                       : ContactSuggestionDelegate?
   
    override func viewDidLoad() {
        super.viewDidLoad()
        contactsTable.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
}

extension ContactSuggestionVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let list = contactsList {
            return list.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactcellidentifier") ?? UITableViewCell(style: .subtitle, reuseIdentifier: "contactcellidentifier")
        if let contact = contactsList?[indexPath.row] {
            if let mainText = contact[ContactSuggessionKeys.operatorname] as? String{
                if mainText.count > 0 {
                    cell.textLabel?.text = "\(contact[ContactSuggessionKeys.contactname]!) (\(contact[ContactSuggessionKeys.operatorname]!))"
                } else {
                    cell.textLabel?.text = "\(contact[ContactSuggessionKeys.contactname]!)"
                }
            }
            cell.detailTextLabel?.text = contact[ContactSuggessionKeys.phonenumber_onscreen] as? String
            cell.textLabel?.textColor = UIColor.black
            cell.detailTextLabel?.textColor = UIColor.darkGray
            cell.textLabel?.font = UIFont(name: appFont, size: 15)
            cell.detailTextLabel?.font = UIFont(name: appFont, size: 14)
            return cell
        }
       return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let contact = contactsList?[indexPath.row]  {
            if let del = self.delegate {
                del.didSelectFromContactSuggession(number: contact)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
