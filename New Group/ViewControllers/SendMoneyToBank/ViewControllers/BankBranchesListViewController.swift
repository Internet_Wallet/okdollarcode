//
//  BankBranchesListViewController.swift
//  OK
//
//  Created by Uma Rajendran on 12/21/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BankBranchesListViewController: SendMoneyBaseViewController {
   
    @IBOutlet weak var branchListTable: UITableView!
    @IBOutlet weak var branchCountLabel: UILabel!
    
    var branchList   = [BankBranch]()
    var branchListDB   = [BankBranchAddress]()

    override func viewDidLoad() {
        super.viewDidLoad()
        loadInitialization()
        loadUI()
        setUpNavigation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        branchListTable.isUserInteractionEnabled = true
    }
    
    func loadUI() {
        self.view.backgroundColor               = kBackGroundGreyColor
        self.branchListTable.backgroundColor    = kBackGroundGreyColor
        self.branchListTable.register(UINib(nibName: "BankBranchCell", bundle: nil), forCellReuseIdentifier: "branchcellidentifier")
        self.branchListTable.tableFooterView = UIView(frame: CGRect.zero)
        self.branchListTable.rowHeight = UITableView.automaticDimension
        self.branchListTable.estimatedRowHeight = 250
        self.branchListTable.reloadData()
    }
    
    func loadInitialization() {
        PaymentVerificationManager.getAllBranchesFromDB(townshipCode: BankViewModel.shared.selectedTownshipDB?.townshipCode, divisionCode: BankViewModel.shared.selectedDivisionDB?.divisionCode, bankId: BankViewModel.shared.selectedBankDB?.bankId) { (status, branches) in
            DispatchQueue.main.async {
                if status, let branch = branches {
                    self.branchListDB = branch.sorted(by: {($0.branchEName ?? "") < ($1.branchEName ?? "")})
                } else {
                    
                }
                if let myFont = UIFont(name: appFont, size: 14)  {
                    self.branchCountLabel.font = myFont
                }
                self.branchCountLabel.text = "\(self.branchListDB.count) \(self.branchSuffix(count: self.branchListDB.count))"
                self.branchListTable.reloadData()
            }
        }
//        branchList   =  (BankViewModel.shared.selectedTownship?.branchList)!
//        sort(arr: branchList)
//        if let myFont = UIFont(name: "Zawgyi-One", size: 14)  {
//            branchCountLabel.font = myFont
//        }
//        branchCountLabel.text = "\(branchList.count) \(branchSuffix(count: branchList.count))"
    }
    
    func branchSuffix(count: Int) -> String {
        return (count > 1) ? "Branches Available".localized : "Branch Available".localized
    }
    
    func sort(arr: [BankBranch]) {
        branchList = arr.sorted(by: {$0.branchNameEN < $1.branchNameEN})
    }
    
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "Select Branch".localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21.0) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
    
    func wrappingEnglish(branchcell: BankBranchCell, branch: BankBranchAddress) {
        var address = ""
        if (branch.branchEAddress?.count ?? 0) > 5 {
            address = branch.branchEName ?? ""
            if (branch.branchEAddress?.count ?? 0) > 0 {
                address += ",\n\(branch.branchEAddress ?? "")"
            }
            if let twnShip = BankViewModel.shared.selectedTownshipDB?.townshipEName {
                if twnShip.count > 0 {
                    address += ",\n\(twnShip)"
                }
            }
            if let dvsnName = BankViewModel.shared.selectedDivisionDB?.divisionEName {
                if dvsnName.count > 0 {
                    address += ", \(dvsnName)"
                }
            }
            if (branch.phonenumber?.count ?? 0) > 0 {
                address += ",\n\(branch.phonenumber ?? "")"
            }
        } else {
            address = "\(branch.branchEName ?? "")"
            if let twnShip = BankViewModel.shared.selectedTownshipDB?.townshipEName {
                if twnShip.count > 0 {
                    address += ",\n\(twnShip)"
                }
            }
            if let dvsnName = BankViewModel.shared.selectedDivisionDB?.divisionEName {
                if dvsnName.count > 0 {
                    address += ", \(dvsnName)"
                }
            }
            if (branch.phonenumber?.count ?? 0) > 0 {
                address += ",\n\(branch.phonenumber ?? "")"
            }
        }
        let bankname = BankViewModel.shared.selectedBankDB?.bankName ?? ""
        branchcell.wrapData(bankName: bankname, branchAddress: address)
    }
    
    func wrappingMyanmar(branchcell: BankBranchCell, branch: BankBranchAddress) {
        var address = ""
        if (branch.branchBAddress?.count ?? 0) > 5 {
            
            address = branch.branchBName ?? ""
            if (branch.branchBAddress?.count ?? 0) > 0 {
                address += ",\n\(branch.branchBAddress ?? "")"
            }
            if let twnShip = BankViewModel.shared.selectedTownshipDB?.townshipBName {
                if twnShip.count > 0 {
                    address += ",\n\(twnShip)"
                }
            }
            if let dvsnName = BankViewModel.shared.selectedDivisionDB?.divisionBName {
                if dvsnName.count > 0 {
                    address += ", \(dvsnName)"
                }
            }
            if (branch.phonenumber?.count ?? 0)  > 0 {
                address += ",\n\(branch.phonenumber ?? "")"
            }
        } else {
            address = branch.branchBName ?? ""
            if let twnShip = BankViewModel.shared.selectedTownshipDB?.townshipBName {
                if twnShip.count > 0 {
                    address += ",\n\(twnShip)"
                }
            }
            if let dvsnName = BankViewModel.shared.selectedDivisionDB?.divisionBName {
                if dvsnName.count > 0 {
                    address += ", \(dvsnName)"
                }
            }
            if (branch.phonenumber?.count ?? 0) > 0 {
                address += ",\n\(branch.phonenumber ?? "")"
            }
        }
        let bankname = BankViewModel.shared.selectedBankDB?.burmeseName ?? ""
        branchcell.wrapData(bankName: bankname, branchAddress: address)
    }
    
    func wrappingMyanmarUnicode(branchcell: BankBranchCell, branch: BankBranchAddress) {
        var address = ""
        if (branch.branchUAddress?.count ?? 0) > 5 {
            
            address = branch.branchUName ?? ""
            if (branch.branchUAddress?.count ?? 0) > 0 {
                address += ",\n\(branch.branchUAddress ?? "")"
            }
            if let twnShip = BankViewModel.shared.selectedTownshipDB?.townshipUName {
                if twnShip.count > 0 {
                    address += ",\n\(twnShip)"
                }
            }
            if let dvsnName = BankViewModel.shared.selectedDivisionDB?.divisionUName {
                if dvsnName.count > 0 {
                    address += ", \(dvsnName)"
                }
            }
            if (branch.phonenumber?.count ?? 0)  > 0 {
                address += ",\n\(branch.phonenumber ?? "")"
            }
        } else {
            address = branch.branchUName ?? ""
            if let twnShip = BankViewModel.shared.selectedTownshipDB?.townshipUName {
                if twnShip.count > 0 {
                    address += ",\n\(twnShip)"
                }
            }
            if let dvsnName = BankViewModel.shared.selectedDivisionDB?.divisionUName {
                if dvsnName.count > 0 {
                    address += ", \(dvsnName)"
                }
            }
            if (branch.phonenumber?.count ?? 0) > 0 {
                address += ",\n\(branch.phonenumber ?? "")"
            }
        }
        let bankname = BankViewModel.shared.selectedBankDB?.unicodeName ?? ""
        branchcell.wrapData(bankName: bankname, branchAddress: address)
    }
}


extension BankBranchesListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return branchListDB.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let branchcell = tableView.dequeueReusableCell(withIdentifier: "branchcellidentifier", for: indexPath) as! BankBranchCell
        let branch = branchListDB[indexPath.row] as BankBranchAddress
        if let curLanguage = UserDefaults.standard.value(forKey: "currentLanguage") as? String {
            switch curLanguage {
            case "en":
                self.wrappingEnglish(branchcell: branchcell, branch: branch)
            case "my":
                self.wrappingMyanmar(branchcell: branchcell, branch: branch)
            case "uni":
                self.wrappingMyanmarUnicode(branchcell: branchcell, branch: branch)
            default:
                self.wrappingEnglish(branchcell: branchcell, branch: branch)
            }
        }
        return branchcell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let branch = branchListDB[indexPath.row] as BankBranchAddress
        let story = UIStoryboard(name: "SendMoney", bundle: nil)
        let branchView = story.instantiateViewController(withIdentifier: "BankAccountTypeView_ID") as! BankAccountTypeViewController
        BankViewModel.shared.selectedBranchDB = branch
        self.navigationController?.pushViewController(branchView, animated: true)
        branchListTable.isUserInteractionEnabled = false
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 250
        tableView.rowHeight = UITableView.automaticDimension
        return tableView.rowHeight
    }
}
