//
//  SendMoneyToBankCoreDataHelper.swift
//  OK
//
//  Created by Uma Rajendran on 12/27/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CoreDataHelper {

 
    class var shared: CoreDataHelper {
        struct Static {
            static let instance = CoreDataHelper()
        }
        return Static.instance
    }
 
    func accountCount_ForThisAccount(accDetails: BankAccountModel) -> Int {
        if accDetails.isMyAccount {
            return myAccBanks_DBCount()
        }else {
            return otherAccBanks_DBCount()
        }
    }
    
    func clearSMBFromDB() {
        let managedContext  =  appDelegate.persistentContainer.viewContext
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: "SendMoneyToBank")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchReq)
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
        }
    }
    
    func myAccBanks_DBCount() -> Int {
        let managedContext        = appDelegate.persistentContainer.viewContext
        let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "SendMoneyToBank")
        do {
            let predicate1        = NSPredicate(format: "isMyAccount == %@", NSNumber(value: true) )
            let predicate2        = NSPredicate(format: "isBankActive == %@", NSNumber(value: true))
            let andPredicate      = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2])
            fetchReq.predicate    = andPredicate
            let fetchedObjects    = try managedContext.fetch(fetchReq) as! [SendMoneyToBank]
            return fetchedObjects.count
         }catch {
            print ("fetch task failed", error)
        }
        return 0
    }

    func otherAccBanks_DBCount() -> Int {
        let managedContext          = appDelegate.persistentContainer.viewContext
        let fetchReq                = NSFetchRequest<NSManagedObject>(entityName: "SendMoneyToBank")
        do {
            let predicate1        = NSPredicate(format: "isMyAccount == %@", NSNumber(value: false) )
            let predicate2        = NSPredicate(format: "isBankActive == %@", NSNumber(value: true))
            let andPredicate      = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2])
            fetchReq.predicate    = andPredicate
            let fetchedObjects      = try managedContext.fetch(fetchReq) as! [SendMoneyToBank]
            return fetchedObjects.count
        }catch {
            print ("fetch task failed", error)
        }
        return 0
    }

    func totalBanksCount() -> Int {
        let managedContext          = appDelegate.persistentContainer.viewContext
        let fetchReq                = NSFetchRequest<NSManagedObject>(entityName: "SendMoneyToBank")
        do {
            let fetchedObjects      = try managedContext.fetch(fetchReq) as! [SendMoneyToBank]
            return fetchedObjects.count
        }catch {
            print ("fetch task failed", error)
        }
        return 0
    }
    
    func totalActiveBanksCount() -> Int {
        let managedContext          = appDelegate.persistentContainer.viewContext
        let fetchReq                = NSFetchRequest<NSManagedObject>(entityName: "SendMoneyToBank")
        do {
            fetchReq.predicate      = NSPredicate(format: "isBankActive == %@", NSNumber(value: true) )
            let fetchedObjects      = try managedContext.fetch(fetchReq) as! [SendMoneyToBank]
            return fetchedObjects.count
        }catch {
            print ("fetch task failed", error)
        }
        return 0
    }

    func makeBankActive(accDetail: BankAccountModel)  {
        
        let managedContext        = appDelegate.persistentContainer.viewContext
        let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "SendMoneyToBank")
        do {
            let predicate1        = NSPredicate(format: "accountNumber == %@", accDetail.accountNumber)
            let predicate2        = NSPredicate(format: "bankId  == %@", NSNumber(value: accDetail.bankId))
            let predicate3        = NSPredicate(format: "branchId == %@", NSNumber(value: accDetail.branchId))
            let andPredicate      = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2, predicate3])
            fetchReq.predicate    = andPredicate
            let fetchedObjects    = try managedContext.fetch(fetchReq) as! [SendMoneyToBank]
            if fetchedObjects.count > 0 {
                accDetail.isBankActive = true
                self.wrap_BankData(accDetail: accDetail, bank: fetchedObjects[0])
                return
            }
        } catch {
            print ("fetch task failed", error)
        }
    }
    
    func wrap_BankData(accDetail: BankAccountModel, bank: SendMoneyToBank) {
        bank.accountHolderName      = accDetail.accountHolderName
        bank.accountIDProofNumber   = accDetail.accountIDProofNumber
        bank.accountFormat          = accDetail.accountFormat
        bank.accountNumber          = accDetail.accountNumber
        bank.accountType            = accDetail.accountType
        bank.amount                 = accDetail.amount
        bank.bankAddressEN          = accDetail.branchAddress
        bank.bankAddressMY          = accDetail.branchBurmeseAddress
        bank.bankId                 = Int64(accDetail.bankId)
        bank.bankNameEN             = accDetail.bankName
        bank.bankNameMY             = accDetail.bankBurmeseName
        bank.branchId               = Int64(accDetail.branchId)
        bank.branchNameEN           = accDetail.branchName
        bank.branchNameMY           = accDetail.branchBurmeseName
        bank.bankPhoneNumber        = accDetail.bankPhoneNumber
        bank.bankFaxNumber          = accDetail.bankFaxNumber
        bank.contactMobileNumber    = accDetail.contactMobileNumber
        bank.emailId                = accDetail.emailid
        bank.isBankActive           = accDetail.isBankActive
        bank.isMyAccount            = accDetail.isMyAccount
        bank.remarks                = accDetail.remarks
        bank.serverBankId           = accDetail.serverBankId // later we will insert using the api response
        bank.stateCode              = accDetail.stateCode
        bank.stateNameEN            = accDetail.stateName
        bank.stateNameMY            = accDetail.stateBurmeseName
        bank.townshipCode           = accDetail.townshipCode
        bank.townshipNameEN         = accDetail.townshipName
        bank.townshipNameMY         = accDetail.townshipBurmeseName
        bank.isIdProofNRC           = (accDetail.selectedIdProofType == .id_nrc) ? true : false
        bank.bankNameUN             = accDetail.bankBurmeseNameUnicode
        bank.townshipNameUN         = accDetail.townshipBurmeseNameUnicode
        bank.stateNameUN            = accDetail.stateBurmeseNameUnicode
        bank.branchNameUN           = accDetail.branchBurmeseNameUnicode
        bank.bankAddressUN          = accDetail.branchBurmeseAddressUnicode
        bank.dateTimeToAddBank      = Date()
        self.saveContext()
        
    }
    
    func saveContext() {
        let managedContext  =  appDelegate.persistentContainer.viewContext
        do {
            try managedContext.save()
        } catch let error as NSError {
            println_debug("could not save :::: \(error)  ,,, \(error.description) ,,, \(error.userInfo)")
        }
    }
    
    
    func saveNewBankDetailsInDB(accDetail: BankAccountModel) {
        let managedContext  =  appDelegate.persistentContainer.viewContext
        let entity      =  NSEntityDescription.entity(forEntityName: "SendMoneyToBank", in: managedContext)!
        let bank        =  NSManagedObject(entity: entity, insertInto: managedContext) as! SendMoneyToBank
        self.wrap_BankData(accDetail: accDetail, bank: bank)
    }
    
    func updateBankInDB(accDetail: BankAccountModel) {
        let managedContext        = appDelegate.persistentContainer.viewContext
        let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "SendMoneyToBank")
        do {
            let predicate1        = NSPredicate(format: "accountNumber == %@", accDetail.accountNumber)
            let predicate2        = NSPredicate(format: "bankId  == %@", NSNumber(value: accDetail.bankId))
            let predicate3        = NSPredicate(format: "branchId == %@", NSNumber(value: accDetail.branchId))
            let andPredicate      = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2, predicate3])
            fetchReq.predicate    = andPredicate
            let fetchedObjects    = try managedContext.fetch(fetchReq) as! [SendMoneyToBank]
            if fetchedObjects.count > 0 {
                for bank in fetchedObjects {
                    bank.amount         =  accDetail.amount
                    bank.emailId        =  accDetail.emailid
                    bank.remarks        =  accDetail.remarks
                    break
                }
                
                do {
                    try managedContext.save()
                } catch let error as NSError {
                    println_debug("could not save :::: \(error)  ,,, \(error.description) ,,, \(error.userInfo)")
                }
            }
        }catch {
            print ("fetch task failed", error)
        }
     }
    
    func myAcc_BanksFromDB() -> [SendMoneyToBank]? {
        let managedContext        = appDelegate.persistentContainer.viewContext
        let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "SendMoneyToBank")
        do {
            let predicate1        = NSPredicate(format: "isMyAccount == %@", NSNumber(value: true) )
            let predicate2        = NSPredicate(format: "isBankActive == %@", NSNumber(value: true))
            let andPredicate      = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2])
            fetchReq.predicate    = andPredicate
            let sortDescriptors   = [NSSortDescriptor(key: "dateTimeToAddBank", ascending: false)]
            fetchReq.sortDescriptors = sortDescriptors
            let fetchedObjects    = try managedContext.fetch(fetchReq) as! [SendMoneyToBank]
            return fetchedObjects
        } catch {
            print ("fetch task failed", error)
        }
        return nil
    }
    
    func otherAcc_BanksFromDB()  -> [SendMoneyToBank]? {
        let managedContext        = appDelegate.persistentContainer.viewContext
        let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "SendMoneyToBank")
        do {
            let predicate1        = NSPredicate(format: "isMyAccount == %@", NSNumber(value: false) )
            let predicate2        = NSPredicate(format: "isBankActive == %@", NSNumber(value: true))
            let andPredicate      = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2])
            fetchReq.predicate    = andPredicate
            let sortDescriptors   = [NSSortDescriptor(key: "dateTimeToAddBank", ascending: false)]
            fetchReq.sortDescriptors = sortDescriptors
            let fetchedObjects    = try managedContext.fetch(fetchReq) as! [SendMoneyToBank]
            return fetchedObjects
        } catch {
            print ("fetch task failed", error)
        }
        return nil
    }
    
    func isBankExistsInMyOrOther(accDetail : BankAccountModel) -> Bool {
        let managedContext        = appDelegate.persistentContainer.viewContext
        let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "SendMoneyToBank")
        do {
            let predicate1        = NSPredicate(format: "accountNumber == %@", accDetail.accountNumber)
            let predicate2        = NSPredicate(format: "bankId  == %@", NSNumber(value: accDetail.bankId))
            let predicate3        = NSPredicate(format: "branchId == %@", NSNumber(value: accDetail.branchId))
            let predicate4        = NSPredicate(format: "isBankActive == %@", NSNumber(value: true))
            
            let andPredicate      = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2, predicate3, predicate4])
            fetchReq.predicate    = andPredicate
            let fetchedObjects    = try managedContext.fetch(fetchReq) as! [SendMoneyToBank]
            if fetchedObjects.count > 0 {
                return true
            } else {
                return false
            }
        } catch {
            print ("fetch task failed", error)
        }
        return false
    }

    func isBankExistsInDB(accDetail : BankAccountModel) -> Bool {
        let managedContext        = appDelegate.persistentContainer.viewContext
        let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "SendMoneyToBank")
        do {
            let predicate1        = NSPredicate(format: "accountNumber == %@", accDetail.accountNumber)
            let predicate2        = NSPredicate(format: "bankId  == %@", NSNumber(value: accDetail.bankId))
            let predicate3        = NSPredicate(format: "branchId == %@", NSNumber(value: accDetail.branchId))
            let andPredicate      = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2, predicate3])
            fetchReq.predicate    = andPredicate
            let fetchedObjects    = try managedContext.fetch(fetchReq) as! [SendMoneyToBank]
            if fetchedObjects.count > 0 {
                return true
            }else {
                return false
            }
        } catch {
             print ("fetch task failed", error)
        }
        return false
    }

    func isBankNotInActivestate(accDetail: BankAccountModel) -> Bool {
        // this func is used to check and return the bank is not in active state
        let managedContext        = appDelegate.persistentContainer.viewContext
        let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "SendMoneyToBank")
        do {
            let predicate1        = NSPredicate(format: "accountNumber == %@", accDetail.accountNumber)
            let predicate2        = NSPredicate(format: "bankId  == %@", NSNumber(value: accDetail.bankId))
            let predicate3        = NSPredicate(format: "branchId == %@", NSNumber(value: accDetail.branchId))
            let predicate4        = NSPredicate(format: "isBankActive == %@", NSNumber(value: false))
            let andPredicate      = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2, predicate3, predicate4])
            fetchReq.predicate    = andPredicate
            let fetchedObjects    = try managedContext.fetch(fetchReq) as! [SendMoneyToBank]
            if fetchedObjects.count > 0 {
                return true
            }else {
                return false
            }
        } catch {
            print ("fetch task failed", error)
        }
        return false
    }
    
    func isBankInActivestate(accDetail: BankAccountModel) -> Bool {
        // this func is used to check and return the bank is not in active state
        let managedContext        = appDelegate.persistentContainer.viewContext
        let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "SendMoneyToBank")
        do {
            let predicate1        = NSPredicate(format: "accountNumber == %@", accDetail.accountNumber)
            let predicate2        = NSPredicate(format: "bankId  == %@", NSNumber(value: accDetail.bankId))
            let predicate3        = NSPredicate(format: "branchId == %@", NSNumber(value: accDetail.branchId))
            let predicate4        = NSPredicate(format: "isBankActive == %@", NSNumber(value: true))
            let andPredicate      = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2, predicate3, predicate4])
            fetchReq.predicate    = andPredicate
            let fetchedObjects    = try managedContext.fetch(fetchReq) as! [SendMoneyToBank]
            if fetchedObjects.count > 0 {
                return true
            }else {
                return false
            }
        } catch {
            print ("fetch task failed", error)
        }
        return false
    }
    
 
    func saveServerBankId(accDetail: BankAccountModel) {
        let managedContext        = appDelegate.persistentContainer.viewContext
        let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "SendMoneyToBank")
        do {
            let predicate1        = NSPredicate(format: "accountNumber == %@", accDetail.accountNumber)
            let predicate2        = NSPredicate(format: "bankId  == %@", NSNumber(value: accDetail.bankId))
            let predicate3        = NSPredicate(format: "branchId == %@", NSNumber(value: accDetail.branchId))
            let andPredicate      = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2, predicate3])
            fetchReq.predicate    = andPredicate
            let fetchedObjects    = try managedContext.fetch(fetchReq) as! [SendMoneyToBank]
            if fetchedObjects.count > 0 {
                for bank in fetchedObjects {
                    bank.serverBankId = accDetail.serverBankId
                    break
                }
                
                do {
                    try managedContext.save()
                } catch let error as NSError {
                    println_debug("could not save :::: \(error)  ,,, \(error.description) ,,, \(error.userInfo)")
                }
            }
        }catch {
            print ("fetch task failed", error)
        }
    }
    
    func makeBankNotActiveState(bank: SendMoneyToBank) {
        let managedContext        = appDelegate.persistentContainer.viewContext
        let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "SendMoneyToBank")
        do {
            let predicate1        = NSPredicate(format: "accountNumber == %@", bank.accountNumber!)
            let predicate2        = NSPredicate(format: "bankId  == %@", NSNumber(value: bank.bankId))
            let predicate3        = NSPredicate(format: "branchId == %@", NSNumber(value: bank.branchId))
            let andPredicate      = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2, predicate3])
            fetchReq.predicate    = andPredicate
            let fetchedObjects    = try managedContext.fetch(fetchReq) as! [SendMoneyToBank]
            if fetchedObjects.count > 0 {
                for bank in fetchedObjects {
                    bank.isBankActive = false
                    break
                }
                
                do {
                    try managedContext.save()
                } catch let error as NSError {
                    println_debug("could not save :::: \(error)  ,,, \(error.description) ,,, \(error.userInfo)")
                }
            }
        }catch {
            print ("fetch task failed", error)
        }
    }

    func getBankFromDB(accDetail: BankAccountModel) -> SendMoneyToBank? {
        let managedContext        = appDelegate.persistentContainer.viewContext
        let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "SendMoneyToBank")
        do {
            let predicate1        = NSPredicate(format: "accountNumber == %@", accDetail.accountNumber)
            let predicate2        = NSPredicate(format: "bankId  == %@", NSNumber(value: accDetail.bankId))
            let predicate3        = NSPredicate(format: "branchId == %@", NSNumber(value: accDetail.branchId))
            let andPredicate      = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2, predicate3])
            fetchReq.predicate    = andPredicate
            let fetchedObjects    = try managedContext.fetch(fetchReq) as! [SendMoneyToBank]
            if fetchedObjects.count > 0 {
                return fetchedObjects[0]
            }
        } catch {
            print ("fetch task failed", error)
        }
        return nil
    }

    // MARK:- Bank CASHOUT Helper Methods -////////////////////////////////////////////////////////////
    
    func saveBankAmountCashOut(cashOutDetail: BankCashOutModel) {
        let managedContext    =  appDelegate.persistentContainer.viewContext
        
        let entity        =  NSEntityDescription.entity(forEntityName: "BankCashOut", in: managedContext)!
        let cashout       =  NSManagedObject(entity: entity, insertInto: managedContext) as! BankCashOut
        cashout.accountNumber           =  cashOutDetail.accountNumber
        cashout.amountTransferTime      =  cashOutDetail.amountTransferTime
        cashout.balanceAmount           =  cashOutDetail.balanceAmount
        cashout.bankId                  =  Int64(cashOutDetail.bankId)
        cashout.branchId                =  Int64(cashOutDetail.branchId)
        cashout.cashOutAmount           =  cashOutDetail.cashOutAmount
        cashout.cashOutStatus           =  cashOutDetail.cashOutStatus
        cashout.isActiveBank            =  cashOutDetail.isActiveBank
        cashout.transactionId           =  cashOutDetail.transactionId
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            println_debug("could not save :::: \(error)  ,,, \(error.description) ,,, \(error.userInfo)")
        }
    }
    
    func makeCashOutNotActiveState(bank: SendMoneyToBank) {
        let managedContext        = appDelegate.persistentContainer.viewContext
        let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "BankCashOut")
        do {
            let predicate1        = NSPredicate(format: "accountNumber == %@", bank.accountNumber!)
            let predicate2        = NSPredicate(format: "bankId  == %@", NSNumber(value: bank.bankId))
            let predicate3        = NSPredicate(format: "branchId == %@", NSNumber(value: bank.branchId))
            let andPredicate      = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2, predicate3])
            fetchReq.predicate    = andPredicate
            let fetchedObjects    = try managedContext.fetch(fetchReq) as! [BankCashOut]
            if fetchedObjects.count > 0 {
                for cashOut in fetchedObjects {
                    cashOut.isActiveBank = false
                    break
                }
                
                do {
                    try managedContext.save()
                } catch let error as NSError {
                    println_debug("could not save :::: \(error)  ,,, \(error.description) ,,, \(error.userInfo)")
                }
            }
        }catch {
            print ("fetch task failed", error)
        }
    }
    
}

