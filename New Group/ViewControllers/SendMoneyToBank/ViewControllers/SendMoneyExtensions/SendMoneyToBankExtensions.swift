//
//  SendMoneyToBankExtensions.swift
//  OK
//
//  Created by Uma Rajendran on 12/21/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import Foundation

let kMyAccount          =   "myaccount"
let kOtherAccount       =   "otheraccount"

extension UIViewController {
    
    var accountFields_BackUp: [BankItems] {
        return [.bank_accountholdername, .bank_accountidproof, .bank_amount, .bank_bankname, .bank_accounttype, .bank_accountnumber, .bank_contactnumber, .bank_remarks, .bank_emailid]
    }
    
    var  accountFieldsDict: [BankItems : String] {
      return [ .bank_accountholdername :"account holdername", .bank_accountidproof : "nrc number", .bank_amount : "amount", .bank_bankname : "bank name", .bank_accounttype : "account type", .bank_accountnumber : "account number" , .bank_contactnumber : "contact number", .bank_remarks : "remarks", .bank_emailid : "email id"]
    
    }
    
    var bankDiffAccFormatDic: [Int: String] {
        return [9 : "c-dddd", 10 : "cddddddcddd", 18 : "cc-ddd", 23 : "cc-dddd", 28 : "cc-dddd", 29 : "ccc-dd-ddddd", 30 : "ccc-dd-dddd"]
    }
    
    func checkAndCreate_BankAccountDetailsInfoDict() {
   
        if BankViewModel.shared.bankAccDetailsInfoDict.count >= 2 {
            return
        }
        
        let myAcc                   = BankAccountModel()
        myAcc.isMyAccount           = true
        myAcc.accountHolderName     = UserModel.shared.name
        myAcc.accountIDProofNumber  = UserModel.shared.nrc
        myAcc.emailid               = UserModel.shared.email
        myAcc.isBankActive          = true
        myAcc.selectedIdProofType   = UserModel.shared.idType == "04" ? .id_passport : .id_nrc
        BankViewModel.shared.bankAccDetailsInfoDict[kMyAccount]    = myAcc
        
        let otherAcc                = BankAccountModel()
        otherAcc.isMyAccount        = false
        otherAcc.isBankActive       = true
        otherAcc.selectedIdProofType   = UserModel.shared.idType == "04" ? .id_passport : .id_nrc
        BankViewModel.shared.bankAccDetailsInfoDict[kOtherAccount] = otherAcc
        
    }
    
    func getAccountDetailsDic() -> BankAccountModel {
        guard BankViewModel.shared.bankAccDetailsInfoDict.count > 0 else {
            return BankAccountModel()
        }
        
        if BankViewModel.shared.currentAccView  ==  .myacc {
            if BankViewModel.shared.currentMyAccView == .myacc_addbank {
                 return  BankViewModel.shared.bankAccDetailsInfoDict[kMyAccount]!
            }
        } else if BankViewModel.shared.currentAccView  ==  .otheracc {
            if BankViewModel.shared.currentOtherAccView == .otheracc_addbank {
                 return  BankViewModel.shared.bankAccDetailsInfoDict[kOtherAccount]!
            }
        }
        return BankAccountModel()
    }
    
    func clear_BankAccountDetailsDic() {
        BankViewModel.shared.bankAccDetailsInfoDict.removeAll()
    }
    
    func clearAndCreate_BankAccountDetailsDic() {
        clear_BankAccountDetailsDic()
        checkAndCreate_BankAccountDetailsInfoDict()
    }
    
    func refreshAccountDetailsDic(isRefreshMyAcc: Bool) {
        if isRefreshMyAcc {
            let myAcc                   = BankAccountModel()
            myAcc.isMyAccount           = true
            myAcc.accountHolderName     = UserModel.shared.name
            myAcc.accountIDProofNumber  = UserModel.shared.nrc
            myAcc.emailid               = UserModel.shared.email
            myAcc.isBankActive          = true
            myAcc.isLoadFromExistingBank  = false
            myAcc.selectedIdProofType   = UserModel.shared.idType == "04" ? .id_passport : .id_nrc
            BankViewModel.shared.bankAccDetailsInfoDict[kMyAccount]    = myAcc
        } else {
            let otherAcc                = BankAccountModel()
            otherAcc.isMyAccount        = false
            otherAcc.isBankActive       = true
            otherAcc.isLoadFromExistingBank  = false
            otherAcc.selectedIdProofType   = UserModel.shared.idType == "04" ? .id_passport : .id_nrc
            BankViewModel.shared.bankAccDetailsInfoDict[kOtherAccount] = otherAcc
        }
    }
    
    // MARK :- Advance Merchant Finding -///////////////////
    func isAdvanceMerchant() -> Bool {
        if UserModel.shared.agentType == .advancemerchant {
            return true
        }
        return false
    }
    
    // MARK :- Wallet Balance  -////////////////////////////
    func walletBalance() -> NSMutableAttributedString {
        if UserLogin.shared.walletBal.count > 0 {
            return attributedString(amount: self.amountInFormat(UserLogin.shared.walletBal))
        }
        return attributedString(amount: "0")
    }
    
    func attributedString(amount: String) -> NSMutableAttributedString {
        let mutableStr = NSMutableAttributedString(string: amount, attributes: [NSAttributedString.Key.font : UIFont(name: appFont, size: 20.0) ?? UIFont.systemFont(ofSize: 20)])
        mutableStr.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.red], range: NSRange(location: 0, length: amount.count))
        
        let mmk = " " + "MMK" //.localized
        let mmkStr = NSMutableAttributedString(string: mmk, attributes: [NSAttributedString.Key.font : UIFont(name: appFont, size: 12.0) ?? UIFont.systemFont(ofSize: 12)])
        mmkStr.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], range: NSRange(location: 0, length: mmk.count))
        
        mutableStr.append(mmkStr)
        return mutableStr
    }
    
    func attributedStringWithFont(amount: String, fontSize: CGFloat) -> NSMutableAttributedString {
        let mutableStr = NSMutableAttributedString(string: amount, attributes: [NSAttributedString.Key.font : UIFont(name: appFont, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)])
        mutableStr.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.red], range: NSRange(location: 0, length: amount.count))
        
        let mmk = " " + "MMK" //.localized
        let mmkStr = NSMutableAttributedString(string: mmk, attributes: [NSAttributedString.Key.font : UIFont(name: appFont, size: 12.0) ?? UIFont.systemFont(ofSize: 12)])
        mmkStr.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], range: NSRange(location: 0, length: mmk.count))
        
        mutableStr.append(mmkStr)
        return mutableStr
    }
    
    func attributedStringWithFontWtihBackColor(amount: String, fontSize: CGFloat) -> NSMutableAttributedString {
        let mutableStr = NSMutableAttributedString(string: amount, attributes: [NSAttributedString.Key.font : UIFont(name: appFont, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)])
        mutableStr.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], range: NSRange(location: 0, length: amount.count))
        
        let mmk = " " + "MMK" //.localized
        let mmkStr = NSMutableAttributedString(string: mmk, attributes: [NSAttributedString.Key.font : UIFont(name: appFont, size: 12.0) ?? UIFont.systemFont(ofSize: 12)])
        mmkStr.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], range: NSRange(location: 0, length: mmk.count))
        
        mutableStr.append(mmkStr)
        return mutableStr
    }
    func attributedStringWithFontWtihAllRedColor(amount: String, fontSize: CGFloat) -> NSMutableAttributedString {
         let mutableStr = NSMutableAttributedString(string: amount, attributes: [NSAttributedString.Key.font : UIFont(name: appFont, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)])
         mutableStr.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.red], range: NSRange(location: 0, length: amount.count))
         
         let mmk = " " + "MMK" //.localized
         let mmkStr = NSMutableAttributedString(string: mmk, attributes: [NSAttributedString.Key.font : UIFont(name: appFont, size: 12.0) ?? UIFont.systemFont(ofSize: 12)])
         mmkStr.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.red], range: NSRange(location: 0, length: mmk.count))
         
         mutableStr.append(mmkStr)
         return mutableStr
     }
    
    
    func attributedStringWithFontWtihStarSMB(symbol: String, symbolSize: CGFloat, string: String, stirngSize: CGFloat, image: UIImage) -> NSMutableAttributedString {
        let mutableStr = NSMutableAttributedString(string: symbol, attributes: [NSAttributedString.Key.font : UIFont(name: appFont, size: symbolSize) ?? UIFont.systemFont(ofSize: symbolSize)])
        mutableStr.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], range: NSRange(location: 0, length: symbol.count))
        
        
        let attachment = NSTextAttachment()
        attachment.image = image
        attachment.bounds = CGRect(x: 0, y: -2, width: 15, height: 15)
        let attachImage = NSAttributedString(attachment: attachment)
        
        let mmkStr = NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.font : UIFont(name: appFont, size: stirngSize) ?? UIFont.systemFont(ofSize: stirngSize)])
        mmkStr.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.init(red: 31/255, green: 46/255, blue: 133/255, alpha: 1)], range: NSRange(location: 0, length: string.count))
        
        mutableStr.append(attachImage)
        mutableStr.append(mmkStr)
        return mutableStr
    }
    
    
    func walletAmount() -> Double {
        if UserLogin.shared.walletBal.count > 0 {
           return Double(UserLogin.shared.walletBal)!
        }
        return 0
    }
    
    // MARK:- Account Model Values Refreshing -//////////////////////////////////////
    func refreshAccountDetails(curRow: BankItems) {
        let accountDetails = getAccountDetailsDic()
        
        guard !accountDetails.isLoadFromExistingBank else {
            return
        }
        
        var items = [0 : accountDetails.accountHolderName, 1 : accountDetails.accountIDProofNumber, 2 : accountDetails.amount, 3 : accountDetails.bankName, 4 : accountDetails.accountType, 5 : accountDetails.accountNumber, 6 : accountDetails.contactMobileNumber, 7 : accountDetails.remarks, 8 : accountDetails.emailid]
        for index in curRow.rawValue + 1 ... accountFields_BackUp.count - 1 {
            items[index] = ""
        }
        
        accountDetails.accountHolderName          = items[0]!
        accountDetails.accountIDProofNumber       = items[1]!
        accountDetails.amount                     = items[2]!
        accountDetails.bankName                   = items[3]!
        accountDetails.accountType                = items[4]!
        accountDetails.accountNumber              = items[5]!
        accountDetails.contactMobileNumber        = (items[6]!.count > 0) ? items[6]! : "09"
        accountDetails.remarks                    = items[7]!
        accountDetails.emailid                    = UserModel.shared.email
    }
    
    // MARK:- Account Number Validations - /////////////////////////////
    func amountInFormat(_ enterAmt: String) -> String {
        if enterAmt.contains(find: ",") {
            return enterAmt
        } else {
            guard let amountInDouble = Double(enterAmt) else { return "" }
            let formattedIntAmount = String(format: "%.2f", locale: Locale.current, amountInDouble)
            return formattedIntAmount
        }
    }
    
    func amountInFormatForValidation(_ enterAmt: String) -> String {
        guard let amountInDouble = Double(enterAmt) else { return "" }
        let formattedIntAmount = String(format: "%.0f", locale: Locale.current, amountInDouble)
        return formattedIntAmount
    }
    
    func isDiffAccFormat(accDetails: BankAccountModel) -> Bool {
        let arr = Array(bankDiffAccFormatDic.keys) // these are the banksid's which are having diff account format
        if arr.contains(accDetails.bankId) {
            return true
        }
        return false
    }
    
    func frameAccNumberByFormat(typingStr: String, accDetails: BankAccountModel) -> String {
        println_debug("account format from server ::::: \(accDetails.multiAccountFormat)")
        var hypenString = ""
        if accDetails.multiAccountFormat.contains(find: ",") {
            let maxCountArr = accDetails.multiAccountFormat.components(separatedBy: ",")
            if maxCountArr[1].count > 0 {
               hypenString = maxCountArr[1]
            }
        }else {
            hypenString = accDetails.multiAccountFormat
        }
        var formatStr: String = ""
        let nonHyphenStr = typingStr.replacingOccurrences(of: "-", with: "")
        var formatIndex = 0
        for index in 0 ... nonHyphenStr.count - 1 {
            let eachVal = nonHyphenStr[index]
            println_debug("each character from string ::::: \(eachVal)")
           
            let eachformatchar = hypenString[formatIndex]
            println_debug("each character from account format :::: \(eachformatchar)")
            if eachformatchar == "-" {
                if eachVal != "-" {
                    formatStr.append(eachformatchar)
                    println_debug("format string inside checking ::: \(formatStr)")
                }
                formatIndex += 1
            }
            formatStr.append(eachVal)
            formatIndex += 1
            println_debug("format string is :::: \(formatStr)")
        }
        println_debug("final formatted string is :::: \(formatStr)")
        return formatStr
    }
    
    func accNumberByDiffFormat(typingStr: String, accDetails: BankAccountModel, cell: SendMoneyMainCell) {
        let diffAccFormat: String = bankDiffAccFormatDic[accDetails.bankId]!
        println_debug("account format is ::: \(diffAccFormat)")
        var formatStr: String = ""
        if typingStr.count <= diffAccFormat.count {
            let lastIndex = typingStr.count - 1
            let typingCharType = checkCharType(char: typingStr[lastIndex])
            let curAccFormatCharType = checkAccountFormatCharacterType(char: diffAccFormat[lastIndex])
            if typingCharType != curAccFormatCharType {
                return
            }
            if lastIndex + 1 < diffAccFormat.count {
                let accFormatChar = diffAccFormat[lastIndex + 1]
                let accFormatChatType = checkAccountFormatCharacterType(char: accFormatChar)
                formatStr.append(typingStr)
                if accFormatChatType == .numeric {
                    if cell.contentTxtField.keyboardType != .numberPad {
                        cell.contentTxtField.keyboardType = UIKeyboardType.numberPad
                        cell.contentTxtField.becomeFirstResponder()
                    }
                } else if accFormatChatType == .alpha {
                    if cell.contentTxtField.keyboardType != .default {
                        cell.contentTxtField.keyboardType = UIKeyboardType.default
                        cell.contentTxtField.becomeFirstResponder()
                        
                    }
                } else if accFormatChatType == .hifen {
                    formatStr.append("-")
                    if lastIndex + 2 < diffAccFormat.count {
                        let nextChar = diffAccFormat[lastIndex + 2]
                        let nextCharType = checkAccountFormatCharacterType(char: nextChar)
                        if nextCharType == .numeric {
                            if cell.contentTxtField.keyboardType != .numberPad {
                                cell.contentTxtField.keyboardType = UIKeyboardType.numberPad
                                cell.contentTxtField.becomeFirstResponder()
                                
                            }
                        } else if nextCharType == .alpha {
                            if cell.contentTxtField.keyboardType != .default {
                                cell.contentTxtField.keyboardType = UIKeyboardType.default
                                cell.contentTxtField.becomeFirstResponder()
                                
                            }
                        }
                    }
                }
            } else {
                formatStr = typingStr
            }
            accDetails.accountNumber = formatStr.uppercased()
            cell.wrap_accountNumber(accDetails: accDetails)
        }
    }
    
    func checkAccountFormatCharacterType(char: Character) -> CharacterType {
        if char == "c" {
            return .alpha
        } else if char == "d" {
            return .numeric
        } else  {
            return .hifen
        }
    }
    
    func checkCharType(char: Character) -> CharacterType {
        let digits = CharacterSet.decimalDigits
        for eachchar in char.unicodeScalars {
            if digits.contains(eachchar) {
                return .numeric
            }
        }
        let alphabets = CharacterSet.letters
        for eachchar in char.unicodeScalars {
            if alphabets.contains(eachchar) {
                return .alpha
            }
        }
        if char == "-" {
            return .hifen
        }
        return .alpha
    }
    
    //MARK :- Validate all Fields - ////////////////////////////////////////////////////////////////
    func isValidAccountHolderName(accDetails : BankAccountModel) -> Bool {
        if accDetails.accountHolderName.count > 2 {
            return true
        }
        return false
    }
    
    func isValidIdProofNumber(accDetails: BankAccountModel) -> Bool {
        if accDetails.selectedIdProofType == .id_passport {
            if accDetails.accountIDProofNumber.count >= PassportNumberCount.minCount && accDetails.accountIDProofNumber.count <= PassportNumberCount.maxCount{
                return true
            }
        } else {
            // Note: we need to check the exact nrc validation here later
            if accDetails.accountIDProofNumber.count >= 6 {
                return true
            }
        }
        return false
    }
    
    func isValidAmount(accDetails: BankAccountModel) -> Bool {
        let amountVal = accDetails.amount.components(separatedBy: ",").joined(separator: "")
        if (amountVal.count >= 5) &&  (amountVal as NSString).integerValue >= 30000 && (amountVal as NSString).doubleValue <= self.walletAmount() {
            return true
        }
        return false
    }
    
    func isValidBank(accDetails: BankAccountModel) -> Bool {
        return (accDetails.bankName.count > 0) ? true : false
    }
    
    func isValidAccountType(accDetails: BankAccountModel) -> Bool {
        return (accDetails.accountType.count > 0) ? true : false
    }
    
    func isValidAccountNumber(accDetails: BankAccountModel) -> Bool {
        guard !accDetails.isLoadFromExistingBank else {
            return true
        }
        if accDetails.accountType == AccountTypes.atmcardAcc {
            // need to check the account number has min 16 to max 19
            if accDetails.accountNumber.count >= AtmAccount.minAccNumber && accDetails.accountNumber.count <= AtmAccount.maxAccNumber {
                return true
            }
            return false
        } else {
            if self.isDiffAccFormat(accDetails: accDetails) {
                // here we need to do different validation for account number
//                let diffAccFormat: String = bankDiffAccFormatDic[accDetails.bankId]!
                if accDetails.bankId == 9 {
                    if accDetails.accountNumber.count >= 4 {
                        return true
                    }
                } else {
                    if accDetails.accountNumber.count >= minAcNumCount {
                        return true
                    }
                }
            } else {
                if accDetails.accountNumber.count >= minAcNumCount {
                    return true
                }
            }
            return false
        }
    }
    
    func isValidContactMobileNumber(accDetails: BankAccountModel, withValidationObject validObj:PayToValidations) -> Bool {
        let validationResult = validObj.getNumberRangeValidation(accDetails.contactMobileNumber)
        if validationResult.isRejected {
            return false
        }
        if !(accDetails.contactMobileNumber.count >= validationResult.min && accDetails.contactMobileNumber.count <= validationResult.max) {
            return false
        }
        return true
    }
    
    func isValidEmail(accDetails: BankAccountModel) -> Bool {
        let emailIds =  accDetails.emailid.components(separatedBy: ",")
        if emailIds.count > 0 {
//           for index in 0 ... emailIds.count - 1 {
                if checkMaidIdFormat(emailid: emailIds[0]) {
//                    if index == emailIds.count - 1 {
                        return true
//                    }
//                    continue

                } else {
                    return false
                }
//            }
        } else {
            return checkMaidIdFormat(emailid: accDetails.emailid)
        }
//        return false
    }
    
    func checkMaidIdFormat(emailid: String) -> Bool {
        let enteredEmail = emailid
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
    
    func getAmount(accDetail: BankAccountModel) -> String {
        return accDetail.amount.components(separatedBy: ",").joined(separator: "")
    }
    
    // MARK:- Check to show and hide Submit button - ////////////////////////////////////////////////////////////////
    func canShowSendBtn(accDetails: BankAccountModel, withValidateObj validObj: PayToValidations) -> Bool {
        if isValidAccountHolderName(accDetails: accDetails) && isValidIdProofNumber(accDetails: accDetails) && isValidAmount(accDetails: accDetails) && isValidBank(accDetails: accDetails) && isValidAccountType(accDetails: accDetails) && isValidAccountNumber(accDetails: accDetails) && isValidContactMobileNumber(accDetails: accDetails, withValidationObject: validObj) && isValidEmail(accDetails: accDetails) {
            println_debug("canShowSendBtn ::::: TRUE RETURN")
            return true
        }
        println_debug("canShowSendBtn ::::: FALSE RETURN")
        return false
    }
    
    // MARK:- API Handling - ///////////////////////////////////////////////////////////////////////
    func validate_NormalOk$Users() -> Bool {
        _ = getAccountDetailsDic()
        guard CoreDataHelper.shared.totalActiveBanksCount() < 5 else{
            // for advance merchant total 4 banks -> 2 bank for myaccount and 2 bank for other account
            // for other normal merchant and users total 4 banks -> 4 banks for my account only
            self.showErrorAlert(errMessage: "Already Four Banks Added to this Account".localized)
            self.view.endEditing(true)
            return false
        }
        return true
    }
    
    func validate_AdvanceMerchant() -> Bool {
        let accountDetails = getAccountDetailsDic()
        guard CoreDataHelper.shared.totalActiveBanksCount() < 4 else{
            // for advance merchant total 4 banks -> 2 bank for myaccount and 2 bank for other account
            // for other normal merchant and users total 4 banks -> 4 banks for my account only
            self.showErrorAlert(errMessage: "Already Four Banks Added to this Account".localized)
            self.view.endEditing(true)
            return false
        }
        guard CoreDataHelper.shared.accountCount_ForThisAccount(accDetails: accountDetails) < 2 else {
            self.showErrorAlert(errMessage: "Already Two Banks Added to this Account".localized)
            self.view.endEditing(true)
            return false
        }
        return true
    }
    
    func checkAndAddNewBankInDB(accountDetails: BankAccountModel) {
        DispatchQueue.main.async {
            if CoreDataHelper.shared.isBankExistsInDB(accDetail: accountDetails) {
                // bank already exists so we are going to update the details in that bank
                if CoreDataHelper.shared.isBankNotInActivestate(accDetail: accountDetails) {
                    CoreDataHelper.shared.makeBankActive(accDetail: accountDetails)
                }
            } else {
                CoreDataHelper.shared.saveNewBankDetailsInDB(accDetail: accountDetails)
            }
        }
    }
    
    //Add bank on click save button
    func addNewBankApi(accDetail: BankAccountModel, completionHandler: @escaping (Bool, Any?) -> Void) {
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
     
        // Check This bank already exits or not
        DispatchQueue.main.async {
            if CoreDataHelper.shared.isBankExistsInDB(accDetail: accDetail) {
                // Update existing bank to serve & Data Base
                println_debug("update Bank Data")
                BankApiClient.updateBankApi(accDetail: accDetail, updateApiHandler: { (isSuccess,errMsg, response) in
                    DispatchQueue.main.async {
                        progressViewObj.removeProgressView()
                    }
                    guard isSuccess else {
                        DispatchQueue.main.async {
                            var error = "Try Again"
                            if let msg = errMsg {
                                error = msg
                            }
                            self.showErrorAlert(errMessage: error)
                            completionHandler(false, nil)
                        }
                        return
                    }
                    if let dic = response as? Dictionary<String, Any> {
                        if let serverBankId = dic["Data"] as? String {
                            accDetail.serverBankId = serverBankId
                            CoreDataHelper.shared.updateBankInDB(accDetail: accDetail)
                            completionHandler(true, response)
                        }
                    } else {
                        completionHandler(false, nil)
                    }
                })
            } else {
                   println_debug("add Bank data")
                   // Add new bank to server & Data Base
                    BankApiClient.addBankApi(accDetails: accDetail) {  (isSuccess,errMsg, response) in
                        DispatchQueue.main.async {
                            progressViewObj.removeProgressView()
                        }
                        guard isSuccess else {
                            DispatchQueue.main.async {
                                var error = "Try Again"
                                if let msg = errMsg {
                                    error = msg
                                }
                                self.showErrorAlert(errMessage: error)
                                completionHandler(false, nil)
                            }
                            return
                        }
                        if let dic = response as? Dictionary<String, Any> {
                            if let serverBankId = dic["Data"] as? String {
                                accDetail.serverBankId = serverBankId
                                CoreDataHelper.shared.saveNewBankDetailsInDB(accDetail: accDetail)
                                completionHandler(true, response)
                            }
                        } else {
                            completionHandler(false, nil)
                        }
                    }
                    
                }
        }
    }
    
    func bankCashOutApi(accDetail: BankAccountModel,cashOutDetail: BankCashOutModel, completionHandler: @escaping (Bool, Any?) -> Void) {
        progressViewObj.showProgressView()
        BankApiClient.bankCashOutAmountApi(accDetail: accDetail, cashOutDetail: cashOutDetail) {  (isSuccess, errMsg, response) in
            progressViewObj.removeProgressView()
            guard isSuccess else {
                DispatchQueue.main.async {
                    var error = "Try Again"
                    if let msg = errMsg {
                        error = msg
                    }
                    self.showErrorAlert(errMessage: error)
                    completionHandler(false, nil)
                }
                return
            }
            if let dic = response as? Dictionary<String, Any> {
                cashOutDetail.cashOutStatus     = true
                cashOutDetail.isActiveBank      = true
                cashOutDetail.transactionId     = dic["transid"] as! String
                cashOutDetail.balanceAmount     = dic["walletbalance"] as! String
                CoreDataHelper.shared.saveBankAmountCashOut(cashOutDetail: cashOutDetail)
                completionHandler(true, response)
            } else {
                completionHandler(false, nil)
            }
        }
    }
    
    func addNewBankAndCashOutApi(accDetail: BankAccountModel,cashOutDetail: BankCashOutModel, completionHandler: @escaping (Bool,String?, Any?) -> Void) {
        progressViewObj.showProgressView()
        println_debug("addNewBankAndCashOutApi")
        BankApiClient.addBankApi(accDetails: accDetail) { (isSuccess,errMsg, response) in
            progressViewObj.removeProgressView()
            guard isSuccess else {
                DispatchQueue.main.async {
                    var error = "Try Again"
                    if let msg = errMsg {
                        error = msg
                    }
                    self.showErrorAlert(errMessage: error)
                    completionHandler(false,"addbank", nil)
                }
                return
            }
            
            if let dic = response as? Dictionary<String, Any> {
                if let serverBankId = dic["Data"] as? String {
                    accDetail.serverBankId = serverBankId
                    self.checkAndAddNewBankInDB(accountDetails: accDetail)

                    ////////////////////////////// -------- Second Bank CashOut Api Call ------- //////////////////////////////////////////////
                    progressViewObj.showProgressView()
                    BankApiClient.bankCashOutAmountApi(accDetail: accDetail, cashOutDetail: cashOutDetail) {  (isSuccess, errMsg, response) in
                        progressViewObj.removeProgressView()
                        guard isSuccess else {
                            var error = "Try Again"
                            if let msg = errMsg {
                                error = msg
                            }
                            self.showErrorAlert(errMessage: error)
                            completionHandler(false,"bankcashout", nil)
                            return
                        }
                        if let dic = response as? Dictionary<String, Any> {
                            cashOutDetail.cashOutStatus     = true
                            cashOutDetail.isActiveBank      = true
                            cashOutDetail.transactionId     = dic["transid"] as! String
                            cashOutDetail.balanceAmount     = dic["walletbalance"] as! String
                            CoreDataHelper.shared.saveBankAmountCashOut(cashOutDetail: cashOutDetail)
                            completionHandler(true,nil, response)
                        } else {
                            completionHandler(false,"bankcashout", nil)
                            
                        }
                    }
                    ////////////////////////////// -------- Second Bank CashOut Api Call ------- //////////////////////////////////////////////
                }
            } else {
                completionHandler(false,"addbank", nil)
            }
        }
    }
    
    func deleteBankApi(bank: SendMoneyToBank, completionHandler: @escaping (Bool, Any?) -> Void) {
        progressViewObj.showProgressView()
        BankApiClient.deleteBank(bankDetail: bank) {  (isSuccess, errMsg, response) in
            progressViewObj.removeProgressView()
            guard isSuccess else {
                DispatchQueue.main.async {
                    var error = "Try Again"
                    if let msg = errMsg {
                        error = msg
                    }
                    self.showErrorAlert(errMessage: error)
                    completionHandler(false, nil)
                }
                return
            }
            DispatchQueue.main.async {
                CoreDataHelper.shared.makeBankNotActiveState(bank: bank)
                CoreDataHelper.shared.makeCashOutNotActiveState(bank: bank)
                completionHandler(true, nil)
            }
        }
    }
    
    func updateBankApi(accDetail: BankAccountModel) {
        BankApiClient.updateBankApi(accDetail: accDetail) { (isSuccess, errMsg, response) in
            guard isSuccess else {
                self.showErrorAlert(errMessage: "Something went wrong".localized)
                return
            }
            DispatchQueue.main.async {
                CoreDataHelper.shared.updateBankInDB(accDetail: accDetail)
            }
        }
    }
    
    func getAllBanksFromServer(completionHandler: @escaping (Bool, Any?, Bool) -> Void) {
        progressViewObj.showProgressView()
        BankApiClient.getAllSavedBanks {  (isSuccess, response) in
            progressViewObj.removeProgressView()
            
            DispatchQueue.main.async {
                guard isSuccess else {
                   // self.showErrorAlert(errMessage: "Something went wrong".localized)
                    completionHandler(true, nil, false)
                   // self.view.endEditing(true)
                //    completionHandler(false, nil, false)
                    return
                }
                var isData = false
                if let resArr = response as? [Dictionary<String, Any>] {
                    if resArr.count > 0 {
                        isData = true
                    }
                    for dic in resArr {
                        let accDetail = BankAccountModel()
                        if let title = dic["accountName"] as? String {
                            accDetail.accountHolderName = title
                        }
                        if let title = dic["accountNumber"] as? String {
                            accDetail.accountNumber = title
                        }
                        if let title = dic["idNo"] as? String {
                            accDetail.accountIDProofNumber = title
                        }
                        if let title = dic["accountType"] as? String {
                            accDetail.accountType = title
                        }
                        if let title = dic["branchAddress"] as? String {
                            accDetail.branchAddress = title
                        }
                        if let title = dic["branchAddress"] as? String {
                            accDetail.branchBurmeseAddress = title
                        }
                        if let title = dic["bankName"] as? String {
                            accDetail.bankName = title
                        }
                        if let title = dic["bankBurmeseName"] as? String {
                            accDetail.bankBurmeseName = title
                        }
                        if let title = dic["bankId"] as? String {
                            accDetail.bankId = Int(title)!
                        }
                        if let title = dic["branchid"] as? String {
                            accDetail.branchId = Int(title)!
                        }
                        if let title = dic["branchName"] as? String {
                            accDetail.branchName = title
                        }
                        if let title = dic["branchBurmeseName"] as? String {
                            accDetail.branchBurmeseName = title
                        }
                        if let title = dic["township"] as? String {
                            accDetail.townshipCode = ""
                            accDetail.townshipName = title
                        }
                        if let title = dic["townshipBurmeseName"] as? String {
                            accDetail.townshipBurmeseName = title
                        }
                        if let title = dic["state"] as? String {
                            accDetail.stateCode = ""
                            accDetail.stateName = title
                        }
                        if let title = dic["divisionBurmeseName"] as? String {
                            accDetail.stateBurmeseName = title
                        }
                        if let title = dic["bankPhoneNumber"] as? String {
                            accDetail.bankPhoneNumber = title
                        }
                        if let title = dic["accountModel"] as? String {
                            if title.lowercased() == "myaccount" {
                                accDetail.isMyAccount = true
                            }else {
                                accDetail.isMyAccount = false
                            }
                        }
                        if let title = dic["id"] as? String {
                            accDetail.serverBankId = title
                        }
                        if let title = dic["beneficiaryMobNumber"] as? String {
                            accDetail.contactMobileNumber = title
                        }
                        if let title = dic["remarks"] as? String {
                            accDetail.remarks = title
                        }
                        if let title = dic["emailId"] as? String {
                            accDetail.emailid = title
                        }
                        if let title = dic["isActive"] as? String {
                            if title.lowercased() == "true" {
                                accDetail.isBankActive = true
                            } else {
                                accDetail.isBankActive = false
                            }
                        }
                        if let title = dic["idType"] as? String {
                            if title == "NRC" {
                                accDetail.selectedIdProofType = .id_nrc
                            } else {
                                accDetail.selectedIdProofType = .id_passport
                            }
                        }
                        self.checkAndAddNewBankInDB(accountDetails: accDetail)
                    }
                }
                completionHandler(true, nil, isData)
            }
        }
    }
    
    func canAllowToAddBank() -> Bool {
        if self.isAdvanceMerchant() {
            // do validation for advance merchant to add the bank
            if validate_AdvanceMerchant() {
                // if its true we can allow to add bank
                return true
            }
        } else {
            // do validation for normal merchant or user to add the bank
            if validate_NormalOk$Users() {
                // if its true we can allow to add bank
                return true
            }
        }
        return false
    }
     
    // MARK :- NRC Details refresh
    func refreshNRCDetails(accountDetails: BankAccountModel) {
        accountDetails.nrcCodeNumber        = ""
        accountDetails.nrcCode              = ""
        accountDetails.nrcFullCode          = ""
        accountDetails.accountIDProofNumber = ""
    }
    
    // MARK :- Insiffient Balance to transfer
    func showInsufficientBalanceAlert() {
        alertViewObj.wrapAlert(title: nil, body: "Amount must be (30,000) MMK & Above.".localized, img: #imageLiteral(resourceName: "smToBank"))
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        alertViewObj.showAlert(controller: self)
    }
}
