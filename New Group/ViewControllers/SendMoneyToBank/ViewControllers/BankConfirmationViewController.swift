//
//  BankConfirmationViewController.swift
//  OK
//
//  Created by Uma Rajendran on 12/21/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol ConfirmationDelegate {
      func removeConfirmationScreen()
}

class BankConfirmationViewController: SendMoneyBaseViewController {

    @IBOutlet weak var confirmationTable: UITableView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var sendBtn: UIButton!
    var cbBankTokenModel: CBBankTokenModel?
    var delegate  : ConfirmationDelegate?
    var instantTransfer = false
    private enum DataType {
        case accountName, idType, amount, bankName, accType, accNumber, divState, township, bankContactNumber, contMobileNumber, remarks, email, none
    }
    private var myDataValues: [(itemType: DataType, itemTitle: String, itemValue: String)] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        loadInitialization()
        loadUI()
        updateLocalization()
        setUpNavigation()
        self.buildDataSet()
        self.confirmationTable.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkNewBank()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func buildDataSet() {
        let bankAccountModel = getAccountDetailsDic()
        let accName = bankAccountModel.accountHolderName
        if accName.count > 0 {
            myDataValues.append((itemType: .accountName, itemTitle: bankAccountModel.isMyAccount ? "OK$ Account Name".localized : "Account Holder Name".localized, itemValue: accName))
        }
        let idProofNumb = bankAccountModel.accountIDProofNumber
        let idTitle = (bankAccountModel.selectedIdProofType == .id_nrc) ? "NRC".localized : "Passport Number".localized
        if idProofNumb.count > 0 {
            myDataValues.append((itemType: .idType, itemTitle: idTitle, itemValue: idProofNumb))
        }
        let amount = bankAccountModel.amount
        if amount.count > 0 {
            myDataValues.append((itemType: .amount, itemTitle: "Amount".localized, itemValue: amount))
        }
        var bankname = ""
        var stateName = ""
        var townshipname = ""
        if let curLanguage = UserDefaults.standard.value(forKey: "currentLanguage") as? String {
            switch curLanguage {
            case "en":
                bankname = bankAccountModel.bankName
                stateName = bankAccountModel.stateName
                townshipname = bankAccountModel.townshipName
            case "my":
                bankname = bankAccountModel.bankBurmeseName
                stateName = bankAccountModel.stateBurmeseName
                townshipname = bankAccountModel.townshipBurmeseName
            default:
                bankname = bankAccountModel.bankName
                stateName = bankAccountModel.stateName
                townshipname = bankAccountModel.townshipName
            }
        }
        if bankname.count > 0 {
            myDataValues.append((itemType: .bankName, itemTitle: "Bank Name".localized, itemValue: bankname))
        }
        let accountTypes = ["CDA" : "Call Deposit Account", "SAV" : "Savings Account", "CUR" : "Current Account", "ACA": "ATM Card Account"]
        if let accTypeVale = accountTypes[bankAccountModel.accountType] {
            myDataValues.append((itemType: .accType, itemTitle: "Account Type".localized, itemValue: accTypeVale.localized))
        }
        let accNumb = bankAccountModel.accountNumber
        if accNumb.count > 0 {
            myDataValues.append((itemType: .accNumber, itemTitle: "Account Number".localized, itemValue: accNumb))
        }
        if stateName.count > 0 {
            myDataValues.append((itemType: .divState, itemTitle: "Division / State".localized, itemValue: stateName))
        }
        if townshipname.count > 0 {
            myDataValues.append((itemType: .township, itemTitle: "Township".localized, itemValue: townshipname))
        }
        let bankPhNumber = bankAccountModel.bankPhoneNumber
        if bankPhNumber.count > 0 {
            myDataValues.append((itemType: .bankContactNumber, itemTitle: "Bank Contact Number".localized, itemValue: bankPhNumber))
        }
        let contNumber = bankAccountModel.contactMobileNumber
        if contNumber.count > 0 {
            myDataValues.append((itemType: .contMobileNumber, itemTitle: "Contact Mobile Number".localized, itemValue: contNumber))
        }
        let remarks = bankAccountModel.remarks
        if remarks.count > 0 {
            myDataValues.append((itemType: .remarks, itemTitle: "Remarks".localized, itemValue: remarks))
        }
        let emailArray = bankAccountModel.emailid.components(separatedBy: ",")
        if emailArray[0].count > 0 {
            myDataValues.append((itemType: .email, itemTitle: "Email".localized, itemValue: emailArray[0]))
        }
        
        if let _ = cbBankTokenModel {
            let accountDetails = getAccountDetailsDic()
            if accountDetails.bankName == "Co-operative Bank Limited" || accountDetails.bankId == 5 {
                myDataValues.append((itemType: .none, itemTitle: "", itemValue: "It will be credit in your \nbank account instantly.".localized))
                instantTransfer = true
            } else {
                myDataValues.append((itemType: .none, itemTitle: "", itemValue: "It will be credited in your bank \naccount within 24 banking hours".localized))
                instantTransfer = false
            }
        }else {
            myDataValues.append((itemType: .none, itemTitle: "", itemValue: "It will be credited in your bank \naccount within 24 banking hours".localized))
            instantTransfer = false
        }
    
    }
    
    func loadUI() {
        self.confirmationTable.backgroundColor  = kBackGroundGreyColor
    }
    
    func loadInitialization() {
        self.confirmationTable.rowHeight             = UITableView.automaticDimension
        self.confirmationTable.estimatedRowHeight    = 450
    }
    
    func updateLocalization() {
        saveBtn.setTitle("Save".localized, for: .normal)
        sendBtn.setTitle("Send".localized, for: .normal)
        saveBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        sendBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        saveBtn.backgroundColor = kYellowColor
        sendBtn.backgroundColor = kYellowColor
    }
    
    func addShadow(view: UIButton) {
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2
    }
    
    func checkNewBank() {
        DispatchQueue.main.async {
            let accountDetails = self.getAccountDetailsDic()
            if CoreDataHelper.shared.isBankExistsInMyOrOther(accDetail: accountDetails) {
                self.saveBtn.isHidden = true
            } else {
                self.saveBtn.isHidden = false
            }
        }
    }
    
    var tenDigitNumber: String {
        var result = ""
        repeat {
            result = String(format:"%10d", arc4random_uniform(1000000000) )
        } while result.count < 10
        return result
    }
    
    private func getNrcInEnglish() -> String {
        return ""
    }
    
    private func getCBBankParams1() -> Dictionary<String, Any>? {
        let bankAccountModel = getAccountDetailsDic()
        var comments = "SEND MONEY TO BANK: \(bankAccountModel.bankName ), \(bankAccountModel.townshipName ), \(bankAccountModel.accountNumber ), \(bankAccountModel.accountHolderName ), \(bankAccountModel.accountType ), \(bankAccountModel.branchAddress )"
        comments = comments.replacingOccurrences(of: "$", with: " ").replacingOccurrences(of: "&", with: "And").replacingOccurrences(of: "/", with: " ").replacingOccurrences(of: "<", with: " ").replacingOccurrences(of: ">", with: " ")
        var appInfoDict = Dictionary<String, Any>()
        appInfoDict["AppVersion"] = buildVersion
        appInfoDict["MobileNumber"] =  UserModel.shared.mobileNo
        appInfoDict["SimId"] =  UserModel.shared.simID
        appInfoDict["MsId"] =  UserModel.shared.msid
        appInfoDict["CellId"] =  ""
        appInfoDict["Latitude"] =  UserModel.shared.lat
        appInfoDict["Longitude"] =  UserModel.shared.long
        appInfoDict["ProfileImg"] = UserModel.shared.proPic
        appInfoDict["SecureToken"] = UserLogin.shared.token
        var cbAccountDict = Dictionary<String, Any>()
        let phNum = bankAccountModel.contactMobileNumber.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "")
        let phNo = "0095\(String(phNum.dropFirst()))"
        cbAccountDict["PhoneNumber"] = phNo
        cbAccountDict["OkAccountNumber"] = UserModel.shared.mobileNo
        cbAccountDict["Nrc"] =  UserModel.shared.nrc.replacingOccurrences(of: "@", with: "/")
        cbAccountDict["Name"] =  bankAccountModel.accountHolderName
        let amount = bankAccountModel.amount.replacingOccurrences(of: ",", with: "")
        cbAccountDict["Amount"] = (amount as NSString).intValue
        cbAccountDict["BankAccountNumber"] = bankAccountModel.accountNumber
        if let model = routingModel {
            cbAccountDict["ProjectId"]  = model.projectId ?? ""
        }
        cbAccountDict["Password"] =  ok_password ?? ""
        cbAccountDict["Comments"] =   comments
        let params = ["AppInfo" : appInfoDict, "CBAcount" : cbAccountDict]
        return params
    }
    
    
    func wrapDataModel() -> Data? {
        
        let bankAccountModel = getAccountDetailsDic()
        var comments = "SEND MONEY TO BANK: \(bankAccountModel.bankName ), \(bankAccountModel.townshipName ), \(bankAccountModel.accountNumber ), \(bankAccountModel.accountHolderName ), \(bankAccountModel.accountType ), \(bankAccountModel.branchAddress )"
        comments = comments.replacingOccurrences(of: "$", with: " ").replacingOccurrences(of: "&", with: "And").replacingOccurrences(of: "/", with: " ").replacingOccurrences(of: "<", with: " ").replacingOccurrences(of: ">", with: " ")
  
        let phNum = bankAccountModel.contactMobileNumber.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "")
        let phNo = "0095\(String(phNum.dropFirst()))"
        let amount = bankAccountModel.amount.replacingOccurrences(of: ",", with: "")

        var proID = ""
        if let model = routingModel {
            proID  = model.projectId ?? ""
        }

        let appInfo = CBPayAppInfo.init(appVersion: buildNumber, cellID: "", latitude: UserModel.shared.lat, longitude: UserModel.shared.long, mobileNumber: UserModel.shared.mobileNo, msID: UserModel.shared.msid, profileImg: UserModel.shared.proPic, secureToken: UserLogin.shared.token, simID: UserModel.shared.simID)
        
        let cbAccount = CBAcountInfo.init(amount: Int(amount) ?? 0, bankAccountNumber: bankAccountModel.accountNumber, comments: comments, name: bankAccountModel.accountHolderName, nrc:  UserModel.shared.nrc.replacingOccurrences(of: "@", with: "/"), okAccountNumber: UserModel.shared.mobileNo, password: ok_password ?? "", phoneNumber: phNo, projectID: proID)
        
        let model = FinaleCBPay.init(appInfo: appInfo, cbAcount: cbAccount)
        println_debug(model)
        do {
            let data  =  try JSONEncoder().encode(model)
            println_debug(String(data: data, encoding: .utf8))
            return data
        } catch {
            println_debug(error)
        }
        return Data.init()
    }
    
    
    
    
    func getCurrentMillis(date: Date)->Int64{
        return  Int64(NSDate().timeIntervalSince1970 * 1000)
    }
    
    func checkAndAddCBBankTODB() {
        let accountDetails = getAccountDetailsDic()
        if self.canAllowToAddBank() {
            self.addNewBankApi(accDetail: accountDetails, completionHandler: { (isSuccess, response) in
                guard isSuccess else {
                    return
                }
                self.payToCBBank()
            })
        }else {
            self.payToCBBank()
        }
    }
    
    
    private func payToCBBank() {
        
//        DispatchQueue.main.async {
//                      let vc = UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "SMBNewReceipt") as? SMBNewReceipt
//            self.navigationController?.pushViewController(vc!, animated: true)
//        }

        
        
        guard let cbToken = cbBankTokenModel else { return }
        if appDelegate.checkNetworkAvail() {
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
            let web      = WebApiClass()
            web.delegate = self
            let urlString = String.init(format: Url.cbBankTransfer)
            let urlStr = getUrl(urlStr: urlString, serverType: .CBBankPay)
//            guard let params = self.getCBBankParams1() else { return
//                PTLoader.shared.hide()
//            }
            println_debug(urlStr)
            let data = wrapDataModel()
            println_debug(String(data: data ?? Data(), encoding: .utf8))
            let dic = Dictionary<String,Any>()
            web.genericClass(url: urlStr, param: dic as AnyObject, httpMethod: "POST", mScreen: "CBBankValidation", hbData: data, authToken: cbToken.data?.token , authTokenKey: "X-access-token")
        }else {
            println_debug("No Internet")
        }
    }
    
    func sendMoney() {
        let accountDetails = getAccountDetailsDic()
        if let _ = cbBankTokenModel {
            if accountDetails.bankName == "Co-operative Bank Limited" || accountDetails.bankId == 5 {
                self.checkAndAddCBBankTODB()
                //  self.payToCBBank()
            } else {
                    //  self.payToCBBank()
                let isBankExists =  CoreDataHelper.shared.isBankInActivestate(accDetail: accountDetails)
                self.didSelect_SendMoney(toExistBank: isBankExists, accDetails: accountDetails)
            }
        } else {
              //    self.payToCBBank()
            let isBankExists =  CoreDataHelper.shared.isBankInActivestate(accDetail: accountDetails)
            self.didSelect_SendMoney(toExistBank: isBankExists, accDetails: accountDetails)
        }
    }
    
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "Confirmation".localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21.0) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
    
    @IBAction func saveBtnAction(_ sender: Any) {
        guard appDelegate.checkNetworkAvail() else {
            alertViewObj.wrapAlert(title: nil, body: "Please check your internet connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK", style: .cancel, action: {
            })
            alertViewObj.showAlert(controller: self)
            return
        }
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "AddingBank", delegate: self)
        } else {
            self.didSelect_SaveBank()
        }
    }
    
    @IBAction func sendMoneyBtnAction(_ sender: Any) {
        guard appDelegate.checkNetworkAvail() else {
            alertViewObj.wrapAlert(title: nil, body: "Please check your internet connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK", style: .cancel, action: {
            })
            alertViewObj.showAlert(controller: self)
            return
        }
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "SendMoney", delegate: self)
        } else {
            self.sendMoney()
        }
    }
}
 
extension BankConfirmationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myDataValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let confirmCell = tableView.dequeueReusableCell(withIdentifier: "confirmationcellidentifier", for: indexPath) as! ConfirmationCell
        let myItemValue = myDataValues[indexPath.row]
        switch myItemValue.itemType {
        case .amount:
            confirmCell.labelTitle.text = myItemValue.itemTitle.localized
            if let attrAmount = confirmCell.attributedString(amount: myItemValue.itemValue.localized) {
                confirmCell.labelValue.attributedText = attrAmount
            } else {
                confirmCell.labelValue.text = myItemValue.itemValue.localized
            }
        case .none :
            let cell = tableView.dequeueReusableCell(withIdentifier: "SMBInfoCell", for: indexPath) as! SMBInfoCell
                cell.lblInfo.text = myItemValue.itemValue.localized
            return cell
        default:
            confirmCell.labelTitle.text = myItemValue.itemTitle.localized
            confirmCell.labelValue.text = myItemValue.itemValue.localized
        }
        return confirmCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 450
        tableView.rowHeight = UITableView.automaticDimension
        return tableView.rowHeight
    }
}

extension BankConfirmationViewController {
    func didSelect_SaveBank() {
        let accountDetails = getAccountDetailsDic()
        if self.canAllowToAddBank() {
            self.addNewBankApi(accDetail: accountDetails, completionHandler: { (isSuccess, response) in
                guard isSuccess else {
                    return
                }
                DispatchQueue.main.async {
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: SendMoneyToBankViewController.self) {
                            self.delegate = controller as? ConfirmationDelegate
                            self.delegate?.removeConfirmationScreen()
                            break
                        }
                    }
                }
            })
        }
    }
    
    func didSelect_SendMoney(toExistBank: Bool, accDetails: BankAccountModel) {
        let cashOut                 = BankCashOutModel()
        cashOut.accountNumber       = accDetails.accountNumber
        cashOut.bankId              = accDetails.bankId
        cashOut.branchId            = accDetails.branchId
        cashOut.cashOutAmount       = accDetails.amount.replacingOccurrences(of: ",", with: "")
        cashOut.cashOutStatus       = false // after success change this to true
        cashOut.isActiveBank        = true
        
        if toExistBank {
            // send money to existing bank means direct hit the cashout api and show the successfull screen
            self.bankCashOutApi(accDetail: accDetails, cashOutDetail: cashOut, completionHandler: { [weak self] (isSuccess, response) in
                // here need to navigate to successfull screen
                guard isSuccess else {
                    return
                }
                if (self?.isUpdateBankNeeded(accDetail: accDetails))! {
                    self?.updateBankApi(accDetail: accDetails)
                }
                
                DispatchQueue.main.async {
                     let vc = UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "SMBNewReceipt") as? SMBNewReceipt
                    vc?.accountDetails = accDetails
                    if let dict = response as? Dictionary<String, Any> {
                        println_debug("OtherBank Response: \(dict)")
                        vc?.instantTransfer = false
                        vc?.transactionDict = dict
                    }
                    self?.navigationController?.pushViewController(vc!, animated: true)
                }
                
            })
        } else {
            // that bank is not in our existing means first hit add bank api and then hit cashout api and show the successfull screen
            if self.canAllowToAddBank() {
                self.addNewBankAndCashOutApi(accDetail: accDetails, cashOutDetail: cashOut, completionHandler: { (isSuccess,errMsg,response) in
                    guard isSuccess else {
                        if errMsg == "addbank" {
                            
                            DispatchQueue.main.async {
                                for controller in self.navigationController!.viewControllers as Array {
                                    if controller.isKind(of: SendMoneyToBankViewController.self) {
                                        self.delegate = controller as? ConfirmationDelegate
                                        self.delegate?.removeConfirmationScreen()
                                        break
                                    }
                                }
                            }
                            
                        }
                        return
                    }
                    
                    DispatchQueue.main.async {
                      let vc = UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "SMBNewReceipt") as? SMBNewReceipt
                        vc?.accountDetails = accDetails
                        if let dict = response as? Dictionary<String, Any> {
                            println_debug("OtherBank Response: \(dict)")
                            vc?.instantTransfer = false
                            vc?.transactionDict = dict
                        }
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }
                    
                })
            }
        }
    }
    
    func isUpdateBankNeeded(accDetail: BankAccountModel) -> Bool {
        if let bankDetail = CoreDataHelper.shared.getBankFromDB(accDetail: accDetail) {
            if bankDetail.remarks == accDetail.remarks {
                if bankDetail.emailId == accDetail.emailid {
                    return false
                }
            }
        }
        return true
    }
}

extension BankConfirmationViewController: BioMetricLoginDelegate {
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if screen == "AddingBank" {
            if isSuccessful{
                self.didSelect_SaveBank()
            }
        } else if screen == "SendMoney" {
            if isSuccessful{
                self.sendMoney()
            }
        }
    }
}

extension BankConfirmationViewController: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        if screen == "CBBankValidation" {
            guard let castedData = json as? Data else {
                PTLoader.shared.hide()
                println_debug("Data error")
                return
            }
            do {
                PTLoader.shared.hide()
                
                let validation = try JSONSerialization.jsonObject(with: castedData, options: .allowFragments) as? Dictionary<String, Any>
                println_debug("CBBankValidation : \(String(describing: validation))")
                if let code = validation?["code"] as? Int {
                    if code == 200 {
                        if let dataDic = validation?["data"] as? Dictionary<String, Any> {
                            if dataDic["StatusCode"] as? Int == 201 || dataDic["StatusCode"] as? String == "201" {
                                if let resultDict = dataDic["Result"] as? Dictionary<String, Any> {
                                    println_debug(resultDict)
                                    if let estelDict = resultDict["EstelData"] as? Dictionary<String, Any> {
                                        println_debug(estelDict)
                                        let accountDetails = getAccountDetailsDic()
                                        if (self.isUpdateBankNeeded(accDetail: accountDetails)) {
                                            self.updateBankApi(accDetail: accountDetails)
                                        }
                                        var cbTransID = ""
                                        if let cbData = resultDict["CBData"] as? Dictionary<String, Any> {
                                            println_debug(cbData)
                                            cbTransID = cbData["BankTransactionId"] as? String ?? ""
                                        }
                                         //New Receipt Code
                                        DispatchQueue.main.async {
                                            let vc = UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "SMBNewReceipt") as? SMBNewReceipt
                                            vc?.accountDetails = accountDetails
                                            let dict = ["walletbalance" : estelDict["PostWallet"] as? String ?? "", "transid" : estelDict["OKTransactionId"] as? String,"cbTransID" : cbTransID]
                                            vc?.transactionDict = dict as [String : Any]
                                            vc?.instantTransfer = self.instantTransfer
                                            self.navigationController?.pushViewController(vc!, animated: true)
                                        }
                                        
                                        // Old Receipt code
//                                        DispatchQueue.main.async {
//                                            let vc = UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "BankSuccessfulScreen_ID") as? BankSuccessfulScreenViewController
//                                            vc?.accountDetails = accountDetails
//                                            let dict = ["walletbalance" : estelDict["PostWallet"] as? String ?? "", "transid" : estelDict["OKTransactionId"] as? String]
//                                            vc?.transactionDict = dict as [String : Any]
//                                            self.navigationController?.pushViewController(vc!, animated: true)
//                                        }
                                        
                                    }
                                }
                            }else if dataDic["StatusCode"] as? Int == 300 || dataDic["StatusCode"] as? String == "300" {
                                if (dataDic["Message"] as? String ?? "").lowercased().contains(find: "secure token expire") {
                                    DispatchQueue.main.async {
                                        alertViewObj.wrapAlert(title: nil, body: "Secure Token Expire".localized, img: #imageLiteral(resourceName: "smToBank"))
                                        alertViewObj.addAction(title: "OK", style: .cancel, action: {
                                            OKPayment.main.authenticate(screenName: "SendMoney", delegate: self)
                                        })
                                        alertViewObj.showAlert(controller: self)
                                        return
                                    }
                                }else {
                                    if let message = dataDic["Message"] as? String {
                                        println_debug(message)
                                        DispatchQueue.main.async {
                                            alertViewObj.wrapAlert(title: nil, body: message, img: #imageLiteral(resourceName: "smToBank"))
                                            alertViewObj.addAction(title: "OK", style: .cancel, action: {
                                            })
                                            alertViewObj.showAlert(controller: self)
                                            return
                                        }
                                    }
                                }
                            }
                        }else {
                            DispatchQueue.main.async {
                                alertViewObj.wrapAlert(title: nil, body: "Something went wrong. Please try again!".localized, img: #imageLiteral(resourceName: "smToBank"))
                                alertViewObj.addAction(title: "OK", style: .cancel, action: {
                                })
                                alertViewObj.showAlert(controller: self)
                                return
                            }
                        }
                        
                        
                        
                    }else {
                        DispatchQueue.main.async {
                            alertViewObj.wrapAlert(title: nil, body: "Something went wrong. Please try again!".localized, img: #imageLiteral(resourceName: "smToBank"))
                            alertViewObj.addAction(title: "OK", style: .cancel, action: {
                            })
                            alertViewObj.showAlert(controller: self)
                            return
                        }
                        
                    }
                }else {
                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: nil, body: "Something went wrong. Please try again!".localized, img: #imageLiteral(resourceName: "smToBank"))
                        alertViewObj.addAction(title: "OK", style: .cancel, action: {
                        })
                        alertViewObj.showAlert(controller: self)
                        return
                    }
                    
                }
            } catch {
                
            }
        }
    }
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        PTLoader.shared.hide()
        if screenName == "CBBankValidation" {
            println_debug("Api Failure in PhNoValidation response")
        }
    }
}

struct CBBankTokenModel: Codable {
    let code: Int?
    let error: Bool?
    let msg: String?
    let data: CBBankTokenDataClass?
    
    enum CodingKeys: String, CodingKey {
        case code = "code"
        case error = "error"
        case msg = "msg"
        case data = "data"
    }
}

// MARK: - DataClass
struct CBBankTokenDataClass: Codable {
    let token, expiresIn: String?
    
    enum CodingKeys: String, CodingKey {
        case token = "token"
        case expiresIn = "ExpiresIn"
    }
}



extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
}


extension BankConfirmationViewController {
    private func getNum(num: String) -> String {
        switch num {
        case "N":
            return "N"
        case "P":
            return "P"
        case "E":
            return "E"
        case "T":
            return "T"
        case "ႏိုင္":
            return "N"
        case "ျပဳ":
            return "P"
        case "ဧည့္":
            return "E"
        case "သာ":
            return "T"
        case "၀" :
            return "0"
        case "၁" :
            return "1"
        case "၂" :
            return "2"
        case "၃" :
            return "3"
        case "၄" :
            return "4"
        case "၅" :
            return "5"
        case "၆" :
            return "6"
        case "၇" :
            return "7"
        case "၈" :
            return "8"
        case "၉" :
            return "9"
        case "၁၀" :
            return "10"
        case "၁၁" :
            return "11"
        case "၁၂" :
            return "12"
        case "၁၃" :
            return "13"
        case "၁၄" :
            return "14"
        case "0" :
            return "0"
        case "1" :
            return "1"
        case "2" :
            return "2"
        case "3" :
            return "3"
        case "4" :
            return "4"
        case "5" :
            return "5"
        case "6" :
            return "6"
        case "7" :
            return "7"
        case "8" :
            return "8"
        case "9" :
            return "9"
        case "10" :
            return "10"
        case "11" :
            return "11"
        case "12" :
            return "12"
        case "13" :
            return "13"
        case "14" :
            return "14"
        default:
            return ""
        }
    }
    
    func getNRCInEnglish(nrc: String) -> String {
        var divisionTownshipName = ""
        if let townshipPath = Bundle.main.path(forResource: "townshipJson", ofType: "json")  {
            do {
                let townshipData = try Data(contentsOf: URL(fileURLWithPath: townshipPath), options: .mappedIfSafe)
                let townshipJson = try JSONSerialization.jsonObject(with: townshipData, options: .mutableLeaves)
                if let townshipJson = townshipJson as? Dictionary<String, AnyObject>, let allTownshipList = townshipJson["data"] as? [AnyObject] {
                    
                    let filterTownShipList = allTownshipList.filter { (town) -> Bool in
                        if let townObj = town as? Dictionary<String, AnyObject> {
                            return (townObj["NrcBCode"] as? String ?? "") == nrc
                        } else {
                            return false
                        }
                    }
                    var nrcName = ""
                    if filterTownShipList.count > 0 {
                        for (_, townshipValue) in filterTownShipList.enumerated() {
                            // here create new township instance for that selected division
                            if let dic = townshipValue as? Dictionary<String, AnyObject> {
                                nrcName = dic["NrcCode"] as! String
                                divisionTownshipName = nrcName
                            }
                        }
                    }
                }
                
            } catch {
                println_debug("something went wrong when extract data from township json file")
            }
        }
        return (divisionTownshipName == "") ? nrc : divisionTownshipName
    }
    
    func convertNrctoEnglish(nrc: String) -> (String, Bool) {
//        let nrc = "၁၃/ဟသတ(၈)၇၄၂၃၉၀"
        let divCode = nrc.components(separatedBy: "/")
        let num = self.getNum(num: divCode.first ?? "")
        let nrcName = divCode.last?.components(separatedBy: "(")
        let second = self.getNRCInEnglish(nrc: nrcName?.first ?? "")
        let char = nrcName?.last?.components(separatedBy: ")")
        let charNum = self.getNum(num: char?.first ?? "")
        let lastnumArr = char?.last ?? ""
        var numCharArr = ""
        if lastnumArr.count != 6 {
            return ("Invalid NRC", false)
        }
        
        for char1 in lastnumArr {
            let charSin = self.getNum(num: String(char1))
            numCharArr = numCharArr + charSin
        }
        
        return ("\(num)/\(second)(\(charNum))\(numCharArr)", true)
        
    }
}



// MARK: - Welcome
struct FinaleCBPay: Codable {
    let appInfo: CBPayAppInfo
    let cbAcount: CBAcountInfo

    enum CodingKeys: String, CodingKey {
        case appInfo = "AppInfo"
        case cbAcount = "CBAcount"
    }
}

// MARK: - AppInfo
struct CBPayAppInfo: Codable {
    let appVersion: String
    let cellID, latitude, longitude, mobileNumber: String
    let msID: String
    let profileImg: String
    let secureToken, simID: String

    enum CodingKeys: String, CodingKey {
        case appVersion = "AppVersion"
        case cellID = "CellId"
        case latitude = "Latitude"
        case longitude = "Longitude"
        case mobileNumber = "MobileNumber"
        case msID = "MsId"
        case profileImg = "ProfileImg"
        case secureToken = "SecureToken"
        case simID = "SimId"
    }
}

// MARK: - CBAcount
struct CBAcountInfo: Codable {
    let amount: Int
    let bankAccountNumber, comments, name, nrc: String
    let okAccountNumber, password, phoneNumber, projectID: String

    enum CodingKeys: String, CodingKey {
        case amount = "Amount"
        case bankAccountNumber = "BankAccountNumber"
        case comments = "Comments"
        case name = "Name"
        case nrc = "Nrc"
        case okAccountNumber = "OkAccountNumber"
        case password = "Password"
        case phoneNumber = "PhoneNumber"
        case projectID = "ProjectId"
    }
}
