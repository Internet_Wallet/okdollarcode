//
//  SMBNewReceipt.swift
//  OK
//
//  Created by Vinod Kumar on 29/07/20.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class SMBNewReceipt: SendMoneyBaseViewController {
    @IBOutlet weak var tvReceipt: UITableView!
    @IBOutlet var myScroll: UIScrollView!
    @IBOutlet var btnHome: UIButton!
    @IBOutlet var btnShare: UIButton!
    
    @IBOutlet var bgReceipt: UIView!
    @IBOutlet var bgImageContainer: UIView!
    @IBOutlet var imgIcon: UIImageView!
    @IBOutlet var lblBankTans: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblOkDetails: UILabel!
    @IBOutlet var lblOKValue: UILabel!
    @IBOutlet var lblTansId: UILabel!
    @IBOutlet var lblTansValue: UILabel!
    @IBOutlet var lblBankDetails: UILabel!
    @IBOutlet var lblBankDetailsValue: UILabel!
    @IBOutlet var lblBankTransID: UILabel!
    @IBOutlet var lblBankTransIDValue: UILabel!
    @IBOutlet var lblDuration: UILabel!
    @IBOutlet var lblDurationValue: UILabel!
    @IBOutlet var lblAmount: UILabel!
    @IBOutlet var lalAmountValue: UILabel!
    @IBOutlet var lblFooter: UILabel!
    
    @IBOutlet var lblBusinessDetails: UILabel!
    @IBOutlet var lblBusiness: UILabel!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var viewBusiness: UIView!
    @IBOutlet var viewCBBank: UIView!
    @IBOutlet var viewBuration: UIView!
    @IBOutlet var businesssTopConstant: NSLayoutConstraint!
    
    @IBOutlet var durationTopConstrants: NSLayoutConstraint!
    var instantTransfer : Bool?
    var accountDetails: BankAccountModel?
    var transactionDict = [String: Any]()
    var titleList = ["Header","Business Details".localized,"OK$ Details".localized,"OK$ Transaction ID".localized,"Bank Details".localized,"Bank Transaction ID".localized,"Duration".localized,"Amount".localized,"Button","Footer"]
        
    var valueList = ["Header","Mr.Subhash Arya +959979275277","Mr.Subhash Arya +959979275277","1234566779","Co-operative Bank Limited, Mo Gaung, Saving Account, 005496808634098609","FRTHF2434344FG","24 Hrs Banking Day","20000","Button","20000"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tvReceipt.delegate = self
        tvReceipt.dataSource = self
        btnBack.isHidden = true
        
        
      //  instantTransfer = true
        // loadNavigationBarSettings()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 21) ?? UIFont.systemFont(ofSize: 21), NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationItem.title = "Receipt".localized
        self.loadUI()
    }
    
    @IBAction func onClickBackButton(_: UIButton) {
       // self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Receipt".localized
        clear_BankAccountDetailsDic()
        BankViewModel.refreshBank()
        BankViewModel.refreshNRC()
    }
    
    @IBAction func onClicHomeAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func onClickShareAction(_ sender: Any) {
        alertViewObj.wrapAlert(title:"", body: "Do you want to share Receipt?".localized, img: UIImage(named: "save_gray"))
        alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
        })
        alertViewObj.addAction(title: "Share".localized, style: .target , action: {
            self.shareQRWithDetail()
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    func shareQRWithDetail() {
        let image = captureScreen(view: bgReceipt)
        let imageToShare = [image]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        DispatchQueue.main.async {
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    func loadUI() {
        self.view.backgroundColor = kBackGroundGreyColor
        lblBankTans.font = UIFont(name: appFont, size: 16)
        lblBusinessDetails.font = UIFont(name: appFont, size: 15)
        lblBusiness.font = UIFont(name: appFont, size: 15)
        lblOkDetails.font = UIFont(name: appFont, size: 15)
        lblOKValue.font = UIFont(name: appFont, size: 15)
        lblTansId.font = UIFont(name: appFont, size: 15)
        lblTansValue.font = UIFont(name: appFont, size: 15)
        lblBankDetails.font = UIFont(name: appFont, size: 15)
        lblBankDetailsValue.font = UIFont(name: appFont, size: 15)
        lblBankTransID.font = UIFont(name: appFont, size: 15)
        lblBankTransIDValue.font = UIFont(name: appFont, size: 15)
        lblDuration.font = UIFont(name: appFont, size: 15)
        lblDurationValue.font = UIFont(name: appFont, size: 15)
        lblAmount.font = UIFont(name: appFont, size: 15)
        lalAmountValue.font = UIFont(name: appFont, size: 15)
        lblFooter.font = UIFont(name: appFont, size: 18)
        
        println_debug("Transaction detials : \(transactionDict)")
        if let accDetail = self.accountDetails {
            var bankname = ""
            var branchname = ""
            if let curLanguage = UserDefaults.standard.value(forKey: "currentLanguage") as? String {
                switch curLanguage {
                case "en":
                    bankname = accDetail.bankName
                    branchname = accDetail.branchName
                case "my":
                    bankname = accDetail.bankBurmeseName
                    branchname = accDetail.branchBurmeseName
                case "uni":
                    bankname = accDetail.bankBurmeseNameUnicode
                    branchname = accDetail.branchBurmeseNameUnicode
                default:
                    bankname = accDetail.bankName
                    branchname = accDetail.branchName
                }
            }
            println_debug("account detials : \(accDetail)")
            valueList[2] = "\(UserModel.shared.name)\n+95 \(self.getActualNum(UserModel.shared.mobileNo,withCountryCode: "+95"))"
            valueList[3] = "\(transactionDict["transid"] as? String ?? "")"
            valueList[4] = "\(bankname)\n\(branchname)\n\(self.setBackAccountType(at: accDetail.accountType))\n\(accDetail.accountNumber)"
            valueList[5] = "\(transactionDict["cbTransID"] as? String ?? "")"
            
            if instantTransfer ?? false {
                valueList[6] = "Quick Transfer".localized
            }else {
                valueList[6] = "24 Hrs Banking Day".localized
            }
            
            let amount = accDetail.amount.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: ".00", with: "")
            valueList[7] = amount
            if let bal = self.transactionDict["walletbalance"] as? String {
                valueList[9] = bal
            }
        }
        valueList[8] = "3923434"
        lblBankTans.text = "Bank Transfer Receipt".localized
        
        
        lblDate.text = getDate()
        lblTime.text = getTime()
        
        lblBusiness.text = "Business Details".localized
        
        if UserModel.shared.agentType == .user {
            self.businesssTopConstant.constant = -43.5
            self.viewBusiness.isHidden = true
            titleList[2] = "OK$ Details".localized
            lblOkDetails.text = titleList[2]
            lblOKValue.text = valueList[2]
        }else {
            self.businesssTopConstant.constant = 0
            self.viewBusiness.isHidden = false
            lblBusinessDetails.text = "\(UserModel.shared.businessName)\n\(UserModel.shared.businessOutlet)\n+95  \(self.getActualNum(UserModel.shared.mobileNo,withCountryCode: "+95"))"
            valueList[1] = "\(UserModel.shared.businessName)\n\(UserModel.shared.businessOutlet)\n+95 \(self.getActualNum(UserModel.shared.mobileNo,withCountryCode: "+95"))"
            lblOkDetails.text = "OK$ Account Holder".localized
            lblOKValue.text = "\(UserModel.shared.name)\n\(UserModel.shared.nrc)"
            titleList[2] = "OK$ Account Holder".localized
            valueList[2] = "\(UserModel.shared.name)\n\(UserModel.shared.nrc)"
        }
        
        self.lblTansId.text = titleList[3]
        self.lblTansValue.text = valueList[3]
        
        self.lblBankDetails.text = titleList[4]
        self.lblBankTransID.text = titleList[5]
        self.lblBankTransIDValue.text = valueList[5]
        if instantTransfer ?? false {
            self.durationTopConstrants.constant = 0
           // self.viewCBBank.isHidden = false
        lblBankDetailsValue.attributedText = attributedStringWithFontWtihStarSMB(symbol: "\(valueList[4])\n", symbolSize: 16, string: " Associated with OK$".localized, stirngSize: 16, image: #imageLiteral(resourceName: "linkSMB.png"))
        }else {
            self.lblBankDetailsValue.text = valueList[4]
            self.durationTopConstrants.constant = -(viewBuration.frame.height) + 3
           // self.viewCBBank.isHidden = true
        }
        
        self.lblDuration.text = titleList[6]
        self.lblDurationValue.text = valueList[6]
        
        self.lblAmount.text = titleList[7]
        self.lalAmountValue.attributedText = attributedStringWithFontWtihBackColor(amount: self.amountInFormat(valueList[7]), fontSize: 18)
        
        lblFooter.text = "Easy - Fast - Secure".localized
        
        let bgimage = UIImageView()
        bgimage.frame = bgReceipt.frame
        bgimage.image = UIImage(named: "bg_receipt")
        bgimage.contentMode = .scaleToFill
        bgImageContainer.addSubview(bgimage)
        
        self.saveToPrint()
        tvReceipt.reloadData()
    }
    
    func setBackAccountType(at: String) -> String {
        var accountType = ""
        switch at {
        case "CDA":
            accountType = "Call Deposit Account".localized
            break
        case "SAV":
            accountType = "Savings Account".localized
            break
        case "CUR":
            accountType = "Current Account".localized
            break
        case "ACA":
            accountType = "ATM Card Account".localized
            break
        default:
            accountType = "Savings Account".localized
            break
        }
        return accountType
    }
    
    func getDate() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "EEE, dd-MMM-yyyy"
        return formatter.string(from: date)
    }
    
    func getTime() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "HH:mm:ss"
        return formatter.string(from: date)
    }
}

extension SMBNewReceipt: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "smbNewReceiptCellHeader", for: indexPath) as! smbNewReceiptCellHeader
            cell.lblTitle.text = "Bank Transfer Successful".localized
            cell.lblDate.text = getDate()
            cell.lblTime.text = getTime()
            cell.selectionStyle = .none
            return cell
        }else if indexPath.row == 8 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "smbNewReceiptButtonCell", for: indexPath) as! smbNewReceiptButtonCell
            cell.selectionStyle = .none
            return cell
        }else if indexPath.row == 9 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "smbNewReceiptCellFootter", for: indexPath) as! smbNewReceiptCellFootter
            cell.lblTitle.text = "OK$ New Balance".localized
            cell.lblValue.attributedText = attributedStringWithFontWtihAllRedColor(amount: self.amountInFormat(valueList[9]), fontSize: 18)
            cell.lblDescription.text = "Receipt already saved in photo gallery".localized
            cell.selectionStyle = .none
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "smbNewReceiptCell", for: indexPath) as! smbNewReceiptCell
            cell.lblTitle.text = titleList[indexPath.row]
            if indexPath.row == 7 {
                cell.lblValue.attributedText = attributedStringWithFontWtihBackColor(amount: self.amountInFormat(valueList[7]), fontSize: 18)
            }else if indexPath.row == 4 {
                if instantTransfer ?? false {
                    cell.lblValue.attributedText = attributedStringWithFontWtihStarSMB(symbol: "\(valueList[indexPath.row])\n", symbolSize: 16, string: " Associated with OK$", stirngSize: 16, image: #imageLiteral(resourceName: "linkSMB.png"))
                }else {
                    cell.lblValue.text = valueList[indexPath.row]
                }
            }else {
                cell.lblValue.text = valueList[indexPath.row]
            }
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 120
        }else if indexPath.row == 1 {
            if UserModel.shared.agentType == .user {
                return 0
            }else {
                return UITableView.automaticDimension
            }
        }else if indexPath.row == 5 {
            if instantTransfer ?? false {
                return UITableView.automaticDimension
            }else {
                return 0
            }
        }else if indexPath.row == 8 {
            return 50
        }else if indexPath.row == 9 {
            return 140
        }else {
            return UITableView.automaticDimension
        }
    }
}

extension SMBNewReceipt {
     func saveToPrint() {
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.authorized) {
            println_debug(".authorized")
            DispatchQueue.main.async {
                self.saveToGallary()
            }
        }else if (status == PHAuthorizationStatus.denied) {
            println_debug(".denied")
            self.showPhotoAlert()
        }else if (status == PHAuthorizationStatus.notDetermined) {
            println_debug(".notDetermined")
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                if (newStatus == PHAuthorizationStatus.authorized) {
                    println_debug("Receipt already saved in photo gallery")
                    DispatchQueue.main.async {
                        self.saveToGallary()
                    }
                }else {
                    self.showPhotoAlert()
                }
            })
        }else if (status == PHAuthorizationStatus.restricted) {
            // Restricted access - normally won't happen.
        }
    }
    
    func showPhotoAlert() {
        alertViewObj.wrapAlert(title:"", body:"Please Allow Photo Access".localized, img: UIImage(named: "alert-icon"))
        alertViewObj.addAction(title: "Setting".localized, style: .target , action: {
            UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
        })
        alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
        }
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
     func saveToGallary() {
            let newImage = self.bgReceipt.toImage()
        UIImageWriteToSavedPhotosAlbum(newImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
            alertViewObj.wrapAlert(title:"", body: "Receipt already saved in photo gallery".localized, img: UIImage(named: "save_gray"))
            alertViewObj.addAction(title: "DoneQR".localized, style: .target , action: {
                
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
    }
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
          if let error = error {
              alertViewObj.wrapAlert(title: "Save error", body: error.localizedDescription, img: nil)
              alertViewObj.addAction(title: "OK".localized, style: .target , action: {
              })
              alertViewObj.showAlert(controller: self)
          } else {
              println_debug("Your altered image has been saved to your photos.")
          }
      }
}

class smbNewReceiptCellHeader : UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblSeperator: UILabel!
    override  func awakeFromNib() {
          super.awakeFromNib()
          lblTitle.font = UIFont(name: appFont, size: 18)
      }
}

class smbNewReceiptCell : UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var lblSeperator: UILabel!
    override  func awakeFromNib() {
        super.awakeFromNib()
        lblTitle.font = UIFont(name: appFont, size: 16)
        lblValue.font = UIFont(name: appFont, size: 16)
    }
}
class smbNewReceiptButtonCell : UITableViewCell {
    @IBOutlet weak var btnValue: UIButton!
    @IBOutlet weak var lblSeperator: UILabel!
    override  func awakeFromNib() {
        super.awakeFromNib()
        btnValue.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
    }
}
class smbNewReceiptCellFootter : UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var vwLayer: UIView!
    @IBOutlet weak var lblDescription: UILabel!
 
    override  func awakeFromNib() {
        super.awakeFromNib()
        vwLayer.layer.cornerRadius = 5
        vwLayer.layer.masksToBounds = true
        vwLayer.layer.borderColor = #colorLiteral(red: 0.9332516193, green: 0.9333856702, blue: 0.9332222342, alpha: 1)
        vwLayer.layer.borderWidth = 1.5
        lblTitle.font = UIFont(name: appFont, size: 18)
        lblDescription.font = UIFont(name: appFont, size: 18)
    }
    
}

