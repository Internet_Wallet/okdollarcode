//
//  BankTownshipViewController.swift
//  OK
//
//  Created by Uma Rajendran on 12/21/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BankTownshipViewController: SendMoneyBaseViewController {

    @IBOutlet weak var townshipTable: UITableView!
    
    private lazy var searchBar = UISearchBar()
    var townshipListUI          = [BankBranchByTownship]()
    var searchList      = [BankBranchByTownship]()
    
    var townshipListUIDB          = [BankByTownship]()
    var searchListDB      = [BankByTownship]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadInitialization()
        loadUI()
        setUpNavigation()
        setUpSearchBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.townshipTable.isUserInteractionEnabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func loadUI() {
        self.view.backgroundColor = kBackGroundGreyColor
        self.townshipTable.backgroundColor = kBackGroundGreyColor
        self.townshipTable.register(UINib(nibName: "BankListCell", bundle: nil), forCellReuseIdentifier: "listcellidentifier")
        self.townshipTable.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    private func removeDuplicatesFromTownship(townshipArray: [BankByTownship], _ completion: @escaping (_ accessGranted: Bool, _ result: [BankByTownship]) -> Void) {
        var result = [BankByTownship]()
        for value in townshipArray {
            let status = result.contains { (singleObj) -> Bool in
                return (singleObj.townshipCode ?? "") == (value.townshipCode ?? "") ? true : false
            }
            if !status {
                result.append(value)
            }
        }
        completion(true, result)
    }
    
    func loadInitialization() {
        PaymentVerificationManager.getAllTownshipFromDB(divisionCode: BankViewModel.shared.selectedDivisionDB?.divisionCode, bankId: BankViewModel.shared.selectedBankDB?.bankId) { (status, townShipArray) in
            DispatchQueue.main.async {
                if status, let townShip = townShipArray {
//                    self.removeDuplicatesFromTownship(townshipArray: townShip, { (status, array) in
                        self.townshipListUIDB = townShip.sorted(by: {($0.townshipEName ?? "") < ($1.townshipEName ?? "")})
                        self.searchListDB = self.townshipListUIDB
                        self.townshipTable.reloadData()
//                    })
                    
                } else {
                    self.townshipListUIDB = []
                }
                self.townshipTable.isUserInteractionEnabled = true
            }
        }
//        if let myList = BankViewModel.shared.selectedDivision?.branchListByTownship {
//            townshipListUI = myList.sorted(by: {$0.townshipNameEN < $1.townshipNameEN})
//            searchList = townshipListUI
//        } else {
//            townshipListUI = []
//        }
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func setUpSearchBar() {
        searchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44)
        if #available(iOS 11.0, *) {
            searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.tintColor = UIColor.white
        searchBar.placeholder = "Search".localized
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
            searchTextField.keyboardType = .asciiCapable
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
                searchTextField.keyboardType = .asciiCapable
            }
        }
        let view = self.searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = UIColor.gray
            }
        }
    }
    
    func resetTownshipScreen() {
        setUpNavigation()
        searchListDB = townshipListUIDB
        townshipTable.reloadData()
    }
    
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        if townshipListUI.count > 1 {
            let searchButton = UIButton(type: .custom)
            searchButton.setImage(#imageLiteral(resourceName: "search_white").withRenderingMode(.alwaysOriginal), for: .normal)
            searchButton.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
            if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
                searchButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
            }else{
                searchButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
            }
            navTitleView.addSubview(searchButton)
        }
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "Select Township".localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21.0) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
    
    @objc func searchAction() {
        searchBar.placeholder = "Search".localized
        searchBar.text = ""
        self.navigationItem.titleView = searchBar
        searchBar.becomeFirstResponder()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInset = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
            self.townshipTable.contentInset = contentInset
            self.townshipTable.scrollIndicatorInsets = contentInset
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.townshipTable.contentInset = UIEdgeInsets.zero
        self.townshipTable.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }

}

extension BankTownshipViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if searchListDB.count == 0 {
            let noDataLabel = UILabel(frame: tableView.bounds)
            if let navFont = UIFont(name: appFont, size: 21) {
                noDataLabel.font = navFont
            }
            noDataLabel.text = "No records found!".localized
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
        } else {
            tableView.backgroundView = nil
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchListDB.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let banktownshipcell = tableView.dequeueReusableCell(withIdentifier: "listcellidentifier", for: indexPath) as! BankListCell
        let bankTownship = searchListDB[indexPath.row] as BankByTownship
        var townshipname = ""
        if let curLanguage = UserDefaults.standard.value(forKey: "currentLanguage") as? String {
            switch curLanguage {
            case "en":
                townshipname = bankTownship.townshipEName ?? ""
            case "my":
                townshipname = bankTownship.townshipBName ?? ""
            case "uni":
                townshipname = bankTownship.townshipUName ?? ""
            default:
                townshipname = bankTownship.townshipEName ?? ""
            }
        }
        banktownshipcell.wrapData(title: townshipname, count: bankTownship.branchCount ?? "0")
        return banktownshipcell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let banktownship = searchListDB[indexPath.row] as BankByTownship
        func pushToBranch() {
            let story = UIStoryboard(name: "SendMoney", bundle: nil)
            let branchView = story.instantiateViewController(withIdentifier: "BankBranchesListView_ID") as! BankBranchesListViewController
            BankViewModel.shared.selectedTownshipDB = banktownship
            self.navigationController?.pushViewController(branchView, animated: true)
        }
        if searchBar.isFirstResponder {
            DispatchQueue.main.async {
                self.searchBar.endEditing(true)
                self.resetTownshipScreen()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.60) {
                    pushToBranch()
                }
            }
        } else {
            pushToBranch()
        }
        townshipTable.isUserInteractionEnabled = false
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

// MARK: -UISearchBarDelegate
extension BankTownshipViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            uiButton.setTitle("Cancel".localized, for:.normal)

            if let navFont = UIFont(name: appFont, size: 18) {
                uiButton.titleLabel?.font = navFont
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            searchListDB = townshipListUIDB
            townshipTable.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            if updatedText.count > 30 {
                return false
            }
            if updatedText != "" && text != "\n" {
                searchListDB = []
                if let curLanguage = UserDefaults.standard.value(forKey: "currentLanguage") as? String {
                    switch curLanguage {
                    case "en":
                        searchListDB = townshipListUIDB.filter {
                            guard let _ = ($0.townshipEName ?? "").localized.localizedStandardRange(of: updatedText) else { return false }
                            return true
                        }
                    case "my":
                        searchListDB = townshipListUIDB.filter {
                            guard let _ = ($0.townshipBName ?? "").localized.localizedStandardRange(of: updatedText) else { return false }
                            return true
                        }
                    case "uni":
                        searchListDB = townshipListUIDB.filter {
                            guard let _ = ($0.townshipUName ?? "").localized.localizedStandardRange(of: updatedText) else { return false }
                            return true
                        }
                    default:
                        searchListDB = townshipListUIDB.filter {
                            guard let _ = ($0.townshipEName ?? "").localized.localizedStandardRange(of: updatedText) else { return false }
                            return true
                        }
                    }
                } else {
                    return false
                }
                townshipTable.reloadData()
            }
        }
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        resetTownshipScreen()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}
