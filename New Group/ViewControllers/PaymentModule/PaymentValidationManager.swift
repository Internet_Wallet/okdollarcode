//
//  PaymentValidationManager.swift
//  OK
//
//  Created by Ashish on 8/27/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreData

// MARK: - PaymentVerificationManager 
class PaymentVerificationManager {
    class func getContext() -> NSManagedObjectContext {
        return appDel.persistentContainer.viewContext
        
    }
    
    // MARK: Store PaymentTransactions to PaymentValidationManager
    class func storeTransactions(model: PaymentTransactions) {
        let context = getContext()
        PaymentVerificationManager.deleteRecordAllTopup()
        let entity = NSEntityDescription.entity(forEntityName: "PaymentValidationManager", in: context)
        if let wrapEntity = entity {
            if let object = NSManagedObject(entity: wrapEntity, insertInto: context) as? PaymentValidationManager {
                object.number       = model.number
                object.type         = model.type
                object.name         = model.name
                object.operatorName = model.operatorName
                object.amount       = model.amount
                object.time         = model.time
                object.backendNumber = model.backendNumber
            }
            do {
                try context.save()
            } catch let error as NSError {
                println_debug(error)
            }
        }
    }
    
    //var ready = convertedArray.sorted(by: { $0.compare($1) == .orderedDescending })
    // MARK: Delete top up records from PaymentValidationManager list
    class func deleteRecordAllTopup() {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PaymentValidationManager")
        let predicate = NSPredicate(format: "type == %@", "TOPUP")
        fetchRequest.predicate = predicate
        let context = getContext()
        let result = try? context.fetch(fetchRequest)
        let resultData = result as! [PaymentValidationManager]
        
        for object in resultData {
            context.delete(object)
            do {
                try context.save()
            } catch let error as NSError  {
                println_debug("Could not save \(error), \(error.userInfo)")
            }
        }
    }
    
    // MARK: Fetching PaymentValidationManager list
    class func fetchRecordsForPaymentEntity() -> [PaymentValidationManager] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PaymentValidationManager")
        let context = PaymentVerificationManager.getContext()
        var result  = [PaymentValidationManager]()
        do {
            let records = try context.fetch(fetchRequest)
            if let records = records as? [PaymentValidationManager] {
                result = records
            }
        } catch {
            println_debug("Unable to fetch managed objects for favorite entity.")
        }
        return result
    }
    
    // MARK: Getting PaymentValidationManager list
   class func getPaymentRecords() -> [PaymentValidationManager] {
        let model = PaymentVerificationManager.fetchRecordsForPaymentEntity()
        return model
    }
    
    // MARK: Getting Recent PaymentValidationManager from list
   class func getRecentTopupPayments() -> [PaymentValidationManager] {
        let model = PaymentVerificationManager.fetchRecordsForPaymentEntity()
        let topupPayments = model.filter({ $0.type?.lowercased() == "topup".lowercased() })
        return topupPayments
    }
    
    // MARK: Checking same number same amount within 2 min
    class func isValidPaymentTransactions(_ number: String, _ amount: String, _ type: String = "PAYTO") -> Bool {
        func minutesBetweenDates(startDate: Date?, endDate: Date?) -> Int? {
            let calendar = Calendar.current
            guard let start = startDate, let end = endDate else { return 1 }
            let components = calendar.dateComponents([Calendar.Component.minute], from: start, to: end)
            return components.minute
        }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PaymentValidationManager")
//        let numberPredicate   = NSPredicate(format: "number contains[c]  %@",number)
//        let amountPredicate   = NSPredicate(format: "amount contains[c]  %@", amount)
//        let compoundPredicate = NSCompoundPredicate.init(type: .and, subpredicates: [amountPredicate])
//
//        fetchRequest.predicate = compoundPredicate
        let context = PaymentVerificationManager.getContext()
        do {
            let result = try context.fetch(fetchRequest)
            guard let dataRecords = result as? [PaymentValidationManager] else { return true }
//            let filter = dataRecords.filter { ($0.number ?? "") == number }
            guard let record = dataRecords.last else { return true }
            //for record in dataRecords {
            var amountStr = record.amount ?? ""
            if amountStr.contains(find: ".") {
               let arr = amountStr.components(separatedBy: ".")
                if arr.count > 0 {
                    amountStr = arr[0]
                }
            }
            if number == record.number && amountStr == amount && type == record.type {
                guard let minutesSpend = minutesBetweenDates(startDate: record.time, endDate: Date.init(timeIntervalSinceNow: 0)) else { return true }
                return (minutesSpend > 2) ? true : false
            }
            return true
            //}
        } catch {
            println_debug(error)
        }
        return true
    }
    
    
    // MARK: Checking same operator same amount within 2 min
    class func isValidPaymentTransactionsForOperator(_ operatorToCheck: String, _ amount: String, _ number: String) -> Bool {
        func minutesBetweenDates(startDate: Date?, endDate: Date?) -> Int? {
            let calendar = Calendar.current
            guard let start = startDate, let end = endDate else { return 1 }
            let components = calendar.dateComponents([Calendar.Component.minute], from: start, to: end)
            return components.minute
        }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PaymentValidationManager")
        //        let numberPredicate   = NSPredicate(format: "number contains[c]  %@",number)
        //        let amountPredicate   = NSPredicate(format: "amount contains[c]  %@", amount)
        //        let compoundPredicate = NSCompoundPredicate.init(type: .and, subpredicates: [amountPredicate])
        //
        //        fetchRequest.predicate = compoundPredicate
        let context = PaymentVerificationManager.getContext()
        do {
            let result = try context.fetch(fetchRequest)
            guard let dataRecords = result as? [PaymentValidationManager] else { return true }
            //            let filter = dataRecords.filter { ($0.number ?? "") == number }
            guard let record = dataRecords.last else { return true }
            //for record in dataRecords {
            var amountStr = record.amount ?? ""
            if amountStr.contains(find: ".") {
                let arr = amountStr.components(separatedBy: ".")
                if arr.count > 0 {
                    amountStr = arr[0]
                }
            }
            if operatorToCheck == record.operatorName && amountStr == amount && number == record.backendNumber {
                guard let minutesSpend = minutesBetweenDates(startDate: record.time, endDate: Date.init(timeIntervalSinceNow: 0)) else { return true }
                return (minutesSpend > 2) ? true : false
            }
            return true
            //}
        } catch {
            println_debug(error)
        }
        return true
    }
    
    
    class func deleteAllRecordsFromDatabase(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void)  {
        appDel.persistentContainer.performBackgroundTask { (context) in
            let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Bank")
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
            
            do {
                try context.execute(deleteRequest)
                try context.save()
            } catch {
                print ("There was an error")
            }
            let deleteFetchDiv = NSFetchRequest<NSFetchRequestResult>(entityName: "BankByDivision")
            let deleteRequestDiv = NSBatchDeleteRequest(fetchRequest: deleteFetchDiv)
            
            do {
                try context.execute(deleteRequestDiv)
                try context.save()
            } catch {
                print ("There was an error")
            }
            let deleteFetchTown = NSFetchRequest<NSFetchRequestResult>(entityName: "BankByTownship")
            let deleteRequestTown = NSBatchDeleteRequest(fetchRequest: deleteFetchTown)
            
            do {
                try context.execute(deleteRequestTown)
                try context.save()
            } catch {
                print ("There was an error")
            }
            let deleteFetchBranch = NSFetchRequest<NSFetchRequestResult>(entityName: "BankBranchAddress")
            let deleteRequestBranch = NSBatchDeleteRequest(fetchRequest: deleteFetchBranch)
            
            do {
                try context.execute(deleteRequestBranch)
                try context.save()
            } catch {
                print ("There was an error")
            }
            completionHandler(true)
        }
    }
    
    class func saveDivisionList(bankDivision: [BankBranchByDivision], bank: Bank, context: NSManagedObjectContext, _ completionHandler: @escaping (_ accessGranted: Bool) -> Void)  {
        let entity = NSEntityDescription.entity(forEntityName: "BankByDivision", in: context)
        for singleDivision in bankDivision {
            if let wrapEntity = entity {
                if let bankDivObject = NSManagedObject(entity: wrapEntity, insertInto: context) as? BankByDivision {
                    bankDivObject.divisionCode = singleDivision.divisionCode
                    bankDivObject.divisionBName = singleDivision.divisionNameMY
                    bankDivObject.divisionEName = singleDivision.divisionNameEN
                    bankDivObject.divisionUName = singleDivision.divisionNameUN
                    bankDivObject.branchCount = singleDivision.branchCount
                    bankDivObject.bankId = bank.bankId
                    self.saveTownshipList(bankTownShip: singleDivision.branchListByTownship, division: bankDivObject, bank: bank, context: context) { status  in
                        if status {
                            do {
                                try(context.save())
                            } catch let err {
                                print(err)
                            }
                        }
                    }
                }
                
            }
        }
        completionHandler(true)        
    }
    
    class func saveTownshipList(bankTownShip: [BankBranchByTownship], division: BankByDivision, bank: Bank, context: NSManagedObjectContext, _ completionHandler: @escaping (_ accessGranted: Bool) -> Void)  {
        let entity = NSEntityDescription.entity(forEntityName: "BankByTownship", in: context)
        for singleTownship in bankTownShip {
            if let wrapEntity = entity {
                if let bankTownObject = NSManagedObject(entity: wrapEntity, insertInto: context) as? BankByTownship {
                    bankTownObject.townshipCode = singleTownship.townshipCode
                    bankTownObject.townshipBName = singleTownship.townshipNameMY
                    bankTownObject.townshipEName = singleTownship.townshipNameEN
                    bankTownObject.townshipUName = singleTownship.townshipNameUN
                    bankTownObject.branchCount = singleTownship.branchCount
                    bankTownObject.bankId = division.bankId
                    bankTownObject.divisionCode = division.divisionCode
                    self.saveBankBranchList(bankBranches: singleTownship.branchList, township: bankTownObject, bank: bank, division: division, context: context) { status  in
                        if status {
                            do {
                                try(context.save())
                            } catch let err {
                                print(err)
                            }
                        }
                    }
                }
                
            }
        }
        completionHandler(true)
    }
    
    class func saveBankBranchList(bankBranches: [BankBranch], township: BankByTownship, bank: Bank, division: BankByDivision, context: NSManagedObjectContext, _ completionHandler: @escaping (_ accessGranted: Bool) -> Void)  {
        let entity = NSEntityDescription.entity(forEntityName: "BankBranchAddress", in: context)
        for branch in bankBranches {
            if let wrapEntity = entity {
                if let branchObject = NSManagedObject(entity: wrapEntity, insertInto: context) as? BankBranchAddress {
                    branchObject.branchEAddress = branch.branchAddressEN
                    branchObject.branchBAddress = branch.branchAddressMY
                    branchObject.branchUAddress = branch.branchNameUN
                    branchObject.branchBName = branch.branchNameMY
                    branchObject.branchEName = branch.branchNameEN
                    branchObject.branchUName = branch.branchNameUN
                    branchObject.faxnumber = branch.branchFaxNumber
                    branchObject.phonenumber = branch.branchPhoneNumber
                    branchObject.branchId = Int16(branch.branchId)
                    branchObject.bankId = township.bankId
                    branchObject.townshipCode = township.townshipCode
                    branchObject.divisionCode = township.divisionCode
                    do {
                        try(context.save())
                    } catch let err {
                        print(err)
                    }
                }
            }
        }
        completionHandler(true)
    }
    
    
    class func insertAllRecordsToBankDatabase(model: [BankModel]) {
        println_debug("insertAllRecordsToBankDatabase")
        appDel.persistentContainer.performBackgroundTask { (context) in
            let entity = NSEntityDescription.entity(forEntityName: "Bank", in: context)
            for singleBank in model {
                if let wrapEntity = entity {
                    if let bankObject = NSManagedObject(entity: wrapEntity, insertInto: context) as? Bank {
                        bankObject.bankId = Int16(singleBank.bankId)
                        bankObject.bankName = singleBank.bankNameEN
                        bankObject.isActive = singleBank.isActiveBank
                        bankObject.accountFormat = singleBank.accountFormat
                        bankObject.validationRule = singleBank.accountValidationRule
                        bankObject.burmeseName = singleBank.bankNameMY
                        bankObject.unicodeName = singleBank.bankNameUN
                        bankObject.multiAccountFormat = singleBank.multiAccountFormat
                        bankObject.totalBranches = Int16(singleBank.totalBranches)
                        self.saveDivisionList(bankDivision: singleBank.branchListByDivision, bank: bankObject, context: context) { status  in
                            if status {
                                do {
                                    try(context.save())
                                } catch let err {
                                    print(err)
                                }
                            }
                        }
                    }
                }
                
            }
            println_debug("Bank inserted")
        }
    }
    
    class func insertAllRecordsToBankRoutingDatabase(model: [BankRoutingModel]) {
        println_debug("Called to insert")
        appDel.persistentContainer.performBackgroundTask { (context) in
            let entity = NSEntityDescription.entity(forEntityName: "BankRoutingStatus", in: context)
            for singleBank in model {
                if let wrapEntity = entity {
                    if let bankObject = NSManagedObject(entity: wrapEntity, insertInto: context) as? BankRoutingStatus {
                        bankObject.bankId = Int16(singleBank.bankId)
                        bankObject.bankName = singleBank.bankName
                        bankObject.projectId = singleBank.projectId
                        bankObject.routingStatus = singleBank.rountingType
                        do {
                            try(context.save())
                        } catch let err {
                            print(err)
                        }
                    }
                }
            }
            println_debug("Bank routing inserted")
        }
    }
    
    class func getRoutingBankStatus(bankId: Int16?, _ completionHandler: @escaping (_ accessGranted: Bool, _ bankList: [BankRoutingStatus]?) -> Void) {
        if  let id = bankId {
            let managedContext        = appDelegate.persistentContainer.viewContext
            let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "BankRoutingStatus")
            do {
                let predicate        = NSPredicate(format: "bankId  == %i", id)
                fetchReq.predicate    = predicate
                let fetchedObjects    = try managedContext.fetch(fetchReq) as! [BankRoutingStatus]
                if fetchedObjects.count > 0 {
                    completionHandler(true, fetchedObjects)
                } else {
                    completionHandler(true, nil)
                }
            } catch {
                print ("fetch task failed", error)
                completionHandler(true, nil)
            }
        }
    }
    
    class func getAllBanksFromDB(_ completionHandler: @escaping (_ accessGranted: Bool, _ bankList: [Bank]?) -> Void) {
        println_debug("getAllBanksFromDB")
        let managedContext        = appDelegate.persistentContainer.viewContext
        let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "Bank")
        do {
            let sortDescriptor = NSSortDescriptor(key: "bankName", ascending: true)
            fetchReq.sortDescriptors = [sortDescriptor]
            let fetchedObjects    = try managedContext.fetch(fetchReq) as! [Bank]
            if fetchedObjects.count > 0 {
                completionHandler(true, fetchedObjects)
            } else {
                completionHandler(true, nil)
            }
        } catch {
            print ("fetch task failed", error)
            completionHandler(true, nil)
        }
    }
    
    class func getAllDivisionsFromDB(bankId: Int16?, _ completionHandler: @escaping (_ accessGranted: Bool, _ bankList: [BankByDivision]?) -> Void) {
        if  let id = bankId {
            let managedContext        = appDelegate.persistentContainer.viewContext
            let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "BankByDivision")
            do {
                let predicate1        = NSPredicate(format: "bankId  == %i", id)
                fetchReq.predicate    = predicate1
                let fetchedObjects    = try managedContext.fetch(fetchReq) as! [BankByDivision]
                if fetchedObjects.count > 0 {
                    completionHandler(true, fetchedObjects)
                } else {
                    completionHandler(true, nil)
                }
            } catch {
                print ("fetch task failed", error)
                completionHandler(true, nil)
            }
        }
    }
    
    class func getAllTownshipFromDB(divisionCode: String?, bankId:Int16?,  _ completionHandler: @escaping (_ accessGranted: Bool, _ bankList: [BankByTownship]?) -> Void) {
        if let division = divisionCode, let id = bankId {
            let managedContext        = appDelegate.persistentContainer.viewContext
            let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "BankByTownship")
            do {
                let predicate1        = NSPredicate(format: "divisionCode == %@", division)
                let predicate2        = NSPredicate(format: "bankId  == %i", id)
                let andPredicate      = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2])
                fetchReq.predicate    = andPredicate
                let fetchedObjects    = try managedContext.fetch(fetchReq) as! [BankByTownship]
                if fetchedObjects.count > 0 {
                    completionHandler(true, fetchedObjects)
                } else {
                    completionHandler(true, nil)
                }
            } catch {
                print ("fetch task failed", error)
                completionHandler(true, nil)
            }
        }
    }
    
    class func getAllBranchesFromDB(townshipCode: String?, divisionCode: String?, bankId:Int16?, _ completionHandler: @escaping (_ accessGranted: Bool, _ bankList: [BankBranchAddress]?) -> Void) {
        if let township = townshipCode, let division = divisionCode, let id = bankId {
            let managedContext        = appDelegate.persistentContainer.viewContext
            let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "BankBranchAddress")
            do {
                let predicate        = NSPredicate(format: "divisionCode == %@", division)
                let predicate1        = NSPredicate(format: "townshipCode == %@", township)
                let predicate2        = NSPredicate(format: "bankId  == %i", id)
                let andPredicate      = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate, predicate1, predicate2])
                fetchReq.predicate    = andPredicate
                let fetchedObjects    = try managedContext.fetch(fetchReq) as! [BankBranchAddress]
                if fetchedObjects.count > 0 {
                    completionHandler(true, fetchedObjects)
                } else {
                    completionHandler(true, nil)
                }
            } catch {
                print ("fetch task failed", error)
                completionHandler(true, nil)
            }
        }
    }
    
    class func stringToDate(date: String) -> Date? {
        if let dateInDF = date.dateValue(dateFormatIs: "MM/dd/yyyy hh:mm:ss a") {
            println_debug("****MM/dd/yyyy hh:mm:ss a*********\(dateInDF))")
           return dateInDF
        }
        if let data = date.dateValue(dateFormatIs: "yyyy-MM-dd HH:mm:ss:SSS") {
            println_debug("olddate ****yyyy-MM-dd HH:mm:ss.SSS*********\(data))")
            /*
            let dateFormatter = DateFormatter()
            dateFormatter.calendar = Calendar(identifier: .gregorian)
            dateFormatter.setLocale()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss:SSS"
            let dateStr = dateFormatter.string(from: data)
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
            if let newDate = dateStr.dateValue(dateFormatIs: "MM/dd/yyyy hh:mm:ss a") {
                println_debug("newdate ****MM/dd/yyyy hh:mm:ss a*********\(newDate))")
                return newDate
            } */
                  return data
               }
        if let data = date.dateValue(dateFormatIs: "yyyy-MM-dd HH:mm:ss.SSS") {
                   println_debug("****yyyy-MM-dd HH:mm:ss.SSS*********\(data))")

                         return data
                      }
        return nil
    }
    
    class func insertRecordsToInboxMessages(dataModel: [AllNotifyData], _ completion: @escaping (_ status: Bool) -> Void) {
        appDel.persistentContainer.performBackgroundTask { (context) in
            let entity = NSEntityDescription.entity(forEntityName: "InboxMessages", in: context)
            for singleModel in dataModel {
                if let wrapEntity = entity {
                    if let inboxObject = NSManagedObject(entity: wrapEntity, insertInto: context) as? InboxMessages {
                        println_debug(inboxObject)
                        inboxObject.id = singleModel.id ?? ""
                        inboxObject.message = singleModel.message ?? ""
                        println_debug(inboxObject.message)
                        inboxObject.mobileNumber = singleModel.mobileNo ?? ""
                        if let date = stringToDate(date: singleModel.createdDate ?? "") {
                            inboxObject.createdDate = date
                        }
                        do {
                            try(context.save())
                        } catch let err {
                            print(err)
                        }
                    }
                }
            }
            completion(true)
            println_debug("inbox inserted")
        }
    }
    
    class func getInboxMessages(_ completionHandler: @escaping (_ accessGranted: Bool, _ bankList: [InboxMessages]?) -> Void) {
        let managedContext        = appDelegate.persistentContainer.viewContext
        let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "InboxMessages")
        do {
            let sortDescriptor = NSSortDescriptor(key: "createdDate", ascending: false)
            fetchReq.sortDescriptors = [sortDescriptor]
            let fetchedObjects    = try managedContext.fetch(fetchReq) as! [InboxMessages]
            if fetchedObjects.count > 0 {
                completionHandler(true, fetchedObjects)
            } else {
                completionHandler(true, nil)
            }
        } catch {
            print ("fetch task failed", error)
            completionHandler(true, nil)
        }
    }
    
    class func deleteAllInboxMessages(id: String?) {
        appDel.persistentContainer.performBackgroundTask { (context) in
            let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "InboxMessages")
            
            if let idToDel = id, idToDel != "" {
                let predicate        = NSPredicate(format: "id == %@", idToDel)
                deleteFetch.predicate    = predicate
            }
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
            print("deleteRequest====\(deleteRequest)")

            do {
                try context.execute(deleteRequest)
                try context.save()

            } catch {
                print ("There was an error")
            }
        }
    }
    
    //MARK:- Advertisement Database
    class func insertAllRecordsToAdvertisementDatabase(imageUrl: String, imageData: Data) {
        appDel.persistentContainer.performBackgroundTask { (context) in
            let entity = NSEntityDescription.entity(forEntityName: "AdvertisementEntity", in: context)
            if let wrapEntity = entity {
                if let advObject = NSManagedObject(entity: wrapEntity, insertInto: context) as? AdvertisementEntity {
                    advObject.imageUrl = imageUrl
                    advObject.imageData = imageData
                    do {
                        try(context.save())
                    } catch let err {
                        print(err)
                    }
                }
            }
            println_debug("Advertisement inserted")
        }
    }
    
    class func getAdImageFromDB(imageUrl: String,  _ completionHandler: @escaping (_ accessGranted: Bool, _ image: Data?) -> Void) {
        let managedContext        = appDelegate.persistentContainer.viewContext
        let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "AdvertisementEntity")
        do {
            let predicate1        = NSPredicate(format: "imageUrl == %@", imageUrl)
            fetchReq.predicate    = predicate1
            let fetchedObjects    = try managedContext.fetch(fetchReq) as! [AdvertisementEntity]
            if fetchedObjects.count > 0 {
                completionHandler(true, fetchedObjects.first?.imageData)
            } else {
                completionHandler(true, nil)
            }
        } catch {
            print ("fetch task failed", error)
            completionHandler(true, nil)
        }
    }
    
    class func getAllAdImageFromDB(_ completionHandler: @escaping (_ accessGranted: Bool, _ adImages: [AdvertisementEntity]?) -> Void) {
        let managedContext        = appDelegate.persistentContainer.viewContext
        let fetchReq              = NSFetchRequest<NSManagedObject>(entityName: "AdvertisementEntity")
        do {
            let fetchedObjects    = try managedContext.fetch(fetchReq) as! [AdvertisementEntity]
            if fetchedObjects.count > 0 {
                completionHandler(true, fetchedObjects)
            } else {
                completionHandler(true, nil)
            }
        } catch {
            print ("fetch task failed", error)
            completionHandler(true, nil)
        }
    }
    
    class func deleteAllAdvertisement() {
        appDel.persistentContainer.performBackgroundTask { (context) in
            let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "AdvertisementEntity")
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
            do {
                try context.execute(deleteRequest)
                try context.save()
            } catch {
                print ("There was an error")
            }
        }
    }
}

struct PaymentTransactions {
    var number : String?
    var type   : String?
    var name   : String?
    var amount : String?
    var operatorName : String?
    var bPoints : String?
    var transID: String?
    var time : Date?
    var backendNumber: String?
    
    init(_ number: String, type: String, name: String, amount: String, transID: String, bonus: String, backendNumber: String? = "") {
        self.number       = number
        self.type         = type
        self.name         = name
        self.amount       = amount
        self.transID      = transID
        self.bPoints      = bonus
        self.backendNumber = backendNumber
        let operatorData = PTManagerClass.decodeMobileNumber(phoneNumber: number)
        if operatorData.country.dialCode == "+95" {
            let opName = PayToValidations().getNumberRangeValidation(operatorData.number)
            self.operatorName = opName.operator
        } else {
            self.operatorName = "Unknown"
        }
        self.time = Date.init(timeIntervalSinceNow: TimeInterval.init(0))
    }
}

// MARK: - TopupRecentContacts
class TopupRecentContacts {
    class func getContext() -> NSManagedObjectContext {
        return appDel.persistentContainer.viewContext
    }
    
    // MARK: Store PaymentTransactions to TopupContactSuggestion
    class func storeTransactions(model: PaymentTransactions) {
        let context = getContext()
        let entity = NSEntityDescription.entity(forEntityName: "TopupContactSuggestion", in: context)
        if let wrapEntity = entity {
            if let object = NSManagedObject(entity: wrapEntity, insertInto: context) as? TopupContactSuggestion {
                object.number       = model.number
                object.operatorName = model.operatorName
                object.amount       = model.amount
                object.date         = model.time
            }
            do {
                try context.save()
            } catch let error as NSError {
                println_debug(error)
            }
        }
    }
    
    //var ready = convertedArray.sorted(by: { $0.compare($1) == .orderedDescending })
    // MARK: Getting recent TopupContactSuggestion list
    class func getRecents() -> [TopupContactSuggestion] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TopupContactSuggestion")
        let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        let context = TopupRecentContacts.getContext()
        var result  = [TopupContactSuggestion]()
        do {
            let records = try context.fetch(fetchRequest)
            if let records = records as? [TopupContactSuggestion] {
                result = records
            }
        } catch {
            println_debug("Unable to fetch managed objects for favorite entity.")
        }
        
        return Array(result.prefix(10))
        
//        let final = result.sorted { (first, second) -> Bool in
//            if first.date == nil {
//                return false
//            }
//            if second.date == nil {
//                return false
//            }
//            if first.date! < second.date! {
//                return true
//            }
//            return false
//        }
//        return Array(final.prefix(10))
    }
}
