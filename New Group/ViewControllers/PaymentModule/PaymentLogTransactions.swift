//
//  PaymentLogTransactions.swift
//  OK
//
//  Created by Ashish on 7/26/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit


/// Log for Payment failure to server
class PaymentLogTransactions {
    static let manager = PaymentLogTransactions()
    func logPaymentApiFailureCase(_ rechargeNumber: String, errCode: String) {
        let baseUrl = "https://www.okdollar.co/RestService.svc/LogTransactionFailures?MobileNumber=%@&Destination=%@&ErrCode=%@&Latitude=%@&Longitude=%@&CellID=%@"
        let stringUrl = String.init(format: baseUrl, UserModel.shared.mobileNo, rechargeNumber,errCode, geoLocManager.currentLatitude ?? "0.0", geoLocManager.currentLongitude ?? "0.0", "")
        guard let stringQueryFormatted = stringUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        guard let url = URL.init(string: stringQueryFormatted) else { return }
        
        TopupWeb.genericClass(url: url, param: Dictionary<String,Any>() as AnyObject, httpMethod: "GET") { (response, success) in
            println_debug(response)
            println_debug(success)
        }
    }
}
