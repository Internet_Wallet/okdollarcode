//
//  InternationalMainExtensionVC.swift
//  OK
//
//  Created by OK$ on 03/06/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

extension InternationalMainVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
       
        textView.resignFirstResponder()
        
        if textView.text == "Select Service" {
            
            textView.textColor = UIColor.lightGray
            
            
        }
        else{
            if textView.text == "" {
                textView.text = "Select Service"
                textView.textColor = UIColor.lightGray
            }
            else{
                textView.textColor = UIColor.black
            }
        }
        
        if appDelegate.checkNetworkAvail() {
            
            if let addWithdrawView = UIStoryboard(name: "InternationalTopUp", bundle: nil).instantiateViewController(withIdentifier: "InternationalOffersVC") as? InternationalOffersVC {
                addWithdrawView.modalPresentationStyle = .fullScreen
                addWithdrawView.mobilenumber = self.mobileNumber.text ?? ""
                addWithdrawView.strCountryID = selectTalktimeCountryId
                addWithdrawView.strOperatorID = strOperatorID
                addWithdrawView.delegate = self
                self.present(addWithdrawView, animated: true, completion: nil)
            }
        } else {
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            alertViewObj.showAlert(controller: UIViewController.init())
        }
    }
}

extension InternationalMainVC: UITextFieldDelegate {
    
     //MARK:- UITextFieldDelegate functions
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
     //   self.view.endEditing(true)
        if textField == self.selectOperator {
            if internationalCountryListModel?.dataPlanCountryId == "0" {
                self.showErrorAlert(errMessage: "Data Plan Not Available For This Country!")
            } else {
                if appDelegate.checkNetworkAvail() {
                if let addWithdrawView = UIStoryboard(name: "InternationalTopUp", bundle: nil).instantiateViewController(withIdentifier: "InternationalOperatorVC") as? InternationalOperatorVC {
                    addWithdrawView.modalPresentationStyle = .fullScreen
                    addWithdrawView.strCountryID = selectCountryId
                    addWithdrawView.delegate = self
                    self.present(addWithdrawView, animated: true, completion: nil)
                }
                }else {
                    alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
                    alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                    }
                    alertViewObj.showAlert(controller: UIViewController.init())
                }
            }
            return false
        }
        
        if textField == self.mobileNumber {
          self.confirmfieldhide = ""
            if textField == self.mobileNumber {
                if let text = self.mobileNumber.text, text.count > 0 {
                   // textField.text = "09"
                    self.mobileNumberRightView.hideClearButton(hide: false)
                }
                else{
                   
                    self.mobileNumberRightView.hideClearButton(hide: true)
                }
            }
        }
        if textField == self.confirmMobileNumber {
            if textField == self.confirmMobileNumber {
                if let text = self.confirmMobileNumber.text, text.count > 0 {
                   // textField.text = "09"
                    self.confMobile?.hideClearButton(hide: false)
                }
                else{
                    self.confMobile?.hideClearButton(hide: true)
                }
            }
        }
        if textField == self.selectCountryTF{
            //self.mobileNumber.becomeFirstResponder()
          //  self.clearActionType()
            var homeVC: InternationalCountryVC?
            homeVC = UIStoryboard(name: "InternationalTopUp", bundle: nil).instantiateViewController(withIdentifier: "InternationalCountryVC") as? InternationalCountryVC
            if let homeVC = homeVC {
                homeVC.selectedCountrydelegate = self
                homeVC.arrCountryList = arrCountryList.sorted(by: { $0.countryName ?? "" < $1.countryName ?? ""})
                self.present(homeVC, animated: true, completion: nil)
            }
            return false
        }
       
        return true
    }
       
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
         if textField == self.mobileNumber {
             self.mobileNumberRightView.hideClearButton(hide: true)
         } else if textField == self.confirmMobileNumber {
            self.confMobile?.hideClearButton(hide: true)
         }
         return true
     }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let topupCount = InternationalTopupManager().validations(countryCode: selectCountryCode)
        self.phoneuiTopConstraint.constant = 2
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            if textField == self.mobileNumber {
                usernameView.isHidden = true
                userviewTopConstraint.constant = 0
                userviewHeightConstraint.constant = 0
                self.confirmcountrycodeLabel.isHidden = false
                self.confmobileLeftImage.image = UIImage(named:"topup_number")
                self.confirmMobileNumber.text = ""
                self.confirmcountrycodeLabel.text = self.mobilecountrycodeLabel.text
               // self.confirmMobileNumber.isHidden = true
               // self.confirmfiledHeightConstraint.constant = 0
                self.confirmuiView.isHidden = true
                self.phoneuiheightConstraint.constant = 0
             //   self.phoneuiView.layer.cornerRadius = 8.0
             //   self.confirmmobileLabel.text = "Confirm Mobile Number".localized
                self.selectOperator.isHidden = true
                self.lblOperatorText.isHidden = true
                self.selectAmountTextView.isHidden = true
                self.amountView.isHidden = true
                self.selectAmountTextView.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: "Select Service".localized, textTwo: "",textThree: "", textfourth: "", sizeOne: 17, sizeTwo: 11, color: .lightGray)
              //  self.selectAmountTextView.text = "Select Service"
                self.imgViewService.image = UIImage(named: "service_overseas")
                self.selectOperator.text = ""
                self.selectserivceImage.isHidden = true
                self.operatoruiView.isHidden = true
            }
            if textField == self.confirmMobileNumber {
                self.selectOperator.isHidden = true
                self.selectAmountTextView.isHidden = true
                self.lblOperatorText.isHidden = true
                self.amountView.isHidden = true
                self.selectOperator.text = ""
                self.selectAmountTextView.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: "Select Service".localized, textTwo: "",textThree: "", textfourth: "", sizeOne: 17, sizeTwo: 11, color: .lightGray)
               // self.selectAmountTextView.text = "Select Service"
                self.imgViewService.image = UIImage(named: "service_overseas")
                self.selectserivceImage.isHidden = true
                self.operatoruiView.isHidden = true
            }
           
        }
        
        if textField == self.mobileNumber {
            let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: mobileNumberAcceptableCharacters).inverted
            let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
            self.confirmMobileNumber.name = false
            self.confMobile?.hideClearButton(hide: true)
            if string != filteredSet { return false }
            if text.count > 0 {
                self.mobileNumberRightView.hideClearButton(hide: false)
            } else {
                self.mobileNumberRightView.hideClearButton(hide: true)
            }
            
            if text.hasPrefix("0000") {
                usernameView.isHidden = true
                userviewTopConstraint.constant = 0
                userviewHeightConstraint.constant = 0
                self.mobileNumber.text = ""
                self.confirmMobileNumber.text = ""
                self.confirmcountrycodeLabel.text = self.mobilecountrycodeLabel.text
                self.confirmcountrycodeLabel.isHidden = false
                self.confmobileLeftImage.image = UIImage(named:"topup_number")
               // self.confirmMobileNumber.isHidden = true
               // self.confirmfiledHeightConstraint.constant = 0
                self.confirmuiView.isHidden = true
                self.phoneuiheightConstraint.constant = 0
               // self.phoneuiView.layer.cornerRadius = 8.0
              //  self.confirmmobileLabel.text = "Confirm Mobile Number".localized
                self.selectOperator.isHidden = true
                self.lblOperatorText.isHidden = true
                self.selectAmountTextView.isHidden = true
                self.amountView.isHidden = true
                self.mmkView.isHidden = true
                self.selectAmountTextView.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: "Select Service".localized, textTwo: "",textThree: "", textfourth: "", sizeOne: 17, sizeTwo: 11, color: .lightGray)
              //  self.selectAmountTextView.text = "Select Service"
                self.imgViewService.image = UIImage(named: "service_overseas")
                self.selectOperator.text = ""
                self.selectserivceImage.isHidden = true
                self.operatoruiView.isHidden = true
                self.mobileNumberRightView.hideClearButton(hide: true)
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                return false
            }
             if textField == self.mobileNumber {
                if self.mobileNumber.text?.count == topupCount.max {
                    if !(isBackSpace == -92) {
                        return false
                    }
                }
            }
           // self.confirmMobileNumber.isHidden = (text.count >= topupCount.min) ? false : true
            self.confirmuiView.isHidden = (text.count >= topupCount.min) ? false : true
            if let previousNumber = self.mobileNumber.text {
                if text.count <= previousNumber.count {
                    if previousNumber != text {
                    usernameView.isHidden = true
                    userviewTopConstraint.constant = 0
                    userviewHeightConstraint.constant = 0
                    self.confirmMobileNumber.text = ""
                    self.confirmcountrycodeLabel.text = self.mobilecountrycodeLabel.text
                    self.confirmcountrycodeLabel.isHidden = false
                    self.confmobileLeftImage.image = UIImage(named:"topup_number")
                   // self.confirmMobileNumber.isHidden = (text.count >= topupCount.min) ? false : true
                    self.confirmuiView.isHidden = (text.count >= topupCount.min) ? false : true
                    self.selectOperator.isHidden = true
                    self.lblOperatorText.isHidden = true
                    self.mmkView.isHidden = true
                    self.selectAmountTextView.isHidden = true
                    self.amountView.isHidden = true
                        self.selectAmountTextView.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: "Select Service".localized, textTwo: "",textThree: "", textfourth: "", sizeOne: 17, sizeTwo: 11, color: .lightGray)
                   // self.selectAmountTextView.text = "Select Service"
                    self.imgViewService.image = UIImage(named: "service_overseas")
                    self.selectOperator.text = ""
                    self.selectserivceImage.isHidden = true
                    self.operatoruiView.isHidden = true
                    } else{
                        return false
                    }
                }
            }
            if text.count >= topupCount.max {
                if text.count <= topupCount.max {
                    self.mobileNumber.text = text
                    if !self.confirmuiView.isHidden {//self.confirmMobileNumber.isHidden {
                       // self.confirmMobileNumber.isHidden = false
                       // self.confirmfiledHeightConstraint.constant = 50
                        //self.phoneuiTopConstraint.constant = 20
                        self.confirmuiView.isHidden = false
                        self.phoneuiheightConstraint.constant = 60
//                        self.confirmuiView.layer.cornerRadius = 8.0
//                        self.phoneuiView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
//                        self.confirmuiView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
                    }
                }
                self.mobileNumber.resignFirstResponder()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.confirmMobileNumber.becomeFirstResponder()
                }
                self.confirmMobileNumber.layoutIfNeeded()
                self.confirmMobileNumberLeftView.wrapinternationalCountryViewData(img: "", str: selectCountryCode)
//                self.confirmMobileNumber.leftView = nil
//                self.confirmMobileNumber.leftView =  confirmMobileNumberLeftView
//                self.confirmMobileNumber.leftViewMode = .always
                self.confirmMobileNumber.isUserInteractionEnabled = true
                return false
            }
            if let cMNumber = confirmMobileNumber.text {
                if text.count > cMNumber.count {
                   // self.confirmfiledHeightConstraint.constant = 50
                    usernameView.isHidden = true
                    userviewTopConstraint.constant = 0
                    userviewHeightConstraint.constant = 0
                    self.phoneuiheightConstraint.constant = 60
                    self.confirmMobileNumber.text = ""
                    self.confirmcountrycodeLabel.text = self.mobilecountrycodeLabel.text
                    self.confirmcountrycodeLabel.isHidden = false
                    self.confmobileLeftImage.image = UIImage(named:"topup_number")
                    self.selectOperator.isHidden = true
                    self.lblOperatorText.isHidden = true
                    self.mmkView.isHidden = true
                    self.selectAmountTextView.isHidden = true
                    self.amountView.isHidden = true
                    self.selectAmountTextView.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: "Select Service".localized, textTwo: "",textThree: "", textfourth: "", sizeOne: 17, sizeTwo: 11, color: .lightGray)
                   // self.selectAmountTextView.text = "Select Service"
                    self.imgViewService.image = UIImage(named: "service_overseas")
                    self.selectOperator.text = ""
                    self.selectserivceImage.isHidden = true
                    self.operatoruiView.isHidden = true
                }
            }
            InternationalTopupManager.receiverName = "Unknown"
            self.strBeneficiaryName = "Unknown"
            self.fromFavorite = "Unknown"
        }
        
        if textField == self.confirmMobileNumber {
            switch self.mobileNumber.text!.hasPrefix(text) {
            case true:
                if self.mobileNumber.text!.count == text.count {
                    self.confirmMobileNumber.text = self.mobileNumber.text
                    self.confirmMobileNumber.resignFirstResponder()
                    self.selectOperator.isHidden = false
                    self.lblOperatorText.isHidden = false
                    self.selectserivceImage.isHidden = false
                    self.operatoruiView.isHidden = false
                    return false
                } else {
                    self.selectOperator.isHidden = true
                    self.lblOperatorText.isHidden = true
                    self.selectserivceImage.isHidden = true
                    self.operatoruiView.isHidden = true
                }
                if text.count > 0 {
                    self.confMobile?.hideClearButton(hide: false)
                } else {
                    self.confMobile?.hideClearButton(hide: true)
                }
                return true
            case false:
                return false
            }
        }
        
        return true
    }
}

extension InternationalMainVC: BioMetricLoginDelegate {
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if screen == InternationalConstantURL.msgs.screenConfirm {
            DispatchQueue.main.async {
                if isSuccessful {
                if let objView = UIStoryboard(name: "InternationalTopUp", bundle: nil).instantiateViewController(withIdentifier: "InternationalConfirmationViewController") as? InternationalConfirmationViewController {
                    objView.dictRequest = self.generatePaymentForDatapackTopUp()
                    objView.countryisdcode = self.selectCountryCode
                    objView.countryName = self.selectCountryName
                    objView.countryFlag = self.strCountryFlag ?? ""
                    objView.strTopUpPlan = self.selectedPlan
                    objView.fromFavorite = self.fromFavorite
                    objView.displayName = self.usernameLabel.text ?? ""
                    objView.DestinationCurrency = self.productcurrency ?? ""
                    objView.strBeneficiaryName = self.strBeneficiaryName ?? "Unknown"
                    objView.modalPresentationStyle = .fullScreen
                    self.present(objView, animated: true, completion: nil)
                }
                }
            }
        }
    }
}

//MARK: - ContactPickerDelegate
extension InternationalMainVC: ContactPickerDelegate {
    //MARK:- Contact Delegates
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
       // self.operatorTableView.isHidden = true
        self.decodeContact(contact: contact)
    }
    
    
    // MARK:- Extra Methods: For Incoming protocols from different module
    
    func decodeContact(contact: ContactPicker) {
        self.btnPayBack.setTitle("BACK".localized, for: .normal)

        func clearFields() {
           // self.confirmmobileLabel.text = "Confirm Mobile Number".localized
            self.confirmMobileNumber.text     = ""
            self.mobileNumber.text = ""
           // self.confirmMobileNumber.isHidden = true
          //  self.confirmfiledHeightConstraint.constant = 0
            self.confirmuiView.isHidden = true
            self.phoneuiheightConstraint.constant = 0
        //    self.phoneuiView.layer.cornerRadius = 8.0
            self.selectOperator.isHidden = true
            self.selectAmountTextView.isHidden   = true
            self.amountView.isHidden = true
            self.selectOperator.text = ""
            self.selectAmountTextView.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: "Select Service".localized, textTwo: "",textThree: "", textfourth: "", sizeOne: 17, sizeTwo: 11, color: .lightGray)
           // self.selectAmountTextView.text = "Select Service"
            self.imgViewService.image = UIImage(named: "service_overseas")
            self.lblOperatorText.isHidden = true
            self.mmkView.isHidden = true
            self.selectserivceImage.isHidden = true
            self.operatoruiView.isHidden = true
        }
        func invalidNumberSelectionAlert() {
            alertViewObj.wrapAlert(title: nil, body: "You have selected invalid number".localized,
                                   img: #imageLiteral(resourceName: "dashboard_overseas_recharge"))
            alertViewObj.addAction(title: "OK", style: .cancel, action: {})
            alertViewObj.showAlert()
        }
        func isMobNumberWithinRange(minValue: Int, maxValue: Int, phoneNumber: String) -> Bool {
            if !(phoneNumber.count >= minValue && phoneNumber.count <= maxValue) {
                invalidNumberSelectionAlert()
                return false
            }
            return true
        }
        func localNumberSelectionAlert() {
            alertViewObj.wrapAlert(title: nil, body: "You Selected Myanmar local Number".localized, img: #imageLiteral(resourceName: "dashboard_overseas_recharge"))
            alertViewObj.addAction(title: "OK", style: .cancel, action: {})
            alertViewObj.showAlert()
        }
        func incorrectMobNumAlert() {
            alertViewObj.wrapAlert(title: nil, body: "Incorrect mobile number. Please try again".localized,
                                   img: UIImage(named : "AppIcon"))
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {})
            alertViewObj.showAlert()
        }
        let phoneCount = contact.phoneNumbers.count
        
        var phoneNumber = ""
        if phoneCount > 0 {
            phoneNumber = contact.phoneNumbers[0].phoneNumber
        } else {
            return
        }
        
        phoneNumber = phoneNumber.deletingZeroPrefixes()
        
        if phoneNumber.count > 0 {
            phoneNumber = phoneNumber.replacingOccurrences(of: " ", with: "")
            phoneNumber = phoneNumber.replacingOccurrences(of: "+", with: "")
            phoneNumber = phoneNumber.trimmingCharacters(in: .whitespaces)
        }
        if phoneNumber.contains(find: " ") {
            let phArray = phoneNumber.components(separatedBy: " ")
            if phArray.count > 1 {
                phoneNumber = "+" + phArray[0] + phArray[1]
            }
        } else {
            phoneNumber = "+" + phoneNumber
        }
     
       // selectCountryCode = String(phoneNumber.prefix(3))
       
        let characterset = CharacterSet(charactersIn: "+0123456789 ")
        if phoneNumber.rangeOfCharacter(from: characterset.inverted) != nil || (phoneNumber == ""){
            invalidNumberSelectionAlert()
        }
        else if !phoneNumber.hasPrefix("+"){
           // localNumberSelectionAlert()
          //  clearFields()
            incorrectMobNumAlert()
        }
        else {
            var country = self.identifyCountry(withPhoneNumber: phoneNumber)
            if country.0 == "" ||  country.1 == "" {
                country = ("+95","myanmar")
            }
            if country.0 == "+95" {
                localNumberSelectionAlert()
            } else {
                InternationalTopupManager.selectedCountry = (country.0, country.1)
                var countryObject = country.0//selectCountryCode
                var country = country.0//selectCountryCode
                self.mobilecountrycodeLabel.text = "(" + selectCountryCode + ")"

                let validNumberWithCountry =  phoneNumber.hasPrefix(countryObject)
                let arrTemp = arrCountryList
                for element in arrTemp {
                    if phoneNumber.contains(find: element.countryCode2 ?? "") {
                        if let imageStr = element.countryFlag, let imageUrl = URL(string: imageStr) {
                            self.flagimage.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: ""))
                            self.selectCountryTF.text =  element.countryName
                            
                            self.selectCountryCode = element.countryCode2 ?? "+66"
                            self.strCountryFlag = element.countryFlag ?? ""
                            self.selectCountryName = element.countryName ?? ""
                            self.selectCountryId = element.countryId ?? ""
                            self.selectTalktimeCountryId = element.talkTimeCountryId ?? ""
                            self.mobileNumberLeftView.wrapinternationalCountryViewData(img: "", str: element.countryCode2 ?? "+66")
                            self.mobilecountrycodeLabel.text = "(" + selectCountryCode + ")"
//                            self.mobileNumber.text = ""
//                            self.mobileNumberRightView.hideClearButton(hide: false)
//                           // self?.confirmMobileNumberLeftView.wrapinternationalCountryViewData(img: "topup_number", str: "")
//
//                            self.confirmMobileNumber.isUserInteractionEnabled = false
//                            self.confMobile?.hideClearButton(hide: true)
//                            self.confirmMobileNumber.isHidden = false
//                            self.selectOperator.isHidden = false
                            
                           
                            countryObject = selectCountryCode//Country.init(name: "", code: selectCountryName, dialCode: selectCountryCode)
                            country = selectCountryCode//Country.init(name: "", code: selectCountryName, dialCode: selectCountryCode)
                            break
                        }
                    }

                }
                    self.checkAvailableCountry(country: country) {_ in
                        
                    }
                    clearFields()
                    if phoneNumber.hasPrefix(countryObject) {
                        phoneNumber = phoneNumber.replacingOccurrences(of: countryObject, with: "")
                    }
                    guard let rangeValue = getMinMaxForOverseas(countryCode: selectCountryCode) else {
                        return
                    }
                    if !isMobNumberWithinRange(minValue: rangeValue.minLength,
                                               maxValue: rangeValue.maxLength,
                                               phoneNumber: phoneNumber) {
                        clearFields()
                        incorrectMobNumAlert()
                        return
                    }
                    if phoneNumber.count <= 0 {
                        clearFields()
                        return
                    }
                
                if fromFavorite == "Fav"{
                    namefromLabel.text = "Name From Favorite".localized
                }
                else{
                    namefromLabel.text = "Name From Contact".localized
                }
                
                
                    self.mobileNumber.text = phoneNumber
                    InternationalTopupManager.receiverName = contact.displayName()
                    self.strBeneficiaryName = contact.displayName()
                
                    self.mobileNumberRightView.hideClearButton(hide: true)
                  
                    self.confirmMobileNumber.leftView = nil
                   // self.confirmMobileNumber.isHidden = false
                    self.phoneuiTopConstraint.constant = 0
                    self.confirmuiView.isHidden = true
                    self.phoneuiheightConstraint.constant = 0
//                    self.confirmuiView.layer.cornerRadius = 8.0
//                    self.phoneuiView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
//                    self.confirmuiView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
                  //  self.confirmmobileLabel.text = "Name".localized
                    self.selectOperator.isHidden = false
                    self.selectserivceImage.isHidden = false
                    self.operatoruiView.isHidden = false
                    self.lblOperatorText.isHidden = false
                    self.confirmMobileNumber.leftViewMode = .always
                    self.confirmMobileNumber.name = true
                  //  self.confirmMobileNumber.layoutIfNeeded()
                 //   self.confirmMobileNumber.leftView = contactViewLeft
                    self.confirmcountrycodeLabel.isHidden = true
                    self.confmobileLeftImage.image = UIImage(named:"newpayuser")
                
                    self.usernameView.isHidden = false
                    self.userviewTopConstraint.constant = 2
                    self.userviewHeightConstraint.constant = 70
                    self.usernameLabel.text = contact.displayName()
                
                    self.confirmMobileNumber.text = contact.displayName()
                    self.contactname = contact.displayName()
                    self.confirmMobileNumber.isUserInteractionEnabled = false
                    self.confMobile?.hideClearButton(hide: true)
                    self.confirmfieldhide = "From Contacts"
            //    }
               
               
            }
        }
    }
    
    func getMinMaxForOverseas(countryCode: String) -> (minLength: Int, maxLength: Int)? {
        if countryCode == "+95" || countryCode == "95" || countryCode == "0095" {
            return nil
        } else {
            switch countryCode {
            case "+91":
                return (10,10)
            case "+86":
                return (11,11)
            case "+66":
                return (9,9)
            case let x where x.hasPrefix("+"):
                return (4,13)
            default:
                return nil
            }
        }
    }
    
    func checkAvailableCountry(country: String, handle: @escaping (Bool) -> Void) {
        let countryFirst = self.countriesDetail?.first(where: {$0.countryCode2.safelyWrappingString() == country})
        if countryFirst == nil {
            //InternationalTopupManager().internationalTopupAlert(title: "", body: InternationalTopupConstants.msgs.serviceNotAvail.localized)
                handle(false)
        } else {
            InternationalTopupManager.selectedCountryObject = countryFirst
            InternationalTopupManager.selectedCountry       = (country,selectCountryName)
            
            self.mobileNumberLeftView.wrapinternationalCountryViewData(img: "", str: selectCountryCode)
            self.confirmMobileNumberLeftView.wrapCountryViewData(img: InternationalTopupManager.selectedCountry.flag, str: InternationalTopupManager.selectedCountry.code)
            
            self.confirmMobileNumber.leftView = nil
            self.mobileNumber.leftView = nil
            
            self.mobileNumber.leftView = self.mobileNumberLeftView
            //self.confirmMobileNumber.leftView = self.confirmMobileNumberLeftView
           // self.confirmMobileNumber.leftView = self.contactViewLeft
          //  self.confirmcountrycodeLabel.isHidden = true
         //   self.confmobileLeftImage.image = UIImage(named:"newpayuser")
            
            usernameView.isHidden = false
            userviewTopConstraint.constant = 2
            userviewHeightConstraint.constant = 70
            usernameLabel.text = ""
            
            
            self.confirmMobileNumber.isUserInteractionEnabled = true
//            if let recharge = self.rechargePlan {
//                recharge.regeneratingRechargePlan()
//            }
            handle(true)
        }
    }
}
