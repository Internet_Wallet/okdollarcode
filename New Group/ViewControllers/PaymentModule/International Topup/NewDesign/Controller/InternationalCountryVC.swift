//
//  InternationalCountryVC.swift
//  OK
//
//  Created by OK$ on 28/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class InternationalCountryVC: OKBaseController {

    @IBOutlet weak var lblTitle: UILabel!{
        didSet {
            lblTitle.text = "International Top-Up".localized
            lblTitle.font = UIFont(name: appFont, size: 18.0)
        }
    }
    var strPlanSelection: String?
    @IBOutlet weak var tblCountry : UITableView!
    var arrCountryList : [InternationalCountryListModel]?
    var arrFilterList : [InternationalCountryListModel] = []
    
    @IBOutlet weak var searchBar: UISearchBar!{
        didSet {
            self.searchBar.placeholder = "Search".localized
        }
    }

    @IBOutlet weak var noRecordsLabel : UILabel!
      {
          didSet {
            self.noRecordsLabel.text = "No countries found!".localized
              self.noRecordsLabel.isHidden = true
          }
      }
    
    @IBOutlet weak var btnBack: UIButton!{
        didSet {
            btnBack.setTitle("BACK".localized, for: .normal)
            if let myFont = UIFont(name: appFont, size: appButtonSize) {
                btnBack.titleLabel?.font =  myFont
            }
        }
    }
    
    @IBOutlet var buttonBottomConstraint: NSLayoutConstraint!
    var KeyboardSize : CGFloat = 0.0
    
    weak var selectedCountrydelegate : InternationalControllerDelegate?

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        println_debug(strPlanSelection)
        // Do any additional setup after loading the view.
        tblCountry.tableFooterView = UIView(frame: CGRect.zero)
        
        searchBar.setImage(UIImage(named: "bluesearch"), for: .search, state: .normal)
        searchBar.placeholder = "Search".localized
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
                //searchTextField.textColor = UIColor.black
            }
        }
        
        noRecordsLabel.isHidden = true
        
      //  self.initialWebCall()
        
        if #available(iOS 13, *) {
                   UIApplication.statusBarBackgroundColor =  kYellowColor
               } else {
                   if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                       statusbar.backgroundColor = kYellowColor
                   }
               }
        
        self.view.layoutIfNeeded()
        subscribeToShowKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification,object : nil)
        self.view.endEditing(true)
    }
    
     @IBAction func onClickBackBtn(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
       }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            //            self.adImageConstraint.constant = 0 - keyboardHeight
            
        if device.type == .iPhoneX || device.type == .iPhoneXR || device.type == .iPhoneXS || device.type == .iPhoneXSMax {
                    self.buttonBottomConstraint.constant = keyboardHeight-10
                }
            else {
                    self.buttonBottomConstraint.constant = keyboardHeight
                }
            
        })
    }
    @objc func keyboardWillHide(_ notification: Notification) {
        //        self.adImageConstraint.constant = 0
        self.buttonBottomConstraint.constant = 0
      
    }
    func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

}

extension InternationalCountryVC : UITableViewDelegate, UITableViewDataSource {
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           let headerView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 40))
        if #available(iOS 13.0, *) {
            headerView.backgroundColor = UIColor.systemGray5
        } else {
            // Fallback on earlier versions
        }//UIColor.colorWithRedValue(redValue: 244, greenValue: 244, blueValue: 245, alpha: 1)
           let headerLabel = UILabel(frame: CGRect(x: 12, y: 5, width: screenWidth-40, height: 30))
           headerLabel.text = "    Select Country".localized
            headerLabel.textColor = kBlueColor
           headerLabel.font = UIFont.init(name: appFont, size: 16)
           headerView.addSubview(headerLabel)
           return headerView
       }
       
       func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           return 40
       }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchBar.text!.count > 0 {
            return arrFilterList.count
              }
        return arrCountryList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row % 2 == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: "InternationalCountryEvenCell"), for: indexPath) as? InternationalCountryCell
            if searchBar.text!.count > 0 {
                let dict = arrFilterList[indexPath.row]
                cell?.lblCountryID.text = dict.countryCode2 ?? ""
                cell?.lblCountryName.text = dict.countryName
                cell?.lblCountryName.textColor = kBlueColor
                cell?.imgFlag.image = nil
                if let imageStr = dict.countryFlag, let imageUrl = URL(string: imageStr) {
                    cell?.imgFlag.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: ""))
                }
            } else {
                let dict = arrCountryList?[indexPath.row]
                cell?.lblCountryID.text = dict?.countryCode2 ?? ""
                cell?.lblCountryName.text = dict?.countryName
                cell?.lblCountryName.textColor = kBlueColor
                cell?.imgFlag.image = nil
                if let imageStr = dict?.countryFlag, let imageUrl = URL(string: imageStr) {
                    cell?.imgFlag.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: ""))
                }
            }
            
            cell?.selectionStyle = .none
            return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: "InternationalCountryOddCell"), for: indexPath) as? InternationalCountryCell
            if searchBar.text!.count > 0 {
                let dict = arrFilterList[indexPath.row]
                cell?.lblCountryID.text = dict.countryCode2 ?? ""
                cell?.lblCountryName.text = dict.countryName
                cell?.lblCountryName.textColor = kBlueColor
                cell?.imgFlag.image = nil
                if let imageStr = dict.countryFlag, let imageUrl = URL(string: imageStr) {
                    cell?.imgFlag.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: ""))
                }
            } else {
                let dict = arrCountryList?[indexPath.row]
                cell?.lblCountryID.text = dict?.countryCode2 ?? ""
                cell?.lblCountryName.text = dict?.countryName
                cell?.lblCountryName.textColor = kBlueColor
                cell?.imgFlag.image = nil
                if let imageStr = dict?.countryFlag, let imageUrl = URL(string: imageStr) {
                    cell?.imgFlag.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: ""))
                }
            }
            cell?.selectionStyle = .none
            return cell!
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // if let addWithdrawView = UIStoryboard(name: "InternationalTopUp", bundle: nil).instantiateViewController(withIdentifier: "InternationalMainVC") as? InternationalMainVC {
        //    addWithdrawView.modalPresentationStyle = .fullScreen
            if searchBar.text!.count > 0 {
//                addWithdrawView.strCountryCode = arrFilterList[indexPath.row].countryCode2
//                addWithdrawView.strCountryName = arrFilterList[indexPath.row].countryName
//                addWithdrawView.strCountryFlag = arrFilterList[indexPath.row].countryFlag
//                addWithdrawView.internationalCountryListModel = arrFilterList[indexPath.row]
               
                
                if arrFilterList[indexPath.row].countryCode2 ?? "" == "+95"{
                    alertViewObj.wrapAlert(title: nil, body: "Please go to topup other number for Myanmar Country topup.".localized, img: #imageLiteral(resourceName: "dashboard_overseas_recharge"))
                    alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                                
                    })
                    alertViewObj.showAlert(controller: self)
                }
                else{
                    self.selectedCountrydelegate?.countrySelected(countryName: arrFilterList[indexPath.row].countryName ?? "", CountryCode: arrFilterList[indexPath.row].countryCode2 ?? "", countryFlag: arrFilterList[indexPath.row].countryFlag ?? "",Countryid: arrFilterList[indexPath.row].countryId ?? "",TalktimeCountryid:arrFilterList[indexPath.row].talkTimeCountryId ?? "", FromScreen: "CountrySelect",internationalCountryListModel:arrFilterList[indexPath.row])
                    
                    self.dismiss(animated: true, completion: nil)
                }
                
                
            } else {
//                addWithdrawView.strCountryCode = arrCountryList?[indexPath.row].countryCode2
//                addWithdrawView.strCountryName = arrCountryList?[indexPath.row].countryName
//                addWithdrawView.strCountryFlag = arrCountryList?[indexPath.row].countryFlag
//                addWithdrawView.internationalCountryListModel = arrCountryList?[indexPath.row]
                
                if arrCountryList?[indexPath.row].countryCode2 ?? "" == "+95"{
                    alertViewObj.wrapAlert(title: nil, body: "Please go to topup other number for Myanmar Country topup.".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                    alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                                
                    })
                    alertViewObj.showAlert(controller: self)
                }
                else{
                    self.selectedCountrydelegate?.countrySelected(countryName: arrCountryList?[indexPath.row].countryName ?? "", CountryCode: arrCountryList?[indexPath.row].countryCode2 ?? "", countryFlag: arrCountryList?[indexPath.row].countryFlag ?? "",Countryid: arrCountryList?[indexPath.row].countryId ?? "",TalktimeCountryid:arrCountryList?[indexPath.row].talkTimeCountryId ?? "",FromScreen: "CountrySelect",internationalCountryListModel: arrCountryList?[indexPath.row])
                    
                    self.dismiss(animated: true, completion: nil)
                }
                
              
            }
          //  addWithdrawView.fromScreen = "CountrySelect"
         //   self.present(addWithdrawView, animated: true, completion: nil)
            
           // self.dismiss(animated: true, completion: nil)
     //   }
    }
}

// MARK: - UISearchDisplayDelegate
extension InternationalCountryVC: UISearchBarDelegate {
    
    // MARK: Methods
    fileprivate func filter(_ searchText: String) -> [InternationalCountryListModel] {
        arrFilterList.removeAll()
        
        arrCountryList?.forEach { (section) -> () in
            if section.countryName?.count ?? 0 >= searchText.count {
                let resultFinal = section.countryName!.lowercased().contains(find: searchText.lowercased())
                if resultFinal {
                    arrFilterList.append(section)
                }
            }
            if section.countryCode2?.count ?? 0 >= searchText.count {
                let resultFinal = section.countryCode2!.lowercased().contains(find: searchText.lowercased())
                if resultFinal {
                    arrFilterList.append(section)
                }
            }
        }
        
        return arrFilterList
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        let countryArray = filter(searchText)
        if countryArray.count == 0 && searchBar.text?.count != 0{
            tblCountry.isHidden = true
            self.noRecordsLabel.isHidden = false
        } else {
            tblCountry.isHidden = false
            self.noRecordsLabel.isHidden = true
        }
        tblCountry.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        

        if range.location == 0 && text == " " { return false }
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            if updatedText.containsEmoji {
                return false
            }
            if searchBar.text?.last == " " && text == " " {
                return false
            }
            if updatedText.count > 26 {
                return false
            }
            
            //applySearch(with: updatedText)
        }
        return true
        
//        let text       = (searchBar.text! as NSString).replacingCharacters(in: range, with: text)
        //let textCount  = text.count
        //let char = text.cString(using: String.Encoding.utf8)!
        //let isBackSpace = strcmp(char, "\\b")
        
//        let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: InternationalTopupConstants.acceptableCharacters.alphaNumeric).inverted
//        let filteredSet = text.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
        
//        if (textCount < 26){
//            return true
//        }
//        return false
    }
    
}

extension InternationalCountryVC{
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        self.KeyboardSize = (keyboardSize?.height)!
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize!.height + 45, right: 0.0)
        self.tblCountry.contentInset = contentInsets
        self.tblCountry.scrollIndicatorInsets = contentInsets
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if searchBar.self != nil{
            if (!aRect.contains((searchBar?.frame.origin)!)){
                self.tblCountry.scrollRectToVisible(searchBar!.frame, animated: true)
            }
        }
    }
    @objc func keyboardWillChangeHeight(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        self.KeyboardSize = (keyboardSize?.height)!
        //println_debug("Keyboard changed \(KeyboardSize,keyboardSize?.height))")
        //        classActionButton?.frame.origin.y = self.view.frame.height - KeyboardSize - 54
        if device.type == .iPhoneX || device.type == .iPhoneXR || device.type == .iPhoneXS || device.type == .iPhoneXSMax {
               self.buttonBottomConstraint.constant = KeyboardSize-10
           }
           else {
                 self.buttonBottomConstraint.constant = KeyboardSize
            }
        
       // bottomGetBillBtnConstraint.constant = KeyboardSize
        self.view.layoutIfNeeded()
    }
    
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.tblCountry.contentInset = contentInsets
        self.tblCountry.scrollIndicatorInsets = contentInsets
        //        classActionButton?.frame.origin.y = self.view.frame.height - 54
        buttonBottomConstraint.constant = 0
    }
}
