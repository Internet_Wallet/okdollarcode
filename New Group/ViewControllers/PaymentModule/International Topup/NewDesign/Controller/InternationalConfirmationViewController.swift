//
//  InternationalConfirmationViewController.swift
//  OK
//
//  Created by iMac on 10/10/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class InternationalConfirmationViewController: OKBaseController {

    @IBOutlet var submitButton: UIButton!{
        didSet {
           
            if let myFont = UIFont(name: appFont, size: appButtonSize) {
                submitButton.titleLabel?.font =  myFont
            }
            submitButton.setTitle("SUBMIT".localized, for: .normal)
        }
    }
    @IBOutlet weak var titleLabel: UILabel!{
        didSet {
            titleLabel.font = UIFont(name: appFont, size: appFontSize)
            titleLabel.text = "Confirmation".localized
            
        }
    }
    
    @IBOutlet var topUpLabel: UILabel!{
        didSet{
            topUpLabel.font = UIFont(name: appFont, size: appFontSize)
            topUpLabel.text = "International Top-Up".localized
        }
    }
    @IBOutlet var countryLabel: UILabel!{
        didSet{
            countryLabel.font = UIFont(name: appFont, size: appFontSize)
            countryLabel.text = "Country".localized
        }
    }
    @IBOutlet var countryValue: UILabel!
    @IBOutlet var telecomLabel: UILabel!{
        didSet{
            telecomLabel.font = UIFont(name: appFont, size: appFontSize)
            telecomLabel.text = "Telecom Operator".localized
        }
    }
    @IBOutlet var telecomValue: UILabel!

    @IBOutlet var mobileLabel: UILabel!{
          didSet{
              mobileLabel.font = UIFont(name: appFont, size: appFontSize)
              mobileLabel.text = "Mobile Number".localized
          }
      }
    @IBOutlet var mobileValue: UILabel!
    @IBOutlet var lblTimeLeft: UILabel!{
        didSet{
            lblTimeLeft.font = UIFont(name: appFont, size: appFontSize)
            lblTimeLeft.text = "Time left to submit".localized
        }
    }

    @IBOutlet var topUpUnitsLabel: UILabel!{
             didSet{
                topUpUnitsLabel.font = UIFont(name: appFont, size: appFontSize)
                 topUpUnitsLabel.text = "Top-Up Units".localized
             }
         }
    @IBOutlet var topUpUnitsValue: UILabel!
    
    @IBOutlet var topUpAmountLabel: UILabel!{
        didSet{
            topUpAmountLabel.font = UIFont(name: appFont, size: appFontSize)
            topUpAmountLabel.text = "Amount".localized
        }
    }
    @IBOutlet var topUpAmountValue: UILabel!

    @IBOutlet var imgViewFlag: UIImageView!
    @IBOutlet var imgViewTopUp: UIImageView!

    @IBOutlet var amountbasecountryLabel: UILabel!{
        didSet{
            amountbasecountryLabel.font = UIFont(name: appFont, size: appFontSize)
            amountbasecountryLabel.text = "Amount".localized
        }
    }
    @IBOutlet var amountmmkLabel: UILabel!{
        didSet{
            amountmmkLabel.font = UIFont(name: appFont, size: appFontSize)
            amountmmkLabel.text = "Amount (MMK)".localized
        }
    }
    @IBOutlet var othercurrencyLabel: UILabel!
    
        
    var dictRequest : Dictionary<String,Any> = [:]
    var dictFinalRequest : Dictionary<String,Any> = [:]

    //Data Display
    var countryisdcode = ""
    var mmkamount = ""
    var finalRequestParam = ""
    var mobileNoParam = ""
    var mobiledisplay = ""
    
    //New Data Display
    var countryName = "Thailand"
    var countryFlag = ""
    var strTopUpPlan = ""
    var DestinationCurrency = ""
    var fromFavorite = ""
    var displayName = ""
    
    //Time display
//    var seconds        = 300
//    var timer          = Timer()
//    var isTimerRunning = false
    //var timerView =  TimerView.updateView()
    @IBOutlet var lblTimerDisplay: UILabel!
    
    var mobilenum = ""
    var ammountmmk = ""

    var strBeneficiaryName: String?
    
    var timerQR: Timer?
    var totalTime = 300
    var timeAddedFromServer = 0
    var previousTime = Date()
    var currentTime = Date()
    var previoushour = 0
    var previousMinute = 0
    var previousSecond = 0
    var currentHour = 0
    var currentMinute = 0
    var currentSecond = 0
    var wentBackGround = false
    var formatter = DateFormatter()
    
    @IBOutlet var contactView: UIView!
    @IBOutlet var nameFromLabel: UILabel!
    @IBOutlet var namedisplayLabel: UILabel!
    @IBOutlet var contactViewHeightContraint: NSLayoutConstraint!
    

    //MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    //    self.timerPay()
        //self.updateTimerViewAccordingtoLayout()
    //    _ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(searchForKeyword(_:)), userInfo: self.topUpAmountValue.text ?? "", repeats: false)
        
    }
         
      override func viewWillDisappear(_ animated: Bool) {
          super.viewDidDisappear(animated)
          //timerView.removeFromSuperview()
       //   timer.invalidate()
        if let timer = self.timerQR {
            timer.invalidate()
        }
          println_debug("Timer Invalidated")
      }

    
//    func updateTimerViewAccordingtoLayout() {
//        timerView.changeFrameForMulti()
//        if let keyWindow = UIApplication.shared.keyWindow {
//            keyWindow.makeKeyAndVisible()
//            keyWindow.addSubview(timerView)
//        }
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTimer()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "hh:mm:ss"
        NotificationCenter.default.addObserver(self, selector: #selector(moveToBackGround), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(enterForgound), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        if fromFavorite == "Unknown"{
            self.contactView.isHidden = true
            self.contactViewHeightContraint.constant = 0
        }
        else if fromFavorite == "Con"{
            self.contactView.isHidden = false
            self.contactViewHeightContraint.constant = 60
            self.nameFromLabel.text = "Name From Contact".localized
            self.namedisplayLabel.text = displayName
        }
        else{
            self.contactView.isHidden = false
            self.contactViewHeightContraint.constant = 60
            self.nameFromLabel.text = "Name From Favorite".localized
            self.namedisplayLabel.text = displayName
        }
        
        //New Data Display
        if strTopUpPlan == "TopUp" {
            topUpUnitsLabel.text = "Top-Up Units".localized
            //topUpUnitsValue.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: DestinationCurrency, textTwo: " ",textThree: " ", textfourth: "\(wrapAmountWithCommaDecimal(key:dictRequest["ProductValue"] as! String))", sizeOne: 17, sizeTwo: 11, color: .black)
            
            let finalString =  "\(wrapAmountWithCommaDecimal(key: dictRequest["ProductValue"] as! String))" + " " + DestinationCurrency
            let attrStr = NSMutableAttributedString(string: finalString)
            let nsRangeA = NSString(string: finalString).range(of:  wrapAmountWithCommaDecimal(key:dictRequest["ProductValue"].safelyWrappingString()), options: String.CompareOptions.caseInsensitive)
            let nsRangeB = NSString(string: finalString).range(of: DestinationCurrency, options: String.CompareOptions.caseInsensitive)
            attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: UIFont(name: appFont, size: 17) ?? UIFont.systemFont(ofSize: 17) , range: nsRangeA)
            attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: UIFont(name: appFont, size: 12) ?? UIFont.systemFont(ofSize: 12) , range: nsRangeB)
            topUpUnitsValue.attributedText = attrStr
            imgViewTopUp.image = #imageLiteral(resourceName: "merchantService").withRenderingMode(.alwaysOriginal)
        } else if strTopUpPlan == "DataPlan" {
            topUpUnitsLabel.text = "Top-Up Data".localized
            topUpUnitsValue.text = "\(wrapAmountWithCommaDecimal(key:dictRequest["ProductValue"] as! String))"// + "  " + DestinationCurrency
            imgViewTopUp.image = #imageLiteral(resourceName: "topup_amount").withRenderingMode(.alwaysOriginal)
        } else {
            topUpUnitsLabel.text = "Special Offer".localized
            topUpUnitsValue.text = "\(dictRequest["ProductValue"] ?? "")"
            imgViewTopUp.image = #imageLiteral(resourceName: "star1").withRenderingMode(.alwaysOriginal)
        }
        
        let finalString =  "\(wrapAmountWithCommaDecimal(key: dictRequest["AmountInMMK"] as! String))" + " " + "MMK"
        let attrStr = NSMutableAttributedString(string: finalString)
        let nsRangeA = NSString(string: finalString).range(of:  wrapAmountWithCommaDecimal(key:dictRequest["AmountInMMK"].safelyWrappingString()), options: String.CompareOptions.caseInsensitive)
        let nsRangeB = NSString(string: finalString).range(of: "MMK", options: String.CompareOptions.caseInsensitive)
        attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: UIFont(name: appFont, size: 17) ?? UIFont.systemFont(ofSize: 17) , range: nsRangeA)
        attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: UIFont(name: appFont, size: 12) ?? UIFont.systemFont(ofSize: 12) , range: nsRangeB)
        topUpAmountValue.attributedText = attrStr
        
        //"\(wrapAmountWithCommaDecimal(key: dictRequest["AmountInMMK"] as! String))" + "  " + "MMK"
        //othercurrencyLabel.text = DestinationCurrency
        if countryName == ""{
            self.countryValue.text = "Thailand"
        }else{
            self.countryValue.text = countryName
        }
       
        let imageUrl = URL(string: countryFlag)
        self.imgViewFlag.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: ""))

        
        //Old Data Display
        lblTimerDisplay.text = "04:59"
        mobiledisplay  = dictRequest["MobileNumber"] as? String ?? ""
      
        mobileValue.text = "(" + countryisdcode + ") " + mobiledisplay
        telecomValue.text = dictRequest["OperatorName"] as? String ?? ""
        mmkamount = dictRequest["AmountInMMK"] as? String ?? ""
        mobileNoParam = countryisdcode.replacingOccurrences(of: "+", with: "") + mobiledisplay
        
        //Model update
        // Transaction cell tower
               var TransactionsCellTower  = Dictionary<String,String>()
               TransactionsCellTower["Lac"] = ""
               TransactionsCellTower["Mcc"] = mccStatus.mcc
               TransactionsCellTower["Mnc"] = mccStatus.mnc
               TransactionsCellTower["SignalStrength"] = ""
               TransactionsCellTower["NetworkType"] = UitilityClass.getNetworkType()
               TransactionsCellTower["NetworkSpeed"] = UitilityClass.getSignalStrength()
               
               if UitilityClass.isConnectedToWifi(){
                   let wifiInfo = UitilityClass.getNetworkInfos()[0].components(separatedBy: ",")
                TransactionsCellTower["ConnectedwifiName"] = wifiInfo[0]
                TransactionsCellTower["Ssid"] = wifiInfo[0]
                TransactionsCellTower["Mac"] = wifiInfo[1]
               }else{
                   TransactionsCellTower["ConnectedwifiName"] = ""
                   TransactionsCellTower["Ssid"] = ""
                   TransactionsCellTower["Mac"] = ""
               }
        
        //Final Request Data
        if strTopUpPlan == "TopUp" {
            let strPipeLine = "ClientId=27ca593c-3f1d-44a0-899b-648f96a66c9c|OkAccountNumber=\(UserModel.shared.mobileNo)|Password=\(ok_password ?? "")|MobileNumber=\(mobileNoParam)|CountryId=\(dictRequest["CountryId"] ?? "")|CountryName=\(dictRequest["CountryName"] ?? "")|OperatorId=\(dictRequest["OperatorId"] ?? "")|OperatorName=\(dictRequest["OperatorName"] ?? "")|ProductCurrency=\(dictRequest["ProductCurrency"] ?? "")|ProductValue=\(dictRequest["ProductValue"] ?? "")|AmountInMMK=\(dictRequest["AmountInMMK"] ?? "")|RetailPriceUSD=\(dictRequest["RetailPriceUSD"] ?? "")|WholeSalePrice=\(dictRequest["WholeSalePrice"] ?? "")|AppliedExchangeRateUSD=\(dictRequest["AppliedExchangeRateUSD"] ?? "")|ContactName=\(dictRequest["ContactName"] ?? "")|Remark=\(dictRequest["Remark"] ?? "")|OKComment=\(dictRequest["OKComment"] ?? "")|TransactionsCellTower=\("{\"ConnectedwifiName\":\"\(TransactionsCellTower["ConnectedwifiName"] ?? "")\",\"Mcc\":\"\(TransactionsCellTower["Mcc"] ?? "")\",\"Mnc\":\"\(TransactionsCellTower["Mnc"] ?? "")\",\"Lac\":\"\(TransactionsCellTower["Lac"] ?? "")\",\"SignalStrength\":\"\(TransactionsCellTower["SignalStrength"] ?? "")\",\"Ssid\":\"\(TransactionsCellTower["Ssid"] ?? "")\",\"Mac\":\"\(TransactionsCellTower["Mac"] ?? "")\",\"NetworkType\":\"\(TransactionsCellTower["NetworkType"] ?? "")\"}")|lGeoLocation=\("{\"Latitude\": \"\(geoLocManager.currentLatitude ?? "0")\",\"Longitude\": \"\(geoLocManager.currentLongitude ?? "0")\",\"CellID\": \"iOS\"}")"
            
            let hash = strPipeLine.hmac_SHA1(key: InternationalConstantURL.ApiKeys.secretKey)
            
            let keys : [String] = ["ClientId","OkAccountNumber","Password","MobileNumber", "CountryId","CountryName","OperatorId","OperatorName", "ProductCurrency","ProductValue","AmountInMMK","RetailPriceUSD", "WholeSalePrice", "AppliedExchangeRateUSD", "ContactName", "Remark","OKComment","TransactionsCellTower","lGeoLocation","HashValue"]
            
            let values : [String] = ["27ca593c-3f1d-44a0-899b-648f96a66c9c",UserModel.shared.mobileNo, ok_password ?? "", "\(mobileNoParam)","\(dictRequest["CountryId"] ?? "")","\(dictRequest["CountryName"] ?? "")","\(dictRequest["OperatorId"] ?? "")", "\(dictRequest["OperatorName"] ?? "")",  "\(dictRequest["ProductCurrency"] ?? "")",  "\(dictRequest["ProductValue"] ?? "")",  "\(dictRequest["AmountInMMK"] ?? "")",  "\(dictRequest["RetailPriceUSD"] ?? "")",  "\(dictRequest["WholeSalePrice"] ?? "")",  "\(dictRequest["AppliedExchangeRateUSD"] ?? "")", "\(dictRequest["ContactName"] ?? "")", "\(dictRequest["Remark"] ?? "")","\(dictRequest["OKComment"] ?? "")","{\\\"ConnectedwifiName\\\":\\\"\(TransactionsCellTower["ConnectedwifiName"] ?? "")\\\",\\\"Mcc\\\":\\\"\(TransactionsCellTower["Mcc"] ?? "")\\\",\\\"Mnc\\\":\\\"\(TransactionsCellTower["Mnc"] ?? "")\\\",\\\"Lac\\\":\\\"\(TransactionsCellTower["Lac"] ?? "")\\\",\\\"SignalStrength\\\":\\\"\(TransactionsCellTower["SignalStrength"] ?? "")\\\",\\\"Ssid\\\":\\\"\(TransactionsCellTower["Ssid"] ?? "")\\\",\\\"Mac\\\":\\\"\(TransactionsCellTower["Mac"] ?? "")\\\",\\\"NetworkType\\\":\\\"\(TransactionsCellTower["NetworkType"] ?? "")\\\"}", "{\\\"Latitude\\\": \\\"\(geoLocManager.currentLatitude ?? "0")\\\",\\\"Longitude\\\": \\\"\(geoLocManager.currentLongitude ?? "0")\\\",\\\"CellID\\\": \\\"iOS\\\"}", hash]
            
            let jsonObj = JSONStringWriter()
            finalRequestParam = jsonObj.writeJsonIntoString(keys, values)
            println_debug(finalRequestParam)
        } else if strTopUpPlan == "DataPlan" {
            let strPipeLine = "ClientId=27ca593c-3f1d-44a0-899b-648f96a66c9c|OkAccountNumber=\(UserModel.shared.mobileNo)|Password=\(ok_password ?? "")|MobileNumber=\(mobileNoParam)|CountryId=\(dictRequest["CountryId"] ?? "")|CountryName=\(dictRequest["CountryName"] ?? "")|OperatorId=\(dictRequest["OperatorId"] ?? "")|OperatorName=\(dictRequest["OperatorName"] ?? "")|ProductId=\(dictRequest["ProductId"] ?? "")|ProductName=\(dictRequest["ProductName"] ?? "")|ProductCurrency=\(dictRequest["ProductCurrency"] ?? "")|ProductValue=\(dictRequest["ProductValue"] ?? "")|AmountInMMK=\(dictRequest["AmountInMMK"] ?? "")|RetailPriceUSD=\(dictRequest["RetailPriceUSD"] ?? "")|WholeSalePrice=\(dictRequest["WholeSalePrice"] ?? "")|AppliedExchangeRateUSD=\(dictRequest["AppliedExchangeRateUSD"] ?? "")|ContactName=\(dictRequest["ContactName"] ?? "")|Remark=\(dictRequest["Remark"] ?? "")|OKComment=\(dictRequest["OKComment"] ?? "")|TransactionsCellTower=\("{\"ConnectedwifiName\":\"\(TransactionsCellTower["ConnectedwifiName"] ?? "")\",\"Mcc\":\"\(TransactionsCellTower["Mcc"] ?? "")\",\"Mnc\":\"\(TransactionsCellTower["Mnc"] ?? "")\",\"Lac\":\"\(TransactionsCellTower["Lac"] ?? "")\",\"SignalStrength\":\"\(TransactionsCellTower["SignalStrength"] ?? "")\",\"Ssid\":\"\(TransactionsCellTower["Ssid"] ?? "")\",\"Mac\":\"\(TransactionsCellTower["Mac"] ?? "")\",\"NetworkType\":\"\(TransactionsCellTower["NetworkType"] ?? "")\"}")|lGeoLocation=\("{\"Latitude\": \"\(geoLocManager.currentLatitude ?? "0")\",\"Longitude\": \"\(geoLocManager.currentLongitude ?? "0")\",\"CellID\": \"iOS\"}")"
            
            let hash = strPipeLine.hmac_SHA1(key: InternationalConstantURL.ApiKeys.secretKey)
            
            let keys : [String] = ["ClientId","OkAccountNumber","Password","MobileNumber", "CountryId","CountryName","OperatorId","OperatorName","ProductId","ProductName", "ProductCurrency","ProductValue","AmountInMMK","RetailPriceUSD", "WholeSalePrice", "AppliedExchangeRateUSD", "ContactName", "Remark","OKComment","TransactionsCellTower","lGeoLocation","HashValue"]
            
            let values : [String] = ["27ca593c-3f1d-44a0-899b-648f96a66c9c",UserModel.shared.mobileNo, ok_password ?? "", "\(mobileNoParam)","\(dictRequest["CountryId"] ?? "")","\(dictRequest["CountryName"] ?? "")","\(dictRequest["OperatorId"] ?? "")", "\(dictRequest["OperatorName"] ?? "")", "\(dictRequest["ProductId"] ?? "")", "\(dictRequest["ProductName"] ?? "")", "\(dictRequest["ProductCurrency"] ?? "")",  "\(dictRequest["ProductValue"] ?? "")",  "\(dictRequest["AmountInMMK"] ?? "")",  "\(dictRequest["RetailPriceUSD"] ?? "")",  "\(dictRequest["WholeSalePrice"] ?? "")",  "\(dictRequest["AppliedExchangeRateUSD"] ?? "")", "\(dictRequest["ContactName"] ?? "")", "\(dictRequest["Remark"] ?? "")","\(dictRequest["OKComment"] ?? "")","{\\\"ConnectedwifiName\\\":\\\"\(TransactionsCellTower["ConnectedwifiName"] ?? "")\\\",\\\"Mcc\\\":\\\"\(TransactionsCellTower["Mcc"] ?? "")\\\",\\\"Mnc\\\":\\\"\(TransactionsCellTower["Mnc"] ?? "")\\\",\\\"Lac\\\":\\\"\(TransactionsCellTower["Lac"] ?? "")\\\",\\\"SignalStrength\\\":\\\"\(TransactionsCellTower["SignalStrength"] ?? "")\\\",\\\"Ssid\\\":\\\"\(TransactionsCellTower["Ssid"] ?? "")\\\",\\\"Mac\\\":\\\"\(TransactionsCellTower["Mac"] ?? "")\\\",\\\"NetworkType\\\":\\\"\(TransactionsCellTower["NetworkType"] ?? "")\\\"}", "{\\\"Latitude\\\": \\\"\(geoLocManager.currentLatitude ?? "0")\\\",\\\"Longitude\\\": \\\"\(geoLocManager.currentLongitude ?? "0")\\\",\\\"CellID\\\": \\\"iOS\\\"}", hash]
            
            let jsonObj = JSONStringWriter()
            finalRequestParam = jsonObj.writeJsonIntoString(keys, values)
            println_debug(finalRequestParam)
        } else {
            let strPipeLine = "ClientId=27ca593c-3f1d-44a0-899b-648f96a66c9c|OkAccountNumber=\(UserModel.shared.mobileNo)|Password=\(ok_password ?? "")|MobileNumber=\(mobileNoParam)|CountryId=\(dictRequest["CountryId"] ?? "")|CountryName=\(dictRequest["CountryName"] ?? "")|OperatorId=\(dictRequest["OperatorId"] ?? "")|OperatorName=\(dictRequest["OperatorName"] ?? "")|ProductCurrency=\(dictRequest["ProductCurrency"] ?? "")|ProductValue=\(dictRequest["ProductValue"] ?? "")|AmountInMMK=\(dictRequest["AmountInMMK"] ?? "")|RetailPriceUSD=\(dictRequest["RetailPriceUSD"] ?? "")|WholeSalePrice=\(dictRequest["WholeSalePrice"] ?? "")|AppliedExchangeRateUSD=\(dictRequest["AppliedExchangeRateUSD"] ?? "")|ContactName=\(dictRequest["ContactName"] ?? "")|Remark=\(dictRequest["Remark"] ?? "")|OKComment=\(dictRequest["OKComment"] ?? "")|TransactionsCellTower=\("{\"ConnectedwifiName\":\"\(TransactionsCellTower["ConnectedwifiName"] ?? "")\",\"Mcc\":\"\(TransactionsCellTower["Mcc"] ?? "")\",\"Mnc\":\"\(TransactionsCellTower["Mnc"] ?? "")\",\"Lac\":\"\(TransactionsCellTower["Lac"] ?? "")\",\"SignalStrength\":\"\(TransactionsCellTower["SignalStrength"] ?? "")\",\"Ssid\":\"\(TransactionsCellTower["Ssid"] ?? "")\",\"Mac\":\"\(TransactionsCellTower["Mac"] ?? "")\",\"NetworkType\":\"\(TransactionsCellTower["NetworkType"] ?? "")\"}")|lGeoLocation=\("{\"Latitude\": \"\(geoLocManager.currentLatitude ?? "0")\",\"Longitude\": \"\(geoLocManager.currentLongitude ?? "0")\",\"CellID\": \"iOS\"}")"
            
            let hash = strPipeLine.hmac_SHA1(key: InternationalConstantURL.ApiKeys.secretKey)
            
            let keys : [String] = ["ClientId","OkAccountNumber","Password","MobileNumber", "CountryId","CountryName","OperatorId","OperatorName", "ProductCurrency","ProductValue","AmountInMMK","RetailPriceUSD", "WholeSalePrice", "AppliedExchangeRateUSD", "ContactName", "Remark","OKComment","TransactionsCellTower","lGeoLocation","HashValue"]
            
            let values : [String] = ["27ca593c-3f1d-44a0-899b-648f96a66c9c",UserModel.shared.mobileNo, ok_password ?? "", "\(mobileNoParam)","\(dictRequest["CountryId"] ?? "")","\(dictRequest["CountryName"] ?? "")","\(dictRequest["OperatorId"] ?? "")", "\(dictRequest["OperatorName"] ?? "")",  "\(dictRequest["ProductCurrency"] ?? "")",  "\(dictRequest["ProductValue"] ?? "")",  "\(dictRequest["AmountInMMK"] ?? "")",  "\(dictRequest["RetailPriceUSD"] ?? "")",  "\(dictRequest["WholeSalePrice"] ?? "")",  "\(dictRequest["AppliedExchangeRateUSD"] ?? "")", "\(dictRequest["ContactName"] ?? "")", "\(dictRequest["Remark"] ?? "")","\(dictRequest["OKComment"] ?? "")","{\\\"ConnectedwifiName\\\":\\\"\(TransactionsCellTower["ConnectedwifiName"] ?? "")\\\",\\\"Mcc\\\":\\\"\(TransactionsCellTower["Mcc"] ?? "")\\\",\\\"Mnc\\\":\\\"\(TransactionsCellTower["Mnc"] ?? "")\\\",\\\"Lac\\\":\\\"\(TransactionsCellTower["Lac"] ?? "")\\\",\\\"SignalStrength\\\":\\\"\(TransactionsCellTower["SignalStrength"] ?? "")\\\",\\\"Ssid\\\":\\\"\(TransactionsCellTower["Ssid"] ?? "")\\\",\\\"Mac\\\":\\\"\(TransactionsCellTower["Mac"] ?? "")\\\",\\\"NetworkType\\\":\\\"\(TransactionsCellTower["NetworkType"] ?? "")\\\"}", "{\\\"Latitude\\\": \\\"\(geoLocManager.currentLatitude ?? "0")\\\",\\\"Longitude\\\": \\\"\(geoLocManager.currentLongitude ?? "0")\\\",\\\"CellID\\\": \\\"iOS\\\"}", hash]
            
            let jsonObj = JSONStringWriter()
            finalRequestParam = jsonObj.writeJsonIntoString(keys, values)
            println_debug(finalRequestParam)
        }
        
    }
    
    func addMoney() {
           alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
            alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                    self.modalPresentationStyle = .fullScreen
                    self.present(addWithdrawView, animated: true, completion: nil)
                }
            })
            alertViewObj.showAlert()
       }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitButtonClick(_ sender: Any) {
        
        if NSString(string: UserLogin.shared.walletBal).doubleValue < NSString(string: mmkamount.replacingOccurrences(of: ".", with: "")).doubleValue {
            
            self.addMoney()
        }
        
        else if NSString(string: UserLogin.shared.walletBal).floatValue < NSString(string: mmkamount.replacingOccurrences(of: ",", with: "")).floatValue {
            self.addMoney()
        }
        else{
            self.initialWebCall()
        }
       
    }
//    @objc   func timerManage() {
//        print("@@@@ Time Expired")
//         userDef.removeObject(forKey: "paymentrepeat")
//   }
//   func runProdTimer() {
//       timer = Timer.scheduledTimer(timeInterval: 120, target: self, selector: (#selector(timerManage)), userInfo: nil, repeats: true)
//       isTimerRunning = true
//
//   }
//
//    func timerPay() {
//        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(InternationalConfirmationViewController.updateTimer)), userInfo: nil, repeats: true)
//        timer.tolerance = 0.1
//    }
//
//    func timeFormatted(_ totalSeconds: Int) -> String {
//           let seconds: Int = totalSeconds % 60
//           let minutes: Int = (totalSeconds / 60) % 60
//           return String(format: "%01d:%02d", minutes, seconds)
//       }
       
//    @objc func updateTimer() {
//        seconds -= 1
////        let timerString = (seconds <= 9) ? "0:" + "0" + "\(seconds)" :  "0:" + "\(seconds)"
////        timerView.wrapTimerView(timer: timerString)
////
////        self.timeLabel.text = self.timeFormatted(seconds)
//
//        let timerString = self.timeFormatted(seconds)//(seconds <= 9) ? "0:" + "0" + "\(seconds)" :  "0:" + "\(seconds)"
//        //timerView.wrapTimerView(timer: timerString)
//
//        lblTimerDisplay.text = timerString
//        lblTimerDisplay.textColor = kBlueColor
//
//        if seconds == 0 {
////            UIApplication.shared.endBackgroundTask(backgroundTask)
////            backgroundTask = UIBackgroundTaskIdentifier.invalid
//            if timer.isValid {
//                timer.invalidate()
//                alertViewObj.wrapAlert(title: nil, body: "Session expired".localized, img:#imageLiteral(resourceName: "alert-icon.png"))
//                alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
//                    self.dismiss(animated: true, completion: nil)
//                    //self.navigationController?.popViewController(animated: true)
//                })
//                alertViewObj.showAlert(controller: self)
//            }
//        } else if seconds <= 10 {
//            lblTimerDisplay.textColor = .red
//
////            timerView.wrapTimerViewUpdate(color: UIColor.red, alpha: 1.00)
////            UIView.animate(withDuration: 0.2, animations: {
////                self.timerView.wrapTimerViewUpdate(color: UIColor.red, alpha: 0.00)
////            }) { (bool) in
////                self.timerView.wrapTimerViewUpdate(color: UIColor.red, alpha: 1.00)
////            }
//        }
//    }
    
    @objc func searchForKeyword(_ timer: Timer) {


        let amout = self.topUpAmountValue.text?.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: ".", with: "")
        if let twoMinutesRef = amout {

//            if var uiNumber = mobiledisplay {
//                if uiNumber.hasPrefix("0") {
//                    uiNumber = uiNumber.deletingPrefix("0")
//                }

                let  uiNumber = mobiledisplay //getAgentCode(numberWithPrefix: mobiledisplay, havingCountryPrefix: countryisdcode)
                if !PaymentVerificationManager.isValidPaymentTransactions(uiNumber, twoMinutesRef) {


                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: nil, body: "Same Number with Same Amount can not be paid before 2 minutes".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                        alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                           // self.amountClearButton()
                        })
                        alertViewObj.showAlert()
                        return
                    }

                }
            }
        }
}

extension InternationalConfirmationViewController{
    @objc func moveToBackGround() {
        let date = Date()
        previousTime = date
        let str = formatter.string(from: previousTime)
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: formatter.date(from: str) ?? Date())
        let minutes = calendar.component(.minute, from: formatter.date(from: str) ?? Date())
        let second = calendar.component(.second, from: formatter.date(from: str) ?? Date())
        previoushour = hour
        previousMinute = minutes
        previousSecond = second
        wentBackGround = true
    }
    
    @objc func enterForgound() {
        if wentBackGround {
            diffTime()
            wentBackGround = false
        }
    }
    
    func diffTime() {
        let str = formatter.string(from: Date())
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: formatter.date(from: str) ?? Date())
        let minutes = calendar.component(.minute, from: formatter.date(from: str) ?? Date())
        let second = calendar.component(.second, from: formatter.date(from: str) ?? Date())
        currentHour = hour
        currentMinute = minutes
        currentSecond = second
        
        if currentHour < previoushour {
            currentMinute = currentMinute + 60
        }
        if currentMinute < previousMinute {
            currentSecond = currentSecond + 60
        }
        let finalMin = currentMinute - previousMinute
        let finalSecond = currentSecond - previousSecond
        totalTime =  totalTime - (finalMin * 60 + finalSecond)
    }
    
    
    fileprivate func setTimer(){
        let value = totalTime/60

        if value<10{
            lblTimerDisplay.text = "\(0)\(value):00"
        }else{
            lblTimerDisplay.text = "\(value):00"
        }

       // timeLabel.text  = "05:00"

        self.startOtpTimer(time: totalTime - 1)
    }
    
    private func startOtpTimer(time: Int) {
        //5 min 300 sec
        self.totalTime = time
        self.timerQR = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        self.lblTimerDisplay.text = self.timeFormatted(self.totalTime)
        
        if totalTime > 0 {
            totalTime -= 1
        }else {
            if let timer = self.timerQR {
                timer.invalidate()
                alertViewObj.wrapAlert(title: "", body: "Session Expired".localized, img: #imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {

                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: nil)
                    }
                })
                
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%01d:%02d", minutes, seconds)
    }
}
