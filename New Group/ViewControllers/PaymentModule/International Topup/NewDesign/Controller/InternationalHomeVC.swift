//
//  InternationalHomeVC.swift
//  OK
//
//  Created by OK$ on 27/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class InternationalHomeVC: OKBaseController {

    @IBOutlet weak var tblInternational : UITableView!
    var arrHomeList = [NSMutableDictionary]()
    var navigation: UINavigationController?

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        if #available(iOS 13, *) {
            UIApplication.statusBarBackgroundColor =  kYellowColor
        } else {
            if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                statusbar.backgroundColor = kYellowColor
            }
        }
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        arrHomeList = [["Image": "top-up_sidemenu_new.png","PlanName":"Top-up"],["Image": "dashboard_postpaid_mobile.png","PlanName":"Data Plan"]]
        tblInternational.reloadData()
        //initialWebCall()
    }
    
    @IBAction func onClickBackBtn(_ sender: Any) {
      self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension InternationalHomeVC : UITableViewDelegate, UITableViewDataSource {
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrHomeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: InternationalHomeCell.self), for: indexPath) as? InternationalHomeCell
        cell?.lblPlanName.text = arrHomeList[indexPath.row]["PlanName"] as? String ?? ""
        cell?.imgPlan.image = UIImage(named: arrHomeList[indexPath.row]["Image"] as? String ?? "")
        cell?.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.00
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let addWithdrawView = UIStoryboard(name: "InternationalTopUp", bundle: nil).instantiateViewController(withIdentifier: "InternationalCountryVC") as? InternationalCountryVC {
            addWithdrawView.modalPresentationStyle = .fullScreen
            addWithdrawView.strPlanSelection = arrHomeList[indexPath.row]["PlanName"] as? String ?? ""
            self.present(addWithdrawView, animated: true, completion: nil)
        }
    }
}
