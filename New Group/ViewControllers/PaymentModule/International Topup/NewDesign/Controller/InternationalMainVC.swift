//
//  InternationalMainVC.swift
//  OK
//
//  Created by OK$ on 27/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Contacts
import SkyFloatingLabelTextField

protocol InternationalMainVCDelegate {
   
    func changeColor(color: UIColor,name: String)
    func singleObjectPayment()
    func singleObjectWithCustomMsg(msg: String)
    func didShowAdvertisement(show: Bool)
    func showAddObjectButton(show: Bool)
}

protocol InternationalControllerDelegate : class {
    func countrySelected(countryName: String, CountryCode:String, countryFlag: String, Countryid: String, TalktimeCountryid: String, FromScreen: String, internationalCountryListModel: InternationalCountryListModel?)
}

class InternationalMainVC: OKBaseController,InternationalControllerDelegate {
    
    var navigation: UINavigationController?
    var strCountryCode: String?
    var strCountryName: String?
    var strCountryFlag: String?
    var strOperatorID: String?
    var strBeneficiaryName: String?
    var internationalCountryListModel: InternationalCountryListModel?
    var internationalDataPlanListModel: InternationalDataPlanListModel?
    
    var fromScreen = ""
    
    var selectCountryCode = ""
    var selectCountryName = ""
    var selectCountryId = ""
    var selectTalktimeCountryId = "884"
 //   var flagimageView =  UIImageView()
    @IBOutlet var countryCodeLabel: UILabel!{
        didSet {
            countryCodeLabel.font = UIFont(name: appFont, size: appFontSize)
            countryCodeLabel.text = "Country".localized
        }
    }
    @IBOutlet var mobileNumberLabel: UILabel!{
        didSet {
            mobileNumberLabel.font = UIFont(name: appFont, size: appFontSize)
            mobileNumberLabel.text = "Mobile Number".localized
        }
    }
//    @IBOutlet var confirmmobileLabel: UILabel!{
//        didSet {
//            confirmmobileLabel.text = "Confirm Mobile Number".localized
//        }
//    }

    @IBOutlet var amountLabel: UILabel!{
           didSet {
               amountLabel.font = UIFont(name: appFont, size: appFontSize)
               amountLabel.text = "Amount".localized
           }
       }
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.font = UIFont(name: appFont, size: 18.0)
            titleLabel.text = "International Top-Up".localized
        }
    }
    
    @IBOutlet weak var btnPayBack : UIButton!{
        didSet {
           
            if let myFont = UIFont(name: appFont, size: appButtonSize) {
                btnPayBack.titleLabel?.font =  myFont
            }
            btnPayBack.setTitle("BACK".localized, for: .normal)
        }
    }
    
    @IBOutlet var selectCountryTF: UITextField!{
        didSet{
            selectCountryTF.font = UIFont(name: appFont, size: 17.0)
        }
    }
    @IBOutlet var selectAmountTextView: UITextView!
    
    @IBOutlet weak var mobileNumber: MobileRightViewSubClassTextField!{
        didSet{
            mobileNumber.font = UIFont(name: appFont, size: 17.0)
        }
    }
   
    @IBOutlet weak var confirmMobileNumber: MobileRightViewSubClassTextField! {
           didSet {
            confirmMobileNumber.font = UIFont(name: appFont, size: 17.0)
               self.confirmMobileNumber.cnf = true
           }
       }
    @IBOutlet weak var selectOperator: RightViewSubClassTextField!{
        didSet{
            selectOperator.font = UIFont(name: appFont, size: 17.0)
        }
    }
    
   // @IBOutlet weak var selectAmount: RightViewSubClassTextField!
    
    let mobileNumberView =  ContactRightView.updateView()
    // mobileNumberView.delegate = self
   let mobileNumberAcceptableCharacters = "0123456789"
    
  //  var mobileNumberLeftView = DefaultIconView.updateView(icon: "topup_number")
  //  var confirmmobileNumberLeftView = DefaultIconView.updateView(icon: "topup_number")
    
     var delegate: InternationalMainVCDelegate?
     var contactsArray = [CNContact]()
    var countriesDetail : [AllCountry]?
   
    @IBOutlet var flagimage: UIImageView!
    @IBOutlet var mobilecountrycodeLabel: UILabel!
    @IBOutlet var confirmcountrycodeLabel: UILabel!
    @IBOutlet var namefromLabel: UILabel!
    var fromFavorite = "Unknown"
    
    func placeholder() {
        self.mobileNumber.placeholder = "Enter Mobile Number".localized
        self.confirmMobileNumber.placeholder = "Confirm Mobile Number".localized
        self.selectOperator.placeholder = "Select Operator".localized
        self.selectCountryTF.placeholder = "Select Country".localized
    }
    
    // leftviews & rightViews
     var mobileNumberLeftView         = PaytoViews.confupdateView()
     var confirmMobileNumberLeftView  = PaytoViews.confupdateView()
     var confMobile : PTClearButton?
     var mobileNumberRightView        = ContactRightView.updateView()
     var selectOperatorLeftView       = DefaultIconView.updateViews(icon: "rechargeTower")
     //var selectOperatorRightView      = ExpandCollapseIconView.updateView(icon: "down_arrow_small")
  //   var selectAmountLeftView         = DefaultIconView.updateView(icon: "merchantService")
     var contactViewLeft              = DefaultIconView.updateView(icon: "newpayuser") //newpayuser
    
    var countryView   : PaytoViews?
    
    
    var validation : (min:Int,max:Int)?
    
    let validObj = PayToValidations.init()
    
    @IBOutlet var mobileLeftImage: UIImageView!
    @IBOutlet var confmobileLeftImage: UIImageView!
    
    
    var MMKAmount: String?
    var retailprice: String?
    var wholesaleprice: String?
    var productcurrency: String?
    var productvalue: String?
    var exchangerateusd: String?
    var productId: String?
    var productName: String?

    var contactname = ""
    var selectedPlan = ""

    @IBOutlet var mmkView: UIView!
    @IBOutlet var lblOperatorText: UILabel!{
        didSet {
            lblOperatorText.font = UIFont(name: appFont, size: appFontSize)
            lblOperatorText.text = "Operator".localized
        }
    }
    @IBOutlet var imgViewService: UIImageView!
    @IBOutlet var amountView: UIView!
    @IBOutlet var plantypeLabel: UILabel!{
        didSet{
            plantypeLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var othercurrencyLabel: UILabel!
    @IBOutlet var mmkLabel: UILabel!
    @IBOutlet var dividerLabel: UILabel!
    @IBOutlet var amountViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var amountTFtrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet var amountTFTopConstraint: NSLayoutConstraint!
    
    @IBOutlet var planTypeHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var arrowimage: UIImageView!
    
    @IBOutlet var selectserivceImage: UIImageView!
    
    @IBOutlet var slectcountryImageView: UIImageView!
    
    
    @IBOutlet var confirmfiledHeightConstraint: NSLayoutConstraint!
    
    
    var confirmfieldhide = ""
    
    @IBOutlet var usernameView: UIView!
    
    @IBOutlet var countryuiView: UIView!
    
    @IBOutlet var phoneuiView: UIView!
    
    @IBOutlet var confirmuiView: UIView!
    @IBOutlet var operatoruiView: UIView!
    
    @IBOutlet var userviewTopConstraint: NSLayoutConstraint!
    @IBOutlet var userviewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var usernameLabel: UILabel!
    
    @IBOutlet var phoneuiheightConstraint: NSLayoutConstraint!
    
    @IBOutlet var phoneuiTopConstraint: NSLayoutConstraint!
    var arrCountryList : [InternationalCountryListModel] = []
    
    //Navigate  from Recent View
    var statusScreen = ""
    var userObject : (number: String, name: String)?
   
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        validation = InternationalManager().validations(countryCode: internationalCountryListModel?.countryCode2 ?? "")
        
        selectAmountTextView.delegate = self
        selectAmountTextView.tintColor = UIColor.clear
        self.placeholder()
        if #available(iOS 13, *) {
            UIApplication.statusBarBackgroundColor =  kYellowColor
        } else {
            if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                statusbar.backgroundColor = kYellowColor
            }
        }
        selectCountryTF.resignFirstResponder()
        
        flagimage.layer.borderWidth = 1
        flagimage.layer.borderColor = UIColor.lightGray.cgColor
        
    //    countryuiView.layer.cornerRadius = 8.0
    //    phoneuiView.layer.cornerRadius = 8.0
      //  confirmuiView.layer.cornerRadius = 8.0
   //     operatoruiView.layer.cornerRadius = 8.0
   //     amountView.layer.cornerRadius = 8.0
   //     mmkView.layer.cornerRadius = 8.0
        
       // self.initialUpdates()
        self.initialSetupForTextfields()
        
        mmkView.isHidden = true
        lblOperatorText.isHidden = false
        
        //self.helpSupportNavigationEnum = .Electricity
        
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        appDel.floatingButtonControl?.window.isHidden = true
    }
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
       // let tappedImage = tapGestureRecognizer.view as! UIImageView

        selectCountryTF.resignFirstResponder()
        self.mobileNumber.becomeFirstResponder()
      //  self.clearActionType()
        var homeVC: InternationalCountryVC?
        homeVC = UIStoryboard(name: "InternationalTopUp", bundle: nil).instantiateViewController(withIdentifier: "InternationalCountryVC") as? InternationalCountryVC
        if let homeVC = homeVC {
            homeVC.selectedCountrydelegate = self
            homeVC.arrCountryList = arrCountryList.sorted(by: { $0.countryName ?? "" < $1.countryName ?? ""})
            self.present(homeVC, animated: true, completion: nil)
        }
    }
    
    @objc func imageTappedoperator(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if internationalCountryListModel?.dataPlanCountryId == "0" {
            self.showErrorAlert(errMessage: "Data Plan Not Available For This Country!")
        } else {
            if appDelegate.checkNetworkAvail() {
            if let addWithdrawView = UIStoryboard(name: "InternationalTopUp", bundle: nil).instantiateViewController(withIdentifier: "InternationalOperatorVC") as? InternationalOperatorVC {
                addWithdrawView.modalPresentationStyle = .fullScreen
                addWithdrawView.strCountryID = selectCountryId
                addWithdrawView.delegate = self
                self.present(addWithdrawView, animated: true, completion: nil)
            }
        }else {
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            alertViewObj.showAlert(controller: UIViewController.init())
        }
        }
    }
    
    @objc func imageTappedtopup(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if appDelegate.checkNetworkAvail() {

        if let addWithdrawView = UIStoryboard(name: "InternationalTopUp", bundle: nil).instantiateViewController(withIdentifier: "InternationalOffersVC") as? InternationalOffersVC {
            addWithdrawView.modalPresentationStyle = .fullScreen
            addWithdrawView.mobilenumber = self.mobileNumber.text ?? ""
            addWithdrawView.strCountryID = selectTalktimeCountryId
            addWithdrawView.strOperatorID = strOperatorID
            addWithdrawView.delegate = self
            self.present(addWithdrawView, animated: true, completion: nil)
        }
        } else {
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            alertViewObj.showAlert(controller: UIViewController.init())
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        slectcountryImageView.isUserInteractionEnabled = true
        slectcountryImageView.addGestureRecognizer(tapGestureRecognizer)
        
        let tapGestureRecognizeroperator = UITapGestureRecognizer(target: self, action: #selector(imageTappedoperator(tapGestureRecognizer:)))
        selectserivceImage.isUserInteractionEnabled = true
        selectserivceImage.addGestureRecognizer(tapGestureRecognizeroperator)
        
        let tapGestureRecognizertopup = UITapGestureRecognizer(target: self, action: #selector(imageTappedtopup(tapGestureRecognizer:)))
        arrowimage.isUserInteractionEnabled = true
        arrowimage.addGestureRecognizer(tapGestureRecognizertopup)
        
        
       
    }
    
    func countrySelected(countryName: String, CountryCode: String, countryFlag: String, Countryid: String, TalktimeCountryid: String, FromScreen: String,internationalCountryListModel: InternationalCountryListModel?) {
         print(countryName,countryFlag,CountryCode,FromScreen,Countryid)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.userviewTopConstraint.constant = 0
            self.mobileNumber.becomeFirstResponder()
        }
        
        if FromScreen == "CountrySelect"{
        
            
            
            selectCountryCode = CountryCode
            selectCountryName = countryName
            selectCountryId = Countryid
            selectTalktimeCountryId = TalktimeCountryid
            strCountryFlag = countryFlag
            
            let imageUrl = URL(string: countryFlag)
            self.flagimage.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: ""))

        //    "(" + CountryCode + ")" + " " +
            
            let displayTF =  countryName
            
            self.selectCountryTF.text =  displayTF
            
            usernameView.isHidden = true
          //  userviewTopConstraint.constant = 0
            userviewHeightConstraint.constant = 0
            
            self.aftercountryselectUpdates()

        }
        else{
            self.initialSetupForTextfields()
        }
    }
    
    private func initialUpdates() {
        
        
        self.btnPayBack.setTitle("BACK".localized, for: .normal)
        mobileNumber.delegate = self
        mobileNumberView.delegate = self
        self.mobileNumber.rightViewMode = .always
        self.mobileNumber.rightView = mobileNumberView
        mobileNumberView.hideClearButton(hide: true)
        
//        self.mobileNumber.leftViewMode = .always
//        self.mobileNumber.leftView  = mobileNumberLeftView
        self.mobileNumber.becomeFirstResponder()
     
        self.mobileNumber.addTarget(self, action: #selector(InternationalMainVC.textFieldDidChange(_:)),
                                    for: UIControl.Event.editingChanged)
        self.mobileNumber.isHidden = true
     //   self.confirmMobileNumber.isHidden = true
        self.confirmuiView.isHidden = true
        self.selectOperator.isHidden = true
        self.selectserivceImage.isHidden = true
        self.selectAmountTextView.isHidden = true
        self.amountView.isHidden = true
        
      //  self.confirmfiledHeightConstraint.constant = 0
        self.phoneuiheightConstraint.constant = 0
        self.operatoruiView.isHidden = true
        
    }
    
    // initial setup for textfiels
     func initialSetupForTextfields() {
        
        usernameView.isHidden = true
        userviewTopConstraint.constant = 0
        userviewHeightConstraint.constant = 0
        
        self.strCountryFlag = "https://ok-dollar.s3-ap-southeast-1.amazonaws.com/CountryFlag/thailand.png"
        //New Value setup
        let imageUrl = URL(string: self.strCountryFlag ?? "")
        self.flagimage.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: ""))
//        flagimageView.frame = CGRect(x: 12, y: 0, width: 30 , height: 30)
//        let paddingView: UIView = UIView.init(frame: CGRect(x: 0, y: 10, width: 50, height: 30))
//        paddingView.addSubview(flagimageView)
//        selectCountryTF.leftViewMode = .always
//        selectCountryTF.leftView = paddingView
        
        let displayTF =  "Thailand"
        selectCountryId = "477db17a-16d8-40ed-a17d-361b50eb3bde"
        
        self.selectCountryTF.text =  displayTF
        
        selectCountryCode = "+66"
//        self.mobileNumberLeftView.wrapinternationalCountryViewData(img: "topup_number", str: selectCountryCode)
//        self.mobileNumber.leftViewMode = .always
//        self.mobileNumber.leftView     = self.mobileNumberLeftView
        
        self.mobilecountrycodeLabel.text = "(" + selectCountryCode + ")"
         
//        self.confirmMobileNumberLeftView.wrapinternationalCountryViewData(img: "topup_number", str: selectCountryCode)
//        self.confirmMobileNumber.leftViewMode = .always
//        self.confirmMobileNumber.leftView     = self.confirmMobileNumberLeftView
        
        self.confirmcountrycodeLabel.text = "(" + selectCountryCode + ")"
        self.confirmMobileNumber.isUserInteractionEnabled = true
        self.mobileNumber.rightViewMode = .always
        self.mobileNumber.rightView     = self.mobileNumberRightView
        self.mobileNumberRightView.hideClearButton(hide: true)
        self.confMobile   = PTClearButton.updateView(strTag: "confMobile", delegate: self)
        self.confirmMobileNumber.rightView = self.confMobile
        self.confirmMobileNumber.rightViewMode = .always
        self.confMobile?.hideClearButton(hide: true)
//        self.selectAmount.leftViewMode = .always
//        self.selectAmount.leftView     = selectAmountLeftView
        
        // leftview & rightView marking
       // self.mobileNumberLeftView.delegate  = self
        self.mobileNumberRightView.delegate = self
        self.mobileNumber.becomeFirstResponder()
        
      //  self.selectOperator.leftViewMode = .always
      //  self.selectOperator.leftView     = selectOperatorLeftView
        //self.selectOperator.rightViewMode = .always
        //self.selectOperator.rightView = selectOperatorRightView
        
       // self.mobileNumber.isHidden = true
      //  self.confirmMobileNumber.isHidden = true
        self.confirmuiView.isHidden = true
        self.selectOperator.isHidden = true
        self.selectserivceImage.isHidden = true
        self.selectAmountTextView.isHidden = true
        self.amountView.isHidden = true
        
        self.operatoruiView.isHidden = true
        
      
       // self.confirmfiledHeightConstraint.constant = 0
        self.phoneuiheightConstraint.constant = 0
        
        amountViewHeightConstraint.constant = 70
       // amountTFtrailingConstraint.constant = 0
     //   amountTFTopConstraint.constant = 0
      //  planTypeHeightConstraint.constant = 0
        arrowimage.isHidden = false
        
  //      plantypeLabel.isHidden = true
        othercurrencyLabel.isHidden = true
        mmkLabel.isHidden = true
        dividerLabel.isHidden = true
        
        //Recent TopUp Country Display
              
//               else {
//                   self.initialWebCall()
//               }
        
      //  if statusScreen == "RecentTx" {
      //      println_debug("RecentTx")
        
        if arrCountryList.count == 0{
            self.initialWebCall()
        }
        
            

      //  }
    }
    
    private func aftercountryselectUpdates() {
        
     //   usernameView.isHidden = true
    //    userviewTopConstraint.constant = 0
    //    userviewHeightConstraint.constant = 0
        
        self.btnPayBack.setTitle("BACK".localized, for: .normal)
        mobileNumber.delegate = self
        mobileNumberView.delegate = self
      //  self.mobileNumber.becomeFirstResponder()
        
//        self.mobileNumberLeftView.wrapinternationalCountryViewData(img: "topup_number", str: selectCountryCode)
//        self.mobileNumber.leftViewMode = .always
//        self.mobileNumber.leftView     = self.mobileNumberLeftView
      
        
        
        self.mobilecountrycodeLabel.text = "(" + selectCountryCode + ")"

        self.clearActionType()
        
      //  self.mobileNumber.becomeFirstResponder()
        
//        self.confirmMobileNumberLeftView.wrapinternationalCountryViewData(img: "topup_number", str: selectCountryCode)
//        self.confirmMobileNumber.leftViewMode = .always
//        self.confirmMobileNumber.leftView     = self.confirmMobileNumberLeftView
        self.confirmcountrycodeLabel.text = "(" + selectCountryCode + ")"
        self.confirmMobileNumber.isUserInteractionEnabled = true
        self.mobileNumber.rightViewMode = .always
        self.mobileNumber.rightView     = self.mobileNumberRightView
        self.mobileNumberRightView.hideClearButton(hide: true)
        self.confMobile   = PTClearButton.updateView(strTag: "confMobile", delegate: self)
        self.confirmMobileNumber.rightView = self.confMobile
        self.confirmMobileNumber.rightViewMode = .always
        self.confMobile?.hideClearButton(hide: true)
//        self.selectAmount.leftViewMode = .always
//        self.selectAmount.leftView     = selectAmountLeftView
        
        // leftview & rightView marking
       // self.mobileNumberLeftView.delegate  = self
        self.mobileNumberRightView.delegate = self
        
      //  self.selectOperator.leftViewMode = .always
      //  self.selectOperator.leftView     = selectOperatorLeftView
//        self.selectOperator.rightViewMode = .always
//        self.selectOperator.rightView = selectOperatorRightView
      //  self.mobileNumber.isHidden = false
       // self.confirmMobileNumber.isHidden = true
        self.confirmuiView.isHidden = true
        self.selectOperator.isHidden = true
        self.selectserivceImage.isHidden = true
        self.selectAmountTextView.isHidden = true
        self.amountView.isHidden = true
        
        
        self.operatoruiView.isHidden = true
      
      //  self.confirmfiledHeightConstraint.constant = 0
        
        self.phoneuiheightConstraint.constant = 0
        
        amountViewHeightConstraint.constant = 70
        //amountTFtrailingConstraint.constant = 0
      //  amountTFTopConstraint.constant = 0
      //  planTypeHeightConstraint.constant = 0
      //  plantypeLabel.isHidden = true
        othercurrencyLabel.isHidden = true
        mmkLabel.isHidden = true
        dividerLabel.isHidden = true
    
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
          
      }
       
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
       
    @IBAction func onClickBackPayAction(_ sender: Any) {
        //API Hit
        //datapack/topup

        //Action Click
        if btnPayBack.titleLabel?.text == "BACK".localized {
            self.dismiss(animated: true, completion: nil)
        } else {
            OKPayment.main.authenticate(screenName: InternationalConstantURL.msgs.screenConfirm, delegate: self)
        }
    }
    
    func newActionSheet(){
        self.fromFavorite = "Unknown"

        let alert:UIAlertController = UIAlertController(title: nil ,message: nil, preferredStyle: .actionSheet)
        
        let title = UIAlertAction(
            title: "Choose From".localized,
            style: .default,
            handler: { action in
                //Do some thing here
            })
        
        let online = UIAlertAction(
            title: "Favorite".localized,
            style: .default,
            handler: { action in
                //Do some thing here
                
                let nav = self.openFavoriteFromNavigation(self, withFavorite: .overseas, selectionType: true,  multiPay: false, payToCount: MultiTopupArray.main.controllers.count - 1)
                self.present(nav, animated: true, completion: nil)
             
                alert.dismiss(animated: true)
            })
        let offline = UIAlertAction(
            title: "Contacts".localized,
            style: .default,
            handler: { action in
                let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
                self.present(nav, animated: true, completion: nil)
                self.fromFavorite = "Con"
                alert.dismiss(animated: true)
            })
        let cancel = UIAlertAction(
            title: "Cancel".localized,
            style: .cancel,
            handler: { action in
            })
        
        online.setValue(UIImage(named: "unionpayFav")?.withRenderingMode(.alwaysOriginal), forKey: "image")
        online.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        offline.setValue(UIImage(named: "unionpaycontact")?.withRenderingMode(.alwaysOriginal), forKey: "image")
        offline.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        
        alert.view.tintColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
        
        alert.addAction(title)
        alert.addAction(online)
        alert.addAction(offline)
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    func contact(contact: ContactPicker) {
//        self.confirmMobileNumber.leftViewMode = .always
//        self.confirmMobileNumber.leftView = nil
//        self.confirmMobileNumber.leftView = DefaultIconView.updateView(icon: "topup_number")
        
        usernameView.isHidden = true
        userviewTopConstraint.constant = 1
        userviewHeightConstraint.constant = 0
        self.mobileNumber.text = ""
        self.confirmMobileNumber.text = ""
        self.confirmcountrycodeLabel.text = self.mobilecountrycodeLabel.text
        self.confirmcountrycodeLabel.isHidden = false
        self.confmobileLeftImage.image = UIImage(named:"topup_number")
        self.selectAmountTextView.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: "Select Service", textTwo: "",textThree: "", textfourth: "", sizeOne: 17, sizeTwo: 11, color: .lightGray)
        
       // self.selectAmountTextView.text = "Select Service".localized
        self.plantypeLabel.font = UIFont(name: appFont, size: appFontSize)
        self.plantypeLabel.text = "Service".localized
        self.imgViewService.image = UIImage(named: "service_overseas")
        self.selectOperator.text = ""
       // self.selectAmount.rightView = nil
      
      //  self.confirmMobileNumber.isHidden = true
        self.confirmuiView.isHidden = true
     //   self.phoneuiView.layer.cornerRadius = 8.0
        self.selectAmountTextView.isHidden = true
        self.amountView.isHidden = true
      //  self.confirmfiledHeightConstraint.constant = 0
        self.phoneuiheightConstraint.constant = 0
        self.confirmMobileNumber.isUserInteractionEnabled = true
        self.mobileNumber.becomeFirstResponder()
        self.btnPayBack.setTitle("BACK".localized, for: .normal)

      //  self.delegate?.changeColor(color: MyNumberTopup.OperatorColorCode.okDefault, name: "")
    }
    func decodeContact(ph: String,name: String, amount: String = "", hide: Bool = false) {
        self.btnPayBack.setTitle("BACK".localized, for: .normal)

        if let cnfrightView = self.confirmMobileNumber.rightView as? PTClearButton {
            cnfrightView.hideClearButton(hide: true)
        }
        self.confirmMobileNumber.leftView = DefaultIconView.updateView(icon: "name_bill")
        var str = ""
        str = ph
        if str.count > 0 {
            str = str.replacingOccurrences(of: "(", with: "")
            str = str.replacingOccurrences(of: ")", with: "")
            str = str.trimmingCharacters(in: .whitespaces)
        }
        if str.hasPrefix("00") {
            _ = str.remove(at: String.Index.init(encodedOffset: 0))
            _ = str.remove(at: String.Index.init(encodedOffset: 0))
            str = "+" + str
        }
        let country = self.identifyCountry(withPhoneNumber: str)
        if country.0 == "+95" ||  country.1 == "myanmar" ||  country.0 == "" ||  country.1 == "" {
            let countryObject = Country.init(name: "", code: "myanmar", dialCode: "+95")
            if str.hasPrefix(countryObject.dialCode) {
                str = str.replacingOccurrences(of: countryObject.dialCode, with: "")
                str = "0" + str
            } else {
                if str.hasPrefix("0") {
                    
                } else {
                    str = "0" + str
                }
            }
            self.selectAmountTextView.text = amount
            if (amount == "") {
              //  self.selectAmount.rightView = nil
            }
            self.mobileNumber.text = str
            if hide {
               // self.confirmMobileNumber.isHidden = hide
                self.confirmuiView.isHidden = hide
               
            } else {
                if name == "" {
                    self.confirmMobileNumber.text = "Unknown"
                } else {
                    self.confirmMobileNumber.text = name
                }
                self.confirmMobileNumber.isUserInteractionEnabled = false
                
               // self.animateView {
                   // self.confirmMobileNumber.isHidden = false
                    self.confirmuiView.isHidden = false
//                    confirmuiView.layer.cornerRadius = 8.0
//                    phoneuiView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
//                    confirmuiView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
                  
                //}
            }
          
            self.delegate?.showAddObjectButton(show: true)
           // self.animateView {
                self.selectAmountTextView.isHidden = false
                self.selectAmountTextView.textColor = UIColor.black
                self.amountView.isHidden = false
            
          //  }
            self.mobileNumberView.hideClearButton(hide: false)
            let rangeCheck = PayToValidations().getNumberRangeValidation(str)
            self.selectOperator.text = (rangeCheck.operator != "Mpt") ? rangeCheck.operator : rangeCheck.operator.uppercased()
            if self.selectOperator.text ?? "" == "MPT" {
              //  self.selectAmount.placeholder = "Select Service".localized
            } else {
              //  self.selectAmount.placeholder = "Enter or Select Amount".localized
            }
         //   self.operatorName.textColor = PayToValidations().getColorForOperator(operatorName: rangeCheck.operator)
        } else {
          //  self.delegate?.showAddObjectButton(show: false)
//            guard let keyWinndow = UIApplication.shared.keyWindow else { return }
//            keyWinndow.makeToast("You Selected Other Country Number".localized, point: keyWinndow.center, title: nil, image: nil, completion: nil)
            if let cnfrightView = self.confirmMobileNumber.rightView as? PTClearButton {
                cnfrightView.hideClearButton(hide: true)
            }
            self.confirmMobileNumber.leftView = DefaultIconView.updateView(icon: "name_bill")
            
            self.mobileNumber.text = str
            if hide {
               // self.confirmMobileNumber.isHidden = hide
                self.confirmMobileNumber.isHidden = hide
               
            } else {
                if name == "" {
                    self.confirmMobileNumber.text = "Unknown"
                } else {
                    self.confirmMobileNumber.text = name
                }
                self.confirmMobileNumber.isUserInteractionEnabled = false
                
               // self.animateView {
                  //  self.confirmMobileNumber.isHidden = false
                    self.confirmuiView.isHidden = false
                    self.phoneuiheightConstraint.constant = 60
//                    confirmuiView.layer.cornerRadius = 8.0
//                    phoneuiView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
//                    confirmuiView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
                    //self.confirmfiledHeightConstraint.constant = 50
                //}
            }
          
            self.delegate?.showAddObjectButton(show: true)
           // self.animateView {
                self.selectAmountTextView.isHidden = false
                self.selectAmountTextView.textColor = UIColor.lightGray
                self.amountView.isHidden = false
            
          //  }
            self.mobileNumberView.hideClearButton(hide: false)
        }
    }
    
    func identifyCountry(withPhoneNumber prefix: String) -> (String, String) {
           let countArray = revertCountryArray() as! Array<NSDictionary>
           var finalCodeString = ""
           var flagCountry = ""
           for dic in countArray {
               if let code = dic .object(forKey: "CountryCode") as? String, let flag = dic.object(forKey: "CountryFlagCode") as? String{
                   if prefix.hasPrefix(code) {
                       finalCodeString = code
                       flagCountry = flag
                   }
               }
           }
           return (finalCodeString,flagCountry)
       }
    func revertCountryArray() -> NSArray {
        do {
            if let file = Bundle.main.url(forResource: "mcc_mnc", withExtension: "txt") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? NSDictionary {
                    if let arr = object.object(forKey: "data") as? NSArray {
                        arrCountry = arr
                        return arrCountry
                    }
                } else if let object = json as? [Any] {
                    println_debug(object)
                } else {
                    println_debug("JSON is invalid")
                }
            } else {
                println_debug("no file")
            }
        } catch {
            println_debug(error.localizedDescription)
        }
        return arrCountry
    }
  

}


//MARK: - PTClearButtonDelegate
extension InternationalMainVC: PTClearButtonDelegate {
    func clearField(strTag: String) {
        if strTag.hasPrefix("confMobile") {
         //   self.confirmMobileNumber.leftViewMode = .always
            self.btnPayBack.setTitle("BACK".localized, for: .normal)
            usernameView.isHidden = true
            userviewTopConstraint.constant = 0
            userviewHeightConstraint.constant = 0
            self.confirmcountrycodeLabel.text = self.mobilecountrycodeLabel.text
            self.confirmcountrycodeLabel.isHidden = false
            self.confmobileLeftImage.image = UIImage(named:"topup_number")
            self.confirmMobileNumber.text = ""
            self.selectOperator.text = ""
            self.confirmMobileNumber.becomeFirstResponder()
            self.selectAmountTextView.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: "Select Service".localized, textTwo: "",textThree: "", textfourth: "", sizeOne: 17, sizeTwo: 11, color: .lightGray)
           // self.selectAmountTextView.text = "Select Service".localized
            self.plantypeLabel.text = "Service".localized
            self.imgViewService.image = UIImage(named: "service_overseas")
            self.selectAmountTextView.isHidden = true
            self.amountView.isHidden = true
            self.mmkView.isHidden = true
            self.lblOperatorText.isHidden = true
            
            self.selectOperator.isHidden = true
            self.operatoruiView.isHidden = true
            self.selectserivceImage.isHidden = true
            self.confMobile?.hideClearButton(hide: true)
        }
    }
}

//MARK: - InternationalOperatorDelegate
extension InternationalMainVC : InternationalOperatorDelegate {
    func sendInternationalOperator(operatorID:String,operatorName: String) {
        println_debug(operatorName + operatorID)
        strOperatorID = operatorID
        self.selectOperator.text = operatorName
        self.selectAmountTextView.isHidden = false
        self.selectAmountTextView.textColor = UIColor.lightGray
        self.lblOperatorText.isHidden = false
        self.amountView.isHidden = false
        self.selectAmountTextView.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: "Select Service".localized, textTwo: "",textThree: "", textfourth: "", sizeOne: 17, sizeTwo: 11, color: .lightGray)
       // self.selectAmountTextView.text = "Select Service".localized
        self.imgViewService.image = UIImage(named: "service_overseas")
        amountViewHeightConstraint.constant = 70
       // amountTFtrailingConstraint.constant = 0
     //   amountTFTopConstraint.constant = 0
     //   planTypeHeightConstraint.constant = 0
        arrowimage.isHidden = false
        
        mmkView.isHidden = true
        plantypeLabel.text = "Service".localized
        othercurrencyLabel.isHidden = true
        mmkLabel.isHidden = true
        dividerLabel.isHidden = true
        self.btnPayBack.setTitle("BACK".localized, for: .normal)

        if confirmfieldhide == "From Contacts" {
          //  self.confirmfiledHeightConstraint.constant = 50
           // self.confirmMobileNumber.isHidden = false
//            self.phoneuiTopConstraint.constant = 2
//            self.phoneuiheightConstraint.constant = 60
//            self.confirmuiView.isHidden = false
            
            self.phoneuiTopConstraint.constant = 0
            self.phoneuiheightConstraint.constant = 0
            self.confirmuiView.isHidden = true
            
//            confirmuiView.layer.cornerRadius = 8.0
//            phoneuiView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
//            confirmuiView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]

        }
        else{
          //  self.confirmfiledHeightConstraint.constant = 0
          //  self.confirmMobileNumber.isHidden = true
         //   self.phoneuiView.layer.cornerRadius = 8.0
            self.phoneuiTopConstraint.constant = 0
            self.phoneuiheightConstraint.constant = 0
            self.confirmuiView.isHidden = true
        }
        
    }
}

//MARK: - InternationalOffersDelegate
extension InternationalMainVC : InternationalOffersDelegate {
    func sendInternationalOffersAmount(MMKAmount: String, retailprice: String, wholesaleprice: String, productcurrency: String, productvalue: String, exchangerateusd: String, selectedPlan: String, productname: String, productdescription:String, productId: String, productName: String) {
        println_debug(MMKAmount)
        if (UserLogin.shared.walletBal as NSString).floatValue <  Float(MMKAmount) ?? 0.0 {
            alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
            alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                self.selectAmountTextView.attributedText = self.returnAttributtedtextview(nameFont: appFont,textOne: "Select Service".localized, textTwo: "",textThree: "", textfourth: "", sizeOne: 17, sizeTwo: 11, color: .lightGray)
              //  self.selectAmountTextView.text = "Select Service".localized
                self.imgViewService.image = UIImage(named: "service_overseas")
                self.selectedPlan = ""
                self.othercurrencyLabel.text = ""
                self.plantypeLabel.text = "Service".localized
                
                self.productId = ""
                self.productName = ""
                self.MMKAmount = ""
                self.retailprice = ""
                self.wholesaleprice = ""
                self.productcurrency = ""
                self.productvalue = ""
                self.exchangerateusd = ""
                
              //  self.selectAmount.borderStyle = .roundedRect
                self.amountViewHeightConstraint.constant = 70
               // self.amountTFtrailingConstraint.constant = 0
            //    self.amountTFTopConstraint.constant = 0
            //    self.planTypeHeightConstraint.constant = 0
                
             //   self.plantypeLabel.isHidden = true
                self.othercurrencyLabel.isHidden = true
                self.mmkLabel.isHidden = true
                self.dividerLabel.isHidden = true
                self.btnPayBack.setTitle("BACK".localized, for: .normal)
                
                if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                    self.modalPresentationStyle = .fullScreen
                    self.present(addWithdrawView, animated: true, completion: nil)
                }
                            
            })
            alertViewObj.showAlert(controller: self)
        } else {
            self.btnPayBack.setTitle("NEXT".localized, for: .normal)

            self.selectedPlan = selectedPlan
            if selectedPlan == "TopUp"{
                //self.selectAmountTextView.text = wrapAmountWithCommaDecimal(key: productvalue) + "  " + productcurrency
                
                 self.selectAmountTextView.attributedText = returnAttributtedtextview(nameFont: "Helvetica Bold",textOne: "\(wrapAmountWithCommaDecimal(key:productvalue))", textTwo: "  " + productcurrency,textThree: "", textfourth: "", sizeOne: 17, sizeTwo: 12, color: .black)
                
                self.othercurrencyLabel.text = wrapAmountWithCommaDecimal(key: MMKAmount )//retailprice //+ " USD "
                self.plantypeLabel.text = "Top-Up Units".localized
                self.imgViewService.image = UIImage(named: "top_up_overseas")
            }
            else if selectedPlan == "DataPlan"{
                let productname = productname
               // let productshortdesc = productdescription
                let finalString = productname //+ "\n" + productshortdesc
                self.selectAmountTextView.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: finalString, textTwo: "",textThree: "", textfourth: "", sizeOne: 17, sizeTwo: 12, color: .black)
                self.othercurrencyLabel.text = wrapAmountWithCommaDecimal(key: MMKAmount )//retailprice //+ " USD "
                self.plantypeLabel.text = "Data Plan".localized
                self.imgViewService.image = UIImage(named: "data_plan_overseas")
            } else if selectedPlan == "SpecialOffers"{
                self.selectAmountTextView.text = ""
                self.othercurrencyLabel.text = wrapAmountWithCommaDecimal(key: MMKAmount )//retailprice //+ " USD "
                self.plantypeLabel.text = "Special Offer".localized
                self.imgViewService.image = UIImage(named: "special_offer_overseas")
            }
            
            self.productId = productId
            self.productName = productName
            self.MMKAmount = MMKAmount
            self.retailprice = retailprice
            self.wholesaleprice = wholesaleprice
            self.productcurrency = productcurrency
            self.productvalue = productvalue
            self.exchangerateusd = exchangerateusd
            
           // self.selectAmount.borderStyle = .none
            amountViewHeightConstraint.constant = 70
           // amountTFtrailingConstraint.constant = 55
         //   amountTFTopConstraint.constant = 4
        //    planTypeHeightConstraint.constant = 21
            arrowimage.isHidden = false
            
            plantypeLabel.isHidden = false
            othercurrencyLabel.isHidden = false
            mmkLabel.isHidden = false
            dividerLabel.isHidden = false
            mmkView.isHidden = false
            
            self.btnPayBack.setTitle("NEXT".localized, for: .normal)
        }
       
       // self.internationalDataPlanListModel = internationalDataPlanListModel
   
    }
  
}

//MARK: - CountryLeftViewDelegate
extension InternationalMainVC: CountryLeftViewDelegate {
    func openAction(type: ButtonType) {
        self.view.endEditing(true)
        switch type {
        case .countryView:
            break
        case .contactView:
            self.newActionSheet()
           break
        }
    }
    
    func clearActionType() {
        self.mobileNumber.becomeFirstResponder()
        usernameView.isHidden = true
        userviewTopConstraint.constant = 0
        userviewHeightConstraint.constant = 0
        self.confirmcountrycodeLabel.text = self.mobilecountrycodeLabel.text
        self.confirmcountrycodeLabel.isHidden = false
        self.confmobileLeftImage.image = UIImage(named:"topup_number")
        self.confirmMobileNumber.text = ""
        self.mobileNumber.text = ""
    //    self.confirmMobileNumber.isHidden = true
    //    self.confirmfieldhide = ""
      //  self.confirmmobileLabel.text = "Confirm Mobile Number".localized
        self.phoneuiTopConstraint.constant = 2
        self.confirmuiView.isHidden = true
    //    self.phoneuiView.layer.cornerRadius = 8.0
        self.selectOperator.isHidden = true
        self.selectserivceImage.isHidden = true
        self.selectAmountTextView.isHidden = true
        self.amountView.isHidden = true
        self.lblOperatorText.isHidden = true
        self.mmkView.isHidden = true
        self.operatoruiView.isHidden = true
        self.selectAmountTextView.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: "Select Service".localized, textTwo: "",textThree: "", textfourth: "", sizeOne: 17, sizeTwo: 11, color: .lightGray)
       // self.selectAmountTextView.text = "Select Service".localized
        self.plantypeLabel.text = "Service".localized
        self.imgViewService.image = UIImage(named: "service_overseas")
        self.selectOperator.text = ""
        self.mobileNumberRightView.hideClearButton(hide: true)
        self.mobileNumber.becomeFirstResponder()
    }
}

//MARK: - PTMultiSelectionDelegate
extension InternationalMainVC: PTMultiSelectionDelegate {
    func didSelectOption(option: PTMultiSelectionOption) {
        switch option {
        case .favorite:
//            let nav = openFavoriteFromNavigation(self, withFavorite: .topup, selectionType: false,  multiPay: true, payToCount: MultiTopupArray.main.controllers.count - 1)
//            self.navigationController?.present(nav, animated: true, completion: nil)
        
        break
        case .contact:
            
            let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: true)
         //   let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false)
            self.navigationController?.present(nav, animated: true, completion: nil)
        case .scanQR:
            break
        }
    }
    
//    func openFavoriteFromNavigation(_ delegate: FavoriteSelectionDelegate, withFavorite type: FavCaseSelection, selectionType: Bool = false, modelPayTo : PayToUIViewController? = nil, multiPay: Bool? = false, payToCount: Int? = 0) -> UINavigationController {
//        let navigation = UIStoryboard(name: "Favorite", bundle: nil).instantiateViewController(withIdentifier: "favoriteNav") as! UINavigationController
//        navigation.modalPresentationStyle = .fullScreen
//        for controllers in navigation.viewControllers {
//            if let fav = controllers as? FavoriteViewController {
//                fav.isFromModules  = true
//                fav.directSegue    = type
//                fav.moduleDelegate = delegate
//                fav.singleSelection = selectionType
//                fav.modelPayTo = modelPayTo
//                fav.multiPay = multiPay ?? false
//                fav.payToCount = payToCount ?? 0
//            }
//        }
//        return navigation
//    }
    
}

//MARK: - FavoriteSelectionDelegate
extension InternationalMainVC: FavoriteSelectionDelegate {
    func selectedFavoriteObject(obj: FavModel) {
        self.fromFavorite = "Fav"
        self.btnPayBack.setTitle("BACK".localized, for: .normal)
        let contact = ContactPicker.init(object: obj)
        self.contact(ContactPickersPicker(), didSelectContact: contact)
    }
}

class InternationalMobileRightViewTextField: SearchTextFieldTopUp {
    var cnf = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
       super.init(coder: coder)
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 0, y: 3, width:  58 , height: 44)
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: (cnf) ? (self.frame.size.width - 82) : (self.frame.size.width - 122), y: 0, width: (cnf) ? 82 : 122 , height: 50)
    }
    override func closestPosition(to point: CGPoint) -> UITextPosition? {
              let beginning = self.beginningOfDocument
              let end = self.position(from: beginning, offset: self.text?.count ?? 0)
              return end
          }
    
//    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
//         if action == #selector(paste(_:)) || action == #selector(cut(_:)) || action == #selector(copy(_:)) || action == #selector(select(_:)) || action == #selector(selectAll(_:)) {
//             return false
//         }
//         return super.canPerformAction(action, withSender: sender)
//     }
    
    override public func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
    
}
