//
//  InternationalReceiptVC.swift
//  OK
//
//  Created by OK$ on 27/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

class InternationalReceiptVC: OKBaseController {

    //PDF update
    @IBOutlet var bgView:UIView!
    var pdfview = UIView()
    var listTitleValue : [Dictionary<String,String>]?
    var yAxis : CGFloat = 0.0
    
    //
    @IBOutlet weak var topUpUnitheight: NSLayoutConstraint!
    @IBOutlet weak var topupDataHeight: NSLayoutConstraint!
    var navigation: UINavigationController?

    @IBOutlet var navigationTitleLabel: UILabel!{
        didSet {
            navigationTitleLabel.text = "Receipt".localized
            navigationTitleLabel.font = UIFont(name: appFont, size: 18.0)
        }
    }
    
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var btnAddContact: UIButton!
    @IBOutlet var paymentSuccessLabel: UILabel!
    {
        didSet {
            paymentSuccessLabel.font = UitilityClass.getZwagiFontWithSize(size: 17.0)
            paymentSuccessLabel.text = "Top-Up Successful".localized
        }
    }
    
    @IBOutlet weak var lblSavedGallery: UILabel!{
        didSet {
            lblSavedGallery.text = "Receipt already saved in gallery".localized
            lblSavedGallery.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var userdetailsView: UIView!
    
    //New Update
    @IBOutlet var headerAmountValue: UILabel!
    @IBOutlet var payerOKLbl: UILabel!{
           didSet {
            //payerOKLbl.text = "Payer OK$".localized
            payerOKLbl.text = "Payer OK$".localized
            
            //payerOKLbl.font = UIFont(name: appFont, size: 17.0)
           }
       }
    @IBOutlet var payerOKNameValue: UILabel!
    @IBOutlet var payerOKNumberValue: UILabel!
   
    @IBOutlet var operatorLabel: UILabel!{
               didSet {
                   operatorLabel.text = "Operator".localized
                   //operatorLabel.font = UIFont(name: appFont, size: 17.0)
               }
           }
     @IBOutlet var operatorValue: UILabel!
    @IBOutlet var topupNumberLabel: UILabel!{
                  didSet {
                      topupNumberLabel.text = "Mobile Number".localized
                      //topupNumberLabel.font = UIFont(name: appFont, size: 17.0)
                  }
              }
    @IBOutlet var topupNumberValue: UILabel!
    @IBOutlet var topupUnitLabel: UILabel!{
        didSet {
            topupUnitLabel.text = "Top-Up Units".localized
            //topupUnitLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var topupUnitValue: UILabel!
    @IBOutlet var topupUnitType: UILabel!

    @IBOutlet var txIDLabel: UILabel!{
        didSet {
            txIDLabel.text = "OK$ Txn ID".localized
            //txIDLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var txIDValue: UILabel!
    
   @IBOutlet var cashbackLabel: UILabel!{
        didSet {
            cashbackLabel.text = "Cash Back".localized
            //cashbackLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var aamountmmkLabel: UILabel!{
        didSet {
            aamountmmkLabel.text = "Amount".localized
            //aamountmmkLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var loyalitypoints: UILabel!{
        didSet {
            loyalitypoints.text = "Loyalty Points".localized
            //loyalitypoints.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var otherCurrencyLabel: UILabel!
    @IBOutlet var bottomView: UIView!
    
    @IBOutlet var cashBackValue: UILabel!
    @IBOutlet var amountValue: UILabel!
    @IBOutlet var loyalityPointsValue: UILabel!
    
    @IBOutlet var okDollarRemaingBalanceLabel: UILabel!
    
    @IBOutlet  var viewReceipt: UIView!
    
    
    @IBOutlet weak var constraintViewUserDetails: NSLayoutConstraint!
    @IBOutlet weak var constraintViewLoyality: NSLayoutConstraint!
    @IBOutlet weak var constraintViewCashBack: NSLayoutConstraint!
    @IBOutlet weak var constraintViewTopUp: NSLayoutConstraint!

    @IBOutlet var loyalityTopConstraint: NSLayoutConstraint!
    
    @IBOutlet var contactView: UIView!
    @IBOutlet var namefromLabel: UILabel!
    @IBOutlet var namedisplayLabel: UILabel!
    @IBOutlet var contactViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var contactViewTopContraint: NSLayoutConstraint!
    //
    @IBOutlet weak var lblReceiptTitle: UILabel!{
        didSet {
            lblReceiptTitle.text = "International Top-Up".localized
            lblReceiptTitle.font = UIFont(name: appFont, size: 17.0)
        }
    }
    var strTime = ""
    var strDate = ""
    var strMobileNo = ""
    var strTelOperator = ""
    var strTxID = ""
    var strAmount = ""
    var strAmountInMMK = ""
    var strCashBack = ""
    var strLoyalityPoints = ""
    var strOKBalance = ""
    var selectedcountrycurrency = ""
    var strTopupUnit = ""
    var strTopupData = ""
    var strTopUpPlan = ""//To check offerplan selection
    var DestinationCurrency = ""//Topup destination currency
    var DestinationNumber = ""//Topup destination number
    var DestinationCountry = ""//Topup destination country
    var strStatus = ""//Transaction Status

    var payermobilenumber = ""
    
    var boolLoyalityStatus = false
    var boolCashBackStatus = false
    
    var isInFav = false
    var isInContact = false
    
    var strBeneficiaryName: String?
    var fromFavorite = ""
    var strErrorMsg = "Success"


    // MARK: - View Life Cycle

    override func viewDidAppear(_ animated: Bool)  {
        super.viewDidAppear(true)
//        let image = captureScreen(view: bgView)
//        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
       
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bgView.frame =  CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0)
        // Do any additional setup after loading the view.
        if #available(iOS 13, *) {
            UIApplication.statusBarBackgroundColor =  kYellowColor
        } else {
            if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                statusbar.backgroundColor = kYellowColor
            }
        }
    
        let saveDBNo = strMobileNo.components(separatedBy: ")")
        let val = saveDBNo[1].replacingOccurrences(of: " ", with: "")
        isInContact = ContactManager().agentgentCodePresentinContactsLoc(number: val)
        isInFav = favoriteManager.agentgentCodePresentinFavoriteLoc(number: val)
          
//        isInFav = checkForFavorites(strMobileNo, type: "OVERSEASTOPUP").isInFavorite
//        isInContact = checkForContact(strMobileNo).isInContact
        
        if fromFavorite == "Fav" {
            btnFavorite.isHidden = true
            self.contactView.isHidden = false
            self.contactViewHeightConstraint.constant = 48
            self.contactViewTopContraint.constant = 1
            self.namefromLabel.text = "Name From Favorite".localized
            self.namedisplayLabel.text = strBeneficiaryName
        }
        if fromFavorite == "Con" {
            btnAddContact.isHidden = true
            self.contactView.isHidden = false
            self.contactViewHeightConstraint.constant = 48
            self.contactViewTopContraint.constant = 1
            self.namefromLabel.text = "Name From Contact".localized
            self.namedisplayLabel.text = strBeneficiaryName
        }
        if fromFavorite == "Unknown"{
            self.contactView.isHidden = true
            self.contactViewHeightConstraint.constant = 0
            self.contactViewTopContraint.constant = 0
        }
    
        self.addFavUI(isInFav: isInFav)
        self.addContactUI(isInContact: isInContact)
        let model = PaymentTransactions.init(val, type: "International Top-Up", name: "", amount: strAmountInMMK, transID: "", bonus: "0.0")
        PaymentVerificationManager.storeTransactions(model: model)
        
        if strErrorMsg == "Success" {
            paymentSuccessLabel.text = "Top-Up Successful".localized
            strStatus = "Successful"
        } else {
            paymentSuccessLabel.text = "Top-Up In-Progress".localized
            strStatus = "In-Progress"
        }
        
        setReceiptUI()
    }
  
    func setReceiptUI() {
        
        let amountNew = wrapAmountWithCommaDecimal(key: strAmountInMMK)//wrapAmountWithCommaDecimal(key: totalamount)
               let finalAmount = amountNew.components(separatedBy: ".")
               
               if finalAmount.count>0{
                   if finalAmount.indices.contains(0) && finalAmount.indices.contains(1){
                       headerAmountValue.attributedText = returnAttributtedtextview(nameFont: "HelveticaNeue-Bold",textOne: finalAmount[0], textTwo: ".\(finalAmount[1]) MMK" ,textThree: "", textfourth: "", sizeOne: 17, sizeTwo: 13, color: .red)
                       
                   }else{
                    var newValue = ""
                    if finalAmount.indices.contains(0){
                        newValue = finalAmount[0]
                    }else{
                        newValue = wrapAmountWithCommaDecimal(key: strAmountInMMK)
                    }
                       headerAmountValue.attributedText = returnAttributtedtextview(nameFont: "HelveticaNeue-Bold",textOne: newValue, textTwo: " MMK",textThree: "", textfourth: "", sizeOne: 17, sizeTwo: 13, color: .red)
                   }
        }

        //headerAmountValue.text = strAmountInMMK + " MMK"
        payerOKNameValue.text = UserModel.shared.name
        payerOKNumberValue.text = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "(+95) 0")
        
        payermobilenumber = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "(+95) 0")
        
        let date = Date()
        let dateFormater = DateFormatter()
        dateFormater.calendar = Calendar(identifier: .gregorian)
        dateFormater.dateFormat = "EEE, dd-MMM-yyyy%20hh:mm a"
        dateFormater.amSymbol = "AM"
        dateFormater.pmSymbol = "PM"
        let currentDate = dateFormater.string(from: date)
        println_debug(currentDate)
        let myStringArr = currentDate.components(separatedBy: "%20")
        dateLabel.text = myStringArr [0]
        timeLabel.text = myStringArr [1]
        
        operatorValue.text = strTelOperator
        topupNumberValue.text = strMobileNo
        
        if strTopUpPlan == "TopUp" {
            topupUnitLabel.text = "Top-Up Units".localized
            topupUnitValue.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: wrapAmountWithCommaDecimal(key:strTopupUnit), textTwo: " " + DestinationCurrency,textThree: "", textfourth: "", sizeOne: 17, sizeTwo: 11, color: .black)
        } else if strTopUpPlan == "DataPlan" {
            topupUnitLabel.text = "Top-Up Data".localized
            topupUnitValue.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: wrapAmountWithCommaDecimal(key:strTopupData), textTwo: "",textThree: "", textfourth: "", sizeOne: 17, sizeTwo: 11, color: .black)
            //topupUnitValue.attributedText = NSMutableAttributedString(string: strTopupData)
        } else {
            topupUnitValue.attributedText = NSMutableAttributedString(string: "-")
        }

        //otherCurrencyLabel.text = DestinationCurrency
        txIDValue.text = strTxID
//        amountValue.text = wrapAmountWithCommaDecimal(key: strAmountInMMK) + " MMK"
//        cashBackValue.text = strCashBack.replacingOccurrences(of: ".00", with: "") + " MMK"
        
        amountValue.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: wrapAmountWithCommaDecimal(key: strAmountInMMK), textTwo: " MMK",textThree: "", textfourth: "", sizeOne: 17, sizeTwo: 11, color: .black)
        cashBackValue.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: wrapAmountWithCommaDecimal(key: strCashBack.replacingOccurrences(of: ".00", with: "")), textTwo: " MMK",textThree: "", textfourth: "", sizeOne: 17, sizeTwo: 11, color: .black)
        loyalityPointsValue.text = strLoyalityPoints
        
        let okBalance = strOKBalance.components(separatedBy: ".")
        if okBalance.count>0{
            if okBalance.indices.contains(0) && okBalance.indices.contains(1){
                okDollarRemaingBalanceLabel.attributedText =   returnAttributtedtextview(nameFont: appFont,textOne: wrapAmountWithCommaDecimal(key:okBalance[0]), textTwo: "." + okBalance[1],textThree: " MMK", textfourth: "OK$ Balance : ".localized, sizeOne: 18, sizeTwo: 15, color: UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0))
            }else{
                
                okDollarRemaingBalanceLabel.attributedText =    returnAttributtedtextview(nameFont: appFont,textOne: wrapAmountWithCommaDecimal(key:okBalance[0]), textTwo: "",textThree: " MMK", textfourth: "OK$ Balance : ".localized, sizeOne: 18, sizeTwo: 15, color: UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0))
            }
            
        }
        
      //  okDollarRemaingBalanceLabel.text = "OK$ Balance :".localized + strOKBalance + " MMK"
        //amountotherCurrencyLabel.text = "Amount".localized + "(" + selectedcountrycurrency + ")"

        if strCashBack.isEmpty || strCashBack.count == 0 || strCashBack == "0" {
            constraintViewCashBack.constant = 0
            //loyalityTopConstraint.constant = 0
            boolCashBackStatus = true//Hide
        } else {
            boolCashBackStatus = false//Display
            constraintViewCashBack.constant = 48
            //loyalityTopConstraint.constant = 1
        }
        
        if strLoyalityPoints.isEmpty || strLoyalityPoints.count == 0 || strLoyalityPoints == "0"{
            boolLoyalityStatus = true
            constraintViewLoyality.constant = 0
        } else {
            boolLoyalityStatus = false
            constraintViewLoyality.constant = 48
        }
        
        if boolLoyalityStatus || boolCashBackStatus {//Any one will be hidden
            constraintViewUserDetails.constant = 416

            if self.fromFavorite == "Fav" ||  self.fromFavorite == "Con" {
                constraintViewUserDetails.constant = 464
            }
        }

        if boolLoyalityStatus && boolCashBackStatus {//Both will be hidden
            constraintViewLoyality.constant = 0
            constraintViewCashBack.constant = 0
            constraintViewUserDetails.constant = 368
            
            if self.fromFavorite == "Fav" ||  self.fromFavorite == "Con" {
                constraintViewUserDetails.constant = 416
            }
        }

        if !boolLoyalityStatus && !boolCashBackStatus {//All will show
            constraintViewLoyality.constant = 48
            constraintViewCashBack.constant = 48
            constraintViewUserDetails.constant = 464
            if self.fromFavorite == "Fav" ||  self.fromFavorite == "Con" {
                constraintViewUserDetails.constant = 512
            }
        }
        
        //Generate PDF
        pdfview.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30)
        self.pdfview.backgroundColor = #colorLiteral(red: 0.9285323024, green: 0.9334669709, blue: 0.9376094937, alpha: 1)
        self.bgView.backgroundColor = .white
       
        //Create PDF
        self.createPDF()
         DispatchQueue.main.async {
            self.saveToGallery()
        }
    }
    
    func saveToGallery() {
        let image = captureScreen(view: self.bgView)
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    //MARK:- Custom Methods Action
    @IBAction func homeButtonClick(_ sender: Any) {               
        performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
    }
    
    @IBAction func addContactAction(_ sender: Any) {
        println_debug("addContactAction")
        self.addContact()
    }
    
    @IBAction func addFavoriteAction(_ sender: Any) {
        println_debug("addFavoriteAction")
        self.addFavorite()
    }
    
    @IBAction func shareButtonClick(_ sender: Any) {
        self.shareQRWithDetail()
    }
    
    func addFavUI(isInFav: Bool) {
            if isInFav {
                btnFavorite.setImage(UIImage(named:"overseas_heart_w"), for: .normal)
            } else {
                btnFavorite.setImage(UIImage(named:"overseas_heart"), for: .normal)
            }
    }
    
    func addContactUI(isInContact: Bool) {
            if isInContact {
                btnAddContact.setImage(UIImage(named:"overseas_contact_w"), for: .normal)
            } else {
                btnAddContact.setImage(UIImage(named:"overseas_contact"), for: .normal)
            }
        
    }
    
    
    func addFavorite() {
        if isInFav {
            PaytoConstants.alert.showToast(msg: "Contact already added in Favorites".localized)
            
        } else {
            self.favorite_recipt()
        }
    }
    
    fileprivate  func favorite_recipt() {
        
        //let favorite = FavoriteDBManager.init()
        //let contacts = favorite.fetchRecordsForFavoriteContactsEntity()
      
        let number = DestinationCountry.replacingOccurrences(of: "+", with: "00") + DestinationNumber
            let name   = strBeneficiaryName ?? "Unknown"
            let amount = strAmountInMMK//dict.safeValueForKey("amount") as? String
            let type = "OVERSEASTOPUP"
           
        let vc = self.addFavoriteController(withName: name, favNum: number , type: type, amount: amount)
            if let vcs = vc {
                vcs.delegate = self
                vcs.modalPresentationStyle = .overCurrentContext
               // vcs.modalPresentationStyle = .fullScreen
                self.present(vcs, animated: true, completion: nil)
            }
    }
    
    func addContact() {
        if isInContact {
            PaytoConstants.alert.showToast(msg: "Contact Already Added".localized)
        } else {
            self.addContact_recipt()
        }
    }
       
    fileprivate  func addContact_recipt() {

        let store     = CNContactStore()
            let contact   = CNMutableContact()
            
        let number = DestinationCountry.replacingOccurrences(of: "+", with: "00") + DestinationNumber

        let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue: number))
            contact.phoneNumbers = [homePhone]
            contact.givenName = strBeneficiaryName ?? "Unknown"
           
            let controller = CNContactViewController.init(forNewContact: contact)
            controller.contactStore = store
            controller.delegate     = self
            controller.title        = "Add Contact".localized
            let navControler = UINavigationController.init(rootViewController: controller)
            navControler.modalPresentationStyle = .overCurrentContext
            self.present(navControler, animated: true, completion: nil)
    }
      
    func shareQRWithDetail() {
            let image = captureScreen(view: bgView)
            let imageToShare = [image]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            DispatchQueue.main.async {
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
    
        @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
            if let error = error {
                alertViewObj.wrapAlert(title: "Error".localized, body: error.localizedDescription, img: nil)
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                alertViewObj.showAlert(controller: self)
            } else {
                println_debug("Your altered image has been saved to your photos.")
            }
        }
}

//MARK:- InternationalReceiptDelegate
extension InternationalReceiptVC: PTFavoriteActionDelegate {
    func favoriteAdded() {
//        if isInFavArray.count >= currentIndex {
//        isInFavArray[currentIndex-1] = true
//        addFavUI(isInFav: true)
        
        //Change Button color
        self.isInFav = true
        addFavUI(isInFav: true)
    }
}

//MARK: - CNContactViewControllerDelegate
extension InternationalReceiptVC: CNContactViewControllerDelegate {

    func changeCancelImplementation() {
        let originalSelector = Selector(("editCancel:"))
        let swizzledSelector = #selector(CNContactViewController.cancelHack)
        if let originalMethod = class_getInstanceMethod(object_getClass(CNContactViewController()), originalSelector),
           let swizzledMethod = class_getInstanceMethod(object_getClass(CNContactViewController()), swizzledSelector) {
            method_exchangeImplementations(originalMethod, swizzledMethod)
        }
    }
    
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        if let newContact = contact {
            
//            if isInContactArray.count == 1{
//                if !isInContactArray[currentIndex]{
//                    isInContactArray[currentIndex] = true
//                }
//            }else{
//                for i in 0..<isInContactArray.count{
//                    if !isInContactArray[i] {
//                        isInContactArray[i] = true
//                        break
//                    }
//                }
//            }
            //addContactUI(isInContact: true)
            //UPdate button color with selected
            self.isInContact = true
            addContactUI(isInContact: true)
            okContacts.append(newContact)
            
            //if isInContactArray.count >= currentIndex {
             //   isInContactArray[currentIndex-1] = true
                
          //  }
        }
        viewController.delegate = nil
        self.dismiss(animated: true, completion: nil)
        //self.navigationController?.popViewController(animated: true)
    }
}
