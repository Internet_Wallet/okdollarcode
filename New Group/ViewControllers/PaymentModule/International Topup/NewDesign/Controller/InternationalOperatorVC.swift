//
//  InternationalOperatorVC.swift
//  OK
//
//  Created by OK$ on 27/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

protocol InternationalOperatorDelegate {
    func sendInternationalOperator(operatorID: String, operatorName: String)
}

class InternationalOperatorVC: OKBaseController {

    var delegate: InternationalOperatorDelegate?
    var strCountryID: String?
    @IBOutlet weak var tblOperator : UITableView!
    var arrOperatorList : [InternationalOperatorListModel] = []
  //  var arrFilterList : [InternationalOperatorListModel] = []
    @IBOutlet weak var searchBar: UISearchBar!{
        didSet {
            self.searchBar.placeholder = "Search".localized
        }
    }
    @IBOutlet weak var titleLabel: UILabel!{
        didSet {
            titleLabel.text = "International Top-Up".localized
            titleLabel.font = UIFont(name: appFont, size: 18.0)
        }
    }
    @IBOutlet weak var btnBack: UIButton!{
        didSet {
            btnBack.setTitle("BACK".localized, for: .normal)
            if let myFont = UIFont(name: appFont, size: appButtonSize) {
                btnBack.titleLabel?.font =  myFont
            }
        }
    }
    @IBOutlet weak var noRecordsLabel : UILabel! {
          didSet {
            noRecordsLabel.isHidden = true
            noRecordsLabel.text = "No Record Found".localized
            noRecordsLabel.font = UIFont(name: appFont, size: 18.0)
          }
      }
    
    @IBOutlet var buttonBottomConstraint: NSLayoutConstraint!
    var KeyboardSize : CGFloat = 0.0
    
    var operatorlistArr : [[String:Any]] = [[:]]
    var arrFilterList : [[String:Any]] = [[:]]
    
    // MARK: - View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        noRecordsLabel.isHidden = true

        println_debug(strCountryID)
        // Do any additional setup after loading the view.
        tblOperator.tableFooterView = UIView(frame: CGRect.zero)
        searchBar.setImage(UIImage(named: "bluesearch"), for: .search, state: .normal)
        searchBar.placeholder = "Search".localized
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
                //searchTextField.textColor = UIColor.black
            }
        }
        self.initialWebCall()
        
        if #available(iOS 13, *) {
                   UIApplication.statusBarBackgroundColor =  kYellowColor
               } else {
                   if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                       statusbar.backgroundColor = kYellowColor
                   }
               }
        self.view.layoutIfNeeded()
        subscribeToShowKeyboardNotifications()
    }
    override func viewWillDisappear(_ animated: Bool){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification,object : nil)
        self.view.endEditing(true)
    }
    
    @IBAction func onClickBackBtn(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
          }
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            //            self.adImageConstraint.constant = 0 - keyboardHeight
            
        if device.type == .iPhoneX || device.type == .iPhoneXR || device.type == .iPhoneXS || device.type == .iPhoneXSMax {
                    self.buttonBottomConstraint.constant = keyboardHeight-10
                }
            else {
                    self.buttonBottomConstraint.constant = keyboardHeight
                }
            
        })
    }
    @objc func keyboardWillHide(_ notification: Notification) {
        //        self.adImageConstraint.constant = 0
        self.buttonBottomConstraint.constant = 0
      
    }
    func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

}

extension InternationalOperatorVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 40))
        //headerView.backgroundColor = UIColor.colorWithRedValue(redValue: 244, greenValue: 244, blueValue: 245, alpha: 1)
        let headerLabel = UILabel(frame: CGRect(x: 12, y: 5, width: screenWidth-40, height: 30))
        headerLabel.text = "     Select Operator".localized
        headerLabel.textColor = kDarkBlueColor
        
        if #available(iOS 13.0, *) {
            headerView.backgroundColor = .systemGray5
        } else {
            // Fallback on earlier versions
        }
        headerLabel.font = UIFont.init(name: appFont, size: 16)
        headerView.addSubview(headerLabel)
        return headerView
    }
       
       func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           return 40
       }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchBar.text!.count > 0 {
            return arrFilterList.count
              }
        return operatorlistArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: "InternationalOperatorCell"), for: indexPath) as? InternationalOperatorCell
            if searchBar.text!.count > 0 {
                let dict = arrFilterList[indexPath.row]
                cell?.lblOperatorName.text =  dict["OperatorName"] .safelyWrappingString()
            } else {
                let dict = operatorlistArr[indexPath.row]
                
                cell?.lblOperatorName.text =  dict["OperatorName"].safelyWrappingString() //dict.operatorName
            }
        
        if indexPath.row % 2 == 1 {
            if #available(iOS 13.0, *) {
                cell?.contentView.backgroundColor = .systemGray5
            } else {
                // Fallback on earlier versions
            }
        } else {
            cell?.contentView.backgroundColor = .white
        }
            
            cell?.selectionStyle = .none
            return cell!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchBar.text!.count > 0 {
            let dict = arrFilterList[indexPath.row]
            self.delegate?.sendInternationalOperator(operatorID:dict["OperatorCode"] as! String ,operatorName:dict["OperatorName"] as! String)
            
        } else {
            let dict = operatorlistArr[indexPath.row]
            self.delegate?.sendInternationalOperator(operatorID:dict["OperatorCode"] as! String ,operatorName:dict["OperatorName"] as! String)
        }
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - UISearchDisplayDelegate
extension InternationalOperatorVC: UISearchBarDelegate {
    
    // MARK: Methods
    fileprivate func filter(_ searchText: String) -> [[String:Any]] {
           arrFilterList.removeAll()
           
        operatorlistArr.forEach { (section) -> () in
               //section.country.forEach({ (country) -> () in
            let dictText = section["OperatorName"] as? String ?? ""
            if dictText.count >= searchText.count {
                       //let result = dictText.compare(searchText, options: [.caseInsensitive], range: searchText.startIndex ..< searchText.endIndex)
                let resultFinal = dictText.lowercased().contains(find: searchText.lowercased())
                       if resultFinal {
                        arrFilterList.append(section)
                       }
                   }
               //})
           }
           
        return arrFilterList
       }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        subscribeToShowKeyboardNotifications()
        let countryArray = filter(searchText)
        if countryArray.count == 0 && searchBar.text?.count != 0{
            tblOperator.isHidden = true
            self.noRecordsLabel.isHidden = false
        } else {
            tblOperator.isHidden = false
            self.noRecordsLabel.isHidden = true
        }
        tblOperator.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
           searchBar.resignFirstResponder()
       }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

            if range.location == 0 && text == " " { return false }
            if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
                let updatedText = searchText.replacingCharacters(in: textRange, with: text)
                if updatedText.containsEmoji {
                    return false
                }
                if searchBar.text?.last == " " && text == " " {
                    return false
                }
                if updatedText.count > 26 {
                    return false
                }
                
                //applySearch(with: updatedText)
            }
            return true
        
        //OLD Search check
        
//        let text       = (searchBar.text! as NSString).replacingCharacters(in: range, with: text)
//        let textCount  = text.count
        //let char = text.cString(using: String.Encoding.utf8)!
        //let isBackSpace = strcmp(char, "\\b")//|| isBackSpace == -92 
        
//        let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: InternationalTopupConstants.acceptableCharacters.alphaNumeric).inverted
//        let filteredSet = text.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
        
//        if (textCount < 26) {
//            return true
//        }
//        return false
    }
    
}

extension InternationalOperatorVC{
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        self.KeyboardSize = (keyboardSize?.height)!
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize!.height + 45, right: 0.0)
        self.tblOperator.contentInset = contentInsets
        self.tblOperator.scrollIndicatorInsets = contentInsets
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if searchBar.self != nil{
            if (!aRect.contains((searchBar?.frame.origin)!)){
                self.tblOperator.scrollRectToVisible(searchBar!.frame, animated: true)
            }
        }
    }
    @objc func keyboardWillChangeHeight(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        self.KeyboardSize = (keyboardSize?.height)!
        //println_debug("Keyboard changed \(KeyboardSize,keyboardSize?.height))")
        //        classActionButton?.frame.origin.y = self.view.frame.height - KeyboardSize - 54
        if device.type == .iPhoneX || device.type == .iPhoneXR || device.type == .iPhoneXS || device.type == .iPhoneXSMax {
               self.buttonBottomConstraint.constant = KeyboardSize-10
           }
           else {
                 self.buttonBottomConstraint.constant = KeyboardSize
            }
        
       // bottomGetBillBtnConstraint.constant = KeyboardSize
        self.view.layoutIfNeeded()
    }
    
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.tblOperator.contentInset = contentInsets
        self.tblOperator.scrollIndicatorInsets = contentInsets
        //        classActionButton?.frame.origin.y = self.view.frame.height - 54
        buttonBottomConstraint.constant = 0
    }
}
