//
//  InternationalOffersVC.swift
//  OK
//
//  Created by OK$ on 27/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

protocol InternationalOffersDelegate {
    func sendInternationalOffersAmount(MMKAmount: String, retailprice: String, wholesaleprice: String, productcurrency: String, productvalue: String, exchangerateusd: String, selectedPlan: String, productname: String, productdescription: String, productId: String, productName: String)
}

class InternationalOffersVC: OKBaseController {

    @IBOutlet weak var viewOffersBottom: UIView!{
        didSet{
            viewOffersBottom.backgroundColor = .clear
        }
    }
    @IBOutlet weak var viewDataPlanBottom: UIView!{
        didSet{
            viewDataPlanBottom.backgroundColor = .clear
        }
    }
    @IBOutlet weak var viewTopUpBottom: UIView!{
        didSet{
            viewTopUpBottom.backgroundColor = .systemBlue
        }
    }
    @IBOutlet weak var btnSpecialOffer: UIButton!{
        didSet {
            
            if appFont == "Myanmar3"{
                self.btnSpecialOffer.titleLabel?.font = UIFont(name: appFont, size: 12.0)
            }else{
                self.btnSpecialOffer.titleLabel?.font = UIFont(name: appFont, size: 15.0)
            }
            
            btnSpecialOffer.setTitle("SPECIAL OFFERS".localized, for: .normal)
           
        }
    }
    @IBOutlet weak var btnDataPlan: UIButton!{
        didSet {
            
            if appFont == "Myanmar3"{
                self.btnDataPlan.titleLabel?.font = UIFont(name: appFont, size: 12.0)
            }else{
                self.btnDataPlan.titleLabel?.font = UIFont(name: appFont, size: 15.0)
            }
            
            btnDataPlan.setTitle("DATA PLAN".localized, for: .normal)
        
        }
    }
    @IBOutlet weak var btnTopUp: UIButton!{
        didSet {
            
            if appFont == "Myanmar3"{
                self.btnTopUp.titleLabel?.font = UIFont(name: appFont, size: 12.0)
            }else{
                self.btnTopUp.titleLabel?.font = UIFont(name: appFont, size: 15.0)
            }
            btnTopUp.setTitle("TOP-UP".localized, for: .normal)
            
        }
    }
    @IBOutlet weak var titleLabel: UILabel!{
        didSet {
            titleLabel.text = "International Top-Up".localized
            titleLabel.font = UIFont(name: appFont, size: 18.0)
        }
    }
    @IBOutlet weak var btnBack: UIButton!{
        didSet {
            btnBack.setTitle("BACK".localized, for: .normal)
            if let myFont = UIFont(name: appFont, size: 18) {
                btnBack.titleLabel?.font =  myFont
            }
        }
    }
    
    var isComingFromTopUP = true
    var isComingFromDataPlan = false
    var isComingFromSpecialOffers = false
   
    
    var delegate: InternationalOffersDelegate?
    var strCountryID: String?
    var strOperatorID: String?
   // var arrOfferList : [InternationalDataPlanListModel] = []
    @IBOutlet weak var txtRechargeAmount : UITextField!
    @IBOutlet weak var tblOffer : UITableView!
    @IBOutlet var topupView: UIView!
    @IBOutlet var dataplanView: UIView!
    @IBOutlet var specialoffersView: UIView!
    
    @IBOutlet var nodataLabel: UILabel!{
        didSet {
          nodataLabel.text = "No Record Found".localized
          nodataLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    var arrOfferList = [[String : Any]]()
    var arrDataPlanList = [[String : Any]]()
    var statusTopUpAPI = false
    var statusDataPlanAPI = false
    
    var selectedplan = "TopUp"
    var DestinationCurrency = ""
    var swipeCount = 0
    var mobilenumber = ""
    
   // var arrdataOfferList = [[String : Any]]()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let tap = UISwipeGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tap.direction = .left
        tblOffer.addGestureRecognizer(tap)
        
        let tapRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleTapRight(_:)))
        tapRight.direction = .right
        tblOffer.addGestureRecognizer(tapRight)
        
        
        let tapLeftView = UISwipeGestureRecognizer(target: self, action: #selector(self.handleTapLeftView(_:)))
        tapLeftView.direction = .left
        self.view.addGestureRecognizer(tapLeftView)
        
        let tapRightView = UISwipeGestureRecognizer(target: self, action: #selector(self.handleTapRightView(_:)))
        tapRightView.direction = .right
        self.view.addGestureRecognizer(tapRightView)
        
        
        self.initialWebCall()
        
        viewTopUpBottom.backgroundColor = kBlueColor
        viewDataPlanBottom.backgroundColor = .clear
        viewOffersBottom.backgroundColor = .clear
        btnTopUp.setTitleColor(kBlueColor, for: .normal)
        btnDataPlan.setTitleColor(.black, for: .normal)
        btnSpecialOffer.setTitleColor(.black, for: .normal)

        // Do any additional setup after loading the view.
        if #available(iOS 13, *) {
            UIApplication.statusBarBackgroundColor =  kYellowColor
        } else {
            if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                statusbar.backgroundColor = kYellowColor
            }
        }
        
        tblOffer.rowHeight = UITableView.automaticDimension
        tblOffer.estimatedRowHeight = 100.0
    }
    
    
    @objc func handleTapLeftView(_ sender: UISwipeGestureRecognizer? = nil) {
        
        
        //left swipe
        if isComingFromTopUP{
            isComingFromTopUP = false
            isComingFromDataPlan = true
            btnDataPlan.sendActions(for: .touchUpInside)
        }else if isComingFromDataPlan{
            isComingFromTopUP = false
            isComingFromDataPlan = false
            isComingFromSpecialOffers = true
            btnSpecialOffer.sendActions(for: .touchUpInside)
        }else{
            print("nothing")
        }
        
    }
    
    @objc func handleTapRightView(_ sender: UISwipeGestureRecognizer? = nil) {
        
        if isComingFromSpecialOffers{
            isComingFromTopUP = false
            isComingFromDataPlan = true
            isComingFromSpecialOffers = false
            btnDataPlan.sendActions(for: .touchUpInside)
        }else if isComingFromDataPlan{
            isComingFromTopUP = true
            isComingFromDataPlan = false
            isComingFromSpecialOffers = false
            btnTopUp.sendActions(for: .touchUpInside)
            
            //  btnSpecialOffer.sendActions(for: .touchUpInside)
        }else{
            print("nothing")
        }
        
    }
    
    @objc func handleTap(_ sender: UISwipeGestureRecognizer? = nil) {
        
        //left swipe
        if isComingFromTopUP{
            isComingFromTopUP = false
            isComingFromDataPlan = true
            btnDataPlan.sendActions(for: .touchUpInside)
        }else if isComingFromDataPlan{
            isComingFromTopUP = false
            isComingFromDataPlan = false
            isComingFromSpecialOffers = true
            btnSpecialOffer.sendActions(for: .touchUpInside)
        }else{
            print("nothing")
        }
        
    }
    
    @objc func handleTapRight(_ sender: UISwipeGestureRecognizer? = nil) {
       
        if isComingFromSpecialOffers{
               isComingFromTopUP = false
               isComingFromDataPlan = true
               isComingFromSpecialOffers = false
               btnDataPlan.sendActions(for: .touchUpInside)
           }else if isComingFromDataPlan{
               isComingFromTopUP = true
               isComingFromDataPlan = false
               isComingFromSpecialOffers = false
              btnTopUp.sendActions(for: .touchUpInside)
        
             //  btnSpecialOffer.sendActions(for: .touchUpInside)
           }else{
               print("nothing")
           }
    
    }
    
    @IBAction func onClickDoneBtn(_ sender: Any) {
        //self.delegate?.sendInternationalOffersAmount(offerAmount: txtRechargeAmount.text ?? "",)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func topupClick(_ sender: Any) {
        selectedplan = "TopUp"
        
        if arrOfferList.count > 0{
            self.tblOffer.isHidden = false
            tblOffer.reloadData()
        }else{
            self.initialWebCall()
        }
        
        viewTopUpBottom.backgroundColor = kBlueColor
        viewDataPlanBottom.backgroundColor = .clear
        viewOffersBottom.backgroundColor = .clear
        btnTopUp.setTitleColor(kBlueColor, for: .normal)
        btnDataPlan.setTitleColor(.black, for: .normal)
        btnSpecialOffer.setTitleColor(.black, for: .normal)
        
        isComingFromTopUP = true
        isComingFromDataPlan = false
        isComingFromSpecialOffers = false
        
    }
    
    @IBAction func dataplanClick(_ sender: Any) {
        
        selectedplan = "DataPlan"
       
        
        if arrDataPlanList.count > 0 {
            self.tblOffer.isHidden = false
            tblOffer.reloadData()
        } else {
            if !statusDataPlanAPI {
                self.DataWebCall()
            } else {
                self.nodataLabel.text =  "No Record Found".localized
                self.tblOffer.isHidden = true
                //self.tblOffer.reloadData()
            }
        }
        
        viewTopUpBottom.backgroundColor = .clear
        viewDataPlanBottom.backgroundColor = kBlueColor
        viewOffersBottom.backgroundColor = .clear
        
        btnTopUp.setTitleColor(.black, for: .normal)
        btnDataPlan.setTitleColor(kBlueColor, for: .normal)
        btnSpecialOffer.setTitleColor(.black, for: .normal)
        
        isComingFromTopUP = false
        isComingFromDataPlan = true
        isComingFromSpecialOffers = false
    }
    
    @IBAction func specialofferClick(_ sender: Any) {
        selectedplan = "SpecialOffer"
        self.nodataLabel.text = "Coming Soon!"
        self.tblOffer.isHidden = true
        
        viewTopUpBottom.backgroundColor = .clear
        viewDataPlanBottom.backgroundColor = .clear
        viewOffersBottom.backgroundColor = kBlueColor
        
        btnTopUp.setTitleColor(.black, for: .normal)
        btnDataPlan.setTitleColor(.black, for: .normal)
        btnSpecialOffer.setTitleColor(kBlueColor, for: .normal)
        
        isComingFromTopUP = false
        isComingFromDataPlan = false
        isComingFromSpecialOffers = true
    }
    
}


extension InternationalOffersVC : UITableViewDelegate, UITableViewDataSource {
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       let identifier = "InternationalOfferHeaderCell"
       var cellView: InternationalOfferHeaderCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? InternationalOfferHeaderCell
       if cellView == nil {
         tableView.register (UINib(nibName: "InternationalOfferHeaderCell", bundle: nil), forCellReuseIdentifier: identifier)
         cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? InternationalOfferHeaderCell
       }
       
        if  selectedplan == "TopUp"{
            cellView.viewLeftwidthConstraint.constant = 100
            let text = "UNITS"
            cellView.lblTitleLeft.attributedText = NSAttributedString(string: text, attributes: [.kern: 3.12])
        }
        else {
            cellView.viewLeftwidthConstraint.constant = 160
            let text = "DESCRIPTION".localized
            cellView.lblTitleLeft.attributedText = NSAttributedString(string: text, attributes: [.kern: 3.12])
        }
        
        cellView.viewUnitDesc.layer.cornerRadius = 15.0
        cellView.viewMMK.layer.cornerRadius = 15.0
        cellView.viewUnitDesc.layer.borderColor = kBlueColor.cgColor
        cellView.viewMMK.layer.borderColor = kBlueColor.cgColor
        cellView.viewUnitDesc.layer.borderWidth = 2.0
        cellView.viewMMK.layer.borderWidth = 2.0
        
        cellView.viewUnitDesc.layer.masksToBounds = true
        cellView.viewMMK.layer.masksToBounds = true
        
        let text = "MMK"
        cellView.lblTitleRight.attributedText = NSAttributedString(string: text, attributes: [.kern: 3.12])
        
       return cellView
     }

       func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           return 40
       }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  selectedplan == "TopUp" {
            return arrOfferList.count
        }
        return arrDataPlanList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row % 2 == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: "InternationalOfferEvenCell"), for: indexPath) as? InternationalOfferCell
            
            if  selectedplan == "TopUp" {
                cell?.fordataplanLabel.isHidden = true
                
                //cell?.lblOfferName.text = dict["Product"].safelyWrappingString() + " " + DestinationCurrency
                let dict = arrOfferList[indexPath.row]

                let finalString =  wrapAmountWithCommaDecimal(key:dict["Product"].safelyWrappingString()) + " " + DestinationCurrency
                let attrStr = NSMutableAttributedString(string: finalString)
                let nsRangeA = NSString(string: finalString).range(of:  wrapAmountWithCommaDecimal(key:dict["Product"].safelyWrappingString()), options: String.CompareOptions.caseInsensitive)
                let nsRangeB = NSString(string: finalString).range(of: DestinationCurrency, options: String.CompareOptions.caseInsensitive)
                attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: UIFont(name: appFont, size: 21) ?? UIFont.systemFont(ofSize: 21) , range: nsRangeA)
                attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: UIFont(name: appFont, size: 15) ?? UIFont.systemFont(ofSize: 15) , range: nsRangeB)
                
                cell?.lblOfferName.attributedText = attrStr
                
                cell?.lblOfferPrice.text =  wrapAmountWithCommaDecimal(key: dict["PriceInMMK"].safelyWrappingString()) //+ " MMK "
                
            }
            else if selectedplan == "DataPlan" {
                cell?.fordataplanLabel.isHidden = false
                if arrDataPlanList.count > 0 {
                let dict = arrDataPlanList[indexPath.row]
                let productname = dict["ProductName"].safelyWrappingString()
                let productshortdesc = dict["ProductShortDesc"].safelyWrappingString()
                let finalString = productname + "\n" + productshortdesc
                let attrStr = NSMutableAttributedString(string: finalString)
                let nsRangeA = NSString(string: finalString).range(of: productname, options: String.CompareOptions.caseInsensitive)
                let nsRangeB = NSString(string: finalString).range(of: productshortdesc, options: String.CompareOptions.caseInsensitive)
                attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: UIFont(name: appFont, size: 15) ?? UIFont.systemFont(ofSize: 15) , range: nsRangeA)
                attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: UIFont(name: appFont, size: 12) ?? UIFont.systemFont(ofSize: 12) , range: nsRangeB)
                
                cell?.lblOfferName.attributedText = attrStr
                cell?.fordataplanLabel.isHidden = true
                cell?.lblOfferPrice.text = wrapAmountWithCommaDecimal(key: dict["PriceInMMK"].safelyWrappingString())
                }
            }
            else{
                
            }
            
            cell?.selectionStyle = .none
            return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: "InternationalOfferOddCell"), for: indexPath) as? InternationalOfferCell
            
            if  selectedplan == "TopUp"{
                cell?.fordataplanLabel.isHidden = true
                let dict = arrOfferList[indexPath.row]

                let finalString =  wrapAmountWithCommaDecimal(key:dict["Product"].safelyWrappingString()) + " " + DestinationCurrency
                let attrStr = NSMutableAttributedString(string: finalString)
                let nsRangeA = NSString(string: finalString).range(of:  wrapAmountWithCommaDecimal(key:dict["Product"].safelyWrappingString()), options: String.CompareOptions.caseInsensitive)
                let nsRangeB = NSString(string: finalString).range(of: DestinationCurrency, options: String.CompareOptions.caseInsensitive)
                attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: UIFont(name: appFont, size: 21) ?? UIFont.systemFont(ofSize: 21) , range: nsRangeA)
                attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: UIFont(name: appFont, size: 15) ?? UIFont.systemFont(ofSize: 15) , range: nsRangeB)
                
                cell?.lblOfferName.attributedText = attrStr
                cell?.lblOfferPrice.text = wrapAmountWithCommaDecimal(key: dict["PriceInMMK"].safelyWrappingString()) //+ " MMK "
            }
            else if selectedplan == "DataPlan" {
                cell?.fordataplanLabel.isHidden = false
                if arrDataPlanList.count > 0 {
                let dict = arrDataPlanList[indexPath.row]

                let productname = dict["ProductName"].safelyWrappingString()
                let productshortdesc = dict["ProductShortDesc"].safelyWrappingString()
                let finalString = productname + "\n" + productshortdesc
                let attrStr = NSMutableAttributedString(string: finalString)
                let nsRangeA = NSString(string: finalString).range(of: productname, options: String.CompareOptions.caseInsensitive)
                let nsRangeB = NSString(string: finalString).range(of: productshortdesc, options: String.CompareOptions.caseInsensitive)
                attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: UIFont(name: appFont, size: 15) ?? UIFont.systemFont(ofSize: 15) , range: nsRangeA)
                attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: UIFont(name: appFont, size: 12) ?? UIFont.systemFont(ofSize: 12), range: nsRangeB)
                
                cell?.lblOfferName.attributedText = attrStr
//                cell?.lblOfferPrice.text = wrapAmountWithCommaDecimal(key:dict["LocalValue"].safelyWrappingString()) + " " + dict["LocalCurrency"].safelyWrappingString()
//                cell?.fordataplanLabel.text = wrapAmountWithCommaDecimal(key: dict["PriceInMMK"].safelyWrappingString()) + " MMK "
                
            cell?.lblOfferPrice.text = wrapAmountWithCommaDecimal(key: dict["PriceInMMK"].safelyWrappingString())
            cell?.fordataplanLabel.isHidden = true
                }
            }
            else{
                
            }
            cell?.selectionStyle = .none
            return cell!
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if selectedplan == "TopUp"{
            let dict = arrOfferList[indexPath.row]
            if (UserLogin.shared.walletBal as NSString).floatValue <  Float(dict["PriceInMMK"].safelyWrappingString()) ?? 0.0 {
                alertViewObj.wrapAlert(title: "Insufficient Balance", body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                    if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                        self.modalPresentationStyle = .fullScreen
                        self.present(addWithdrawView, animated: true, completion: nil)
                    }
                    
                })
                alertViewObj.showAlert(controller: self)
            } else {
                
                if !PaymentVerificationManager.isValidPaymentTransactions(mobilenumber, dict["PriceInMMK"].safelyWrappingString(),"International Top-Up") {
                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: nil, body: "Same Number with Same Amount can not be paid before 2 minutes".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                        alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                          
                        })
                        alertViewObj.showAlert()
                        return
                    }
                }
                else{
                    self.delegate?.sendInternationalOffersAmount(MMKAmount: dict["PriceInMMK"].safelyWrappingString(),retailprice: dict["Retail_price"].safelyWrappingString(), wholesaleprice: dict["Wholesale_price"].safelyWrappingString(), productcurrency: DestinationCurrency, productvalue: dict["Product"].safelyWrappingString(), exchangerateusd: dict["AppliedExchangeRateUSD"].safelyWrappingString(), selectedPlan: selectedplan, productname: "", productdescription: "", productId: "", productName: "")
                    self.dismiss(animated: true, completion: nil)
                }
           
            }
        }
        else if selectedplan == "DataPlan" {
            let dict = arrDataPlanList[indexPath.row]
            if (UserLogin.shared.walletBal as NSString).floatValue <  Float(dict["PriceInMMK"].safelyWrappingString()) ?? 0.0 {
                alertViewObj.wrapAlert(title: "Insufficient Balance", body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                    if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                        self.modalPresentationStyle = .fullScreen
                        self.present(addWithdrawView, animated: true, completion: nil)
                    }
                    
                })
                alertViewObj.showAlert(controller: self)
            } else {
                
                if !PaymentVerificationManager.isValidPaymentTransactions(mobilenumber, dict["PriceInMMK"].safelyWrappingString(),"International Top-Up") {
                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: nil, body: "Same Number with Same Amount can not be paid before 2 minutes".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                        alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                          
                        })
                        alertViewObj.showAlert()
                        return
                    }
                }
                else{
                    
                  self.delegate?.sendInternationalOffersAmount(MMKAmount: dict["PriceInMMK"].safelyWrappingString(), retailprice: dict["Retail_price"].safelyWrappingString(), wholesaleprice: dict["Wholesale_price"].safelyWrappingString(), productcurrency: DestinationCurrency, productvalue: dict["ProductValue"].safelyWrappingString(), exchangerateusd: dict["AppliedExchangeRateUSD"].safelyWrappingString(), selectedPlan: selectedplan, productname: dict["ProductName"].safelyWrappingString(), productdescription: dict["ProductShortDesc"].safelyWrappingString(), productId: dict["ProductId"].safelyWrappingString(), productName: dict["ProductName"].safelyWrappingString())
                    self.dismiss(animated: true, completion: nil)

                }
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView,heightForRowAt indexPath:IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
     return 100
    }
}
