//
//  InternationalConstant.swift
//  OK
//
//  Created by OK$ on 28/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

public struct InternationalConstantURL {
    // stricture for api keys
    public struct ApiKeys {
        static let secretKey = "5aV5R18f1UiW7hyd5CWNQ7gX9Ox9MYXMbEdKb09rTXk1bWVoSG1FSWlHTUhNNTY4dHNGQzBZR3BYMzRXdzJsVmx3eUFWSnpISTlzamVYSHFLSzRnZks4aGd6Sk5zcldxZTBmYUc1bmoyNzMxcGdLQXdOTWw3RjBheFFlZkFsWmJtQ0ZvMXJqT2hVbmhSRzI5b0piZWZ2c1o5RW0wYUhqUmhjT25iZ0pGbzdEbmJUSnJZZjExSzQ1MVRwb3dFdHhhdlc5ejFPaEhBVjBockY3cTU0YjVRc0duZ1hUdjJXQTVKbjFGVFR6Szd0MHpFWEpPcjcwSlIyS0NQV1RlNHZQblU1WUVOMldtRlI4ZzFSaUlORkU0ZjBuUjExYzNhODk2LTM5NWItNDI1Yi05Y2VkLTNlMGE4ZTgzNWFjZA=="
        static let accessKey = "xyo3zNJhy9Sf+ZDhJvFOXg=="
    }
    private struct OverseasUrlConfig {
        static let overseasTopUpProdUrl = "https://internationaltopupapi.okdollar.org/"
        static let overseasTopUpTestUrl = "http://test.internationaltopup.api.okdollar.org/"
        
        static let appProdUrl = "https://www.okdollar.co/"
        static let appTestUrl = "http://103.242.99.234:8001/"
        
        static let estelProdUrl = "http://www.okdollar.net/"
        static let estelTestUrl = "http://120.50.43.150:8090/"
    }
    // base url formation
    public var baseUrl: String {
        #if DEBUG
        return serverUrl == .productionUrl ? OverseasUrlConfig.overseasTopUpProdUrl: OverseasUrlConfig.overseasTopUpTestUrl
        #else
        return OverseasUrlConfig.overseasTopUpProdUrl
        #endif
    }
    public var serverApp: String {
        #if DEBUG
        return serverUrl == .productionUrl ? OverseasUrlConfig.appProdUrl: OverseasUrlConfig.appTestUrl
        #else
        return OverseasUrlConfig.appProdUrl
        #endif
    }
    public var serverEstel: String {
        #if DEBUG
        return serverUrl == .productionUrl ? OverseasUrlConfig.estelProdUrl: OverseasUrlConfig.estelTestUrl
        #else
        return OverseasUrlConfig.estelProdUrl
        #endif
    }
    
    public struct urls {
        static var getCountries = "datapack/allcountries"
        static var getTokens    = "token"
        static var getOperators = "countries/" //"countries/%@/operators"
        static var getConfirmation = "RestService.svc/RetrieveTopUpSim?&MobileNumber=%@&Lat=%@&Long=%@&CellID=%@&Operator=%@&Amount=%@&Customer=%@&Type=0&ByCellID=false&balance=%@"
        static var getKickbackInfo = "WebServiceIpay/services/request;requesttype=FEELIMITKICKBACKINFO;agentcode=%@;destination=%@;amount=%@;pin=%@;vendorcode=IPAY;clientip=%@;clientos=%@;transtype=PAYTO;clienttype=GPRS;securetoken=%@"
        //Data Plan
        static var getDataPlanCountries = "countries/allcountriesNew"
        static var getDataPlanOperators = "datapack/operator/"
        static var getDataPlanPackage = "TalkTime/product/"
        static var getDataPack = "datapack/product/operatorId/"
        static var getTopUpPackage = "talktime/topup"
        static var getDataPackage = "datapack/topup"
    }
    // error messages
    public struct msgs {
        static let numberAlert     = "Invalid Number ! Please Select Correct Number Range 4 to 13"
        static let serviceNotAvail = "Service not available in this country"
        static let screenConfirm   = "ConfirmationModelScreen"
    }
    // acceptableCharacters
    public struct acceptableCharacters {
        static let mobileNumber = "0123456789"
        static let alphaNumeric = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "
    }
    // storyboard Connections & segues
    public struct storyboardConnections {
        static let rechargeSegue              = "internationalRechargeEmbededSegue"
        static let designScreenidentifier     = "InternationalTopupDesignViewController"
        static let dataIdentifier             = "InternationalTopupDataOffer"
        static let specialOfferIdentifier     = "InternationalTopupSpecialOffer"
    }
    public struct headers {
        static let rechargeHeader = "TOP-UP".localized
        static let dataHeader = "Data-Plan".localized
        static let specialOfferHeader = "Special Offers".localized
        static let mainheader = "International Top-Up".localized
    }
}

class InternationalManager {
    func internationalTopupAlert(title: String, body: String) {
        alertViewObj.wrapAlert(title: title, body: body, img: #imageLiteral(resourceName: "dashboard_overseas_recharge"))
        alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            
        }
        alertViewObj.showAlert(controller: UIViewController.init())
    }
    func validations(countryCode: String) -> (min: Int, max: Int) {
        var validation = (0,0)
        switch countryCode {
        case "+91":
            validation = (10,10)
        case "+86":
            validation = (11,11)
        case "+66":
            validation = (9,9)
        default:
            validation = (9,13)
        }
        return validation
    }
    public static var selectedCountryObject: AllCountry?
    public static var selectedOperatorObject: InternationalOperatorModel?
    public static var selectedCountry: (code: String, flag: String) = ("+66", "thailand")
    public static var receiverName = "Unknown"
    struct accessManager {
        var accessToken = ""
        var type        = ""
        init(_ token: String, type: String) {
            self.accessToken = token
            self.type        = type
        }
    }
    class func getUrl(_ appendedUrl: String) -> URL? {
        let urlString = String.init(format: "%@%@", InternationalConstantURL.init().baseUrl, appendedUrl)
        if let urlStr = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            if  let url = URL.init(string: urlStr) {
                return url
            }
        }
        return nil
    }
    class func getServerEstel(_ appendedUrl: String) -> URL? {
        let urlString = String.init(format: "%@%@", InternationalConstantURL.init().serverEstel, appendedUrl)
        if let urlStr = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            if  let url = URL.init(string: urlStr) {
                return url
            }
        }
        return nil
    }
    class func getServerAppUrl(_ appendedUrl: String) -> URL? {
        let urlString = String.init(format: "%@%@", InternationalConstantURL.init().serverApp, appendedUrl)
        if let urlStr = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            if  let url = URL.init(string: urlStr) {
                return url
            }
        }
        return nil
    }
}
