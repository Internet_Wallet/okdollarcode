//
//  InternationalWeb.swift
//  OK
//
//  Created by OK$ on 28/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

extension InternationalCountryVC {
    
    func initialWebCall() {
        self.showProgressView()
           if appDelegate.checkNetworkAvail() {
               self.getAllDetailsForInternationalTopup(handle: { [weak self](tokenManager) in
                   DispatchQueue.main.async {
                    self?.removeProgressView()
                   self?.getAllCountries(accessManager: tokenManager)
                   }
               })
           } else {
            self.removeProgressView()
               self.showErrorAlert(errMessage: "Check Internet Connection")
           }
       }
       
    func getAllDetailsForInternationalTopup(handle:@escaping (_ tokenManager: InternationalManager.accessManager) -> Void) {
        let url  = InternationalManager.getUrl(InternationalConstantURL.urls.getTokens)
           let hash = InternationalConstantURL.ApiKeys.accessKey.hmac_SHA1(key: InternationalConstantURL.ApiKeys.secretKey)
           let parameters = String.init(format: "password=%@&grant_type=password", hash)
           self.showProgressView()

           TopupWeb.genericApiWithHeader(url: url!, param: parameters as AnyObject, httpMethod: "POST", header: nil) { (result, success) in
            self.removeProgressView()
            if success {

                   if let dictionary = result as? Dictionary<String,Any> {
                       let token = dictionary.safeValueForKey("access_token") as? String
                       let type  = dictionary.safeValueForKey("token_type") as? String
                       let tokenManager = InternationalManager.accessManager.init(token ?? "", type: type ?? "")
                       DispatchQueue.main.async {
                          handle(tokenManager)
                       }
                   }
               }
           }
       }
    
    func getAllCountries(accessManager: InternationalManager.accessManager) {
    let url = InternationalManager.getUrl(InternationalConstantURL.urls.getDataPlanCountries)
        let paramDict = Dictionary<String,String>()
        let headerString = String.init(format: "%@ %@", accessManager.type, accessManager.accessToken)
        self.showProgressView()
        println_debug(headerString)
        TopupWeb.genericApiWithHeader(url: url!, param: paramDict as AnyObject, httpMethod: "GET", header: headerString) { [weak self](result, success) in
            self?.removeProgressView()

            if success {

                DispatchQueue.main.async {

                    if let dictionary = result as? Dictionary<String,Any> {
                    println_debug(dictionary)
                    
                        if let jsonData = dictionary.jsonString().data(using: .utf8) {
                            if  let internationalTopupObject = try? JSONDecoder().decode(InternationalCountryModel.self, from: jsonData) {
                                if internationalTopupObject.message.safelyWrappingString().lowercased() == "Success".lowercased() {
                                    if let countries = internationalTopupObject.results {
                                        self?.arrCountryList = countries.sorted(by: { $0.countryName ?? "" < $1.countryName ?? ""})
                                        self?.arrFilterList = countries.sorted(by: { $0.countryName ?? "" < $1.countryName ?? ""})
                                        self?.tblCountry.reloadData()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

extension InternationalOperatorVC {
    
    func initialWebCall() {
        self.showProgressView()
           if appDelegate.checkNetworkAvail() {
               self.getAllDetailsForInternationTopup(handle: { [weak self](tokenManager) in
                   DispatchQueue.main.async {
                    self?.removeProgressView()
                   self?.getAllCountries(accessManager: tokenManager)
                   }
               })
           } else {
                self.removeProgressView()
               self.showErrorAlert(errMessage: "Check Internet Connection")
           }
       }
       
       private func getAllDetailsForInternationTopup(handle:@escaping (_ tokenManager: InternationalManager.accessManager) -> Void) {
        let url  = InternationalManager.getUrl(InternationalConstantURL.urls.getTokens)
           let hash = InternationalConstantURL.ApiKeys.accessKey.hmac_SHA1(key: InternationalConstantURL.ApiKeys.secretKey)
           let parameters = String.init(format: "password=%@&grant_type=password", hash)
           self.showProgressView()

           TopupWeb.genericApiWithHeader(url: url!, param: parameters as AnyObject, httpMethod: "POST", header: nil) { (result, success) in
            self.removeProgressView()

            if success {

                   if let dictionary = result as? Dictionary<String,Any> {
                       let token = dictionary.safeValueForKey("access_token") as? String
                       let type  = dictionary.safeValueForKey("token_type") as? String
                       let tokenManager = InternationalManager.accessManager.init(token ?? "", type: type ?? "")
                       DispatchQueue.main.async {
                          handle(tokenManager)
                       }
                   }
               }
           }
       }
       
       func getAllCountries(accessManager: InternationalManager.accessManager) {
        let url = InternationalManager.getUrl(InternationalConstantURL.urls.getOperators + (strCountryID ?? "") + "/NewOperators")
           let paramDict = Dictionary<String,String>()
           let headerString = String.init(format: "%@ %@", accessManager.type, accessManager.accessToken)
           self.showProgressView()

           TopupWeb.genericApiWithHeader(url: url!, param: paramDict as AnyObject, httpMethod: "GET", header: headerString) { [weak self](result, success) in
            self?.removeProgressView()

            if success {
                
                   DispatchQueue.main.async {
                    self?.tblOperator.isHidden = false
                    self?.noRecordsLabel.isHidden = true
                   
                       if let dictionary = result as? Dictionary<String,Any> {
                           println_debug(dictionary)
                        let code = dictionary["Message"] as? String
                         if let isValidCode = code {
                           if isValidCode == "Success" {
                            self?.operatorlistArr = dictionary["Result"] as! [[String : Any]]
                            self?.arrFilterList = dictionary["Result"] as! [[String : Any]]
                            if let value = self?.operatorlistArr {
                                if value.count>0{
                                    self?.tblOperator.reloadData()
                                }
                            }
                          }
                          else
                          {
                              alertViewObj.wrapAlert(title: "", body: (dictionary["Message"] as? String)!, img: #imageLiteral(resourceName: "alert-icon"))
                              alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                  })
                              alertViewObj.showAlert(controller: self)
                            }
                          }
                       }
                   }
               } else {
                DispatchQueue.main.async {
                self?.tblOperator.isHidden = true
                self?.noRecordsLabel.isHidden = false
               }
               }
           }
       }
}

extension InternationalOffersVC {
    //Top Up data Request
    func initialWebCall() {
        self.showProgressView()
           if appDelegate.checkNetworkAvail() {
               self.getAllDetailsForInternationTopup(handle: { [weak self](tokenManager) in
                   DispatchQueue.main.async {
                    self?.removeProgressView()
                   self?.getAllCountries(accessManager: tokenManager)
                   }
               })
           } else {
            self.removeProgressView()
               self.showErrorAlert(errMessage: "Check Internet Connection")
           }
       }
       
       private func getAllDetailsForInternationTopup(handle:@escaping (_ tokenManager: InternationalManager.accessManager) -> Void) {
        let url  = InternationalManager.getUrl(InternationalConstantURL.urls.getTokens)
           let hash = InternationalConstantURL.ApiKeys.accessKey.hmac_SHA1(key: InternationalConstantURL.ApiKeys.secretKey)
           let parameters = String.init(format: "password=%@&grant_type=password", hash)
           self.showProgressView()

           TopupWeb.genericApiWithHeader(url: url!, param: parameters as AnyObject, httpMethod: "POST", header: nil) { (result, success) in
            self.removeProgressView()

            if success {

                   if let dictionary = result as? Dictionary<String,Any> {
                       let token = dictionary.safeValueForKey("access_token") as? String
                       let type  = dictionary.safeValueForKey("token_type") as? String
                       let tokenManager = InternationalManager.accessManager.init(token ?? "", type: type ?? "")
                       DispatchQueue.main.async {
                          handle(tokenManager)
                       }
                   }
               }
           }
       }
       //TalkTime= Top-Up Data Access
       func getAllCountries(accessManager: InternationalManager.accessManager) {
        let url = InternationalManager.getUrl(InternationalConstantURL.urls.getDataPlanPackage + (strOperatorID ?? ""))
           let paramDict = Dictionary<String,String>()
           let headerString = String.init(format: "%@ %@", accessManager.type, accessManager.accessToken)
           self.showProgressView()

           TopupWeb.genericApiWithHeader(url: url!, param: paramDict as AnyObject, httpMethod: "GET", header: headerString) { [weak self](result, success) in
            self?.removeProgressView()
            self?.statusTopUpAPI = true

            if success {
                   DispatchQueue.main.async {
                    
                       if let dictionary = result as? Dictionary<String,Any> {
                        println_debug(dictionary)
                        let code = dictionary["Message"] as? String
                         print("#####",code as Any)
                         if let isValidCode = code {
                           if isValidCode == "Success" {
                            let dictResult = dictionary["Result"] as! [String : Any]
                            if dictResult.count > 0 {
                                self?.DestinationCurrency = dictResult["DestinationCurrency"] as? String ?? ""
                                self?.arrOfferList = dictResult["ProductList"] as! [[String : Any]]
                                if let value = self?.arrOfferList{
                                    if value.count>0{
                                        self?.tblOffer.isHidden = false
                                        self?.tblOffer.reloadData()
                                    } else {
                                        self?.nodataLabel.text = "No Record Found".localized
                                            self?.tblOffer.isHidden = true
                                    }
                                }
                               else {
                                self?.nodataLabel.text = "No Record Found".localized
                                    self?.tblOffer.isHidden = true
                                    self?.tblOffer.reloadData()
                                }
                            }
                            else{
                                self?.nodataLabel.text =  "No Record Found".localized
                                self?.tblOffer.isHidden = true
                                self?.tblOffer.reloadData()
                            }
                            
                          
                          }
                          else
                          {
                              alertViewObj.wrapAlert(title: "", body: (dictionary["Message"] as? String)!, img: #imageLiteral(resourceName: "alert-icon"))
                              alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                  })
                              alertViewObj.showAlert(controller: self)
                            }
                          }
                       }
                   }
            } else {
                self?.nodataLabel.text = "No Record Found".localized
                self?.tblOffer.isHidden = true
                self?.tblOffer.reloadData()
            }
           }
       }
}

extension InternationalOffersVC {
    //Top up data plan
    func DataWebCall() {
        self.showProgressView()
           if appDelegate.checkNetworkAvail() {
               self.getAllDetailsForInternationDataPlan(handle: { [weak self](tokenManager) in
                   DispatchQueue.main.async {
                    self?.removeProgressView()
                   self?.getAllCountriesDataplan(accessManager: tokenManager)
                   }
               })
           } else {
            self.removeProgressView()
               self.showErrorAlert(errMessage: "Check Internet Connection")
           }
       }
       
       private func getAllDetailsForInternationDataPlan(handle:@escaping (_ tokenManager: InternationalManager.accessManager) -> Void) {
        let url  = InternationalManager.getUrl(InternationalConstantURL.urls.getTokens)
           let hash = InternationalConstantURL.ApiKeys.accessKey.hmac_SHA1(key: InternationalConstantURL.ApiKeys.secretKey)
           let parameters = String.init(format: "password=%@&grant_type=password", hash)
           self.showProgressView()

           TopupWeb.genericApiWithHeader(url: url!, param: parameters as AnyObject, httpMethod: "POST", header: nil) { (result, success) in
            self.removeProgressView()

            if success {

                   if let dictionary = result as? Dictionary<String,Any> {
                       let token = dictionary.safeValueForKey("access_token") as? String
                       let type  = dictionary.safeValueForKey("token_type") as? String
                       let tokenManager = InternationalManager.accessManager.init(token ?? "", type: type ?? "")
                       DispatchQueue.main.async {
                          handle(tokenManager)
                       }
                   }
               }
           }
       }
       func getAllCountriesDataplan(accessManager: InternationalManager.accessManager) {
        let url = InternationalManager.getUrl(InternationalConstantURL.urls.getDataPack + (strOperatorID ?? "") + "/countryId/" + (strCountryID ?? ""))
           let paramDict = Dictionary<String,String>()
           let headerString = String.init(format: "%@ %@", accessManager.type, accessManager.accessToken)
           self.showProgressView()

           TopupWeb.genericApiWithHeader(url: url!, param: paramDict as AnyObject, httpMethod: "GET", header: headerString) { [weak self](result, success) in
            self?.removeProgressView()
            self?.statusDataPlanAPI = true
            if success {

                   DispatchQueue.main.async {
                    
                       if let dictionary = result as? Dictionary<String,Any> {
                        println_debug(dictionary)
                        let code = dictionary["Message"] as? String
                         print("#####",code as Any)
                         if let isValidCode = code {
                            
                           if isValidCode == "Success" {
                            let dictResult = dictionary["Result"] as! [Dictionary<String,Any>]
                            
                            if dictResult.count > 0 {
                                self?.arrDataPlanList = dictResult.sorted(by: { $1["PriceInMMK"] as! Int > $0["PriceInMMK"] as! Int })
                                self?.tblOffer.isHidden = false
                                self?.tblOffer.reloadData()
                            }
                            else
                            {
                                self?.nodataLabel.text =  "No Record Found".localized
                                self?.tblOffer.isHidden = true

                                self?.tblOffer.reloadData()
                            }
                          
                          }
                          else
                          {
                              alertViewObj.wrapAlert(title: "", body: (dictionary["Message"] as? String)!, img: #imageLiteral(resourceName: "alert-icon"))
                              alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                  })
                              alertViewObj.showAlert(controller: self)
                            }
                          }
                       }
                   }
               } else {
                DispatchQueue.main.async {
                    self?.nodataLabel.text =  "No Record Found".localized
                    self?.tblOffer.isHidden = true
                    self?.tblOffer.reloadData()
                }
            }
           }
       }
}

extension InternationalConfirmationViewController {
    
    func initialWebCall() {
        self.showProgressView()
           if appDelegate.checkNetworkAvail() {
               self.getAllDetailsForInternationTopup(handle: { [weak self](tokenManager) in
                   DispatchQueue.main.async {
                    self?.removeProgressView()
                   self?.postDataPackTopUp(accessManager: tokenManager)
                   }
               })
           } else {
                self.removeProgressView()
               self.showErrorAlert(errMessage: "Check Internet Connection")
           }
       }
       
       private func getAllDetailsForInternationTopup(handle:@escaping (_ tokenManager: InternationalManager.accessManager) -> Void) {
        let url  = InternationalManager.getUrl(InternationalConstantURL.urls.getTokens)
           let hash = InternationalConstantURL.ApiKeys.accessKey.hmac_SHA1(key: InternationalConstantURL.ApiKeys.secretKey)
           let parameters = String.init(format: "password=%@&grant_type=password", hash)
           self.showProgressView()

           TopupWeb.genericApiWithHeader(url: url!, param: parameters as AnyObject, httpMethod: "POST", header: nil) { (result, success) in
            self.removeProgressView()
            if success {

                   if let dictionary = result as? Dictionary<String,Any> {
                       let token = dictionary.safeValueForKey("access_token") as? String
                       let type  = dictionary.safeValueForKey("token_type") as? String
                       let tokenManager = InternationalManager.accessManager.init(token ?? "", type: type ?? "")
                       DispatchQueue.main.async {
                          handle(tokenManager)
                       }
                   }
               }
           }
       }
       
       func postDataPackTopUp(accessManager: InternationalManager.accessManager) {
        
        var url = InternationalManager.getUrl(InternationalConstantURL.urls.getTopUpPackage)

        if strTopUpPlan == "TopUp" {
            url = InternationalManager.getUrl(InternationalConstantURL.urls.getTopUpPackage)
        } else {
            url = InternationalManager.getUrl(InternationalConstantURL.urls.getDataPackage)
        }
           //let paramDict = Dictionary<String,String>()
           let headerString = String.init(format: "%@ %@", accessManager.type, accessManager.accessToken)
           self.showProgressView()
            println_debug(finalRequestParam)
           TopupWeb.genericApiWithHeader(url: url!, param: finalRequestParam as AnyObject, httpMethod: "POST", header: headerString) { [weak self](result, success) in
            self?.removeProgressView()

            if success {

                   DispatchQueue.main.async {
                    
                       if let dictionary = result as? Dictionary<String,Any> {
                          println_debug(dictionary)
                        
                        let code = dictionary["Message"] as? String
                         print("#####",code as Any)
                         if let isValidCode = code {
                           if isValidCode == "Success" || isValidCode == "Failed" {
                            let dictResult = dictionary["Result"] as! [String : Any]
                            let datetime = dictResult["CreatedDate"] as? String ?? ""
                            let myStringArr = datetime.components(separatedBy: " ")
                                                        
                            let date: String = myStringArr [0]
                            let time: String = myStringArr [1]
                            
                            if dictResult.count > 0 {
                                //Save for recent international topup
                                
                                let dictRecentTopUp = NSMutableDictionary()
                                dictRecentTopUp.setValue(self?.dictRequest["CountryName"], forKey: "CountryName")
                                dictRecentTopUp.setValue(self?.dictRequest["CountryCode"], forKey: "CountryCode")
                                dictRecentTopUp.setValue(self?.dictRequest["CountryFlag"], forKey: "CountryFlag")
                                dictRecentTopUp.setValue(self?.dictRequest["CountryId"], forKey: "CountryId")
                                dictRecentTopUp.setValue(self?.dictRequest["TalkTimeCountryId"], forKey: "TalkTimeCountryId")
                                dictRecentTopUp.setValue(self?.dictRequest["AmountInMMK"], forKey: "RecentAmount")
                                dictRecentTopUp.setValue(self!.mobiledisplay, forKey: "RecentNumber")
                                UserDefaults.standard.setValue(dictRecentTopUp, forKey: "RecentTopUp")
                                
                                userDef.set(dictRecentTopUp, forKey: "paymentrepeat")

                                //Navigate to receipt
                                if let objView = UIStoryboard(name: "InternationalTopUp", bundle: nil).instantiateViewController(withIdentifier: "InternationalReceiptVC") as? InternationalReceiptVC {
                                    objView.modalPresentationStyle = .fullScreen
                                    objView.strTime = time
                                    objView.strDate = date
                                    objView.strErrorMsg = dictResult["ErrorMessage"] as? String ?? ""
                                    objView.DestinationNumber = self!.mobiledisplay
                                    objView.DestinationCountry = self!.countryisdcode

                                    objView.strMobileNo = "(" + self!.countryisdcode + ") " + self!.mobiledisplay
                                    objView.strTelOperator = self?.dictRequest["OperatorName"] as? String ?? ""
                                    objView.strTxID = dictResult["OKTransactionId"] as? String ?? ""
                                    objView.strAmount = self?.dictRequest["ProductValue"] as? String ?? ""
                                    objView.strAmountInMMK = self?.dictRequest["AmountInMMK"] as? String ?? ""
                                    objView.strCashBack = dictResult["Cashback"] as? String ?? ""
                                    objView.strLoyalityPoints = dictResult["LoyaltyPoints"] as? String ?? ""
                                    objView.strOKBalance = dictResult["OKBalance"] as? String ?? ""
                                    objView.selectedcountrycurrency = self?.dictRequest["ProductCurrency"] as? String ?? ""
                                    objView.strTopupData = self?.dictRequest["ProductValue"] as? String ?? ""
                                    objView.strTopupUnit = self?.dictRequest["ProductValue"] as? String ?? ""
                                    objView.strTopUpPlan = self?.strTopUpPlan ?? ""
                                    objView.fromFavorite = self?.fromFavorite ?? ""
                                    objView.DestinationCurrency = self?.DestinationCurrency ?? ""
                                    objView.strBeneficiaryName = self?.strBeneficiaryName ?? "Unknown"
                                    self?.present(objView, animated: true, completion: nil)
                                }
                                
                            }
                            else
                            {
                                alertViewObj.wrapAlert(title: "", body: (dictionary["Message"] as? String)!, img: #imageLiteral(resourceName: "alert-icon"))
                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                    })
                                alertViewObj.showAlert(controller: self)
                            }
                          
                          }
                          else
                          {
                              alertViewObj.wrapAlert(title: "", body: (dictionary["Message"] as? String)!, img: #imageLiteral(resourceName: "alert-icon"))
                              alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                  })
                              alertViewObj.showAlert(controller: self)
                            }
                          }
                        
//                           if let jsonData = dictionary.jsonString().data(using: .utf8) {
//                               if  let internationalTopupObject = try? JSONDecoder().decode(InternationalPlanModel.self, from: jsonData) {
//                                   if internationalTopupObject.message.safelyWrappingString().lowercased() == "Success".lowercased() {
//
//                                   }
//                               }
//                           }
                       }
                   }
               }
           }
       }
      
}

extension InternationalMainVC{
    func generatePaymentForDatapackTopUp() -> Dictionary<String,Any> {
     
        // Final payment Dictionary
        var lTransactions = Dictionary<String, Any>()
        lTransactions["ClientId"] = "27ca593c-3f1d-44a0-899b-648f96a66c9c"
        lTransactions["OkAccountNumber"] = UserModel.shared.mobileNo
        lTransactions["Password"] = ok_password ?? ""
        lTransactions["MobileNumber"] = self.mobileNumber.text ?? ""
        lTransactions["CountryId"] = self.selectCountryId
        lTransactions["CountryName"] = self.selectCountryName
        lTransactions["OperatorId"] = self.strOperatorID ?? ""
        lTransactions["OperatorName"] = self.selectOperator.text
        lTransactions["ProductCurrency"]  = self.productcurrency
        lTransactions["ProductValue"]  = self.productvalue ?? ""
        lTransactions["DataValue"]  = self.selectAmountTextView.text ?? ""
        lTransactions["AmountInMMK"] = self.MMKAmount ?? ""
        lTransactions["RetailPriceUSD"] = self.retailprice
        lTransactions["WholeSalePrice"] = self.wholesaleprice
        lTransactions["AppliedExchangeRateUSD"] = self.exchangerateusd
        lTransactions["ContactName"] = self.contactname
        lTransactions["CountryFlag"] = self.strCountryFlag
        lTransactions["CountryCode"] = self.selectCountryCode
        lTransactions["TalkTimeCountryId"] = self.selectTalktimeCountryId
        lTransactions["Remark"] = ""
       
        var strPlanType = ""
        if selectedPlan == "TopUp"{
                   strPlanType = "TopUpPlan"
        } else {
            lTransactions["ProductId"] = self.productId
            lTransactions["ProductName"] = self.productName
            strPlanType = "DataPlan"
        }
        
        let comments = String.init(format: "#OK-00-%@-%@-%@-%@-%@-%@-%@-na#",self.selectOperator.text ?? "",self.mobileNumber.text ?? "",strPlanType,self.productvalue ?? "","Overseas",self.selectCountryCode,self.self.productcurrency ?? "")
        lTransactions["OKComment"] = comments
        
        println_debug(lTransactions)
        return lTransactions
    }
    
    func initialWebCall() {
        self.showProgressView()
           if appDelegate.checkNetworkAvail() {
               self.getAllDetailsForInternationalTopup(handle: { [weak self](tokenManager) in
                   DispatchQueue.main.async {
                    self?.removeProgressView()
                   self?.getAllCountries(accessManager: tokenManager)
                   }
               })
           } else {
            self.removeProgressView()
               self.showErrorAlert(errMessage: "Check Internet Connection")
           }
       }
       
    func getAllDetailsForInternationalTopup(handle:@escaping (_ tokenManager: InternationalManager.accessManager) -> Void) {
        let url  = InternationalManager.getUrl(InternationalConstantURL.urls.getTokens)
           let hash = InternationalConstantURL.ApiKeys.accessKey.hmac_SHA1(key: InternationalConstantURL.ApiKeys.secretKey)
           let parameters = String.init(format: "password=%@&grant_type=password", hash)
           self.showProgressView()

           TopupWeb.genericApiWithHeader(url: url!, param: parameters as AnyObject, httpMethod: "POST", header: nil) { (result, success) in
            self.removeProgressView()
            if success {

                   if let dictionary = result as? Dictionary<String,Any> {
                       let token = dictionary.safeValueForKey("access_token") as? String
                       let type  = dictionary.safeValueForKey("token_type") as? String
                       let tokenManager = InternationalManager.accessManager.init(token ?? "", type: type ?? "")
                       DispatchQueue.main.async {
                          handle(tokenManager)
                       }
                   }
               }
           }
       }
    
    func getAllCountries(accessManager: InternationalManager.accessManager) {
    let url = InternationalManager.getUrl(InternationalConstantURL.urls.getDataPlanCountries)
        let paramDict = Dictionary<String,String>()
        let headerString = String.init(format: "%@ %@", accessManager.type, accessManager.accessToken)
        self.showProgressView()
        println_debug(headerString)
        TopupWeb.genericApiWithHeader(url: url!, param: paramDict as AnyObject, httpMethod: "GET", header: headerString) { [weak self](result, success) in
            self?.removeProgressView()
            if success {

                DispatchQueue.main.async {

                    if let dictionary = result as? Dictionary<String,Any> {
                    println_debug(dictionary)
                    
                        if let jsonData = dictionary.jsonString().data(using: .utf8) {
                            if  let internationalTopupObject = try? JSONDecoder().decode(InternationalCountryModel.self, from: jsonData) {
                                if internationalTopupObject.message.safelyWrappingString().lowercased() == "Success".lowercased() {
                                    if let countries = internationalTopupObject.results {
                                        
                                        self?.arrCountryList = countries
                                        DispatchQueue.main.async {

                                        if self?.statusScreen == "RecentTx" {
                                            println_debug("RecentTx")
                                            for element in countries {
                                                
                                                let arrCode = self?.userObject?.number.components(separatedBy: ")")
                                                let getcountryCode = arrCode?[0].replacingOccurrences(of: "(", with: "")
                                                let remaingnumber = arrCode?[1]
                                                
                                                if getcountryCode ==  element.countryCode2 ?? "+66"{
                                                    if let imageStr = element.countryFlag, let imageUrl = URL(string: imageStr) {
                                                        self?.flagimage.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: ""))
                                                        self?.selectCountryTF.text =  element.countryName
                                                        
                                                        self?.selectCountryCode = element.countryCode2 ?? "+66"
                                                        
                                                        self?.selectCountryName = element.countryName ?? ""
                                                        self?.selectCountryId = element.countryId ?? ""
                                                        self?.selectTalktimeCountryId = element.talkTimeCountryId ?? ""
                                                        self?.mobileNumberLeftView.wrapinternationalCountryViewData(img: "topup_number", str: getcountryCode ?? "+66")
                                                        self?.mobileNumber.text = remaingnumber
                                                        self?.mobileNumberRightView.hideClearButton(hide: false)
                                                        self?.mobilecountrycodeLabel.text = "(" + getcountryCode! + ")"
                                                        self?.usernameView.isHidden = false
                                                        self?.userviewTopConstraint.constant = 2
                                                        self?.userviewHeightConstraint.constant = 60
                                                        self?.usernameLabel.text = self?.userObject?.name ?? "Unknown"
                                                        self?.phoneuiTopConstraint.constant = 0
                                                        self?.confirmuiView.isHidden = true
                                                        self?.phoneuiheightConstraint.constant = 0
                                                        
                                                        self?.contactname = self?.userObject?.name ?? ""
                                                        self?.selectOperator.isHidden = false
                                                          self?.selectserivceImage.isHidden = false
                                                          self?.operatoruiView.isHidden = false
                                                          self?.lblOperatorText.isHidden = false
                                                        self?.confirmfieldhide = "From Contacts"
                                                    }
                                                    
                                                }
                                                
                                                
                                            }
                                            
                                        } else if self?.statusScreen == "FavoriteTx" {
                                            
                                            self?.lblOperatorText.isHidden = false
                                            let favObj = FavModel.init("", phone: self?.userObject?.number ?? "", name: self?.userObject?.name ?? "", createDate: "", agentID: "", type: "")
                                            let userContact = ContactPicker.init(object: favObj)
                                            self?.confirmfieldhide = "From Contacts"
                                            self?.fromFavorite = "Fav"
                                            self?.contact(ContactPickersPicker(), didSelectContact: userContact)

                                        } else  if UserDefaults.standard.value(forKey: "RecentTopUp") != nil {
                                           
                                            var dictRecent = NSDictionary()
                                            dictRecent = UserDefaults.standard.value(forKey: "RecentTopUp") as! NSDictionary
                                         self?.strCountryFlag = dictRecent["CountryFlag"] as? String ?? ""
                                         
                                            let imageUrl = URL(string: self?.strCountryFlag! ?? "")
                                            self?.flagimage.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: ""))
                                            var country = ""
                                             country = dictRecent["CountryName"] as? String ?? "Thailand"
                                            if country == ""{
                                                self?.selectCountryTF.text = "Thailand"
                                            }else{
                                                self?.selectCountryTF.text =  dictRecent["CountryName"] as? String ?? "Thailand"
                                            }
                                            self?.selectCountryCode = dictRecent["CountryCode"] as? String ?? "+66"
                                            self?.selectCountryName = dictRecent["CountryName"] as? String ?? "Thailand"
                                            self?.selectCountryId = dictRecent["CountryId"] as? String ?? ""
                                            self?.selectTalktimeCountryId = dictRecent["TalkTimeCountryId"] as? String ?? ""
                                            self?.mobileNumberLeftView.wrapinternationalCountryViewData(img: "topup_number", str: dictRecent["CountryCode"] as? String ?? "+66")
                                         let cCode = dictRecent["CountryCode"] as? String ?? "+66"
                                         self?.mobilecountrycodeLabel.text = "(" + cCode + ")"
                                            self?.confirmMobileNumberLeftView.wrapinternationalCountryViewData(img: "topup_number", str:  dictRecent["CountryCode"] as? String ?? "+66")
                                        }
                                        else{
                                            
                                            //Show overseas country nearest based on GPS
                                            SingletonClass.sharedInstance.getAdress(completion: { (address, error) in
                                                println_debug(address?["Country"])
                                                let contryName = address?["Country"] ?? ""
                                                for element in countries {
                                                    //if (element.countryName!.contains(find: address?["Country"] as! String))
                                                    if ((contryName as AnyObject).contains(element.countryName!))
                                                    {
                                                        if element.countryName == "Myanmar" {
                                                            break
                                                        }
                                                        
                                                        if let imageStr = element.countryFlag, let imageUrl = URL(string: imageStr) {
                                                            self?.flagimage.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: ""))
                                                            self?.selectCountryTF.text =  element.countryName
                                                            
                                                            self?.selectCountryCode = element.countryCode2 ?? "+66"
                                                            
                                                            self?.selectCountryName = element.countryName ?? ""
                                                            self?.selectCountryId = element.countryId ?? ""
                                                            self?.selectTalktimeCountryId = element.talkTimeCountryId ?? ""
                                                            self?.mobileNumberLeftView.wrapinternationalCountryViewData(img: "topup_number", str: element.countryCode2 ?? "+66")
                                                            self?.confirmMobileNumberLeftView.wrapinternationalCountryViewData(img: "topup_number", str: element.countryCode2 ?? "+66")
                                                        }
                                                        break
                                                    }
                                                }
                                            })
                                        }
                                            self?.mobileNumber.becomeFirstResponder()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
