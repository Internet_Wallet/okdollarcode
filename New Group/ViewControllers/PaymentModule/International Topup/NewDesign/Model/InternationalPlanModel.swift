//
//  InternationalDataPlanModel.swift
//  OK
//
//  Created by OK$ on 28/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

struct InternationalPlanModel : Codable {
    
    let message : String?
    let result : String?
    let results : [InternationalDataPlanListModel]?
    let status : String?
    let statusCode : String?

    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case result = "Result"
        case results = "Results"
        case status = "Status"
        case statusCode = "StatusCode"
    }
}

struct InternationalDataPlanListModel : Codable {
    
    let ProductId : String?
    let ProductName : String?
    let ProductShortDesc : String?
    let OperatorId : String?
    let Operator : String?
    let CountryId : String?
    let Country : String?
    let ServiceId : String?
    let Service : String?
    let AccountCurrency : String?
    let WholesalePrice : String?
    let RetailPrice : String?
    let Fee : String?
    let ProductCurrency : String?
    let ProductValue : String?
    let LocalCurrency : String?
    let LocalValue : String?
    let PriceInMMK : String?
    
    enum CodingKeys: String, CodingKey {
        case ProductId = "ProductId"
        case ProductName = "ProductName"
        case ProductShortDesc = "ProductShortDesc"
        case OperatorId = "OperatorId"
        case Operator = "Operator"
        case CountryId = "CountryId"
        case Country = "Country"
        case ServiceId = "ServiceId"
        case Service = "Service"
        case AccountCurrency = "AccountCurrency"
        case WholesalePrice = "WholesalePrice"
        case RetailPrice = "RetailPrice"
        case Fee = "Fee"
        case ProductCurrency = "ProductCurrency"
        case ProductValue = "ProductValue"
        case LocalCurrency = "LocalCurrency"
        case LocalValue = "LocalValue"
        case PriceInMMK = "PriceInMMK"
    }
}

