//
//  InternationalOperatorModel.swift
//  OK
//
//  Created by OK$ on 28/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

struct InternationalOperatorModel : Codable {
    
    let message : String?
    let result : [InternationalOperatorListModel]?
    let status : Int?
    let statusCode : Int?

    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case result = "Result"
        case status = "Status"
        case statusCode = "StatusCode"
    }
}

struct InternationalOperatorListModel : Codable {
    
    let countryId : String?
    let createdDate : String?
    let dataPlanCountryId : Int?
    let IsActive : Int?
    let operatorCode : Int?
    let operatorId : String?
    let operatorLogo : String?
    let operatorName : String?
    let talkTimeCountryId  : Int?

    enum CodingKeys: String, CodingKey {
        case countryId = "CountryId"
        case createdDate = "CreatedDate"
        case dataPlanCountryId = "DataPlanCountryId"
        case IsActive = "IsActive"
        case operatorCode = "OperatorCode"
        case operatorId = "OperatorId"
        case operatorLogo = "OperatorLogo"
        case operatorName = "OperatorName"
        case talkTimeCountryId  = "TalkTimeCountryId"
        
    }
}
