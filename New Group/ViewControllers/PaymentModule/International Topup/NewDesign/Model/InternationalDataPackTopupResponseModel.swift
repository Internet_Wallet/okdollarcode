//
//  InternationalDataPackTopupResponseModel.swift
//  OK
//
//  Created by OK$ on 25/08/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

struct InternationalDataPackTopupResponseModel : Codable {
    
    let message : String?
    let result : [InternationalDataPackTopupResponse]?
    let results : String?
    let status : String?
    let statusCode : String?

    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case result = "Result"
        case results = "Results"
        case status = "Status"
        case statusCode = "StatusCode"
    }
}

struct InternationalDataPackTopupResponse : Codable {
    
    let DataPackTransactionId : String?
    let MobileNumber : String?
    let TotalAmount : String?
    let CustomerId : String?
    let OkDestinationNumber : String?
    let ServiceCharges : String?
    let IsActive : Bool?
    let CreatedDate : String?
    let ClientId : String?
    let Request : InternationalDataPackTopupRequest?
    let Response : InternationalDataPackTopupTxResponse?
    let Status : String?
    let countryId : String?
    let countryName : String?
    let operatorId : String?
    let operatorName : String?
    let productId : String?
    let productName : String?
    let retailPriceUSD : String?
    let productCurrency : String?
    let productValue : String?
    let amountInMMK : String?
    let okTransactionId : String?
    let okTransactionStatus : String?
    let topupStatus : String?
    let customerOkTransactionId : String?
    let customerOkPaymentStatus : String?
    let dTOneApiStatus : String?
    let dTOneApiMessage : String?
    let refundOkTransactionId : String?
    let refundOkPaymentStatus : String?
    let wholeSalePrice : String?
    let appliedExchangeRateUSD : String?
    
    enum CodingKeys: String, CodingKey {
        case DataPackTransactionId = "DataPackTransactionId"
        case MobileNumber = "MobileNumber"
        case TotalAmount = "TotalAmount"
        case CustomerId = "CustomerId"
        case OkDestinationNumber = "OkDestinationNumber"
        case ServiceCharges = "ServiceCharges"
        case IsActive = "IsActive"
        case CreatedDate = "CreatedDate"
        case ClientId = "ClientId"
        case Request = "Request"
        case Response = "Response"
        case Status = "Status"
        case countryId = "countryId"
        case countryName = "countryName"
        case operatorId = "operatorId"
        case operatorName = "operatorName"
        case productId = "productId"
        case productName = "productName"
        case retailPriceUSD = "retailPriceUSD"
        case productCurrency = "productCurrency"
        case productValue = "productValue"
        case amountInMMK = "amountInMMK"
        case okTransactionId = "okTransactionId"
        case okTransactionStatus = "okTransactionStatus"
        case topupStatus = "topupStatus"
        case customerOkTransactionId = "customerOkTransactionId"
        case customerOkPaymentStatus = "customerOkPaymentStatus"
        case dTOneApiStatus = "dTOneApiStatus"
        case dTOneApiMessage = "dTOneApiMessage"
        case refundOkTransactionId = "refundOkTransactionId"
        case refundOkPaymentStatus = "refundOkPaymentStatus"
        case wholeSalePrice = "wholeSalePrice"
        case appliedExchangeRateUSD = "appliedExchangeRateUSD"
    }
}

struct InternationalDataPackTopupRequest : Codable {
    let account_number : String?
    let external_id : String?
    let product_id : String?
    let recipient : InternationalDataPackTopupRecipient?
    let recipient_sms_notification : String?
    let recipient_sms_text : String?
    let sender : InternationalDataPackTopupRecipient?
    let sender_sms_notification : String?
    let sender_sms_text : String?
    let simulation : String?

    enum CodingKeys: String, CodingKey {
        case account_number = "account_number"
        case external_id = "external_id"
        case product_id = "product_id"
        case recipient = "recipient"
        case recipient_sms_notification = "recipient_sms_notification"
        case recipient_sms_text = "recipient_sms_text"
        case sender = "sender"
        case sender_sms_notification = "sender_sms_notification"
        case sender_sms_text = "sender_sms_text"
        case simulation = "simulation"
    }
}

struct InternationalDataPackTopupTxResponse : Codable {
    let transaction_id : String?
    let simulation : String?
    let status : String?
    let status_message : String?
    let date : String?
    let account_number : String?
    let external_id : String?
    let operator_reference : String?
    let product_id : String?
    let product : String?
    let product_desc : String?
    let product_currency : String?
    let product_value : String?
    let local_currency : String?
    let local_value : String?
    let operator_id : String?
    let operatorVal : String?
    let country_id : String?
    let country : String?
    let account_currency : String?
    let wholesale_price : String?
    let retail_price : String?
    let fee : String?
    let sender : InternationalDataPackTopupRecipient?
    let recipient : InternationalDataPackTopupRecipient?
    let sender_sms_notification : String?
    let sender_sms_text : String?
    let recipient_sms_notification : String?
    let recipient_sms_text : String?
    let custom_field_1 : String?
    let custom_field_2 : String?
    let custom_field_3 : String?
    
    enum CodingKeys: String, CodingKey {
        case transaction_id = "transaction_id"
        case simulation = "simulation"
        case status = "status"
        case status_message = "status_message"
        case date = "date"
        case account_number = "account_number"
        case external_id = "external_id"
        case operator_reference = "operator_reference"
        case product_id = "product_id"
        case product = "product"
        case product_desc = "product_desc"
        case product_currency = "product_currency"
        case product_value = "product_value"
        case local_currency = "local_currency"
        case local_value = "local_value"
        case operator_id = "operator_id"
        case operatorVal = "operator"
        case country_id = "country_id"
        case country = "country"
        case account_currency = "account_currency"
        case wholesale_price = "wholesale_price"
        case retail_price = "retail_price"
        case fee = "fee"
        case sender = "sender"
        case recipient = "recipient"
        case sender_sms_notification = "sender_sms_notification"
        case sender_sms_text = "sender_sms_text"
        case recipient_sms_notification = "recipient_sms_notification"
        case recipient_sms_text = "recipient_sms_text"
        case custom_field_1 = "custom_field_1"
        case custom_field_2 = "custom_field_2"
        case custom_field_3 = "custom_field_3"
    }
}

struct InternationalDataPackTopupRecipient : Codable {
    let email : String?
    let first_name : String?
        let last_name : String?
    let middle_name : String?
    let mobile : String?
    let custom_field_1 : String?
    let custom_field_2 : String?
    let custom_field_3 : String?

    enum CodingKeys: String, CodingKey {
        case email = "email"
        case first_name = "first_name"
        case last_name = "last_name"
        case middle_name = "middle_name"
        case mobile = "mobile"
        case custom_field_1 = "custom_field_1"
        case custom_field_2 = "custom_field_2"
        case custom_field_3 = "custom_field_3"
    }
}
