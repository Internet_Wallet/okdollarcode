//
//  InternationalHomeCellTableViewCell.swift
//  OK
//
//  Created by OK$ on 28/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class InternationalHomeCell: UITableViewCell {

    @IBOutlet weak var lblPlanName: UILabel!
    @IBOutlet weak var imgPlan: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
