//
//  InternationalCountryCell.swift
//  OK
//
//  Created by OK$ on 28/05/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class InternationalCountryCell: UITableViewCell {
    @IBOutlet weak var lblCountryName: UILabel!
    @IBOutlet weak var lblCountryID: UILabel!
    @IBOutlet weak var imgFlag: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        imgFlag.layer.borderWidth = 1.0
        imgFlag.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
