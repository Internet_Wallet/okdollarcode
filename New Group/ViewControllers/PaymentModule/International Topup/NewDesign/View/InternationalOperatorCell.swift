//
//  InternationalOperatorCell.swift
//  OK
//
//  Created by OK$ on 25/06/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class InternationalOperatorCell: UITableViewCell {

    @IBOutlet weak var lblOperatorName: UILabel!
    @IBOutlet weak var lblOperatorID: UILabel!
    @IBOutlet weak var viewBG: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
