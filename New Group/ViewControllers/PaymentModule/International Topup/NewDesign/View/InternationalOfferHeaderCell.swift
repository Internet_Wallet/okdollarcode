//
//  InternationalOfferHeaderCell.swift
//  OK
//
//  Created by OK$ on 10/11/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class InternationalOfferHeaderCell: UITableViewCell {

    @IBOutlet weak var lblTitleLeft: UILabel!
    @IBOutlet var lblTitleRight: UILabel!
    @IBOutlet var viewLeftwidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viewMMK: UIView!
    @IBOutlet weak var viewUnitDesc: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
