//
//  InternationalOfferrCell.swift
//  OK
//
//  Created by OK$ on 26/06/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class InternationalOfferCell: UITableViewCell {
    @IBOutlet weak var lblOfferName: UILabel!
    @IBOutlet weak var lblOfferPrice: UILabel!
    
    @IBOutlet var fordataplanLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
