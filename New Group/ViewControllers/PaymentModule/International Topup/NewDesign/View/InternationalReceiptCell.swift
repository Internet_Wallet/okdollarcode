//
//  InternationalReceiptCell.swift
//  OK
//
//  Created by OK$ on 20/08/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class InternationalReceiptCell: UITableViewCell {

    @IBOutlet var lblMobileNumber: UILabel!
    {
        didSet {
            lblMobileNumber.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
            lblMobileNumber.text = "Mobile Number".localized
        }
    }
    @IBOutlet var lblMobileNumberValue: UILabel!
    
    @IBOutlet var lblTelecomOperator: UILabel!
       {
           didSet {
               lblTelecomOperator.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
               lblTelecomOperator.text = "Telecom Operator".localized
           }
       }
    @IBOutlet var lblTelecomOperatorValue: UILabel!

    @IBOutlet var lblAmount: UILabel!
    {
        didSet {
            lblAmount.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
            lblAmount.text = "Amount".localized
        }
    }
    @IBOutlet var lblAmountValue: UILabel!
    
    @IBOutlet var lblEasyFastSecure: UILabel!
       {
           didSet {
               lblEasyFastSecure.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
               lblEasyFastSecure.text = "Easy - Fast - Secure".localized
           }
       }
       
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
