//
//  InternationalTopupDesignWebExtension.swift
//  OK
//
//  Created by Ashish on 4/3/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

extension InternationalTopupDesignViewController {
    func initialWebCall() {
        if appDelegate.checkNetworkAvail() {
            self.getAllDetailsForInternationTopup(handle: { [weak self](tokenManager) in
                self?.getAllCountries(accessManager: tokenManager)
            })
        } else {
            self.showErrorAlert(errMessage: "Check Internet Connection")
        }
    }
    
    private func getAllDetailsForInternationTopup(handle:@escaping (_ tokenManager: InternationalTopupManager.accessManager) -> Void) {
        let url  = InternationalTopupManager.getUrl(InternationalTopupConstants.urls.getTokens)
        let hash = InternationalTopupConstants.ApiKeys.accessKey.hmac_SHA1(key: InternationalTopupConstants.ApiKeys.secretKey)
        let parameters = String.init(format: "password=%@&grant_type=password", hash)
        
        TopupWeb.genericApiWithHeader(url: url, param: parameters as AnyObject, httpMethod: "POST", header: nil) { (result, success) in
            if success {
                if let dictionary = result as? Dictionary<String,Any> {
                    let token = dictionary.safeValueForKey("access_token") as? String
                    let type  = dictionary.safeValueForKey("token_type") as? String
                    let tokenManager = InternationalTopupManager.accessManager.init(token ?? "", type: type ?? "")
                    DispatchQueue.main.async {
                       handle(tokenManager)
                    }
                }
            }
        }
    }
    
    func getAllCountries(accessManager: InternationalTopupManager.accessManager) {
        let url = InternationalTopupManager.getUrl(InternationalTopupConstants.urls.getCountries)
        let paramDict = Dictionary<String,String>()
        let headerString = String.init(format: "%@ %@", accessManager.type, accessManager.accessToken)
        let hash = InternationalTopupConstants.ApiKeys.accessKey.hmac_SHA1(key: InternationalTopupConstants.ApiKeys.secretKey)

        let request = ITRequest_allCountries.init(Bundle.main.releaseVersionNumber, build: Bundle.main.buildVersionNumber, dName: UIDevice.current.name, deviceOS: "ios", hashValue: hash)
        do {
            let _ = try JSONEncoder().encode(request)
        } catch {
            println_debug(error)
        }
        
        TopupWeb.genericApiWithHeader(url: url, param: paramDict as AnyObject, httpMethod: "GET", header: headerString) { [weak self](result, success) in
            if success {
                DispatchQueue.main.async {
                    if let dictionary = result as? Dictionary<String,Any> {
                        if let jsonData = dictionary.jsonString().data(using: .utf8) {
                            if  let internationalTopupObject = try? JSONDecoder().decode(InternationalTopupModel.self, from: jsonData) {
                                if internationalTopupObject.message.safelyWrappingString().lowercased() == "Success".lowercased() {
                                    if let countries = internationalTopupObject.results {
                                        self?.countriesDetail = countries
                                        let country = Country.init(name: "", code: InternationalTopupManager.selectedCountry.flag, dialCode: InternationalTopupManager.selectedCountry.code)
                                        self?.checkAvailableCountry(country: country) {_ in 
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func getOperatorsAccordingly(countries: [AllCountry]) {
        if let country = countries.first(where: {$0.countryCode2.safelyWrappingString() == InternationalTopupManager.selectedCountry.code}) {
            self.getAllDetailsForInternationTopup(handle: { [weak self](tokenManager) in
                DispatchQueue.main.async {
                    let paramDict = Dictionary<String,String>()
                    let headerString = String.init(format: "%@ %@", tokenManager.type, tokenManager.accessToken)
                    let urlString = String.init(format: InternationalTopupConstants.urls.getOperators, country.countryId.safelyWrappingString())
                    let url = InternationalTopupManager.getUrl(urlString)
                    TopupWeb.genericApiWithHeader(url: url, param: paramDict as AnyObject, httpMethod: "GET", header: headerString, handle: { (response, success) in
                        if success {
                            DispatchQueue.main.async {
                                if let dictionary = response as? Dictionary<String,Any> {
                                    if let jsonData = dictionary.jsonString().data(using: .utf8) {
                                        if let internationalTopupOperator = try? JSONDecoder().decode(InternationalTopupOperatorModel.self, from: jsonData) {
                                            if internationalTopupOperator.message.safelyWrappingString().lowercased() == "Success".lowercased() {
                                                if let operatorModel = internationalTopupOperator.results {
                                                    var filterModel = [InternationalTopupOperatorListModel]()
                                                    for element in operatorModel {
                                                        if let productArray = element.productList, productArray.count > 0 {
                                                            filterModel.append(element)
                                                        }
                                                    }
                                                    self?.operatorArray = filterModel.sorted(by: {$0.operatorName.safelyWrappingString() < $1.operatorName.safelyWrappingString()})
                                                    self?.operatorTableView.reloadData()
                                                    self?.operatorTableView.isHidden = false
                                                    self?.isOnOperatorListArrowShowing(isOn: true)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    })
                }
            })
        }
    }
}

struct ITRequest_allCountries: Codable {
    
    let appVersion: String
    let buildVersion: String
    let deviceName: String
    let deviceOs: String
    let hashValue: String
    
    init(_ appVer: String, build: String, dName: String, deviceOS: String, hashValue: String) {
        self.appVersion = appVer
        self.buildVersion = build
        self.deviceName = dName
        self.deviceOs = deviceOS
        self.hashValue = hashValue
    }

    enum CodingKeys: String, CodingKey {
        case appVersion = "AppVersion"
        case buildVersion = "BuildVersion"
        case deviceName = "DeviceName"
        case deviceOs = "DeviceOs"
        case hashValue = "HashValue"
    }
}
