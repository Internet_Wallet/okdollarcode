//
//  InternationalDataPlanModel.swift
//  OK
//
//  Created by OK$ on 19/02/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

//MARK:- Data Plan Model
struct InternationalDataPlanModel {
    var details  = ""
    var size     = ""
    var amount   = ""
    var isActive = true
    var attributedAmount = NSAttributedString.init(string: "")
    var developerAmount = NSString.init()
    var ussdCode = ""
    

    init(_ details: String,_ size: String, _ amount: String, _ isActive: Bool, _ ussd: String) {
        self.details  = details
        self.size     = size
        self.isActive = isActive
        self.ussdCode = ussd
        self.developerAmount =  NSString.init(string: amount)
        
        let amt = (amount as NSString).intValue
        let nsAmount = NSNumber.init(value: amt)
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NumberFormatter.Style.decimal
        if let amtnt  = currencyFormatter.string(from: nsAmount) {
            self.amount = amtnt
        } else {
            self.amount = amount
        }
        
        let mmk = [NSAttributedString.Key.font: UIFont(name: appFont, size: 12.0) ?? UIFont.systemFont(ofSize: 12)]
        let amtMMK = [NSAttributedString.Key.font: UIFont(name: appFont, size: 15.0) ?? UIFont.systemFont(ofSize: 15)]
        let mmkString = NSMutableAttributedString(string: " MMK", attributes: mmk)
        let amountStr    = NSMutableAttributedString.init(string: self.amount, attributes: amtMMK)
        
        let stringAttributed = NSMutableAttributedString()
        stringAttributed.append(amountStr)
        stringAttributed.append(mmkString)
        
        self.attributedAmount = stringAttributed
        
    }
}
