//
//  InternationalTopupOperatorModel.swift
//  OK
//
//  Created by Ashish on 4/4/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct InternationalTopupOperatorModel : Codable {
    
    let message : String?
    let result : String?
    let results : [InternationalTopupOperatorListModel]?
    let status : String?
    let statusCode : String?

    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case result = "Result"
        case results = "Results"
        case status = "Status"
        case statusCode = "StatusCode"
    }
}

struct InternationalTopupOperatorListModel : Codable {
    
    let countryId : String?
    let createdDate : String?
    let isActive : String?
    let operatorCode : String?
    let operatorId : String?
    let operatorLogo : String?
    let operatorName : String?
    let productList : [String]?

    enum CodingKeys: String, CodingKey {
        case countryId = "CountryId"
        case createdDate = "CreatedDate"
        case isActive = "IsActive"
        case operatorCode = "OperatorCode"
        case operatorId = "OperatorId"
        case operatorLogo = "OperatorLogo"
        case operatorName = "OperatorName"
        case productList = "ProductList"
    }
}
