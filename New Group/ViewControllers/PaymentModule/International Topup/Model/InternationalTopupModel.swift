//
//  InternationalTopupModel.swift
//  OK
//
//  Created by Ashish on 4/3/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct InternationalTopupModel: Codable {
    let message: String?
    let result: String?
    let results: [AllCountry]?
    let status: String?
    let statusCode: String?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case result  = "Result"
        case results = "Results"
        case status  = "Status"
        case statusCode = "StatusCode"
    }
}

struct AllCountry: Codable {
    let countryCode: String?
    let countryCode2: String?
    let countryFlag: String?
    let countryId: String?
    let countryName: String?
    let countryNameBurmese: String?
    let createdDate: String?
    let currencyDetail: CurrencyDetail?
    let isActive: String?
    let country: String?

    enum CodingKeys: String, CodingKey {
        case countryCode = "CountryCode"
        case countryCode2 = "CountryCode2"
        case countryFlag = "CountryFlag"
        case countryId = "CountryId"
        case countryName = "CountryName"
        case countryNameBurmese = "CountryNameBurmese"
        case createdDate = "CreatedDate"
        case currencyDetail = "CurrencyDetail"
        case isActive = "IsActive"
        case country = "Country"
    }
}

struct CurrencyDetail: Codable {
    let countryName: String?
    let createdDate: String?
    let currencyDetailId: String?
    let currencyType: String?
    let exchangeRate: String?
    
    enum CodingKeys: String, CodingKey {
        case countryName = "CountryName"
        case createdDate = "CreatedDate"
        case currencyDetailId = "CurrencyDetailId"
        case currencyType = "CurrencyType"
        case exchangeRate = "ExchangeRate"
    }
}

struct MobNumValidationResponse: Codable {
    let code: Int
    let data, msg: String?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case msg = "Msg"
    }
}

struct MobNumDataResponse: Codable {
    let mobileAppVersion: [MobileAppVersion]?
    let version: [PhonenumberValidationVersion]?
    let kycAppControlInfo: [KycAppControlInfo]
    let serviceControlAppInfo: [ServiceControlAppInfo]
    let telecoTopupSubscriptionValue : [TelecoTopupSubscriptionValue]?
    
    enum CodingKeys: String, CodingKey {
        case mobileAppVersion = "MobileAppVersion"
        case version = "PhonenumberValidationVersion"
        case kycAppControlInfo = "KycAppControlInfo"
        case serviceControlAppInfo = "ServiceControlAppInfo"
        case telecoTopupSubscriptionValue = "TelecoTopupSubscriptionValue"
    }
}

struct MobileAppVersion: Codable {
    let latestVersion: String
    let url: String
    let type, appVersion, newUpdate: String
    let bankRoutingapiversion: String?
    
    enum CodingKeys: String, CodingKey {
        case latestVersion = "LatestVersion"
        case url = "Url"
        case type = "Type"
        case appVersion = "AppVersion"
        case newUpdate = "NewUpdate"
        case bankRoutingapiversion = "BankRoutingapiversion"
    }
}

struct KycAppControlInfo: Codable {
    let kycApprovalStatus, kycBusinessApprovalStatus, kycSignupPriority, kycTakingPhotoRights: Int
    let registrationStatus: String
    let upUpdateControlStatus: Int
    let sendmoneyToBankVersion, locationupdateVersion, isMerchantPayment, OkToBankType: Int?
    let imageUrl, validityDate, visible: String?
    let numberofTimes, perdayShowCount, agentType: String?
    
    enum CodingKeys: String, CodingKey {
        case kycApprovalStatus = "KycApprovalStatus"
        case kycBusinessApprovalStatus = "KycBusinessApprovalStatus"
        case kycSignupPriority = "KycSignupPriority"
        case kycTakingPhotoRights = "KycTakingPhotoRights"
        case registrationStatus = "RegistrationStatus"
        case upUpdateControlStatus = "UpUpdateControlStatus"
        case locationupdateVersion = "LocationupdateVersion"
        case sendmoneyToBankVersion = "SendmoneyToBankVersion"
        case isMerchantPayment = "isMerchantPayment"
        case OkToBankType = "OkToBankType"
        case imageUrl = "Imageurl"
        case validityDate = "Validitydate"
        case numberofTimes = "Numberoftimes"
        case visible = "Visible"
        case perdayShowCount = "PerdayshowCount"
        case agentType = "AgentType"
    }
}

struct TelecoTopupSubscriptionValue: Codable {
    let name: String?
//    let mart: String?
    let serviceAcess: String?
    let cashBackConfigList: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case serviceAcess = "ServiceAcess"
        case cashBackConfigList = "CashBackConfigList"
    }
}

struct CashBackConfigList: Codable {
    let telenor: String?
    let ooredoo: String?
    let mpt: String?
    let mytel: String?
    let mecTel: String?
    let oversasTopup: String?

    enum CodingKeys: String, CodingKey {
        case telenor = "Telenor"
        case ooredoo = "Ooredoo"
        case mpt = "Mpt"
        case mytel = "Mytel"
        case mecTel = "Mectel"
        case oversasTopup = "OversasTopup"
    }
}


struct ServiceControlAppInfo: Codable {
    let id, serviceName: String
    let overses, myanmar: Int
    let isActive: Bool
    let createdDate: String
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case serviceName = "ServiceName"
        case overses = "Overses"
        case myanmar = "Myanmar"
        case isActive = "IsActive"
        case createdDate = "CreatedDate"
    }
}


struct OtherAppApkInfo: Codable {
    let id, appName: String
    let url: String
    let canBeDownload, fileSize, updateType: Int
    let packageName: String
    let isActive: Bool
    let createdDate, updatedDate: String
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case appName = "AppName"
        case url = "Url"
        case canBeDownload = "CanBeDownload"
        case fileSize = "FileSize"
        case updateType = "UpdateType"
        case packageName = "PackageName"
        case isActive = "IsActive"
        case createdDate = "CreatedDate"
        case updatedDate = "UpdatedDate"
    }
}

struct PhonenumberValidationVersion: Codable {
    let validationJSON: String?
    
    enum CodingKeys: String, CodingKey {
        case validationJSON = "ValidateJson"
    }
}

struct ValidationListResponse: Codable {
    let code: Int?
    let msg: String?
    let validations: [PhNumValidationList]?
}

struct PhNumValidationList: Codable {
    let countryCode, countryName: String?
    let phOperators: [PhOperators]?
    
    enum CodingKeys: String, CodingKey {
        case countryCode, countryName
        case phOperators = "operators"
    }
}

struct PhOperators: Codable {
    let numberSeries: [String]?
    let operatorName: String?
    let maxLenth, minLength: Int?
    let isActive: Bool?
    let operatorColor: String?
    
    enum CodingKeys: String, CodingKey {
        case numberSeries
        case operatorName = "OperatorName"
        case maxLenth, minLength, isActive
        case operatorColor = "OperatorColor"
    }
}
