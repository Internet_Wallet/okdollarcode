//
//  InternationalTopUpDataOfferViewController.swift
//  OK
//
//  Created by Kethan Kumar on 30/10/2019.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class InternationalTopUpDataOfferViewController: OKBaseController {
    
    @IBOutlet weak var dataOfferLbl: UILabel! {
        didSet {
            self.dataOfferLbl.text = self.dataOfferLbl.text?.localized
        }
    }

  
    //MARK: - View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}
