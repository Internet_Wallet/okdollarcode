//
//  InternationalTopupDesignViewController.swift
//  OK
//
//  Created by Ashish on 4/3/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol InternationalTopupSliderDelegate : class {
    func didSlideOpen(flag: Bool)
}

class InternationalTopupDesignViewController: OKBaseController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var selectOperatorView: UIView!
    @IBOutlet weak var mobileNumber: MobileRightViewSubClassTextField!
    @IBOutlet weak var confirmMobileNumber: MobileRightViewSubClassTextField! {
        didSet {
            self.confirmMobileNumber.cnf = true
        }
    }
    @IBOutlet weak var selectOperator: RightViewSubClassTextField!
    
    @IBOutlet weak var selectAmount: RightViewSubClassTextField!
    @IBOutlet weak var operatorTableView: UITableView!
    @IBOutlet weak var rechargePlanContainerView: UIView!
    
    func placeholder() {
        self.mobileNumber.placeholder = "Enter Mobile Number".localized
        self.confirmMobileNumber.placeholder = "Confirm Mobile Number".localized
        self.selectOperator.placeholder = "Select Operator".localized
        self.selectAmount.placeholder = "Select Amount".localized
    }
    
    var scroller : PaytoScroller?
    
    // leftviews & rightViews
    var mobileNumberLeftView         = PaytoViews.updateView()
    var confirmMobileNumberLeftView  = PaytoViews.updateView()
    var confMobile : PTClearButton?
    var mobileNumberRightView        = ContactRightView.updateView()
    var selectOperatorLeftView       = DefaultIconView.updateViews(icon: "rechargeTower")
    var selectOperatorRightView      = ExpandCollapseIconView.updateView(icon: "down_arrow_small")
    var selectAmountLeftView         = DefaultIconView.updateView(icon: "amount_bill")
    var contactViewLeft              = DefaultIconView.updateView(icon: "newpayuser")
    
    var operatorArray : [InternationalTopupOperatorListModel]?
    
    var countriesDetail : [AllCountry]?
    
    var validation : (min:Int,max:Int)?
    
    var navigation: UINavigationController?
    
    var internationalDesign: InternationalTopupDesignViewController?
    
    weak var delegate : InternationalTopupSliderDelegate?
    
    weak var rechargePlan : InternationalTopupRechargePlanViewController?
    
    var userObject : (number: String, name: String)?

    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        validation = InternationalTopupManager().validations(countryCode: InternationalTopupManager.selectedCountry.code)
        self.initialSetup()
        self.placeholder()
        self.initialSetupForTextfields()
        self.initialWebCall()
        
        self.operatorTableView.tableHeaderView = UIView.init()
        self.rechargePlanContainerView.isHidden = true
        
        if let obj = userObject {
            let favObj = FavModel.init("", phone: obj.number, name: obj.name, createDate: "", agentID: "", type: "")
            let userContact = ContactPicker.init(object: favObj)
            self.decodeContact(contact: userContact)
        }
        mobileNumber.becomeFirstResponder()
    }
    
   

    func initialSetup() {
        self.confirmMobileNumber.isHidden = true
        self.selectOperator.isHidden = true
        self.selectAmount.isHidden = true
        self.operatorTableView.isHidden = true
    }
    
    //MARK:- Segue connections
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == InternationalTopupConstants.storyboardConnections.rechargeSegue {
            if let rechargeVC = segue.destination as? InternationalTopupRechargePlanViewController {
                self.rechargePlan = rechargeVC
                rechargeVC.navigation = self.navigation
                rechargeVC.internationalRechargeController = self
            }
        }
    }
    
    //MARK:- UITableViewDelegate & UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.operatorArray != nil) ? self.operatorArray!.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: InternationalOperatorCellClass.self), for: indexPath) as? InternationalOperatorCellClass
        if let operatorArry = self.operatorArray {
            let operatorDetail = operatorArry[indexPath.row]
                cell?.wrapCellData(operatorDetail.operatorName.safelyWrappingString())
        }
        return (cell != nil) ? cell! : UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let operatorDetail = self.operatorArray {
            InternationalTopupManager.selectedOperatorObject = operatorDetail[indexPath.row]
            if let operatorObject = InternationalTopupManager.selectedOperatorObject {
                self.selectOperator.text = operatorObject.operatorName.safelyWrappingString()
                self.navigation?.navigationBar.barTintColor = MyNumberTopup.OperatorColorCode.telenor
                scroller?.selectionIndicatorView.backgroundColor = MyNumberTopup.OperatorColorCode.telenor
                scroller?.selectionIndicatorColor = MyNumberTopup.OperatorColorCode.telenor
                self.animateTableHiding(flag: true)
                self.isOnOperatorListArrowShowing(isOn: false)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.00
    }
    
    private func animateTableHiding(flag: Bool) {
        self.selectAmount.isHidden = !flag
        self.operatorTableView.isHidden = flag
        
        if let delegate = self.delegate {
            delegate.didSlideOpen(flag: flag)
        }
    }
    
    func changeToDefaultNavigationColor() {
        if let nav = self.navigation {
            nav.navigationBar.barTintColor = MyNumberTopup.OperatorColorCode.okDefault
            scroller?.selectionIndicatorView.backgroundColor = MyNumberTopup.OperatorColorCode.okDefault
            scroller?.selectionIndicatorColor = MyNumberTopup.OperatorColorCode.okDefault
        }
    }
}

//MARK:- InternationalOperatorCellClass
class InternationalOperatorCellClass : UITableViewCell {
    
    @IBOutlet weak var operatorLabel: UILabel!
    
    func wrapCellData(_ name: String) {
        self.operatorLabel.text = name
    }

}


//MARK: - PTClearButtonDelegate
extension InternationalTopupDesignViewController: PTClearButtonDelegate {
    func clearField(strTag: String) {
        if strTag.hasPrefix("confMobile") {
            self.confirmMobileNumber.leftViewMode = .always
            self.confirmMobileNumber.text = ""
            self.selectOperator.text = ""
            self.confirmMobileNumber.becomeFirstResponder()
            self.selectAmount.text = ""
            self.selectAmount.isHidden = true
            self.selectOperator.isHidden = true
            self.operatorTableView.isHidden = true
            self.confMobile?.hideClearButton(hide: true)
        }
    }
}

extension InternationalTopupDesignViewController{
    
    fileprivate func setUI(){
        let dropDownTap = UITapGestureRecognizer(target: self, action: #selector(self.handleTapOnDropDown(_:)))
      selectOperatorView.addGestureRecognizer(dropDownTap)
        
       let onClickNetworkTap = UITapGestureRecognizer(target: self, action: #selector(self.handleTapOnNetwork(_:)))
       selectOperatorLeftView.addGestureRecognizer(onClickNetworkTap)
    }
    
   @objc func handleTapOnDropDown(_ sender: UITapGestureRecognizer? = nil) {
      showOperatorTable()
   }
    
    @objc func handleTapOnNetwork(_ sender: UITapGestureRecognizer? = nil) {
        showOperatorTable()
    }
    
    fileprivate func showOperatorTable(){
        
        if let s = self.operatorArray, s.count > 0 {
            if self.operatorTableView.isHidden {
                self.operatorTableView.isHidden = false
                self.isOnOperatorListArrowShowing(isOn: true)
                self.selectAmount.isHidden = true
                selectOperator.text = ""
            } else {
                self.operatorTableView.isHidden = true
                self.isOnOperatorListArrowShowing(isOn: false)
            }
        } else {
            guard let countryList = self.countriesDetail else { return  }
            self.selectAmount.isHidden = true
            selectOperator.text = ""
            changeToDefaultNavigationColor()
            self.getOperatorsAccordingly(countries: countryList)
        }
    }
    
    
}
