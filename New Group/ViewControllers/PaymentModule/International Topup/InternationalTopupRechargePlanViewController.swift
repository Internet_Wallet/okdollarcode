//
//  InternationalTopupRechargePlanViewController.swift
//  OK
//
//  Created by Ashish on 4/5/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class InternationalTopupRechargePlanViewController: OKBaseController, UITableViewDelegate, UITableViewDataSource, BioMetricLoginDelegate {
    
    var rechargeModel : [ITRechargeAmountModel]?
    
    @IBOutlet weak var rechargeModelTableView : UITableView!
    
    var customerNumber : String?
    
    fileprivate var transaction : Dictionary<String,Any> = [:]
    
    var navigation: UINavigationController?
    
    fileprivate var confirmRespModel : ITConfirmationResponseModel?
    fileprivate var rechargeObj    : ITRechargeAmountModel?
    
    var internationalRechargeController: InternationalTopupDesignViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.rechargeModelTableView.tableFooterView = UIView.init()
    }
    
    func regeneratingRechargePlan() {
        
        guard let selectedCountry = InternationalTopupManager.selectedCountryObject else { return }
        
        guard let currencyDetail  = selectedCountry.currencyDetail else { return }
        
        guard let selectedOperator = InternationalTopupManager.selectedOperatorObject else { return }
        
        guard let amountList = selectedOperator.productList else { return }
        
        var arrayModel = [ITRechargeAmountModel]()
        
        for amount in amountList {
            if let exchenagedRate = currencyDetail.exchangeRate {
                var curTit: String?
                if let cType = currencyDetail.currencyType {
                    curTit = cType
                }
                let modelAmount = ITRechargeAmountModel.init(amount, exchangeRate: exchenagedRate, currencyTitle: curTit)
                arrayModel.append(modelAmount)
            }
        }
        
        if arrayModel.count > 0 {
            self.rechargeModel = arrayModel.sorted(by: { ($0.localAmount! as NSString).floatValue < ($1.localAmount! as NSString).floatValue })
            self.rechargeModelTableView.isHidden = false
            self.rechargeModelTableView.reloadData()
        } else {
            self.rechargeModelTableView.isHidden = true
            self.showErrorAlert(errMessage: "No records found!".localized)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        GeoLocationManager.shared.startUpdateLocation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        GeoLocationManager.shared.stopUpdateLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (rechargeModel == nil) ? 0 : rechargeModel!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: InternationalTopupRechargeAmountCell.self), for: indexPath) as? InternationalTopupRechargeAmountCell
        if let model = self.rechargeModel {
            cell?.wrapCell(model: model[indexPath.row])
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.00
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let modelArr = rechargeModel {
            let model = modelArr[indexPath.row]
            if (UserLogin.shared.walletBal as NSString).floatValue < (model.localAmount.safelyWrappingString() as NSString).floatValue {
                alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                    if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                        addWithdrawView.modalPresentationStyle = .fullScreen
                        self.navigation?.present(addWithdrawView, animated: true, completion: nil)
                    }
                })
                alertViewObj.showAlert()
            } else {
                self.rechargeConfirmationApi(model: model)
            }
        }
    }
    
    //MARK:- Api calling
    func rechargeConfirmationApi(model: ITRechargeAmountModel) {
        
         guard let selectedOperator = InternationalTopupManager.selectedOperatorObject else { return }
        
         guard let operatorName = selectedOperator.operatorName else { return }
        
         guard let customerMobile = customerNumber else { return }
        
         let encodedOperatorName = operatorName.replacingOccurrences(of: " ", with: "")
        
        var stringUrl = String.init(format: InternationalTopupConstants.urls.getConfirmation, UserModel.shared.mobileNo,GeoLocationManager.shared.currentLatitude ?? "", GeoLocationManager.shared.currentLongitude ?? "", "", encodedOperatorName, model.localAmount.safelyWrappingString() , customerMobile, UserLogin.shared.walletBal)
       stringUrl = stringUrl + "&SubScriberName=" + (telecoTopupSubscription?.first?.name ?? "").replacingOccurrences(of: " ", with: "%20")
        guard let url = InternationalTopupManager.getServerAppUrl(stringUrl) else { return }
        println_debug(url)
        
        let param = Dictionary<String,Any>()
        TopupWeb.genericClass(url: url, param: param as AnyObject, httpMethod: "GET") { [weak self](response, success) in
            if success {
                DispatchQueue.main.async {
                    if let dictionary = response as? Dictionary<String,Any> {
                        if let code = dictionary.safeValueForKey("Code") as? NSNumber, code == 200 {
                            if let stringJson = dictionary.safeValueForKey("Data") as? String {
                                if let arrDict = OKBaseController.convertToArrDictionary(text: stringJson) {
                                    guard let objectAny = arrDict.first else { return }
                                    guard let dic = objectAny as? Dictionary<String, Any>  else { return }
                                    if let convertedJson = dic.jsonString().data(using: .utf8) {
                                        if let confirmModel = try? JSONDecoder().decode(ITConfirmationResponseModel.self, from: convertedJson) {
                                            self?.rechargeObj = model
                                            self?.checkKickbackApi(model: confirmModel)
                                        }
                                    }
                                }
                            }
                        } else {//Gauri Mar13
                            self?.showErrorAlert(errMessage:"Your Service Provider is Currently Unavailable.".localized)
                        }
                    }
                }
            }
        }
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if isSuccessful == false { return }
        
        guard let model = confirmRespModel else { return }
        
        guard let recharge = rechargeObj else { return }
        
        let urlString = String(format: InternationalTopupConstants.urls.getKickbackInfo, UserModel.shared.mobileNo, model.mobileNumber.safelyWrappingString(), recharge.localAmount.safelyWrappingString(), ok_password.safelyWrappingString(), OKBaseController.getIPAddress().safelyWrappingString(), "ios", UserLogin.shared.token)
        
        guard let url = InternationalTopupManager.getServerEstel(urlString) else { return  }
        
        let param = Dictionary<String,Any>()
 
        TopupWeb.genericClassXML(url: url, param: param as AnyObject, httpMethod: "POST") { [weak self](response, success) in
            DispatchQueue.main.async {
                if success {
                    if let xmlString = response as? String {
                        let xml = SWXMLHash.parse(xmlString)
                        self?.enumerate(indexer: xml)
                        if self?.transaction.safeValueForKey("resultdescription") as? String ?? "" == "Transaction Successful" {
                            
                            let vc = self?.storyboard?.instantiateViewController(withIdentifier: "CommonTopupTransactionViewController") as? CommonTopupTransactionViewController
                            if let controller = self?.internationalRechargeController {
                                self?.self.transaction["internationalNumber"] = controller.mobileNumber.text.safelyWrappingString()
                            }

                            self?.transaction["userNumber"] = self?.internationalRechargeController?.mobileNumber.text.safelyWrappingString()
                            if let transact = self?.transaction {
                                vc?.arrDictionary = [transact]
                                vc?.isInternationalMobileNumber = ""
                                vc?.isInternationl = true
                                vc?.currency = recharge.amount.safelyWrappingString()
                                self?.navigation?.pushViewController(vc!, animated: true)
                            }
                        }
                    }
                }
            }
        }
    }

    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            if let element = child.element {
                transaction[element.name] = element.text
            }
            enumerate(indexer: child)
        }
    }

    
    func checkKickbackApi(model: ITConfirmationResponseModel) {
        confirmRespModel = model
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: InternationalTopupConstants.msgs.screenConfirm, delegate: self)
        } else {
            self.biometricAuthentication(isSuccessful: true, inScreen: InternationalTopupConstants.msgs.screenConfirm)
        }
    }
    
}

class InternationalTopupRechargeAmountCell : UITableViewCell {
    
    @IBOutlet weak var amountDisplay : UILabel!
    @IBOutlet weak var kyatAmountDisplay : UILabel!
    
    func wrapCell(model: ITRechargeAmountModel) {
        //self.amountDisplay.text = model.amount.safelyWrappingString()
        if let amtAtrStr = model.displayAmount {
            self.amountDisplay.attributedText = amtAtrStr
        }
        if let attr = model.localDisplayAmount {
            self.kyatAmountDisplay.attributedText = attr
        }
    }
    
}

struct ITRechargeAmountModel {
    var amount : String?
    var displayAmount: NSMutableAttributedString?
    var localAmount : String?
    var localDisplayAmount : NSMutableAttributedString?
    
    init(_ amount: String, exchangeRate rate: String, currencyTitle: String? = nil) {
        
        //Local amount
        let isdAmount = (amount as NSString).floatValue
        let exRate = (rate as NSString).floatValue
        let finalAmount = isdAmount * exRate
        self.localAmount = String(format: "%.2f", finalAmount)
        
        //Local amount with currency
        let nsAmount = NSNumber.init(value: finalAmount)
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NumberFormatter.Style.decimal
        
        var formatAmount = ""
        if let amtnt  = currencyFormatter.string(from: nsAmount) {
            formatAmount = amtnt
        } else {
            formatAmount = String(format: "%.2f", formatAmount)
        }
        let amountAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 15.0) ?? UIFont.systemFont(ofSize: 15)]
        let localAmountAttrStr    = NSMutableAttributedString.init(string: formatAmount, attributes: amountAttr)
        
        let currAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 12.0) ?? UIFont.systemFont(ofSize: 12)]
        let localCurrAttrStr = NSMutableAttributedString(string: " MMK", attributes: currAttr)

        let stringAttributed = NSMutableAttributedString()
        stringAttributed.append(localAmountAttrStr)
        stringAttributed.append(localCurrAttrStr)
        self.localDisplayAmount = stringAttributed
        
        //self.amount = amount
        //Overseas amount
        self.amount = String(format: "%.2f", isdAmount)
        
        //Overseas amount with currency
        var oFormatAmount = ""
        let oNSAmount = NSNumber.init(value: isdAmount)
        if let amtnt  = currencyFormatter.string(from: oNSAmount) {
            oFormatAmount = amtnt
        } else {
            oFormatAmount = String(format: "%.2f", oFormatAmount)
        }
        self.displayAmount = NSMutableAttributedString()
        if let cTitle = currencyTitle {
            let oAmountAttrStr    = NSMutableAttributedString.init(string: oFormatAmount, attributes: amountAttr)
            let overseasCurrAttrStr = NSMutableAttributedString(string: " \(cTitle)", attributes: currAttr)
            let oStringAttributed = NSMutableAttributedString()
            oStringAttributed.append(oAmountAttrStr)
            oStringAttributed.append(overseasCurrAttrStr)
            self.displayAmount = oStringAttributed
        }
    }
}

struct ITResponseKickbackModel : Codable {
    
    let agentcode : String?
    let agentname : String?
    let amount : String?
    let clientip : String?
    let clientos : String?
    let clienttype : String?
    let comments : String?
    let destination : String?
    let estel : String?
    let fee : String?
    let header : String?
    let kickback : String?
    let limit : Bool?
    let limittype : String?
    let requestcts : String?
    let response : String?
    let responsetype : String?
    let resultcode : String?
    let resultdescription : String?
    let securetoken : String?
    let source : String?
    let transid : String?
    let transtype : String?
    let vendorcode : String?
    
    
    enum CodingKeys: String, CodingKey {
        case agentcode = "agentcode"
        case agentname = "agentname"
        case amount = "amount"
        case clientip = "clientip"
        case clientos = "clientos"
        case clienttype = "clienttype"
        case comments = "comments"
        case destination = "destination"
        case estel = "estel"
        case fee = "fee"
        case header = "header"
        case kickback = "kickback"
        case limit = "limit"
        case limittype = "limittype"
        case requestcts = "requestcts"
        case response = "response"
        case responsetype = "responsetype"
        case resultcode = "resultcode"
        case resultdescription = "resultdescription"
        case securetoken = "securetoken"
        case source = "source"
        case transid = "transid"
        case transtype = "transtype"
        case vendorcode = "vendorcode"
    }
}

struct ITConfirmationResponseModel : Codable {
    
    let operatorName : String?
    let mobileNumber : String?

    enum CodingKeys: String, CodingKey {
        case operatorName = "Operator"
        case mobileNumber = "MobileNumber"
    }
}

