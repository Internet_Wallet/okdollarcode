//
//  InternationalTopupHelperFunctions.swift
//  OK
//
//  Created by Ashish on 4/4/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

extension InternationalTopupDesignViewController: CountryLeftViewDelegate, ContactPickerDelegate, CountryViewControllerDelegate, FavoriteSelectionDelegate, PTMultiSelectionDelegate {
    
    // initial setup for textfiels
     func initialSetupForTextfields() {
        self.mobileNumber.leftViewMode = .always
        self.mobileNumber.leftView     = self.mobileNumberLeftView
        
        self.confirmMobileNumber.leftViewMode = .always
        self.confirmMobileNumber.leftView     = self.confirmMobileNumberLeftView
        self.confirmMobileNumber.isUserInteractionEnabled = true
        self.mobileNumber.rightViewMode = .always
        self.mobileNumber.rightView     = self.mobileNumberRightView
        self.mobileNumberRightView.hideClearButton(hide: true)
        self.confMobile   = PTClearButton.updateView(strTag: "confMobile", delegate: self)
        self.confirmMobileNumber.rightView = self.confMobile
        self.confirmMobileNumber.rightViewMode = .always
        self.confMobile?.hideClearButton(hide: true)
        self.selectAmount.leftViewMode = .always
        self.selectAmount.leftView     = selectAmountLeftView
        
        // leftview & rightView marking
        self.mobileNumberLeftView.delegate  = self
        self.mobileNumberRightView.delegate = self
        
        //formatting country
        self.mobileNumberLeftView.wrapCountryViewData(img: InternationalTopupManager.selectedCountry.flag, str: InternationalTopupManager.selectedCountry.code)
        self.confirmMobileNumberLeftView.wrapCountryViewData(img: InternationalTopupManager.selectedCountry.flag, str: InternationalTopupManager.selectedCountry.code)
        
        self.selectOperator.leftViewMode = .always
        self.selectOperator.leftView     = selectOperatorLeftView
        self.selectOperator.rightViewMode = .always
        self.selectOperator.rightView = selectOperatorRightView
    }
    
    func isOnOperatorListArrowShowing(isOn: Bool) {
        self.selectOperator.rightView = nil
        if isOn {
            self.selectOperator.rightView = ExpandCollapseIconView.updateView(icon: "up_arrow_small")
        } else {
            self.selectOperator.rightView = ExpandCollapseIconView.updateView(icon: "down_arrow_small")
        }
    }
    
    func checkAvailableCountry(country: Country, handle: @escaping (Bool) -> Void) {
        let countryFirst = self.countriesDetail?.first(where: {$0.countryCode2.safelyWrappingString() == country.dialCode})
        if countryFirst == nil {
            //InternationalTopupManager().internationalTopupAlert(title: "", body: InternationalTopupConstants.msgs.serviceNotAvail.localized)
                handle(false)
        } else {
            InternationalTopupManager.selectedCountryObject = countryFirst
            InternationalTopupManager.selectedCountry       = (country.dialCode,country.code)
            
            self.mobileNumberLeftView.wrapCountryViewData(img: InternationalTopupManager.selectedCountry.flag, str: InternationalTopupManager.selectedCountry.code)
            self.confirmMobileNumberLeftView.wrapCountryViewData(img: InternationalTopupManager.selectedCountry.flag, str: InternationalTopupManager.selectedCountry.code)
            
            self.confirmMobileNumber.leftView = nil
            self.mobileNumber.leftView = nil
            
            self.mobileNumber.leftView = self.mobileNumberLeftView
            //self.confirmMobileNumber.leftView = self.confirmMobileNumberLeftView
            self.confirmMobileNumber.leftView = self.contactViewLeft
            self.confirmMobileNumber.isUserInteractionEnabled = true
            if let recharge = self.rechargePlan {
                recharge.regeneratingRechargePlan()
            }
            handle(true)
        }
    }
    
    func getTopupAmountList() {
        if let rechargeAmount = rechargePlan {
            rechargeAmount.customerNumber = self.mobileNumber.text ?? ""
            rechargeAmount.regeneratingRechargePlan()
        }
        
        UIView.animate(withDuration: 0.3) {
            self.rechargePlanContainerView.isHidden = false
        }
    }
    
    
    //MARK:- Country & Contact Opening Actions
    func openAction(type: ButtonType) {
        self.view.endEditing(true)
        switch type {
        case .countryView:
            self.openCountryScreen()
        case .contactView:
            PTManagerClass.openMultiContactSelection(self, hideScan: true)
        }
    }
    
    func didSelectOption(option: PTMultiSelectionOption) {
        switch option {
        case .favorite:
            let nav = openFavoriteFromNavigation(self, withFavorite: .overseas)
            if let navigation = self.navigationController {
                navigation.present(nav, animated: true, completion: nil)
            } else if let navs = self.navigation {
                navs.present(nav, animated: true, completion: nil)
            }
        case .contact:
            self.openContactScreen()
        case .scanQR:
            break
        }
    }
    
    func selectedFavoriteObject(obj: FavModel) {
        let contact = ContactPicker.init(object: obj)
        self.decodeContact(contact: contact)
    }
    
    func openCountryScreen() {
        if let navigation = self.navigationController {
            navigation.present(countryViewController(delegate: self), animated: true, completion: nil)
        } else if let nav = self.navigation {
            nav.present(countryViewController(delegate: self), animated: true, completion: nil)
        }
    }
    
    func openContactScreen() {
        let navContact = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: true)
     //   let navContact = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false)
        if let navigation = self.navigationController {
            navigation.present(navContact, animated: true, completion: nil)
        } else if let nav = self.navigation {
            nav.present(navContact, animated: true, completion: nil)
        }
    }
    
    func clearActionType() {
        self.mobileNumber.becomeFirstResponder()
        self.confirmMobileNumber.text = ""
        self.mobileNumber.text = ""
        self.confirmMobileNumber.isHidden = true
        self.selectOperator.isHidden = true
        self.selectAmount.isHidden = true
        self.selectAmount.text = ""
        self.selectOperator.text = ""
        self.operatorTableView.isHidden = true
        self.mobileNumberRightView.hideClearButton(hide: true)
        changeToDefaultNavigationColor()
        self.mobileNumber.becomeFirstResponder()
    }
    
    //MARK:- Country Delegates
    func countryViewController(_ list: CountryViewController, country: Country) {
        self.clearActionType()
        if country.dialCode == "+95" {
            self.showErrorAlert(errMessage: "Please go to Topup Other Number for Myanmar Country Topup".localized)
            return
        }
        
        self.checkAvailableCountry(country: country) { isAvailable in
            if isAvailable {
                self.operatorArray = []
            }
        }
         list.dismiss(animated: true, completion: nil)
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
         list.dismiss(animated: true, completion: nil)
    }

    //MARK:- Contact Delegates
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        self.operatorTableView.isHidden = true
        self.decodeContact(contact: contact)
    }
    
    // MARK:- Extra Methods: For Incoming protocols from different module
    
    func decodeContact(contact: ContactPicker) {
        func clearFields() {
            self.confirmMobileNumber.text     = ""
            self.mobileNumber.text = ""
            self.confirmMobileNumber.isHidden = true
            self.selectOperator.isHidden = true
            self.selectAmount.isHidden   = true
            self.selectOperator.text = ""
            self.selectAmount.text = ""
        }
        func invalidNumberSelectionAlert() {
            alertViewObj.wrapAlert(title: nil, body: "You have selected invalid number".localized,
                                   img: #imageLiteral(resourceName: "dashboard_overseas_recharge"))
            alertViewObj.addAction(title: "OK", style: .cancel, action: {})
            alertViewObj.showAlert()
        }
        func isMobNumberWithinRange(minValue: Int, maxValue: Int, phoneNumber: String) -> Bool {
            if !(phoneNumber.count >= minValue && phoneNumber.count <= maxValue) {
                invalidNumberSelectionAlert()
                return false
            }
            return true
        }
        func localNumberSelectionAlert() {
            alertViewObj.wrapAlert(title: nil, body: "You have selected Myanmar local number".localized, img: #imageLiteral(resourceName: "dashboard_overseas_recharge"))
            alertViewObj.addAction(title: "OK", style: .cancel, action: {})
            alertViewObj.showAlert()
        }
        func incorrectMobNumAlert() {
            alertViewObj.wrapAlert(title: nil, body: "Incorrect mobile number. Please try again".localized,
                                   img: UIImage(named : "AppIcon"))
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {})
            alertViewObj.showAlert()
        }
        self.operatorTableView.isHidden = true
        let phoneCount = contact.phoneNumbers.count
        
        var phoneNumber = ""
        if phoneCount > 0 {
            phoneNumber = contact.phoneNumbers[0].phoneNumber
        } else {
            return
        }
        
        phoneNumber = phoneNumber.deletingZeroPrefixes()
        
        if phoneNumber.count > 0 {
            phoneNumber = phoneNumber.replacingOccurrences(of: "(", with: "")
            phoneNumber = phoneNumber.replacingOccurrences(of: ")", with: "")
            phoneNumber = phoneNumber.trimmingCharacters(in: .whitespaces)
        }
        if phoneNumber.contains(find: " ") {
            var phArray = phoneNumber.components(separatedBy: " ")
            if phArray.count > 1 {
                phoneNumber = "+" + phArray[0] + phArray[1]
            }
        }
       
        let characterset = CharacterSet(charactersIn: "+0123456789 ")
        if phoneNumber.rangeOfCharacter(from: characterset.inverted) != nil || (phoneNumber == ""){
            invalidNumberSelectionAlert()
        } else if !phoneNumber.hasPrefix("+") {
            localNumberSelectionAlert()
        } else {
            var country = self.identifyCountry(withPhoneNumber: phoneNumber)
            if country.0 == "" ||  country.1 == "" {
                country = ("+95","myanmar")
            }
            if country.0 == "+95" {
                localNumberSelectionAlert()
            } else {
                InternationalTopupManager.selectedCountry = (country.0, country.1)
                let countryObject = Country.init(name: "", code: InternationalTopupManager.selectedCountry.flag, dialCode: InternationalTopupManager.selectedCountry.code)
                let country = Country.init(name: "", code: InternationalTopupManager.selectedCountry.flag, dialCode: InternationalTopupManager.selectedCountry.code)
                self.checkAvailableCountry(country: country) {_ in
                    
                }
                clearFields()
                if phoneNumber.hasPrefix(countryObject.dialCode) {
                    phoneNumber = phoneNumber.replacingOccurrences(of: countryObject.dialCode, with: "")
                }
                guard let rangeValue = getMinMaxForOverseas(countryCode: countryObject.dialCode) else {
                    return
                }
                if !isMobNumberWithinRange(minValue: rangeValue.minLength,
                                           maxValue: rangeValue.maxLength,
                                           phoneNumber: phoneNumber) {
                    clearFields()
                    incorrectMobNumAlert()
                    return
                }
                if phoneNumber.count <= 0 {
                    clearFields()
                    return
                }
                self.mobileNumber.text = phoneNumber
                self.mobileNumberLeftView.wrapCountryViewData(img: InternationalTopupManager.selectedCountry.flag, str: InternationalTopupManager.selectedCountry.code)
                self.confirmMobileNumberLeftView.wrapCountryViewData(img: InternationalTopupManager.selectedCountry.flag, str: InternationalTopupManager.selectedCountry.code)
                InternationalTopupManager.receiverName = contact.displayName()
                self.operatorArray = []
                self.confirmMobileNumber.leftView = nil
                self.confirmMobileNumber.isHidden = false
                self.selectOperator.isHidden = false
                self.isOnOperatorListArrowShowing(isOn: false)
                self.confirmMobileNumber.leftViewMode = .always
                self.confirmMobileNumber.name = true
                self.confirmMobileNumber.layoutIfNeeded()
                self.confirmMobileNumber.leftView = contactViewLeft
                self.confirmMobileNumber.text = contact.displayName()
                self.confirmMobileNumber.isUserInteractionEnabled = false
                self.mobileNumberRightView.hideClearButton(hide: false)
            }
        }
    }
    
    func getMinMaxForOverseas(countryCode: String) -> (minLength: Int, maxLength: Int)? {
        if countryCode == "+95" || countryCode == "95" || countryCode == "0095" {
            return nil
        } else {
            switch countryCode {
            case "+91":
                return (10,10)
            case "+86":
                return (11,11)
            case "+66":
                return (9,9)
            case let x where x.hasPrefix("+"):
                return (4,13)
            default:
                return nil
            }
        }
    }
    
    //MARK:- Helper Methods
    func identifyCountry(withPhoneNumber prefix: String) -> (String, String) {
        let countArray = revertCountryArray() as! Array<NSDictionary>
        var finalCodeString = ""
        var flagCountry = ""
        
        for dic in countArray {
            if let code = dic .object(forKey: "CountryCode") as? String, let flag = dic.object(forKey: "CountryFlagCode") as? String{
                if prefix.hasPrefix(code) {
                    finalCodeString = code
                    flagCountry = flag
                }
            }
        }
        return (finalCodeString,flagCountry)
    }
    
    func revertCountryArray() -> NSArray {
        do {
            if let file = Bundle.main.url(forResource: "mcc_mnc", withExtension: "txt") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? NSDictionary {
                    if let arr = object.object(forKey: "data") as? NSArray {
                        arrCountry = arr
                        return arrCountry
                    }
                } else if let object = json as? [Any] {
                    println_debug(object)
                } else {
                    println_debug("JSON is invalid")
                }
            } else {
                println_debug("no file")
            }
        } catch {
            println_debug(error.localizedDescription)
        }
        return arrCountry
    }
}
