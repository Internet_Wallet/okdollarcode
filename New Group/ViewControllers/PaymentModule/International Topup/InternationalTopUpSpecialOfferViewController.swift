//
//  InternationalTopUpSpecialOfferViewController.swift
//  OK
//
//  Created by Kethan Kumar on 30/10/2019.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class InternationalTopUpSpecialOfferViewController: OKBaseController {

    @IBOutlet weak var specialOfferLbl: UILabel! {
        didSet {
            self.specialOfferLbl.text = self.specialOfferLbl.text?.localized
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

  

}
