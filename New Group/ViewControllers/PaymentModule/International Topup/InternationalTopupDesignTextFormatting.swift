//
//  InternationalTopupDesignTextFormatting.swift
//  OK
//
//  Created by Ashish on 4/3/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit


extension InternationalTopupDesignViewController: UITextFieldDelegate {
    
    //MARK:- UITextFieldDelegate functions
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if textField == self.selectOperator {
            if let s = self.operatorArray, s.count > 0 {
                if self.operatorTableView.isHidden {
                    self.operatorTableView.isHidden = false
                    self.isOnOperatorListArrowShowing(isOn: true)
                    self.selectAmount.isHidden = true
                    textField.text = ""
                } else {
                    self.operatorTableView.isHidden = true
                    self.isOnOperatorListArrowShowing(isOn: false)
                }
            } else {
                guard let countryList = self.countriesDetail else { return false }
                self.selectAmount.isHidden = true
                textField.text = ""
                changeToDefaultNavigationColor()
                self.getOperatorsAccordingly(countries: countryList)
            }
            return false
        }
        if textField == self.selectAmount {
           self.getTopupAmountList()
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let topupCount = InternationalTopupManager().validations(countryCode: InternationalTopupManager.selectedCountry.code)
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            if textField == self.mobileNumber {
                self.confirmMobileNumber.text = ""
                self.confirmMobileNumber.isHidden = true
                self.selectOperator.isHidden = true
                self.selectAmount.isHidden = true
                self.selectAmount.text = ""
                self.selectOperator.text = ""
                self.operatorTableView.isHidden = true
            }
            if textField == self.confirmMobileNumber {
                self.selectOperator.isHidden = true
                self.selectAmount.isHidden = true
                self.selectOperator.text = ""
                self.selectAmount.text = ""
                self.selectOperator.isHidden = true
                self.selectAmount.isHidden = true
                self.selectAmount.text = ""
                self.selectOperator.text = ""
                self.operatorTableView.isHidden = true
            }
            changeToDefaultNavigationColor()
        }
        
        if textField == self.mobileNumber {
            self.confirmMobileNumber.name = false
            self.confMobile?.hideClearButton(hide: true)
            if text.count > 0 {
                self.mobileNumberRightView.hideClearButton(hide: false)
            } else {
                self.mobileNumberRightView.hideClearButton(hide: true)
            }
            if text.hasPrefix("0000") {
                self.mobileNumber.text = ""
                self.confirmMobileNumber.text = ""
                self.confirmMobileNumber.isHidden = true
                self.selectOperator.isHidden = true
                self.selectAmount.isHidden = true
                self.selectAmount.text = ""
                self.selectOperator.text = ""
                self.operatorTableView.isHidden = true
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                return false
            }
            if textField == self.mobileNumber {
                if self.mobileNumber.text?.count == topupCount.max {
                    if !(isBackSpace == -92) {
                        return false
                    }
                }
            }
            self.confirmMobileNumber.isHidden = (text.count >= topupCount.min) ? false : true
            if let previousNumber = self.mobileNumber.text {
                if text.count <= previousNumber.count {
                    if previousNumber != text {
                    self.confirmMobileNumber.text = ""
                    self.confirmMobileNumber.isHidden = (text.count >= topupCount.min) ? false : true
                    self.selectOperator.isHidden = true
                    self.selectAmount.isHidden = true
                    self.selectAmount.text = ""
                    self.selectOperator.text = ""
                    self.operatorTableView.isHidden = true
                    changeToDefaultNavigationColor()
                    } else{
                        return false
                    }
                }
            }
            if text.count >= topupCount.max {
                if text.count <= topupCount.max {
                    self.mobileNumber.text = text
                    if !self.confirmMobileNumber.isHidden {
                        self.confirmMobileNumber.isHidden = false
                    }
                }
                self.mobileNumber.resignFirstResponder()
                self.confirmMobileNumber.becomeFirstResponder()
                self.confirmMobileNumber.layoutIfNeeded()
                self.confirmMobileNumberLeftView.wrapCountryViewData(img: InternationalTopupManager.selectedCountry.flag, str: InternationalTopupManager.selectedCountry.code)
                self.confirmMobileNumber.leftView = nil
                self.confirmMobileNumber.leftView =  confirmMobileNumberLeftView
                self.confirmMobileNumber.leftViewMode = .always
                self.confirmMobileNumber.isUserInteractionEnabled = true
                return false
            }
            if let cMNumber = confirmMobileNumber.text {
                if text.count > cMNumber.count {
                    self.confirmMobileNumber.text = ""
                    self.selectOperator.isHidden = true
                    self.selectAmount.isHidden = true
                    self.selectAmount.text = ""
                    self.selectOperator.text = ""
                    self.operatorTableView.isHidden = true
                    changeToDefaultNavigationColor()
                }
            }
            InternationalTopupManager.receiverName = "Unknown"
        }
        
        if textField == self.confirmMobileNumber {
            switch self.mobileNumber.text!.hasPrefix(text) {
            case true:
                if self.mobileNumber.text!.count == text.count {
                    self.confirmMobileNumber.text = self.mobileNumber.text
                    self.confirmMobileNumber.resignFirstResponder()
                    self.selectOperator.isHidden = false
                    self.isOnOperatorListArrowShowing(isOn: false)
                    return false
                } else {
                    self.selectOperator.isHidden = true
                }
                if text.count > 0 {
                    self.confMobile?.hideClearButton(hide: false)
                } else {
                    self.confMobile?.hideClearButton(hide: true)
                }
                return true
            case false:
                return false
            }
        }
        
        return true
    }
}

