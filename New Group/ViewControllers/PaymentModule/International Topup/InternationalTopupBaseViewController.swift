//
//  InternationalTopupBaseViewController.swift
//  OK
//
//  Created by Ashish on 4/12/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class InternationalTopupBaseViewController: OKBaseController, PaytoScrollerDelegate, InternationalTopupSliderDelegate{
    
    fileprivate var viewControllers = [UIViewController]()
    
    @IBOutlet weak var containerView: UIView!
    
    fileprivate var internationalMenuSlider : PaytoScroller?
    
    var globalDesignController : InternationalTopupDesignViewController?
    
    var userObject : (number: String, name: String)?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 17.00) ?? UIFont.systemFont(ofSize: 17.00), NSAttributedString.Key.foregroundColor: UIColor.white]

        self.title = InternationalTopupConstants.headers.mainheader.localized
        
        MyNumberTopup.theme = UIColor.black
        
        self.setupBackButton()
        
        guard let topup = self.storyboard?.instantiateViewController(withIdentifier: InternationalTopupConstants.storyboardConnections.designScreenidentifier) else { return }
        topup.title = InternationalTopupConstants.headers.rechargeHeader
        
        guard let designView = topup as? InternationalTopupDesignViewController else { return }
        designView.delegate = self
        self.globalDesignController = designView
        
        designView.userObject = self.userObject
        designView.navigation = self.navigationController
        
        guard let dataPlan = self.storyboard?.instantiateViewController(withIdentifier: InternationalTopupConstants.storyboardConnections.dataIdentifier) else { return }
        dataPlan.title = InternationalTopupConstants.headers.dataHeader
        
        guard let specialOffer = self.storyboard?.instantiateViewController(withIdentifier: (InternationalTopupConstants.storyboardConnections.specialOfferIdentifier)) else { return }
        specialOffer.title = InternationalTopupConstants.headers.specialOfferHeader
        
        self.viewControllers = [topup, dataPlan, specialOffer]
        
        let parameters = [PaytoScrollerOptionMenuItemSeparatorWidth: 0, PaytoScrollerOptionMenuItemSeparatorPercentageHeight: 0.1,
                          PaytoScrollerOptionScrollMenuBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectedMenuItemLabelColor: UIColor.black,
                          PaytoScrollerOptionViewBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectionIndicatorColor: MyNumberTopup.OperatorColorCode.okDefault,
                          PaytoScrollerOptionUnselectedMenuItemLabelColor:UIColor.darkText,
                          PaytoScrollerOptionMenuHeight: 50,
                          PaytoScrollerOptionMenuItemFont: "Zawgyi-One",
                          PaytoScrollerOptionMenuItemWidth: (self.containerView.frame.width / 3.0) ] as [String: Any]
        
        let yAxis = self.navigationController?.navigationBar.frame.height ?? 0.0 + UIApplication.shared.statusBarFrame.height
        internationalMenuSlider = PaytoScroller(viewControllers: self.viewControllers, frame: CGRect(x: 0.0, y: 0.0, width: self.containerView.frame.width, height: containerView.frame.height), options: parameters)
//        internationalMenuSlider = PaytoScroller(viewControllers: self.viewControllers, frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight), options: parameters)
        self.initialUpdatesAfterParameterBinding()
        designView.scroller = internationalMenuSlider
        NotificationCenter.default.addObserver(self, selector: #selector(InternationalTopupBaseViewController.morePayment), name: NSNotification.Name(rawValue: "MultiTopupReloadMorePaymentOverseas"), object: nil)
    }
    
    @objc func morePayment() {
        if let first = self.viewControllers.first {
            if let overseas = first as? InternationalTopupDesignViewController {
                overseas.rechargePlanContainerView.isHidden = true
                overseas.clearActionType()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(InternationalTopupBaseViewController.dismissMyTopupScreen), name: NSNotification.Name(rawValue: "GoToDashboard"), object: nil)
    }
    
    @objc func dismissMyTopupScreen() {
        self.dismiss(animated: true, completion: nil)
    }

    
    private func initialUpdatesAfterParameterBinding() -> Void {
        
        guard let slider = internationalMenuSlider else { return }
        
        slider.delegate = self
        
        slider.scrollingEnable(true)

        self.containerView.addSubview(slider.view)
        
        self.containerView.bringSubviewToFront(slider.view)
        
        //self.view.backgroundColor = MyNumberTopup.OperatorColorCode.okDefault
        
    }
    
    func didSlideOpen(flag: Bool) {
        
        guard let slider = internationalMenuSlider else { return }
        
        slider.scrollingEnable(flag)

    }
    
    func willMove(toPage controller: UIViewController!, index: Int) {
    }
    
    func didMove(toPage controller: UIViewController!, index: Int) {
        if index == 1 || index == 2 {
            if let overseas = self.viewControllers.first as? InternationalTopupDesignViewController {
                if let menu =  self.internationalMenuSlider {
                    var alertMessage = ""
                    if overseas.confirmMobileNumber.isHidden {
                        alertMessage = "Enter Mobile Number"
                    } else {
                        if let cMNTxt = overseas.confirmMobileNumber.text {
                            if cMNTxt.count == 0 {
                                alertMessage = "Enter Confirm Mobile Number"
                            } else {
                                if overseas.selectOperator.isHidden {
                                    alertMessage = "Confirm Mobile number must be same as Mobile number"
                                } else {
                                    if overseas.selectAmount.isHidden {
                                        alertMessage = "Please select operator"
                                    }
                                }
                            }
                        }
                    }
                    if alertMessage.count > 0 {
                        self.showErrorAlert(errMessage: "\(alertMessage)".localized)
                        menu.move(toPage: 0)
                    }
                }
            }
        }

    }
    
    private func setupBackButton() {
        let buttonIcon      = UIImage(named: "tabBarBack")
        let leftBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: self, action: #selector(InternationalTopupBaseViewController.dismiss(_:)))
        leftBarButton.image = buttonIcon
        leftBarButton.tintColor = .white
        self.navigationItem.leftBarButtonItem = leftBarButton
    }
    
    @objc private func dismiss(_ sender:UIBarButtonItem!) {
        self.view.endEditing(true)
        
        if let internationalTopupVC = globalDesignController {
            if let containerView = internationalTopupVC.rechargePlanContainerView {
                if  containerView.isHidden {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    containerView.isHidden = true
                }
                return
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "GoToDashboard"), object: nil)
    }
}





