//
//  TopupMyNumberAlertSMSViewController.swift
//  OK
//
//  Created by Ashish on 5/12/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol openSmsScreenForTopupDelegate : class {
    func openSmsScreen(with number: String, code: String)
}

class TopupMyNumberAlertSMSViewController: OKBaseController {

    @IBOutlet weak var hearderOne: UILabel!{
        didSet
        {
            hearderOne.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    
    @IBOutlet weak var btnOutlet: UIButton!{
        didSet
        {
            btnOutlet.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    
    var ussdCode : String?
    
    weak var delegate : openSmsScreenForTopupDelegate?
    var isComingFromRecent = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hearderOne.text =  self.hearderOne.text?.localized
        self.btnOutlet.setTitle(self.btnOutlet.titleLabel?.text?.localized, for: .normal)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func okAction(_ sender: UIButton) {
        self.dismiss(animated: true) {
            guard let ussd = self.ussdCode else { return }
            let codeArray = ussd.components(separatedBy: "-")
            guard let num = codeArray.last, let code = codeArray.first else { return }
            guard let weakDelegate = self.delegate else { return }
            weakDelegate.openSmsScreen(with: num, code: code)
        }
    }
    
    @IBAction func backgroundDismiss(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
