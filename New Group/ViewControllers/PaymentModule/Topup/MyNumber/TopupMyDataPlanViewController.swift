//
//  TopupMyDataPlanViewController.swift
//  OK
//
//  Created by Ashish on 1/27/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import MessageUI
import IQKeyboardManagerSwift

class TopupMyDataPlanViewController: OKBaseController {
    
    //MARK: - Outlets
    @IBOutlet var dataPlanTable: UITableView!
    @IBOutlet weak var errLabel: UILabel!{
        didSet
        {
            errLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var containerStack: UIStackView!

    //MARK: - Properties
    fileprivate var selectedObject: DataPlanModel?
    fileprivate var selectedMerchant: TopupMerchantNumberModel?
    weak var requestMoneyDelegate: requestMoneyAmountCallback?
    weak var dismiss: RequestMoneyAmountDismissCallback?
    var model = [DataPlanModel]()
    var kickBackInfoDictionary = Dictionary<String,Any>()
    var arrayKickBackInfo = [TopupConfirmationModel]()
    var navigation: UINavigationController!
    var requestMoney: String?
    var otherNumberTopup: String?
    var amountValidationCheck: Bool = false
    var isComingFromRecent = false
    fileprivate lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(TopupMyDataPlanViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = MyNumberTopup.theme
        return refreshControl
    }()
    
    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        //vinnu commented
//        if MyNumberTopup.theme  == MyNumberTopup.OperatorColorCode.mpt || MyNumberTopup.theme == MyNumberTopup.OperatorColorCode.mactel, let _ = otherNumberTopup {
//            self.dataPlanTable.isHidden = true
//            self.containerStack.isHidden = false
//            self.errLabel.text = "No Data Plan".localized
      //  } else {
            IQKeyboardManager.sharedManager().enable            = false
            IQKeyboardManager.sharedManager().enableAutoToolbar = false
            self.dataPlanTable.tableFooterView = UIView.init()
            self.dataPlanTable.addSubview(refreshControl)
            self.dataPlanTable.isHidden = false
            self.containerStack.isHidden = true
            if appDelegate.checkNetworkAvail() == false {
                self.dataPlanTable.isHidden = true
                self.containerStack.isHidden = false
                self.errLabel.text = "No Internet Connection".localized
            }
            self.urlRefresh()
            NotificationCenter.default.addObserver(self, selector: #selector(TopupMyDataPlanViewController.networkLost), name: .networkLost, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(TopupMyDataPlanViewController.networkConnected), name: .networkConnected, object: nil)
        //}
    }
    
    //MARK: - Target Methods
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.urlRefresh()
        refreshControl.endRefreshing()
    }
    
    @objc func networkLost() {
        self.dataPlanTable.isHidden = true
        self.containerStack.isHidden = false
        self.errLabel.text = "No Internet Connection".localized
    }
    
    @objc func networkConnected() {
        //vinnu commented
//        if let _ = otherNumberTopup {
//            otherNumberUI()
//            return
//        }
        self.okActivityIndicator(view: self.view)
        let urlString = String.init(format: TopupUrl.dataPlan, MyNumberTopup.opName)
        let url = URL.init(string: urlString)
        let par = Dictionary<String,String>()
        TopupWeb.genericClassWithoutLoader(url: url!, param: par as AnyObject, httpMethod: "GET") { [weak self](response, success) in
            DispatchQueue.main.async {
                guard let weakSelf = self else { return }
                self?.removeActivityIndicator(view: weakSelf.view)
            }
            if success {
                self?.parseJsonData(response)
            }
        }
    }
    
    //MARK: - Methods
    func urlRefresh() {
        let urlString = String.init(format: TopupUrl.dataPlan, MyNumberTopup.opName)
        let url = URL.init(string: urlString)
        let par = Dictionary<String,String>()
        println_debug("\n\nUrl -> Get Method ->")
        println_debug(urlString)
        TopupWeb.genericClass(url: url!, param: par as AnyObject, httpMethod: "GET") { [weak self](response, success) in
            if success {
                self?.parseJsonData(response)
            }
        }
    }
    
    //vinnu commented
    
//    func otherNumberUI() {
//        if let _ = otherNumberTopup {
//            if MyNumberTopup.theme  == MyNumberTopup.OperatorColorCode.mpt || MyNumberTopup.theme == MyNumberTopup.OperatorColorCode.mactel {
//                self.dataPlanTable.isHidden = true
//                self.containerStack.isHidden = false
//                self.errLabel.text = "No Data Plan".localized
//            }
//        }
//    }
    
    func parseJsonData(_ json: Any) {
        if let jsonWrapped = json as? Dictionary<String,Any> {
            if let stringData = jsonWrapped["Data"] as? String, (jsonWrapped["Code"] as? NSNumber)?.intValue == 200 {
                let dictionaryArray = OKBaseController.convertToArrDictionary(text: stringData)
                if dictionaryArray == nil {return}
                if let arrayObjects = dictionaryArray as? [Dictionary<String,Any>] {
                    self.model.removeAll()
                    for element in arrayObjects {
                        let detail = element["PromotionMessage"] as? String ?? ""
                        let ussdCode = element.safeValueForKey("UssdDialling") as? String
                        let seperatedComponent = detail.components(separatedBy: "-")
                        var finalString = seperatedComponent[0]
                        //let finalPackDetails = seperatedComponent[safe: 0] ?? "" + " @ " + seperatedComponent[1]
                        let twoArrValue = seperatedComponent[safe: 1] ?? ""
                        finalString = finalString + " @ " + twoArrValue
                        let dataSize = seperatedComponent[safe: 2] ?? ""
                        let amount = element["Amount"] as? String
                        let isActive = (element["IsActive"] as? NSNumber)?.boolValue
                        let object = DataPlanModel.init(finalString, dataSize, amount ?? "", isActive ?? false, ussdCode ?? "")
                        if isActive == true {
                            self.model.append(object)
                        }
                    }
                    model = model.sorted(by: { $0.developerAmount.integerValue < $1.developerAmount.integerValue})
                    DispatchQueue.main.async {
                        if self.model.count > 0 {
                            self.dataPlanTable.isHidden = false
                            self.dataPlanTable.reloadData()
                            self.containerStack.isHidden = true
                        } else {
                            self.dataPlanTable.isHidden = true
                            self.containerStack.isHidden = false
                            self.errLabel.text = "No Data Plan Found".localized
                        }
                    }
                }
            }
        }
    }
    
    func callApi(row: Int) {
        let model = self.model[row]
        //vinnu commented
//        if otherNumberTopup == nil {
//            if MyNumberTopup.theme  == MyNumberTopup.OperatorColorCode.mpt || MyNumberTopup.theme == MyNumberTopup.OperatorColorCode.mactel {
//                guard let ussdVC = self.storyboard?.instantiateViewController(withIdentifier: String(describing: TopupMyNumberAlertSMSViewController.self)) as? TopupMyNumberAlertSMSViewController else { return }
//                ussdVC.modalPresentationStyle = .overCurrentContext
//                ussdVC.ussdCode = model.ussdCode
//                ussdVC.delegate = self
//                navigation.present(ussdVC, animated: true, completion: nil)
//                return
//            }
//        }
        
        var totalSum: Float = 0.0
        for controllers in MultiTopupArray.main.controllers {
            if let amountStr = controllers.amount.text, let amount = Float(amountStr) {
                totalSum = totalSum + amount
            }
        }
        if totalSum > (UserLogin.shared.walletBal as NSString).floatValue {
            alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
            alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                    addWithdrawView.modalPresentationStyle = .fullScreen
                    self.navigation.present(addWithdrawView, animated: true, completion: nil)
                }
            })
            alertViewObj.showAlert()
            return
        }
        if (UserLogin.shared.walletBal as NSString).floatValue < (model.developerAmount).floatValue {
            alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
            alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                    addWithdrawView.modalPresentationStyle = .fullScreen
                    self.navigation.present(addWithdrawView, animated: true, completion: nil)
                }
            })
            alertViewObj.showAlert()
            return
        }
        if appDelegate.checkNetworkAvail() == false {
            alertViewObj.wrapAlert(title: nil, body: "No Internet Connection", img: #imageLiteral(resourceName: "dashboard_my_number"))
            alertViewObj.addAction(title: "OK", style: .cancel, action: {
                
            })
            alertViewObj.showAlert(controller: self)
            return
        }
        if let _ = otherNumberTopup {
            requestMoneyDelegate?.didGetAmount(model.amount, withPlanType: "Data Plan")
            if MultiTopupArray.main.controllers.count > 1 {
                dismiss?.didDismissCallback()
            } else {
                requestMoneyDelegate?.paymentCallOtherNumberThroughDelegate()
                dismiss?.didDismissCallback()
            }
            return
        }
        if MyNumberTopup.operatorCase == .mactel {
            self.selectedObject = model
            self.callingFeeLimitKickBack()
        } else {
            var urlString = String.init(format: TopupUrl.merchantNumberCheck, MyNumberTopup.opName,UserModel.shared.mobileNo,rechargeType.dataPlan,UserModel.shared.formattedNumber,model.developerAmount, GeoLocationManager.shared.currentLongitude, GeoLocationManager.shared.currentLatitude, UserLogin.shared.walletBal)
            urlString = urlString + "&SubScriberName=" + (telecoTopupSubscription?.first?.name ?? "").replacingOccurrences(of: " ", with: "%20")
            println_debug(urlString)
            let url = URL.init(string: urlString)
            let par = Dictionary<String,String>()
            TopupWeb.genericClass(url: url!, param: par as AnyObject, httpMethod: "GET") { [weak self] (response, success) in
                if success {
                    if let json = response as? Dictionary<String,Any> {
                        if let code = json["Code"] as? NSNumber, code == 200 {
                            if let msg = json["Msg"] as? String, msg == "Success" {
                                if let data = json["Data"] as? String {
                                    let array = OKBaseController.convertToArrDictionary(text: data)
                                    if array == nil { return }
                                    var merchantModel = [TopupMerchantNumberModel]()
                                    if let merchantData = array as? [Dictionary<String,Any>] {
                                        for element in merchantData {
                                            let operatr = element["Operator"] as? String
                                            let merNumber = element["MobileNumber"] as? String
                                            let modelMerchant = TopupMerchantNumberModel.init((operatr == nil) ? "" : operatr!, merchant: merNumber == nil ? "" : merNumber!)
                                            merchantModel.append(modelMerchant)
                                        }
                                        if merchantModel.count > 0 {
                                            self?.selectedMerchant = merchantModel.first
                                            self?.selectedObject = model
                                            self?.callingFeeLimitKickBack()
                                        }
                                    }
                                }
                            }
                        } else if let code = json["Code"] as? NSNumber, code == 300 {
                            self?.showErrorAlert(errMessage: "Your Service Provider is Currently Unavailable.".localized)
                        }
                    }
                }
            }
        }
    }
    
    func callingFeeLimitKickBack() {
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "", delegate: self)
        } else {
            self.biometricAuthentication(isSuccessful: true, inScreen: "")
        }
    }
    
    func parsingFeeLimitKickbackResponse(xml: String) -> Void {
        self.arrayKickBackInfo.removeAll()
        let xml = SWXMLHash.parse(xml)
        self.enumerate(indexer: xml)
        println_debug(kickBackInfoDictionary)
        if let transaction = kickBackInfoDictionary.safeValueForKey("resultdescription") as? String, transaction == "Transaction Successful" {
            let number = (kickBackInfoDictionary.safeValueForKey("destination") as? String) == nil ? "" : (kickBackInfoDictionary.safeValueForKey("destination") as? String)
            let operatorName = MyNumberTopup.opName
            let type     = RechargePlanType.dataPlan
            let cashback = (kickBackInfoDictionary.safeValueForKey("kickback") as? String) == nil ? "" : (kickBackInfoDictionary.safeValueForKey("kickback") as? String)
            let amount   = (kickBackInfoDictionary.safeValueForKey("amount") as? String) == nil ? "" : (kickBackInfoDictionary.safeValueForKey("amount") as? String)

            let model = TopupConfirmationModel.init(UserModel.shared.name, number: number.safelyWrappingString(), opeName: operatorName, type: type, cashbk: cashback.safelyWrappingString(), topAmt: amount.safelyWrappingString(), payNumber: UserModel.shared.mobileNo)
            self.arrayKickBackInfo.append(model)
            
            DispatchQueue.main.async {
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TopupConfirmationViewController.self)) as? TopupConfirmationViewController else { return }
                vc.confirmResponseModel = self.arrayKickBackInfo
                vc.type = "Data Plan"
                vc.screenType = ConfirmScreenType.mynumber
                vc.isComingFromRecent = self.isComingFromRecent
                self.navigation.pushViewController(vc, animated: true)
            }
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            kickBackInfoDictionary[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    //MARK: Deinit
    deinit {
        println_debug("TopupMyDataPlanViewController deinit called")
        NotificationCenter.default.removeObserver(self)
        self.model.removeAll()
        self.selectedObject = nil
        self.selectedMerchant = nil
    }
}

//MARK: - UITableViewDataSource
extension TopupMyDataPlanViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: TopupDataPlanTableViewCell.self), for: indexPath) as? TopupDataPlanTableViewCell
        cell?.wrap(model: self.model[indexPath.row])
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.00
    }
}

//MARK: - UITableViewDelegate
extension TopupMyDataPlanViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let _ = otherNumberTopup {
            let model = self.model[indexPath.row]
            if let vc = MultiTopupArray.main.controllers.last  {
                let number = getAgentCode(numberWithPrefix: vc.mobileNumber.text.safelyWrappingString(), havingCountryPrefix: "+95")
                if !PaymentVerificationManager.isValidPaymentTransactions(number, model.developerAmount as String, "TOPUP") {
                    alertViewObj.wrapAlert(title: nil, body: "Same Number with Same Amount can not be paid before 2 minutes".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                    alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                    })
                    alertViewObj.showAlert()
                    return
                }
            }
            if MultiTopupArray.main.controllers.count > 1 {
                if let last = MultiTopupArray.main.controllers[safe: MultiTopupArray.main.controllers.count - 1], let secondLast = MultiTopupArray.main.controllers[safe: MultiTopupArray.main.controllers.count - 2] {
                    if let lastText = last.mobileNumber.text, let secondLastText = secondLast.mobileNumber.text {
                        if lastText == secondLastText {
                            if let lastAmount = last.amount.text, let secondLastAmount = secondLast.amount.text {
                                if lastAmount == secondLastAmount {
                                    alertViewObj.wrapAlert(title: nil, body: "Same Mobile Number with Same Top-Up amount, Same Data Plan or Same Special Offer Not Allowed".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                                    alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                                    })
                                    alertViewObj.showAlert()
                                    return
                                }
                            }
                        }
                    }
                }
            }
            var totalAmount = (model.developerAmount).floatValue
            for element in MultiTopupArray.main.controllers {
                totalAmount = totalAmount + ((element.amount.text?.replacingOccurrences(of: ",", with: "") ?? "0") as NSString).floatValue
            }
            
            if amountValidationCheck {
                let model = self.model[indexPath.row]
                requestMoneyDelegate?.didGetAmount(model.amount, withPlanType: "Data Plan")
                dismiss?.didDismissCallback()
                return
            }
            
            if totalAmount > (UserLogin.shared.walletBal as NSString).floatValue {
                alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                    if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                        addWithdrawView.modalPresentationStyle = .fullScreen
                        self.navigation.present(addWithdrawView, animated: true, completion: nil)
                    }
                })
                alertViewObj.showAlert()
                return
            }
            self.callApi(row: indexPath.row)
            return
        }
        if let _ = requestMoney {
            let model = self.model[indexPath.row]
            requestMoneyDelegate?.didGetAmount(model.amount, withPlanType: "Data Plan")
            dismiss?.didDismissCallback()
        } else {
            let model = self.model[indexPath.row]
            if !PaymentVerificationManager.isValidPaymentTransactions(UserModel.shared.mobileNo, model.amount, "TOPUP") {
                alertViewObj.wrapAlert(title: nil, body: "Same Number with Same Amount can not be paid before 2 minutes".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                })
                alertViewObj.showAlert()
                return
            }
            self.callApi(row: indexPath.row)
        }
    }
}

//MARK: - MFMessageComposeViewControllerDelegate
extension TopupMyDataPlanViewController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
}

//MARK: - openSmsScreenForTopupDelegate
extension TopupMyDataPlanViewController: openSmsScreenForTopupDelegate {
    func openSmsScreen(with number: String, code: String) {
        let defaultNumber = [number]
        if (MFMessageComposeViewController.canSendText()) {
            let controller        = MFMessageComposeViewController()
            controller.body       = code
            controller.recipients = defaultNumber
            controller.messageComposeDelegate = self
            self.navigation.present(controller, animated: true, completion: nil)
        }
    }
}

//MARK: - BioMetricLoginDelegate
extension TopupMyDataPlanViewController: BioMetricLoginDelegate {
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful == false {
            return
        }
        var type = "PAYTO"
        if MyNumberTopup.operatorCase == .mactel {
            selectedMerchant = TopupMerchantNumberModel.init(MyNumberTopup.opName, merchant: UserModel.shared.mobileNo)
            type = "TOPUP"
        }
        let urlString = String.init(format: TopupUrl.cashback,UserModel.shared.mobileNo, selectedMerchant!.merchantNumber, selectedObject!.developerAmount,ok_password ?? "",type,UserLogin.shared.token)
        println_debug(urlString)
        let url = URL.init(string: urlString)
        let par = Dictionary<String,String>()
        TopupWeb.genericClassXML(url: url!, param: par as AnyObject, httpMethod: "POST") { [weak self](respponse, success) in
            if success {
                if let xmlResponse = respponse as? String {
                    self?.parsingFeeLimitKickbackResponse(xml: xmlResponse)
                }
            }
        }
    }
}
