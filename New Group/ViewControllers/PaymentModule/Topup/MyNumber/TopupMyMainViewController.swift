//
//  TopupMyMainViewController.swift
//  OK
//
//  Created by Ashish on 1/27/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
//import IQKeyboardManager

enum TopupButtonEnum {
    case topup, dataPlan, specialOffer
}

protocol requestMoneyAmountCallback: class {
    func didGetAmount(_ amount: String, withPlanType type: String)
    func paymentCallOtherNumberThroughDelegate()
    func paymentCallOtherNumberWithCustomMsg(index: String)
}

extension requestMoneyAmountCallback  {
    func didGetAmount(_ amount: String, withPlanType type: String) {}
    func paymentCallOtherNumberThroughDelegate() {}
    func paymentCallOtherNumberWithCustomMsg(index: String) {}
}

protocol OtherNumberCallBack: class{
    func selectedAmountFromTopupPlan(_ amount: String)
}

protocol RequestMoneyAmountDismissCallback: class {
    func didDismissCallback()
}

class TopupMyMainViewController: OKBaseController {
    
    //MARK: - Properties
    fileprivate var topupMenu: PaytoScroller?
    fileprivate var controllerArray: [UIViewController] = []
    var requestMoneyNumber: String?
    var otherNumberTopup: String?
    var isPlusSelected: Bool = false
    var amountValidationCheck: Bool = false
    var buttonTapped: TopupButtonEnum = .topup
    weak var delegate: requestMoneyAmountCallback?
    weak var otherNumberDelegate: OtherNumberCallBack?
    var isComingFromRecent = false

    //MARK: - Views life cycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Entered")
        println_debug("TopupMyMainViewController")
        geoLocManager.startUpdateLocation()
        var opName = ""
        if let requestMoney = requestMoneyNumber {
            var main = requestMoney
            _ = main.remove(at: String.Index.init(encodedOffset: 0))
            _ = main.remove(at: String.Index.init(encodedOffset: 0))
            main = "+" + main
            let number = identifyCountry(withPhoneNumber: main)
            main = main.replacingOccurrences(of: "(", with: "")
            main = main.replacingOccurrences(of: "(", with: "")
            main = main.replacingOccurrences(of: number.countryCode, with: "")
            main = "0" + main
            opName = self.setupColorAndOperator(mobile: requestMoney)
        } else {
             opName = self.setupColorAndOperator(mobile: UserModel.shared.formattedNumber)
        }
        // should change number to user model. for testing append any number here in agent code format
        self.navigationController?.navigationBar.tintColor = MyNumberTopup.theme
        let size  = opName.size(withAttributes: [.font: UIFont.init(name: appFont, size: 13.0) ?? UIFont.systemFont(ofSize: 13.0)])
        let label = UILabel(frame: .init(origin: CGPoint.zero, size: size))
        label.textColor = .white
        if opName.lowercased() == "mectel".lowercased(){
            label.text = "MecTel"
        } else{
            label.text = opName.lowercased().capitalizedFirst()
        }
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: label)
        self.navigationController?.navigationBar.barTintColor = MyNumberTopup.theme
        IQKeyboardManager.sharedManager().enable            = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 16.0) ?? UIFont.systemFont(ofSize: 14.0), NSAttributedString.Key.foregroundColor: UIColor.white]
        if requestMoneyNumber != nil {
            self.title = "Top-Up Other Number".localized
        } else {
            self.title = "Top-Up My Number".localized
        }
        self.loadingUI()
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "tabBarBack"), for: UIControl.State.normal)
        button.addTarget(self, action:#selector(dismissTheViewControllerPT(_:)), for:.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        self.navigationController?.navigationBar.tintColor = .white
        let parameters = [PaytoScrollerOptionMenuItemSeparatorWidth: 0, PaytoScrollerOptionMenuItemSeparatorPercentageHeight: 0.1,
                          PaytoScrollerOptionScrollMenuBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectedMenuItemLabelColor: MyNumberTopup.theme,
                          PaytoScrollerOptionViewBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectionIndicatorColor: MyNumberTopup.theme,
                          PaytoScrollerOptionUnselectedMenuItemLabelColor:UIColor.darkText,
                          PaytoScrollerOptionMenuHeight: 50,
                          PaytoScrollerOptionEnableHorizontalBounce: true,
                          PaytoScrollerOptionMenuItemWidthBasedOnTitleTextWidth: false,
                          PaytoScrollerOptionAddBottomMenuHairline: true,
                          PaytoScrollerOptionBottomMenuHairlineColor: UIColor.gray,
                          PaytoScrollerOptionMenuItemFont: "Zawgyi-One",
                          PaytoScrollerOptionUseMenuLikeSegmentedControl: true] as [String: Any]
        var cFrame = self.view.bounds
        if cFrame.height > 800 {
            cFrame = CGRect(x: cFrame.origin.x, y: cFrame.origin.y, width: cFrame.width, height: cFrame.height - 20)
        }
        topupMenu = PaytoScroller(viewControllers: controllerArray, frame: cFrame, options: parameters)
        topupMenu?.delegate = self
        topupMenu?.scrollingEnable(false)
        if let menu = topupMenu {
            self.view.addSubview(menu.view)
        }
        topupMenu?.scrollingEnable(true)
        self.view.backgroundColor = MyNumberTopup.theme
        guard let menu = topupMenu else { return }
        switch buttonTapped {
        case .topup:
            menu.move(toPage: 0)
            break
        case .dataPlan:
            menu.move(toPage: 1)
            break
        case .specialOffer:
            menu.move(toPage: 2)
            break
        }
        self.topupMenu?.change(forButtonTap: true)
        subscribeToShowKeyboardNotifications()
//        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
              
    }
    
    @objc func appMovedToBackground() {
        performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
    }
    
    func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        topupMenu?.scrollingEnable(false)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        topupMenu?.scrollingEnable(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(TopupMyMainViewController.dismissMyTopupScreen), name: NSNotification.Name(rawValue: "GoToDashboard"), object: nil)
        self.viewDidLoad()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //MARK:- Target Methods
    @objc func dismissMyTopupScreen() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func dismissTheViewControllerPT(_ sender:UIBarButtonItem!) {
        if let navigation = self.navigationController {
            navigation.dismiss(animated: true, completion: nil)
//            performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK:- Methods
    private func loadingUI() {
        if let reqMoneyNumber = requestMoneyNumber {
            let topup = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: topupMyRechargePlanViewController.self)) as? topupMyRechargePlanViewController
            topup?.title = "TOP-UP".localized
            topup?.navigation = self.navigationController
            topup?.requestMoney = reqMoneyNumber
            topup?.dismiss = self
            topup?.amountValidationCheck = amountValidationCheck
            topup?.otherNumberTopup = otherNumberTopup
            topup?.isPlusSelected = isPlusSelected
            topup?.requestMoneyDelegate = delegate
            topup?.isComingFromRecent = isComingFromRecent
            
            let dataPlan = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TopupMyDataPlanViewController.self)) as? TopupMyDataPlanViewController
            dataPlan?.title = "DATA PLAN".localized
            dataPlan?.otherNumberTopup = otherNumberTopup
            dataPlan?.requestMoney = reqMoneyNumber
            dataPlan?.requestMoneyDelegate = delegate
            dataPlan?.dismiss = self
            dataPlan?.amountValidationCheck = amountValidationCheck
            dataPlan?.navigation = self.navigationController
            dataPlan?.isComingFromRecent = isComingFromRecent
            
            let specialOffer = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TopupMySpecialOfferViewController.self)) as? TopupMySpecialOfferViewController
            specialOffer?.title = "SPECIAL OFFERS".localized
            specialOffer?.requestMoney = reqMoneyNumber
            specialOffer?.requestMoneyDelegate = delegate
            specialOffer?.dismiss = self
            specialOffer?.amountValidationCheck = amountValidationCheck
            specialOffer?.otherNumberTopup = otherNumberTopup
            specialOffer?.navigation = self.navigationController
            specialOffer?.isComingFromRecent = isComingFromRecent
            
            guard let myTopup   = topup        else { return }
            guard let myData    = dataPlan     else { return }
            guard let myspOffer = specialOffer else { return }
            
            controllerArray = [myTopup, myData, myspOffer]

        } else {
            let topup = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: topupMyRechargePlanViewController.self)) as? topupMyRechargePlanViewController
            topup?.title = "TOP-UP".localized
            topup?.navigation = self.navigationController
            topup?.isComingFromRecent = isComingFromRecent
            let dataPlan = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TopupMyDataPlanViewController.self)) as? TopupMyDataPlanViewController
            dataPlan?.title = "DATA PLAN".localized
            dataPlan?.navigation = self.navigationController
            dataPlan?.isComingFromRecent = isComingFromRecent
            
            let specialOffer = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TopupMySpecialOfferViewController.self)) as? TopupMySpecialOfferViewController
            specialOffer?.title = "SPECIAL OFFERS".localized
            specialOffer?.navigation = self.navigationController
            specialOffer?.isComingFromRecent = isComingFromRecent
            
            guard let myTopup = topup else { return }
            guard let myData  = dataPlan else { return }
            guard let myspOffer = specialOffer else { return }
            
            controllerArray = [myTopup, myData, myspOffer]
        }
    }
    
    func setupColorAndOperator(mobile: String) -> String {
        var number = mobile
        if mobile.hasPrefix("0095") {
            number = "0" + mobile.dropFirst(4)
        }
        let object = PayToValidations().getNumberRangeValidation(number)
        var operatorName = object.operator
        if operatorName.lowercased() == "MPT".lowercased() || operatorName.lowercased() == "MPT CDMA (800)".lowercased() || operatorName.lowercased() == "MPT CDMA (450)".lowercased() {
            operatorName = "MPT"
            MyNumberTopup.operatorCase = .mpt
        } else if operatorName.lowercased() == "Telenor".lowercased() {
            MyNumberTopup.operatorCase = .telenor
        } else if operatorName.lowercased() == "Ooredoo".lowercased() {
            MyNumberTopup.operatorCase = .ooreedo
        } else if operatorName.lowercased() == "MecTel".lowercased() || operatorName.lowercased() == "MecTel CDMA".lowercased() {
            MyNumberTopup.operatorCase = .mactel
            operatorName = "MecTel"
        } else if operatorName.lowercased() == "Mytel".lowercased() || operatorName.lowercased() == "MyTel".lowercased() {
            MyNumberTopup.operatorCase = .mytel
        } else {
            MyNumberTopup.operatorCase = .okDefault
        }
        return operatorName
    }
    
    deinit {
        geoLocManager.stopUpdateLocation()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "GoToDashboard"), object: nil)
    }
}

//MARK:- RequestMoneyAmountDismissCallback
extension TopupMyMainViewController: RequestMoneyAmountDismissCallback {
    func didDismissCallback() {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- PaytoScrollerDelegate
extension TopupMyMainViewController: PaytoScrollerDelegate {
    func willMove(toPage controller: UIViewController!, index: Int) {
        
    }
    
    func didMove(toPage controller: UIViewController!, index: Int) {
        
    }
}
