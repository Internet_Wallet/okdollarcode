//
//  topupMyRechargePlanViewController.swift
//  OK
//
//  Created by Ashish on 1/27/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class topupMyRechargePlanViewController: OKBaseController {
    
    //MARK:- Outlets
    @IBOutlet weak var bottomLayoutConst: NSLayoutConstraint!
    @IBOutlet fileprivate var rechargePlanTableView: UITableView!
    @IBOutlet weak var backvView: UIView!
    @IBOutlet weak var buttonHolder: CardDesignView!
    @IBOutlet weak var stackContainer: UIStackView!
    @IBOutlet weak var errLabel: UILabel!{
        didSet
        {
            errLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var nxtBtn: UIButton!{
        didSet{
            self.nxtBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
        
    }
    @IBOutlet weak var enterAmount: UITextField!{
        didSet
        {
            enterAmount.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var constrarintEAView: NSLayoutConstraint!
    
    //MARK:- Properties
    fileprivate var selectedObject: RechargePlanModel?
    fileprivate var selectedMerchant: TopupMerchantNumberModel?
    let mobileNumberAcceptableCharacters = "0123456789"
    var requestMoney: String?
    var otherNumberTopup: String?
    var amountValidationCheck: Bool = false
    var kickBackInfoDictionary = Dictionary<String,Any>()
    var arrayKickBackInfo = [TopupConfirmationModel]()
    var  model = [RechargePlanModel]()
    var navigation: UINavigationController!
    var isPlusSelected: Bool = false
    var cashBackdictionary = [Dictionary<String,Any>]()
    var processedTransaction = [Dictionary<String,Any>]()
    weak var requestMoneyDelegate: requestMoneyAmountCallback?
    weak var dismiss: RequestMoneyAmountDismissCallback?
    var isComingFromRecent = false
    
    //MARK: Pull To Refresh
    fileprivate lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(topupMyRechargePlanViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = MyNumberTopup.theme
        return refreshControl
    }()
    
    //MARK:- Views life cycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        self.enterAmount.placeholder = "Enter Amount".localized
        self.nxtBtn.setTitle("Next".localized, for: .normal)
        IQKeyboardManager.sharedManager().enable            = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        self.rechargePlanTableView.tableFooterView = UIView.init()
        self.backvView.backgroundColor = MyNumberTopup.theme
        self.rechargePlanTableView.addSubview(refreshControl)
        self.rechargePlanTableView.isHidden = false
        self.stackContainer.isHidden = true
        if appDelegate.checkNetworkAvail() == false {
            self.rechargePlanTableView.isHidden = true
            self.stackContainer.isHidden = false
            self.errLabel.text = "No Internet Connection".localized
        }
        self.urlRefresh()
        self.buttonHolder.backgroundColor = .lightGray
        NotificationCenter.default.addObserver(self, selector: #selector(topupMyRechargePlanViewController.networkLost), name: .networkLost, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(topupMyRechargePlanViewController.networkConnected), name: .networkConnected, object: nil)
        switch MyNumberTopup.theme {
        case MyNumberTopup.OperatorColorCode.mytel, MyNumberTopup.OperatorColorCode.telenor, MyNumberTopup.OperatorColorCode.mactel, MyNumberTopup.OperatorColorCode.oredo :
            constrarintEAView.constant = 60
        default:
            constrarintEAView.constant = 0
        }
        self.view.layoutIfNeeded()
        GeoLocationManager.shared.startUpdateLocation()
        subscribeToShowKeyboardNotifications()
    }
    
    func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //bottomLayoutConst.constant = 0
        if self.enterAmount != nil {
            self.enterAmount.text = ""
        }
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        UIView.animate(withDuration: 1, animations: { () -> Void in
            self.bottomLayoutConst.constant = keyboardHeight
            self.view.layoutIfNeeded()
        })
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        bottomLayoutConst.constant = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK:- Target Methods
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.urlRefresh()
        refreshControl.endRefreshing()
    }
    
    @objc func networkLost() {
        self.rechargePlanTableView.isHidden = true
        self.stackContainer.isHidden = false
        self.errLabel.text = "No Internet Connection".localized
    }
    
    @objc func networkConnected() {
        self.okActivityIndicator(view: self.view)
        let urlString = String.init(format: TopupUrl.rechargePlan, MyNumberTopup.opName)
        guard let url = URL.init(string: urlString) else { return }
        let par = Dictionary<String,String>()
        println_debug("\n\nUrl -> Get Method ->")
        println_debug(urlString)
        TopupWeb.genericClassWithoutLoader(url: url, param: par as AnyObject, httpMethod: "GET") { [weak self](response, success) in
            DispatchQueue.main.async {
                guard let weakSelf = self else { return }
                self?.removeActivityIndicator(view: weakSelf.view)
            }
            if success {
                self?.parseJsonData(response)
            }
        }
    }
    
    //MARK:- Methods
    func urlRefresh() {
        let urlString = String.init(format: TopupUrl.rechargePlan, MyNumberTopup.opName)
        guard let url = URL.init(string: urlString) else { return }
        let par = Dictionary<String,String>()
        println_debug("\n\nUrl -> Get Method ->")
        println_debug(urlString)
        TopupWeb.genericClass(url: url, param: par as AnyObject, httpMethod: "GET") { [weak self](response, success) in
            if success {
                self?.parseJsonData(response)
            }
        }
    }
    
    func parseJsonData(_ json: Any) {
        if let jsonWrapped = json as? Dictionary<String,Any> {
            if let stringData = jsonWrapped["Data"] as? String, (jsonWrapped["Code"] as? NSNumber)?.intValue == 200 {
                let dictionaryArray = OKBaseController.convertToArrDictionary(text: stringData)
                if dictionaryArray == nil {return}
                if let arrayObjects = dictionaryArray as? [Dictionary<String,Any>] {
                    self.model.removeAll()
                    for element in arrayObjects {
                        let amount = element.safeValueForKey("Amount") as? String ?? ""
                        let proMsg = element.safeValueForKey("PromotionMessage") as? String ?? ""
                        let customMSg = element.safeValueForKey("CustomJsonData") as? String ?? ""
                        // let modelRecharge = RechargePlanModel.init(amount ?? "0")
                        let modelRecharge = RechargePlanModel.init(amount, promoMsg: proMsg, customData: customMSg)
                        let isActive = (element["IsActive"] as? NSNumber)?.boolValue
                        if let active = isActive, active == true {
                            self.model.append(modelRecharge)
                        }
                    }
                    model = model.sorted(by: { $0.developerAmount.integerValue < $1.developerAmount.integerValue})
                    DispatchQueue.main.async {
                        if self.model.count > 0 {
                            self.rechargePlanTableView.isHidden = false
                            self.rechargePlanTableView.reloadData()
                            self.stackContainer.isHidden = true
                        } else {
                            self.rechargePlanTableView.isHidden = true
                            self.stackContainer.isHidden = false
                            self.errLabel.text = "No Recharge Plan Found".localized
                        }
                    }
                }
            }
        }
    }
    
    private func paymentgatewayNextButton() {
        self.enterAmount.resignFirstResponder()
        let textAmount = self.enterAmount.text
        guard let text = textAmount?.replacingOccurrences(of: ",", with: "") else { return }
        let model = RechargePlanModel.init(text)
        self.executionOfPayment(model: model)
    }
    
    private func executionOfPayment(model: RechargePlanModel) {
        if MyNumberTopup.operatorCase == .mactel {
            self.selectedObject = model
            self.callingFeeLimitKickBack()
        } else {
            let lat  = (GeoLocationManager.shared.currentLatitude == nil) ? "0.0" : GeoLocationManager.shared.currentLatitude
            let long = (GeoLocationManager.shared.currentLongitude == nil) ? "0.0" : GeoLocationManager.shared.currentLongitude
            var urlString = ""
            if let _ = otherNumberTopup {
                if model.selectedIndex.count > 0 {
                    guard let items = MultiTopupArray.main.controllers.first else { return }
                    guard var customer = items.mobileNumber.text else { return }
                    _ = customer.remove(at: String.Index.init(encodedOffset: 0))
                    let number  = "0" + customer
                    if model.selectedIndex == "2" {
                        var amount = model.developerAmount.integerValue
                        amount = amount + 1
                        urlString = String.init(format: TopupUrl.merchantNumberCheck, MyNumberTopup.opName,UserModel.shared.mobileNo,rechargeType.topup,number,"\(amount)", long ?? "0.0", lat ?? "0.0", UserLogin.shared.walletBal)
                    } else {
                        urlString = String.init(format: TopupUrl.merchantNumberCheck, MyNumberTopup.opName,UserModel.shared.mobileNo,rechargeType.topup,number,model.developerAmount, long ?? "0.0", lat ?? "0.0", UserLogin.shared.walletBal)
                    }
                } else {
                    guard let items = MultiTopupArray.main.controllers.first else { return }
                    guard var customer = items.mobileNumber.text else { return }
                    _ = customer.remove(at: String.Index.init(encodedOffset: 0))
                    let number  = "0" + customer
                    urlString = String.init(format: TopupUrl.merchantNumberCheck, MyNumberTopup.opName,UserModel.shared.mobileNo,rechargeType.topup,number,model.developerAmount, long ?? "0.0", lat ?? "0.0", UserLogin.shared.walletBal)
                }
            } else {
                if model.selectedIndex.count > 0 {
                    if model.selectedIndex == "2" {
                        var amount = model.developerAmount.integerValue
                        amount = amount + 1
                        urlString = String.init(format: TopupUrl.merchantNumberCheck, MyNumberTopup.opName,UserModel.shared.mobileNo,rechargeType.topup,UserModel.shared.formattedNumber,"\(amount)", long ?? "0.0", lat ?? "0.0", UserLogin.shared.walletBal)
                    } else {
                        urlString = String.init(format: TopupUrl.merchantNumberCheck, MyNumberTopup.opName,UserModel.shared.mobileNo,rechargeType.topup,UserModel.shared.formattedNumber,model.developerAmount, long ?? "0.0", lat ?? "0.0", UserLogin.shared.walletBal)
                    }
                } else {
                    urlString = String.init(format: TopupUrl.merchantNumberCheck, MyNumberTopup.opName,UserModel.shared.mobileNo,rechargeType.topup,UserModel.shared.formattedNumber,model.developerAmount, long ?? "0.0", lat ?? "0.0", UserLogin.shared.walletBal)
                }
            }
            // extra param added for lTopupPath
            if model.selectedIndex.count > 0 {
                urlString = urlString + "&lTopupPath=" + model.selectedIndex
            }
            urlString = urlString + "&SubScriberName=" + (telecoTopupSubscription?.first?.name ?? "").replacingOccurrences(of: " ", with: "%20")
            println_debug(urlString)
            guard let url = URL.init(string: urlString) else { return }
            let par = Dictionary<String,String>()
            TopupWeb.genericClass(url: url, param: par as AnyObject, httpMethod: "GET") { [weak self] (response, success) in
                if success {
                    if let json = response as? Dictionary<String,Any> {
                        if let code = json["Code"] as? NSNumber, code == 200 {
                            if let msg = json["Msg"] as? String, msg.lowercased() == "Success".lowercased() {
                                if let data = json["Data"] as? String {
                                    let array = OKBaseController.convertToArrDictionary(text: data)
                                    if array == nil { return }
                                    var merchantModel = [TopupMerchantNumberModel]()
                                    if let merchantData = array as? [Dictionary<String,Any>] {
                                        for element in merchantData {
                                            let operatr = element["Operator"] as? String
                                            let merNumber = element["MobileNumber"] as? String
                                            let modelMerchant = TopupMerchantNumberModel.init((operatr == nil) ? "" : operatr!, merchant: merNumber == nil ? "" : merNumber!)
                                            merchantModel.append(modelMerchant)
                                        }
                                        if merchantModel.count > 0 {
                                            self?.selectedMerchant = merchantModel.first
                                            self?.selectedObject = model
                                            self?.callingFeeLimitKickBack()
                                        }
                                    }
                                }
                            }
                        } else if let code = json["Code"] as? NSNumber, code == 300 {
                            self?.showErrorAlert(errMessage: "Your Service Provider is Currently Unavailable.".localized)
                        }
                    }
                }
            }
        }
    }
    
    private func paymentGatewayCalling(indexPath: IndexPath) {
        let model = self.model[indexPath.row]
        if (UserLogin.shared.walletBal as NSString).floatValue < (model.developerAmount).floatValue {
            alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
            alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                    addWithdrawView.modalPresentationStyle = .fullScreen
                    self.navigation.present(addWithdrawView, animated: true, completion: nil)
                }
            })
            alertViewObj.showAlert()
            return
        }
        var totalSum: Float = 0.0
        for controllers in MultiTopupArray.main.controllers {
            if let amountStr = controllers.amount.text, let amount = Float(amountStr) {
                totalSum = totalSum + amount
            }
        }
        if totalSum > (UserLogin.shared.walletBal as NSString).floatValue {
            alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
            alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                    addWithdrawView.modalPresentationStyle = .fullScreen
                    self.navigation.present(addWithdrawView, animated: true, completion: nil)
                }
            })
            alertViewObj.showAlert()
            return
        }
        if appDelegate.checkNetworkAvail() == false {
            alertViewObj.wrapAlert(title: nil, body: "No Internet Connection", img: #imageLiteral(resourceName: "dashboard_my_number"))
            alertViewObj.addAction(title: "OK", style: .cancel, action: {
            })
            alertViewObj.showAlert(controller: self)
            return
        }
        if let _ = otherNumberTopup {
            if let delegate = self.requestMoneyDelegate {
                delegate.didGetAmount(model.formattedAmount, withPlanType: "Top-Up Plan")
                if MultiTopupArray.main.controllers.count > 1 {
                    dismiss?.didDismissCallback()
                } else {
                    if model.selectedIndex.count > 0 {
                        delegate.paymentCallOtherNumberWithCustomMsg(index: model.selectedIndex)
                    } else {
                        delegate.paymentCallOtherNumberThroughDelegate()
                    }
                    dismiss?.didDismissCallback()
                }
            }
        } else {
            self.executionOfPayment(model: model)
        }
    }
    
    func otherNumberPayment(model: RechargePlanModel) {
        self.selectedObject = model
        guard let url = URL.init(string: TopupUrl.otherNumberTopupRetrieve) else { return }
        let params = MultiTopupParameters().getMultiTopupSim(MultiTopupArray.main.controllers, plan: "Top-Up Plan", type: "0")
        let jsonParam = JSONStringFromAnyObject(value: params as AnyObject)
        TopupWeb.genericClass(url: url, param: jsonParam as AnyObject, httpMethod: "POST") { [weak self](response, success) in
            if success {
                DispatchQueue.main.async {
                    if let json = response as? [Dictionary<String,Any>] {
                        self?.cashBackdictionary.removeAll()
                        for element in json {
                            if let str = element.safeValueForKey("Data") as? String {
                                if let code = element.safeValueForKey("Code") as? Int, code == 200 {
                                    if let dict = OKBaseController.convertToArrDictionary(text: str) {
                                        if let arrDict = dict as? [Dictionary<String,Any>] {
                                            if let first = arrDict.first {
                                                self?.cashBackdictionary.append(first)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if UserLogin.shared.loginSessionExpired {
                            guard let weakSelf = self else { return }
                            OKPayment.main.authenticate(screenName: "multiCashback", delegate: weakSelf)
                        } else {
                            self?.cashbackApiCallingForOtherNumber()
                        }
                    }
                }
            }
        }
    }
    
    func cashbackApiCallingForOtherNumber() {
        guard  let url = URL.init(string: TopupUrl.otherNumberMultiCashback) else { return }
        guard let model = self.selectedObject else { return }
        let param = JSONStringFromAnyObject(value: MultiTopupParameters().cashbackMultiApiParameters(mpModel: MultiTopupArray.main.controllers, amount: model.developerAmount as String, dictCashback: self.cashBackdictionary) as AnyObject)
        TopupWeb.genericClass(url: url, param: param as AnyObject, httpMethod: "POST") { [weak self](response, successfull) in
            if successfull {
                DispatchQueue.main.async {
                    if let value = response as? [Dictionary<String,AnyObject>] {
                        for (_,item) in value.enumerated() {
                            let obj = item["Data"] as? String ?? ""
                            let xml = SWXMLHash.parse(obj)
                            self?.enumerate(indexer: xml)
                            if let transaction = self?.kickBackInfoDictionary.safeValueForKey("resultdescription") as? String, transaction == "Transaction Successful" {
                                let number = (self?.kickBackInfoDictionary.safeValueForKey("destination") as? String) == nil ? "" : (self?.kickBackInfoDictionary.safeValueForKey("destination") as? String)
                                let operatorName = MyNumberTopup.opName
                                let type     = RechargePlanType.recharge
                                let cashback = (self?.kickBackInfoDictionary.safeValueForKey("kickback") as? String) == nil ? "" : (self?.kickBackInfoDictionary.safeValueForKey("kickback") as? String)
                                let amount   = (self?.kickBackInfoDictionary.safeValueForKey("amount") as? String) == nil ? "" : (self?.kickBackInfoDictionary.safeValueForKey("amount") as? String)
                                
                                let model = TopupConfirmationModel.init("Unknown", number: number ?? "", opeName: operatorName, type: type, cashbk: cashback ?? "", topAmt: amount ?? "", payNumber: UserModel.shared.mobileNo)
                                self?.arrayKickBackInfo.append(model)
                                // PaymentVerificationManager.storeTransactions(model: PaymentTransactions.init(number ?? "", type: "Top-Up Plan", name: "", amount: amount ?? "", opName: operatorName))
                                guard  let vc = self?.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TopupConfirmationViewController.self)) as? TopupConfirmationViewController else { return }
                                guard let weakSelf = self else { return }
                                vc.confirmResponseModel = weakSelf.arrayKickBackInfo
                                vc.type = "Top-Up Plan"
                                vc.screenType = ConfirmScreenType.otherNumber
                                vc.isComingFromRecent = weakSelf.isComingFromRecent
                                self?.navigation.pushViewController(vc, animated: true)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func callingFeeLimitKickBack() {
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "", delegate: self)
        } else {
            self.biometricAuthentication(isSuccessful: true, inScreen: "")
        }
    }
    
    func parsingFeeLimitKickbackResponse(xml: String) {
        self.arrayKickBackInfo.removeAll()
        let xml = SWXMLHash.parse(xml)
        self.enumerate(indexer: xml)
        println_debug(kickBackInfoDictionary)
        if let transaction = kickBackInfoDictionary.safeValueForKey("resultdescription") as? String, transaction == "Transaction Successful" {
            let number = (kickBackInfoDictionary.safeValueForKey("destination") as? String) == nil ? "" : (kickBackInfoDictionary.safeValueForKey("destination") as? String)
            let operatorName = MyNumberTopup.opName
            let type     = RechargePlanType.recharge
            let cashback = (kickBackInfoDictionary.safeValueForKey("kickback") as? String) == nil ? "" : (kickBackInfoDictionary.safeValueForKey("kickback") as? String)
            let amount   = (kickBackInfoDictionary.safeValueForKey("amount") as? String) == nil ? "" : (kickBackInfoDictionary.safeValueForKey("amount") as? String)
            
            if  !PaymentVerificationManager.isValidPaymentTransactionsForOperator(operatorName, amount ?? "", number ?? "") {
                self.biometricAuthentication(isSuccessful: true, inScreen: "")
                return
            }
            
//            //Remove alert for topupMyRechargePlanViewController
//            DispatchQueue.main.async {
//                alertViewObj.wrapAlert(title: "Backend Number", body: number ?? "".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
//                alertViewObj.addAction(title: "OK", style: .cancel, action: {
//                })
//                alertViewObj.showAlert()
//            }
            
            let model = TopupConfirmationModel.init(UserModel.shared.name, number: number ?? "", opeName: operatorName, type: type, cashbk: cashback ?? "", topAmt: amount ?? "", payNumber: UserModel.shared.mobileNo)
            self.arrayKickBackInfo.append(model)
            DispatchQueue.main.async {
                // PaymentVerificationManager.storeTransactions(model: PaymentTransactions.init(UserModel.shared.mobileNo, type: "Top-Up Plan", name: "", amount: amount ?? "", opName: operatorName))
                guard  let vc = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TopupConfirmationViewController.self)) as? TopupConfirmationViewController else { return }
                vc.confirmResponseModel = self.arrayKickBackInfo
                vc.type = "Top-Up Plan"
                vc.screenType = ConfirmScreenType.mynumber
                vc.isComingFromRecent = self.isComingFromRecent
                self.navigation.pushViewController(vc, animated: true)
            }
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            kickBackInfoDictionary[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    fileprivate func checkSameAmountAndNumber(_ amount: String) -> Bool {
        if MultiTopupArray.main.controllers.count > 1 {
            guard let last = MultiTopupArray.main.controllers[safe: MultiTopupArray.main.controllers.count - 1] else { return false }
            for (_, lastObject) in MultiTopupArray.main.controllers.enumerated() {
                if last.mobileNumber.text == lastObject.mobileNumber.text && amount == lastObject.amount.text {
                    alertViewObj.wrapAlert(title: nil, body: "Same Mobile Number with Same Top-Up amount, Same Data Plan or Same Special Offer Not Allowed".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                    alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                    })
                    alertViewObj.showAlert()
                    return true
                }
            }
        }
        return false
    }
    
    fileprivate func check2MinsValidation(_ amount: String) -> Bool {
        let amountStr = amount.replacingOccurrences(of: ",", with: "")
        if let vc = MultiTopupArray.main.controllers.last  {
            let number = getAgentCode(numberWithPrefix: vc.mobileNumber.text.safelyWrappingString(), havingCountryPrefix: "+95")
            if !PaymentVerificationManager.isValidPaymentTransactions(number, amountStr, "TOPUP") {
                alertViewObj.wrapAlert(title: nil, body: "Same Number with Same Amount can not be paid before 2 minutes".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                })
                alertViewObj.showAlert()
                return true
            }
        }
        return false
    }
    
    //MARK:- Button Action Methods
    @IBAction func nextActionButton(_ sender: UIButton) {
//        let min = self.model.min(by: { $0.amount < $1.amount })
//        if ((self.enterAmount.text?.replacingOccurrences(of: ",", with: "") ?? "") as NSString).floatValue < min?.developerAmount.floatValue ?? 0.00 {
//            let image = #imageLiteral(resourceName: "dashboard_my_number")
//            alertViewObj.wrapAlert(title: nil, body: "Entered amount is invalid. Please re-enter valid denomination Top-Up amount".localized, img: image)
//            alertViewObj.addAction(title: "OK".localized, style: .target, action: {
//            })
//            alertViewObj.showAlert()
//            return
//        }
        
        if let text = self.enterAmount.text, text.count > 0 {
            if (text.replacingOccurrences(of: ",", with: "") as NSString).floatValue >= 1000.0 {
                if !amountValidationCheck, (UserLogin.shared.walletBal as NSString).floatValue < (text.replacingOccurrences(of: ",", with: "") as NSString).floatValue {
                    alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                    alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                        if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                            addWithdrawView.modalPresentationStyle = .fullScreen
                            self.navigation.present(addWithdrawView, animated: true, completion: nil)
                        }
                    })
                    alertViewObj.showAlert()
                    return
                }
                
                if MyNumberTopup.OperatorColorCode.telenor == MyNumberTopup.theme || MyNumberTopup.OperatorColorCode.mactel == MyNumberTopup.theme || MyNumberTopup.OperatorColorCode.mytel == MyNumberTopup.theme {
                    if (text.replacingOccurrences(of: ",", with: "") as NSString).floatValue.truncatingRemainder(dividingBy: 1000.0) != 0  {
                        var image = #imageLiteral(resourceName: "dashboard_my_number")
                        if let _ = otherNumberTopup {
                            image = #imageLiteral(resourceName: "dashboard_other_number.png")
                        }
                        alertViewObj.wrapAlert(title: nil, body: "Entered amount is invalid. Please re-enter valid denomination Top-Up amount".localized, img: image)
                        alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                        })
                        alertViewObj.showAlert()
                    } else {
                        if let _ = requestMoney {
                            if  let text = self.enterAmount.text, text.count > 0 {
                                if (text.replacingOccurrences(of: ",", with: "") as NSString).floatValue <= 0 {
                                    if MultiTopupArray.main.controllers.count > 1 {
                                        alertViewObj.wrapAlert(title: nil, body: "Please Enter Amount".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
                                    } else {
                                        alertViewObj.wrapAlert(title: nil, body: "Please Enter Amount".localized, img: #imageLiteral(resourceName: "dashboard_my_number"))
                                    }
                                    alertViewObj.addAction(title: "OK", style: .cancel, action: {
                                        self.enterAmount.text = ""
                                        self.enterAmount.becomeFirstResponder()
                                    })
                                    alertViewObj.showAlert(controller: self)
                                    return
                                }
                                let status2Mins = self.check2MinsValidation(text)
                                if status2Mins {
                                    self.enterAmount.text = ""
                                    return
                                }
                                let status = self.checkSameAmountAndNumber(text)
                                if status {
                                    self.enterAmount.text = ""
                                    return
                                } else {
                                    requestMoneyDelegate?.didGetAmount(text, withPlanType: "Top-Up Plan")
                                    if let delegate = self.requestMoneyDelegate {
                                        delegate.paymentCallOtherNumberThroughDelegate()
                                    }
                                    dismiss?.didDismissCallback()
                                    return
                                }
                            }
                        } else {
                            if (text.replacingOccurrences(of: ",", with: "") as NSString).floatValue.truncatingRemainder(dividingBy: 1000.0) == 0 {
                                self.paymentgatewayNextButton()
                                dismiss?.didDismissCallback()
                                return
                            } else if MyNumberTopup.OperatorColorCode.telenor == MyNumberTopup.theme || MyNumberTopup.OperatorColorCode.mactel == MyNumberTopup.theme {
                                var image = #imageLiteral(resourceName: "dashboard_my_number")
                                if let _ = otherNumberTopup {
                                    image = #imageLiteral(resourceName: "dashboard_other_number.png")
                                }
                                alertViewObj.wrapAlert(title: nil, body: "Entered amount is invalid. Please re-enter valid denomination Top-Up amount.".localized, img: image)
                                alertViewObj.addAction(title: "OK", style: .cancel, action: {
                                    self.enterAmount.text = ""
                                    self.enterAmount.becomeFirstResponder()
                                })
                                alertViewObj.showAlert(controller: self)
                                return
                            } else {
                                self.paymentgatewayNextButton()
                            }
                        }
                    }
                } else if MyNumberTopup.OperatorColorCode.oredo == MyNumberTopup.theme {
                    let status2Mins = self.check2MinsValidation(text)
                    if status2Mins {
                        self.enterAmount.text = ""
                        return
                    }
                    let status = self.checkSameAmountAndNumber(text)
                    if status {
                        self.enterAmount.text = ""
                        return
                    } else {
                        requestMoneyDelegate?.didGetAmount(text, withPlanType: "Top-Up Plan")
                        if let delegate = self.requestMoneyDelegate {
                            delegate.paymentCallOtherNumberThroughDelegate()
                        }
                        self.paymentgatewayNextButton()
                        dismiss?.didDismissCallback()
                        return
                    }
                    
                    
                }
                if appDelegate.checkNetworkAvail() == false {
                    alertViewObj.wrapAlert(title: nil, body: "No Internet Connection", img: #imageLiteral(resourceName: "dashboard_my_number"))
                    alertViewObj.addAction(title: "OK", style: .cancel, action: {
                        self.enterAmount.becomeFirstResponder()
                    })
                    alertViewObj.showAlert(controller: self)
                    return
                }
                if let _ = requestMoney {
                    if  let text = self.enterAmount.text, text.count > 0 {
                        if (text.replacingOccurrences(of: ",", with: "") as NSString).floatValue <= 0 {
                            if MultiTopupArray.main.controllers.count > 1 {
                                alertViewObj.wrapAlert(title: nil, body: "Please Enter Amount".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
                            } else {
                                alertViewObj.wrapAlert(title: nil, body: "Please Enter Amount".localized, img: #imageLiteral(resourceName: "dashboard_my_number"))
                            }
                            alertViewObj.addAction(title: "OK", style: .cancel, action: {
                                self.enterAmount.text = ""
                                self.enterAmount.becomeFirstResponder()
                            })
                            alertViewObj.showAlert(controller: self)
                            return
                        }
                        if let _ = otherNumberTopup {
                            if (text.replacingOccurrences(of: ",", with: "") as NSString).floatValue.truncatingRemainder(dividingBy: 1000.0) == 0 {
                                requestMoneyDelegate?.didGetAmount(text, withPlanType: "Top-Up Plan")
                                dismiss?.didDismissCallback()
                                return
                                // self.paymentgatewayNextButton()
                            } else if MyNumberTopup.OperatorColorCode.telenor == MyNumberTopup.theme || MyNumberTopup.OperatorColorCode.mactel == MyNumberTopup.theme || MyNumberTopup.OperatorColorCode.mytel == MyNumberTopup.theme || MyNumberTopup.OperatorColorCode.mytel == MyNumberTopup.theme {
                                var image = #imageLiteral(resourceName: "dashboard_my_number")
                                if let _ = otherNumberTopup {
                                    image = #imageLiteral(resourceName: "dashboard_other_number.png")
                                }
                                alertViewObj.wrapAlert(title: nil, body: "Entered amount is invalid. Please re-enter valid denomination Top-Up amount.".localized, img: image)
                                alertViewObj.addAction(title: "OK", style: .cancel, action: {
                                    self.enterAmount.text = ""
                                    self.enterAmount.becomeFirstResponder()
                                })
                                alertViewObj.showAlert(controller: self)
                                return
                            } else {
                                requestMoneyDelegate?.didGetAmount(text, withPlanType: "Top-Up Plan")
                                dismiss?.didDismissCallback()
                                // self.paymentgatewayNextButton()
                            }
                            return
                        }
                        requestMoneyDelegate?.didGetAmount(text, withPlanType: "Top-Up Plan")
                        dismiss?.didDismissCallback()
                        return
                    }
                } else {
                    if (text.replacingOccurrences(of: ",", with: "") as NSString).floatValue.truncatingRemainder(dividingBy: 1000.0) == 0 {
                        self.paymentgatewayNextButton()
                    } else if MyNumberTopup.OperatorColorCode.telenor == MyNumberTopup.theme || MyNumberTopup.OperatorColorCode.mactel == MyNumberTopup.theme ||  MyNumberTopup.OperatorColorCode.mytel == MyNumberTopup.theme {
                        var image = #imageLiteral(resourceName: "dashboard_my_number")
                        if let _ = otherNumberTopup {
                            image = #imageLiteral(resourceName: "dashboard_other_number.png")
                        }
                        alertViewObj.wrapAlert(title: nil, body: "Entered amount is invalid. Please re-enter valid denomination Top-Up amount.".localized, img: image)
                        alertViewObj.addAction(title: "OK", style: .cancel, action: {
                            self.enterAmount.text = ""
                            self.enterAmount.becomeFirstResponder()
                        })
                        alertViewObj.showAlert(controller: self)
                        return
                    } else {
                        self.paymentgatewayNextButton()
                    }
                }
            } else {
                var image = #imageLiteral(resourceName: "dashboard_my_number")
                if let _ = otherNumberTopup {
                    image = #imageLiteral(resourceName: "dashboard_other_number.png")
                }
                if MyNumberTopup.OperatorColorCode.oredo == MyNumberTopup.theme {
                    alertViewObj.wrapAlert(title: nil, body: "You can Top-Up amount between 1,000 MMK and 50,000 MMK".localized, img: image)
                } else if MyNumberTopup.OperatorColorCode.mactel == MyNumberTopup.theme {
                    alertViewObj.wrapAlert(title: nil, body: "You can Top-Up amount between 1,000 MMK and 10,000 MMK".localized, img: image)
                } else {
                    alertViewObj.wrapAlert(title: nil, body: "You can Top-Up amount between 1,000 MMK and 100,000 MMK".localized, img: image)
                }
                alertViewObj.addAction(title: "OK", style: .cancel, action: {
                    self.enterAmount.becomeFirstResponder()
                })
                alertViewObj.showAlert(controller: self)
            }
        } else {
            if let _ = otherNumberTopup {
                alertViewObj.wrapAlert(title: nil, body: "Please Enter Amount".localized, img: #imageLiteral(resourceName: "dashboard_other_number.png"))
                alertViewObj.addAction(title: "OK", style: .cancel, action: {
                    self.enterAmount.becomeFirstResponder()
                })
            } else {
                alertViewObj.wrapAlert(title: nil, body: "Please Enter Amount".localized, img: #imageLiteral(resourceName: "dashboard_my_number"))
                alertViewObj.addAction(title: "OK", style: .cancel, action: {
                    self.enterAmount.becomeFirstResponder()
                })
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    deinit {
        self.model.removeAll()
        self.selectedObject = nil
        self.selectedMerchant = nil
        NotificationCenter.default.removeObserver(self)
    }
}

//MARK: - UITableViewDelegate, UITableViewDataSource
extension topupMyRechargePlanViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        println_debug(model.count)
        return model.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TopupMyRechargeCell.self), for: indexPath) as? TopupMyRechargeCell
        cell?.wrap(self.model[indexPath.row].amount)
        var myNumbertopUp = true
        if let _ = otherNumberTopup {
            if MultiTopupArray.main.controllers.count > 1 {
                myNumbertopUp = false
            }else {
                myNumbertopUp = true
            }
        }
        cell?.appendingPromotionMessage(str: self.model[indexPath.row].promoMsg, popUpTrue: self.model[indexPath.row].isPopUp, myNumber: myNumbertopUp, offervalue: self.model[indexPath.row].offerValue)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.00
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        func checkSufficientBalance(reqAmount: Float) -> Bool {
            if reqAmount > (UserLogin.shared.walletBal as NSString).floatValue {
                alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                    if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                        addWithdrawView.modalPresentationStyle = .fullScreen
                        self.navigation.present(addWithdrawView, animated: true, completion: nil)
                    }
                })
                alertViewObj.showAlert()
                return false
            }
            return true
        }
        if let _ = otherNumberTopup {
            let model = self.model[indexPath.row]
            print(model.developerAmount as String)
            if let vc = MultiTopupArray.main.controllers.last  {
                let number = getAgentCode(numberWithPrefix: vc.mobileNumber.text.safelyWrappingString(), havingCountryPrefix: "+95")
                if !PaymentVerificationManager.isValidPaymentTransactions(number, model.developerAmount as String, "TOPUP") {
                    alertViewObj.wrapAlert(title: nil, body: "Same Number with Same Amount can not be paid before 2 minutes".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                    alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                    })
                    alertViewObj.showAlert()
                    return
                }
            }
            if MultiTopupArray.main.controllers.count > 1 {
                if let last = MultiTopupArray.main.controllers[safe: MultiTopupArray.main.controllers.count - 1], let secondLast = MultiTopupArray.main.controllers[safe: MultiTopupArray.main.controllers.count - 2] {
                    if let lastText = last.mobileNumber.text, let secondLastText = secondLast.mobileNumber.text {
                        if lastText == secondLastText {
                            if let lastAmount = last.amount.text, let secondLastAmount = secondLast.amount.text {
                                if lastAmount == secondLastAmount {
                                    alertViewObj.wrapAlert(title: nil, body: "Same Mobile Number with Same Top-Up amount, Same Data Plan or Same Special Offer Not Allowed".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                                    alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                                    })
                                    alertViewObj.showAlert()
                                    return
                                }
                            }
                        }
                    }
                }
            }
            
            var totalAmount = (model.developerAmount).floatValue
            for element in MultiTopupArray.main.controllers {
                totalAmount = totalAmount + ((element.amount.text?.replacingOccurrences(of: ",", with: "") ?? "0") as NSString).floatValue
            }
            if amountValidationCheck {
                let model = self.model[indexPath.row]
                requestMoneyDelegate?.didGetAmount(model.formattedAmount, withPlanType: "Top-Up Plan")
                dismiss?.didDismissCallback()
                return
            }
            if !checkSufficientBalance(reqAmount: totalAmount) {
                return
            }
            if model.eloadMsg.count > 0 {
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TopupMyRechargeDetailSelection.self)) as? TopupMyRechargeDetailSelection else { return }
                vc.delegate = self
                vc.selectedModel = model
                vc.displayString = (model.eloadMsg, model.mainBalMsg)
                vc.modalPresentationStyle = .overCurrentContext
                navigation.present(vc, animated: true, completion: nil)
            }else {
            self.paymentGatewayCalling(indexPath: indexPath)
            }
            return
        }
        if let _ = requestMoney {
            let model = self.model[indexPath.row]
            requestMoneyDelegate?.didGetAmount(model.formattedAmount, withPlanType: "Top-Up Plan")
            dismiss?.didDismissCallback()
        } else {
            
            
            let model = self.model[indexPath.row]
            if !PaymentVerificationManager.isValidPaymentTransactions(UserModel.shared.mobileNo, model.developerAmount as String, "TOPUP") {
                alertViewObj.wrapAlert(title: nil, body: "Same Number with Same Amount can not be paid before 2 minutes".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                })
                alertViewObj.showAlert()
                return
            }
            if !checkSufficientBalance(reqAmount: (model.developerAmount).floatValue) {
                return
            }
            if model.eloadMsg.count > 0 {
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TopupMyRechargeDetailSelection.self)) as? TopupMyRechargeDetailSelection else { return }
                vc.delegate = self
                vc.selectedModel = model
                vc.displayString = (model.eloadMsg, model.mainBalMsg)
                vc.modalPresentationStyle = .overCurrentContext
                navigation.present(vc, animated: true, completion: nil)
            } else {
                self.paymentGatewayCalling(indexPath: indexPath)
            }
        }
    }
}

//MARK:- TopupMyRechargeDetailSelectionDelegate
extension topupMyRechargePlanViewController: TopupMyRechargeDetailSelectionDelegate {
    func didSelectEloadMsg(model: RechargePlanModel) {
        if let _ = otherNumberTopup {
            if let delegate = self.requestMoneyDelegate {
                delegate.didGetAmount(model.formattedAmount, withPlanType: "Top-Up Plan")
                if MultiTopupArray.main.controllers.count > 1 {
                    dismiss?.didDismissCallback()
                } else {
                    if model.selectedIndex.count > 0 {
                        delegate.paymentCallOtherNumberWithCustomMsg(index: model.selectedIndex)
                    } else {
                        delegate.paymentCallOtherNumberThroughDelegate()
                    }
                    navigation.dismiss(animated: true, completion: nil)
                }
            }
        } else {
            self.executionOfPayment(model: model)
        }
    }
}

//MARK:- UITextFieldDelegate
extension topupMyRechargePlanViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: mobileNumberAcceptableCharacters).inverted
        let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
        if string != filteredSet {
            return false
        }
        if text.count > 0 {
            self.buttonHolder.backgroundColor = MyNumberTopup.theme
        } else {
            self.buttonHolder.backgroundColor = .lightGray
        }
        if text.contains(".") {
            return false
        } else {
            let max = self.model.max(by: { $0.amount < $1.amount })
     
            
            if MyNumberTopup.OperatorColorCode.mpt == MyNumberTopup.theme {
                if (text.replacingOccurrences(of: ",", with: "") as NSString).floatValue > max?.developerAmount.floatValue ?? 0.00  {
                    return false
                }
            }
            if MyNumberTopup.OperatorColorCode.telenor == MyNumberTopup.theme {
                if (text.replacingOccurrences(of: ",", with: "") as NSString).floatValue > max?.developerAmount.floatValue ?? 0.00  {
                    return false
                }
            }
            if MyNumberTopup.OperatorColorCode.oredo == MyNumberTopup.theme {
                if (text.replacingOccurrences(of: ",", with: "") as NSString).floatValue > max?.developerAmount.floatValue ?? 0.00  {
                    return false
                }
            }
            if MyNumberTopup.OperatorColorCode.mactel == MyNumberTopup.theme {
                if (text.replacingOccurrences(of: ",", with: "") as NSString).floatValue > max?.developerAmount.floatValue ?? 0.00 {
                    return false
                }
            }
            let mystring = text.replacingOccurrences(of: ",", with: "")
            let number = NSDecimalNumber(string: mystring)
            let formatter = NumberFormatter()
            formatter.groupingSeparator = ","
            formatter.groupingSize = 3
            formatter.usesGroupingSeparator = true
            var num = formatter.string(from: number)
            if num == "NaN" {
                num = ""
            }
            textField.text = num
            return false
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.refreshControl.endRefreshing()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        bottomLayoutConst.constant = 0
        self.refreshControl.endRefreshing()
        self.rechargePlanTableView.scrollToTop()
        println_debug("STARTED text editing ")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //bottomLayoutConst.constant = 0
        println_debug("Ended text editing")
    }
}

//MARK:- BioMetricLoginDelegate
extension topupMyRechargePlanViewController: BioMetricLoginDelegate {
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if screen == "multiCashback" {
            if isSuccessful {
                self.cashbackApiCallingForOtherNumber()
            }
            return
        }
        if isSuccessful {
            var type = "PAYTO"
            if MyNumberTopup.operatorCase == .mactel {
                selectedMerchant = TopupMerchantNumberModel.init(MyNumberTopup.opName, merchant: UserModel.shared.mobileNo)
                type = "TOPUP"
            }
            
            let urlString = String.init(format: TopupUrl.cashback,UserModel.shared.mobileNo, selectedMerchant!.merchantNumber, selectedObject!.developerAmount,ok_password ?? "",type,UserLogin.shared.token)
            println_debug(urlString)
            let url = URL.init(string: urlString)
            let par = Dictionary<String,String>()
            TopupWeb.genericClassXML(url: url!, param: par as AnyObject, httpMethod: "POST") { [weak self](respponse, success) in
                if success {
                    if let xmlResponse = respponse as? String {
                        self?.parsingFeeLimitKickbackResponse(xml: xmlResponse)
                    }
                } else {
                    println_debug(respponse)
                }
            }
        }
    }
}

//MARK:- UITableViewCell
class TopupMyRechargeCell: UITableViewCell {
    weak var timer = Timer()
    //MARK:- Outlets
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var offerLabel: UILabel! {
        didSet {
            self.offerLabel.isHidden = true
        }
    }
    @IBOutlet weak var offerView: UIView! {
        didSet {
            self.offerView.isHidden = true
        }
    }
    
    //MARK:- Methods
    func wrap<Amount>(_ amount: Amount) {
        if let stringAmount = amount as? String {
            self.amountLabel.text = stringAmount.replacingOccurrences(of: "MMK", with: "")
        }
    }
    
    func appendingPromotionMessage(str: String, popUpTrue: Bool = false, myNumber: Bool, offervalue: String) {
        timer?.invalidate()
        if popUpTrue, myNumber {
            self.offerLabel.text = offervalue
            timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(showHideOfferLabel), userInfo: nil, repeats: true)
        } else {
            offerLabel.isHidden = true
            offerView.isHidden = true
        }
        let _ = self.amountLabel.text?.appending(" ")
        let _ = self.amountLabel.text?.appending(str)
    }
    
    @objc func showHideOfferLabel() {
        if offerLabel.isHidden == false {
            offerLabel.isHidden = true
            offerView.isHidden = true
        } else {
            offerLabel.isHidden = false
            offerView.isHidden = false
        }
    }
    
    deinit {
        timer?.invalidate()
    }
}

//MARK:- Model classes
struct rechargeType {
    static let topup: String  = "0"
    static let dataPlan: String = "1"
    static let specialoffer: String = "2"
}

struct TopupMerchantNumberModel {
    var operatorNumber = ""
    var merchantNumber = ""
    init(_ op: String, merchant: String) {
        self.merchantNumber = merchant
        self.operatorNumber = op
    }
}

struct TopupConfirmationModel {
    var name = "Unknown"
    var number = ""
    var operatorName = ""
    var type = ""
    var cashback = ""
    var developerCashback = ""
    var topupAmount = ""
    var developerAmount = ""
    var paymentNumber  = ""
    
    init(_ name: String, number: String, opeName: String, type: String, cashbk: String, topAmt: String, payNumber: String) {
        self.name = name
        self.number = number
        self.operatorName = opeName
        self.type = type
        self.cashback = cashbk + " " +  "MMK"
        self.topupAmount = topAmt + " " + "MMK"
        self.developerAmount = topAmt
        self.developerCashback = cashbk
        self.paymentNumber = payNumber
    }
}
