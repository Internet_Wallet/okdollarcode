//
//  TopupMySpecialOfferViewController.swift
//  OK
//
//  Created by Ashish on 1/27/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import MessageUI
import IQKeyboardManagerSwift

class TopupMySpecialOfferViewController: OKBaseController, UITableViewDelegate, UITableViewDataSource, BioMetricLoginDelegate, MFMessageComposeViewControllerDelegate, openSmsScreenForTopupDelegate {

    @IBOutlet var specialOfferTableView: UITableView!
    var navigation : UINavigationController!

    
    var arrayKickBackInfo = [TopupConfirmationModel]()
    var kickBackInfoDictionary = Dictionary<String,Any>()

    fileprivate var model = [SpecialOfferModel]()
    fileprivate var selectedObject   : SpecialOfferModel?
    fileprivate var selectedMerchant : TopupMerchantNumberModel?
    @IBOutlet weak var containerStack: UIStackView!
    @IBOutlet weak var errLabel: UILabel!{
        didSet
        {
            errLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    var requestMoney : String?
    var otherNumberTopup : String?
    var amountValidationCheck: Bool = false
    weak var requestMoneyDelegate : requestMoneyAmountCallback?
    weak var dismiss : RequestMoneyAmountDismissCallback?
    var isComingFromRecent = false
    
    //MARK:- Pull To Refresh
    fileprivate lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(TopupMySpecialOfferViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = MyNumberTopup.theme
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.urlRefresh()
        
        refreshControl.endRefreshing()
        
    }

    func urlRefresh() {
        let urlString = String.init(format: TopupUrl.specialOffer, MyNumberTopup.opName)
        let url = URL.init(string: urlString)
        let par = Dictionary<String,String>()
        TopupWeb.genericClass(url: url!, param: par as AnyObject, httpMethod: "GET") { [weak self](response, success) in
            if success {
                self?.parseJsonData(response)
            }
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //vinnu commented
        if (MyNumberTopup.theme  == MyNumberTopup.OperatorColorCode.mpt || MyNumberTopup.theme == MyNumberTopup.OperatorColorCode.mactel || MyNumberTopup.theme  == MyNumberTopup.OperatorColorCode.mytel), let _ = otherNumberTopup {
            self.specialOfferTableView.isHidden = true
            self.containerStack.isHidden = false
            self.errLabel.text = "No Special Offers".localized
        } else {
            IQKeyboardManager.sharedManager().enable            = false
            IQKeyboardManager.sharedManager().enableAutoToolbar = false
            
            self.specialOfferTableView.tableFooterView = UIView.init()
            
            self.specialOfferTableView.addSubview(refreshControl)
            
            self.containerStack.isHidden = false
            self.errLabel.text = "No Special Offers".localized
            
            self.urlRefresh()
            
            if appDelegate.checkNetworkAvail() == false {
                self.specialOfferTableView.isHidden = true
                self.containerStack.isHidden = false
                self.errLabel.text = "No Internet Connection".localized
            }
            
            NotificationCenter.default.addObserver(self, selector: #selector(TopupMySpecialOfferViewController.networkLost), name: .networkLost, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(TopupMySpecialOfferViewController.networkConnected), name: .networkConnected, object: nil)
            
        }
        
        
    }
    
    //vinnu commented
    
    func otherNumberUI() {
        if let _ = otherNumberTopup {
            if MyNumberTopup.theme  == MyNumberTopup.OperatorColorCode.mpt || MyNumberTopup.theme == MyNumberTopup.OperatorColorCode.mactel {
                self.specialOfferTableView.isHidden = true
                self.containerStack.isHidden = false
                self.errLabel.text = "No Special Offers".localized
            }
        }
    }
    
    @objc func networkLost() {
        self.specialOfferTableView.isHidden = true
        self.containerStack.isHidden = false
        self.errLabel.text = "No Internet Connection".localized
    }
    
    @objc func networkConnected() {
        
        //vinnu commented
        if let _ = otherNumberTopup {
            otherNumberUI()
            return
        }
        self.containerStack.isHidden = true
        self.okActivityIndicator(view: self.view)
        
        let urlString = String.init(format: TopupUrl.specialOffer, MyNumberTopup.opName)
        let url = URL.init(string: urlString)
        let par = Dictionary<String,String>()
        TopupWeb.genericClassWithoutLoader(url: url!, param: par as AnyObject, httpMethod: "GET") { [weak self](response, success) in
            DispatchQueue.main.async {
                guard let weakSelf = self else { return }
                self?.removeActivityIndicator(view: weakSelf.view)
            }

            if success {
                self?.parseJsonData(response)
            }
        }
    }

    
    
    func parseJsonData(_ json: Any) {
        
        if let jsonWrapped = json as? Dictionary<String,Any> {
            if let stringData = jsonWrapped["Data"] as? String, (jsonWrapped["Code"] as? NSNumber)?.intValue == 200 {
                let dictionaryArray = OKBaseController.convertToArrDictionary(text: stringData)
                
                if dictionaryArray == nil {return}
                
                if let arrayObjects = dictionaryArray as? [Dictionary<String,Any>] {
                    self.model.removeAll()
                    for element in arrayObjects {
                        
                        let promotionMessage = element["PromotionMessage"] as? String
                        let amount = element["Amount"] as? String
                        let ussdCode = element.safeValueForKey("UssdDialling") as? String
                        let isActive = (element["IsActive"] as? NSNumber)?.boolValue
                        
                        if let active = isActive, active == true {
                            let modelOffer = SpecialOfferModel.init(promotionMessage ?? "", amount: amount ?? "", code: ussdCode ?? "")
                            self.model.append(modelOffer)
                        }
                    }
                    
                    
                    model = model.sorted(by: { $0.developerAmount.integerValue < $1.developerAmount.integerValue})
                    DispatchQueue.main.async {
                        if self.model.count > 0 {
                            self.specialOfferTableView.isHidden = false
                            self.containerStack.isHidden = true
                            self.specialOfferTableView.reloadData()
                        } else {
                            self.specialOfferTableView.isHidden = true
                            self.containerStack.isHidden = false
                            self.errLabel.text = "No Special Offer".localized
                        }
                    }
                }
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TopupSpecialOfferTableViewCell.self), for: indexPath) as! TopupSpecialOfferTableViewCell
        cell.wrap(model[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.00
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let _ = otherNumberTopup {
            
            let model = self.model[indexPath.row]

            
            if let vc = MultiTopupArray.main.controllers.last  {
                let number = getAgentCode(numberWithPrefix: vc.mobileNumber.text.safelyWrappingString(), havingCountryPrefix: "+95")
                if !PaymentVerificationManager.isValidPaymentTransactions(number, model.developerAmount as String, "TOPUP") {
                    alertViewObj.wrapAlert(title: nil, body: "Same Number with Same Amount can not be paid before 2 minutes".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                    alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                    })
                    alertViewObj.showAlert()
                    return
                }
            }

            
            if MultiTopupArray.main.controllers.count > 1 {
                
                if let last = MultiTopupArray.main.controllers[safe: MultiTopupArray.main.controllers.count - 1], let secondLast = MultiTopupArray.main.controllers[safe: MultiTopupArray.main.controllers.count - 2] {
                    if let lastText = last.mobileNumber.text, let secondLastText = secondLast.mobileNumber.text {
                        if lastText == secondLastText {
                            if let lastAmount = last.amount.text, let secondLastAmount = secondLast.amount.text {
                                if lastAmount == secondLastAmount {
                                    alertViewObj.wrapAlert(title: nil, body: "Same Mobile Number with Same Top-Up amount, Same Data Plan or Same Special Offer Not Allowed".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                                    alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                                    })
                                    alertViewObj.showAlert()
                                    return
                                }
                            }
                        }
                    }
                }
                
                
            }

            if amountValidationCheck {
                let model = self.model[indexPath.row]
                requestMoneyDelegate?.didGetAmount(model.amount, withPlanType: "Special Offers")
                dismiss?.didDismissCallback()
                return 
            }
            
            if (UserLogin.shared.walletBal as NSString).floatValue < (model.developerAmount as NSString).floatValue {
                
                alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                    if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                        addWithdrawView.modalPresentationStyle = .fullScreen
                        self.navigation.present(addWithdrawView, animated: true, completion: nil)
                    }
                })
                alertViewObj.showAlert()
                
                return
            }
            
            
            var totalAmount = (model.developerAmount as NSString).floatValue
            for element in MultiTopupArray.main.controllers {
                totalAmount = totalAmount + ((element.amount.text?.replacingOccurrences(of: ",", with: "") ?? "0") as NSString).floatValue
            }
            
            if totalAmount > (UserLogin.shared.walletBal as NSString).floatValue {
                alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                    if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                        addWithdrawView.modalPresentationStyle = .fullScreen
                        self.navigation.present(addWithdrawView, animated: true, completion: nil)
                    }
                })
                alertViewObj.showAlert()
                return
            }

            
            
            if appDelegate.checkNetworkAvail() == false {
                alertViewObj.wrapAlert(title: nil, body: "No Internet Connection", img: #imageLiteral(resourceName: "dashboard_my_number"))
                alertViewObj.addAction(title: "OK", style: .cancel, action: {
                    
                })
                alertViewObj.showAlert(controller: self)
                return
            }
            
                requestMoneyDelegate?.didGetAmount(model.amount, withPlanType: "Special Offers")
                if MultiTopupArray.main.controllers.count > 1 {
                    dismiss?.didDismissCallback()
                } else {
                    requestMoneyDelegate?.paymentCallOtherNumberThroughDelegate()
                    dismiss?.didDismissCallback()
                }
                return
        }
        
        if let _ = requestMoney {
            let model = self.model[indexPath.row]
            requestMoneyDelegate?.didGetAmount(model.amount, withPlanType: "Special Offers")
            dismiss?.didDismissCallback()
        } else {
            let model = self.model[indexPath.row]
            
            if !PaymentVerificationManager.isValidPaymentTransactions(UserModel.shared.mobileNo, model.developerAmount as String, "TOPUP") {
                alertViewObj.wrapAlert(title: nil, body: "Same Number with Same Amount can not be paid before 2 minutes".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                })
                alertViewObj.showAlert()
                return
            }

            
            //vinnu commented
//            if MyNumberTopup.theme  == MyNumberTopup.OperatorColorCode.mpt || MyNumberTopup.theme == MyNumberTopup.OperatorColorCode.mactel {
//                guard let ussdVC = self.storyboard?.instantiateViewController(withIdentifier: String(describing: TopupMyNumberAlertSMSViewController.self)) as? TopupMyNumberAlertSMSViewController else { return }
//                ussdVC.modalPresentationStyle = .overCurrentContext
//                ussdVC.ussdCode = model.ussdCode
//                ussdVC.delegate = self
//                navigation.present(ussdVC, animated: true, completion: nil)
//                return
//            }
            
            var totalSum : Float = 0.0
            for controllers in MultiTopupArray.main.controllers {
                if let amountStr = controllers.amount.text, let amount = Float(amountStr) {
                    totalSum = totalSum + amount
                }
            }
            
            if totalSum > (UserLogin.shared.walletBal as NSString).floatValue {
                alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                    if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                        addWithdrawView.modalPresentationStyle = .fullScreen
                        self.navigation.present(addWithdrawView, animated: true, completion: nil)
                    }
                })
                alertViewObj.showAlert()
                return
            }
            
            
            if (UserLogin.shared.walletBal as NSString).floatValue < (model.developerAmount as NSString).floatValue {
                
                alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                    if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                        addWithdrawView.modalPresentationStyle = .fullScreen
                        self.navigation.present(addWithdrawView, animated: true, completion: nil)
                    }
                })
                alertViewObj.showAlert()
                
                return
            }
            
            
            if appDelegate.checkNetworkAvail() == false {
                alertViewObj.wrapAlert(title: nil, body: "No Internet Connection", img: #imageLiteral(resourceName: "dashboard_my_number"))
                alertViewObj.addAction(title: "OK", style: .cancel, action: {
                    
                })
                alertViewObj.showAlert(controller: self)
                return
            }
            
            
            
            if MyNumberTopup.operatorCase == .mactel {
                self.selectedObject = model
                self.callingFeeLimitKickBack()
            } else {
                
                let model = self.model[indexPath.row]
                if !PaymentVerificationManager.isValidPaymentTransactions(UserModel.shared.mobileNo, model.amount, "TOPUP") {
                    alertViewObj.wrapAlert(title: nil, body: "Same Number with Same Amount can not be paid before 2 minutes".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                    alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                    })
                    alertViewObj.showAlert()
                    return
                }

                
                var urlString = String.init(format: TopupUrl.merchantNumberCheck, MyNumberTopup.opName,UserModel.shared.mobileNo,rechargeType.specialoffer,UserModel.shared.formattedNumber,model.developerAmount, GeoLocationManager.shared.currentLongitude, GeoLocationManager.shared.currentLatitude, UserLogin.shared.walletBal)
                urlString = urlString + "&SubScriberName=" + (telecoTopupSubscription?.first?.name ?? "").replacingOccurrences(of: " ", with: "%20")
                println_debug(urlString)
                let url = URL.init(string: urlString)
                let par = Dictionary<String,String>()
                TopupWeb.genericClass(url: url!, param: par as AnyObject, httpMethod: "GET") { [weak self] (response, success) in
                    if success {
                        if let json = response as? Dictionary<String,Any> {
                            println_debug(json)
                            if let code = json.safeValueForKey("Code") as? NSNumber, code == 200 {
                                if let msg = json["Msg"] as? String, msg == "Success" {
                                    if let data = json["Data"] as? String {
                                        let array = OKBaseController.convertToArrDictionary(text: data)
                                        if array == nil { return }
                                        var merchantModel = [TopupMerchantNumberModel]()
                                        if let merchantData = array as? [Dictionary<String,Any>] {
                                            for element in merchantData {
                                                let operatr = element["Operator"] as? String
                                                let merNumber = element["MobileNumber"] as? String
                                                let modelMerchant = TopupMerchantNumberModel.init((operatr == nil) ? "" : operatr!, merchant: merNumber == nil ? "" : merNumber!)
                                                merchantModel.append(modelMerchant)
                                            }
                                            if merchantModel.count > 0 {
                                                self?.selectedMerchant = merchantModel.first
                                                self?.selectedObject = model
                                                self?.callingFeeLimitKickBack()
                                            }
                                        }
                                    }
                                }
                            } else if let code = json["Code"] as? NSNumber, code == 300 {
                                self?.showErrorAlert(errMessage: "Your Service Provider is Currently Unavailable.".localized)
                            }
                        }
                    }
                }
            }

        }
        
    }
    
    func callingFeeLimitKickBack() {
        
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "", delegate: self)
        } else {
            self.biometricAuthentication(isSuccessful: true, inScreen: "")
        }
        
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful {
            var type = "PAYTO"
            if MyNumberTopup.operatorCase == .mactel {
                selectedMerchant = TopupMerchantNumberModel.init(MyNumberTopup.opName, merchant: UserModel.shared.mobileNo)
                type = "TOPUP"
            }
            
            guard let selectedMer = selectedMerchant else { return }
            guard let selectedObj = selectedObject else { return }
            
            let urlString = String.init(format: TopupUrl.cashback,UserModel.shared.mobileNo, selectedMer.merchantNumber, selectedObj.developerAmount,ok_password ?? "",type,UserLogin.shared.token)
            println_debug(urlString)
            let url = URL.init(string: urlString)
            let par = Dictionary<String,String>()
            TopupWeb.genericClassXML(url: url!, param: par as AnyObject, httpMethod: "POST") { [weak self](respponse, success) in
                if success {
                    if let xmlResponse = respponse as? String {
                        self?.parsingFeeLimitKickbackResponse(xml: xmlResponse)
                    }
                }
            }
        }
    }
    
    
    func parsingFeeLimitKickbackResponse(xml: String) {
        self.arrayKickBackInfo.removeAll()
        let xml = SWXMLHash.parse(xml)
        self.enumerate(indexer: xml)
        println_debug(kickBackInfoDictionary)
        
        if let transaction = kickBackInfoDictionary.safeValueForKey("resultdescription") as? String, transaction == "Transaction Successful" {
            let number = (kickBackInfoDictionary.safeValueForKey("destination") as? String) == nil ? "" : (kickBackInfoDictionary.safeValueForKey("destination") as? String)
            let operatorName = MyNumberTopup.opName
            let type     = RechargePlanType.specialOffers
            let cashback = (kickBackInfoDictionary.safeValueForKey("kickback") as? String) == nil ? "" : (kickBackInfoDictionary.safeValueForKey("kickback") as? String)
            let amount   = (kickBackInfoDictionary.safeValueForKey("amount") as? String) == nil ? "" : (kickBackInfoDictionary.safeValueForKey("amount") as? String)

            let model = TopupConfirmationModel.init(UserModel.shared.name, number: number!, opeName: operatorName, type: type, cashbk: cashback!, topAmt: amount ?? "", payNumber: UserModel.shared.mobileNo)
            self.arrayKickBackInfo.append(model)
            
            DispatchQueue.main.async {
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TopupConfirmationViewController.self)) as? TopupConfirmationViewController else { return }
                vc.confirmResponseModel = self.arrayKickBackInfo
                vc.type = "Special Offers"
                vc.screenType = ConfirmScreenType.mynumber
                vc.isComingFromRecent = self.isComingFromRecent
                self.navigation.pushViewController(vc, animated: true)
            }
            
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            kickBackInfoDictionary[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    //MARK:- Open SMS Screen For Topup
    func openSmsScreen(with number: String, code: String) {
        let defaultNumber = [number]
        if (MFMessageComposeViewController.canSendText()) {
            let controller        = MFMessageComposeViewController()
            controller.body       = code
            controller.recipients = defaultNumber
            controller.messageComposeDelegate = self
            self.navigation.present(controller, animated: true, completion: nil)
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }

    
    deinit {
        self.model.removeAll()
        self.selectedObject = nil
        self.selectedMerchant = nil
        NotificationCenter.default.removeObserver(self)
    }

    
}

