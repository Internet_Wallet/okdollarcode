//
//  OtherNumberReceiptViewController.swift
//  OK
//
//  Created by Ashish on 5/29/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

protocol OtherNumberReceiptCollectionCellDelegate : class {
    func ratingActionCalled()
}

class OtherNumberReceiptViewController: OKBaseController {
    
     @IBOutlet weak var viewTab: UIView!
    var viewOptions: TabOptionsOnReceipt?
    var isInFavArray = [Bool]()
    var isInContactArray = [Bool]()
    let optionTextDefaultColor = UIColor.init(hex: "162D9F")
    var currentIndex = 0
    //MARK: - Outlets
    @IBOutlet weak var receiptCollection: UICollectionView!
    @IBOutlet weak var home: UITabBarItem!
    @IBOutlet weak var more: UITabBarItem!
    @IBOutlet weak var contactTb: UITabBarItem!
    @IBOutlet weak var favtab: UITabBarItem!
    @IBOutlet weak var constraintCollectionH: NSLayoutConstraint!
    var cashBackDestNum: String?
    var nameToDisplay = [String]()
    var isComingFromRecent = false
    
    @IBOutlet var contentViewRecipt: UIView!
    @IBOutlet weak var tabBar: UITabBar!
    var fromPendingApproval = ""
    
    //MARK: - Properties
    var modelResponse = [OtherNumberResponse]()
    
    //MARK: - Views method
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            self.changeCancelImplementation()
        }
        if modelResponse.count > 1 {
            let size  = "\(modelResponse.count)".size(withAttributes: [.font: UIFont.init(name: appFont, size: 13.0) ?? UIFont.systemFont(ofSize: 13.0)])
            let label = UILabel(frame: .init(origin: CGPoint.zero, size: size))
            label.textColor = .white
            label.text = "\(modelResponse.count)"
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: label)
        }
        
        for records in modelResponse {
            let number   = self.getAgentCode(numberWithPrefix: records.paymentNumber, havingCountryPrefix: "+95")
            let type     = "TOPUP"
            let amount   = records.amount.safelyWrappingString()
            let name     = records.name
            let model = PaymentTransactions.init(number, type: type, name: name, amount: amount, transID: "", bonus: "0.0", backendNumber: cashBackDestNum)
            PaymentVerificationManager.storeTransactions(model: model)
            TopupRecentContacts.storeTransactions(model: model)
            let isInFav = checkForFavorites(number, type: "TOPUPOTHER").isInFavorite
            let isInCont = checkForContact(number).isInContact
            isInFavArray.append(isInFav)
            isInContactArray.append(isInCont)
        }
        GetProfile.updateBalance()
        //        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11)!], for: .selected)
        //        UITabBar.appearance().unselectedItemTintColor = .darkGray
        
        
        guard let bView = Bundle.main.loadNibNamed("TabOptionsOnReceipt", owner: self, options: nil)?.first as? TabOptionsOnReceipt else { return }
        viewOptions = bView
        bView.delegate = self
        viewOptions?.frame = CGRect(x: 0, y: 0, width: viewTab.frame.size.width, height: viewTab.frame.size.height)
        if let _ = viewOptions {
            viewTab.addSubview(viewOptions!)
            viewOptions!.lblHome.text = "HomePagePT".localized
            viewOptions!.lblMore.text = "More".localized
            viewOptions!.lblAddContact.text = "TabbarAddContact".localized
            viewOptions!.lblAddFav.text = "Add Favorite".localized
            if isInContactArray.count > 0 && isInContactArray[0] == true {
                addContactUI(isInContact: true)
            }
            if isInFavArray.count > 0 && isInFavArray[0] == true {
                addFavUI(isInFav: true)
            }
            viewOptions!.layoutIfNeeded()
        }
        viewOptions!.lblAddFav.textColor = UIColor.init(red: 22.0/255.0, green: 45.0/255.0, blue: 159.0/255.0, alpha: 1.0)
        viewOptions!.lblAddContact.textColor = UIColor.init(red: 22.0/255.0, green: 45.0/255.0, blue: 159.0/255.0, alpha: 1.0)
        viewOptions!.lblHome.textColor = UIColor.init(red: 22.0/255.0, green: 45.0/255.0, blue: 159.0/255.0, alpha: 1.0)
        viewOptions!.lblMore.textColor = UIColor.init(red: 22.0/255.0, green: 45.0/255.0, blue: 159.0/255.0, alpha: 1.0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let color = UIColor.lightGray
        let attrFont = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.0) ?? UIFont.systemFont(ofSize: 11.0), NSAttributedString.Key.foregroundColor: color]
        
        self.home.setTitleTextAttributes(attrFont, for: .normal)
        self.more.setTitleTextAttributes(attrFont, for: .normal)
        self.contactTb.setTitleTextAttributes(attrFont, for: .normal)
        self.favtab.setTitleTextAttributes(attrFont, for: .normal)
        self.home.title = "Home Page".localized
        self.more.title = "More".localized
        self.contactTb.title = "TabbarAddContact".localized
        self.favtab.title = "Add Favorite".localized
        self.navigationController?.navigationBar.backgroundColor = MyNumberTopup.OperatorColorCode.okDefault
        self.navigationController?.navigationBar.tintColor = MyNumberTopup.OperatorColorCode.okDefault
        self.navigationItem.setHidesBackButton(true, animated: true)
        
        self.navigationController?.navigationBar.titleTextAttributes = PaytoConstants.global.attributesForTitle()
        self.title = "Receipt".localized
        let image = self.view.snapshotOfCustomeView
        UIImageWriteToSavedPhotosAlbum(image, self, nil, nil)
    }
    
    override func viewDidAppear(_ animated: Bool)  {
        super.viewDidAppear(true)
//        if device.type == .iPhoneX || device.type == .iPhoneXR || device.type == .iPhoneXS || device.type == .iPhoneXSMax || device.type == .iPhone11 {
//            bottomViewHeightConstraint.constant = 250
//        }else {
//            bottomViewHeightConstraint.constant = 90
//        }
        
        let image = captureScreen(view: contentViewRecipt)
         UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)

    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            alertViewObj.wrapAlert(title: "Error".localized, body: error.localizedDescription, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        } else {
            println_debug("Your altered image has been saved to your photos.")
        }
    }
    
    //MARK: - Methods
    func addFavUI(isInFav: Bool) {
        if let _ = viewOptions {
            viewOptions?.imgAddFav.image = viewOptions?.imgAddFav.image?.withRenderingMode(.alwaysTemplate)
            if isInFav {
                viewOptions?.imgAddFav.tintColor = kYellowColor
                viewOptions!.lblAddFav.textColor = kYellowColor
            } else {
                viewOptions?.imgAddFav.tintColor = UIColor.gray
                viewOptions!.lblAddFav.textColor = UIColor.init(red: 22.0/255.0, green: 45.0/255.0, blue: 159.0/255.0, alpha: 1.0)
            }
            viewOptions!.layoutIfNeeded()
        }
    }
    
    func addContactUI(isInContact: Bool) {
        if let _ = viewOptions {
            viewOptions?.imgAddContact.image = viewOptions?.imgAddContact.image?.withRenderingMode(.alwaysTemplate)
            if isInContact {
                viewOptions?.imgAddContact.tintColor = kYellowColor
                viewOptions!.lblAddContact.textColor = kYellowColor
            } else {
                viewOptions?.imgAddContact.tintColor = UIColor.gray
                viewOptions!.lblAddContact.textColor = UIColor.init(red: 22.0/255.0, green: 45.0/255.0, blue: 159.0/255.0, alpha: 1.0)
            }
            viewOptions!.layoutIfNeeded()
        }
    }
    
    //MARK: - Methods
    func getCellFromDisplay() -> UICollectionViewCell? {
        let visibleCell = CGRect.init(origin: self.receiptCollection.contentOffset, size: self.receiptCollection.bounds.size)
        let visiblePoint = CGPoint.init(x: visibleCell.midX, y: visibleCell.midY)
        let indexPath = self.receiptCollection.indexPathForItem(at: visiblePoint)
        if let path = indexPath {
            let cell = self.receiptCollection.cellForItem(at: path)
            return cell
        }
        
        return nil
    }
    
    func addFav(model: OtherNumberResponse) {
        let name = (model.name.count > 0) ? model.name : ""
//        let favVC = self.addFavoriteController(withName: name, favNum: model.paymentNumber, type: "TOPUPOTHER", amount: model.amount.safelyWrappingString())
//        if let vc = favVC {
//            vc.delegate = self
//             vc.modalPresentationStyle = .overCurrentContext
//            self.navigationController?.present(vc, animated: false, completion: nil)
//        }
        let vc = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier:"PTFavoriteViewController") as! PTFavoriteViewController
        vc.view.frame = CGRect.zero
        vc.delegate = self
        vc.addFavDetails = (name,model.paymentNumber,"TOPUPOTHER", model.amount.safelyWrappingString())
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: false, completion: nil)
    }
    
    func addContacts(model: OtherNumberResponse) {
        let store     = CNContactStore()
        let contact   = CNMutableContact()
        let number = (model.paymentNumber.count > 0) ? model.paymentNumber : ""
        let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue : number))
        contact.givenName = (model.name.count > 0) ? model.name : ""
        contact.phoneNumbers = [homePhone] //
        //contact.givenName = model.agentname.safelyWrappingString()
        let controller = CNContactViewController.init(forNewContact: contact)
        controller.contactStore = store
        controller.delegate     = self
        controller.title        = "Add Contact".localized
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.backgroundColor = UIColor.init(hex: "F3C632")
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
     func goToHome() {
        
        if isComingFromRecent{
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MoveToDashboardFromTopUp"), object: nil)
        }else{
            if fromPendingApproval == "pendingRequestMoney" {
                self.navigationController?.popToRootViewController(animated: true)
            }
            // self.navigationController?.popToRootViewController(animated: true)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoToDashboardMultiTopup"), object: nil, userInfo: nil)
        }
        
    }
    
    func moreAct() {
        guard  let vc = UIStoryboard.init(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PTMoreViewController") as? PTMoreViewController else {return}
        addChild(vc)
        vc.view.frame = self.view.bounds
        vc.didMove(toParent: self)
        vc.delegate = self
        
        if let cell = getCellFromDisplay() as? OtherNumberReceiptCollectionCell {
            if cell.model?.type == "Top-Up Plan" {
                vc.headerTopup = ("More Top-Up".localized,"Repeat Top-Up".localized)
            } else if cell.model?.type == "Data Plan" {
                vc.headerTopup = ("More Data Plan".localized,"Repeat Data Plan".localized)
            } else if cell.model?.type == "Special Offers" {
                vc.headerTopup = ("More Special Offers".localized,"Repeat Special Offers".localized)
            } else if cell.model?.type == "Special Offer"  {
                vc.headerTopup = ("More Special Offers".localized,"Repeat Special Offers".localized)
            } else {
                vc.headerTopup = ("More Top-Up".localized,"Repeat Top-Up".localized)
            }
        }
        
        vc.titleMorePayment   = "More Topup"
        vc.titleRepeatPayment = "Repeat Topup"
        
        vc.morePayment.setTitle(vc.headerTopup!.0, for: .normal)
        vc.repeatPayment.setTitle(vc.headerTopup!.1, for: .normal)
        
        let transition = CATransition()
        transition.duration = 0.2
        transition.type = CATransitionType.moveIn
        
        vc.view.layer.add(transition, forKey: nil)
        view.addSubview(vc.view)
    }
    
    private func morePayment() {
        self.navigationController?.popToRootViewController(animated: false)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MultiTopupReloadMorePayment"), object: nil, userInfo: nil)
    }
    
    private func moreRepeatPayment() {
        self.navigationController?.popToRootViewController(animated: false)
        if let cell = self.getCellFromDisplay() {
            guard let model = (cell as! OtherNumberReceiptCollectionCell).model else { return }
            let userInfo : [String:String] = ["key": model.paymentNumber]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MultiTopupReloadMoreRepeatPayment"), object: nil, userInfo: userInfo)
        }
    }
    
    private func invoice() {
        if let cell = getCellFromDisplay() {
            guard let model = (cell as! OtherNumberReceiptCollectionCell).model else { return }
            
            var pdfDictionary = Dictionary<String,Any>()
            pdfDictionary["senderAccName"]      = UserModel.shared.name
            
            var number = UserModel.shared.mobileNo
            _ = number.remove(at: String.Index.init(encodedOffset: 0))
            _ = number.remove(at: String.Index.init(encodedOffset: 0))
            number  = "+" + number
            number  = number.replacingOccurrences(of: "+95", with: "(+95) 0")
            
            var destination = model.destination.safelyWrappingString()
            _ = destination.remove(at: String.Index.init(encodedOffset: 0))
            _ = destination.remove(at: String.Index.init(encodedOffset: 0))
            destination  = "+" + destination
            destination  = destination.replacingOccurrences(of: "+95", with: "(+95)")
            
            pdfDictionary["invoiceTitle"]        = "Transaction Receipt".localized
            pdfDictionary["senderAccNo"]       = number
            pdfDictionary["mobileNumberTopup"] =  "(+95)" + " " + model.paymentNumber
            pdfDictionary["transactionID"]      = model.transid
            pdfDictionary["transactionType"]    = model.type
            let dtStr = model.responsects.safelyWrappingString()
            if let dtVal = dtStr.dateValue(dateFormatIs: "dd-MMM-yyyy HH:mm:ss") {
                let datStr = dtVal.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss")
                pdfDictionary["transactionDate"] = datStr
            } else {
                pdfDictionary["transactionDate"] = dtStr
                
            }
            pdfDictionary["amount"] = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: model.amount.safelyWrappingString()) + " MMK"
            //pdfDictionary["amount"]     = self.formattingWalletBalance(model.amount.safelyWrappingString())
            
            if !(model.kickvalue.safelyWrappingString().lowercased() == "Kickback rules not defined".lowercased() || model.kickvalue.safelyWrappingString().lowercased() == "" || model.kickvalue.safelyWrappingString().lowercased() == "Kick-Back Rules Not Define".lowercased()) {
                
                pdfDictionary["cashback"] =  model.kickvalue.safelyWrappingString().replacingOccurrences(of: ".00", with: "") + " " + "MMK"
            }
            pdfDictionary["logoName"]   = "appIcon_Ok"
            
            guard let pdfUrl = CodeSnippets.createInvoicePDF(dictValues: pdfDictionary, pdfFile: "OK$ otherNumber Receipt") else {
                println_debug("Error - pdf not generated")
                return
            }
            
            guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "SharePDFViewController") as? SharePDFViewController else { return }
            vc.url = pdfUrl
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func formattingWalletBalance(_ amount: String) -> NSMutableAttributedString {
        
        let number = NSDecimalNumber(string: amount)
        var puntuatedAmount = ""
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        if let num = formatter.string(from: number) {
            puntuatedAmount = (num == "NaN") ? "" : num
        }
        puntuatedAmount = puntuatedAmount.replacingOccurrences(of: ".00", with: "")
        let mmk = [NSAttributedString.Key.font: UIFont(name: appFont, size: 12.0) ?? UIFont.systemFont(ofSize: 12)]
        let amtMMK = [NSAttributedString.Key.font: UIFont(name: appFont, size: 15.0) ?? UIFont.systemFont(ofSize: 15)]
        let mmkString = NSMutableAttributedString(string: " MMK", attributes: mmk)
        let amountStr    = NSMutableAttributedString.init(string: puntuatedAmount, attributes: amtMMK)
        
        let stringAttributed = NSMutableAttributedString()
        stringAttributed.append(amountStr)
        stringAttributed.append(mmkString)
        return  stringAttributed
    }
    
    private func share() {
        if let cell = getCellFromDisplay() {
            guard let design = cell as? OtherNumberReceiptCollectionCell else { return }
            guard  let view = (design).viewDesign else { return }
            design.rateLabel.isHidden = true
            design.stackViews.subviews.forEach { (view) in
                view.isHidden = true
            }
            design.cashbackKey.isHidden = true
            design.cashbackAmount.isHidden = true
            design.balanceKey.isHidden = true
            design.walletBalance.isHidden = true
            design.categoriesKey.isHidden = true
            design.categoryValue.isHidden = true
            design.constraintTopTransID.constant = -62
            design.layoutIfNeeded()
            let snapImage = view.snapshotOfCustomeView
            let vc = UIActivityViewController(activityItems: [snapImage], applicationActivities: [])
            DispatchQueue.main.async {self.present(vc, animated: true)}
            design.rateLabel.isHidden = false
            design.stackViews.subviews.forEach { (view) in
                view.isHidden = false
            }
            
            design.cashbackKey.isHidden = true
            design.cashbackAmount.isHidden = true
//            if design.model!.kickvalue.safelyWrappingString().lowercased() != "Kickback rules not defined".lowercased() {
//                design.cashbackKey.isHidden = true
//                design.cashbackAmount.isHidden = true
//            } else {
//                design.cashbackKey.isHidden = false
//                design.cashbackAmount.isHidden = false
//            }
            
            design.balanceKey.isHidden = false
            design.walletBalance.isHidden = false
            design.categoriesKey.isHidden = false
            design.categoryValue.isHidden = false
            design.constraintTopTransID.constant = 12
            design.layoutIfNeeded()
        }
    }
    
    private func screenShotMethod(_ view: UIView) -> UIImage? {
        //Create the UIImage
        UIGraphicsBeginImageContext(view.frame.size)
        
        if let context = UIGraphicsGetCurrentContext() {
            view.layer.render(in: context)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            if let imageFinal = image {
                UIImageWriteToSavedPhotosAlbum(imageFinal, nil, nil, nil)
            }
            return image
        }
        return nil
    }
    
    
}

//MARK: - UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension OtherNumberReceiptViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OtherNumberReceiptCollectionCell", for: indexPath) as? OtherNumberReceiptCollectionCell else { return OtherNumberReceiptCollectionCell() }
        if modelResponse.count > 1 {
            cell.hideRatingView()
            cell.wrapPageNumber("\(indexPath.section + 1)")
        } else {
            cell.wrapPageNumber("")
        }
       
        currentIndex = indexPath.row
        cell.name.text = nameToDisplay[indexPath.section]
        cell.nameTop.text = nameToDisplay[indexPath.section]
        cell.wrapView(model: modelResponse[indexPath.section])
        self.loadViewIfNeeded()
        self.view.layoutIfNeeded()
        self.constraintCollectionH.constant = cell.frame.maxY //597.0 //8 + cell.viewRating.frame.maxY + 40
        self.view.layoutIfNeeded()
        if currentIndex < isInFavArray.count && isInFavArray.count > 0 {
            addFavUI(isInFav: isInFavArray[currentIndex])
        }
        if currentIndex < isInContactArray.count && isInContactArray.count > 0 {
            addContactUI(isInContact: isInContactArray[currentIndex])
        }
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
       
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return modelResponse.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
}

//MARK: - OtherNumberReceiptCollectionCellDelegate
extension OtherNumberReceiptViewController: OtherNumberReceiptCollectionCellDelegate {
    func ratingActionCalled() {
        if let cell = getCellFromDisplay() {
            guard let model = (cell as! OtherNumberReceiptCollectionCell).model else { return }
            guard let ratingScreen = self.storyboard?.instantiateViewController(withIdentifier: String(describing: PaymentRateScreenViewController.self)) as? PaymentRateScreenViewController else { return }
            ratingScreen.modalPresentationStyle = .overCurrentContext
            ratingScreen.delegate = (cell as! OtherNumberReceiptCollectionCell)
            ratingScreen.destinationNumber = model.destination.safelyWrappingString()
            self.navigationController?.present(ratingScreen, animated: true, completion: nil)
        }
    }
}

//MARK: - MoreControllerActionDelegate
extension OtherNumberReceiptViewController: MoreControllerActionDelegate {
    func actionFor(option: MorePaymentAction) {
        switch option {
        case .morePayment:
            if (UserLogin.shared.walletBal as NSString).floatValue < 100.00 {
                alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                    if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                        addWithdrawView.modalPresentationStyle = .fullScreen
                        self.navigationController?.present(addWithdrawView, animated: true, completion: nil)
                    }
                })
                alertViewObj.showAlert()
                
                return
            }
            self.morePayment()
        case .repeatPayment:
            
            if (UserLogin.shared.walletBal as NSString).floatValue < 100.00 {
                alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                    if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                        addWithdrawView.modalPresentationStyle = .fullScreen
                        self.navigationController?.present(addWithdrawView, animated: true, completion: nil)
                    }
                })
                alertViewObj.showAlert()
                
                return
            }
            
            self.moreRepeatPayment()
        case .invoice:
            self.invoice()
        case .share:
            self.share()
        }
    }
}

//MARK: - UITabBarDelegate
extension OtherNumberReceiptViewController: UITabBarDelegate {
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if let cell = getCellFromDisplay() {
            if let otherModel =  (cell as! OtherNumberReceiptCollectionCell).model {
                switch item.tag {
                case 31:
                    self.addFav(model: otherModel)
                    break
                case 32:
                    self.addContacts(model: otherModel)
                    break
                case 33:
                    self.goToHome()
                    break
                case 34:
                    self.moreAct()
                    break
                default:
                    break
                }
            }
        }
    }
}

//MARK: - CNContactViewControllerDelegate
extension OtherNumberReceiptViewController: CNContactViewControllerDelegate {
    
    func changeCancelImplementation() {
        let originalSelector = Selector(("editCancel:"))
        let swizzledSelector = #selector(CNContactViewController.cancelHack)
        if let originalMethod = class_getInstanceMethod(object_getClass(CNContactViewController()), originalSelector),
           let swizzledMethod = class_getInstanceMethod(object_getClass(CNContactViewController()), swizzledSelector) {
            method_exchangeImplementations(originalMethod, swizzledMethod)
        }
    }
    
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        if let newContact = contact {
//            isInContactArray[currentIndex] = true
//            addContactUI(isInContact: true)
//            okContacts.append(newContact)
                
                if isInContactArray.count == 1{
                    if !isInContactArray[currentIndex]{

                    isInContactArray[currentIndex] = true
                    }
                }else{
                    for i in 0..<isInContactArray.count{
                        if !isInContactArray[i] {
                            isInContactArray[i] = true
                            break
                        }
                    }
                }
                addContactUI(isInContact: true)
                okContacts.append(newContact)
            
        }
        viewController.delegate = nil
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - OtherNumberReceiptCollectionCell
class OtherNumberReceiptCollectionCell : UICollectionViewCell, PaymentRateDelegate {
    //MARK: - Outlets
    @IBOutlet weak var viewDesign: CardDesignView!
    @IBOutlet weak var mobileNumber: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var nameTop: UILabel!
    @IBOutlet weak var walletBalance: UILabel!
    @IBOutlet weak var transactionId: UILabel!
    @IBOutlet weak var planType: UILabel!
    @IBOutlet weak var cashbackAmount: UILabel!
    @IBOutlet weak var days: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var cashbackKey: UILabel!
    @IBOutlet weak var heightLayout: NSLayoutConstraint!
    @IBOutlet weak var stackViews: UIStackView!
    @IBOutlet weak var nameKey: UILabel!
    @IBOutlet weak var categoriesKey: UILabel!
    @IBOutlet weak var categoryValue: UILabel!
    @IBOutlet weak var balanceKey: UILabel!
    @IBOutlet weak var transactionIdKey: UILabel!
    {
        didSet
        {
            transactionIdKey.text = "Transaction ID".localized
        }
    }
    @IBOutlet weak var transactiontypeKey: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    {
        didSet
        {
            rateLabel.text = "Rate OK$ Services".localized
        }
    }
    @IBOutlet weak var pageNumber : UILabel!
    @IBOutlet weak var viewRating: UIView!
    @IBOutlet weak var constraintTopTransID: NSLayoutConstraint!
    
    //MARK: - Properties
    weak var delegate : OtherNumberReceiptCollectionCellDelegate?
    var printingObject : Bool = false
    var model: OtherNumberResponse?
    
    //MARK: - Methods
    func wrapPageNumber(_ index: String) {
        self.pageNumber.text = index
    }
    
    func getNameFromContact(number: String, _ completion: @escaping(_ name: String) -> Void) {
        if number.count > 7 {
            var name = ""
            let numb = String(number.dropFirst(4))
            if okContacts.count > 0 {
                outerLoop: for contact in okContacts {
                    for phoneNumber in contact.phoneNumbers {
                        let phoneNumberStruct = phoneNumber.value
                        let phoneNumberString = phoneNumberStruct.stringValue
                        let strMobNo = phoneNumberString
                        let trimmed = String(strMobNo.filter { !" ".contains($0) })
                        if trimmed.contains(find: numb) {
                            name = contact.givenName
                            break outerLoop
                        }
                    }
                }
            }
            completion(name)
        } else {
            completion("")
        }
    }
    
    func wrapView(model: OtherNumberResponse) {
        self.model = model
        func wrapAmount(key: String) -> String {
            let number = NSDecimalNumber(string: key)
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            formatter.minimumFractionDigits = 2
            formatter.maximumFractionDigits = 2
            
            formatter.groupingSeparator = ","
            formatter.groupingSize = 3
            formatter.usesGroupingSeparator = true
            if let num = formatter.string(from: number) {
                return (num == "NaN") ? "" : (num.contains(find: ".") ? num.replacingOccurrences(of: ".00", with: "") : num)
            }
            return ""
        }
        
       // self.name.text = model.name
       // self.nameTop.text = model.name
        
//        DispatchQueue.global(qos: .background).async {
//            self.getNameFromContact(number: model.paymentNumber) { (nameStr) in //need to check for more than 1000 contacts
//                DispatchQueue.main.async {
//                    if nameStr != "" {
//                        self.name.text = nameStr
//                        self.nameTop.text = nameStr
//                    }
//                }
//            }
//        }
        
        self.nameKey.text = self.nameKey.text?.localized
        self.categoriesKey.text = self.categoriesKey.text?.localized
        self.balanceKey.text = self.balanceKey.text?.localized
        self.transactionId.text = self.transactionId.text?.localized
        self.transactiontypeKey.text = self.transactiontypeKey.text?.localized
        self.mobileNumber.text = model.paymentNumber
        self.amount.text = wrapAmount(key: model.amount.safelyWrappingString())
        
        self.walletBalance.text = wrapAmount(key: model.walletbalance.safelyWrappingString())  + " " + "MMK"
        
        self.transactionId.text = model.transid.safelyWrappingString()
        
        self.planType.text = model.type
        
        if model.kickvalue.safelyWrappingString().lowercased() == "Kickback rules not defined" || model.kickvalue.safelyWrappingString().lowercased() == "" || model.kickvalue.safelyWrappingString() == "Kick-Back Rules Not Define" || model.kickvalue.safelyWrappingString().lowercased() == "Kick-Back Rules Not Define".lowercased() {
            
            self.cashbackAmount.text = ""
            self.cashbackKey.text = ""
            
            
            self.heightLayout.constant = 20.00
        } else {
            self.cashbackAmount.text = model.developerCashback.replacingOccurrences(of: ".00", with: "") + " " + "MMK"
            self.cashbackKey.text = "Cashback".localized
            self.heightLayout.constant = 45.00
        }
        
        if let date = model.responsects {
            if let dateFormat = date.components(separatedBy: " ").first?.dateValue(dateFormatIs: "dd-MMM-yyyy") {
                let dateKey = (dateFormat.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy"))
                self.days.text = dateKey
                
                let timeKey = date.components(separatedBy: " ").last ?? ""
                self.time.text = timeKey
            } else {
                let dateFormtter = DateFormatter()
                dateFormtter.dateFormat = "EEE, dd-MMM-yyyy"
                self.days.text = dateFormtter.string(from: Date())
                dateFormtter.dateFormat = "HH:mm:ss"
                self.time.text = dateFormtter.string(from: Date())
            }
        }
        //old time format
//        let preciseTime  = model.responsects.safelyWrappingString().components(separatedBy: " ")
//
//        self.days.text = preciseTime.first.safelyWrappingString()
//
//
//        self.time.text = preciseTime.last.safelyWrappingString()
        
        for view in stackViews.subviews {
            if let img = view as? UIImageView {
                img.isHighlighted = true
            }
        }
    }
    
    func hideRatingView() {
        for view in stackViews.subviews {
            view.isHidden = true
        }
        rateLabel.isHidden = true
    }
    
    func ratingShow(_ rate: Int, commentText: String?) {
        for view in stackViews.subviews {
            if view.tag <= rate {
                if let img = view as? UIImageView {
                    img.isHighlighted = false
                }
            } else {
                if let img = view as? UIImageView {
                    img.isHighlighted = true
                }
            }
        }
    }
    
    @IBAction func ratingAction(_ sender: UIButton) {
        var count = 0
        for view in stackViews.subviews {
            if let img = view as? UIImageView {
                if img.isHighlighted == false {
                    count = count + 1
                }
            }
        }
        if count == 0 {
            delegate?.ratingActionCalled()
        }
    }
}


//MARK:- TabOptionProtocol
extension OtherNumberReceiptViewController: TabOptionProtocol {
    func addFavorite() {
        if let _ = viewOptions {
            if viewOptions!.lblAddFav.textColor != kYellowColor {
                if let cell = getCellFromDisplay() {
                    if let otherModel =  (cell as! OtherNumberReceiptCollectionCell).model {
                        self.addFav(model: otherModel)
                    }
                }
            } else {
                PaytoConstants.alert.showToast(msg: "Contact already added in Favorites".localized)
            }
        }
    }
    
    func addContact() {
        if let _ = viewOptions {
            if viewOptions!.lblAddContact.textColor != kYellowColor {
                if let cell = getCellFromDisplay() {
                    if let otherModel =  (cell as! OtherNumberReceiptCollectionCell).model {
                        self.addContacts(model: otherModel)
                    }
                }
            } else {
                PaytoConstants.alert.showToast(msg: "Contact Already Added".localized)
            }
        }
    }
    
    
    func goHome() {
        self.goToHome()
    }
    
    func showMore() {
        self.moreAct()
    }
    
    
}


//MARK:- PTFavoriteActionDelegate
extension OtherNumberReceiptViewController: PTFavoriteActionDelegate {
    func favoriteAdded() {
        isInFavArray[currentIndex] = true
        addFavUI(isInFav: true)
    }
}
