//
//  PaymentFinalRecieptModel.swift
//  OK
//
//  Created by Ashish on 5/9/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct PaymentFinalRecieptModel : Codable {
    
        let agentcode : String?
        let agentname : String?
        let amount : String?
        let clientip : String?
        let clientos : String?
        let clienttype : String?
        let comments : String?
        let destination : String?
        let destlevel : String?
        let fee : String?
        let greeting : String?
        let iskickbackmsisdnregister : String?
        let isregisteragent : String?
        let kickbackenable : String?
        let kickvalue : String?
        let kickwallet : String?
        let loyaltypoints : String?
        let maxwalletvalue : String?
        let merchantname : String?
        let merchantnamesms : String?
        let minwalletvalue : String?
        let operators : String?
        let oprwallet : String?
        let prekickwallet : String?
        let preoprwallet : String?
        let prewalletbalance : String?
        let referredlang : String?
        let requestcts : String?
        let responsects : String?
        let responsevalue : String?
        let resultcode : String?
        let resultdescription : String?
        let rtranstype : String?
        let securetoken : String?
        let source : String?
        let transid : String?
        let unregipostbalance : String?
        let unregiprebalance : String?
        let vendorcode : String?
        let walletalertvalue : String?
        let walletbalance : String?
        
        
        
        enum CodingKeys: String, CodingKey {
            case agentcode = "agentcode"
            case agentname = "agentname"
            case amount = "amount"
            case clientip = "clientip"
            case clientos = "clientos"
            case clienttype = "clienttype"
            case comments = "comments"
            case destination = "destination"
            case destlevel = "destlevel"
            case fee = "fee"
            case greeting = "greeting"
            case iskickbackmsisdnregister = "iskickbackmsisdnregister"
            case isregisteragent = "isregisteragent"
            case kickbackenable = "kickbackenable"
            case kickvalue = "kickvalue"
            case kickwallet = "kickwallet"
            case loyaltypoints = "loyaltypoints"
            case maxwalletvalue = "maxwalletvalue"
            case merchantname = "merchantname"
            case merchantnamesms = "merchantnamesms"
            case minwalletvalue = "minwalletvalue"
            case operators = "operator"
            case oprwallet = "oprwallet"
            case prekickwallet = "prekickwallet"
            case preoprwallet = "preoprwallet"
            case prewalletbalance = "prewalletbalance"
            case referredlang = "referredlang"
            case requestcts = "requestcts"
            case responsects = "responsects"
            case responsevalue = "responsevalue"
            case resultcode = "resultcode"
            case resultdescription = "resultdescription"
            case rtranstype = "rtranstype"
            case securetoken = "securetoken"
            case source = "source"
            case transid = "transid"
            case unregipostbalance = "unregipostbalance"
            case unregiprebalance = "unregiprebalance"
            case vendorcode = "vendorcode"
            case walletalertvalue = "walletalertvalue"
            case walletbalance = "walletbalance"
        }

}

enum RecipetPlanCases: String {
    case myNumber = "My Number"
    case otherNumber = "Other Number"
    case payto = "PAYTO"
}

enum RecieptTabSelectionType : Int {
    case addFavorite = 31
    case addContact = 32
    case home = 33
    case more = 34
}






