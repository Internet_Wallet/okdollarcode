//
//  TopupMyRechargeDetailSelection.swift
//  OK
//
//  Created by Ashish on 10/12/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol TopupMyRechargeDetailSelectionDelegate : class {
    func didSelectEloadMsg(model: RechargePlanModel)
}

class TopupMyRechargeDetailSelection: OKBaseController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var displayTable : UITableView!
    
    var displayString : (first: String, second: String) = ("","")
    
    weak var delegate : TopupMyRechargeDetailSelectionDelegate?
    
    var selectedModel : RechargePlanModel?
    var isComingFromRecent = false

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func dismissButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- TableView Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: TopupMyRechargeDetailSelectionCell.self), for: indexPath) as? TopupMyRechargeDetailSelectionCell
        if indexPath.row == 0 {
            cell?.wrapData(info: displayString.first)
        } else if indexPath.row == 1 {
            cell?.wrapData(info: displayString.second)
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true, completion: nil)
        if indexPath.row == 0 {
            if let cell = tableView.cellForRow(at: indexPath) as? TopupMyRechargeDetailSelectionCell {
                cell.check.isHighlighted = true
            }
            selectedModel!.selectedIndex = "1"
            self.delegate?.didSelectEloadMsg(model: selectedModel!)
        } else {
            if let cell = tableView.cellForRow(at: indexPath) as? TopupMyRechargeDetailSelectionCell {
                cell.check.isHighlighted = true
            }
            selectedModel!.selectedIndex = "2"
            self.delegate?.didSelectEloadMsg(model: selectedModel!)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.00
    }

}

class TopupMyRechargeDetailSelectionCell : UITableViewCell {
    
    @IBOutlet weak var check : UIImageView!
    @IBOutlet weak var info : UILabel!{
        didSet {
            self.info.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    func wrapData(info: String?) {
        self.info.text = info
    }
    
}
