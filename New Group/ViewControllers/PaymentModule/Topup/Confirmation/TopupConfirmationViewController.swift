//
//  TopupConfirmationViewController.swift
//  OK
//
//  Created by Ashish on 1/29/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct RechargePlanType {
    static let recharge = "Top-Up Plan"
    static let dataPlan = "Data Plan"
    static let specialOffers = "Special Offer"
}

enum ConfirmScreenType {
    case mynumber, otherNumber
}

enum TopupConfirmationCellType {
    case name, number, type, cashback, amount, headerTitle, bonusPoint
}


class TopupConfirmationViewController: OKBaseController, BioMetricLoginDelegate {

    @IBOutlet var confirmationTopup: UITableView!
    @IBOutlet var payBtn: UIButton!
    
    var confirmResponseModel = [TopupConfirmationModel]()
    
    let manager = GeoLocationManager.shared
    
    var screenType : ConfirmScreenType = ConfirmScreenType.mynumber
    
    var transaction = Dictionary<String,Any>()
    
    var modelArray = [Dictionary<String,Any>]()
    
    var bonusPoint : BonusPointTopupModel?
    var backendNumber: String?
    var failureViewModel = FailureViewModel()
    var isComingFromRecent = false

    @IBOutlet weak var advertisementContainer: UIView!
    @IBOutlet weak var adImage: UIImageView!
    
    var enumArray : [TopupConfirmationCellType] = [TopupConfirmationCellType.headerTitle,TopupConfirmationCellType.name, TopupConfirmationCellType.number, TopupConfirmationCellType.type, TopupConfirmationCellType.cashback, TopupConfirmationCellType.amount, TopupConfirmationCellType.bonusPoint]
    
    var timer : Timer?
    
    var seconds : Int = 0
    
    var type : String = "Recharge Plan"
    var tapCount = 0
    var failureCount = 0

    override func viewDidLoad() {
        println_debug("TopupConfirmationViewController")
        super.viewDidLoad()
        geoLocManager.startUpdateLocation()
        self.backButton(withTitle: "Confirmation".localized)
        self.confirmationTopup.tableFooterView = UIView.init()
        self.initiateTimer()
        
        if self.confirmResponseModel.count > 1 {
            enumArray  = [ TopupConfirmationCellType.headerTitle,TopupConfirmationCellType.name, TopupConfirmationCellType.number, TopupConfirmationCellType.type, TopupConfirmationCellType.cashback, TopupConfirmationCellType.amount]
        } else {
            if let _ = bonusPoint {
                enumArray  = [ TopupConfirmationCellType.name, TopupConfirmationCellType.number, TopupConfirmationCellType.type, TopupConfirmationCellType.cashback, TopupConfirmationCellType.amount, TopupConfirmationCellType.bonusPoint]
            } else {
                enumArray  = [ TopupConfirmationCellType.name, TopupConfirmationCellType.number, TopupConfirmationCellType.type, TopupConfirmationCellType.cashback, TopupConfirmationCellType.amount]
            }
        }
        
        if confirmResponseModel.count <= 1 {
            loadAdvertisement()
        } else {
            self.advertisementContainer.isHidden = true
        }
    }
    
    func backButton(withTitle title:String){

        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "tabBarBack"), for: UIControl.State.normal)

        button.tintColor = UIColor.white
        
        button.sizeToFit()
        button.addTarget(self, action: #selector(backButtonCustomAction), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        
        self.title = title
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.payBtn.setTitle("TopUpPay".localized, for: .normal)
        self.payBtn.backgroundColor = MyNumberTopup.theme
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.timer?.invalidate()
    }
    
    //MARK:- Timer --- Local Session Handling
    
    func initiateTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(TopupConfirmationViewController.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc fileprivate func updateTimer() {
        
        self.seconds = self.seconds + 1
        
        if self.seconds == 300 {
            self.seconds = 0
            timer?.invalidate()
            alertViewObj.wrapAlert(title: nil, body: "Session expired".localized, img: #imageLiteral(resourceName: "dashboard_my_number"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                OKPayment.main.authenticate(screenName: "topup", delegate: self)
//                self.navigationController?.popViewController(animated: true)
            })
            alertViewObj.showAlert(controller: self)
        }
    }
    
    //MARK:- Advertisement Views
    func loadAdvertisement() {
        let randomIndex = Int(arc4random_uniform(UInt32(advImagesUrl.AdImageData.count)))
        if let urlString = advImagesUrl.AdImageData[safe: randomIndex] {
            DispatchQueue.main.async {
                self.adImage.image = UIImage(data: urlString)
                self.adImage.contentMode = .scaleAspectFit
//                self.adImage.setImage(url: urlString, contentMode: UIView.ContentMode.scaleAspectFit, placeholderImage: nil)
            }
        }
    }

    //MARK:- Pay Action
    @IBAction func payAction(_ sender: UIButton) {
        
        if UserLogin.shared.loginSessionExpired {
            alertViewObj.wrapAlert(title: nil, body: "Session Expired".localized, img: #imageLiteral(resourceName: "dashboard_my_number"))
            alertViewObj.addAction(title: "OK", style: .cancel, action: {
                OKPayment.main.authenticate(screenName: "topup", delegate: self)
            })
            alertViewObj.showAlert()
            return
        }
        
        if appDelegate.checkNetworkAvail() == false {
            alertViewObj.wrapAlert(title: nil, body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "dashboard_my_number"))
            alertViewObj.addAction(title: "OK", style: .cancel, action: {
                
            })
            alertViewObj.showAlert(controller: self)
            return
        }
        
        switch screenType {
        case .mynumber:
            if let _  = self.bonusPoint {
                self.bonusPointPaymentScene()
            }else {
                if self.tapCount == 0 {
                    self.tapCount = tapCount + 1
                    self.myNumberPaymentScene()
                }else {
                  println_debug("tapCount in Other = \(tapCount)")
                }
            }
            break
        case .otherNumber:
             self.otherNumberPayment()
            break
        }
    }
    
   private func myNumberPaymentScene() {

        if MyNumberTopup.operatorCase == .mactel {
            
            guard let model = confirmResponseModel.first else {
                return
            }
            
            var num = ""
            for index in 0 ...  UserModel.shared.mobileNo.count - 1 {
                num = num + String.init(UserModel.shared.mobileNo[index])
                if num == "0095" {
                    num = "0"
                }
            }

            let comments : String = String.init(format: "OOK-00-%@-%@-%@-naO",model.operatorName,num,model.type)
            
            let stringUrl = String.init(format: TopupUrl.mectelSinglePay, UserModel.shared.mobileNo, model.paymentNumber, model.developerAmount, UserModel.shared.mobileNo, comments, ok_password ?? "", UserLogin.shared.token, MyNumberTopup.opName.uppercased() + model.developerAmount)
            guard let urlQuery = stringUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
            guard let url = URL.init(string: urlQuery) else { return }
            let par = Dictionary<String,Any>()
            
            println_debug("Check Agent url : \(stringUrl)")
            DispatchQueue.main.async {
                TopupWeb.genericClassXML(url: url, param: par as AnyObject, httpMethod: "POST") { [weak self](response, success) in
                    if success {
                        self?.tapCount = 0
                        guard let stringData = response as? String else { return }
                        let xmlString = SWXMLHash.parse(stringData)
                        self?.enumerate(indexer: xmlString)
                        if self?.transaction.safeValueForKey("resultdescription") as? String == "Transaction Successful" {
                            if let jsonData = self?.transaction.jsonString().data(using: .utf8) {
                                if let model1 = try? JSONDecoder().decode(PaymentFinalRecieptModel.self, from: jsonData) {
                                    DispatchQueue.main.async {
                                        guard let vc = self?.storyboard?.instantiateViewController(withIdentifier: "TopupSingleViewRecieptController") as? TopupSingleViewRecieptController else { return }
                                        vc.reciptModel = model1
                                        vc.nameOfPerson = model1.agentname ?? ""
                                        vc.categoryValue = RecipetPlanCases(rawValue: RecipetPlanCases.myNumber.rawValue)
                                        vc.planType = self?.type
                                        vc.backendNumber = model.paymentNumber
                                        if let value = self{
                                            vc.isComingFromRecent = value.isComingFromRecent
                                        }
                                        self?.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                            }
                        } else {
                            
                            self?.failureCount = self?.failureCount ?? 0 + 1
                            if self?.failureCount == 2 {
                            self?.failureViewModel.sendDataToFailureApi(request: par as AnyObject, response: self?.transaction, type: FailureHelper.FailureType.TOPUP.rawValue, finished: {
                                
                            })
                            }
                            
                            
                            guard let stringData = response as? String else { return }
                            let xmlString = SWXMLHash.parse(stringData)
                            self?.enumerate(indexer: xmlString)
                            PaymentLogTransactions.manager.logPaymentApiFailureCase(self?.transaction.safeValueForKey("destination") as? String ?? "", errCode: self?.transaction.safeValueForKey("resultcode") as? String ?? "")
                        }
                        appDelegate.logEventWithAnaylitics(eventName: k_Payment, EventType: k_MECTEL, WithUser: UserModel.shared.mobileNo, AndEventStatus: k_TransactionSuccess)
                    } else {
                        self?.tapCount = 0
                        appDelegate.logEventWithAnaylitics(eventName: k_Payment, EventType: k_MECTEL, WithUser: UserModel.shared.mobileNo, AndEventStatus: k_TransactionFailure)
                    }
                }
            }
        } else {
            let stringUrl = TopupUrl.myNumberPayment
            guard let url = URL.init(string: stringUrl) else { return }
            let par = self.getParametersForPaymentTopup()
            println_debug("Check Agent url : \(url)")
            println_debug(String(data: try! JSONSerialization.data(withJSONObject: par, options: .prettyPrinted), encoding: .utf8 )!)
            DispatchQueue.main.async {
                TopupWeb.genericClass(url: url, param: par as AnyObject, httpMethod: "POST") { [weak self](response, success) in
                    if success {
                        self?.tapCount = 0
                        if let json = response as? Dictionary<String,Any> {
                            println_debug("topup result : \(json)")
                            if let code = json["Code"] as? NSNumber, code == 200 {
                                if let data = json["Data"] as? String {
                                    let xmlString = SWXMLHash.parse(data)
                                    self?.enumerate(indexer: xmlString)
                                    if self?.transaction.safeValueForKey("resultdescription") as? String == "Transaction Successful" {
                                        if let jsonData = self?.transaction.jsonString().data(using: .utf8) {
                                            if let model = try? JSONDecoder().decode(PaymentFinalRecieptModel.self, from: jsonData) {
                                                DispatchQueue.main.async {
                                                    guard let vc = self?.storyboard?.instantiateViewController(withIdentifier: "TopupSingleViewRecieptController") as? TopupSingleViewRecieptController else { return }
                                                    vc.reciptModel = model
                                                    vc.nameOfPerson = model.agentname ?? ""
                                                    vc.categoryValue = RecipetPlanCases(rawValue: RecipetPlanCases.myNumber.rawValue)
                                                    vc.planType = self?.type
                                                    if let value = self{
                                                        vc.isComingFromRecent = value.isComingFromRecent
                                                    }
                                                    //vc.backendNumber = self?.backendNumber
                                                    self?.navigationController?.pushViewController(vc, animated: true)
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                self?.failureCount = self?.failureCount ?? 0 + 1
                                if self?.failureCount == 2 {
                                self?.failureViewModel.sendDataToFailureApi(request: par as AnyObject, response: json, type: FailureHelper.FailureType.TOPUP.rawValue, finished: {
                                    
                                })
                                }
                                
                                if let msg = json["Msg"] as? String {
                                    self?.showErrorAlert(errMessage: msg)
                                }
                            }
                        }
                        if let model = self?.confirmResponseModel.first {
                            appDelegate.logEventWithAnaylitics(eventName: k_Payment, EventType: model.operatorName, WithUser: UserModel.shared.mobileNo, AndEventStatus: k_TransactionSuccess)
                        }
                    } else {
                        
                       
                        
                        self?.tapCount = 0
                        if let model = self?.confirmResponseModel.first {
                            appDelegate.logEventWithAnaylitics(eventName: k_Payment, EventType: model.operatorName, WithUser: UserModel.shared.mobileNo, AndEventStatus: k_TransactionFailure)
                        }                    }
                }
            }
    }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            transaction[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    private func otherNumberPayment() {
        if appDelegate.checkNetworkAvail() == false {
            alertViewObj.wrapAlert(title: nil, body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "dashboard_my_number"))
            alertViewObj.addAction(title: "OK", style: .cancel, action: {
                
            })
            alertViewObj.showAlert(controller: self)
            return
        }
        
        
    }
    
       fileprivate  func getTheParams() -> [[String: Any]] {
        
        var finalArray = [[String: Any]]()
        
        var arrayParam = [String: Any]()
        for (_,model) in self.confirmResponseModel.enumerated() {
            // Transaction cell tower
//            var TransactionsCellTower = [String: Any]()
//            TransactionsCellTower["Lac"] = ""
//            TransactionsCellTower["Mac"] = ""
//            TransactionsCellTower["Mcc"] = mccStatus.mcc
//            TransactionsCellTower["Mnc"] = mccStatus.mnc
//            TransactionsCellTower["SignalStrength"] = UitilityClass.getSignalStrength()
//            TransactionsCellTower["Ssid"] = ""
//            TransactionsCellTower["ConnectedwifiName"] = ""
//            TransactionsCellTower["NetworkType"] = ""
            var TransactionsCellTower  = [String: Any]()
            TransactionsCellTower["Lac"] = ""
            TransactionsCellTower["Mcc"] = mccStatus.mcc
            TransactionsCellTower["Mnc"] = mccStatus.mnc
            TransactionsCellTower["SignalStrength"] = ""
            TransactionsCellTower["NetworkType"] = UitilityClass.getNetworkType()
            TransactionsCellTower["NetworkSpeed"] = UitilityClass.getSignalStrength()
            
            if UitilityClass.isConnectedToWifi(){
                let wifiInfo = UitilityClass.getNetworkInfos()[0].components(separatedBy: ",")
                TransactionsCellTower["ConnectedwifiName"] = wifiInfo[0]
                TransactionsCellTower["Ssid"] = wifiInfo[0]
                TransactionsCellTower["Mac"] = wifiInfo[1]
            }else{
                TransactionsCellTower["ConnectedwifiName"] = ""
                TransactionsCellTower["Ssid"] = ""
                TransactionsCellTower["Mac"] = ""
            }
            
            // l External Reference
            var lExternalReference = [String: Any]()
            lExternalReference["Direction"] = true
            lExternalReference["EndStation"] = ""
            lExternalReference["StartStation"] = ""
            lExternalReference["VendorID"] = ""
            
            // L geo Location
            var lGeoLocation = [String: Any]()
            lGeoLocation["CellID"] = ""
            lGeoLocation["Latitude"] = manager.currentLatitude
            lGeoLocation["Longitude"] = manager.currentLongitude
            
            // L Proximity
            var lProximity = [String: Any]()
            lProximity["BlueToothUsers"] = [Any]()
            lProximity["SelectionMode"]  = false
            lProximity["WifiUsers"]      = [Any]()
            lProximity["mBltDevice"]     = ""
            lProximity["mWifiDevice"]    = ""
            
            // Final payment Dictionary
            // comments for the particular transaction
            // Final payment Dictionary
            let operatorNameLocal = self.confirmResponseModel.first?.operatorName.lowercased() ?? ""
            
            
            
            println_debug(model)
            // lTransactions//
            var lTransactionsDict = [String: Any]()
            lTransactionsDict["Amount"]                 = model.developerAmount
            lTransactionsDict["Balance"]                 = ""
            lTransactionsDict["BonusPoint"]              = 0
            lTransactionsDict["CashBackFlag"]            = 0 // need to check the flag value
            lTransactionsDict["Comments"]                = ""
            lTransactionsDict["Destination"]            = model
            lTransactionsDict["DiscountPayTo"]          = "0" // discount need to check from the server
            lTransactionsDict["IsMectelTopUp"]          = false
            lTransactionsDict["accounType"]             = UserModel.shared.agentServiceTypeString()
            let kickBackNumber = self.getKickbackTopupByOperator(opaerator: operatorNameLocal)
            lTransactionsDict["KickBackMsisdn"]         = kickBackNumber
            
            arrayParam["TransactionsCellTower"] = TransactionsCellTower
            arrayParam["lExternalReference"]    = lExternalReference
            arrayParam["lGeoLocation"]          = lGeoLocation
            arrayParam["lProximity"]            = lProximity
            arrayParam["lTransactions"]         = lTransactionsDict
            
            finalArray.append(arrayParam)
        }
        
        println_debug(finalArray)
        return finalArray
        
    }


    
    func getParametersForPaymentTopup() -> Dictionary<String, Any> {
        
        // Transaction cell tower
//        var TransactionsCellTower  = Dictionary<String,Any>()
//        TransactionsCellTower["Lac"] = ""
//        TransactionsCellTower["Mac"] = ""
//        TransactionsCellTower["Mcc"] = mccStatus.mcc
//        TransactionsCellTower["Mnc"] = mccStatus.mnc
//        TransactionsCellTower["SignalStrength"] = ""
//        TransactionsCellTower["Ssid"] = ""
//        TransactionsCellTower["ConnectedwifiName"] = ""
//        TransactionsCellTower["NetworkType"] = ""
        // Transaction cell tower

        var TransactionsCellTower  = Dictionary<String,Any>()
        TransactionsCellTower["Lac"] = ""
        TransactionsCellTower["Mcc"] = mccStatus.mcc
        TransactionsCellTower["Mnc"] = mccStatus.mnc
        TransactionsCellTower["SignalStrength"] = ""
        TransactionsCellTower["NetworkType"] = UitilityClass.getNetworkType()
        TransactionsCellTower["NetworkSpeed"] = UitilityClass.getSignalStrength()
        
        if UitilityClass.isConnectedToWifi(){
            let wifiInfo = UitilityClass.getNetworkInfos()[0].components(separatedBy: ",")
            TransactionsCellTower["ConnectedwifiName"] = wifiInfo[0]
            TransactionsCellTower["Ssid"] = wifiInfo[0]
            TransactionsCellTower["Mac"] = wifiInfo[1]
        }else{
            TransactionsCellTower["ConnectedwifiName"] = ""
            TransactionsCellTower["Ssid"] = ""
            TransactionsCellTower["Mac"] = ""
        }

        // l External Reference
        var lExternalReference = Dictionary<String,Any>()
        lExternalReference["Direction"] = true
        lExternalReference["EndStation"] = ""
        lExternalReference["StartStation"] = ""
        lExternalReference["VendorID"] = ""
        
        // L geo Location

        var lGeoLocation = Dictionary<String,Any>()
        lGeoLocation["CellID"] = ""
        lGeoLocation["Latitude"] = manager.currentLatitude
        lGeoLocation["Longitude"] = manager.currentLongitude
        
        // L Proximity
        var lProximity = Dictionary<String,Any>()
        lProximity["BlueToothUsers"] = [Any]()
        lProximity["SelectionMode"] = false
        lProximity["WifiUsers"] = [Any]()

        // Final payment Dictionary
        guard let model = self.confirmResponseModel.first else {
            return [:]
        }
        
        var num = ""
        
        if model.paymentNumber.count < 1 {
            return [:]
        }
        
        for index in 0 ...  model.paymentNumber.count - 1 {
            num = num + String.init(UserModel.shared.mobileNo[index])
            if num == "0095" {
                num = "0"
            }
        }
        
        var planType = ""
        
        if model.type == "Top-Up Plan"{
            planType = "TopUpPlan"
        }
        else if model.type == "Data Plan" {
            planType = "DataPlan"
        }
        else {
            planType = "SpecialOffer"
        }
        let comments = String.init(format: "#OK-00-%@-%@-%@-na-%@-%@#",model.operatorName.lowercased().capitalizedFirst(),num,planType,manager.currentLatitude,manager.currentLongitude)
        
        var lTransactions = Dictionary<String, Any>()
        lTransactions["Amount"] = model.developerAmount
        lTransactions["DestinationNumberAccountType"] = ""
        lTransactions["Comments"] = comments
        lTransactions["Destination"] = model.number
        backendNumber = model.number
        lTransactions["LocalTransactionType"]  = "TOPUP"
        lTransactions["MobileNumber"] = UserModel.shared.mobileNo
        lTransactions["Password"] = ok_password ?? ""
        lTransactions["PromoCodeId"] = ""
        lTransactions["SecureToken"] = UserLogin.shared.token
        lTransactions["TransactionType"] = "PAYTO"
        let kickBackNumber = self.getKickbackTopupByOperator(opaerator: model.operatorName.lowercased())
        lTransactions["KickBackMsisdn"]         = kickBackNumber
        
        var finalParameterDictionaried = Dictionary<String,Any>()
        finalParameterDictionaried["TransactionsCellTower"] = TransactionsCellTower
        finalParameterDictionaried["lExternalReference"] = lExternalReference
        finalParameterDictionaried["lGeoLocation"] = lGeoLocation
        finalParameterDictionaried["lProximity"] = lProximity
        finalParameterDictionaried["lTransactions"] = lTransactions
        
        println_debug(finalParameterDictionaried)
        
        return finalParameterDictionaried
    }

    deinit {
        geoLocManager.stopUpdateLocation()
    }
}

//MARK:- TableView Delegates
extension TopupConfirmationViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.confirmResponseModel.count > 1 {
            return 6
        } else {
            return 5
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.confirmResponseModel.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: TopupConfirmationTableViewCell.self), for: indexPath) as? TopupConfirmationTableViewCell
        let headerString = String.init(format: "Recharge %i", indexPath.section + 1)
        cell?.wrapCellData(self.confirmResponseModel[indexPath.section], withCellType: enumArray[indexPath.row], havingHeader: headerString)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let model = self.confirmResponseModel[indexPath.section]
        
        if self.confirmResponseModel.count > 1 {
            if indexPath.row == 3, model.developerCashback == "Kick-Back Rules Not Define" {
                return 0.00
            }
        } else {
            if indexPath.row == 3, model.developerCashback == "Kick-Back Rules Not Define" {
                return 0.00
            }
        }
        return 50.00
    }
        
}

//MARK:- Bonus Point Functions
extension TopupConfirmationViewController {
    
    private func bonusPointPaymentScene() {
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "bonusPointPay", delegate: self)
        } else {
            self.payBonusPoint()
        }
    }
    
    private func payBonusPoint() {
        guard let model = self.bonusPoint else { return }
        let bonusPayUrl = getUrl(urlStr: Url.bonusPointPay, serverType: .serverApp)
        let params = self.JSONStringFromAnyObject(value: self.getParamsForBonusPoints(bonusModel: model))
        println_debug("Check Agent url : \(bonusPayUrl)")
        println_debug(params)

        TopupWeb.genericClass(url: bonusPayUrl, param: params as AnyObject, httpMethod: "POST") { (response, success) in
            println_debug(response)
        }
    }
    
    private func getParamsForBonusPoints(bonusModel: BonusPointTopupModel) -> Dictionary<String,Any> {
        
        var loginParams = Dictionary<String,Any>()
        loginParams["AppId"] = LoginParams.setUniqueIDLogin()
        loginParams["Limit"] = 0
        loginParams["MobileNumber"] = UserModel.shared.mobileNo
        loginParams["Msid"] = getMsid()
        loginParams["Offset"] = 0
        loginParams["Ostype"] = 1
        loginParams["Otp"] = ""
        loginParams["Simid"] = simid

        var bonusParams = Dictionary<String,Any>()
        bonusParams["AvialableBonusPoints"]    = bonusModel.availableBonus.safelyWrappingString()
        bonusParams["BeneficiaryAmount"]      = bonusModel.amount.safelyWrappingString()
        bonusParams["BeneficiaryNumber"]      = UserModel.shared.formattedNumber
        bonusParams["BonusPointSourceNumber"] = bonusModel.paymentNumber.safelyWrappingString()
        bonusParams["Comments"]            = ""
        bonusParams["Destination"]            = bonusModel.paymentNumber.safelyWrappingString()
        bonusParams["Password"]             = ok_password.safelyWrappingString()
        bonusParams["QtyOfRedeemPoints"]      = bonusModel.points.safelyWrappingString()
        bonusParams["SecureToken"]           = UserLogin.shared.token
        bonusParams["Source"]               = UserModel.shared.mobileNo
        bonusParams["TelcoName"]            = bonusModel.operatorName.safelyWrappingString()
        bonusParams["TransactionType"]         = "TOPUP"
        
        var finalParams = Dictionary<String,Any>()
        finalParams["LoginInfo"] = loginParams
        finalParams["RedeempointsSellingDto"] = bonusParams
        
        return finalParams
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful, screen.lowercased() == "bonusPointPay".lowercased() {
            self.payBonusPoint()
        }
    }
}


extension StringProtocol {
    var firstUppercased: String {
        return prefix(1).uppercased()  + dropFirst()
    }
//    var firstCapitalized: String {
//        return prefix(1).capitalized + dropFirst()
//    }
}
