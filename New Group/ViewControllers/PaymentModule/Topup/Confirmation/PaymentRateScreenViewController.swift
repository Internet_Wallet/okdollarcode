//
//  PaymentRateScreenViewController.swift
//  OK
//
//  Created by Ashish on 5/15/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol PaymentRateDelegate {
    func ratingShow(_ rate: Int, commentText: String?)
}

class PaymentRateScreenViewController: OKBaseController {
    
    //MARK: - Outlets
    @IBOutlet weak var lblRateService: UILabel!
        {
        didSet
        {
            lblRateService.font = UIFont(name: appFont, size: appFontSize)
                lblRateService.text = lblRateService.text?.localized
        }
    }
    @IBOutlet weak var lblFeedback: UILabel!
        {
        didSet
        {
            lblFeedback.font = UIFont(name: appFont, size: appFontSize)
            lblFeedback.text = lblFeedback.text?.localized
        }
    }
    @IBOutlet weak var btnClose: UIButton!
        {
        didSet
        {
            self.btnClose.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
            self.btnClose.setTitle(self.btnClose.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var btnSubmit: UIButton!
        {
        didSet
        {
            self.btnSubmit.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
            self.btnSubmit.setTitle(self.btnSubmit.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var starArray: [UIButton]!
    @IBOutlet weak var feedbackTF: UITextField!
    
    //MARK: - Properties
    var destinationNumber: String?
    var delegate: PaymentRateDelegate?
    var rating: Int = -1
    var confirmationCellScreen: String?
    var isComingFromRecent = false
    
    //MARK: - Views method
    override func viewDidLoad() {
        super.viewDidLoad()
        for star in starArray {
            star.setImage(#imageLiteral(resourceName: "rate_blue_star"), for: .normal)
        }
        feedbackTF.attributedPlaceholder = NSAttributedString(string: "",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    
    //MARK: - Methods
    private func starSelected(tag: Int) {
        rating = tag
        for star in starArray {
            if star.tag <= tag {
                UIView.animate(withDuration: 0.2, animations: {
                    star.setImage(#imageLiteral(resourceName: "rate_yellow_star"), for: .normal)
                })
            } else {
                star.setImage(#imageLiteral(resourceName: "rate_blue_star"), for: .normal)
            }
        }
    }
    
    func thanksPopup() {
        DispatchQueue.main.async() {
            alertViewObj.wrapAlert(title: nil, body: "Thanks for your valuable feedback!".localized, img: #imageLiteral(resourceName: "ratingsuc.png"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        }
    }
    
    //MARK: API for FeedBack
    func callingFeedbackApi() {
        let param = self.JSONStringFromAnyObject(value: self.feedbackParametersGeneration() as AnyObject)
        let url = getUrl(urlStr: Url.feedbackPayment, serverType: .serverApp)
        TopupWeb.genericClass(url: url, param: param as AnyObject, httpMethod: "POST") { [weak self](response, success) in
            if success {
                 DispatchQueue.main.async() {
                self?.dismiss(animated: true, completion: nil)
                if let delegate = self?.delegate {
                    if let weakSelf = self {
                        delegate.ratingShow(weakSelf.rating, commentText: "")
                    }
                }
                }
            }
        }
    }
    
    func feedbackParametersGeneration() -> Dictionary<String,Any> {
        var finalParam = Dictionary<String,Any>()
        var customerFeedback = Dictionary<String,Any>()
        customerFeedback["CellId"] = ""
        
        if  let comments = feedbackTF.text {
            customerFeedback["Comments"] = comments
        } else {
            customerFeedback["Comments"] = ""
        }
        customerFeedback["CustNum"] = UserModel.shared.mobileNo
        guard let destination = destinationNumber else { return finalParam }
        customerFeedback["DestNum"] = destination
        customerFeedback["Latitude"]  = GeoLocationManager.shared.currentLatitude
        customerFeedback["Longitude"] = GeoLocationManager.shared.currentLongitude
        customerFeedback["RatingNum"] = rating
        customerFeedback["SubTransactionTypeName"] = "PAYTO"
        customerFeedback["TransactionTypeName"] = "Payto"
        
        var login = Dictionary<String,Any>()
        login["AppId"] = 0
        login["Limit"] = 0
        login["MobileNumber"] = UserModel.shared.mobileNo
        login["Msid"] = getMsid()
        login["Offset"] = 0
        login["Ostype"] = 1
        login["Otp"] = ""
        login["Simid"] = uuid
        
        finalParam["CustomerFeedBackInfo"] = customerFeedback
        finalParam["Login"] = login
        
        return finalParam
    }
    
    //MARK: - Button Action Methods
    @IBAction func selectionStarDispaly(_ sender: UIButton) {
        self.starSelected(tag: sender.tag)

    }
    
    //MARK: Button Action close & Submit
    @IBAction func closeAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        if rating >= 0 {
            if confirmationCellScreen == nil {
                self.dismiss(animated: true, completion: nil)
                if let delegate = self.delegate {
                    delegate.ratingShow(self.rating, commentText: feedbackTF.text)            }
            } else {
                self.callingFeedbackApi()
            }
            self.thanksPopup()
        } else {
            self.noratingPopup(errMessage: "No Rating Selected! Kindly give Rating to improve our Quality of Service".localized)
        }
    }
}
