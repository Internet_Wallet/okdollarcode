//
//  TopupSingleViewRecieptController.swift
//  OK
//
//  Created by Ashish on 5/9/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

class TopupSingleViewRecieptController: OKBaseController {
    
    //MARK: - Outlets
    @IBOutlet weak var nameLabel: UILabel!{
        didSet
        {
            nameLabel.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    @IBOutlet weak var mobileNumber: UILabel!{
        didSet
        {
            mobileNumber.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
        
    @IBOutlet weak var amount: UILabel!{
        didSet
        {
            amount.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    
    @IBOutlet weak var keyName: UILabel!{
        didSet
        {
            keyName.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    @IBOutlet weak var valueName: UILabel!{
    didSet
    {
        valueName.font = UIFont(name: appFont, size: appTitleButton)
    }
}
    
    
    @IBOutlet weak var imageType: UIImageView!
    @IBOutlet weak var keyCategories: UILabel!{
        didSet
        {
            keyCategories.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    
    @IBOutlet weak var valueCategories: UILabel!{
        didSet
        {
            valueCategories.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    @IBOutlet weak var keyBalance: UILabel!{
        didSet
        {
            keyBalance.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var valueBalance: UILabel!{
    didSet
    {
        valueBalance.font = UIFont(name: appFont, size: appTitleButton)
    }
}
    @IBOutlet weak var keyTransactionID: UILabel!{
        didSet
        {
            keyTransactionID.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    
    @IBOutlet weak var valueTransactionID: UILabel!{
        didSet
        {
            valueTransactionID.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    
    @IBOutlet weak var keyTransactionType: UILabel!{
        didSet
        {
            keyTransactionType.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    
    @IBOutlet weak var valueTransactionType: UILabel!{
        didSet
        {
            valueTransactionType.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    @IBOutlet weak var valueDate: UILabel!{
        didSet
        {
            valueDate.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    @IBOutlet weak var valueTime: UILabel!{
        didSet
        {
            valueTime.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    
    @IBOutlet weak var keycashback: UILabel!{
        didSet
        {
            keycashback.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    @IBOutlet weak var valueCashBack: UILabel!{
        didSet
        {
            valueCashBack.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    @IBOutlet weak var constraintTopDate: NSLayoutConstraint!
    @IBOutlet weak var constraintTopTransID: NSLayoutConstraint!
    
    @IBOutlet weak var rateservice: UILabel!{
        didSet
        {
            rateservice.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var tabHome : UITabBarItem!
    @IBOutlet weak var tabMore : UITabBarItem!
    @IBOutlet weak var tabContact : UITabBarItem!
    @IBOutlet weak var tabfav: UITabBarItem!
    @IBOutlet weak var cardView: CardDesignView!
    @IBOutlet  var starArray: [UIImageView]!
    
    //MARK: - Properties
    var reciptModel : PaymentFinalRecieptModel?
    var categoryValue : RecipetPlanCases?
    var tabBarAction : RecieptTabSelectionType?
    var rechangeNumber: String?
    var planType : String?
    var InternationalTopup : String?
    var currencyInt : String?
    var contactAdded : Bool = false
    var favoriteAddedCheck: Bool = false
    var receiverName = ""
    var manualRate = 0
    var category = ""
    var nameOfPerson = ""
    
    @IBOutlet weak var viewTab: UIView!
    var viewOptions: TabOptionsOnReceipt?
    let optionTextDefaultColor = UIColor.init(hex: "162D9F")
    var currentIndex = 0
    var backendNumber: String?
    var isComingFromRecent = false
    
    //MARK: - Views life cycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            self.changeCancelImplementation()
        }
        self.uiUpdates()
        GetProfile.updateBalance()
        navigationItem.title = "Receipt".localized
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: ConstantsColor.navigationHeaderSendMoney ]
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = kYellowColor
//        self.tabBar.unselectedItemTintColor = .lightGray
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.title = "Receipt".localized
        let color = UIColor.lightGray
        let attrFont = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.0) ?? UIFont.systemFont(ofSize: 11.0), NSAttributedString.Key.foregroundColor: color]
        
        self.tabHome.setTitleTextAttributes(attrFont, for: .normal)
        self.tabMore.setTitleTextAttributes(attrFont, for: .normal)
        self.tabContact.setTitleTextAttributes(attrFont, for: .normal)
        self.tabfav.setTitleTextAttributes(attrFont, for: .normal)
        self.tabfav.title = "Add Favorite".localized
        
        self.tabHome.title = "Home Page".localized
        self.tabMore.title = "More".localized
        self.tabContact.title = "TabbarAddContact".localized
        self.rateservice.text = "Rate OK$ Service".localized
        self.navigationController?.navigationBar.backgroundColor = MyNumberTopup.OperatorColorCode.okDefault
        //        self.navigationController?.navigationBar.tintColor = MyNumberTopup.OperatorColorCode.okDefault
        self.navigationItem.setHidesBackButton(true, animated: true)
        viewOptions!.lblAddFav.textColor = UIColor.init(red: 22.0/255.0, green: 45.0/255.0, blue: 159.0/255.0, alpha: 1.0)
        viewOptions!.lblAddContact.textColor = UIColor.init(red: 22.0/255.0, green: 45.0/255.0, blue: 159.0/255.0, alpha: 1.0)
        viewOptions!.lblHome.textColor = UIColor.init(red: 22.0/255.0, green: 45.0/255.0, blue: 159.0/255.0, alpha: 1.0)
        viewOptions!.lblMore.textColor = UIColor.init(red: 22.0/255.0, green: 45.0/255.0, blue: 159.0/255.0, alpha: 1.0)
    }
    
    //MARK: - Methods
    func addFavUI(isInFav: Bool) {
        if let _ = viewOptions {
            viewOptions?.imgAddFav.image = viewOptions?.imgAddFav.image?.withRenderingMode(.alwaysTemplate)
            if isInFav {
                viewOptions?.imgAddFav.tintColor = kYellowColor
                viewOptions!.lblAddFav.textColor = kYellowColor
            } else {
                viewOptions?.imgAddFav.tintColor = UIColor.gray
                viewOptions!.lblAddFav.textColor = UIColor.init(red: 22.0/255.0, green: 45.0/255.0, blue: 159.0/255.0, alpha: 1.0)
            }
            viewOptions!.layoutIfNeeded()
        }
    }
    
    func addContactUI(isInContact: Bool) {
        if let _ = viewOptions {
            viewOptions?.imgAddContact.image = viewOptions?.imgAddContact.image?.withRenderingMode(.alwaysTemplate)
            if isInContact {
                viewOptions?.imgAddContact.tintColor = kYellowColor
                viewOptions!.lblAddContact.textColor = kYellowColor
            } else {
                viewOptions?.imgAddContact.tintColor = UIColor.gray
                viewOptions!.lblAddContact.textColor = UIColor.init(red: 22.0/255.0, green: 45.0/255.0, blue: 159.0/255.0, alpha: 1.0)
            }
            viewOptions!.layoutIfNeeded()
        }
    }
    
    //MARK: - Methods
    private func uiUpdates() {
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.keyName.text = self.keyName.text.safelyWrappingString().localized
        self.keyCategories.text = self.keyCategories.text.safelyWrappingString().localized
        self.keyBalance.text = self.keyBalance.text.safelyWrappingString().localized
        self.keyTransactionID.text = self.keyTransactionID.text.safelyWrappingString().localized
        self.keyTransactionType.text = self.keyTransactionType.text.safelyWrappingString().localized
        self.valueDate.text = self.valueDate.text.safelyWrappingString().localized
        self.valueTime.text = self.valueTime.text.safelyWrappingString().localized
        
        guard let model = reciptModel else { return }
      //  self.mobileNumber.text = (InternationalTopup == nil) ?  wrapFormattedNumber(model.agentcode.safelyWrappingString()) : InternationalTopup.safelyWrappingString()
        if let num = self.rechangeNumber {
            self.mobileNumber.text = num
        }else {
            self.mobileNumber.text = (InternationalTopup == nil) ?  wrapFormattedNumber(model.agentcode.safelyWrappingString()) : InternationalTopup.safelyWrappingString()
        }
        self.amount.attributedText = self.formattingWalletBalance(model.amount.safelyWrappingString(), amountColor: UIColor.red, denomColor: UIColor(red: 255.0/255.0, green: 110.0/255.0, blue: 110.0/255.0, alpha: 1.0))
        self.valueName.text    =  nameOfPerson //model.agentname.safelyWrappingString()
        if let cases = categoryValue {
            switch cases {
            case .myNumber:
                self.valueCategories.text = "My Number"
                category = "My Number"
            case .otherNumber:
                self.valueCategories.text = "Other Number"
                category = "Other Number"
            case .payto:
                self.valueCategories.text = "PayTo"
                 category = "PayTo"
            }
        } else {
            self.valueCategories.text = "Overseas Top-Up".localized
            category = "Overseas Top-Up".localized
            imageType.image = UIImage.init(named: "dashboard_overseas_recharge")
        }
        
        self.valueBalance.attributedText = self.formattingWalletBalance(model.walletbalance.safelyWrappingString(), amountColor: UIColor.red, denomColor: UIColor(red: 255.0/255.0, green: 110.0/255.0, blue: 110.0/255.0, alpha: 1.0))
        self.valueTransactionID.text = model.transid.safelyWrappingString()
        self.valueTransactionType.text = planType.safelyWrappingString()
        
        if let date = model.responsects {
            if let dateFormat = date.components(separatedBy: " ").first?.dateValue(dateFormatIs: "dd-MMM-yyyy") {
                let dateKey = (dateFormat.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy"))
                self.valueDate.text = dateKey
                
                let timeKey = date.components(separatedBy: " ").last ?? ""
                self.valueTime.text = timeKey
            } else {
                let dateFormtter = DateFormatter()
                dateFormtter.dateFormat = "EEE, dd-MMM-yyyy"
                self.valueDate.text = dateFormtter.string(from: Date())
                dateFormtter.dateFormat = "HH:mm:ss"
                self.valueTime.text = dateFormtter.string(from: Date())
            }
        }
        
//        let preciseTime  = model.responsects.safelyWrappingString().components(separatedBy: " ")
//        self.valueDate.text = preciseTime.first.safelyWrappingString()
//        self.valueTime.text = preciseTime.last.safelyWrappingString()
        UserLogin.shared.walletBal = model.walletbalance.safelyWrappingString()
        //        self.navigationController?.navigationBar.backgroundColor = MyNumberTopup.OperatorColorCode.okDefault
        //        self.navigationController?.navigationBar.tintColor = MyNumberTopup.OperatorColorCode.okDefault
        
        if let _ = InternationalTopup {
            guard let operatorCountryModel = InternationalTopupManager.selectedCountryObject else { return  }
            guard let currency = operatorCountryModel.currencyDetail else  { return }
            var cCode = operatorCountryModel.countryCode.safelyWrappingString()
            cCode = cCode.deletingZeroPrefixes()
            cCode = cCode.hasPrefix("+") ? cCode : "+" + cCode
            self.mobileNumber.text = cCode + " " + InternationalTopup.safelyWrappingString()
            let denomination = " " + currency.currencyType.safelyWrappingString()
            self.keycashback.text = "Buy In".localized + " " + denomination
            self.keyName.text = "Operator".localized
            self.valueName.text = InternationalTopupManager.selectedOperatorObject?.operatorName.safelyWrappingString()
            self.valueCashBack.attributedText =  self.formattingCashbackAmount(currencyInt.safelyWrappingString(), denom: denomination, amountColor: UIColor.red, denomColor: UIColor(red: 255.0/255.0, green: 110.0/255.0, blue: 110.0/255.0, alpha: 1.0))
            self.nameLabel.text = self.receiverName
        } else {
            if model.kickvalue.safelyWrappingString() != "", model.kickvalue.safelyWrappingString().lowercased() != "Kickback rules not defined".lowercased() {
                if (model.kickvalue.safelyWrappingString() as NSString).floatValue > 0.00 {
                    self.keycashback.text = "Cashback".localized
                    self.valueCashBack.attributedText = self.formattingCashbackAmount(model.kickvalue.safelyWrappingString(), amountColor: UIColor.red, denomColor: UIColor(red: 255.0/255.0, green: 110.0/255.0, blue: 110.0/255.0, alpha: 1.0))
                } else {
                    self.hideCashback()
                }
            } else {
                self.hideCashback()
            }
            self.nameLabel.text = nameOfPerson //model.agentname.safelyWrappingString()
        }
        let number   = self.mobileNumber.text ?? ""
        // let number   = (InternationalTopup == nil) ?  model.agentcode.safelyWrappingString() : InternationalTopup.safelyWrappingString()
        let type     = "TOPUP"
        let amount   = model.amount.safelyWrappingString()
        let name     = "Unknown"
        let models = PaymentTransactions.init(number, type: type, name: name, amount: amount, transID: "", bonus: "0.0", backendNumber: backendNumber)
        PaymentVerificationManager.storeTransactions(model: models)
        TopupRecentContacts.storeTransactions(model: models)
        let isInCont = checkForContact(number).isInContact
       // let isInCont = checkForContact(model.agentcode.safelyWrappingString()).isInContact
        if isInCont {
            contactAdded = true
        }
        var typeFav = ""
        var dest = ""
        if let _ = InternationalTopup {
            typeFav = "OVERSEASTOPUP"
            dest = number
           // dest = self.mobileNumber.text!
        } else {
            typeFav = "TOPUPOTHER"
            dest = self.mobileNumber.text ?? ""//model.agentcode.safelyWrappingString()
        }
        let isInFav = checkForFavorites(dest, type: typeFav).isInFavorite
        if isInFav {
            favoriteAddedCheck = true
            
        }
        guard let bView = Bundle.main.loadNibNamed("TabOptionsOnReceipt", owner: self, options: nil)?.first as? TabOptionsOnReceipt else { return }
        viewOptions = bView
        bView.delegate = self
        viewOptions?.frame = CGRect(x: 0, y: 0, width: viewTab.frame.size.width, height: viewTab.frame.size.height)
        if let _ = viewOptions {
            viewTab.addSubview(viewOptions!)
            viewOptions!.lblHome.text = "HomePagePT".localized
            viewOptions!.lblMore.text = "More".localized
            viewOptions!.lblAddContact.text = "TabbarAddContact".localized
            viewOptions!.lblAddFav.text = "Add Favorite".localized
            if contactAdded {
                addContactUI(isInContact: true)
            }
            if favoriteAddedCheck {
                addFavUI(isInFav: true)
            }
            viewOptions!.layoutIfNeeded()
        }
    }
    
    func hideCashback() {
        self.constraintTopDate.constant = -22.00
        self.keycashback.isHidden = true
        self.valueCashBack.isHidden = true
        self.view.layoutIfNeeded()
    }
    
    func formattingWalletBalance(_ amount: String, amountColor: UIColor, denomColor: UIColor) -> NSMutableAttributedString {
        
        let number = NSDecimalNumber(string: amount)
        var puntuatedAmount = ""
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        if let num = formatter.string(from: number) {
            puntuatedAmount = (num == "NaN") ? "" : (num.contains(find: ".") ? num : num + ".00")
        }
        puntuatedAmount = puntuatedAmount.replacingOccurrences(of: ".00", with: "")
        let amtAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 15.0) ?? UIFont.systemFont(ofSize: 15),
                       NSAttributedString.Key.backgroundColor: UIColor.white,
                       NSAttributedString.Key.foregroundColor: amountColor]
        let mmkAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.0) ?? UIFont.systemFont(ofSize: 11),
                       NSAttributedString.Key.backgroundColor: UIColor.white,
                       NSAttributedString.Key.foregroundColor: denomColor]
        let amountStr    = NSMutableAttributedString.init(string: puntuatedAmount, attributes: amtAttr)
        let mmkString = NSMutableAttributedString(string: " MMK", attributes: mmkAttr)
        
        let stringAttributed = NSMutableAttributedString()
        stringAttributed.append(amountStr)
        stringAttributed.append(mmkString)
        return  stringAttributed
    }
    
    func formattingCashbackAmount(_ amount: String, denom: String = " MMK", amountColor: UIColor, denomColor: UIColor, forPdf: Bool = false) -> NSMutableAttributedString {
        let number = NSDecimalNumber(string: amount)
        var puntuatedAmount = ""
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        if let num = formatter.string(from: number) {
            puntuatedAmount = (num == "NaN") ? "" : (num.contains(find: ".") ? num : num + ".00")
        }
        puntuatedAmount = puntuatedAmount.replacingOccurrences(of: ".00", with: "")
        if forPdf {
            puntuatedAmount = ":    " + puntuatedAmount
        }
        let denomAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.0) ?? UIFont.systemFont(ofSize: 11),
                         NSAttributedString.Key.backgroundColor: UIColor.clear,
                         NSAttributedString.Key.foregroundColor: denomColor]
        let amntAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 15.0) ?? UIFont.systemFont(ofSize: 15),
                        NSAttributedString.Key.backgroundColor: UIColor.clear,
                        NSAttributedString.Key.foregroundColor: amountColor]
        let denomStr = NSMutableAttributedString(string: denom, attributes: denomAttr)
        let amountStr    = NSMutableAttributedString.init(string: puntuatedAmount, attributes: amntAttr)
        
        let stringAttributed = NSMutableAttributedString()
        stringAttributed.append(amountStr)
        stringAttributed.append(denomStr)
        return  stringAttributed
    }
    
    //MARK: ADD FAVORITE
    private func addFav(model: PaymentFinalRecieptModel) {
        if favoriteAddedCheck {
            PaytoConstants.alert.showToast(msg: "Contact already added in Favorites".localized)
        } else {
            var name = ""
            var type = ""
            var dest = ""
            
            if let _ = InternationalTopup {
                name = receiverName
                type = "OVERSEASTOPUP"
                dest = self.mobileNumber.text ?? ""
              //  dest = self.mobileNumber.text!
            } else {
                name = nameOfPerson//model.agentname.safelyWrappingString()
                
                type = "TOPUPOTHER"
                dest =  self.mobileNumber.text ?? ""  //model.agentcode.safelyWrappingString()
            }
//            let favVC = self.addFavoriteController(withName: name, favNum: dest, type: type, amount: model.amount.safelyWrappingString())
//            if let vc = favVC {
//                vc.delegate = self
//                vc.modalPresentationStyle = .overCurrentContext
//                self.navigationController?.present(vc, animated: false, completion: nil)
//            }
            
            let vc = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier:"PTFavoriteViewController") as! PTFavoriteViewController
             vc.view.frame = CGRect.zero
             vc.delegate = self
             vc.addFavDetails = (name, dest, type, model.amount.safelyWrappingString())
             vc.modalPresentationStyle = .overCurrentContext
             self.navigationController?.present(vc, animated: false, completion: nil)
        }
    }
    
    //MARK: ADD CONTACT
    private func addContacts(model: PaymentFinalRecieptModel) {
        if contactAdded {
            self.showErrorAlert(errMessage: "Contact Already Added".localized)
            return
        }
        let store     = CNContactStore()
        let contact   = CNMutableContact()
        
        var name = ""
        if let _ = InternationalTopup {
            name = receiverName
        } else {
            name = nameOfPerson//model.agentname.safelyWrappingString()
        }
        contact.givenName = name
       // let number = (InternationalTopup == nil) ? wrapFormattedNumber(model.agentcode.safelyWrappingString()) : InternationalTopup.safelyWrappingString()
        
        var mobileNumber = ""
        if let num = self.rechangeNumber {
            mobileNumber = num
        }else {
            mobileNumber = (InternationalTopup == nil) ?  wrapFormattedNumber(model.agentcode.safelyWrappingString()) : InternationalTopup.safelyWrappingString()
        }
        let number = mobileNumber
        let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue : number))
        contact.phoneNumbers = [homePhone]
        let controller = CNContactViewController.init(forNewContact: contact)
        controller.contactStore = store
        controller.delegate     = self
        controller.title        = "Add Contact".localized
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.backgroundColor = UIColor.init(hex: "F3C632")
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK: HOME
    private func goToHome() {
        
        if let cases = categoryValue {
            switch cases {
            case .myNumber:
                self.valueCategories.text = "My Number"
                
                if isComingFromRecent{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MoveToDashboardFromTopUp"), object: nil)
                }else{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoToDashboard"), object: nil, userInfo: nil)
                }
                
                
                break
            case .otherNumber:
                self.valueCategories.text = "Other Number"
                
                if isComingFromRecent{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MoveToDashboardFromTopUp"), object: nil)
                }else{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoToDashboardMultiTopup"), object: nil, userInfo: nil)
                }
                
               // NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoToDashboardMultiTopup"), object: nil, userInfo: nil)
                break
            case .payto:
                self.valueCategories.text = "PayTo"
                if isComingFromRecent{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MoveToDashboardFromTopUp"), object: nil)
                }else{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoToDashboard"), object: nil, userInfo: nil)
                }
                break
            }
        }
        //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoToDashboard"), object: nil, userInfo: nil)
    }
    
    //MARK: MORE
    private func more() {
        guard  let vc = UIStoryboard.init(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PTMoreViewController") as? PTMoreViewController else {return}
        addChild(vc)
        vc.view.frame = self.view.bounds
        vc.didMove(toParent: self)
        vc.delegate = self
        
        if planType.safelyWrappingString().lowercased() == "Data Plan".lowercased() {
            //            vc.titleMorePayment   = "More Data Plan"
            //            vc.titleRepeatPayment = "Repeat Data Plan"
            vc.headerTopup = ("More Data Plan".localized,"Repeat Data Plan".localized)
            
        } else if planType.safelyWrappingString().lowercased() == "Top-Up Plan".lowercased()  {
            //            vc.titleMorePayment   = "More Top-up"
            //            vc.titleRepeatPayment = "Repeat Top-up"
            vc.headerTopup = ("More Top-up".localized,"Repeat Top-up".localized)
            
        } else if planType.safelyWrappingString().lowercased() == "Special Offers".lowercased()  {
            //            vc.titleMorePayment   = "More Special Offer"
            //            vc.titleRepeatPayment = "Repeat Special Offer"
            
            vc.headerTopup = ("More Special Offer".localized,"Repeat Special Offer".localized)
        } else if planType.safelyWrappingString().lowercased() == "Special Offer".lowercased() {
            //            vc.titleMorePayment   = "More Topup"
            //            vc.titleRepeatPayment = "Repeat Topup"
            vc.headerTopup = ("More Special Offer".localized,"Repeat Special Offer".localized)
        } else {
            vc.headerTopup = ("More Top-up".localized,"Repeat Top-up".localized)
        }
        
        vc.morePayment.setTitle(vc.headerTopup!.0, for: .normal)
        vc.repeatPayment.setTitle(vc.headerTopup!.1, for: .normal)
        
        let transition = CATransition()
        transition.duration = 0.2
        transition.type = CATransitionType.moveIn
        
        vc.view.layer.add(transition, forKey: nil)
        view.addSubview(vc.view)
    }
    
    private func morePayment() {
        if (InternationalTopup != nil) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MultiTopupReloadMorePaymentOverseas"), object: nil, userInfo: nil)
        }else if category == "Other Number".localized {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MultiTopupReloadMorePayment"), object: nil, userInfo: nil)
        }else {
            
        }
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    private func moreRepeatPayment() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MultiTopupReloadMorePaymentOverseas"), object: nil, userInfo: nil)
        if (InternationalTopup != nil) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MultiTopupReloadMorePaymentOverseas"), object: nil, userInfo: nil)
        }else if category == "Other Number".localized {
            if let num = rechangeNumber {
                let userInfo : [String:String] = ["key": num]
                println_debug(userInfo)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MultiTopupReloadMoreRepeatPayment"), object: nil, userInfo: userInfo)
            }
        }else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MultiTopupReloadMorePaymentOverseas"), object: nil, userInfo: nil)
        }
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    private func invoice() {
        guard let model = reciptModel else { return }
        var pdfDictionary = Dictionary<String,Any>()
        pdfDictionary["senderAccName"]      = UserModel.shared.name
        
        let number = UserModel.shared.formattedNumber
        if let _ = InternationalTopup {
            pdfDictionary["invoiceTitle"]       = "Transaction Receipt".localized
            pdfDictionary["senderAccNo"]        =  "(+95)" + "0" + number
            pdfDictionary["transactionID"]      = model.transid
            pdfDictionary["transactionType"]    = planType.safelyWrappingString()
            pdfDictionary["transactionDate"]    = model.responsects.safelyWrappingString()
            pdfDictionary["amount"] = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: model.amount.safelyWrappingString()) + " MMK"
//            pdfDictionary["amount"]             = self.formattingWalletBalance(model.amount.safelyWrappingString(), amountColor: UIColor.black, denomColor: UIColor(red: 80.0/255.0, green: 80.0/255.0, blue: 80.0/255.0, alpha: 1.0))
            var cCode = ""
            if let operatorCountryModel = InternationalTopupManager.selectedCountryObject{
                cCode = operatorCountryModel.countryCode.safelyWrappingString()
                if let currency = operatorCountryModel.currencyDetail {
                    let denomination = " " + currency.currencyType.safelyWrappingString()
                    pdfDictionary["buyInTitle"] = denomination
                    pdfDictionary["buyIn"]             = self.formattingCashbackAmount(currencyInt.safelyWrappingString(), denom: denomination, amountColor: UIColor.black, denomColor: UIColor(red: 80.0/255.0, green: 80.0/255.0, blue: 80.0/255.0, alpha: 1.0), forPdf: true)
                }
            }
            if cCode.count > 0 {
                cCode = cCode.deletingZeroPrefixes()
                cCode = cCode.hasPrefix("+") ? cCode : "+" + cCode
                pdfDictionary["mobileNumberTopup"]  = "("+cCode+") 0"+InternationalTopup.safelyWrappingString()
            } else {
                pdfDictionary["mobileNumberTopup"]  = InternationalTopup.safelyWrappingString()
            }
            pdfDictionary["operatorName"] = InternationalTopupManager.selectedOperatorObject?.operatorName.safelyWrappingString()
            let dtStr = model.responsects
            if let dtVal = dtStr!.dateValue(dateFormatIs: "dd-MMM-yyyy HH:mm:ss") {
                let datStr = dtVal.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss")
                pdfDictionary["transactionDate"] = datStr
            } else {
                pdfDictionary["transactionDate"] = dtStr
                
            }
            pdfDictionary["logoName"]           = "appIcon_Ok"
        } else {
            pdfDictionary["invoiceTitle"]       = "Transaction Receipt".localized
            pdfDictionary["senderAccNo"]        = "(+95)" + " " + number
            var receiverNumber = ""
            if let num = self.rechangeNumber {
                receiverNumber = "(+95)" + " " + num
            }else {
                receiverNumber =  "(+95)" + " " + number
            }
            pdfDictionary["mobileNumberTopup"] = receiverNumber
            pdfDictionary["transactionID"]      = model.transid
            pdfDictionary["transactionType"]    = planType.safelyWrappingString()
            pdfDictionary["transactionDate"]    = model.responsects.safelyWrappingString()
            pdfDictionary["amount"] = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: model.amount.safelyWrappingString()) + " MMK"
            //pdfDictionary["amount"]         = self.formattingWalletBalance(model.amount.safelyWrappingString(), amountColor: UIColor.black, denomColor: UIColor(red: 80.0/255.0, green: 80.0/255.0, blue: 80.0/255.0, alpha: 1.0))
            if let kValue = model.kickvalue {
                if kValue.lowercased() != "Kickback rules not defined".lowercased() || kValue.lowercased() != ""  {
                    
                    pdfDictionary["cashback"] = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: model.kickvalue.safelyWrappingString()) + " MMK"
                    
                    /*
                    let cashback = model.kickvalue.safelyWrappingString().components(separatedBy: " ")
                    if cashback.indices.contains(0){
                        let newValue = cashback[0].components(separatedBy: ".")
                        if newValue.count>0{
                            var finalValue = ""
                            if newValue.indices.contains(0){
                                finalValue = newValue[0]
                            }
                            
                            if newValue.indices.contains(1){
                                if newValue[1] != "00" || newValue[1] != "0"{
                                    finalValue = newValue[0] + "." +  newValue[1] + " MMK"
                                }
                            }
                            
                            pdfDictionary["cashback"] = finalValue
                         //   self.cellPopulation(withImage: #imageLiteral(resourceName: "topup_cashback"), key: "Cashback", value: finalValue)
                            
                        }
                    }
                    */
                    
                   // pdfDictionary["cashback"]       = model.kickvalue.safelyWrappingString() + " " + "MMK"
                }
            }
            if let date = model.responsects {
                let dtStr = date
                if let dtVal = dtStr.dateValue(dateFormatIs: "dd-MMM-yyyy HH:mm:ss") {
                    let datStr = dtVal.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss")
                    pdfDictionary["transactionDate"] = datStr
                } else {
                    pdfDictionary["transactionDate"]    = self.getDisplayDate(date)
                }
            }
            pdfDictionary["logoName"]           = "appIcon_Ok"
        }
        guard let pdfUrl = CodeSnippets.createInvoicePDF(dictValues: pdfDictionary, pdfFile: "OK$ TopUP Transaction Receipt") else {
            println_debug("Error - pdf not generated")
            return
        }
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "SharePDFViewController") as? SharePDFViewController else { return }
        vc.url = pdfUrl
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func share() {
        if InternationalTopup == nil {
            self.keycashback.isHidden = true
            self.valueCashBack.isHidden = true
        }
        self.rateservice.isHidden = true
        self.keyBalance.isHidden = true
        self.valueBalance.isHidden = true
        self.keyCategories.isHidden = true
        self.valueCategories.isHidden = true
        self.starArray.forEach { (view) in
            view.isHidden = true
        }
        constraintTopTransID.constant = -68
        self.view.layoutIfNeeded()
        let snapImage = self.cardView.snapshotOfCustomeView
        constraintTopTransID.constant = 12
        self.view.layoutIfNeeded()
        let vc = UIActivityViewController(activityItems: [snapImage], applicationActivities: [])
        DispatchQueue.main.async {self.present(vc, animated: true)}
        
        if InternationalTopup == nil {
            if reciptModel!.kickvalue.safelyWrappingString().lowercased() != "Kickback rules not defined".lowercased() || reciptModel!.kickvalue.safelyWrappingString().lowercased() != "" || reciptModel!.kickvalue.safelyWrappingString().lowercased() != "Kick-Back Rules Not Define".lowercased() {
                self.keycashback.isHidden = true
                self.valueCashBack.isHidden = true
            } else {
                self.keycashback.isHidden = false
                self.valueCashBack.isHidden = false
            }
        }
        
        self.rateservice.isHidden = false
        self.keyBalance.isHidden = false
        self.valueBalance.isHidden = false
        self.keyCategories.isHidden = false
        self.valueCategories.isHidden = false
        
        self.starArray.forEach { (view) in
            view.isHidden = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool)  {
        super.viewDidAppear(true)
//        if device.type == .iPhoneX || device.type == .iPhoneXR || device.type == .iPhoneXS || device.type == .iPhoneXSMax || device.type == .iPhone11 {
//            bottomViewHeightConstraint.constant = 250
//        }else {
//            bottomViewHeightConstraint.constant = 90
//        }
        let image = captureScreen(view: self.cardView)
         UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)

    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            alertViewObj.wrapAlert(title: "Error".localized, body: error.localizedDescription, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        } else {
            println_debug("Your altered image has been saved to your photos.")
        }
    }
    
    private func screenShotMethod(_ view: UIView) -> UIImage? {
        //Create the UIImage
        UIGraphicsBeginImageContext(view.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            view.layer.render(in: context)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            if let imageFinal = image {
                UIImageWriteToSavedPhotosAlbum(imageFinal, nil, nil, nil)
            }
            return image
        }
        return nil
    }
    
    //MARK: - Button Action Methods
    //MARK: Rating Functionality
    @IBAction func openRateWindowAction(_ sender: UIButton) {
        if manualRate == 0 {
            guard let model = reciptModel else { return }
            guard let ratingScreen = self.storyboard?.instantiateViewController(withIdentifier: String(describing: PaymentRateScreenViewController.self)) as? PaymentRateScreenViewController else { return }
            ratingScreen.modalPresentationStyle = .overCurrentContext
            ratingScreen.delegate = self
            ratingScreen.destinationNumber = model.destination.safelyWrappingString()
//            ratingScreen.modalPresentationStyle = .fullScreen
            self.navigationController?.present(ratingScreen, animated: true, completion: nil)
        }
    }
}

//MARK:- TAB BAR DELEGATE
extension TopupSingleViewRecieptController: UITabBarDelegate {
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        tabBarAction = RecieptTabSelectionType(rawValue: item.tag)
        guard let action = tabBarAction else { return }
        guard let modelRec = reciptModel else { return }
        switch action {
        case .addFavorite:
            self.addFav(model: modelRec)
        case .addContact:
            self.addContacts(model: modelRec)
        case .home:
            self.goToHome()
        case .more:
            self.more()
        }
    }
}

//MARK: - CNContactViewControllerDelegate
extension TopupSingleViewRecieptController: CNContactViewControllerDelegate {
    func changeCancelImplementation() {
        let originalSelector = Selector(("editCancel:"))
        let swizzledSelector = #selector(CNContactViewController.cancelHack)
        if let originalMethod = class_getInstanceMethod(object_getClass(CNContactViewController()), originalSelector),
           let swizzledMethod = class_getInstanceMethod(object_getClass(CNContactViewController()), swizzledSelector) {
            method_exchangeImplementations(originalMethod, swizzledMethod)
        }
    }
    
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        if let newContact = contact {
            contactAdded = true
            addContactUI(isInContact: true)
            okContacts.append(newContact)
        }
        viewController.delegate = nil
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - MoreControllerActionDelegate
extension TopupSingleViewRecieptController: MoreControllerActionDelegate {
    func actionFor(option: MorePaymentAction) {
        switch option {
        case .morePayment:
            if (UserLogin.shared.walletBal as NSString).floatValue < 100.00 {
                alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                    if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                        addWithdrawView.modalPresentationStyle = .fullScreen
                        self.navigationController?.present(addWithdrawView, animated: true, completion: nil)
                    }
                })
                alertViewObj.showAlert()
                return
            }
            self.morePayment()
        case .repeatPayment:
            if (UserLogin.shared.walletBal as NSString).floatValue < 100.00 {
                alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                    if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                         addWithdrawView.modalPresentationStyle = .fullScreen
                        self.navigationController?.present(addWithdrawView, animated: true, completion: nil)
                    }
                })
                alertViewObj.showAlert()
                return
            }
            self.moreRepeatPayment()
        case .invoice:
            self.invoice()
        case .share:
            self.share()
        }
    }
}

//MARK: - PaymentRateDelegate
extension TopupSingleViewRecieptController: PaymentRateDelegate {
    func ratingShow(_ rate: Int, commentText: String?) {
        manualRate = 10
        DispatchQueue.main.async {
            for star in self.starArray {
                if star.tag <= rate {
                    UIView.animate(withDuration: 0.2, animations: {
                        star.image = #imageLiteral(resourceName: "receiptStarFilled")
                    })
                } else {
                    star.image = #imageLiteral(resourceName: "receiptStarUnfilled")
                }
            }
        }
    }
}


//MARK:- TabOptionProtocol
extension TopupSingleViewRecieptController: TabOptionProtocol {
    func addFavorite() {
        if let _ = viewOptions {
            if viewOptions!.lblAddFav.textColor != kYellowColor {
                guard let modelRec = reciptModel else { return }
                self.addFav(model: modelRec)
            } else {
                PaytoConstants.alert.showToast(msg: "Contact already added in Favorites".localized)
            }
        }
    }
    
    func addContact() {
        if let _ = viewOptions {
            if viewOptions!.lblAddContact.textColor != kYellowColor {
                guard let modelRec = reciptModel else { return }
                self.addContacts(model: modelRec)
            } else {
                PaytoConstants.alert.showToast(msg: "Contact Already Added".localized)
            }
        }
    }
    
    
    func goHome() {
        self.goToHome()
    }
    
    func showMore() {
        self.more()
    }
}


//MARK:- PTFavoriteActionDelegate
extension TopupSingleViewRecieptController: PTFavoriteActionDelegate {
    func favoriteAdded() {
        favoriteAddedCheck = true
        addFavUI(isInFav: true)
    }
}
