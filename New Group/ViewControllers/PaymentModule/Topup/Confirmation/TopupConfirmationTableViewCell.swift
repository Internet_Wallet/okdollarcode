//
//  TopupConfirmationTableViewCell.swift
//  OK
//
//  Created by Ashish on 1/29/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol TopupConfirmationTableViewCellDelegate : class {
    func changeHeight(cell: TopupConfirmationTableViewCell)
}

class TopupConfirmationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var keyLabel: UILabel!{
        didSet
        {
            keyLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    
    @IBOutlet weak var valueLabel: UILabel!{
        didSet
        {
            valueLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    
    @IBOutlet weak var imageKeyView: UIImageView!
    
    
    // constraints to hide for first cell
    @IBOutlet weak var keyLabelWidth: NSLayoutConstraint!
    @IBOutlet weak var imgViewWidth: NSLayoutConstraint!
    
    var headerCell : Bool = false {
        didSet {
            if headerCell {
                self.keyLabelWidth.constant = 0.00
                self.imgViewWidth.constant = 0.00
                self.keyLabel.isHidden = true
                self.imageKeyView.isHidden = true
            }
        }
    }
    
    var delegate : TopupConfirmationTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func wrapCellData(_ model: TopupConfirmationModel, withCellType type: TopupConfirmationCellType, havingHeader title: String) {
        switch type {
        case .name:
            self.cellPopulation(withImage: #imageLiteral(resourceName: "topup_name"), key: "Name", value: model.name)
        case .number:
            self.cellPopulation(withImage: #imageLiteral(resourceName: "topup_number"), key: "Number", value: model.paymentNumber)
        case .type:
            self.cellPopulation(withImage: #imageLiteral(resourceName: "topup_type"), key: "Type", value: model.type)
        case .cashback:
            if model.developerCashback == "Kick-Back Rules Not Define" {
                self.imageKeyView.isHidden = true
                self.keyLabel.isHidden = true
                self.valueLabel.isHidden = true
            }
            
            let cashback = model.developerCashback.components(separatedBy: " ")
            if cashback.indices.contains(0){
                let newValue = cashback[0].components(separatedBy: ".")
                if newValue.count>0{
                    var finalValue = ""
                    if newValue.indices.contains(0){
                        finalValue = newValue[0] + " MMK"
                    }
                    
                    if newValue.indices.contains(1){
                        if newValue[1] != "00"{
                            finalValue = newValue[0] + "." +  newValue[1] + " MMK"
                        }
                    }
                    
                    self.cellPopulation(withImage: #imageLiteral(resourceName: "topup_cashback"), key: "Cashback", value: finalValue)
                    
                }
            }
            
//            let cashback = (model.developerCashback as NSString).floatValue
//            let strCashback = String.init(format: "%.2f MMK", cashback)
//            self.cellPopulation(withImage: #imageLiteral(resourceName: "topup_cashback"), key: "Cashback", value: strCashback)
        case .amount:
            headerCell = false
            if model.type == RechargePlanType.recharge {
                self.cellPopulation(withImage: #imageLiteral(resourceName: "topup_amount"), key: "Top-Up Amount", value: model.developerAmount)
            } else if model.type == RechargePlanType.dataPlan {
                self.cellPopulation(withImage: #imageLiteral(resourceName: "topup_amount"), key: "Data Plan Amount", value: model.developerAmount)
            } else if model.type == RechargePlanType.specialOffers {
                self.cellPopulation(withImage: #imageLiteral(resourceName: "topup_amount"), key: "Special Offer Amount", value: model.developerAmount)
            }

        case .headerTitle:
            headerCell = true
            self.valueLabel.text = title
            self.backgroundColor = .clear
        case .bonusPoint:
            break
        }
    }
    
    fileprivate func cellPopulation(withImage img: UIImage, key: String, value: String) {
        self.imageKeyView.image = img
       // self.keyLabel.font = UIFont.init(name: self.keyLabel.font.familyName, size: 20.00)
        self.keyLabel.text = key.localized
        self.valueLabel.text = value
        
        if key == "Number" {
            self.valueLabel.text = self.wrapFormattedNumber(key: value)
        }
        
        if key.hasSuffix("Amount") {
            self.valueLabel.text = self.wrapAmount(key: value) + " " + "MMK"
        }
        
        if key == "CashBack" {
            
            let cash = value.replacingOccurrences(of: " MMK", with: "")
            
            self.valueLabel.text = self.wrapAmount(key: cash) + " " + "MMK"
        }
        
        println_debug((key, value))
    }
    
    fileprivate func wrapAmount(key: String) -> String {
        let number = NSDecimalNumber(string: key)
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 0

        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        if let num = formatter.string(from: number) {
            return (num == "NaN") ? "" : num
        }
        return ""
    }
    
    fileprivate func wrapFormattedNumber(key: String) -> String {
        var agentCode = key
        if agentCode.count > 2 {
            agentCode.remove(at: String.Index.init(encodedOffset: 0))
            agentCode.remove(at: String.Index.init(encodedOffset: 0))
            agentCode.insert("+", at: String.Index.init(encodedOffset: 0))
            
            let countryDetails = identifyCountry(withPhoneNumber: agentCode)
            
            if agentCode.hasPrefix(countryDetails.countryCode) {
                
                agentCode = agentCode.replacingOccurrences(of: countryDetails.countryCode, with: "0")
                return agentCode
                
            } else {
                return ""
            }
            
        } else {
            return ""
        }
    }



}
