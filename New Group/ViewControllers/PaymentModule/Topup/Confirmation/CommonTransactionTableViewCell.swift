//
//  CommonTransactionTableViewCell.swift
//  OK
//
//  Created by Ashish on 5/22/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct MultiTopupModel {
    var name = ""
    var number = ""
    var type = ""
    var cashback = ""
    var amount = ""
    var planType = ""
    
    init(name: String, number: String, type: String, cashback: String, amount: String, plan: String) {
        self.name   = name
        self.number = number
        self.type = type
        self.cashback = cashback
        self.amount = amount
        self.planType = plan
    }
}

struct MultiTopupConfirmationArray {
    static let mode = MultiTopupConfirmationArray()
    var modelArray = [MultiTopupModel]()
    
    mutating func wrapModel(_ mode: [MultiTopupModel]) {
        self.modelArray = mode
    }
}

class CommonTransactionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var keyImage: UIImageView!
    @IBOutlet weak var keyLabel: UILabel!{
        didSet
        {
            keyLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var keyValue: UILabel!{
        didSet
        {
            keyValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    func wrapLoyaltyCell(_ key: String, value: String, img: String) {
        var imageName = ""
        if key == "Name" {
            imageName = "topup_name"
        } else if key == "Number" {
            imageName = "topup_number"
        } else if key == "Type" {
            imageName = "topup_type"
        } else if key == "Top-Up Amount" {
            imageName = "topup_amount"
        } else if key == "Redeem Points" {
            imageName = "points"
        }
        
        self.keyImage.image = UIImage.init(named: imageName)
        self.keyLabel.text = key.localized
        
        if key == "Top-Up Amount" {
            self.keyValue.text = self.wrapAmount(key: value) + " " + "MMK"
        } else if key == "Redeem Points" {
            self.keyValue.text = self.wrapAmount(key: value) + " " + "PTS"
        } else {
            self.keyValue.text = value
        }
        
        
    }
    
    func wrapCell(_ model: TopupConfirmationModel, header: String, withIndex index: Int) {
        
        switch index {
        case 0:
            self.keyImage.isHidden = true
            self.keyValue.isHidden = true
            self.keyLabel.text = header
          //  self.keyLabel.font = UIFont.init(name: self.keyLabel.font.familyName, size: 20.00)
        case 1:
            self.keyImage.image = UIImage.init(named: "topup_name")
            self.keyLabel.text = "Name".localized
            self.keyValue.text = model.name
        case 2:
            self.keyImage.image = UIImage.init(named: "topup_number")
            self.keyLabel.text = "Number".localized
            self.keyValue.text = model.paymentNumber
        case 3:
            self.keyImage.image = UIImage.init(named: "topup_type")
            self.keyLabel.text = "Type".localized
            
            self.keyValue.text = (model.type == "Top-Up Plan") ? "Top-Up Amount" :  model.type
        case 4:
            self.keyImage.image = UIImage.init(named: "rechargeTower")
            self.keyLabel.text = "Operator".localized
            self.keyValue.text = PayToValidations().getNumberRangeValidation(model.paymentNumber).operator
        case 5:
            self.keyImage.image = UIImage.init(named: "topup_cashback")
           // let cashbackDisplay =  model.cashback // self.wrapAmount(key: model.developerCashback)
            let cashbackDisplay = self.wrapAmount(key: model.developerCashback) + " " + "MMK"
            self.keyLabel.text = "Cashback".localized
            self.keyValue.text = cashbackDisplay
        case 6:
            self.keyImage.image = UIImage.init(named: "topup_amount")
            if model.type == "Data Plan" {
                self.keyLabel.text = "Data Plan Amount".localized
            }
            else if model.type == "Special Offers"{
                self.keyLabel.text = "Special Offer Amount".localized
            }
            else{
            self.keyLabel.text = "Topup Amount".localized
            }
           // let amountDisplay = model.topupAmount
            let amountDisplay =  self.wrapAmount(key: model.developerAmount) + " " + "MMK"
            self.keyValue.text = amountDisplay 
        default:
            break
        }
        
    }
    
    fileprivate func wrapAmount(key: String) -> String {
        let number = NSDecimalNumber(string: key)
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2

        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        if let num = formatter.string(from: number) {
            if num.contains(find: ".") {
                return (num == "NaN") ? "" : num.replacingOccurrences(of: ".00", with: "")
            } else {
                //num = num + ".00"
                return (num.contains(find: "NaN")) ? "" : num
            }
        }
        return ""
    }


}

class InternationalTransactionTableViewCell: UITableViewCell {
    @IBOutlet weak var mobileNumber: UILabel!{
        didSet
        {
            mobileNumber.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var operatorName: UILabel!{
        didSet
        {
            operatorName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var buyIn: UILabel!{
        didSet
        {
            buyIn.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var paidIn: UILabel!{
        didSet
        {
            paidIn.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var keyBuyIn: UILabel!{
        didSet
        {
            keyBuyIn.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var keyPaidIn: UILabel! {
        didSet {
            keyPaidIn.font = UIFont(name: appFont, size: appFontSize)
            self.keyPaidIn.text = "Paid in MMK".localized
        }
    }
    
    @IBOutlet weak var keyMobile: UILabel! {
        didSet {
            self.keyMobile.font = UIFont(name: appFont, size: appFontSize)
            self.keyMobile.text = "Mobile Number".localized
        }
    }
    @IBOutlet weak var keyOperator: UILabel! {
        didSet {
            self.keyOperator.font = UIFont(name: appFont, size: appFontSize)
            self.keyOperator.text = "Operator".localized
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func wrapCell(dict: Dictionary<String,Any>, extra: String) {
        self.mobileNumber.text = InternationalTopupManager.selectedCountry.code + " " +  dict.safeValueForKey("userNumber").safelyWrappingString()
        
        guard let operatorCountryModel = InternationalTopupManager.selectedCountryObject else { return  }
        guard let currency = operatorCountryModel.currencyDetail else  { return }

        self.keyBuyIn.text = "Buy In".localized + " " + currency.currencyType.safelyWrappingString()
        self.operatorName.text = ""
        if let opName = InternationalTopupManager.selectedOperatorObject?.operatorName.safelyWrappingString() {
            self.operatorName.text = opName
        }
       // self.buyIn.text = extra
        
        let am = (extra as NSString).intValue
        let amountIN = NSNumber.init(value: am)
        
        let amountFormatted = dict.safeValueForKey("amount").safelyWrappingString()
        
        let amt = (amountFormatted as NSString).intValue
        let nsAmount = NSNumber.init(value: amt)
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NumberFormatter.Style.decimal
        if let amIn  = currencyFormatter.string(from: amountIN) {
            self.buyIn.text = amIn
        } else {
            self.buyIn.text = extra
        }
        if let amtnt  = currencyFormatter.string(from: nsAmount) {
            self.paidIn.text = amtnt + " " + "MMK"
        } else {
            self.paidIn.text = dict.safeValueForKey("amount").safelyWrappingString() + " " + "MMK"
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
