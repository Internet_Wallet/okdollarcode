//
//  CommonTopupTransactionViewController.swift
//  OK
//
//  Created by Ashish on 5/22/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol CommonTopupTransactionViewControllerDelegate : class {
    func didSelectBackOptionFromConfromation()
    func didSelectBackOptionFromSingleConfirmation()
    func didSelectBackOption(type: String)
}

class CommonTopupTransactionViewController: OKBaseController, UITableViewDelegate, UITableViewDataSource, BioMetricLoginDelegate {
    
    var isInternationl : Bool = false
    
    var isLoyalty : Bool = false
    
    var confirmModel : BuyRedeemPointsModel?
    
    var isInternationalMobileNumber : String?
    
    var loyaltyTopupName : String? = "Unknown"
    
    var delegate : CommonTopupTransactionViewControllerDelegate?
    
    var timer : Timer?
    
    var seconds : Int = 0

    var cashBackDestNum: String?
    var isComingFromRecent = false
    
    var keys : [String]?
    var values : [String]?
    var displayNumber : String?
    var totalBonusAmount : String?
    
    var objViewModel = FailureViewModel()
    var arrDictionary : [Dictionary<String,Any>] = [Dictionary<String,Any>]()
    
    let manager = GeoLocationManager.shared
    
    var currency: String?
    
    var strnumber = ""
    
    var fromPendingApproval = ""
    
    var modelArray : [TopupConfirmationModel] = [TopupConfirmationModel]()
    
    @IBOutlet weak var payAction: UIButton!{
        didSet{
            payAction.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    @IBOutlet weak var tableViewConfirmation: UITableView!
    
    var cellArray = [Int:[CommonTransactionTableViewCell]]()
    
    var cashBackMerchantNumber = [Dictionary<String,Any>]()
    
    var loyaltyDictionary : Dictionary<String,Any>?
    
    var failureViewModelObj = FailureViewModel()
    
    var nameOfPerson = ""
    
    
    @IBOutlet weak var adImageView: UIImageView!
    
    //prabu
    var requestMoney : Bool = false
    var displayResponse = [Dictionary<String,Any>]()

    var Rechangetype = "Top Up"
    var RechargeNumber = ""
    var payCount = 0
        
    var failureCount = 0

    override func viewDidLoad() {
        
        super.viewDidLoad()
        print("common Topup displayResponse===\(displayResponse)")

        geoLocManager.startUpdateLocation()
        
        self.tableViewConfirmation.tableFooterView = UIView.init()
        
        self.payAction.backgroundColor = MyNumberTopup.theme
        
        self.backButton(withTitle: "Confirmation".localized)
        if fromPendingApproval == "pendingRequestMoney" {
            println_debug(strnumber)
            let number = PayToValidations().getNumberRangeValidation(strnumber)
            let colorChange = PayToValidations().getColorForOperator(operatorName: number.operator)
            
            self.navigationController?.navigationBar.backgroundColor = colorChange
            self.navigationController?.navigationBar.barTintColor = colorChange
        }
        
        self.initiateTimer()
        
        if isInternationl {
            self.adImageView.isHidden = true
            self.payAction.setTitle("TopUpPay".localized, for: .normal)
            self.payAction.backgroundColor = self.navigationController?.navigationBar.barTintColor
             self.tableViewConfirmation.reloadData()
        } else if isLoyalty {
            self.adImageView.isHidden = false
            guard let model = confirmModel else { return }
            keys   = ["Name","Number", "Type", "Top-Up Amount", "Redeem Points"]
            values = valuesWrap(model)
            self.payAction.setTitle("TopUpPay".localized, for: .normal)
            self.tableViewConfirmation.reloadData()
            let randomIndex = Int(arc4random_uniform(UInt32(advImagesUrl.AdImageData.count)))
            if let urlString = advImagesUrl.AdImageData[safe: randomIndex] {
                DispatchQueue.main.async {
                    self.adImageView.image = UIImage(data: urlString)
                    self.adImageView.contentMode = .scaleAspectFit
                    //                self.adImageView.setImage(url: urlString, contentMode: UIView.ContentMode.scaleAspectFit, placeholderImage: nil)
                }
            }

        }
        else {
            self.wrapCell()
            self.payAction.setTitle("TopUpPay".localized, for: .normal)
            if self.modelArray.count > 1 {
                self.adImageView.isHidden = true
            } else {
                let randomIndex = Int(arc4random_uniform(UInt32(advImagesUrl.AdImageData.count)))
                if let urlString = advImagesUrl.AdImageData[safe: randomIndex] {
                    DispatchQueue.main.async {
                        self.adImageView.image = UIImage(data: urlString)
                        self.adImageView.contentMode = .scaleAspectFit
                        //                self.adImageView.setImage(url: urlString, contentMode: UIView.ContentMode.scaleAspectFit, placeholderImage: nil)
                    }
                }
            }
           
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        failureCount = 0
        self.payAction.backgroundColor = self.navigationController?.navigationBar.barTintColor
    }
    
    func initiateTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(CommonTopupTransactionViewController.updateTimer)), userInfo: nil, repeats: true)
    }

    
    func valuesWrap(_ model: BuyRedeemPointsModel) -> [String] {
        
        guard let buyingPrice = model.buyingPrice else { return [String]() }
        guard let bonusPoints = model.redeemPoints else { return [String]()}
        guard let expiresOn   = model.expiryDate else { return [String]()}
        
        var valuesArray = [String]()
        
        let stringBuyingPrice = String.init(format: "%d", buyingPrice)
        let stringBonusPoints = String.init(format: "%d", bonusPoints)
        
        valuesArray.append((loyaltyTopupName.safelyWrappingString() != "") ? loyaltyTopupName.safelyWrappingString() : "Unknown" )
        valuesArray.append(displayNumber.safelyWrappingString())
        valuesArray.append("Bonus Point Topup")
        valuesArray.append(stringBuyingPrice)
        valuesArray.append(stringBonusPoints)
        
        if let name = model.merchantName, name.lowercased() == "OK$ TopUp Point".lowercased() {
            valuesArray.append("Unlimited")
        } else {
            let modifiedDate = self.getDateAndTime(timeStr: expiresOn)
            valuesArray.append(modifiedDate)
        }
        
        return valuesArray
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.timer?.invalidate()
    }

    @objc fileprivate func updateTimer() {
        
        self.seconds = self.seconds + 1
        
        if self.seconds == 300 {
            self.seconds = 0
            timer?.invalidate()
            alertViewObj.wrapAlert(title: nil, body: "Session expired".localized, img: #imageLiteral(resourceName: "dashboard_my_number"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                //self.navigationController?.popViewController(animated: true)
                 OKPayment.main.authenticate(screenName: "topup", delegate: self)
            })
            alertViewObj.showAlert(controller: self)
        }
    }

    
    private func getDateAndTime(timeStr: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        guard let time = dateFormatter.date(from: timeStr) else { return "" }
        dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss"//hh:mm a"
        let dateAndTime = dateFormatter.string(from: time)
        return dateAndTime
    }

    
    func wrapCell() -> Void  {
        if self.modelArray.count > 0 {
            for section in 0 ... self.modelArray.count - 1 {
                var dictionaryCell = [CommonTransactionTableViewCell]()
                for row in 0 ... 6 {
                    let cell = tableViewConfirmation.dequeueReusableCell(withIdentifier: "CommonTransactionTableViewCell") as? CommonTransactionTableViewCell
                    let header = String.init(format: "Recharge %d", section + 1)
                    cell?.wrapCell(modelArray[section], header: header, withIndex: row)
                    dictionaryCell.append(cell!)
                }
                cellArray[section] = dictionaryCell
            }
            self.tableViewConfirmation.reloadData()
        }
    }

    
   private func backButton(withTitle title:String){
    
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "tabBarBack"), for: UIControl.State.normal)
        
        button.tintColor = UIColor.white
    
        button.sizeToFit()
        button.addTarget(self, action: #selector(CommonTopupTransactionViewController.backButtonCustomActions), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        
        self.navigationItem.title = title
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    @objc
    func backButtonCustomActions(){
        if MultiTopupArray.main.controllers.count < 2, MultiTopupArray.main.controllers.count > 0 {
            self.delegate?.didSelectBackOptionFromConfromation()
            self.delegate?.didSelectBackOptionFromSingleConfirmation()
            
            if let model = modelArray.first {
                if model.type == "Special Offers" {
                    self.delegate?.didSelectBackOption(type: "2")
                } else if model.type == "Data Plan" {
                    self.delegate?.didSelectBackOption(type: "1")
                } else {
                    self.delegate?.didSelectBackOption(type: "0")
                }
            }
            
            
        }
        if fromPendingApproval == "pendingRequestMoney"
        {
        self.navigationController?.navigationBar.backgroundColor = kYellowColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
        }
        else
        {
            //
        }
        self.navigationController?.popViewController(animated: true)
    }


    //MARK:- TableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        if isInternationl {
            return 1
        } else if isLoyalty {
            return 1
        }
        return modelArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isInternationl {
            return arrDictionary.count
        }
        
        if isLoyalty {
            return (keys != nil) ? keys!.count : 0
        }
        
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isInternationl {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InternationalTransactionTableViewCell") as? InternationalTransactionTableViewCell
            cell?.wrapCell(dict: arrDictionary[indexPath.row], extra: currency ?? "")
            return cell!
        } else if isLoyalty {
            let cell = tableViewConfirmation.dequeueReusableCell(withIdentifier: "CommonTransactionTableViewCell") as? CommonTransactionTableViewCell
            cell?.wrapLoyaltyCell(self.keys![indexPath.row], value: self.values![indexPath.row], img: "")
            return cell!
        } else {
            let cell = cellArray[indexPath.section]
            let rowCell = cell![indexPath.row]
            return rowCell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if isInternationl {
            return 10.00
        } else {
            return 10.00
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if isLoyalty {
            return 50.00
        }
        
        if !isInternationl {
            let model = modelArray[indexPath.section]
            
            if indexPath.row == 0, modelArray.count <= 1 {
                return 0.00
            }
            
            if indexPath.row == 5 {
                if model.developerCashback  ==  "Kick-Back Rules Not Define" {
                    return 0.00
                }
            }
            
            return 50
        }
        
//        if let height = InternationalTopupManager.selectedOperatorObject?.operatorName.safelyWrappingString().heightWithConstrainedWidth(width: (screenWidth - 155), font: UIFont.init(name: "Zawgyi-One", size: 15.0) ?? UIFont.systemFont(ofSize: 15.00)) {
//            return  105 + height + 5.00
//        }
        tableView.estimatedRowHeight = 450
        tableView.rowHeight = UITableView.automaticDimension
        return tableView.rowHeight
        //return 140.00
        
    }
    
    func topupPaymentSelling() {
        
        let urlString = String.init(format: "%@", Url.getSellingTopup)
        let url = getUrl(urlStr: urlString, serverType: .serverApp)
        let params = self.JSONStringFromAnyObject(value: getAvailableParamsForSelling() as AnyObject)
        
        DispatchQueue.main.async {
            TopupWeb.genericClass(url: url, param: params as AnyObject, httpMethod: "POST") { [weak self](response, success) in
                if success {
                    if let json = response as? Dictionary<String,Any> {
                        if let code = json["Code"] as? NSNumber, code == 200 {
                            if var data = json["Data"] as? String {
                                data = data.replacingOccurrences(of: "\"", with: "")
                                let xmlString = SWXMLHash.parse(data)
                                self?.transaction.removeAll()
                                self?.enumerate(indexer: xmlString)
                                if self?.transaction.safeValueForKey("resultdescription") as? String == "Transaction Successful" {
                                            DispatchQueue.main.async {
                                                guard let vc = self?.storyboard?.instantiateViewController(withIdentifier: "LoyaltyTopupReceiptSingleViewController") as? LoyaltyTopupReceiptSingleViewController else { return }
                                                vc.dictionary = self?.transaction
                                                vc.confirmModel = self?.confirmModel
                                                vc.displayNumber = self?.displayNumber
                                                vc.totalBonusAmount = self?.totalBonusAmount
                                                vc.cashBackDestNum = self?.cashBackDestNum
                                                self?.navigationController?.pushViewController(vc, animated: true)
                                            }
                                } else {
                                    self?.showErrorAlert(errMessage: self?.transaction.safeValueForKey("resultdescription") as? String ?? "")
                                }
                            }
                        } else {
                            if let msg = json["Msg"] as? String {
                                self?.showErrorAlert(errMessage: msg)
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    func getAvailableParamsForSelling() -> Dictionary<String,Any> {
        
        guard let model = self.confirmModel else { return Dictionary<String,Any>() }
        
        let amountBeneficiary  = String.init(format: "%d", model.buyingPrice ?? 0)
        
        var finalParams = Dictionary<String,Any>()
        
        var loginInfo = Dictionary<String,Any>()
        loginInfo["AppId"] = LoginParams.setUniqueIDLogin()
        loginInfo["Limit"]  = 0
        loginInfo["MobileNumber"]  = UserModel.shared.mobileNo
        loginInfo["Msid"] = getMsid()
        loginInfo["Offset"] = 0
        loginInfo["Ostype"] = 1
        loginInfo["Otp"] = ""
        loginInfo["Simid"] = simid
        
        var redeemSelling = Dictionary<String, Any>()
        
        var bonus = totalBonusAmount.safelyWrappingString()
        bonus = bonus.replacingOccurrences(of: ",", with: "")
        bonus = bonus.replacingOccurrences(of: " ", with: "")
        bonus = bonus.replacingOccurrences(of: "PTS", with: "")
        
        redeemSelling["AvialableBonusPoints"] = bonus
        redeemSelling["BeneficiaryAmount"]  =  amountBeneficiary
        
        redeemSelling["BeneficiaryNumber"]  = displayNumber.safelyWrappingString()
        redeemSelling["BonusPointName"]    = model.merchantName.safelyWrappingString()
        redeemSelling["BonusPointSourceNumber"] = model.merchantMobileNumber.safelyWrappingString()
        redeemSelling["Comments"] = ""
        redeemSelling["Destination"] = model.merchantMobileNumber.safelyWrappingString()
        redeemSelling["Password"] = ok_password ?? ""
        redeemSelling["QtyOfRedeemPoints"] = model.redeemPoints ?? 0
        redeemSelling["SecureToken"] = UserLogin.shared.token
        redeemSelling["Source"] = UserModel.shared.mobileNo
        if model.telcoName.safelyWrappingString().lowercased() == "Mytel".lowercased() {
            redeemSelling["TelcoName"] = "MyTel"
        } else {
            redeemSelling["TelcoName"] = model.telcoName.safelyWrappingString()
        }
        redeemSelling["TransactionType"] = "TOPUP"
        
        finalParams["LoginInfo"] = loginInfo
        finalParams["RedeempointsSellingDto"] = redeemSelling
        
        return finalParams
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful, screen.lowercased() == "loyaltyScreen".lowercased() {
            self.topupPaymentSelling()
        }
        
        if isSuccessful, screen.lowercased() == "otherNumberPay".lowercased() {
            self.othernumberPay()
        }
        
        if isSuccessful, screen.lowercased() == "internationalPay".lowercased() {
            self.internationalPay()
        }
    }


    
    @IBAction func payBtnAction(_ sender: UIButton) {
        println_debug("payBtnAction")
        guard appDelegate.checkNetworkAvail() else {
            PaytoConstants.alert.showToast(msg: PaytoConstants.messages.noInternet.localized)
            return
        }

        if isLoyalty {
            if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "loyaltyScreen", delegate: self)
            } else {
                self.topupPaymentSelling()
            }
            return
        }
        
        if isInternationl {
            if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "internationalPay", delegate: self)
            } else {
                self.internationalPay()
            }
        } else {
            if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "otherNumberPay", delegate: self)
            } else {
                self.othernumberPay()
            }
        }
    }
    
    var transaction = Dictionary<String,Any>()
    
    
    func internationalPay() {
        
    let param =  self.generatePaymentForSingleTopup()
    
        guard let url = URL.init(string: "https://www.okdollar.co/RestService.svc/GenericPayment") else { return }
        
        TopupWeb.genericClass(url: url, param: param as AnyObject, httpMethod: "POST") { [weak self](response, success) in
            if success {
                print("internationalPay response=====\(response)")
                if let json = response as? Dictionary<String,Any> {
                    if let code = json["Code"] as? NSNumber, code == 200 {
                        if let data = json["Data"] as? String {
                            let xmlString = SWXMLHash.parse(data)
                            self?.enumerate(indexer: xmlString)
                            if self?.transaction.safeValueForKey("resultdescription") as? String == "Transaction Successful" {
                                if let jsonData = self?.transaction.jsonString().data(using: .utf8) {
                                    if let model = try? JSONDecoder().decode(PaymentFinalRecieptModel.self, from: jsonData) {
                                        DispatchQueue.main.async {
                                            guard let vc = self?.storyboard?.instantiateViewController(withIdentifier: "TopupSingleViewRecieptController") as? TopupSingleViewRecieptController else { return }
                                            vc.reciptModel = model
                                            vc.currencyInt = self?.currency
                                            if self?.modelArray.count ?? 0>0{
                                                vc.nameOfPerson = self?.modelArray[0].name ?? ""
                                            }
                                            vc.InternationalTopup = self?.arrDictionary.first?.safeValueForKey("internationalNumber") as? String ?? ""
                                            if let _ = self?.isInternationl {
                                                vc.receiverName = InternationalTopupManager.receiverName
                                            }
                                            vc.planType =  "Overseas Top-Up Plan"
                                            self?.navigationController?.pushViewController(vc, animated: true)
                                        }
                                    }
                                }
                            } else {
                                self?.showErrorAlert(errMessage: self?.transaction.safeValueForKey("resultdescription") as? String ?? "Please Try Again".localized)
                            }
                        }
                        appDelegate.logEventWithAnaylitics(eventName: k_Payment, EventType: k_OVERSEASE, WithUser: UserModel.shared.mobileNo, AndEventStatus: k_TransactionSuccess)
                    } else {
                        if let msg = json["Msg"] as? String {
                            self?.showErrorAlert(errMessage: msg)
                        }
                        appDelegate.logEventWithAnaylitics(eventName: k_Payment, EventType: k_OVERSEASE, WithUser: UserModel.shared.mobileNo, AndEventStatus: k_TransactionFailure)
                    }
                }
            }
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            transaction[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }

    
    func othernumberPay() {
        if payCount == 0 {
            payCount = payCount + 1
        }else {
            println_debug("payCount in Other = \(payCount)")
            return
        }
        var operatorName = ""
        if modelArray.count>0{
            if modelArray.indices.contains(0) {
                operatorName = modelArray[0].operatorName.lowercased()
            }
        }
        
        
        if modelArray.count == 1 && operatorName != "MecTel".lowercased(){
            let stringUrl = TopupUrl.otherNumberSinglePayment
            guard let url = URL.init(string: stringUrl) else { return }
            let par = self.generatePaymentForSingleTopupOther()
            println_debug("Check Agent url : \(url)")
            println_debug(String(data: try! JSONSerialization.data(withJSONObject: par, options: .prettyPrinted), encoding: .utf8 )!)
            DispatchQueue.main.async {
                TopupWeb.genericClass(url: url, param: par as AnyObject, httpMethod: "POST") { [weak self](response, success) in
                    if success {
                        self?.payCount = 0
                        if let json = response as? Dictionary<String,Any> {
                            println_debug("topup result : \(json)")
                            if let code = json["Code"] as? NSNumber, code == 200 {
                                if let data = json["Data"] as? String {
                                    let xmlString = SWXMLHash.parse(data)
                                    self?.enumerate(indexer: xmlString)
                                    if self?.transaction.safeValueForKey("resultdescription") as? String == "Transaction Successful" {
                                        if let jsonData = self?.transaction.jsonString().data(using: .utf8) {
                                            if let model = try? JSONDecoder().decode(PaymentFinalRecieptModel.self, from: jsonData) {
                                                DispatchQueue.main.async {
                                                    guard let vc = self?.storyboard?.instantiateViewController(withIdentifier: "TopupSingleViewRecieptController") as? TopupSingleViewRecieptController else { return }
                                                    vc.reciptModel = model
                                                    vc.categoryValue = RecipetPlanCases(rawValue: "Other Number")
                                                    vc.planType = self?.Rechangetype
                                                    if self?.modelArray.count ?? 0>0{
                                                        vc.nameOfPerson = self?.modelArray[0].name ?? ""
                                                    }
                                                   // vc.nameOfPerson = self?.nameOfPerson ?? ""
                                                    vc.rechangeNumber = self?.RechargeNumber
                                                    if let value = self{
                                                        vc.isComingFromRecent = value.isComingFromRecent
                                                    }
                                                    self?.navigationController?.pushViewController(vc, animated: true)
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                               
                                if appDelegate.checkNetworkAvail() {
                                    progressViewObj.showProgressView()
                                    self?.failureCount = self?.failureCount ?? 0 + 1
                                    if self?.failureCount == 2 {
                                    self?.objViewModel.sendDataToFailureApi(request: par as Any,response: json as Any,type: FailureHelper.FailureType.TOPUP.rawValue,finished:{
                                        progressViewObj.removeProgressView()
                                    })
                                    }
                                } else {
                                    PaytoConstants.alert.showToast(msg: PaytoConstants.messages.noInternet.localized)
                                    progressViewObj.removeProgressView()
                                }

                                if let msg = json["Msg"] as? String {
                                    self?.showErrorAlert(errMessage: msg)
                                }
                            }
                        }
                        if let model = self?.modelArray.first {
                            appDelegate.logEventWithAnaylitics(eventName: k_Payment, EventType: model.operatorName, WithUser: UserModel.shared.mobileNo, AndEventStatus: k_TransactionSuccess)
                        }
                    } else {
                        
                        self?.payCount = 0
                        if let model = self?.modelArray.first {
                            appDelegate.logEventWithAnaylitics(eventName: k_Payment, EventType: model.operatorName, WithUser: UserModel.shared.mobileNo, AndEventStatus: k_TransactionFailure)
                        }                    }
                }
            }
           
        }else {
            let stringUrl  = TopupUrl.otherNumberMultiPayment
            guard let url = URL.init(string: stringUrl) else {return}
            let params = self.JSONStringFromAnyObject(value: getOtherNumberParams() as AnyObject)
            print("othernumberPay url====\(url)")
            print("othernumberPay params====\(params)")
            
            TopupWeb.genericClass(url: url, param: params as AnyObject, httpMethod: "POST") { [weak self] (response, success) in
                if success {
                    self?.payCount = 0
                    DispatchQueue.main.async {
                        self?.parseMultiPaymentResult(data: response)
                    }
                }else {
                    self?.payCount = 0
                }
            }
        }
    }
    
    func parseMultiPaymentResult(data : Any) {
        
        var modelArr = [OtherNumberResponse]()
        print("parseMultiPaymentResult data====\(data)")

        if let dict = data as? Array<Dictionary<String,Any>> {
            for (index,dictionaries) in dict.enumerated() {
                if let xmlString = dictionaries["Data"] as? String {
                    let parserString = SWXMLHash.parse(xmlString)
                    transaction.removeAll()
                    self.enumerate(indexer: parserString)
                    if transaction["resultdescription"] as? String == "Transaction Successful" {
                        
                        let model = OtherNumberResponse.init(fromDictionary: self.transaction, model: self.modelArray[index])
                        modelArr.append(model)
                        appDelegate.logEventWithAnaylitics(eventName: k_Payment, EventType: model.operatorName, WithUser: UserModel.shared.mobileNo, AndEventStatus: k_TransactionSuccess)
                        
                    } else {
                        self.showErrorAlert(errMessage: transaction.safeValueForKey("resultdescription") as? String ?? "Please Try again".localized)
                        appDelegate.logEventWithAnaylitics(eventName: k_Payment, EventType: modelArray[index].operatorName, WithUser: UserModel.shared.mobileNo, AndEventStatus: k_TransactionFailure)
                    }
                }
            }
            if modelArr.count > 0 {
                //RequestMoney Status change
                DispatchQueue.global(qos: .background).async {
                    if self.requestMoney {
                        self.updateRequestMoneyStatus()
                    }
                }
                
                DispatchQueue.main.async {
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtherNumberReceiptViewController") as? OtherNumberReceiptViewController else { return }
                    vc.nameToDisplay = self.modelArray.compactMap({$0.name})
                vc.modelResponse = modelArr
                vc.cashBackDestNum = self.cashBackDestNum
                vc.fromPendingApproval = self.fromPendingApproval
                vc.isComingFromRecent = self.isComingFromRecent
                self.navigationController?.pushViewController(vc, animated: true)
                }
                
            } else {
//               self.showErrorAlert(errMessage: "Please Try Again".localized)
            }
        }

    }
    
    //MARK:- Topup Stauts Change
    func updateRequestMoneyStatus()
    {
        var requestId : String?
        if let dic = self.displayResponse.first {
            requestId = dic.safeValueForKey("requestId") as? String ?? ""
        }
        let urlStr   = Url.changeStatusOfRequest
        let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
        var urlComponents = URLComponents.init(url: ur, resolvingAgainstBaseURL: false)
        let userData = UserModel.shared
        // add params
        
       // print("updateRequestMoneyStatus-----\(apiForCancelRequest)")

        
        let mobileNUmberQuery       = URLQueryItem(name:"MobileNumber",value : userData.mobileNo)
        let simIdQuery              = URLQueryItem(name:"Simid",value :userData.simID)
        let msIDQuery               = URLQueryItem(name:"MSID" , value:userData.msid)
        let osTypeQuery             = URLQueryItem(name:"OSType", value: userData.osType)
        let otpQuery                = URLQueryItem(name:"OTP", value: userData.simID)
        let requestIDQuery          = URLQueryItem(name:"RequestId", value: "\(requestId ?? "")")
        let statusQuery             = URLQueryItem(name:"Status", value: "Accepted")
        
        urlComponents?.queryItems  = [mobileNUmberQuery,simIdQuery,msIDQuery,osTypeQuery,otpQuery,requestIDQuery,statusQuery]
        guard let apiForCancelRequest = urlComponents?.url else { return }
        
        
       // println_debug("api \(apiForCancelRequest.absoluteString) ")
        //print("updateRequestMoneyStatus-----\(apiForCancelRequest)")

        
        JSONParser.GetCancelApiResponse(apiUrl: apiForCancelRequest, type: "GET") { (isSuccess : Bool, data : Any?) in
            
            //print("updateRequestMoneyStatusdata-----\(data)")

            
            if isSuccess == true {
                
            } else {
                ///
                if let message = data as? String {
                    
                    DispatchQueue.main.async(execute: {
                        
                        alertViewObj.wrapAlert(title: "", body: message.localized, img: UIImage(named : "AppIcon"))
                        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                            
                        })
                        alertViewObj.showAlert(controller: self)
                    })
                }
            }
        }
        
    }
   

    //MARK:- Parameters

    func getOtherNumberParams() -> [Dictionary<String,Any>] {
        
        var array = [Dictionary<String,Any>]()
        
        for (index,_) in self.modelArray.enumerated() {
            // Transaction cell tower
            var TransactionsCellTower  = Dictionary<String,Any>()
            TransactionsCellTower["Lac"] = ""
            TransactionsCellTower["Mcc"] = mccStatus.mcc
            TransactionsCellTower["Mnc"] = mccStatus.mnc
            TransactionsCellTower["SignalStrength"] = ""
            TransactionsCellTower["NetworkType"] = UitilityClass.getNetworkType()
            TransactionsCellTower["NetworkSpeed"] = UitilityClass.getSignalStrength()
            
            if UitilityClass.isConnectedToWifi(){
                let wifiInfo = UitilityClass.getNetworkInfos()[0].components(separatedBy: ",")
                TransactionsCellTower["ConnectedwifiName"] = wifiInfo[0]
                TransactionsCellTower["Ssid"] = wifiInfo[0]
                TransactionsCellTower["Mac"] = wifiInfo[1]
            }else{
                TransactionsCellTower["ConnectedwifiName"] = ""
                TransactionsCellTower["Ssid"] = ""
                TransactionsCellTower["Mac"] = ""
            }
            
            // l External Reference
            var lExternalReference = Dictionary<String,Any>()
            lExternalReference["Direction"] = true
            lExternalReference["EndStation"] = ""
            lExternalReference["StartStation"] = ""
            lExternalReference["VendorID"] = ""
            
            // L geo Location
            var lGeoLocation = Dictionary<String,Any>()
            lGeoLocation["CellID"] = ""
            lGeoLocation["Latitude"] = manager.currentLatitude
            lGeoLocation["Longitude"] = manager.currentLongitude
            
            // L Proximity
            var lProximity = Dictionary<String,Any>()
            lProximity["BlueToothUsers"] = [Any]()
            lProximity["SelectionMode"] = false
            lProximity["WifiUsers"] = [Any]()
            
            // Final payment Dictionary
            let model = modelArray[index]
            var num = ""
            if model.paymentNumber.count < 1 {
                return [[:]]
            }

            num = model.paymentNumber.deletingPrefix("0095")
            if num.hasPrefix("0") {
                
            }else {
                num = "0" + num
            }
            
            var comments = ""
            var planType = ""
            
            if model.type == "Top-Up Plan"{
                planType = "TopUpPlan"
            }else if model.type == "Data Plan" {
                planType = "DataPlan"
            }else {
                planType = "SpecialOffer"
            }
            
            if model.operatorName.lowercased() == "Mectel".lowercased() {
                 comments = String.init(format: "OOK-00-%@-%@-%@-naO",model.operatorName.lowercased().capitalizedFirst(),num,planType)
            } else {
                 comments = String.init(format: "#OK-00-%@-%@-%@-na-%@-%@#",model.operatorName.lowercased().capitalizedFirst(),num,planType,manager.currentLatitude,manager.currentLongitude)
            }
            
            
            println_debug(comments)
            
            var lTransactions = Dictionary<String, Any>()
            lTransactions["Amount"] = model.developerAmount
            lTransactions["Comments"] = comments
            
            let dict = self.cashBackMerchantNumber[safe: index]
            var destNumber = ""
            if let localDict = dict {
                destNumber = localDict.safeValueForKey("MobileNumber") as? String ?? ""
            }
            if MultiTopupArray.main.controllers.count == 1 {
                cashBackDestNum = destNumber
            }
            lTransactions["Destination"] = destNumber
            lTransactions["MobileNumber"] = UserModel.shared.mobileNo
            lTransactions["Password"] = ok_password ?? ""
            lTransactions["PromoCodeId"] = ""
            lTransactions["SecureToken"] = UserLogin.shared.token
            if model.operatorName.lowercased() == "MecTel".lowercased() {
                lTransactions["LocalTransactionType"]  = ""
                lTransactions["IsMectelTopUp"] = true
                lTransactions["ProductCode"] = "MECTEL\(model.developerAmount)"
                lTransactions["TransactionType"] = "TOPUP"
            } else {
                lTransactions["LocalTransactionType"]  = "TOPUP"
                lTransactions["IsMectelTopUp"] = false
                lTransactions["ProductCode"] = ""
                lTransactions["TransactionType"] = "PAYTO"
            }
            
            lTransactions["Mode"] = true
            lTransactions["DestinationNumberWalletBalance"] = ""
            lTransactions["DiscountPayTo"] = 0
            lTransactions["IsPromotionApplicable"] = "0"
            lTransactions["KickBack"] =  (model.developerCashback  ==  "Kick-Back Rules Not Define") ? "0.0" : model.developerCashback
            let kickBackNumber = self.getKickbackTopupByOperator(opaerator: model.operatorName)
            lTransactions["KickBackMsisdn"]         = kickBackNumber
            
            lTransactions["MerchantName"] = ""
            
            
            if model.operatorName.lowercased() == "mectel" {
                lTransactions["Operator"] = "MECTEL"
                
            } else {
                lTransactions["Operator"] = (model.operatorName.count > 0) ? model.operatorName.lowercased().capitalizedFirst() : ""
            }
            
            lTransactions["OsType"] = "1"
            lTransactions["PromoCodeId"] = ""
            lTransactions["ResultCode"] = "0"
            lTransactions["ResultDescription"] = ""
            lTransactions["TransactionID"] = ""
            lTransactions["TransactionTime"] = ""
            lTransactions["accounType"] = ""
            lTransactions["senderBusinessName"] = ""
            lTransactions["userName"] = "Unknown"
            let kickBackNumberLoc = self.getKickbackTopupByOperator(opaerator: model.operatorName.lowercased())
            lTransactions["KickBackMsisdn"]         = kickBackNumberLoc
            lTransactions["Balance"] = UserLogin.shared.walletBal
            
            var finalParameterDictionaried = Dictionary<String,Any>()
            finalParameterDictionaried["TransactionsCellTower"] = TransactionsCellTower
            finalParameterDictionaried["lExternalReference"] = lExternalReference
            finalParameterDictionaried["lGeoLocation"] = lGeoLocation
            finalParameterDictionaried["lProximity"] = lProximity
            finalParameterDictionaried["lTransactions"] = lTransactions
            
            // Destinaiton & Benificiary number are same
            if destNumber.contains(find: num) || (destNumber == num) {
                println_debug("Destinaiton & Benificiary number are same")
            }else {
                println_debug("Destinaiton & Benificiary number are not same")
                array.append(finalParameterDictionaried)
            }
        }
        return array
    }
    
    func generatePaymentForSingleTopupOther() -> Dictionary<String,Any> {
        
        var TransactionsCellTower  = Dictionary<String,Any>()
        TransactionsCellTower["Lac"] = ""
        TransactionsCellTower["Mcc"] = mccStatus.mcc
        TransactionsCellTower["Mnc"] = mccStatus.mnc
        TransactionsCellTower["SignalStrength"] = ""
        
        if UitilityClass.isConnectedToWifi(){
            let wifiInfo = UitilityClass.getNetworkInfos()[0].components(separatedBy: ",")
            TransactionsCellTower["Ssid"] = wifiInfo[0]
            TransactionsCellTower["Mac"] = wifiInfo[1]
        }else{
            TransactionsCellTower["Ssid"] = ""
            TransactionsCellTower["Mac"] = ""
        }
        
        // l External Reference
        var lExternalReference = Dictionary<String,Any>()
        lExternalReference["Direction"] = true
        lExternalReference["EndStation"] = ""
        lExternalReference["StartStation"] = ""
        lExternalReference["VendorID"] = ""
        
        // L geo Location
        
        var lGeoLocation = Dictionary<String,Any>()
        lGeoLocation["CellID"] = ""
        lGeoLocation["Latitude"] = manager.currentLatitude
        lGeoLocation["Longitude"] = manager.currentLongitude
        
        // L Proximity
        var lProximity = Dictionary<String,Any>()
        lProximity["BlueToothUsers"] = [Any]()
        lProximity["SelectionMode"] = false
        lProximity["WifiUsers"] = [Any]()
        
        // Final payment Dictionary
        
        let model = modelArray[0]
        var num = ""
        
        if model.paymentNumber.count < 1 {
            return [:]
        }
        
        num = model.paymentNumber.deletingPrefix("0095")
        if num.hasPrefix("0") {
        }else {
            num = "0" + num
        }
        
        RechargeNumber = num
        
        var planType = ""
        
        if model.type == "Top-Up Plan"{
            planType = "TopUpPlan"
        }
        else if model.type == "Data Plan" {
            planType = "DataPlan"
        }
        else {
            planType = "SpecialOffer"
        }
        
        
        /*OOK-134957334-Mectel-0930965918-TopUpPlan-naO
        
        "TransactionType":"TOPUP"

        "IsMectelTopUp":true,*/

        Rechangetype = model.type
        let comments = String.init(format: "#OK-00-%@-%@-%@-na-%@-%@#",model.operatorName.lowercased().capitalizedFirst(),num,planType,manager.currentLatitude,manager.currentLongitude)
        
        var lTransactions = Dictionary<String, Any>()
        lTransactions["Amount"] = model.developerAmount
        lTransactions["Balance"] = UserLogin.shared.walletBal
        lTransactions["BonusPoint"] = 0
        lTransactions["BusinessName"] = UserModel.shared.businessName
        lTransactions["CashBackFlag"] = 0
        lTransactions["Comments"] = comments
        lTransactions["Destination"] = model.number
        lTransactions["DiscountPayTo"] = 0
        if model.operatorName.lowercased() == "MecTel".lowercased() {
            lTransactions["IsMectelTopUp"] = true
        } else {
            lTransactions["IsMectelTopUp"] = false
        }
        lTransactions["IsPromotionApplicable"] = "0"
        lTransactions["KickBack"] =  (model.developerCashback  ==  "Kick-Back Rules Not Define") ? "0.0" : model.developerCashback
        let kickBackNumber = self.getKickbackTopupByOperator(opaerator: model.operatorName.lowercased())
        lTransactions["KickBackMsisdn"] = kickBackNumber
        lTransactions["MerchantName"] = ""
        lTransactions["MobileNumber"] = UserModel.shared.mobileNo
        lTransactions["Mode"] = true
        lTransactions["Password"] = ok_password ?? ""
        lTransactions["PromoCodeId"] = ""
        lTransactions["ResultCode"] = "0"
        lTransactions["ResultDescription"] = ""
        lTransactions["SecureToken"] = UserLogin.shared.token
        lTransactions["TransactionID"] = ""
        lTransactions["TransactionTime"] = ""
        lTransactions["TransactionType"] = "PAYTO"
        lTransactions["accounType"] = ""
        lTransactions["senderBusinessName"] = ""
        lTransactions["userName"] = "Unknown"
        lTransactions["LocalTransactionType"]  = "TOPUP"
        
        var finalParameterDictionaried = Dictionary<String,Any>()
        finalParameterDictionaried["TransactionsCellTower"] = TransactionsCellTower
        finalParameterDictionaried["lExternalReference"] = lExternalReference
        finalParameterDictionaried["lGeoLocation"] = lGeoLocation
        finalParameterDictionaried["lProximity"] = lProximity
        finalParameterDictionaried["lTransactions"] = lTransactions
        
        println_debug(finalParameterDictionaried)
        return finalParameterDictionaried
    }
        
    func generatePaymentForSingleTopup() -> Dictionary<String,Any> {
        
        var TransactionsCellTower  = Dictionary<String,Any>()
        TransactionsCellTower["Lac"] = ""
        TransactionsCellTower["Mcc"] = mccStatus.mcc
        TransactionsCellTower["Mnc"] = mccStatus.mnc
        TransactionsCellTower["SignalStrength"] = ""
        TransactionsCellTower["NetworkType"] = UitilityClass.getNetworkType()
        TransactionsCellTower["NetworkSpeed"] = UitilityClass.getSignalStrength()

        
        if UitilityClass.isConnectedToWifi(){
            let wifiInfo = UitilityClass.getNetworkInfos()[0].components(separatedBy: ",")
            TransactionsCellTower["ConnectedwifiName"] = wifiInfo[0]
            TransactionsCellTower["Ssid"] = wifiInfo[0]
            TransactionsCellTower["Mac"] = wifiInfo[1]
        }else{
            TransactionsCellTower["ConnectedwifiName"] = ""
            TransactionsCellTower["Ssid"] = ""
            TransactionsCellTower["Mac"] = ""
        }

        
        // l External Reference
        var lExternalReference = Dictionary<String,Any>()
        lExternalReference["Direction"] = true
        lExternalReference["EndStation"] = ""
        lExternalReference["StartStation"] = ""
        lExternalReference["VendorID"] = ""
        
        // L geo Location
        
        var lGeoLocation = Dictionary<String,Any>()
        lGeoLocation["CellID"] = ""
        lGeoLocation["Latitude"] = manager.currentLatitude
        lGeoLocation["Longitude"] = manager.currentLongitude
        
        // L Proximity
        var lProximity = Dictionary<String,Any>()
        lProximity["BlueToothUsers"] = [Any]()
        lProximity["SelectionMode"] = false
        lProximity["WifiUsers"] = [Any]()
        lProximity["mBltDevice"] = ""
        lProximity["mWifiDevice"] = ""

        // Final payment Dictionary
        
        guard let firstDict = self.arrDictionary.first else { return  [:] }
        guard let nameOperator = InternationalTopupManager.selectedOperatorObject else { return [:] }
        guard let country = InternationalTopupManager.selectedCountryObject else { return [:]}

        let code = country.countryCode2.safelyWrappingString()
        
        let operatorName = nameOperator.operatorName.safelyWrappingString()
        
        let number = firstDict.safeValueForKey("internationalNumber").safelyWrappingString()
        let comments = String.init(format: "#OK-00-%@-%@-%@-%@-%@-%@-na#",operatorName,number,"TopUpPlan",currency ?? "","Overseas",code)

        var lTransactions = Dictionary<String, Any>()
        lTransactions["Amount"] = firstDict.safeValueForKey("amount").safelyWrappingString()
        lTransactions["DestinationNumberAccountType"] = ""
        lTransactions["Comments"] = comments
        lTransactions["Destination"] = firstDict.safeValueForKey("destination").safelyWrappingString()
        lTransactions["LocalTransactionType"]  = "TOPUP"
        lTransactions["MobileNumber"] = UserModel.shared.mobileNo
        lTransactions["Password"] = ok_password ?? ""
        
        lTransactions["PromoCodeId"] = ""
        lTransactions["SecureToken"] = UserLogin.shared.token
        lTransactions["TransactionType"] = "PAYTO"
        let kickBackNumber = self.getKickbackTopupByOperator(opaerator: operatorName.lowercased())
        lTransactions["KickBackMsisdn"]         = kickBackNumber
        lTransactions["Balance"] = UserLogin.shared.walletBal
        
        var finalParameterDictionaried = Dictionary<String,Any>()
        finalParameterDictionaried["TransactionsCellTower"] = TransactionsCellTower
        finalParameterDictionaried["lExternalReference"] = lExternalReference
        finalParameterDictionaried["lGeoLocation"] = lGeoLocation
        finalParameterDictionaried["lProximity"] = lProximity
        finalParameterDictionaried["lTransactions"] = lTransactions
        
        println_debug(finalParameterDictionaried)
        return finalParameterDictionaried
    }
    
    deinit {
        geoLocManager.stopUpdateLocation()
    }
}











