//
//  LoyaltyTopupViewController.swift
//  OK
//
//  Created by Ashish on 8/15/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class LoyaltyTopupViewController: OKBaseController, UITableViewDelegate, UITableViewDataSource {
    
    var dataModel : BuyBonusPointModel?
    
    var redemmedDisplayArray : [BuyRedeemPointsModel]?
  var isComingFromRecent =  false
    @IBOutlet weak var topupTable: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.backButton()
        self.topupTable.tableFooterView = UIView.init()
        NotificationCenter.default.addObserver(self, selector: #selector(LoyaltyTopupViewController.dismissMyTopupScreen), name: NSNotification.Name(rawValue: "GoToDashboardLoyaltySellingTopup"), object: nil)
    }
    
    @objc func dismissMyTopupScreen() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = PaytoConstants.global.attributesForTitle()
        //self.title = LoyaltyTopupConstant.titleHeaders.buyBonusPoint.safelyWrappingString()
        self.title = "Buy Bouns Point Title".localized
        self.callGetRedeemPointsHistoryByCategory()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "GoToDashboardLoyaltySellingTopup"), object: nil)
    }
    
    // back button init
    private func backButton() {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "tabBarBack"), for: UIControl.State.normal)
        button.tintColor = UIColor.white
        button.sizeToFit()
        button.addTargetClosure { [weak self](button) in
            guard let weakSelf = self else { return }
            weakSelf.dismiss(animated: true, completion: nil)
        }
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    //MARK:- GetRedeemPointsHistoryByCategory
  private  func callGetRedeemPointsHistoryByCategory() {
        okActivityIndicator(view: self.view)
        let param = loadGetRedeemPointsHistoryByCategoryParams()
        
        guard let urlString = Url.getHistoryByCategory.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {  return }
         let url = getUrl(urlStr: urlString, serverType: .serverApp)
    
        TopupWeb.genericClassWithoutLoader(url: url, param: param as AnyObject, httpMethod: "POST") { [weak self](response, success) in
            DispatchQueue.main.async {
                guard let weakself = self else { return }
                if success {
                    guard let dictionary = response as? Dictionary<String, Any>  else { return }
                    if dictionary.safeValueForKey("Code") as? Int == 200 {
                        guard let dataString = dictionary.safeValueForKey("Data") as? String else { return }
                        guard let jsonObject = self?.jsonObject(string: dataString) else { return }
                        guard let modelData  = jsonObject.data(using: .utf8) else { return }
                        do {
                             let model = try JSONDecoder().decode(BuyBonusPointModel.self, from: modelData)
                            self?.generatingUI(model)
                        } catch {
                            println_debug(error.localizedDescription)
                        }
                    }
                }
                weakself.removeActivityIndicator(view: weakself.view)
            }
        }
        
    }
    
    private func generatingUI(_ model: BuyBonusPointModel) {

        self.dataModel = model
        
       var redeemedModel = [BuyRedeemPointsModel]()
       
        for redemmedObject in model {
            if let subModel = redemmedObject.buyRedeemPointsModel {
                redeemedModel.append(contentsOf: subModel)
            }
        }

        self.redemmedDisplayArray = redeemedModel.unique(map: {$0.merchantMobileNumber.safelyWrappingString()})
        self.topupTable.reloadData()
    }
    
   private func loadGetRedeemPointsHistoryByCategoryParams() -> Dictionary<String,Any> {
        var param = Dictionary<String,Any>()
        
        param["AppId"] = LoginParams.setUniqueIDLogin()
        param["Limit"] = 0
        param["MobileNumber"] = UserModel.shared.mobileNo
        param["Msid"] = getMsid()
        param["Offset"] = 0
        param["Ostype"] = 1
        param["Otp"] = ""
        param["Simid"] = simid
        
        var mainParam = Dictionary<String,Any>()
        mainParam["LoginInfo"] = param
        mainParam["RedeemPointsCategory"] = "BUY"
        return mainParam
    }
    
    //MARK:- TableViewDelegate & DataSource

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.redemmedDisplayArray == nil) ? 0 : self.redemmedDisplayArray!.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.00
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: LoyaltyTopupViewControllerCell.self)) as? LoyaltyTopupViewControllerCell
        if let model = self.redemmedDisplayArray, let data = model[safe: indexPath.row] {
            cell?.wrapImageView(name: data.merchantName.safelyWrappingString(), imgUrl: data.merchantImageURL.safelyWrappingString())
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let merchantVC = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: LoyaltyTopupRedeemedMerchantViewController.self)) as? LoyaltyTopupRedeemedMerchantViewController else { return }
        guard let cell = tableView.cellForRow(at: indexPath) as? LoyaltyTopupViewControllerCell else { return }
        guard  cell.loyaltyName != nil else { return }
        merchantVC.header = cell.loyaltyName.text.safelyWrappingString()
        let objects = self.extractingModels(name: cell.loyaltyName.text.safelyWrappingString())
        
        merchantVC.redeemModel = objects.sorted(by: {$0.buyingPrice.safelyWrappingString() < $1.buyingPrice.safelyWrappingString()})
        merchantVC.isComingFromRecent = self.isComingFromRecent
        self.navigationController?.pushViewController(merchantVC, animated: true)
    }
    
    func extractingModels(name: String) -> [BuyRedeemPointsModel] {
        
        var redeemedModel = [BuyRedeemPointsModel]()
        
        guard let model = self.dataModel else { return [BuyRedeemPointsModel]() }
        
        for redemmedObject in model {
            if let subModel = redemmedObject.buyRedeemPointsModel {
                redeemedModel.append(contentsOf: subModel)
            }
        }
        
        var models = redeemedModel.filter({$0.merchantName.safelyWrappingString() == name })
        
        models = models.sorted(by: {
            guard let amount = $0.buyingPrice, let amountIterate = $1.buyingPrice else { return false }
            return (amount > amountIterate)
        })
        
        return models
    }
    
    
}

class LoyaltyTopupViewControllerCell : UITableViewCell {
    
    @IBOutlet weak var loyaltyName : UILabel!{
        didSet
        {
            self.loyaltyName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var loyaltyImage : UIImageView!
    
    func wrapImageView(name: String, imgUrl: String) {
        self.loyaltyName.text = name
        guard let encodedURl = imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        guard let imageURL = URL.init(string: encodedURl) else { return }
        self.loyaltyImage.setImage(url: imageURL, placeholderImage: nil)
    }
    
}






