//
//  BonuPointTopupViewController.swift
//  OK
//
//  Created by Ashish on 8/20/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit


class BonuPointTopupViewController: OKBaseController, UITableViewDelegate, UITableViewDataSource {

    var models : BonusPointCollection?
    
    var merchantNumber : String? = ""
    
    var userEnteredNumber : String? = ""
    
    var sellingBackResponse : Dictionary<String,Any>?
    
    var loyaltyName : String? = "Unknown"
    
    @IBOutlet weak var bonusPointTableView : UITableView?
    @IBOutlet weak var headerHolderView : UIView!
    @IBOutlet weak var lblDescriptions   : UILabel!
        {
        didSet
        {
            lblDescriptions.font = UIFont(name: appFont, size: appFontSize)
            lblDescriptions.text = "DESCRIPTION".localized
            
        }
    }
    @IBOutlet weak var lblTopupAmount    : UILabel!
        {
        didSet
        {
            lblTopupAmount.font = UIFont(name: appFont, size: appFontSize)
            lblTopupAmount.text = "TOP-UP AMOUNT".localized
            
        }
    }
    @IBOutlet weak var lblBonusPoints    : UILabel!
        {
        didSet
        {
            lblBonusPoints.font = UIFont(name: appFont, size: appFontSize)
            lblBonusPoints.text = "BONUS POINTS".localized
        }
    }
    
    @IBOutlet weak var myAvailPoints : UILabel! {
        didSet {
            self.myAvailPoints.font = UIFont(name: appFont, size: appFontSize)
            self.myAvailPoints.text = "My Available Point".localized
        }
    }
    
    @IBOutlet weak var noOffers : UILabel! {
        didSet {
            self.noOffers.text = "No Topup Offers!".localized
            self.noOffers.font = UIFont(name: appFont, size: appFontSize)
        }
    }

    var points : Int = 0
    
    var isComingFromRecent =  false
    
    @IBOutlet weak var myPoints : UILabel!{
        didSet {
            self.myPoints.text = "No Topup Offers!".localized
            self.myPoints.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myPoints.text = ""
        self.bonusPointTableView?.tableFooterView = UIView()
        self.noOffers.isHidden = true
        
        let rangeCheck = PayToValidations().getNumberRangeValidation(userEnteredNumber.safelyWrappingString())
        let colorChange = PayToValidations().getColorForOperator(operatorName: rangeCheck.operator)
                
        self.navigationController?.navigationBar.tintColor = colorChange
        
        let name = rangeCheck.operator
        let size  = name.size(withAttributes: [.font: UIFont.init(name: appFont, size: 13.0) ?? UIFont.systemFont(ofSize: 13.0)])
        let label = UILabel(frame: .init(origin: CGPoint.zero, size: size))
        label.textColor = .white
        label.text = name.lowercased().capitalizedFirst()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: label)
        
        self.navigationController?.navigationBar.barTintColor = colorChange
        
        self.backButton()
        self.calculateBonusPoints()
        
        guard let model = self.models else { return }
        if model.count == 0 {
            self.bonusPointTableView!.isHidden = true
            self.myAvailPoints.isHidden = true
            self.myPoints.isHidden = true
            self.headerHolderView.isHidden = true
            self.noOffers.isHidden = false
        }
        
        models = model.sorted(by: { $0.model?.buyingPrice ?? 0 < $1.model?.buyingPrice ?? 0 })
        models = models?.filter({$0.model?.redeemPoints ?? 0 <= points})
        if let table = self.bonusPointTableView {
            table.reloadData()
        }
    }
    
    func calculateBonusPoints() {
        let number = self.merchantNumber.safelyWrappingString()
        
        guard let sellRespone = sellingBackResponse else { return }
        
      guard  let merNumbers = sellRespone.safeValueForKey("merchantid") as? String else {
        self.myPoints.text = "0" + "PTS"
        return
        }
        guard let loyaltyPoints = sellRespone.safeValueForKey("loyaltypoints") as? String else { return }
        
        let merchantArray = merNumbers.components(separatedBy: "|")
        let pointsArray   = loyaltyPoints.components(separatedBy: "|")
        
        var countingPoint = [Int]()
        
        if merchantArray.count == pointsArray.count {
            for (index,element) in merchantArray.enumerated() {
                if element.lowercased() == number.lowercased() {
                    let integerValueIndex = (pointsArray[index] as NSString).integerValue
                    countingPoint.append(integerValueIndex)
                }
            }
        }
        
        if countingPoint.count > 0 {
            let sumedArr = countingPoint.reduce(0, {$0 + $1})
            points = sumedArr
            if sumedArr > 0 {
                self.myPoints.attributedText = self.attributedString(amount: "\(sumedArr)", sym: "PTS")
            } else {
               self.myPoints.text = "0" + "PTS"
            }
        }
    }
    
    private func attributedString(amount: String, sym: String) -> NSAttributedString {
        
        var amountStr = ""
        
        //        let amt = (amount as NSString).intValue
        //        let nsAmount = NSNumber.init(value: amt)
        
        guard let amt = Double(amount) else { return  NSMutableAttributedString()}
        let nsAmount = NSNumber(value: amt)
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NumberFormatter.Style.decimal
        if let amtnt  = currencyFormatter.string(from: nsAmount) {
            amountStr = amtnt
        } else {
            amountStr = amount
        }
        
        let mmk = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.00) ?? UIFont.systemFont(ofSize: 11.00)]
        let amtMMK = [NSAttributedString.Key.font: UIFont(name: appFont, size: 14.0) ?? UIFont.systemFont(ofSize: 14.0)]
        let mmkString = NSMutableAttributedString(string: sym, attributes: mmk)
        let amountString    = NSMutableAttributedString.init(string: amountStr, attributes: amtMMK)
        
        let stringAttributed = NSMutableAttributedString()
        stringAttributed.append(amountString)
        stringAttributed.append(NSAttributedString.init(string: " "))
        stringAttributed.append(mmkString)
        
        return stringAttributed
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.titleTextAttributes = PaytoConstants.global.attributesForTitle()
        self.title = "Bonus Point Topup".localized
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.models == nil) ? 0 : self.models!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: BonusPointTopupListCell.self), for: indexPath)
        if let displaycell = cell as? BonusPointTopupListCell, let model = self.models {
            displaycell.wrapView(model[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180.00
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.navigationController?.topViewController != self {
            return
        }
        
        if let mainModel = self.models {
            if points < mainModel[indexPath.row].model!.redeemPoints ?? 0 {
                let str = "Your account have insufficient OK$ Topup Bonus Point. Please Buy OK$ Topup Bonus Points.".localized
                alertViewObj.wrapAlert(title: nil, body: str, img: #imageLiteral(resourceName: "buy_bonuspoint"))
                alertViewObj.addAction(title: "OK".localized, style: .target) {
                    if appDel.checkNetworkAvail() {
                        let vc = UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "LoyaltyTopupNavigation"))
                        self.present(vc, animated: true, completion: nil)
                    } else {
                        self.showErrorAlert(errMessage: PaytoConstants.messages.noInternet.localized)
                    }
                }
                alertViewObj.addAction(title: "Cancel".localized, style: .target) {
                    
                }
                alertViewObj.showAlert()
                return
            }
            
            let str = String.init(format: "%d", mainModel[indexPath.row].model!.buyingPrice ?? 0)
            let number = getAgentCode(numberWithPrefix: userEnteredNumber ?? "", havingCountryPrefix: "+95")
            if !PaymentVerificationManager.isValidPaymentTransactions(number, str, "TOPUP") {
                    alertViewObj.wrapAlert(title: nil, body: "Same Number with Same Amount can not be paid before 2 minutes".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                    alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                    })
                    alertViewObj.showAlert()
                    return
                }

            
            guard let confirmation = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: CommonTopupTransactionViewController.self)) as? CommonTopupTransactionViewController else { return }
            guard let modelSub = mainModel[safe: indexPath.row] else { return }
            confirmation.displayNumber = self.userEnteredNumber.safelyWrappingString()
            confirmation.totalBonusAmount = self.myPoints.text
            confirmation.confirmModel = modelSub.model
            confirmation.loyaltyTopupName = self.loyaltyName
            confirmation.isLoyalty = true
            confirmation.isComingFromRecent = self.isComingFromRecent
            self.navigationController?.pushViewController(confirmation, animated: true)
        }
    }
    
    private func backButton() {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "tabBarBack"), for: UIControl.State.normal)
        button.tintColor = UIColor.white
        button.sizeToFit()
        button.addTargetClosure { [weak self](button) in
            guard let weakSelf = self else { return }
            weakSelf.dismiss(animated: true, completion: nil)
        }
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }
    
}

class BonusPointTopupListCell : UITableViewCell {
    
    @IBOutlet weak var descriptions   : UILabel!{
        didSet
        {
            descriptions.font = UIFont(name: appFont, size: appFontSize)
            
        }
    }
    
    @IBOutlet weak var topupAmount : UILabel!{
        didSet
        {
            topupAmount.font = UIFont(name: appFont, size: appFontSize)
            
        }
    }
    @IBOutlet weak var bonusPoints    : UILabel!{
        didSet
        {
            bonusPoints.font = UIFont(name: appFont, size: appFontSize)
            
        }
    }
    @IBOutlet weak var staticLabel: UILabel!{
        didSet
        {
            staticLabel.font = UIFont(name: appFont, size: appFontSize)
            
        }
    }
    
    @IBOutlet weak var bonusPointView : CardDesignView!
    var mobOperator = ""
    
    private func attributedString(amount: String, sym: String) -> NSAttributedString {
        
        var amountStr = ""
        
        guard let amt = Double(amount) else { return  NSMutableAttributedString()}
        let nsAmount = NSNumber(value: amt)
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NumberFormatter.Style.decimal
        if let amtnt  = currencyFormatter.string(from: nsAmount) {
            amountStr = amtnt
        } else {
            amountStr = amount
        }
        
        let mmk = [NSAttributedString.Key.font: UIFont(name: appFont, size: 14.00) ?? UIFont.systemFont(ofSize: 9.00)]
        var amtMMK: [NSAttributedString.Key : NSObject]
        if mobOperator == "MecTel" {
            amtMMK = [NSAttributedString.Key.font: UIFont(name: appFont, size: 18.00) ?? UIFont.systemFont(ofSize: 11.00), NSAttributedString.Key.foregroundColor: UIColor.white]
        } else {
            amtMMK = [NSAttributedString.Key.font: UIFont(name: appFont, size: 18.00) ?? UIFont.systemFont(ofSize: 11.00), NSAttributedString.Key.foregroundColor: UIColor.init(hex: "#EAA13F")]
        }
        let mmkString = NSMutableAttributedString(string: sym, attributes: mmk)
        
        if amountStr.contains(find: ".") {
            
        } else {
            //amountStr.append(".00")
        }
        
        let amountString = NSMutableAttributedString.init(string: amountStr, attributes: amtMMK)
        
        
        let stringAttributed = NSMutableAttributedString()
        stringAttributed.append(amountString)
        stringAttributed.append(NSAttributedString.init(string: " "))
        stringAttributed.append(mmkString)
        
        return stringAttributed
    }
    
    func wrapView(_ model : BonusPointTopupModel) {
        self.descriptions.text              = model.desc.safelyWrappingString()
        self.topupAmount.attributedText     = self.attributedString(amount: model.amount ?? "0", sym: "MMK")
        self.bonusPoints.attributedText     = self.attributedString(amount: model.points ?? "0", sym: "OK$")
        mobOperator = model.operatorName ?? ""
        switch mobOperator {
        case "MPT", "MPT CDMA 800", "MPT CDMA 450":
            self.backgroundColor = UIColor.init(hex: "#054da2")
        case "Telenor":
            self.backgroundColor = UIColor.init(hex: "#00b6ee")
        case "Ooredoo":
            self.backgroundColor = UIColor.init(hex: "#f33e0d")
        case "MecTel":
            self.backgroundColor = kYellowColor
        case "Mytel":
            self.backgroundColor = UIColor.init(hex: "#df8119")
        default:
            self.backgroundColor = kYellowColor
        }
//        self.bonusPointView.backgroundColor = model.color
    }
    
}
