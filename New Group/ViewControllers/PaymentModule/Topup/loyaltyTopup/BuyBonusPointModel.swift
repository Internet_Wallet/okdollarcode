//
//  BuyBonusPointModel.swift
//  OK
//
//  Created by Ashish on 8/16/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

typealias BuyBonusPointModel = [BuyBonusPointModelElement]

struct BuyBonusPointModelElement: Codable {
    let transactionTypeID, transactionTypeName: String?
    let buyRedeemPointsModel: [BuyRedeemPointsModel]?
    
    enum CodingKeys: String, CodingKey {
        case transactionTypeID = "TransactionTypeId"
        case transactionTypeName = "TransactionTypeName"
        case buyRedeemPointsModel = "BuyRedeemPointsModel"
    }
    
}

struct BuyRedeemPointsModel : Codable {
    let merchantMobileNumber: String?
    let buyingPrice, redeemPoints: Int?
    let isItPercentage: Bool?
    let description, expiryDate, imageURL, telcoName: String?
    let merchantName, merchantImageURL: String?
    
    enum CodingKeys: String, CodingKey {
        case merchantMobileNumber = "MerchantMobileNumber"
        case buyingPrice = "BuyingPrice"
        case redeemPoints = "RedeemPoints"
        case isItPercentage = "IsItPercentage"
        case description = "Description"
        case expiryDate = "ExpiryDate"
        case imageURL = "ImageUrl"
        case telcoName = "TelcoName"
        case merchantName = "MerchantName"
        case merchantImageURL = "MerchantImageUrl"
    }

}


typealias BonusPointCollection = [BonusPointTopupModel]

struct BonusPointTopupModel {
    var desc   : String?
    var amount : String?
    var points : String?
    var color  : UIColor?
    
    var displayAmount : NSAttributedString?
    var displayPoints : NSAttributedString?
    
    //extra payment additional properties
    var availableBonus : String?
    var paymentNumber  : String?
    var operatorName   : String?
    
    var model: BuyRedeemPointsModel?
    
    init(model: BuyRedeemPointsModel, color: UIColor) {
        
        self.model = model
        
        self.desc   = model.description.safelyWrappingString()
        
        guard let amountMMK = model.buyingPrice else { return }
        let amountMMKString = String.init(format: "%d", amountMMK)
        
        guard let bonusPoint = model.redeemPoints else { return }
        let bonusPointString = String.init(format: "%d", bonusPoint)
        
        self.amount = amountMMKString
        self.points = bonusPointString
        self.color  = color
        
        self.displayAmount = self.attributedString(amount: amountMMKString, sym: "MMK")
        self.displayPoints = self.attributedString(amount: bonusPointString, sym: "POINTS")
        
        self.availableBonus = points
        
        self.operatorName = model.telcoName.safelyWrappingString()
    }
    
    init(_ descriptions: String, rawAmount: String, rawPoints: String, availBonus points: String) {
        self.desc   = descriptions
        self.amount = rawAmount
        self.points = rawPoints
        self.color  = MyNumberTopup.theme
        
        self.displayAmount = self.attributedString(amount: rawAmount, sym: "MMK")
        self.displayPoints = self.attributedString(amount: rawPoints, sym: "POINTS")
        
        self.availableBonus = points
    }
    
    private func attributedString(amount: String, sym: String) -> NSAttributedString {
        
        var amountStr = ""
        
        //        let amt = (amount as NSString).intValue
        //        let nsAmount = NSNumber.init(value: amt)
        
        guard let amt = Double(amount) else { return  NSMutableAttributedString()}
        let nsAmount = NSNumber(value: amt)
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NumberFormatter.Style.decimal
        if let amtnt  = currencyFormatter.string(from: nsAmount) {
            amountStr = amtnt
        } else {
            amountStr = amount
        }
        
        let mmk = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.00) ?? UIFont.systemFont(ofSize: 11.00)]
        let amtMMK = [NSAttributedString.Key.font: UIFont(name: appFont, size: 13.0) ?? UIFont.systemFont(ofSize: 13.0)]
        let mmkString = NSMutableAttributedString(string: sym, attributes: mmk)
        let amountString    = NSMutableAttributedString.init(string: amountStr, attributes: amtMMK)
        
        let stringAttributed = NSMutableAttributedString()
        stringAttributed.append(amountString)
        stringAttributed.append(NSAttributedString.init(string: " "))
        stringAttributed.append(mmkString)
        
        return stringAttributed
    }

    
}









