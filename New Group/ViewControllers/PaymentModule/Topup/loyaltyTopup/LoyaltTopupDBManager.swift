//
//  LoyaltTopupDBManager.swift
//  OK
//
//  Created by Ashish on 8/30/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreData

class LoyaltyTopupManager {
    
    class func getContext() -> NSManagedObjectContext {
        return appDel.persistentContainer.viewContext
    }
    
    class func storeTransactions(model: PaymentTransactions) {
        let context = getContext()
        let transactionRecord = ReportTransactionRecord(context: context)
        transactionRecord.isFromApi = Int16(0)
        transactionRecord.operatorName = model.operatorName
        if let amnt = model.amount {
            transactionRecord.amount = NSDecimalNumber(string: amnt)
        }
        transactionRecord.transID = model.transID
        transactionRecord.transactionDate = model.time
        var nameFor = "Unknown"
        if let name = model.name {
            if name.count == 0 {
                nameFor = "Unknown"
            } else {
                nameFor = name
            }
        }
        transactionRecord.receiverName =  nameFor
        transactionRecord.transType = model.type
        transactionRecord.accTransType = model.type
        transactionRecord.destination = model.number
        var operatorName = ""
        
        if let num = model.number {
            operatorName = CodeSnippets.getOparatorName(num)
        }
        transactionRecord.rawDesc = "#OK-" + "\(operatorName)" + "-Bonus Point Top Up"
        transactionRecord.desc = "Remarks: Bonus Point Top Up"
        transactionRecord.bonus = NSDecimalNumber(string: model.bPoints)
        transactionRecord.walletBalance = NSDecimalNumber(string: UserLogin.shared.walletBal)
        transactionRecord.accTransType = "Dr"
        
        if let bAmnt = model.bPoints {
            transactionRecord.bonus = NSDecimalNumber(string: bAmnt)
        }
        do {
            try context.save()
        } catch let error as NSError {
            println_debug(error)
        }
    }
}

struct LoyaltyTransactionModel {
    
}
