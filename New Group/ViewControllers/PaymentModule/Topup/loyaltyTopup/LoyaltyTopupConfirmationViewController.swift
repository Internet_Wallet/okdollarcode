//
//  LoyaltyTopupConfirmationViewController.swift
//  OK
//
//  Created by Ashish on 8/18/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class LoyaltyTopupConfirmationViewController: OKBaseController, UITableViewDelegate, UITableViewDataSource, BioMetricLoginDelegate {
    
    @IBOutlet weak var adImageView : UIImageView!
    @IBOutlet weak var cnfTable    : UITableView!
    @IBOutlet weak var payBtn: UIButton! {
        didSet {
            self.payBtn.setTitle("Pay".localized, for: .normal)
        }
    }
    
    var confirmModel : BuyRedeemPointsModel?
    
    var fromSelling : Bool = false
    
    var enteredNumber : String?
    
    var totalBonusAmount : String?
    
    var keys : [String]?
    var values : [String]?
    var isComingFromRecent =  false

    override func viewDidLoad() {
        super.viewDidLoad()
     guard let model = confirmModel else { return }
     keys   = ["Buying Price", "Type", "Bonus Points", "Expires On"]
     values = valuesWrap(model)
     self.cnfTable.reloadData()
     self.loadAdvertisement()
     self.cnfTable.tableFooterView = UIView.init()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.backButton()
        self.navigationController?.navigationBar.titleTextAttributes = PaytoConstants.global.attributesForTitle()
        self.title = "Confirmation".localized
    }
    
    // back button init
    private func backButton() {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "tabBarBack"), for: UIControl.State.normal)
        button.tintColor = UIColor.white
        button.sizeToFit()
        button.addTargetClosure { [weak self](button) in
            guard let weakSelf = self else { return }
            weakSelf.navigationController?.popViewController(animated: true)
        }
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }

    
    @IBAction func genericPaymentPayButton(_ sender: UIButton) {
        
        guard appDel.checkNetworkAvail() else {
            self.showErrorAlert(errMessage: "No Internet Connection!".localized)
            return
        }
        
        guard UserLogin.shared.loginSessionExpired else {
            if fromSelling {
                self.topupPaymentSelling()
            } else {
                self.initiatePayment()
            }
            return
        }
        OKPayment.main.authenticate(screenName: "loyaltyTopupScreen", delegate: self)
        
    }
    
    //MARK:- Advertisement Views
    func loadAdvertisement() {
        let randomIndex = Int(arc4random_uniform(UInt32(advImagesUrl.AdImageData.count)))
        if let urlString = advImagesUrl.AdImageData[safe: randomIndex] {
            DispatchQueue.main.async {
                self.adImageView.image = UIImage(data: urlString)
                self.adImageView.contentMode = .scaleAspectFit
                //                self.adImageView.setImage(url: urlString, contentMode: UIView.ContentMode.scaleAspectFit, placeholderImage: nil)
            }
        }
    }
    
    func valuesWrap(_ model: BuyRedeemPointsModel) -> [String] {
        
        guard let buyingPrice = model.buyingPrice else { return [String]() }
        guard let bonusPoints = model.redeemPoints else { return [String]()}
        guard let expiresOn   = model.expiryDate else { return [String]()}
        
        var valuesArray = [String]()
        
        let stringBuyingPrice = String.init(format: "%d", buyingPrice)
        let stringBonusPoints = String.init(format: "%d", bonusPoints)
        
        valuesArray.append(stringBuyingPrice)
        valuesArray.append("Buy Bonus Point")
        valuesArray.append(stringBonusPoints)
        
        if let name = model.merchantName, name.lowercased() == "OK$ TopUp Point".lowercased() {
            valuesArray.append("Unlimited")
        } else {
            let modifiedDate = self.getDateAndTime(timeStr: expiresOn)
            valuesArray.append(modifiedDate)
        }
        
        return valuesArray
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (values == nil) ? 0 : values!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: LoyaltyTopupConfirmationCell.self), for: indexPath) as? LoyaltyTopupConfirmationCell
        if let key = keys, let value = values {
            cell?.wrapCell(key: key[safe: indexPath.row] ?? "", value: value[safe: indexPath.row] ?? "")
        }
        return cell!
    }
    
    private func getDateAndTime(timeStr: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        guard let time = dateFormatter.date(from: timeStr) else { return "" }
        dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss"//hh:mm a"
        let dateAndTime = dateFormatter.string(from: time)
        return dateAndTime
    }
    
    //MARK:- Payment Initiate
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        guard isSuccessful else { return }
        if fromSelling {
            self.topupPaymentSelling()
        } else {
            self.initiatePayment()
        }
        
    }
    
        
    //MARK:- Selling APIs
    var transaction = Dictionary<String,Any>()

    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            transaction[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func topupPaymentSelling() {
        let urlString = String.init(format: "%@", Url.getSellingTopup)
        let url = getUrl(urlStr: urlString, serverType: .serverApp)
        let params = self.JSONStringFromAnyObject(value: getAvailableParamsForSelling() as AnyObject)
        
        DispatchQueue.main.async {
            TopupWeb.genericClass(url: url, param: params as AnyObject, httpMethod: "POST") { [weak self](response, success) in
                if success {
                    if let json = response as? Dictionary<String,Any> {
                        if let code = json["Code"] as? NSNumber, code == 200 {
                            if var data = json["Data"] as? String {
                                data = data.replacingOccurrences(of: "\"", with: "")
                                let xmlString = SWXMLHash.parse(data)
                                self?.enumerate(indexer: xmlString)
                                if self?.transaction.safeValueForKey("resultdescription") as? String == "Transaction Successful" {
                                    if let jsonData = self?.transaction.jsonString().data(using: .utf8) {
                                        if let model = try? JSONDecoder().decode(PaymentFinalRecieptModel.self, from: jsonData) {
                                            DispatchQueue.main.async {
                                                guard let vc = self?.storyboard?.instantiateViewController(withIdentifier: "TopupSingleViewRecieptController") as? TopupSingleViewRecieptController else { return }
                                                vc.reciptModel = model
                                                vc.categoryValue = RecipetPlanCases(rawValue: RecipetPlanCases.otherNumber.rawValue)
                                                vc.planType = "Top-Up Plan"
                                                if let value = self{
                                                    vc.isComingFromRecent = value.isComingFromRecent
                                                }
                                                self?.navigationController?.pushViewController(vc, animated: true)
                                            }
                                        }
                                    }
                                    let operatorName = PayToValidations().getNumberRangeValidation(self?.enteredNumber.safelyWrappingString() ?? "").operator
                                    appDelegate.logEventWithAnaylitics(eventName: k_Payment, EventType: operatorName, WithUser: UserModel.shared.mobileNo, AndEventStatus: k_TransactionSuccess)
                                } else {
                                    self?.showErrorAlert(errMessage: self?.transaction.safeValueForKey("resultdescription") as? String ?? "")
                                    let operatorName = PayToValidations().getNumberRangeValidation(self?.enteredNumber.safelyWrappingString() ?? "").operator
                                    appDelegate.logEventWithAnaylitics(eventName: k_Payment, EventType: operatorName, WithUser: UserModel.shared.mobileNo, AndEventStatus: k_TransactionFailure)
                                }
                            }
                        } else {
                            if let msg = json["Msg"] as? String {
                                self?.showErrorAlert(errMessage: msg)
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    func getAvailableParamsForSelling() -> Dictionary<String,Any> {
        
        guard let model = self.confirmModel else { return Dictionary<String,Any>() }
        
        let amountBeneficiary  = String.init(format: "%d", model.buyingPrice ?? 0)
        
        var finalParams = Dictionary<String,Any>()
        
        var loginInfo = Dictionary<String,Any>()
        loginInfo["AppId"] = LoginParams.setUniqueIDLogin()
        loginInfo["Limit"]  = 0
        loginInfo["MobileNumber"]  = UserModel.shared.mobileNo
        loginInfo["Msid"] = getMsid()
        loginInfo["Offset"] = 0
        loginInfo["Ostype"] = 1
        loginInfo["Otp"] = ""
        loginInfo["Simid"] = simid
        
        var redeemSelling = Dictionary<String, Any>()
        redeemSelling["AvialableBonusPoints"] = totalBonusAmount.safelyWrappingString()
        redeemSelling["BeneficiaryAmount"]  =  amountBeneficiary
        redeemSelling["BeneficiaryNumber"]  = enteredNumber.safelyWrappingString()
        redeemSelling["BonusPointName"]    = model.merchantName.safelyWrappingString()
        redeemSelling["BonusPointSourceNumber"] = model.merchantMobileNumber.safelyWrappingString()
        redeemSelling["Comments"] = ""
        redeemSelling["Destination"] = model.merchantMobileNumber.safelyWrappingString()
        redeemSelling["Password"] = ok_password ?? ""
        redeemSelling["QtyOfRedeemPoints"] = model.redeemPoints ?? 0
        redeemSelling["SecureToken"] = UserLogin.shared.token
        redeemSelling["Source"] = UserModel.shared.mobileNo
        redeemSelling["TelcoName"] = model.telcoName.safelyWrappingString()
        redeemSelling["TransactionType"] = "TOPUP"
        
        finalParams["LoginInfo"] = loginInfo
        finalParams["RedeempointsSellingDto"] = redeemSelling
        
        return finalParams
    }
    
    //MARK:- Buying APIs
    func initiatePayment() {
        
        let urlString = String.init(format: "%@", Url.genericPayment)
        let url = getUrl(urlStr: urlString, serverType: .serverApp)
        let params = self.JSONStringFromAnyObject(value: getParams() as AnyObject)
        
        TopupWeb.genericClass(url: url, param: params as AnyObject, httpMethod: "POST") { [weak self] (response, success) in
            if  success {
                DispatchQueue.main.async {
                    self?.parseMultiPaymentResult(data: response)
                }
            }
        }
    }
    
    func parseMultiPaymentResult(data : Any) {
        
        var transaction = Dictionary<String,Any>()
        
        func enumerate(indexer: XMLIndexer) {
            for child in indexer.children {
                transaction[child.element!.name] = child.element!.text
                enumerate(indexer: child)
            }
        }

        guard let dictionary = data as? Dictionary<String,Any> else { return }
        guard let xmlString  = dictionary.safeValueForKey("Data") as? String else { return }
        let parserString     = SWXMLHash.parse(xmlString)
        enumerate(indexer: parserString)
        guard let transactionValue = transaction.safeValueForKey("resultdescription") as? String else { return }
        guard transactionValue.lowercased() == "Transaction Successful".lowercased() else {
            self.showErrorAlert(errMessage: transactionValue.localized)
            return
        }
        
        guard let receipt = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: LoyaltyTopupReceiptViewController.self)) as? LoyaltyTopupReceiptViewController else { return }
        receipt.receiptDictionary = transaction
        receipt.model             = self.confirmModel
        receipt.isComingFromRecent = self.isComingFromRecent
        self.navigationController?.pushViewController(receipt, animated: true)
    }
    
    func getParams() -> Dictionary<String,Any> {
        // Transaction cell tower
        var TransactionsCellTower  = Dictionary<String,Any>()
        TransactionsCellTower["Lac"] = ""
        TransactionsCellTower["Mcc"] = mccStatus.mcc
        TransactionsCellTower["Mnc"] = mccStatus.mnc
        TransactionsCellTower["SignalStrength"] = ""
        TransactionsCellTower["NetworkType"] = UitilityClass.getNetworkType()
        TransactionsCellTower["NetworkSpeed"] = UitilityClass.getSignalStrength()
        
        if UitilityClass.isConnectedToWifi(){
            let wifiInfo = UitilityClass.getNetworkInfos()[0].components(separatedBy: ",")
            TransactionsCellTower["ConnectedwifiName"] = wifiInfo[0]
            TransactionsCellTower["Ssid"] = wifiInfo[0]
            TransactionsCellTower["Mac"] = wifiInfo[1]
        }else{
            TransactionsCellTower["ConnectedwifiName"] = ""
            TransactionsCellTower["Ssid"] = ""
            TransactionsCellTower["Mac"] = ""
        }

        
        // l External Reference
        var lExternalReference = Dictionary<String,Any>()
        lExternalReference["Direction"] = true
        lExternalReference["EndStation"] = ""
        lExternalReference["StartStation"] = ""
        lExternalReference["VendorID"] = ""
        
        // L geo Location
        var lGeoLocation = Dictionary<String,Any>()
        lGeoLocation["CellID"] = ""
        lGeoLocation["Latitude"] = GeoLocationManager.shared.currentLatitude ?? "0.0"
        lGeoLocation["Longitude"] = GeoLocationManager.shared.currentLongitude ?? "0.0"
        
        // L Proximity
        var lProximity = Dictionary<String,Any>()
        lProximity["BlueToothUsers"] = [Any]()
        lProximity["SelectionMode"] = false
        lProximity["WifiUsers"] = [Any]()
        
        // Final payment Dictionary
        
        guard let model  = confirmModel else { return Dictionary<String,Any>() }
        guard let amount = model.buyingPrice else { return Dictionary<String,Any>() }
        guard let points = model.redeemPoints else { return Dictionary<String,Any>() }
        
        var lTransactions = Dictionary<String, Any>()
        lTransactions["Amount"] = String.init(format: "%d", amount)
        lTransactions["Comments"] = "#point-redeempoints-\(points)"
        lTransactions["DestinationNumberAccountType"] = ""
        lTransactions["Destination"] = model.merchantMobileNumber.safelyWrappingString()
        lTransactions["MobileNumber"] = UserModel.shared.mobileNo
        lTransactions["Password"] = ok_password ?? ""
        lTransactions["PromoCodeId"] = ""
        lTransactions["SecureToken"] = UserLogin.shared.token
        lTransactions["LocalTransactionType"]  = "RPBONUSPAYTO"
        lTransactions["IsMectelTopUp"] = false
        lTransactions["ProductCode"] = ""
        lTransactions["Mode"] = true
        lTransactions["DestinationNumberWalletBalance"] = ""
        lTransactions["DiscountPayTo"] = 0
        lTransactions["IsPromotionApplicable"] = "0"
        lTransactions["KickBack"] = "0.0"
        let operatorName = PayToValidations().getNumberRangeValidation(self.enteredNumber.safelyWrappingString() ).operator
        let kickBackNumber = self.getKickbackTopupByOperator(opaerator: operatorName.lowercased())
        lTransactions["KickBackMsisdn"]         = kickBackNumber
        lTransactions["TransactionType"] = "PAYTO"
        lTransactions["MerchantName"] = ""
        lTransactions["OsType"] = "1"
        lTransactions["PromoCodeId"] = ""
        lTransactions["ResultCode"] = "0"
        lTransactions["ResultDescription"] = "Bonus Points"
        lTransactions["TransactionID"] = ""
        lTransactions["TransactionTime"] = ""
        lTransactions["accounType"] = ""
        lTransactions["senderBusinessName"] = ""
        lTransactions["userName"] = "Unknown"
        
        var finalParameterDictionaried = Dictionary<String,Any>()
        finalParameterDictionaried["TransactionsCellTower"] = TransactionsCellTower
        finalParameterDictionaried["lExternalReference"] = lExternalReference
        finalParameterDictionaried["lGeoLocation"] = lGeoLocation
        finalParameterDictionaried["lProximity"] = lProximity
        finalParameterDictionaried["lTransactions"] = lTransactions
        
        return finalParameterDictionaried
    }

}


class LoyaltyTopupConfirmationCell : UITableViewCell {
    
    @IBOutlet weak var keyLabel   : UILabel!
    {
        didSet
        {
            self.keyLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var valueLabel : UILabel!{
        didSet
        {
            self.valueLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    func wrapCell(key: String, value: String) {
        
        if key.lowercased() == "Buying Price".lowercased() {
            self.keyLabel.text = key.localized
            self.valueLabel.attributedText = self.attributedString(amount: value, sym: "MMK")
        } else if key.lowercased() == "Bonus Points".lowercased() {
            self.keyLabel.text = key.localized
            self.valueLabel.attributedText = self.attributedString(amount: value, sym: "PTS")
        } else if key.lowercased() == "Balance".lowercased() {
            self.keyLabel.text = key.localized
            self.valueLabel.textColor = .red
            self.valueLabel.attributedText = self.attributedString(amount: value, sym: "MMK")
        } else if key.lowercased() == "Buy Bonus Point".lowercased() {
            self.keyLabel.text = key.localized
            self.valueLabel.attributedText = self.attributedString(amount: value, sym: "PTS")
            self.valueLabel.textColor = .red
        }else if key.lowercased() == "Received Bonus Points".lowercased() {
            self.keyLabel.text = key.localized
            self.valueLabel.attributedText = self.attributedString(amount: value, sym: "PTS")
            self.valueLabel.textColor = .red
        }
            
        else {
            self.keyLabel.text = key.localized
            self.valueLabel.text = value
            self.valueLabel.textColor = .black
        }
    }
    
    private func attributedString(amount: String, sym: String) -> NSAttributedString {
        
        var amountStr = ""
        
        //        let amt = (amount as NSString).intValue
        //        let nsAmount = NSNumber.init(value: amt)
        
        guard let amt = Double(amount) else { return  NSMutableAttributedString()}
        let nsAmount = NSNumber(value: amt)
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NumberFormatter.Style.decimal
        currencyFormatter.maximumFractionDigits = 2
        currencyFormatter.minimumFractionDigits = 2
        
        
        if let amtnt  = currencyFormatter.string(from: nsAmount) {
            
            if amtnt.contains(find: ".") {
                amountStr = amtnt
            } else {
                amountStr = amtnt + ".00"
            }

            
        } else {
            if amount.contains(find: ".") {
                amountStr = amount
            } else {
                amountStr = amount + ".00"
            }
        }

        let mmk = [NSAttributedString.Key.font: UIFont(name: appFont, size: 12.0) ?? UIFont.systemFont(ofSize: 12.0)]
        let amtMMK = [NSAttributedString.Key.font: UIFont(name: appFont, size: 16.0) ?? UIFont.systemFont(ofSize: 16.0)]
        let mmkString = NSMutableAttributedString(string: sym, attributes: mmk)
        let amountString    = NSMutableAttributedString.init(string: amountStr, attributes: amtMMK)
        
        let stringAttributed = NSMutableAttributedString()
        stringAttributed.append(amountString)
        stringAttributed.append(NSAttributedString.init(string: " "))
        stringAttributed.append(mmkString)
        
        return stringAttributed
    }
    
}



