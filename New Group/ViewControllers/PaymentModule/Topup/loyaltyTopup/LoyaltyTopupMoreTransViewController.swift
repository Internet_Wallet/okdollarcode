//
//  LoyaltyTopupMoreTransViewController.swift
//  OK
//
//  Created by Ashish on 8/22/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol LoyaltyTopupMoreTransViewControllerDelegate : class {
    func didSelectMorePurchase()
    func didSelectInvoice()
    func didSelectShare()
}

class LoyaltyTopupMoreTransViewController: OKBaseController , UIGestureRecognizerDelegate{
    @IBOutlet weak var touchResitantView: CardDesignView!
    @IBOutlet weak var btnMorePurchase : UIButton!
    @IBOutlet weak var btnInvoice : UIButton!
    @IBOutlet weak var btnShare : UIButton!
    var isComingFromRecent =  false
    
    weak var delegate : LoyaltyTopupMoreTransViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideWhenTappedAround()

        self.btnMorePurchase.setTitle("More Purchase".localized, for: .normal)
        self.btnInvoice.setTitle("Invoice".localized, for: .normal)
        self.btnShare.setTitle("Share".localized, for: .normal)
        
        self.btnMorePurchase.addTargetClosure { [weak self](sender) in
            if let weak = self, let deleg = weak.delegate {
                weak.view.removeFromSuperview()
                deleg.didSelectMorePurchase()
            }
        }
        
        self.btnInvoice.addTargetClosure { [weak self](sender) in
            if let weak = self, let deleg = weak.delegate {
                weak.view.removeFromSuperview()
                deleg.didSelectInvoice()
            }
        }

        self.btnShare.addTargetClosure { [weak self](sender) in
            if let weak = self, let deleg = weak.delegate {
                weak.view.removeFromSuperview()
                deleg.didSelectShare()
            }
        }
    }
    
    func hideWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(dismissSelf))
        tap.delegate = self
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissSelf() {
        println_debug("removed called")
        removeView()
    }
    
    func removeView() {
        self.view.removeFromSuperview()
    }
    
    //MARK:- UIGestureRecognizerDelegate
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        let point = touch.location(in: self.view)
        if self.touchResitantView.frame.contains(point) {
            println_debug(self.touchResitantView.frame.contains(point))
            return false
        }
        return true
    }
}






















