//
//  LoyaltyTopupReceiptSingleViewController.swift
//  OK
//
//  Created by Ashish on 8/25/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI


class LoyaltyTopupReceiptSingleViewController: OKBaseController, UITabBarDelegate, MoreControllerActionDelegate, PaymentRateDelegate, CNContactViewControllerDelegate {
    
    @IBOutlet weak var rechargeNumber : UILabel!
    var isComingFromRecent =  false
    
    @IBOutlet weak var category : UILabel! {
        didSet {
            self.category.text = self.category.text?.localized
        }
    }
    @IBOutlet weak var topupAmount : UILabel! {
        didSet {
            self.topupAmount.text = self.topupAmount.text?.localized
        }
    }
    @IBOutlet weak var transID : UILabel! {
        didSet {
            self.transID.text = self.transID.text?.localized
        }
    }
    @IBOutlet weak var transType : UILabel! {
        didSet {
            self.transType.text = self.transType.text?.localized
        }
    }
    @IBOutlet weak var balancePoints : UILabel! {
        didSet {
            self.balancePoints.text = self.balancePoints.text?.localized
        }
    }
    
    @IBOutlet weak var cardView : UIView!
    
    @IBOutlet  var starArray: [UIImageView]!
    
    @IBOutlet weak var rate : UILabel!
    {
        didSet
        {
            rate.text = "Rate OK$ Services".localized
        }
    }
    @IBOutlet weak var rechargeAmount : UILabel!
    
     @IBOutlet weak var vCategory : UILabel!
     @IBOutlet weak var vtopupAmount : UILabel!
     @IBOutlet weak var vtransID : UILabel!
     @IBOutlet weak var vtransType : UILabel!
     @IBOutlet weak var vbalancePoints : UILabel!
    
    @IBOutlet weak var date : UILabel!
    @IBOutlet weak var time : UILabel!
    
    var confirmModel : BuyRedeemPointsModel?
    
    var displayNumber : String?
    
    var totalBonusAmount : String?
    var ratingDone = false
    var dictionary : Dictionary<String,Any>?
    var cashBackDestNum: String?
    
    @IBOutlet weak var viewTab: UIView!
    var viewOptions: TabOptionsOnReceipt?
    
    var isInFav = false
    var isInContact = false

//    @IBOutlet weak var rTabBar: UITabBar!
    
//    @IBOutlet weak var fav: UITabBarItem!
//    @IBOutlet weak var addContact: UITabBarItem!
//    @IBOutlet weak var home: UITabBarItem!
//    @IBOutlet weak var more: UITabBarItem!
//
    let optionTextDefaultColor = UIColor.init(hex: "162D9F")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            self.changeCancelImplementation()
        }
        //let color = UIColor.init(hex: "162D9F")
        //let attrFont = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.0) ?? UIFont.systemFont(ofSize: 11.0), NSAttributedString.Key.foregroundColor: color]
        
//        self.home.setTitleTextAttributes(attrFont, for: .normal)
//        self.more.setTitleTextAttributes(attrFont, for: .normal)
//        self.addContact.setTitleTextAttributes(attrFont, for: .normal)
//        self.fav.setTitleTextAttributes(attrFont, for: .normal)
        
//        self.fav.title = "Add Favorite".localized
//        self.home.title = "HomePagePT".localized
//        self.more.title = "More".localized
//        self.addContact.title = "TabbarAddContact".localized
        
        self.wrapData()
        
        guard let model = confirmModel else { return }
        
        let number   = self.getAgentCode(numberWithPrefix: displayNumber.safelyWrappingString(), havingCountryPrefix: "+95")
        let type     = "TOPUP"
        let amount   = model.buyingPrice ?? 0
        let name     = "Unknown"
        let bPoints  = "\(model.redeemPoints ?? 0)"
        let modelLog = PaymentTransactions.init(number, type: type, name: name, amount: String.init(format: "%d", amount), transID: self.vtransID.text ?? "", bonus: bPoints, backendNumber: cashBackDestNum)
        PaymentVerificationManager.storeTransactions(model: modelLog)
        TopupRecentContacts.storeTransactions(model: modelLog)
        LoyaltyTopupManager.storeTransactions(model: modelLog)
        GetProfile.updateBalance()
        
        isInFav = checkForFavorites(number, type: "TOPUPOTHER").isInFavorite
        isInContact = checkForContact(number).isInContact

        guard let bView = Bundle.main.loadNibNamed("TabOptionsOnReceipt", owner: self, options: nil)?.first as? TabOptionsOnReceipt else { return }
        viewOptions = bView
        bView.delegate = self
        viewOptions?.frame = CGRect(x: 0, y: 0, width: viewTab.frame.size.width, height: viewTab.frame.size.height)
        if let _ = viewOptions {
            viewTab.addSubview(viewOptions!)
            viewOptions!.lblHome.text = "HomePagePT".localized
            viewOptions!.lblMore.text = "More".localized
            viewOptions!.lblAddContact.text = "TabbarAddContact".localized
            viewOptions!.lblAddFav.text = "Add Favorite".localized
            addContactUI(isInContact: isInContact)
            addFavUI(isInFav: isInFav)
            viewOptions!.layoutIfNeeded()
        }
    }
    
    //MARK: - Methods
       func addFavUI(isInFav: Bool) {
           if let _ = viewOptions {
               viewOptions?.imgAddFav.image = viewOptions?.imgAddFav.image?.withRenderingMode(.alwaysTemplate)
               if isInFav {
                   viewOptions?.imgAddFav.tintColor = kYellowColor
                   viewOptions!.lblAddFav.textColor = kYellowColor
               } else {
                   viewOptions?.imgAddFav.tintColor = UIColor.gray
                   viewOptions!.lblAddFav.textColor = optionTextDefaultColor
               }
               viewOptions!.layoutIfNeeded()
           }
       }
       
       func addContactUI(isInContact: Bool) {
           if let _ = viewOptions {
               viewOptions?.imgAddContact.image = viewOptions?.imgAddContact.image?.withRenderingMode(.alwaysTemplate)
               if isInContact {
                   viewOptions?.imgAddContact.tintColor = kYellowColor
                   viewOptions!.lblAddContact.textColor = kYellowColor
               } else {
                   viewOptions?.imgAddContact.tintColor = UIColor.gray
                   viewOptions!.lblAddContact.textColor = optionTextDefaultColor
               }
               viewOptions!.layoutIfNeeded()
           }
       }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = PaytoConstants.global.attributesForTitle()
        self.title = "Receipt".localized
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.hidesBackButton = true
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .normal)
        UITabBar.appearance().tintColor = .lightGray
        UITabBar.appearance().barTintColor = .white
        UITabBar.appearance().unselectedItemTintColor = .darkGray
    }
    
    func wrapData() {
        guard let model = confirmModel else { return }
        
        let amount = String.init(format: "%d", model.redeemPoints ?? 0)
        self.rechargeAmount.attributedText = self.attributedString(amount: amount, sym: "POINTS")
            //amount + " " + "POINTS"
        
        self.rechargeNumber.text = self.displayNumber.safelyWrappingString()
        
        self.vCategory.text = "Other Number"
        
        let amountMMK = String.init(format: "%d", model.buyingPrice ?? 0)
        self.vtopupAmount.attributedText = self.attributedString(amount: amountMMK, sym: "MMK")
        
        self.vtransID.text = dictionary?.safeValueForKey("transid") as? String
        self.vtransType.text = "Top-Up Plan"
        
        var amnt = totalBonusAmount.safelyWrappingString()
        amnt = amnt.replacingOccurrences(of: ",", with: "")
        amnt = amnt.replacingOccurrences(of: " ", with: "")
        amnt = amnt.replacingOccurrences(of: "PTS", with: "")
        
        let balance = (amnt as NSString).floatValue - Float(model.redeemPoints ?? 0)
        self.vbalancePoints.attributedText = self.attributedString(amount: "\(balance)", sym: "PTS")

        let preciseTime  = (dictionary?.safeValueForKey("responsects") as? String ?? "").components(separatedBy: " ")
        
        self.date.text = preciseTime.first.safelyWrappingString()
        self.time.text = preciseTime.last.safelyWrappingString()

    }
    
    private func attributedString(amount: String, sym: String, forPdf: Bool = false) -> NSAttributedString {
        
        var amountStr = ""
        
        //        let amt = (amount as NSString).intValue
        //        let nsAmount = NSNumber.init(value: amt)
        
        guard let amt = Double(amount) else { return  NSMutableAttributedString()}
        let nsAmount = NSNumber(value: amt)
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NumberFormatter.Style.decimal
        if let amtnt  = currencyFormatter.string(from: nsAmount) {
            amountStr = amtnt
        } else {
            amountStr = amount
        }
        if forPdf {
            amountStr = ":    " + amountStr
        }
        let mmk = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.00) ?? UIFont.systemFont(ofSize: 11.00)]
        let amtMMK = [NSAttributedString.Key.font: UIFont(name: appFont, size: 14.0) ?? UIFont.systemFont(ofSize: 14.0)]
        let mmkString = NSMutableAttributedString(string: sym, attributes: mmk)
        let amountString    = NSMutableAttributedString.init(string: amountStr, attributes: amtMMK)
        
        let stringAttributed = NSMutableAttributedString()
        stringAttributed.append(amountString)
        stringAttributed.append(NSAttributedString.init(string: " "))
        stringAttributed.append(mmkString)
        
        return stringAttributed
    }
    
    @IBAction func openRate(_ sender: UIButton) {
        if !ratingDone {
            guard let ratingScreen = self.storyboard?.instantiateViewController(withIdentifier: String(describing: PaymentRateScreenViewController.self)) as? PaymentRateScreenViewController else { return }
            ratingScreen.modalPresentationStyle = .overCurrentContext
            ratingScreen.delegate = self
            ratingScreen.confirmationCellScreen = "YES"
            ratingScreen.destinationNumber = self.displayNumber.safelyWrappingString()
            self.navigationController?.present(ratingScreen, animated: true, completion: nil)
        } else {
            self.showToast(message: "Already feedback submitted".localized)
        }
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.tag == 31 {
            
            let favorite = FavoriteDBManager.init()
            let contacts = favorite.fetchRecordsForFavoriteContactsEntity()
            
            if contacts.filter({$0.phone.safelyWrappingString() == self.displayNumber.safelyWrappingString()}).count > 0 {
                PaytoConstants.alert.showToast(msg: "Contact  Already Added".localized)
            } else {
                guard let model = confirmModel else { return }
                let amount = String.init(format: "%d", model.buyingPrice ?? 0)
                let favVC = self.addFavoriteController(withName: "", favNum: self.displayNumber.safelyWrappingString(), type: "TOPUPOTHER", amount: amount)
                favVC?.modalPresentationStyle = .overCurrentContext
                if let vc = favVC {
                    self.navigationController?.present(vc, animated: false, completion: nil)
                }
            }
        } else if item.tag == 32 {
            let store     = CNContactStore()
            let contact   = CNMutableContact()
            let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue : displayNumber.safelyWrappingString()))
            contact.phoneNumbers = [homePhone]
            contact.givenName = "Unknown"
            let controller = CNContactViewController.init(forNewContact: contact)
            controller.contactStore = store
            controller.delegate     = self
            controller.title        = "Add Contact".localized
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.navigationBar.backgroundColor = UIColor.init(hex: "F3C632")
            self.navigationController?.navigationBar.tintColor = .white
            self.navigationController?.pushViewController(controller, animated: true)
        } else if item.tag == 33 {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoToDashboardMultiTopupLoyalty"), object: nil, userInfo: nil)
        } else if item.tag == 34 {
            guard  let vc = UIStoryboard.init(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PTMoreViewController") as? PTMoreViewController else {return}
            addChild(vc)
            vc.view.frame = self.view.bounds
            vc.didMove(toParent: self)
            vc.delegate = self
            vc.titleMorePayment   = "More Topup"
            vc.titleRepeatPayment = "Repeat Topup"
            
            let transition = CATransition()
            transition.duration = 0.2
            transition.type = CATransitionType.moveIn
            
            vc.view.layer.add(transition, forKey: nil)
            view.addSubview(vc.view)

        }
    }
    
    
    func moreAct() {
        guard  let vc = UIStoryboard.init(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PTMoreViewController") as? PTMoreViewController else {return}
        addChild(vc)
        vc.view.frame = self.view.bounds
        vc.didMove(toParent: self)
        vc.delegate = self
        vc.titleMorePayment   = "More Topup"
        vc.titleRepeatPayment = "Repeat Topup"
        
        let transition = CATransition()
        transition.duration = 0.2
        transition.type = CATransitionType.moveIn
        
        vc.view.layer.add(transition, forKey: nil)
        view.addSubview(vc.view)
    }
    
    func actionFor(option: MorePaymentAction) {
        switch option {
        case .morePayment:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MultiTopupReloadMorePayment"), object: nil, userInfo: nil)
            self.navigationController?.dismiss(animated: true, completion: nil)
        case .repeatPayment:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MultiTopupReloadMoreRepeatPayment"), object: nil, userInfo: nil)
            self.navigationController?.dismiss(animated: true, completion: nil)
        case .invoice:
            self.invoice()
            break
        case .share:
            self.rate.isHidden = true
            self.balancePoints.isHidden = true
            self.vbalancePoints.isHidden = true
            self.starArray.forEach { (imageView) in
                imageView.isHidden = true
            }

            sharingTaskCompletion {
                self.rate.isHidden = false
                self.starArray.forEach { (imageView) in
                    imageView.isHidden = false
                }
                self.balancePoints.isHidden  = false
                self.vbalancePoints.isHidden = false
            }
        }
    }
    
    public func sharingTaskCompletion(_ completion: @escaping () -> Void) {
        UIView.animate(withDuration: 0, animations: {
            let snapImage = self.cardView.snapshotOfCustomeView
            let vc = UIActivityViewController(activityItems: [snapImage], applicationActivities: [])
            DispatchQueue.main.async {self.present(vc, animated: true)}
        }, completion: { _ in
            completion()
        })
    }

    
    func ratingShow(_ rate: Int, commentText: String?) {
        
        DispatchQueue.main.async {
            for star in self.starArray {
                if star.tag <= rate {
                    UIView.animate(withDuration: 0.2, animations: {
                        star.image = #imageLiteral(resourceName: "receiptStarFilled")
                    })
                } else {
                    star.image = #imageLiteral(resourceName: "receiptStarUnfilled")
                }
            }
        }
        self.ratingDone = true
    }
    
    func invoice() {
        guard let model = confirmModel else { return }
        
        var pdfDictionary = Dictionary<String,Any>()
        pdfDictionary["senderAccName"]      = UserModel.shared.name
        
        var number = UserModel.shared.mobileNo
        _ = number.remove(at: String.Index.init(encodedOffset: 0))
        _ = number.remove(at: String.Index.init(encodedOffset: 0))
        number  = "+" + number
        number = number.replacingOccurrences(of: "+95", with: "0")
        
        var destination =  self.displayNumber.safelyWrappingString()
        destination = "(+95)" + " " + destination
        
        let amount = Float(model.buyingPrice ?? 0)
        let point  = Float(model.redeemPoints ?? 0)

        pdfDictionary["invoiceTitle"]       = "Transaction Receipt".localized
            pdfDictionary["senderAccNo"]      = "(+95)" + " " + number
            pdfDictionary["mobileNumberTopup"] = destination
            pdfDictionary["transactionID"]      = dictionary?.safeValueForKey("transid") as? String
            pdfDictionary["transactionType"]    = "Top-Up Plan"
        let dtStr = dictionary?.safeValueForKey("responsects") as? String
        if let dtVal = dtStr!.dateValue(dateFormatIs: "dd-MMM-yyyy HH:mm:ss") {
            let datStr = dtVal.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss")
            pdfDictionary["transactionDate"] = datStr
        } else {
            pdfDictionary["transactionDate"] = dtStr
        }
        
        //pdfDictionary["transactionDate"]    = dictionary?.safeValueForKey("responsects") as? String
        //pdfDictionary["amountBuyBonus"]          = self.attributedString(amount: String.init(format: "%.2f", amount), sym: "MMK")
        pdfDictionary["amountBuyBonus"] = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: String(amount)) + " MMK"
        pdfDictionary["Redeem Point"]      = self.attributedString(amount: String.init(format: "%.2f", point) , sym: "Points").string
            pdfDictionary["logoName"]         = "appIcon_Ok"

        guard let pdfUrl = CodeSnippets.createInvoicePDF(dictValues: pdfDictionary, pdfFile: "OK$ loyality TopUp Receipt") else {
            println_debug("Error - pdf not generated")
            return
        }
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "SharePDFViewController") as? SharePDFViewController else { return }
        vc.url = pdfUrl
        self.navigationController?.pushViewController(vc, animated: true)

    }

    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        if let newContact = contact {
            self.isInContact = true
            addContactUI(isInContact: true)
            okContacts.append(newContact)
        }
        viewController.delegate = nil
        self.navigationController?.popViewController(animated: true)
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.hidesBackButton = true
    }
    
    func changeCancelImplementation() {
        let originalSelector = Selector(("editCancel:"))
        let swizzledSelector = #selector(CNContactViewController.cancelHack)
        if let originalMethod = class_getInstanceMethod(object_getClass(CNContactViewController()), originalSelector),
           let swizzledMethod = class_getInstanceMethod(object_getClass(CNContactViewController()), swizzledSelector) {
            method_exchangeImplementations(originalMethod, swizzledMethod)
        }
    }

    
    func addFav(model: BuyRedeemPointsModel) {
        let favVC = self.addFavoriteController(withName: "Unknown", favNum: self.displayNumber.safelyWrappingString(), type: "TOPUPOTHER", amount: "\(model.buyingPrice ?? 0)")
        if let vc = favVC {
            vc.delegate = self
             vc.modalPresentationStyle = .overCurrentContext
            self.navigationController?.present(vc, animated: false, completion: nil)
        }
    }
    
    func addContacts(model: BuyRedeemPointsModel) {
        let store     = CNContactStore()
        let contact   = CNMutableContact()
        let number = self.displayNumber.safelyWrappingString()
        let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue : number ))
        contact.givenName = "Unknown"
        contact.phoneNumbers = [homePhone] //
        //contact.givenName = model.agentname.safelyWrappingString()
        let controller = CNContactViewController.init(forNewContact: contact)
        controller.contactStore = store
        controller.delegate     = self
        controller.title        = "Add Contact".localized
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.backgroundColor = UIColor.init(hex: "F3C632")
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
     func goToHome() {
        self.navigationController?.dismiss(animated: true, completion: nil)
         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoToDashboardMultiTopupLoyalty"), object: nil, userInfo: nil)
    }
    
    
    
}


//MARK:- PTFavoriteActionDelegate
extension LoyaltyTopupReceiptSingleViewController: PTFavoriteActionDelegate {
    func favoriteAdded() {
        self.isInFav = true
        addFavUI(isInFav: true)
    }
}



//MARK:- TabOptionProtocol
extension LoyaltyTopupReceiptSingleViewController: TabOptionProtocol {
    func addFavorite() {
        if let _ = viewOptions {
            if viewOptions!.lblAddFav.textColor != kYellowColor {
                if let otherModel = confirmModel  {
                    self.addFav(model: otherModel)
                }
            } else {
                PaytoConstants.alert.showToast(msg: "Contact already added in Favorites".localized)
            }
        }
    }
    
    func addContact() {
        if let _ = viewOptions {
            if viewOptions!.lblAddContact.textColor != kYellowColor {
                if let model = confirmModel {
                    self.addContacts(model: model)
                }
            } else {
                PaytoConstants.alert.showToast(msg: "Contact Already Added".localized)
            }
        }
    }
    
    func goHome() {
        self.goToHome()
    }
    
    func showMore() {
        self.moreAct()
    }
}

