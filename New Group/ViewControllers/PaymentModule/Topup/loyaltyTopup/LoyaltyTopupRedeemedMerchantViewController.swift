//
//  LoyaltyTopupRedeemedMerchantViewController.swift
//  OK
//
//  Created by Ashish on 8/17/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class LoyaltyTopupRedeemedMerchantViewController: OKBaseController, UITableViewDelegate, UITableViewDataSource {
    
    var header : String?
    
    var redeemModel : [BuyRedeemPointsModel]?
    
    @IBOutlet weak var redeemTable : UITableView!
    var isComingFromRecent =  false
    override func viewDidLoad() {
        super.viewDidLoad()
        redeemModel = redeemModel?.sorted(by: {($0.buyingPrice ?? 0) < ($1.buyingPrice ?? 0)})
        if self.redeemTable != nil {
            self.redeemTable.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = PaytoConstants.global.attributesForTitle()
        self.title = header.safelyWrappingString()
        self.backButton()
    }
    
    // back button init
    private func backButton() {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "tabBarBack"), for: UIControl.State.normal)
        button.tintColor = UIColor.white
        button.sizeToFit()
        button.addTargetClosure { [weak self](button) in
            guard let weakSelf = self else { return }
            weakSelf.navigationController?.popViewController(animated: true)
        }
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }

    
    //MARK:- TableView Delegate & DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.redeemModel == nil) ? 0 : self.redeemModel!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: LoyaltyTopupRedeemedCell.self)) as? LoyaltyTopupRedeemedCell
        if let subModel = self.redeemModel {
            cell?.wrapDetails(model: subModel[indexPath.row])
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180.00
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let subModel = self.redeemModel {
            guard let confirmation = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: LoyaltyTopupConfirmationViewController.self)) as? LoyaltyTopupConfirmationViewController else { return }
            guard let model = subModel[safe: indexPath.row] else { return }
            confirmation.confirmModel = model
            
            if let number = MultiTopupArray.main.controllers.first {
                if !PaymentVerificationManager.isValidPaymentTransactions("0095" + number.mobileNumber.text.safelyWrappingString(), "\(model.buyingPrice ?? 0)", "TOPUP") {
                    alertViewObj.wrapAlert(title: nil, body: "Same Number with Same Amount can not be paid before 2 minutes".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                    alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                    })
                    alertViewObj.showAlert()
                    return
                }
            }

            if (UserLogin.shared.walletBal as NSString).floatValue > Float(model.buyingPrice ?? 0) {
                if self.navigationController?.topViewController == self {
                    self.navigationController?.pushViewController(confirmation, animated: true)
                }
            } else {
                
                alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                    if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                        addWithdrawView.modalPresentationStyle = .fullScreen
                        self.navigationController?.present(addWithdrawView, animated: true, completion: nil)
                    }
                })
                alertViewObj.showAlert()

            }
        }
    }
    
}




class LoyaltyTopupRedeemedCell : UITableViewCell {
    
    @IBOutlet weak var amountPoint : UILabel!
    {
        didSet
        {
            self.amountPoint.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var amountMMK : UILabel!{
        didSet
        {
            self.amountMMK.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var time  : UILabel!{
        didSet
        {
            self.time.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var desc        : UILabel!{
        didSet
        {
            self.desc.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var shadowView  : UIView!
    
    func wrapDetails(model: BuyRedeemPointsModel) {
        guard let redeemPoint = model.redeemPoints else  { return }
        guard let buyingPrice = model.buyingPrice  else  { return }
        guard let timeValue   = model.expiryDate   else  { return }
        guard let description = model.description  else  { return }
        
        let pointString = String.init(format: "%d", redeemPoint)
        let mmkString   = String.init(format: "%d", buyingPrice)
        self.amountPoint.attributedText = self.attributedString(amount: pointString, sym: "OK$")
        self.amountMMK.attributedText   = self.attributedString(amount: mmkString, sym: "MMK")
        
//        self.time.text = self.getDateAndTime(timeStr: timeValue)
        
        var componentArray = description.components(separatedBy: " ")
        componentArray = componentArray.filter({$0 != " " })
        componentArray = componentArray.filter({$0 != "" })
        
        var desc : String = ""
        
        for string in componentArray {
            desc = desc + " " + string
        }
        
        self.desc.text = desc

    }
    
    private func attributedString(amount: String, sym: String) -> NSAttributedString {
        
        var amountStr = ""
        
        //        let amt = (amount as NSString).intValue
        //        let nsAmount = NSNumber.init(value: amt)
        
        guard let amt = Double(amount) else { return  NSMutableAttributedString()}
        let nsAmount = NSNumber(value: amt)
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NumberFormatter.Style.decimal
        if let amtnt  = currencyFormatter.string(from: nsAmount) {
            amountStr = amtnt
        } else {
            amountStr = amount
        }
        
        let mmk = [NSAttributedString.Key.font: UIFont(name: appFont, size: 14.00) ?? UIFont.systemFont(ofSize: 9.00)]
        let amtMMK = [NSAttributedString.Key.font: UIFont(name: appFont, size: 18.00) ?? UIFont.systemFont(ofSize: 11.00), NSAttributedString.Key.foregroundColor: kYellowColor]
        let mmkString = NSMutableAttributedString(string: sym, attributes: mmk)
        
        if amountStr.contains(find: ".") {
            
        } else {
            //amountStr.append(".00")
        }
        
        let amountString = NSMutableAttributedString.init(string: amountStr, attributes: amtMMK)

        
        let stringAttributed = NSMutableAttributedString()
        stringAttributed.append(amountString)
        stringAttributed.append(NSAttributedString.init(string: " "))
        stringAttributed.append(mmkString)
        
        return stringAttributed
    }
    
    private func getDateAndTime(timeStr: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        guard let time = dateFormatter.date(from: timeStr) else { return "" }
        dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss"//hh:mm a"
        let dateAndTime = dateFormatter.string(from: time)
        return dateAndTime
    }

}
