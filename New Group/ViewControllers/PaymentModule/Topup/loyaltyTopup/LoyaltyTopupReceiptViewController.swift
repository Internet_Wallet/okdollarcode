//
//  LoyaltyTopupReceiptViewController.swift
//  OK
//
//  Created by Ashish on 8/18/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class LoyaltyTopupReceiptViewController: OKBaseController, UITableViewDelegate, UITableViewDataSource, UITabBarDelegate, LoyaltyTopupMoreTransViewControllerDelegate {
    
    @IBOutlet weak var rTabBar: UITabBar!
    @IBOutlet weak var home: UITabBarItem!
    @IBOutlet weak var more: UITabBarItem!
    
    @IBOutlet weak var receiptName: UILabel!{
        didSet
        {
            self.receiptName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var amountHeade: UILabel!{
        didSet
        {
            self.amountHeade.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var receiptTable: UITableView!
    
    @IBOutlet weak var viewReceipt: UIView!
    
    var model : BuyRedeemPointsModel?
    var snapImage: UIImage?
    
    var receiptDictionary : Dictionary<String,Any>?
        
    var keys   : [String]?
    var values : [String]?
    
    var isComingFromRecent =  false

    override func viewDidLoad() {

        super.viewDidLoad()
        
        let color = UIColor.init(hex: "162D9F")
        let attrFont = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.0) ?? UIFont.systemFont(ofSize: 11.0), NSAttributedString.Key.foregroundColor: color]
        
        self.home.setTitleTextAttributes(attrFont, for: .normal)
        self.more.setTitleTextAttributes(attrFont, for: .normal)
        
        self.home.title = "HomePagePT".localized
        self.more.title = "More".localized
        
        guard let redeemModel = self.model else { return }
        guard let dictionary  = self.receiptDictionary else { return }
        self.wrapReceipt(model: redeemModel, withDictionary: dictionary)
        UIImageWriteToSavedPhotosAlbum(self.viewReceipt.snapshotOfCustomeView, nil, nil, nil)
        GetProfile.updateBalance()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationBar.titleTextAttributes = PaytoConstants.global.attributesForTitle()
        self.navigationController?.navigationBar.backgroundColor = MyNumberTopup.OperatorColorCode.okDefault
        self.title = "Receipt".localized
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.hidesBackButton = true
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .normal)
        UITabBar.appearance().tintColor = .lightGray
        UITabBar.appearance().barTintColor = .white
        UITabBar.appearance().unselectedItemTintColor = .darkGray
    }
    
    func wrapReceipt(model: BuyRedeemPointsModel, withDictionary dict: Dictionary<String,Any>) {
        
        guard let balanceWallet = dict.safeValueForKey("walletbalance") as? String else { return }
        guard let transactionID = dict.safeValueForKey("transid") as? String else { return }
        guard let buyBonusPoint = model.redeemPoints else { return }
        guard let amountHeader  = dict.safeValueForKey("amount") as? String else { return }
        guard let serverDate    = dict.safeValueForKey("responsects") as? String else { return }

        self.receiptName.text = UserModel.shared.name
        self.amountHeade.attributedText = self.attributedString(amount: amountHeader, sym: "MMK")
        
        self.keys = ["Transaction ID", "Transaction Type", "Received Bonus Points", "date"]
        self.values = [transactionID, "Buy Bonus Point", "\(buyBonusPoint)", serverDate]
        self.receiptTable.reloadData()
        self.view.layoutIfNeeded()
        snapImage = self.viewReceipt.snapshotOfCustomeView
        self.keys = ["Category", "Transaction ID", "Transaction Type", "Balance", "Received Bonus Points", "date"]
        self.values = ["Buy Bonus Point", transactionID, "Buy Bonus Point", balanceWallet, "\(buyBonusPoint)", serverDate]
        self.receiptTable.reloadData()
    }
    
    private func attributedString(amount: String, sym: String, forPdf: Bool = false) -> NSMutableAttributedString {
        
        var amountStr = ""
        
//        let amt = (amount as NSString).intValue
//        let nsAmount = NSNumber.init(value: amt)
        
        guard let amt = Double(amount) else { return  NSMutableAttributedString()}
        let nsAmount = NSNumber(value: amt)
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NumberFormatter.Style.decimal
        if let amtnt  = currencyFormatter.string(from: nsAmount) {
            
            if amtnt.contains(find: ".") {
                amountStr = amtnt
            } else {
                amountStr = amtnt + ".00"
            }
            
            
        } else {
            if amount.contains(find: ".") {
                amountStr = amount
            } else {
                amountStr = amount + ".00"
            }
        }
        if forPdf {
            amountStr = ":    " + amountStr
        }
        let mmk = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.00) ?? UIFont.systemFont(ofSize: 11.00)]
        let amtMMK = [NSAttributedString.Key.font: UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: 17.0)]
        let mmkString = NSMutableAttributedString(string: sym, attributes: mmk)
        let amountString    = NSMutableAttributedString.init(string: amountStr, attributes: amtMMK)
        
        let stringAttributed = NSMutableAttributedString()
        stringAttributed.append(amountString)
        stringAttributed.append(NSAttributedString.init(string: " "))
        stringAttributed.append(mmkString)
        
        return stringAttributed
    }

    
    //MARK:- UITableView DataSource & Delegates
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.values != nil) ? self.values!.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var identifierString = String.init(describing: LoyaltyTopupConfirmationCell.self)
        
        if let keys = self.keys, keys[indexPath.row].lowercased() == "date".lowercased() {
            identifierString = String.init(describing: LoyaltyTopupReceiptDateCell.self)
        } else {
            identifierString = String.init(describing: LoyaltyTopupConfirmationCell.self)
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifierString, for: indexPath)
        
        if let displayCell = cell as? LoyaltyTopupConfirmationCell {
            if let keys = self.keys, let values = self.values {
                let keyName   = keys[indexPath.row]
                let valueName = values[indexPath.row]
                displayCell.wrapCell(key: keyName, value: valueName)
            }
        }
        
        if let receiptCell = cell as? LoyaltyTopupReceiptDateCell {
            if let _ = self.keys, let values = self.values {
                let valueName = values[indexPath.row]
                receiptCell.wrapDate(serverDate: valueName)
            }
        }
        
        return cell
    }
    
    //MARK:- TABBAR DELEGATE
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.tag == 200 {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GoToDashboardLoyaltySellingTopup"), object: nil, userInfo: nil)
        } else if item.tag == 300 {
            guard  let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoyaltyTopupMoreTransViewController") as? LoyaltyTopupMoreTransViewController else {return}
            addChild(vc)
            vc.view.frame = self.view.bounds
            vc.didMove(toParent: self)
            vc.delegate = self
            
            let transition = CATransition()
            transition.duration = 0.2
            transition.type = CATransitionType.moveIn
            
            vc.view.layer.add(transition, forKey: nil)
            view.addSubview(vc.view)
        }
    }

    func didSelectMorePurchase() {
        guard let navigation = self.navigationController else { return }
        for controllers in navigation.viewControllers {
            if let vc = controllers as? LoyaltyTopupRedeemedMerchantViewController {
                navigation.popToViewController(vc, animated: true)
                return
            }
        }
    }

    
    func didSelectInvoice() {
        let senderObject = PTManagerClass.decodeMobileNumber(phoneNumber: UserModel.shared.mobileNo)
        let senderFormatted = String.init(format: "(%@) %@", senderObject.country.dialCode, UserModel.shared.formattedNumber)
        let transactionID = receiptDictionary?.safeValueForKey("transid") as? String ?? ""
        let redeemPoints  = Float((model != nil) ? model!.redeemPoints ?? 0 : 0)
        let balanceMMk   = Float((model != nil) ? model!.buyingPrice ?? 0 : 0)
        
        var pdfDictionary = Dictionary<String,Any>()
        
        pdfDictionary["invoiceTitle"]           = "Transaction Receipt".localized
        pdfDictionary["senderAccName"]        = UserModel.shared.name
        pdfDictionary["senderAccNo"]         = senderFormatted
        pdfDictionary["transactionID"]         = transactionID
        pdfDictionary["transactionType"]      = "Buy Bonus Point".localized
        pdfDictionary["Buy Bonus Point"]     = self.attributedString(amount: String.init(format: "%.2f", redeemPoints), sym: "PTS", forPdf: true)
        pdfDictionary["transactionDate"]     = receiptDictionary?.safeValueForKey("responsects") as? String ?? ""
        pdfDictionary["amount"]    = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: String(balanceMMk)) + " MMK"
        //pdfDictionary["amount"]    = self.attributedString(amount: String.init(format: "%.2f", balanceMMk), sym: "MMK", forPdf: true)
        //pdfDictionary["remarks"]           = receiptDictionary?.safeValueForKey("remarks") as? String ?? "Buy Bonus Point"
        pdfDictionary["logoName"]         = "appIcon_Ok"
        
        guard let pdfUrl = CodeSnippets.createInvoicePDF(dictValues: pdfDictionary, pdfFile: "OK$ LoyalityTopUp Receipt") else {
            println_debug("Error - pdf not generated")
            return
        }
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "SharePDFViewController") as? SharePDFViewController else { return }
        vc.url = pdfUrl
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func didSelectShare() {
        if let safeSnapImage = self.snapImage {
            let vc = UIActivityViewController(activityItems: [safeSnapImage], applicationActivities: [])
            DispatchQueue.main.async {self.present(vc, animated: true)}
        }
    }
    
    fileprivate func wrapAmount(key: String) -> String {
        let number = NSDecimalNumber(string: key)
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        if var num = formatter.string(from: number) {
            if num.contains(find: ".") {
                return (num == "NaN") ? "" : num
            } else {
                num = num + ".00"
                return (num.contains(find: "NaN")) ? "" : num
            }
        }
        return ""
    }

}


class LoyaltyTopupReceiptDateCell : UITableViewCell {
    
    @IBOutlet weak var dateString : UILabel!
    {
        didSet
        {
            self.dateString.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var timeString : UILabel!
    {
        didSet
        {
            self.timeString.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    func wrapDate(serverDate string : String) {
        let preciseTime  = string.components(separatedBy: " ")
        self.dateString.text = preciseTime.first.safelyWrappingString()
        self.timeString.text = preciseTime.last.safelyWrappingString()
    }
    
}
















