//
//  MultiTopupModel.swift
//  OK
//
//  Created by Ashish on 5/3/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

// Topup layout model
struct MultiTopupDesign {
    let model : [TopupViewController]?
}

// Retrieve MultiTopup Model
struct RetrieveMultiTopupRequest : Codable {
    let amount : String?
    let byCellID : Bool?
    let cellID : String?
    let customer : String?
    let lat : String?
    let longField : String?
    let mobileNumber : String?
    let operators : String?
    let planType : String?
    let type : String?
    
    enum CodingKeys: String, CodingKey {
        case amount = "Amount"
        case byCellID = "ByCellID"
        case cellID = "CellID"
        case customer = "Customer"
        case lat = "Lat"
        case longField = "Long"
        case mobileNumber = "MobileNumber"
        case operators = "Operator"
        case planType = "PlanType"
        case type = "Type"
    }
}


