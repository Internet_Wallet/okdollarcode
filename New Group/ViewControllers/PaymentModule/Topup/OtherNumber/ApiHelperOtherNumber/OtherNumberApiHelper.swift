//
//  OtherNumberApiHelper.swift
//  OK
//
//  Created by Ashish on 5/3/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class OtherNumberApi {
    
    func getOthernNumberMultiplePaymentServerNumberMapping(params: [RetrieveMultiTopupRequest], handle :@escaping (_ result: Any, _ success: Bool) -> Void) {
        guard let retrieveUrl = URL.init(string: TopupUrl.otherNumberTopupRetrieve) else { return }
        TopupWeb.genericClass(url: retrieveUrl, param: params.jsonString() as AnyObject, httpMethod: "POST") { (response, success) in
            handle(response, success)
        }
    }
    
    func getOtherNumberMultiPaymentCashbackRequest(jsonString: String, handle :@escaping (_ result: Any, _ success: Bool) -> Void) {
        guard let cashbackUrl = URL.init(string: TopupUrl.otherNumberMultiCashback) else { return }
        TopupWeb.genericClass(url: cashbackUrl, param: jsonString as AnyObject, httpMethod: "POST") { (response, success) in
            handle(response, success)
        }
    }

}

class MultiTopupParameters {

    func getMultiTopupSim(_ controllers: [TopupViewController], plan: String, type: String) -> [Dictionary<String,Any>]{
        
        var finalDict = [Dictionary<String,Any>]()
        
        for items in controllers {
          var subDictionary = Dictionary<String,Any>()
            var amount = items.amount.text
            amount = amount?.replacingOccurrences(of: ",", with: "")
            amount = amount?.replacingOccurrences(of: " MMK", with: "")
            
            subDictionary["Amount"] = amount
            subDictionary["ByCellID"] = "false"
            
            guard var mobileNumber = items.mobileNumber.text else { return  []}
            _ = mobileNumber.remove(at: String.Index.init(encodedOffset: 0))
            subDictionary["Customer"] = "0" + mobileNumber
            subDictionary["Lat"] = "16.816666"
            subDictionary["Long"] = "96.131"
            subDictionary["MobileNumber"] = UserModel.shared.mobileNo
            subDictionary["Operator"] = items.opName?.lowercased().capitalizedFirst()
            subDictionary["PlanType"] = items.plantype
            subDictionary["SubScriberName"] = telecoTopupSubscription?.first?.name ?? ""
            subDictionary["balance"] = UserLogin.shared.walletBal
            var tType = ""
            
            if items.plantype == "Data Plan" {
                tType = "1"
            } else if items.plantype == "Special Offers" {
                tType = "2"
            } else {
                tType = "0"
            }

            subDictionary["Type"] = tType
            finalDict.append(subDictionary)
        }
        println_debug(finalDict)
        return finalDict
    }
    
    func cashbackMultiApiParameters(mpModel: [TopupViewController], amount: String?, dictCashback: [Dictionary<String,Any>]) -> Dictionary<String,AnyObject> {
                
        var finalObject =  Dictionary<String,AnyObject>()
        var dict        = [Dictionary<String,String>]()
        
            for (_,dictionaryCashback) in dictCashback.enumerated() {
                var cashbackobj = Dictionary<String,String>()
                cashbackobj["Agentcode"]   = UserModel.shared.mobileNo
                let mobile = dictionaryCashback.safeValueForKey("MobileNumber") as? String
                let customerMobile = dictionaryCashback.safeValueForKey("mobileKey") as? String
                let amountMobile   = dictionaryCashback.safeValueForKey("amountKey") as? String
                
                var vc : TopupViewController?
                for element in mpModel {
                    if element.mobileNumber.text == customerMobile, element.amount.text == amountMobile {
                        vc = element
                    }
                }
                var amountA = (vc == nil) ? "" : vc!.amount.text
                amountA = amountA?.replacingOccurrences(of: ",", with: "")
                amountA = amountA?.replacingOccurrences(of: " MMK", with: "")
                cashbackobj["Amount"]      = amountA ?? ""
                
                cashbackobj["Clientip"]    = OKBaseController.getIPAddress()
                cashbackobj["Clientos"]    = "iOS"
                cashbackobj["Clienttype"]  = "GPRS"
                
                cashbackobj["Destination"]    = mobile ?? ""
                cashbackobj["Pin"]            = ok_password ?? ""
                cashbackobj["Requesttype"]    = "FEELIMITKICKBACKINFO"
                cashbackobj["Securetoken"]    = UserLogin.shared.token
                
                if vc?.operatorName.text?.lowercased() == "MecTel".lowercased() {
                    cashbackobj["Transtype"]      =  "TOPUP"
                } else {
                    cashbackobj["Transtype"]      =  "PAYTO"
                }
                cashbackobj["Vendorcode"]     = "IPAY"
                dict.append(cashbackobj)
            }
        
        var log = Dictionary<String,Any>()
        log["MobileNumber"] = UserModel.shared.mobileNo
        log["Msid"]         = UserModel.shared.msid
        log["Ostype"]       = 1
        log["Otp"]          = ""
        log["SimId"]        = uuid
        
        finalObject["CashBackRequestList"] = dict as AnyObject
        finalObject["Login"] = log as AnyObject
        println_debug(finalObject)
        return finalObject
    }
}

struct ResponseMultiTopupSim : Codable {
    let code : Int?
    let data : String?
    let msg : String?

    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case msg = "Msg"
    }
}

