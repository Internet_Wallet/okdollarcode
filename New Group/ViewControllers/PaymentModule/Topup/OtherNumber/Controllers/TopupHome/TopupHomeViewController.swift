//
//  TopupHomeViewController.swift
//  OK
//
//  Created by Ashish on 1/24/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class TopupHomeViewController: OKBaseController {
    
    var shortcutMenu: Int = 1
    //MARK: - Outlets
    @IBOutlet weak var btnTopUp: UIButton!
        {
        didSet
        {
            if appFont == "Myanmar3"{
                self.btnTopUp.titleLabel?.font = UIFont(name: appFont, size: 12.0)
            }else{
                self.btnTopUp.titleLabel?.font = UIFont(name: appFont, size: 14.0)
            }
            
            self.btnTopUp.setTitle("TOP-UP", for: .normal)
            
        }
    }
    @IBOutlet weak var btnDataPlan: UIButton!
        {
        didSet
        {
            if appFont == "Myanmar3"{
                self.btnDataPlan.titleLabel?.font = UIFont(name: appFont, size: 12.0)
            }else{
                self.btnDataPlan.titleLabel?.font = UIFont(name: appFont, size: 14.0)
            }
            self.btnDataPlan.setTitle("DATA PLAN".localized, for: .normal)
        }
    }
    @IBOutlet weak var btnSpecialOffers: UIButton!
        {
        didSet
        {
            if appFont == "Myanmar3"{
                self.btnSpecialOffers.titleLabel?.font = UIFont(name: appFont, size: 12.0)
            }else{
                self.btnSpecialOffers.titleLabel?.font = UIFont(name: appFont, size: 14.0)
            }
            self.btnSpecialOffers.setTitle("SPECIAL OFFERS".localized, for: .normal)
        }
    }
    @IBOutlet weak var tableViewObjectHolder: UITableView!
    @IBOutlet weak var submitButton: UIButton!{
        didSet
        {
            self.submitButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var containerSubmitButton: CardDesignView!
    
    var selectedObject: (String, String) = ("", "")
    
    //MARK: Ads Image
    @IBOutlet weak var advView: UIView!
    @IBOutlet weak var adViewImage: UIImageView!
    @IBOutlet weak var heightConstraints: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraints: NSLayoutConstraint!
    @IBOutlet weak var addButton: UIButton!
    
    //MARK: - Properties
    var favoriteTouple: (phone: String,name: String)?
    var arrayFavorite : [(phone: String,name: String, amount: String, hide: Bool)]?
    var viewArray = [TopupViewController]() {
        didSet {
            
            MultiTopupArray.main.wrapControllers(viewArray)
            self.advView.isHidden = true
            if MultiTopupArray.main.controllers.count > 1 {
                self.changeColor(color: MyNumberTopup.OperatorColorCode.okDefault, name: "")
            } else if MultiTopupArray.main.controllers.count == 1 {
                if self.viewArray.first?.mobileNumber != nil {
                    let mobileNumber = self.viewArray.first?.mobileNumber.text ?? ""
                    let rangeCheck = PayToValidations().getNumberRangeValidation(mobileNumber)
                    let colorChange = PayToValidations().getColorForOperator(operatorName: rangeCheck.operator)
                    if mobileNumber.count > 0 {
                        self.changeColor(color: colorChange, name: rangeCheck.operator)
                    } else {
                        self.changeColor(color: MyNumberTopup.OperatorColorCode.okDefault, name: "")
                        
                    }
                }
            }
            
            if self.viewArray.count > 0, self.viewArray.count < 2 {
                if let last = viewArray.last {
                    if let label = last.rechargeHeaderLabel {
                        label.text = ""
                    }
                }
            }
            
        }
    }
    
    //MARK: Variables for multi topup
    var cashBackdictionary = [Dictionary<String,Any>]()
    var kickBackInfoDictionary = Dictionary<String,Any>()
    var arrayKickBackInfo = [TopupConfirmationModel]()
    var cellArray = [TopupUITableViewCell]()
    
    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadAdvertisement()
        println_debug("TopupHomeViewController")
        //        NotificationCenter.default.addObserver(self, selector: #selector(TopupHomeViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(TopupHomeViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
        //        IQKeyboardManager.sharedManager().enable = true
        tableViewObjectHolder.isScrollEnabled = false
        self.backButton(withTitle: "Other Number".localized)
        containerSubmitButton.isHidden = true
        
        self.submitButton.setTitle("SUBMIT".localized, for: .normal)
        
        if let array = arrayFavorite {
            self.viewArray.removeAll()
            MultiTopupArray.main.controllers.removeAll()
            for favoriteTouple in array {
                
                guard let firstUI = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TopupViewController.self))
                    as? TopupViewController else {return}
                firstUI.view.frame = .zero
                firstUI.firstObject = false
                firstUI.decodeContact(ph: favoriteTouple.phone, name: favoriteTouple.name, amount: favoriteTouple.amount, hide: favoriteTouple.hide)
                firstUI.navigation = self.navigationController
                firstUI.confimMobile.isUserInteractionEnabled = true
                self.viewArray.append(firstUI)
                firstUI.delegate = self
            }
            self.tableViewObjectHolder.isScrollEnabled = true
            self.loadMultiCell()
            self.tableViewObjectHolder.tableFooterView = UIView.init()
            self.showAddObjectButton(show: false)
            
        } else if let _ = favoriteTouple {
            guard let firstUI = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TopupViewController.self))
                as? TopupViewController else {return}
            firstUI.view.frame = .zero
            firstUI.firstObject = false
            firstUI.decodeContact(ph: favoriteTouple?.phone ?? "", name: favoriteTouple?.name ?? "")
            firstUI.navigation = self.navigationController
            firstUI.confimMobile.isUserInteractionEnabled = false
            self.viewArray.append(firstUI)
            firstUI.delegate = self
            self.loadCell()
            self.tableViewObjectHolder.tableFooterView = UIView.init()
            self.showAddObjectButton(show: true)
            self.didShowAdvertisement(show: false)
            self.updateNavColor(string: favoriteTouple?.phone)
        } else {
            guard let firstUI = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TopupViewController.self))
                as? TopupViewController else {return}
            firstUI.delegate = self
            firstUI.firstObject = true
            firstUI.navigation = self.navigationController
            firstUI.view.frame = firstUI.view.frame
            self.viewArray.append(firstUI)
            self.loadCell()
            self.didShowAdvertisement(show: true)
            self.tableViewObjectHolder.tableFooterView = UIView.init()
            if firstUI.mobileNumber != nil {
                firstUI.mobileNumber.becomeFirstResponder()
            }
        }
        
        self.navigationItem.rightBarButtonItem = nil
        self.navigationController?.navigationBar.barTintColor = MyNumberTopup.OperatorColorCode.okDefault
        NotificationCenter.default.addObserver(self, selector: #selector(updateModel(_:)), name: Notification.Name(rawValue: "TopupFavMultipleObjects"), object: nil)
        
//        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        
    }
    
    @objc func appMovedToBackground() {
        performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
    }
    
    @objc func updateModel(_ notification: Notification) {
        DispatchQueue.main.async {
            var count = 0
            var touple = [(String,String, String, Bool)]()
            if let data = notification.userInfo as? [Int: Any] {
                for (_, score) in data {
                    if let obj = score as? (String, String, String, Bool) {
                        touple.append((obj.0, obj.1, obj.2, obj.3))
                    }
                }
            }
            count = touple.count
            if self.viewArray.count == 1 {
                if touple.count > 0 {
                    guard let favoriteTouple =  touple.first else { return }
                    guard let firstUI = self.viewArray.first else { return }
                    self.viewArray.removeLast()
                    firstUI.view.frame = .zero
                    firstUI.firstObject = false
                    
                    firstUI.decodeContact(ph: favoriteTouple.0, name: favoriteTouple.1, amount: favoriteTouple.2, hide: favoriteTouple.3)
                    firstUI.navigation = self.navigationController
                    firstUI.confimMobile.isUserInteractionEnabled = false
                    firstUI.rechargeHeaderLabel.text = "Recharge".localized + " " + "1"
                    self.viewArray.append(firstUI)
                    firstUI.delegate = self
                }
                self.tableViewObjectHolder.isScrollEnabled = true
                self.loadMultiCell()
                self.tableViewObjectHolder.tableFooterView = UIView.init()
                if count > 1 {
                    self.showAddObjectButton(show: false)
                } else {
                    self.showAddObjectButton(show: true)
                }
                self.tableViewObjectHolder.scrollToBottom(animated: true)
                touple.removeFirst()
            } else {
                self.viewArray.removeLast()
                self.tableViewObjectHolder.reloadData()
            }
            if touple.count > 0 {
                for (index, favoriteTouple) in touple.enumerated() {
                    
                    guard let firstUI = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TopupViewController.self))
                        as? TopupViewController else {return}
                    firstUI.view.frame = .zero
                    firstUI.firstObject = false
                    firstUI.decodeContact(ph: favoriteTouple.0, name: favoriteTouple.1, amount: favoriteTouple.2, hide: favoriteTouple.3)
                    firstUI.navigation = self.navigationController
                    firstUI.confimMobile.isUserInteractionEnabled = false
                    firstUI.rechargeHeaderLabel.text = "Recharge".localized + " " + "\(index + 1)"
                    self.viewArray.append(firstUI)
                    firstUI.delegate = self
                }
                self.tableViewObjectHolder.isScrollEnabled = true
                self.loadMultiCell()
                self.tableViewObjectHolder.tableFooterView = UIView.init()
                if count > 1 {
                    self.showAddObjectButton(show: false)
                } else {
                    self.showAddObjectButton(show: true)
                }
                self.tableViewObjectHolder.scrollToBottom(animated: true)
            }
            
        }
    }
    
    private func updateNavColor(string: String?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            var str = ""
            str = string ?? ""
            if str.count > 0 {
                str = str.replacingOccurrences(of: "(", with: "")
                str = str.replacingOccurrences(of: ")", with: "")
                str = str.trimmingCharacters(in: .whitespaces)
            }
            if str.hasPrefix("00") {
                _ = str.remove(at: String.Index.init(encodedOffset: 0))
                _ = str.remove(at: String.Index.init(encodedOffset: 0))
                str = "+" + str
            }
            let country = self.identifyCountry(withPhoneNumber: str)
            if country.0 == "+95" ||  country.1 == "myanmar" ||  country.0 == "" ||  country.1 == "" {
                let countryObject = Country.init(name: "", code: "myanmar", dialCode: "+95")
                if str.hasPrefix(countryObject.dialCode) {
                    str = str.replacingOccurrences(of: countryObject.dialCode, with: "")
                    str = "0" + str
                } else {
                    if str.hasPrefix("0") {
                        
                    } else {
                        str = "0" + str
                    }
                }
                
                let number = PayToValidations().getNumberRangeValidation(str)
                
                if str.count > 3 {
                    let colorChange = PayToValidations().getColorForOperator(operatorName: number.operator)
                    self.changeColor(color: colorChange, name: number.operator)
                } else {
                    self.changeColor(color: MyNumberTopup.OperatorColorCode.okDefault, name: "")
                }
            }
        }
    }
    
    func identifyCountry(withPhoneNumber prefix: String) -> (String, String) {
        let countArray = revertCountryArray() as! Array<NSDictionary>
        var finalCodeString = ""
        var flagCountry = ""
        for dic in countArray {
            if let code = dic .object(forKey: "CountryCode") as? String, let flag = dic.object(forKey: "CountryFlagCode") as? String{
                if prefix.hasPrefix(code) {
                    finalCodeString = code
                    flagCountry = flag
                }
            }
        }
        return (finalCodeString,flagCountry)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideShowSubmitButton()
        NotificationCenter.default.addObserver(self, selector: #selector(TopupHomeViewController.dismissMyTopupScreen), name: NSNotification.Name(rawValue: "GoToDashboardMultiTopup"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TopupHomeViewController.dismissMyTopupScreen), name: NSNotification.Name(rawValue: "GoToDashboardMultiTopupLoyalty"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TopupHomeViewController.morepayment), name: NSNotification.Name(rawValue: "MultiTopupReloadMorePayment"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TopupHomeViewController.moreRepeatpayment), name: NSNotification.Name(rawValue: "MultiTopupReloadMoreRepeatPayment"), object: nil)
        self.initNotifications()
        if self.viewArray.count >= 5 {
            
        } else {
            if let _ = arrayFavorite {
                if let lastFramePhNo = viewArray.last?.amount.text, lastFramePhNo.count > 0 {
                    self.containerSubmitButton.isHidden = false
                } else {
                    self.containerSubmitButton.isHidden = true
                }
            } else {
                var showAddBtn = true
                for (index, object) in self.viewArray.enumerated() {
                    if index != self.viewArray.count - 1 {
                        if (object.amount.text?.count ?? 0) == 0 {
                            showAddBtn = true
                            break
                        }
                    } else {
                        if let lastFramePhNo = object.mobileNumber.text, lastFramePhNo.count > 0 {
                            showAddBtn = false
                        }
                    }
                }
                self.containerSubmitButton.isHidden = showAddBtn
            }
            if self.viewArray.last?.mobileNumber.text == "09" {
                self.containerSubmitButton.isHidden = true
            }
            if MultiTopupArray.main.controllers.count > 0 {
                MultiTopupArray.main.controllers.first!.view.isUserInteractionEnabled = true
            }
            if MultiTopupArray.main.controllers.count > 4 {
                self.containerSubmitButton.isHidden = true
                MultiTopupArray.main.controllers.first!.view.isUserInteractionEnabled = false
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.destroyNotification()
    }
    
    
    func loadAdvertisement() {
        let randomIndex = Int(arc4random_uniform(UInt32(advImagesUrl.AdImageData.count)))
        if let urlString = advImagesUrl.AdImageData[safe: randomIndex] {
            DispatchQueue.main.async {
                self.adViewImage.image = UIImage(data: urlString)
                self.adViewImage.contentMode = .scaleAspectFill
                //                self.adViewImage.setImage(url: urlString, contentMode: UIView.ContentMode.scaleAspectFit, placeholderImage: nil)
            }
        }
    }
    
    func backButton(withTitle title:String){
        
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "tabBarBack"), for: UIControl.State.normal)
        
        button.tintColor = UIColor.white
        
        button.sizeToFit()
        button.addTarget(self, action: #selector(dismissController), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        
        self.navigationItem.title = title
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 17.00) ?? UIFont.systemFont(ofSize: 21), NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    //MARK: Topup MultiView
    func loadCell() {
        self.cellArray.removeAll()
        guard let lastObject = self.viewArray.last else { return }
        for (index,topupView) in self.viewArray.enumerated() {
            topupView.view.frame = topupView.view.frame
            if topupView == lastObject {
                topupView.view.isUserInteractionEnabled = true
            }
            addChild(topupView)
            guard let cell = self.tableViewObjectHolder.dequeueReusableCell(withIdentifier: String(describing: TopupUITableViewCell.self)) as? TopupUITableViewCell else { return }
            topupView.rechargeHeader = "Recharge".localized + " " + "\(index + 1)"
            topupView.rechargeHeaderLabel.text = "Recharge".localized + " " + "\(index + 1)"
            //            topupView.headerUpdate()
            cell.wrap(topupView)
            topupView.didMove(toParent: self)
            self.cellArray.append(cell)
        }
        
        if self.viewArray.count > 0, self.viewArray.count < 2 {
            if let last = viewArray.last {
                if let label = last.rechargeHeaderLabel {
                    label.text = ""
                }
                
                last.view.frame = last.view.frame
                last.mobileNumber.isUserInteractionEnabled = true
                last.confimMobile.isUserInteractionEnabled = true
                last.amount.isUserInteractionEnabled = true
                
                if let amount = last.amount, let text = amount.text, text.count > 0 {
                    self.containerSubmitButton.isHidden = false
                    if MultiTopupArray.main.controllers.count > 4 {
                        self.containerSubmitButton.isHidden = true
                    }
                }
            }
        }
    }
    
    func loadMultiCell() {
        self.cellArray.removeAll()
        for (index,topupView) in self.viewArray.enumerated() {
            addChild(topupView)
            guard let cell = self.tableViewObjectHolder.dequeueReusableCell(withIdentifier: String(describing: TopupUITableViewCell.self)) as? TopupUITableViewCell else { return }
            topupView.rechargeHeader = "Recharge".localized + " " + "\(index + 1)"
            cell.wrap(topupView)
            topupView.didMove(toParent: self)
            topupView.view.isUserInteractionEnabled = true
            self.cellArray.append(cell)
            self.reloadObjectHeight(obj: self.viewArray[index])
        }
        self.tableViewObjectHolder.reloadData()
    }
    
    private func addNewPostPaidNum(newObject: TopupViewController) {
        for otherObjects in self.viewArray {
            if otherObjects != newObject {
                if let amount = otherObjects.amount.text, amount.count > 0 {
                    if let lastAmount = newObject.amount.text,let number = otherObjects.mobileNumber.text,let lastNumber = newObject.mobileNumber.text  {
                        if lastAmount.hasPrefix(amount), number.hasPrefix(lastNumber) {
                            alertViewObj.wrapAlert(title: nil, body: "You cannot Topup same number with same amount ".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
                            alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                                newObject.amount.text = ""
                                newObject.amount.rightView = nil
                                self.submitButton.isHidden = true
                                self.showAddObjectButton(show: true)
                            })
                            alertViewObj.showAlert()
                            return
                        }
                    }
                }
            }
        }
        
        if  let _ = newObject.mobileNumber.text {
            //            if cnfText.hasPrefix(mobileText) || newObject.confimMobile.isUserInteractionEnabled == false {
            
            if let amount = newObject.amount.text, amount.count > 0 {
                guard let firstUI = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TopupViewController.self))
                    as? TopupViewController else {return}
                firstUI.delegate = self
                firstUI.navigation = self.navigationController
                self.viewArray.append(firstUI)
                self.loadCell()
                self.tableViewObjectHolder.reloadData()
            } else {
                if var text = newObject.mobileNumber.text, text.count > 0 {
                    let _ = text.remove(at: String.Index.init(encodedOffset: 0))
                    let mobileNumber = "0095" + text
                    if let nav = self.navigationController {
                        MyNumberTopup().navigateToAmountSelectionScreen(mobile: mobileNumber, delegate: self, present: nav, otherNumber: true)
                    }
                } else {
                    alertViewObj.wrapAlert(title: nil, body: "Please enter the mobile number".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
                    alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                        if newObject.confimMobile.isHidden {
                            newObject.mobileNumber.becomeFirstResponder()
                        }
                        //                            else {
                        //                                newObject.confimMobile.becomeFirstResponder()
                        //                            }
                    })
                    alertViewObj.showAlert()
                }
                //                }
            }
        }
    }
    
    //MARK: Extra check for cdma number
    func extraCheckForCDMA(obj: TopupViewController) {
        guard let customerNumber = obj.mobileNumber.text else { return }
        let newObject = obj
        let urlString = String.init(format: TopupUrl.cdmaCheck, customerNumber)
        guard let url = URL.init(string: urlString) else { return }
        TopupWeb.genericClass(url: url, param: Dictionary<String,Any>() as AnyObject, httpMethod: "GET") { (response, success) in
            if success {
                DispatchQueue.main.async {
                    if let dictionary = response as? Dictionary<String,Any> {
                        if let code = dictionary.safeValueForKey("Code") as? Int, code == 300 {
                            if let last = self.viewArray.last, (last.amount.text?.count ?? 0 == 0) {
                                alertViewObj.wrapAlert(title: nil, body: "Postpaid mobile number cannot recharge. You can only recharge if your mobile number is Prepaid".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
                                alertViewObj.addAction(title: "Cancel".localized, style: .cancel, action: {
                                    self.showAddObjectButton(show: true)
                                })
                                alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                                    self.addNewPostPaidNum(newObject: newObject)
                                    
                                })
                                alertViewObj.showAlert()
                            } else {
                                self.addNewPostPaidNum(newObject: newObject)
                            }
                        } else if let code = dictionary.safeValueForKey("Code") as? Int, code == 200 {
                            self.addNewPostPaidNum(newObject: newObject)
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func addNewObject(_ sender: UIButton) {
        
        self.containerSubmitButton.isHidden = true
        
        var money: Float = 0.0
        
        for element in MultiTopupArray.main.controllers {
            if let amountField = element.amount {
                if let amountText = amountField.text {
                    money = money + (amountText as NSString).floatValue
                }
            }
        }
        
        if money > (UserLogin.shared.walletBal as NSString).floatValue {
            if let element = MultiTopupArray.main.controllers.last {
                if let amountField = element.amount {
                    amountField.text = ""
                    return
                }
            }
        }
        
        self.addButton.isHidden = false
        
        guard let newObject = self.viewArray.last else { return }
        
        if let textMobileNumber = newObject.mobileNumber.text, textMobileNumber.hasPrefix("095") {
            
            self.extraCheckForCDMA(obj: newObject)
            
        } else {
            for otherObjects in self.viewArray {
                if otherObjects != newObject {
                    if let amount = otherObjects.amount.text, amount.count > 0 {
                        if let lastAmount = newObject.amount.text,let number = otherObjects.mobileNumber.text,let lastNumber = newObject.mobileNumber.text  {
                            if lastAmount.hasPrefix(amount), number.hasPrefix(lastNumber) {
                                alertViewObj.wrapAlert(title: nil, body: "You cannot Topup same number with same amount ".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
                                alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                                    newObject.amount.text = ""
                                    newObject.amount.rightView = nil
                                    self.submitButton.isHidden = true
                                    self.showAddObjectButton(show: true)
                                })
                                alertViewObj.showAlert()
                                return
                            }
                        }
                    }
                }
            }
            
            if let _ = newObject.mobileNumber.text {
                
                //                if cnfText.hasPrefix(mobileText) || newObject.confimMobile.text!.containsAlphabets() {
                
                if let amount = newObject.amount.text, amount.count > 0 {
                    if MultiTopupArray.main.controllers.count == 5 {
                        alertViewObj.wrapAlert(title: nil, body: "Maximum Payment Reached ".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
                        alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                            self.addButton.isHidden = true
                            self.containerSubmitButton.isHidden = true
                        })
                        alertViewObj.showAlert()
                    } else {
                        guard let firstUI = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TopupViewController.self))
                            as? TopupViewController else {return}
                        firstUI.delegate = self
                        firstUI.navigation = self.navigationController
                        self.viewArray.append(firstUI)
                        self.loadCell()
                        self.tableViewObjectHolder.reloadData()
                    }
                } else {
                    if var text = newObject.mobileNumber.text, text.count > 0 {
                        let _ = text.remove(at: String.Index.init(encodedOffset: 0))
                        let mobileNumber = "0095" + text
                        if let nav = self.navigationController {
                            MyNumberTopup().navigateToAmountSelectionScreen(mobile: mobileNumber, delegate: self, present: nav, otherNumber: true, index: .topup, isPlusSelected: true)
                        }
                    } else {
                        alertViewObj.wrapAlert(title: nil, body: "Please enter the mobile number".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
                        alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                            if newObject.confimMobile.isHidden {
                                newObject.mobileNumber.becomeFirstResponder()
                            } else {
                                newObject.confimMobile.becomeFirstResponder()
                            }
                        })
                        alertViewObj.showAlert()
                    }
                }
                //                }
            }
        }
    }
    
    
    
    func amount(_ amount: String, withPlanType type: String) { //added
        guard let lastObject = MultiTopupArray.main.controllers.last else { return }
        
        lastObject.amount.text = amount
        
        let label   = UILabel.init(frame: .zero)
        label.text  = type
        label.font  = lastObject.amount.font
        label.textColor = .lightGray
        let size   = type.size(withAttributes: [.font: label.font])
        let vWidth = size.width
        
        label.frame = CGRect.init(x: 0, y: 0, width: vWidth +  25.00, height: lastObject.amount.frame.height)
        
        lastObject.plantype = type
        
        lastObject.opName = MyNumberTopup.opName
        
        lastObject.amount.rightViewMode = .always
        
        lastObject.amount.rightView = label
        
        lastObject.firstObject = false
        
        if MultiTopupArray.main.controllers.count == 5 {
            self.containerSubmitButton.isHidden = true
            self.addButton.isHidden = true
        } else {
            guard let firstUI = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TopupViewController.self))
                as? TopupViewController else {return}
            firstUI.delegate    = self
            firstUI.firstObject = false
            firstUI.navigation  = self.navigationController
            self.viewArray.append(firstUI)
            self.loadCell()
            self.tableViewObjectHolder.reloadData()
            self.hideShowSubmitButton()
            
            self.tableViewObjectHolder.scrollToBottom(animated: true)
        }
        
    }
    
    func hideShowSubmitButton() {
        guard let lastObject = self.viewArray.last else { return }
        guard let lastText   = lastObject.amount.text else { return }
        
        if lastText.count > 0 {
            self.submitButton.isHidden = false
        } else {
            self.submitButton.isHidden = true
        }
    }
    
    //MARK: - Target Methods
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            println_debug("notification: Keyboard will show")
            if bottomConstraints.constant == 0 {
                bottomConstraints.constant = keyboardSize.height
            }
        }
    }
    
    @objc  func keyboardWillHide(notification: Notification) {
        if let _ = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if bottomConstraints.constant != 0 {
                bottomConstraints.constant = 0
            }
        }
    }
    
    @objc func dismissMyTopupScreen() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func morepayment() {
        self.viewArray.removeAll()
        self.cellArray.removeAll()
        MultiTopupArray.main.controllers.removeAll()
        guard let firstUI = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TopupViewController.self))
            as? TopupViewController else {return}
        firstUI.delegate = self
        firstUI.firstObject = true
        firstUI.navigation = self.navigationController
        self.viewArray.append(firstUI)
        self.loadCell()
        self.tableViewObjectHolder.tableFooterView = UIView.init()
        
        self.tableViewObjectHolder.reloadData()
    }
    
    @objc func moreRepeatpayment(notification: NSNotification) {
        if let userInfo = notification.userInfo as? [String: String] {
            if let number = userInfo.safeValueForKey("key") {
                guard let firstObject = self.viewArray.first(where: {$0.mobileNumber.text! == number}) else { return }
                self.viewArray.removeAll()
                MultiTopupArray.main.controllers.removeAll()
                self.viewArray.append(firstObject)
                self.loadCell()
                self.tableViewObjectHolder.tableFooterView = UIView.init()
                self.tableViewObjectHolder.reloadData()
                
                firstObject.amount.text = ""
                firstObject.amount.rightView = nil
                
                firstObject.hideFirstCellItem()
                self.reloadObjectHeight(obj: firstObject)
            }
        } else {
            guard let firstObject = self.viewArray.first else { return }
            firstObject.amount.text = ""
            self.viewArray.removeAll()
            MultiTopupArray.main.controllers.removeAll()
            self.viewArray.append(firstObject)
            self.loadCell()
            self.tableViewObjectHolder.tableFooterView = UIView.init()
            self.tableViewObjectHolder.reloadData()
            
            firstObject.amount.text = ""
            firstObject.amount.rightView = nil
            
            firstObject.hideFirstCellItem()
            self.reloadObjectHeight(obj: firstObject)
        }
    }
    
    @objc private func dismissController() {
        self.view.endEditing(true)
        MultiTopupArray.main.controllers.removeAll()
//
        
        if shortcutMenu == 1{
            performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
        }
        else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK:- Button Action Methods
    @IBAction func sliderButtonAction(_ sender: UIButton) {
        guard let lastObject = self.viewArray.last else { return }
        let numberValid = PayToValidations().getNumberRangeValidation(lastObject.mobileNumber.text ?? "")
        if let mobileNumber = lastObject.mobileNumber.text, mobileNumber.count >= numberValid.min {
            //            if !lastObject.confimMobile.isUserInteractionEnabled {
            if let navigation = self.navigationController, var number = lastObject.mobileNumber.text {
                _ = number.remove(at: String.Index.init(encodedOffset: 0))
                let topupNumber = "0095" + number
                
                var type = TopupButtonEnum.topup
                if sender == self.btnTopUp {
                    type = TopupButtonEnum.topup
                } else if sender == self.btnDataPlan {
                    type = TopupButtonEnum.dataPlan
                } else if sender == self.btnSpecialOffers {
                    type = TopupButtonEnum.specialOffer
                }
                MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: topupNumber, delegate: lastObject, present: navigation, otherNumber: true, index: type)
            }
            return
            //            }
        } else {
            self.showErrorAlert(errMessage: "Please enter a valid mobile number".localized)
            return
        }
        //        if lastObject.confimMobile.isHidden == true {
        //
        //            alertViewObj.wrapAlert(title: nil, body: "Please enter the mobile number".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
        //            alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
        //            })
        //            alertViewObj.showAlert()
        //            return
        //
        //
        //        }else {
        
        //        if let confirmText = lastObject.confimMobile.text, confirmText.hasPrefix(lastObject.mobileNumber.text ?? ""), confirmText.count > 2 {
        //
        //        } else {
        //            if let confirmText = lastObject.confimMobile.text, confirmText.containsAlphabets() {
        //
        //            } else {
        //                alertViewObj.wrapAlert(title: nil, body: "Please enter confirm mobile number".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
        //                alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
        //                })
        //                alertViewObj.showAlert()
        //                return
        //            }
        //        }
        //        }
        
        if let text = lastObject.amount.text, text.count > 0 {
            if let navigation = self.navigationController, var number = lastObject.mobileNumber.text {
                _ = number.remove(at: String.Index.init(encodedOffset: 0))
                let topupNumber = "0095" + number
                var type = TopupButtonEnum.topup
                if sender == self.btnTopUp {
                    type = TopupButtonEnum.topup
                } else if sender == self.btnDataPlan {
                    type = TopupButtonEnum.dataPlan
                } else if sender == self.btnSpecialOffers {
                    type = TopupButtonEnum.specialOffer
                }
                //                if lastObject.mobileNumber.text!.hasPrefix(lastObject.confimMobile.text!) {
                
                MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: topupNumber, delegate: lastObject, present: navigation, otherNumber: true, index: type)
                //                } else {
                //                    if let confirmText = lastObject.confimMobile.text, confirmText.containsAlphabets() {
                //
                //                    } else {
                //                        self.showErrorAlert(errMessage: "Entered Mobile Number and Confirmation Mobile number must be same".localized)
                //                        return
                //                    }
                //                }
            }
            return
        } else {
            if let navigation = self.navigationController, var number = lastObject.mobileNumber.text {
                _ = number.remove(at: String.Index.init(encodedOffset: 0))
                let topupNumber = "0095" + number
                var type = TopupButtonEnum.topup
                if sender == self.btnTopUp {
                    type = TopupButtonEnum.topup
                } else if sender == self.btnDataPlan {
                    type = TopupButtonEnum.dataPlan
                } else if sender == self.btnSpecialOffers {
                    type = TopupButtonEnum.specialOffer
                }
                //                if lastObject.mobileNumber.text!.hasPrefix(lastObject.confimMobile.text!) {
                
                MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: topupNumber, delegate: lastObject, present: navigation, otherNumber: true, index: type)
                //                } else {
                //
                //                    if let confirmText = lastObject.confimMobile.text, confirmText.containsAlphabets() {
                //
                //                    } else {
                //                        self.showErrorAlert(errMessage: "Entered Mobile Number and Confirmation Mobile number must be same".localized)
                //                        return
                //                    }
                //
                //                }
            }
        }
    }
    
    //MARK:- Submit Button
    @IBAction func submitButtonAction(_ sender: UIButton) {
        if appDelegate.checkNetworkAvail() {
            self.multiTopupSimCall()
        }
    }
    
    //MARK:- Notification Observers
    
    @IBOutlet weak var submitBottomConst : NSLayoutConstraint!
    
    func initNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardOpen), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardClose), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func destroyNotification() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardOpen(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            //submitBottomConst.constant = keyboardHeight
            self.view.layoutIfNeeded()
        }
    }
    
    @objc private func keyboardClose() {
        submitBottomConst.constant = 0.0
        self.view.layoutIfNeeded()
    }
    
    
    deinit {
        println_debug("Dealloc called in other number topup")
    }
}
