//
//  TopupHomeExtensionViewController.swift
//  OK
//
//  Created by Ashish on 12/14/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit

//MARK: - CommonTopupTransactionViewControllerDelegate
extension TopupHomeViewController: CommonTopupTransactionViewControllerDelegate {
    func didSelectBackOptionFromConfromation() {
        if MultiTopupArray.main.controllers.count < 1, MultiTopupArray.main.controllers.count > 0 {
            guard let vc = MultiTopupArray.main.controllers.last else { return }
            if vc.amount != nil {
                vc.amount.text = ""
                vc.amount.rightView = nil
            }
        }
    }
    
    func didSelectBackOptionFromSingleConfirmation() {
        if MultiTopupArray.main.controllers.count < 1, MultiTopupArray.main.controllers.count > 0 {
            guard let vc = MultiTopupArray.main.controllers.last else { return }
            if vc.mobileNumber != nil {
                if let navigation = self.navigationController, var number = vc.mobileNumber.text {
                    _ = number.remove(at: String.Index.init(encodedOffset: 0))
                    let topupNumber = "0095" + number
                    MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: topupNumber, delegate: vc, present: navigation, otherNumber: true)
                }
            }
        }
    }
    
    func didSelectBackOption(type: String) {
        if let last = self.viewArray.last {
            if let amount = last.amount {
                amount.text = ""
                amount.rightView = nil
            }
        }
        if type == "0" {
            self.btnTopUp.sendActions(for: .touchUpInside)
        } else if type == "1" {
            self.btnDataPlan.sendActions(for: .touchUpInside)
        } else if type == "2" {
            self.btnSpecialOffers.sendActions(for: .touchUpInside)
        }
    }
    
    //MARK:- API Call For Multi Payment
    func multiTopupSimCall() {
        
        if MultiTopupArray.main.controllers.count > 1 {
            guard let url = URL.init(string: TopupUrl.otherNumberTopupRetrieve) else { return }
            let params = MultiTopupParameters().getMultiTopupSim(MultiTopupArray.main.controllers, plan: "Top-Up Plan", type: "0")
            
            let jsonParam = JSONStringFromAnyObject(value: params as AnyObject)
            
            println_debug("Check Agent url : \(url)")
            println_debug(jsonParam)
            
            TopupWeb.genericClass(url: url, param: jsonParam as AnyObject, httpMethod: "POST") { [weak self](response, success) in
                if success {
                    DispatchQueue.main.async {
                        if let json = response as? [Dictionary<String,Any>] {
                            self?.cashBackdictionary.removeAll()
                            for (index,element) in json.enumerated() {
                                if let str = element.safeValueForKey("Data") as? String {
                                    println_debug("otherNumberTopupRetrieve\(str)")
                                    if element.safeValueForKey("Code") as? Int == 300 {
                                        if let object = MultiTopupArray.main.controllers[safe: index] {
                                            if object.operatorName.text?.lowercased() == "MecTel".lowercased() {
                                                var first = Dictionary<String,Any>()
                                                first["mobileKey"] = MultiTopupArray.main.controllers[safe: index]?.mobileNumber.text
                                                var number = MultiTopupArray.main.controllers[safe: index]?.mobileNumber.text
                                                _ = number?.removeFirst()
                                                if let num = number {
                                                    first["MobileNumber"] = "0095" + num
                                                }
                                                first["amountKey"] = MultiTopupArray.main.controllers[safe: index]?.amount.text
                                                self?.cashBackdictionary.append(first)
                                            }
                                        }
                                    }
                                    if let dict = OKBaseController.convertToArrDictionary(text: str) {
                                        if let arrDict = dict as? [Dictionary<String,Any>] {
                                            if var first = arrDict.first {
                                                first["mobileKey"] = MultiTopupArray.main.controllers[safe: index]?.mobileNumber.text
                                                first["amountKey"] = MultiTopupArray.main.controllers[safe: index]?.amount.text
                                                self?.cashBackdictionary.append(first)
                                            }
                                        }
                                    }
                                }
                            }
                            guard let weakSelf = self else { return }
                            if weakSelf.cashBackdictionary.count == MultiTopupArray.main.controllers.count {
                                println_debug(self?.cashBackdictionary)
                                if UserLogin.shared.loginSessionExpired {
                                    OKPayment.main.authenticate(screenName: "multiCashback", delegate: weakSelf)
                                } else {
                                    self?.cashbackApiCallingForOtherNumber()
                                }
                            } else {
                                alertViewObj.wrapAlert(title: nil, body: "Please Try Again!".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
                                alertViewObj.addAction(title: "OK", style: .cancel, action: {
                                })
                                alertViewObj.showAlert()
                            }
                        }
                    }
                }
            }
        } else {
            if MultiTopupArray.main.controllers.first?.operatorName.text?.lowercased() == "MecTel".lowercased() {
                self.cashBackdictionary.removeAll()
                var first = Dictionary<String,Any>()
                first["mobileKey"] = MultiTopupArray.main.controllers.first?.mobileNumber.text
                var number = MultiTopupArray.main.controllers.first?.mobileNumber.text
                _ = number?.removeFirst()
                if let num = number {
                    first["MobileNumber"] = "0095" + num
                }
                first["amountKey"] = MultiTopupArray.main.controllers.first?.amount.text
                self.cashBackdictionary.append(first)
                if UserLogin.shared.loginSessionExpired {
                    OKPayment.main.authenticate(screenName: "cashbackSingleNumber", delegate: self)
                } else {
                    self.cashbackApiCallingSingleNumber()
                }
            } else {
                
                let lat  = (GeoLocationManager.shared.currentLatitude == nil) ? "0.0" : GeoLocationManager.shared.currentLatitude
                let long = (GeoLocationManager.shared.currentLongitude == nil) ? "0.0" : GeoLocationManager.shared.currentLongitude
                var urlString = ""
                
                guard let items = MultiTopupArray.main.controllers.first else { return }
                let textAmount = items.amount.text
                guard let text = textAmount?.replacingOccurrences(of: ",", with: "") else { return }
                let model = RechargePlanModel.init(text)
                guard var customer = items.mobileNumber.text else { return }
                _ = customer.remove(at: String.Index.init(encodedOffset: 0))
                let number  = "0" + customer
                
                var tType = ""
                if items.plantype == "Data Plan" {
                    tType = "1"
                } else if items.plantype == "Special Offers" {
                    tType = "2"
                } else {
                    tType = "0"
                }
                
                
                urlString = String.init(format: TopupUrl.merchantNumberCheck, MyNumberTopup.opName,UserModel.shared.mobileNo,tType,number,model.developerAmount, long ?? "0.0", lat ?? "0.0", UserLogin.shared.walletBal)
                
                // extra param added for lTopupPath
                
                if model.selectedIndex.count > 0 {
                    urlString = urlString + "&lTopupPath=" + model.selectedIndex
                }
                urlString = urlString + "&SubScriberName=" + (telecoTopupSubscription?.first?.name ?? "").replacingOccurrences(of: " ", with: "%20")
                println_debug(urlString)
                guard let url = URL.init(string: urlString) else { return }
                let par = Dictionary<String,String>()
                TopupWeb.genericClass(url: url, param: par as AnyObject, httpMethod: "GET") { [weak self] (response, success) in
                    if success {
                        DispatchQueue.main.async {
                            if let json = response as? Dictionary<String,Any> {
                                if let code = json["Code"] as? NSNumber, code == 200 {
                                    self?.cashBackdictionary.removeAll()
                                    if let msg = json["Msg"] as? String, msg.lowercased() == "Success".lowercased() {
                                        if let data = json["Data"] as? String {
                                            if let dict = OKBaseController.convertToArrDictionary(text: data) {
                                                if let arrDict = dict as? [Dictionary<String,Any>] {
                                                    println_debug(arrDict)
                                                    if var first = arrDict.first {
                                                        first["mobileKey"] = MultiTopupArray.main.controllers.first?.mobileNumber.text ?? ""
                                                        first["amountKey"] = MultiTopupArray.main.controllers.first?.amount.text ?? ""
                                                        self?.cashBackdictionary.append(first)
                                                        self?.selectedObject.1 = (first["amountKey"] as? String ?? "").replacingOccurrences(of: ",", with: "")
                                                        self?.selectedObject.0 = first["MobileNumber"] as? String ?? ""
                                                    }
                                                }
                                            }
                                            
                                            guard let weakSelf = self else { return }
                                            if weakSelf.cashBackdictionary.count > 0 {
                                                println_debug(self?.cashBackdictionary)
                                                if UserLogin.shared.loginSessionExpired {
                                                    OKPayment.main.authenticate(screenName: "cashbackSingleNumber", delegate: weakSelf)
                                                } else {
                                                    weakSelf.cashbackApiCallingSingleNumber()
                                                }
                                            }else {
                                                alertViewObj.wrapAlert(title: nil, body: "Please Try Again!".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
                                                alertViewObj.addAction(title: "OK", style: .cancel, action: {
                                                })
                                                alertViewObj.showAlert()
                                            }
                                        }
                                    }
                                } else if let code = json["Code"] as? NSNumber, code == 300 {
                                    self?.showErrorAlert(errMessage: "Your Service Provider is Currently Unavailable.".localized)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    // for multi topup
    func cashbackApiCallingForOtherNumber() {
        println_debug("cashbackApiCallingForOtherNumber")
        let url = URL.init(string: TopupUrl.otherNumberMultiCashback)
        let param = JSONStringFromAnyObject(value: MultiTopupParameters().cashbackMultiApiParameters(mpModel: MultiTopupArray.main.controllers, amount: "", dictCashback: self.cashBackdictionary) as AnyObject)
        
        println_debug("Check Agent url : \(String(describing: url))")
        println_debug(param)

        
        TopupWeb.genericClass(url: url!, param: param as AnyObject, httpMethod: "POST") { [weak self](response, successfull) in
            if successfull {
                DispatchQueue.main.async {
                    if let value = response as? [Dictionary<String,AnyObject>] {
                        guard let weakSelf = self else { return }
                        self?.arrayKickBackInfo.removeAll()
                        for (index,item) in value.enumerated() {
                            let obj = item["Data"] as? String ?? ""
                            let xml = SWXMLHash.parse(obj)
                            self?.kickBackInfoDictionary.removeAll()
                            self?.enumerate(indexer: xml)
                            
                            if let transaction = weakSelf.kickBackInfoDictionary.safeValueForKey("resultdescription") as? String, transaction == "Transaction Successful" {
                                let number = (weakSelf.kickBackInfoDictionary.safeValueForKey("destination") as? String) == nil ? "" : (weakSelf.kickBackInfoDictionary.safeValueForKey("destination") as? String)
                                let numberMobile = self?.cashBackdictionary[safe: index]?.safeValueForKey("mobileKey") as? String ?? ""
                                let amountCheck = self?.cashBackdictionary[safe: index]?.safeValueForKey("amountKey") as? String ?? ""
                                
                                var num = ""
                                for index in numberMobile {
                                    num = num + String(index)
                                    if num == "0095" {
                                        num = "0"
                                    }
                                }
                                
                                var elementScan: TopupViewController?
                                for element in MultiTopupArray.main.controllers {
                                    if element.mobileNumber.text == num, element.amount.text == amountCheck {
                                        elementScan = element
                                    }
                                }
                                
                                let operatorName = (elementScan == nil) ? "" : elementScan!.opName
                                let type         = (elementScan == nil) ? "" : elementScan!.plantype
                                let cashback = (weakSelf.kickBackInfoDictionary.safeValueForKey("kickback") as? String) == nil ? "" : (weakSelf.kickBackInfoDictionary.safeValueForKey("kickback") as? String)
                                let amount   = (weakSelf.kickBackInfoDictionary.safeValueForKey("amount") as? String) == nil ? "" : (weakSelf.kickBackInfoDictionary.safeValueForKey("amount") as? String)
                                
                                var nameUser = "Unknown"
                                if let obj = elementScan, obj.confimMobile.text!.containsAlphabets() {
                                    nameUser = obj.confimMobile.text ?? nameUser
                                }
                                
                                if MultiTopupArray.main.controllers.count == 1 && !PaymentVerificationManager.isValidPaymentTransactionsForOperator(operatorName ?? "", amountCheck, number ?? "") {
                                    self?.cashbackApiCallingForOtherNumber()
                                    return
                                }
                                
//                                //Remove alert for TopupHomeExtensionViewController
//                                DispatchQueue.main.async {
//                                    alertViewObj.wrapAlert(title: "Backend Number", body: number ?? "".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
//                                    alertViewObj.addAction(title: "OK", style: .cancel, action: {
//                                    })
//                                    alertViewObj.showAlert()
//                                }
                                
                                let payNum = elementScan?.mobileNumber.text ?? ""
                                
                                let model = TopupConfirmationModel.init(nameUser, number: number ?? "", opeName: operatorName ?? "", type: type ?? "", cashbk: cashback ?? "", topAmt: amount ?? "", payNumber: payNum)
                                if elementScan != nil {
                                    self?.arrayKickBackInfo.append(model)
                                }
                            } else {
                                alertViewObj.wrapAlert(title: nil, body: weakSelf.kickBackInfoDictionary.safeValueForKey("resultdescription") as? String ?? "PLease Try Again!".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
                                alertViewObj.addAction(title: "OK", style: .cancel, action: {
                                })
                                alertViewObj.showAlert()
                            }
                        }
                        if weakSelf.arrayKickBackInfo.count > 0 {
                            guard let vc = weakSelf.storyboard?.instantiateViewController(withIdentifier: "CommonTopupTransactionViewController") as? CommonTopupTransactionViewController else { return }
                            vc.arrDictionary = [weakSelf.kickBackInfoDictionary]
                            vc.delegate = self
                            vc.modelArray = weakSelf.arrayKickBackInfo
                            vc.cashBackMerchantNumber = weakSelf.cashBackdictionary
                            weakSelf.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
            }
        }
    }
    
    func cashbackApiCallingSingleNumber() {
        var urlString = ""
        if MultiTopupArray.main.controllers.first?.operatorName.text?.lowercased() == "MecTel".lowercased() {
            guard let items = MultiTopupArray.main.controllers.first else { return }
            guard var customer = items.mobileNumber.text else { return }
            _ = customer.remove(at: String.Index.init(encodedOffset: 0))
            let number = "0095" + customer
            let textAmount = items.amount.text?.replacingOccurrences(of: ",", with: "") ?? ""
            urlString = String.init(format: TopupUrl.cashback,UserModel.shared.mobileNo, number , textAmount, ok_password ?? "","TOPUP",UserLogin.shared.token)
        } else {
            urlString = String.init(format: TopupUrl.cashback,UserModel.shared.mobileNo, selectedObject.0, selectedObject.1,ok_password ?? "","PAYTO",UserLogin.shared.token)
        }
        println_debug(urlString)
        
        let url = URL.init(string: urlString)
        let par = Dictionary<String,String>()
        TopupWeb.genericClassXML(url: url!, param: par as AnyObject, httpMethod: "POST") { [weak self](respponse, success) in
            if success {
                if let xmlResponse = respponse as? String {
                    self?.parsingFeeLimitKickbackResponse(xml: xmlResponse)
                }
            } else {
                println_debug(respponse)
            }
        }
    }




    func parsingFeeLimitKickbackResponse(xml: String) {
        self.arrayKickBackInfo.removeAll()
        let xml = SWXMLHash.parse(xml)
        self.enumerate(indexer: xml)
        println_debug(kickBackInfoDictionary)
        DispatchQueue.main.async {
            
            if let transaction = self.kickBackInfoDictionary.safeValueForKey("resultdescription") as? String, transaction == "Transaction Successful" {
                
                let number = (self.kickBackInfoDictionary.safeValueForKey("destination") as? String) == nil ? "" : (self.kickBackInfoDictionary.safeValueForKey("destination") as? String)
                let numberMobile = self.cashBackdictionary.first?.safeValueForKey("mobileKey") as? String ?? ""
                let amountCheck = self.cashBackdictionary.first?.safeValueForKey("amountKey") as? String ?? ""
                
                var num = ""
                for index in numberMobile {
                    num = num + String(index)
                    if num == "0095" {
                        num = "0"
                    }
                }
                
                let  elementScan = MultiTopupArray.main.controllers.first
//                for element in MultiTopupArray.main.controllers {
//                    if element.mobileNumber.text == num, element.amount.text == amountCheck {
//                        elementScan = element
//                    }
//                }
                
                let operatorName = (elementScan == nil) ? "" : (elementScan?.opName ?? "")
                let type         = (elementScan == nil) ? "" : (elementScan?.plantype ?? "")
                let cashback = (self.kickBackInfoDictionary.safeValueForKey("kickback") as? String) == nil ? "" : (self.kickBackInfoDictionary.safeValueForKey("kickback") as? String)
                let amount   = (self.kickBackInfoDictionary.safeValueForKey("amount") as? String) == nil ? "" : (self.kickBackInfoDictionary.safeValueForKey("amount") as? String)
                
                var nameUser = "Unknown"
                if let obj = elementScan, obj.confimMobile.text!.containsAlphabets() {
                    nameUser = obj.confimMobile.text ?? nameUser
                }
                
                if MultiTopupArray.main.controllers.count == 1 && !PaymentVerificationManager.isValidPaymentTransactionsForOperator(operatorName, amountCheck, number ?? "") {
                    self.cashbackApiCallingSingleNumber()
                    return
                }
                
                let payNum = elementScan?.mobileNumber.text ?? ""
                let model = TopupConfirmationModel.init(nameUser, number: number ?? "", opeName: operatorName, type: type, cashbk: cashback ?? "", topAmt: amount ?? "", payNumber: payNum)
                if elementScan != nil {
                    self.arrayKickBackInfo.append(model)
                }
            } else {
                alertViewObj.wrapAlert(title: nil, body: self.kickBackInfoDictionary.safeValueForKey("resultdescription") as? String ?? "PLease Try Again!".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
                alertViewObj.addAction(title: "OK", style: .cancel, action: {
                })
                alertViewObj.showAlert()
            }
            if self.arrayKickBackInfo.count > 0 {
                // PaymentVerificationManager.storeTransactions(model: PaymentTransactions.init(UserModel.shared.mobileNo, type: "Top-Up Plan", name: "", amount: amount ?? "", opName: operatorName))
                guard  let vc = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: CommonTopupTransactionViewController.self)) as? CommonTopupTransactionViewController else { return }
                vc.arrDictionary = [self.kickBackInfoDictionary]
                vc.delegate = self
                println_debug(self.arrayKickBackInfo)
                vc.modelArray = self.arrayKickBackInfo
                vc.cashBackMerchantNumber = self.cashBackdictionary
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }

  
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            kickBackInfoDictionary[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
}

//MARK: - requestMoneyAmountCallback
extension TopupHomeViewController: requestMoneyAmountCallback {
    func didGetAmount(_ amount: String, withPlanType type: String) {
        if MultiTopupArray.main.controllers.count > 1 {
            guard let last = MultiTopupArray.main.controllers[safe: MultiTopupArray.main.controllers.count - 1] else { return }
            for (_, lastObject) in MultiTopupArray.main.controllers.enumerated() {
                if last.mobileNumber.text == lastObject.mobileNumber.text && amount == lastObject.amount.text {
                    alertViewObj.wrapAlert(title: nil, body: "Same Mobile Number with Same Top-Up amount, Same Data Plan or Same Special Offer Not Allowed".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                    alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                    })
                    alertViewObj.showAlert()
                    return
                }
            }
        }
        for (_, lastObject) in self.viewArray.enumerated() {
            //guard let lastObject = MultiTopupArray.main.controllers.last else { return }
            if lastObject.amount.text != "" {
                
            } else {
                lastObject.amount.text = amount
                let label   = UILabel.init(frame: .zero)
                label.text  = type
                label.font  = lastObject.amount.font
                label.textColor = .lightGray
                let size   = type.size(withAttributes: [.font: label.font])
                let vWidth = size.width
                label.frame = CGRect.init(x: 0, y: 0, width: vWidth +  25.00, height: lastObject.amount.frame.height)
                lastObject.plantype = type
                lastObject.opName = MyNumberTopup.opName
                lastObject.amount.rightViewMode = .always
                lastObject.amount.rightView = label
                lastObject.firstObject = false
                DispatchQueue.main.async {
                    if MultiTopupArray.main.controllers.count == 5 {
                        self.containerSubmitButton.isHidden = true
                        self.addButton.isHidden = true
                    } else {
                        guard let firstUI = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TopupViewController.self))
                            as? TopupViewController else {return}
                        firstUI.delegate    = self
                        firstUI.firstObject = false
                        firstUI.navigation  = self.navigationController
                        self.viewArray.append(firstUI)
                        self.loadCell()
                        //            self.tableViewObjectHolder.beginUpdates()
                        self.tableViewObjectHolder.reloadData()
                        self.hideShowSubmitButton()
                        //            self.tableViewObjectHolder.scrollToBottom(animated: true)
                        self.showAddObjectButton(show: false)
                        if let last = self.viewArray.last {
                            last.mobileNumber.becomeFirstResponder()
                        }
                        //            self.tableViewObjectHolder.endUpdates()
                    }
                }
                break
            }
        }
    }
    
    func paymentCallOtherNumberThroughDelegate() {
        
    }
    
    func paymentCallOtherNumberWithCustomMsg(index: String) {
        
        let lat  = (GeoLocationManager.shared.currentLatitude == nil) ? "0.0" : GeoLocationManager.shared.currentLatitude
        let long = (GeoLocationManager.shared.currentLongitude == nil) ? "0.0" : GeoLocationManager.shared.currentLongitude
        guard let items = self.viewArray.first else { return }
        guard var customer = items.mobileNumber.text else { return }
        _ = customer.remove(at: String.Index.init(encodedOffset: 0))
        let number  = "0" + customer
        
        let operatorName = items.opName!.lowercased().capitalizedFirst()
        var amount = items.amount.text
        amount = amount?.replacingOccurrences(of: ",", with: "")
        amount = amount?.replacingOccurrences(of: " MMK", with: "")
        amount = amount?.replacingOccurrences(of: " ", with: "")
        if let amntStr = amount, var amntInt = Int(amntStr) {
            if index == "2" {
                amntInt += 1
                amount = "\(amntInt)"
            }
        }
        var urlString = String.init(format: TopupUrl.merchantNumberCheck, operatorName,UserModel.shared.mobileNo,rechargeType.topup,number,amount ?? "0", long ?? "0.0", lat ?? "0.0", UserLogin.shared.walletBal)
        urlString = urlString + "&lTopupPath=" + index
        urlString = urlString + "&SubScriberName=" + (telecoTopupSubscription?.first?.name ?? "").replacingOccurrences(of: " ", with: "%20")
        let url = URL.init(string: urlString)
        
        println_debug("Check Agent url : \(urlString)")

        let par = Dictionary<String,String>()
        TopupWeb.genericClass(url: url!, param: par as AnyObject, httpMethod: "GET") { [weak self] (response, success) in
            if success {
                if let json = response as? Dictionary<String,Any> {
                    if let code = json["Code"] as? NSNumber, code == 200 {
                        if let msg = json["Msg"] as? String, msg == "Success" {
                            if let data = json["Data"] as? String {
                                if let dict = OKBaseController.convertToArrDictionary(text: data) {
                                    if let arrDict = dict as? [Dictionary<String,Any>] {
                                        if var first = arrDict.first {
                                            DispatchQueue.main.async {
                                                first["mobileKey"] = MultiTopupArray.main.controllers.first!.mobileNumber.text
                                                first["amountKey"] = MultiTopupArray.main.controllers.first!.amount.text
                                                self?.cashBackdictionary.append(first)
                                            }
                                        }
                                        if UserLogin.shared.loginSessionExpired {
                                            guard let weakSelf = self else { return }
                                            OKPayment.main.authenticate(screenName: "multiCashback", delegate: weakSelf)
                                        } else {
                                            guard let weakSelf = self else { return }
                                            if weakSelf.cashBackdictionary.count > 0 {
                                                self?.cashbackApiCallingForOtherNumber()
                                            } else {
                                                DispatchQueue.main.async {
                                                    alertViewObj.wrapAlert(title: nil, body: "Please Try Again!".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
                                                    alertViewObj.addAction(title: "OK", style: .cancel, action: {
                                                    })
                                                    alertViewObj.showAlert()
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else if let code = json["Code"] as? NSNumber, code == 300 {
                        DispatchQueue.main.async {
                            self?.showErrorAlert(errMessage: "Your Service Provider is Currently Unavailable.".localized)
                        }
                    }
                }
            }
        }
    }
}

//MARK: - BioMetricLoginDelegate
extension TopupHomeViewController: BioMetricLoginDelegate {
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if screen == "multiCashback" {
            if isSuccessful {
                self.cashbackApiCallingForOtherNumber()
            }
            return
        } else if screen == "cashbackSingleNumber" {
            if isSuccessful {
                self.cashbackApiCallingSingleNumber()
            }
            return
        }
    }
}


//MARK: - TopupViewControllerDelegate
extension TopupHomeViewController: TopupViewControllerDelegate {
    func reloadObjectHeight(obj: TopupViewController) {
        if let first = self.viewArray.first {
            first.firstObject = false
            if let cell = self.cellArray.first {
                
                if self.viewArray.count > 1 {
                    first.view.frame = first.view.frame
                    first.topupHeightConstraint.constant = 37.00
                } else {
                    first.view.frame = first.view.frame
                    first.topupHeightConstraint.constant = 0.00
                }
                cell.frame = CGRect.init(x: 0, y: 0, width: cell.bounds.width, height: (first.dHeight ?? 0.00) + 10.00)
                cell.layoutIfNeeded()
                cell.layoutSubviews()
                UIView.setAnimationsEnabled(false)
            }
        }
        
        for (index, _) in self.viewArray.enumerated() {
            let uiView = self.viewArray[index]
            if uiView == obj, self.cellArray.count - 1  >= index {
                uiView.firstObject = false
                if self.viewArray.count > 1 {
                    uiView.topupHeightConstraint.constant = 37.00
                }
                else{
                    uiView.topupHeightConstraint.constant = 0.00
                }
                let cell   = self.cellArray[index]
                cell.frame = CGRect.init(x: cell.frame.origin.x, y: cell.frame.origin.y, width: cell.bounds.width, height: (uiView.dHeight ?? 0.00) + 10.00)
                cell.layoutIfNeeded()
                cell.layoutSubviews()
            }
        }
        if let last = self.viewArray.last, let mob = last.mobileNumber.text {
            let rangeCheck = PayToValidations().getNumberRangeValidation(mob)
            if mob.count > rangeCheck.max {
                self.tableViewObjectHolder.reloadData {
                    self.tableViewObjectHolder.beginUpdates()
                    self.tableViewObjectHolder.scrollToTop(animated: false)
                    self.tableViewObjectHolder.layoutIfNeeded()
                    self.tableViewObjectHolder.reloadData()
                    self.tableViewObjectHolder.endUpdates()
                }
            } else  if mob.count >= rangeCheck.min {
                DispatchQueue.main.async {
                    self.tableViewObjectHolder.beginUpdates()
                    self.tableViewObjectHolder.layoutIfNeeded()
                    self.tableViewObjectHolder.endUpdates()
                }
                
            }
//
//            if cnf.containsAlphabets() {
//                self.tableViewObjectHolder.reloadData {
//                    self.tableViewObjectHolder.beginUpdates()
//                    self.tableViewObjectHolder.scrollToTop(animated: false)
//                    self.tableViewObjectHolder.layoutIfNeeded()
//                    self.tableViewObjectHolder.reloadData()
//                    self.tableViewObjectHolder.endUpdates()
//                }
//            }
        }
        self.hideShowSubmitButton()
    }
    
    func deleteCell(obj: TopupViewController) {
        var string = ""
        if let index = self.viewArray.index(of: obj) {
            string = String.init(format: "%d", index + 1)
        }
        
        alertViewObj.wrapAlert(title: nil, body: "\("Do you want to delete Recharge detail".localized) \(string)".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
        alertViewObj.addAction(title: "Cancel".localized, style: .cancel, action: {
        })
        
        alertViewObj.addAction(title: "OK".localized, style: .target) {
            for (index,element) in self.viewArray.enumerated() {
                if obj == element {
                    self.cellArray.removeAll()
                    self.viewArray.remove(at: index)
                    obj.view.removeFromSuperview()
                    self.loadCell()
                    
                    if let first = self.viewArray.first {
                        if self.viewArray.count == 1 {
                            let myIntValue = Int(first.dHeight ?? 0.00)
                            if myIntValue < 100 {
                                first.dHeight = 50
                            }
                        }
                        first.firstObject = false
                        if let cell = self.cellArray.first {
                            
                            if self.viewArray.count > 1 {
                                first.view.frame = first.view.frame
                                first.topupHeightConstraint.constant = 37.00
                            } else {
                                first.view.frame = first.view.frame
                                first.topupHeightConstraint.constant = 0.00
                            }
                            
                            cell.frame = CGRect.init(x: 0, y: 0, width: cell.bounds.width, height: (first.dHeight ?? 0.00) + 10.00)
                            cell.layoutIfNeeded()
                            cell.layoutSubviews()
                            UIView.setAnimationsEnabled(false)
                            self.tableViewObjectHolder.layoutIfNeeded()
                        }
                    }
                    
                    self.tableViewObjectHolder.reloadData()
                    if let last = self.viewArray.last {
                        last.view.isUserInteractionEnabled = true
                        if let mobileNumber = last.mobileNumber.text, mobileNumber.count > 0 {
                            self.showAddObjectButton(show: true)
                        }
                    }
                    break
                }
            }
            
            if self.viewArray.count < 2, self.viewArray.count > 0 {
                if let first = self.viewArray.first {
                    first.view.isUserInteractionEnabled = true
                }
            } else {
                if let first = self.viewArray.last {
                    first.view.isUserInteractionEnabled = true
                }
            }
            self.hideShowSubmitButton()
        }
        alertViewObj.showAlert()
    }
    
    func addObject(Obj: TopupViewController) {
        guard let firstUI = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TopupViewController.self))
            as? TopupViewController else {return}
        firstUI.delegate = self
        firstUI.firstObject = false
        firstUI.navigation = self.navigationController
        self.viewArray.append(firstUI)
        self.loadCell()
        self.tableViewObjectHolder.reloadData()
        self.hideShowSubmitButton()
    }
    
    func changeColor(color: UIColor, name: String) {
        self.containerSubmitButton.backgroundColor = color
        self.submitButton.backgroundColor = color
        self.navigationController?.navigationBar.backgroundColor = color
        self.navigationController?.navigationBar.barTintColor = color
        
        let size  = name.size(withAttributes: [.font: UIFont.init(name: appFont, size: 13.0) ?? UIFont.systemFont(ofSize: 13.0)])
        let label = UILabel(frame: .init(origin: CGPoint.zero, size: size))
        label.textColor = .white
        if name.lowercased().hasPrefix("Mpt".lowercased()) {
            label.text = "Mpt"
        } else {
            label.text = name
        }
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: label)
    }
    
    func singleObjectPayment() {
        if MultiTopupArray.main.controllers.count == 1 ||  MultiTopupArray.main.controllers.count == 5 {
            self.multiTopupSimCall()
        } else {
            
        }
    }
    
    // for single other number topup
    func singleObjectWithCustomMsg(msg: String) {
        if let first = self.viewArray.first {
            if (first.operatorName.text!.lowercased() == "Mectel".lowercased()) {
                self.multiTopupSimCall()
            } else {
                self.paymentCallOtherNumberWithCustomMsg(index: msg)
            }
        } else {
            self.multiTopupSimCall()
        }
    }
    
    func didShowAdvertisement(show: Bool) { // show means false execution
        self.loadAdvertisement()
        self.advView.isHidden = !show
        self.adViewImage.isHidden = !show
        self.heightConstraints.constant = (show) ? 120.00 : 0.00
        self.view.layoutIfNeeded()
    }
    
    func showAddObjectButton(show: Bool) {
        self.containerSubmitButton.isHidden = !show
        if MultiTopupArray.main.controllers.count > 4 {
            self.containerSubmitButton.isHidden = true
        }
        //        if MultiTopupArray.main.controllers.count < 5 {
        //            if let vc = MultiTopupArray.main.controllers.last {
        //                if let mobile = vc.mobileNumber.text, let cnf = vc.confimMobile.text {
        //                    if cnf.hasPrefix(mobile), mobile.count == cnf.count, cnf.count > 4 {
        //                        self.containerSubmitButton.isHidden = false
        //                    } else {
        //                        self.containerSubmitButton.isHidden = true
        //                    }
        //
        //                    if cnf.containsAlphabets() {
        //                        self.containerSubmitButton.isHidden = true
        //                    }
        //
        //                }
        //            }
        //        }
    }
    
    func scrollEnable(show: Bool) {
        tableViewObjectHolder.isScrollEnabled = true
    }
    
    func updateHeightTableView() {
        self.tableViewObjectHolder.beginUpdates()
        self.tableViewObjectHolder.endUpdates()
    }
    
    func scrolltoBottom() {
        if self.viewArray.count > 2 {
            self.tableViewObjectHolder.scrollToBottom(animated: false)
        }
    }
}

//MARK: - UITableViewDelegate, UITableViewDataSource
extension TopupHomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.cellArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = self.viewArray[indexPath.row].dHeight {
            
            return height + 10.00
            
        }
        return 0.0
    }
}

