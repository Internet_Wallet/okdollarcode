//
//  TopupSpecialOfferTableViewCell.swift
//  OK
//
//  Created by Ashish on 1/25/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class TopupSpecialOfferTableViewCell: UITableViewCell {
    
    @IBOutlet var detailLabel: UILabel!{
        didSet
        {
            detailLabel.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet var amount: UILabel!{
        didSet
        {
            amount.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet var designView: CardDesignView!
    
    func wrap(_ model: SpecialOfferModel) {
        self.detailLabel.text = model.offerDetails
        self.amount.attributedText = model.attributeAmount
        self.designView.backgroundColor = MyNumberTopup.theme
    }
    
}





