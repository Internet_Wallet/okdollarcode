//
//  TopupUITableViewCell.swift
//  OK
//
//  Created by Ashish on 1/24/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class TopupUITableViewCell: UITableViewCell {

    @IBOutlet var cardViewLayer: CardDesignView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func wrap(_ vc : TopupViewController) -> Void {
        vc.view.frame = self.cardViewLayer.bounds
        self.cardViewLayer.addSubview(vc.view)
    }
}
