//
//  TopupDataPlanTableViewCell.swift
//  OK
//
//  Created by Ashish on 1/24/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class TopupDataPlanTableViewCell: UITableViewCell {
    
    @IBOutlet var packDetails: UILabel!{
        didSet
        {
            packDetails.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    @IBOutlet var dataSiz: UILabel!{
        didSet
        {
            dataSiz.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet var amount: UILabel!{
        didSet
        {
            amount.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet var amountViewColor: CardDesignView!

    func wrap(model: DataPlanModel) {
        self.packDetails.text = model.details
        self.dataSiz.text = model.size
        self.amount.attributedText = model.attributedAmount
        self.amountViewColor.backgroundColor = MyNumberTopup.theme
    }
    
    func wrapInternational(model: InternationalDataPlanModel) {
        self.packDetails.text = model.details
        self.dataSiz.text = model.size
        self.amount.attributedText = model.attributedAmount
        self.amountViewColor.backgroundColor = MyNumberTopup.theme
    }
}
