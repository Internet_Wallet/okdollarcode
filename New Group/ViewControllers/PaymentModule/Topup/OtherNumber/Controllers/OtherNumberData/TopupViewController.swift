//
//  TopupViewController.swift
//  OK
//
//  Created by Ashish on 1/24/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Contacts

protocol TopupViewControllerDelegate {
    func reloadObjectHeight(obj: TopupViewController )
    func deleteCell(obj: TopupViewController)
    func addObject(Obj: TopupViewController)
    func changeColor(color: UIColor,name: String)
    func singleObjectPayment()
    func singleObjectWithCustomMsg(msg: String)
    func didShowAdvertisement(show: Bool)
    func showAddObjectButton(show: Bool)
    func scrollEnable(show: Bool)
    func updateHeightTableView()
    func scrolltoBottom()
}

class TopupViewController: OKBaseController {

    //MARK: - Outlets
    @IBOutlet weak var rechargeHeaderLabel: UILabel!
    {
        didSet
        {
            rechargeHeaderLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var operatorName: UILabel!{
        didSet
        {
            operatorName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var mobileNumber: TopupMobileRightViewTextField!{
        didSet{
            mobileNumber.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var confimMobile: RightViewSubClassTextField!{
        didSet{
            confimMobile.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var amount: RightViewSubClassTextField!{
        didSet{
            
            amount.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var confirmMobConst: NSLayoutConstraint!
    @IBOutlet weak var mobileNoConst: NSLayoutConstraint!
    @IBOutlet weak var amountConst: NSLayoutConstraint!
    @IBOutlet weak var topupHeightConstraint: NSLayoutConstraint!
    // For bonus point recharge
    @IBOutlet weak var bonusPointTextField: RightViewSubClassTextField!{
        didSet{
            bonusPointTextField.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var bonusPointHeightConstraints: NSLayoutConstraint!
    // contact suggestion
    @IBOutlet weak var contactsSuggestionTableView: UITableView!
    @IBOutlet var contactSuggestionHeightConstraint: NSLayoutConstraint!
    @IBOutlet var cardDesignHeightConstraint: NSLayoutConstraint!
    
    //MARK: - Properties
    let mobileNumberView =  ContactRightView.updateView()
    private let mobileNumberAcceptableCharacters = "0123456789"
    var delegate: TopupViewControllerDelegate?
    var navigation: UINavigationController?
    var plantype: String?
    var opName: String?
    var heightChanges: Bool = true {
        didSet {
            
            if MultiTopupArray.main.controllers.count > 1 {
                self.topupHeightConstraint.constant = 37.00
            } else {
                self.topupHeightConstraint.constant = 0.00
            }
            
            let first = self.topupHeightConstraint.constant + mobileNoConst.constant + confirmMobConst.constant + contactSuggestionHeightConstraint.constant
            let second = amountConst.constant + bonusPointHeightConstraints.constant
            self.dHeight = first + second
            self.cardDesignHeightConstraint.constant = (confirmMobConst.constant == 0.0) ? 103.0 : 153.0
            if let delegate = self.delegate {
                delegate.reloadObjectHeight(obj: self)
            }
        }
    }
    var firstObject: Bool = false
    var rechargeHeader: String = "Recharge"
    private var mobileNumberLeftView    =  DefaultIconView.updateView(icon: "topup_number")
    private var cnfMobileNumberLeftView =  DefaultIconView.updateView(icon: "topup_number")
    private var amountNumberLeftView    =  DefaultIconView.updateView(icon: "per_amount")
    private var bonusPointLeftView      =  DefaultIconView.updateView(icon: "points")
    
    
   // @IBOutlet weak var tableListContacts: UITableView!
    var contactsArray = [CNContact]()
    var confMobile = UIView()
    var recentContactsSuggestionArr = [TopupContactSuggestion]()
    
    
    //MARK: - Views Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialUpdates()
        self.setupForObject()
        if MultiTopupArray.main.controllers.count <= 1 {
            self.hideFirstCellItem()
        }
        self.operatorName.text = ""
//       self.hideContactTable()
        //self.reloadContacts()
        
        var recent = TopupRecentContacts.getRecents()
        recentContactsSuggestionArr = TopupRecentContacts.getRecents()
        self.removeDuplicatesForSuggestion()
        recent = recentContactsSuggestionArr
        let filteredItems = recent.map({SearchTextFieldItem.init(contactSuggestion: ($0.number.safelyWrappingString(), $0.operatorName.safelyWrappingString(), $0.amount.safelyWrappingString()))})
        self.mobileNumber.filterItems(filteredItems)
        self.mobileNumber.backgroundColor = .white
//        self.contactsSuggestionTableView.reloadData()
        
        self.mobileNumber.itemSelectionHandler = { items, itemsPosition in
            println_debug(items)
            println_debug(itemsPosition)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.rechargeHeaderLabel.text = ""
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.rechargeHeaderLabel.text = ""
        self.headerUpdate()
    }
    
    fileprivate func hideContactTable(){
      // self.tableListContacts.isHidden = true
        self.contactsSuggestionTableView.isHidden = true
        self.contactSuggestionHeightConstraint.constant = 0
    }
    fileprivate func showContactTable(){
     // self.tableListContacts.isHidden = false
        self.contactsSuggestionTableView.isHidden = false
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
    }
    
    //MARK: - Methods
    func updateView() {
        self.amount.text = ""
        self.amount.rightView = nil
        self.amountConst.constant = 0.00
        self.bonusPointHeightConstraints.constant = 0.0
        self.amount.isHidden = true
        self.bonusPointTextField.isHidden = true
        
        self.heightChanges = true
        self.amountConst.constant = 50.0
        if MultiTopupArray.main.controllers.count <= 1 {
            self.bonusPointHeightConstraints.constant = 50.0
            self.bonusPointTextField.isHidden = false
        }
        self.animateView {
            self.amount.isHidden = false
        }
        self.confirmMobConst.constant = 50.00
        self.animateView {
            self.confimMobile.isHidden = false
        }
    }
    
    func removeDuplicatesForSuggestion() {
        var result = [TopupContactSuggestion]()
        
        for value in recentContactsSuggestionArr {
            var contains = false
            for dup in result {
                if dup.number == value.number {
                    contains = true
                    break
                }
            }
            if !contains {
                result.append(value)
            }
        }
        recentContactsSuggestionArr = result
    }
    
    func headerUpdate() {
        self.rechargeHeaderLabel.text = rechargeHeader
    }
    
    private func initialUpdates() {
        self.mobileNumber.placeholder        = "Mobile Number".localized
        self.confimMobile.placeholder        = "Confirm Mobile Number".localized
        self.amount.placeholder              = "Select Amount".localized
        self.bonusPointTextField.placeholder = "Select from OK$ Bonus Points".localized
        
        
        self.confimMobile.leftViewMode = .always
        self.confimMobile.leftView     = cnfMobileNumberLeftView
        
        self.amount.leftViewMode   = .always
        self.amount.leftView       = amountNumberLeftView
        
        self.bonusPointTextField.leftViewMode = .always
        self.bonusPointTextField.leftView     = bonusPointLeftView
        
     
        mobileNumberView.delegate = self
        self.mobileNumber.rightViewMode = .always
        self.mobileNumber.rightView = mobileNumberView
        mobileNumberView.hideClearButton(hide: true)
        
        self.mobileNumber.leftViewMode = .always
//        self.mobileNumber.editingRect(forBounds: CGRect(x: 58, y: 0, width: self.mobileNumber.frame.size.width - 185, height: 50))
        self.mobileNumber.leftView  = mobileNumberLeftView

        
        self.mobileNumber.addTarget(self, action: #selector(TopupViewController.textFieldDidChange(_:)),
                             for: UIControl.Event.editingChanged)
        
    }
    
    func contact(contact: ContactPicker) {
        self.confimMobile.leftViewMode = .always
        self.confimMobile.leftView = nil
        self.confimMobile.leftView = DefaultIconView.updateView(icon: "topup_number")
        self.mobileNumber.text = "09"
        self.confimMobile.text = ""
        self.amount.text = ""
        self.operatorName.text = ""
        self.amount.rightView = nil
        self.confirmMobConst.constant = 0.00
        self.amountConst.constant = 0.00
        self.bonusPointHeightConstraints.constant = 0.0
        self.confimMobile.isHidden = true
        self.amount.isHidden = true
        self.bonusPointTextField.isHidden = true
        self.heightChanges = true
        self.confimMobile.isUserInteractionEnabled = true
        self.mobileNumber.becomeFirstResponder()
        self.delegate?.changeColor(color: MyNumberTopup.OperatorColorCode.okDefault, name: "")
    }
    
    func decodeContact(ph: String,name: String, amount: String = "", hide: Bool = false) {
        self.delegate?.scrollEnable(show: true)
        if let cnfrightView = self.confimMobile.rightView as? PTClearButton {
            cnfrightView.hideClearButton(hide: true)
        }
        self.confimMobile.leftView = DefaultIconView.updateView(icon: "name_bill")
        var str = ""
        str = ph
        if str.count > 0 {
            str = str.replacingOccurrences(of: "(", with: "")
            str = str.replacingOccurrences(of: ")", with: "")
            str = str.trimmingCharacters(in: .whitespaces)
        }
        if str.hasPrefix("00") {
            _ = str.remove(at: String.Index.init(encodedOffset: 0))
            _ = str.remove(at: String.Index.init(encodedOffset: 0))
            str = "+" + str
        }
        let country = self.identifyCountry(withPhoneNumber: str)
        if country.0 == "+95" ||  country.1 == "myanmar" ||  country.0 == "" ||  country.1 == "" {
            let countryObject = Country.init(name: "", code: "myanmar", dialCode: "+95")
            if str.hasPrefix(countryObject.dialCode) {
                str = str.replacingOccurrences(of: countryObject.dialCode, with: "")
                str = "0" + str
            } else {
                if str.hasPrefix("0") {
                    
                } else {
                    str = "0" + str
                }
            }
            self.amount.text = amount
            if (amount == "") {
                self.amount.rightView = nil
            }
            self.mobileNumber.text = str
            if hide {
                self.confimMobile.isHidden = hide
                self.confirmMobConst.constant = 0.0
            } else {
                if name == "" {
                    self.confimMobile.text = "Unknown"
                } else {
                    self.confimMobile.text = name
                }
                self.confimMobile.isUserInteractionEnabled = false
                self.confirmMobConst.constant = 50.0
                self.animateView {
                    self.confimMobile.isHidden = false
                }
            }
            self.amountConst.constant     = 50.0
            if MultiTopupArray.main.controllers.count <= 1 {
                self.bonusPointHeightConstraints.constant = 50.0
                self.bonusPointTextField.isHidden = false
            }
            self.delegate?.showAddObjectButton(show: true)
            self.animateView {
                self.amount.isHidden = false
            }
            self.mobileNumberView.hideClearButton(hide: false)
            let rangeCheck = PayToValidations().getNumberRangeValidation(str)
            self.operatorName.text = (rangeCheck.operator != "Mpt") ? rangeCheck.operator : rangeCheck.operator.uppercased()
            if self.operatorName.text ?? "" == "MPT" {
                self.amount.placeholder = "Select Amount".localized
            } else {
                self.amount.placeholder = "Enter or Select Amount".localized
            }
            self.operatorName.textColor = PayToValidations().getColorForOperator(operatorName: rangeCheck.operator)
        } else {
            self.delegate?.showAddObjectButton(show: false)
            guard let keyWinndow = UIApplication.shared.keyWindow else { return }
            keyWinndow.makeToast("You Selected Other Country Number".localized, point: keyWinndow.center, title: nil, image: nil, completion: nil)
        }
    }
    
    func identifyCountry(withPhoneNumber prefix: String) -> (String, String) {
        let countArray = revertCountryArray() as! Array<NSDictionary>
        var finalCodeString = ""
        var flagCountry = ""
        for dic in countArray {
            if let code = dic .object(forKey: "CountryCode") as? String, let flag = dic.object(forKey: "CountryFlagCode") as? String{
                if prefix.hasPrefix(code) {
                    finalCodeString = code
                    flagCountry = flag
                }
            }
        }
        return (finalCodeString,flagCountry)
    }
    
    func revertCountryArray() -> NSArray {
        do {
            if let file = Bundle.main.url(forResource: "mcc_mnc", withExtension: "txt") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? NSDictionary {
                    if let arr = object.object(forKey: "data") as? NSArray {
                        arrCountry = arr
                        return arrCountry
                    }
                } else if let object = json as? [Any] {
                    println_debug(object)
                } else {
                    println_debug("JSON is invalid")
                }
            } else {
                println_debug("no file")
            }
        } catch {
            println_debug(error.localizedDescription)
        }
        return arrCountry
    }
    
    private func setupForObject() {
        self.confirmMobConst.constant = 0.00
        self.amountConst.constant = 0.00
        self.bonusPointHeightConstraints.constant = 0.00
        self.confimMobile.isHidden = true
        self.amount.isHidden = true
        self.bonusPointTextField.isHidden = true
        self.heightChanges = true
    }
    
    func animateView(handle: @escaping () -> Void) {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
            self.heightChanges = true
            handle()
        })
    }
    
    //MARK: SELECT AMOUNT ACTION
    private func openTopupController() {
        let vc = UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "myNumberNavigation"))
        navigation?.present(vc, animated: true, completion: nil)
    }
    
    //MARK: Extra check for cdma number
    func extraCheckForCDMA() {
        guard let customerNumber = self.mobileNumber.text else { return }
        let urlString = String.init(format: TopupUrl.cdmaCheck, customerNumber)
        guard let url = URL.init(string: urlString) else { return }
        TopupWeb.genericClass(url: url, param: Dictionary<String,Any>() as AnyObject, httpMethod: "GET") { (response, success) in
            if success {
                DispatchQueue.main.async {
                    if let dictionary = response as? Dictionary<String,Any> {
                        if let code = dictionary.safeValueForKey("Code") as? Int, code == 300 {
                            alertViewObj.wrapAlert(title: nil, body: "Postpaid mobile number cannot recharge. You can only recharge if your mobile number is Prepaid".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
                            alertViewObj.addAction(title: "Cancel".localized, style: .cancel, action: {
                                self.delegate?.showAddObjectButton(show: true)
                            })
                            alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                                if let nav = self.navigation {
                                    self.delegate?.showAddObjectButton(show: true)
                                    var numberAgentCodeFormat = customerNumber
                                    _ = numberAgentCodeFormat.remove(at: String.Index.init(encodedOffset: 0))
                                    
                                    numberAgentCodeFormat = "0095" +  numberAgentCodeFormat
                                    
                                    MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: numberAgentCodeFormat, delegate: self, present: nav, otherNumber: true)
                                }
                            })
                            if MultiTopupArray.main.controllers.count < 2, MultiTopupArray.main.controllers.count > 0 {
                                alertViewObj.showAlert()
                            } else {
                                alertViewObj.showAlert()
                            }
                            
                        } else if let code = dictionary.safeValueForKey("Code") as? Int, code == 200 {
                            if let nav = self.navigation {
                                var numberAgentCodeFormat = customerNumber
                                _ = numberAgentCodeFormat.remove(at: String.Index.init(encodedOffset: 0))
                                
                                numberAgentCodeFormat = "0095" +  numberAgentCodeFormat
                                
                                MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: numberAgentCodeFormat, delegate: self, present: nav, otherNumber: true)
                            }
                        }
                    }
                }
            }
        }
    }
    
    //MARK: Extra setup for bonus point
    func hideBonusPointTextField() {
        self.view.frame = self.view.frame
        self.bonusPointHeightConstraints.constant = 0.0
        self.bonusPointTextField.isHidden = true
    }
    
    func pospaidBonusPoint() {
        
    }
    
    //MARK: Bonus Point Api
    func bonusPointCallback() {
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "bonusPointApi", delegate: self)
        } else {
            self.callGetRedeemPointsHistoryByCategory()
        }
    }
    
    private func callGetRedeemPointsHistoryByCategory() {
        let param = loadGetRedeemPointsHistoryByCategoryParams()
        guard let urlString = Url.getHistoryByCategory.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {  return }
        let url = getUrl(urlStr: urlString, serverType: .serverApp)
        
        TopupWeb.genericClassWithoutLoader(url: url, param: param as AnyObject, httpMethod: "POST") { [weak self](response, success) in
            DispatchQueue.main.async {
                guard let weakself = self else { return }
                if success {
                    if let dictionary = response as? Dictionary<String,Any> {
                        if let code = dictionary.safeValueForKey("Code") as? Int, code == 200 {
                            guard let dictionary = response as? Dictionary<String, Any>  else { return }
                            if dictionary.safeValueForKey("Code") as? Int == 200 {
                                guard let dataString = dictionary.safeValueForKey("Data") as? String else { return }
                                guard let jsonObject = self?.jsonObject(string: dataString) else { return }
                                guard let modelData  = jsonObject.data(using: .utf8) else { return }
                                do {
                                    let model = try JSONDecoder().decode(BuyBonusPointModel.self, from: modelData)
                                    weakself.generatingUI(model)
                                } catch {
                                    println_debug(error.localizedDescription)
                                }
                            }
                        } else if let code = dictionary.safeValueForKey("Code") as? Int, code == 500 {
                            self?.showErrorAlert(errMessage: "Please Try Again!".localized)
                        }
                    }
                }
            }
        }
    }
    
    private func generatingUI(_ model: BuyBonusPointModel) {
        guard let topupModule = model.first(where: {$0.transactionTypeName.safelyWrappingString().lowercased() == "TOPUP".lowercased()}) else { return }
        guard let redeemmodel = topupModule.buyRedeemPointsModel else { return }
        guard let firstRedeemModel = redeemmodel.first else { return }
        guard let merchantNumber = firstRedeemModel.merchantMobileNumber else { return }
        
        let number = self.mobileNumber.text.safelyWrappingString()
        let rangeCheck = PayToValidations().getNumberRangeValidation(number)
        let colorChange = PayToValidations().getColorForOperator(operatorName: rangeCheck.operator)
        
        var bonusPointTopupModel = [BonusPointTopupModel]()
        for element in redeemmodel {
            let model = BonusPointTopupModel.init(model: element, color: colorChange)
            bonusPointTopupModel.append(model)
        }
        
        var operatorName = rangeCheck.operator
        if operatorName.lowercased().hasPrefix("mpt".lowercased()) {
            operatorName = "Mpt"
        }
        
        bonusPointTopupModel = bonusPointTopupModel.filter({$0.operatorName.safelyWrappingString().lowercased() == operatorName.lowercased()})
        
        self.callbackApiBonusPoint(merchant: merchantNumber, withModels: bonusPointTopupModel)
    }
    
    private func loadGetRedeemPointsHistoryByCategoryParams() -> Dictionary<String,Any> {
        var param = Dictionary<String,Any>()
        
        param["AppId"] = LoginParams.setUniqueIDLogin()
        param["Limit"] = 0
        param["MobileNumber"] = UserModel.shared.mobileNo
        param["Msid"] = getMsid()
        param["Offset"] = 0
        param["Ostype"] = 1
        param["Otp"] = ""
        param["Simid"] = simid
        
        var mainParam = Dictionary<String,Any>()
        mainParam["LoginInfo"] = param
        mainParam["RedeemPointsCategory"] = "SELLING"
        return mainParam
    }
    
    var responseDictionaryBonusPoint: Dictionary<String,Any>?
    
    private func callbackApiBonusPoint(merchant: String, withModels models: [BonusPointTopupModel]) {
        responseDictionaryBonusPoint = Dictionary<String,Any>()
        
        func enumerate(indexer: XMLIndexer) {
            for child in indexer.children {
                responseDictionaryBonusPoint![child.element!.name] = child.element!.text
                enumerate(indexer: child)
            }
        }
        let urlSellTopup   = String.init(format: Url.sellTopup, UserModel.shared.mobileNo, UserLogin.shared.token)
        let url = getUrl(urlStr: urlSellTopup, serverType: .serverEstel)
        TopupWeb.genericClassXML(url: url, param: Dictionary<String,Any>() as AnyObject, httpMethod: "POST") { [weak self](response, success) in
            DispatchQueue.main.async {
                if success {
                    guard let weakself = self else { return }
                    guard let string = response as? String else { return }
                    println_debug(response)
                    let xmlString = SWXMLHash.parse(string)
                    enumerate(indexer: xmlString)
                    let number = weakself.mobileNumber.text.safelyWrappingString()
                    guard let sellTopup = weakself.storyboard?.instantiateViewController(withIdentifier: "sellLoyaltyPointsNavigation") else { return }
                    if let bonusController = sellTopup as? UINavigationController {
                        if let firstController = bonusController.viewControllers.first {
                            if let bonusVC = firstController as? BonuPointTopupViewController {
                                guard let mobile = self?.mobileNumber.text, let confirm = self?.confimMobile.text else { return }
                                bonusVC.loyaltyName         = (confirm.hasPrefix(mobile)) ? "Unknown": confirm
                                bonusVC.merchantNumber      = merchant
                                bonusVC.models              = models
                                bonusVC.userEnteredNumber   = number
                                bonusVC.sellingBackResponse = weakself.responseDictionaryBonusPoint
                            }
                        }
                    }

                    DispatchQueue.main.async {
                        weakself.calculateBonusPoints(sellingBackResponse: weakself.responseDictionaryBonusPoint, merchantNumber: merchant, vc: sellTopup)
                    }
                    //                    }
                }
            }
        }
    }
    
    func calculateBonusPoints(sellingBackResponse: Dictionary<String,Any>?, merchantNumber: String?, vc: UIViewController) {
        let number = merchantNumber.safelyWrappingString()
        
        guard let sellRespone = sellingBackResponse else { return }
        if let sss = sellRespone.safeValueForKey("merchantid") as? String {
            println_debug(sss)
        }
        guard let merNumbers = sellRespone.safeValueForKey("merchantid") as? String else {
            alertViewObj.wrapAlert(title: nil, body: "You Don’t have enough Bonus Point for Topup, You can add OK Dollar Topup Bonus Points or Topup from wallet Balance.".localized, img: #imageLiteral(resourceName: "buy_bonuspoint"))
            alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                if appDel.checkNetworkAvail() {
                    let vc = UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "LoyaltyTopupNavigation"))
                    self.present(vc, animated: true, completion: nil)
                } else {
                    self.showErrorAlert(errMessage: PaytoConstants.messages.noInternet.localized)
                }
            })
            alertViewObj.addAction(title: "Cancel".localized, style: .cancel, action: {
            })
            alertViewObj.showAlert()
            return
        }
        guard let loyaltyPoints = sellRespone.safeValueForKey("loyaltypoints") as? String else { return }
        
        let merchantArray = merNumbers.components(separatedBy: "|")
        let pointsArray   = loyaltyPoints.components(separatedBy: "|")
        
        var countingPoint = [Int]()
        
        if merchantArray.count == pointsArray.count {
            for (index,element) in merchantArray.enumerated() {
                if element.lowercased() == number.lowercased() {
                    let integerValueIndex = (pointsArray[index] as NSString).integerValue
                    countingPoint.append(integerValueIndex)
                }
            }
        }
        
        if countingPoint.count > 0 {
            let sumedArr = countingPoint.reduce(0, {$0 + $1})
            println_debug(sumedArr)
            if sumedArr >= 1000 {
                self.present(vc, animated: true, completion: nil)
            } else {
                self.showBonusPointAddAlert()
            }
        } else {
            self.showBonusPointAddAlert()
        }
    }
    
    fileprivate func showBonusPointAddAlert() {
        alertViewObj.wrapAlert(title: nil, body: "You Don’t have enough Bonus Point for Topup, You can add OK Dollar Topup Bonus Points or Topup from wallet Balance.".localized, img: #imageLiteral(resourceName: "buy_bonuspoint"))
        alertViewObj.addAction(title: "OK".localized, style: .target, action: {
            if appDel.checkNetworkAvail() {
                let vc = UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "LoyaltyTopupNavigation"))
                self.present(vc, animated: true, completion: nil)
            } else {
                self.showErrorAlert(errMessage: PaytoConstants.messages.noInternet.localized)
            }
        })
        alertViewObj.addAction(title: "Cancel".localized, style: .cancel, action: {
        })
        alertViewObj.showAlert()
    }
    
    //MARK: Disable Edit Option
    func disableView() {
        if self.mobileNumber == nil { return }
        self.mobileNumber.isUserInteractionEnabled = false
        self.confimMobile.isUserInteractionEnabled = false
//        self.amount.isUserInteractionEnabled = false
    }
    
    func enableView() {
        if self.mobileNumber == nil || self.confimMobile == nil || self.amount == nil { return }
        self.mobileNumber.isUserInteractionEnabled = true
        if let text = self.confimMobile.text, text.containsAlphabets() {
            self.confimMobile.isUserInteractionEnabled = false
        } else {
            self.confimMobile.isUserInteractionEnabled = true
        }
        
        self.amount.isUserInteractionEnabled = true
    }
    
    func hideFirstCellItem() {
        self.view.frame = self.view.frame
        if self.deleteBtn == nil || self.operatorName == nil || self.rechargeHeaderLabel == nil { return }
        self.deleteBtn.isHidden = true
        self.operatorName.isHidden = true
        self.rechargeHeaderLabel.isHidden = true
        self.bonusPointHeightConstraints.constant = 50.00
        self.bonusPointTextField.isHidden = false
    }
    
    func showFirstItem() {
        self.view.frame = self.view.frame
        if self.deleteBtn == nil || self.operatorName == nil || self.rechargeHeaderLabel == nil { return }
        self.deleteBtn.isHidden = false
        self.operatorName.isHidden = false
        self.rechargeHeaderLabel.isHidden = false
    }
    
    //MARK: - Button Action Methods
    @IBAction func deleteAction(_ sender: UIButton) {
        self.contactSuggestionHeightConstraint.constant = 0
        self.hideContactTable()
        heightChanges = true
        if let delegate = self.delegate {
            delegate.deleteCell(obj: self)
        }
    }
}

//MARK: - MultiTopupArray
//MARK: Class object to carry multi topup controllers
class MultiTopupArray {
    static var main = MultiTopupArray()
    var controllers = [TopupViewController]()
    
    func wrapControllers(_ controller: [TopupViewController]) {
        self.controllers = controller
        guard let lastObject = self.controllers.last else { return }
        
        for elementObject in self.controllers {
            if controller.count > 1 {
                controller.forEach({ (obj) in
                    obj.showFirstItem()
                    obj.hideBonusPointTextField()
                })
            } else {
                controller.forEach({ (obj) in
                    obj.enableView()
                    obj.hideFirstCellItem()
                })
            }
            
            if lastObject == elementObject {
                if let amount = lastObject.amount, let text = amount.text, text.count > 0 {
                    elementObject.disableView()
                } else {
                    elementObject.enableView()
                }
            } else {
                elementObject.disableView()
            }
            
            if MultiTopupArray.main.controllers.count > 0, MultiTopupArray.main.controllers.count < 2 {
                if let first = MultiTopupArray.main.controllers.first {
                    first.enableView()
                }
            }
            
        }
    }
}

//MARK: - PTClearButtonDelegate
extension TopupViewController: PTClearButtonDelegate {
    func clearField(strTag: String) {
        if strTag.hasPrefix("confMobile") {
            self.delegate?.scrollEnable(show: false)
            self.contactSuggestionHeightConstraint.constant = 0
            self.hideContactTable()
            self.confimMobile.text = "09"
            self.amount.text = ""
            self.operatorName.text = ""
            self.amount.rightView = nil
            self.amountConst.constant = 0.00
            self.bonusPointHeightConstraints.constant = 0.0
            self.amount.isHidden = true
            self.bonusPointTextField.isHidden = true
            
            self.heightChanges = true
            self.confimMobile.isUserInteractionEnabled = true
            self.confimMobile.becomeFirstResponder()
            
            self.delegate?.showAddObjectButton(show: false)
            if let cnfView = confimMobile.rightView as? PTClearButton {
                cnfView.hideClearButton(hide: true)
            }
        }
    }
}

//MARK: - UITextFieldDelegate
extension TopupViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let rangeCheck = PayToValidations().getNumberRangeValidation(text)
        if !text.hasPrefix("09") {
            return false
        }
        if rangeCheck.isRejected {
            textField.text = "09"
            self.operatorName.text = ""
            mobileNumberView.hideClearButton(hide: true)
             self.contactsSuggestionTableView.isHidden = true
            PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
            return false
        }
        
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                self.delegate?.showAddObjectButton(show: false)
                self.confimMobile.leftViewMode = .always
                self.confimMobile.leftView = nil
                self.confimMobile.leftView = DefaultIconView.updateView(icon: "topup_number")
                if textField == self.mobileNumber {
                    if textField.text == "09" {
                        println_debug(textField.text)
                        return false
                    }
                    if text == "09" {
                        mobileNumberView.hideClearButton(hide: true)
                    }
                    
                    if MultiTopupArray.main.controllers.count > 1 {
                        self.delegate?.didShowAdvertisement(show: false)
                    } else {
                        if text.count > 3 {
                            self.delegate?.didShowAdvertisement(show: false)
                        } else {
                            self.delegate?.didShowAdvertisement(show: true)
                        }
                    }
                    if text.count < rangeCheck.min {
                        self.amountConst.constant = 0.00
                        self.bonusPointHeightConstraints.constant = 0.0
                        self.confirmMobConst.constant = 0.00
                        self.amount.text = ""
                        self.confimMobile.text = "09"
                        self.amount.rightView = nil
                        self.amount.isHidden = true
                        self.bonusPointTextField.isHidden = true
                        self.confimMobile.isHidden = true
                        if let cnfRightView = self.confimMobile.rightView as? PTClearButton {
                            cnfRightView.hideClearButton(hide: true)
                        }
                    }
                    if text.count < 3 {
                        self.contactSuggestionHeightConstraint.constant = 0
                        self.hideContactTable()
                    }
                    self.heightChanges = true
                    self.mobileNumber.text = text
                    return false
                }
                
                if textField == self.confimMobile {
                    if textField.text == "09" {
                        return false
                    }
                    self.bonusPointTextField.isHidden = true
                    self.amount.rightView = nil
                    self.amountConst.constant = 0.0
                    self.bonusPointHeightConstraints.constant = 0.0
                    self.amount.text = ""
                    self.amount.isHidden = true
                    self.bonusPointTextField.isHidden = true
                    if !(text.count > 2) {
                        if self.confimMobile.rightView != nil {
                            self.confimMobile.rightView = nil
                        }
                    }
                    self.delegate?.showAddObjectButton(show: false)
                    self.confimMobile.text = text
                    self.heightChanges = true
                    return false
                }
            }
        }
        
        if textField == self.mobileNumber {
            if recentContactsSuggestionArr.count > 0 {
                if text.count > 2 {
                    var recent = TopupRecentContacts.getRecents()
                    recentContactsSuggestionArr = TopupRecentContacts.getRecents()
                    let textCheck = String(text.dropFirst())
                    self.removeDuplicatesForSuggestion()
                    recent = recentContactsSuggestionArr
                    recentContactsSuggestionArr = recentContactsSuggestionArr.filter({ (textNum) -> Bool in
                        return textNum.number?.contains(find: textCheck) ?? false
                    })
                    let filteredItems = recent.map({SearchTextFieldItem.init(contactSuggestion: ($0.number.safelyWrappingString(), $0.operatorName.safelyWrappingString(), $0.amount.safelyWrappingString()))})
                    self.mobileNumber.filterItems(filteredItems)
                    self.mobileNumber.backgroundColor = .white
                    if recentContactsSuggestionArr.count > 1 {
                        self.contactSuggestionHeightConstraint.constant = 85
                    } else if recentContactsSuggestionArr.count == 1 {
                        self.contactSuggestionHeightConstraint.constant = 45
                    } else {
                        self.contactSuggestionHeightConstraint.constant = 0
                    }
                    self.showContactTable()
                    self.contactsSuggestionTableView.reloadData()
                    
                    self.mobileNumber.itemSelectionHandler = { items, itemsPosition in
                        println_debug(items)
                        println_debug(itemsPosition)
                    }
                }
                else {
                    self.contactSuggestionHeightConstraint.constant = 0
                    self.hideContactTable()
                    heightChanges = true
                }
            } else {
                recentContactsSuggestionArr = TopupRecentContacts.getRecents()
            }
            
            if text.count > 2 {
                mobileNumberView.hideClearButton(hide: false)
            }else {
                mobileNumberView.hideClearButton(hide: true)
            }
            
            let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: mobileNumberAcceptableCharacters).inverted
            let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
            
            if string != filteredSet { return false }
            if MultiTopupArray.main.controllers.count > 1 {
                self.delegate?.didShowAdvertisement(show: false)
            } else {
                if text.count > 3 {
                    self.delegate?.didShowAdvertisement(show: false)
                } else {
                    self.delegate?.didShowAdvertisement(show: true)
                }
            }
            
            if text.count > 3 {
                self.operatorName.text = (rangeCheck.operator != "Mpt") ? rangeCheck.operator : rangeCheck.operator.uppercased()
                if self.operatorName.text ?? "" == "MPT" {
                    self.amount.placeholder = "Select Amount".localized
                } else {
                    self.amount.placeholder = "Enter or Select Amount".localized
                }
                self.operatorName.textColor = PayToValidations().getColorForOperator(operatorName: rangeCheck.operator)
            } else {
                self.operatorName.text = ""
                self.operatorName.textColor = PayToValidations().getColorForOperator(operatorName: rangeCheck.operator)
            }
            
            if  MultiTopupArray.main.controllers.count > 1 {
                self.delegate?.changeColor(color: MyNumberTopup.OperatorColorCode.okDefault, name: rangeCheck.operator)
            } else {
                if text.count > 3 {
                    let colorChange = PayToValidations().getColorForOperator(operatorName: rangeCheck.operator)
                    self.delegate?.changeColor(color: colorChange, name: rangeCheck.operator)
                } else {
                    self.delegate?.changeColor(color: MyNumberTopup.OperatorColorCode.okDefault, name: "")
                }
            }
            if text.count > rangeCheck.max {
                self.mobileNumber.resignFirstResponder()
                return false
            }
            
            if text.count >= rangeCheck.min {
                self.hideContactTable()
                self.confimMobile.text = ""
//                self.amountConst.constant = 0.00
//                self.bonusPointHeightConstraints.constant = 0.0
//                self.amount.isHidden = true
//                self.bonusPointTextField.isHidden = true
//                if let cnfRightView = self.confimMobile.rightView as? PTClearButton {
//                    cnfRightView.hideClearButton(hide: true)
//                }
                
                self.delegate?.showAddObjectButton(show: true)
                self.delegate?.scrollEnable(show: true)
                self.confirmMobConst.constant = 0.0
                if MultiTopupArray.main.controllers.count <= 1 {
                    self.bonusPointHeightConstraints.constant = 50.0
                    self.bonusPointTextField.isHidden = false
                }
                if self.amount.isHidden {
                    self.amountConst.constant = 50.0
                    self.animateView {
                        self.amount.isHidden = false
                    }
                    self.delegate?.updateHeightTableView()
                }
                
//                if self.confimMobile.isHidden {
//                    self.contactSuggestionHeightConstraint.constant = 0
//                    self.confirmMobConst.constant = 50.00
//                    self.animateView {
//                        self.confimMobile.isHidden = false
//                    }
//                    self.delegate?.scrollEnable(show: true)
//                    self.delegate?.showAddObjectButton(show: false)
//                    self.delegate?.updateHeightTableView()
//                }
                self.heightChanges = true
                if text.count == rangeCheck.max {
                    self.mobileNumber.text = text
                    self.delegate?.scrolltoBottom()
//                    self.confimMobile.becomeFirstResponder()
                    self.mobileNumber.resignFirstResponder()
                    return false
                } else if text.count > rangeCheck.max {
                    self.mobileNumber.resignFirstResponder()
                    self.delegate?.scrolltoBottom()
                    return false
                }
                self.mobileNumber.text = text
                return false
            } else {
//                self.confimMobile.text = ""
//                self.amount.text = ""
//                self.amount.rightView = nil
//                self.confimMobile.isUserInteractionEnabled = true
//                self.confirmMobConst.constant = 0.00
//                self.amountConst.constant = 0.00
//                self.bonusPointHeightConstraints.constant = 0.0
//                self.confimMobile.isHidden = true
//                self.amount.isHidden = true
//                self.bonusPointTextField.isHidden = true
//                self.heightChanges = true
                
                self.delegate?.showAddObjectButton(show: false)
//                self.confimMobile.text = text
                self.amountConst.constant = 0.00
                self.bonusPointHeightConstraints.constant = 0.0
                self.amount.isHidden = true
                self.bonusPointTextField.isHidden = true
                
                self.heightChanges = true
            }
        }
        
        if textField == self.confimMobile {
            let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: mobileNumberAcceptableCharacters).inverted
            let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
            if string != filteredSet { return false }
            let oper = operatorName.text?.lowercased()
            if oper == "mpt".lowercased() || oper == "mytel".lowercased(){
                self.amount.placeholder              = "Select Amount".localized
                
            } else {
                self.amount.placeholder              = "Enter or Select Amount".localized
            }
            
            
            if text.count > 2 {
                if self.confimMobile.rightView == nil {
                    confMobile   = PTClearButton.updateView(strTag: "confMobile", delegate: self)
                    self.confimMobile.rightViewMode = .always
                    self.confimMobile.rightView = confMobile
                } else {
                    if let cnfrightView = self.confimMobile.rightView as? PTClearButton {
                        cnfrightView.hideClearButton(hide: false)
                    }
                }
            } else {
                self.amount.text = ""
                self.amount.rightView = nil
                if self.confimMobile.rightView != nil {
                    self.confimMobile.rightView = nil
                }
            }
            
            
            
            if PayToValidations().checkMatchingNumber(string: text, withString: self.mobileNumber.text!) {
                if let textMobile = self.mobileNumber.text, textMobile.count <= text.count {
                    self.delegate?.showAddObjectButton(show: true)
                    self.delegate?.scrollEnable(show: true)
                    self.confimMobile.text = text
                    self.amountConst.constant = 50.0
                    if MultiTopupArray.main.controllers.count <= 1 {
                        self.bonusPointHeightConstraints.constant = 50.0
                        self.bonusPointTextField.isHidden = false
                    }
                    self.view.endEditing(true)
                    self.amount.isHidden = false
                    self.heightChanges = true
                    self.delegate?.scrolltoBottom()
                } else {
                    self.delegate?.showAddObjectButton(show: false)
                    self.confimMobile.text = text
                    self.amountConst.constant = 0.00
                    self.bonusPointHeightConstraints.constant = 0.0
                    self.amount.isHidden = true
                    self.bonusPointTextField.isHidden = true
                    
                    self.heightChanges = true
                }
                return false
            } else {
                return false
            }
        }
        
        if textField == self.amount {
            if text.count > 9 {
                return false
            }
        }
        return true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.mobileNumber || textField == self.confimMobile {
            if textField == self.mobileNumber {
                if let text = self.mobileNumber.text, text.count < 3 {
                    textField.text = "09"
                    mobileNumberView.hideClearButton(hide: true)
                }
            }
            if textField == self.confimMobile {
                //textField.rightView = nil
                if let text = self.confimMobile.text, text.count < 3 {
                    textField.text = "09"
                }
            }
        }
        
        if textField == self.amount {
            self.contactSuggestionHeightConstraint.constant = 0.0
            self.view.endEditing(true)
            
            if let txt = textField.text {
                if MultiTopupArray.main.controllers.count > 1 {
                    if txt.count > 0 {
                        return false
                    }
                }
            }
            
            if let textMobileNumber = self.mobileNumber.text, textMobileNumber.hasPrefix("095") {
                self.extraCheckForCDMA()
                self.delegate?.showAddObjectButton(show: true)
            } else if let navigation = self.navigation, var number = self.mobileNumber.text {
                _ = number.remove(at: String.Index.init(encodedOffset: 0))
                let topupNumber = "0095" + number
                if MultiTopupArray.main.controllers.count > 1 {
                    if MultiTopupArray.main.controllers.count == 2 {
                        if MultiTopupArray.main.controllers.first?.amount.text == "" && MultiTopupArray.main.controllers.first?.amount == textField {
                            MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: topupNumber, delegate: self, present: navigation, otherNumber: true)
                        } else if MultiTopupArray.main.controllers.first?.amount.text != "" {
                           MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: topupNumber, delegate: self, present: navigation, otherNumber: true)
                        } else {
                            alertViewObj.wrapAlert(title: nil, body: "Please select the amount in above frame".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
                            alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                                
                            })
                            alertViewObj.showAlert()
                        }
                    } else if MultiTopupArray.main.controllers.count == 3 {
                        if MultiTopupArray.main.controllers[0].amount.text == "" && MultiTopupArray.main.controllers.first?.amount == textField {
                            MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: topupNumber, delegate: self, present: navigation, otherNumber: true)
                        } else if MultiTopupArray.main.controllers[0].amount.text != "" && MultiTopupArray.main.controllers[1].amount == textField {
                            MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: topupNumber, delegate: self, present: navigation, otherNumber: true)
                        } else if MultiTopupArray.main.controllers[0].amount.text != "" && MultiTopupArray.main.controllers[1].amount.text != "" && MultiTopupArray.main.controllers[2].amount == textField {
                            MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: topupNumber, delegate: self, present: navigation, otherNumber: true)
                        } else {
                            alertViewObj.wrapAlert(title: nil, body: "Please select the amount in above frame".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
                            alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                                
                            })
                            alertViewObj.showAlert()
                        }
                    } else if MultiTopupArray.main.controllers.count == 4 {
                        if MultiTopupArray.main.controllers[0].amount.text == "" && MultiTopupArray.main.controllers.first?.amount == textField {
                            MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: topupNumber, delegate: self, present: navigation, otherNumber: true)
                        } else if MultiTopupArray.main.controllers[0].amount.text != "" && MultiTopupArray.main.controllers[1].amount == textField {
                            MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: topupNumber, delegate: self, present: navigation, otherNumber: true)
                        } else if MultiTopupArray.main.controllers[0].amount.text != "" && MultiTopupArray.main.controllers[1].amount.text != "" && MultiTopupArray.main.controllers[2].amount == textField {
                            MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: topupNumber, delegate: self, present: navigation, otherNumber: true)
                        } else if MultiTopupArray.main.controllers[0].amount.text != "" && MultiTopupArray.main.controllers[1].amount.text != "" && MultiTopupArray.main.controllers[2].amount.text != "" && MultiTopupArray.main.controllers[3].amount == textField {
                            MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: topupNumber, delegate: self, present: navigation, otherNumber: true)
                        } else {
                            alertViewObj.wrapAlert(title: nil, body: "Please select the amount in above frame".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
                            alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                                
                            })
                            alertViewObj.showAlert()
                        }
                    } else if MultiTopupArray.main.controllers.count == 5 {
                        if MultiTopupArray.main.controllers[0].amount.text == "" && MultiTopupArray.main.controllers.first?.amount == textField {
                            MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: topupNumber, delegate: self, present: navigation, otherNumber: true)
                        } else if MultiTopupArray.main.controllers[0].amount.text != "" && MultiTopupArray.main.controllers[1].amount == textField {
                            MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: topupNumber, delegate: self, present: navigation, otherNumber: true)
                        } else if MultiTopupArray.main.controllers[0].amount.text != "" && MultiTopupArray.main.controllers[1].amount.text != "" && MultiTopupArray.main.controllers[2].amount == textField {
                            MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: topupNumber, delegate: self, present: navigation, otherNumber: true)
                        } else if MultiTopupArray.main.controllers[0].amount.text != "" && MultiTopupArray.main.controllers[1].amount.text != "" && MultiTopupArray.main.controllers[2].amount.text != "" && MultiTopupArray.main.controllers[3].amount.text == "" && MultiTopupArray.main.controllers[3].amount == textField {
                            MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: topupNumber, delegate: self, present: navigation, otherNumber: true)
                        } else if MultiTopupArray.main.controllers[0].amount.text != "" && MultiTopupArray.main.controllers[1].amount.text != "" && MultiTopupArray.main.controllers[2].amount.text != "" && MultiTopupArray.main.controllers[3].amount.text != "" && MultiTopupArray.main.controllers[4].amount.text == "" && MultiTopupArray.main.controllers[4].amount == textField {
                            MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: topupNumber, delegate: self, present: navigation, otherNumber: true)
                        } else {
                            alertViewObj.wrapAlert(title: nil, body: "Please select the amount in above frame".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
                            alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                                
                            })
                            alertViewObj.showAlert()
                        }
                    }
                } else {
                    MyNumberTopup.init().navigateToAmountSelectionScreen(mobile: topupNumber, delegate: self, present: navigation, otherNumber: true)
                }
            }
            return false
        }
        
        if textField == self.bonusPointTextField {
            self.view.endEditing(true)
            if self.mobileNumber.text!.hasPrefix("095") {
                alertViewObj.wrapAlert(title: nil, body: "Postpaid mobile number cannot recharge. You can only recharge if your mobile number is Prepaid".localized, img: #imageLiteral(resourceName: "dashboard_other_number"))
                alertViewObj.addAction(title: "Cancel".localized, style: .cancel, action: {
                    self.delegate?.showAddObjectButton(show: true)
                })
                alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                    self.bonusPointCallback()
                })
                alertViewObj.showAlert()
            } else {
                self.bonusPointCallback()
            }
            return false
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == self.mobileNumber {
            //mobileNumberView.hideClearButton(hide: true)
        }
        return true
    }
}

//MARK: - requestMoneyAmountCallback
extension TopupViewController: requestMoneyAmountCallback {
    func didGetAmount(_ amount: String, withPlanType type: String) {
        if (UserLogin.shared.walletBal as NSString).floatValue < (amount.replacingOccurrences(of: ",", with: "") as NSString).floatValue {
            alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
            alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                    addWithdrawView.modalPresentationStyle = .fullScreen
                    self.navigationController?.present(addWithdrawView, animated: true, completion: nil)
                }
            })
            alertViewObj.showAlert()
            return
        }
       
        if MultiTopupArray.main.controllers.count > 1 {
            guard let last = MultiTopupArray.main.controllers[safe: MultiTopupArray.main.controllers.count - 1] else { return }
            for (_, lastObject) in MultiTopupArray.main.controllers.enumerated() {
                if last.mobileNumber.text == lastObject.mobileNumber.text && amount == lastObject.amount.text {
                    alertViewObj.wrapAlert(title: nil, body: "Same Mobile Number with Same Top-Up amount, Same Data Plan or Same Special Offer Not Allowed".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                    alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                    })
                    alertViewObj.showAlert()
                    return
                }
            }
        }
        
        self.amount.text = amount
        self.delegate?.showAddObjectButton(show: true)
        let label   = UILabel.init(frame: .zero)
        label.text  = type
        label.font  = UIFont(name: appFont, size: appFontSize)
        label.textColor = .lightGray
        let size   = type.size(withAttributes: [.font: label.font])
        let vWidth = size.width
        label.frame = CGRect(x: 0, y: 0, width: vWidth + 25.00, height: self.amount.frame.height)
        self.amount.rightViewMode = .always
        self.amount.rightView = label
        self.plantype = type
        self.opName = MyNumberTopup.opName
        if MultiTopupArray.main.controllers.count == 5 {
            if self.amount == MultiTopupArray.main.controllers.last?.amount {
                self.delegate?.singleObjectPayment()
            }
        }
//        if MultiTopupArray.main.controllers.count != 5 {
//            for (index, object) in MultiTopupArray.main.controllers.enumerated() {
//                if index == MultiTopupArray.main.controllers.count - 1 {
//                    if object.amount.text?.count ?? 0 == 0 {
//                        self.delegate?.showAddObjectButton(show: true)
//                    }
//                } else {
//                    if object.amount.text?.count ?? 0 == 0 {
//                        self.delegate?.showAddObjectButton(show: false)
//                    }
//                }
//            }
//        }
    }
    
    func paymentCallOtherNumberThroughDelegate() {
        self.delegate?.singleObjectPayment()
    }
    
    func paymentCallOtherNumberWithCustomMsg(index: String) {
        self.delegate?.singleObjectWithCustomMsg(msg: index)
    }
}

//MARK: - CountryLeftViewDelegate
extension TopupViewController: CountryLeftViewDelegate {
    func openAction(type: ButtonType) {
        self.view.endEditing(true)
        switch type {
        case .countryView:
            break
        case .contactView:
            PTManagerClass.openMultiContactSelection(self, hideScan: true)
        }
    }
    
    func clearActionType() {
        self.confimMobile.leftViewMode = .always
        self.confimMobile.leftView = nil
        self.confimMobile.leftView = DefaultIconView.updateView(icon: "topup_number")
        self.delegate?.scrollEnable(show: false)
        self.contactSuggestionHeightConstraint.constant = 0
        self.hideContactTable()
        self.mobileNumber.text = "09"
        self.confimMobile.text = ""
        self.amount.text = ""
        self.operatorName.text = ""
        self.amount.rightView = nil
        self.confirmMobConst.constant = 0.00
        self.amountConst.constant = 0.00
        self.bonusPointHeightConstraints.constant = 0.0
        self.confimMobile.isHidden = true
        self.amount.isHidden = true
        self.bonusPointTextField.isHidden = true
        
        self.heightChanges = true
        self.confimMobile.isUserInteractionEnabled = true
        self.mobileNumber.becomeFirstResponder()
        
        self.delegate?.changeColor(color: MyNumberTopup.OperatorColorCode.okDefault, name: "")
        self.delegate?.showAddObjectButton(show: false)
        self.delegate?.didShowAdvertisement(show: true)
        mobileNumberView.hideClearButton(hide: true)
        if let cnfView = confimMobile.rightView as? PTClearButton {
            cnfView.hideClearButton(hide: true)
        }
    }
}

//MARK: - ContactPickerDelegate
extension TopupViewController: ContactPickerDelegate {
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        let phoneCount = contact.phoneNumbers.count
        var phoneNumber = ""
        if phoneCount > 0 {
            phoneNumber = contact.phoneNumbers[0].phoneNumber
        }
        
        if phoneNumber.count <= 0 {
            self.amountConst.constant = 0.00
            self.bonusPointHeightConstraints.constant = 0.0
            self.confirmMobConst.constant = 0.00
            self.amount.text = ""
            self.operatorName.text = ""
            self.mobileNumber.text = "09"
            self.confimMobile.text = "09"
            self.amount.text = ""
            self.amount.rightView = nil
            self.amount.isHidden = true
            self.bonusPointTextField.isHidden = true
            self.confimMobile.isHidden = true
            self.heightChanges = true
            self.operatorName.text = ""
            mobileNumberView.hideClearButton(hide: true)
            self.contactsSuggestionTableView.isHidden = true
            PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
            // self.showErrorAlert(errMessage: "Rejected Number".localized)
            self.delegate?.changeColor(color: MyNumberTopup.OperatorColorCode.okDefault, name: "")
            self.delegate?.showAddObjectButton(show: false)
            return
        }
        
        if let cnfView = confimMobile.rightView as? PTClearButton {
            cnfView.hideClearButton(hide: true)
        }
        
        var str = ""
        str = phoneNumber
        if str.count > 0 {
            str = str.replacingOccurrences(of: "(", with: "")
            str = str.replacingOccurrences(of: ")", with: "")
            str = str.trimmingCharacters(in: .whitespaces)
        }
        if str.hasPrefix("00") {
            _ = str.remove(at: String.Index.init(encodedOffset: 0))
            _ = str.remove(at: String.Index.init(encodedOffset: 0))
            str = "+" + str
        }
        
        let country = self.identifyCountry(withPhoneNumber: str)
        if country.0 == "+95" ||  country.1 == "myanmar" ||  country.0 == "" ||  country.1 == "" {
            let countryObject = Country.init(name: "", code: "myanmar", dialCode: "+95")
            if str.hasPrefix(countryObject.dialCode) {
                str = str.replacingOccurrences(of: countryObject.dialCode, with: "")
                str = "0" + str
            } else {
                if str.hasPrefix("0") {
                    
                } else {
                    str = "0" + str
                }
            }
            let number = PayToValidations().getNumberRangeValidation(str)

            if str.count > number.max || str.count < number.min || number.isRejected {
                self.operatorName.text = ""
                mobileNumberView.hideClearButton(hide: true)
                 self.contactsSuggestionTableView.isHidden = true
                self.mobileNumber.text = "09"
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                self.delegate?.showAddObjectButton(show: false)
                self.clearActionType()
                return
            }
            
            self.contactSuggestionHeightConstraint.constant = 0
            self.hideContactTable()
            self.mobileNumber.text = str
            self.confimMobile.text = contact.displayName()
            self.confimMobile.leftViewMode = .always
            self.confimMobile.leftView = nil
            self.confimMobile.leftView = DefaultIconView.updateView(icon: "name_bill")
            self.confimMobile.isUserInteractionEnabled = false
            self.amountConst.constant     = 50.0
            self.confirmMobConst.constant = 50.0
            if MultiTopupArray.main.controllers.count <= 1 {
                self.bonusPointHeightConstraints.constant = 50.0
                self.bonusPointTextField.isHidden = false
            }
            self.animateView {
                self.confimMobile.isHidden = false
                self.amount.isHidden = false
            }
            self.amount.text = ""
            self.amount.rightView = nil
//            if str.count > number.max || str.count < number.min || number.isRejected {
//                self.amountConst.constant = 0.00
//                self.bonusPointHeightConstraints.constant = 0.0
//                self.confirmMobConst.constant = 0.00
//                self.amount.text = ""
//                self.mobileNumber.text = "09"
//                self.confimMobile.text = "09"
//                self.amount.rightView = nil
//                self.amount.isHidden = true
//                self.bonusPointTextField.isHidden = true
//                self.confimMobile.isHidden = true
//                self.heightChanges = true
//                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
//                // self.showErrorAlert(errMessage: "Rejected Number".localized)
//                self.delegate?.showAddObjectButton(show: false)
//                return
//            }
            
//            if str.count < number.min {
//                self.amountConst.constant = 0.00
//                self.bonusPointHeightConstraints.constant = 0.0
//                self.confirmMobConst.constant = 0.00
//                self.amount.text = ""
//                self.mobileNumber.text = "09"
//                self.confimMobile.text = "09"
//                self.amount.rightView = nil
//                self.amount.isHidden = true
//                self.bonusPointTextField.isHidden = true
//                self.confimMobile.isHidden = true
//                self.heightChanges = true
//                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
//                // self.showErrorAlert(errMessage: "Rejected Number".localized)
//                self.delegate?.showAddObjectButton(show: false)
//                return
//            }
//
//            if number.isRejected {
//                self.amountConst.constant = 0.00
//                self.bonusPointHeightConstraints.constant = 0.0
//                self.confirmMobConst.constant = 0.00
//                self.amount.text = ""
//                self.mobileNumber.text = "09"
//                self.confimMobile.text = "09"
//                self.amount.rightView = nil
//                self.amount.isHidden = true
//                self.bonusPointTextField.isHidden = true
//                self.confimMobile.isHidden = true
//                self.heightChanges = true
//                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
//                //                self.showErrorAlert(errMessage: "Rejected Number".localized)
//                self.delegate?.showAddObjectButton(show: false)
//                return
//            }
            self.delegate?.scrollEnable(show: true)
            self.delegate?.showAddObjectButton(show: true)
            if  MultiTopupArray.main.controllers.count > 1 {
                self.delegate?.changeColor(color: MyNumberTopup.OperatorColorCode.okDefault, name: number.operator)
            } else {
                if str.count > 3 {
                    let colorChange = PayToValidations().getColorForOperator(operatorName: number.operator)
                    self.delegate?.changeColor(color: colorChange, name: number.operator)
                } else {
                    self.delegate?.changeColor(color: MyNumberTopup.OperatorColorCode.okDefault, name: "")
                }
            }
            self.operatorName.text = number.operator
            if self.operatorName.text ?? "" == "MPT" {
                self.amount.placeholder = "Select Amount".localized
            } else {
                self.amount.placeholder = "Enter or Select Amount".localized
            }
            self.operatorName.textColor = PayToValidations().getColorForOperator(operatorName: number.operator)
            self.mobileNumberView.hideClearButton(hide: false)
        } else {
            self.delegate?.showAddObjectButton(show: false)
            guard let keyWinndow = UIApplication.shared.keyWindow else { return }
            keyWinndow.makeToast("You Selected Other Country Number".localized, point: keyWinndow.center, title: nil, image: nil, completion: nil)
        }
    }
}

//MARK: - BioMetricLoginDelegate
extension TopupViewController: BioMetricLoginDelegate {
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful, screen.lowercased() == "bonusPointApi".lowercased() {
            self.bonusPointCallback()
        }
    }
}

//MARK: - PTMultiSelectionDelegate
extension TopupViewController: PTMultiSelectionDelegate {
    func didSelectOption(option: PTMultiSelectionOption) {
        self.delegate?.didShowAdvertisement(show: false)
        switch option {
        case .favorite:
            let nav = openFavoriteFromNavigation(self, withFavorite: .topup, selectionType: false,  multiPay: true, payToCount: MultiTopupArray.main.controllers.count - 1)
            self.navigationController?.present(nav, animated: true, completion: nil)
        case .contact:
            
            let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: true)
         //   let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false)
            self.navigationController?.present(nav, animated: true, completion: nil)
        case .scanQR:
            break
        }
    }
}

//MARK: - FavoriteSelectionDelegate
extension TopupViewController: FavoriteSelectionDelegate {
    func selectedFavoriteObject(obj: FavModel) {
        let contact = ContactPicker.init(object: obj)
        self.contact(ContactPickersPicker(), didSelectContact: contact)
    }
}


class RightViewSubClassTextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
       super.init(coder: coder)
    }
    

    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 0, y: 0, width: 0 , height: 50)
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x:0 , y: 0, width: 58 , height: 50)
    }
}

class BonuspointSubClassTextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
       super.init(coder: coder)
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 0, y: 0, width: 0 , height: 50)
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x:0 , y: 1.5, width: 100 , height: 44)
    }
}

class MobileRightViewSubClassTextField: UITextField {
    var cnf = false
    var name = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
       super.init(coder: coder)
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: (cnf) ? (self.frame.size.width - 42) : (self.frame.size.width - 82), y: 0, width: (cnf) ? 42 : 82 , height: 50)
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x:0 , y: 3, width: (name) ? 60 : 100 , height: 44)
    }
}


class TopupMobileRightViewTextField: SearchTextFieldTopUp {
    var cnf = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
       super.init(coder: coder)
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 0, y: 3, width:  58 , height: 44)
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: (cnf) ? (self.frame.size.width - 42) : (self.frame.size.width - 82), y: 0, width: (cnf) ? 42 : 82 , height: 50)
    }
}
