//
//  TopupViewController+ContactView.swift
//  OK
//
//  Created by Imac on 1/17/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation
import Contacts

extension TopupViewController : UITableViewDelegate,UITableViewDataSource {
    
    open func reloadContacts() {
        getContacts( {(contacts, error) in
            if (error == nil) {
                DispatchQueue.main.async(execute: {
                    // self.tableListContacts?.reloadData()
                })
            }
        })
    }
    
    func getContacts(_ completion:  @escaping ContactsHandler) {
        var contactsStore: CNContactStore?
        if contactsStore == nil {
            contactsStore = CNContactStore()
        }
        
        let error = NSError(domain: "ContactPickerPickerErrorDomain", code: 1, userInfo: [NSLocalizedDescriptionKey: "No Contacts Access"])
        
        switch CNContactStore.authorizationStatus(for: CNEntityType.contacts) {
        case CNAuthorizationStatus.denied, CNAuthorizationStatus.restricted:
            //User has denied the current app to access the contacts.
            
            let productName = Bundle.main.infoDictionary!["CFBundleName"]!
            
            let alert = UIAlertController(title: "Unable to access contacts", message: "\(productName) does not have access to contacts. Kindly enable it in privacy settings ", preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: {  action in
                completion([], error)
                self.dismiss(animated: true, completion: {
                    // self.contactDelegate?.contact(self, didContactFetchFailed: error)
                })
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            
        case CNAuthorizationStatus.notDetermined:
            //This case means the user is prompted for the first time for allowing contacts
            contactsStore?.requestAccess(for: CNEntityType.contacts, completionHandler: { (granted, error) -> Void in
                //At this point an alert is provided to the user to provide access to contacts. This will get invoked if a user responds to the alert
                if  (!granted ) {
                    DispatchQueue.main.async(execute: { () -> Void in
                        completion([], error! as NSError?)
                    })
                }
                else {
                    self.getContacts(completion)
                }
            })
        case  CNAuthorizationStatus.authorized:
            //Authorization granted by user for this app.
            
            let contactFetchRequest = CNContactFetchRequest(keysToFetch: allowedContactKeys())
            do {
                try contactsStore?.enumerateContacts(with: contactFetchRequest, usingBlock: { (contact, stop) -> Void in
                    //Ordering contacts based on alphabets in firstname
                    // changes to display all contacts according to user details
                    if self.contactsArray.count > 1 {
                        for phone in contact.phoneNumbers {
                            let contactMutable = CNMutableContact()
                            let stringPhone = phone.value.stringValue
                            if stringPhone.count > 3 {
                                let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue :stringPhone ))
                                contactMutable.givenName = (contact.givenName.count > 0) ? contact.givenName : "Unknown"
                                contactMutable.phoneNumbers = [homePhone]
                                self.contactsArray.append(contactMutable)
                            }
                        }
                    } else {
                        self.contactsArray.append(contact)
                        var contacts = [CNContact]()
                        contacts.append(contact)
                    }
                })
                completion(contactsArray, nil)
            }
                //Catching exception as enumerateContactsWithFetchRequest can throw errors
            catch let error as NSError {
                println_debug(error.localizedDescription)
            }
        }
    }
    
    func allowedContactKeys() -> [CNKeyDescriptor]{
        //We have to provide only the keys which we have to access. We should avoid unnecessary keys when fetching the contact. Reducing the keys means faster the access.
        return [CNContactNamePrefixKey as CNKeyDescriptor,
                CNContactGivenNameKey as CNKeyDescriptor,
                CNContactFamilyNameKey as CNKeyDescriptor,
                CNContactOrganizationNameKey as CNKeyDescriptor,
                CNContactBirthdayKey as CNKeyDescriptor,
                CNContactImageDataKey as CNKeyDescriptor,
                CNContactThumbnailImageDataKey as CNKeyDescriptor,
                CNContactImageDataAvailableKey as CNKeyDescriptor,
                CNContactPhoneNumbersKey as CNKeyDescriptor,
                CNContactEmailAddressesKey as CNKeyDescriptor,
        ]
    }
    
    
    //    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //        return self.contactsArray.count
    //    }
    //
    //    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    //        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
    //        let info = contactsArray[indexPath.row]
    //        cell.textLabel?.text = info.givenName
    //        let num = info.phoneNumbers[0]
    //        cell.detailTextLabel?.text = num.value.stringValue
    //        return cell
    //    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recentContactsSuggestionArr.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = contactsSuggestionTableView.dequeueReusableCell(withIdentifier: "ContactSuggestionTopUpCell", for: indexPath) as! ContactSuggestionTopUpTableViewCell
        let number = self.recentContactsSuggestionArr[indexPath.row].number?.replacingOccurrences(of: "0095", with: "0")
        cell.contactNoLbl.text = number
        cell.operatorLbl.text = self.recentContactsSuggestionArr[indexPath.row].operatorName
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        let enteredAmount = (self.recentContactsSuggestionArr[indexPath.row].amount! as NSString).integerValue
        let formattedNum = formatter.string(from: NSNumber(value: enteredAmount))
        cell.amountLbl.text = formattedNum! + " MMK"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.didShowAdvertisement(show: false)
        let num = self.recentContactsSuggestionArr[indexPath.row].number?.replacingOccurrences(of: "0095", with: "0")
        self.mobileNumber.text = num
        self.confimMobile.text = "UNKNOWN"
        self.contactSuggestionHeightConstraint.constant = 0
        self.contactsSuggestionTableView.isHidden = true
        self.confimMobile.leftViewMode = .always
        self.confimMobile.leftView = nil
        self.confimMobile.leftView = DefaultIconView.updateView(icon: "name_bill")
        self.confimMobile.isUserInteractionEnabled = false
        self.amountConst.constant     = 50.0
        self.confirmMobConst.constant = 50.0
        if MultiTopupArray.main.controllers.count <= 1 {
            self.bonusPointHeightConstraints.constant = 50.0
            self.bonusPointTextField.isHidden = false
        }
        self.animateView {
            self.confimMobile.isHidden = false
            self.amount.isHidden = false
        }
        self.amount.text = ""
        self.amount.rightView = nil
        let number = PayToValidations().getNumberRangeValidation(self.mobileNumber.text!)
        self.delegate?.showAddObjectButton(show: true)
        if  MultiTopupArray.main.controllers.count > 1 {
            self.delegate?.changeColor(color: MyNumberTopup.OperatorColorCode.okDefault, name: number.operator)
        } else {
            if num!.count > 3 {
                let colorChange = PayToValidations().getColorForOperator(operatorName: number.operator)
                self.delegate?.changeColor(color: colorChange, name: number.operator)
            } else {
                self.delegate?.changeColor(color: MyNumberTopup.OperatorColorCode.okDefault, name: "")
            }
        }
        self.operatorName.text = number.operator
        self.operatorName.textColor = PayToValidations().getColorForOperator(operatorName: number.operator)
    }
    
}


class topupContactCell : UITableViewCell {
    
}

class ContactSuggestionTopUpTableViewCell: UITableViewCell {
    @IBOutlet var contactNoLbl: UILabel!
    @IBOutlet var operatorLbl: UILabel!
    @IBOutlet var amountLbl: UILabel!
    
}
