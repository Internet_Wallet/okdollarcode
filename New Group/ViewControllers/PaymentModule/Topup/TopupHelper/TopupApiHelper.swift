//
//  TopupApiHelper.swift
//  OK
//
//  Created by Ashish on 1/24/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct TopupUrl {
    
    struct TopupUrlConfig {
        
        static let dataPlanProdUrl = "https://www.okdollar.co/RestService.svc/RetriveTopupOffers?Telco=%@&Type=datapack"
        static let dataPlanTestUrl = "http://69.160.4.151:8001/RestService.svc/RetriveTopupOffers?Telco=%@&Type=datapack"
        
        static let rchrgPlanProdUrl = "https://www.okdollar.co/RestService.svc/RetriveTopupOffers?Telco=%@&Type=topup"
        static let rchrgPlanTestUrl = "http://69.160.4.151:8001/RestService.svc/RetriveTopupOffers?Telco=%@&Type=topup"
        
        static let specialOfferProdUrl = "https://www.okdollar.co/RestService.svc/RetriveTopupOffers?Telco=%@&Type=specialoffer"
        static let specialOfferTestUrl = "http://69.160.4.151:8001/RestService.svc/RetriveTopupOffers?Telco=%@&Type=specialoffer"
        
        static let merchNoChckProdUrl = "https://www.okdollar.co/RestService.svc/RetrieveTopUpSim?operator=%@&MobileNumber=%@&Type=%@&ByCellID=false&Customer=%@&Amount=%@&Long=%@&CellID=0&Lat=%@&balance=%@"
        static let merchNoChckTestUrl = "http://69.160.4.151:8001/RestService.svc/RetrieveTopUpSim?operator=%@&MobileNumber=%@&Type=%@&ByCellID=false&Customer=%@&Amount=%@&Long=%@&CellID=0&Lat=%@&balance=%@"
        
        static let cashBackProdUrl = "http://www.okdollar.net/WebServiceIpay/services/request;requesttype=FEELIMITKICKBACKINFO;agentcode=%@;destination=%@;amount=%@;pin=%@;vendorcode=IPAY;clientip=error;clientos=ios;transtype=%@;clienttype=GPRS;securetoken=%@"
        static let cashBackTestUrl = "http://120.50.43.150:8090/WebServiceIpay/services/request;requesttype=FEELIMITKICKBACKINFO;agentcode=%@;destination=%@;amount=%@;pin=%@;vendorcode=IPAY;clientip=error;clientos=ios;transtype=%@;clienttype=GPRS;securetoken=%@"
        
        static let myNumberPayProdUrl = "https://www.okdollar.co/RestService.svc//GenericPayment"
        static let myNumberPayTestUrl = "http://69.160.4.151:8001/RestService.svc//GenericPayment"
        
        static let mectelSingleProdUrl = "http://www.okdollar.net/WebServiceIpay/services/request;requesttype=TOPUP;agentcode=%@;destination=%@;amount=%@;source=%@;comments=%@;pin=%@;vendorcode=IPAY;clienttype=GPRS;securetoken=%@;productcode=%@;operator=MECTEL"
        static let mectelSingleTestUrl = "http://120.50.43.150:8090/WebServiceIpay/services/request;requesttype=TOPUP;agentcode=%@;destination=%@;amount=%@;source=%@;comments=%@;pin=%@;vendorcode=IPAY;clienttype=GPRS;securetoken=%@;productcode=%@;operator=MECTEL"
        
        static let oNoTopupRtrvProdUrl = "https://www.okdollar.co/RestService.svc/MultipleRetrieveTopUpSim"
        static let oNoTopupRtrvTestUrl = "http://69.160.4.151:8001/RestService.svc/MultipleRetrieveTopUpSim"
        
        static let oNoMultiCshBackProdUrl = "https://www.okdollar.co/RestService.svc/MultiCashBack"
        static let oNoMultiCshBackTestUrl = "http://69.160.4.151:8001/RestService.svc/MultiCashBack"
        
        static let oNoMultiPayProdUrl = "https://www.okdollar.co/RestService.svc/MultipleTopup"
        static let oNoMultiPayTestUrl = "http://69.160.4.151:8001/RestService.svc/MultipleTopup"
        
        static let oNoSinglePayProdUrl = "https://www.okdollar.co/RestService.svc/GenericPayment"
        static let oNoSinglePayTestUrl = "http://69.160.4.151:8001/RestService.svc/GenericPayment"
        
        static let cdmaChckProdUrl = "https://www.okdollar.co/RestService.svc/VerifyPostPaidNumber?&MobileNumber=\(UserModel.shared.mobileNo)&Simid=\(simid)&MSID=\(msid)&OSType=1&OTP=&CustomerNumber=%@"
        static let cdmaChckTestUrl = "http://69.160.4.151:8001/RestService.svc/VerifyPostPaidNumber?&MobileNumber=\(UserModel.shared.mobileNo)&Simid=\(simid)&MSID=\(msid)&OSType=1&OTP=&CustomerNumber=%@"
        
        static let reqOkContactProdUrl = "http://advertisement.api.okdollar.org/AdService.svc/VerifyMultiOkDollarUser"
        static let reqOkContactTestUrl = "http://advertisement.api.okdollar.org/AdService.svc/VerifyMultiOkDollarUser"
    }
    
    #if DEBUG
    static let dataPlan = serverUrl == .productionUrl ? TopupUrlConfig.dataPlanProdUrl : TopupUrlConfig.dataPlanTestUrl
    static let rechargePlan = serverUrl == .productionUrl ? TopupUrlConfig.rchrgPlanProdUrl : TopupUrlConfig.rchrgPlanTestUrl
    static let specialOffer = serverUrl == .productionUrl ? TopupUrlConfig.specialOfferProdUrl : TopupUrlConfig.specialOfferTestUrl
    static let merchantNumberCheck = serverUrl == .productionUrl ? TopupUrlConfig.merchNoChckProdUrl : TopupUrlConfig.merchNoChckTestUrl
    static let cashback = serverUrl == .productionUrl ? TopupUrlConfig.cashBackProdUrl : TopupUrlConfig.cashBackTestUrl
    static let myNumberPayment = serverUrl == .productionUrl ? TopupUrlConfig.myNumberPayProdUrl : TopupUrlConfig.myNumberPayTestUrl
    static let mectelSinglePay = serverUrl == .productionUrl ? TopupUrlConfig.mectelSingleProdUrl : TopupUrlConfig.mectelSingleTestUrl
    static let otherNumberTopupRetrieve = serverUrl == .productionUrl ? TopupUrlConfig.oNoTopupRtrvProdUrl : TopupUrlConfig.oNoTopupRtrvTestUrl
    static let otherNumberMultiCashback = serverUrl == .productionUrl ? TopupUrlConfig.oNoMultiCshBackProdUrl : TopupUrlConfig.oNoMultiCshBackTestUrl
    static let otherNumberMultiPayment  = serverUrl == .productionUrl ? TopupUrlConfig.oNoMultiPayProdUrl : TopupUrlConfig.oNoMultiPayTestUrl
    static let otherNumberSinglePayment = serverUrl == .productionUrl ? TopupUrlConfig.oNoSinglePayProdUrl: TopupUrlConfig.oNoSinglePayTestUrl
    static let cdmaCheck = serverUrl == .productionUrl ? TopupUrlConfig.cdmaChckProdUrl : TopupUrlConfig.cdmaChckTestUrl
    static let getOkContacts = serverUrl == .productionUrl ? TopupUrlConfig.reqOkContactProdUrl : TopupUrlConfig.reqOkContactTestUrl
    #else
    static let dataPlan = TopupUrlConfig.dataPlanProdUrl
    static let rechargePlan = TopupUrlConfig.rchrgPlanProdUrl
    static let specialOffer = TopupUrlConfig.specialOfferProdUrl
    static let merchantNumberCheck = TopupUrlConfig.merchNoChckProdUrl
    static let cashback = TopupUrlConfig.cashBackProdUrl
    static let myNumberPayment = TopupUrlConfig.myNumberPayProdUrl
    static let mectelSinglePay = TopupUrlConfig.mectelSingleProdUrl
    static let otherNumberTopupRetrieve = TopupUrlConfig.oNoTopupRtrvProdUrl
    static let otherNumberMultiCashback = TopupUrlConfig.oNoMultiCshBackProdUrl
    static let otherNumberMultiPayment  = TopupUrlConfig.oNoMultiPayProdUrl
    static let otherNumberSinglePayment = TopupUrlConfig.oNoSinglePayProdUrl
    static let cdmaCheck = TopupUrlConfig.cdmaChckProdUrl
    static let getOkContacts = TopupUrlConfig.reqOkContactProdUrl
    #endif
}

class TopupWeb: NSObject {
    
    class func genericClassWithoutLoader(url: URL, param: AnyObject, httpMethod: String, handle :@escaping (_ result: Any, _ success: Bool) -> Void) {
        
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.capitalized
        
        if httpMethod == "POST" {
            
            if let stringParams = param as? String {
                let postLength = NSString(format:"%lu", stringParams.count) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = stringParams.data(using: String.Encoding.utf8, allowLossyConversion:true)
            } else {
                let theJSONData = try? JSONSerialization.data(
                    withJSONObject: param ,
                    options: JSONSerialization.WritingOptions(rawValue: 0))
                let jsonString          = NSString(data: theJSONData!,encoding: String.Encoding.ascii.rawValue)
                let postLength = NSString(format:"%lu", jsonString!.length) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
            }
        }
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if((error) != nil) {
                handle(error!.localizedDescription, false)
            }else {
                do {
                    guard let dataResponse = data else {
                        handle("error", false)
                        return }
                    let json = try JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments)
                    handle(json, true)
                } catch {
                    handle(error.localizedDescription, false)
                }
            }
        }
        dataTask.resume()
    }
    
    class func genericClassData(url: URL, param: AnyObject, httpMethod: String, handle :@escaping (_ result: Any, _ success: Bool) -> Void) {
        PTLoader.shared.show()
        
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.capitalized
        
        if httpMethod == "POST" {
            
            if let stringParams = param as? String {
                let postLength = NSString(format:"%lu", stringParams.count) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = stringParams.data(using: String.Encoding.utf8, allowLossyConversion:true)
            } else if let stringParams = param as? Data {
                request.httpBody = stringParams
            } else {
                let theJSONData = try? JSONSerialization.data(
                    withJSONObject: param ,
                    options: JSONSerialization.WritingOptions(rawValue: 0))
                let jsonString          = NSString(data: theJSONData!,encoding: String.Encoding.ascii.rawValue)
                let postLength = NSString(format:"%lu", jsonString!.length) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
            }
        }
        println_debug(request)
        println_debug(required_argument)
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            PTLoader.shared.hide()
            if((error) != nil) {
                PTLoader.shared.hide()
                handle(error!.localizedDescription, false)
            }else {
                    PTLoader.shared.hide()
                if let dataRaw = data {
                    handle(dataRaw, true)
                }
        }
        
    }
        dataTask.resume()
    }

    class func genericClassWithHeaderInfo(url: URL, param: AnyObject, httpMethod: String, isCicoPayTo: Bool? = false, handle :@escaping (_ result: Any, _ success: Bool) -> Void) {
        if let status = isCicoPayTo, status == true {
            
        } else {
            PTLoader.shared.show()
        }
        
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.capitalized
      
        
        if httpMethod == "POST" {
            
            if let stringParams = param as? String {
                let postLength = NSString(format:"%lu", stringParams.count) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = stringParams.data(using: String.Encoding.utf8, allowLossyConversion:true)
            } else {
                let theJSONData = try? JSONSerialization.data(
                    withJSONObject: param ,
                    options: JSONSerialization.WritingOptions(rawValue: 0))
                let jsonString          = NSString(data: theJSONData!,encoding: String.Encoding.ascii.rawValue)
                let postLength = NSString(format:"%lu", jsonString!.length) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
            }
        }
        println_debug(request)
        println_debug(required_argument)
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            PTLoader.shared.hide()
            if((error) != nil) {
                PTLoader.shared.hide()
                handle(error!.localizedDescription, false)
            }else {
                do {
                    guard let dataResponse = response else {
                        handle("error", false)
                        return }
                    PTLoader.shared.hide()
                    handle(dataResponse, true)
                }catch {
                    PTLoader.shared.hide()
                    handle(error.localizedDescription, false)
                }
            }
        }
        dataTask.resume()
    }
    class func genericClassWithHeader(url: URL, param: AnyObject, httpMethod: String, httpHeader: String, isCicoPayTo: Bool? = false, handle :@escaping (_ result: Any, _ success: Bool) -> Void) {
        if let status = isCicoPayTo, status == true {
            
        } else {
            PTLoader.shared.show()
        }
        
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.capitalized
        request.allHTTPHeaderFields = ["X-Auth-Token": httpHeader]
        if httpMethod == "POST" {
            if let stringParams = param as? String {
                let postLength = NSString(format:"%lu", stringParams.count) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = stringParams.data(using: String.Encoding.utf8, allowLossyConversion:true)
            } else {
                let theJSONData = try? JSONSerialization.data(
                    withJSONObject: param ,
                    options: JSONSerialization.WritingOptions(rawValue: 0))
                let jsonString          = NSString(data: theJSONData!,encoding: String.Encoding.ascii.rawValue)
                let postLength = NSString(format:"%lu", jsonString!.length) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
            }
        }
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            PTLoader.shared.hide()
            if((error) != nil) {
                PTLoader.shared.hide()
                handle(error!.localizedDescription, false)
            }else {
                do {
                    guard let dataResponse = data else {
                        handle("error", false)
                        return }
                    let json = try JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments)
                    PTLoader.shared.hide()
                    handle(json, true)
                } catch {
                    PTLoader.shared.hide()
                    handle(error.localizedDescription, false)
                }
            }
        }
        dataTask.resume()
    }
    
    
    class func genericClassForFacePay(url: URL, param: AnyObject, httpMethod: String, isCicoPayTo: Bool? = false, handle :@escaping (_ result: Any, _ success: Bool) -> Void) {
        if let status = isCicoPayTo, status == true {
            
        } else {
           // PTLoader.shared.show()
        }
        
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.capitalized
        
        if httpMethod == "POST" {
            
            if let stringParams = param as? String {
                let postLength = NSString(format:"%lu", stringParams.count) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = stringParams.data(using: String.Encoding.utf8, allowLossyConversion:true)
            } else {
                let theJSONData = try? JSONSerialization.data(
                    withJSONObject: param ,
                    options: JSONSerialization.WritingOptions(rawValue: 0))
                let jsonString          = NSString(data: theJSONData!,encoding: String.Encoding.ascii.rawValue)
                let postLength = NSString(format:"%lu", jsonString!.length) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
            }
        }
        println_debug(request)
        println_debug(required_argument)
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if((error) != nil) {
                PTLoader.shared.hide()
                handle(error!.localizedDescription, false)
            }else {
                do {
                    guard let dataResponse = data else {
                        handle("error", false)
                        return }
                    let json = try JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments)
                    handle(json, true)
                } catch {
                    PTLoader.shared.hide()
                    handle(error.localizedDescription, false)
                }
            }
        }
        dataTask.resume()
    }
    
    class func genericClass(url: URL, param: AnyObject, httpMethod: String, isCicoPayTo: Bool? = false, handle :@escaping (_ result: Any, _ success: Bool) -> Void) {
        if let status = isCicoPayTo, status == true {
            
        } else {
            PTLoader.shared.show()
        }
        
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.capitalized
        
        if httpMethod == "POST" {
            
            if let stringParams = param as? String {
                let postLength = NSString(format:"%lu", stringParams.count) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = stringParams.data(using: String.Encoding.utf8, allowLossyConversion:true)
            } else {
                let theJSONData = try? JSONSerialization.data(
                    withJSONObject: param ,
                    options: JSONSerialization.WritingOptions(rawValue: 0))
                let jsonString          = NSString(data: theJSONData!,encoding: String.Encoding.ascii.rawValue)
                let postLength = NSString(format:"%lu", jsonString!.length) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
            }
        }
        println_debug(request)
        println_debug(required_argument)
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            PTLoader.shared.hide()
            if((error) != nil) {
                PTLoader.shared.hide()
                handle(error!.localizedDescription, false)
            }else {
                do {
                    guard let dataResponse = data else {
                        handle("error", false)
                        return }
                    let json = try JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments)
                    PTLoader.shared.hide()
                    handle(json, true)
                } catch {
                    PTLoader.shared.hide()
                    handle(error.localizedDescription, false)
                }
            }
        }
        dataTask.resume()
    }
    
    class func genericClassLogin(url: URL, param: AnyObject, httpMethod: String, handle :@escaping (_ result: Any, _ success: Bool) -> Void) {
        
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.capitalized
        
        if httpMethod == "POST" {
            
            if let stringParams = param as? String {
                let postLength = NSString(format:"%lu", stringParams.count) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = stringParams.data(using: String.Encoding.utf8, allowLossyConversion:true)
            } else {
                let theJSONData = try? JSONSerialization.data(
                    withJSONObject: param ,
                    options: JSONSerialization.WritingOptions(rawValue: 0))
                let jsonString          = NSString(data: theJSONData!,encoding: String.Encoding.ascii.rawValue)
                let postLength = NSString(format:"%lu", jsonString!.length) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
            }
        }
        println_debug(request)
        println_debug(required_argument)
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if((error) != nil) {
                handle(error!.localizedDescription, false)
            }else {
                do {
                    guard let dataResponse = data else {
                        handle("error", false)
                        return }
                    let json = try JSONSerialization.jsonObject(with: dataResponse, options: .allowFragments)
                    handle(json, true)
                } catch {
                    handle(error.localizedDescription, false)
                }
            }
        }
        dataTask.resume()
    }
    
    class func genericClassXML(url: URL, param: AnyObject, httpMethod: String , handle :@escaping (_ result: Any?, _ success: Bool) -> Void ) {
        
        PTLoader.shared.show()
        
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.capitalized
        
        if httpMethod == "POST" {
            let theJSONData = try? JSONSerialization.data(
                withJSONObject: param ,
                options: JSONSerialization.WritingOptions(rawValue: 0))
            let jsonString          = NSString(data: theJSONData!,
                                               encoding: String.Encoding.ascii.rawValue)
            let postLength = NSString(format:"%lu", jsonString!.length) as String
            request.setValue(postLength, forHTTPHeaderField:"Content-Length")
            request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
        }
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if((error) != nil) {
                PTLoader.shared.hide()
                handle(error!.localizedDescription, false)
            }else {
                if data == nil {
                    handle(nil, false)
                    PTLoader.shared.hide()
                    return
                }
                
                let parsedXML = String.init(data: data!, encoding: String.Encoding.utf8)
                handle(parsedXML, true)
                
                PTLoader.shared.hide()
            }
            
        }
        dataTask.resume()
    }
    
     class   func genericClassXMLWithoutLoader(url: URL, param: AnyObject, httpMethod: String , handle :@escaping (_ result: Any?, _ success: Bool) -> Void ) {
        
        PTLoader.shared.show()
        
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.capitalized
        
        if httpMethod == "POST" {
            let theJSONData = try? JSONSerialization.data(
                withJSONObject: param ,
                options: JSONSerialization.WritingOptions(rawValue: 0))
            let jsonString          = NSString(data: theJSONData!,
                                               encoding: String.Encoding.ascii.rawValue)
            let postLength = NSString(format:"%lu", jsonString!.length) as String
            request.setValue(postLength, forHTTPHeaderField:"Content-Length")
            request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
        }
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if((error) != nil) {
                PTLoader.shared.hide()
                handle(error!.localizedDescription, false)
            }else {
                if data == nil {
                    handle(nil, false)
                    PTLoader.shared.hide()
                    return
                }
                
                let parsedXML = String.init(data: data!, encoding: String.Encoding.utf8)
                handle(parsedXML, true)
                
                PTLoader.shared.hide()
            }
            
        }
        dataTask.resume()
    }
    
    class func genericApiWithHeader(url: URL, param: AnyObject, httpMethod: String, header: String?,data: Data? = nil, handle :@escaping (_ result: Any, _ success: Bool) -> Void) {
        PTLoader.shared.show()
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        if let auth = header {
            request.addValue(auth, forHTTPHeaderField: "Authorization")
        }
        
        request.httpMethod = httpMethod.capitalized
        
        if httpMethod == "POST" {
            
            if let stringParams = param as? String {
                let postLength = NSString(format:"%lu", stringParams.count) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = stringParams.data(using: String.Encoding.utf8, allowLossyConversion:true)
            } else {
                let theJSONData = try? JSONSerialization.data(
                    withJSONObject: param ,
                    options: JSONSerialization.WritingOptions(rawValue: 0))
                let jsonString          = NSString(data: theJSONData!,encoding: String.Encoding.ascii.rawValue)
                let postLength = NSString(format:"%lu", jsonString!.length) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
            }
            
            if let requestData = data {
                request.httpBody = requestData
            }
        }
                
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if((error) != nil) {
                PTLoader.shared.hide()
                handle(error!.localizedDescription, false)
            } else {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                    PTLoader.shared.hide()
                    handle(json, true)
                } catch {
                    PTLoader.shared.hide()
                    handle(error.localizedDescription, false)
                }
            }
        }
        dataTask.resume()
    }
}
