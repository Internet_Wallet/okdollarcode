//
//  OtherNumberModel.swift
//  OK
//
//  Created by Ashish on 5/29/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit



struct OtherNumberResponse {
    
    var agentcode : String?
    var agentname : String?
    var amount : String?
    var clientip : String?
    var clientos : String?
    var clienttype : String?
    var comments : String?
    var destination : String?
    var destlevel : String?
    var fee : String?
    var greeting : String?
    var iskickbackmsisdnregister : String?
    var isregisteragent : String?
    var kickbackenable : String?
    var kickvalue : String?
    var kickwallet : String?
    var loyaltypoints : String?
    var maxwalletvalue : String?
    var merchantname : String?
    var merchantnamesms : String?
    var minwalletvalue : String?
    var operators : String?
    var oprwallet : String?
    var prekickwallet : String?
    var preoprwallet : String?
    var prewalletbalance : String?
    var referredlang : String?
    var requestcts : String?
    var responsects : String?
    var responsevalue : String?
    var resultcode : String?
    var resultdescription : String?
    var rtranstype : String?
    var securetoken : String?
    var source : String?
    var transid : String?
    var unregipostbalance : String?
    var unregiprebalance : String?
    var vendorcode : String?
    var walletalertvalue : String?
    var walletbalance : String?
    
    //extra property for this functions
    var name = "Unknown"
    var number = ""
    var operatorName = ""
    var type = ""
    var cashback = ""
    var developerCashback = ""
    var topupAmount = ""
    var developerAmount = ""
    var paymentNumber  = ""
    
    
    init(fromDictionary dictionary: [String:Any], model: TopupConfirmationModel){
        agentcode = dictionary["agentcode"] as? String ?? ""
        agentname = dictionary["agentname"] as? String ?? ""
        amount = dictionary["amount"] as? String ?? ""
        clientip = dictionary["clientip"] as? String ?? ""
        clientos = dictionary["clientos"] as? String ?? ""
        clienttype = dictionary["clienttype"] as? String ?? ""
        comments = dictionary["comments"] as? String ?? ""
        destination = dictionary["destination"] as? String ?? ""
        destlevel = dictionary["destlevel"] as? String ?? ""
        fee = dictionary["fee"] as? String ?? ""
        greeting = dictionary["greeting"] as? String ?? ""
        iskickbackmsisdnregister = dictionary["iskickbackmsisdnregister"] as? String ?? ""
        isregisteragent = dictionary["isregisteragent"] as? String ?? ""
        kickbackenable = dictionary["kickbackenable"] as? String ?? ""
        kickvalue = dictionary["kickvalue"] as? String ?? ""
        kickwallet = dictionary["kickwallet"] as? String ?? ""
        loyaltypoints = dictionary["loyaltypoints"] as? String ?? ""
        maxwalletvalue = dictionary["maxwalletvalue"] as? String ?? ""
        merchantname = dictionary["merchantname"] as? String ?? ""
        merchantnamesms = dictionary["merchantnamesms"] as? String ?? ""
        minwalletvalue = dictionary["minwalletvalue"] as? String ?? ""
        operators = dictionary["operator"] as? String ?? ""
        oprwallet = dictionary["oprwallet"] as? String ?? ""
        prekickwallet = dictionary["prekickwallet"] as? String ?? ""
        preoprwallet = dictionary["preoprwallet"] as? String ?? ""
        prewalletbalance = dictionary["prewalletbalance"] as? String ?? ""
        referredlang = dictionary["referredlang"] as? String ?? ""
        requestcts = dictionary["requestcts"] as? String ?? ""
        responsects = dictionary["responsects"] as? String ?? ""
        responsevalue = dictionary["responsevalue"] as? String ?? ""
        resultcode = dictionary["resultcode"] as? String ?? ""
        resultdescription = dictionary["resultdescription"] as? String ?? ""
        rtranstype = dictionary["rtranstype"] as? String ?? ""
        securetoken = dictionary["securetoken"] as? String ?? ""
        source = dictionary["source"] as? String ?? ""
        transid = dictionary["transid"] as? String ?? ""
        unregipostbalance = dictionary["unregipostbalance"] as? String ?? ""
        unregiprebalance = dictionary["unregiprebalance"] as? String ?? ""
        vendorcode = dictionary["vendorcode"] as? String ?? ""
        walletalertvalue = dictionary["walletalertvalue"] as? String ?? ""
        walletbalance = dictionary["walletbalance"] as? String ?? ""
        
        self.name = model.name
        self.number = model.number
        self.operatorName = model.operatorName
        self.type = model.type
        self.cashback = model.cashback
        self.topupAmount = model.topupAmount
        self.developerAmount = model.developerAmount
        self.developerCashback = model.developerCashback
        self.paymentNumber = model.paymentNumber

    }
}
