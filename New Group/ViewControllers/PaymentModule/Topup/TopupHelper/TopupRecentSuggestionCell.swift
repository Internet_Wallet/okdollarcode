//
//  TopupRecentSuggestionCell.swift
//  OK
//
//  Created by Ashish on 12/22/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit

class TopupRecentSuggestionCell: UITableViewCell {
    
    static let reusableIdentifier = "TopupRecentSuggestionCell"
    
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var operatorName: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func wrapCell(_ number: String, opName: String, amount: String) {
        self.number.text        = number
        self.operatorName.text  = opName
        self.amount.text        = amount
    }
    
}
