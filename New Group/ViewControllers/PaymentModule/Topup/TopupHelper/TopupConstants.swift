//
//  TopupConstants.swift
//  OK
//
//  Created by Ashish on 1/26/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

public class TopupConstants {
    
    public static var otherNumber: String?
    
    public static var topupThemeColor : UIColor = TopupConstants.OperatorColorCode.yellow  {
        didSet {
            NotificationCenter.default.post(name: .topupThemeChanged, object: TopupConstants.topupThemeColor)
        }
    }
    
    struct OperatorColorCode {
        static let yellow = UIColor.init(hex: "F3C632")
        static let red = UIColor.red
    }
    
}

@objc class MyNumberTopup : NSObject  {
    
    struct OperatorColorCode {
        static let mpt     = UIColor.init(hex: "004C92")
        static let telenor = UIColor.init(hex: "069ADF")
        static let oredo   = UIColor.init(hex: "DF0327")
        static let mactel  = UIColor.init(hex: "214C8D")
        static let mytel = UIColor.init(hex: "DF8119")
        static let okDefault = UIColor.init(hex: "F3C632")
    }
    
    enum OperatorCode {
        case mpt, telenor, ooreedo, mactel, mytel, okDefault
    }
    
    func navigateToAmountSelectionScreen(mobile: String, delegate: requestMoneyAmountCallback, present: UIViewController, otherNumber: Bool = false, index: TopupButtonEnum = TopupButtonEnum.topup, isPlusSelected : Bool = false, amountValidationCheck: Bool = false) {
        let vc = UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "myNumberNavigation"))
        
        if let navigation = vc as? UINavigationController {
            for controller in navigation.viewControllers {
                if let control = controller as? TopupMyMainViewController {
                    control.requestMoneyNumber = mobile
                    control.delegate = delegate
                    control.isPlusSelected = isPlusSelected
                    control.otherNumberTopup = (otherNumber) ? "" : nil
                    control.buttonTapped = index
                    control.amountValidationCheck = amountValidationCheck
                }
            }
        }
        
        present.present(vc, animated: true, completion: nil)
    }
    
  @objc public static var theme = UIColor.init(hex: "F3C632")
    public static var opName = ""
    public static var topupNumber = ""
    
    public static var operatorCase = MyNumberTopup.OperatorCode.mpt {
        didSet {
            if operatorCase == .mpt {
                MyNumberTopup.theme  = MyNumberTopup.OperatorColorCode.mpt
                MyNumberTopup.opName = "MPT"
            } else if operatorCase == .mactel {
                MyNumberTopup.theme  = MyNumberTopup.OperatorColorCode.mactel
                MyNumberTopup.opName = "MecTel"
            } else if operatorCase == .telenor {
                MyNumberTopup.theme  = MyNumberTopup.OperatorColorCode.telenor
                MyNumberTopup.opName = "Telenor"
            } else if operatorCase == .ooreedo {
                MyNumberTopup.theme  = MyNumberTopup.OperatorColorCode.oredo
                MyNumberTopup.opName = "Ooredoo"
            } else if operatorCase == .mytel{
                MyNumberTopup.theme = MyNumberTopup.OperatorColorCode.mytel
                MyNumberTopup.opName = "Mytel"
            }else if operatorCase == .okDefault {
                MyNumberTopup.theme  = MyNumberTopup.OperatorColorCode.okDefault
                MyNumberTopup.opName = ""
            }
        }
    }
}


extension Notification.Name {
    static let topupThemeChanged = Notification.Name("topupThemeChanged")
}
