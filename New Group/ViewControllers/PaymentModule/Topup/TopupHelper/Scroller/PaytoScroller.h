//
//  PaytoScroller.h
//  
//
//  Created by Jin Sasaki on 2015/05/30.
//
//

#import <UIKit/UIKit.h>

@class PaytoScroller;

#pragma mark - Delegate functions
@protocol PaytoScrollerDelegate <NSObject>
@optional
- (void)willMoveToPage:(UIViewController *)controller index:(NSInteger)index;
- (void)didMoveToPage:(UIViewController *)controller index:(NSInteger)index;
@end

@interface MenuItemView : UIView

@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) UIView *menuItemSeparator;
@property (nonatomic) NSString *FontValue;

- (void)setUpMenuItemView:(CGFloat)menuItemWidth menuScrollViewHeight:(CGFloat)menuScrollViewHeight indicatorHeight:(CGFloat)indicatorHeight separatorPercentageHeight:(CGFloat)separatorPercentageHeight separatorWidth:(CGFloat)separatorWidth separatorRoundEdges:(BOOL)separatorRoundEdges menuItemSeparatorColor:(UIColor *)menuItemSeparatorColor;

- (void)setTitleText:(NSString *)text;

@end

@interface PaytoScroller : UIViewController <UIScrollViewDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIScrollView *menuScrollView;
@property (nonatomic, strong) UIScrollView *controllerScrollView;
@property (nonatomic) UIView *selectionIndicatorView;

@property (nonatomic, strong) NSArray *controllerArray;
@property (nonatomic, readonly) NSArray *menuItems;
@property (nonatomic, readonly) NSArray *menuItemWidths;

@property (nonatomic) NSInteger currentPageIndex;
@property (nonatomic) NSInteger lastPageIndex;

@property (nonatomic) CGFloat menuHeight;
@property (nonatomic) CGFloat menuMargin;
@property (nonatomic) CGFloat menuItemWidth;
@property (nonatomic) CGFloat selectionIndicatorHeight;
@property (nonatomic) NSInteger scrollAnimationDurationOnMenuItemTap;

@property (nonatomic) UIColor *selectionIndicatorColor;
@property (nonatomic) UIColor *selectedMenuItemLabelColor;
@property (nonatomic) UIColor *unselectedMenuItemLabelColor;
@property (nonatomic) UIColor *scrollMenuBackgroundColor;
@property (nonatomic) UIColor *viewBackgroundColor;
@property (nonatomic) UIColor *bottomMenuHairlineColor;
@property (nonatomic) UIColor *menuItemSeparatorColor;

@property (nonatomic) UIFont *menuItemFont;
@property (nonatomic) CGFloat menuItemSeparatorPercentageHeight;
@property (nonatomic) CGFloat menuItemSeparatorWidth;
@property (nonatomic) BOOL menuItemSeparatorRoundEdges;

@property (nonatomic) BOOL addBottomMenuHairline;
@property (nonatomic) BOOL menuItemWidthBasedOnTitleTextWidth;
@property (nonatomic) BOOL useMenuLikeSegmentedControl;
@property (nonatomic) BOOL centerMenuItems;
@property (nonatomic) BOOL enableHorizontalBounce;
@property (nonatomic) BOOL hideTopMenuBar;
@property (nonatomic) NSMutableArray *mutableMenuItems;

@property (nonatomic) BOOL onlyButton;

@property (nonatomic, weak) id <PaytoScrollerDelegate> delegate;

- (void)addPageAtIndex:(NSInteger)index;
- (void)moveToPage:(NSInteger)index;
- (void)scrollingEnable:(BOOL)isEnable;
- (void)changeForButtonTap: (BOOL)isTapOnly;

- (instancetype)initWithViewControllers:(NSArray *)viewControllers frame:(CGRect)frame options:(NSDictionary *)options;

extern NSString * const PaytoScrollerOptionSelectionIndicatorHeight;
extern NSString * const PaytoScrollerOptionMenuItemSeparatorWidth;
extern NSString * const PaytoScrollerOptionScrollMenuBackgroundColor;
extern NSString * const PaytoScrollerOptionViewBackgroundColor;
extern NSString * const PaytoScrollerOptionBottomMenuHairlineColor;
extern NSString * const PaytoScrollerOptionSelectionIndicatorColor;
extern NSString * const PaytoScrollerOptionMenuItemSeparatorColor;
extern NSString * const PaytoScrollerOptionMenuMargin;
extern NSString * const PaytoScrollerOptionMenuHeight;
extern NSString * const PaytoScrollerOptionSelectedMenuItemLabelColor;
extern NSString * const PaytoScrollerOptionUnselectedMenuItemLabelColor;
extern NSString * const PaytoScrollerOptionUseMenuLikeSegmentedControl;
extern NSString * const PaytoScrollerOptionMenuItemSeparatorRoundEdges;
extern NSString * const PaytoScrollerOptionMenuItemFont;
extern NSString * const PaytoScrollerOptionMenuItemSeparatorPercentageHeight;
extern NSString * const PaytoScrollerOptionMenuItemWidth;
extern NSString * const PaytoScrollerOptionEnableHorizontalBounce;
extern NSString * const PaytoScrollerOptionAddBottomMenuHairline;
extern NSString * const PaytoScrollerOptionMenuItemWidthBasedOnTitleTextWidth;
extern NSString * const PaytoScrollerOptionScrollAnimationDurationOnMenuItemTap;
extern NSString * const PaytoScrollerOptionCenterMenuItems;
extern NSString * const PaytoScrollerOptionHideTopMenuBar;

@end
