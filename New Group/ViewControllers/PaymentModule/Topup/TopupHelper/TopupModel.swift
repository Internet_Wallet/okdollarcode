//
//  TopupModel.swift
//  OK
//
//  Created by Ashish on 5/18/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

//MARK:- Data Plan Model
struct DataPlanModel {
    var details  = ""
    var size     = ""
    var amount   = ""
    var isActive = true
    var attributedAmount = NSAttributedString.init(string: "")
    var developerAmount = NSString.init()
    var ussdCode = ""
    
    
    init(_ details: String,_ size: String, _ amount: String, _ isActive: Bool, _ ussd: String) {
        self.details  = details
        self.size     = size
        self.isActive = isActive
        self.ussdCode = ussd
        self.developerAmount =  NSString.init(string: amount)
        
        let amt = (amount as NSString).intValue
        let nsAmount = NSNumber.init(value: amt)
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NumberFormatter.Style.decimal
        if let amtnt  = currencyFormatter.string(from: nsAmount) {
            self.amount = amtnt
        } else {
            self.amount = amount
        }
        
        let mmk = [NSAttributedString.Key.font: UIFont(name: appFont, size: 12.0) ?? UIFont.systemFont(ofSize: 12)]
        let amtMMK = [NSAttributedString.Key.font: UIFont(name: appFont, size: 15.0) ?? UIFont.systemFont(ofSize: 15)]
        let mmkString = NSMutableAttributedString(string: " MMK", attributes: mmk)
        let amountStr    = NSMutableAttributedString.init(string: self.amount, attributes: amtMMK)
        
        let stringAttributed = NSMutableAttributedString()
        stringAttributed.append(amountStr)
        stringAttributed.append(mmkString)
        
        self.attributedAmount = stringAttributed
        
    }
}

//MARK:- Recharge Model
struct RechargePlanModel {
    
    var amount = ""
    var formattedAmount = ""
    var developerAmount = NSString.init()
    
    // extra added for promo message and lTopupPath
    var isPopUp     = false
    var promoMsg    = ""
    var eloadMsg    = ""
    var mainBalMsg  = ""
    var offerValue  = ""
    var selectedIndex : String = ""
    
    init(_ amount : String, promoMsg : String = "", customData: String = "") {
        
        self.developerAmount = amount as NSString
        
        let amt = (amount as NSString).intValue
        let nsAmount = NSNumber.init(value: amt)
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NumberFormatter.Style.decimal
        if let amtnt  = currencyFormatter.string(from: nsAmount) {
            self.formattedAmount = amtnt
            self.amount = amtnt + " MMK"
        } else {
            self.formattedAmount = amount
            self.amount = amount + " MMK"
        }
        
        if promoMsg.count > 0 {
            self.promoMsg = promoMsg
        }
        
        var newMsg =  self.jsonObject(string: customData).replacingOccurrences(of: "\r\n", with: "")
        println_debug(newMsg)
        newMsg = newMsg.replacingOccurrences(of: "\"", with: "")
        newMsg = newMsg.replacingOccurrences(of: "{", with: "")
        newMsg = newMsg.replacingOccurrences(of: "}", with: "")
        if newMsg.count > 0 {
            let msgArr = newMsg.components(separatedBy: ",")
            let itemExists = msgArr.contains(where: {
                $0.range(of: "popup:true", options: .caseInsensitive) != nil
            })
            if itemExists {
                self.isPopUp = true
                for msg in msgArr {
                    if msg.contains(find: "eload_msg") {
                        let dicMsg = msg.components(separatedBy: ":")
                        if dicMsg.count > 0 {
                            println_debug("Eload Msg")
                            println_debug(dicMsg[1])
                            self.eloadMsg = dicMsg[1]
                        }
                    } else if msg.contains(find: "main_balance") {
                        let dicMsg = msg.components(separatedBy: ":")
                        if dicMsg.count > 0 {
                            println_debug("Main Balance")
                            println_debug(dicMsg[1])
                            self.mainBalMsg = dicMsg[1]
                        }
                    } else if msg.contains(find: "Offer_Msg") {
                        let dicMsg = msg.components(separatedBy: ":")
                        if dicMsg.count > 0 {
                            println_debug("Main Balance")
                            println_debug(dicMsg[1])
                            self.offerValue = dicMsg[1]
                        }
                    }
                }
            }
        }
    }
    
    func jsonObject<object>(string: object) -> object {
        return string
    }
    
    func JSONStringFromAnyObject(value: AnyObject, prettyPrinted: Bool = true) -> String {
        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : nil
        if JSONSerialization.isValidJSONObject(value) {
            do {
                let data = try JSONSerialization.data(withJSONObject: value, options: options!)
                if let string = String(data: data, encoding: String.Encoding.utf8) {
                    return string
                }
            } catch {
                println_debug(error)
            }
        }
        return ""
    }
}

//MARK:- Special Offer Model
struct SpecialOfferModel {
    var offerDetails = ""
    var amount = ""
    var developerAmount = NSString.init()
    var ussdCode = ""
    var attributeAmount = NSAttributedString.init(string: "")
    
    init(_ offer: String, amount: String, code: String) {
        self.offerDetails = offer
        self.developerAmount = amount as NSString
        self.ussdCode = code
        let amt = (amount as NSString).intValue
        let nsAmount = NSNumber.init(value: amt)
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NumberFormatter.Style.decimal
        if let amtnt  = currencyFormatter.string(from: nsAmount) {
            self.amount = amtnt
        } else {
            self.amount = amount
        }
        let mmk = [NSAttributedString.Key.font: UIFont(name: appFont, size: 12.0) ?? UIFont.systemFont(ofSize: 12)]
        let amtMMK = [NSAttributedString.Key.font: UIFont(name: appFont, size: 15.0) ?? UIFont.systemFont(ofSize: 15)]
        let mmkString = NSMutableAttributedString(string: " MMK", attributes: mmk)
        let amountStr    = NSMutableAttributedString.init(string: self.amount, attributes: amtMMK)
        let stringAttributed = NSMutableAttributedString()
        stringAttributed.append(amountStr)
        stringAttributed.append(mmkString)
        self.attributeAmount = stringAttributed
    }
}
