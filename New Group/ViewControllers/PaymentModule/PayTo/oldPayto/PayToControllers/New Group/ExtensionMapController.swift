//
//  ExtensionMapController.swift
//  OK
//
//  Created by Ashish on 6/26/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

extension MapPayToViewController : MKMapViewDelegate {
    
    func locationAuthorisation() {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            self.alertViewPermission.isHidden = false
            break
        case .restricted:
            self.alertViewPermission.isHidden = false
            break
        case .denied:
            self.alertViewPermission.isHidden = false
            break
        case .authorizedAlways:
            GeoLocationManager.shared.startUpdateLocation()
            self.alertViewPermission.isHidden = true
            break
        case .authorizedWhenInUse:
            GeoLocationManager.shared.startUpdateLocation()
            self.alertViewPermission.isHidden = true
            break
        }
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        self.reloadMap()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var reuseId = ""
        if annotation is FBAnnotationCluster {
            reuseId = "Cluster"
            var clusterView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
            if clusterView == nil {
                clusterView = FBAnnotationClusterView(annotation: annotation, reuseIdentifier: reuseId, configuration: FBAnnotationClusterViewConfiguration.default())
            } else {
                clusterView?.annotation = annotation
            }
            return clusterView
        } else {
            reuseId = "Pin"
            var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
            if pinView == nil {
                pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
                pinView?.pinTintColor = kYellowColor
            } else {
                pinView?.annotation = annotation
            }
            return pinView
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation else { return }
        
        if let cluster = annotation as? FBAnnotationCluster {
            var zoomRect = MKMapRectNull
            for annotation in cluster.annotations {
                
                let annotationPoint = MKMapPointForCoordinate(annotation.coordinate)
                let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0)
                if MKMapRectIsNull(zoomRect) {
                    zoomRect = pointRect
                } else {
                    zoomRect = MKMapRectUnion(zoomRect, pointRect)
                }
            }
            mapView.setVisibleMapRect(zoomRect, animated: true)
        }else {
            if let eachAnnotation = annotation as? FBAnnotation {
                if let userdata = eachAnnotation.userData as? PTMapNearbyMerchant {
                    let nearby = NearByServicesModel.init(model: userdata)
                    let promotion = PromotionsModel.init(model: nearby)
                    self.showPromotionInfoView(promotion: promotion)
                }
            }
        }
    }
    
    func showPromotionInfoView(promotion: PromotionsModel) {

    }

    func reloadMap() {
        DispatchQueue.main.async {
            let mapBoundsWidth = Double(self.map.bounds.size.width)
            let mapRectWidth = self.map.visibleMapRect.size.width
            let scale = mapBoundsWidth / mapRectWidth
            let annotationArray = self.clusterManager.clusteredAnnotations(withinMapRect: self.map.visibleMapRect, zoomScale:scale)
            self.clusterManager.display(annotations: annotationArray, onMapView:self.map)
        }
    }

}
