//
//  PTMapListTableViewCell.swift
//  OK
//
//  Created by Ashish on 6/25/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol PTMapListTableViewCellDelegate: class {
    func callToMerchant(index: IndexPath)
}

class PTMapListTableViewCell: UITableViewCell {

    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var textFirst: UILabel!
    @IBOutlet weak var textSecond: UILabel!
    @IBOutlet weak var textThird: UILabel!
    @IBOutlet weak var callBtn: UIButton!
    
    var index : IndexPath!
    
    weak var delegate : PTMapListTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func callButtonAction(_ sender: UIButton) {
        if index != nil {
            delegate?.callToMerchant(index: index)
        }
    }
    
    func wrapModel(model: PTMapNearbyMerchant) {
        if let name = model.agentName {
            self.name.text = name
        }
        
        if let distance =  model.shopInMeter {
            let roundValue = (distance as NSString).floatValue
            if roundValue > 1000 {
                let stringDistance = String.init(format: "%f Km", roundValue/1000)
                self.distance.text = stringDistance
            } else {
                let stringDistance = String.init(format: "%f m", roundValue)
                self.distance.text = stringDistance
            }
        }
        
        if let address = model.address, let town = address.townShip, let name = town.name {
            self.textFirst.text = name
        }
        if model.shopOpenTime == "<null>" {
            self.textSecond.text =  "NA"
        } else {
            self.textSecond.text = model.shopOpenTime ?? "NA"
        }
        if let promo = model.promotion, promo.count > 0, let promoObj = promo.first {
            if promoObj.header.safelyWrappingString() == "<null>" {
                self.textThird.text = "NA"
            } else {
                self.textThird.text = promoObj.header ?? "NA"
            }
        }
        
    }
    

}




