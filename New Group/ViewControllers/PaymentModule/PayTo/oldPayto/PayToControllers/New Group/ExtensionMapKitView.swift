//
//  ExtensionMapKitView.swift
//  OK
//
//  Created by Ashish on 6/26/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import MapKit

extension MKMapView {

    open var currentZoomLevel: Float {
        let maxZoom: Double = 24
        let zoomScale = visibleMapRect.size.width / Double(frame.size.width)
        let zoomExponent = log2(zoomScale)
        return Float(maxZoom - ceil(zoomExponent))
    }

    
}
