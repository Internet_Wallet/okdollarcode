//
//  MapWebserviceExtensionViewController.swift
//  OK
//
//  Created by Ashish on 6/25/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

extension MapPayToViewController {
    
    func callNearbyMerchant() {
        let long = GeoLocationManager.shared.currentLongitude
        let lat  = GeoLocationManager.shared.currentLatitude
        let distance = String.init(format: "%d", self.slider.value)
        let strUrl = String.init(format: Url.nearbyMerchantWithPromotions, getMsid(),distance,long ?? 0.0, lat ?? 0.0)
        guard let url = URL.init(string: strUrl) else { return }
        
        TopupWeb.genericClassWithoutLoader(url: url, param: Dictionary<String,Any>() as AnyObject, httpMethod: "GET") { [weak self](response, success) in
            if success {
                if let jsonData = response as? Dictionary<String,Any> {
                    if let code = jsonData.safeValueForKey("Code") as? Int, code == 200 {
                        if let stringData = jsonData.safeValueForKey("Data") as? String {
                            if let parse = stringData.parseJSONString {
                                if let arrDict = parse as? [Dictionary<String,Any>] {
                                    self?.clusterManager.removeAll()
                                    self?.merchantModel.removeAll()
                                    for dict in arrDict {
                                        if let jsonData = dict.jsonString().data(using: .utf8) {
                                            if let mapper = try? JSONDecoder().decode(PTMapNearbyMerchant.self, from: jsonData) {
                                                self?.merchantModel.append(mapper)
                                            }
                                        }
                                    }
                                    DispatchQueue.main.async {
                                        if let weak = self {
                                            weak.listTableView.reloadData()
                                            weak.addClustersToMap(modelArray: weak.merchantModel)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
    
}

extension String {
    var parseJSONString: AnyObject? {
        let data = self.data(using: .utf8)
        if let jsonData = data {
            if let json = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) {
                return json as AnyObject
            }
         return nil
        } else {
            return nil
        }
    }
}

