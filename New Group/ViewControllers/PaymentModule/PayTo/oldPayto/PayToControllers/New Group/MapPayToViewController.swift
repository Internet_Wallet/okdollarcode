//
//  MapPayToViewController.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/27/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import MapKit

enum SelectLocationEnum {
    case currentLocation, division, nearbyMerchants, allMerchants, merchantWithPromotions
}

protocol SelectLocationDelegate : class {
    func didSelectLocation(withOptions option: SelectLocationEnum)
}


class MapPayToViewController: PayToBaseViewController, PTIndicatorInfoProvider, CategoriesSelectionDelegate, SelectLocationDelegate, PTMapListTableViewCellDelegate, TownshipSelectionDelegate{
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PTPagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo.init(title: "Map".localized)
    }
    @IBOutlet weak var alertViewPermission: UIView!
    @IBOutlet weak var mapViewContainer: UIView!
    @IBOutlet weak var listDisplayButton: ButtonWithBages!
    
    @IBOutlet  weak var lblCurrentLoc      : UILabel!
        {
        didSet
        {
            lblCurrentLoc.text = lblCurrentLoc.text?.localized
        }
    }
    @IBOutlet  weak var lblList      : UILabel!
        {
        didSet
        {
            lblList.text = lblList.text?.localized
        }
    }
    @IBOutlet  weak var lblCategories      : UILabel!
        {
        didSet
        {
            lblCategories.text = lblCategories.text?.localized
        }
    }
    @IBOutlet  weak var lblSatView      : UILabel!
        {
        didSet
        {
            lblSatView.text = lblSatView.text?.localized
        }
    }
    
    var navigation : UINavigationController?
    
    var pulsingEffect : PulseManager?
    
    @IBOutlet weak var listViewContainer: UIView!
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var heightListViewContainer: NSLayoutConstraint!
    @IBOutlet weak var heightUpDownImage: UIImageView!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var locationOutlet: UILabel!
    
    // cluster manager
    let clusterManager = FBClusteringManager()
    
    var merchantModel = [PTMapNearbyMerchant]()
    
    var selectedMerchant : PTMapNearbyMerchant?
    
    //////////////////
    
    // list view data source
    var listMerchantModel : [String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pulsingEffect = PulseManager.updateView()
        
        pulsingEffect?.startPulsing(distance: slider.value)
        
        self.map.delegate = self
        
//        self.listDisplayButton.badgeCount = 99
        
        slider.isContinuous = false
        listMerchantModel = ["",""]
        self.listTableView.tableFooterView = UIView.init()
        self.listViewContainer.isHidden = true
        self.heightUpDownImage.isHighlighted = false
//        self.mapViewContainer.addSubview(pulsingEffect!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.locationAuthorisation()
        if self.alertViewPermission.isHidden {
            self.initialSetupForMap()
        }
        
        self.currentLocationUpdate()
    }
    
    @IBAction func goToSettings(_ sender: UIButton) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(NSURL(string: UIApplicationOpenSettingsURLString)! as URL, options: [:]
                , completionHandler: nil)
        } else {
            UIApplication.shared.openURL(NSURL(string: UIApplicationOpenSettingsURLString)! as URL)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func initialSetupForMap() {
        let locationLat  = CLLocationDegrees.init(GeoLocationManager.shared.currentLatitude)
        let locationLong = CLLocationDegrees.init(GeoLocationManager.shared.currentLongitude)

        let location = CLLocationCoordinate2D.init(latitude: locationLat ?? 0.0, longitude: locationLong ?? 0.0)
        let distance = self.slider.value / 3.2808
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location, CLLocationDistance(distance), CLLocationDistance(distance))
        
        map.setRegion(coordinateRegion, animated: true)
        
        callNearbyMerchant()
    }
    
    func addClustersToMap(modelArray: [PTMapNearbyMerchant]) {
        var array:[FBAnnotation] = []
        
        for model in modelArray {
            let pinOne = FBAnnotation()
            if let geoLocation = model.geoLocationShop, let lat =  geoLocation.latitude, let long = geoLocation.longitude {
                let lattitude : Float = (lat as NSString).floatValue
                let longitude : Float = (long as NSString).floatValue
                pinOne.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(lattitude) , longitude: CLLocationDegrees(longitude))
            }
            array.append(pinOne)
        }
        clusterManager.add(annotations: array)
    }
    
    // MARK: Categories Helpers
    private func showCategoriesView() {
        let storyBoardName = UIStoryboard(name: "Promotion", bundle: nil)
        let categoriesView = storyBoardName.instantiateViewController(withIdentifier: "CategoriesListView_ID") as! CategoriesListViewController
        categoriesView.delegate = self
        categoriesView.modalPresentationStyle = .overCurrentContext
        self.present(categoriesView, animated: true, completion: nil)
    }

    func didSelectLocationByCategory(category: SubCategoryDetail?) {
        PTLoader.shared.show()
        if let cat = category {
            let array = self.merchantModel.filter({$0.businessCategory?.code == cat.subCategoryCode})
            self.addClustersToMap(modelArray: array)
        }
        PTLoader.shared.hide()
    }
    
    func didSelectTownship(location: LocationDetail, township: TownShipDetail) {
        PTLoader.shared.show()
        self.locationOutlet.text = township.cityNameEN
        let array = self.merchantModel.filter({$0.shopTownShip == township.cityNameEN})
        self.addClustersToMap(modelArray: array)
        PTLoader.shared.hide()
    }
    
    //MARK:- Slider Actions


    //MARK:- Button Actions
    @IBAction func sliderAction(_ sender: UISlider) {
//        let valueChnaged = sender.value
        self.initialSetupForMap()
        
    }
    
    @IBAction func listAction(_ sender: UIButton) {
        if listViewContainer.isHidden {
            listViewContainer.isHidden = false
        } else {
            listViewContainer.isHidden = true
        }
    }
    
    
    @IBAction func currentLocationAction(_ sender: UIButton) {
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: String(describing: CurrentLocationViewController.self)) as? CurrentLocationViewController else { return }
        vc.delegate = self
        self.navigation?.pushViewController(vc, animated: true)
    }
    
    @IBAction func openCategoriesAction(_ sender: UIButton) {
        self.showCategoriesView()
    }
    
    @IBAction func mapViewChangeAction(_ sender: UIButton) {
            if map.mapType != .satellite {
                map.mapType = .satellite
            } else {
                map.mapType = .standard
            }
    }
    
    //MARK:- List Container View Button Action
    @IBAction func listViewUpDownAction(_ sender: UIButton) {
        if self.heightUpDownImage.isHighlighted {
            self.heightUpDownImage.isHighlighted = false
            self.heightListViewContainer.constant = 150.00
        } else {
            self.heightUpDownImage.isHighlighted = true
            let height = self.mapViewContainer.bounds.height - 65
            self.heightListViewContainer.constant = height
        }
        
        UIView.animate(withDuration: 0.1) {
            self.view.layoutIfNeeded()
        }
        
    }
    
    //MARK:- Select Location Delegate
    func didSelectLocation(withOptions option: SelectLocationEnum) {
        switch option {
        case .currentLocation:
            self.currentLocationUpdate()
            break
        case .division:
//            let storyboard: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
//            let divisionStateView = storyboard.instantiateViewController(withIdentifier: "SelectDivisionStateView_ID") as! SelectDivisionStateViewController
//            divisionStateView.delegate = self
//            divisionStateView.nav = self.navigationController
//            self.navigationController?.present(divisionStateView, animated: true, completion: nil)
            break
        case .nearbyMerchants:
            self.callNearbyMerchant()
            break
        case .allMerchants:
            self.callNearbyMerchant()
            break
        case .merchantWithPromotions:
            self.callNearbyMerchant()
            break
        }
    }
    
    func merchantWithPromotions() {
        
    }
    
    func currentLocationUpdate() {
        PTLoader.shared.show()
        let longitude :CLLocationDegrees = (GeoLocationManager.shared.currentLongitude != nil) ? (GeoLocationManager.shared.currentLongitude as NSString).doubleValue : 0.0
        let latitude :CLLocationDegrees  = (GeoLocationManager.shared.currentLatitude != nil) ? (GeoLocationManager.shared.currentLatitude as NSString).doubleValue : 0.0
        
        let location = CLLocation(latitude: latitude, longitude: longitude) //changed!!!
        
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            PTLoader.shared.hide()
            if error != nil {
                println_debug("Reverse geocoder failed with error" )
                return
            }
            
            if let places = placemarks {
                if places.count > 0 {
                    if let adressData = places.first {
                        if let dic = adressData.addressDictionary {
                            var str = ""
                            str = str + (dic.safeValueForKey("Name") as? String ?? "")
                            str = str + (dic.safeValueForKey("SubLocality") as? String ?? "")
                            str = str + (dic.safeValueForKey("City") as? String ?? "")
                            str = str + (dic.safeValueForKey("Country") as? String ?? "")
                            self.locationOutlet.text = str
                        }
                    }
                }
                else {
                    self.locationOutlet.text = ""
                    println_debug("Problem with the data received from geocoder")
                }
            }
        })
    }

    
}

//MARK:- List TableView Datasource & Delegate
extension MapPayToViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return merchantModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: PTMapListTableViewCell.self), for: indexPath) as? PTMapListTableViewCell
            if let model = self.merchantModel[safe: indexPath.row] {
                cell?.wrapModel(model: model)
            }
        
        cell?.delegate = self
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let model = merchantModel[safe: indexPath.row] {
            let storyboard: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
            let protionDetailsView = storyboard.instantiateViewController(withIdentifier: "PromotionDetailView_ID") as! PromotionDetailViewController

            let promotion = NearByServicesModel.init(model: model)
            protionDetailsView.selectedNearByServices = promotion
            protionDetailsView.strNearByPromotion = "NearBy"
            navigation?.pushViewController(protionDetailsView, animated: true)
        }
    }
    
    // call to number
    func callToMerchant(index: IndexPath) {
        if let model = self.merchantModel[safe:index.row] {
            var number = model.phoneNumber.safelyWrappingString()
            if number.hasPrefix("00") {
                number = "+" + number
            }
            let phone = "tel://\(number)"
            if let url = URL.init(string: phone) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }

        }
    }
    
}







