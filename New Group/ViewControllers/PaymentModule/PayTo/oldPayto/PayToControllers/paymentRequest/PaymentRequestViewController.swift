//
//  PaymentRequestViewController.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/27/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

 enum PTFilterType {
    case date, sort, filter
}

//Mark: Controller
class PaymentRequestViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PaymentRequestCellPayToDelegate, PTIndicatorInfoProvider, FilterPaymentRequestDelegate, CalendarPopUpDelegate {
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PTPagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo.init(title: indicator!)
    }
    
    @IBOutlet var dateFilter: UIButton!
    @IBOutlet var sortFilter: UIButton!
    @IBOutlet var filter: UIButton!
    
   fileprivate lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(PaymentRequestViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = MyNumberTopup.OperatorColorCode.okDefault
        return refreshControl
    }()

    
    @IBOutlet weak var filterViewUpdates: CardDesignView!
    
    var indicator : String?
    
    var filters : PTFilterType?
    
    let manager = PTPaymentRequestManager.shared
    
    var navigation : UINavigationController?

    @IBOutlet var tableView: UITableView!
    
    var dataModel = [PaymentRequestCellPayTo]()
    
    var copyModel  = [PaymentRequestCellPayTo]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dateFilter.addBorder(side: .Right, color: .lightGray, width: 1.00)
        self.sortFilter.addBorder(side: .Right, color: .lightGray, width: 1.00)
        if indicator == "Recieved Payment Request" {
            manager.requestManager(type: .recieve, withEntity: self)
        } else {
            manager.requestManager(type: .sent, withEntity: self)
        }
    }

    func refreshView() {
        DispatchQueue.main.async {
            self.dataModel.removeAll()
            
            var payObject = [AnyObject]()
            
            if self.indicator == "Recieved Payment Request" {
                 payObject = self.manager.getResponseTypeArray(type: .recieve)
            } else {
                 payObject = self.manager.getResponseTypeArray(type: .sent)
            }

            switch (payObject.count > 0) {
            case true : self.tableView.isHidden = false; self.tableView.addSubview(self.refreshControl)
            case false: self.tableView.isHidden = true; self.tableView.addSubview(self.refreshControl)
            }
            
            if let objectArray  = payObject as? [PTSentRequestObject] {
                for (index, element) in objectArray.enumerated() {
                    _ = IndexPath.init(row: index, section: 0)
                    let cell = self.tableView.dequeueReusableCell(withIdentifier: "PaymentRequestCellPayTo") as? PaymentRequestCellPayTo
                    cell?.updateArraySent(model: element)
                    cell?.delegate = self
                    self.self.dataModel.append(cell!)
                }
            } else if let objectArray = payObject as? [PTRecieveRequestObject] {
                for (index, element) in objectArray.enumerated() {
                    _ = IndexPath.init(row: index, section: 0)
                    let cell = self.tableView.dequeueReusableCell(withIdentifier: "PaymentRequestCellPayTo") as? PaymentRequestCellPayTo
                    cell?.updateArrayRecieve(model: element)
                    cell?.delegate = self
                    self.dataModel.append(cell!)
                }
            }
            
            if self.dataModel.count > 0 {
                self.filterViewUpdates.isHidden = false
            } else {
                self.filterViewUpdates.isHidden = true
            }
            
            self.tableView.reloadData()
            
            self.copyModel = self.dataModel
            
        }
    }
    
    @IBAction func filterAction(_ sender: UIButton) {
        if sender == dateFilter {
            filters = PTFilterType.date
        } else if sender == sortFilter {
            filters = PTFilterType.sort
        } else if sender == filter {
            filters = PTFilterType.filter
        } else {
            filters = PTFilterType.filter
        }
        
        
        if sender == dateFilter {
            let xibView = Bundle.main.loadNibNamed("CalendarPopUp", owner: nil, options: nil)?[0] as! CalendarPopUp
            xibView.calendarDelegate = self
            xibView.currentSelection = .startDateBtn
            PopupContainer.generatePopupWithView(xibView).show()
            return
        }

        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as? PayToFilterViewController
        vc?.delegate = self
        vc?.modalPresentationStyle = .overCurrentContext
        vc?.filterType = filters
            if let nav = self.navigationController {
                nav.present(vc!, animated: true, completion: nil)
            }
            
            if let nav = self.navigation {
                nav.present(vc!, animated: true, completion: nil)
            }

    }
    
    func dismiss() {
        self.dismiss(animated: true, completion: nil)
    }

    //Mark: TableView Delegate & Datasources
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var payObject = [AnyObject]()
        
        if self.indicator == "Recieved Payment Request" {
            payObject = self.manager.getResponseTypeArray(type: .recieve)
        } else {
            payObject = self.manager.getResponseTypeArray(type: .sent)
        }

        if let objectArray  = payObject as? [PTSentRequestObject] {
            
            let obj = objectArray[indexPath.row]
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PTGalleryViewController") as? PTGalleryViewController
            
            vc?.arrayImages = obj.attachPath
            if obj.attachPath.count > 0 {
                if let nav = self.navigationController {
                    nav.pushViewController(vc!, animated: true)
                }
                
                if let nav = self.navigation {
                    nav.pushViewController(vc!, animated: true)
                }

            } else {
                showToast(message: "No Files Attached")
            }
        } else if let objectArray = payObject as? [PTRecieveRequestObject] {
            let obj = objectArray[indexPath.row]
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PTGalleryViewController") as? PTGalleryViewController
            vc?.arrayImages = obj.attachPath
            if obj.attachPath.count > 0 {
                if let nav = self.navigationController {
                     nav.pushViewController(vc!, animated: true)
                }
                
                if let nav = self.navigation {
                    nav.pushViewController(vc!, animated: true)
                }
            } else {
                showToast(message: "No Files Attached")
            }
        }
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        if indicator == "Recieved Payment Request" {
            manager.requestManager(type: .recieve, withEntity: self)
        } else {
            manager.requestManager(type: .sent, withEntity: self)
        }
        refreshControl.endRefreshing()
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return dataModel[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 415
    }
    
    func didSelectPay(index: IndexPath) {
        
        
        
    }
    
    func didSelectCancel(index: IndexPath) {
        
        
        
    }
    
    //MARK:- Filter Elements
    func didSelectFilterType(type: PaymentRequestFilterType) {
        self.dataModel.removeAll()
        
        var payObject = [AnyObject]()
        
        if self.indicator == "Recieved Payment Request" {
            payObject = self.manager.getResponseTypeArray(type: .recieve)
        } else {
            payObject = self.manager.getResponseTypeArray(type: .sent)
        }

        if var objectArray  = payObject as? [PTSentRequestObject] {
            
            switch type {
            case .def:
                break
            case .amount_hl:
                objectArray = objectArray.sorted(by: { $0.amount > $1.amount} )
                break
            case .amount_lh:
                objectArray = objectArray.sorted(by: { $0.amount < $1.amount} )
                break
            case .name_az:
                objectArray = objectArray.sorted(by: { $0.agentName < $1.agentName} )
                break
            case .name_za:
                objectArray = objectArray.sorted(by: { $0.agentName > $1.agentName} )
                break
            case .normal:
                objectArray = objectArray.filter({$0.categoryName.lowercased() == "normal"})
            case .personal:
                objectArray = objectArray.filter({$0.categoryName.lowercased() == "personal"})
            }

            for (index, element) in objectArray.enumerated() {
                _ = IndexPath.init(row: index, section: 0)
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "PaymentRequestCellPayTo") as? PaymentRequestCellPayTo
                cell?.updateArraySent(model: element)
                self.self.dataModel.append(cell!)
            }
        } else if var objectArray = payObject as? [PTRecieveRequestObject] {
            
            switch type {
            case .def: break
            case .amount_hl:
                objectArray = objectArray.sorted(by: { $0.amount > $1.amount} )
            case .amount_lh:
                objectArray = objectArray.sorted(by: { $0.amount < $1.amount} )
            case .name_az:
                objectArray = objectArray.sorted(by: { $0.agentName < $1.agentName} )
            case .name_za:
                objectArray = objectArray.sorted(by: { $0.agentName > $1.agentName} )
            case .normal:
                objectArray = objectArray.filter({$0.categoryName.lowercased() == "normal"})
            case .personal:
                objectArray = objectArray.filter({$0.categoryName.lowercased() == "personal"})
            }

            for (index, element) in objectArray.enumerated() {
                _ = IndexPath.init(row: index, section: 0)
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "PaymentRequestCellPayTo") as? PaymentRequestCellPayTo
                cell?.updateArrayRecieve(model: element)
                self.dataModel.append(cell!)
            }
        }

        if self.dataModel.count > 0 {
            self.filterViewUpdates.isHidden = false
        } else {
            self.filterViewUpdates.isHidden = true
        }
        
        self.tableView.reloadData()

    }
    
    //MARK:- Cell Transactions Delegate Cancel & Pay
    func paymentRequestNeedToPay(recieveReq: PTRecieveRequestObject?, sentReq: PTSentRequestObject?) {
        if let recieve = recieveReq {
            println_debug(recieve)

            
        } else if let sent = sentReq {
            println_debug(sent)
            

        }
    }
    
    func didCancelRequestWithID(_ id: String) {
        alertViewObj.wrapAlert(title: "", body: "Do you want to Cancel Payment Request?" , img: #imageLiteral(resourceName: "dashboard_requestmoney"))
        alertViewObj.addAction(title: "Cancel", style: .cancel) {
        }
        alertViewObj.addAction(title: "OK", style: .target) {
            let urlStr = String.init(format: PTHelper.cancelRecieveReq, id)
            let param = Dictionary<String,Any>() as AnyObject
            if let  urlStr = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                if  let url = URL.init(string: urlStr) {
                    TopupWeb.genericClass(url: url, param: param, httpMethod: "GET", handle: { (response, success) in
                        println_debug(response)
                    })
                }
            }
        }
        
        alertViewObj.showAlert(controller: self)

    }
    
    //MARK:- Date Filter Delegate
    func dateChaged(startDate: Date, endDate: Date, duration: Int) {
        
    }

}


//MARK:- Cell
class PaymentRequestCellPayTo : UITableViewCell {
    
    @IBOutlet var merchantKey     : UILabel!
    @IBOutlet var categoriesKey   : UILabel!
    @IBOutlet var numberKey       : UILabel!
    @IBOutlet var attachedFileKey : UILabel!
    @IBOutlet var amountKey       : UILabel!
    @IBOutlet var dateKey         : UILabel!
    
    @IBOutlet var merchantValue    : UILabel!
    @IBOutlet var categoriesValue  : UILabel!
    @IBOutlet var numberValue      : UILabel!
    @IBOutlet var attachedValue    : UILabel!
    @IBOutlet var amountValue      : UILabel!
    @IBOutlet var dateValue        : UILabel!
    
    @IBOutlet var imageView_profile: RoundableUIImageView!

    weak var delegate : PaymentRequestCellPayToDelegate?
    
    var recieveModel : PTRecieveRequestObject?
    var sentModel : PTSentRequestObject?

    override func awakeFromNib() {
        
    }
    
    @IBAction func cancelAction(_ sender: UIButton) {
        if let model = self.recieveModel {
            
            if let delegate = self.delegate {
                delegate.didCancelRequestWithID(model.reqId)
            }
            
        } else if let model = self.sentModel {
            
            if let delegate = self.delegate {
                delegate.didCancelRequestWithID(model.reqId)
            }
        }
    }
    
    @IBAction func payAction(_ sender: UIButton) {
        if let model = self.recieveModel {
            
            if let delegate = self.delegate {
                delegate.paymentRequestNeedToPay(recieveReq: model, sentReq: nil)
            }
            
        } else if let model = self.sentModel {
            
            if let delegate = self.delegate {
                delegate.paymentRequestNeedToPay(recieveReq: nil, sentReq: model)
            }
        }
    }
    
    
    
    func updateArraySent(model : PTSentRequestObject) {
        
        self.sentModel = model
        
        self.merchantValue.text   = model.agentName
        self.categoriesValue.text = model.categoryName
        self.numberValue.text     = model.source
        self.attachedValue.text   = model.attachPath.count > 0 ? "YES" : "NO"
        self.amountValue.text     = (model.amount as NSNumber).stringValue
        self.dateValue.text       = model.date
        
        
        if let stringUrl = model.proImg.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            if let url = URL.init(string: stringUrl) {
                self.imageView_profile.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ok"), options: .scaleDownLargeImages, completed: { (image, error, type, url) in
                    DispatchQueue.main.async {
                        self.imageView_profile.image = image
                        self.imageView_profile.contentMode = .scaleAspectFill
                        self.imageView_profile.makeCircle()
                    }
                })
            }
        }
    }

    func updateArrayRecieve(model : PTRecieveRequestObject) {
        
        self.recieveModel = model
        
        self.merchantValue.text   = model.agentName
        self.categoriesValue.text = model.categoryName
        self.numberValue.text     = model.source
        self.attachedValue.text   = model.attachPath.count > 0 ? "YES" : "NO"
        self.amountValue.text     = (model.amount as NSNumber).stringValue
        self.dateValue.text       = model.date
        
        if let stringUrl = model.proImg.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            if let url = URL.init(string: stringUrl) {
                self.imageView_profile.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ok"), options: .scaleDownLargeImages, completed: { (image, error, type, url) in
                    DispatchQueue.main.async {
                        self.imageView_profile.image = image
                        self.imageView_profile.contentMode = .scaleAspectFill
                        self.imageView_profile.makeCircle()
                    }
                })
            }
        }
    }

}

//Mark: Delegate
protocol PaymentRequestCellPayToDelegate : class {
    func didCancelRequestWithID(_ id: String)
    func paymentRequestNeedToPay(recieveReq: PTRecieveRequestObject?, sentReq: PTSentRequestObject?)
}
