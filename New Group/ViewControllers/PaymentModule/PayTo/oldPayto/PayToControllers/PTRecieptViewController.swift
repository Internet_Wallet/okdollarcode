//
//  PTRecieptViewController.swift
//  OK
//
//  Created by Ashish on 12/14/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

protocol DissmissDelegate : class {
    func paytoDismissEvent()
}

fileprivate enum TabBarItem : Int {
    case favorite = 900
    case contact
    case home
    case more
}

var CategoryStr:String = ""

enum recieptType {
    case topupMyNumber, topupOtherNumber, payto, overseas
}


class PTRecieptViewController: PayToBaseViewController, UITabBarDelegate, RecieptCellDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var rTabBar: UITabBar!
    
    @IBOutlet weak var fav: UITabBarItem!
    @IBOutlet weak var addContact: UITabBarItem!
    @IBOutlet weak var home: UITabBarItem!
    @IBOutlet weak var more: UITabBarItem!
    
    var typeResponse = "PAYTO"
    
   weak  var delegate : DissmissDelegate?
    
    var cellA : [RecieptCell] = [RecieptCell]() {
        didSet {
            for (_,element) in cellA.enumerated() {
                let snapImage = element.snapshotOfCustomeView
                print("category string-----\(CategoryStr)")
                if CategoryStr == "TICKET" || CategoryStr == "TOLL"  {
                    saveImageIntoDocument(finalImage: snapImage)
                }
            }
        }
    }
    
    var transactionDetails = [Dictionary<String,Any>]()
    
    var recieptType : recieptType = .payto
    
    var printSnapShot : Int = 0
    
    override func viewDidLoad() {
//        super.viewDidLoad()
        geoLocManager.startUpdateLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        let color = UIColor.lightGray
        let attrFont = [NSAttributedStringKey.font: UIFont(name: "Zawgyi-One", size: 11.0) ?? UIFont.systemFont(ofSize: 11.0), NSAttributedStringKey.foregroundColor: color]
        
        self.home.setTitleTextAttributes(attrFont, for: .normal)
        self.more.setTitleTextAttributes(attrFont, for: .normal)
        self.addContact.setTitleTextAttributes(attrFont, for: .normal)
        self.fav.setTitleTextAttributes(attrFont, for: .normal)

        self.fav.title = "Add Favorite".localized
        self.home.title = "Home".localized
        self.more.title = "More".localized
        self.addContact.title = "TabbarAddContact".localized

        if transactionDetails.count > 1 {
            self.title = "MultiPayment".localized
        } else {
            self.title = "Receipt".localized
        }
        
        self.snapAllRows()
        
        self.navigationController?.navigationBar.backgroundColor = MyNumberTopup.OperatorColorCode.okDefault
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
         geoLocManager.stopUpdateLocation()
        self.navigationItem.hidesBackButton = false
    }
    
    func snapAllRows() {
        
        if printSnapShot > self.cellA.count {
            return
        }
        
        for (_,element) in cellA.enumerated() {
            let snapImage = element.snapshotOfCustomeView
            println_debug("SnapShot Taken")
            UIImageWriteToSavedPhotosAlbum(snapImage, nil, nil, nil)
            printSnapShot = printSnapShot + 1
        }
    }
    
    func snapshotrow(cell : RecieptCell) {
        //get the cell
        guard let imageView = cell.snapshotView(afterScreenUpdates: true) else { return }
        let image = UIImage.init(view: imageView)
        UIImageWriteToSavedPhotosAlbum(image, self, nil, nil)
    }

    //MARK:PrabusnapshotstoreintoDocument
    
    func saveImageIntoDocument(finalImage:UIImage) {
        
        
        //////////////////
        //Sote image into document path with new Directory
        
        let filemgr = FileManager.default
        
        let dirPaths = filemgr.urls(for: .documentDirectory, in: .userDomainMask)
        
        let docsURL = dirPaths[0]
        
        let newDir = docsURL.appendingPathComponent("LocalCache").path
        
        do {
            try filemgr.createDirectory(atPath: newDir,
                                        withIntermediateDirectories: true, attributes: nil)
            
            let getImagePath = newDir.appending("/\(createUUID()).png")
            
            let theFileName = (getImagePath as NSString).lastPathComponent
            
            println_debug("sub path \(theFileName)")
            
            let checkValidation = FileManager.default
            
            if (checkValidation.fileExists(atPath: getImagePath))
            {
                //remove file as its already existed
                try!  checkValidation.removeItem(atPath: getImagePath)
            }
            else
            {
                //write file as its not available
                //  let imageData =  UIImageJPEGRepresentation(UIImage(named: "account_number.png")!, 1.0)
                let imageData =  UIImageJPEGRepresentation(finalImage, 1.0)
                try! imageData?.write(to: URL.init(fileURLWithPath: getImagePath), options: .atomicWrite)
                
                saveUserdefaults(imageName: theFileName , imageUrl: getImagePath)
                
            }
            
        } catch let error as NSError {
            println_debug("Error: \(error.localizedDescription)")
        }
    }

    func saveUserdefaults(imageName:String , imageUrl:String) {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let result = formatter.string(from: date)
        println_debug("Date string------\(result)")
        println_debug("Image name and url string------\(imageName)---\(imageUrl)")
        
        // println_debug("Date string------\(date)")
        
        let strval = TicketGallery(tearing:false , signature:false , phonegallery:false , timer:false ,identify:false ,timerdate:result , image:imageUrl)
        
        let kUserDefault = UserDefaults.standard
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: strval)
        kUserDefault.set(encodedData, forKey: imageName)
        kUserDefault.synchronize()
        
        retrieveUserdefaults(selectedImageName: imageName )

    }
    
    func retrieveUserdefaults (selectedImageName:String) {
        
        /////Retrieve stored object
        let defaultval = UserDefaults.standard
        let decoded  = defaultval.object(forKey:selectedImageName) as! Data
        
        if let object  = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? TicketGallery {
            userdefaultObject = object
        } else { return }
        
        println_debug("All values------>\(userdefaultObject.tearingstatus)--->\(userdefaultObject.signaturestatus)---\(userdefaultObject.phonegallerystatus)---\(userdefaultObject.timerstatus)---\(userdefaultObject.identifystatus)---\(userdefaultObject.timervalue)----\(userdefaultObject.imageUrl)")
        
    }
    
    func createUUID() -> String {
        let uuid = NSUUID().uuidString
        println_debug("UUDID----->\(uuid)")
        return uuid
    }
    
    //MARK:- Tabbar Delegate
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch(item) {
        case fav        : self.favorite_recipt()
        case addContact : self.addContact_recipt()
        case home       : self.home_recipt()
        case more       : self.more_recipt()
        default: break
        }
    }
    
    fileprivate  func favorite_recipt() {
        if let cell = self.getCellFromDisplay() {
            for (index,modelCell) in self.cellA.enumerated() {
                if cell == modelCell {
                    let dict = self.transactionDetails[index]
                    let number = dict.safeValueForKey("destination") as? String
                    let name   = dict.safeValueForKey("merchantname") as? String
                    let amount = dict.safeValueForKey("amount") as? String
                    
                    if  self.typeResponse.lowercased() == "FERRY".lowercased() {
                        let vc = self.addFavoriteController(withName: name ?? "", favNum: number ?? "", type: "FERRY", amount: amount ?? "")
                        if let vcs = vc {
                            self.navigationController?.present(vcs, animated: true, completion: nil)
                        }

                    } else if self.typeResponse.lowercased() == "TOLL".lowercased() {
                        let vc = self.addFavoriteController(withName: name ?? "", favNum: number ?? "", type: "TOLL", amount: amount ?? "")
                        if let vcs = vc {
                            self.navigationController?.present(vcs, animated: true, completion: nil)
                        }

                    } else if self.typeResponse.lowercased() == "BUS".lowercased() {
                        let vc = self.addFavoriteController(withName: name ?? "", favNum: number ?? "", type: "BUS", amount: amount ?? "")
                        if let vcs = vc {
                            self.navigationController?.present(vcs, animated: true, completion: nil)
                        }

                    } else {
                        let vc = self.addFavoriteController(withName: name ?? "", favNum: number ?? "", type: "PAYTO", amount: amount ?? "")
                        if let vcs = vc {
                            self.navigationController?.present(vcs, animated: true, completion: nil)
                        }
                    }
                }
                
            }
        }
    }
    
    func didSelectRateOption(cell: RecieptCell) {
        let dict = transactionDetails[safe: cell.indexPath?.row ?? 0]
        let story = UIStoryboard.init(name: "Topup", bundle: nil)
        guard let ratingScreen = story.instantiateViewController(withIdentifier: String(describing: PaymentRateScreenViewController.self)) as? PaymentRateScreenViewController else { return }
        ratingScreen.modalPresentationStyle = .overCurrentContext
        ratingScreen.delegate = cell
        ratingScreen.confirmationCellScreen = ""
        ratingScreen.destinationNumber = (dict?.safeValueForKey("destination") as? String != nil) ? dict?.safeValueForKey("destination") as? String : ""
        self.navigationController?.present(ratingScreen, animated: true, completion: nil)
    }

    
    fileprivate  func addContact_recipt() {
        if #available(iOS 9.0, *) {
            guard let cell = self.getCellFromDisplay() else { return }
            for (index,modelCell) in self.cellA.enumerated() {
                if cell == modelCell {
                    let dict = self.transactionDetails[index]
                    let store     = CNContactStore()
                    let contact   = CNMutableContact()
                    let phone = dict.safeValueForKey("destination") as? String
                    let name  = dict.safeValueForKey("merchantname") as? String
                    let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue : wrapFormattedNumber(phone ?? "") ))
                    contact.phoneNumbers = [homePhone]
                    contact.givenName = name ?? "Unknown"
                    let controller = CNContactViewController.init(forNewContact: contact)
                    controller.contactStore = store
                    controller.delegate     = self
                    controller.title        = "Add Contact".localized
                    self.navigationController?.setNavigationBarHidden(false, animated: true)
                    self.navigationController?.navigationBar.isHidden = false
                    self.navigationController?.navigationBar.backgroundColor = UIColor.init(hex: "F3C632")
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }
        }
    }
    
    fileprivate func home_recipt() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RequestMoneyHomeTapAction"), object: nil)
        UserDefaults.standard.set(true, forKey: "popPayTo")
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    fileprivate func more_recipt() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PTMoreViewController") as? PTMoreViewController
        addChildViewController(vc!)
        vc?.isFromTopup = false
        vc?.view.frame = self.view.bounds
        vc?.didMove(toParentViewController: self)
        vc?.delegate = self
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        
        vc?.view.layer.add(transition, forKey: nil)
        view.addSubview(vc!.view)
    }
}

extension PTRecieptViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, CNContactViewControllerDelegate {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecieptCell", for: indexPath) as! RecieptCell
        cell.layoutIfNeeded()
        cell.wrapData(dict: transactionDetails[indexPath.section])
        cell.wrapConditions(type: self.recieptType)
        cell.changeButton()
        cell.indexPath = indexPath
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if self.cellA.contains(cell as! RecieptCell) {
            
        } else {
            self.cellA.append(cell as! RecieptCell)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return transactionDetails.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.bounds.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    //MARK:- CNContactViewControllerDelegate
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        self.navigationController?.popViewController(animated: true)
    }
}

protocol RecieptCellDelegate : class {
    func didSelectRateOption(cell:RecieptCell)
}

class RecieptCell : UICollectionViewCell, PaymentRateDelegate {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var amount: UILabel!
    
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet weak var paymentCat: UILabel!
    @IBOutlet weak var balance: UILabel!
    @IBOutlet weak var transactId: UILabel!
    @IBOutlet weak var transactType: UILabel!
    @IBOutlet weak var rateService: UILabel!

    @IBOutlet weak var date_Val: UILabel!
    @IBOutlet weak var time_Val: UILabel!
    @IBOutlet weak var bName_Val: UILabel!
    @IBOutlet weak var PaymentCt_Val: UILabel!
    @IBOutlet weak var balance_val: UILabel!
    @IBOutlet var transact_Val: UILabel!
    @IBOutlet var transact_Value: UILabel!
    
    weak var delegate : RecieptCellDelegate?
    
    @IBOutlet weak var imageViewtype: UIImageView!
    
    var indexPath : IndexPath?
    
    @IBOutlet weak var stackViews: UIStackView!
    
    override func awakeFromNib() {
        
    }
    
    @IBAction func actionButton(_ sender: UIButton) {
        self.delegate?.didSelectRateOption(cell: self)
    }
    
    func changeButton() {
        for views in self.stackViews.subviews {
            if let imageV = views as? UIImageView {
                imageV.isHighlighted = false
            }
        }
    }
    
    func wrapData (dict: Dictionary<String,Any>) {
        
        let comments = dict.safeValueForKey("comments") as? String ?? ""
        let recieverMerchantName = comments.components(separatedBy: "##")
        
        if recieverMerchantName.count == 4 {
            let recieverName = recieverMerchantName[1]
            let recieverMerchantName = recieverMerchantName[2]
            
            self.businessName.text = (recieverMerchantName == "") ? "Subscriber" : "Merchant"
            self.bName_Val.text = (recieverMerchantName == "") ? recieverName : recieverMerchantName
        }
        

        self.nameLbl.text = self.append09Format(dict.safeValueForKey("destination") as? String ?? "")
        //
        let amount  = self.wrapAmount(key: dict.safeValueForKey("amount") as? String ?? "0")
        self.amount.text = amount
        
        if let date = dict["responsects"] as? String {
            self.date_Val.text = date.components(separatedBy: " ").first
            self.time_Val.text = date.components(separatedBy: " ").last
        }
        
        self.PaymentCt_Val.text = dict["responsetype"] as? String
        let bal = self.wrapAmount(key: dict["walletbalance"] as? String ?? "0")
        self.balance_val.text   = bal + " " + "MMK"
        self.transact_Val.text  = dict["transid"] as? String
        self.transact_Value.text  = dict["responsetype"] as? String
        
        //Prabu Category for Ticket Gallery
        CategoryStr = (dict["responsetype"] as? String) ?? ""
        
        businessName.text = businessName.text?.localized
        paymentCat.text  = paymentCat.text?.localized
        balance.text = balance.text?.localized
        transactId.text = transactId.text?.localized
        transactType.text = transactType.text?.localized
        rateService.text = rateService.text?.localized
    }
    
    func append09Format(_ dest: String) -> String {
        if dest.hasPrefix("00") {
            var number = dest.deletingPrefix("00")
             number = "+" + number
            let country = identifyCountry(withPhoneNumber: number)
            let final = number.replacingOccurrences(of: country.countryCode, with: "0")
            return final
        }
        return ""
    }
    
    fileprivate func wrapAmount(key: String) -> String {
        let number = NSDecimalNumber(string: key)
        
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        if let num = formatter.string(from: number) {
            return (num == "NaN") ? "" : num
        }
        return ""
    }

    
    func ratingShow(_ rate: Int) {
        DispatchQueue.main.async {
            for view in self.stackViews.subviews {
                if view.tag <= rate {
                    if let img = view as? UIImageView {
                        img.isHighlighted = true
                    }
                } else {
                    if let img = view as? UIImageView {
                        img.isHighlighted = false
                    }
                }
            }
        }
    }
    
    func wrapConditions(type: recieptType) {
        switch type {
        case .topupMyNumber:
            self.imageViewtype.image = #imageLiteral(resourceName: "dashboard_my_number")
        case .topupOtherNumber:
            self.imageViewtype.image = #imageLiteral(resourceName: "dashboard_other_number")
        case .payto:
            self.imageViewtype.image = #imageLiteral(resourceName: "dashboard_pay_send")
        case .overseas:
            self.imageViewtype.image = #imageLiteral(resourceName: "dashboard_overseas_recharge")
        }
    }
    
}

extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in:UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: image!.cgImage!)
    }
}

//MARK:- More Payment Function
extension PTRecieptViewController: MoreControllerActionDelegate {
    
    func actionFor(option: MorePaymentAction) {
        switch option {
        case .morePayment  : self.morePayment()
        case .repeatPayment: self.repeatPayment()
        case .invoice      : self.invoice()
        case .share        : self.share()
        }
    }
    
    func morePayment() {
        UserDefaults.standard.set(true, forKey: "morePaymentPayto")
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func repeatPayment() {
        if let cell = self.getCellFromDisplay() {
            for (index,modelCell) in self.cellA.enumerated() {
                if cell == modelCell {
                    let dict = self.transactionDetails[index]
                    let number = dict.safeValueForKey("destination") as? String
                    UserDefaults.standard.set(number, forKey: "stringPaymentPayto")
                    UserDefaults.standard.set(true, forKey: "repeatPaymentPayto")
                    self.navigationController?.setNavigationBarHidden(false, animated: true)
                    self.navigationController?.navigationBar.tintColor = .white
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
    }
    
    func getCellFromDisplay() -> UICollectionViewCell? {
        let visibleCell = CGRect.init(origin: self.collectionView.contentOffset, size: self.collectionView.bounds.size)
        let visiblePoint = CGPoint.init(x: visibleCell.midX, y: visibleCell.midY)
        let indexPath = self.collectionView.indexPathForItem(at: visiblePoint)
        if let path = indexPath {
            let cell = self.collectionView.cellForItem(at: path)
            return cell
        }
        return nil
    }
    
    func invoice() {
     //   Passing dictionary values:
        if let cell = self.getCellFromDisplay() {
            for (index,modelCell) in self.cellA.enumerated() {
                if cell == modelCell {
                    let dict = self.transactionDetails[index]
                    var pdfDictionary = Dictionary<String,Any>()
                    pdfDictionary["businessName"]       = dict.safeValueForKey("businessName") ?? dict.safeValueForKey("merchantname") ?? "Unknown" as String
                    pdfDictionary["senderAccName"]      = UserModel.shared.name
                    pdfDictionary["senderAccNo"]        = UserModel.shared.mobileNo
                    pdfDictionary["receiverAccName"]    =  pdfDictionary["businessName"]
                    pdfDictionary["receiverAccNo"]      = dict.safeValueForKey("destination") ?? ""
                    pdfDictionary["transactionID"]      = dict.safeValueForKey("transid") ?? ""
                    pdfDictionary["transactionType"]    = dict.safeValueForKey("responsetype") ?? ""
                    pdfDictionary["transactionDate"]    = dict.safeValueForKey("responsects") ?? ""
                    pdfDictionary["remarks"]            = dict.safeValueForKey("greeting") ?? ""
                    pdfDictionary["amount"]             = dict.safeValueForKey("amount") ?? ""
                    pdfDictionary["invoiceTitle"]       = "Transaction Receipt".localized
                    pdfDictionary["logoName"]           = "appIcon_Ok"
                    // string generation logic for scan qr code
                    
                    let lat  = geoLocManager.currentLatitude ?? ""
                    let long = geoLocManager.currentLongitude ?? ""
                   
                    let firstPart  = "00#" + "\(pdfDictionary["businessName"] ?? "")" + "-" + "\(pdfDictionary["receiverAccNo"] ?? "") " + "@" + "\(pdfDictionary["amount"] ?? "")" + "&"
                    let secondPart =  "\(pdfDictionary["loyaltypoints"] ?? "")" + "β" + "\(lat)" + "γ" + "\(long)" + "α" + "\(pdfDictionary["transactionID"] ?? "")" + "" + "`,,"
                    
                    let finalPart = String.init(format:"%@%@", firstPart,secondPart)
                    
                    guard let hashedQRKey = AESCrypt.encrypt(finalPart, password: "m2n1shlko@$p##d") else { return }
                    
                    let qrObject = PTQRGenerator.init()
                    pdfDictionary["qrImage"] = qrObject.getQRImage(stringQR: hashedQRKey, withSize: 10)
                    
                    guard let pdfUrl = CodeSnippets.createInvoicePDF(dictValues: pdfDictionary) else {
                        println_debug("Error - pdf not generated")
                        return
                    }
                    
                    let story = UIStoryboard.init(name: "Topup", bundle: nil)
                    guard let vc = story.instantiateViewController(withIdentifier: "SharePDFViewController") as? SharePDFViewController else { return }
                    vc.url = pdfUrl
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    func share() {
        if let cell = self.getCellFromDisplay() {
            let imageShare = cell.snapshotOfCustomeView
                let vc = UIActivityViewController(activityItems: [imageShare], applicationActivities: [])
                present(vc, animated: true)
        }
    }
}





