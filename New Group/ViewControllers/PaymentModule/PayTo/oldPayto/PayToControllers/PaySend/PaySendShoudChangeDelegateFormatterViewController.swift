//
//  PaySendShoudChangeDelegateFormatterViewController.swift
//  OK
//
//  Created by Ashish on 3/21/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

extension PaySendViewController {
    
    func shouldChangeCharacters(textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case self.mobileNumber:
            let mobileShouldReturn = self.MobileNumberHandler(textField: textField, shouldChangeCharactersIn: range, replacementString: string)
            return mobileShouldReturn
        case self.confirmMobileNum:
            let confirmShouldReturn = self.confirmMobileNumberHandler(textField: textField, shouldChangeCharactersIn: range, replacementString: string)
            return confirmShouldReturn
        case self.enterAmount:
            let amountHandlerReturn = self.amountHandler(textField: textField, shouldChangeCharactersIn: range, replacementString: string)
            return amountHandlerReturn
        default:
            return true
        }
    }
    
    //MARK:- Amount Handler
    func amountHandler(textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let textCount = text.count
        // Amount Validation
        
        let typingBalance = (text.replacingOccurrences(of: ",", with: "") as NSString).floatValue
        let walletBalance = (UserLogin.shared.walletBal as NSString).floatValue
        
        println_debug(typingBalance)
        println_debug(walletBalance)
        
        if typingBalance > walletBalance {
            textField.text = ""
            self.showErrorAlert(errMessage: "Insufficient Balance".localized)
            self.hideForMultiPayment()
            return false
        }
        
        if let check = self.amountCheck {
            if check.type == "1" {
                if typingBalance > (check.limit as NSString).floatValue  {
                    return false
                }
            }
        }


        if textCount > 0 { show_multi_submit() } else {
                self.heightCons_remarks.constant = 0.00
                self.remarks.isHidden = true
                self.hide_multi_submit()
            }
            if textCount >= 1 {
                self.heightCons_remarks.constant   = 50.00
                self.heightCons_burAmount.constant = 50.00
                self.burAmount.isHidden = false
                self.remarks.isHidden = false
                dynamicHeight = ModelDesignHeight.amountEntered
                self.view.layoutIfNeeded()
            } else {
                self.heightCons_remarks.constant   = 0.00
                self.heightCons_burAmount.constant = 0.00
                self.burAmount.isHidden = true
                self.remarks.isHidden = true
                dynamicHeight = ModelDesignHeight.confirmedEntered
                self.view.layoutIfNeeded()
            }
            if text.contains(".") {

                // if a timer is already active, prevent it from firing
                if amountCheckingTimer != nil {
                    amountCheckingTimer?.invalidate()
                    amountCheckingTimer = nil
                }
                
                // reschedule the search: in 1.0 second, call the searchForKeyword method on the new textfield content
                self.amountCheckingTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(searchForKeyword(_:)), userInfo: textField.text ?? "", repeats: false)
                
                let amountArray = text.components(separatedBy: ".")
                if let last = amountArray.last {
                    if last.count > 2 {
                        return false
                    } else {
                        return true
                    }
                }
                
                return true
            } else {
                let mystring = text.replacingOccurrences(of: ",", with: "")
                let number = NSDecimalNumber(string: mystring)
                
                let formatter = NumberFormatter()
                formatter.groupingSeparator = ","
                formatter.groupingSize = 3
                formatter.usesGroupingSeparator = true
                var num = formatter.string(from: number)
                if num == "NaN" {
                    num = ""
                }
                self.enterAmount.text = num
                // if a timer is already active, prevent it from firing
                if amountCheckingTimer != nil {
                    amountCheckingTimer?.invalidate()
                    amountCheckingTimer = nil
                }
                
                // reschedule the search: in 1.0 second, call the searchForKeyword method on the new textfield content
                self.amountCheckingTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(searchForKeyword(_:)), userInfo: textField.text ?? "", repeats: false)
                return false
            }
    }
    
    //MARK:- Mobile Number Formatter
    func confirmMobileNumberHandler(textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let textCount  = text.count

        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            self.dynamicHeight = ModelDesignHeight.confirmedEntered
            if countryObject.code == "+95" {
                self.hideAll()
                self.textfieldArray.forEach({ [weak self](textfield) in
                    self?.enterAmount.text?.removeAll()
                    self?.confirmMobileNum.isHidden = false
                    self?.userType.isHidden = false
                })
            }
        }
        if self.mobileNumber.text?.count == textCount, String(describing: self.mobileNumber!.text!.last!) == string {
                self.view.layoutIfNeeded()
                let agentCode = getDestinationNum(self.mobileNumber.text!, withCountryCode: countryObject.code)
                self.checkAgentCode(agentCode: agentCode)
            }
            if validObj.checkMatchingNumber(string: text, withString: self.mobileNumber.text!) {
                self.confirmMobileNum.text = text
                return false
            } else {  return false  }
    }

    func MobileNumberHandler(textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        let objectValidation = validObj.getNumberRangeValidation(text)
        
        let textCount  = text.count
        
        var  validCount = objectValidation.max
        var minValid = objectValidation.min

        if countryObject.code != "+95" {
            validCount = 13
            minValid = 4
        }
        
        if textCount > 3 {
            self.loadContact(txt: text)
            self.contactSuggestionView(toHide: false)
            if validCount == textCount {
                self.contactSuggestionView(toHide: true)
            }
            if textCount > validCount {
                self.contactSuggestionView(toHide: true)
            }
        } else {
            self.contactSuggestionView(toHide: true)
        }
        
        let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: mobileNumberAcceptableCharacters).inverted
        let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
        
        if string != filteredSet {
            return false
        }
        
        let char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                    self.hideAll()
                    self.confirmMobileNum.isUserInteractionEnabled = true
                    self.confirmMobileNum.placeholder = "Confirm Mobile Number".localized
                    self.confirmMobileNum.isEnabled = true
                    self.confirmMobileNum.leftView = nil
                let countryCode = String.init(format: "(%@)", countryObject.code)
                    leftViewCountryForConfirmation.wrapCountryViewData(img: countryObject.flag , str: countryCode)
                    self.confirmMobileNum.leftView = leftViewCountryForConfirmation
                    self.confirmMobileNum.leftViewMode  = UITextFieldViewMode.always
                    self.dynamicHeight = ModelDesignHeight.justShown
                    if textField.text == "09" {
                        return (countryObject.code == "+95") ? false : true
                    }
            }
            
            // Normal Validations
            if validObj.checkRejectedNumber(prefix: text) == true {
                return rejectNumberAlgorithm()
            }

        if textCount >= minValid {
            self.hideAll()
            self.confirmMobileNum.isHidden = false
            self.userType.isHidden = false
            self.dynamicHeight = ModelDesignHeight.mobileNumberEntered
        }
        
        if textCount == validCount {
            self.hideAll()
            self.confirmMobileNum.isHidden = false
            self.userType.isHidden = false
            self.dynamicHeight = ModelDesignHeight.mobileNumberEntered
        }

        if countryObject.code == "+95" {
            if validCount == textCount {
                self.mobileNumber.text = text
                self.contactSuggestionView(toHide: true)
                self.confirmMobileNum.becomeFirstResponder()
                return false
            }
            if textCount > validCount {
                self.contactSuggestionView(toHide: true)
                self.confirmMobileNum.becomeFirstResponder()
                return false
            }
        }
        
        return true
    }
    
    private func rejectNumberAlgorithm() -> Bool {
        hideAll()
        self.mobileNumber.text = "09"
        self.showErrorAlert(errMessage: "Rejected Number".localized)
        dynamicHeight = ModelDesignHeight.justShown
        self.contactSuggestionView(toHide: true)
        return false
    }
    
    // amount update delegate fire for pay send view controller
    @objc func searchForKeyword(_ timer: Timer) {
        
        if var burmese = self.enterAmount.text {
            burmese = burmese.replacingOccurrences(of: ",", with: "")
            let amount = (burmese as NSString).integerValue
            let amntStr = String.init(format: "%i", amount)
                self.burAmount.text = objectSound.getBurmeseSoundText(text: amntStr)
        }

        // retrieve the keyword from user info
        guard let delegate = self.delegate  else { return }

        delegate.amountValidationForMultipayment(obj: self)
        
    }

    
}

