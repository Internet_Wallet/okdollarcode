//
//  PaySendViewController.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/27/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftMultiSelect
import IQKeyboardManagerSwift
import Contacts

enum selectionType {
    case name
    case number
}

enum MultiSelectionType {
    case payto, multi, selfUpdate
}

enum PaytoIDTransactionType {
    case paytoWithID, paytoWithoutID, taxi, bus, toll, ferry
}

struct ModelDesignHeight {
    static let justShown           : CGFloat = 110.00
    static let mobileNumberEntered : CGFloat = 210.00
    static let confirmedEntered    : CGFloat = 360.00 + 100
    static let amountEntered       : CGFloat = 460.00 + 100
}


class PaySendViewController: PayToBaseViewController, CountryLeftViewDelegate, CountryViewControllerDelegate,PTIndicatorInfoProvider, BioMetricLoginDelegate, PTClearButtonDelegate, TipMerchantViewControllerDelegate, defaultVehicleDelegate, ContactSuggestionDelegate {
    
    // Header info For PayToViewController
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PTPagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo.init(title: "Pay To".localized)
    }

    struct CountryPayTo {
        var code = ""
        var flag = ""
        
        init(code: String, flag: String) {
            self.code = code
            self.flag = flag
        }
    }
    
    var scanObj : PTScanObject?
    
 // Basic Outlets and Variables
    @IBOutlet  weak var mobileNumber     : UITextField!
    @IBOutlet  weak var confirmMobileNum : UITextField!
    @IBOutlet  weak var userType         : UITextField!
    @IBOutlet  weak var enterAmount      : UITextField!
        {
        didSet
        {
            enterAmount.placeholder = enterAmount.placeholder?.localized
        }
    }
    @IBOutlet  weak var burAmount        : UITextField!
        {
        didSet
        {
            burAmount.placeholder = burAmount.placeholder?.localized
        }
    }
    @IBOutlet  weak var remarks          : UITextField!
        {
        didSet
        {
            remarks.placeholder = remarks.placeholder?.localized
        }
    }
    @IBOutlet  weak var hideMyNumber     : UITextField!
        
    
    // added fields
    @IBOutlet weak var merchantSubscriberTextfield: UITextField!
    var businessName = ""
    
    var varType = "PAYTO"
    
    var textfieldArray = [UITextField]()
    let validObj       = PayToValidations()
    let formatter      = NumberFormatter()
    var pContacts      = [ContactPicker]()
    var pSearchedContact = [ContactPicker]()
    var transaction      = Dictionary<String,Any>()
    var processedTransaction = [Dictionary<String,Any>]()
    
   weak var delegate : TableReloadDelegate?
    
    var isTransportation : String?
    
    let objectSound = BurmeseSoundObject()
    
    var dynamicHeight : CGFloat = ModelDesignHeight.justShown {
        didSet {
            for textfield in self.textfieldArray {
                if textfield.isHidden == true, textfield != self.userType, textfield != self.hideMyNumber {
                    textfield.text = ""
                } else if textfield.isHidden == true, textfield == self.userType {
                    textfield.text = "PAYTO"
                }
            }
            if dynamicHeight != oldValue {
//                self.scrollerForTextfields.contentSize = .init(width: self.scrollerForTextfields.frame.width, height: dynamicHeight + 8)
                guard (self.delegate?.reloadTableData(obj: self) != nil) else { return }
            }
        }
    }
    
    var multiPayments : String?
    
    var amountCheckingTimer: Timer? // check to compare amount in multi payto controller through delegate
    
    var objectType : PayToTypes = PayToTypes.singlePay
    
    var paymentType : String?
    
    @IBOutlet weak var heightCons_burAmount: NSLayoutConstraint!
    @IBOutlet weak var heightCons_remarks: NSLayoutConstraint!
    
    @IBOutlet weak var heightAddvehicle: NSLayoutConstraint!
    @IBOutlet weak var heightBusinessname: NSLayoutConstraint!
    
    var multiScreen : MultiSelectionType = MultiSelectionType.payto

    @IBOutlet weak var submitBtn: UIButton!
        {
        didSet
        {
            self.submitBtn.setTitle(self.submitBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    var multiButton : ActionButton?
    var toRemoveMultiBtn = 1
    
    var countryObject = CountryPayTo.init(code: "+95", flag: "myanmar")
    
    @IBOutlet weak var addYourVehicle: UITextField!
    @IBOutlet weak var businessNametf: UITextField!
    
    var leftViewCountry                = PaytoViews.updateView()
    var leftViewCountryForConfirmation = PaytoViews.updateView()
    var contactRightView               = ContactRightView.updateView()
    
    var typeLeftView        = DefaultIconView.updateView(icon: "pt_categories")
    var merchantLeftView    = DefaultIconView.updateView(icon: "user")
    var businessLeftView    = DefaultIconView.updateView(icon: "user")
    var amountLeftView      = DefaultIconView.updateView(icon: "amount")
    var burAmountLeftView   = DefaultIconView.updateView(icon: "amount")
    var remarkLeftView      = DefaultIconView.updateView(icon: "remark")
    var addVehicleView      = DefaultIconView.updateView(icon: "add_car")
    var hideLeftView        :  HideMyView?

    var transactionType : PaytoIDTransactionType = PaytoIDTransactionType.paytoWithID
    
    var amountCheck : (type: String, limit: String)?

    // outlets and variables for contact searching view
    @IBOutlet weak var contactSearchTableView        : UITableView!
    @IBOutlet weak var heightConstantContactSearch   : NSLayoutConstraint!
    @IBOutlet weak var searchingContactView          : CardDesignView!
    
     var contactsArray      = Array<CNContact>()
     var searchingContacts  = Array<CNContact>()
     let contactsStore      = CNContactStore()

    @IBOutlet weak var scrollerForTextfields: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadContactSuggessionView()
        self.contactSuggestionView(toHide: true)
        
        self.heightAddvehicle.constant = 0.0
        self.heightBusinessname.constant = 0.0
        
        let framHideMyView = CGRect.init(x: 5, y: 5, width: self.hideMyNumber.bounds.width - 5, height: self.hideMyNumber.bounds.height - 5)
        hideLeftView = HideMyView.updateView(framHideMyView, delegate: self)

        UserDefaults.standard.set(false, forKey: "popPayTo")

        self.submitBtn.isHidden = true
        DispatchQueue.global(qos: .background).async { self.getContacts() }

        self.wrapMobileLeftView()
        self.updateViewsInitialLogic()
        self.loadFloatButtons()
        self.hide_multi_submit()
        self.hideAll()
        self.wrapClearButtons()
        
       self.mobileNumber.becomeFirstResponder()
        

    }
    
    override func setNeedsStatusBarAppearanceUpdate() {
        println_debug(UIApplication.shared.statusBarFrame)
    }
    
    var contactSuggessionView : ContactSuggestionVC?
    
    func loadContactSuggessionView() {
//        contactSuggessionView  =  UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "ContactSugestView_ID") as? ContactSuggestionVC
//        contactSuggessionView?.delegate   = self  //as? ContactSuggestionDelegate
//        contactSuggessionView?.view.frame = CGRect.zero
//        addChildViewController(contactSuggessionView!)
//        contactSuggessionView!.didMove(toParentViewController: self)
//        let frame = CGRect.init(x: 55.00, y: self.mobileNumber.frame.origin.y + 51.00, width: self.mobileNumber.frame.width - 120.00, height: 100)
//        contactSuggessionView?.view.frame = frame
//        self.view.addSubview(contactSuggessionView!.view)
//        contactSuggessionView?.view.isHidden = true
    }
    
    var contactSearchedDict = [Dictionary<String,Any>]()
    
    func loadContact(txt: String) {
        
        DispatchQueue.global(qos: .background).async {
            self.contactSuggesstionCompletion(txt) { (contacts) in
                DispatchQueue.main.async {

                    if contacts.count <= 0 {
                        self.contactSuggestionView(toHide: true)
                    }
                    
                    self.contactSearchedDict = contacts
                    self.contactSearchTableView.reloadData()
                }
            }

        }
        
    }
    
    func contactSuggestionView(toHide flag: Bool) {
        self.searchingContactView.isHidden = flag
    }
    
    func didSelectFromContactSuggession(number: Dictionary<String, Any>) {
        self.contactSuggestionView(toHide: true)
        let mobileNumber = number[ContactSuggessionKeys.phonenumber_backend] as? String ?? ""
        let name = number[ContactSuggessionKeys.contactname] as? String ?? "Unknown"
        let fav = FavModel.init("", phone: mobileNumber, name: name, createDate: "", agentID: "", type: "")
        let contact = ContactPicker.init(object: fav)
        self.decodeContact(contact: contact)
    }

    func hide_multi_submit() {
        
        if self.multiPayments == nil {
            self.hideForMultiPayment()
            return
        }
            multiButton?.hide()
            self.submitBtn.isHidden = true
    }
    
    func show_multi_submit() {
        multiButton?.show()
        submitBtn.isHidden = false
    }
    
    func makeNilForMultipayment() {
        self.multiPayments = nil
    }
    
    func hideForMultiPayment() {
        self.multiButton?.hide()
        self.submitBtn.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.backNavigationAction()
        let buttonIcon      = UIImage(named: "tabBarBack")
        let leftBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.done, target: self, action: #selector(PayToBaseViewController.dismissTheViewControllerPT(_:)))
        leftBarButton.image = buttonIcon
        self.navigationItem.leftBarButtonItem = leftBarButton
        self.navigationController?.navigationBar.tintColor = .white

    }
    
    func wrapClearButtons() {
        
        let confirmClearBtn = PTClearButton.updateView(strTag: "confirmClearBtn", delegate: self)
        let amountClearBtn  = PTClearButton.updateView(strTag: "amountClearBtn", delegate: self)
        let remarksClearBtn = PTClearButton.updateView(strTag: "remarksClearBtn", delegate: self)
        
        self.confirmMobileNum.rightViewMode = .always
        self.confirmMobileNum.rightView     = confirmClearBtn
        
        self.enterAmount.rightViewMode = .always
        self.enterAmount.rightView     = amountClearBtn
        
        self.remarks.rightViewMode = .always
        self.remarks.rightView = remarksClearBtn

        self.mobileNumber.setBottomBorder()
        self.confirmMobileNum.setBottomBorder()
        self.userType.setBottomBorder()
        self.merchantSubscriberTextfield.setBottomBorder()
        self.enterAmount.setBottomBorder()
        self.burAmount.setBottomBorder()
        self.remarks.setBottomBorder()
        self.businessNametf.setBottomBorder()
        self.addYourVehicle.setBottomBorder()
        
    }
    
    func clearField(strTag: String) {
        switch strTag {
        case "confirmClearBtn":
            self.hideAll()
            self.confirmMobileNum.isHidden = false
            self.confirmMobileNum.text = (countryObject.code == "+95") ? "09" : ""
            self.dynamicHeight = ModelDesignHeight.mobileNumberEntered
            self.hideLeftView?.fireDelegate()
            break
        case "amountClearBtn":
            self.enterAmount.text?.removeAll()
            self.heightCons_remarks.constant   = 0.00
            self.heightCons_burAmount.constant = 0.00
            self.burAmount.isHidden = true
            self.remarks.isHidden = true
            self.dynamicHeight = ModelDesignHeight.confirmedEntered
            self.hide_multi_submit()
            self.view.layoutIfNeeded()
            break
        case "remarksClearBtn":
            self.remarks.text = ""
            break
        default:
            break
        }
    }
    
    override func popTheViewControllerPT(_ sender: UIBarButtonItem!) {
        super.popTheViewControllerPT(sender)
        self.dismiss(animated: true, completion: nil)
    }
    
    func forMultiViewControllerFirstCell(dic: Dictionary<String,Any>) {
        self.mobileNumber.isHidden     = false
        self.confirmMobileNum.isHidden = false
        self.enterAmount.isHidden      = false
        
        self.heightCons_remarks.constant   = 50.00
        self.heightCons_burAmount.constant = 50.00
        
        self.burAmount.isHidden    = false
        self.remarks.isHidden      = false
        
        self.userType.isHidden     = false
        self.merchantSubscriberTextfield.isHidden = false
        
        
        self.hideMyNumber.isHidden = false
        
        self.view.isUserInteractionEnabled = false
        
        dynamicHeight = ModelDesignHeight.amountEntered
        
        self.mobileNumber.text = dic["mobileNumber"] as? String
        self.confirmMobileNum.text = dic["confirm"] as? String
        self.enterAmount.text = dic["amount"] as? String
        self.burAmount.text   = dic["burmeseAmount"] as? String
        self.remarks.text = dic["remark"] as? String
        self.hideMyNumber.text = dic["hide"] as? String
        self.userType.text = (dic["type"] as? String)?.uppercased()
        self.merchantSubscriberTextfield.text = dic["businessName"] as? String
        self.confirmMobileNum.text = dic["confirm"] as? String
    }
    

    func navigateToMultiPayent(secondObj: PaySendViewController) {
        
    }
    
    //Submit Action
    @IBAction func submit_PayAction(_ sender: UIButton) {
        if self.userType.text?.uppercased() == "Restaurant".uppercased(), !(self.remarks.text!.hasPrefix("BILL")){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TipMerchantViewController") as? TipMerchantViewController
            vc?.modalPresentationStyle = .overCurrentContext
            vc?.delegate = self
            vc?.amount = self.enterAmount.text!.replacingOccurrences(of: ",", with: "")
            self.navigationController?.present(vc!, animated: true, completion: nil)
            return
        }

        if let text = self.userType.text {
            if text.localized == "FUEL".localized || text.localized == "PARKING".localized || text.localized == "TOLL".localized {
                if let vehicle = self.addYourVehicle.text, vehicle.count <= 0 {
                    self.showErrorAlert(errMessage: "Please select Vehicle".localized)
                    return
                }
            }
        }
        
        if let text = self.userType.text, text.lowercased().hasPrefix("Unknown".lowercased()) {
            alertViewObj.wrapAlert(title: nil, body: "Are you sure want to payto Unregister number?".localized, img: #imageLiteral(resourceName: "dashboard_pay_send"))
            alertViewObj.addAction(title: "OK".localized, style: .target) {
                self.callSubmit()
            }
            alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
                
            }
            alertViewObj.showAlert()
            return
        }

        self.callSubmit()
    }
    
    func didselectNoThnx() {
        self.callSubmit()
    }
    
    func didSelectOK(amount: String) {
        if let amountStr = self.enterAmount.text, amount.count > 0 {
            let amnt = amountStr.replacingOccurrences(of: ",", with: "")
            self.enterAmount.text = "\((amnt as NSString).floatValue + (amount as NSString).floatValue)"
            self.remarks.text = String.init(format: "BILL %@ MMK + Tip %@ MMK", amnt, amount)
            self.callSubmit()
        }
        
    }
    
    func callSubmit() {
        let mobileNo =  getAgentCode(numberWithPrefix: self.mobileNumber.text!, havingCountryPrefix: self.countryObject.code)
        if mobileNo == UserModel.shared.mobileNo {
            alertViewObj.wrapAlert(title: "", body: "Source Number and Destination Number should not be same".localized, img: #imageLiteral(resourceName: "dashboard_pay_send"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                DispatchQueue.main.async {
                    self.hideAll()
                    self.mobileNumber.text?.removeAll()
                    self.mobileNumber.becomeFirstResponder()
                }
            })
            alertViewObj.showAlert(controller: self)
            return
        }
        if appDelegate.checkNetworkAvail() {
            OKPayment.main.authenticate(screenName: "payto", delegate: self)
        } else {
            self.showErrorAlert(errMessage: "No Internet Connection".localized)
        }
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if isSuccessful == false { return }
        
        if appDelegate.checkNetworkAvail() {
            self.initPayment()
        }
    }
    
    
    @IBAction func didChangeAmount(_ sender: UITextField) {
    }
    
    deinit {
        self.textfieldArray.removeAll()
        self.pContacts.removeAll()
        self.pSearchedContact.removeAll()
        self.transaction.removeAll()
        self.processedTransaction.removeAll()
        self.multiButton = nil
        self.delegate = nil
    }
}

extension PaySendViewController: UITextFieldDelegate {
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    // delegate callback
    func slelectedVehicle(defaultVehicleStr: String) {
       self.addYourVehicle.text = defaultVehicleStr
    }
    
    func deletedVehicleCallback() {
        self.addYourVehicle.text = ""
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == self.addYourVehicle {
                let viewController = UIStoryboard(name: "AddVehicle", bundle: Bundle.main).instantiateViewController(withIdentifier: "addVehicleRoot")
                
                if let nav = viewController as? UINavigationController {
                    for views in nav.viewControllers {
                        if let vc = views as? AddVehicleVC {
                            vc.delegate = self
                            vc.isFromPayto = true
                        }
                    }
                }
                self.present(viewController, animated: true, completion: nil)
            return false
        }

        var textCount = 0
        if let text = textField.text {
            textCount = text.count
        }
        if textField == self.hideMyNumber || textField == self.userType { return false }
        if textField == self.mobileNumber || textField == self.confirmMobileNum {
            if textCount <= 2, countryObject.code == "+95" {
                textField.text = "09"
            }
            
            if textField == self.confirmMobileNum {
                self.contactSuggestionView(toHide: true)
            }
        }
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if textField == self.confirmMobileNum {
            self.hideAll()
            self.confirmMobileNum.isHidden = false
            self.dynamicHeight = ModelDesignHeight.mobileNumberEntered
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let changingAccetable = self.shouldChangeCharacters(textField: textField, shouldChangeCharactersIn: range, replacementString: string)
        return changingAccetable
	}
    
    func updateSearchResult(text: String) {
        var num = text
        if num.first == "0" {
            num.remove(at: .init(encodedOffset: 0))
        }
    }
}
