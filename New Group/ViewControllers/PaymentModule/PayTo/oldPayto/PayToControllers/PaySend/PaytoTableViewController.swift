//
//  PaytoTableViewController.swift
//  OK
//
//  Created by Ashish Kumar Singh on 12/4/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Contacts

//MARK: Contact Searching Methods
extension PaySendViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactSearchedDict.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchingCell", for: indexPath)
        
        let dict = contactSearchedDict[indexPath.row]
        
        let name  = dict.safeValueForKey(ContactSuggessionKeys.contactname) as? String ?? "Unknown"
        let phone = dict.safeValueForKey(ContactSuggessionKeys.phonenumber_onscreen) as? String ?? "Unknown"
        
        cell.textLabel?.text = name
        cell.detailTextLabel?.text = phone
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = self.contactSearchedDict[indexPath.row]
        let mobileNumber = dict[ContactSuggessionKeys.phonenumber_backend] as? String ?? ""
        let name = dict[ContactSuggessionKeys.contactname] as? String ?? "Unknown"
        
        var selectedNumber = ""
        
        if let number = mobileNumber.addingPercentEncoding(withAllowedCharacters: .decimalDigits) {
            let phones  = number.replacingOccurrences(of: "%20", with: "")
            let phones2 = phones.replacingOccurrences(of: "%C2%A0", with: "")
            if let final = phones2.removingPercentEncoding {
                selectedNumber = final
                let fav = FavModel.init("", phone: selectedNumber, name: name, createDate: "", agentID: "", type: "")
                let contact = ContactPicker.init(object: fav)
                self.decodeContact(contact: contact)
                self.contactSuggestionView(toHide: true)
            }
        }
    }
    
    func filterContacts(searchString: String) {
        
        let prefixPredicate = NSPredicate(format: "phoneNumbers = '\(searchString)'")
        
        self.searchingContacts = (self.contactsArray as NSArray).filtered(using: prefixPredicate) as! [CNContact]
        self.contactSearchTableView.reloadData()
    }
    
    func allowedContactKeys() -> [CNKeyDescriptor]{
        //We have to provide only the keys which we have to access. We should avoid unnecessary keys when fetching the contact. Reducing the keys means faster the access.
        return [CNContactNamePrefixKey          as CNKeyDescriptor,
                CNContactGivenNameKey           as CNKeyDescriptor,
                CNContactFamilyNameKey          as CNKeyDescriptor,
                CNContactOrganizationNameKey    as CNKeyDescriptor,
                CNContactBirthdayKey            as CNKeyDescriptor,
                CNContactImageDataKey           as CNKeyDescriptor,
                CNContactThumbnailImageDataKey  as CNKeyDescriptor,
                CNContactImageDataAvailableKey  as CNKeyDescriptor,
                CNContactPhoneNumbersKey        as CNKeyDescriptor,
                CNContactEmailAddressesKey      as CNKeyDescriptor,
        ]
    }
}
//MARK: --------------------------------------------------------------------------------------------------------------------------------------
//MARK: Contact Delegates
extension PaySendViewController: ContactPickerDelegate  {
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        
        if multiScreen == MultiSelectionType.payto {
            decodeContact(contact: contact)
        } else {
            multiScreen = MultiSelectionType.payto
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "firstVC") as? PaySendViewController
           self.navigateWithContact(secondObj: vc!, contact: contact)
        }
        
    }
}

