//
//  PayToWebserviceClassExtension.swift
//  OK
//
//  Created by Ashish on 12/11/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct TransactionType {
    static let paytoID = "PAYTO"
}
extension PaySendViewController: PTWebResponseDelegate {
    
    func checkAgentCode(agentCode: String) {
        
        if agentCode == UserModel.shared.mobileNo {
            DispatchQueue.main.async {
                alertViewObj.wrapAlert(title: "", body: "You can't make payment to own mobile Number", img: #imageLiteral(resourceName: "dashboard_pay_send"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                        self.hideAll()
                        self.mobileNumber.text?.removeAll()
                        self.mobileNumber.text = (self.countryObject.code == "+95") ? "09" : ""
                        self.mobileNumber.becomeFirstResponder()
                        self.dynamicHeight = ModelDesignHeight.justShown
                        let countryArray = self.revertCountryArray() as! Array<NSDictionary>
                        let cDetails = self.identifyCountry(withPhoneNumber: UserModel.shared.formattedNumber, inCountryArray: countryArray) // 1 -> flag, 0 -> code
                        self.nameORnumber(selectiontype: .number, codeDetails: cDetails)
                })
                alertViewObj.showAlert(controller: self)
            }
            return
        }
        
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            self.view.endEditing(true)
            self.businessName = ""
            println_debug("checkAgentCode with Agent => " + agentCode)
            let web = PayToWebApi()
            web.delegate = self
            
            let type = self.getTransactionString(self.transactionType)
            
            let  urlString = String.init(format: PTHelper.chkAgentCode,agentCode,type)
            let pram = Dictionary<String,String>() as AnyObject
            guard let ur = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
            guard let str = URL.init(string: ur) else { return }

            if multiScreen == MultiSelectionType.payto {
                web.genericClass(url: str, param: pram, httpMethod: "GET", mScreen: ScreenName.chkAgentCode)
            } else {
                web.genericClass(url: str, param: pram, httpMethod: "GET", mScreen: ScreenName.chkAgentCodeMulti)
            }
        } else {
            println_debug("No Internet")
        }
    }
    
    func initPayment() {
        
        DispatchQueue.main.async {
            if appDelegate.checkNetworkAvail() {
                println_debug("Single Payment Initiated")
                let web = PayToWebApi()
                web.delegate = self
                let mobileNo =  self.getAgentCode(numberWithPrefix: self.mobileNumber.text!, havingCountryPrefix: self.countryObject.code)
                var ip = ""
                if let ipAdd = OKBaseController.getIPAddress() {
                    ip = ipAdd
                }
                
                let amount = self.enterAmount.text?.replacingOccurrences(of: ",", with: "") ?? ""
                
                let urlString = String.init(format: PTHelper.confirmPay, UserModel.shared.mobileNo,mobileNo,amount,ok_password ?? "","IPAY",ip,"ios",self.getTransactionString(self.transactionType),"GPRS",UserLogin.shared.token)
                println_debug(urlString)
                guard let urlThreadSafe = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
                guard let urlQuery  = URL.init(string: urlThreadSafe) else { return }
                let pRam = Dictionary<String,String>()
                web.genericClassXML(url: urlQuery, param: pRam as AnyObject, httpMethod: "POST", mScreen: ScreenName.cashBack)
            }
        }
        
    }
    
    func webSuccessResult(data: Any, screen: String) {
        if screen == ScreenName.chkAgentCode {
            if let json = data as? [String: Any] {
                let jsonStr = json["Data"] as! String
                if jsonStr == "" {
                    DispatchQueue.main.async {
                        self.hideAll()
                        self.mobileNumber.text?.removeAll()
                        self.mobileNumber.text = (self.countryObject.code == "+95") ? "09" : ""
                        self.mobileNumber.becomeFirstResponder()
                        self.dynamicHeight = ModelDesignHeight.justShown
                        let msg = json["Msg"] as? String ?? ""
                        alertViewObj.wrapAlert(title: "", body: msg.localized, img: #imageLiteral(resourceName: "dashboard_pay_send"))
                        alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                            if let view = self.hideLeftView {
                                view.fireDelegate()
                            }
                        })
                        alertViewObj.showAlert(controller: self)
                    }
                } else {
                    if let data = jsonStr.data(using: String.Encoding.utf8) {
                        do {
                            let dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
                            if dict != nil {
                                DispatchQueue.main.async {
                                    if self.transactionType == .bus || self.transactionType == .toll || self.transactionType == .ferry {
                                        if let type = dict?.safeValueForKey("AccountType") as? String {
                                            if type.lowercased() == "SUBSBR".lowercased() {
                                                self.hideAll()
                                                self.mobileNumber.text?.removeAll()
                                                self.mobileNumber.text = (self.countryObject.code == "+95") ? "09" : ""
                                                self.mobileNumber.becomeFirstResponder()
                                                self.dynamicHeight = ModelDesignHeight.justShown
                                                return
                                            }
                                        }
                                    }
                                    self.userType.isHidden = false
                                    self.merchantSubscriberTextfield.isHidden = false
                                    self.enterAmount.isHidden  = false
                                    self.hideMyNumber.isHidden = false
                                    
                                    if self.enterAmount.text != "" {
                                        self.dynamicHeight = ModelDesignHeight.amountEntered
                                    } else {
                                        self.heightCons_remarks.constant   = 0.00
                                        self.heightCons_burAmount.constant = 0.00
                                        self.dynamicHeight = ModelDesignHeight.confirmedEntered
                                    }
                                    
                                    self.heightAddvehicle.constant = 0.00
                                    self.addYourVehicle.isHidden = true

                                    self.userType.text = (dict?["LocalTransType"] as?  String)?.uppercased()
                                    self.merchantSubscriberTextfield.text = dict?.safeValueForKey("MercantName") as? String ?? "Unknown"
                                    self.businessName = dict?.safeValueForKey("BusinessName") as? String ?? ""
                                    if self.businessName.count > 0 {
                                        self.businessNametf.text = self.businessName
                                        self.heightBusinessname.constant = 50.00
                                        self.businessNametf.isHidden = false
                                    } else {
                                        self.businessNametf.text = self.businessName
                                        self.heightBusinessname.constant = 0.0
                                        self.businessNametf.isHidden = true
                                    }
                                    
                                    if let userType = self.userType.text, userType.count <= 0 {
                                        self.userType.text = "Unknown"
                                    }
                                    
                                    if let merchant = self.merchantSubscriberTextfield.text, merchant.count <= 0 {
                                        self.merchantSubscriberTextfield.text = "Unregister User"
                                    }
                                    
                                    let status = dict?.safeValueForKey("TransStatus") as? String ?? "0"
                                    let limitValue = dict?.safeValueForKey("TransAmountLimit") as? String ?? "0"
                                    
                                    self.amountCheck = (status,limitValue)
                                    
                                    if (dict?["LocalTransType"] as?  String)?.uppercased() == "FUEL".uppercased() || (dict?["LocalTransType"] as?  String)?.uppercased() == "PARKING".uppercased() || (dict?["LocalTransType"] as?  String)?.uppercased() == "TOLL".uppercased() {
                                        self.callForDetectVehicle()
                                      self.heightAddvehicle.constant = 50.00
                                        self.addYourVehicle.isHidden = false
                                    }

                                    self.enterAmount.becomeFirstResponder()
                                    
                                }
                            }
                        } catch let error as NSError {
                            println_debug(error)
                        }
                    }
                }
            }
        }
        
        //MARK:- CashBack Payment
        if screen == ScreenName.cashBack {
            DispatchQueue.main.async {
                if let xmlString = data as? String {
                    let xml = SWXMLHash.parse(xmlString)
                    self.transaction.removeAll()
                    self.enumerate(indexer: xml)
                    self.processedTransaction.removeAll()
                    self.transaction["businessName"] = self.businessName
                    self.transaction["destinationName"]  = self.merchantSubscriberTextfield.text ?? ""
                    self.transaction["localRemark"] = self.remarks.text ?? ""
                    self.processedTransaction.append(self.transaction)
                    if self.transaction["resultdescription"] as? String == "Transaction Successful" {
                        DispatchQueue.main.async {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PayToTransactionConfirmationVC") as? PayToTransactionConfirmationVC
                            vc?.displayResponse = self.processedTransaction
                            vc?.varType = self.varType
                            vc?.viewModel = [self]
                            self.navigationController?.pushViewController(vc!, animated: true)
                        }
                    }
                }
            }
            println_debug(data)
        }
    }
    
    func callForDetectVehicle() {
            
        if appDelegate.checkNetworkAvail() {
            let type:Int = 1
            let mobStr:String = UserModel.shared.mobileNo
            let urlString =  "\(Url.GetAllVehicleAPI)MobileNumber=\(mobStr)&SimId=\(uuid)&MsId=\(msid)&OsType=\(type)&Otp=\(uuid)"
            
            guard let urlStr = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else { return }
            let url = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug(url)
            let params = [String: AnyObject]() as AnyObject
            TopupWeb.genericClass(url: url, param: params, httpMethod: "GET", handle: { (response, success) in
                if success {
                    let jsonData = JSON(response)
                    let categoryData = jsonData["Data"].stringValue
                    let categorydDict = OKBaseController.convertToArrDictionary(text: categoryData)
                    let categoryjsonData = JSON(categorydDict!)
                    println_debug(categoryjsonData)
                    for i in 0..<categoryjsonData.count {
                        if categoryjsonData[i]["IsDefaultCar"].boolValue {
                            DispatchQueue.main.async {
                                self.addYourVehicle.text = "\(categoryjsonData[i]["DivisionCode"].stringValue)-\(categoryjsonData[i]["CarNumber"].stringValue)-\(categoryjsonData[i]["CarName"].stringValue)"
                            }
                        }
                    }
                }
            })
        }
    }
    
    func webFailureResult(screen: String) {
        if screen == ScreenName.chkAgentCode {
            println_debug("Something went wrong" + ScreenName.chkAgentCode)
            DispatchQueue.main.sync {
                self.hideAll()
            }
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            transaction[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }

    
    //MARK:- Parameters Functions
    func wrapModel() {
        
    }

}

struct MPViewModel {
    var model = PaySendViewController()
    init(obj: PaySendViewController) {
        model = obj
    }
}
