//
//  PaySendHelperExtension.swift
//  OK
//
//  Created by Ashish Kumar Singh on 12/4/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SwiftMultiSelect

extension PaySendViewController: ScanObject_MultiPayDelegate, PTMultiSelectionDelegate, FavoriteSelectionDelegate, HideMyNumberDelegate {
    
    func navigateToMulti(obj: PTScanObject) {
        let firstVC = self.storyboard?.instantiateViewController(withIdentifier: "firstVC") as? PaySendViewController
        self.navigateWithScanObject(secondObj: firstVC!, scanObj: obj)
    }
    
    func selfUpdateScan(obj: PTScanObject) {
        self.decodeScanningObject(obj: obj)
    }
    
    func getContacts() {
        let contact = ContactPickersPicker.init(delegate: nil, multiSelection: false)
        contact.getContacts { (contacts, error) in
            DispatchQueue.global(qos: .background).async {
                contacts.forEach({ (cn) in
                    let pContcat = ContactPicker.init(contact: cn)
                    self.pContacts.append(pContcat)
                })
            }
        }
    }
    
    
    func updateViewsInitialLogic() {
        self.mobileNumber.delegate     = self
        self.confirmMobileNum.delegate = self
        self.userType.delegate         = self
        self.enterAmount.delegate      = self
        self.burAmount.delegate        = self
        self.remarks.delegate          = self
        self.hideMyNumber.delegate     = self
        self.businessNametf.delegate   = self
        self.addYourVehicle.delegate   = self
        
        self.textfieldArray.append(self.mobileNumber)
        self.textfieldArray.append(self.confirmMobileNum)
        self.textfieldArray.append(self.userType)
        self.textfieldArray.append(self.merchantSubscriberTextfield)
        self.textfieldArray.append(self.enterAmount)
        self.textfieldArray.append(self.burAmount)
        self.textfieldArray.append(self.remarks)
        self.textfieldArray.append(self.hideMyNumber)
        self.textfieldArray.append(self.businessNametf)
        self.textfieldArray.append(self.addYourVehicle)
        
        self.burAmount.isUserInteractionEnabled = false
        self.remarks.isUserInteractionEnabled = true
        
    }
    
    func updateTextfields(textfields: [UITextField],toHide flag : Bool) {
        for tf in self.textfieldArray {
            for visibleTf  in textfields {
                if tf == visibleTf {
                    visibleTf.isHidden = flag
                }
            }
        }
    }
    
    func loadFloatButtons() {
        
        if self.transactionType == .bus {
            return
        } else if self.transactionType == .toll {
            return
        } else if self.transactionType == .ferry {
            return
        } else if self.transactionType == .taxi {
            return
        }
        
        if toRemoveMultiBtn == 1 {
            self.submitBtn.isHidden = false
            SwiftMultiSelect.dataSourceType = .phone
            
            let scanPay = ActionButtonItem(title: "Scan To Pay", image: #imageLiteral(resourceName: "scan"))
            scanPay.action = { item in
                let scanController = self.storyboard?.instantiateViewController(withIdentifier: "secondVC") as? ScanToPayViewController
                scanController?.screen    = MultiSelectionType.multi
                scanController?.mDelegate = self
                self.navigationController?.pushViewController(scanController!, animated: true)
                self.multiButton?.toggleMenu()
            }
            
            let conatct = ActionButtonItem(title: "Add Pay From Contact", image: #imageLiteral(resourceName: "add_pay"))
            conatct.action = { item in
                self.multiScreen = MultiSelectionType.multi
                let nav = UitilityClass.openContact(multiSelection: false, self)
                self.present(nav, animated: true, completion: {
                    DispatchQueue.main.async {
                        self.multiButton?.toggleMenu()
                    }
                })
            }
            
            let favorite = ActionButtonItem(title: "Add Pay From Favorite", image: #imageLiteral(resourceName: "favourit"))
            favorite.action = { item in
                let vc = self.openFavoriteFromNavigation(self, withFavorite: .payto)
                self.present(vc, animated: true, completion: nil)
                self.multiButton?.toggleMenu()
            }
            
            let mobile = ActionButtonItem(title: "Add Mobile Number to Pay", image: #imageLiteral(resourceName: "payto"))
            mobile.action = { item in
                println_debug("Add Mobile Number to Pay")
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "firstVC") as? PaySendViewController
                self.navigateWithManualInsertion(secondObj: vc!)
                self.multiButton?.toggleMenu()
            }
            
//            multiButton = ActionButton(attachedToView: self.view, items: [mobile, favorite, scanPay, conatct])
//            multiButton?.action = { button in button.toggleMenu() }
//            multiButton?.setTitle("+", forState: UIControlState())
//            multiButton?.backgroundColor = UIColor.init(hex: "F3C632")
        }
    }
    
    // favorite model to work in
    func selectedFavoriteObject(obj: FavModel) {
        if toRemoveMultiBtn == 0 {
            self.decodePromotionObject(promoNumber: obj.phone, withName: obj.name)
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "firstVC") as? PaySendViewController
            self.navigateWithFavorites(secondObj: vc!, contact: obj)
        }
        
        self.toRemoveMultiBtn = 1
    }
    
    
    //MARK:- Helper Methods
    func identifyCountry(withPhoneNumber prefix: String, inCountryArray countArray: Array<NSDictionary>) -> (String, String) {
        
        let countArray = revertCountryArray() as! Array<NSDictionary>
        
        var finalCodeString = ""
        var flagCountry = ""
        
        for dic in countArray {

            if let code = dic .object(forKey: "CountryCode") as? String, let flag = dic.object(forKey: "CountryFlagCode") as? String{
                if prefix.hasPrefix(code) {
                    finalCodeString = code
                    flagCountry = flag
                }
            }
        }
        
        
        return (finalCodeString,flagCountry)
    }
    
    func revertCountryArray() -> NSArray {
        do {
            if let file = Bundle.main.url(forResource: "mcc_mnc", withExtension: "txt") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? NSDictionary {
                    if let arr = object.object(forKey: "data") as? NSArray {
                        arrCountry = arr
                        return arrCountry
                    }
                } else if let object = json as? [Any] {
                    println_debug(object)
                } else {
                    println_debug("JSON is invalid")
                }
            } else {
                println_debug("no file")
            }
        } catch {
            println_debug(error.localizedDescription)
        }
        return arrCountry
    }
    
    
    func decodeContact(contact: ContactPicker) {
        
        self.hideAll()
        dynamicHeight = ModelDesignHeight.confirmedEntered
        
        self.enterAmount.isHidden  = false
        self.hideMyNumber.isHidden = false
        self.confirmMobileNum.isHidden = false
        self.userType.isHidden = false
        self.merchantSubscriberTextfield.isHidden = self.userType.isHidden
        
        self.view.layoutIfNeeded()
        
        self.heightCons_remarks.constant   = 0.00
        self.heightCons_burAmount.constant = 0.00
        
        
        let phoneNumberCount = contact.phoneNumbers.count
        var phoneNumber = ""
        
        if phoneNumberCount == 1  {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        }
        else if phoneNumberCount > 1 {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        }
        else {
            
        }
        
        if phoneNumber != "" {
            phoneNumber = phoneNumber.replacingOccurrences(of: ")", with: "")
            phoneNumber = phoneNumber.replacingOccurrences(of: "(", with: "")
            phoneNumber = phoneNumber.trimmingCharacters(in: .whitespaces)
        }
        
        if phoneNumber.hasPrefix("00") {
            _ = phoneNumber.remove(at: String.Index.init(encodedOffset: 0))
            _ = phoneNumber.remove(at: String.Index.init(encodedOffset: 0))
            
            if phoneNumber.hasPrefix("+") {
            } else {
                phoneNumber = "+" + phoneNumber
            }
        }
        
        let countryArray = revertCountryArray() as! Array<NSDictionary>
        
        let cDetails = self.identifyCountry(withPhoneNumber: phoneNumber, inCountryArray: countryArray) // 1 -> flag, 0 -> code
        var countryCode = String.init(format: "(%@)", "+95")
        if cDetails.0 == "" || cDetails.1 == "" {
            countryObject.flag = "myanmar"
            countryObject.code = "+95"
            leftViewCountry.wrapCountryViewData(img: "myanmar", str: countryCode)
            leftViewCountryForConfirmation.wrapCountryViewData(img: "myanmar", str: countryCode)
        } else {
            countryObject.flag = cDetails.1
            countryObject.code = cDetails.0
            countryCode = String.init(format: "(%@)", cDetails.0)
            leftViewCountry.wrapCountryViewData(img: cDetails.1, str: countryCode)
            leftViewCountryForConfirmation.wrapCountryViewData(img: cDetails.1, str: countryCode)
        }
        
        
        self.mobileNumber.leftView     = nil
        self.confirmMobileNum.leftView = nil
        
        self.mobileNumber.leftView     = leftViewCountry
        self.mobileNumber.leftViewMode = UITextFieldViewMode.always
        
        self.confirmMobileNum.leftView      = leftViewCountryForConfirmation
        self.confirmMobileNum.leftViewMode  = UITextFieldViewMode.always
        
        self.view.layoutIfNeeded()
        
        var phone = ""
        
        if phoneNumber.hasPrefix("+95") || phoneNumber.hasPrefix("(+95)") {
        phone = phoneNumber.replacingOccurrences(of: cDetails.0, with: "0")
        phone = phone.replacingOccurrences(of: " ", with: "")
            
            if phone.hasPrefix("0") {
                self.mobileNumber.text = phone
            } else {
                self.mobileNumber.text = "0" + phone
            }
            
        }else {
            phone = phoneNumber.replacingOccurrences(of: cDetails.0, with: "")
            phone = phone.replacingOccurrences(of: " ", with: "")
            self.mobileNumber.text = phone
        }
       
        self.nameORnumber(selectiontype: .name, codeDetails: cDetails)
        if contact.displayName().count > 0 {
            self.confirmMobileNum.text = contact.displayName()
        } else {
            self.confirmMobileNum.text = "+95"
        }

        countryCode = String.init(format: "(%@)", countryObject.code)
        let agentCode = getDestinationNum(self.mobileNumber.text!, withCountryCode: countryCode)
        let code = agentCode.trimmingCharacters(in: .whitespacesAndNewlines)
        checkAgentCode(agentCode: code)
    }
    
    func decodeFavoriteContact(contact: FavModel) {
        self.decodePromotionObject(promoNumber: contact.phone, withName: contact.name)
    }
    
    func decodePromotionObject(promoNumber: String, withName name: String) {
        let object = PTScanObject.init(amt: "", nme: name, agent: promoNumber)
        self.decodeScanningObject(obj: object)
    }
    
    func decodeScanningObject(obj: PTScanObject) {
        
        self.hideAll()
        dynamicHeight = ModelDesignHeight.confirmedEntered
        
        self.enterAmount.isHidden  = false
        self.hideMyNumber.isHidden = false
        self.confirmMobileNum.isHidden = false
        self.userType.isHidden = false
        self.merchantSubscriberTextfield.isHidden = self.userType.isHidden
        
        self.view.layoutIfNeeded()
        
        self.heightCons_remarks.constant   = 0.00
        self.heightCons_burAmount.constant = 0.00
        
        
        var reframedCode = obj.agentNo
        if reframedCode.hasPrefix("00") {
            reframedCode.remove(at: String.Index.init(encodedOffset: 0))
            reframedCode.remove(at: String.Index.init(encodedOffset: 0))
            reframedCode.insert("+", at: String.Index.init(encodedOffset: 0))
        }
        
        let countryArray = revertCountryArray() as! Array<NSDictionary>
        
        let cDetails = self.identifyCountry(withPhoneNumber: reframedCode, inCountryArray: countryArray) // 1 -> flag, 0 -> code
        
        var countryCode = String.init(format: "(%@)", "+95")
        if cDetails.0 == "" || cDetails.1 == "" {
            countryObject.flag = "myanmar"
            countryObject.code = "+95"
            leftViewCountry.wrapCountryViewData(img: "myanmar", str: countryCode)
            leftViewCountryForConfirmation.wrapCountryViewData(img: "myanmar", str: countryCode)
        } else {
            countryObject.flag = cDetails.1
            countryObject.code = cDetails.0
            countryCode = String.init(format: "(%@)", cDetails.0)
            leftViewCountry.wrapCountryViewData(img: cDetails.1, str: countryCode)
            leftViewCountryForConfirmation.wrapCountryViewData(img: cDetails.1, str: countryCode)
        }
        
        reframedCode = reframedCode.replacingOccurrences(of: cDetails.0, with: "0")
        
        self.mobileNumber.text     = reframedCode
        
        if obj.name.count > 0 {
            self.confirmMobileNum.text = obj.name
        } else {
            self.confirmMobileNum.text = "Unknown"
        }

        if obj.amount != "" {
            
            dynamicHeight = ModelDesignHeight.amountEntered
            
            if (UserLogin.shared.walletBal as NSString).floatValue >= (obj.amount as NSString).floatValue {
                
                if (obj.amount as NSString).floatValue >= 0.1 {
                    self.enterAmount.text = obj.amount
                    self.burAmount.isHidden = false
                    self.remarks.isHidden = false
                    
                    self.heightCons_remarks.constant   = 50.00
                    self.heightCons_burAmount.constant = 50.00
                    self.view.layoutIfNeeded()
                    
                    self.enterAmount.becomeFirstResponder()
                    show_multi_submit()

                } else {
                    self.enterAmount.text = ""
                    self.enterAmount.becomeFirstResponder()
                    self.burAmount.isHidden = true
                    self.remarks.isHidden = true
                    self.heightCons_remarks.constant   = 0.00
                    self.heightCons_burAmount.constant = 0.00
                }
                
            } else {
                self.showErrorAlert(errMessage: "Insufficient amount in your OK$ Account".localized)
                self.enterAmount.becomeFirstResponder()
                self.burAmount.isHidden = true
                self.remarks.isHidden = true
                self.heightCons_remarks.constant   = 0.00
                self.heightCons_burAmount.constant = 0.00
            }
            
        } else {
            self.burAmount.isHidden = true
            self.remarks.isHidden = true
        }
        
        if obj.agentNo.hasPrefix("00") {
            let formattedNumber = obj.agentNo
            _ = formattedNumber.dropFirst()
            _ = formattedNumber.dropFirst()
            let newNumber = "+" + formattedNumber
            let countryArray = revertCountryArray() as! Array<NSDictionary>
            let cDetails = self.identifyCountry(withPhoneNumber: newNumber, inCountryArray: countryArray) // 1 -> flag, 0 -> code
            //            self.mobileNumber.text = "0"+formattedNumber
            self.nameORnumber(selectiontype: .name, codeDetails: cDetails)
        }
        
        checkAgentCode(agentCode: obj.agentNo)
        
    }
    
    func nameORnumber(selectiontype: selectionType, codeDetails: (String, String)) {
        self.view.endEditing(true)
        switch selectiontype {
        case .name:
            self.confirmMobileNum.isUserInteractionEnabled = false
            self.confirmMobileNum.placeholder = "Name"
            self.confirmMobileNum.isEnabled = false
            self.confirmMobileNum.leftView = nil
            let confirmDets = DefaultIconView.updateView(icon: "name_bill")
            self.confirmMobileNum.leftView      = confirmDets
            self.confirmMobileNum.leftViewMode  = UITextFieldViewMode.always
            
        case .number:
            self.confirmMobileNum.isUserInteractionEnabled = true
            self.confirmMobileNum.placeholder = "Confirm Mobile Number"
            self.confirmMobileNum.isEnabled = true
            self.confirmMobileNum.leftView = nil
            let countryCode = String.init(format: "(%@)", codeDetails.0)
            leftViewCountryForConfirmation.wrapCountryViewData(img: codeDetails.1, str: countryCode)
            self.confirmMobileNum.leftView = leftViewCountryForConfirmation
            self.confirmMobileNum.leftViewMode  = UITextFieldViewMode.always
        }
    }
    
    func wrapMobileLeftView() {
        
        leftViewCountry.delegate = self

        let countryCode = String.init(format: "(%@)", "+95")
        leftViewCountry.wrapCountryViewData(img: "myanmar", str: countryCode)
        leftViewCountryForConfirmation.wrapCountryViewData(img: "myanmar", str: countryCode)
        
        self.mobileNumber.leftView     = leftViewCountry
        self.mobileNumber.leftViewMode = UITextFieldViewMode.always
        
        self.confirmMobileNum.leftView      = leftViewCountryForConfirmation
        self.confirmMobileNum.leftViewMode  = UITextFieldViewMode.always
        
        contactRightView.delegate = self
        self.mobileNumber.rightView     = contactRightView
        self.mobileNumber.rightViewMode = UITextFieldViewMode.always
        
        self.userType.leftView      = typeLeftView
        self.userType.leftViewMode  = UITextFieldViewMode.always
        
        self.merchantSubscriberTextfield.leftView      = merchantLeftView
        self.merchantSubscriberTextfield.leftViewMode  = UITextFieldViewMode.always
        
        self.burAmount.leftView      = burAmountLeftView
        self.burAmount.leftViewMode  = UITextFieldViewMode.always
        
        self.enterAmount.leftView      = amountLeftView
        self.enterAmount.leftViewMode  = UITextFieldViewMode.always
        
        self.remarks.leftView      = remarkLeftView
        self.remarks.leftViewMode  = UITextFieldViewMode.always
        
        self.hideMyNumber.leftView      = hideLeftView
        self.hideMyNumber.leftViewMode  = UITextFieldViewMode.always
        
        self.addYourVehicle.leftViewMode = .always
        self.addYourVehicle.leftView = addVehicleView
        
        self.businessNametf.leftViewMode = .always
        self.businessNametf.leftView = businessLeftView
    }
    
    func hideMyNumberDelegate(hide: Bool) {
        self.transactionType = hide ? .paytoWithoutID : .paytoWithID
    }
    
    func hideAll() {
        for tf in self.textfieldArray {
            tf.isHidden = true
        }
        self.mobileNumber.isHidden = false
        self.submitBtn.isHidden = true
    }
    
    func clearAll() {
        for tf in self.textfieldArray {
            if tf != self.mobileNumber {
                tf.text?.removeAll()
            }
            
        }
    }
    
    func showTextfield(textfield: UITextField) {
        textfield.isHidden = true
    }
    
    func hideTextfield(textfield: UITextField) {
        textfield.isHidden = false
    }
    
    //MARK:- open Country VC
    
    func clearActionType() {
        self.hideAll()
        self.mobileNumber.text = "09"
        self.confirmMobileNum.isUserInteractionEnabled = true
        self.confirmMobileNum.placeholder = "Confirm Mobile Number"
        self.confirmMobileNum.isEnabled = true
        self.confirmMobileNum.leftView = nil
        let countryCode = String.init(format: "(%@)", countryObject.code)
        leftViewCountryForConfirmation.wrapCountryViewData(img: countryObject.flag , str: countryCode)
        self.confirmMobileNum.leftView = leftViewCountryForConfirmation
        self.confirmMobileNum.leftViewMode  = UITextFieldViewMode.always
        self.dynamicHeight = ModelDesignHeight.justShown
        self.mobileNumber.becomeFirstResponder()
        self.contactSuggestionView(toHide: true)
    }
    
    func openAction(type: ButtonType) {
        self.contactSuggestionView(toHide: true)
        self.view.endEditing(true)
        switch type {
            case .contactView:
                self.toRemoveMultiBtn = 0
                self.openMultiContactSelection()
            case .countryView:
                self.navigationController?.present(, animated: true, completion: nil)
            }
        } 
    
    private func openMultiContactSelection() {
//        self.toRemoveMultiBtn = 1
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "PayToMultiSelection") as? PayToMultiSelection {
            vc.delegate = self
            if let keyWindow = UIApplication.shared.keyWindow {
                vc.view.frame = keyWindow.bounds
                vc.view.tag = 4354
                UIView.transition(with: keyWindow, duration: 1.5, options: UIViewAnimationOptions.curveEaseIn,
                                  animations:
                    {keyWindow.addSubview(vc.view)}
                    , completion: nil)
                keyWindow.makeKeyAndVisible()
            }
        }
    }
    
    func didSelectOption(option: PTMultiSelectionOption) {
        switch option {
        case .contact   :
            let nav = UitilityClass.openContact(multiSelection: false, self)
            self.navigationController?.present(nav, animated: true, completion: nil)
            self.toRemoveMultiBtn = 1
            break
        case .favorite :
            let nav = self.openFavoriteFromNavigation(self, withFavorite: .payto)
            self.navigationController?.present(nav, animated: true, completion: nil)
            break
        case .scanQR :
            let scanController = self.storyboard?.instantiateViewController(withIdentifier: "secondVC") as? ScanToPayViewController
            scanController?.screen    = MultiSelectionType.selfUpdate
            scanController?.mDelegate = self
            self.navigationController?.pushViewController(scanController!, animated: true)
            break
        }
    }
    
    
    
    func layouting() {
        for view in self.view.subviews {
            view.layoutIfNeeded()
        }
    }
    
    //MARK:- Favorite Selection
    
    
    //MARK:- Country Selection Delegates
    func countryViewController(_ list: CountryViewController, country: Country) {
        
        self.hideForMultiPayment()
        
        self.mobileNumber.text = ""
        self.confirmMobileNum.text = ""
        
        countryObject.code = country.dialCode
        countryObject.flag = country.code
        
        let countryCode = String.init(format: "(%@)", country.dialCode)
        leftViewCountry.wrapCountryViewData(img: country.code, str: countryCode)
        leftViewCountryForConfirmation.wrapCountryViewData(img: country.code, str: countryCode)
        
        self.mobileNumber.leftView = nil
        self.confirmMobileNum.leftView = nil
        
        self.mobileNumber.leftView     = leftViewCountry
        self.mobileNumber.leftViewMode = UITextFieldViewMode.always
        
        self.confirmMobileNum.leftView      = leftViewCountryForConfirmation
        self.confirmMobileNum.leftViewMode  = UITextFieldViewMode.always
        
        self.nameORnumber(selectiontype: .number, codeDetails: (country.dialCode,country.code))
        
        self.hideAll()
        
        self.view.layoutIfNeeded()
        list.dismiss(animated: true, completion: nil)
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: true, completion: nil)
    }
}
