//
//  NavigationToMultiPayment.swift
//  OK
//
//  Created by Ashish on 12/19/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Contacts

extension PaySendViewController {
    
    //MARK:- Navigation through Contact
    func navigateWithFavorites(secondObj: PaySendViewController, contact: FavModel) {
        
    }

    
    //MARK:- Navigation through Contact
    func navigateWithContact(secondObj: PaySendViewController, contact: ContactPicker) {
        
    }
    
    //MARK:- Navigation Manually to insert number
    
    func navigateWithManualInsertion(secondObj: PaySendViewController) {
        
    }
 
    func navigateWithScanObject(secondObj: PaySendViewController, scanObj: PTScanObject) {
    }
    
    
    func backNavigationAction() {
        // while coming from more payment view controller
        if UserDefaults.standard.bool(forKey: "popPayTo") {
            self.multiButton = nil
            self.dismiss(animated: true, completion: nil)
            UserDefaults.standard.set(false, forKey: "popPayTo")
        }
        
        if UserDefaults.standard.bool(forKey: "morePaymentPayto") {
            self.hideAll()
            self.nameORnumber(selectiontype: .number, codeDetails: ("+95","myanmar"))
            self.clearAll()
            self.mobileNumber.text?.removeAll()
            UserDefaults.standard.set(false, forKey: "morePaymentPayto")
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.submitBtn.isHidden = true
        }
        
        if UserDefaults.standard.bool(forKey: "repeatPaymentPayto") {
            self.hideAll()
            self.nameORnumber(selectiontype: .number, codeDetails: ("+95","myanmar"))
            self.clearAll()
            self.mobileNumber.text?.removeAll()
            
            var mobileNumber = UserDefaults.standard.string(forKey: "stringPaymentPayto")
            
            mobileNumber?.remove(at: String.Index.init(encodedOffset: 0))
            mobileNumber?.remove(at: String.Index.init(encodedOffset: 0))
            mobileNumber?.insert("+", at: String.Index.init(encodedOffset: 0))
            
            let countryArray = revertCountryArray() as! Array<NSDictionary>
            let cDetails = self.identifyCountry(withPhoneNumber: mobileNumber!, inCountryArray: countryArray) // 1 -> flag, 0 -> code
            
            if cDetails.0 == "" || cDetails.1 == "" {
                countryObject.flag = "myanmar"
                countryObject.code = "+95"
                let countryCode = String.init(format: "(%@)", "+95")
                leftViewCountry.wrapCountryViewData(img: "myanmar", str: countryCode)
                leftViewCountryForConfirmation.wrapCountryViewData(img: "myanmar", str: countryCode)
            } else {
                countryObject.flag = cDetails.1
                countryObject.code = cDetails.0
                let countryCode = String.init(format: "(%@)", cDetails.0)
                leftViewCountry.wrapCountryViewData(img: cDetails.1, str: countryCode)
                leftViewCountryForConfirmation.wrapCountryViewData(img: cDetails.1, str: countryCode)
            }
            
            mobileNumber =  mobileNumber?.replacingOccurrences(of: cDetails.0, with: "0")
            dynamicHeight = ModelDesignHeight.mobileNumberEntered
            self.confirmMobileNum.isHidden = false
            self.userType.isHidden = false
            self.merchantSubscriberTextfield.isHidden = false
            self.enterAmount.isHidden = false
            self.mobileNumber.text = mobileNumber
            self.confirmMobileNum.text = mobileNumber
            
            if let code = UserDefaults.standard.string(forKey: "stringPaymentPayto") {
                self.checkAgentCode(agentCode: code)
            } else {
                self.hideAll()
                self.clearAll()
                self.mobileNumber.text?.removeAll()
            }
            UserDefaults.standard.set(false, forKey: "repeatPaymentPayto")
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.submitBtn.isHidden = true
        }

    }

}










