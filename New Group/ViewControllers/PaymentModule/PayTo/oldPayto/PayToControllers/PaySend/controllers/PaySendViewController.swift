//
//  PaySendViewController.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/27/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PaySendViewController: PayToBaseViewController, PTIndicatorInfoProvider {
    
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    //MARK:- Extra Parent Functions
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PTPagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo.init(title: "PAYTO".localized)
    }

}
