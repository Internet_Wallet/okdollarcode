//
//  PaytoViews.swift
//  OK
//
//  Created by Ashish Kumar Singh on 11/10/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

 enum ButtonType {
    case countryView
    case contactView
}

protocol PTClearButtonDelegate : class {
    func clearField(strTag: String)
}

 protocol CountryLeftViewDelegate : class {
     func openAction(type: ButtonType)
     func clearActionType()
}

protocol HideMyNumberDelegate : class {
    func hideMyNumberDelegate(hide: Bool) -> Void
}

class PaytoViews: UIView {
    
    @IBOutlet var countryImage: UIImageView!
    @IBOutlet var countryLabel: UILabel!
    
   weak var delegate : CountryLeftViewDelegate?
    
    class func updateView() -> PaytoViews {
        let nib   =  UINib(nibName: "PayToLeftView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as! PaytoViews
        nib.frame = .init(x: 0.00, y: 0.00, width: 105.00, height: 40.00)
        nib.layoutUpdates()
        return nib
    }
    
    func layoutUpdates() {
        self.layoutIfNeeded()
    }
    
    func wrapCountryViewData(img: String, str: String) {
        self.countryImage.image = UIImage.init(named: img)
        self.countryLabel.text  = str
        
        let size   = self.countryLabel.text?.size(withAttributes: [.font: self.countryLabel.font]) ?? .zero
        let vWidth = 50 + size.width
        self.frame = .init(x: 0, y: 0, width: vWidth, height: 40.00)
        self.layoutIfNeeded()
    }
    
    @IBAction func countryLeftViewAction(_ sender: UIButton) {
        if let delegate = delegate {
            delegate.openAction(type: ButtonType.countryView)
        }
    }
    
    deinit {
        self.delegate = nil
    }
}

class ContactRightView : UIView {
    
   weak var delegate : CountryLeftViewDelegate?

    class func updateView() -> ContactRightView {
        let nib   =  UINib(nibName: "PayToLeftView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[1] as! ContactRightView
        nib.frame = .init(x: 0.00, y: 0.00, width: 80.00, height: 50.00)
        nib.layoutUpdates()
        return nib
    }
    
    func layoutUpdates() {
        self.layoutIfNeeded()
    }
    
    @IBAction func clearMobileNumberAction(_ sender: UIButton) {
        if let delegate = self.delegate  {
            delegate.clearActionType()
        }
    }
    
    @IBAction func countryLeftViewAction(_ sender: UIButton) {
        if let delegate = delegate {
            delegate.openAction(type: ButtonType.contactView)
        }
    }
    
    deinit {
        self.delegate = nil
    }

}

class HideMyView : UIView {
    @IBOutlet weak var imageCheckBox: UIImageView!
    @IBOutlet weak var mobileNumber: UILabel!
    @IBOutlet weak var hideMyNumber: UILabel!
        {
        didSet
        {
            hideMyNumber.text = "Hide My Number".localized
        }
    }
    weak var delegate : HideMyNumberDelegate?
    
    let hiddenString = "XXXXXXXXXX"
    
    class func updateView(_ fr: CGRect, delegate: HideMyNumberDelegate) -> HideMyView {
        let nib   =  UINib(nibName: "PayToLeftView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[3] as! HideMyView
        nib.frame = fr
        nib.delegate = delegate
        nib.layoutUpdates()
        return nib
    }
    @IBAction func HideMyNumberAction(_ sender: UIButton) {
        if let delegate = self.delegate {
            
            UIView.animate(withDuration: 0.2, animations: {
                self.imageCheckBox.isHighlighted = !self.imageCheckBox.isHighlighted
                
                self.mobileNumber.text = (!self.imageCheckBox.isHighlighted) ? UserModel.shared.formattedNumber : self.hiddenString
                
                delegate.hideMyNumberDelegate(hide: self.imageCheckBox.isHighlighted)
            })
            
        } else {
            
            self.imageCheckBox.isHighlighted = false
            
            self.mobileNumber.text = UserModel.shared.formattedNumber
        }
    }
    
    func fireDelegate() {
        self.imageCheckBox.isHighlighted = false
        
        self.mobileNumber.text = (!self.imageCheckBox.isHighlighted) ? UserModel.shared.formattedNumber : self.hiddenString
        
        delegate?.hideMyNumberDelegate(hide: self.imageCheckBox.isHighlighted)
    }
    
    func layoutUpdates() {
        self.mobileNumber.text = UserModel.shared.formattedNumber
        self.layoutIfNeeded()
    }
    
}

class TimerView : UIView {
    
    @IBOutlet weak var timerLaber: UILabel!
    @IBOutlet weak var circularView: CardDesignView!
    @IBOutlet weak var timerHeight: NSLayoutConstraint!
    @IBOutlet weak var timerWidth: NSLayoutConstraint!
    @IBOutlet weak var secondsLeftString: UILabel!
    
    class func updateView() -> TimerView {
        let nib = UINib.init(nibName: "PayToLeftView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[5]  as! TimerView
        nib.frame = .init(x: screenWidth/2 - 45.00, y: screenHeight - 180.00, width: 90.00, height: 90.00)
        nib.layoutUpdates()
        return nib
    }
    
    func layoutUpdates() {
        self.layoutIfNeeded()
    }
    
    func wrapTimerView(timer: String) {
        self.timerLaber.text = timer
    }
    
    func changeFrameForMulti() {
        
        self.circularView.cornerRadius = 20.00
        self.timerHeight.constant = 40
        self.timerWidth.constant = 40
        
        self.frame = CGRect.init(x: screenWidth - 60 - 8, y: 20.00, width: 60.00, height: 60.00)
        
        guard let secfont = UIFont(name: "Zawgyi-One", size: 15.00) else { return }
        guard let timerfont = UIFont(name: "Zawgyi-One", size: 12.00) else { return }
        self.secondsLeftString.textColor = .white
        self.secondsLeftString.text = "".localized
        
        self.secondsLeftString.font = secfont
        self.timerLaber.font = timerfont
        
        self.layoutIfNeeded()
        
    }
    
    func wrapTimerViewUpdate(color: UIColor = UIColor.black, alpha: CGFloat = 1.00) {
        self.timerLaber.textColor = color
        self.timerLaber.alpha = alpha
    }
    
    func wrapOrigin(point: CGPoint, size: CGSize) {
        self.frame.origin = point
        self.frame.size   = size
        self.layoutUpdates()
    }
}

class PTClearButton : UIView {
    
    weak var delegate : PTClearButtonDelegate?
    
    var stringTag : String?
    
    class func updateView(strTag: String, delegate: PTClearButtonDelegate) -> PTClearButton {
        let nib   =  UINib(nibName: "PayToLeftView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[4] as! PTClearButton
            nib.frame = .init(x: 0.00, y: 0.00, width: 50.00, height: 40.00)
        nib.stringTag = strTag
        nib.delegate  = delegate
        nib.layoutUpdates()
        return nib
    }
    
    func layoutUpdates() {
        self.layoutIfNeeded()
    }
    
    @IBAction func clearButtonAction(_ sender: UIButton) {
        if let delegate = self.delegate, let str = stringTag {
            delegate.clearField(strTag: str)
        }
    }

}

class DefaultIconView : UIView {
    
    @IBOutlet var iconImageView: UIImageView!

    class func updateView(icon: String) -> DefaultIconView {
        let nib   =  UINib(nibName: "PayToLeftView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[2] as! DefaultIconView
        if icon == "pt_categories" {
            nib.frame = .init(x: 0.00, y: 0.00, width: 43.00, height: 40.00)
        } else {
            nib.frame = .init(x: 0.00, y: 0.00, width: 50.00, height: 40.00)
        }
        nib.layoutUpdates(img: icon)
        return nib
    }
    
    func layoutUpdates(img: String) {
        self.iconImageView.image = UIImage(named: img)
        self.layoutIfNeeded()
    }

}
