//
//  PulseManager.swift
//  OK
//
//  Created by Ashish on 6/26/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PulseManager: UIView {
    
    @IBOutlet weak var distanceLabel: UILabel!
    
    class func updateView() -> PulseManager {
        let nib   =  UINib(nibName: "PulseManager", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as? PulseManager
        nib?.frame = .init(x: 0.00, y: 0.00, width: 200.00, height: 200.00)
        nib?.layoutUpdates()
        return nib!
    }

    func startPulsing(distance: Float) {
        
        var distanceInMetrics = ""
        
        if distance < 3280 {
            distanceInMetrics = String.init(format: "%.2f m", distance / 3.2808)
        } else {
            distanceInMetrics = String.init(format: "%.2f Km", distance / 3280.84)
        }
        
        let pulse = Pulsator()
        pulse.numPulse = 3
        pulse.backgroundColor = kYellowColor.cgColor
        pulse.radius = 150.00
        self.distanceLabel.text = distanceInMetrics
        self.distanceLabel.layer.addSublayer(pulse)
    }
    
    func removePulsing() {
        self.removeFromSuperview()
    }
    
    func layoutUpdates() {
        self.layoutIfNeeded()
    }
    
}
