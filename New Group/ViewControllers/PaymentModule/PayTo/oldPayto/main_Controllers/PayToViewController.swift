//
//  PayToViewController.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/25/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol ScrollChangeDelegate : class {
    func changeScrollPage(point: Int, withScanObject object: PTScanObject)
    func scanningFailed()
}

class PayToViewController: PTScroller, UIPopoverPresentationControllerDelegate, ScrollChangeDelegate {
    
    let blueInstagramColor = UIColor(red: 37/255.0, green: 111/255.0, blue: 206/255.0, alpha: 1.0)
    
    var paytoController : PaySendViewController?
    
    var scanObjectFromTabbarScan : PTScanObject?
    
    var favoriteObject : FavModel?
    
    var isTransportation : String?

    var promotionMobileNumber : (number: String, name: String)?
    
    var titlesRightHeader : [String] = [String]()
    
    var transactionType : PaytoIDTransactionType = PaytoIDTransactionType.paytoWithID
    
    let menu = DropDown()
    
    var headerTitle : String?
    
    var resType = "PAYTO"
    
    override func viewDidLoad() {

        super.viewDidLoad()
        
        self.restorationIdentifier = "done"
        
        self.helpSupportNavigationEnum = .Pay_Send
        //self.title = "Pay / Send".localized//headerTitle?.localized ?? "Pay / Send".localized
        self.title = appDelegate.getlocaLizationLanguage(key: "Pay / Send")
         self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedStringKey.font: UIFont(name: "Zawgyi-One", size: 17.00) ?? UIFont.systemFont(ofSize: 17.00), NSAttributedStringKey.foregroundColor: UIColor.white]
        
        let buttonIcon      = UIImage(named: "tabBarBack")
        let leftBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.done, target: self, action: #selector(PayToViewController.dismissTheViewControllerPT(_:)))
        leftBarButton.image = buttonIcon
        self.navigationItem.leftBarButtonItem = leftBarButton
        self.statusBarHeightChanged()
    }
    
    @objc func dismissTheViewControllerPT(_ sender:UIBarButtonItem!) {
        paytoController?.multiButton?.items = nil
        paytoController?.multiButton = nil
        paytoController = nil
        self.scanObjectFromTabbarScan = nil

        if let navigation = self.navigationController {
            navigation.dismiss(animated: true, completion: nil)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadSettings()
         adaptiveLeftViewButtonSetup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    @objc func statusBarHeightChanged() {
        
//        let statusBarHeight = UIApplication.shared.statusBarFrame.height
//        if statusBarHeight > 20.0 {
//            let yAxis = 20 - statusBarHeight
//            self.view.frame = CGRect.init(x: 0, y: yAxis, width: self.view.frame.width, height: self.view.frame.height - yAxis)
//        } else {
//            self.view.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
//        }
    }

    func loadSettings() {
        
        settings.style.buttonBarBackgroundColor = .red
        settings.style.buttonBarItemBackgroundColor = .white
        settings.style.selectedBarBackgroundColor = UIColor.orange
        settings.style.buttonBarItemFont = UIFont(name: appFont, size: 14.0)
        settings.style.selectedBarHeight = 2.0
        settings.style.selectedBarBackgroundColor = kYellowColor

        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .black
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0

        changeCurrentIndexProgressive = {  [weak self](oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool,_ index:Int) -> Void in
            guard changeCurrentIndex == true else { return }

            oldCell?.label.textColor = .black
            newCell?.label.textColor = UIColor.orange
            oldCell?.contentView.backgroundColor = .white
            
            switch index {
            case 0:
                self?.titlesRightHeader = ["Add Money".localized]
                println_debug("case zero")
            case 1:
                self?.titlesRightHeader = ["Add Money".localized]
                println_debug("case one")
            case 2:
                self?.titlesRightHeader = ["Add Money".localized]
                println_debug("case two")
            case 3:
                self?.titlesRightHeader = ["Add Money".localized]//["All", "Pending Approval", "Request Approved", "Cancelled"]
                println_debug("case three")
            case 4:
                self?.titlesRightHeader = ["Add Money".localized]//["Pending", "Successful", "Reject", "Scheduled"]
                println_debug("case four")
            default:
                self?.titlesRightHeader = ["Add Money".localized]
                println_debug("default")
            }

        }
        self.view.layoutIfNeeded()
    }
    
    override func dismissControllerWhenTap() {
        super.dismissControllerWhenTap()
    }
    
    
    override func viewControllersForPagerTabStrip(pagerTabStripController: PTPagerTabStripViewController) -> [UIViewController] {
       
        let first  = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "firstVC") as? PaySendViewController
        first?.view.frame = CGRect.zero
        first?.transactionType = self.transactionType
        paytoController = first
        
        let second = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "secondVC") as? ScanToPayViewController
        second?.delegate = self
        
       let third  = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "thirdVC") as? MapPayToViewController
        third?.navigation = self.navigationController
//        let fourth = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "fourthVC") as? PaymentRequestViewController
        
        let fourth = UIStoryboard(name: "PendingRequestSB", bundle: nil).instantiateViewController(withIdentifier: "pending_approval_request_result") as! PendingApprovalRequestVC
        fourth.selectedItemIndex = 4

//        fourth.indicator = "Recieved Payment Request"

//        let fifth  = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "fourthVC") as? PaymentRequestViewController
//        fifth?.indicator = "Sent Payment Request"
        
        let fifth = UIStoryboard(name: "PendingRequestSB", bundle: nil).instantiateViewController(withIdentifier: "pending_request_result") as! PendingRequestVC
        fifth.selectedItemIndex = 1

        // scan object from tab bar scanner
        if let scanObj = scanObjectFromTabbarScan {
            first?.decodeScanningObject(obj: scanObj)
        } else if let promo = promotionMobileNumber, let payto = paytoController {
            let scObj = PTScanObject.init(amt: "", nme: promo.name, agent: promo.number)
            payto.decodeScanningObject(obj: scObj)
            
        } else if let fav = self.favoriteObject, let payto = paytoController {
            let scObj = PTScanObject.init(amt: "", nme: fav.name, agent: fav.phone)
            payto.decodeScanningObject(obj: scObj)
        }
        
        
        if let _ = headerTitle {
            first?.varType = resType
            return [first!,second!, third!]
        } else {
            return [first!,second!, third!,fourth,fifth]
        }
        
    }
    
    //MARK:- IBAction
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func adaptiveLeftViewButtonSetup() -> Void {
        
        let sideMenuButton = UIButton(frame: .init(x: 0, y: 0, width: 25, height: 25))
        sideMenuButton.setBackgroundImage(#imageLiteral(resourceName: "menu_white_payto"), for: .normal)
        let rightBarButton = UIBarButtonItem.init(customView: sideMenuButton)
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        menu.anchorView = rightBarButton
        menu.dataSource = self.titlesRightHeader
        DropDown.setupDefaultAppearance()
        menu.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
        menu.customCellConfiguration = nil

        
        sideMenuButton.addTargetClosure { [weak self](sender) in
            DispatchQueue.main.async {
                guard let weakSelf = self else { return }
                weakSelf.menu.dataSource = weakSelf.titlesRightHeader
                weakSelf.menu.show()
            }
        }
        
        menu.selectionAction = { [weak self] (index: Int, item: String) in
                if let addWithdrawView = UIStoryboard(name: "AddWithdrawMain", bundle: nil).instantiateViewController(withIdentifier: "AddWithdrawView_ID") as? AddWithdrawViewController {
                    let aObjNav = UINavigationController(rootViewController: addWithdrawView)
                    self?.navigationController?.present(aObjNav, animated: true, completion: nil)
                }
        }
    }
    
    func changeScrollPage(point: Int, withScanObject object: PTScanObject) {
        DispatchQueue.main.async {
            self.moveToViewControllerAtIndex(index: point)
        }
        
        if let payto = paytoController {
            payto.decodeScanningObject(obj: object)
        }
        
    }
    
    func scanningFailed() {
        println_debug("scanning failed for payto")
    }


    
    //MARK:- DEINIT
    deinit {
        println_debug("called payto deinit")
    }
}

