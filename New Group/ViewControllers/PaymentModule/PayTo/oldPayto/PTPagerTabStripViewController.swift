
//
//  IndicatorInfoProvider.swift
//  OK
//
//  Created by palnar on 29/11/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//
import Foundation
import UIKit


// MARK: Protocols

 protocol PTIndicatorInfoProvider {
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PTPagerTabStripViewController) -> IndicatorInfo
}

 protocol PTPagerTabStripDelegate: class {
    
    func pagerTabStripViewController(pagerTabStripViewController: PTPagerTabStripViewController, updateIndicatorFromIndex fromIndex: Int, toIndex: Int)
}

 protocol PTPagerTabStripIsProgressiveDelegate : PTPagerTabStripDelegate {

    func pagerTabStripViewController(pagerTabStripViewController: PTPagerTabStripViewController, updateIndicatorFromIndex fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool)
}

 protocol PTPagerTabStripDataSource: class {
    
    func viewControllersForPagerTabStrip(pagerTabStripController: PTPagerTabStripViewController) -> [UIViewController]
}


//MARK: PagerTabStripViewController

 class PTPagerTabStripViewController: OKBaseController, UIScrollViewDelegate {
    
    @IBOutlet lazy public var containerView: UIScrollView! = { [unowned self] in
        let containerView = UIScrollView(frame: CGRect(origin : CGPoint(x:0, y:0), size:CGSize(width:self.view.bounds.width, height:self.view.bounds.height)))
        containerView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return containerView
    }()
    
    public weak var delegate: PTPagerTabStripDelegate?
    public weak var datasource: PTPagerTabStripDataSource?
    
    public var pagerBehaviour = PagerTabStripBehaviour.Progressive(skipIntermediateViewControllers: true, elasticIndicatorLimit: true)
    
    public private(set) var viewControllers = [UIViewController]()
    public private(set) var currentIndex = 0
    
    public var pageWidth: CGFloat {
        return containerView.bounds.width
    }
    
    public var scrollPercentage: CGFloat {
        if swipeDirection != .Right {
            let module = fmod(containerView.contentOffset.x, pageWidth)
            return module == 0.0 ? 1.0 : module / pageWidth
        }
        return 1 - fmod(containerView.contentOffset.x >= 0 ? containerView.contentOffset.x : pageWidth + containerView.contentOffset.x, pageWidth) / pageWidth
    }
    
    public var swipeDirection: SwipeDirection {
        if containerView.contentOffset.x > lastContentOffset {
            return .Left
        }
        else if containerView.contentOffset.x < lastContentOffset {
            return .Right
        }
        return .None
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        if containerView.superview == nil {
            view.addSubview(containerView)
        }
        containerView.bounces = true
        containerView.alwaysBounceHorizontal = true
        containerView.alwaysBounceVertical = false
        containerView.scrollsToTop = false
        containerView.delegate = self
        containerView.showsVerticalScrollIndicator = false
        containerView.showsHorizontalScrollIndicator = false
        containerView.isPagingEnabled = true
        reloadViewControllers()
        
        isViewAppearing = true
        lastSize = containerView.bounds.size
        updateIfNeeded()
        isViewAppearing = false

        
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override public func viewDidLayoutSubviews(){
        super.viewDidLayoutSubviews()
        updateIfNeeded()
    }
    
    public func moveToViewControllerAtIndex(index: Int, animated: Bool = true) {
        guard isViewLoaded && view.window != nil else {
            currentIndex = index
            return
        }
        if animated && pagerBehaviour.skipIntermediateViewControllers && abs(currentIndex - index) > 1 {
            var tmpViewControllers = viewControllers
            let currentChildVC = viewControllers[currentIndex]
            let fromIndex = currentIndex < index ? index - 1 : index + 1
            let fromChildVC = viewControllers[fromIndex]
            tmpViewControllers[currentIndex] = fromChildVC
            tmpViewControllers[fromIndex] = currentChildVC
            pagerTabStripChildViewControllersForScrolling = tmpViewControllers
            containerView.setContentOffset(CGPoint(x: pageOffsetForChildIndex(index: fromIndex),y: 0), animated: false)
            (navigationController?.view ?? view)?.isUserInteractionEnabled = false
            containerView.setContentOffset(CGPoint( x: pageOffsetForChildIndex(index: index), y: 0), animated: true)
        }
        else {
            (navigationController?.view ?? view).isUserInteractionEnabled = false
            containerView.setContentOffset(CGPoint( x: pageOffsetForChildIndex(index: index), y: 0), animated: animated)
        }
    }
    
    public func moveToViewController(viewController: UIViewController, animated: Bool = true) {
        moveToViewControllerAtIndex(index: viewControllers.index(of: viewController)!, animated: animated)
    }
    
    //MARK: - PagerTabStripDataSource
    
    public func viewControllersForPagerTabStrip(pagerTabStripController: PTPagerTabStripViewController) -> [UIViewController] {
        assertionFailure("Sub-class must implement the PagerTabStripDataSource viewControllersForPagerTabStrip: method")
        return []
    }
    
    //MARK: - Helpers
    
    public func updateIfNeeded() {
        if isViewLoaded  && !lastSize.equalTo(containerView.bounds.size){
            updateContent()
        }
    }
    
    public func canMoveToIndex( index: Int) -> Bool {
        return currentIndex != index && viewControllers.count > index
    }

    public func pageOffsetForChildIndex( index: Int) -> CGFloat {
        return CGFloat(index) * containerView.bounds.width
    }
    
    public func offsetForChildIndex(index: Int) -> CGFloat{
        return (CGFloat(index) * containerView.bounds.width) + (((containerView.bounds.width) - (view.bounds.width)) * 0.5)
    }
    
    public func offsetForChildViewController(viewController: UIViewController) throws -> CGFloat{
        guard let index = viewControllers.index(of : viewController) else {
            throw PagerTabStripError.ViewControllerNotContainedInPagerTabStrip
        }
        return offsetForChildIndex(index: index)
    }
    
    public func pageForContentOffset(contentOffset: CGFloat) -> Int {
        let result = virtualPageForContentOffset(contentOffset: contentOffset)
        return pageForVirtualPage(virtualPage: result)
    }
    
    public func virtualPageForContentOffset(contentOffset: CGFloat) -> Int {
        return Int((contentOffset + 1.5 * pageWidth) / pageWidth) - 1
    }
    
    public func pageForVirtualPage(virtualPage: Int) -> Int{
        if virtualPage < 0 {
            return 0
        }
        if virtualPage > viewControllers.count - 1 {
            return viewControllers.count - 1
        }
        return virtualPage
    }
    
    public func updateContent() {
        if lastSize.width != containerView.bounds.size.width {
            lastSize = containerView.bounds.size
            containerView.contentOffset = CGPoint(x: pageOffsetForChildIndex(index: currentIndex),y:  0)
        }
        lastSize = containerView.bounds.size
        
        let pagerViewControllers = pagerTabStripChildViewControllersForScrolling ?? viewControllers
        containerView.contentSize = CGSize(width: containerView.bounds.width * CGFloat(pagerViewControllers.count), height: containerView.contentSize.height)
        
        for (index, childController) in pagerViewControllers.enumerated() {
            let pageOffsetForChild = pageOffsetForChildIndex(index: index)
            if fabs(containerView.contentOffset.x - pageOffsetForChild) < containerView.bounds.standardized.width {
                if let _ = childController.parent {
                    childController.view.frame = CGRect (origin: CGPoint (x: offsetForChildIndex(index: index), y: 0),size: CGSize(width: view.bounds.standardized.width, height: containerView.bounds.height))
                    childController.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
                }
                else {
                    addChildViewController(childController)
                    childController.beginAppearanceTransition(true, animated: false)
                    childController.view.frame = CGRect(origin: CGPoint (x: offsetForChildIndex(index: index), y: 0), size: CGSize(width :view.bounds.width, height: containerView.bounds.height))
                    childController.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
                    containerView.addSubview(childController.view)
                    childController.didMove(toParentViewController: self)
                    childController.endAppearanceTransition()
                }
            }
            else {
                if let _ = childController.parent {
                    childController.willMove(toParentViewController: nil)
                    childController.beginAppearanceTransition(false, animated: false)
                    childController.view.removeFromSuperview()
                    childController.removeFromParentViewController()
                    childController.endAppearanceTransition()
                }
            }
        }
        
        let oldCurrentIndex = currentIndex
        let virtualPage = virtualPageForContentOffset(contentOffset: containerView.contentOffset.x)
        let newCurrentIndex = pageForVirtualPage(virtualPage: virtualPage)
        currentIndex = newCurrentIndex
        let changeCurrentIndex = newCurrentIndex != oldCurrentIndex
        
        if let progressiveDeledate = self as? PTPagerTabStripIsProgressiveDelegate, pagerBehaviour.isProgressiveIndicator {
            
            let (fromIndex, toIndex, scrollPercentage) = progressiveIndicatorData(virtualPage: virtualPage)
            progressiveDeledate.pagerTabStripViewController(pagerTabStripViewController: self, updateIndicatorFromIndex: fromIndex, toIndex: toIndex, withProgressPercentage: scrollPercentage, indexWasChanged: changeCurrentIndex)
        }
        else{
            delegate?.pagerTabStripViewController(pagerTabStripViewController: self, updateIndicatorFromIndex: min(oldCurrentIndex, pagerViewControllers.count - 1), toIndex: newCurrentIndex)
        }
    }
        
    public func reloadPagerTabStripView() {
        guard isViewLoaded else { return }
        for childController in viewControllers {
            if let _ = childController.parent {
                childController.view.removeFromSuperview()
                childController.willMove(toParentViewController: nil)
                childController.removeFromParentViewController()
            }
        }
        reloadViewControllers()
        containerView.contentSize = CGSize(width: containerView.bounds.width * CGFloat(viewControllers.count), height: containerView.contentSize.height)
        if currentIndex >= viewControllers.count {
            currentIndex = viewControllers.count - 1
        }
        containerView.contentOffset = CGPoint(x: pageOffsetForChildIndex(index: currentIndex), y: 0)
        updateContent()
    }
    
    //MARK: - UIScrollDelegate
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if containerView == scrollView {
            updateContent()
        }
    }
    
    public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if containerView == scrollView {
            lastPageNumber = pageForContentOffset(contentOffset: scrollView.contentOffset.x)
            lastContentOffset = scrollView.contentOffset.x
        }
    }
    
    public func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        if containerView == scrollView {
            pagerTabStripChildViewControllersForScrolling = nil
            (navigationController?.view ?? view).isUserInteractionEnabled = true
            updateContent()
        }
    }
    
    //MARK: - Orientation
    
    /*public override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        isViewRotating = true
        pageBeforeRotate = currentIndex
        coordinator.animate(alongsideTransition: nil) { [weak self] _ in
            guard let me = self else { return }
            me.isViewRotating = false
            me.currentIndex = me.pageBeforeRotate
            me.updateIfNeeded()
        }
    }*/
    
    
    //MARK: Private
    
    private func progressiveIndicatorData(virtualPage: Int) -> (Int, Int, CGFloat) {
        let count = viewControllers.count
        var fromIndex = currentIndex
        var toIndex = currentIndex
        let direction = swipeDirection
        
        if direction == .Left {
            if virtualPage > count - 1 {
                fromIndex = count - 1
                toIndex = count
            }
            else {
                if self.scrollPercentage >= 0.5 {
                    fromIndex = max(toIndex - 1, 0)
                }
                else {
                    toIndex = fromIndex + 1
                }
            }
        }
        else if direction == .Right {
            if virtualPage < 0 {
                fromIndex = 0
                toIndex = -1
            }
            else {
                if self.scrollPercentage > 0.5 {
                    fromIndex = min(toIndex + 1, count - 1)
                }
                else {
                    toIndex = fromIndex - 1
                }
            }
        }
        let scrollPercentage = pagerBehaviour.isElasticIndicatorLimit ? self.scrollPercentage : ((toIndex < 0 || toIndex >= count) ? 0.0 : self.scrollPercentage)
        return (fromIndex, toIndex, scrollPercentage)
    }
    
    private func reloadViewControllers(){
        guard let dataSource = datasource else {
            fatalError("dataSource must not be nil")
        }
        viewControllers = dataSource.viewControllersForPagerTabStrip(pagerTabStripController: self)
        // viewControllers
        guard viewControllers.count != 0 else {
            fatalError("viewControllersForPagerTabStrip should provide at least one child view controller")
        }
        viewControllers.forEach { if !($0 is PTIndicatorInfoProvider) { fatalError("Every view controller provided by PagerTabStripDataSource's viewControllersForPagerTabStrip method must conform to  InfoProvider") }}

    }
    
    private var pagerTabStripChildViewControllersForScrolling : [UIViewController]?
    private var lastPageNumber = 0
    private var lastContentOffset: CGFloat = 0.0
    private var pageBeforeRotate = 0
    private var lastSize = CGSize(width: 0, height: 0)
    internal var isViewRotating = false
    internal var isViewAppearing = false
    
}
