//
//  PTMoreViewController.swift
//  OK
//
//  Created by Ashish on 12/14/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol MoreControllerActionDelegate : class {
    func actionFor(option: MorePaymentAction)
}

class PTMoreViewController: PayToBaseViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var touchResitantView: CardDesignView!
    
    @IBOutlet weak var morePayment: UIButton!
    @IBOutlet weak var repeatPayment: UIButton!
    @IBOutlet weak var invoiceBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!

    
    var titleMorePayment : String?
    var titleRepeatPayment : String?
    
    var internationalTopup : String?
    
    var isFromTopup : Bool = true
    
   weak var delegate : MoreControllerActionDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideWhenTappedAround()
        
        if let _ = internationalTopup {
            self.morePayment.setTitle("Overseas More Top-up".localized, for: .normal)
            self.repeatPayment.setTitle("Repeat Same Country".localized, for: .normal)
            self.invoiceBtn.setTitle("Invoice".localized, for: .normal)
            self.shareBtn.setTitle("Share".localized, for: .normal)
        } else {
            if isFromTopup {
                self.morePayment.setTitle("More Top-up".localized, for: .normal)
                self.repeatPayment.setTitle("Repeat Top-up".localized, for: .normal)
            } else {
                self.morePayment.setTitle("More Payment".localized, for: .normal)
                self.repeatPayment.setTitle("Repeat Payment".localized, for: .normal)
            }
            self.invoiceBtn.setTitle("Invoice".localized, for: .normal)
            self.shareBtn.setTitle("Share".localized, for: .normal)
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func hideWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(dismissSelf))
        tap.delegate = self
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissSelf() {
        println_debug("removed called")
        removeView()
    }
    
    func removeView() {
        self.view.removeFromSuperview()
    }
    
    //MARK:- More Controller Actions
    @IBAction func morePayment(_ sender: UIButton) {
        removeView()
        if let delegate = self.delegate {
            delegate.actionFor(option: .morePayment)
        }
    }
    
    @IBAction func repeatPayment(_ sender: UIButton) {
        removeView()
        if let delegate = self.delegate {
            delegate.actionFor(option: .repeatPayment)
        }
    }
    
    @IBAction func invoice(_ sender: UIButton) {
        removeView()
        if let delegate = self.delegate {
            delegate.actionFor(option: .invoice)
        }
    }
    
    @IBAction func share(_ sender: UIButton) {
        removeView()
        if let delegate = self.delegate {
            delegate.actionFor(option: .share)
        }
    }
    
    //MARK:- UIGestureRecognizerDelegate
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        let point = touch.location(in: self.view)
        if self.touchResitantView.frame.contains(point) {
            println_debug(self.touchResitantView.frame.contains(point))
            return false
        }
        return true
    }
}

enum MorePaymentAction {
    case morePayment, repeatPayment, invoice, share
}
