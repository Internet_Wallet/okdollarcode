//
//  PTSentRequestObject.swift
//  OK
//
//  Created by Ashish on 12/22/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PTSentRequestObject: NSObject {

    var agentName = ""
    var amount = 0
    var attachPath = [String]()
    var categoryName = ""
    var commonTransType = ""
    var date = ""
    var dest = ""
    var block = false
    var myNum = false
    var scheduled = false
    var localTransType = ""
    var proImg = ""
    var remarks = ""
    var remindCount = 0
    var reqId = ""
    var scheduledType = 0
    var scheduledTime = ""
    var source = ""
    var sourceAgentName = ""
    var status = 0

    func wrapData(dict:Dictionary<String, Any>) {
        self.agentName      = dict["AgentName"] as! String
        self.amount         = dict["Amount"] as! Int
        self.attachPath     = dict["AttachmentPath"] as! Array
        self.categoryName   = dict["CategoryName"] as! String
        self.date           = dict["Date"] as! String
        self.dest           = dict["Destination"] as! String
        self.block          = dict["IsBlocked"] as! Bool
        self.myNum          = dict["IsMynumber"] as! Bool
        self.scheduled      = dict["IsScheduled"] as! Bool
        self.localTransType = dict["LocalTransactionType"] as! String
        self.proImg         = dict["ProfileImage"] as! String
        self.source         = dict["Source"] as! String
        self.scheduledTime  = dict["Scheduledtime"] as! String
        self.reqId          = dict["RequestId"] as! String
        println_debug(dict)
    }

}
