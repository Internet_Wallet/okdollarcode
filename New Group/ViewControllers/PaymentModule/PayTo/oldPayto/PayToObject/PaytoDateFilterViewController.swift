//
//  PaytoDateFilterViewController.swift
//  OK
//
//  Created by Ashish on 6/4/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol PaytoDatePickerDelegate : class {
    func didSelectDate(from: String, to: String)
}

class PaytoDateFilterViewController: OKBaseController {
    
    @IBOutlet private weak var fromDatePicker: UIDatePicker!
    @IBOutlet private weak var toDatePicker: UIDatePicker!
    
    @IBOutlet private weak var fromLabelString: UILabel!
    @IBOutlet private weak var toLabelString: UILabel!
    
    weak var delegate : PaytoDatePickerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat      = "dd-MM-yyyy"
        self.fromLabelString.text     = dateFormatter.string(from: Date())
        self.toLabelString.text       = dateFormatter.string(from: Date())
        
    }
    
    //MARK:- Date Picker Delegate
    @IBAction func fromDatePickerAction(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        self.fromLabelString.text = dateFormatter.string(from: sender.date)
    }
    
    @IBAction func toDatePickerAction(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        self.toLabelString.text = dateFormatter.string(from: sender.date)
    }
    
    
    //MARK:- IBAction Button
    @IBAction func doneAction(_ sender: UIButton) {
        
        let fromDate = self.fromDatePicker.date.stringValue()
        let toDate   = self.toDatePicker.date.stringValue()
        
        if let delegate = self.delegate {
            delegate.didSelectDate(from: fromDate, to: toDate)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    @IBAction func resetAction(_ sender: UIButton) {
        self.fromDatePicker.date = Date()
        self.toDatePicker.date = Date()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        self.fromLabelString.text  = dateFormatter.string(from: Date())
        self.toLabelString.text    = dateFormatter.string(from: Date())
    }
    
    @IBAction func cancelAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

