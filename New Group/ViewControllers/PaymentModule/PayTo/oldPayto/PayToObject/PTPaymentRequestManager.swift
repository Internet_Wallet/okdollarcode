//
//  PTPaymentRequestManager.swift
//  OK
//
//  Created by Ashish on 12/22/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

enum PaymentRequestType {
    case sent, recieve
}

class PTPaymentRequestManager: NSObject {
    
    static let shared  = PTPaymentRequestManager()
    
    var sentRequest    = [PTSentRequestObject]()
    var recieveRequest = [PTRecieveRequestObject]()
    
    var requestType  : PaymentRequestType?
    
    var object : PaymentRequestViewController?
    
    func requestManager(type: PaymentRequestType, withEntity obj : PaymentRequestViewController) {
        object     = obj
        requestType = type
        
        switch type {
        case .recieve: self.recieveRequestInitialization()
        case .sent:    self.sentRequestInitialization()
        }
    }
    
    func getResponseTypeArray(type: PaymentRequestType) -> [AnyObject] {
            switch type {
            case .recieve: return recieveRequest
            case .sent: return sentRequest
            }
    }
    
    func recieveRequestInitialization() {
        let web = PayToWebApi()
        web.delegate = self
        let  urlString = String.init(format: PTHelper.paymentRequest, UserModel.shared.mobileNo, UserModel.shared.simID, UserModel.shared.msid, "1", UserModel.shared.simID, "Requested","ALL","20","0","0")
        let pram = Dictionary<String,String>() as AnyObject
        let ur = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let str = getUrl(urlStr: ur!, serverType: .serverApp)
        println_debug(str)
        web.genericClass(url: str, param: pram, httpMethod: "GET", mScreen: ScreenName.paymentRequest)
    }
    
    func sentRequestInitialization() {
        let web = PayToWebApi()
        web.delegate = self
        let  urlString = String.init(format: PTHelper.recPaymentReq, UserModel.shared.mobileNo, UserModel.shared.simID, UserModel.shared.msid, UserModel.shared.osType,"Requested","All","20","0","0")
        let pram = Dictionary<String,String>() as AnyObject
        let ur = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let str = getUrl(urlStr: ur!, serverType: .serverApp)
        println_debug(str)
        web.genericClass(url: str, param: pram, httpMethod: "GET", mScreen: ScreenName.sentPaymentReq)
    }
}

extension PTPaymentRequestManager: PTWebResponseDelegate {
    
    func webSuccessResult(data: Any, screen: String) {
        if screen == ScreenName.paymentRequest {
            if let json = data as? [String: Any] {
                if let stringObject = json["Data"] as? String {
                    if let data = stringObject.data(using: String.Encoding.utf8) {
                        do {
                            let dict = try JSONSerialization.jsonObject(with: data, options: []) as? Array<AnyObject>
                            if let dataDict = dict as? Array<Dictionary<String, Any>> {
                                self.recieveRequest.removeAll()
                                for item in dataDict {
                                    let pdObject = PTRecieveRequestObject()
                                    pdObject.wrapData(dict: item)
                                   self.recieveRequest.append(pdObject)
                                }
                                object?.refreshView()
                            }
                        } catch let error as NSError {
                            println_debug(error)
                        }
                    }
                }
            }
        } else if screen == ScreenName.sentPaymentReq {
            if let json = data as? [String: Any] {
                if let stringObject = json["Data"] as? String {
                    if let data = stringObject.data(using: String.Encoding.utf8) {
                        do {
                            let dict = try JSONSerialization.jsonObject(with: data, options: []) as? Array<AnyObject>
                            if let dataDict = dict as? Array<Dictionary<String, Any>> {
                                self.sentRequest.removeAll()
                                for item in dataDict {
                                    let pdObject = PTSentRequestObject()
                                    pdObject.wrapData(dict: item)
                                    self.sentRequest.append(pdObject)
                                }
                                if let object = object {
                                    object.refreshView()
                                }
                            }
                        } catch let error as NSError {
                            println_debug(error)
                        }
                    }
                }
            }
        }
    }
    
    func webFailureResult(screen: String) {
        if screen == ScreenName.paymentRequest {
            println_debug("Something went wrong in " +  ScreenName.paymentRequest)
        }
        if screen == ScreenName.sentPaymentReq {
            println_debug("Something went wrong in " +  ScreenName.sentPaymentReq)
        }
    }
}



