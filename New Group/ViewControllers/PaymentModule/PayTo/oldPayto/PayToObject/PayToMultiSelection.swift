//
//  PayToMultiSelection.swift
//  OK
//
//  Created by Ashish on 12/20/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol PTMultiSelectionDelegate {
    func didSelectOption(option: PTMultiSelectionOption)
}

enum PTMultiSelectionOption {
    case favorite, contact, scanQR
}

class PayToMultiSelection: UIViewController {
    
    @IBOutlet weak var favoriteButton : UIButton!
    @IBOutlet weak var okContactBtn   : UIButton!
    @IBOutlet weak var scanQRbtn      : UIButton!
    @IBOutlet weak var backgroundBtn  : UIButton!
    
    var delegate : PTMultiSelectionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        favoriteButton.addTargetClosure { (sender) in
            self.removeFromSuper()
            if let delegate = self.delegate {
                delegate.didSelectOption(option: PTMultiSelectionOption.favorite)
            }
        }
        
        okContactBtn.addTargetClosure { (sender) in
            self.removeFromSuper()
            if let delegate = self.delegate {
                delegate.didSelectOption(option: PTMultiSelectionOption.contact)
            }
        }
        
        scanQRbtn.addTargetClosure { (sender) in
            self.removeFromSuper()
            if let delegate = self.delegate {
                delegate.didSelectOption(option: PTMultiSelectionOption.scanQR)
                self.dismiss(animated: true, completion: nil)
            }
        }
        
        self.backgroundBtn.addTargetClosure { (sender) in
            self.removeFromSuper()
        }
        
        if favoriteManager.count(.payto) > 0 {
            self.favoriteButton.isHidden = false
        } else {
            self.favoriteButton.isHidden = true
        }

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.favoriteButton.setTitle("Add Favorite".localized, for: .normal)
        self.okContactBtn.setTitle("Add from OK $ Contact".localized, for: .normal)
        self.scanQRbtn.setTitle("Scan QR Code".localized, for: .normal)
    }
    
    func removeFromSuper() {
        if let keyWindow = UIApplication.shared.keyWindow {
            for view in keyWindow.subviews {
                if view.tag == 4354 {
                    view.removeFromSuperview()
                }
            }
        }
    }
    
}

@IBDesignable class CurvedButton: UIButton
{
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateCornerRadius()
    }
    
    @IBInspectable var rounded: Bool = false {
        didSet {
            updateCornerRadius()
        }
    }
    
    func updateCornerRadius() {
        layer.cornerRadius = rounded ? frame.size.height / 2 : 0
    }
}
