//
//  PaytoMainMenuSideActionViewController.swift
//  OK
//
//  Created by Ashish on 3/9/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

enum PaytoMainMenuSideActionEnum {
    case addMoney, paymentRequestRecieve, paymentRequestSent
}

class PaytoMainMenuSideActionViewController: OKBaseController, UITableViewDelegate, UITableViewDataSource {
    
    var enumEntity : PaytoMainMenuSideActionEnum = PaytoMainMenuSideActionEnum.addMoney
    
    var dataArray : [String]?
    
    @IBOutlet weak var tableSideMenuView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch enumEntity {
        case .addMoney:
            dataArray = ["Add Money"]
            break
        case .paymentRequestRecieve:
            dataArray = ["All", "Pending Approval", "Request Approved", "Cancelled"]
            break
        case .paymentRequestSent:
            dataArray = ["Pending", "Successful", "Reject", "Scheduled"]
            break
        }
        
        self.tableSideMenuView.delegate   = self
        self.tableSideMenuView.dataSource = self
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (dataArray == nil) ? 0 : dataArray!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PaytoMainMenuSideActionViewControllerCell.self), for: indexPath) as? PaytoMainMenuSideActionViewControllerCell
        return (cell == nil) ? UITableViewCell.init() : cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.2) {
            (cell as! PaytoMainMenuSideActionViewControllerCell).wrapContent(self.dataArray![indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

}

class PaytoMainMenuSideActionViewControllerCell : UITableViewCell {
    
    @IBOutlet weak var nameLabel : UILabel!
    
    func wrapContent(_ name: String) {
        self.nameLabel.text = name
    }
    
}
