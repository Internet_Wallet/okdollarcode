//
//  PayToFilterViewController.swift
//  OK
//
//  Created by Ashish on 12/14/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PayToFilterViewController: PayToBaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    var filterType : PTFilterType?

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var filterTableView: CardDesignView!
    
    var delegate : FilterPaymentRequestDelegate?

    var itemsInSections: Array<Array<String>> = [["Default"], ["Normal"], ["Personal"]]
    var sectionsArray: Array<String> = ["", "Type", "Categories"]

    @IBOutlet weak var filterContainerView: NSLayoutConstraint!
    @IBOutlet weak var filterViewHeaderTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPOPButton()
        self.filterTableView.isHidden = false
        decideFilterType()
        dismiss()
        self.tableView.tableFooterView = UIView()
        
    }
    
   private func dismiss() {
        dismissButton.addTargetClosure { (_) in
            self.dismiss(animated: true, completion: nil)
        }
    }
    
  private func decideFilterType() {
        switch filterType ?? .filter {
        case .filter:
            self.filter()
        case .date:
            break
        case .sort:
            self.sort()
        }
    }
    
    private func filter() {
        self.filterViewHeaderTitle.text = "Filter By"
        sectionsArray =  ["", "Type", "Categories"]
        self.filterContainerView.constant = (CGFloat(sectionsArray.count) * 65.00 +  CGFloat(self.itemsInSections.count) * 65.00) + 50.00
    }
    
    private func sort() {
        self.filterViewHeaderTitle.text = "Sort By"
        let filters = ["Default", "Amount High to Low", "Amount Low to High", "Name A to Z", "Name Z to A"]
        itemsInSections = [filters]
        sectionsArray =  [""]
        self.filterContainerView.constant = 5 * 65.00 + 50.00
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        if filterType == .sort {
            return 1
        }
        return self.sectionsArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemsInSections[section].count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sectionsArray[section]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilteredArrayCell") as! FilteredArrayCell
        let text = self.itemsInSections[indexPath.section][indexPath.row]
        cell.nameLabel.text = text
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if filterType == .sort {
            return 0.0
        }
        
        if filterType == .filter, section == 0 {
            return 0.0
        }
        
        return 65.00
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.00
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.filterTypeDependency(indexPath)
    }
    
  private func filterTypeDependency(_ indexPath: IndexPath) {
        switch filterType ?? .sort {
        case .sort:
            self.sortSelection(indexPath)
        case .filter:
            self.filterSelection(indexPath)
        case .date:
            self.filterSelection(indexPath)
        }
    }
    
    private func filterMethodsUpdation(_ type: PaymentRequestFilterType) {
        if let delegate = self.delegate {
            delegate.didSelectFilterType(type: type)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    private func sortSelection(_ indexPath: IndexPath) {
        var prSortType = PaymentRequestFilterType.def
        switch indexPath.row {
        case 0:
            prSortType = .def
        case 1:
            prSortType = .amount_hl
        case 2:
            prSortType = .amount_lh
        case 3:
            prSortType = .name_az
        case 4:
            prSortType = .name_za
        default:
            prSortType = .def
        }
        self.filterMethodsUpdation(prSortType)
    }
    
    private func filterSelection(_ indexPath: IndexPath) {
        var prSortType = PaymentRequestFilterType.def
        switch (indexPath.section,indexPath.row) {
        case (0,0):
            prSortType = .def
        case (1,0):
            prSortType = .normal
        case (2,0):
            prSortType = .personal
        default:
            prSortType = .def
        }
        self.filterMethodsUpdation(prSortType)
    }
    
    
}

class FilteredArrayCell : UITableViewCell {
    var isCollapsed : Bool = false
    @IBOutlet weak var nameLabel: UILabel!
}

protocol FilterPaymentRequestDelegate {
    func didSelectFilterType(type: PaymentRequestFilterType)
}

enum PaymentRequestFilterType {
    case def, amount_hl, amount_lh, name_az, name_za, normal, personal
}


