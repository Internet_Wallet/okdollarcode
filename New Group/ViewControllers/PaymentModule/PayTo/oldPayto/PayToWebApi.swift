//
//  PayToWebApi.swift
//  OK
//
//  Created by Ashish on 12/11/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct PTHelper {
    static let chkAgentCode      = "https://www.okdollar.co/RestService.svc/GetCategoryAndMercNameByMobileNumber?MobileNumber=%@&SourceNumber=\(UserModel.shared.mobileNo)&TransType=%@&AppBuildNumber=\(buildNumber)&OsType=1"
    static let cashback          = "https://www.okdollar.co/RestService.svc/MultiCashBack"
    static let paymentRequest    = "/RestService.svc/RequestMoneyViewByReciever?MobileNumber=%@&Simid=%@&MSID=%@&OSType=%@&OTP=%@&Status=%@&LocalTransType=%@&Limit=%@&OffSet=%@&RequestModuleType=%@"
    static let recPaymentReq     = "/RestService.svc/RequestMoneyViewBySender?MobileNumber=%@&Simid=%@&M SID=%@&OSType=%@&OTP=&RequestedMoneyStatus=%@&RequestType=%@&Limit=%@&OffSet=%@&RequestModuleType=%@"
    static let confirmPay        = "https://www.okdollar.net/WebServiceIpay/services/request;requesttype=FEELIMITKICKBACKINFO;agentcode=%@;destination=%@;amount=%@;pin=%@;vendorcode=%@;clientip=%@;clientos=%@;transtype=%@;clienttype=%@;securetoken=%@"
    static let singlePay        = "https://www.okdollar.net/WebServiceIpay/services/request;requesttype=%@;agentcode=%@;pin=%@;source=%@;destination=%@;amount=%@;vendorcode=IPAY;clienttype=GPRS;comments=%@;securetoken=%@"
    static let multiPay         = "https://www.okdollar.co/RestService.svc/MultipleGenericPayment"
    static let cancelRecieveReq =  "https://www.okdollar.co/RestService.svc/RequestMoneyStatusUpdate?MobileNumber=\(UserModel.shared.mobileNo)&Simid=\(uuid)&MSID=\(msid)&OSType=0&OTP=\(uuid)&RequestId=%@&Status=Cancelled"
    static let reqMoneySinglePay        = "https://www.okdollar.net/WebServiceIpay/services/request;requesttype=%@;agentcode=%@;pin=%@;source=%@;destination=%@;amount=%@;vendorcode=IPAY;clienttype=GPRS;comments=%@;securetoken=%@;agenttransid=%@;clientip=%@;clientos=%@"
}

struct ScreenName {
    static let chkAgentCode   = "checkAgentCode"
    static let chkAgentCodeMulti   = "checkAgentCodeMulti"
    static let cashBack       = "cashBack"
    static let cashBackMulti       = "cashBack_Multi"
    static let paymentRequest = "PaymentRequestScreen"
    static let sentPaymentReq = "SendPaymentRequestScreen"
    static let singlePay      = "SinglePaymentScreen"
}


protocol PTWebResponseDelegate {
    func webSuccessResult(data: Any,screen: String)
    func webFailureResult(screen: String)
}

class PayToWebApi: NSObject {
        var delegate : PTWebResponseDelegate?
            
        func genericClass(url: URL, param: AnyObject, httpMethod: String, mScreen: String) {
            
            DispatchQueue.main.async {
                PTLoader.shared.show()
            }
            
            let session             = URLSession.shared
            let request             = NSMutableURLRequest(url:url)
            request.timeoutInterval = 60
            request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
            
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = httpMethod.capitalized
            
            if httpMethod == "POST" {
                let theJSONData = try? JSONSerialization.data(
                    withJSONObject: param ,
                    options: JSONSerialization.WritingOptions(rawValue: 0))
                let jsonString = NSString(data: theJSONData!,
                                                   encoding: String.Encoding.ascii.rawValue)
                let postLength = NSString(format:"%lu", jsonString!.length) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
            }
            
            let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
                if((error) != nil) {
                    self.failureParse(mscr: mScreen)
                    println_debug(error!.localizedDescription)
                }else {
                        do {
                            let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                            self.successParse(json: json, mscr: mScreen)
                        } catch {
                            self.failureParse(mscr: mScreen)
                        }
                }
            }
            dataTask.resume()
        }
    
    func genericClassXML(url: URL, param: AnyObject, httpMethod: String, mScreen: String) {
        
        PTLoader.shared.show()
        
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.capitalized
        
        if httpMethod == "POST" {
            let theJSONData = try? JSONSerialization.data(
                withJSONObject: param ,
                options: JSONSerialization.WritingOptions(rawValue: 0))
            let jsonString          = NSString(data: theJSONData!,
                                               encoding: String.Encoding.ascii.rawValue)
            let postLength = NSString(format:"%lu", jsonString!.length) as String
            request.setValue(postLength, forHTTPHeaderField:"Content-Length")
            request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
        }
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            
            if((error) != nil) {
                self.failureParse(mscr: mScreen)
            }else {
                
                if data == nil {
                    self.failureParse(mscr: mScreen)
                    return
                }
                
                let parsedXML = String.init(data: data!, encoding: String.Encoding.utf8)
                self.successParse(json: parsedXML ?? "", mscr: mScreen)
            }
        }
        dataTask.resume()
    }

    
  private  func successParse(json: Any,mscr: String) {
    
        if let delegate = self.delegate {
            delegate.webSuccessResult(data: json, screen: mscr)
        }
        PTLoader.shared.hide()
    }
    
 private func failureParse(mscr: String) {
        if let delegate = self.delegate {
            delegate.webFailureResult(screen: mscr)
        }
        PTLoader.shared.hide()
    }

    
    
}
