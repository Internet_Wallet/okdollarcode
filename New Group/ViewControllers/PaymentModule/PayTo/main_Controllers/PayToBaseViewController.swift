//
//  PayToBaseViewController.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/25/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import IQKeyboardManagerSwift

enum PayToTypes {
    case singlePay
    case multiPay
}

protocol TableReloadDelegate : class {
    func reloadTableData(obj: PaySendViewController)
    func multiPaymentHideshowButton(flag: Bool)
    func amountValidationForMultipayment(obj: PaySendViewController)
}

let pYellowColor = UIColor.init(hex: "F3C632")


class PayToBaseViewController: OKBaseController {
    
    let mobileNumberAcceptableCharacters = "0123456789"
    
    var kbHeight: CGFloat!

    override func viewDidLoad() {
        super.viewDidLoad()
//        NotificationCenter.default.addObserver(self, selector: #selector(PayToBaseViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(PayToBaseViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func setPOPButton() {
        let buttonIcon      = UIImage(named: "tabBarBack")
        let leftBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: self, action: #selector(PayToBaseViewController.dismissTheViewControllerPT(_:)))
        leftBarButton.image = buttonIcon
        self.navigationItem.leftBarButtonItem = leftBarButton
    }
    
    @objc func popTheViewControllerPT(_ sender:UIBarButtonItem!) {
        if let navigation = self.navigationController {
            navigation.popViewController(animated: true)
        }
    }

    @objc func dismissTheViewControllerPT(_ sender:UIBarButtonItem!) {
        if let navigation = self.navigationController {
            navigation.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
        
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().enable = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //NotificationCenter.default.removeObserver(self)
        self.view.endEditing(true)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func viewLayouting(onView v : UIView) {
        v.layer.cornerRadius = v.frame.width / 2
        // border
        v.layer.borderColor = UIColor.init(hex: "F3C632").cgColor
        v.layer.borderWidth = 1.5
        // drop shadow
        v.layer.shadowColor = UIColor.lightGray.cgColor
        v.layer.shadowOpacity = 0.8
        v.layer.shadowRadius = 2.0
        v.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
    }
    
    func makeJsonFromDict(mpModel: [PaySendViewController]) -> Dictionary<String,AnyObject> {
        var finalObject =  Dictionary<String,AnyObject>()
        let dict        = [Dictionary<String,String>]()
    
//        for vc in mpModel {
//            var cashbackobj = Dictionary<String,String>()
//            cashbackobj["Agentcode"]   = UserModel.shared.mobileNo
//            cashbackobj["Amount"]      = vc.enterAmount.text
//            cashbackobj["Clientip"]    = OKBaseController.getIPAddress()
//            cashbackobj["Clientos"]    = "iOS"
//            cashbackobj["Clienttype"]  = "GPRS"
//
//            cashbackobj["Destination"]    = getAgentCode(numberWithPrefix: vc.mobileNumber.text!, havingCountryPrefix: vc.countryObject.code)
//            cashbackobj["Pin"]            = ok_password ?? ""
//            cashbackobj["Requesttype"]    = "FEELIMITKICKBACKINFO"
//            cashbackobj["Securetoken"]    = UserLogin.shared.token
//            cashbackobj["Transtype"]      = self.getTransactionString(vc.transactionType)
//            cashbackobj["Vendorcode"]     = "IPAY"
//            dict.append(cashbackobj)
//        }
        
        var log = Dictionary<String,Any>()
        log["MobileNumber"] = UserModel.shared.mobileNo
        log["Msid"]         = UserModel.shared.msid
        log["Ostype"]       = 1
        log["Otp"]          = ""
        log["SimId"]        = uuid
        
        finalObject["CashBackRequestList"] = dict as AnyObject
        finalObject["Login"] = log as AnyObject
        return finalObject
    }


}

class countryFlagClassPT : OKBaseController {
    @IBOutlet weak var flagImgView: UIImageView!
    @IBOutlet weak var codeLbl: UILabel!
    
    var delegate : PayToButtonActionDelegates?
    
    @IBAction func countrySelectionTapped(_ sender: UIButton) {
        guard (self.delegate?.countryAction() != nil) else {
            return
        }
    }
    
    func wrapData(img: String, code: String) {
        self.flagImgView.image = UIImage.init(named: img)
        self.codeLbl.text      = code
    }
}

class contactViewClassPT : OKBaseController {
    
    @IBOutlet weak var contactBtnImageView: UIImageView!
    
    var delegate : PayToButtonActionDelegates?
    
    @IBAction func contactButtonTapped(_ sender: UIButton) {
        guard (self.delegate?.contactAction() != nil) else {
            return
        }
    }
}

class ClearViewClassPT : OKBaseController {
    @IBOutlet weak var clearBtnImageView: UIImageView!
    
    var delegate : PayToButtonActionDelegates?
    
    @IBAction func clearButoonTapped(_ sender: UIButton) {
        guard (self.delegate?.clearAction() != nil) else {
            return
        }
    }
}

protocol PayToButtonActionDelegates {
    func countryAction()
    func clearAction()
    func contactAction()
}

extension UIViewController {
    
    func showToast(message : String) {
        let toastLabel = UILabel()
        toastLabel.text = message
        toastLabel.numberOfLines = 0
        toastLabel.font = UIFont(name: appFont,size: 14.0)
        toastLabel.textAlignment = .center
        toastLabel.lineBreakMode = .byWordWrapping
        toastLabel.textColor = .white
        toastLabel.backgroundColor = UIColor.clear
        
        let maxMessageSize = CGSize(width:  screenWidth - 70, height: toastLabel.bounds.size.height)
        let messageSize = toastLabel.sizeThatFits(maxMessageSize)
            let actualWidth = min(messageSize.width, maxMessageSize.width)
            let actualHeight = min(messageSize.height, maxMessageSize.height)
            toastLabel.frame = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func showToast(message: String, align: ToastAlignment) {
        let bounds = self.view.bounds
        var frame: CGRect {
            if align == .top {
                return CGRect(x: bounds.size.width/2 - 75, y: bounds.origin.y + 150, width: 150, height: 35)
            }else if align == .center {
                return CGRect(x: bounds.size.width/2 - 75, y: bounds.size.height/2 - 100, width: 150, height: 35)

            }else {
                return CGRect(x: bounds.size.width/2 - 75, y: bounds.size.height-100, width: 150, height: 35)
            }
        }
        
        let toastLabel = UILabel(frame: frame)
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: appFont, size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 3.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func showCICOToast(message: String) {
        let bounds = self.view.bounds
        let font = UIFont(name: appFont, size: 14) ?? UIFont.systemFont(ofSize: 14)
        let height = message.height(withConstrainedWidth: bounds.size.width - 30, font: font)
        var frame: CGRect {
            return CGRect(x: 10, y: bounds.size.height-100, width: bounds.size.width - 30, height: height + 15)
        }
        
        let toastLabel = UILabel(frame: frame)
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = font
        toastLabel.numberOfLines = 0
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 2.0, delay: 2.0, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}

@IBDesignable class RoundableUIImageView: UIImageView {
    private var _round = false
    @IBInspectable var round: Bool {
        set {
            _round = newValue
            makeRound()
        }
        get {
            return self._round
        }
    }
    override internal var frame: CGRect {
        set {
            super.frame = newValue
            makeRound()
        }
        get {
            return super.frame
        }
    }
    
    private func makeRound() {
        if self.round == true {
            self.clipsToBounds = true
            self.layer.cornerRadius = (self.frame.width + self.frame.height) / 4
        } else {
            self.layer.cornerRadius = 0
        }
    }
    
    override func layoutSubviews() {
        makeRound()
    }
}


@IBDesignable class CircleView  : UIView {
    @IBInspectable var diameter    : CGFloat     = 50.0
    @IBInspectable var lineWidth   : CGFloat    = 2.0
    @IBInspectable var strokeColor : UIColor  = UIColor.clear
    @IBInspectable var fillColor: UIColor    = UIColor.clear
    
    override func draw(_ rect: CGRect) {
        let shapeLayer = CAShapeLayer()
        
        shapeLayer.path = UIBezierPath(
            ovalIn: CGRect(x: 0, y: 0, width: diameter, height: diameter)).cgPath
        
        shapeLayer.fillColor = fillColor.cgColor
        shapeLayer.strokeColor = strokeColor.cgColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.position = CGPoint(
            x: self.frame.midX - (diameter / 2.0),
            y: self.frame.midY - (diameter / 2.0)
        )
        
        layer.addSublayer(shapeLayer)
    }
}

extension UIView {
    func boundInside(superView: UIView){
        self.translatesAutoresizingMaskIntoConstraints = false
        superView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[subview]-0-|", options: NSLayoutConstraint.FormatOptions.directionLeadingToTrailing, metrics:nil, views:["subview":self]))
        superView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[subview]-0-|", options: NSLayoutConstraint.FormatOptions.directionLeadingToTrailing, metrics:nil, views:["subview":self]))
    }
}

@IBDesignable class RoundedButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        updateCornerRadius()
    }
    
    @IBInspectable var rounded: Bool = false {
        didSet {
            updateCornerRadius()
        }
    }
    
    func updateCornerRadius() {
        layer.cornerRadius = rounded ? frame.size.height / 2 : 0
    }
}

class HidingObserver : UITextField {
    override var isHidden: Bool {
        didSet {
            println_debug("Hide Status changed")
        }
    }
}
