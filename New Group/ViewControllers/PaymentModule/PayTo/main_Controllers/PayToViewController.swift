//
//  PayToViewController.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/25/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol ScrollChangeDelegate : class {
    func changeScrollPage(point: Int, withScanObject object: PTScanObject)
    func dismissScanOffer(object: PTScanObject, controller: ScanToPayViewController)
    func scanningFailed()
}

extension ScrollChangeDelegate {
    func dismissScanOffer(object: PTScanObject, controller: ScanToPayViewController){
        
    }
}

class PayToViewController: OKBaseController, PaytoScrollerDelegate, ScrollChangeDelegate, UIPopoverPresentationControllerDelegate {
    
    fileprivate var paytoMenu : PaytoScroller?
    var otherRequirement : (number: String, name: String, amount: String, remark: String, cashInQR: Bool, referenceNumber: String)?
    //var transType : PaytoIDTransactionType = PaytoIDTransactionType.paytoWithID
    var transType = PayToType(type: PayToType.ModuleType.payTo)
    var headerTitle : String = PaytoConstants.headers.title.localized//.safelyWrappingString()
    var headerNewTitle : String = userDef.value(forKey: "lastLoginKey") as? String ?? ""
    var isOfferEnabled: Bool = false
    var offerModel: OffersListModel?
    var offerListModel: [OffersListModel]?
    var offerHeader : String = "Offers"
    let menu = DropDown()
    var fromMerchant: Bool? = false
    var indexToShowData = 0
    var userInfo: Dictionary<String, Any> = [:]
    var searchActive: Bool = false
    //Tushar
    //This will be only used for two condition from new map view
    var isComingFromMapAndMakePayment = false
    
    var titlesRightHeader : [String] = [String]()
    var presentedFrom: String?
    enum From {
        case favorite, Recent, none
    }
    var fromScreen = From.Recent
    var repeatPay = false
    
    var currentViewcontroller = UIViewController()
    
    //
    var sideview = UIView()
    
    let fullview = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
    
    var compareStr = String()
    var inboxSearchBar = UISearchBar()
    
    var moreTapped = false
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
       

        
        println_debug(offerListModel)
        NotificationCenter.default.addObserver(self, selector: #selector(backButtonAction), name: NSNotification.Name(rawValue: "RequestMoneyHomeTapAction"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(pop(notification:)), name: .popToDashboard, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showParentView), name: NSNotification.Name(rawValue: "MapNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideAndClearSearch), name: NSNotification.Name(rawValue: "ClearAndHideSearchBar"), object: nil)
        
        MyNumberTopup.theme = UIColor.orange
        geoLocManager.startUpdateLocation()
        self.titlesRightHeader = ["Add Money".localized]
        PaytoConstants.global.navigation = self.navigationController
        //checking the type and allocing the view controller here
     
        self.allocControllers { [weak self](controllers) in
            guard let weakself = self else { return }
           
            var parameters = [String : Any]()
            if self?.transType.type == .transferTo {
                parameters = [PaytoScrollerOptionMenuItemSeparatorWidth: screenWidth, PaytoScrollerOptionMenuItemSeparatorPercentageHeight: 0,
                              PaytoScrollerOptionScrollMenuBackgroundColor: UIColor.white,
                              PaytoScrollerOptionSelectedMenuItemLabelColor: UIColor.orange,
                              PaytoScrollerOptionViewBackgroundColor: UIColor.white,
                              PaytoScrollerOptionSelectionIndicatorColor: UIColor.black,
                              PaytoScrollerOptionUnselectedMenuItemLabelColor:UIColor.darkText,
                              PaytoScrollerOptionMenuHeight: 0,
                              PaytoScrollerOptionMenuItemWidth: screenWidth,
                              PaytoScrollerOptionEnableHorizontalBounce: true,
                              PaytoScrollerOptionMenuItemWidthBasedOnTitleTextWidth: true,
                              PaytoScrollerOptionAddBottomMenuHairline: true,
                              PaytoScrollerOptionBottomMenuHairlineColor: UIColor.gray,
                              PaytoScrollerOptionUseMenuLikeSegmentedControl: true,
                              PaytoScrollerOptionMenuItemFont: "Zawgyi-One"] as [String : Any]
            } else if self?.transType.type == .cashIn || self?.transType.type == .cashOut  {
                parameters = [PaytoScrollerOptionMenuItemSeparatorWidth: screenWidth / 2.0, PaytoScrollerOptionMenuItemSeparatorPercentageHeight: 0.1,
                              PaytoScrollerOptionScrollMenuBackgroundColor: UIColor.white,
                              PaytoScrollerOptionSelectedMenuItemLabelColor: UIColor.orange,
                              PaytoScrollerOptionViewBackgroundColor: UIColor.white,
                              PaytoScrollerOptionSelectionIndicatorColor: UIColor.black,
                              PaytoScrollerOptionUnselectedMenuItemLabelColor:UIColor.darkText,
                              PaytoScrollerOptionMenuHeight: 50,
                              PaytoScrollerOptionMenuItemWidth: screenWidth,
                              PaytoScrollerOptionEnableHorizontalBounce: true,
                              PaytoScrollerOptionMenuItemWidthBasedOnTitleTextWidth: true,
                              PaytoScrollerOptionAddBottomMenuHairline: true,
                              PaytoScrollerOptionBottomMenuHairlineColor: UIColor.gray,
                              PaytoScrollerOptionUseMenuLikeSegmentedControl: true,
                              PaytoScrollerOptionMenuItemFont: "Zawgyi-One"] as [String : Any]
                
            } else if self?.transType.type == .cashInFree || self?.transType.type == .cashOutFree {
                parameters = [PaytoScrollerOptionMenuItemSeparatorWidth: screenWidth / 2.0, PaytoScrollerOptionMenuItemSeparatorPercentageHeight: 0.1,
                              PaytoScrollerOptionScrollMenuBackgroundColor: UIColor.white,
                              PaytoScrollerOptionSelectedMenuItemLabelColor: UIColor.orange,
                              PaytoScrollerOptionViewBackgroundColor: UIColor.white,
                              PaytoScrollerOptionSelectionIndicatorColor: UIColor.black,
                              PaytoScrollerOptionUnselectedMenuItemLabelColor:UIColor.darkText,
                              PaytoScrollerOptionMenuHeight: 0,
                              PaytoScrollerOptionMenuItemWidth: screenWidth,
                              PaytoScrollerOptionEnableHorizontalBounce: true,
                              PaytoScrollerOptionMenuItemWidthBasedOnTitleTextWidth: true,
                              PaytoScrollerOptionAddBottomMenuHairline: true,
                              PaytoScrollerOptionBottomMenuHairlineColor: UIColor.gray,
                              PaytoScrollerOptionUseMenuLikeSegmentedControl: true,
                              PaytoScrollerOptionMenuItemFont: "Zawgyi-One"] as [String : Any]
                
            } else {
                //this will work for normal payto
                parameters = [PaytoScrollerOptionMenuItemSeparatorWidth: 0, PaytoScrollerOptionMenuItemSeparatorPercentageHeight: 0.1,
                              PaytoScrollerOptionScrollMenuBackgroundColor: UIColor.white,
                              PaytoScrollerOptionSelectedMenuItemLabelColor: UIColor.orange,
                              PaytoScrollerOptionViewBackgroundColor: UIColor.white,
                              PaytoScrollerOptionSelectionIndicatorColor: UIColor.black,
                              PaytoScrollerOptionUnselectedMenuItemLabelColor:UIColor.darkText,
                              PaytoScrollerOptionMenuHeight: 50,
                              PaytoScrollerOptionEnableHorizontalBounce: true,
                              PaytoScrollerOptionMenuItemWidthBasedOnTitleTextWidth: true,
                              PaytoScrollerOptionAddBottomMenuHairline: true,
                              PaytoScrollerOptionBottomMenuHairlineColor: UIColor.gray,
                              PaytoScrollerOptionUseMenuLikeSegmentedControl: false,
                              PaytoScrollerOptionMenuItemFont: "Zawgyi-One"] as [String : Any]
            }
            
            if weakself.isOfferEnabled {
                parameters = [PaytoScrollerOptionMenuItemSeparatorWidth: 0, PaytoScrollerOptionMenuItemSeparatorPercentageHeight: 0.1,
                              PaytoScrollerOptionScrollMenuBackgroundColor: UIColor.white,
                              PaytoScrollerOptionSelectedMenuItemLabelColor: UIColor.orange,
                              PaytoScrollerOptionViewBackgroundColor: UIColor.white,
                              PaytoScrollerOptionSelectionIndicatorColor: UIColor.black,
                              PaytoScrollerOptionUnselectedMenuItemLabelColor:UIColor.clear,
                              PaytoScrollerOptionMenuHeight: 0.00,
                              PaytoScrollerOptionEnableHorizontalBounce: true,
                              PaytoScrollerOptionMenuItemWidthBasedOnTitleTextWidth: true,
                              PaytoScrollerOptionAddBottomMenuHairline: true,
                              PaytoScrollerOptionBottomMenuHairlineColor: UIColor.gray,
                              PaytoScrollerOptionUseMenuLikeSegmentedControl: false,
                              PaytoScrollerOptionMenuItemFont: "Zawgyi-One"] as [String : Any]
            }
            
            weakself.paytoMenu?.menuItemFont = UIFont(name: "Zawgyi-One", size: 13.0)
          
                weakself.paytoMenu = PaytoScroller(viewControllers: controllers, frame: weakself.view.bounds, options: parameters)
            
            
            weakself.paytoMenu?.delegate = self
            
            weakself.paytoMenu?.scrollingEnable(false)
            
            if let menu = weakself.paytoMenu {
                weakself.view.addSubview(menu.view)
            }
            weakself.paytoMenu?.scrollingEnable(true)
        }
        
        if !isOfferEnabled {
            self.adaptiveLeftViewButtonSetup()
        }
        
        notfDef.addObserver(forName:NSNotification.Name(rawValue: "OffersHeaderTitle"), object:nil, queue:nil, using:displayOffersHeader)
        notfDef.addObserver(forName:NSNotification.Name(rawValue: "PayToStateDivision"), object:nil, queue:nil, using:reloadHeaderData)
        NotificationCenter.default.addObserver(self, selector: #selector(PayToViewController.facePayCheck), name: NSNotification.Name(rawValue: "ReloadNotificationVC"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PayToViewController.facePayCheck), name: NSNotification.Name(rawValue: "ReloadNotificationVCRepeat"), object: nil)
        
        
        
//        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        menu.hide()
    }
    
    
    @objc func appMovedToBackground() {
        performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
    }
    
    
    @objc func facePayCheck() {
        self.repeatPay = true
    }
    
    func reloadHeaderData(notification:Notification) -> Void {
        userDef.set(nil, forKey: "PTMsortBy")
        userDef.set(nil, forKey: "PTMnearBy")
        
        if currentViewcontroller is MapPayToViewController {
            //print("\(currentinde)")
            println_debug("reloadHeaderData")
            headerNewTitle = notification.userInfo?["Name"] as? String ?? ""
            //self.setNavigation()
        } else {
            self.setOldNavigation()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        if isOfferEnabled {
            self.title = offerHeader.localized
        } else {
            self.title = self.headerTitle
        }
         self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        //self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: PaytoConstants.global.zawgyiFont, NSAttributedString.Key.foregroundColor: UIColor.red]
        self.backButton()
        
        guard let payto  = self.paytoMenu?.controllerArray.first as? PaySendViewController else { return }
        guard  payto.mainTableView != nil else { return }
        payto.mainTableView.reloadData()
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        appDel.floatingButtonControl?.window.isHidden = true
        
        if(appDel.currentLanguage == "uni")
        {
            sideview = UIView(frame: CGRect(x: screenWidth - 250, y: 0, width: 250, height: 200))
        }
            
        else{
            sideview = UIView(frame: CGRect(x: screenWidth - 140, y: 0, width: 200, height: 200))
        }
        
        
    }
    
    //MARK:- Post Notification Offers
    func displayOffersHeader(notification:Notification) -> Void {
        
        let dict = notification.object as! NSDictionary
        let receivednumber = dict["number"] as? String ?? ""
        let arrOffers = dict["arrOffer"] as? [OffersListModel]
        println_debug(receivednumber)
        
        self.title = offerHeader.localized
        self.navigationItem.rightBarButtonItems = nil
        
        self.paytoMenu?.controllerArray.removeAll()
        
        offerListModel = arrOffers
        offerModel = offerListModel?[0]
        isOfferEnabled = true
        
        self.allocControllers { [weak self](controllers) in
            guard let weakself = self else { return }
            var parameters = [String : Any]()
            parameters = [PaytoScrollerOptionMenuItemSeparatorWidth: 0, PaytoScrollerOptionMenuItemSeparatorPercentageHeight: 0.1,
                          PaytoScrollerOptionScrollMenuBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectedMenuItemLabelColor: UIColor.orange,
                          PaytoScrollerOptionViewBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectionIndicatorColor: UIColor.black,
                          PaytoScrollerOptionUnselectedMenuItemLabelColor:UIColor.clear,
                          PaytoScrollerOptionMenuHeight: 0.00,
                          PaytoScrollerOptionEnableHorizontalBounce: true,
                          PaytoScrollerOptionMenuItemWidthBasedOnTitleTextWidth: true,
                          PaytoScrollerOptionAddBottomMenuHairline: true,
                          PaytoScrollerOptionBottomMenuHairlineColor: UIColor.gray,
                          PaytoScrollerOptionUseMenuLikeSegmentedControl: false,
                          PaytoScrollerOptionMenuItemFont: "Zawgyi-One"] as [String : Any]
            
            weakself.paytoMenu = PaytoScroller(viewControllers: controllers, frame: weakself.view.bounds, options: parameters)
            
            if let menu = weakself.paytoMenu {
                weakself.view.addSubview(menu.view)
            }
        }
        
        if let payto = self.paytoMenu?.controllerArray.first as? PaySendViewController {
            payto.offerUpdateThroughScan(number: receivednumber)
        }
        
        notfDef.post(name:Notification.Name(rawValue:"OffersReloadUI"),
                     object: nil,
                     userInfo: ["text": receivednumber])
    }
    
    private func adaptiveLeftViewButtonSetup() -> Void {
        
        if transType.type == .cashIn || transType.type == .cashOut || transType.type == .cashInFree || transType.type == .cashOutFree || transType.type == .transferTo  {
            self.navigationItem.rightBarButtonItems = nil
            self.navigationItem.titleView?.isHidden = true
        } else {
            let sideMenuButton = UIButton(frame: .init(x: 0, y: 0, width: 25, height: 25))
            sideMenuButton.setBackgroundImage(#imageLiteral(resourceName: "menu_white_payto"), for: .normal)
            let rightBarButton = UIBarButtonItem.init(customView: sideMenuButton)
            self.navigationItem.rightBarButtonItems = [rightBarButton]
            
            menu.anchorView = rightBarButton
            menu.dataSource = self.titlesRightHeader
            DropDown.setupDefaultAppearance()
            menu.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
            menu.customCellConfiguration = nil
            
            sideMenuButton.addTargetClosure { [weak self](sender) in
                DispatchQueue.main.async {
                    guard let weakSelf = self else { return }
                    weakSelf.menu.dataSource = weakSelf.titlesRightHeader
                     weakSelf.menu.show()
                }
            }
            
            menu.selectionAction = { [weak self] (index: Int, item: String) in
                if self?.transType.type == .cashIn || self?.transType.type == .cashInFree {
                    self?.dismiss(animated: true, completion: nil)
                } else {
                    if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "AddWithdrawMoneyVC") as? AddWithdrawMoneyVC {
                        addWithdrawView.screenFrom.0 = "DashBoard/Payto"
                        addWithdrawView.screenFrom.1 = "Cash In"
                        self?.navigationController?.pushViewController(addWithdrawView, animated: true)
                    }
                }
            }
        }
    }
    
    @objc func locationButtonAction(_ sender: UIButton) {
        println_debug("tap call on current location view")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MapPayToLocation"), object: nil)
    }
    
    //This notification will only work when coming from map and male payment from new map ui
    
    @objc func pop(notification: NSNotification) {
        guard let value = notification.userInfo!["isComingFromMap"] else { return }
        self.isComingFromMapAndMakePayment = value as! Bool
        //        print(isComingFromMapAndMakePayment)
    }
    
    @objc func backButtonAction() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    private func backButton() {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "tabBarBack"), for: UIControl.State.normal)
        button.tintColor = UIColor.white
        button.sizeToFit()
        button.addTargetClosure { [weak self](button) in
            self?.callUnwindSegue()
        }
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    deinit {
        println_debug(PayToViewController.self)
        geoLocManager.stopUpdateLocation()
    }
    
    func callUnwindSegue () {
        self.view.endEditing(true)
        if transType.type == .cashOut || transType.type == .cashIn || transType.type == .cashInFree || transType.type == .cashOutFree {
            self.dismiss(animated: true, completion: nil)
            //performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
        } else {
            if let merType = fromMerchant, merType == true {
                if isComingFromMapAndMakePayment {
                    performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
                    NotificationCenter.default.post(name: .goDashboardScreen, object: nil)
                } else {
//                    self.dismiss(animated: true, completion: nil)
                    performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
                }
            } else if fromScreen == From.favorite {
                self.dismiss(animated: true, completion: nil)
//                performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
            } else if presentedFrom == "FACE_PAY_ID" {
                if repeatPay == true {
                    performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
                } else {
                    self.dismiss(animated: true, completion: nil)
             //       performSegue(withIdentifier: "unwindSegueToVC1", sender: self)

                }
            } else if fromScreen == From.Recent{
                self.dismiss(animated: true, completion: nil)
//                performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
            } else {
                performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
            }
        }
    }
    
    //MARK:- Menu Setup
    func allocControllers(completion :@escaping (_ controllers: [UIViewController]) -> Void) {
        var viewControllers : [UIViewController] = []
        guard let first  = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "firstVC") as? PaySendViewController else { return }
        first.transType        = self.transType
        first.otherRequirement = otherRequirement
        first.isOfferEnabled   = isOfferEnabled
        first.offerModel       = offerModel
        first.offerListModel = offerListModel
        
        guard let second = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "secondVC") as? ScanToPayViewController else { return }
        second.delegate = self
        second.transType        = self.transType
        
        guard let third  = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "thirdVC") as? MapPayToViewController else { return }
        guard let fourth = UIStoryboard(name: "PendingRequestSB", bundle: nil).instantiateViewController(withIdentifier: "pending_approval_request_result") as? PendingApprovalRequestVC else { return }
        fourth.selectedItemIndex = 1
        fourth.navigationObject = self.navigationController
        guard let fifth = UIStoryboard(name: "PendingRequestSB", bundle: nil).instantiateViewController(withIdentifier: "pending_request_result") as? PendingRequestVC else { return }
        fifth.selectedItemIndex = 1
        fifth.navigationObject = self.navigationController
        
        let cicoStory = UIStoryboard.init(name: cashInOutViewControllers.cashinoutStoryBoard, bundle: nil)
        guard let sixth = cicoStory.instantiateViewController(withIdentifier: cashInOutViewControllers.viewControllers.cioHomeVC) as? CIOHomeViewController else { return }
        sixth.navigationFromPayto = self.navigationController
        //Menu title selection
        
        if transType.categoryType == .toll || transType.categoryType == .bus || transType.categoryType == .ferry {
            
            first.title  = PaytoConstants.headers.controllersTitles.mobileNumber.localized
            second.title = PaytoConstants.headers.controllersTitles.scan.localized
            third.title  = PaytoConstants.headers.controllersTitles.map.localized
            fourth.title = PaytoConstants.headers.controllersTitles.recievedRequest.localized
            fifth.title  = PaytoConstants.headers.controllersTitles.sentRequest.localized
            
            viewControllers.append(first)
            viewControllers.append(second)
            viewControllers.append(third)
            
        } else if transType.type == .transferTo {
            first.title  = PaytoConstants.headers.controllersTitles.payto.localized
            viewControllers.append(first)
        } else if transType.type == .cashIn || transType.type == .cashOut {
            first.title  = PaytoConstants.headers.controllersTitles.mobileNumber.localized
            if self.transType.type == .cashIn {
                sixth.screenName = "Cash In"
                sixth.title  = "Request Cash In_without_newline".localized
            } else {
                sixth.screenName = "Cash Out"
                sixth.title  = "Request Cash Out_without_newline".localized
            }
            viewControllers.append(first)
            viewControllers.append(sixth)
        } else if  transType.type == .cashInFree || transType.type == .cashOutFree {
            first.title  = PaytoConstants.headers.controllersTitles.mobileNumber.localized
            //            if transType.type == .cashIn || transType.type == .cashInFree {
            //                second.title = "Scan QR".localized
            //            } else {
            //                second.title = PaytoConstants.headers.controllersTitles.scan.localized
            //            }
            
            viewControllers.append(first)
            //viewControllers.append(second)
        }  else {
            first.title  = PaytoConstants.headers.controllersTitles.payto.localized
            second.title = PaytoConstants.headers.controllersTitles.scan.localized
            third.title  = PaytoConstants.headers.controllersTitles.map.localized
            fourth.title = PaytoConstants.headers.controllersTitles.recievedRequest.localized
            fifth.title  = PaytoConstants.headers.controllersTitles.sentRequest.localized
            
            viewControllers.append(first)
            viewControllers.append(second)
            viewControllers.append(third)
            viewControllers.append(fourth)
            viewControllers.append(fifth)
        }
        
        if isOfferEnabled {
            first.title  = offerHeader.localized
            viewControllers.removeAll()
            viewControllers.append(first)
        }
        
        completion(viewControllers)
    }
    
    func setCashNavigation() {
        self.navigationItem.titleView = nil
        self.navigationItem.titleView?.isHidden = true
        self.title = self.headerTitle
    }
    
    func setOldNavigation() {
        self.navigationItem.titleView = nil
        self.navigationItem.titleView?.isHidden = true
        self.title = self.headerTitle
        self.adaptiveLeftViewButtonSetup()
    }
    
    
    func setRequestCashInNaviagtion() {
        self.navigationItem.titleView?.isHidden = false
        self.setNavigation(cashIn: true)
        //        self.title = self.headerTitle
        let btn = UIButton(type: .custom)
        btn.setImage(UIImage(named: "chatMore"), for: .normal)
        btn.addTargetClosure { (btn) in
            self.currentMenuAction()
        }
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: btn)]
    }
    
    func currentMenuAction() {
        
        let cicoStory = UIStoryboard.init(name: cashInOutViewControllers.cashinoutStoryBoard, bundle: nil)
        let popoverContent = cicoStory.instantiateViewController(withIdentifier: "CIOTransactionListPopover_ID") as? CIOTransactionListPopover
        popoverContent?.delegate = self
        popoverContent?.modalPresentationStyle = .popover
        let popover = popoverContent?.popoverPresentationController
        popoverContent?.preferredContentSize = CGSize(width: 200.0, height: 45.0)
        popover?.delegate = self
        popover?.sourceView = self.view
        popover?.permittedArrowDirections = .init(rawValue: 0)
        popover?.sourceRect = CGRect(x: self.view.frame.size.width, y: 0, width: 0, height: 0)
        self.present(popoverContent!, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func setNavigation(cashIn: Bool = false) {
        self.navigationItem.rightBarButtonItems = nil
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        var width = UIScreen.main.bounds.size.width - 30
        if cashIn {
            width = UIScreen.main.bounds.size.width - 80
        }
        let titleViewSize = CGSize(width: width, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let locButton = UIButton(type: .custom)
        locButton.layer.cornerRadius = 15
        locButton.layer.borderColor = UIColor.white.cgColor
        locButton.layer.borderWidth = 1.0
        locButton.addTarget(self, action: #selector(locationButtonAction), for: .touchUpInside)
        locButton.frame = CGRect(x: 0, y: ypos, width: navTitleView.frame.size.width-30, height: 30)
        navTitleView.addSubview(locButton)
        
        let label = MarqueeLabel()
        label.text = headerNewTitle + "  "
        label.font =  UIFont(name: appFont, size: appFontSize)
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.frame = CGRect(x: 0, y: ypos, width: width - 30, height: 30)
        navTitleView.addSubview(label)
        
        if !cashIn {
            var imageView : UIImageView
            imageView  = UIImageView(frame:CGRect(x: navTitleView.frame.size.width-60, y: ypos + 3, width:30 , height: 30));
            imageView.image = UIImage(named:"arrow_down.png")
            navTitleView.addSubview(imageView)
        }
        self.navigationItem.titleView = navTitleView
    }
    
    @objc func showParentView(_ notification: Notification) {
        paytoMenu?.move(toPage: 0)
        //self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Menu Delegates Callback
    func willMove(toPage controller: UIViewController!, index: Int) {
        
        if let payto = controller as? PaySendViewController {
            payto.addNewObject()
        }
        currentViewcontroller = controller
        switch index {
        case 0:
            self.titlesRightHeader = ["Add Money".localized]
            self.setOldNavigation()
            println_debug("case zero")
        case 1:
            if currentViewcontroller is CIOHomeViewController {
                self.setRequestCashInNaviagtion()
            } else {
                self.titlesRightHeader = ["Add Money".localized]
                self.setOldNavigation()
                println_debug("case one")
            }
        case 2:
            print("***********")
            //            self.titlesRightHeader = ["Add Money".localized]
            //  self.setNavigation()
        //println_debug("case two")
        case 3, 4:
            indexToShowData = index
            //            self.titlesRightHeader = ["Add Money".localized]
            //            self.setOldNavigation()
            self.inboxRightBar()
            println_debug("case three")
            
        default:
            self.titlesRightHeader = ["Add Money".localized]
            self.setOldNavigation()
            println_debug("default")
        }
    }
    
    func didMove(toPage controller: UIViewController!, index: Int) {
        if index == 2 {
            //            self.setNavigation()
            //            currentViewcontroller = controller
            let storyboard : UIStoryboard = UIStoryboard(name: "NBSMain", bundle: nil)
            let vc : NearByServicesMainVC = storyboard.instantiateViewController(withIdentifier: "NearByServicesMainVC") as! NearByServicesMainVC
            vc.isComingFromMap = true
            //   self.navigationController?.present(vc, animated: true, completion: nil)
            
            self.presentDetail(vc)
            
            //          let storyboard : UIStoryboard = UIStoryboard(name: "Map", bundle: nil)
            //         let vc : MapPayToVC = storyboard.instantiateViewController(withIdentifier: "MapPayToVC") as! MapPayToVC
            //          let navigationController = UINavigationController(rootViewController: vc)
            //        self.presentDetail(navigationController)
            //weak.present(navigationController, animated: true, completion: nil)
        } else if index == 3 || index == 4{
            indexToShowData = index
            self.inboxRightBar()
        } else {
            if currentViewcontroller is CIOHomeViewController {
                
            } else {
                self.setOldNavigation()
            }
            currentViewcontroller = controller
        }
        
        if let scan = controller as? ScanToPayViewController {
            self.view.endEditing(true)
            if let qrCapture = scan.captureSession {
                DispatchQueue.global(qos: .background).async {
                    qrCapture.startRunning()
                }
            }
        }
    }
    
    func changeScrollPage(point: Int, withScanObject object: PTScanObject) {
        if !isOfferEnabled {
            self.paytoMenu?.move(toPage: 0)
            if let payto = self.paytoMenu?.controllerArray.first as? PaySendViewController {
                payto.selfUpdateObject(object: object)
            }
        }
    }
    
    func dismissScanOffer(object: PTScanObject, controller: ScanToPayViewController) {
        if isOfferEnabled {
            if let payto = self.paytoMenu?.controllerArray.first as? PaySendViewController {
                payto.offerUpdateThroughScan(number: object.agentNo)
            }
        }
    }
    
    func updatePayToObject(vc: PayToUIViewController) {
        if let payto = self.paytoMenu?.controllerArray.first as? PaySendViewController {
            payto.backToSinglePayment(vc: vc)
        }
    }
    
    func scanningFailed() {
        
    }
    
    
    func inboxRightBar() {
        
        self.navigationItem.titleView?.isHidden = false
        self.navigationItem.rightBarButtonItems = nil
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        self.title = self.headerTitle
        let btn = UIButton(type: .custom)
        btn.setImage(UIImage(named: "r_search"), for: .normal)
        btn.addTargetClosure { (btn) in
            self.searchInBoxAction()
        }
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "menu_white_payto"), for: .normal)
        btn1.addTargetClosure { (btn) in
            self.moreInBoxAction()
        }
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: btn1), UIBarButtonItem(customView: btn)]
    }
    
    @objc func moreInBoxAction() {
        
        if moreTapped {
            inboxSearchBar.isHidden = true
            self.navigationItem.titleView = nil
            fullview.isHidden = true
            sideview.isHidden = true
            moreTapped = false
            return
        }
        
        moreTapped = true
        
        //   let sideview = UIView(frame: CGRect(x: screenWidth - 140, y: 0, width: 140, height: 200))
        var locaArray = [String]()
        
        inboxSearchBar.isHidden = true
        self.navigationItem.titleView = nil
        
        if indexToShowData == 3 {
            
            if(appDel.currentLanguage == "uni") || (appDel.currentLanguage == "my")
            {
                sideview = UIView(frame: CGRect(x: screenWidth - 250, y: 0, width: 250, height: 200))
            }
                
            else{
                sideview = UIView(frame: CGRect(x: screenWidth - 185, y: 0, width: 220, height: 200))
            }
            
            sideview.isHidden = false
            fullview.isHidden = false
            locaArray = ["All", "Pending Approval", "Request Approved", "Cancelled"]
            fullview.backgroundColor = .clear
            self.view.addSubview(fullview)
            self.view.bringSubviewToFront(fullview)
            
        }
        if indexToShowData == 4 {
            
            sideview = UIView(frame: CGRect(x: screenWidth - 185, y: 0, width: 200, height: 200))
            
            sideview.isHidden = false
            fullview.isHidden = false
            locaArray =  ["Pending", "Successful", "Reject", "Scheduled"]
            
            fullview.backgroundColor = .clear
            self.view.addSubview(fullview)
            self.view.bringSubviewToFront(fullview)
        }
        
        if compareStr == "0"
        {
            sideview.isHidden = false
            compareStr = "1"
            
        }
        else if compareStr == "1"
        {
            // sideview.isHidden = true
            compareStr = "0"
        }
        
        
        sideview.backgroundColor = .white
        let myButton = UIButton(type: UIButton.ButtonType.custom) // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
        myButton.backgroundColor = UIColor.white
        myButton.setTitle(locaArray[0].localized, for: .normal)
        myButton.tag = 0
        myButton.titleLabel?.font = UIFont(name: "Zawgyi-One", size: appButtonSize)
        myButton.setTitleColor(UIColor.black, for: .normal)
        myButton.frame = CGRect(x: 10, y: 10, width: 250, height: 40)
        myButton.addTarget(self, action: #selector(calllInBoxAction(_:)), for: .touchUpInside)
        myButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        sideview.addSubview( myButton)
        
        let myButton1 = UIButton(type: UIButton.ButtonType.custom) // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
        myButton1.backgroundColor = UIColor.white
        myButton1.setTitle(locaArray[1].localized, for: .normal)
        myButton1.tag = 1
        myButton1.titleLabel?.font = UIFont(name: "Zawgyi-One", size: appButtonSize)
        myButton1.setTitleColor(UIColor.black, for: .normal)
        myButton1.frame = CGRect(x: 10, y: 60, width: 250, height: 40)
        myButton1.addTarget(self, action: #selector(calllInBoxAction(_:)), for: .touchUpInside)
        myButton1.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        sideview.addSubview( myButton1)
        
        let myButton2 = UIButton(type: UIButton.ButtonType.custom) // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
        myButton2.backgroundColor = UIColor.white
        myButton2.setTitle(locaArray[2].localized, for: .normal)
        myButton2.tag = 2
        myButton2.titleLabel?.font = UIFont(name: "Zawgyi-One", size: appButtonSize)
        myButton2.setTitleColor(UIColor.black, for: .normal)
        myButton2.frame = CGRect(x: 10, y: 110, width: 250, height: 40)
        myButton2.addTarget(self, action:#selector(calllInBoxAction(_:)), for: .touchUpInside)
        myButton2.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        sideview.addSubview( myButton2)
        
        let myButton3 = UIButton(type: UIButton.ButtonType.custom) // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
        myButton3.backgroundColor = UIColor.white
        myButton3.setTitle(locaArray[3].localized, for: .normal)
        myButton3.tag = 3
        myButton3.titleLabel?.font = UIFont(name: "Zawgyi-One", size: appButtonSize)
        myButton3.setTitleColor(UIColor.black, for: .normal)
        myButton3.frame = CGRect(x: 10, y: 160, width: 250, height: 40)
        myButton3.addTarget(self, action: #selector(calllInBoxAction(_:)), for: .touchUpInside)
        myButton3.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        sideview.addSubview( myButton3)        
        self.view.addSubview(sideview)
        self.view.bringSubviewToFront(sideview)
    }
    
    @objc func hideAndClearSearch() {
        inboxSearchBar.text = ""
        self.navigationItem.titleView = nil
        self.title = self.headerTitle
        inboxSearchBar.isHidden = true
        searchActive = false
    }
    
  
    
    func setupSearchBar() {
        inboxSearchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
        inboxSearchBar.showsCancelButton = false
        inboxSearchBar.delegate = self
        inboxSearchBar.tintColor = UIColor.blue
        inboxSearchBar.placeholder = "Search".localized
        self.navigationItem.titleView = inboxSearchBar
        if let searchTextField = inboxSearchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: "Zawgyi-One", size: 18) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
                searchTextField.keyboardType = .asciiCapable
            }
        }
        let view = self.inboxSearchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = UIColor.gray
            }
        }
        inboxSearchBar.becomeFirstResponder()
    }
    
    @objc func searchInBoxAction() {
        sideview.isHidden = true
        fullview.isHidden = true
        
        if !inboxSearchBar.isHidden {
            self.navigationItem.titleView = nil
            self.title = self.headerTitle
            inboxSearchBar.isHidden = true
            searchActive = false
            inboxSearchBar.text = ""
            self.searchTextChangeNotification(searchText: "", status: false)
            return
        }
        inboxSearchBar.isHidden = false
        compareStr = "0"
        //   self.navigationTitleViewSetup()
        self.setupSearchBar()
    }
    
    @objc func calllInBoxAction(_ sender: UIButton) {
        if indexToShowData == 3 {
            callNotificationForSideMenuDropDown(index: sender.tag)
        } else {
            callNotificationForSideMenu(index: sender.tag)
        }
        
    }
    fileprivate func callNotificationForSideMenuDropDown(index: Int){
        let passIndex:[String: Int] = ["Title": index + 1]
        NotificationCenter.default.post(name: NSNotification.Name("DashboardVC"), object: nil, userInfo: passIndex)
        sideview.isHidden = true
        fullview.isHidden = true
        compareStr = "0"
    }
    
    fileprivate func callNotificationForSideMenu(index: Int){
        let passIndex:[String: Int] = ["Title2": index+1]
        NotificationCenter.default.post(name: NSNotification.Name("DashboardVCsent"), object: nil, userInfo: passIndex)
        sideview.isHidden = true
        fullview.isHidden = true
        compareStr = "0"
    }
    
    private func searchTextChangeNotification(searchText: String, status: Bool) {
        userInfo.removeAll()
        userInfo["searchText"] = searchText
        userInfo["searchStatus"] = searchActive
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DashboardSearchTextChange"), object: nil, userInfo: userInfo)
    }
}

extension PayToViewController : ViewAllTransactionProtocol{
    func showViewAllTransactionVC() {
        let cicoStory = UIStoryboard.init(name: cashInOutViewControllers.cashinoutStoryBoard, bundle: nil)
        if let vc = cicoStory.instantiateViewController(withIdentifier: cashInOutViewControllers.viewControllers.cioTransHomeVC) as? CIOTransactionHomeViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}



extension PayToViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //        //        searchActive = false
        //        //        self.searchTextChangeNotification(searchText: "", status: false)
        //        //   inboxSearchBar.text = ""
        //        if indexToShowData == 2 {
        //            inboxSearchBar.text = ""
        //        }
        //        else if indexToShowData == 3 {
        //            inboxSearchBar.text = ""
        //        }
        //inboxSearchBar.isHidden = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        self.searchTextChangeNotification(searchText: "", status: false)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if (searchBar.text?.count ?? 0) > 0 {
            searchActive = true
        } else {
            searchActive = false
        }
        self.searchTextChangeNotification(searchText: searchBar.text ?? "", status: false)
    }
    
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//
//
////        if range.location == 0 && text == " " { return false }
////        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
////            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
////            if updatedText.containsEmoji {
////                return false
////            }
////            if searchBar.text?.last == " " && text == " " {
////                return false
////            }
////            if updatedText.count > 50 {
////                return false
////            }
////
////            applySearch(with: updatedText)
////        }
////        return true
//
//        self.searchTextChangeNotification(searchText: searchText, status: true)
//    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if range.location == 0 && text == " " { return false }
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            if updatedText.containsEmoji {
                return false
            }
            if searchBar.text?.last == " " && text == " " {
                return false
            }
            if updatedText.count > 30 {
                return false
            }
            
            self.searchTextChangeNotification(searchText: searchText, status: true)
        }
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            self.searchTextChangeNotification(searchText: searchText, status: true)
        }
    }
    
}
