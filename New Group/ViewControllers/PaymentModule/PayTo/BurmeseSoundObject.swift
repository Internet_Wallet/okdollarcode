//
//  BurmeseSoundObject.swift
//  OK
//
//  Created by Ashish on 4/24/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import AVFoundation

protocol BurmeseSoundObjectDelegate : class {
    func doChangeVoiceImage(on: Bool)
}

class BurmeseSoundObject: NSObject, AVAudioPlayerDelegate, DeviceSoundDelegateBurmeseVoice {
    
    var strDigit    = ""
    var currentSoundIndex = 0
    var audioPlayer: AVAudioPlayer!
    var arrAudio    = Array<String>()
    
    weak var delegate : BurmeseSoundObjectDelegate?
    
    var enableSound = true
    
    override init(){
        currentSoundIndex = 0
    }
    
    func didChangeVoice(status: Bool) {
        if status {
            enableSound = true
            self.delegate?.doChangeVoiceImage(on: true)
        } else {
            enableSound = false
            self.delegate?.doChangeVoiceImage(on: false)
        }
    }
    
    func didChangeVoice() {
        if enableSound {
            enableSound = false
            self.delegate?.doChangeVoiceImage(on: false)
        } else {
            enableSound = true
            self.delegate?.doChangeVoiceImage(on: true)
        }
    }
    
    func mappingDelegate(_ parent: PayToUIViewController) {
        parent.deviceSound = self
    }

    func getBurmeseSoundText(text: String) -> String {
        if appDel.currentLanguage == "uni" {
            if text == "1000" {
                fourDigitsound(strdigit: "1")
                return  "တစ်ထောင်ကျပ် ";
            }else if text == "2000" {
                fourDigitsound(strdigit: "2")
                return  "နှစ်ထောင်ကျပ် ";
            }else if text == "3000" {
                fourDigitsound(strdigit: "3")
                return  "သုံးထောင်ကျပ် ";
            }else if text == "4000" {
                fourDigitsound(strdigit: "4")
                return  "လေးထောင်ကျပ် ";
            }else if text == "5000" {
                fourDigitsound(strdigit: "5")
                return  "ငါးထောင်ကျပ် ";
            }else if text == "6000" {
                fourDigitsound(strdigit: "6")
                return  "ခြောက်ထောင်ကျပ် ";
            }else if text == "7000" {
                fourDigitsound(strdigit: "7")
                return  "ခုနှစ်ထောင်ကျပ် ";
            }else if text == "8000" {
                fourDigitsound(strdigit: "8")
                return  "ရှစ်ထောင်ကျပ် ";
            }else if text == "9000" {
                fourDigitsound(strdigit: "9")
                return  "ကိုးထောင်ကျပ် ";
            } else {
                strDigit = ""
                arrAudio.removeAll()
                
                let strBeforeDecimal = text.components(separatedBy: ".")
                println_debug(strBeforeDecimal)
                println_debug(strBeforeDecimal.count)
                
                if strBeforeDecimal[0] == "" || strBeforeDecimal[0] == "0" || strBeforeDecimal[0] == "00" || strBeforeDecimal[0] == "000"{
                    // logic for .1 , 0.1 , 00.1 issue
                } else {
                    // Logic for 1000-9000 with decimal point
                    if strBeforeDecimal[0] == "1000"{
                        strDigit.append("တစ် ")
                        strDigit.append("ထောင် ")
                        strDigit.append("ကျပ် ")
                        arrAudio.append("1")
                        arrAudio.append("1000_9000")
                        arrAudio.append("Kyat")
                    }else if strBeforeDecimal[0] == "2000"{
                        strDigit.append("နှစ် ")
                        strDigit.append("ထောင် ")
                        strDigit.append("ကျပ် ")
                        arrAudio.append("2")
                        arrAudio.append("1000_9000")
                        arrAudio.append("Kyat")
                    }else if strBeforeDecimal[0] == "3000"{
                        strDigit.append("သုံး ")
                        strDigit.append("ထောင် ")
                        strDigit.append("ကျပ် ")
                        arrAudio.append("3")
                        arrAudio.append("1000_9000")
                        arrAudio.append("Kyat")
                    }else if strBeforeDecimal[0] == "4000"{
                        strDigit.append("လေး ")
                        strDigit.append("ထောင် ")
                        strDigit.append("ကျပ် ")
                        arrAudio.append("4")
                        arrAudio.append("1000_9000")
                        arrAudio.append("Kyat")
                    }else if strBeforeDecimal[0] == "5000"{
                        strDigit.append("ငါး ")
                        strDigit.append("ထောင် ")
                        strDigit.append("ကျပ် ")
                        arrAudio.append("5")
                        arrAudio.append("1000_9000")
                        arrAudio.append("Kyat")
                    }else if strBeforeDecimal[0] == "6000"{
                        strDigit.append("ခြောက် ")
                        strDigit.append("ထောင် ")
                        strDigit.append("ကျပ် ")
                        arrAudio.append("6")
                        arrAudio.append("1000_9000")
                        arrAudio.append("Kyat")
                    }else if strBeforeDecimal[0] == "7000"{
                        strDigit.append("ခုနှစ် ")
                        strDigit.append("ထောင် ")
                        strDigit.append("ကျပ် ")
                        arrAudio.append("7")
                        arrAudio.append("1000_9000")
                        arrAudio.append("Kyat")
                    }else if strBeforeDecimal[0] == "8000"{
                        strDigit.append("ရှစ် ")
                        strDigit.append("ထောင် ")
                        strDigit.append("ကျပ် ")
                        arrAudio.append("8")
                        arrAudio.append("1000_9000")
                        arrAudio.append("Kyat")
                    }else if strBeforeDecimal[0] == "9000"{
                        strDigit.append("ကိုး ")
                        strDigit.append("ထောင် ")
                        strDigit.append("ကျပ် ")
                        arrAudio.append("9")
                        arrAudio.append("1000_9000")
                        arrAudio.append("Kyat")
                    }else {
                        beforeDecimal(decimalString:strBeforeDecimal[0] as NSString )
                    }
                }
                
                if strBeforeDecimal.count > 1 {
                    if strBeforeDecimal[1] == "0" || strBeforeDecimal[1] == "00"{
                        // logic for 23.0 , 245.00 issue
                    }else {
                        afterDecimal(decimalString:strBeforeDecimal[1] as NSString )
                    }
                }
                return printStringAndPlaySound()
            }
        } else {
            if text == "1000" {
                fourDigitsound(strdigit: "1")
                return  "တစ္ေထာင္က်ပ္ ";
            }else if text == "2000" {
                fourDigitsound(strdigit: "2")
                return  "ႏွစ္ေထာင္က်ပ္ ";
            }else if text == "3000" {
                fourDigitsound(strdigit: "3")
                return  "သံုးေထာင္က်ပ္ ";
            }else if text == "4000" {
                fourDigitsound(strdigit: "4")
                return  "ေလးေထာင္က်ပ္ ";
            }else if text == "5000" {
                fourDigitsound(strdigit: "5")
                return  "ငါးေထာင္က်ပ္ ";
            }else if text == "6000" {
                fourDigitsound(strdigit: "6")
                return  "ေျခာက္ေထာင္က်ပ္ ";
            }else if text == "7000" {
                fourDigitsound(strdigit: "7")
                return  "ခုႏွစ္ေထာင္က်ပ္ ";
            }else if text == "8000" {
                fourDigitsound(strdigit: "8")
                return  "ရွစ္ေထာင္က်ပ္ ";
            }else if text == "9000" {
                fourDigitsound(strdigit: "9")
                return  "ကိုးေထာင္က်ပ္ ";
            } else {
                strDigit = ""
                arrAudio.removeAll()
                
                let strBeforeDecimal = text.components(separatedBy: ".")
                println_debug(strBeforeDecimal)
                println_debug(strBeforeDecimal.count)
                
                if strBeforeDecimal[0] == "" || strBeforeDecimal[0] == "0" || strBeforeDecimal[0] == "00" || strBeforeDecimal[0] == "000"{
                    // logic for .1 , 0.1 , 00.1 issue
                } else {
                    // Logic for 1000-9000 with decimal point
                    if strBeforeDecimal[0] == "1000"{
                        strDigit.append("တစ္ ")
                        strDigit.append("ေထာင္ ")
                        strDigit.append("က်ပ္ ")
                        arrAudio.append("1")
                        arrAudio.append("1000_9000")
                        arrAudio.append("Kyat")
                    }else if strBeforeDecimal[0] == "2000"{
                        strDigit.append("ႏွစ္ ")
                        strDigit.append("ေထာင္ ")
                        strDigit.append("က်ပ္ ")
                        arrAudio.append("2")
                        arrAudio.append("1000_9000")
                        arrAudio.append("Kyat")
                    }else if strBeforeDecimal[0] == "3000"{
                        strDigit.append("သံုး ")
                        strDigit.append("ေထာင္ ")
                        strDigit.append("က်ပ္ ")
                        arrAudio.append("3")
                        arrAudio.append("1000_9000")
                        arrAudio.append("Kyat")
                    }else if strBeforeDecimal[0] == "4000"{
                        strDigit.append("ေလး ")
                        strDigit.append("ေထာင္ ")
                        strDigit.append("က်ပ္ ")
                        arrAudio.append("4")
                        arrAudio.append("1000_9000")
                        arrAudio.append("Kyat")
                    }else if strBeforeDecimal[0] == "5000"{
                        strDigit.append("ငါး ")
                        strDigit.append("ေထာင္ ")
                        strDigit.append("က်ပ္ ")
                        arrAudio.append("5")
                        arrAudio.append("1000_9000")
                        arrAudio.append("Kyat")
                    }else if strBeforeDecimal[0] == "6000"{
                        strDigit.append("ေျခာက္ ")
                        strDigit.append("ေထာင္ ")
                        strDigit.append("က်ပ္ ")
                        arrAudio.append("6")
                        arrAudio.append("1000_9000")
                        arrAudio.append("Kyat")
                    }else if strBeforeDecimal[0] == "7000"{
                        strDigit.append("ခုႏွစ္ ")
                        strDigit.append("ေထာင္ ")
                        strDigit.append("က်ပ္ ")
                        arrAudio.append("7")
                        arrAudio.append("1000_9000")
                        arrAudio.append("Kyat")
                    }else if strBeforeDecimal[0] == "8000"{
                        strDigit.append("ရွစ္ ")
                        strDigit.append("ေထာင္ ")
                        strDigit.append("က်ပ္ ")
                        arrAudio.append("8")
                        arrAudio.append("1000_9000")
                        arrAudio.append("Kyat")
                    }else if strBeforeDecimal[0] == "9000"{
                        strDigit.append("ကိုး ")
                        strDigit.append("ေထာင္ ")
                        strDigit.append("က်ပ္ ")
                        arrAudio.append("9")
                        arrAudio.append("1000_9000")
                        arrAudio.append("Kyat")
                    }else {
                        beforeDecimal(decimalString:strBeforeDecimal[0] as NSString )
                    }
                }
                
                if strBeforeDecimal.count > 1 {
                    if strBeforeDecimal[1] == "0" || strBeforeDecimal[1] == "00"{
                        // logic for 23.0 , 245.00 issue
                    }else {
                        afterDecimal(decimalString:strBeforeDecimal[1] as NSString )
                    }
                }
                return printStringAndPlaySound()
            }
        }
    }
    
    
  private func printStringAndPlaySound() -> String{
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            for item in self.arrAudio {
                println_debug("sound array \(item)")
                if  let catSound = Bundle.main.url(forResource: item, withExtension: "mp3") {
                    do {
                        self.audioPlayer = try AVAudioPlayer(contentsOf: catSound)
                        self.audioPlayer.prepareToPlay()
                    } catch {
                        println_debug("Problem in getting File")
                    }
                    if self.enableSound {
                        self.audioPlayer.play()
                        usleep(500000)
                    }
                }
            }
        })
    
     return self.strDigit
    }

    
    func playSound(fileName: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.arrAudio.removeAll()
            if  let catSound = Bundle.main.url(forResource: fileName, withExtension: "m4a") {
                do {
                    self.audioPlayer = try AVAudioPlayer(contentsOf: catSound)
                    self.audioPlayer.prepareToPlay()
                } catch {
                    println_debug("Problem in getting File")
                }
                if self.enableSound {
                    self.audioPlayer.play()
                    usleep(500000)
                }
            }
        })
    }
    
   private func fourDigitsound(strdigit : NSString) {
        
        var arr4digitSound = Array<String>()
        arr4digitSound.append(strdigit as String)
        arr4digitSound.append("1000_9000")
        arr4digitSound.append("Kyat")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            for item in arr4digitSound {
                println_debug("sound array \(item)")
                let catSound = Bundle.main.url(forResource: item, withExtension: "mp3")
                do {
                    self.audioPlayer = try AVAudioPlayer(contentsOf: catSound!)
                    self.audioPlayer.prepareToPlay()
                } catch {
                    println_debug("Problem in getting File")
                }
                
                if self.enableSound {
                    self.audioPlayer.play()
                    usleep(500000)
                }
            }
        })
    }
    
    
    private func beforeDecimal(decimalString : NSString) {
        
        var arrDigit = Array<String>()
        var  num = Int (decimalString as String) ?? 0
        var digit = Int()
        
        if appDel.currentLanguage == "uni" {
            repeat {
                digit = num % 10;
                
                if (digit == 0){
                    arrDigit.append("zero")
                }else if (digit == 1){
                    arrDigit.append("တစ် ")
                }else if (digit == 2){
                    arrDigit.append("နှစ် ")
                }else if (digit == 3){
                    arrDigit.append("သုံး ")
                }else if (digit == 4){
                    arrDigit.append("လေး ")
                }else if (digit == 5){
                    arrDigit.append("ငါး ")
                }else if (digit == 6){
                    arrDigit.append("ခြောက် ")
                }else if (digit == 7){
                    arrDigit.append("ခုနှစ် ")
                }else if (digit == 8){
                    arrDigit.append("ရှစ် ")
                }else if (digit == 9){
                    arrDigit.append("ကိုး ")
                }
                num = num / 10;
            } while (num != 0)
        } else {
            repeat {
                digit = num % 10;
                
                if (digit == 0){
                    arrDigit.append("zero")
                }else if (digit == 1){
                    arrDigit.append("တစ္ ")
                }else if (digit == 2){
                    arrDigit.append("ႏွစ္ ")
                }else if (digit == 3){
                    arrDigit.append("သံုး ")
                }else if (digit == 4){
                    arrDigit.append("ေလး ")
                }else if (digit == 5){
                    arrDigit.append("ငါး ")
                }else if (digit == 6){
                    arrDigit.append("ေျခာက္ ")
                }else if (digit == 7){
                    arrDigit.append("ခုႏွစ္ ")
                }else if (digit == 8){
                    arrDigit.append("ရွစ္ ")
                }else if (digit == 9){
                    arrDigit.append("ကိုး ")
                }
                num = num / 10;
            } while (num != 0)
            
        }
        
        arrDigit.reverse()
        println_debug("arrDigit : \(arrDigit)")
        var arrSecond = Array<String>()
        
        if arrDigit.count > 5 {
            
            for i in 0 ... arrDigit.count - 6 {
                arrSecond.insert(arrDigit[0], at: i)
                arrDigit.remove(at: 0)
            }
            
            printDigit(array: arrSecond, strLakhBool: true)
            printDigit(array: arrDigit, strLakhBool: false)
        } else {
            printDigit(array: arrDigit, strLakhBool: false)
        }
        if appDel.currentLanguage == "uni" {
            strDigit.append("ကျပ် ")
        } else {
            strDigit.append("က်ပ္ ")
        }
        arrAudio.append("Kyat")
        
    }

    
  private func afterDecimal(decimalString : NSString) {
        
        var arrDigit = Array<String>()
        var  num = Int(decimalString as String) ?? 0
        var digit = Int()
      if appDel.currentLanguage == "uni" {
        repeat {
            digit = num % 10;
            
            if (digit == 0){
                arrDigit.append("zero")
            }else if (digit == 1){
                arrDigit.append("တစ် ")
            }else if (digit == 2){
                arrDigit.append("နှစ် ")
            }else if (digit == 3){
                arrDigit.append("သုံး ")
            }else if (digit == 4){
                arrDigit.append("လေး ")
            }else if (digit == 5){
                arrDigit.append("ငါး ")
            }else if (digit == 6){
                arrDigit.append("ခြောက် ")
            }else if (digit == 7){
                arrDigit.append("ခုနှစ် ")
            }else if (digit == 8){
                arrDigit.append("ရှစ် ")
            }else if (digit == 9){
                arrDigit.append("ကိုး ")
            }
            num = num / 10;
        } while (num != 0)
      } else {
        repeat {
            digit = num % 10;
            
            if (digit == 0){
                arrDigit.append("zero")
            }else if (digit == 1){
                arrDigit.append("တစ္ ")
            }else if (digit == 2){
                arrDigit.append("ႏွစ္ ")
            }else if (digit == 3){
                arrDigit.append("သံုး ")
            }else if (digit == 4){
                arrDigit.append("ေလး ")
            }else if (digit == 5){
                arrDigit.append("ငါး ")
            }else if (digit == 6){
                arrDigit.append("ေျခာက္ ")
            }else if (digit == 7){
                arrDigit.append("ခုႏွစ္ ")
            }else if (digit == 8){
                arrDigit.append("ရွစ္ ")
            }else if (digit == 9){
                arrDigit.append("ကိုး ")
            }
            num = num / 10;
        } while (num != 0)
    }
        
        arrDigit.reverse()
        
        var arrSecond = Array<String>()
        
        if arrDigit.count > 5 {
            
            for i in 0 ... arrDigit.count - 6 {
                arrSecond.insert(arrDigit[0], at: i)
                arrDigit.remove(at: 0)
            }
            
            printDigit(array: arrSecond, strLakhBool: true)
            printDigit(array: arrDigit, strLakhBool: false)
        }else {
            printDigit(array: arrDigit, strLakhBool: false)
        }
    if appDel.currentLanguage == "uni" {
        strDigit.append("ပြား ")
    } else {
        strDigit.append("ျပား ")
    }
        arrAudio.append("Pya")
        println_debug("afterDecimal string is: \(strDigit) and sound array :\(arrAudio)")
    }
    
    
    private func printDigit(array : Array<String>, strLakhBool : Bool)  {
        
        println_debug("printDigit")
        
        var count = Int()
        count = array.count
        
        if appDel.currentLanguage == "uni" {
            for i in 0 ... array.count-1 {
                if count == 5 {
                    
                    if i == 0 {
                        println_debug(array[i])
                        if array[i] == "zero" {
                        }else {
                            strDigit.append(array[i])
                            stringWithWhiteSpace(pString:"သောင်း ")
                            digitSound(digitsound: array[i])
                            unitSound(unitsound:"သောင်း ")
                        }
                    }else if i == 1 {
                        println_debug(array[i])
                        if array[i] == "zero" {
                        }else {
                            if array[2] == "zero" && array[3] == "zero" && array[4] == "zero" {
                                strDigit.append(array[i])
                                stringWithWhiteSpace(pString:"ထောင် ")
                                digitSound(digitsound: array[i])
                                unitSound(unitsound:"ထောင် ")
                            }else {
                                strDigit.append(array[i])
                                stringWithWhiteSpace(pString:"ထောင့် ")
                                digitSound(digitsound: array[i])
                                unitSound(unitsound:"ထောင့် ")
                            }
                        }
                    }else if i == 2 {
                        println_debug(array[i])
                        if array[i] == "zero" {
                        }else {
                            strDigit.append(array[i])
                            stringWithWhiteSpace(pString:"ရာ ")
                            digitSound(digitsound: array[i])
                            unitSound(unitsound:"ရာ ")
                            
                        }
                    }else if i == 3 {
                        println_debug(array[i])
                        if array[i] == "zero" {
                        }else {
                            strDigit.append(array[i])
                            if array.last == "zero" {
                                stringWithWhiteSpace(pString:"ဆယ် ")
                                digitSound(digitsound: array[i])
                                unitSound(unitsound:"ဆယ် ")
                            }else{
                                stringWithWhiteSpace(pString:"ဆယ့် ")
                                digitSound(digitsound: array[i])
                                unitSound(unitsound:"ဆယ့် ")
                            }
                        }
                    }else if i == 4{
                        println_debug(array[i])
                        if array[i] == "zero" {
                            
                        } else{
                            strDigit.append(array[i])
                            digitSound(digitsound: array[i])
                            
                        }
                    }
                    
                }else if count == 4 {
                    
                    if i == 0 {
                        if array[i] == "zero" {
                        }else {
                            strDigit.append(array[i])
                            stringWithWhiteSpace(pString:"ထောင့် ")
                            digitSound(digitsound: array[i])
                            unitSound(unitsound:"ထောင့် ")
                            
                        }
                    }else if i == 1 {
                        if array[i] == "zero" {
                        }else {
                            strDigit.append(array[i])
                            stringWithWhiteSpace(pString:"ရာ ")
                            digitSound(digitsound: array[i])
                            unitSound(unitsound:"ရာ ")
                        }
                    }else if i == 2 {
                        if array[i] == "zero" {
                        }else {
                            strDigit.append(array[i])
                            if array.last == "zero" {
                                stringWithWhiteSpace(pString:"ဆယ် ")
                                digitSound(digitsound: array[i])
                                unitSound(unitsound:"ဆယ် ")
                            }else{
                                stringWithWhiteSpace(pString:"ဆယ့် ")
                                digitSound(digitsound: array[i])
                                unitSound(unitsound:"ဆယ့် ")
                            }
                        }
                    }else if i == 3{
                        if array[i] == "zero" {
                            
                        } else{
                            strDigit.append(array[i])
                            digitSound(digitsound: array[i])
                            
                        }
                    }
                }else if count == 3 {
                    if i == 0 {
                        if array[i] == "zero" {
                        }else {
                            strDigit.append(array[i])
                            stringWithWhiteSpace(pString:"ရာ ")
                            digitSound(digitsound: array[i])
                            unitSound(unitsound:"ရာ ")
                        }
                    }else if i == 1 {
                        if array[i] == "zero" {
                        }else {
                            strDigit.append(array[i])
                            if array.last == "zero" {
                                stringWithWhiteSpace(pString:"ဆယ် ")
                                digitSound(digitsound: array[i])
                                unitSound(unitsound:"ဆယ် ")
                            }else{
                                stringWithWhiteSpace(pString:"ဆယ့် ")
                                digitSound(digitsound: array[i])
                                unitSound(unitsound:"ဆယ့် ")
                            }
                        }
                    }else if i == 2{
                        if array[i] == "zero" {
                            
                        } else{
                            strDigit.append(array[i])
                            digitSound(digitsound: array[i])
                            
                        }
                    }
                    
                }else if count == 2 {
                    if i == 0 {
                        if array[i] == "zero" {
                        }else {
                            strDigit.append(array[i])
                            if array.last == "zero" {
                                stringWithWhiteSpace(pString:"ဆယ် ")
                                digitSound(digitsound: array[i])
                                unitSound(unitsound:"ဆယ် ")
                            }else{
                                stringWithWhiteSpace(pString:"ဆယ့် ")
                                digitSound(digitsound: array[i])
                                unitSound(unitsound:"ဆယ့် ")
                            }
                        }
                    }else if i == 1{
                        if array[i] == "zero" {
                            
                        } else{
                            strDigit.append(array[i])
                            digitSound(digitsound: array[i])
                        }
                    }
                    
                } else if count == 1 {
                    if i == 0{
                        if array[i] == "zero" {
                            
                        } else{
                            strDigit.append(array[i])
                            digitSound(digitsound: array[i])
                        }
                    }
                }
                
            }
            
            if strLakhBool == true {
                stringWithWhiteSpace(pString:"သိန်း ")
                unitSound(unitsound:"သိန်း ")
            }
        } else {
            
            for i in 0 ... array.count-1 {
                
                if count == 5 {
                    
                    if i == 0 {
                        println_debug(array[i])
                        if array[i] == "zero" {
                        }else {
                            strDigit.append(array[i])
                            stringWithWhiteSpace(pString:"ေသာင္း ")
                            digitSound(digitsound: array[i])
                            unitSound(unitsound:"ေသာင္း ")
                        }
                    }else if i == 1 {
                        println_debug(array[i])
                        if array[i] == "zero" {
                        }else {
                            if array[2] == "zero" && array[3] == "zero" && array[4] == "zero" {
                                strDigit.append(array[i])
                                stringWithWhiteSpace(pString:"ေထာင္ ")
                                digitSound(digitsound: array[i])
                                unitSound(unitsound:"ေထာင္ ")
                            }else {
                                strDigit.append(array[i])
                                stringWithWhiteSpace(pString:"ေထာင့္ ")
                                digitSound(digitsound: array[i])
                                unitSound(unitsound:"ေထာင့္ ")
                            }
                        }
                    }else if i == 2 {
                        println_debug(array[i])
                        if array[i] == "zero" {
                        }else {
                            strDigit.append(array[i])
                            stringWithWhiteSpace(pString:"ရာ ")
                            digitSound(digitsound: array[i])
                            unitSound(unitsound:"ရာ ")
                            
                        }
                    }else if i == 3 {
                        println_debug(array[i])
                        if array[i] == "zero" {
                        }else {
                            strDigit.append(array[i])
                            if array.last == "zero" {
                                stringWithWhiteSpace(pString:"ဆယ္ ")
                                digitSound(digitsound: array[i])
                                unitSound(unitsound:"ဆယ္ ")
                            }else{
                                stringWithWhiteSpace(pString:"ဆယ့္ ")
                                digitSound(digitsound: array[i])
                                unitSound(unitsound:"ဆယ့္ ")
                            }
                        }
                    }else if i == 4{
                        println_debug(array[i])
                        if array[i] == "zero" {
                            
                        } else{
                            strDigit.append(array[i])
                            digitSound(digitsound: array[i])
                            
                        }
                    }
                    
                }else if count == 4 {
                    
                    if i == 0 {
                        if array[i] == "zero" {
                        }else {
                            strDigit.append(array[i])
                            stringWithWhiteSpace(pString:"ေထာင့္ ")
                            digitSound(digitsound: array[i])
                            unitSound(unitsound:"ေထာင့္ ")
                            
                        }
                    }else if i == 1 {
                        if array[i] == "zero" {
                        }else {
                            strDigit.append(array[i])
                            stringWithWhiteSpace(pString:"ရာ ")
                            digitSound(digitsound: array[i])
                            unitSound(unitsound:"ရာ ")
                        }
                    }else if i == 2 {
                        if array[i] == "zero" {
                        }else {
                            strDigit.append(array[i])
                            if array.last == "zero" {
                                stringWithWhiteSpace(pString:"ဆယ္ ")
                                digitSound(digitsound: array[i])
                                unitSound(unitsound:"ဆယ္ ")
                            }else{
                                stringWithWhiteSpace(pString:"ဆယ့္ ")
                                digitSound(digitsound: array[i])
                                unitSound(unitsound:"ဆယ့္ ")
                            }
                        }
                    }else if i == 3{
                        if array[i] == "zero" {
                            
                        } else{
                            strDigit.append(array[i])
                            digitSound(digitsound: array[i])
                            
                        }
                    }
                }else if count == 3 {
                    if i == 0 {
                        if array[i] == "zero" {
                        }else {
                            strDigit.append(array[i])
                            stringWithWhiteSpace(pString:"ရာ ")
                            digitSound(digitsound: array[i])
                            unitSound(unitsound:"ရာ ")
                        }
                    }else if i == 1 {
                        if array[i] == "zero" {
                        }else {
                            strDigit.append(array[i])
                            if array.last == "zero" {
                                stringWithWhiteSpace(pString:"ဆယ္ ")
                                digitSound(digitsound: array[i])
                                unitSound(unitsound:"ဆယ္ ")
                            }else{
                                stringWithWhiteSpace(pString:"ဆယ့္ ")
                                digitSound(digitsound: array[i])
                                unitSound(unitsound:"ဆယ့္ ")
                            }
                        }
                    }else if i == 2{
                        if array[i] == "zero" {
                            
                        } else{
                            strDigit.append(array[i])
                            digitSound(digitsound: array[i])
                            
                        }
                    }
                    
                }else if count == 2 {
                    if i == 0 {
                        if array[i] == "zero" {
                        }else {
                            strDigit.append(array[i])
                            if array.last == "zero" {
                                stringWithWhiteSpace(pString:"ဆယ္ ")
                                digitSound(digitsound: array[i])
                                unitSound(unitsound:"ဆယ္ ")
                            }else{
                                stringWithWhiteSpace(pString:"ဆယ့္ ")
                                digitSound(digitsound: array[i])
                                unitSound(unitsound:"ဆယ့္ ")
                            }
                        }
                    }else if i == 1{
                        if array[i] == "zero" {
                            
                        } else{
                            strDigit.append(array[i])
                            digitSound(digitsound: array[i])
                        }
                    }
                    
                } else if count == 1 {
                    if i == 0{
                        if array[i] == "zero" {
                            
                        } else{
                            strDigit.append(array[i])
                            digitSound(digitsound: array[i])
                        }
                    }
                }
                
            }
            
            if strLakhBool == true {
                stringWithWhiteSpace(pString:"သိန္း ")
                unitSound(unitsound:"သိန္း ")
            }
        }
    }
    
  private func stringWithWhiteSpace(pString : String) {
        strDigit.append(pString)
        println_debug("stringWithWhiteSpace")
    }
    
    
    private func digitSound(digitsound : String )  {
        
        println_debug("digitSound is : \(digitsound)")
        if appDel.currentLanguage == "uni" {
            if digitsound == "တစ် " {
                arrAudio.append("1")
            }else if digitsound == "နှစ် " {
                arrAudio.append("2")
            }else if digitsound == "သုံး " {
                arrAudio.append("3")
            }else if digitsound == "လေး " {
                arrAudio.append("4")
            }else if digitsound == "ငါး " {
                arrAudio.append("5")
            }else if digitsound == "ခြောက် " {
                arrAudio.append("6")
            }else if digitsound == "ခုနှစ် " {
                arrAudio.append("7")
            }else if digitsound == "ရှစ် " {
                arrAudio.append("8")
            }else if digitsound == "ကိုး " {
                arrAudio.append("9")
            }
        } else {
            if digitsound == "တစ္ " {
                arrAudio.append("1")
            }else if digitsound == "ႏွစ္ " {
                arrAudio.append("2")
            }else if digitsound == "သံုး " {
                arrAudio.append("3")
            }else if digitsound == "ေလး " {
                arrAudio.append("4")
            }else if digitsound == "ငါး " {
                arrAudio.append("5")
            }else if digitsound == "ေျခာက္ " {
                arrAudio.append("6")
            }else if digitsound == "ခုႏွစ္ " {
                arrAudio.append("7")
            }else if digitsound == "ရွစ္ " {
                arrAudio.append("8")
            }else if digitsound == "ကိုး " {
                arrAudio.append("9")
            }
        }
        
    }
    //  "ေထာင္"
    
    private func unitSound(unitsound : String) {
        if appDel.currentLanguage == "uni" {
            if unitsound == "သောင်း " {
                arrAudio.append("TenThousand")
            }else if unitsound == "ထောင့် " {
                arrAudio.append("Thousand")
            }else if unitsound == "ထောင် " {
                arrAudio.append("1000_9000")
            }else if unitsound == "ရာ " {
                arrAudio.append("Hundred")
            }else if unitsound == "ဆယ့် " {
                arrAudio.append("Ten")
            }else if unitsound == "ဆယ် " {
                arrAudio.append("10_90") // New changes
            }else if unitsound == "သိန်း " {
                arrAudio.append("Lakh")
            }
        } else {
            println_debug("unitsound is : \(unitsound)")
            if unitsound == "ေသာင္း " {
                arrAudio.append("TenThousand")
            }else if unitsound == "ေထာင့္ " {
                arrAudio.append("Thousand")
            }else if unitsound == "ေထာင္ " {
                arrAudio.append("1000_9000")
            }else if unitsound == "ရာ " {
                arrAudio.append("Hundred")
            }else if unitsound == "ဆယ့္ " {
                arrAudio.append("Ten")
            }else if unitsound == "ဆယ္ " {
                arrAudio.append("10_90") // New changes
            }else if unitsound == "သိန္း " {
                arrAudio.append("Lakh")
            }
        }
    }
    
    func stopPlay() {
        arrAudio.removeAll()
    }
    
}
