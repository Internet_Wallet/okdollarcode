//
//  PaytoTransactionTableViewCell.swift
//  OK
//
//  Created by Ashish on 8/11/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PaytoTransactionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var headerCell: UILabel!{
        didSet{
            self.headerCell.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var receiverKey: UILabel!{
        didSet{
            self.receiverKey.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var nameKey: UILabel!{
        didSet{
            self.nameKey.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var businessName: UILabel!{
        didSet{
            self.businessName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var amountKey: UILabel!{
        didSet{
            self.amountKey.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var rateMyService: UILabel!{
        didSet{
            self.rateMyService.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var cashbackKey: UILabel!{
        didSet{
            self.cashbackKey.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var myNumber: UIButton!{
        didSet{
            self.myNumber.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var otherNumber: UIButton!{
        didSet{
            self.otherNumber.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    @IBOutlet weak var mobileNumber: UITextField!{
        didSet{
            self.mobileNumber.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var confirmMobileNumber: UITextField!{
        didSet{
            self.confirmMobileNumber.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var name: UITextField!{
        didSet{
            self.name.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var receiverValue: UILabel!{
        didSet{
            self.receiverValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var nameValue: UILabel!{
        didSet{
            self.nameValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var businessNameValue: UILabel!{
        didSet{
            self.businessNameValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var amountValue: UILabel!{
        didSet{
            self.amountValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var cashbackValue: UILabel!{
        didSet{
            self.cashbackValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet var starFlags: [UIButton]!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func wrapModel() {
        self.receiverKey.text = "Receiver".localized
        self.nameKey.text = "Name".localized
        self.businessName.text = "Business Name".localized
        self.amountKey.text = "Amount".localized
        self.rateMyService.text = "Rate My Service".localized
        self.cashbackKey.text = "Cashback".localized
    }

}
