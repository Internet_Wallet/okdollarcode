//
//  PayToTransactionConfirmationVC.swift
//  OK
//
//  Created by Ashish Kumar Singh on 11/28/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PayToTransactionConfirmationVC: PayToBaseViewController {
    
    //MARK : - Properties
    
    var ratingCount = 0
    var ratingText = ""
    var cellHeight : CGFloat = 425
    var seconds        = 60
    var timer : Timer?
    var isTimerRunning = false
    var displayResponse = [Dictionary<String,Any>]()
    var dataCell = [PayToTransactionCell]()
    var transactionModelArray = [PaytoTransactionCellModel]()
    var viewModel = [PayToUIViewController]()
    let manager = GeoLocationManager.shared
    var transaction = Dictionary<String,Any>()
    var timerView =  TimerView.updateView()
    var requestMoney : Bool = false
    var destinationAgentName = ""
    var paymentRequest : String?
    var instaPay = false
    var agentModel: PTAgentModel?
    //WebParsing Response
    var responseFinal = [String]()
    var varType = PayToType(type: .payTo)
    var instaRemarks = ""
    var mainAccountType = ""
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier.invalid
    var instaaccountType = [String]()
    var mobileNumberArray = [String]()
    var agentType = [String]()
    var count = 0
    var agentTypeNew : String = ""
    var businessNameNew = ""
    var failureViewModel = FailureViewModel()
    
    
    var previousTime = Date()
    var currentTime = Date()
    var previoushour = 0
    var previousMinute = 0
    var previousSecond = 0
    var currentHour = 0
    var currentMinute = 0
    var currentSecond = 0
    var wentBackGround = false
    var formatter = DateFormatter()
    var isSelectedFromFav = false
    
    //MARK: - Outlets
    @IBOutlet weak var tableV: UITableView!
    @IBOutlet weak var stackView: UIStackView!
    //PromotionView
    @IBOutlet weak var beforePromotionView: UIView!
    @IBOutlet weak var beforePromotionLabel: UILabel!{
        didSet{
        beforePromotionLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var befPromotionHeight: NSLayoutConstraint!
    @IBOutlet weak var payBtn: UIButton!
        {
        didSet
        {
            self.payBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.payBtn.setTitle("TopupPay".localized, for: .normal)
        }
    }
    
    var takePhoto = false
    var failureCount = 0
    
    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        instaaccountType.removeAll()
       self.formattingTransactionResponseIntoModel()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "hh:mm:ss"
        self.tableV.delegate = self
        self.tableV.dataSource = self
        self.setPOPButton()
        self.tableV.tableFooterView = UIView.init()
       timerView.wrapTimerView(timer: "1:00")
        befPromotionHeight.constant = 0.0
        beforePromotionLabel.text = ""
        if let promotionObj = UserDefaults.standard.value(forKey: "ScanQRPromotion") as? [String: Any] {
            if let status = promotionObj["status"] as? Bool, status == true {
                if let beforePromoText = promotionObj["beforePromotion"] as? String, beforePromoText != "" {
                    if beforePromoText.contains(find: ",,")  { //beforePromoText != ",,"
                        befPromotionHeight.constant = 0.0
                        beforePromotionLabel.text = ""
                    } else {
                        let arrStr = beforePromoText.components(separatedBy: ",")
                        befPromotionHeight.constant = 40.0
                        if arrStr.count > 1 {
                            beforePromotionLabel.text = arrStr[1]
                        } else {
                            beforePromotionLabel.text = arrStr[0]
                        }
                    }
                }
            }
        }
        
    
        NotificationCenter.default.addObserver(self, selector: #selector(moveToBackGround), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(endBackgroundTask), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        failureCount = 0
        
        geoLocManager.startUpdateLocation()
        self.timerPay()
        self.tableV.reloadData()
        
        if transactionModelArray.count > 1 {
            self.title = "MultiPayment".localized
        } else {
            self.title = "Confirmation".localized
        }
        self.checkNeedToTakePhoto()
        
        if appFont == "Zawgyi-One"{
            navigationController?.navigationBar.titleTextAttributes  =  [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        }else{
            navigationController?.navigationBar.titleTextAttributes  =  [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: appFont, size: 16) ?? UIFont.systemFont(ofSize: 16)]
        }
        
        
        self.updateTimerViewAccordingtoLayout()
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        geoLocManager.stopUpdateLocation()
        timerView.removeFromSuperview()
        
        if let timerObj = self.timer {
            timerObj.invalidate()
        }
        
        println_debug("Timer Invalidated")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Methods
    func paytoTypeDifferences(type: PayToTypes) {
        switch type {
        case .singlePay:
            break
        case .multiPay:
            break
        }
    }
    
    func updateTimerViewAccordingtoLayout() {
        timerView.changeFrameForMulti()
        if let keyWindow = UIApplication.shared.keyWindow {
            keyWindow.makeKeyAndVisible()
            keyWindow.addSubview(timerView)
        }
    }
    
    private func checkNeedToTakePhoto() {
        for model in viewModel {
            if model.needToTakePhoto {
               let amount = ((model.amount.text ?? "").replacingOccurrences(of: ",", with: "") as NSString).doubleValue
                let checkAmt = (model.takePhotoAmount.replacingOccurrences(of: ",", with: "") as NSString).doubleValue
                if amount >= checkAmt {
                    takePhoto = true
                    break
                }
            }
        }
        
        if takePhoto {
            DispatchQueue.global(qos: .background).async {
                let vcPhoto = GenericPhotoClass()
                vcPhoto.delegate = self
                vcPhoto.fromPayto = true
                vcPhoto.cameraStart()
            }
        }
    }
    
    @objc func moveToBackGround() {
        let date = Date()
        previousTime = date
        let str = formatter.string(from: previousTime)
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: formatter.date(from: str) ?? Date())
        let minutes = calendar.component(.minute, from: formatter.date(from: str) ?? Date())
        let second = calendar.component(.second, from: formatter.date(from: str) ?? Date())
        previoushour = hour
        previousMinute = minutes
        previousSecond = second
        wentBackGround = true
    }
    
    func timerPay() {
//        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(PayToTransactionConfirmationVC.updateTimer)), userInfo: nil, repeats: true)
//        timer.tolerance = 0.1
        
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)

        
        
        
        
    }
    
    @objc func endBackgroundTask() {
        if wentBackGround {
            diffTime()
            wentBackGround = false
        }
    }
    
    func diffTime() {
        let str = formatter.string(from: Date())
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: formatter.date(from: str) ?? Date())
        let minutes = calendar.component(.minute, from: formatter.date(from: str) ?? Date())
        let second = calendar.component(.second, from: formatter.date(from: str) ?? Date())
        currentHour = hour
        currentMinute = minutes
        currentSecond = second
        
        if currentHour < previoushour {
            currentMinute = currentMinute + 60
        }
        if currentMinute < previousMinute {
            currentSecond = currentSecond + 60
        }
        let finalMin = currentMinute - previousMinute
        let finalSecond = currentSecond - previousSecond
        seconds =  seconds - (finalMin * 60 + finalSecond)
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        
        print(String(format: "%01d:%02d", minutes, seconds))
        return String(format: "%01d:%02d", minutes, seconds)
    }
    
    
    
    @objc func updateTimer() {
        timerView.wrapTimerView(timer: self.timeFormatted(self.seconds))
        if seconds > 0 {
            seconds -= 1
        }else{
            if let timerObj = self.timer {
                UIApplication.shared.endBackgroundTask(backgroundTask)
                backgroundTask = UIBackgroundTaskIdentifier.invalid
                if timerObj.isValid {
                    timerObj.invalidate()
                    alertViewObj.wrapAlert(title: nil, body: "Session expired".localized, img:#imageLiteral(resourceName: "alert-icon.png"))
                    alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                        
                        userDef.set("insta", forKey: "FromInstaPaySession")
                        userDef.synchronize()
                        self.navigationController?.popViewController(animated: true)
                    })
                    alertViewObj.showAlert(controller: self)
                }
            }
        }
        
//        else if seconds <= 10 {
//            timerView.wrapTimerViewUpdate(color: UIColor.red, alpha: 1.00)
//            UIView.animate(withDuration: 0.2, animations: {
//                self.timerView.wrapTimerViewUpdate(color: UIColor.red, alpha: 0.00)
//            }) { (bool) in
//                self.timerView.wrapTimerViewUpdate(color: UIColor.red, alpha: 1.00)
//            }
    }
    
    private func agentServiceTypeString(type: AgentType) -> String {
        switch type {
        case .user:
            return "SUBSBR"
        case .merchant:
            return "MER"
        case .agent:
            return "AGENT"
        case .advancemerchant:
            return "ADVMER"
        case .dummymerchant:
            return "DUMMY"
        case .oneStopMart:
            return "ONESTOP"
        }
    }
    
    private func getMITPayParams() -> Dictionary<String, Any> {
        var params = Dictionary<String,Any>()
        if displayResponse.count > 0, displayResponse.count < 2  , let dic = displayResponse.first {
            params["SourceNumber"] = dic.safeValueForKey("source") as? String ?? "" //  //dic.safeValueForKey("source") as? String ?? ""
            params["DestinationNumber"] = dic.safeValueForKey("destination") as? String ?? "" //"00959261834224" //dic.safeValueForKey("destination") as? String
            params["Amount"] = Double(dic.safeValueForKey("amount") as? String ?? "0")
            if let vc = self.viewModel.first {
                params["RefrenceNumber"] = vc.referenceNumberMIT
            }
            params["ServiceFee"] = 0.0
            params["OkdollarFee"] = 0.0
            params["Password"] = ok_password ?? ""
            params["Spid"] = ""
        }
        return params
    }
    
    fileprivate func showMITReceipt(dictToPass: Dictionary<String,Any>) {
        DispatchQueue.main.async {
            guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "MITReceiptViewController") as? MITReceiptViewController else { return }
            vc.valueDictionary = dictToPass
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    //MARK:- Payment Action
    @IBAction func payAction(_ sender: UIButton) {
        if let timerObj = self.timer {
            timerObj.invalidate()
        }
        seconds = 60
        
        if appDelegate.checkNetworkAvail() {
            UIApplication.shared.endBackgroundTask(backgroundTask)
            backgroundTask = UIBackgroundTaskIdentifier.invalid
            
            //this will work for mit Qr and if api related any doubt contact nanaji because mit related api is done from nanaji side
            if let vc = self.viewModel.first, vc.mitQRCode == true {
                let params = self.getMITPayParams()
                guard let url = URL(string: Url.mitPayemntUrl) else { return  }
               // println_debug(params)
                print("payAction getMITPayParamsurl-----\(url)")
                TopupWeb.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", handle: { [weak self](resp, success) in
                    var dictToPass = Dictionary<String,Any>()
                    if success {
                        if let response = resp as? Dictionary<String,Any> {
                            print("payAction response-----\(response)")

                            if let msgData = response["Msg"] as? String {
                                let strToDict = OKBaseController.convertToDictionary(text: msgData)
                                if strToDict?["Description"] as? String ?? "" == "Transaction Successful" {
//                                    dictToPass["destination"] = strToDict?["Destinstion"] as? String ?? ""
                                    dictToPass["balance"] = strToDict?["Balance"] as? String ?? ""
                                    dictToPass["transId"] = strToDict?["TransactionId"] as? String ?? ""
                                    dictToPass["date"] = strToDict?["TransactionTime"] as? String ?? "0"
                                    dictToPass["okFee"] = strToDict?["OkFee"] as? String ?? "0"
                                    dictToPass["serviceFee"] = strToDict?["ServiceFee"] as? String ?? "0"
//                                    dictToPass["amount"] = strToDict?["Amount"] as? String ?? "0"
                                    
                                    if (self?.displayResponse.count ?? 0) > 0, (self?.displayResponse.count ?? 0) < 2  , let dic = self?.displayResponse.first {
                                        dictToPass["destination"] = dic.safeValueForKey("destination") as? String ?? ""
                                        dictToPass["amount"] = dic.safeValueForKey("amount") as? String ?? "0"
                                        if let vc = self?.viewModel.first {
                                            dictToPass["ReferenceNumber"] = vc.referenceNumberMIT
                                        }
                                    }
                                    self?.showMITReceipt(dictToPass: dictToPass)
                                }
                            }
                        }
                        
                    }
                })
            } else {
                if appDelegate.checkNetworkAvail() {
                    let web = PayToWebApi()
                    web.delegate = self
                    if displayResponse.count > 0, displayResponse.count < 2  , let dic = displayResponse.first {
                        let subPaymentType = dic.safeValueForKey("CICOPayTo") as? PayToType
                        let source   = dic.safeValueForKey("source") as? String
                        var amount   = dic.safeValueForKey("amount") as? String
                        if self.varType.type == .cashIn || self.varType.type == .transferTo || self.viewModel.first?.cashInCashOut != "" {
                            amount  =  (self.viewModel.first?.cashInCashOut != "Cash Out") ? dic.safeValueForKey("totalAmount") as? String : dic.safeValueForKey("amount") as? String
                        }
                        let dest     = dic.safeValueForKey("destination") as? String
                        //let clientOs = dic.safeValueForKey("clientos") as? String
                        //let clientip = dic.safeValueForKey("clientip") as? String
                        //let clientTansId = dic.safeValueForKey("transid") as? String
                       
                        if requestMoney {
                            amount   = dic.safeValueForKey("amount") as? String
                            //                        amount = "1"
                            //let vc = self.viewModel.first
                            let type = "PAYTO"
                            //                    let aName = (UserModel.shared.agentType == .merchant) ?  UserModel.shared.businessName : UserModel.shared.name.uppercased()
                            let aName =  UserModel.shared.name.uppercased()
                            //let gender = UserModel.shared.gender
                            //let age = UserModel.shared.ageNow
                            //let area = ""
                            let recieverName = dic.safeValueForKey("destinationName") as? String ?? ""
                        
                            let recieverBusiness = dic.safeValueForKey("businessName") as? String ?? ""
                            let senderBusiness = UserModel.shared.businessName
                                                
                            //let senderName = UserModel.shared.name
                            //let accountType = self.agentServiceTypeString(type: UserModel.shared.agentType)
                            let techComments = String.init(format: "[%@,%@,%@,%@,%@,%@,##%@##%@##%@##]-OKNAME-%@", manager.currentLatitude, manager.currentLongitude, "",UserModel.shared.gender,UserModel.shared.ageNow,"",recieverName as CVarArg,recieverBusiness as CVarArg,senderBusiness as CVarArg, aName as CVarArg)
                            
                            func replaceEscapeCharacter(_ value: String) -> String {
                                if value == "" { return ""}
                                var str = value.replacingOccurrences(of: "$", with: "")
                                str = value.replacingOccurrences(of: "&", with: "AND")
                                str = value.replacingOccurrences(of: "/", with: "")
                                str = value.replacingOccurrences(of: "<", with: "")
                                str = value.replacingOccurrences(of: ">", with: "")
                                return str
                            }
                            instaRemarks = replaceEscapeCharacter(dic.safeValueForKey("localRemark") as? String ?? "")
                            //27th July Gauri
                               let newlyAddedStr = "@sender" + UserModel.shared.name + "@businessname"  + UserModel.shared.businessName + "@accounttype"  +  "\(UserModel.shared.agentType.intValue)" + "@receiveraccounttype"  +  UitilityClass.returnAccountTypeFromName(type: "\(instaaccountType[0])")
                                     
                            var final_comment = String.init(format: "%@%@", instaRemarks, techComments) + getSubPaymentTypeString(value: subPaymentType) + newlyAddedStr
                            
                            if ratingCount  > 0 {
                                let dateSec = self.getDateInMillSeconds()
                                final_comment = String.init(format: "%@@rating@-%@-%@-%@-%@-%@", final_comment, "\(ratingCount + 1)" , ratingText, dest ?? "", dateSec, recieverName)
                            }
                            let str = String.init(format: PTHelper.singlePay, type, UserModel.shared.mobileNo,ok_password ?? "",
                                                  source ?? "", dest ?? "", amount ?? "", final_comment, UserLogin.shared.token)
                            guard let urlEncoded = str.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
                            guard let url = URL.init(string: urlEncoded) else { return }
                            let param = Dictionary<String,Any>()
                            let cell = transactionModelArray[0].cell
                            let cellButton = cell.first(where: {$0.mobileNumberTextfield != nil})
                            var otherNumber = ""
                            if let btn = cellButton {
                                otherNumber = btn.getOtherNumber()
                            }
                            if  otherNumber.count > 0 {
                                var strcash = String.init(format: PTHelper.singlePay , type, UserModel.shared.mobileNo,ok_password ?? "",source ?? "",dest ?? "",amount ?? "",final_comment ,UserLogin.shared.token)
                                strcash = strcash + ";kickbackmsisdn=\(otherNumber)"
                                guard let urlEncodedCash = strcash.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
                                guard let urlcash = URL.init(string: urlEncodedCash) else { return }
                                web.genericClassXML(url: urlcash, param: param as AnyObject, httpMethod: "POST", mScreen: ScreenName.singlePay)
                            } else {
                                web.genericClassXML(url: url, param: param as AnyObject, httpMethod: "POST", mScreen: ScreenName.singlePay)
                            }
                        } else if instaPay {
                            amount   = dic.safeValueForKey("amount") as? String
                            //                        amount = "1"
                            //let vc = self.viewModel.first
                            let type = "PAYTO"
                            //                    let aName = (UserModel.shared.agentType == .merchant) ?  UserModel.shared.businessName : UserModel.shared.name.uppercased()
                            let aName =  UserModel.shared.name.uppercased()
                            //let gender = UserModel.shared.gender
                            //let age = UserModel.shared.ageNow
                            //let area = ""
                            let recieverName = dic.safeValueForKey("destinationName") as? String ?? ""
                            let recieverBusiness = dic.safeValueForKey("businessName") as? String ?? ""
                            let senderBusiness = UserModel.shared.businessName
                        
                            //let senderName = UserModel.shared.name
                            //let accountType = self.agentServiceTypeString(type: UserModel.shared.agentType)
                            
                            let techComments = String.init(format: "[%@,%@,%@,%@,%@,%@,##%@##%@##%@##]-OKNAME-%@", manager.currentLatitude, manager.currentLongitude, "",UserModel.shared.gender,UserModel.shared.ageNow,"",recieverName as CVarArg,recieverBusiness as CVarArg, senderBusiness as CVarArg,aName as CVarArg)
                            func replaceEscapeCharacter(_ value: String) -> String {
                                if value == "" { return ""}
                                var str = value.replacingOccurrences(of: "$", with: "")
                                str = value.replacingOccurrences(of: "&", with: "AND")
                                str = value.replacingOccurrences(of: "/", with: "")
                                str = value.replacingOccurrences(of: "<", with: "")
                                str = value.replacingOccurrences(of: ">", with: "")
                                return str
                            }
                            instaRemarks = replaceEscapeCharacter(dic.safeValueForKey("localRemark") as? String ?? "")
                            //27th July Gauri
                              let newlyAddedStr = "@sender" + UserModel.shared.name + "@businessname"  + UserModel.shared.businessName + "@accounttype"  +  "\(UserModel.shared.agentType.intValue)" + "@receiveraccounttype"  + UitilityClass.returnAccountTypeFromName(type: "\(instaaccountType[0])")
                            
                            var final_comment = String.init(format: "%@%@", instaRemarks, techComments) + getSubPaymentTypeString(value: subPaymentType) + newlyAddedStr
                            if ratingCount  > 0 {
                                let dateSec = self.getDateInMillSeconds()
                                final_comment = String.init(format: "%@@rating@-%@-%@-%@-%@-%@", final_comment, "\(ratingCount + 1)" , ratingText, dest ?? "", dateSec, recieverName)
                            }
                            let str = String.init(format: PTHelper.singlePay, type, UserModel.shared.mobileNo,ok_password ?? "",
                                                  source ?? "", dest ?? "", amount ?? "", final_comment, UserLogin.shared.token)
                            guard let urlEncoded = str.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
                            guard let url = URL.init(string: urlEncoded) else { return }
                            let param = Dictionary<String,Any>()
                            let cell = transactionModelArray[0].cell
                            let cellButton = cell.first(where: {$0.mobileNumberTextfield != nil})
                            var otherNumber = ""
                            if let btn = cellButton {
                                otherNumber = btn.getOtherNumber()
                            }
                            if  otherNumber.count > 0 {
                                var strcash = String.init(format: PTHelper.singlePay , type, UserModel.shared.mobileNo,ok_password ?? "",source ?? "",dest ?? "",amount ?? "",final_comment ,UserLogin.shared.token)
                                strcash = strcash + ";kickbackmsisdn=\(otherNumber)"
                                print("payAction strcash-----\(strcash)")

                                guard let urlEncodedCash = strcash.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
                                guard let urlcash = URL.init(string: urlEncodedCash) else { return }
                                web.genericClassXML(url: urlcash, param: param as AnyObject, httpMethod: "POST", mScreen: ScreenName.singlePay)
                            } else {
                                web.genericClassXML(url: url, param: param as AnyObject, httpMethod: "POST", mScreen: ScreenName.singlePay)
                            }
                        } else {
                            //                        guard let transRequestType = self.viewModel.first else {
                            //                            return
                            //                        }
                            //                        let type = self.getTransactionString(transRequestType.transType)
                            ////                        amount   = dic.safeValueForKey("amount") as? String
                            ////                        amount = "1"
                            //                        //                    let aName = (UserModel.shared.agentType == .merchant) ?  UserModel.shared.businessName : UserModel.shared.name.uppercased()
                            //                        let aName = UserModel.shared.name.uppercased()
                            //                        let gender = UserModel.shared.gender
                            //                        let age    = UserModel.shared.ageNow
                            //                        let area    = ""
                            //                        let recieverName     = transRequestType.subscriber.text ?? ""
                            //                        let recieverBusiness = transRequestType.merchant.text ?? ""
                            //                        let senderBusiness = UserModel.shared.businessName
                            //                        let senderName = UserModel.shared.name
                            //                        let accountType = self.agentServiceTypeString(type: UserModel.shared.agentType)
                            //                        let techComments = String.init(format: "```S^A -OKNAME-%@", manager.currentLatitude, manager.currentLongitude, "",UserModel.shared.gender,UserModel.shared.ageNow,"",transRequestType.subscriber.text ?? "" as CVarArg,recieverBusiness as CVarArg,aName as CVarArg)
                            //
                            //                        func replaceEscapeCharacter(_ value: String) -> String {
                            //                            if value == "" { return ""}
                            //                            var str = value.replacingOccurrences(of: "$", with: "")
                            //                            str = value.replacingOccurrences(of: "&", with: "AND")
                            //                            str = value.replacingOccurrences(of: "/", with: "")
                            //                            str = value.replacingOccurrences(of: "<", with: "")
                            //                            str = value.replacingOccurrences(of: ">", with: "")
                            //                            return str
                            //                        }
                            //
                            //                        var final_comment = String.init(format: "%@%@", replaceEscapeCharacter(transRequestType.remarks.text ?? ""), techComments) + getSubPaymentTypeString(value: subPaymentType)
                            //
                            //                        let str = String.init(format: PTHelper.singlePay , type, UserModel.shared.mobileNo,ok_password ?? "",source ?? "",dest ?? "",amount ?? "",final_comment ,UserLogin.shared.token)
                            //
                            //                        guard let urlEncoded = str.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
                            //
                            //                        guard let url = URL.init(string: urlEncoded) else { return }
                            //
                            //                        if ratingCount  > 0 {
                            //                            let dateSec = self.getDateInMillSeconds()
                            //                            final_comment = String.init(format: "%@@rating@-%@-%@-%@-%@-%@", final_comment, "\(ratingCount + 1)" ,ratingText, dest ?? "", dateSec, recieverName)
                            //                        }
                            //                        var amount: String?
                            //
                            //                        amount = dic.safeValueForKey("amount") as? String
                            //                        if self.varType.type == .cashIn || self.varType.type == .transferTo || self.viewModel.first?.cashInCashOut != "" {
                            //                            amount  =  (self.viewModel.first?.cashInCashOut != "Cash Out") ? dic.safeValueForKey("totalAmount") as? String : dic.safeValueForKey("amount") as? String
                            //                        }
                            //
                            //                        let param = Dictionary<String,Any>()
                            //
                            //                        let cell = transactionModelArray[0].cell
                            //
                            //                        let cellButton = cell.first(where: {$0.mobileNumberTextfield != nil})
                            //
                            //                        var otherNumber = ""
                            //                        if let btn = cellButton {
                            //                            otherNumber = btn.getOtherNumber()
                            //                        }
                            //                        if  otherNumber.count > 0 {
                            //                            var strcash = String.init(format: PTHelper.singlePay , type, UserModel.shared.mobileNo,ok_password ?? "",source ?? "",dest ?? "",amount ?? "",final_comment ,UserLogin.shared.token)
                            //
                            //                            strcash = strcash + ";kickbackmsisdn=\(otherNumber)"
                            //
                            //                            guard let urlEncodedCash = strcash.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
                            //
                            //                            guard let urlcash = URL.init(string: urlEncodedCash) else { return }
                            //
                            //                            web.genericClassXML(url: urlcash, param: param as AnyObject, httpMethod: "POST", mScreen: ScreenName.singlePay)
                            //                        } else {
                            //                            web.genericClassXML(url: url, param: param as AnyObject, httpMethod: "POST", mScreen: ScreenName.singlePay)
                            //                        }
                            
                            var genericURL = ""
                            if serverUrl == .productionUrl{
                                genericURL = "https://www.okdollar.co/RestService.svc/GenericPayment"
                            }else{
                                genericURL = "http://69.160.4.151:8001/RestService.svc//GenericPayment"
                            }
                            
                                     
                            
                                                   let urlStr = URL(string: genericURL)
                                                   guard let paramsSinglePay = self.getSinglePayParamas() else { return }
                                                    web.genericClassXML(url: urlStr!, param: paramsSinglePay as AnyObject, httpMethod: "POST", mScreen: ScreenName.singlePayGeneric)
                            
//                            guard let url = URL(string: "https://www.okdollar.co/RestService.svc/GenericPayment") else { return }
//                          //  let params = self.JSONStringFromAnyObject(value: self.getTheParams() as AnyObject)
//                          //  println_debug(params)
//                            TopupWeb.genericClass(url: url, param: self.getSinglePayParamas() as AnyObject, httpMethod: "POST", handle: { [weak self](resp, success) in
//                                if success {
//                                    DispatchQueue.main.async {
//                                        self?.parseMultiPaymentResult(data: resp)
//                                    }
//                                }
//                            })
                            
                        }
                    } else {
                        guard let url = URL(string: PTHelper.multiPay) else { return }
                        let params = self.JSONStringFromAnyObject(value: self.getTheParams() as AnyObject)
                        println_debug(params)
                        TopupWeb.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", handle: { [weak self](resp, success) in
                            if success {
                                DispatchQueue.main.async {
                                    self?.parseMultiPaymentResult(data: resp)
                                }
                            }else{
                                self?.failureCount = self?.failureCount ?? 0 + 1
                                if self?.failureCount == 2 {
                                self?.failureViewModel.sendDataToFailureApi(request: params as Any, response: resp, type: FailureHelper.FailureType.PAYTO.rawValue, finished: {
                                     
                                })
                                }
                            }
                        })
                    }
                }
            }
        } else {
            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.noInternet.localized)
            return
        }
    }
    
    private func getSubPaymentTypeString(value: PayToType?) -> String {
        //guard let subPaymentType = value else { return "" }
        if self.varType.type == .transferTo || self.viewModel.first?.cashInCashOut == "Cash In" {
            return "-cashin"
        } else if self.varType.type == .cashOut || self.viewModel.first?.cashInCashOut == "Cash Out" {
             return "-cashout"
        } else {
            return ""
        }
//        switch subPaymentType.type {
//        case .cashIn:
//            return "-cashin"
//        case .cashOut:
//            return "-cashout"
//        case .transferTo:
//            return "-cashin"
//        default:
//            break
//        }
//        return ""
    }
    
    func getDateInMillSeconds() -> String {
        return  String(Date().millisecondsSince1970)
    }
    
    //Commented function not in use
    fileprivate func getSinglePayParamas() -> [String: Any]? {
        var arrayParam = [String: Any]()
        var TransactionsCellTower = [String: Any]()
        TransactionsCellTower["Lac"] = ""
        TransactionsCellTower["Mcc"] = mccStatus.mcc
        TransactionsCellTower["Mnc"] = mccStatus.mnc
        TransactionsCellTower["SignalStrength"] = ""
        TransactionsCellTower["NetworkType"] = UitilityClass.getNetworkType()
        TransactionsCellTower["NetworkSpeed"] = UitilityClass.getSignalStrength()
        
        if UitilityClass.isConnectedToWifi(){
            let wifiInfo = UitilityClass.getNetworkInfos()[0].components(separatedBy: ",")
            TransactionsCellTower["ConnectedwifiName"] = wifiInfo[0]
            TransactionsCellTower["Ssid"] = wifiInfo[0]
            TransactionsCellTower["Mac"] = wifiInfo[1]
        }else{
            TransactionsCellTower["ConnectedwifiName"] = ""
            TransactionsCellTower["Ssid"] = ""
            TransactionsCellTower["Mac"] = ""
        }
        
        // l External Reference
        var lExternalReference = [String: Any]()
        lExternalReference["Direction"] = true
        lExternalReference["EndStation"] = ""
        lExternalReference["StartStation"] = ""
        lExternalReference["VendorID"] = ""
        
        // L geo Location
        var lGeoLocation = [String: Any]()
        lGeoLocation["CellID"] = ""
        
        let lat  = (manager.currentLatitude == nil) ? "0.0" : manager.currentLatitude
        let long = (manager.currentLongitude == nil) ? "0.0" : manager.currentLongitude
        
        lGeoLocation["Latitude"] = lat ?? "0.0"
        lGeoLocation["Longitude"] = long ?? "0.0"
        
        // L Proximity
        var lProximity = [String: Any]()
        lProximity["BlueToothUsers"]  = [Any]()
        lProximity["SelectionMode"]   = false
        lProximity["WifiUsers"]       = [Any]()
        lProximity["mBltDevice"]      = ""
        lProximity["mWifiDevice"]    = ""
        
        // Final payment Dictionary
        guard let appendedModel = self.viewModel.first else { return nil }
        guard let model = self.displayResponse.first else { return nil }

        let subPaymentType = model.safeValueForKey("CICOPayTo") as? PayToType
        
        //comments
        //let gender = UserModel.shared.gender
        //let age    = UserModel.shared.ageNow
        //let area    = ""
        let recieverName     = appendedModel.subscriber.text ?? ""
        let recieverBusiness = appendedModel.merchant.text ?? ""
        var senderBusiness = UserModel.shared.businessName
         if  UserModel.shared.agentType == .user{
                       senderBusiness = ""
                   }
        //let senderName = UserModel.shared.name
        let aName = UserModel.shared.name.uppercased()
        
        let techComments = String.init(format: "[%@,%@,%@,%@,%@,%@,##%@##%@##%@##]-OKNAME-%@", manager.currentLatitude, manager.currentLongitude, "",UserModel.shared.gender,UserModel.shared.ageNow,"",appendedModel.subscriber.text ?? "" as CVarArg,recieverBusiness as CVarArg, senderBusiness as CVarArg ,aName as CVarArg)
        
        func replaceEscapeCharacter(_ value: String) -> String {
            if value == "" { return ""}
            var str = value.replacingOccurrences(of: "$", with: "")
            str = value.replacingOccurrences(of: "&", with: "AND")
            str = value.replacingOccurrences(of: "/", with: "")
            str = value.replacingOccurrences(of: "<", with: "")
            str = value.replacingOccurrences(of: ">", with: "")
            return str
        }
        
        var final_comment = ""
        
        //27th July Gauri
        //let newlyAddedStr = "@sender" + UserModel.shared.name + "@businessname"  + UserModel.shared.businessName + "@accounttype"  +  "\(UserModel.shared.agentType.intValue)"
                
        //27th July Gauri
           let newlyAddedStr = "@sender" + UserModel.shared.name + "@businessname"  + UserModel.shared.businessName + "@accounttype"  +  "\(UserModel.shared.agentType.intValue)" + "@receiveraccounttype"  +  UitilityClass.returnAccountTypeFromName(type: "\(instaaccountType[0])")

        final_comment = String.init(format: "%@%@", replaceEscapeCharacter(appendedModel.remarks.text ?? ""), techComments) + getSubPaymentTypeString(value: subPaymentType) + newlyAddedStr
        
        println_debug(model)
        println_debug(appendedModel)
        
        var otherNumber = ""
        
        let cell = transactionModelArray[0].cell
        
        let cellButton = cell.first(where: {$0.mobileNumberTextfield != nil})
        
        
        if let btn = cellButton {
            otherNumber = btn.getOtherNumber()
        }
        
        if ratingCount  > 0 {
            let dateSec = self.getDateInMillSeconds()
            final_comment = String.init(format: "%@@rating@-%@-%@-%@-%@-%@", final_comment, "\(ratingCount + 1)" ,ratingText, model.safeValueForKey("destination") as? String ?? "", dateSec, recieverName)
        }
        var amount: String?

        if displayResponse.count > 0, displayResponse.count < 2  , let dic = displayResponse.first {
            amount = model.safeValueForKey("amount") as? String
            if self.varType.type == .cashIn || self.varType.type == .transferTo || self.viewModel.first?.cashInCashOut != "" {
                amount  =  (self.viewModel.first?.cashInCashOut != "Cash Out") ? dic.safeValueForKey("totalAmount") as? String : dic.safeValueForKey("amount") as? String
            }
        }
        // lTransactions//
        var lTransactionsDict = [String: Any]()
        lTransactionsDict["Amount"]                = amount
        lTransactionsDict["Balance"]                = ""
        lTransactionsDict["BonusPoint"]              = 0
        lTransactionsDict["CashBackFlag"]            = 0 // need to check the flag value
        lTransactionsDict["Comments"]               = final_comment
        lTransactionsDict["Destination"]            = model.safeValueForKey("destination") as? String ?? ""
        lTransactionsDict["DiscountPayTo"]          = "0" // discount need to check from the server
        lTransactionsDict["IsMectelTopUp"]          = false
        lTransactionsDict["KickBack"]               = model.safeValueForKey("kickback") as? String ?? ""
        lTransactionsDict["KickBackMsisdn"]         = otherNumber
        lTransactionsDict["LocalTransactionType"]   = self.getLocalTransactionString(appendedModel.transType) // need to describe the id
        lTransactionsDict["MerchantName"]           = appendedModel.subscriber.text ?? "" // check for the merchant name
        lTransactionsDict["MobileNumber"]           = UserModel.shared.mobileNo
        lTransactionsDict["Mode"]                   = true
        lTransactionsDict["Password"]               = ok_password ?? ""
        lTransactionsDict["PromoCodeId"]            = ""
        lTransactionsDict["ResultCode"]             = 0
        lTransactionsDict["ResultDescription"]      = ""
        lTransactionsDict["SecureToken"]            = UserLogin.shared.token
        lTransactionsDict["TransactionID"]          = model.safeValueForKey("transid") as? String ?? ""
        lTransactionsDict["TransactionTime"]        = Date.init(timeIntervalSinceNow: 0).stringValue()
        lTransactionsDict["TransactionType"]        = self.getTransactionString(appendedModel.transType)
        lTransactionsDict["accounType"]             = UserModel.shared.agentServiceTypeString()
        if instaaccountType.count>0{
            lTransactionsDict["DestinationNumberAccountType"] = instaaccountType[0]
        }
        
        arrayParam["TransactionsCellTower"] = TransactionsCellTower
        arrayParam["lExternalReference"]    = lExternalReference
        arrayParam["lGeoLocation"]          = lGeoLocation
        arrayParam["lProximity"]            = lProximity
        arrayParam["lTransactions"]         = lTransactionsDict
        return arrayParam
        
    }
    
    //MARK:- Multi payment Action
    fileprivate  func getTheParams() -> [[String: Any]] {
        
        var finalArray = [[String: Any]]()
        
        var arrayParam = [String: Any]()
        for (index,model) in self.displayResponse.enumerated() {
            // Transaction cell tower
            var TransactionsCellTower = [String: Any]()
            TransactionsCellTower["Lac"] = ""
            TransactionsCellTower["Mcc"] = mccStatus.mcc
            TransactionsCellTower["Mnc"] = mccStatus.mnc
            TransactionsCellTower["SignalStrength"] = ""
            TransactionsCellTower["NetworkType"] = UitilityClass.getNetworkType()
            TransactionsCellTower["NetworkSpeed"] = UitilityClass.getSignalStrength()
            
            if UitilityClass.isConnectedToWifi(){
                let wifiInfo = UitilityClass.getNetworkInfos()[0].components(separatedBy: ",")
                TransactionsCellTower["ConnectedwifiName"] = wifiInfo[0]
                TransactionsCellTower["Ssid"] = wifiInfo[0]
                TransactionsCellTower["Mac"] = wifiInfo[1]
            }else{
                TransactionsCellTower["ConnectedwifiName"] = ""
                TransactionsCellTower["Ssid"] = ""
                TransactionsCellTower["Mac"] = ""
            }
            
            // l External Reference
            var lExternalReference = [String: Any]()
            lExternalReference["Direction"] = true
            lExternalReference["EndStation"] = ""
            lExternalReference["StartStation"] = ""
            lExternalReference["VendorID"] = ""
            
            // L geo Location
            var lGeoLocation = [String: Any]()
            lGeoLocation["CellID"] = ""
            
            let lat  = (manager.currentLatitude == nil) ? "0.0" : manager.currentLatitude
            let long = (manager.currentLongitude == nil) ? "0.0" : manager.currentLongitude
            
            lGeoLocation["Latitude"] = lat ?? "0.0"
            lGeoLocation["Longitude"] = long ?? "0.0"
            
            // L Proximity
            var lProximity = [String: Any]()
            lProximity["BlueToothUsers"]  = [Any]()
            lProximity["SelectionMode"]   = false
            lProximity["WifiUsers"]       = [Any]()
            lProximity["mBltDevice"]      = ""
            lProximity["mWifiDevice"]    = ""
            
            // Final payment Dictionary
            let appendedModel = self.viewModel[index]
//            <comments>[16.8166693,96.1318454,3037185,M,24,KAMARYOU,##RECEIVER NAME##RECEIVER BUSINESS NAME##SENDER BUSINESS NAME##]-OKNAME-SENDER NAME
            
            

            // comments for the particular transaction
            let subPaymentType = model.safeValueForKey("CICOPayTo") as? PayToType
            //comments
            let gender = UserModel.shared.gender
            let age    = UserModel.shared.ageNow
            let area    = ""
           // var techComments = ""
            
            let recieverName     = appendedModel.subscriber.text ?? ""
            let recieverBusiness = appendedModel.merchant.text ?? ""
            var senderBusiness = UserModel.shared.businessName
            let aName = UserModel.shared.name.uppercased()
            
            //Tushar***********
            
            //print(appendedModel.accountType)
            var techComments = ""
            if  UserModel.shared.agentType == .user{
                senderBusiness = ""
            }
        
            techComments = String.init(format: "[%@,%@,%@,%@,%@,%@,##%@##%@##%@##]-OKNAME-%@", lat ?? "0.0", long ?? "0.0", "",gender, age,area,recieverName as CVarArg,recieverBusiness as CVarArg,senderBusiness as CVarArg, aName as CVarArg)
            //27th July Gauri
            let newlyAddedStr = "@sender" + UserModel.shared.name + "@businessname"  + UserModel.shared.businessName + "@accounttype"  +  "\(UserModel.shared.agentType.intValue)" + "@receiveraccounttype"  +  UitilityClass.returnAccountTypeFromName(type: "\(instaaccountType[index])")
            var final_comment = String.init(format: "%@%@", appendedModel.remarks.text ?? "", techComments) + getSubPaymentTypeString(value: subPaymentType)  + newlyAddedStr
            
            print(final_comment)
            println_debug(model)
            println_debug(appendedModel)
            
            var otherNumber = ""
            
            let cell = transactionModelArray[index].cell
            
            let cellButton = cell.first(where: {$0.mobileNumberTextfield != nil})
            
            if let btn = cellButton {
                otherNumber = btn.getOtherNumber()
            }
            
            if ratingCount  > 0 {
                let dateSec = self.getDateInMillSeconds()
                final_comment = String.init(format: "%@@rating@-%@-%@-%@-%@-%@", final_comment, "\(ratingCount + 1)" ,ratingText, model.safeValueForKey("destination") as? String ?? "", dateSec, recieverName)
            }
            var amount: String?
            
            amount = model.safeValueForKey("amount") as? String
            if self.varType.type == .cashIn || self.varType.type == .transferTo || self.viewModel.first?.cashInCashOut != "" {
                amount  =  (self.viewModel.first?.cashInCashOut != "Cash Out") ? model.safeValueForKey("totalAmount") as? String : model.safeValueForKey("amount") as? String
            }
            
            // lTransactions//
            var lTransactionsDict = [String: Any]()
            lTransactionsDict["Amount"]                = amount ?? ""
            lTransactionsDict["Balance"]                = ""
            lTransactionsDict["BonusPoint"]              = 0
            lTransactionsDict["CashBackFlag"]            = 0 // need to check the flag value
            lTransactionsDict["Comments"]               = final_comment
            lTransactionsDict["Destination"]            = model.safeValueForKey("destination") as? String ?? ""
            lTransactionsDict["DiscountPayTo"]          = "0" // discount need to check from the server
            lTransactionsDict["IsMectelTopUp"]          = false
            lTransactionsDict["KickBack"]               = model.safeValueForKey("kickback") as? String ?? ""
            lTransactionsDict["KickBackMsisdn"]         = otherNumber
            lTransactionsDict["LocalTransactionType"]   = self.getLocalTransactionString(appendedModel.transType) // need to describe the id
            lTransactionsDict["MerchantName"]           = appendedModel.subscriber.text ?? "" // check for the merchant name
            lTransactionsDict["MobileNumber"]           = UserModel.shared.mobileNo
            lTransactionsDict["Mode"]                   = true
            lTransactionsDict["Password"]               = ok_password ?? ""
            lTransactionsDict["PromoCodeId"]            = ""
            lTransactionsDict["ResultCode"]             = 0
            lTransactionsDict["ResultDescription"]      = ""
            lTransactionsDict["SecureToken"]            = UserLogin.shared.token
            lTransactionsDict["TransactionID"]          = model.safeValueForKey("transid") as? String ?? ""
            lTransactionsDict["TransactionTime"]        = Date.init(timeIntervalSinceNow: 0).stringValue()
            lTransactionsDict["TransactionType"]        = self.getTransactionString(appendedModel.transType)
            lTransactionsDict["accounType"]             = UserModel.shared.agentServiceTypeString()
            lTransactionsDict["DestinationNumberAccountType"] = self.viewModel[index].accountType
            
            arrayParam["TransactionsCellTower"] = TransactionsCellTower
            arrayParam["lExternalReference"]    = lExternalReference
            arrayParam["lGeoLocation"]          = lGeoLocation
            arrayParam["lProximity"]            = lProximity
            arrayParam["lTransactions"]         = lTransactionsDict
            
            finalArray.append(arrayParam)
        }
        print(finalArray)
        return finalArray
    }
    
    //     parsing multipayment module
    func parseFinalPaymentResponse(resp: AnyObject) {
        println_debug(resp)
    }
    
    deinit {
        
    }
    
}

//MARK:- TableView Delegates & Datasource
extension PayToTransactionConfirmationVC : UITableViewDelegate, UITableViewDataSource, PayToTransactionCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactionModelArray[section].cell.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return transactionModelArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return transactionModelArray[indexPath.section].cell[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch transactionModelArray.count {
        case 1:
            return ""
        default:
            let payDetailsStr = "Payment Details".localized
            let stringHeader = String.init(format: "\(payDetailsStr) %i", section + 1)
            return stringHeader
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            if let label = headerView.textLabel {
                label.font = UIFont.init(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: 17.0)
                label.textAlignment = .center
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let height : CGFloat = (self.transactionModelArray.count > 1) ? 40.00 : 0.00
        return height
    }
    
    // for timer addition in cell
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard transactionModelArray.count < 1 else { return nil }
        return timerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        guard transactionModelArray.count < 1 else { return 0.00 }
        return 80.00
    }
    
    //MARK:- PayToTransactionCellDelegate
    func transactionCellEvent(cell: PayToTransactionCell) {
        self.tableV.reloadData()
    }
    
    func didTapButton(height: CGFloat, inCell cell: PayToTransactionCell) {
        let frame = cell.frame
        cell.frame = .init(x: frame.origin.x, y: frame.origin.y, width: frame.width, height: height)
        UIView.setAnimationsEnabled(false)
        self.tableV.beginUpdates()
        self.tableV.layoutIfNeeded()
        self.tableV.layoutSubviews()
        self.tableV.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    func showPaybutton(hide: Bool) {
        self.payBtn.isHidden = hide
    }
    
    func ratingCountUpdate(rate: Int, text: String) {
        ratingCount = rate
        ratingText = text
    }
    
    
    func openCountryORContacts(cell: PayToTransactionCell, type: ButtonType) {
        switch type {
        case .countryView:
            self.navigationController?.present(countryViewController(delegate: cell), animated: true, completion: nil)
        case .contactView:
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "PayToMultiSelection") as? PayToMultiSelection {
                vc.delegate = cell
                vc.hideScan = true
                if let keyWindow = UIApplication.shared.keyWindow {
                    vc.view.frame = keyWindow.bounds
                    vc.view.tag = 4354
                    UIView.transition(with: keyWindow, duration: 1.5, options: UIView.AnimationOptions.curveEaseIn,
                                      animations:
                        {keyWindow.addSubview(vc.view)}
                        , completion: nil)
                    keyWindow.makeKeyAndVisible()
                }
            }
        }
    }
    
    func didSelectOptions(cell: PayToTransactionCell, option: PTMultiSelectionOption) {
        switch option {
        case .favorite:
            let nav = self.openFavoriteFromNavigation(cell, withFavorite: .payto, selectionType: true)
            isSelectedFromFav = true
            self.navigationController?.present(nav, animated: true, completion: nil)
        case .contact:
            let nav = UitilityClass.openContact(multiSelection: false, cell, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
            isSelectedFromFav = true
            self.navigationController?.present(nav, animated: true, completion: nil)
        case .scanQR:
            break
        }
    }
    
    
    
    func didSelectRateService(cell: PayToTransactionCell) {
        let dict = displayResponse[safe: cell.index ?? 0]
        let story = UIStoryboard.init(name: "Topup", bundle: nil)
        guard let ratingScreen = story.instantiateViewController(withIdentifier: String(describing: PaymentRateScreenViewController.self)) as? PaymentRateScreenViewController else { return }
        ratingScreen.modalPresentationStyle = .overCurrentContext
        ratingScreen.delegate = cell
        ratingScreen.destinationNumber = (dict?.safeValueForKey("destination") as? String != nil) ? dict?.safeValueForKey("destination") as? String : ""
//        ratingScreen.modalPresentationStyle = .fullScreen
        self.navigationController?.present(ratingScreen, animated: true, completion: nil)
    }
    
    func formattingTransactionResponseIntoModel() {
        for (index, dictionary) in self.displayResponse.enumerated() {
            print(index)
            print(dictionary)
            var cellArray = [PayToTransactionCell]()
            
            let numberCell = self.tableV.dequeueReusableCell(withIdentifier: TransTypeStringIdentifiers.labelDesign) as? PayToTransactionCell
            numberCell?.wrapCellContent(dict: dictionary, type: .number)
            
            let nameCell = self.tableV.dequeueReusableCell(withIdentifier: TransTypeStringIdentifiers.labelDesign) as? PayToTransactionCell
            nameCell?.wrapCellContent(dict: dictionary, type: .name)
            
            let accountTypeCell = self.tableV.dequeueReusableCell(withIdentifier: TransTypeStringIdentifiers.labelDesign) as? PayToTransactionCell
           
            if self.viewModel.count > 0 {
                accountTypeCell?.wrapCellContent(dict: dictionary, type: .accountType, accountType: self.viewModel[index].accountType)
                instaaccountType.append(self.viewModel[index].accountType ?? "")
              //  instaaccountType = self.viewModel[index].accountType ?? ""
                
            } else {
                
                
                accountTypeCell?.wrapCellContent(dict: dictionary, type: .accountType, accountType: agentTypeNew)
                instaaccountType.append(agentTypeNew)
               // instaaccountType = agentTypeNew
            }
            
            let amountCell = self.tableV.dequeueReusableCell(withIdentifier: TransTypeStringIdentifiers.labelDesign) as? PayToTransactionCell
            amountCell?.wrapCellContent(dict: dictionary, type: .amount)
            
            let commissionCell = self.tableV.dequeueReusableCell(withIdentifier: TransTypeStringIdentifiers.labelDesign) as? PayToTransactionCell
            commissionCell?.wrapCellContent(dict: dictionary, type: .commission)
            
            let totalAmountCell = self.tableV.dequeueReusableCell(withIdentifier: TransTypeStringIdentifiers.labelDesign) as? PayToTransactionCell
            totalAmountCell?.wrapCellContent(dict: dictionary, type: .totalAmount)
            
            let remarks = self.tableV.dequeueReusableCell(withIdentifier: TransTypeStringIdentifiers.labelDesign) as? PayToTransactionCell
            remarks?.wrapCellContent(dict: dictionary, type: .remarks)
            
            let rate = self.tableV.dequeueReusableCell(withIdentifier: TransTypeStringIdentifiers.rateMyService) as? PayToTransactionCell
            rate?.delegate = self
            rate?.index = index
            rate?.wrapCellContent(dict: dictionary, type: .rating)
            guard let cellRating  = rate else { return }
            
           print(self.viewModel)
            
            //merchant
            
          //  if (!agentTypeNew.isEmpty) || (self.agentType[index] != "SUBSBR"){
                
             //   if agentTypeNew != "SUBSBR"{
            
           // if viewModel.indices.contains(index){
        // if self.viewModel[index].accountType != "SUBSBR"{
            if let bName = dictionary.safeValueForKey("businessName") as? String  , bName != "" {
                
                let businessName = self.tableV.dequeueReusableCell(withIdentifier: TransTypeStringIdentifiers.labelDesign) as? PayToTransactionCell

               // if self.viewModel[index].accountType != "SUBSBR"{
                  businessName?.wrapCellContent(dict: dictionary, type: .businessName)
                //}
                
                
                guard let cellTypeNumber = numberCell else { return }
                guard let cellTypeName   = nameCell else { return }
                guard let cellTypeAccountType   = accountTypeCell else { return }
                
                guard let cellTypeBusinessName = businessName else { return }
                guard let cellTypeAmount = amountCell else { return }
                guard let cellTypeCommission = commissionCell else { return }
                guard let cellTypeTotalAmount = totalAmountCell else { return }
                
                if dictionary.safeValueForKey("localRemark") as? String != "" {
                    guard let cellTypeRemarks = remarks else { return }
                    
                    if let type = dictionary.safeValueForKey("CICOPayTo") as? PayToType, (type.type == .cashIn || type.type == .cashOut || type.type == .transferTo || self.viewModel.first?.cashInCashOut != "") {
                        
                        if let amnt = dictionary.safeValueForKey("amount") as? String, amnt.count > 0 {
                            let doubleAmount = (amnt as NSString).floatValue
                          
                            if doubleAmount >= 1000.0 {
                                
                                
                                cellArray = [cellTypeNumber, cellTypeName, cellTypeAccountType, cellTypeBusinessName,cellTypeAmount,cellTypeCommission,cellTypeTotalAmount,cellTypeRemarks, cellRating]
                            } else {
                                cellArray = [cellTypeNumber, cellTypeName, cellTypeAccountType, cellTypeBusinessName,cellTypeAmount,cellTypeCommission,cellTypeTotalAmount,cellTypeRemarks]
                            }
                        } else {
                            cellArray = [cellTypeNumber, cellTypeName,cellTypeAccountType, cellTypeBusinessName,cellTypeAmount,cellTypeCommission,cellTypeTotalAmount,cellTypeRemarks]
                        }
                    } else {
                        if let amnt = dictionary.safeValueForKey("amount") as? String, amnt.count > 0 {
                            let doubleAmount = (amnt as NSString).floatValue
                            if doubleAmount >= 1000.0 {
                                cellArray = [cellTypeNumber, cellTypeName,cellTypeAccountType, cellTypeBusinessName,cellTypeAmount,cellTypeRemarks,cellRating]
                            } else {
                                cellArray = [cellTypeNumber, cellTypeName,cellTypeAccountType, cellTypeBusinessName,cellTypeAmount,cellTypeRemarks]
                            }
                        } else {
                            cellArray = [cellTypeNumber, cellTypeName,cellTypeAccountType, cellTypeBusinessName,cellTypeAmount,cellTypeRemarks]
                        }
                    }
                    
                } else {
                    if let type = dictionary.safeValueForKey("CICOPayTo") as? PayToType, (type.type == .cashIn || type.type == .cashOut || type.type == .transferTo || self.viewModel.first?.cashInCashOut != "") {

                        if let amnt = dictionary.safeValueForKey("amount") as? String, amnt.count > 0 {
                            let doubleAmount = (amnt as NSString).floatValue
                            if doubleAmount >= 1000.0 {
                                
                                if self.viewModel.count > 0 {
                                    if self.viewModel[index].accountType == "MER"{
                                      cellArray = [cellTypeNumber, cellTypeName,cellTypeAccountType, cellTypeBusinessName,cellTypeAmount,cellTypeCommission,cellTypeTotalAmount, cellRating]
                                    }else{
                                       cellArray = [cellTypeNumber, cellTypeName,cellTypeAccountType, cellTypeBusinessName,cellTypeAmount,cellTypeCommission,cellTypeTotalAmount]
                                    }
                                }else if agentTypeNew != ""{
                                    if agentTypeNew == "MER"{
                                         cellArray = [cellTypeNumber, cellTypeName,cellTypeAccountType, cellTypeBusinessName,cellTypeAmount,cellTypeCommission,cellTypeTotalAmount, cellRating]
                                    }else{
                                        cellArray = [cellTypeNumber, cellTypeName,cellTypeAccountType, cellTypeBusinessName,cellTypeAmount,cellTypeCommission,cellTypeTotalAmount]
                                    }
                                }
                            } else {
                                cellArray = [cellTypeNumber, cellTypeName,cellTypeAccountType, cellTypeBusinessName,cellTypeAmount,cellTypeCommission,cellTypeTotalAmount]
                            }
                        } else {
                            cellArray = [cellTypeNumber, cellTypeName,cellTypeAccountType, cellTypeBusinessName,cellTypeAmount]
                        }
                    } else {
                        if let amnt = dictionary.safeValueForKey("amount") as? String, amnt.count > 0 {
                            let doubleAmount = (amnt as NSString).floatValue
                            if doubleAmount >= 1000.0 {
                                if self.viewModel.count > 0 {
                                    if self.viewModel[index].accountType == "MER"{
                                        cellArray = [cellTypeNumber, cellTypeName,cellTypeAccountType, cellTypeBusinessName,cellTypeAmount,cellRating]
                                    }else{
                                        cellArray = [cellTypeNumber, cellTypeName,cellTypeAccountType, cellTypeBusinessName,cellTypeAmount]
                                    }
                                }
                                else if instaPay {
                                    cellArray = [cellTypeNumber, cellTypeName,cellTypeAccountType, cellTypeBusinessName,cellTypeAmount]
                                }
                                else if agentTypeNew != ""{
                                    if agentTypeNew == "MER"{
                                        cellArray = [cellTypeNumber, cellTypeName,cellTypeAccountType, cellTypeBusinessName,cellTypeAmount,cellRating]
                                    }else{
                                        cellArray = [cellTypeNumber, cellTypeName,cellTypeAccountType, cellTypeBusinessName,cellTypeAmount]
                                    }
                                }
                            } else {
                                cellArray = [cellTypeNumber, cellTypeName,cellTypeAccountType, cellTypeBusinessName,cellTypeAmount]
                            }
                        } else {
                            cellArray = [cellTypeNumber, cellTypeName,cellTypeAccountType, cellTypeBusinessName,cellTypeAmount]
                        }
                    }
                }
                
      //  }
     //   }
                
            }
            else {
                guard let cellTypeNumber = numberCell else { return }
                guard let cellTypeName   = nameCell else { return }
                guard let cellTypeAccountType   = accountTypeCell else { return }
                guard let cellTypeAmount = amountCell else { return }
                guard let cellTypeRemarks = remarks else { return }
                guard let cellTypeCommission = commissionCell else { return }
                guard let cellTypeTotalAmount = totalAmountCell else { return }
                
                if let type = dictionary.safeValueForKey("CICOPayTo") as? PayToType, (type.type == .cashIn || type.type == .cashOut || type.type == .transferTo || self.viewModel.first?.cashInCashOut != "") {
                   
                    if dictionary.safeValueForKey("localRemark") as? String != "" {
                        cellArray = [cellTypeNumber, cellTypeName, cellTypeAccountType, cellTypeAmount, cellTypeCommission, cellTypeTotalAmount,cellTypeRemarks]
                    } else {
                        cellArray = [cellTypeNumber, cellTypeName,cellTypeAccountType, cellTypeAmount, cellTypeCommission, cellTypeTotalAmount]
                    }
                } else {
                    if dictionary.safeValueForKey("localRemark") as? String != "" {
                        if dictionary.safeValueForKey("localRemark") as? String == "Request Money" {
                            cellArray = [cellTypeNumber, cellTypeName, cellTypeAmount, cellTypeRemarks]
                        } else {
                            cellArray = [cellTypeNumber, cellTypeName,cellTypeAccountType, cellTypeAmount, cellTypeRemarks]
                        }
                    } else {
                        cellArray = [cellTypeNumber, cellTypeName,cellTypeAccountType, cellTypeAmount]
                    }
                }
            }
            
            if let type = dictionary.safeValueForKey("CICOPayTo") as? PayToType, (type.type == .cashIn || type.type == .cashOut || type.type == .transferTo || self.viewModel.first?.cashInCashOut != "") {
                
            } else {
                if let cashback = dictionary.safeValueForKey("kickback") as? String {
                    if cashback == "Kick-Back Rules Not Define" {
                    } else {
                        let cashback = self.tableV.dequeueReusableCell(withIdentifier: TransTypeStringIdentifiers.labelDesign) as? PayToTransactionCell
                        cashback?.wrapCellContent(dict: dictionary, type: .cashback)
                        
                        let button = self.tableV.dequeueReusableCell(withIdentifier: TransTypeStringIdentifiers.buttonDesign) as? PayToTransactionCell
                        button?.wrapCellContent(dict: dictionary, type: .button)
                        button?.delegate = self
                        guard let cashBk = cashback else { return }
                        guard let btn    = button else { return }
                        
                        cellArray.append(cashBk)
                        cellArray.append(btn)
                    }
                }
            }
            
            let model = PaytoTransactionCellModel.init(section: index, cell: cellArray)
            self.transactionModelArray.append(model)
        }
    }
    
    
}

extension PayToTransactionConfirmationVC : PTWebResponseDelegate {
    
    func parseMultiPaymentResult(data: Any) {
        var arrayOfTransactions = Array<Dictionary<String,Any>>()
        if let dict = data as? Array<Dictionary<String,Any>> {
            for dictionaries in dict {
                if let xmlString = dictionaries["Data"] as? String {
                    let parserString = SWXMLHash.parse(xmlString)
                    self.enumerate(indexer: parserString)
                    if transaction["resultdescription"] as? String == "Transaction Successful" {
                        arrayOfTransactions.append(self.transaction)
                    } else {
                        DispatchQueue.main.async(execute: {
                            alertViewObj.wrapAlert(title: "", body: (self.transaction["resultdescription"] as? String ?? "").localized, img: UIImage(named : "AppIcon"))
                            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                                self.navigationController?.popViewController(animated: true)
                            })
                            alertViewObj.showAlert(controller: self)
                        })
                        PaymentLogTransactions.manager.logPaymentApiFailureCase(transaction.safeValueForKey("destination") as? String ?? "", errCode: transaction.safeValueForKey("resultcode") as? String ?? "")
                    }
                }
            }
            
            if arrayOfTransactions.count > 0 {
                DispatchQueue.main.async {
                    guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "PTRecieptViewController") as? PTRecieptViewController else { return }
                    vc.transactionDetails = arrayOfTransactions
                    if self.varType.type == .transferTo || self.varType.type == .cashOutFree || self.varType.type == .cashOut {
                        vc.typeResponse = self.varType
                    }
                    print(arrayOfTransactions)
                    print(self.viewModel)
                    print(self.instaaccountType)
                    vc.displayResponse = self.displayResponse
                    vc.viewModel = self.viewModel
                  vc.instaAccount = self.instaaccountType
                   // vc.instaAccount = ""
                    self.navigationController?.setNavigationBarHidden(false, animated: true)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                userDef.removeObject(forKey: "FromInstaPaySession")
                userDef.synchronize()
            }
        }
    }
    
    func webSuccessResult(data: Any, screen: String) {
        
        if screen == ScreenName.singlePayGeneric {
            if let dataDict = data as? String {
                guard let dic = OKBaseController.convertToDictionary(text: dataDict) else { return }
                if let code = dic["Code"] as? Int, code == 200 {
                    //tushar
                    //this is multi payto total amount i am tracking to show user insufficient balance
                    UserDefaults.standard.removeObject(forKey: "MultiplePayTOAmount")
                    if let parseData = dic["Data"] {
                       self.parseAndMoveToReceipt(data: parseData)
                    }
                }
//                else if let code = dic["Code"] as? Int, code == 303 {//Advance merchant only 5 number validation
//                    if let parseData = dic["Data"] {
//                       self.parseAndMoveToReceipt(data: parseData)
//                    }
//                }
                else{
                    let parseData = dic["Data"] as? String ?? ""
                    let parserString = SWXMLHash.parse(parseData)
                    self.enumerate(indexer: parserString)
                    
                    DispatchQueue.main.async(execute: {
                        
                        alertViewObj.wrapAlert(title: "", body: (self.transaction["resultdescription"] as? String ?? "").localized, img: #imageLiteral(resourceName: "dashboard_pay_send"))
                        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                            guard let paramsSinglePay = self.getSinglePayParamas() else { return }
                            
                            self.failureCount = self.failureCount + 1
                            if self.failureCount == 2 {
                                self.failureViewModel.sendDataToFailureApi(request: paramsSinglePay as Any, response: dic, type: FailureHelper.FailureType.PAYTO.rawValue, finished: {
                                    
                                })
                            }
                        })
                        alertViewObj.showAlert(controller: self)
                    })
                    
                  
                }
            }
        } else if screen == ScreenName.singlePay {
            self.parseAndMoveToReceipt(data: data)
        }
        
    }
    
    func parseAndMoveToReceipt(data: Any) {
        let xmlString = SWXMLHash.parse(data as! String)
        self.enumerate(indexer: xmlString)
        if transaction["resultdescription"] as? String == "Transaction Successful" {
            DispatchQueue.global(qos: .background).async {
                if self.requestMoney {
                    self.updateRequestMoneyStatus()
                }
            }
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PaymentSuccess"), object: nil)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PTRecieptViewController") as? PTRecieptViewController
                vc?.transactionDetails = [self.transaction]
                vc?.displayResponse = self.displayResponse
                vc?.typeResponse = self.varType
                if self.instaPay {
                    vc?.instaPay = self.instaPay
                    vc?.agentModel = self.agentModel
                    vc?.instaRemarks = self.instaRemarks
                }
                vc?.instaAccount = self.instaaccountType
                vc?.requestMoney = self.requestMoney
                vc?.viewModel = self.viewModel
                self.navigationController?.setNavigationBarHidden(false, animated: true)
                self.navigationController?.pushViewController(vc!, animated: true)
            }
        } else {
            DispatchQueue.main.async {
                PaytoConstants.alert.showErrorAlert(title: nil, body: self.transaction.safeValueForKey("resultdescription") as? String ?? "Please try Again")
            }
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            transaction[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func webFailureResult(screen: String) {
        println_debug("Failed for single pay ")
    }
    
    
    func updateRequestMoneyStatus()
    {
        var requestId : String?
        if let dic = self.displayResponse.first {
            requestId = dic.safeValueForKey("requestId") as? String ?? ""
        }
        let urlStr   = Url.changeStatusOfRequest
        let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
        var urlComponents = URLComponents.init(url: ur, resolvingAgainstBaseURL: false)
        let userData = UserModel.shared
        // add params
        
       // print("updateRequestMoneyStatus-----\(apiForCancelRequest)")

        
        let mobileNUmberQuery       = URLQueryItem(name:"MobileNumber",value : userData.mobileNo)
        let simIdQuery              = URLQueryItem(name:"Simid",value :userData.simID)
        let msIDQuery               = URLQueryItem(name:"MSID" , value:userData.msid)
        let osTypeQuery             = URLQueryItem(name:"OSType", value: userData.osType)
        let otpQuery                = URLQueryItem(name:"OTP", value: userData.simID)
        let requestIDQuery          = URLQueryItem(name:"RequestId", value: "\(requestId ?? "")")
        let statusQuery             = URLQueryItem(name:"Status", value: "Accepted")
        
        urlComponents?.queryItems  = [mobileNUmberQuery,simIdQuery,msIDQuery,osTypeQuery,otpQuery,requestIDQuery,statusQuery]
        guard let apiForCancelRequest = urlComponents?.url else { return }
        
        
       // println_debug("api \(apiForCancelRequest.absoluteString) ")
        print("updateRequestMoneyStatus-----\(apiForCancelRequest)")

        
        JSONParser.GetCancelApiResponse(apiUrl: apiForCancelRequest, type: "GET") { (isSuccess : Bool, data : Any?) in
            
            //print("updateRequestMoneyStatusdata-----\(data)")

            
            if isSuccess == true {
                
            } else {
                ///
                if let message = data as? String {
                    
                    DispatchQueue.main.async(execute: {
                        
                        alertViewObj.wrapAlert(title: "", body: message.localized, img: UIImage(named : "AppIcon"))
                        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                            
                        })
                        alertViewObj.showAlert(controller: self)
                    })
                }
            }
        }
        
    }
    
}



extension PayToTransactionConfirmationVC: HiddenCameraDelegate {
    func didCameraFinishedUpdate(success: Bool, images: [UIImage]) {
        var imageArry = [String]()
        var dic = Dictionary<String,Any>()
        if success {
            for image in images {
                let strBase64String = self.convertImageToBase64(image: image)
                imageArry.append(strBase64String)
            }
            dic = ["Base64String":imageArry,"MobileNumber": UserModel.shared.mobileNo]
            if appDelegate.checkNetworkAvail() {
                let web      = WebApiClass()
                web.delegate = self
                let urlStr   = Url.URL_GetImageurls
                let url = getUrl(urlStr: urlStr, serverType: .serverApp)
                println_debug(url)
                web.genericClass(url: url, param: dic as AnyObject, httpMethod: "POST", mScreen: "getImageUrlPayto")
            } else {
//                showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
            }
        }
    }
    
}

extension PayToTransactionConfirmationVC: WebServiceResponseDelegate {
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        if screen == "getImageUrlPayto"{
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        if dic["Code"] as? NSNumber == 200 {
                            guard let imageString = dic["Data"] as? String else { return }
                            var imageStrArray = [String]()
                            let imageArray = imageString.components(separatedBy: ",")
                            for img in imageArray {
                               imageStrArray.append(img.replacingOccurrences(of: " ", with: "%20"))
                            }
                            let dicsend = ["MobileNumber":UserModel.shared.mobileNo,"Msid":msid,"SimId":uuid,"AppBuildNumber":"","AppBuildVersion":"57","AppType":0,"CellTowerId":"","DeviceInfo":"","Type":"PAYTO","Latitude":manager.currentLatitude ?? "0.0","Longitude":manager.currentLongitude ?? "0.0","OsType":1,"ImageUrl":imageStrArray] as [String : Any]
                            if appDelegate.checkNetworkAvail() {
                                let web      = WebApiClass()
                                web.delegate = self
                                let urlStr   = Url.URL_AddUnauthorisedAccessLog
                                let url = getUrl(urlStr: urlStr, serverType: .serverApp)
                                web.genericClass(url: url, param: dicsend as AnyObject, httpMethod: "POST", mScreen: "UploadImageLogPayTo")
                            } else     {
                                //                                showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
                            }
                        }
                    }
                }
            } catch {
                //                showAlert(alertTitle: "", alertBody:"Network error and try again".localized , alertImage: #imageLiteral(resourceName: "r_user"))
            }
        } else if screen == "UploadImageLogPayTo" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    println_debug(dict)
                }
            } catch {
                
            }
        }else if screen == "mMobileValidation"
        {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(dic)
                        print("BankCounterDepositDetailsVC Dic----\(dic)")

                        
                        if dic["Code"]! as! Int == 200
                        {
                            
                            DispatchQueue.main.async {
                               
                                let dataDict = dic["Data"] as? String
                                let tempData = OKBaseController.convertToDictionary(text: dataDict!)
                                self.agentType.append(tempData!["AccountType"]! as! String)
                                self.count = self.count + 1
                                
                                
//                                if self.count == self.mobileNumberArray.count{
//                                    self.tableV.delegate = self
//                                    self.tableV.dataSource = self
//
//
//                                }
                              //  DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
                                   //
                               // })
     
                            }
                        }
                        else {
                            DispatchQueue.main.async {
                                
                                alertViewObj.wrapAlert(title: nil, body:"This Number is not registered with OK$".localized, img: #imageLiteral(resourceName: "phone_alert"))
                                
                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                    
                                }
                                alertViewObj.showAlert(controller: self)
                                
                            }
                        }
                    }
                    
                }
            } catch {
                
            }
        }
    }
}

