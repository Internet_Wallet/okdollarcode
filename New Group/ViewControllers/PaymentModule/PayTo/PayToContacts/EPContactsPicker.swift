//
//  ContactPickersPicker.swift
//  ContactPickers
//
//  Created by Prabaharan Elangovan on 12/10/15.
//  Copyright © 2015 Prabaharan Elangovan. All rights reserved.
//

import UIKit
import Contacts
import IQKeyboardManagerSwift


enum buttonAction : Int {
    case pay = 0
    case share = 1
    case contact = 2
    case call = 3
    case details = 4
}

class ContactNavigationController : UINavigationController {}

public protocol ContactPickerDelegate: class {
    func contact(_: ContactPickersPicker, didContactFetchFailed error: NSError)
    func contact(_: ContactPickersPicker, didCancel error: NSError)
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker)
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker])
}

public extension ContactPickerDelegate {
    func contact(_: ContactPickersPicker, didContactFetchFailed error: NSError) { }
    func contact(_: ContactPickersPicker, didCancel error: NSError) { }
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) { }
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]) { }
}

typealias ContactsHandler = (_ contacts : [CNContact] , _ error : NSError?) -> Void
typealias okHandler = (_ contactsOk : [String] , _ error : NSError?) -> Void

public enum SubtitleCellValue{
    
    case phoneNumber
    case email
    case birthday
    case organization
    
}

open class ContactPickersPicker: UIViewController, UISearchBarDelegate , UITableViewDataSource , UITableViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var resultSearchController : UISearchBar!
    
    // MARK: - Properties
    open weak var contactDelegate: ContactPickerDelegate?
    var contactsStore: CNContactStore?
    //this variable will be true in only two case when coming from oversea recharge and other number just to push the user directly to pay screen
    var isComingFromOtherNumberTopUp = false
    //this variable will be used to change the text in contact edit view controller
    var isComingFromPayTo = false
  
    var orderedContacts = [String: [CNContact]]() //Contacts ordered in dicitonary alphabetically
    var sortedContactKeys = [String]()
    var collapsableContactArray = [String:Array<Bool>]()
    var sortedContactBool = [String]()
    var contactAll : [CNContact] = [CNContact]()
    var selectedContacts = [ContactPicker]()
    var filteredContacts = [CNContact]()
    var okContacts = [String]()
    var subtitleCellValue = SubtitleCellValue.phoneNumber
    var multiSelectEnabled: Bool = false //Default is single selection contact
    var cellTouched = false
    var cellRow = 0
    var cellSection = 0
    var lastIndex : IndexPath?
    var searchString: String?
    // MARK: - Lifecycle Methods
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        registerContactCell()
        setUpNavigation()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 70
        tableView.backgroundColor = UIColor.colorWithRedValue(redValue: 215.0, greenValue: 215.0, blueValue: 215.0, alpha: 1.0)
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        if #available(iOS 11.0, *) {
            self.tableView.contentInsetAdjustmentBehavior = .never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
        self.resultSearchController.showsCancelButton = false
        resultSearchController.delegate = self
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
               doneToolbar.barStyle = .default

               let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
               let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))

               let items = [flexSpace, done]
               doneToolbar.items = items
               doneToolbar.sizeToFit()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
       
        
        if let searchTextField = resultSearchController.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
           // searchTextField.frame = CGRect(x: 0.0, y: 0.0, width: 0.0, height: 0.0)
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
               // searchTextField.font = myFont
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.keyboardType = .asciiCapable
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor = .white
                searchTextField.inputAccessoryView = doneToolbar
            }
        }
        
//        if let textfield = resultSearchController.value(forKey: "searchField") as? UITextField {
//            textfield.textColor = UIColor.black
//            if let backgroundview = textfield.subviews.first {
//                let myFont = UIFont(name: appFont, size: 14)
//                textfield.font = myFont
//                textfield.keyboardType = .asciiCapable
//                // Background color
//                backgroundview.backgroundColor = UIColor.white
//                textfield.placeholder = "Search".localized
//                textfield.backgroundColor = .white
//                textfield.inputAccessoryView = doneToolbar
//                // Rounded corner
//                backgroundview.layer.cornerRadius = 10;
//                backgroundview.clipsToBounds = true;
//            }
//        }

        
        
        let view = self.resultSearchController.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
        if subView.isKind(of: UITextField.self) {
        subView.tintColor = ConstantsColor.navigationHeaderTransaction
        }
        }
    }
    
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default

        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))

        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
    
        
      //  txtMobileNumber.inputAccessoryView = doneToolbar
    }

    @objc func doneButtonAction(){
        self.view.endEditing(true)
       // txtMobileNumber.resignFirstResponder()
    }
    
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      //  addDoneButtonOnKeyboard()
        getContact()
    }
    
    
    fileprivate func getContact(){
        self.tableView.isUserInteractionEnabled = true
        self.contactAll.removeAll()
        self.orderedContacts.removeAll()
        reloadContacts()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    @objc private func keyboardWillShow(notification: NSNotification) {
      
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height + 100, right: 0)
        }
      
    }

    @objc private func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.2, animations: {
            self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        })
      
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }

    
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(onTouchCancelButton), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        if multiSelectEnabled {
            let doneButton = UIButton(type: .custom)
            doneButton.setTitle("Done".localized, for: .normal)
            //doneButton.setImage(#imageLiteral(resourceName: "reportSearch").withRenderingMode(.alwaysOriginal), for: .normal)
            doneButton.addTarget(self, action: #selector(onTouchDoneButton), for: .touchUpInside)
            doneButton.frame = CGRect(x: navTitleView.frame.size.width - 60, y: ypos, width: 30, height: 30)
            navTitleView.addSubview(doneButton)
        }
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 140, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "Contacts".localized + " "//ContactGlobalConstants.Strings.contactsTitle + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 25.0) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
        
        self.checkContactPermission()
    }
    
    private func checkContactPermission() {
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            break
        case .denied, .restricted, .notDetermined:
            showSettingsAlert(sms: "Please Enable Contact Permission".localized)
            break
        }
    }
    
    private func showSettingsAlert(sms : String) {
        let alert = UIAlertController(title: nil, message: sms, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Settings".localized, style: .default) { action in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        })
        alert.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel) { action in
        })
        present(alert, animated: true)
    }
    
    
    func initializeSearchBar() {
//        self.resultSearchController = ( {
//
//            let controller = UISearchController(searchResultsController: nil)
//            controller.searchResultsUpdater = self
//            controller.dimsBackgroundDuringPresentation = false
//            controller.hidesNavigationBarDuringPresentation = false
//            controller.searchBar.sizeToFit()
//            controller.searchBar.delegate = self
//            controller.searchBar.barTintColor = kYellowColor
//            controller.searchBar.keyboardType = .default
//            controller.searchBar.autocorrectionType = .no
//            //self.tableView.tableHeaderView = controller.searchBar
//            return controller
//
//        })()
        
    }
    
    func inititlizeBarButtons() {
        
        let buttonIcon      = UIImage(named: "tabBarBack")
        let leftBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: self, action: #selector(onTouchCancelButton))
        leftBarButton.image = buttonIcon
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        if multiSelectEnabled {
            let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(onTouchDoneButton))
            self.navigationItem.rightBarButtonItem = doneButton
        }
        
    }
    
    fileprivate func registerContactCell() {
        
        let podBundle = Bundle(for: self.classForCoder)
        if let bundleURL = podBundle.url(forResource: ContactGlobalConstants.Strings.bundleIdentifier, withExtension: "bundle") {
            if let bundle = Bundle(url: bundleURL) {
                let cellNib = UINib(nibName: ContactGlobalConstants.Strings.cellNibIdentifier, bundle: bundle)
                tableView.register(cellNib, forCellReuseIdentifier: "Cell")
            }
            else {
                assertionFailure("Could not load bundle")
            }
        }
        else {
            // Cell loafing changes according to cell. if any crashes come then put bundle as nil.
            let cellNib = UINib(nibName: ContactGlobalConstants.Strings.cellNibIdentifier, bundle: Bundle.main)
            tableView.register(cellNib, forCellReuseIdentifier: "Cell")
        }
        
    }
    
    override open func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

//    // MARK: - Initializers
//    convenience public init(delegate: ContactPickerDelegate?) {
//        
//        self.init(delegate: delegate, multiSelection: false)
//    }
//    
//    convenience public init(delegate: ContactPickerDelegate?, multiSelection : Bool) {
//        
//        self.init(style: .plain)
//        self.multiSelectEnabled = multiSelection
//        contactDelegate = delegate
//    }
//
//    convenience public init(delegate: ContactPickerDelegate?, multiSelection : Bool, subtitleCellType: SubtitleCellValue) {
//        
//        self.init(style: .plain)
//        self .multiSelectEnabled = multiSelection
//        contactDelegate = delegate
//        subtitleCellValue = subtitleCellType
//        
//    }
    
    // MARK: - Contact Operations
    open func reloadContacts() {
        contactsStore = nil
        getContacts( {(contacts, error) in
            if (error == nil) {
                self.contactAll = contacts
                self.getOkDollarContacts({ ( okContacts , errorK) in
                    if (errorK == nil ) {
                        self.okContacts = okContacts
                    DispatchQueue.main.async(execute: {
                            self.tableView.delegate  = self
                            self.tableView.dataSource = self
                            self.tableView.reloadData()
                        })
                    }
                })
            }
        })
    }
    
    func getContacts(_ completion:  @escaping ContactsHandler) {
       
        if contactsStore == nil {
            //ContactStore is control for accessing the Contacts
            contactsStore = CNContactStore()
        }
        let error = NSError(domain: "ContactPickerPickerErrorDomain", code: 1, userInfo: [NSLocalizedDescriptionKey: "No Contacts Access"])
        
        switch CNContactStore.authorizationStatus(for: CNEntityType.contacts) {
        case CNAuthorizationStatus.denied, CNAuthorizationStatus.restricted:
            //User has denied the current app to access the contacts.
            
            let productName = Bundle.main.infoDictionary!["CFBundleName"]!
            
            let alert = UIAlertController(title: "Unable to access contacts", message: "\(productName) does not have access to contacts. Kindly enable it in privacy settings ", preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: {  action in
                completion([], error)
                self.dismiss(animated: true, completion: {
                    self.contactDelegate?.contact(self, didContactFetchFailed: error)
                })
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            
        case CNAuthorizationStatus.notDetermined:
            //This case means the user is prompted for the first time for allowing contacts
            contactsStore?.requestAccess(for: CNEntityType.contacts, completionHandler: { (granted, error) -> Void in
                //At this point an alert is provided to the user to provide access to contacts. This will get invoked if a user responds to the alert
                if  (!granted ) {
                    DispatchQueue.main.async(execute: { () -> Void in
                        completion([], error! as NSError?)
                    })
                }
                else {
                    self.getContacts(completion)
                }
            })
        case  CNAuthorizationStatus.authorized:
            //Authorization granted by user for this app.
            var contactsArray = [CNContact]()
            let contactFetchRequest = CNContactFetchRequest(keysToFetch: allowedContactKeys())
            do {
                try contactsStore?.enumerateContacts(with: contactFetchRequest, usingBlock: { (contact, stop) -> Void in
                    //Ordering contacts based on alphabets in firstname
                    // changes to display all contacts according to user details
                    
                    if contactsArray.count > 1 {
                        for phone in contact.phoneNumbers {
                            let contactMutable = contact.mutableCopy() as! CNMutableContact//CNMutableContact()
                            let stringPhone = phone.value.stringValue
                            if stringPhone.count > 3 {
                                let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue :stringPhone ))
                                contactMutable.givenName = (contact.givenName.count > 0) ? contact.givenName : "Unknown"
                                contactMutable.phoneNumbers = [homePhone]
                                contactsArray.append(contactMutable)
                                var key: String = "#"
                                //If ordering has to be happening via family name change it here.
                                if let firstLetter = contactMutable.givenName[0..<1] , firstLetter.containsAlphabets() {
                                    key = firstLetter.uppercased()
                                }
                                var contacts = [CNContact]()
                                if let segregatedContact = self.orderedContacts[key] {
                                    contacts = segregatedContact
                                }
                                
                                if contactMutable.phoneNumbers.count>0{
                                    contacts.append(contactMutable)
                                    self.orderedContacts[key] = contacts
                                    self.collapsableContactArray[key] = Array.init(repeating: false, count: contacts.count)
                                }
                            }
                        }
                    } else {
                       
                        if contact.phoneNumbers.count>0{
                            contactsArray.append(contact)
                            
                            var key: String = "#"
                            //If ordering has to be happening via family name change it here.
                            if let firstLetter = contact.givenName[0..<1] , firstLetter.containsAlphabets() {
                                key = firstLetter.uppercased()
                            }
                            var contacts = [CNContact]()
                            if let segregatedContact = self.orderedContacts[key] {
                                contacts = segregatedContact
                            }
                            contacts.append(contact)
                            self.orderedContacts[key] = contacts
                            self.collapsableContactArray[key] = Array.init(repeating: false, count: contacts.count)
                        }
                    }
                })
                self.sortedContactKeys = Array(self.orderedContacts.keys).sorted(by: <)
                self.sortedContactBool = Array(self.collapsableContactArray.keys).sorted(by: <)
                if self.sortedContactKeys.first == "#" {
                    self.sortedContactKeys.removeFirst()
                    self.sortedContactKeys.append("#")
                }
                if self.sortedContactBool.first == "#" {
                    self.sortedContactBool.removeFirst()
                    self.sortedContactBool.append("#")
                }
                completion(contactsArray, nil)
            }
                //Catching exception as enumerateContactsWithFetchRequest can throw errors
            catch let error as NSError {
                println_debug(error.localizedDescription)
            }
        }
    }
    
    
    
    
    func getOkDollarContacts (_ completion:  @escaping okHandler) {
        
        if appDelegate.checkNetworkAvail() {
            let urlString = String.init(format: "%@", TopupUrl.getOkContacts)
            let url = URL(string: urlString)
            let params = self.JSONStringFromAnyObject(value: getOkCOntactParamas() as AnyObject)
            
            DispatchQueue.main.async {
                TopupWeb.genericClassWithoutLoader(url: url!, param: params as AnyObject, httpMethod: "POST") { [weak self](response, success) in
                    DispatchQueue.main.async {
                        if success {
                            if let dictionary = response as? Dictionary<String,Any> {
                                if let code = dictionary.safeValueForKey("Code") as? Int, code == 0 {
                                    let numbers = dictionary["Data"] as? String
                                    let numbersArray = numbers?.parseJSONString
                                    completion(numbersArray! as! [String],nil)
                                } else if let code = dictionary.safeValueForKey("Code") as? Int, code == 500 {
                                    println_debug("Avneeeeeeesh:- sync is failed. ")
                                    //self?.showErrorAlert(errMessage: "Please Try Again!".localized)
                                }
                            }
                        }
                    }
                }
            }
            
        } else {
            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.noInternet.localized)
            return
        }
    }
    
    
    func getOkCOntactParamas() -> Dictionary<String,Any> {
        var finalParams = Dictionary<String,Any>()
        var loginInfo = Dictionary<String,Any>()
        loginInfo["AppId"] = LoginParams.setUniqueIDLogin()
        loginInfo["MobileNumber"]  = UserModel.shared.mobileNo
        loginInfo["Msid"] = getMsid()
        loginInfo["Ostype"] = 1
        loginInfo["Otp"] = ""
        loginInfo["Simid"] = simid
        let phoneNumbers : Array<String> = self.getAllCOntactsArrayConverted()
        finalParams["Login"] = loginInfo
        finalParams["PhoneNumber"] = phoneNumbers
        
        return finalParams
    }
    
    func getAllCOntactsArrayConverted () -> Array<String> {
        var contactsString : Array<String> = [String]()
        if contactAll.count > 0 {
            for contact in contactAll {
                if let tempContact = contact as? CNContact {
                  var numbers = ""
                    if let countX = tempContact.phoneNumbers.count as? Int,countX > 1 {
                        for lableName in tempContact.phoneNumbers {
                            numbers.append("\(lableName.value.stringValue.getModifiedContact())")
                            numbers.append(",")
                        }
                    }
                    else {
                        if let phoneLbl = tempContact.phoneNumbers.last {
                        numbers.append("\(phoneLbl.value.stringValue.getModifiedContact())")
                        }
                    }
                    contactsString.append(numbers)
                }
            }
        } else {
            return contactsString
        }
        return contactsString
    }
    
    func JSONStringFromAnyObject(value: AnyObject, prettyPrinted: Bool = true) -> String {
        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : nil
        if JSONSerialization.isValidJSONObject(value) {
            do {
                let data = try JSONSerialization.data(withJSONObject: value, options: options!)
                if let string = String(data: data, encoding: String.Encoding.utf8) {
                    return string
                }
            } catch {
                println_debug(error)
            }
        }
        return ""
    }
    func allowedContactKeys() -> [CNKeyDescriptor]{
        //We have to provide only the keys which we have to access. We should avoid unnecessary keys when fetching the contact. Reducing the keys means faster the access.
        return [CNContactNamePrefixKey as CNKeyDescriptor,
                CNContactGivenNameKey as CNKeyDescriptor,
                CNContactFamilyNameKey as CNKeyDescriptor,
                CNContactOrganizationNameKey as CNKeyDescriptor,
                CNContactBirthdayKey as CNKeyDescriptor,
                CNContactImageDataKey as CNKeyDescriptor,
                CNContactThumbnailImageDataKey as CNKeyDescriptor,
                CNContactImageDataAvailableKey as CNKeyDescriptor,
                CNContactPhoneNumbersKey as CNKeyDescriptor,
                CNContactEmailAddressesKey as CNKeyDescriptor,
                CNContactPostalAddressesKey as CNKeyDescriptor ,
        ]
    }
    
    
    @objc func expendAndCollapsedAction(sender : UILongPressGestureRecognizer ) {
        
        if (sender.state == .ended) {
            
            //        if lastIndex != nil {
            //            self.tableView.beginUpdates()
            //            self.tableView.reloadRows(at: [lastIndex!], with: UITableView.RowAnimation.automatic)
            //            self.tableView.endUpdates()
            //        }
            NSLog("UIGestureRecognizerStateEnded");
            let superViewX = sender.view as! ContactPickerCell
            println_debug("current cell : \(superViewX)")
            let indexPath : IndexPath = self.tableView.indexPath(for: superViewX)!
            let boolForSection = collapsableContactArray[sortedContactBool[indexPath.section]]
            let isOpen = boolForSection![indexPath.row]
            
            cellSection = indexPath.section
            cellRow = indexPath.row
            self.lastIndex = indexPath
            
            println_debug("Section: \(cellSection) and row: \(cellRow)")
            if isOpen {
                cellTouched = false
                self.collapsableContactArray[sortedContactBool[indexPath.section]]?.insert(false, at: indexPath.row)
            }
            else {
                cellTouched = true
                self.collapsableContactArray[sortedContactBool[indexPath.section]]?.insert(true, at: indexPath.row)
                //            UIView.animate(withDuration: 0.5, animations: {
                //                self.tableView.beginUpdates()
                //                //self.tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
                //                self.tableView.reloadData()
                //                self.tableView.endUpdates()
                //                self.view.layoutIfNeeded()
                //                self.view.superview?.layoutIfNeeded()
                //            })
                
            }
            UIView.animate(withDuration: 0.5, animations: {
                self.tableView.beginUpdates()
                //self.tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
                self.tableView.reloadData()
                self.tableView.endUpdates()
                self.view.layoutIfNeeded()
                self.view.superview?.layoutIfNeeded()
            })
        }
    }
    
    
    @objc func performActionOnBUtton (sender : UIButton) {
        let buttonActionCase = sender.tag
        let cell = tableView.cellForRow(at: IndexPath(row: cellRow, section: cellSection)) as! ContactPickerCell
        let selectedContact = cell.contact!
        
        switch buttonActionCase {
        case buttonAction.pay.rawValue:
            do {
                if multiSelectEnabled {
                    //Keeps track of enable=ing and disabling contacts
                    if cell.accessoryType == UITableViewCell.AccessoryType.checkmark {
                        cell.accessoryType = UITableViewCell.AccessoryType.none
                        cell.contactSelectedImageView.isHidden = true
                        selectedContacts = selectedContacts.filter(){
                            return selectedContact.contactId != $0.contactId
                        }
                    }
                    else {
                        cell.contactSelectedImageView.isHidden = false
                        cell.accessoryType = UITableViewCell.AccessoryType.checkmark
                        selectedContacts.append(selectedContact)
                    }
                }
                else {
                    //Single selection code
                   // resultSearchController.isActive = false
                    self.dismiss(animated: true, completion: {
                        DispatchQueue.main.async {
                            self.contactDelegate?.contact(self, didSelectContact: selectedContact)
                        }
                    })
                }
            }
        case buttonAction.share.rawValue:
            do {
                let newImage = self.getQrCodeForTheCard(contact: selectedContact)
                let activityViewController = UIActivityViewController(activityItems: [newImage], applicationActivities: nil)
                DispatchQueue.main.async {self.navigationController?.present(activityViewController, animated: true, completion: nil)}
            }
        case buttonAction.contact.rawValue:
            print("contact button clicked")
        case buttonAction.call.rawValue:
            print("call button clicked")
            do {
                let callURL = URL(string: "telprompt://\(selectedContact.phoneNumbers.last?.phoneNumber ?? "")")
                UIApplication.shared.open(callURL!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        case buttonAction.details.rawValue:
            print("details button clicked")
            let story = PaytoConstants.global.storyboard()
            guard let deatils = story.instantiateViewController(withIdentifier: "EPContactEditViewController") as? EPContactEditViewController else { return }
            deatils.contact = selectedContact
            deatils.selectedContactColor = cell.contactInitialLabel.backgroundColor as! Color
            deatils.qrImage = self.getQrCodeForTheCard(contact: selectedContact)
            self.navigationController?.pushViewController(deatils, animated: true)
        default:
            break;
        }
        
    }
    
    open func getQrCodeForTheCard(contact : ContactPicker) -> UIImage {
        
        let name = contact.firstName+" "+contact.lastName
        var email = ""
        var phonenumber = ""
        for number in contact.phoneNumbers {
            let tempString = "TEL:\(number.phoneNumber);"
            phonenumber.append(tempString)
        }
        for emailX in contact.emails {
            let tempString = "EMAIL:\(emailX.email);"
            email.append(tempString)
        }//
        var tempString = "ADR:"
        for address in contact.postalAddress {
            var details = ""
            if let stret = address.street as? String , stret.count > 0 {
                let streetX = stret.replacingOccurrences(of: ",", with: "")
                details.append(streetX.trimmingCharacters(in: .whitespacesAndNewlines))
                details.append(",")
            }
            if let city = address.city as? String , city.count > 0 {
                details.append(city.trimmingCharacters(in: .whitespacesAndNewlines))
                details.append(",")
            }
            if let state = address.state as? String , state.count > 0 {
                details.append(state.trimmingCharacters(in: .whitespacesAndNewlines))
                details.append(",")
            }
            if let postelcode = address.postalCode as? String , postelcode.count > 0 {
                details.append(postelcode.trimmingCharacters(in: .whitespacesAndNewlines))
                details.append(",")
            }
            
            if let country = address.country as? String , country.count > 0 {
                details.append(country.trimmingCharacters(in: .whitespacesAndNewlines))
                // details.append(",")
            }
            
            // details.removeLast()
            details.replacingOccurrences(of: "\n", with: "")
            tempString.append(";")
        }
        var myString = ""
        if tempString == "ADR:;" {
            myString = "MECARD:N:\(name);\(phonenumber)\(email)"
        }
        else {
            myString = "MECARD:N:\(name);\(phonenumber)\(email)\(tempString)"
        }
        println_debug(myString)
        let data = myString.data(using: String.Encoding.utf8)
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return UIImage() }
        qrFilter.setValue(data, forKey: "inputMessage")
        guard let qrImage = qrFilter.outputImage else { return UIImage() }
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let scaledQrImage = qrImage.transformed(by: transform)
        let context = CIContext()
        guard let cgImage = context.createCGImage(scaledQrImage, from: scaledQrImage.extent) else { return UIImage()}
        let processedImage = UIImage(cgImage: cgImage)
        return processedImage
    }
    
    
    // MARK: -- UIScrollViewDelegate
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 0 {
            var headerFrame = self.resultSearchController.frame.origin.y
            headerFrame += abs(scrollView.contentOffset.y)
            self.resultSearchController.frame.origin.y = headerFrame
            println_debug(self.resultSearchController.frame.origin.y)
        }
    }
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if self.resultSearchController.frame.height > 150 {
            animateHeader()
        }
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if self.resultSearchController.frame.height > 150 {
            animateHeader()
        }
    }
    
    func animateHeader() {
        //self.resultSearchController.dHeight = 150
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    // MARK: - Table View DataSource
     open func numberOfSections(in tableView: UITableView) -> Int {
         tableView.tableFooterView = UIView()
        if  let text = self.searchString , text.count > 0 {
             if filteredContacts.count == 0 {
                 let noDataLabel = UILabel(frame: tableView.bounds)
                 noDataLabel.text = "No Contact Found!!!".localized
                 noDataLabel.textAlignment = .center
                 tableView.backgroundView = noDataLabel
             } else {
                 tableView.backgroundView = nil
             }
             return 1
         }
         return sortedContactKeys.count
     }
     
     open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if  let text = self.searchString , text.count > 0
         { return filteredContacts.count }
         if let contactsForSection = orderedContacts[sortedContactKeys[section]] {
             return contactsForSection.count
         }
         return 0
     }
    
    // MARK: - Table View Delegates
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ContactPickerCell
        cell.accessoryType = UITableViewCell.AccessoryType.none
        //Convert CNContact to ContactPicker
        var contact: ContactPicker
        
        if  let text = searchString , text.count > 0 && filteredContacts.count > 0 {
            contact = ContactPicker(contact: filteredContacts[(indexPath as NSIndexPath).row])
        } else {
            guard let contactsForSection = orderedContacts[sortedContactKeys[(indexPath as NSIndexPath).section]] else {
                assertionFailure()
                return UITableViewCell()
            }
            contact = ContactPicker(contact: contactsForSection[(indexPath as NSIndexPath).row])
        }
        
        if multiSelectEnabled  && selectedContacts.contains(where: { $0.contactId == contact.contactId }) {
            cell.accessoryType = UITableViewCell.AccessoryType.checkmark
        }
        cell.updateContactsinUI(contact, indexPath: indexPath, subtitleType: subtitleCellValue)
        if let number = (contact.phoneNumbers.last?.phoneNumber)?.getModifiedContact() {
            let isContains = okContacts.contains(number)
                   if isContains { cell.imgOkDollar.isHidden = false }
                   else { cell.imgOkDollar.isHidden = true }
        }
        //        cell.btnPay.addTarget(self, action: #selector(performActionOnBUtton(sender:)), for: .touchUpInside)
        //        cell.btnShare.addTarget(self, action: #selector(performActionOnBUtton(sender:)), for: .touchUpInside)
        //        cell.btnContact.addTarget(self, action: #selector(performActionOnBUtton(sender:)), for: .touchUpInside)
        //        cell.btnCall.addTarget(self, action: #selector(performActionOnBUtton(sender:)), for: .touchUpInside)
        //        cell.btnDetails.addTarget(self, action: #selector(performActionOnBUtton(sender:)), for: .touchUpInside)
        //        let longPress = UILongPressGestureRecognizer.init(target: self, action: #selector(expendAndCollapsedAction(sender:)))
        //        longPress.minimumPressDuration = 0.3
        // cell.addGestureRecognizer(longPress)
        return cell
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! ContactPickerCell
        let selectedContact = cell.contact!
        
        //        let cell = tableView.cellForRow(at: indexPath) as! ContactPickerCell
        //        let selectedContact = cell.contact!
        
        if multiSelectEnabled {
            //Keeps track of enable=ing and disabling contacts
            if cell.accessoryType == UITableViewCell.AccessoryType.checkmark {
                cell.accessoryType = UITableViewCell.AccessoryType.none
                cell.contactSelectedImageView.isHidden = true
                selectedContacts = selectedContacts.filter(){
                    return selectedContact.contactId != $0.contactId
                }
            }
            else {
                cell.contactSelectedImageView.isHidden = false
                cell.accessoryType = UITableViewCell.AccessoryType.checkmark
                selectedContacts.append(selectedContact)
            }
        }
        else {
            

                //Single selection code
               // resultSearchController.isActive = false
                //s     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FromContactsSelected"), object: nil)
                //      self.contactDelegate?.contact(self, didSelectContact: selectedContact)
            
            if selectedContact.phoneNumbers.count > 0{
                
                if isComingFromOtherNumberTopUp{
                    print("genius ur code is called")
                   self.dismiss(animated: true, completion: {
                                         DispatchQueue.main.async {
                                             self.contactDelegate?.contact(self, didSelectContact: selectedContact)
                                         }
                                     })
                }else{
                    
                    
                    tableView.isUserInteractionEnabled = false
                                   let story = PaytoConstants.global.storyboard()
                                   guard let deatils = story.instantiateViewController(withIdentifier: "EPContactEditViewController") as? EPContactEditViewController else { return }
                                   deatils.contact = selectedContact
                                   deatils.isComingFromPayTo = isComingFromPayTo
                                   deatils.previousController = self
                                   deatils.contactDelegate = self.contactDelegate
                                   deatils.selectedContactColor = cell.contactInitialLabel.backgroundColor
                                   deatils.qrImage = self.getQrCodeForTheCard(contact: selectedContact)
                                   self.navigationController?.pushViewController(deatils, animated: true)
                    
                }
              
            }
                
//            self.dismiss(animated: true, completion: {
//               
//            })

//            //Single selection code
//            resultSearchController.isActive = false
//            //s     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FromContactsSelected"), object: nil)
//            //      self.contactDelegate?.contact(self, didSelectContact: selectedContact)
//            tableView.isUserInteractionEnabled = false
//            let story = PaytoConstants.global.storyboard()
//            guard let deatils = story.instantiateViewController(withIdentifier: "EPContactEditViewController") as? EPContactEditViewController else { return }
//            deatils.contact = selectedContact
//            deatils.previousController = self
//            deatils.contactDelegate = self.contactDelegate
//            deatils.selectedContactColor = cell.contactInitialLabel.backgroundColor
//            deatils.qrImage = self.getQrCodeForTheCard(contact: selectedContact)
//            self.navigationController?.pushViewController(deatils, animated: true)
//            //            self.dismiss(animated: true, completion: {
//            //
//            //            })

        }
    }
    
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if cellSection == indexPath.section && cellRow == indexPath.row {
            if self.cellTouched {
                return 120.0
            }else {
                return 64.0
            }
        }
        else {
            return 64.0
        }
    }
    
    open func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        if  let text = resultSearchController.text, text.count > 0 { return 0 }
        tableView.scrollToRow(at: IndexPath(row: 0, section: index), at: UITableView.ScrollPosition.top , animated: false)        
        return sortedContactKeys.index(of: title)!
    }
    open func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if  let text = resultSearchController.text, text.count > 0 { return 0 }
        return 40
    }
    
    open func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect(x: 30, y: 0, width: tableView.frame.size.width, height: 35))
        view.backgroundColor = UIColor.colorWithRedValue(redValue: 215.0, greenValue: 215.0, blueValue: 215.0, alpha: 1.0)
        if let text = resultSearchController.text, text.count > 0{}
        else {
            let label = MarqueeLabel(frame: CGRect(x: 25, y: 0, width: 40 , height: 40))
            label.type = .continuous
            label.speed = .duration(4)
            if sortedContactKeys.indices.contains(section) {
                label.text =  sortedContactKeys[section] + " "
            }
            
            label.textColor = UIColor.black
            if let navFont = UIFont(name: appFont, size: 17.0) {
                label.font = navFont
            }
            label.textAlignment = .left
            view.addSubview(label)
        }
        return view
    }
    
    open func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if let text = resultSearchController.text, text.count > 0 { return nil }
        return sortedContactKeys
    }
    
    //    override open func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    //        if resultSearchController.isActive , let text = resultSearchController.searchBar.text, text.count > 0{ return nil }
    //        return sortedContactKeys[section]
    //    }
    
    // MARK: - Button Actions
    @objc func onTouchCancelButton(){

        
        self.dismiss(animated: true, completion: nil)
        //        dismiss(animated: true, completion: {
        //            self.contactDelegate?.contact(self, didCancel: NSError(domain: "ContactPickerPickerErrorDomain", code: 2, userInfo: [ NSLocalizedDescriptionKey: "User Canceled Selection"]))
        //        })
    }
    
    @objc func onTouchDoneButton(){
        dismiss(animated: true, completion: {
            //self.contactDelegate?.contact(self, didSelectMultipleContacts: self.selectedContacts)
        })

        //        dismiss(animated: true, completion: {
        //            self.contactDelegate?.contact(self, didSelectMultipleContacts: self.selectedContacts)
        //        })
    }
    
    // MARK: - Search Actions
    open func updateSearchResults( searchText : String ) {
      println_debug(searchText)
        if !searchText.isEmpty {
            let predicate: NSPredicate
            if searchText.count > 0 {
                predicate = CNContact.predicateForContacts(matchingName: searchText)
            } else {
                predicate = CNContact.predicateForContactsInContainer(withIdentifier: contactsStore!.defaultContainerIdentifier())
            }
            let contactFetchRequest = CNContactFetchRequest(keysToFetch: allowedContactKeys())
            contactFetchRequest.predicate = predicate
            filteredContacts.removeAll()
            do {
                try contactsStore?.enumerateContacts(with: contactFetchRequest, usingBlock: { (contact, stop) -> Void in
                    //Ordering contacts based on alphabets in firstname
                    // changes to display all contacts according to user details
                    for phone in contact.phoneNumbers {
                        
                        let contactMutable = contact.mutableCopy() as! CNMutableContact//CNMutableContact()
                        let stringPhone = phone.value.stringValue
                        if stringPhone.count > 3 {
                            
                            let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue :stringPhone ))
                            contactMutable.givenName = (contact.givenName.count > 0) ? contact.givenName : "Unknown"
                            contactMutable.phoneNumbers = [homePhone]
                            
                            var key: String = "#"
                            //If ordering has to be happening via family name change it here.
                            if let firstLetter = contactMutable.givenName[0..<1] , firstLetter.containsAlphabets() {
                                key = firstLetter.uppercased()
                            }
                            self.filteredContacts.append(contactMutable)
                        }
                    }
                })
                  println_debug("\(filteredContacts.count) count")
                  self.tableView.reloadData()
            }
            catch{
                println_debug("Error!")
            }
        } else {
            filteredContacts.removeAll()
            DispatchQueue.main.async {
                self.resultSearchController.text = self.searchString
                self.tableView.reloadData()
                self.tableView.backgroundView = nil
            }
        }
    }
    
    
    public func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
            let value = (searchBar.text! as NSString).replacingCharacters(in: range, with: text)
            if range.location == 0 && text == " " {
                return false
            }
            if searchBar.text!.last == " " && text == " " {
                return false
            }
        if searchBar.text?.count ?? 0 > 40 {
                return false
            }
            if !(text == text.components(separatedBy: NSCharacterSet(charactersIn: SEARCHCHARSETOffer).inverted).joined(separator: "")) {
                println_debug("Char not found!")
                return false
            }
            self.searchString = value
            self.updateSearchResults(searchText: value)
            return restrictMultipleSpacesSearchBar(str: value, searchBar: searchBar, range: range)
        }
    
    
    open func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        self.resultSearchController.showsCancelButton = false
        searchString = nil
        searchBar.endEditing(true)
        self.tableView.reloadData()
        self.tableView.backgroundView = nil
    }
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    public func searchBarTextDidEndEditing(searchBar: UISearchBar) {
         searchBar.showsCancelButton = false
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchString = nil
        self.tableView.delegate  = self
        self.tableView.dataSource = self
        self.tableView.reloadData()
        self.tableView.backgroundView = nil
        return true
    }
    
    
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            searchString = nil
           self.tableView.delegate  = self
           self.tableView.dataSource = self
           self.tableView.reloadData()
           self.tableView.backgroundView = nil
        }
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }
}


extension UISearchBar {
    func setPlaceHolder(text: String?) {
        self.layoutIfNeeded()
        self.placeholder = text
        var textFieldInsideSearchBar:UITextField?
        if #available(iOS 13.0, *) {
            textFieldInsideSearchBar = self.searchTextField
        } else {
            for view : UIView in (self.subviews[0]).subviews {
                if let textField = view as? UITextField {
                    textFieldInsideSearchBar = textField
                }
            }
        }
        //get the sizes
        let offset = UIOffset(horizontal: 0, vertical: 1)
        self.setPositionAdjustment(offset, for: .search)
    }
}
