//
//  ShowQRCodeViewController.swift
//  OK
//
//  Created by Imac on 8/23/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class ShowQRCodeViewController: OKBaseController {
    
    
    @IBOutlet weak var qImageView: UIImageView!
    @IBOutlet weak var qInitialLabel: UILabel!
    @IBOutlet weak var qContainerView: UIView!
    
    @IBOutlet weak var msgLblTitle: UILabel!
    @IBOutlet weak var msgLblSubTitle: UILabel!
    
    
    @IBOutlet weak var contactTitle: UILabel!
    @IBOutlet weak var qrImageView: UIImageView!
    
    var contact : ContactPicker!
    var selectedContactColor : Color?
    var qrImage : UIImage!
    
    override func viewDidLoad() {
        // Do any additional setup after loading the view.
        super.viewDidLoad()
        self.updateImageuser()
        self.updateUI()
        self.setUpNavigation()
        self.msgLblTitle.text = "Scan this QR Code to add this contact's information to contacts".localized
        qContainerView.layer.masksToBounds = true
        qContainerView.layer.cornerRadius = qContainerView.frame.size.width/2
    }
    
    func setUpNavigation() {
        
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 140, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "Select Contact".localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21.0) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
        
    }
    
    @objc override func backAction() {
        self.navigationController?.popViewController(animated: true)
    }

        func updateUI() {
            
                self.contactTitle.text = contact.displayName()
                self.qrImageView.image = qrImage
        }

        func  updateImageuser () {
                //Update all UI in the controller here
                self.qInitialLabel?.text = contact.displayName()
                if contact.thumbnailProfileImage != nil {
                self.qImageView?.image = contact.thumbnailProfileImage
                self.qImageView.isHidden = false
                self.qInitialLabel.isHidden = true
                } else {
                self.qInitialLabel.text = contact.contactInitials()
                self.qInitialLabel.backgroundColor = selectedContactColor
                self.qImageView.isHidden = true
                self.qInitialLabel.isHidden = false
                }
        }
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
