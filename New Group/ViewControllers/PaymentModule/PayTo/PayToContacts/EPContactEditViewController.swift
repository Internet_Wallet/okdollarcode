//
//  EOContactEditViewController.swift
//  OK
//
//  Created by Kethan on 8/10/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import Contacts
import MobileCoreServices
import MessageUI

class EPContactEditViewController: OKBaseController  {
    
//    @IBOutlet weak var contactImageView: UIImageView!
//    @IBOutlet weak var contactInitialLabel: UILabel!
//    @IBOutlet weak var contactContainerView: UIView!
    
//    @IBOutlet weak var txtName: UITextField!
//    @IBOutlet weak var txtMobileNumber: UITextField!
//
//    @IBOutlet weak var txtMobileType: UITextField!
//    @IBOutlet weak var txtEmail: UITextField!
//
//    @IBOutlet weak var QRImageView: UIImageView!
    
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var btnQRCode: UIButton!
    @IBOutlet weak var btnPayTo: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    
    @IBOutlet weak var ContactShareDesignView:UIView!
    
    @IBOutlet weak var lblShare: UILabel! {
        
        didSet{
            lblShare.text = "Share".localized
        }
    }
    
    @IBOutlet weak var lblPay: UILabel! {
        didSet{
            if isComingFromPayTo{
                lblPay.text = "Pay".localized
            }else{
                lblPay.text = "Select".localized 
            }
        }
    }
    
    @IBOutlet weak var lblQRCode: UILabel! {
        
        didSet{
            lblQRCode.text = "QR Code".localized
        }
    }
    
    @IBOutlet weak var shareContactAS: UILabel! {
        
        didSet{
            shareContactAS.text = "Share Contact As".localized
        }
    }
    
    @IBOutlet weak var vcardContactLBL: UILabel! {
        
        didSet{
            vcardContactLBL.text = "vCard Contact".localized
        }
    }
    
    @IBOutlet weak var textLbl: UILabel! {
        
        didSet{
            textLbl.text = "Text".localized
        }
    }
    
    @IBOutlet weak var cancelBtn: UIButton! {
        didSet{
            cancelBtn.setTitle("Cancel".localized, for: .normal)
        }
    }
    
     open weak var contactDelegate: ContactPickerDelegate?
    
     var contact : ContactPicker!
     var selectedContactColor : Color?
     var qrImage : UIImage!
     var store: CNContactStore!
     var previousController:ContactPickersPicker?
     var CHARSET = ""
    
    var isComingFromPayTo = false
        
        
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.setUpNavigation()
       // self.updateImageuser()
       // self.updateUI()
        
        let button = UIButton(type: .custom)
        button.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: 20, height: 20))
        button.setImage(UIImage(named: "edit_blue.png"),for: UIControl.State.normal)
        button.setImage(UIImage(named: "save_iconqr.png"),for: UIControl.State.selected)
        //button.sizeToFit()
        button.addTarget(self, action: #selector(EditContactAction(sender:)), for: .touchUpInside)
        if contact.emails.count == 0 ||  contact.postalAddress.count == 0 {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
        }
        self.tblView.rowHeight = UITableView.automaticDimension
        self.tblView.estimatedRowHeight = 239
        store = CNContactStore()
        
        if ok_default_language == "en"{
            CHARSET =  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789/-,:()@. "
        }else if ok_default_language == "my" {
            CHARSET =  "ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအေ ် ိ ္ ့ ံ ျ ု ဳ ူ ဴ  း  ၚ ွ ီ ြ ၤ ဲ ွ်ၽႊ ႏၽြႊ ႈဥဧ ၿၾ၌ဋ႑ ဍ ၨ ၳ ၡ ႅၻဉဎၺႎ ႍ`ါ ႄၶၦ ၱၷ  ၼဤၸ ၠ၍ ႆၥၮ၎ဩႀဦ ၢ႐ဪႁ ႂၯ ၩ႔႔၏ ာ၁၂၃၄၅၆၇၈၉၀/-,:()@. "
        }
        self.ContactShareDesignView.isHidden = true
        
    }
    
     @objc func onTouchCancelButton(){}
     @IBAction func touchButtonAction(_ sender: UIButton) {
        UIView.animate(withDuration: 0.2) {
            self.ContactShareDesignView.isHidden = true
        }
    }
   
    @IBAction func qrBtnAction(_ sender: UIButton) {
        
        let story = PaytoConstants.global.storyboard()
        guard let qrController = story.instantiateViewController(withIdentifier: "ShowQRCodeViewController") as? ShowQRCodeViewController else { return }
        qrController.contact = contact
        qrController.selectedContactColor = selectedContactColor
        qrController.qrImage = qrImage
        self.navigationController?.pushViewController(qrController, animated: true)
        
    }
    
    
    @IBAction func shareBtnAction(_ sender: Any) {
        UIView.animate(withDuration: 0.2) {
            self.ContactShareDesignView.isHidden = false
        }
    }
    
    
    @IBAction func shareWithVcardAction(_ sender: UIButton) {
        
        let contactMutable: CNMutableContact = CNMutableContact()
        let homePhone = CNLabeledValue(label: self.contact.phoneNumbers[0].phoneLabel, value: CNPhoneNumber(stringValue :self.contact.phoneNumbers[0].phoneNumber ))
        contactMutable.givenName = self.contact.displayName()
        contactMutable.phoneNumbers = [homePhone]
        
        if self.contact.emails.count > 0 {
            var arrEmails : [CNLabeledValue<NSString>] = []
            for emailX in self.contact.emails {
                let emailAdd =   CNLabeledValue(label:(emailX.emailLabel as? String), value:(emailX.email ?? "") as NSString)
                arrEmails.append(emailAdd)
            }
            contactMutable.emailAddresses = arrEmails
            print("having emails")
        }
        if self.contact.postalAddress.count > 0 {
            
            var arrAddress : [CNLabeledValue<CNPostalAddress>] = []
            for addressX in self.contact.postalAddress {
                let address = CNMutablePostalAddress()
                address.street = addressX.street
                address.city = addressX.city
                address.state = addressX.state
                address.postalCode = addressX.postalCode
                address.country = addressX.country
                let add = CNLabeledValue<CNPostalAddress>(label:(addressX.AdressLable), value:address)
                arrAddress.append(add)
            }
            contactMutable.postalAddresses = arrAddress
            print("having address")
            
        }
        
        let fileManager = FileManager.default
        let cacheDirectory = try! fileManager.url(for: FileManager.SearchPathDirectory.cachesDirectory, in: FileManager.SearchPathDomainMask.userDomainMask, appropriateFor: nil, create: true)
        let fileLocation = cacheDirectory.appendingPathComponent("\(String(describing: CNContactFormatter().string(from: contactMutable)!)).vcf")
        let contactData = try! CNContactVCardSerialization.data(with: [contactMutable])
        do {
            try contactData.write(to: fileLocation , options: .atomicWrite)
        } catch {
            print("try error")
        }
        let provider = NSItemProvider(contentsOf: fileLocation)
        let activityItems = [provider]
        let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activityViewController.setValue("Contact", forKey: "Subject")
         DispatchQueue.main.async {self.navigationController?.present(activityViewController, animated: true, completion: {
            UIView.animate(withDuration: 0.2) {
                self.ContactShareDesignView.isHidden = true
            }
        })
        }
    }
    
    @IBAction func shareWithTextAction(_ sender: UIButton) {
        
        var ShairingString = "Name : \(self.contact.displayName()) \n Number : \(self.contact.phoneNumbers[0].phoneNumber) \n "
        var emailsString = " Email : "
        if self.contact.emails.count > 0 {
            for emailX in self.contact.emails {
                let emailAdd = emailX.email
                emailsString.append(emailAdd)
                emailsString.append(",")
            }
            emailsString.removeLast()
            emailsString.append(" \n")
            ShairingString.append(emailsString)
        }
        var addressString = " Address : "
        if self.contact.postalAddress.count > 0 {
            for address in self.contact.postalAddress {
                if let street = address.street as? String , street.count > 0 {
                    addressString.append(street)
                    addressString.append(",")
                }
                if let city = address.city as? String , city.count > 0 {
                    addressString.append("\(city)")
                    addressString.append(",")
                }
                if let state = address.state as? String , state.count > 0 {
                    addressString.append("\(state)")
                    addressString.append(",")
                }
                if let country = address.country as? String , country.count > 0 {
                    addressString.append("\(country)")
                    addressString.append(",")
                }
                if let postalCode = address.postalCode as? String , postalCode.count > 0 {
                    addressString.append("\(postalCode)")
                }
                if let charX = addressString.first , charX == "," {
                    
                    addressString.removeFirst()
                }
                if let charX = addressString.last , charX == "," {
                    
                    addressString.removeLast()
                }
            }
            addressString.append(" \n")
            ShairingString.append(addressString)
        }
        let emailItem = EmailItemProvider()
        emailItem.subject = "Contact"
        emailItem.body = ShairingString
        
        let activityViewController = UIActivityViewController(activityItems: [emailItem], applicationActivities: nil)
        DispatchQueue.main.async { self.navigationController?.present(activityViewController, animated: true, completion: {
            UIView.animate(withDuration: 0.2) {
                self.ContactShareDesignView.isHidden = true
            }
        })
        }
    }
    
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        UIView.animate(withDuration: 0.2) {
            self.ContactShareDesignView.isHidden = true
        }
    }
    
    
    @IBAction func payBtnAction(_ sender: UIButton) {
        
        let selectedContact = self.contact
        //Single selection code
        self.dismiss(animated: true, completion: {
            DispatchQueue.main.async {
                self.contactDelegate?.contact(self.previousController!, didSelectContact: selectedContact!)
                self.presentingViewController?.dismiss(animated: true, completion: nil)
            }
        })
       }
    
    func setUpNavigation() {

        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2

        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)

        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 140, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "Select Contact".localized + " "//ContactGlobalConstants.Strings.selectContactTitle + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21.0) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center

        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView

    }
    
    @objc func EditContactAction (sender : UIButton) {
        if sender.isSelected == true {
              sender.isSelected = false
             //sender.setImage(UIImage(named: "edit_blue.png"),for: UIControl.State.normal)
          }
        else {
             sender.isSelected = true
            //sender.setImage(UIImage(named: "save_iconqr.png"),for: UIControl.State.normal)
         }
        self.enableEditingAndSaveAction(isselectedFlag: sender.isSelected)
    }
    
    @objc override func backAction() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func CallToNumberAction(sender : UIButton) {
        
        let callURL = URL(string: "telprompt://\(contact.phoneNumbers.last?.phoneNumber ?? "")")
        UIApplication.shared.open(callURL!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
    }
    
    @objc func facetimeAction(sender : UIButton) {
        
        self.facetime(phoneNumber: (contact.phoneNumbers.last?.phoneNumber ?? ""))
    }
    
    @objc func messageAction(sender : UIButton) {
        let phoneNumber = contact.phoneNumbers.last?.phoneNumber ?? ""
        let messageVC = MFMessageComposeViewController()
        messageVC.body = "";
        messageVC.recipients = [phoneNumber]
        messageVC.messageComposeDelegate = self
        self.present(messageVC, animated: true, completion: nil)
    }
    
    private func facetime(phoneNumber:String) {
        if let facetimeURL:NSURL = NSURL(string: "facetime://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(facetimeURL as URL)) {
                application.open(facetimeURL as URL , options: [:]) { (isOpens) in
                    if isOpens {
                        print("sucess Facetime")
                    }
                    else {
                     print("failed Facetime")
                    }
                }//open(facetimeURL as URL);
            }
        }
    }
    func enableEditingAndSaveAction(isselectedFlag : Bool) {
        
        let cell :EditContactCellTwo = self.tblView.cellForRow(at: IndexPath.init(row: 1, section: 0)) as! EditContactCellTwo
        if isselectedFlag {
            
            if contact.emails.count == 0 {
                cell.txtEmail.isUserInteractionEnabled = true
                cell.txtEmail.text = ""
                cell.bottomView2.isHidden = false
            }
            if contact.postalAddress.count == 0 {
                cell.txtAddress.isUserInteractionEnabled = true
                cell.txtAddress.isHidden = false
                cell.lblAddress.isHidden = true
                cell.lblAddress.text = ""
                cell.txtAddress.text = ""
                cell.bottomView1.isHidden = false
                cell.txtAddress.becomeFirstResponder()
            }
        }
        else {
             cell.txtAddress.isUserInteractionEnabled = false
             cell.txtEmail.isUserInteractionEnabled = false
             cell.bottomView1.isHidden = true
             cell.bottomView2.isHidden = true
            if cell.txtAddress.text == "" || cell.lblAddress.text == "No Address." {
                cell.lblAddress?.text = "No Address.".localized
                cell.txtAddress.text = ""
                cell.txtAddress.isHidden = true
                cell.lblAddress.isHidden = false
            }
            if cell.txtEmail.text == "" || cell.txtEmail.text == "No Email." {
                cell.txtEmail?.text = "No Email.".localized
            }
            else{
                cell.lblAddress.text = cell.txtAddress.text
                cell.txtAddress.isHidden = true
                cell.lblAddress.isHidden = false
                self.showProgressView()
                if cell.txtAddress.text!.count == 0 && cell.lblAddress.text != "No Address."{
                   self.saveContact(email: cell.txtEmail.text! , AndAddress: cell.lblAddress.text!)
                }
                else {
                     self.saveContact(email: cell.txtEmail.text! , AndAddress: cell.txtAddress.text!)
                }
            }
        }
    }
    
    func saveContact(email : String , AndAddress address:String){
        //Email
        let contact : CNMutableContact! = self.contact.storeContect?.mutableCopy() as? CNMutableContact
        if email != "" {
            let workEmail = CNLabeledValue.init(label: CNLabelWork, value: NSString.init(string: email))
            contact.emailAddresses = [workEmail]
        }
        if address != "" {
            let addressArray = address.split(separator: ",")
            // Address
            let address = CNMutablePostalAddress()
            if addressArray.count == 5 {
                
                address.street = String(addressArray[0])
                address.city = String(addressArray[1])
                address.state = String(addressArray[2])
                address.postalCode = String(addressArray[3])
                address.country = String(addressArray[4])
            }
            else if addressArray.count == 4 {
                address.street = String(addressArray[0])
                address.city = String(addressArray[1])
                address.state = String(addressArray[2])
                address.country = String(addressArray[3])
            }
            else if addressArray.count == 3{
                
                address.street = String(addressArray[0])
                address.city = String(addressArray[1])
                address.state = String(addressArray[2])
            }
            else if addressArray.count == 2{
                address.street = String(addressArray[0])
                address.city = String(addressArray[1])
            }
            else if addressArray.count == 1 {
                address.street = String(addressArray[0])
            }
            else {
                print("Nothing to save")
            }
            let home = CNLabeledValue<CNPostalAddress>(label:CNLabelHome, value:address)
            contact.postalAddresses = [home]
        }
        let saveRequest = CNSaveRequest()
        saveRequest.update(contact)
        do {
            try store.execute(saveRequest)
        } catch {print("error")}
        print("saved")
        if email != "" && address != "" {
            self.navigationItem.rightBarButtonItem = nil
        }
        self.removeProgressView()
    }
    

    /*
    func updateUI() {
        
        self.txtName.text = contact.displayName()
        var email = ""
        var phonenumber = ""
        var numberType = ""
        for number in contact.phoneNumbers {
            let tempString = "\(number.phoneNumber)"
            phonenumber.append(tempString)
            let tempStringType = "\(number.phoneLabel)"
            numberType.append(tempStringType)
        }
        for emailX in contact.emails {
            let tempString = "\(emailX.email)"
            email.append(tempString)
        }
        self.txtEmail.text = email
        self.txtMobileType.text = numberType
        self.txtMobileNumber.text = phonenumber
        self.QRImageView.image = qrImage
    }
    
    func  updateImageuser () {
            //Update all UI in the controller here
            self.contactInitialLabel?.text = contact.displayName()
            if contact.thumbnailProfileImage != nil {
                self.contactImageView?.image = contact.thumbnailProfileImage
                self.contactImageView.isHidden = false
                self.contactInitialLabel.isHidden = true
            } else {
                self.contactInitialLabel.text = contact.contactInitials()
                self.contactInitialLabel.backgroundColor = selectedContactColor
                self.contactImageView.isHidden = true
                self.contactInitialLabel.isHidden = false
            }
       }
 
    
    private func checkContactsAccess() {
        switch CNContactStore.authorizationStatus(for: .contacts) {
        // Update our UI if the user has granted access to their Contacts
        case .authorized:
            self.accessGrantedForContacts()
            
        // Prompt the user for access to Contacts if there is no definitive answer
        case .notDetermined :
            self.requestContactsAccess()
            
        // Display a message if the user has denied or restricted access to Contacts
        case .denied,
             .restricted:
            let alert = UIAlertController(title: "Privacy Warning!",
                                          message: "Permission was not granted for Contacts.",
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func requestContactsAccess() {
        
        store.requestAccess(for: .contacts) { granted , error in
            if granted {
                DispatchQueue.main.async {
                    self.accessGrantedForContacts()
                    return
                }
            }
        }
    }
    
    // This method is called when the user has granted access to their address book data.
    private func accessGrantedForContacts() {
        self.saveContact()
    }
    
    func saveContact(){
        
            let contact = CNMutableContact()
            contact.givenName = self.txtName.text!
            contact.familyName = self.contact.lastName
            contact.phoneNumbers = [CNLabeledValue(
                label:CNLabelHome,
                value:CNPhoneNumber(stringValue:self.txtMobileNumber.text!))]
            
            let workEmail = CNLabeledValue.init(label: CNLabelWork, value: NSString.init(string: self.txtEmail.text!))
            contact.emailAddresses = [workEmail]
            let saveRequest = CNSaveRequest()
            saveRequest.update(contact)
            do {
                 try store.execute(saveRequest)
            } catch {print("error")}
            print("saved")
            self.removeProgressView()
            self.backAction()
        }
    *//*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EPContactEditViewController : ContactPickerDelegate , UITableViewDelegate , UITableViewDataSource , MFMessageComposeViewControllerDelegate , UITextFieldDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     if indexPath.row == 0 {
           return 298
        }
     else {
        return UITableView.automaticDimension
        }
     }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {  return 2 }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell : EditContactCell = tableView.dequeueReusableCell(withIdentifier: "EditContactCell") as! EditContactCell
            cell.updateCellUI(self.contact,index:indexPath,selectedColor: self.selectedContactColor ?? .clear)
            cell.btnCall.addTarget(self, action: #selector(CallToNumberAction(sender:)), for: .touchUpInside)
            cell.btnMessage.addTarget(self, action: #selector(messageAction(sender:)), for: .touchUpInside)
            cell.btnFaceTime.addTarget(self, action: #selector(facetimeAction(sender:)), for: .touchUpInside)
            return cell
        }
        else {
            let cell : EditContactCellTwo = tableView.dequeueReusableCell(withIdentifier: "EditContactCellTwo") as! EditContactCellTwo
            cell.updateCellUITwo(self.contact , index: indexPath)
            return cell
            
        }
      
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
            dismiss(animated: true, completion: nil)
        case .failed:
            print("Message failed")
            dismiss(animated: true, completion: nil)
        case .sent:
            print("Message was sent")
            dismiss(animated: true, completion: nil)
        default:
            break
        }
    }
    
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]) {
        for contact in contacts {
            decodeContact(contact: contact, isMulti: true)
        }
    }
    
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        decodeContact(contact: contact, isMulti: false)
    }
    
    func decodeContact(contact: ContactPicker, isMulti: Bool) {
        
       // println_debug(intContactSelectionRow)
        
      
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textField{
            
            let value = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            if range.location == 0 && string == " " {
                return false
            }
            if textField.tag == 101 {
                let maxLength = 30
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }
            if textField.tag == 102 {
                let maxLength = 100
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
            if restrictMultipleSpaces(str: value, textField: textField) {
                return true
            }
            else {
                if  textField.text?.last == " " {
                    textField.text?.removeLast()
                }
              return false
            }
        }
        else {
            return true
        }
    }

    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }
    
}


class EditContactCell : UITableViewCell {
    
    @IBOutlet weak var containerFrameMain: UIView!
    
    @IBOutlet weak var contactImageView: UIImageView!
    @IBOutlet weak var contactInitialLabel: UILabel!
    @IBOutlet weak var contactContainerView: UIView!
    
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    
    @IBOutlet weak var btnFaceTime: UIButton!
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var btnCall: UIButton!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
        selectionStyle = UITableViewCell.SelectionStyle.none
        
        contactContainerView.layer.masksToBounds = true
        contactContainerView.layer.cornerRadius = contactContainerView.frame.size.width/2
        
        contactImageView.layer.masksToBounds = true
        contactImageView.layer.cornerRadius = contactImageView.frame.size.width/2
        contactImageView.isHidden = true
        
        
    }
    

    
    func updateCellUI(_ contact: ContactPicker , index : IndexPath , selectedColor : Color ) {
        
        //Update all UI in the cell here
        self.titleName.text = contact.displayName()
        if contact.phoneNumbers.count > 0{
           // if let number = contact.phoneNumbers[0].phoneNumber{
                  self.phoneNumber.text = contact.phoneNumbers[0].phoneNumber
            //  }
            
        }
      //  updateSubtitleBasedonType(subtitleType, contact: contact)
        if contact.thumbnailProfileImage != nil {
            self.contactImageView?.image = contact.thumbnailProfileImage
            self.contactImageView.isHidden = false
            self.contactInitialLabel.isHidden = true
        } else {
            self.contactInitialLabel.text = contact.contactInitials()
            //updateInitialsColorForIndexPath(indexPath)
            self.contactInitialLabel.backgroundColor = selectedColor
            self.contactImageView.isHidden = true
            self.contactInitialLabel.isHidden = false
        }
    }
}


class EditContactCellTwo : UITableViewCell {
    
    @IBOutlet weak var containerFrameSubModule: UIView!
    
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var bottomView1: UIView!
    @IBOutlet weak var bottomView2: UIView!

    
    
    @IBOutlet weak var lblEmail: UILabel!{
        didSet{
          lblEmail.text = "Email".localized
        }
    }
    
    @IBOutlet weak var lblWorkAddress: UILabel!{
        didSet{
            lblWorkAddress.text = "Work Address".localized
        }
    }
    
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
        selectionStyle = UITableViewCell.SelectionStyle.none
        
        txtEmail.isUserInteractionEnabled = false
        txtAddress.isUserInteractionEnabled = false
        txtAddress.isHidden = true
        
    }
    
    
    func updateCellUITwo(_ contact: ContactPicker , index : IndexPath ) {
        
        bottomView2.isHidden = true
        bottomView1.isHidden = true
        if let emails = contact.emails as? [(email: String, emailLabel: String)], emails.count > 0 {
            
            txtEmail.text = contact.emails[0].email
        }
        else {
            txtEmail.text = "No Email.".localized
        }
        var addressString :String = String()
        if let addresses = contact.postalAddress as? [(street : String , city : String , state : String, postalCode : String , country : String , AdressLable : String)] , addresses.count > 0 {
            if let street = contact.postalAddress[0].street as? String , street.count > 0 {
                addressString.append(street)
            }
            if let city = contact.postalAddress[0].city as? String , city.count > 0 {
                addressString.append(", \(city)")
            }
            if let state = contact.postalAddress[0].state as? String , state.count > 0 {
                addressString.append(", \(state)")
            }
            if let country = contact.postalAddress[0].country as? String , country.count > 0 {
                addressString.append(", \(country)")
            }
            if let postalCode = contact.postalAddress[0].postalCode as? String , postalCode.count > 0 {
                addressString.append(", \(postalCode)")
            }
            if let charX = addressString.first , charX == "," {
                
                addressString.removeFirst()
            }
            if let charX = addressString.last , charX == "," {
                
                addressString.removeLast()
            }
            lblAddress.text = addressString as String
            txtAddress.text = addressString as String
        }
        else {
            lblAddress.text = "No Address.".localized
            txtAddress.text = ""
        }
    }
}

class EmailItemProvider: NSObject, UIActivityItemSource {
    var subject = ""
    var body = ""
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return body
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        return body
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        return subject
    }
}
