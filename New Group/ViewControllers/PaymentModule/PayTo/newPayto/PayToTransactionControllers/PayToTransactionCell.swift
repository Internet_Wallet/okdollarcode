//
//  PayToTransactionCell.swift
//  OK
//
//  Created by Ashish on 12/11/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreFoundation

enum PaytoTransactionCellType {
    case name, number, amount,cashback, button, businessName, remarks, rating
}

struct TransTypeStringIdentifiers {
    static let labelDesign     = "PayToTransactionCellTypeLabel"
    static let buttonDesign    = "PayToTransactionCellTypeButton"
    static let textfieldDesign = "PayToTransactionCellTypeTextfield"
    static let rateMyService   = "PayToTransactionCellTypeRate"
}

struct PaytoTransactionCellModel {
    var section = 0
    var cell : [PayToTransactionCell] = [PayToTransactionCell]()
}

protocol PayToTransactionCellDelegate {
    func transactionCellEvent(cell: PayToTransactionCell)
    func didTapButton(height: CGFloat, inCell cell: PayToTransactionCell)
    func didSelectRateService(cell: PayToTransactionCell)
    func openCountryORContacts(cell: PayToTransactionCell, type: ButtonType)
    func didSelectOptions(cell: PayToTransactionCell, option: PTMultiSelectionOption)
}

class PayToTransactionCell: UITableViewCell, PaymentRateDelegate, UITextFieldDelegate, CountryLeftViewDelegate , CountryViewControllerDelegate, PTMultiSelectionDelegate, FavoriteSelectionDelegate, ContactPickerDelegate {
    
    
    struct CountryPayTo {
        var code = ""
        var flag = ""
        
        init(code: String, flag: String) {
            self.code = code
            self.flag = flag
        }
    }
    
    var leftViewCountry                = PaytoViews.updateView()
    var leftViewCountryForConfirmation = PaytoViews.updateView()
    var contactRightView               = ContactRightView.updateView()

    
    @IBOutlet var keyLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!
    
    @IBOutlet var myNumberButton: UIButton!
    @IBOutlet var otherNumberButton: UIButton!
    
    @IBOutlet var myNumberSelectionImage: UIImageView!
    @IBOutlet var otherNumberSelectionImage: UIImageView!

    
    @IBOutlet var mobileNumberTextfield: UITextField!
    @IBOutlet var confirmMobileTextfield: UITextField!
    
    @IBOutlet var mobileNumberConstraints: NSLayoutConstraint!
    @IBOutlet var cnfMobileNumberConstraints: NSLayoutConstraint!
    
    var indexPath = IndexPath()
    
    var index : Int?
    
    var delegate : PayToTransactionCellDelegate?
    
    var otherNumber : String?

    @IBOutlet weak var rateService: UILabel!
    @IBOutlet weak var stackviews: UIStackView!
    @IBOutlet weak var ratebtn: UIButton!
    
    func append09Format(_ dest: String) -> String {
        if dest.hasPrefix("00") {
            var number = dest.deletingPrefix("00")
            number = "+" + number
            let country = identifyCountry(withPhoneNumber: number)
            let final = number.replacingOccurrences(of: country.countryCode, with: "0")
            return final
        }
        return ""
    }
    
    fileprivate func wrapAmount(key: String) -> String {
        let number = NSDecimalNumber(string: key)
        
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        if let num = formatter.string(from: number) {
            return (num == "NaN") ? "" : num
        }
        return ""
    }


    
    func wrapCellContent(dict: Dictionary<String,Any>, type: PaytoTransactionCellType) {
        switch type {
        case .number:
            self.keyLabel.text = "Receiver".localized
            self.valueLabel.text = self.append09Format((dict.safeValueForKey("destination") as? String != nil) ? dict.safeValueForKey("destination") as? String ?? "" : "")
            break
        case .name:
            self.keyLabel.text = "Name".localized
            self.valueLabel.text = dict.safeValueForKey("destinationName") as? String ?? ""
            break
        case .amount:
            self.keyLabel.text = "Amount".localized
            self.valueLabel.text = self.wrapAmount(key: (dict.safeValueForKey("amount") as? String != nil) ? dict.safeValueForKey("amount") as? String ?? "" : "") + " " + "MMK"
            break
        case .cashback:
            self.keyLabel.text = "Cashback".localized
            self.valueLabel.text = self.wrapAmount(key: (dict.safeValueForKey("kickback") as? String != nil) ? dict.safeValueForKey("kickback") as? String ?? "" : "") + " " + "MMK"
            break
        case .button:
            
            self.myNumberButton.setTitle("My Number".localized, for: .normal)
            self.otherNumberButton.setTitle("Other Number".localized, for: .normal)
            
            self.mobileNumberTextfield.placeholder = "Enter Mobile Number".localized
            self.confirmMobileTextfield.placeholder = "Confirm Mobile Number".localized
            
            self.myNumberSelectionImage.isHighlighted = true
            self.otherNumberSelectionImage.isHighlighted = false
            self.mobileNumberTextfield.isHidden = true
            self.confirmMobileTextfield.isHidden = true
            self.cnfMobileNumberConstraints.constant = 0.0
            self.mobileNumberConstraints.constant = 0.0
            self.myNumberButton.addTargetClosure(closure: { (sender) in
                self.myNumberSelectionImage.isHighlighted = true
                self.otherNumberSelectionImage.isHighlighted = false
                self.cnfMobileNumberConstraints.constant = 0.0
                self.mobileNumberConstraints.constant = 0.0
                if let delegate = self.delegate {
                    self.mobileNumberTextfield.leftView = nil
                    self.confirmMobileTextfield.leftView = nil
                    self.mobileNumberTextfield.rightView = nil
                    self.mobileNumberTextfield.isHidden = true
                    self.confirmMobileTextfield.isHidden = true
                    let height = 50.00 + self.cnfMobileNumberConstraints.constant + self.mobileNumberConstraints.constant
                    delegate.didTapButton(height: height, inCell: self)
                    UIView.animate(withDuration: 0.5, animations: {
                        self.layoutIfNeeded()
                    })
                }
            })
            
            self.otherNumberButton.addTargetClosure(closure: { (sender) in
                self.myNumberSelectionImage.isHighlighted = false
                self.otherNumberSelectionImage.isHighlighted = true

                self.cnfMobileNumberConstraints.constant = 50.0
                self.mobileNumberConstraints.constant = 50.0
                
                self.mobileNumberTextfield.isHidden = false
                
                self.confirmMobileTextfield.isHidden = false
                
                self.mobileNumberTextfield.delegate = self
                self.confirmMobileTextfield.delegate = self
                
                self.mobileNumberTextfield.text = ""
                self.confirmMobileTextfield.text = ""
                
                self.mobileNumberTextfield.leftViewMode = .always
                self.confirmMobileTextfield.leftViewMode = .always
                self.mobileNumberTextfield.leftView = self.leftViewCountry
                self.confirmMobileTextfield.leftView = self.leftViewCountryForConfirmation
                
                self.leftViewCountry.delegate = self
                
                self.mobileNumberTextfield.rightViewMode = .always
                self.mobileNumberTextfield.rightView = self.contactRightView
                
                self.contactRightView.delegate = self
                if let delegate = self.delegate {
                    let height = 50.00 + self.cnfMobileNumberConstraints.constant + self.mobileNumberConstraints.constant
                    delegate.didTapButton(height: height, inCell: self)
                    UIView.animate(withDuration: 0.5, animations: {
                        self.layoutIfNeeded()
                    })
                }
            })
            break
        case .businessName:
            self.keyLabel.text = "Business Name".localized
            self.valueLabel.text = dict.safeValueForKey("businessName") as? String ?? ""
        case .remarks:
            self.keyLabel.text = "Remarks".localized
            self.valueLabel.text = dict.safeValueForKey("localRemark") as? String ?? ""
        case .rating:
            self.rateService.text = "Rate my service".localized
            self.ratebtn.addTargetClosure(closure: { (sender) in
                self.delegate?.didSelectRateService(cell: self)
            })
        }
    }
    
    func ratingShow(_ rate: Int) {
        for view in stackviews.subviews {
            if view.tag <= rate {
                if let img = view as? UIImageView {
                    img.isHighlighted = true
                }
            }
        }
    }
    
    //MARK:- Country Delegate

    func openAction(type: ButtonType) {
        self.delegate?.openCountryORContacts(cell: self, type: type)
    }
    
    func clearActionType() {
        self.nameORnumber(selectiontype: .number, codeDetails: (countryObject.code,countryObject.flag))
        self.mobileNumberTextfield.text  = ""
        self.confirmMobileTextfield.text = ""
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: true, completion: nil)
    }
    
    func countryViewController(_ list: CountryViewController, country: Country) {

        self.mobileNumberTextfield.text = ""
        self.confirmMobileTextfield.text = ""
        
        countryObject.code = country.dialCode
        countryObject.flag = country.code
        
        let countryCode = String.init(format: "(%@)", country.dialCode)
        leftViewCountry.wrapCountryViewData(img: country.code, str: countryCode)
        leftViewCountryForConfirmation.wrapCountryViewData(img: country.code, str: countryCode)
        
        self.mobileNumberTextfield.leftView = nil
        self.confirmMobileTextfield.leftView = nil
        
        self.mobileNumberTextfield.leftView     = leftViewCountry
        self.mobileNumberTextfield.leftViewMode = UITextFieldViewMode.always
        
        self.confirmMobileTextfield.leftView      = leftViewCountryForConfirmation
        self.confirmMobileTextfield.leftViewMode  = UITextFieldViewMode.always
        
        self.nameORnumber(selectiontype: .number, codeDetails: (country.dialCode,country.code))
        
        list.dismiss(animated: true, completion: nil)
    }

    
    //MARK:- Textfields functionalities
    let mobileNumberAcceptableCharacters = "0123456789"

    var countryObject = CountryPayTo.init(code: "+95", flag: "myanmar")
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == mobileNumberTextfield {
            if countryObject.code == "+95" {
                self.mobileNumberTextfield.text = "09"
            } else {
                self.mobileNumberTextfield.text = ""
            }

        } else if textField == confirmMobileTextfield{
            if countryObject.code == "+95" {
                self.confirmMobileTextfield.text = "09"
            } else {
                self.confirmMobileTextfield.text = ""
            }
        }
        return true
    }
    
    func didSelectOption(option: PTMultiSelectionOption) {
        switch option {
        case .favorite:
            self.delegate?.didSelectOptions(cell: self, option: .favorite)
        case .contact:
            self.delegate?.didSelectOptions(cell: self, option: .contact)
        case .scanQR:
            break
        }
    }
    
    func selectedFavoriteObject(obj: FavModel) {
        let contact = ContactPicker.init(object: obj)
        self.decodeContact(contact: contact)
    }
    
    func contact(_ list: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        self.decodeContact(contact: contact)
        list.dismiss(animated: true, completion: nil)
    }
    
    func contact(_ list: ContactPickersPicker, didCancel error: NSError) {
        list.dismiss(animated: true, completion: nil)
    }
    
    func decodeContact(contact: ContactPicker) {

        let phoneNumberCount = contact.phoneNumbers.count
        var phoneNumber = ""
        
        if phoneNumberCount == 1  {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        }
        else if phoneNumberCount > 1 {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        }
        else {
            
        }
        
        if phoneNumber != "" {
            phoneNumber = phoneNumber.replacingOccurrences(of: ")", with: "")
            phoneNumber = phoneNumber.replacingOccurrences(of: "(", with: "")
            phoneNumber = phoneNumber.trimmingCharacters(in: .whitespaces)
        }
        
        if phoneNumber.hasPrefix("00") {
            _ = phoneNumber.remove(at: String.Index.init(encodedOffset: 0))
            _ = phoneNumber.remove(at: String.Index.init(encodedOffset: 0))
            phoneNumber = "+" + phoneNumber
        }
        
        let cDetails = identifyCountry(withPhoneNumber: phoneNumber) // 1 -> flag, 0 -> code
        var countryCode = String.init(format: "(%@)", "+95")
        if cDetails.0 == "" || cDetails.1 == "" {
            countryObject.flag = "myanmar"
            countryObject.code = "+95"
            leftViewCountry.wrapCountryViewData(img: "myanmar", str: countryCode)
            leftViewCountryForConfirmation.wrapCountryViewData(img: "myanmar", str: countryCode)
        } else {
            countryObject.flag = cDetails.1
            countryObject.code = cDetails.0
            countryCode = String.init(format: "(%@)", cDetails.0)
            leftViewCountry.wrapCountryViewData(img: cDetails.1, str: countryCode)
            leftViewCountryForConfirmation.wrapCountryViewData(img: cDetails.1, str: countryCode)
        }
        
        self.mobileNumberTextfield.leftView     = nil
        self.confirmMobileTextfield.leftView = nil
        
        self.mobileNumberTextfield.leftView     = leftViewCountry
        self.mobileNumberTextfield.leftViewMode = UITextFieldViewMode.always
        
        self.confirmMobileTextfield.leftView      = leftViewCountryForConfirmation
        self.confirmMobileTextfield.leftViewMode  = UITextFieldViewMode.always
        
        var phone = ""
        
        if phoneNumber.hasPrefix("+95") || phoneNumber.hasPrefix("(+95)") {
            phone = phoneNumber.replacingOccurrences(of: cDetails.0, with: "0")
            phone = phone.replacingOccurrences(of: " ", with: "")
            
            if phone.hasPrefix("0") {
                self.mobileNumberTextfield.text = phone
            } else {
                self.mobileNumberTextfield.text = "0" + phone
            }
            
        }else {
            phone = phoneNumber.replacingOccurrences(of: cDetails.0, with: "")
            phone = phone.replacingOccurrences(of: " ", with: "")
            self.mobileNumberTextfield.text = phone
        }
        
        self.nameORnumber(selectiontype: .name, codeDetails: cDetails)
        self.confirmMobileTextfield.text = contact.displayName()
        
        countryCode = String.init(format: "(%@)", countryObject.code)
    }

    func nameORnumber(selectiontype: selectionType, codeDetails: (String, String)) {
        switch selectiontype {
        case .name:
            self.confirmMobileTextfield.isUserInteractionEnabled = false
            self.confirmMobileTextfield.placeholder = "Name"
            self.confirmMobileTextfield.isEnabled = false
            self.confirmMobileTextfield.leftView = nil
            let confirmDets = DefaultIconView.updateView(icon: "name_bill")
            self.confirmMobileTextfield.leftView      = confirmDets
            self.confirmMobileTextfield.leftViewMode  = UITextFieldViewMode.always
            
        case .number:
            self.confirmMobileTextfield.isUserInteractionEnabled = true
            self.confirmMobileTextfield.placeholder = "Confirm Mobile Number"
            self.confirmMobileTextfield.isEnabled = true
            self.confirmMobileTextfield.leftView = nil
            let countryCode = String.init(format: "(%@)", codeDetails.0)
            leftViewCountryForConfirmation.wrapCountryViewData(img: codeDetails.1, str: countryCode)
            self.confirmMobileTextfield.leftView = leftViewCountryForConfirmation
            self.confirmMobileTextfield.leftViewMode  = UITextFieldViewMode.always
        }
    }

    

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let textCount  = text.count
        if textField == self.mobileNumberTextfield {

            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)

            let objectValidation = PayToValidations().getNumberRangeValidation(text)

            let textCount  = text.count
            let validCount = objectValidation.max
            let minValid = objectValidation.min

            let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: mobileNumberAcceptableCharacters).inverted
            let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")

            if string != filteredSet {
                return false
            }

            let char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                self.confirmMobileTextfield.text = ""
                self.nameORnumber(selectiontype: .number, codeDetails: (countryObject.code,countryObject.flag))
            }

            // Normal Validations
            if PayToValidations().checkRejectedNumber(prefix: text) == true {
                return rejectNumberAlgorithm()
            }

            if textCount >= minValid {

            }


            if countryObject.code == "+95" {
                if validCount == textCount {
                    self.mobileNumberTextfield.text = text
                    self.confirmMobileTextfield.becomeFirstResponder()
                    return false
                }
                if textCount > validCount {
                    self.confirmMobileTextfield.becomeFirstResponder()
                    return false
                }
            }

        }

        if textField == self.confirmMobileTextfield {
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                self.nameORnumber(selectiontype: .number, codeDetails: (countryObject.code,countryObject.flag))
                if countryObject.code == "+95" {
                }
            }
            if self.mobileNumberTextfield.text?.count == textCount, String(describing: self.mobileNumberTextfield!.text!.last!) == string {
            }
            if PayToValidations().checkMatchingNumber(string: text, withString: self.mobileNumberTextfield.text!) {
                self.confirmMobileTextfield.text = text
                return false
            } else {  return false  }

        }

        return true
    }
    
    func getOtherNumber() -> String {
        if self.mobileNumberTextfield.isHidden {
            return ""
        } else {
            let agentCode = getDestinationNumber(self.mobileNumberTextfield.text ?? "", withCountryCode: countryObject.code)
            return agentCode
        }
    }

    private func rejectNumberAlgorithm() -> Bool {
        return false
    }


}




















