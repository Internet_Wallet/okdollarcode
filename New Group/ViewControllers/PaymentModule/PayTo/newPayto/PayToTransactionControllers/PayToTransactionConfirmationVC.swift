//
//  PayToTransactionConfirmationVC.swift
//  OK
//
//  Created by Ashish Kumar Singh on 11/28/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PayToTransactionConfirmationVC: PayToBaseViewController {

    var cellHeight : CGFloat = 425
    
    var seconds        = 60

    var timer          = Timer()
    
    var isTimerRunning = false
    
    var displayResponse = [Dictionary<String,Any>]()
    
    var dataCell        = [PayToTransactionCell]()
    
    var transactionModelArray = [PaytoTransactionCellModel]()
    
    var viewModel = [PaySendViewController]()
    
    let manager = GeoLocationManager.shared

    var transaction = Dictionary<String,Any>()

    var timerView =  TimerView.updateView()
    
    var requestMoney : Bool = false
    
    var destinationAgentName = ""
    
    var paymentRequest : String?

    @IBOutlet weak var tableV: UITableView!

    //WebParsing Response
    var responseFinal = [String]()
    
    @IBOutlet weak var stackView: UIStackView!
    
    var varType = "PAYTO"
    @IBOutlet weak var payBtn: UIButton!
        {
        didSet
        {
            self.payBtn.setTitle("TopupPay".localized, for: .normal)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.formattingTransactionResponseIntoModel()

        self.tableV.delegate = self
        self.tableV.dataSource = self
        
        self.setPOPButton()
        
        self.tableV.tableFooterView = UIView.init()
        
        timerView.wrapTimerView(timer: "0:60")
        self.updateTimerViewAccordingtoLayout()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        geoLocManager.startUpdateLocation()
        self.timerPay()
        self.tableV.reloadData()
        
        if transactionModelArray.count > 1 {
            self.title = "MultiPayment".localized
        } else {
            self.title = "Confirmation".localized
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        geoLocManager.stopUpdateLocation()
        timerView.removeFromSuperview()
        timer.invalidate()
        println_debug("Timer Invalidated")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func paytoTypeDifferences(type: PayToTypes) {
        switch type {
        case .singlePay:
            break
        case .multiPay:
            break
        }
    }
    
    func updateTimerViewAccordingtoLayout() {

            timerView.changeFrameForMulti()
        
        if let keyWindow = UIApplication.shared.keyWindow {
            keyWindow.makeKeyAndVisible()
            keyWindow.addSubview(timerView)
        }

    }
    
    func timerPay() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(PayToTransactionConfirmationVC.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {

        seconds -= 1

        let timerString = (seconds <= 9) ? "0:" + "0" + "\(seconds)" :  "0:" + "\(seconds)"
        
        timerView.wrapTimerView(timer: timerString)
        if seconds == 0 {
            if timer.isValid {
                timer.invalidate()
                alertViewObj.wrapAlert(title: nil, body: "Session expired".localized, img: #imageLiteral(resourceName: "dashboard_pay_send"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                    self.navigationController?.popViewController(animated: true)
                })
                alertViewObj.showAlert(controller: self)
            }
        } else if seconds <= 10 {
            timerView.wrapTimerViewUpdate(color: UIColor.red, alpha: 1.00)
            UIView.animate(withDuration: 0.2, animations: {
                self.timerView.wrapTimerViewUpdate(color: UIColor.red, alpha: 0.00)
            }) { (bool) in
                self.timerView.wrapTimerViewUpdate(color: UIColor.red, alpha: 1.00)
            }
        }
    }

    //MARK:- Payment Action
    @IBAction func payAction(_ sender: UIButton) {
        if appDelegate.checkNetworkAvail() {
            let web = PayToWebApi()
            web.delegate = self
            
            if displayResponse.count > 0, displayResponse.count < 2  , let dic = displayResponse.first {
                let source   = dic.safeValueForKey("source") as? String
                let amount   = dic.safeValueForKey("amount") as? String
                let dest     = dic.safeValueForKey("destination") as? String
                let clientOs = dic.safeValueForKey("clientos") as? String
                let clientip = dic.safeValueForKey("clientip") as? String
                let clientTansId = dic.safeValueForKey("transid") as? String
                if requestMoney {
                    let comments     =  dic.safeValueForKey("localRemark") as? String ?? ""
                    let aName = (UserModel.shared.agentType == .merchant) ?  UserModel.shared.businessName : UserModel.shared.name.uppercased()
                    let techComments = String.init(format: "[%@,%@,%@,%@,%@,%@,##%@##%@##]-OKNAME-%@", manager.currentLatitude, manager.currentLongitude, "",UserModel.shared.gender,UserModel.shared.ageNow,"",comments as CVarArg,aName as CVarArg,aName as CVarArg)
                    let final_comment = String.init(format: "%@%@", comments, techComments)
                    
                    let str = String.init(format: PTHelper.reqMoneySinglePay , "PAYTO", UserModel.shared.mobileNo,ok_password ?? "",source ?? "",dest ?? "",amount ?? "",final_comment , UserLogin.shared.token ,clientTansId ?? "",clientip ?? "",clientOs ?? "")
                    
                    guard let urlEncoded = str.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
                    
                    guard  let url = URL.init(string: urlEncoded) else { return }
                    let param = Dictionary<String,Any>()
                    
                    
                    web.genericClassXML(url: url, param: param as AnyObject, httpMethod: "POST", mScreen: ScreenName.singlePay)
                    
                } else {
                    guard let transRequestType = self.viewModel.first else {
                        return
                    }
                    let type = self.getTransactionString(transRequestType.transactionType)
                    
                    let aName = (UserModel.shared.agentType == .merchant) ?  UserModel.shared.businessName : UserModel.shared.name.uppercased()
                    
                    let techComments = String.init(format: "[%@,%@,%@,%@,%@,%@,##%@##%@##]-OKNAME-%@", manager.currentLatitude, manager.currentLongitude, "",UserModel.shared.gender,UserModel.shared.ageNow,"",transRequestType.merchantSubscriberTextfield.text ?? "" as CVarArg,aName as CVarArg,aName as CVarArg)

                    func replaceEscapeCharacter(_ value: String) -> String {
                        if value == "" { return ""}
                        var str = value.replacingOccurrences(of: "$", with: "")
                        str = value.replacingOccurrences(of: "&", with: "AND")
                        str = value.replacingOccurrences(of: "/", with: "")
                        str = value.replacingOccurrences(of: "<", with: "")
                        str = value.replacingOccurrences(of: ">", with: "")
                        return str
                    }

                    let final_comment = String.init(format: "%@%@", replaceEscapeCharacter(transRequestType.remarks.text ?? ""), techComments)

                    let str = String.init(format: PTHelper.singlePay , type, UserModel.shared.mobileNo,ok_password ?? "",source ?? "",dest ?? "",amount ?? "",final_comment ,UserLogin.shared.token)
                    
                    guard let urlEncoded = str.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
                    
                    guard let url = URL.init(string: urlEncoded) else { return }
                    
                    let param = Dictionary<String,Any>()
                    
                    let cell = transactionModelArray[0].cell
                    
                    let cellButton = cell.first(where: {$0.mobileNumberTextfield != nil})
                    
                    var otherNumber = ""
                    if let btn = cellButton {
                        otherNumber = btn.getOtherNumber()
                    }
                    
                    if  otherNumber.count > 0 {
                        
                        var strcash = String.init(format: PTHelper.singlePay , type, UserModel.shared.mobileNo,ok_password ?? "",source ?? "",dest ?? "",amount ?? "",final_comment ,UserLogin.shared.token)
                        
                         strcash = strcash + ";KickBackMsisdn=\(otherNumber)"
                        
                        guard let urlEncodedCash = strcash.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
                        
                        guard let urlcash = URL.init(string: urlEncodedCash) else { return }

                        web.genericClassXML(url: urlcash, param: param as AnyObject, httpMethod: "POST", mScreen: ScreenName.singlePay)
                    } else {
                        web.genericClassXML(url: url, param: param as AnyObject, httpMethod: "POST", mScreen: ScreenName.singlePay)
                    }
                }
            } else {
                guard let url = URL(string: PTHelper.multiPay) else { return }
                let params = self.JSONStringFromAnyObject(value: self.getTheParams() as AnyObject)
                println_debug(params)
                TopupWeb.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", handle: { [weak self](resp, success) in
                    if success {
                        DispatchQueue.main.async {
                            self?.parseMultiPaymentResult(data: resp)
                        }
                    }
                })
            }
        }
    }
    
    //MARK:- Multi payment Action
    fileprivate  func getTheParams() -> [[String: Any]] {
        
        var finalArray = [[String: Any]]()
        
        var arrayParam = [String: Any]()
        for (index,model) in self.displayResponse.enumerated() {
            // Transaction cell tower
            var TransactionsCellTower = [String: Any]()
            TransactionsCellTower["Lac"] = ""
            TransactionsCellTower["Mac"] = ""
            TransactionsCellTower["Mcc"] = mccStatus.mcc
            TransactionsCellTower["Mnc"] = mccStatus.mnc
            TransactionsCellTower["SignalStrength"] = ""
            TransactionsCellTower["Ssid"] = ""
            
            // l External Reference
            var lExternalReference = [String: Any]()
            lExternalReference["Direction"] = true
            lExternalReference["EndStation"] = ""
            lExternalReference["StartStation"] = ""
            lExternalReference["VendorID"] = ""
            
            // L geo Location
            var lGeoLocation = [String: Any]()
            lGeoLocation["CellID"] = ""
            
            let lat  = (manager.currentLatitude == nil) ? "0.0" : manager.currentLatitude
            let long = (manager.currentLongitude == nil) ? "0.0" : manager.currentLongitude
                
            lGeoLocation["Latitude"] = lat ?? "0.0"
            lGeoLocation["Longitude"] = long ?? "0.0"
            
            // L Proximity
            var lProximity = [String: Any]()
            lProximity["BlueToothUsers"] = [Any]()
            lProximity["SelectionMode"]  = false
            lProximity["WifiUsers"]      = [Any]()
            lProximity["mBltDevice"]     = ""
            lProximity["mWifiDevice"]    = ""

            // Final payment Dictionary
            let appendedModel = self.viewModel[index]
            
            // comments for the particular transaction
            let aName = model.safeValueForKey("agentname") ?? ""
            let techComments = String.init(format: "[%@,%@,%@,%@,%@,%@,##%@##%@##]-OKNAME-%@", lat ?? "0.0", long ?? "0.0", "",UserModel.shared.gender,UserModel.shared.ageNow,"",aName as! CVarArg,appendedModel.merchantSubscriberTextfield.text ?? "" as CVarArg,aName as! CVarArg)
            let final_comment = String.init(format: "%@%@", appendedModel.remarks.text ?? "", techComments)
            
            println_debug(model)
            println_debug(appendedModel)
            
            var otherNumber = ""
            
            let cell = transactionModelArray[index].cell
            
            let cellButton = cell.first(where: {$0.mobileNumberTextfield != nil})
            
            if let btn = cellButton {
                otherNumber = btn.getOtherNumber()
            }
            
            // lTransactions//
            var lTransactionsDict = [String: Any]()
            lTransactionsDict["Amount"]                 = model.safeValueForKey("amount") as? String ?? ""
            lTransactionsDict["Balance"]                = ""
            lTransactionsDict["BonusPoint"]             = 0
            lTransactionsDict["CashBackFlag"]           = 0 // need to check the flag value
            lTransactionsDict["Comments"]               = final_comment
            lTransactionsDict["Destination"]            = model.safeValueForKey("destination") as? String ?? ""
            lTransactionsDict["DiscountPayTo"]          = "0" // discount need to check from the server
            lTransactionsDict["IsMectelTopUp"]          = false
            lTransactionsDict["KickBack"]               = model.safeValueForKey("kickback") as? String ?? ""
            lTransactionsDict["KickBackMsisdn"]         = otherNumber
            lTransactionsDict["LocalTransactionType"]   = self.getTransactionString(appendedModel.transactionType) // need to describe the id
            lTransactionsDict["MerchantName"]           = appendedModel.merchantSubscriberTextfield.text ?? "" // check for the merchant name
            lTransactionsDict["MobileNumber"]           = UserModel.shared.mobileNo
            lTransactionsDict["Mode"]                   = true
            lTransactionsDict["Password"]               = ok_password ?? ""
            lTransactionsDict["PromoCodeId"]            = ""
            lTransactionsDict["ResultCode"]             = 0
            lTransactionsDict["ResultDescription"]      = ""
            lTransactionsDict["SecureToken"]            = UserLogin.shared.token
            lTransactionsDict["TransactionID"]          = model.safeValueForKey("transid") as? String ?? ""
            lTransactionsDict["TransactionTime"]        = Date.init(timeIntervalSinceNow: 0).stringValue()
            lTransactionsDict["TransactionType"]        = self.getTransactionString(appendedModel.transactionType)
            lTransactionsDict["accounType"]             = UserModel.shared.agentServiceTypeString()
            
            arrayParam["TransactionsCellTower"] = TransactionsCellTower
            arrayParam["lExternalReference"]    = lExternalReference
            arrayParam["lGeoLocation"]          = lGeoLocation
            arrayParam["lProximity"]            = lProximity
            arrayParam["lTransactions"]         = lTransactionsDict
            
           finalArray.append(arrayParam)
        }

        return finalArray
    }

//     parsing multipayment module
    func parseFinalPaymentResponse(resp: AnyObject) {
        println_debug(resp)
    }

}

//MARK:- TableView Delegates & Datasource
extension PayToTransactionConfirmationVC : UITableViewDelegate, UITableViewDataSource, PayToTransactionCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactionModelArray[section].cell.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return transactionModelArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return transactionModelArray[indexPath.section].cell[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch transactionModelArray.count {
        case 1:
            return ""
        default:
            let stringHeader = String.init(format: "Payment Details %i", section + 1)
            return stringHeader
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            if let label = headerView.textLabel {
                label.textAlignment = .center
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let height : CGFloat = (self.transactionModelArray.count > 1) ? 40.00 : 0.00
        return height
    }
    
    // for timer addition in cell
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard transactionModelArray.count < 1 else { return nil }
        return timerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        guard transactionModelArray.count < 1 else { return 0.00 }
        return 80.00
    }
    
    //MARK:- PayToTransactionCellDelegate
    func transactionCellEvent(cell: PayToTransactionCell) {
        self.tableV.reloadData()
    }
    
    func didTapButton(height: CGFloat, inCell cell: PayToTransactionCell) {
        let frame = cell.frame
        cell.frame = .init(x: frame.origin.x, y: frame.origin.y, width: frame.width, height: height)
        UIView.setAnimationsEnabled(false)
        self.tableV.beginUpdates()
        self.tableV.layoutIfNeeded()
        self.tableV.layoutSubviews()
        self.tableV.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    
    func openCountryORContacts(cell: PayToTransactionCell, type: ButtonType) {
        switch type {
        case .countryView:
            self.navigationController?.present(countryViewController(delegate: cell), animated: true, completion: nil)
        case .contactView:
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "PayToMultiSelection") as? PayToMultiSelection {
                vc.delegate = cell
                if let keyWindow = UIApplication.shared.keyWindow {
                    vc.view.frame = keyWindow.bounds
                    vc.view.tag = 4354
                    UIView.transition(with: keyWindow, duration: 1.5, options: UIViewAnimationOptions.curveEaseIn,
                                      animations:
                        {keyWindow.addSubview(vc.view)}
                        , completion: nil)
                    keyWindow.makeKeyAndVisible()
                }
            }
        }
    }
    
    func didSelectOptions(cell: PayToTransactionCell, option: PTMultiSelectionOption) {
        switch option {
        case .favorite:
            let nav = self.openFavoriteFromNavigation(cell, withFavorite: .payto)
            self.navigationController?.present(nav, animated: true, completion: nil)
        case .contact:
            let nav = UitilityClass.openContact(multiSelection: false, cell)
            self.navigationController?.present(nav, animated: true, completion: nil)
        case .scanQR:
            break
        }
    }
    

    
    func didSelectRateService(cell: PayToTransactionCell) {
        let dict = displayResponse[safe: cell.index ?? 0]
        let story = UIStoryboard.init(name: "Topup", bundle: nil)
        guard let ratingScreen = story.instantiateViewController(withIdentifier: String(describing: PaymentRateScreenViewController.self)) as? PaymentRateScreenViewController else { return }
        ratingScreen.modalPresentationStyle = .overCurrentContext
        ratingScreen.delegate = cell
        ratingScreen.destinationNumber = (dict?.safeValueForKey("destination") as? String != nil) ? dict?.safeValueForKey("destination") as? String : ""
        self.navigationController?.present(ratingScreen, animated: true, completion: nil)
    }

    func formattingTransactionResponseIntoModel() {
        for (index, dictionary) in self.displayResponse.enumerated() {
            
                var cellArray = [PayToTransactionCell]()
                
                let numberCell = self.tableV.dequeueReusableCell(withIdentifier: TransTypeStringIdentifiers.labelDesign) as? PayToTransactionCell
                numberCell?.wrapCellContent(dict: dictionary, type: .number)
            
                let nameCell = self.tableV.dequeueReusableCell(withIdentifier: TransTypeStringIdentifiers.labelDesign) as? PayToTransactionCell
                nameCell?.wrapCellContent(dict: dictionary, type: .name)
            
                let amountCell = self.tableV.dequeueReusableCell(withIdentifier: TransTypeStringIdentifiers.labelDesign) as? PayToTransactionCell
                amountCell?.wrapCellContent(dict: dictionary, type: .amount)
            
            let remarks = self.tableV.dequeueReusableCell(withIdentifier: TransTypeStringIdentifiers.labelDesign) as? PayToTransactionCell
            remarks?.wrapCellContent(dict: dictionary, type: .remarks)
            
            let rate = self.tableV.dequeueReusableCell(withIdentifier: TransTypeStringIdentifiers.rateMyService) as? PayToTransactionCell
            rate?.delegate = self
            rate?.index = index
            rate?.wrapCellContent(dict: dictionary, type: .rating)
            guard let cellRating  = rate else { return }

            if let bName = dictionary.safeValueForKey("businessName") as? String  , bName != "" {
                let businessName = self.tableV.dequeueReusableCell(withIdentifier: TransTypeStringIdentifiers.labelDesign) as? PayToTransactionCell
                businessName?.wrapCellContent(dict: dictionary, type: .businessName)
                
                guard let cellTypeNumber = numberCell else { return }
                guard let cellTypeName   = nameCell else { return }
                guard let cellTypeBusinessName = businessName else { return }
                guard let cellTypeAmount = amountCell else { return }

                if dictionary.safeValueForKey("localRemark") as? String != "" {
                    guard let cellTypeRemarks = remarks else { return }
                    
                    if let amnt = dictionary.safeValueForKey("amount") as? String, amnt.count > 0 {
                        let doubleAmount = (amnt as NSString).floatValue
                        if doubleAmount > 50.0 {
                            cellArray = [cellTypeNumber, cellTypeName, cellTypeBusinessName,cellTypeAmount,cellTypeRemarks,cellRating]
                        } else {
                            cellArray = [cellTypeNumber, cellTypeName, cellTypeBusinessName,cellTypeAmount,cellTypeRemarks]
                        }
                    } else {
                        cellArray = [cellTypeNumber, cellTypeName, cellTypeBusinessName,cellTypeAmount,cellTypeRemarks]
                    }
                    
                } else {
                    if let amnt = dictionary.safeValueForKey("amount") as? String, amnt.count > 0 {
                        let doubleAmount = (amnt as NSString).floatValue
                        if doubleAmount > 50.0 {
                            cellArray = [cellTypeNumber, cellTypeName, cellTypeBusinessName,cellTypeAmount,cellRating]
                        } else {
                            cellArray = [cellTypeNumber, cellTypeName, cellTypeBusinessName,cellTypeAmount]
                        }
                    } else {
                        cellArray = [cellTypeNumber, cellTypeName, cellTypeBusinessName,cellTypeAmount]
                    }
                }
                
            } else {
                guard let cellTypeNumber = numberCell else { return }
                guard let cellTypeName   = nameCell else { return }
                guard let cellTypeAmount = amountCell else { return }
                guard let cellTypeRemarks = remarks else { return }
                
                if dictionary.safeValueForKey("localRemark") as? String != "" {
                    cellArray = [cellTypeNumber, cellTypeName,cellTypeAmount,cellTypeRemarks]
                } else {
                    cellArray = [cellTypeNumber, cellTypeName, cellTypeAmount]
                }
            }

            if let cashback = dictionary.safeValueForKey("kickback") as? String {
                    if cashback == "Kick-Back Rules Not Define" {
                    } else {
                        let cashback = self.tableV.dequeueReusableCell(withIdentifier: TransTypeStringIdentifiers.labelDesign) as? PayToTransactionCell
                        cashback?.wrapCellContent(dict: dictionary, type: .cashback)
                        
                        let button = self.tableV.dequeueReusableCell(withIdentifier: TransTypeStringIdentifiers.buttonDesign) as? PayToTransactionCell
                        button?.wrapCellContent(dict: dictionary, type: .button)
                        button?.delegate = self
                        guard let cashBk = cashback else { return }
                        guard let btn    = button else { return }
                        
                        cellArray.append(cashBk)
                        cellArray.append(btn)
                    }
                }
                
                let model = PaytoTransactionCellModel.init(section: index, cell: cellArray)
                self.transactionModelArray.append(model)
        }
    }
    
    
}

extension PayToTransactionConfirmationVC : PTWebResponseDelegate {
    
    func parseMultiPaymentResult(data: Any) {
        var arrayOfTransactions = Array<Dictionary<String,Any>>()
        if let dict = data as? Array<Dictionary<String,Any>> {
            for dictionaries in dict {
                if let xmlString = dictionaries["Data"] as? String {
                    let parserString = SWXMLHash.parse(xmlString)
                    self.enumerate(indexer: parserString)
                    if transaction["resultdescription"] as? String == "Transaction Successful" {
                        arrayOfTransactions.append(self.transaction)
                    } else {
                        PaymentLogTransactions.manager.logPaymentApiFailureCase(transaction.safeValueForKey("destination") as? String ?? "", errCode: transaction.safeValueForKey("resultcode") as? String ?? "")
                    }
                }
            }
            
            if arrayOfTransactions.count > 0 {
                DispatchQueue.main.async {
                    guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "PTRecieptViewController") as? PTRecieptViewController else { return }
                    vc.transactionDetails = arrayOfTransactions
                    self.navigationController?.setNavigationBarHidden(false, animated: true)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    func webSuccessResult(data: Any, screen: String) {
        if screen == ScreenName.singlePay {
            let xmlString = SWXMLHash.parse(data as! String)
            self.enumerate(indexer: xmlString)
            if transaction["resultdescription"] as? String == "Transaction Successful" {
                
                DispatchQueue.global(qos: .background).async {
                    if self.requestMoney {
                        self.updateRequestMoneyStatus()
                    }
                }
                DispatchQueue.main.async {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PTRecieptViewController") as? PTRecieptViewController
                    vc?.transactionDetails = [self.transaction]
                    vc?.typeResponse = self.varType
                    self.navigationController?.setNavigationBarHidden(false, animated: true)
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
            }
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            transaction[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func webFailureResult(screen: String) {
        println_debug("Failed for single pay ")
    }
    
    
    func updateRequestMoneyStatus()
    {
        var requestId : String?
        if let dic = self.displayResponse.first {
            requestId = dic.safeValueForKey("requestId") as? String ?? ""
        }
        let urlStr   = Url.changeStatusOfRequest
        let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
        var urlComponents = URLComponents.init(url: ur, resolvingAgainstBaseURL: false)
        let userData = UserModel.shared
        // add params
        
        let mobileNUmberQuery       = URLQueryItem(name:"MobileNumber",value : userData.mobileNo)
        let simIdQuery              = URLQueryItem(name:"Simid",value :userData.simID)
        let msIDQuery               = URLQueryItem(name:"MSID" , value:userData.msid)
        let osTypeQuery             = URLQueryItem(name:"OSType", value: userData.osType)
        let otpQuery                = URLQueryItem(name:"OTP", value: userData.simID)
        let requestIDQuery          = URLQueryItem(name:"RequestId", value: "\(requestId ?? "")")
        let statusQuery             = URLQueryItem(name:"Status", value: "Accepted")
        
        urlComponents?.queryItems  = [mobileNUmberQuery,simIdQuery,msIDQuery,osTypeQuery,otpQuery,requestIDQuery,statusQuery]
        guard let apiForCancelRequest = urlComponents?.url else { return }
        
        
        println_debug("api \(apiForCancelRequest.absoluteString) ")
        
        
        JSONParser.GetCancelApiResponse(apiUrl: apiForCancelRequest, type: "GET") { (isSuccess : Bool, data : Any?) in
            
            if isSuccess == true {
                
            } else {
                ///
                if let message = data as? String {
                    
                    DispatchQueue.main.async(execute: {
                        
                        alertViewObj.wrapAlert(title: "", body: message.localized, img: UIImage(named : "AppIcon"))
                        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                            
                        })
                        alertViewObj.showAlert(controller: self)
                    })
                }
            }
        }
        
    }
    
}

