//
//  PTQRGenerator.swift
//  OK
//
//  Created by Ashish on 12/21/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox
import CoreGraphics

class PTQRGenerator {
    
    var qrImage : CIImage?
    
    func getQRImage(stringQR: String, withSize rate: CGFloat) -> UIImage? {
        if let filter = CIFilter.init(name: "CIQRCodeGenerator") {
            filter.setDefaults()
            if let data : Data = stringQR.data(using: String.Encoding.utf8) {
                filter.setValue(data, forKey: "inputMessage")
                if let resultImage : CIImage = filter.outputImage {
                    let context = CIContext.init(options: nil)
                    let cgImage = context.createCGImage(resultImage, from: resultImage.extent)
                    var image   = UIImage.init(cgImage: cgImage!, scale: 1.0, orientation: UIImageOrientation.up)
                    let width  =  image.size.width * rate
                    let height =  image.size.height * rate
                    UIGraphicsBeginImageContext(.init(width: width, height: height))
                    let cgContext = UIGraphicsGetCurrentContext()
                    cgContext?.interpolationQuality = .none
                    image.draw(in: .init(origin: .init(x: 0, y: 0), size: .init(width: width, height: height)))
                    image = UIGraphicsGetImageFromCurrentImageContext()!
                    UIGraphicsEndImageContext()
                    return image
                }
            }
        }
        return nil
    }
}
