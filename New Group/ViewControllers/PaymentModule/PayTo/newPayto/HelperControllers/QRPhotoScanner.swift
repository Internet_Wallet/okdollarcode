//
//  QRPhotoScannerViewController.swift
//  OK
//
//  Created by Ashish Kumar Singh on 11/7/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Photos

@objc protocol ImagePickerDelegate {
    @objc optional func imagePicker(pickedImage image: UIImage?, filteredImage: UIImage?)
}

 class QRPhotoScanner : PayToBaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var galleryImages            = [UIImage]()
    fileprivate var assets       : PHFetchResult<AnyObject>?
    @IBOutlet var collectionView : UICollectionView!
    fileprivate var sideSize     : CGFloat!
    
    var qrImageArray = [UIImage]()
    
    var delegate:ImagePickerDelegate?
        
    var scanQRController : ScanToPayViewController?
    var tbScanQRController : TBScanToPayViewController?
    
     override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Gallery"
        
        let collectionViewLayout = UICollectionViewFlowLayout.init()
        collectionViewLayout.minimumLineSpacing      = 0
        collectionViewLayout.minimumInteritemSpacing = 0
        self.setBackButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        PTLoader.shared.show()
        
        if PHPhotoLibrary.authorizationStatus() == .authorized {
            reloadAssets()
        } else {
            PHPhotoLibrary.requestAuthorization({ (status: PHAuthorizationStatus) -> Void in
                if status == .authorized {
                    self.reloadAssets()
                } else {
                    self.requestAlertForPermissions(withBody: "OK$ needs permission to Scan QR Code from your Gallery", handler: { (cancelled) in
                        if let navigation = self.navigationController {
                            if let backController = self.tbScanQRController {
                                backController.captureSession.startRunning()
                            }
                            navigation.popViewController(animated: true)
                        }
                    })
                }
            })
        }

        let time : DispatchTime = DispatchTime.now()
        DispatchQueue.main.asyncAfter(deadline: time) {
            PTLoader.shared.hide()
        }
    }
    
    private func setBackButton() {
        let buttonIcon      = UIImage(named: "tabBarBack")
        let leftBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.done, target: self, action: #selector(QRPhotoScanner.popViewController(_:)))
        leftBarButton.image = buttonIcon
        self.navigationItem.leftBarButtonItem = leftBarButton
    }

    @objc private func popViewController(_ sender:UIBarButtonItem!) {
       self.navigationController?.popViewController(animated: true)
    }

    
    func checkQRCode(img: UIImage, cell: GalleryCell, view: UIView)  {
        
        let time : DispatchTime = DispatchTime.now() + 4
        DispatchQueue.main.asyncAfter(deadline: time) {
        
            guard  let detector:CIDetector = CIDetector(ofType: CIDetectorTypeQRCode, context: nil, options: [CIDetectorAccuracy:CIDetectorAccuracyHigh]) else { return }
            
            
            guard let ciImage:CIImage = CIImage(image:img) else {
                return
            }
            
            var qrCodeLink=""
            
            guard  let features = detector.features(in: ciImage) as? [CIQRCodeFeature] else { return }
            for feature in features {
                guard let message = feature.messageString else { return }
                qrCodeLink += message
            }

            
            if qrCodeLink=="" {
                println_debug("No QR Code Found")
                DispatchQueue.main.async(){
                    alertViewObj.wrapAlert(title: nil, body: "No QR Code Found".localized, img: nil)
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        
                    })
                    alertViewObj.showAlert(controller: self)
                    
                }
                
            }else{
                self.navigationController?.popViewController(animated: false)
                if let vc = self.scanQRController {
                    vc.scannedString(str: qrCodeLink)
                    println_debug("message: \(qrCodeLink)")
                } else if let vc = self.tbScanQRController {
                    vc.scannedString(str: qrCodeLink)
                }
            }
            view.removeFromSuperview()
            cell.viewAnimate.layer.removeAllAnimations()
        }
    }
    
    //Loading photos functions
    fileprivate func showNeedAccessMessage() {
        let alert = UIAlertController(title: "Image picker", message: "App need get access to photos", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction) -> Void in
            self.dismiss(animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: { (action: UIAlertAction) -> Void in
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: { (bool) in
                println_debug(bool)
            })
        }))
        
        show(alert, sender: nil)
    }
    
    fileprivate func reloadAssets() {
        DispatchQueue.main.async {
            self.assets = nil
            self.collectionView.reloadData()
            let fetchOptions = PHFetchOptions()
            fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
            self.assets = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions) as? PHFetchResult<AnyObject>
        }
    }

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? GalleryCell
        self.animateCellItem(cell: cell!, indexPathRow: indexPath.row)
    }

    func animateCellItem(cell: GalleryCell, indexPathRow: Int) {
        let view = UIView.init(frame: self.collectionView.frame)
        view.backgroundColor = UIColor.clear
        let hover = CABasicAnimation(keyPath: "position")
        hover.isAdditive = true
        hover.fromValue = NSValue(cgPoint: CGPoint.zero)
        hover.toValue = NSValue(cgPoint: CGPoint(x: 0.0, y: screenWidth/3.2 - 2))
        hover.autoreverses = true
        hover.duration = 2
        hover.repeatCount = Float.infinity
        cell.viewAnimate.layer.add(hover, forKey: "myHoverAnimation")
        self.view.addSubview(view)
        
        PHImageManager.default().requestImage(for: assets?[indexPathRow] as! PHAsset, targetSize: CGSize.init(width: 320, height: 560), contentMode: PHImageContentMode.default, options: nil) { (image, resp) in
            if let img = image {
                self.checkQRCode(img: img, cell: cell, view: view)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (assets != nil) ? assets!.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryIdentifier", for: indexPath) as? GalleryCell
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = screenWidth/3.2
        return CGSize.init(width: size, height: size)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        PHImageManager.default().requestImage(for: assets?[indexPath.row] as! PHAsset, targetSize: CGSize(width: screenWidth/3, height: screenWidth/3), contentMode: .aspectFit, options: nil) { (image: UIImage?, info: [AnyHashable: Any]?) -> Void in
            (cell as! GalleryCell).imageV.image = image
        }
    }
    
}

class GalleryCell: UICollectionViewCell {
    @IBOutlet var yConstraint: NSLayoutConstraint!
    @IBOutlet var viewAnimate: CardDesignView!
    
    @IBOutlet var imageV: UIImageView!
    
    override func awakeFromNib() {
        
    }
    
    func wrapCell(image: String) {
        self.yConstraint.constant = 0
        self.imageV.image = UIImage.init(named: image)
    }
    
    
}
