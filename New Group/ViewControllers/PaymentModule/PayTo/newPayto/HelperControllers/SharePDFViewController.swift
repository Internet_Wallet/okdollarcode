//
//  SharePDFViewController.swift
//  OK
//
//  Created by Ashish on 5/14/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class SharePDFViewController: OKBaseController, UIWebViewDelegate{
    
    var url : URL?
    @IBOutlet weak var pdfDisplayWV: UIWebView!
    
    @IBOutlet weak var shareBtn : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.backButton(withTitle: "Transaction Receipt".localized)

        if let pdfUrl = url {
            let request = URLRequest.init(url: pdfUrl)
            self.pdfDisplayWV.loadRequest(request)
        }
        
        self.shareBtn.setTitle("Share".localized, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func backButton(withTitle title:String){
        
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "tabBarBack"), for: UIControlState.normal)
        
        button.tintColor = UIColor.white
        
        button.sizeToFit()
        button.addTarget(self, action: #selector(backButtonCustomAction), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        
        self.navigationItem.title = title
    }

    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
    }

    @IBAction func sharePdf(_ sender: UIButton) {
        guard let pdfUrl = url else { return }
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
    }
    

}
