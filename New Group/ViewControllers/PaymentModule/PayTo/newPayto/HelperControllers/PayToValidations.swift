//
//  PayToValidations.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/29/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PayToValidations: NSObject {
    
    let prefixRejected = ["0992",
                          "0993",
                          "0994",
                          "0982",
                          "0988",
                          "0980",
                          "0960",
                          "0961",
                          "0962",
                          "0971",
                          "0972",
                          "0974",
                          "0975"]
    
    let eightDigit = ["0941",
                      "0943",
                      "0947",
                      "0949",
                      "0973",
                      "0991"]
    
    let sevenDigit = ["0920",
                      "0921",
                      "0923",
                      "0924",
                      "095",
                      "096",
                      "098",
                      "0963",
                      "0968",
                      "0981",
                      "0983",
                      "0984",
                      "0985",
                      "0986",
                      "0987",
                      ]
    
    let tenDigit = ["093"]
    
    let specialValidation = ["0986"]
    
    func checkRejectedNumber(prefix: String) -> Bool {
        for numbers in self.prefixRejected {
            if numbers == prefix {
                return true
            }
        }
        
        return false
    }
    
    func checkStringHasRejectedPrefix(string: String) -> Bool {
        for numbers in self.prefixRejected {
            if string.hasPrefix(numbers) {
                return true
            }
        }
        return false
    }
    
    func checkMatchingNumber(string: String, withString str: String) -> Bool {
        if str.hasPrefix(string) {
            return true
        } else {
            return false
        }
    }
    
    func getSpecialPrefixValidation(prefix: String) -> Bool {
        for keys in self.specialValidation {
            if prefix.hasPrefix(keys) {
                return true
            }
        }
        return false
    }
    
    func checkEightDigit(prefix: String) -> Bool {
        for keys in self.eightDigit {
            if prefix.hasPrefix(keys) {
                return true
            }
        }
        
        return false
    }
    
    func checkSevenDigit(prefix: String) -> Bool {
        for keys in self.sevenDigit {
            if prefix.hasPrefix(keys) {
                return true
            }
        }
        return false
    }
    
    func getNumberRangeValidation(prefix: String) -> Int {
        
        if prefix == "0989" {
            return 11
        }
        
        for keys in self.sevenDigit {
            if prefix.hasPrefix(keys) {
                return 9
            }
        }
        
        for keys in self.eightDigit {
            if prefix.hasPrefix(keys) {
                return 10
            }
        }
        
        for keys in self.tenDigit {
            if prefix.hasPrefix(keys) {
                return 12
            }
        }
        
        return 11
    }
    

    
    func getColorForOperator(operatorName: String) -> UIColor {
        switch operatorName {
        case "Mpt":
           return MyNumberTopup.OperatorColorCode.mpt
        case "Telenor":
            return MyNumberTopup.OperatorColorCode.telenor
        case "Ooredoo":
            return MyNumberTopup.OperatorColorCode.oredo
        case "MecTel":
            return MyNumberTopup.OperatorColorCode.mactel
        case "MPT CDMA 800":
            return MyNumberTopup.OperatorColorCode.mpt
        case "MPT CDMA 450":
            return MyNumberTopup.OperatorColorCode.mpt
        case "Mytel":
            return MyNumberTopup.OperatorColorCode.mytel
        default:
           return MyNumberTopup.OperatorColorCode.okDefault
        }
    }

    func getNumberRangeValidation(_ prefix: String) -> (min: Int, max: Int,operator: String, isRejected: Bool) {
        
        var validObject =  (11,11,"Mpt",false)

        if prefix.hasPrefix("0960") || prefix.hasPrefix("0961") || prefix.hasPrefix("0962") || prefix.hasPrefix("0963") || prefix.hasPrefix("0971") || prefix.hasPrefix("0972") || prefix.hasPrefix("0974") || prefix.hasPrefix("0975") || prefix.hasPrefix("0992") || prefix.hasPrefix("0993") || prefix.hasPrefix("0994") || prefix.hasPrefix("091") || prefix.hasPrefix("090") {
            validObject = (0,0,"",true)
            return validObject
        }
        
        if prefix.hasPrefix("0979") || prefix.hasPrefix("0978") || prefix.hasPrefix("0977") || prefix.hasPrefix("0976") {
            validObject = (11,11,"Telenor",false)
            return validObject
        }

        if prefix.hasPrefix("0997") || prefix.hasPrefix("0996") || prefix.hasPrefix("0995") {
            validObject = (11,11,"Ooredoo",false)
            return validObject
        }
        
        if prefix.hasPrefix("093") {
            validObject = (9,11,"MecTel",false)
            return validObject
        }
        
        if prefix.hasPrefix("0973") || prefix.hasPrefix("0991") {
            validObject = (10,10,"MPT CDMA (800)",false)
            return validObject
        }
        
        if prefix.hasPrefix("0947") || prefix.hasPrefix("0949") {
            validObject = (10,10,"MPT CDMA (450)",false)
            return validObject
        }
        
        if prefix.hasPrefix("0969") || prefix.hasPrefix("0965") {
            validObject = (11,11,"Mytel",false)
            return validObject
        }
        
        if prefix.hasPrefix("0920") || prefix.hasPrefix("0921") || prefix.hasPrefix("0922") || prefix.hasPrefix("0923") || prefix.hasPrefix("0924") || prefix.hasPrefix("095") || prefix.hasPrefix("096"){
            validObject = (9,9,"MPT",false)
            return validObject
        }
        
        if prefix.hasPrefix("0941") || prefix.hasPrefix("0943") { // prefix.hasPrefix("0949") {
            validObject = (10,10,"MPT",false)
            return validObject
        }
        
        if prefix.hasPrefix("098") {
            validObject = (9,11,"MPT",false)
            return validObject
        }
        
        return validObject
    }
}
