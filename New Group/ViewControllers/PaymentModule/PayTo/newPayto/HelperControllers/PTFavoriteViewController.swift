//
//  PTFavoriteViewController.swift
//  OK
//
//  Created by Ashish on 2/28/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PTFavoriteViewController: OKBaseController {
    
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var nameTextfield: UITextField!
    
    @IBOutlet weak var centralX: NSLayoutConstraint!
    
    var addFavDetails : (name: String, number: String, transType: String, amount: String)?
    
    fileprivate let addFav = "https://www.okdollar.co/RestService.svc//AddFavourite?MobileNumber=%@&Simid=\(uuid)&MSID=\(uuid)&OSType=1&OTP=\(uuid)&FavNum=%@&Type=%@&Name=%@&Amount=%@"
    
    @IBOutlet weak var enterName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nameTextfield.text = ""
        self.enterName.text = "Please enter name".localized
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if  let nameArray = addFavDetails?.name.components(separatedBy: ",") {
            let secondName = (nameArray.count > 1) ? nameArray.last : ""
            self.nameTextfield.text = secondName ?? ""
        }
        
    }
    
    @IBAction func action_save(_ sender: UIButton) {
        
        var name = self.nameTextfield.text
        if name == "" {
            name = "Unknown"
        }
        
        let finalUrlString = String.init(format: addFav, UserModel.shared.mobileNo, addFavDetails?.number ?? "", addFavDetails?.transType ?? "", name ?? "Unknown", addFavDetails?.amount ?? "")
        if let formattedString = finalUrlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            
            let url = URL.init(string: formattedString)
            
            let param = Dictionary<String,Any>() as AnyObject
            
            TopupWeb.genericClass(url: url!, param: param, httpMethod: "GET", handle: { [weak self](response, success) in
                DispatchQueue.main.async {
                    if success {
                        println_debug(response)
                        DispatchQueue.main.async {
                        favoriteManager.sync()
                            alertViewObj.wrapAlert(title: "", body: "Contact Added as Favorite successfully".localized, img: #imageLiteral(resourceName: "ok"))
                            alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                                
                            })
                            alertViewObj.showAlert(controller: self ?? UIViewController())
                        }
                    }
                    self?.dismiss(animated: true, completion: nil)
                }
            })
        }
    }
    
    @IBAction func action_skip(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
