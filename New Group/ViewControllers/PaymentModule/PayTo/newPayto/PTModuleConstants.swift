//
//  PTModuleConstants.swift
//  OK
//
//  Created by Ashish on 7/29/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol PTBaseLayoutDelegates : class {
    func generateNewCell(with type: PTLayoutCellEnum)
    func openContact(_ cell: UITableViewCell)
    func openCountry(_ cell: UITableViewCell)
}

protocol PTMobileNumberDelegate : class {
    func deleteAllCell()
    func becomeResponder(with type: PTLayoutCellEnum)
}

enum PTLayoutCellEnum {
    case offer, mobile, cnfMobile, name, subscriber, merchant, addVehicle, type,amount, burmeseAmount, remarks, hideMyNumber
}

struct PTConstants {
    
    struct CommonStrings {
       static let myanmarCountryCode = "+95"
       static let  mobileNumberAcceptableCharacters = "0123456789"
    }
    
}















