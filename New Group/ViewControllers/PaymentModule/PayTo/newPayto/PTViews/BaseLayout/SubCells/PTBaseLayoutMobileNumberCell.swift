//
//  PTBaseLayoutMobileNumberCell.swift
//  OK
//
//  Created by Ashish on 7/29/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PTBaseLayoutMobileNumberCell: UITableViewCell, UITextFieldDelegate {
   
    //MARK:- Properties
    @IBOutlet weak var countryFlag: UIImageView!
    @IBOutlet weak var countryCode: UILabel!
    @IBOutlet weak var mobileNumber: UITextField!
    @IBOutlet weak var contactBtn: UIButton!
    @IBOutlet weak var clearBtn: UIButton!
    @IBOutlet weak var countryBtn: UIButton!
    
    @IBOutlet weak var widthClearBtn: NSLayoutConstraint!
    
    var showClearButton : Bool = true {
        didSet {
            if showClearButton {
                self.widthClearBtn.constant = 30.00
            } else {
                self.widthClearBtn.constant = 0.00
            }
        }
    }
    
    var country : Country = Country.init(name: "Myanmar", code: "myanmar", dialCode: "+95") {
        didSet {
            self.countryFlag.image = UIImage(named: country.code)
            self.countryCode.text  = "(\(country.dialCode))"
        }
    }
    
    weak var mobileNumberDelegate : PTBaseLayoutDelegates?
    weak var delegate : PTMobileNumberDelegate?
    
    //MARK:- Functions
    
    //MARK:- IBActions
    
    @IBAction func openCountry(_ sender: UIButton) {
        self.mobileNumberDelegate?.openCountry(self)
    }
    
    @IBAction func openContactAction(_ sender: UIButton) {
        self.mobileNumberDelegate?.openContact(self)
    }
    
    @IBAction func clearAction(_ sender: UIButton) {
        if country.dialCode == PTConstants.CommonStrings.myanmarCountryCode {
            self.mobileNumber.text = "09"
        } else {
            self.mobileNumber.text = ""
        }
    }
    
    //MARK:- Delegates
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let objectValidation = PayToValidations().getNumberRangeValidation(text)
        
            let textCount  = text.count
            let validCount = objectValidation.max
        
            let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: PTConstants.CommonStrings.mobileNumberAcceptableCharacters).inverted
            let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
            
            if string != filteredSet {
                return false
            }
            
           guard  let char = string.cString(using: String.Encoding.utf8) else { return false }
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                self.delegate?.deleteAllCell()
            }
        
            // Normal Validations
            if objectValidation.isRejected {
                return false
            }
        
            if country.dialCode == PTConstants.CommonStrings.myanmarCountryCode {
                if validCount == textCount {
                    self.mobileNumber.text = text
                    self.mobileNumberDelegate?.generateNewCell(with: .cnfMobile)
                    return false
                }
                if textCount > validCount {
                    self.delegate?.becomeResponder(with: .cnfMobile)
                    return false
                }
            }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch country.dialCode == PTConstants.CommonStrings.myanmarCountryCode {
        case true:
            if let text = textField.text, text.count <= 2 {
                textField.text = "09"
            }
        case false:
            break
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
}











