//
//  PTBaseLayoutViewController.swift
//  OK
//
//  Created by Ashish on 7/29/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PTBaseLayoutViewController: OKBaseController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableViewUI: UITableView!
    
    var cellArray : [UITableViewCell] = [UITableViewCell]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.generateCell(.mobile)
    }
    
    //MARK:- Functions
    func generateCell(_ cellType: PTLayoutCellEnum) {
        
        var identifier : String?
        
        switch cellType {
        case .offer:
            identifier = String(describing: PTBaseLayoutOfferCell.self)
        case .mobile:
            identifier = String(describing: PTBaseLayoutMobileNumberCell.self)
        case .cnfMobile:
            identifier = String(describing: PTBaseLayoutConfirmMobileCell.self)
        case .name:
            identifier = String(describing: PTBaseLayoutNameCell.self)
        case .subscriber:
            identifier = String(describing: PTBaseLayoutSubscriberCell.self)
        case .merchant:
            identifier = String(describing: PTBaseLayoutMerchantCell.self)
        case .addVehicle:
            identifier = String(describing: PTBaseLayoutAddVehicleCell.self)
        case .type:
            identifier = String(describing: PTBaseLayoutUserTypeCell.self)
        case .amount:
            identifier = String(describing: PTBaseLayoutAmountCell.self)
        case .burmeseAmount:
            identifier = String(describing: PTBaseLayoutBurmeseAmountCell.self)
        case .remarks:
            identifier = String(describing: PTBaseLayoutBurmeseAmountCell.self)
        case .hideMyNumber:
            identifier = String(describing: PTBaseLayoutHideMyNumberCell.self)
        }
        
        guard let idString = identifier else { return }
        guard let cell = self.tableViewUI.dequeueReusableCell(withIdentifier: idString) else { return }
        self.cellArray.append(cell)
    }

    //MARK:- TableView Delegate & Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cellArray[indexPath.row]
    }
    
}

