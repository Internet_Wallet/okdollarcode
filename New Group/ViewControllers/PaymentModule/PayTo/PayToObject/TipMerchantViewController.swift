//
//  TipMerchantViewController.swift
//  OK
//
//  Created by Ashish on 6/30/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol TipMerchantViewControllerDelegate : class{
    func didSelectOK(amount: String)
    func didselectNoThnx()
}

class TipMerchantViewController: OKBaseController, UITextFieldDelegate {

    @IBOutlet weak var lblTitle: UILabel!
    {
        didSet
        {
            lblTitle.font = UIFont(name: appFont, size: appFontSize)
            lblTitle.text = lblTitle.text?.localized
        }
    }
    @IBOutlet weak var lblBillAmount: UILabel!
    {
        didSet
        {
            lblBillAmount.font = UIFont(name: appFont, size: appFontSize)
            lblBillAmount.text = lblBillAmount.text?.localized
        }
    }
    @IBOutlet weak var lblTipAmount: UILabel!
    {
        didSet
        {
            lblTipAmount.font = UIFont(name: appFont, size: appFontSize)
            lblTipAmount.text = lblTipAmount.text?.localized
        }
    }
    @IBOutlet weak var amountText: UILabel!{
        didSet{
            amountText.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var textfieldTip: UITextField!
    {
        didSet
        {
            textfieldTip.font = UIFont(name: appFont, size: appFontSize)
            textfieldTip.placeholder = textfieldTip.placeholder?.localized
        }
    }
    
    @IBOutlet weak var nothnxBtn: UIButton!
        {
        didSet
        {
            nothnxBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            nothnxBtn.setTitle("NO THANKS".localized, for: .normal)
        }
    }
    @IBOutlet weak var okbtn: UIButton!
        {
        didSet
        {
            okbtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            okbtn.setTitle("OK".localized, for: .normal)
        }
    }
    @IBOutlet weak var labelAmount: UILabel!
    
    var amount  = "0"
    
    weak var delegate : TipMerchantViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelAmount.text = wrapAmountWithCommaDecimal(key:amount)  + " " + "MMK"
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        let filterAmt = text.replacingOccurrences(of: ",", with: "")
        let textAmount = (filterAmt as NSString).floatValue
        let payAmount = (amount as NSString).floatValue
        
        if textAmount >= payAmount {
            textField.text = ""
            self.showErrorAlert(errMessage: "Tip amount must be lesser than Bill amount".localized)
            return false
        }

        if text.contains(".") {
            return false
        } else {
            let mystring = text.replacingOccurrences(of: ",", with: "")
            let number = NSDecimalNumber(string: mystring)
            
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = 0

            formatter.groupingSeparator = ","
            formatter.groupingSize = 3
            formatter.usesGroupingSeparator = true
            var num = formatter.string(from: number)
            if num == "NaN" {
                num = ""
            }
            self.textfieldTip.text = num
            return false
        }
        
    }
    
    @IBAction func OKAction(_ sender: UIButton) {
        let amount = self.textfieldTip.text?.replacingOccurrences(of: ",", with: "")
        if let txt = self.textfieldTip.text, txt.count > 0 {
            self.delegate?.didSelectOK(amount: amount ?? "0")
        }else{
            self.delegate?.didselectNoThnx()
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func noThnxAction(_ sender: UIButton) {
        self.delegate?.didselectNoThnx()
        self.dismiss(animated: true, completion: nil)
    }
    
}
















