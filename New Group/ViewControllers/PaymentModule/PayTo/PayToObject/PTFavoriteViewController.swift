//
//  PTFavoriteViewController.swift
//  OK
//
//  Created by Ashish on 2/28/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
protocol PTFavoriteActionDelegate: class {
    func favoriteAdded()
}

class PTFavoriteViewController: OKBaseController, UITextFieldDelegate {
    
    //MARK: - Outlet
    @IBOutlet weak var skipButton: UIButton! {
        didSet {
            self.skipButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.skipButton.setTitle("Skip".localized, for: .normal)
        }
    }
    @IBOutlet weak var saveButton: UIButton! {
        didSet {
            self.saveButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.saveButton.setTitle("Save".localized, for: .normal)
        }
    }
    @IBOutlet weak var nameTextfield: UITextField!{
        didSet{
            nameTextfield.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var centralX: NSLayoutConstraint!
    @IBOutlet weak var enterName: UILabel!{
        didSet{
            enterName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    //MARK: - Properties
    weak var delegate: PTFavoriteActionDelegate?
    var addFav = ""
    var addFavDetails : (name: String, number: String, transType: String, amount: String)?
    
    //MARK: - Views Method
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nameTextfield.text = ""
        self.enterName.text = "Please enter name".localized
        
        switch serverUrl {
        case .productionUrl:
            addFav = "https://www.okdollar.co/RestService.svc//AddFavourite?MobileNumber=%@&Simid=\(uuid)&MSID=\(uuid)&OSType=1&OTP=\(uuid)&FavNum=%@&Type=%@&Name=%@&Amount=%@"
        case .testUrl:
            addFav = "http://69.160.4.151:8001/RestService.svc//AddFavourite?MobileNumber=%@&Simid=\(uuid)&MSID=\(uuid)&OSType=1&OTP=\(uuid)&FavNum=%@&Type=%@&Name=%@&Amount=%@"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        var name = ""
        if  let nameArray = addFavDetails?.name.components(separatedBy: ",") {
            if nameArray.count == 1 {
                name = nameArray[0]
            } else if nameArray.count > 1 {
                name = nameArray[nameArray.count-1]
            }
            self.nameTextfield.text = addFavDetails?.name
        }
    }
    
    //MARK: - Button Action Method
    @IBAction func action_save(_ sender: UIButton) {
        var name = self.nameTextfield.text
        if name == "" {
            name = "Unknown"
        }
        
let finalUrlString = String.init(format: addFav, UserModel.shared.mobileNo, addFavDetails?.number ?? "", addFavDetails?.transType ?? "", name ?? "Unknown", addFavDetails?.amount.replacingOccurrences(of: ",", with: "") ?? "")
        if let formattedString = finalUrlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            
            let url = URL.init(string: formattedString)
            let param = Dictionary<String,Any>() as AnyObject
            TopupWeb.genericClass(url: url!, param: param, httpMethod: "GET", handle: { [weak self](response, success) in
                DispatchQueue.main.async {
                    if success {
                        println_debug(response)
                        let jsonData = JSON(response)
                        println_debug(jsonData)
                        if jsonData["Code"].intValue == 200 {
                            self?.delegate?.favoriteAdded()
                            DispatchQueue.main.async {
                                favoriteManager.sync()
                                alertViewObj.wrapAlert(title: "", body: "Added into Favorite list".localized, img: UIImage(named: "star"))
                                alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                                    
                                })
                                alertViewObj.showAlert(controller: self ?? UIViewController())
                            }
                        } else {
                            var alertMsg = jsonData["Msg"].stringValue
                            if alertMsg.hasPrefix("Failed . ") {
                                alertMsg = alertMsg.substring(from: 9)
                            }
                            DispatchQueue.main.async {
                                alertViewObj.wrapAlert(title: "", body: "This number already added as Favourite.".localized, img: UIImage(named: "star"))
                                alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {
                                    
                                })
                                alertViewObj.showAlert(controller: self ?? UIViewController())
                            }
                        }
                    }
                    self?.dismiss(animated: true, completion: nil)
                }
            })
        }
    }
    
    @IBAction func action_skip(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       var CHARSET = ""
        
        if appDel.currentLanguage == "my"{
            CHARSET = STREETNAME_CHAR_SET_En
        }else if appDel.currentLanguage == "uni"{
            CHARSET = STREETNAME_CHAR_SET_Uni
        }else{
             
            CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789/-,(). _"
        }
                
        if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) {
            return false
        }
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
         if text.count > 40 {
            return false
        }
        return restrictMultipleSpaces(str: string, textField: nameTextfield)
        
        
    }
    
}
