//
//  PayToValidations.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/29/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PayToValidations: NSObject, PhValidationProtocol {
    
    let prefixRejected = ["0992",
                          "0993",
                          "0982",
                          "0988",
                          "0980",
                          "0960",
                          "0961",
                          "0962",
                          "0971",
                          "0972",
                          "0974"] // "0975"
    
    let eightDigit = ["0941",
                      "0943",
                      "0947",
                      "0949",
                      "0973",
                      "0991"]
    
    let sevenDigit = ["0920",
                      "0921",
                      "0923",
                      "0924",
                      "095",
                      "096",
                      "098",
                      "0963",
                      "0968",
                      "0981",
                      "0983",
                      "0984",
                      "0985",
                      "0986",
                      "0987",
                      ]
    
     let tenDigit = ["093"]
    
    let specialValidation = ["0986"]
    
    func checkRejectedNumber(prefix: String) -> Bool {
        for numbers in self.prefixRejected {
            if numbers == prefix {
                return true
            }
        }
        
        return false
    }
    
    func checkStringHasRejectedPrefix(string: String) -> Bool {
        for numbers in self.prefixRejected {
            if string.hasPrefix(numbers) {
                return true
            }
        }
        return false
    }
    
    func checkMatchingNumber(string: String, withString str: String) -> Bool {
        if str.hasPrefix(string) {
            return true
        } else {
            return false
        }
    }
    
    func getSpecialPrefixValidation(prefix: String) -> Bool {
        for keys in self.specialValidation {
            if prefix.hasPrefix(keys) {
                return true
            }
        }
        return false
    }
    
    func checkEightDigit(prefix: String) -> Bool {
        for keys in self.eightDigit {
            if prefix.hasPrefix(keys) {
                return true
            }
        }
        
        return false
    }
    
    func checkSevenDigit(prefix: String) -> Bool {
        for keys in self.sevenDigit {
            if prefix.hasPrefix(keys) {
                return true
            }
        }
        return false
    }
    
    func getNumberRangeValidation(prefix: String) -> Int {
        
        if prefix == "0989" {
            return 11
        }
        
        for keys in self.sevenDigit {
            if prefix.hasPrefix(keys) {
                return 9
            }
        }
        
        for keys in self.eightDigit {
            if prefix.hasPrefix(keys) {
                return 10
            }
        }
        
        for keys in self.tenDigit {
            if prefix.hasPrefix(keys) {
                return 12
            }
        }

        return 11
    }
    
    func getColorForOperator(operatorName: String) -> UIColor {
        switch operatorName {
        case "MPT":
           return MyNumberTopup.OperatorColorCode.mpt
        case "Telenor":
            return MyNumberTopup.OperatorColorCode.telenor
        case "Ooredoo":
            return MyNumberTopup.OperatorColorCode.oredo
        case "MecTel":
            return MyNumberTopup.OperatorColorCode.mactel
        case "MPT CDMA 800":
            return MyNumberTopup.OperatorColorCode.mpt
        case "MPT CDMA 450":
            return MyNumberTopup.OperatorColorCode.mpt
        case "Mytel":
            return MyNumberTopup.OperatorColorCode.mytel
        default:
           return MyNumberTopup.OperatorColorCode.mpt
        }
    }

    func getNumberRangeValidation(_ prefix: String) -> (min: Int, max: Int, operator: String, isRejected: Bool, color: String) {
        let tuple = myanmarValidation(prefix)
        return tuple
    }
}
