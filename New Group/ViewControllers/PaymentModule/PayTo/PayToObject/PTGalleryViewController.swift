//
//  PTGalleryViewController.swift
//  OK
//
//  Created by Ashish on 12/14/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PTGalleryViewController: PayToBaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet var collectV: UICollectionView!
    var arrayImages = [String]()
    
    var reqMoney = ""
    
    @IBOutlet var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setPOPButton()
        if reqMoney == "" {
            self.title = "Image Gallery".localized
        } else {
            self.title = "Attached File".localized
        }
        println_debug(reqMoney)
        self.pageControl.numberOfPages = self.arrayImages.count
        self.pageControl.currentPage = 0
        self.pageControl.pageIndicatorTintColor = UIColor.lightGray
        self.pageControl.currentPageIndicatorTintColor = UIColor.white
        
        self.setupRotationImage()
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    //MARK:- Rotation Button Action
    fileprivate  func setupRotationImage() -> Void {
        let buttonIcon      = UIImage(named: "refresh_icon")
        let rightBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: self, action: #selector(PTGalleryViewController.rotationActionOnUIImage(_:)))
        rightBarButton.image = buttonIcon
        rightBarButton.tintColor = .blue
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    @objc fileprivate func rotationActionOnUIImage(_ sender:UIBarButtonItem!) -> Void {
        guard let cell = self.collectV.visibleCells.first else { return }
        if let galleryCell = cell as? GalleryCell_Payto {
            galleryCell.rotatedImage()
        }
    }
    

    //MARK:- Collection View Datasource & Delegate
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryCollectionIdentifier", for: indexPath) as! GalleryCell_Payto
        cell.wrapImage(string: arrayImages[indexPath.section], fromReqMoney: reqMoney)
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
       return self.arrayImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.section
        cell.layoutIfNeeded()
        
        guard let cellGesture = cell as? GalleryCell_Payto else { return }

        if let pinch = cellGesture.scrollV.pinchGestureRecognizer {
            UIView.transition(with: cellGesture, duration: 0.8, options: .transitionFlipFromRight, animations: {
                self.collectV.addGestureRecognizer(pinch)
                self.collectV.addGestureRecognizer(cellGesture.scrollV.panGestureRecognizer)
            })
          
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cellGesture = cell as? GalleryCell_Payto else { return }
        
        if let pinch = cellGesture.scrollV.pinchGestureRecognizer {
            self.collectV.removeGestureRecognizer(pinch)
            self.collectV.removeGestureRecognizer(cellGesture.scrollV.panGestureRecognizer)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize{
        let cellHeight = collectionView.frame.height
        let cellWidth  = collectionView.frame.width
        return CGSize(width: cellWidth, height: cellHeight)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
    }
}

 //MARK:- Gallery Cell class
 class GalleryCell_Payto : UICollectionViewCell, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollV: UIScrollView!
    
    @IBOutlet var gImage: UIImageView!
    
    override func awakeFromNib() {
        self.scrollV.delegate = self
        self.scrollV.minimumZoomScale = 1.00
        self.scrollV.maximumZoomScale = 3.5
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.gImage
    }
    
    func wrapImage(string: String, fromReqMoney : String?) {
        if fromReqMoney == "" {
            if let stringUrl = string.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                if let url = URL.init(string: stringUrl) {
                    DispatchQueue.global(qos: .background).async {
                        self.gImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ok"), options: .scaleDownLargeImages, completed: { [weak self] (image, error, type, url) in
                            DispatchQueue.main.async {
                                self?.gImage.image = image
                            }
                        })
                    }
                }
            }
        } else {
            if string != "" {
                if let url = URL.init(string: string) {
                    DispatchQueue.global(qos: .background).async {
                        self.gImage.sd_setImage(with: url, placeholderImage: UIImage(named: "ok"), options: .scaleDownLargeImages, completed: { [weak self] (image, error, type, url) in
                            DispatchQueue.main.async {
                                self?.gImage.image = image
                            }
                        })
                    }
                } else {
                    if let stringUrl = string.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                        if let url = URL.init(string: stringUrl) {
                            DispatchQueue.global(qos: .background).async {
                                self.gImage.sd_setImage(with: url, placeholderImage: UIImage(named: "ok"), options: .scaleDownLargeImages, completed: { [weak self] (image, error, type, url) in
                                    DispatchQueue.main.async {
                                        self?.gImage.image = image
                                    }
                                })
                            }
                        }
                    }
                }
            }
        }
    }
    
    func rotatedImage() {
        if let image = self.gImage.image {
            self.gImage.image = image.imageRotatedByDegrees(degrees: CGFloat(90))
        }
    }
}
