//
//  SharePDFViewController.swift
//  OK
//
//  Created by Ashish on 5/14/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import WebKit

class SharePDFViewController: OKBaseController,WKNavigationDelegate,WKUIDelegate {
    
    //MARK : - Outlets
 //   @IBOutlet weak var pdfDisplayWV: UIWebView!
    @IBOutlet weak var shareBtn : UIButton!{
        didSet{
            shareBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    @IBOutlet var pdfDisplayWV: UIWebView!
    //MARK : - Properties
    var url : URL?
    var urlWithCashBack: URL?
    
    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backButton(withTitle: "".localized)
        
        pdfDisplayWV.delegate = self
//        pdfDisplayWV.uiDelegate = self
        
        if let pdfUrl = urlWithCashBack {
            let request = URLRequest.init(url: pdfUrl)
            self.pdfDisplayWV.loadRequest(request)
//            let urlString: String = pdfUrl.absoluteString
//
//            let fileURL = URL(fileURLWithPath: urlString)
//            //print(fileURL)
//            pdfDisplayWV.loadFileURL(fileURL, allowingReadAccessTo: fileURL)
            
        } else if let pdfUrl = url {
            let request = URLRequest.init(url: pdfUrl)
            self.pdfDisplayWV.loadRequest(request)
            
//            let urlString: String = pdfUrl.absoluteString
            
//            let fileURL = URL(fileURLWithPath: urlString)
            //print(fileURL)
//            pdfDisplayWV.loadFileURL(fileURL, allowingReadAccessTo: fileURL)
         
        }
        self.shareBtn.setTitle("Share".localized, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    //MARK: - Methods
    func backButton(withTitle title:String) {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "tabBarBack"), for: UIControl.State.normal)
        button.tintColor = UIColor.white
        button.sizeToFit()
        button.addTarget(self, action: #selector(backButtonCustomAction), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.title = title
    }
    
    //MARK: - Button Action Methods
    @IBAction func sharePdf(_ sender: UIButton) {
        guard let pdfUrl = url else { return }
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    
    private func webView(webView: WKWebView!, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError!) {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        //   println_debug("Started")
        
        print("load")
        PTLoader.shared.show()
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        //   println_debug("Finished")
        print("finish")
        PTLoader.shared.hide()
    }
    
}

extension SharePDFViewController: UIWebViewDelegate {
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
    }
}
