//
//  PTMoreViewController.swift
//  OK
//
//  Created by Ashish on 12/14/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol MoreControllerActionDelegate : class {
    func actionFor(option: MorePaymentAction)
}

class PTMoreViewController: PayToBaseViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var touchResitantView: CardDesignView!
    
    @IBOutlet weak var morePayment: UIButton!{
        didSet{
            self.morePayment.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var repeatPayment: UIButton!{
        didSet{
            self.repeatPayment.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var invoiceBtn: UIButton!{
        didSet{
            self.invoiceBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var shareBtn: UIButton!{
        didSet{
            self.shareBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }

    
    var titleMorePayment : String?
    var titleRepeatPayment : String?
    
    var internationalTopup : String?
    
    var isFromTopup : Bool = true
    var isFromEarnPointTrf: Bool = false
    
    var headerTopup : (String,String)? = ("More Top-Up".localized,"Repeat Top-Up".localized)
    
   weak var delegate : MoreControllerActionDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideWhenTappedAround()
        
        if let _ = internationalTopup {
            self.morePayment.setTitle("Overseas More Top-up".localized, for: .normal)
            self.repeatPayment.setTitle("Repeat Same Country".localized, for: .normal)
            self.invoiceBtn.setTitle("Invoice".localized, for: .normal)
            self.shareBtn.setTitle("Share".localized, for: .normal)
        } else {
            if isFromEarnPointTrf {
                self.morePayment.setTitle("More Transaction".localized, for: .normal)
                self.repeatPayment.setTitle("Repeat Transaction".localized, for: .normal)
            } else if isFromTopup {
//                self.morePayment.setTitle(headerTopup!.0.localized, for: .normal)
//                self.repeatPayment.setTitle(headerTopup!.1.localized, for: .normal)
                self.morePayment.setTitle(headerTopup!.0, for: .normal)
                self.repeatPayment.setTitle(headerTopup!.1, for: .normal)
            } else {
                self.morePayment.setTitle("More Transaction".localized, for: .normal)
                self.repeatPayment.setTitle("Repeat Transaction".localized, for: .normal)
            }
            self.invoiceBtn.setTitle("Invoice".localized, for: .normal)
            self.shareBtn.setTitle("Share".localized, for: .normal)
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func hideWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(dismissSelf))
        tap.delegate = self
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissSelf() {
        println_debug("removed called")
        removeView()
    }
    
    func removeView() {
        self.view.removeFromSuperview()
    }
    
    //MARK:- More Controller Actions
    @IBAction func morePayment(_ sender: UIButton) {
        removeView()
        if let delegate = self.delegate {
            delegate.actionFor(option: .morePayment)
        }
    }
    
    @IBAction func repeatPayment(_ sender: UIButton) {
        removeView()
        if let delegate = self.delegate {
            delegate.actionFor(option: .repeatPayment)
        }
    }
    
    @IBAction func invoice(_ sender: UIButton) {
        removeView()
        if let delegate = self.delegate {
            delegate.actionFor(option: .invoice)
        }
    }
    
    @IBAction func share(_ sender: UIButton) {
        removeView()
        if let delegate = self.delegate {
            delegate.actionFor(option: .share)
        }
    }
    
    //MARK:- UIGestureRecognizerDelegate
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        let point = touch.location(in: self.view)
        if self.touchResitantView.frame.contains(point) {
            println_debug(self.touchResitantView.frame.contains(point))
            return false
        }
        return true
    }
}

enum MorePaymentAction {
    case morePayment, repeatPayment, invoice, share
}
