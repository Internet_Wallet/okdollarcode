//
//  PTQRGenerator.swift
//  OK
//
//  Created by Ashish on 12/21/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox
import CoreGraphics

class PTQRGenerator: NSObject {
    
    var qrImage : CIImage?
    
    func getQRImage(stringQR: String, withSize rate: CGFloat) -> UIImage? {
        if let filter = CIFilter.init(name: "CIQRCodeGenerator") {
            filter.setDefaults()
            if let data : Data = stringQR.data(using: String.Encoding.utf8) {
                filter.setValue(data, forKey: "inputMessage")
                if let resultImage : CIImage = filter.outputImage {
                    let transform = CGAffineTransform(scaleX: 12, y: 12)
                    let translatedImage = resultImage.transformed(by: transform)
                    guard let logo = UIImage(named: "okEmbedInQR"), let logoInCGImage = logo.cgImage else {
                        return nil
                    }
                    guard let qrWithLogoCI = translatedImage.combined(with:  CIImage(cgImage: logoInCGImage)) else {
                        return nil
                    }
                    let context = CIContext.init(options: nil)
                    guard let qrWithLogoCG = context.createCGImage(qrWithLogoCI, from: qrWithLogoCI.extent) else {
                        return nil
                    }
                    var image   = UIImage.init(cgImage: qrWithLogoCG, scale: 1.0, orientation: UIImage.Orientation.up)
                    let width  =  image.size.width * rate
                    let height =  image.size.height * rate
                    UIGraphicsBeginImageContext(.init(width: width, height: height))
                    let cgContext = UIGraphicsGetCurrentContext()
                    cgContext?.interpolationQuality = .none
                    image.draw(in: .init(origin: .init(x: 0, y: 0), size: .init(width: width, height: height)))
                    guard let imageFromCurrentImageContext = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
                    image = imageFromCurrentImageContext
                    UIGraphicsEndImageContext()
                    return image
                }
            }
        }
        return nil
    }
}
