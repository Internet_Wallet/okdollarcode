//
//  PayToTransactionCell.swift
//  OK
//
//  Created by Ashish on 12/11/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreFoundation

enum PaytoTransactionCellType {
    case name, number, amount,cashback, button, businessName
}

struct TransTypeStringIdentifiers {
    static let labelDesign  = "PayToTransactionCellTypeLabel"
    static let buttonDesign = "PayToTransactionCellTypeButton"
    static let textfieldDesign = "PayToTransactionCellTypeTextfield"
}

struct PaytoTransactionCellModel {
    var section = 0
    var cell : [PayToTransactionCell] = [PayToTransactionCell]()
}

protocol PayToTransactionCellDelegate {
    func transactionCellEvent(cell: PayToTransactionCell)
    func didTapButton(height: CGFloat, inCell cell: PayToTransactionCell)
}

class PayToTransactionCell: UITableViewCell {
    
    @IBOutlet var keyLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!
    
    @IBOutlet var myNumberButton: UIButton!
    @IBOutlet var otherNumberButton: UIButton!
    
    @IBOutlet var mobileNumberTextfield: UITextField!
    @IBOutlet var confirmMobileTextfield: UITextField!
    
    @IBOutlet var mobileNumberConstraints: NSLayoutConstraint!
    @IBOutlet var cnfMobileNumberConstraints: NSLayoutConstraint!
    
    var indexPath = IndexPath()
    
    var delegate : PayToTransactionCellDelegate?
    
    func wrapCellContent(dict: Dictionary<String,Any>, type: PaytoTransactionCellType) {

        switch type {
        case .number:
            self.keyLabel.text = "Reciever"
            self.valueLabel.text = (dict.safeValueForKey("destination") as? String != nil) ? dict.safeValueForKey("destination") as? String : ""
            break
        case .name:
            self.keyLabel.text = "Name"
            self.valueLabel.text = dict.safeValueForKey("destinationName") as? String ?? ""
            break
        case .amount:
            self.keyLabel.text = "Amount"
            self.valueLabel.text = (dict.safeValueForKey("amount") as? String != nil) ? dict.safeValueForKey("amount") as? String : ""
            break
        case .cashback:
            self.keyLabel.text = "Cashback"
            self.valueLabel.text = (dict.safeValueForKey("kickback") as? String != nil) ? dict.safeValueForKey("kickback") as? String : ""
            break
        case .button:
            self.cnfMobileNumberConstraints.constant = 0.0
            self.mobileNumberConstraints.constant = 0.0
            self.myNumberButton.addTargetClosure(closure: { (sender) in
                self.cnfMobileNumberConstraints.constant = 0.0
                self.mobileNumberConstraints.constant = 0.0
                if let delegate = self.delegate {
                    let height = 50.00 + self.cnfMobileNumberConstraints.constant + self.mobileNumberConstraints.constant
                    delegate.didTapButton(height: height, inCell: self)
                    UIView.animate(withDuration: 0.5, animations: {
                        self.layoutIfNeeded()
                    })
                }
            })
            
            self.otherNumberButton.addTargetClosure(closure: { (sender) in
                self.cnfMobileNumberConstraints.constant = 50.0
                self.mobileNumberConstraints.constant = 50.0
                if let delegate = self.delegate {
                    let height = 50.00 + self.cnfMobileNumberConstraints.constant + self.mobileNumberConstraints.constant
                    delegate.didTapButton(height: height, inCell: self)
                    UIView.animate(withDuration: 0.5, animations: {
                        self.layoutIfNeeded()
                    })
                }
            })
            break
        case .businessName:
            self.keyLabel.text = "Business Name"
            self.valueLabel.text = dict.safeValueForKey("businessName") as? String ?? ""
        }
    }

}
