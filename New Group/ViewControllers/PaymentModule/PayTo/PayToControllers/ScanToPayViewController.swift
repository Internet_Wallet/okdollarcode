//
//  ScanToPayViewController.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/27/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import PhotosUI
import AVFoundation
import AudioToolbox

struct PTScanObject {
    var amount  = ""
    var name    = "Unknown"
    var agentNo = ""
    var remarks = ""
    var cashInQR = false
    var cashInLimit: Double = 0.0
    var cashoutLimit: Double = 0.0
    var referenceNumberMIT = ""
    init(amt: String, nme: String, agent: String, remark: String?, cashInQR: Bool? = false, cashInLimit: Double? = 0, cashOutLimit: Double? = 0, referenceNumber: String = "") {
        self.amount  = amt
        self.name    = nme
        self.agentNo = agent
        self.remarks = remark ?? ""
        self.cashInQR = cashInQR ?? false
        self.cashInLimit = cashInLimit ?? 0.0
        self.cashoutLimit = cashOutLimit ?? 0.0
        self.referenceNumberMIT = referenceNumber
    }
}

protocol ScanObject_MultiPayDelegate : class {
    func navigateToMulti(obj: PTScanObject)
    func selfUpdateScan(obj: PTScanObject)
}


class ScanToPayViewController: PayToBaseViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet weak var lblScanQR: UILabel!
        {
        didSet
        {
            lblScanQR.font = UIFont(name: appFont, size: appFontSize)
            lblScanQR.text = "Scan OK$ QR From Gallery".localized
        }
    }
    
    @IBOutlet weak var closeBtn: UIButton! {
        didSet {
            self.closeBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.closeBtn.isHidden = true
        }
    }
    
    @IBOutlet weak var scanToPayLbl: UILabel! {
        didSet {
            self.scanToPayLbl.isHidden = true
            self.scanToPayLbl.font = UIFont(name: appFont, size: appFontSize)
            self.scanToPayLbl.text = "Scan OK$ QR Code to Pay".localized
        }
    }
    
    var screenFromEarnPointTfr: Bool = false
    var cicoQRScreen: Bool = false
    var navigation: UINavigationController?
    var transType: PayToType = PayToType(type: .payTo)
    let systemSoundId : SystemSoundID = 1016
    
    var isOfferEnabled: Bool = false

   weak var delegate  : ScrollChangeDelegate?
    
    weak var mDelegate : ScanObject_MultiPayDelegate?
    
    var scanObj  : PTScanObject?
    
    var scanDad  : PayToViewController?
    
    var screen = MultiSelectionType.payto
    
    var popBackScreen : PaySendViewController?
    
    @IBOutlet weak var containerViews: UIView!
    @IBOutlet weak var buttonContainerView: CardDesignView!
    
    //captureSession manages capture activity and coordinates between input device and captures outputs
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    
    //Empty Rectangle with green border to outline detected QR or BarCode
    let codeFrame:UIView = {
        let codeFrame = UIView()
        codeFrame.layer.borderColor =  UIColor.init(hex: "F3C632").cgColor
        codeFrame.layer.borderWidth = 2.5
        codeFrame.frame = CGRect.zero
        codeFrame.translatesAutoresizingMaskIntoConstraints = false
        return codeFrame
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setPOPButton()
        if screenFromEarnPointTfr {
            self.closeBtn.isHidden = false
            self.scanToPayLbl.isHidden = false
            self.navigationController?.isNavigationBarHidden = true
            navigation?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20.00) ?? UIFont.systemFont(ofSize: 15.00), NSAttributedString.Key.foregroundColor: UIColor.white]
        } else {
            PaytoConstants.global.navigation?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20.00) ?? UIFont.systemFont(ofSize: 15.00), NSAttributedString.Key.foregroundColor: UIColor.white]
        }
        self.title = "QR Code".localized
        
        if self.scanDad != nil {
            println_debug("Scan Code Parent Found : Party")
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(ScanToPayViewController.appWillResignActive), name: UIApplication.willEnterForegroundNotification, object: nil)
        
    }
    
    @IBAction func closeBtnAction(_ sender: UIButton) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigation?.popViewController(animated: true)
    }
    
    @objc func appWillResignActive() {
        self.flashImage.isHighlighted = false
    }
    
    private func loadCamera() {
        DispatchQueue.global(qos: .background).async {
            //AVCaptureDevice allows us to reference a physical capture device (video in our case)
//            DispatchQueue.main.async {
//                self.okActivityIndicator(view: self.view)
//            }
            let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
            if let captureDevice = captureDevice {
                do {
                    self.captureSession = AVCaptureSession()
                    // CaptureSession needs an input to capture Data from
                    let input = try AVCaptureDeviceInput(device: captureDevice)
                    self.captureSession?.addInput(input)
                    // CaptureSession needs and output to transfer Data to
                    let captureMetadataOutput = AVCaptureMetadataOutput()
                    self.captureSession?.addOutput(captureMetadataOutput)
                    //We tell our Output the expected Meta-data type
                    captureMetadataOutput.setMetadataObjectsDelegate(self , queue: DispatchQueue.main)
                    captureMetadataOutput.metadataObjectTypes = [.code128, .qr,.ean13, .ean8, .code39, .upce, .aztec, .pdf417] //AVMetadataObject.ObjectType
                    self.captureSession?.startRunning()
                    //The videoPreviewLayer displays video in conjunction with the captureSession
                    if let session = self.captureSession {
                        self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
                    }
                    self.videoPreviewLayer?.videoGravity = .resizeAspectFill
                    DispatchQueue.main.async {
                        self.videoPreviewLayer?.frame = self.view.layer.bounds
                        if let viewVideo = self.videoPreviewLayer {
                            self.view.layer.addSublayer(viewVideo)
                        }
                        self.view.bringSubviewToFront(self.containerViews)
                        self.view.bringSubviewToFront(self.buttonContainerView)
                    }
                }
                catch {
                    println_debug("Error")
                    DispatchQueue.main.async {
                        self.removeActivityIndicator(view: self.view)
                    }
                }
            }
            DispatchQueue.main.async {
                self.removeActivityIndicator(view: self.view)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.flashImage.isHighlighted = false
        if AVCaptureDevice.authorizationStatus(for: .video) == AVAuthorizationStatus.authorized {
            self.loadCamera()
            self.flashImage.isHighlighted = false
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted) in
                switch granted {
                case true:
                    self.loadCamera()
                case false:
                    self.requestAlertForPermissions(withBody: "OK$ need camera access to scan".localized , handler: { (cancelled) in
                        
                    })
                }
            })
        }
        if screenFromEarnPointTfr {
            self.navigationController?.isNavigationBarHidden = true
        }
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if let device = AVCaptureDevice.default(for: AVMediaType.video) {
            if (device.hasTorch) {
                do {
                    try device.lockForConfiguration()
                    if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                        device.torchMode = AVCaptureDevice.TorchMode.off
                        self.flashImage.isHighlighted = false
                    }
                    device.unlockForConfiguration()
                } catch {
                    println_debug(error)
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if screenFromEarnPointTfr {
            self.navigationController?.isNavigationBarHidden = false
        }
        if captureSession?.isRunning ?? false {
            captureSession?.stopRunning()
        }
    }

    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects.count == 0 {
            println_debug("no objects returned")
            return
        }
        
        guard let metaDataObject = metadataObjects[0] as? AVMetadataMachineReadableCodeObject else { return }
        guard let StringCodeValue = metaDataObject.stringValue else {
            return
        }
        
        view.addSubview(codeFrame)

        guard let metaDataCoordinates = videoPreviewLayer?.transformedMetadataObject(for: metaDataObject) else {
            return
        }
        
        codeFrame.frame = metaDataCoordinates.bounds
        let feedbackGenerator = UIImpactFeedbackGenerator(style: .heavy)
        feedbackGenerator.impactOccurred()
        captureSession?.stopRunning()
        println_debug(StringCodeValue)
        DispatchQueue.main.async {
            self.scannedString(str: StringCodeValue)
        }
    }

    func scannedString(str: String) {

        _ = AESCrypt.decrypt(str.components(separatedBy: "---")[0], password: "m2n1shlko@$p##d")
        
        var sttt: String = ""
        println_debug(AESCrypt.decrypt(str, password: "m2n1shlko@$p##d"))
        if let stringQR = AESCrypt.decrypt(str, password: "m2n1shlko@$p##d") {
            sttt = stringQR
        } else {
            DispatchQueue.main.async {
                alertViewObj.wrapAlert(title: nil, body: "Please scan correct QR code".localized, img: #imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    self.captureSession?.startRunning()
                })
                alertViewObj.showAlert(controller: self)
                self.captureSession?.stopRunning()
            }
            
            return
        }
        /*
        if sttt != "" {
            
            if sttt.contains("α") {
                if sttt.contains("-1") {
                    sttt = ""
                }
                else {
                }
                if sttt.contains("`") {
                }
                else {
                    // sttt=@"";
                }
                println_debug("contains yes")
            }
            else {
                sttt = ""
            }
            
        }
        */
        if(sttt.count>0){
            
//            var cellId: String = ""
            var businessName: String = ""
            var businessMobile: String = ""
            
            let myArray: [Any] = sttt.components(separatedBy: "#")
//            _ = myArray[0] as! String ?? ""
            
            let seconArray: [Any] = (myArray[1] as AnyObject).components(separatedBy: "-")
            businessName = seconArray[0] as? String ?? ""
            
            let thirdArray: [Any] = (seconArray[1] as AnyObject).components(separatedBy: "@")
            businessMobile = thirdArray[0] as? String ?? ""
            
            let fourthArray: [Any] = (thirdArray[1] as AnyObject).components(separatedBy: "&")
            let amt : String = fourthArray[0] as! String
            var remrk = ""
            
            let fifthArray: [String] = (fourthArray[1] as AnyObject).components(separatedBy: "`")
            if fifthArray.indices.contains(1) {
                if sttt.contains(find: "%CashInQR%") {
                    let seventhArray:[String] = (fifthArray[1] as AnyObject).components(separatedBy: "!")
                    let sixthArray:[String] = (seventhArray[0] as AnyObject).components(separatedBy: ",")
                    if sixthArray.count > 0 {
                        let scanPromotionObj = ["status" :true, "beforePromotion" : sixthArray[1], "afterPromotion": sixthArray[0]] as [String : Any]
                        UserDefaults.standard.set(scanPromotionObj, forKey: "ScanQRPromotion")
                        if sixthArray.indices.contains(2) {
                            remrk = sixthArray[2]
                        }
                    }
                } else {
                    let sixthArray:[String] = (fifthArray[1] as AnyObject).components(separatedBy: ",")
                    if sixthArray.count > 0 {
                        let scanPromotionObj = ["status" :true, "beforePromotion" : sixthArray[1], "afterPromotion": sixthArray[0]] as [String : Any]
                        UserDefaults.standard.set(scanPromotionObj, forKey: "ScanQRPromotion")
                        if sixthArray.indices.contains(2) {
                            remrk = sixthArray[2]
                        }
                    }
                }
            }
            var cashInQR = false
            if self.transType.type != .cashIn && sttt.contains(find: "%CashInQR%") {
                DispatchQueue.main.async { 
                    alertViewObj.wrapAlert(title: nil, body: "Please scan QR code from Transfer To".localized, img: #imageLiteral(resourceName: "alert-icon"))
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        self.captureSession?.startRunning()
                    })
                    alertViewObj.showAlert(controller: self)
                }
                self.captureSession?.stopRunning()
                return
            } else if self.transType.type == .cashIn && sttt.contains(find: "%CashInQR%") {
                let destArray: [Any] = (myArray[1] as AnyObject).components(separatedBy: ">")
                var destNum : String = destArray[1] as! String
                destNum = destNum.replacingOccurrences(of: "%CashInQR%", with: "")
                if UserModel.shared.formattedNumber != destNum {
                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: nil, body: "Please scan correct QR code".localized, img: #imageLiteral(resourceName: "alert-icon"))
                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                            self.captureSession?.startRunning()
                        })
                        alertViewObj.showAlert(controller: self)
                        self.captureSession?.stopRunning()
                    }
                    
                    return
                } else {
                    let remrkArr = remrk.components(separatedBy: "!")
                    remrk = remrkArr.first ?? ""
                    cashInQR = true
                }
            } else {
                
            }
            
            self.scanObj = PTScanObject.init(amt: amt, nme: businessName, agent: businessMobile, remark: remrk, cashInQR: cashInQR)
            if screenFromEarnPointTfr == true {
                if let delegate = self.delegate, let scanObj = self.scanObj {
                    delegate.dismissScanOffer(object: scanObj, controller: self)
                    self.navigationController?.isNavigationBarHidden = false
                    self.navigation?.popViewController(animated: false)
                }
            } else  if cicoQRScreen == true {
                if let delegate = self.delegate, let scanObj = self.scanObj {
                    delegate.changeScrollPage(point: 0, withScanObject: scanObj)
                    PaytoConstants.global.navigation?.popViewController(animated: true)
                }
            } else if screen == .payto {
                if let delegate = self.delegate, let scanObj = self.scanObj {
                    if isOfferEnabled {
                        delegate.changeScrollPage(point: 0, withScanObject: scanObj)
                        delegate.dismissScanOffer(object: scanObj, controller: self)
                        PaytoConstants.global.navigation?.popViewController(animated: true)
                    } else {
                        delegate.changeScrollPage(point: 0, withScanObject: scanObj)
                        delegate.dismissScanOffer(object: scanObj, controller: self)
                    }
                    
                }
                
            } else if screen == .multi {
                screen = .payto
                if let delegate = self.delegate, let scanObj = self.scanObj {
                    delegate.changeScrollPage(point: 0, withScanObject: scanObj)
                    
                    PaytoConstants.global.navigation?.popViewController(animated: false)
                }
            } else if screen == .selfUpdate {
                screen = .payto
                if let delegate = mDelegate, let scanObj = self.scanObj {
                    delegate.selfUpdateScan(obj: scanObj)
                    PaytoConstants.global.navigation?.popViewController(animated: false)
                }
            } 
        } else {
             captureSession?.startRunning()
            return
        }
        codeFrame.removeFromSuperview()
        captureSession?.stopRunning()
    }
    
    @IBOutlet var flashImage: UIImageView!
    @IBAction func camera_flash(_ sender: UIButton) {
        if let device = AVCaptureDevice.default(for: AVMediaType.video) {
            if (device.hasTorch) {
                do {
                    try device.lockForConfiguration()
                    if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                        self.flashImage.isHighlighted = false
                        device.torchMode = AVCaptureDevice.TorchMode.off
                    } else {
                         self.flashImage.isHighlighted = true
                        do {
                            try device.setTorchModeOn(level: 1.0)
                        } catch {
                            println_debug(error)
                        }
                    }
                    device.unlockForConfiguration()
                } catch {
                    println_debug(error)
                }
            }
        }
    }

    @IBAction func scanFromGallery(_ sender: UIButton) {
        
        if let device = AVCaptureDevice.default(for: AVMediaType.video) {
            if (device.hasTorch) {
                do {
                    try device.lockForConfiguration()
                    if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                        device.torchMode = AVCaptureDevice.TorchMode.off
                        self.flashImage.isHighlighted = false
                    }
                    device.unlockForConfiguration()
                } catch {
                    println_debug(error)
                }
            }
        }
        let storyBoard = UIStoryboard(name: "PayTo", bundle: nil)
        guard let vc = storyBoard.instantiateViewController(withIdentifier: "QRPhotoScanner") as? QRPhotoScanner else { return }
        vc.scanQRController = self
        
        if screenFromEarnPointTfr {
            self.navigation?.pushViewController(vc, animated: true)
        } else {
            PaytoConstants.global.navigation?.pushViewController(vc, animated: true)
        }
    }
    
    deinit {
        println_debug("deinit called scan qr code in payto")
        NotificationCenter.default.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
    }

}
