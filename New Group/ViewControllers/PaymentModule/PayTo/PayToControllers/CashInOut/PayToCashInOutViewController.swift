//
//  PayCashInOutViewController.swift
//  OK
//
//  Created by iMac on 2/6/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import CoreLocation

class PayToCashInOutViewController: OKBaseController {
    
    @IBOutlet weak var cicoTableView: UITableView!
    
    let cashInArray = ["Paid Commission", "Non Paid Commission", "Cash In Nearby Agent"]
    let cashoutArray = ["Paid Commission", "Non Paid Commission", "Cash Out Nearby Agent"]
    
    let cashInImageArray = ["cashin_with_fee","cashin_okdollar","add_money_ok_agent"]
    let cashOutImageArray = ["cashout_with_fee","cashaout_okdollar","add_money_ok_agent"]

    var transType: PayToType = PayToType(type: .payTo)
    
    let objectSound = BurmeseSoundObject()
    
    @IBOutlet weak var voiceLabel: UILabel! {
        didSet {
            voiceLabel.font = UIFont(name: appFont, size: appFontSize)
            voiceLabel.text = "Voice".localized
        }
    }

    var playSound: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: PaytoConstants.global.zawgyiFont, NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title =  (transType.type == .cashIn) ? "Cash In".localized : "Cash Out".localized
        self.backButton()
        self.cicoTableView.tableFooterView = UIView.init()
        // Do any additional setup after loading the view.
    }
    
    func navigateToPayto(type: PayToType, headerTitle: String = "Pay / Send") {
        guard let payto = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PayToNavigation") as? UINavigationController else { return }
        isPayToDashboard = false
        for controller in payto.viewControllers {
            if let payToVC = controller as? PayToViewController {
                payToVC.headerTitle = headerTitle.localized
                payToVC.fromMerchant = true
                payToVC.transType = type
            }
        }
        self.present(payto, animated: true, completion: nil)
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:"OK$ would like to use your Current Location. Please turn on the Location Services for your device.".localized, img:#imageLiteral(resourceName: "map"))
        
        alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
            
        })
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: self.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            }
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    private func navigateToOK$Service() {
        if(appDel.checkNetworkAvail()) {
            if CLLocationManager.locationServicesEnabled() {
                switch(CLLocationManager.authorizationStatus()) {
                case .notDetermined:
                    println_debug("notDetermined")
                    loadNearByServicesView()
                    break
                case .denied :
                    println_debug("denied")
                    showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                    break
                case .restricted :
                    println_debug("restricted")
                    showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                    break
                case .authorizedAlways, .authorizedWhenInUse:
                    loadNearByServicesView()
                    break
                }
            } else {
                alertViewObj.wrapAlert(title:"", body: "Please Enable Location Service".localized, img: #imageLiteral(resourceName: "location"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    
                })
                DispatchQueue.main.async {
                    alertViewObj.showAlert(controller: self)
                }
            }
        } else {
            self.noInternetAlert()
        }
    }
    
    @IBAction func voiceChangeAction(_ sender: UISwitch) {
        if sender.isOn {
            playSound = true
        } else {
            playSound = false
        }
    }
    
    func loadNearByServicesView() {
        let story: UIStoryboard = UIStoryboard(name: "NBSMain", bundle: nil)
        let nearByOkServicesView = story.instantiateViewController(withIdentifier: "NearByServicesMainVC") as! NearByServicesMainVC
        nearByOkServicesView.statusScreen = "NearBy"
        nearByOkServicesView.isPushed = true
        self.navigationController?.pushViewController(nearByOkServicesView, animated: true)
    }
    
    
    private func backButton() {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "tabBarBack"), for: UIControl.State.normal)
        button.tintColor = UIColor.white
        button.sizeToFit()
        button.addTargetClosure { [weak self](button) in
            self?.navigationController?.popViewController(animated: true)
        }
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    func navigateTo(index: IndexPath) {
        switch index.row {
        case 0:
            if transType.type == .cashIn {
                if playSound {
                    objectSound.playSound(fileName: "Cash In with commission")
                }
                self.navigateToPayto(type: transType, headerTitle: "Request Cash In Title")
            } else {
                if playSound {
                    objectSound.playSound(fileName: "Cash Out with commission")
                }
                self.navigateToPayto(type: transType, headerTitle: "Request Cash Out Title")
            }
            
        case 1:
            if playSound {
                if transType.type == .cashIn {
                    objectSound.playSound(fileName: "Cash In without commission in OK$ counter")
                } else {
                    objectSound.playSound(fileName: "Cash Out without commission in OK$ counter")
                }
            }
            self.navigateToPayto(type: transType)
        case 2:
            if playSound {
                if transType.type == .cashIn {
                    objectSound.playSound(fileName: "Near by Agent for Cash In")
                } else {
                    objectSound.playSound(fileName: "Near by Agent for Cash Out")
                }
            }
            self.navigateToOK$Service()
        default:
            break
        }
    }
}


extension PayToCashInOutViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if transType.type == .cashIn {
            return self.cashInArray.count
        } else {
            return self.cashoutArray.count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? PayTOCashinOutTableCell
        if transType.type == .cashIn {
            cell?.wrapData(str: self.cashInArray[indexPath.row], imageName: cashInImageArray[indexPath.row])
        } else {
            cell?.wrapData(str: self.cashoutArray[indexPath.row], imageName: cashOutImageArray[indexPath.row])
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigateTo(index: indexPath)
    }
}


class PayTOCashinOutTableCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!{
        didSet{
            self.titleLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var rightArrowImageView: UIImageView! {
        didSet {
            rightArrowImageView.image = rightArrowImageView.image?.withRenderingMode(.alwaysTemplate)
            rightArrowImageView.tintColor = .darkGray
        }
    }
    
    func wrapData(str: String, imageName: String) {
        titleLbl.text = str.localized
        titleImageView.image = UIImage(named: imageName)
    }
    
}
