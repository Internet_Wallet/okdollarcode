//
//  CashInQRViewController.swift
//  OK
//
//  Created by iMac on 4/27/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class CashInQRViewController: OKBaseController {
    
    var model = Dictionary<String, Any>()
    var qrcodeImage: CIImage!
    @IBOutlet weak var imgQRCode: UIImageView!
    @IBOutlet weak var remarksHeight: NSLayoutConstraint!
    @IBOutlet weak var mainViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var numberLbl: UILabel!{
        didSet{
            numberLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var nameLbl: UILabel!{
        didSet{
            nameLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var updateView: CashInQRUpdateView!
    
    @IBOutlet weak var scantopayLabel: UILabel! {
        didSet {
            scantopayLabel.font = UIFont(name: appFont, size: appFontSize)
            self.scantopayLabel.text = self.scantopayLabel.text?.localized
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureBackButton(withTitle: "Cash In QR".localized)
        self.nameLbl.text = UserModel.shared.name
        self.numberLbl.text = "(\(UserModel.shared.countryCode))\(UserModel.shared.formattedNumber)"
        generateQR()
        updateView.updateView(dict: model)
        // Do any additional setup after loading the view.
        if let remark = model["remark"] as? String, remark != "" {
            let height = remark.height(withConstrainedWidth: screenWidth * 0.46, font: UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: 15.0))
            remarksHeight.constant = height +  10
            mainViewHeight.constant = 667 + height
        } else {
            remarksHeight.constant = 0.0
        }
    }
    
    func changeAmountNumberFormat(amount : String , toDisplay : Bool ) -> String {
        
        if toDisplay == true {
            
            if amount.contains(GlobalConstants.Strings.mmk) {
                return amount
            } else {
                if(amount.length == 0)
                {
                    return "0 MMK"
                }
                return "\(amount)\(GlobalConstants.Strings.mmk)"
            }
        } else {
            if amount.contains(GlobalConstants.Strings.mmk) {
                return amount.replacingOccurrences(of: GlobalConstants.Strings.mmk, with: "")
            } else {
                return amount
            }
        }
    }
    
    func changeMobileNumberFormat(mobileNumber : String , toDisplay : Bool ) -> String {
        
        if toDisplay == true {
            
            if mobileNumber.starts(with: "00") {
                return mobileNumber.substring(from: 1)
            } else {
                return mobileNumber
            }
        } else {
            
            if mobileNumber.starts(with: "00"){
                return mobileNumber
            } else {
                return "0\(mobileNumber)"
            }
        }
    }
    
    func encryptQrCodeData() -> Data? {
        
        var encryptedData : String = ""
        encryptedData.append("00#")
        if let accoutName = model["name"] as? String {
            
            encryptedData.append(accoutName)
        } else {
            
            encryptedData.append(UserModel.shared.name)
        }
        
        encryptedData.append("-")
        if let accountNumber =  model["number"] as? String {
            
            encryptedData.append(changeMobileNumberFormat(mobileNumber: accountNumber, toDisplay: false))
        } else {
            encryptedData.append(UserModel.shared.mobileNo)
        }
        encryptedData.append("@")
        
        if let amount = model["amount"] as? String {
            
            encryptedData.append(changeAmountNumberFormat(amount: amount, toDisplay: false))
        } else {
            
            encryptedData.append("(null)")
        }
        encryptedData.append("&")
        encryptedData.append("0β0.0γ0.0α")
        let updatedTime = todayDateWithOutputFormat(outputFormat: GlobalConstants.Strings.qrCodeDateFormat)
        encryptedData.append(updatedTime)
        encryptedData.append("`,,")
        if let description = model["remark"] as? String {
            encryptedData.append(description)
        }
        encryptedData.append("!")
        if let commissionaAmount = model["commission"] as? String {
            
            encryptedData.append(changeAmountNumberFormat(amount: commissionaAmount, toDisplay: false))
        } else {
            
            encryptedData.append("(null)")
        }
        encryptedData.append(">")
        if var destno = model["destinationNumber"] as? String, var country = model["countryCode"] as? String {
            if country == "+95" {
                country = "00\(country.dropFirst())"
                destno = "\(country)\(destno.dropFirst())"
            } else {
                country = "00\(country.dropFirst())"
                destno = "\(country)\(destno)"
            }
            encryptedData.append(destno)
        } else {
            
            encryptedData.append("(null)")
        }
        
        encryptedData.append("%CashInQR%")
        encryptedData = AESCrypt.encrypt(encryptedData, password: "m2n1shlko@$p##d")
        return encryptedData.data(using: .utf8);
    }
    
    
    func getQRImage(stringQR: String, withSize rate: CGFloat) -> UIImage? {
        if let filter = CIFilter.init(name: "CIQRCodeGenerator") {
            filter.setDefaults()
            if let data : Data = stringQR.data(using: String.Encoding.utf8) {
                filter.setValue(data, forKey: "inputMessage")
                if let resultImage : CIImage = filter.outputImage {
                    let transform = CGAffineTransform(scaleX: 12, y: 12)
                    let translatedImage = resultImage.transformed(by: transform)
                    guard let logo = UIImage(named: "okEmbedInQR"), let logoInCGImage = logo.cgImage else {
                        return nil
                    }
                    guard let qrWithLogoCI = translatedImage.combined(with:  CIImage(cgImage: logoInCGImage)) else {
                        return nil
                    }
                    let context = CIContext.init(options: nil)
                    guard let qrWithLogoCG = context.createCGImage(qrWithLogoCI, from: qrWithLogoCI.extent) else {
                        return nil
                    }
                    var image   = UIImage.init(cgImage: qrWithLogoCG, scale: 1.0, orientation: UIImage.Orientation.up)
                    let width  =  image.size.width * rate
                    let height =  image.size.height * rate
                    UIGraphicsBeginImageContext(.init(width: width, height: height))
                    let cgContext = UIGraphicsGetCurrentContext()
                    cgContext?.interpolationQuality = .none
                    image.draw(in: .init(origin: .init(x: 0, y: 0), size: .init(width: width, height: height)))
                    image = UIGraphicsGetImageFromCurrentImageContext()!
                    UIGraphicsEndImageContext()
                    return image
                }
            }
        }
        return nil
    }
    
    
    func generateQR() {
        var accoutNUmber : String = ""
        
        if let number = model["number"] as? String  {
            accoutNUmber = changeMobileNumberFormat(mobileNumber: number, toDisplay: false)
        }
        
        var accoutNAme : String =  ""
        if let name = model["name"] as? String {
            accoutNAme = name
        }
        
        var merchantNAme : String = ""
        if let mername = model["merchant"] as? String {
            merchantNAme = mername
        }
        
        var operatorNAme : String = ""
        if let opName = model["operator"] as? String {
            operatorNAme = opName
        }
        
        var amountOfREcharge : String = ""
        if let amunt = model["amount"] as? String {
            amountOfREcharge = changeAmountNumberFormat(amount: amunt, toDisplay: false)
        }
        
        var proBfrPayment : String = ""
        if let prmoBfr = model["commission"] as? String {
            proBfrPayment = prmoBfr
        }
        
        var proAftrPayment : String = ""
        if let prmoAfr = model["totalAmount"] as? String {
            proAftrPayment = prmoAfr
        }
        
        var remark : String = ""
        if let remarkQuery = model["remark"] as? String {
            remark = remarkQuery
        }
        
        var destNum : String = ""
        if let destNo = model["destinationNumber"] as? String {
            destNum = destNo
        }
        
        QrCodeModel.wrapUserData(accountName: accoutNAme, accountNumber: accoutNUmber, merchantName: merchantNAme, operatorName: operatorNAme, amountofRecharge: amountOfREcharge, promotionBefrePayment: proBfrPayment, promotionOfferAfterpayment: proAftrPayment, remark: remark, destNum: destNum)
        
        generateQrCodeFromUserInput()
    }
    
    func generateQrCodeFromUserInput(){
        //        let rowQrCodeModelData =  QrCodeModel.getWrappedDataModel(qrCodeModel:qrCodeModel )
        //        let data = txtAmount.text?.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
        // txtRemark.text.data
        //if let qrCodeData = QrCodeModel.parameterForQrCodeGeneration() {
        if let qrCodeData = encryptQrCodeData(){
            let filter = CIFilter(name: "CIQRCodeGenerator")
            
            filter?.setValue(qrCodeData, forKey: "inputMessage")
            filter?.setValue("Q", forKey: "inputCorrectionLevel")
            
            qrcodeImage = filter?.outputImage
            displayQRCodeImage()
            // imgQRCode.image = UIImage.init(ciImage: qrcodeImage)
            
            //if isCallFromRoot == false{
            // configureWithData(isCallFromRoot: false)
            // }
        }
    }
    
    /* func generateQrCodeFromDefaultUserData(isCallFromRoot : Bool){
     if let qrCodeData = QrCodeModel.qrCodeDataGenerateFromDefaultUserData(operatorName: lblOperator.text!) {
     let filter = CIFilter(name: "CIQRCodeGenerator")
     
     filter?.setValue(qrCodeData, forKey: "inputMessage")
     filter?.setValue("Q", forKey: "inputCorrectionLevel")
     
     qrcodeImage = filter?.outputImage
     // imgQRCode.image = UIImage.init(ciImage: qrcodeImage)
     displayQRCodeImage()
     if isCallFromRoot == false {
     configureWithData(isCallFromRoot: false)
     }
     }
     }*/
    
    
    
    func displayQRCodeImage() {
        let scaleX = imgQRCode.frame.size.width / qrcodeImage.extent.size.width
        let scaleY = imgQRCode.frame.size.height / qrcodeImage.extent.size.height
        
        let transformedImage = qrcodeImage.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))
        
        imgQRCode.image = UIImage.init(ciImage:transformedImage)
    }
    
}


class CashInQRUpdateView: UIView {
    @IBOutlet weak var numberLabel: UILabel! {
        didSet {
            self.numberLabel.font = UIFont(name: appFont, size: appFontSize)
            self.numberLabel.text = self.numberLabel.text?.localized
        }
    }
    
    @IBOutlet weak var numberValue: UILabel! {
        didSet{
            numberValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var amountLabel: UILabel! {
        didSet {
            self.amountLabel.font = UIFont(name: appFont, size: appFontSize)
            self.amountLabel.text = self.amountLabel.text?.localized
        }
    }
    @IBOutlet weak var amountValue: UILabel!{
        didSet{
            amountValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var commissionLabel: UILabel! {
        didSet {
            self.commissionLabel.font = UIFont(name: appFont, size: appFontSize)
            self.commissionLabel.text = "Agent Commission".localized
            self.commissionLabel.text = self.commissionLabel.text?.localized
        }
    }
    @IBOutlet weak var commissionValue: UILabel!{
        didSet{
            commissionValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var digitalMoneyLabel: UILabel! {
        didSet {
            self.digitalMoneyLabel.font = UIFont(name: appFont, size: appFontSize)
            self.digitalMoneyLabel.text = "Net Receive Digital Amount".localized
            self.digitalMoneyLabel.text = self.digitalMoneyLabel.text?.localized
        }
    }
    @IBOutlet weak var digitalMoneyValue: UILabel!{
        didSet{
            digitalMoneyValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            self.dateLabel.font = UIFont(name: appFont, size: appFontSize)
            self.dateLabel.text = self.dateLabel.text?.localized
        }
    }
    @IBOutlet weak var dateValue: UILabel!{
        didSet{
            dateValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    
    @IBOutlet weak var remarksView: UIView!
    @IBOutlet weak var remarksLabel: UILabel! {
        didSet {
            self.remarksLabel.font = UIFont(name: appFont, size: appFontSize)
            self.remarksLabel.text = self.remarksLabel.text?.localized
        }
    }
    @IBOutlet weak var remarksValue: UILabel!{
        didSet{
            remarksValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    func updateView(dict: Dictionary<String, Any>?) {
        if let number = dict?["destinationNumber"] as? String {
            var mob = changeMobileNumberFormat(mobileNumber: number, toDisplay: true)
               mob = "(\(dict?["countryCode"] as? String ?? ""))\(mob)"
            self.numberValue.text = mob
        }
        if let amunt = dict?["amount"] as? String {
            let amount = amountWithCommaFormat(textStr: amunt)
            self.amountValue.attributedText = wrapAmountWithMMK(bal: amount)
        }
        if let cmssn = dict?["commission"] as? String {
            let amount = amountWithCommaFormat(textStr: cmssn)
            self.commissionValue.attributedText = wrapAmountWithMMK(bal: amount)
        }
        if let totAmt = dict?["totalAmount"] as? String {
            let amount = amountWithCommaFormat(textStr: totAmt)
            self.digitalMoneyValue.attributedText = wrapAmountWithMMK(bal: amount)
        }
        
        self.dateValue.text = self.updateCurrentDate()
        if let remark = dict?["remark"] as? String, remark != "" {
            self.remarksValue.text = remark
        } else {
            self.remarksView.isHidden = true
        }
    }
    
    private func updateCurrentDate() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE, dd-MMM-yyyy HH:mm"
        let dateStr = formatter.string(from: date)
        return dateStr
    }
    
    
    func changeAmountNumberFormat(amount : String , toDisplay : Bool ) -> String {
        
        if toDisplay == true {
            
            if amount.contains(GlobalConstants.Strings.mmk) {
                return amount
            } else {
                if(amount.length == 0)
                {
                    return "0 MMK"
                }
                return "\(amount)\(GlobalConstants.Strings.mmk)"
            }
        } else {
            if amount.contains(GlobalConstants.Strings.mmk) {
                return amount.replacingOccurrences(of: GlobalConstants.Strings.mmk, with: "")
            } else {
                return amount
            }
        }
    }
    
    func changeMobileNumberFormat(mobileNumber : String , toDisplay : Bool ) -> String {
        
        if toDisplay == true {
            
            if mobileNumber.starts(with: "00") {
                return mobileNumber.substring(from: 1)
            } else {
                return mobileNumber
            }
        } else {
            
            if mobileNumber.starts(with: "00"){
                return mobileNumber
            } else {
                return "0\(mobileNumber)"
            }
        }
    }
    
}
