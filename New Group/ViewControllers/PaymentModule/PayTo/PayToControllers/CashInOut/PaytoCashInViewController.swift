//
//  PaytoCashInViewController.swift
//  OK
//
//  Created by iMac on 4/27/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class PaytoCashInViewController: OKBaseController, AmountRightViewDelegate {
  
    @IBOutlet weak var contactSuggestion: CardDesignView!
    @IBOutlet weak var contactTableView: UITableView!
    @IBOutlet weak var heightContactSuggestion: NSLayoutConstraint!
    var contactSearchedDict = [Dictionary<String,Any>]()
    var amountCheckingTimer: Timer?
     let objectSound = BurmeseSoundObject()
    
    @IBOutlet weak var mobileNumber: PTMobileNumberSubclass! {
        didSet {
            self.mobileNumber.font = UIFont(name: appFont, size: appFontSize)
            self.mobileNumber.placeholder = "Enter Digital Payment Mobile Number".localized
        }
    }
    @IBOutlet weak var cnfMobileNumber: PTMobileNumberSubclass! {
        didSet {
            self.cnfMobileNumber.font = UIFont(name: appFont, size: appFontSize)
            self.cnfMobileNumber.placeholder = "Confirm Digital Payment Mobile Number".localized
        }
    }
    
    @IBOutlet weak var name: CashInTextFieldSubClass! {
        didSet {
            self.name.font = UIFont(name: appFont, size: appFontSize)
            self.name.placeholder = "Name".localized
        }
    }
    
    @IBOutlet weak var amount: PTAmountValidation! {
        didSet {
            self.amount.font = UIFont(name: appFont, size: appFontSize)
            self.amount.placeholder = "Enter Receive Cash In Amount".localized
        }
    }
    @IBOutlet weak var burmeseAmount: CashInTextFieldSubClass! {
        didSet {
            self.burmeseAmount.font = UIFont(name: appFont, size: appFontSize)
            self.burmeseAmount.placeholder = "Burmese Amount".localized
        }
    }
    @IBOutlet weak var remarks: CashInTextFieldSubClass! {
        didSet {
            self.remarks.font = UIFont(name: appFont, size: appFontSize)
            self.remarks.placeholder = "Enter Remarks".localized
        }
    }
    @IBOutlet weak var commissionTxt: CashInTextFieldSubClass! {
        didSet {
            self.commissionTxt.font = UIFont(name: appFont, size: appFontSize)
            self.commissionTxt.isUserInteractionEnabled = false
        }
    }
    @IBOutlet weak var totalAmountTxt: CashInTextFieldSubClass! {
        didSet {
            self.totalAmountTxt.font = UIFont(name: appFont, size: appFontSize)
            self.totalAmountTxt.placeholder = "Net Payable Amount".localized
            self.totalAmountTxt.isUserInteractionEnabled = false
        }
    }
    
    var cnfRightView : PTClearButton?
    
    @IBOutlet weak var generateQRBtn: UIButton! {
        didSet {
            self.generateQRBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.generateQRBtn.isHidden = true
            self.generateQRBtn.setTitle("Generate QR".localized, for: .normal)
            self.generateQRBtn.layer.cornerRadius = 15.0
            self.generateQRBtn.layer.borderWidth = 1.0
            self.generateQRBtn.layer.borderColor = kYellowColor.cgColor
        }
    }
    
    @IBOutlet weak var holderView: CardDesignView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureBackButton(withTitle: "Cash In".localized)
        // Do any additional setup after loading the view.
        mobileNumber.show()
        self.cnfMobileNumber.hide()
        self.name.hide()
        self.amount.hide()
        self.burmeseAmount.hide()
        self.commissionTxt.hide()
        self.totalAmountTxt.hide()
        self.remarks.hide()
        self.mobileNumber.updateRightView(self)
        self.initLeftViews()
        self.mobileNumber.subDelegate = self
        self.cnfMobileNumber.subDelegate = self
        self.amount.subDelegate = self
        contactTableView.tableFooterView = UIView()
        self.updateHeight(increase: true)
    }
    
    func updateHeight(increase: Bool) {
        if increase {
            //holderViewHeight.constant = holderViewHeight.constant + 52.0
        } else {
            //holderViewHeight.constant = holderViewHeight.constant - 52.0
        }
    }
    
    func initLeftViews() {
        let merchantLeftView    = DefaultIconView.updateView(icon: "name_bill")
        let amountLeftView      = DefaultIconView.updateView(icon: "amount")
        let burAmountLeftView   = DefaultIconView.updateView(icon: "amount")
        let commissionLeftView  = DefaultIconView.updateView(icon: "amount")
        let totAmountLeftView   = DefaultIconView.updateView(icon: "amount")
        let remarkLeftView      = DefaultIconView.updateView(icon: "remark")
        
        self.burmeseAmount.leftView      = burAmountLeftView
        self.burmeseAmount.leftViewMode  = UITextField.ViewMode.always
        
        self.amount.leftView      = amountLeftView
        self.amount.leftViewMode  = UITextField.ViewMode.always
        
        self.commissionTxt.leftView      = commissionLeftView
        self.commissionTxt.leftViewMode  = UITextField.ViewMode.always
        
        self.totalAmountTxt.leftView      = totAmountLeftView
        self.totalAmountTxt.leftViewMode  = UITextField.ViewMode.always
        
        let amountRightView       = AmountRightView.updateView("act_speaker")
        amountRightView.delegate  = self
        self.amount.rightView     = amountRightView
        self.amount.rightViewMode = .always
        
        if let text = self.amount.text, text.count == 0 {
            amountRightView.hideClearButton(hide: true)
        }
        
        self.remarks.leftView      = remarkLeftView
        self.remarks.leftViewMode  = UITextField.ViewMode.always
        
        self.name.leftView      = merchantLeftView
        self.name.leftViewMode  = UITextField.ViewMode.always
        

        cnfRightView?.buttonClear.isHidden = true
        self.cnfMobileNumber.rightViewMode = .whileEditing
        self.cnfMobileNumber.rightView     = cnfRightView
        self.cnfMobileNumber.hideClear?.hideClearButton(hide: true)
        
    }
    
    // Amount Clear Field Delegate
    func amountClearButton() {
        self.amount.text = ""
        self.remarks.text = ""
        self.remarks.placeholder = "Enter Remarks".localized
        if let rightView = self.amount.rightView as? AmountRightView {
            rightView.hideClearButton(hide: true)
        }
        self.remarks.hide(); self.amount.placeholder = "Enter Amount".localized
        self.commissionTxt.text = ""
        self.totalAmountTxt.text = ""
        self.commissionTxt.hide(); self.totalAmountTxt.hide()
        self.burmeseAmount.hide()
    }
    
    func soundOffOn(sender: UIButton) {
        
    }
    
    @IBAction func generateQRAction(_ sender: UIButton) {
        let model: Dictionary<String, Any> = ["number": mobileNumber.text ?? "", "name": self.name.text ?? "", "merchant": "", "amount": amount.text ?? "", "commission": commissionTxt.text ?? "", "totalAmount": totalAmountTxt.text ?? "", "remark": remarks.text ?? ""]
        guard let cashInQR = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "CashInQRViewController") as? CashInQRViewController else { return }
        cashInQR.model = model
        self.navigationController?.pushViewController(cashInQR, animated: true)
    }
}

extension PaytoCashInViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactSearchedDict.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.00
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchingCell", for: indexPath)
        
        let dict = contactSearchedDict[indexPath.row]
        
        let name  = dict.safeValueForKey(ContactSuggessionKeys.contactname) as? String ?? "Unknown"
        let phone = dict.safeValueForKey(ContactSuggessionKeys.phonenumber_onscreen) as? String ?? "Unknown"
        
        cell.textLabel?.text = name
        println_debug(dict.safeValueForKey(ContactSuggessionKeys.operatorname) as? String)
        if let operatorName = dict.safeValueForKey(ContactSuggessionKeys.operatorname) as? String {
            if phone.hasPrefix("+95") || phone.first == "0" {
                cell.detailTextLabel?.text = phone + " " + "(\(operatorName))"
            } else {
                cell.detailTextLabel?.text = phone
            }
        } else {
            cell.detailTextLabel?.text = phone
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.hideContactSuggestion()
        
        let dict = self.contactSearchedDict[indexPath.row]
        let mobileNumber = dict[ContactSuggessionKeys.phonenumber_backend] as? String ?? ""
        let name = dict[ContactSuggessionKeys.contactname] as? String ?? "Unknown"
        
        var selectedNumber = ""
        
        if let number = mobileNumber.addingPercentEncoding(withAllowedCharacters: .decimalDigits) {
            let phones  = number.replacingOccurrences(of: "%20", with: "")
            let phones2 = phones.replacingOccurrences(of: "%C2%A0", with: "")
            if let final = phones2.removingPercentEncoding {
                selectedNumber = final
                let fav = FavModel.init("", phone: selectedNumber, name: name, createDate: "", agentID: "", type: "")
                self.remarks.text = ""
                self.contactUpgrade(number: selectedNumber, name: fav.name)
                self.hideContactSuggestion()
            }
        }
    }
}


extension PaytoCashInViewController: CountryLeftViewDelegate, CountryViewControllerDelegate, PTMultiSelectionDelegate, FavoriteSelectionDelegate, ContactPickerDelegate, ScrollChangeDelegate, PTTextFieldSubclassDelegate {
    func backspaceHandler(string: String, view: UITextField) {
        if view == self.mobileNumber {
            if self.mobileNumber.country.dialCode != "+95" {
                if view.text!.count < 5 {
                    self.hidelAllFields()
                    mobileNumber.hideClear?.hideClearButton(hide: true)
                } else {
                    cnfMobileNumber.hide()
                    self.hidelAllFields()
                }
            } else {
                let validations = PayToValidations().getNumberRangeValidation(string)
                if view.text!.count <= validations.min {
                    self.hidelAllFields()
                } else {
                    cnfMobileNumber.hide()
                    self.hidelAllFields()
                }
            }
            if view.text!.count <= 5 {
                self.hideContactSuggestion()
            }
            
        } else if view == self.cnfMobileNumber {
            self.hidelAllFields()
        }
        
        self.amount.text = ""
        self.remarks.text = ""
    }
    
    func response(text: String, view: UITextField) -> Bool {
        let validations = PayToValidations().getNumberRangeValidation(text)
        
        if view == self.mobileNumber {
            if validations.isRejected {
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                self.mobileNumber.hideClear?.hideClearButton(hide: true)
                return false
            }
            
            if text.count > 3 {
                if validations.max == validations.min {
                    self.loadContacts(txt: text, maxReached: (text.count >= validations.max))
                } else {
                    if text.count == validations.min {
                        self.loadContacts(txt: text, maxReached: true)
                    } else {
                        self.loadContacts(txt: text, maxReached: (text.count >= validations.max))
                    }
                }
            } else {
                let char = text.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) && (text.count == 0) {
                    mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                    self.hideContactSuggestion()
                    return false
                }
                self.hideContactSuggestion()
            }
            
            
            if self.mobileNumber.country.dialCode.hasPrefix("+95") {
                
                
                if text.count >= validations.min {
                    if self.mobileNumber.country.dialCode.hasPrefix("+95") {
                        
                        if validations.max == text.count {
                            self.cnfMobileNumber.show()
                            self.hidelAllFields()
                            self.cnfMobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                            self.mobileNumber.text = text
                            self.cnfMobileNumber.becomeFirstResponder()
                            self.hideContactSuggestion()
                            return false
                        }
                        if text.count > validations.max {
                            //self.cnfMobileNumber.becomeFirstResponder()
                            self.hideContactSuggestion()
                            return false
                        } else {
                            self.cnfMobileNumber.show()
                            self.hidelAllFields()
                            self.cnfMobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                        }
                    }
                    
                    return true
                } else {
                    self.cnfMobileNumber.hide()
                }
                
                if text.count > validations.max {
                    return false
                }
                
            } else {
                if text.count > 3 {
                    
                    if text.count >= 13 {
                        self.hideContactSuggestion()
                        if text.count <= 13 {
                            self.mobileNumber.text = text
                        }
                        self.cnfMobileNumber.becomeFirstResponder()
                        self.cnfMobileNumber.show()
                        self.cnfMobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                        return false
                    }
                    if text.count > 8 {
                        self.loadContacts(txt: text, maxReached: true)
                    } else {
                        self.loadContacts(txt: text)
                    }
                    self.cnfMobileNumber.show()
                    self.cnfMobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                    return true
                } else if text.count == 13 {
                    self.hideContactSuggestion()
                    self.mobileNumber.text = text
                    self.cnfMobileNumber.becomeFirstResponder()
                    return false
                } else if text.count > 13 {
                    self.hideContactSuggestion()
                    self.cnfMobileNumber.becomeFirstResponder()
                    return false
                }
            }
            
        } else if view == self.cnfMobileNumber {
            
            if !self.mobileNumber.text!.hasPrefix(text) {
                return false
            }
            
            if self.mobileNumber.text?.count == text.count, String(describing: self.mobileNumber.text!.last!) == String(describing:text.last!) {
                self.cnfMobileNumber.text = self.mobileNumber.text
                let agentCode = self.getAgentCode(numberWithPrefix: self.mobileNumber.text!, havingCountryPrefix: self.mobileNumber.country.dialCode)
                self.checkAgentCode(with: agentCode)
                let char = text.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) && (text.count == 0) {
                    self.cnfMobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                    return false
                }
            }
        }
        return true
    }
    
    
    func amountResponse(text: String, view: UITextField) -> Bool {
        if let rightView = view.rightView as? AmountRightView {
            if text.count > 0 {
                rightView.hideClearButton(hide: false)
            } else {
                rightView.hideClearButton(hide: true)
            }
        }
        
        if amountCheckingTimer != nil {
            amountCheckingTimer?.invalidate()
            amountCheckingTimer = nil
        }
        
        if text.hasPrefix("0"), text.count == 1 {
            return false
        }
        
        if text.count > 0 {
            self.generateQRBtn.isHidden = false
            self.amountCheckingTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(getCommissionForCICOFromApi), userInfo: view.text ?? "", repeats: false)
            self.remarks.show();
            self.amount.placeholder = "Receive Digital Money".localized
//            self.burmeseAmount.show()
            self.commissionTxt.show()
            self.totalAmountTxt.show()
            self.amount.placeholder = "Receive Digital Money".localized
            
        } else {
            self.generateQRBtn.isHidden = true
            self.amount.placeholder = "Enter Receive Digital Money".localized
            self.remarks.hide(); self.amount.placeholder = "Enter Receive Digital Money".localized
            self.commissionTxt.text = ""
            self.totalAmountTxt.text = ""
            self.commissionTxt.hide(); self.totalAmountTxt.hide()
            self.burmeseAmount.hide()
        }
        return true
    }
    
    //MARK:- CashIn and CashOut Commission Api
    @objc func getCommissionForCICOFromApi() {
        println_debug(self.amount.text)
        guard appDel.checkNetworkAvail() else {
            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.noInternet.localized)
            return
        }
        let url = getUrl(urlStr: "GetCashSettings", serverType: .cashInOutPayto)
        var paramsDict = Dictionary<String, Any>()
        paramsDict["PhoneNumber"] = UserModel.shared.mobileNo
        paramsDict["Amount"] = self.amount.text ?? "0"
        
        let params = self.JSONStringFromAnyObject(value: paramsDict as AnyObject)
        println_debug(paramsDict)
        TopupWeb.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", isCicoPayTo: true, handle: { [weak self](resp, success) in
            if success {
                DispatchQueue.main.async {
                    self?.parseMultiPaymentResult(data: resp)
                }
            }
        })
        
    }
    
    private func parseMultiPaymentResult(data: Any) {
        if let dict = data as? Dictionary<String,Any> {
            guard let success = dict.safeValueForKey("StatusCode") as? Int, success == 200 else { return }
            if let commissionDict = dict.safeValueForKey("Content") as? Dictionary<String, Any> {
                self.commissionTxt.text = self.amountFormatter(text: "\(commissionDict.safeValueForKey("Commission") as? Int ?? 0)")
                self.totalAmountTxt.text = self.amountFormatter(text: "\(commissionDict.safeValueForKey("TotalAmount") as? Int ?? 0)")
                _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(searchForKeyword(_:)), userInfo: self.totalAmountTxt.text ?? "", repeats: false)
                self.generateQRBtn.isHidden = false
            }
        }
    }
    
    @objc func searchForKeyword(_ timer: Timer) {
        
        if let twoMinutesRef =  self.totalAmountTxt.text  {
            
            if var uiNumber = self.mobileNumber.text {
                if uiNumber.hasPrefix("0") {
                    uiNumber = uiNumber.deletingPrefix("0")
                }
                
                uiNumber = getAgentCode(numberWithPrefix: self.mobileNumber.text ?? "", havingCountryPrefix: self.mobileNumber.country.dialCode)
                if !PaymentVerificationManager.isValidPaymentTransactions(uiNumber, twoMinutesRef) {
                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: nil, body: "Same Number with Same Amount can not be paid before 2 minutes".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                        alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                            self.amountClearButton()
                        })
                        alertViewObj.showAlert()
                        return
                    }
                }
            }
        }
        if var burmese =  totalAmountTxt.text  {
            burmese = burmese.replacingOccurrences(of: ",", with: "")
            if let ch = burmese.last, ch == "." {
                
            } else {
                let text = objectSound.getBurmeseSoundText(text: burmese)
                self.burmeseAmount.text = text
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.objectSound.playSound(fileName: "commissionAmount")
                    if var burmeseCommission = self.commissionTxt.text {
                        burmeseCommission = burmeseCommission.replacingOccurrences(of: ",", with: "")
                        if let ch = burmeseCommission.last, ch == "." {
                            
                        } else {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                let _ = self.objectSound.getBurmeseSoundText(text: burmeseCommission)
                            }
                        }
                    }
                }
            }
            self.burmeseAmount.placeholder = ""
            //self.delegate?.checkForSameNumberSameAmount()
        }
    }
    
    private func amountFormatter(text: String) -> String  {
        let mystring = text.replacingOccurrences(of: ",", with: "")
        let number = NSDecimalNumber(string: mystring)
        
        let formatter = NumberFormatter()
        
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        var num = formatter.string(from: number)
        if num == "NaN" {
            num = ""
        }
        return num ?? ""
    }
    
    func amountForCICO(text: Int?) {
        if let txt = text, txt == 0 {
            self.amount.placeholder = "Enter Receive Digital Money".localized
        } else {
            self.amount.placeholder = "Receive Digital Money".localized
        }
    }
    
    func openContacts(selection: ButtonType) {
        self.hideContactSuggestion()
        PaytoConstants.global.navigation = self.navigationController
        self.view.endEditing(true)
        switch selection {
        case .countryView:
            let nav = self.countryViewController(delegate: self)
            if let _ = PaytoConstants.global.navigation {
                PaytoConstants.global.navigation?.present(nav, animated: true, completion: nil)
            }
            break
        case .contactView:
            if let nav = PaytoConstants.global.navigation {
                if nav.viewControllers.contains(where: {$0.isKind(of: MultiPaytoViewController.self)}) {
                    PTManagerClass.openMultiContactSelection(self, hideScan: false)
                } else {
                    PTManagerClass.openMultiContactSelection(self, hideScan: true)
                }
            } else {
                PTManagerClass.openMultiContactSelection(self, hideScan: true)
            }
            
        }
    }
    
    func hideHideMySuggestionContacts() {
        
    }
    
    func mobClearfield() {
        
    }
    
    func hideContactSuggestionWhenEndEditing() {
        
    }
    
    func hideCnfMobileClear(view: UITextField) {
        
    }
    
    func selectedFavoriteObject(obj: FavModel) {
        self.amount.text = ""
        self.remarks.text = ""
        self.contactUpgrade(number: obj.phone, name: obj.name)
    }
    
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        self.amount.text = ""
        guard let numbers = contact.phoneNumbers.first else {
            self.hidelAllFields()
            self.amount.text = ""
            
            return
        }
        self.remarks.text = ""
        self.contactUpgrade(number: numbers.phoneNumber)
    }
    
    func hidelAllFields() {
        amount.hide()
        amount.text = ""
        commissionTxt.hide()
        commissionTxt.text = ""
        totalAmountTxt.hide()
        totalAmountTxt.text = ""
        remarks.hide()
        remarks.text = ""
    }
    
    func contactUpgrade(number: String, name: String = "Unknown", checkApi: Bool = true, amount: String? = nil, remark: String? = "") {
        
        
        let mobileObject = PTManagerClass.decodeMobileNumber(phoneNumber: number)
        
        if mobileObject.country.dialCode == "+95" {
            
            let numberValid = PayToValidations().getNumberRangeValidation(mobileObject.number)
            
            if numberValid.isRejected {
                self.hidelAllFields()
                self.amount.text = ""
                
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                return
            }
            
            if mobileObject.number.count < numberValid.min {
                self.hidelAllFields()
                self.amount.text = ""
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                return
            }
            
            if mobileObject.number.count > numberValid.max {
                self.hidelAllFields()
                self.amount.text = ""
                
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                return
            }
            
        } else {
            
            if  mobileObject.number.count > 13 {
                self.hidelAllFields()
                self.amount.text = ""
                
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                return
            }
        }
        
        let number = self.getAgentCode(numberWithPrefix: mobileObject.number, havingCountryPrefix: mobileObject.country.dialCode)
        if number == UserModel.shared.mobileNo {
            self.mobileNumber.clearText()
            //            self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
            self.hidelAllFields()
            self.amount.text = ""
            PaytoConstants.alert.showToast(msg: PaytoConstants.messages.ownMobileNumber.localized)
            return
        }
        if checkApi {
            self.checkAgentCode(with: number)
            self.mobileNumber.text = mobileObject.number
            self.mobileNumber.updateLeftView(country: mobileObject.country)
            self.cnfMobileNumber.updateLeftView(country: mobileObject.country)
            self.hidelAllFields()
            if let amt = amount, amt.count > 0 {
                if let rightView = self.amount.rightView as? AmountRightView {
                    rightView.hideClearButton(hide: false)
                }
                self.amount.text = amt
                //self.searchForKeyword(Timer())
                self.burmeseAmount.show()
            }
            self.amount.show()
            self.amount.becomeFirstResponder()
            self.remarks.text = remark
        } else {
            self.hidelAllFields()
        }
        
    }
    
    //MARK:- Country Delegates
    func countryViewController(_ list: CountryViewController, country: Country) {
        self.mobileNumber.updateLeftView(country: country)
        self.cnfMobileNumber.updateLeftView(country: country)
        self.mobileNumber.text = (country.code == "+95") ? "09" : ""
        self.cnfMobileNumber.text = (country.code == "+95") ? "09" : ""
        self.cnfMobileNumber.hide()
        self.amount.hide()
        self.commissionTxt.hide()
        self.totalAmountTxt.hide()
        self.remarks.hide()
        self.amount.text = ""
        self.commissionTxt.text = ""
        self.totalAmountTxt.text = ""
        self.remarks.text = ""
        list.dismiss(animated: true, completion: nil)
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: true, completion: nil)
    }
    
    func didSelectOption(option: PTMultiSelectionOption) {
        if let nav = self.navigationController {
            PaytoConstants.global.navigation = nav
        }
        switch option {
        case .favorite:
            let nav = openFavoriteFromNavigation(self, withFavorite: .payto, selectionType: true)
            if let _ = PaytoConstants.global.navigation {
                PaytoConstants.global.navigation?.present(nav, animated: true, completion: nil)
            }
            break
        case .contact:
            let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
            if let _ = PaytoConstants.global.navigation {
                PaytoConstants.global.navigation?.present(nav, animated: true, completion: nil)
            }
            break
        case .scanQR:
            guard let scanController = self.storyboard?.instantiateViewController(withIdentifier: "secondVC") as? ScanToPayViewController else { return }
            scanController.delegate = self
            if let nav = PaytoConstants.global.navigation {
                if nav.viewControllers.contains(where: {$0.isKind(of: MultiPaytoViewController.self)}) {
                    scanController.screen = .multi
                }
            }
            if let _ = PaytoConstants.global.navigation {
                PaytoConstants.global.navigation?.pushViewController(scanController, animated: true)
            }
            break
        }
    }
    
    func changeScrollPage(point: Int, withScanObject object: PTScanObject) {
        DispatchQueue.main.async {
                if (object.amount as NSString).floatValue > 0.00 {
                    if (object.amount.replacingOccurrences(of: ",", with: "") as NSString).floatValue > (UserLogin.shared.walletBal as NSString).floatValue {
                        self.amount.text = ""
                        PaytoConstants.alert.showToast(msg: PaytoConstants.messages.lessBalance.localized)
                        self.remarks.hide()
                        self.amount.placeholder = "Enter Amount".localized
                        self.burmeseAmount.hide()
                        self.remarks.text = ""
                        self.remarks.text = object.remarks
                        self.contactUpgrade(number: object.agentNo, name: object.name)
                    } else {
                        self.remarks.show(); self.amount.placeholder = "Amount".localized
                        self.burmeseAmount.show()
                        self.amount.text = object.amount
                        if object.remarks != "" {
                            self.remarks.placeholder = "Remarks".localized
                        }
                        self.remarks.text = ""
                        self.remarks.text = object.remarks
                        //self.searchForKeyword(Timer())
                        self.contactUpgrade(number: object.agentNo, name: object.name, amount: self.amount.text, remark: object.remarks)
                    }
                } else {
                    self.amount.text = ""
                    self.remarks.text = ""
                    self.contactUpgrade(number: object.agentNo, name: object.name)
                }
        }
        
    }
    
    func scanningFailed() {
        println_debug(scanningFailed)
    }
    
    
    func hideContactSuggestion() {
        self.contactSuggestion.isHidden = true
        self.contactTableView.isHidden  = true
        self.heightContactSuggestion.constant = 0.0
//        let yHeight = self.heightUpdates
//        self.delegate?.didChangeHeight(height: yHeight, withIndex: nil)
    }
    
    func showContactSuggestion() {
        self.contactSuggestion.isHidden = false
        self.contactTableView.isHidden  = false
        if self.contactSearchedDict.count == 1 {
            self.heightContactSuggestion.constant = 50.00
        } else if self.contactSearchedDict.count == 2 {
            self.heightContactSuggestion.constant = 100.00
        } else {
            self.heightContactSuggestion.constant = 150.00
        }
//        let yHeight = self.heightUpdates
//        self.delegate?.didChangeHeight(height: yHeight, withIndex: nil)
    }
    
    func loadContacts(txt: String, maxReached: Bool = false) {
        DispatchQueue.main.async {
            self.contactSuggesstionCompletion(number: txt) { (contacts) in
                DispatchQueue.main.async {
                    guard contacts.count > 0 else {
                        self.hideContactSuggestion()
                        self.heightContactSuggestion.constant = 0.00
                        return
                    }
                    self.contactSearchedDict = contacts
                    if maxReached {
                        self.hideContactSuggestion()
                        self.heightContactSuggestion.constant = 0.00
                    } else {
                        self.showContactSuggestion()
                    }
                    self.contactTableView.reloadData()
                }
            }
        }
    }
    

    
    func openAction(type: ButtonType) {
        self.hideContactSuggestion()
        if let nav = self.navigationController {
            PaytoConstants.global.navigation = nav
        }
        self.view.endEditing(true)
        switch type {
        case .countryView:
            let nav = self.countryViewController(delegate: self)
            if let _ = PaytoConstants.global.navigation {
                PaytoConstants.global.navigation?.present(nav, animated: true, completion: nil)
            }
            break
        case .contactView:
            if let _ = PaytoConstants.global.navigation {
                PTManagerClass.openMultiContactSelection(self, hideScan: false)
            } else {
                PTManagerClass.openMultiContactSelection(self, hideScan: true)
            }
            
            //            if favoriteManager.count(.payto) > 0 {
            //            } else {
            //                let nav = UitilityClass.openContact(multiSelection: false, self)
            //                PaytoConstants.global.navigation?.present(nav, animated: true, completion: nil)
            //            }
        }
    }
    
    func clearActionType() {
        self.amount.text = ""
        self.mobileNumber.clearText()
        self.mobileNumber.becomeFirstResponder()
        self.amount.text = ""
        self.contactSuggestion.isHidden = true
        self.heightContactSuggestion.constant = 0.0

    }
    
    
}

extension PaytoCashInViewController {
    func checkAgentCode(with agentCode: String) {
        
        if agentCode.hasPrefix(UserModel.shared.mobileNo) {
            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.ownMobileNumber.localized)
            self.mobileNumber.text = ""
            self.cnfMobileNumber.hide()
            return
        } else {
            self.cnfMobileNumber.endEditing(true)
        }
        
        guard appDelegate.checkNetworkAvail() else {
            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.noInternet.localized)
            return
        }
        
        let baseString = PaytoConstants.url.checkAgentCode.safelyWrappingString()
        
        let mobile = URLQueryItem.init(name: "MobileNumber", value: agentCode)
        let source = URLQueryItem.init(name: "SourceNumber", value: UserModel.shared.mobileNo)
        let transactionType = URLQueryItem.init(name: "TransType", value: "PAYTO")
        let appBuildNumber = URLQueryItem.init(name: "AppBuildNumber", value: buildNumber)
        let osType  = URLQueryItem.init(name: "OsType", value: "1")
        
        let itemsArray = [mobile, source, transactionType, appBuildNumber, osType]
        
        guard let url = PTManagerClass.createUrlComponents(base: baseString, queryItems: itemsArray) else {
            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.pleaseTryAgain.localized)
            return
        }
        
        TopupWeb.genericClass(url: url, param: Dictionary<String,Any>() as AnyObject, httpMethod: "GET") { (response, success) in
            if success {
                DispatchQueue.main.async {
                    self.formatAgentCodeResponse(response)
                }
            } else {
                PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.pleaseTryAgain.localized)
            }
        }
        
    }
    
    func formatAgentCodeResponse(_ response: Any) {
        guard let dictionary = response as? Dictionary<String,Any> else { return }
        if let code = dictionary.safeValueForKey("Code") as? Int, code != 200  {
            PaytoConstants.alert.showErrorAlert(title: nil, body: dictionary.safeValueForKey("Msg") as? String ?? "")
            self.hidelAllFields()
            self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
            return
        }
        guard let dataString = dictionary.safeValueForKey("Data") as? String else { return }
        guard dataString.count > 0 else { return }
        guard let dictionaryJson = OKBaseController.convertToDictionary(text: dataString) else { return }
        
        let agentModel = PTAgentModel(withDictionary: dictionaryJson)
        
        
        self.generalizingUIAgainst(agentModel: agentModel)
    }
    func noSubscriber(){
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: nil, body: "Subscriber Account cannot be used".localized, img:#imageLiteral(resourceName: "dashboard_pay_send.png") )
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func generalizingUIAgainst(agentModel model: PTAgentModel ) {
        
        
        if model.merchantCatName.safelyWrappingString().count > 0 {
        } else {
            
            if let nav = PaytoConstants.global.navigation {
                if nav.viewControllers.contains(where: {$0.isKind(of: MultiPaytoViewController.self)}) {
                        self.hidelAllFields()
                        self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                        //PaytoConstants.alert.showToast(msg: "Invalid OK$ Account".localized)
                        invalidNumber()
                        return
                    }
                }
        }
        
        
        self.name.show()
        self.name.placeholder = "Name".localized
        self.name.text = model.mercantName.safelyWrappingString()
        self.amount.show()
        
        if let amountText = self.amount.text, amountText.count > 0 {
            //self.burmeseAmount.show()
            self.remarks.show(); self.amount.placeholder = "Amount".localized
            self.generateQRBtn.isHidden = false
        } else {
            self.burmeseAmount.hide()
            self.remarks.hide(); self.amount.placeholder = "Enter Amount".localized
        }
        
        self.amount.becomeFirstResponder()
       
    }
    
    func invalidNumber(){
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: nil, body: "Invalid OK$ Account", img:#imageLiteral(resourceName: "dashboard_pay_send.png") )
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        }
    }
}


extension PaytoCashInViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}


class CashInTextFieldSubClass: SkyFloatingLabelTextField {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.lowerBorder()
        
        self.selectedTitleColor = UIColor.colorWithRedValue(redValue: 1, greenValue: 145, blueValue: 85, alpha: 1.0)
        
        if self.tag == 300 {
            self.show()
        } else {
            self.hide()
        }
        
    }
        
    func show() {
        self.updateConstraint(constant: 50.00)
        self.isHidden = false
    }
    
    func hide() {
        //self.text = ""
        self.updateConstraint(constant: 0.00)
        self.isHidden = true
    }
    
    private func updateConstraint(constant: CGFloat) -> Void {
        if let constraint = self.constraints.filter({$0.identifier == "heightConstraints"}).first {
            constraint.constant = constant
            self.layoutIfNeeded()
        }
    }
}


class CashInAmountValidation: CashInTextFieldSubClass {
    
}
