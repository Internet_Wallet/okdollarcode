//
//  PaytoNearByListViewController.swift
//  OK
//
//  Created by iMac on 6/14/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol SelectedAgentPayToProtocol: class {
    func selectedAgent(object: PTScanObject)
}

class PaytoNearByListViewController: OKBaseController {
    
    @IBOutlet weak var nearByAgentTableView: UITableView!
    var nearByServicesList = [NearByServicesNewModel]()
    weak var delegate: SelectedAgentPayToProtocol?
    
    @IBOutlet weak var nearbyAgentImageView: UIImageView!
    @IBOutlet weak var nearbyAgentBtn: UIButton!
    
    @IBOutlet weak var nearbyAgentView: UIView! {
        didSet {
            self.nearbyAgentView.layer.cornerRadius = 20.0
            self.nearbyAgentView.layer.masksToBounds = true
        }
    }
    
    @IBOutlet weak var nearbyAgentLbl: UILabel! {
        didSet {
            self.nearbyAgentLbl.font = UIFont(name: appFont, size: appFontSize)
            self.nearbyAgentLbl.textColor = .white
            self.nearbyAgentLbl.text = "Nearby OK $ Agents".localized
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}

extension PaytoNearByListViewController {
    func loadUI() {
        let image = UIImage(named: "downArrow")?.withRenderingMode(.alwaysTemplate).tintImage(tintColor: .white)
        self.nearbyAgentImageView.image = image
        
        let nib = UINib.init(nibName: "MapListCell", bundle: nil)
        self.nearByAgentTableView.register(nib, forCellReuseIdentifier: "maplistcellidentifier")
        self.nearByAgentTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.nearByAgentTableView.rowHeight = UITableView.automaticDimension
        self.nearByAgentTableView.estimatedRowHeight = 250
        self.nearByAgentTableView.delegate = self
        self.nearByAgentTableView.dataSource = self
        self.nearByAgentTableView.reloadData()
    }
    
    @objc func callBtnAction(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to: self.nearByAgentTableView)
        if let indexPath = self.nearByAgentTableView.indexPathForRow(at: buttonPosition), nearByServicesList.count >= 0 {
            let selectedpromotion = nearByServicesList[indexPath.row]
            let promotion = selectedpromotion
            if let phonenumber = promotion.UserContactData?.PhoneNumber {
                if phonenumber.count > 0 {
                    var mobNumber = getPhoneNumberByFormat(phoneNumber: phonenumber)
                    mobNumber = mobNumber.removingWhitespaces()
                    let phone = "tel://\(mobNumber)"
                    if let url = URL.init(string: phone) {
                        UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    }
                }
            }
        } else {
            alertViewObj.wrapAlert(title: nil, body: "No Phonenumber Exists".localized, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            alertViewObj.showAlert(controller: self)
        }
    }
    
    @IBAction func agentBtnAction(_ sender: UIButton) {
        PaytoConstants.global.navigation?.dismiss(animated: true, completion: nil)
    }
}

extension PaytoNearByListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let listCell = tableView.dequeueReusableCell(withIdentifier: "maplistcellidentifier", for: indexPath) as! MapListCell
        let promotion = nearByServicesList[indexPath.row]
        listCell.callBtn.addTarget(self, action: #selector(callBtnAction(_:)), for: UIControl.Event.touchUpInside)
        listCell.wrapData(promotion: promotion)
        return listCell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nearByServicesList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 250
        tableView.rowHeight = UITableView.automaticDimension
        return tableView.rowHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let promotion = nearByServicesList[indexPath.row]
        if let number = promotion.UserContactData?.PhoneNumber {
            let object = PTScanObject.init(amt: "", nme: promotion.UserContactData?.FirstName ?? "", agent: number, remark: "", cashInLimit: promotion.UserContactData?.CashInLimit, cashOutLimit: promotion.UserContactData?.CashOutLimit)
            self.delegate?.selectedAgent(object: object)
            PaytoConstants.global.navigation?.dismiss(animated: true, completion: nil)
        } else {
            PaytoConstants.alert.showToast(msg: "Mobile Number not available for this agent".localized)
        }
    }
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
