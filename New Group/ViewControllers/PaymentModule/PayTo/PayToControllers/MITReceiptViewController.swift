//
//  MITReceiptViewController.swift
//  OK
//
//  Created by Kethan Kumar on 9/10/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class MITReceiptViewController: OKBaseController {
    @IBOutlet weak var destinationLbl: UILabel!
    @IBOutlet weak var destinationVal: UILabel!
    @IBOutlet weak var okFeeLbl: UILabel!
    @IBOutlet weak var okFeeVal: UILabel!
    @IBOutlet weak var serviceFeeLbl: UILabel!
    @IBOutlet weak var serviceFeeVal: UILabel!
    @IBOutlet weak var transIdLbl: UILabel!
    @IBOutlet weak var transIdVal: UILabel!
    @IBOutlet weak var refNumLbl: UILabel!
    @IBOutlet weak var refNumVal: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var amountVal: UILabel!
    @IBOutlet weak var balanceLbl: UILabel!
    @IBOutlet weak var balanceVal: UILabel!
    @IBOutlet weak var dateVal: UILabel!
    @IBOutlet weak var timeVal: UILabel!
    @IBOutlet weak var mitQRImage: UIImageView!
    @IBOutlet weak var balanceView: UIView!
    var valueDictionary = Dictionary<String, Any>()
    
    //Tab
    var viewOptions: TabOptionsOnReceipt?
    @IBOutlet weak var viewTab: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let bView = Bundle.main.loadNibNamed("TabOptionsOnReceipt", owner: self, options: nil)?.first as? TabOptionsOnReceipt else { return }
        viewOptions = bView
        bView.delegate = self
        viewOptions?.frame = CGRect(x: 0, y: 0, width: viewTab.frame.size.width, height: viewTab.frame.size.height)
        if let _ = viewOptions {
            viewTab.addSubview(viewOptions!)
            viewOptions!.lblHome.text = "HomePagePT".localized
            viewOptions!.lblMore.text = "Share".localized
            viewOptions!.viewAddFav.isHidden = true
            viewOptions!.viewAddContact.isHidden = true
            viewOptions!.layoutIfNeeded()
        }
        self.updateUI()
    }
    
    private func updateUI() {
        self.destinationLbl.text = "Destination Number".localized
        self.okFeeLbl.text = "OK$ Fee".localized
        self.serviceFeeLbl.text = "Service Fee".localized
        self.transIdLbl.text = "Transaction ID".localized
        self.amountLbl.text = "Amount".localized
        self.balanceLbl.text = "Balance".localized
        self.refNumLbl.text = "Reference Number".localized
        
        self.destinationVal.text = self.changeMobFormat(valueDictionary["destination"] as? String ?? "")
        self.okFeeVal.attributedText = self.getAttributedMMK(amountStr: valueDictionary["okFee"] as? String ?? "0")
        self.serviceFeeVal.attributedText = self.getAttributedMMK(amountStr: valueDictionary["serviceFee"] as? String ?? "0")
        self.transIdVal.text = valueDictionary["transId"] as? String ?? ""
        self.amountVal.attributedText = self.getAttributedMMK(amountStr: valueDictionary["amount"] as? String ?? "0")
        self.balanceVal.attributedText = self.getAttributedMMK(amountStr: (valueDictionary["balance"] as? String ?? "0").replacingOccurrences(of: ".00", with: ""))
        if let date = valueDictionary["date"] as? String {
            if let dateFormat = date.components(separatedBy: " ").first?.dateValue(dateFormatIs: "dd-MMM-yyyy") {
                self.dateVal.text = dateFormat.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy")
            }
            self.timeVal.text = date.components(separatedBy: " ").last
        }
        self.refNumVal.text = valueDictionary["ReferenceNumber"] as? String ?? ""
        self.generateQRCode()
    }
    
    private func getAttributedMMK(amountStr: String) -> NSMutableAttributedString {
        let denomAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 15.0) ?? UIFont.systemFont(ofSize: 15), NSAttributedString.Key.foregroundColor: UIColor.red]
        let amntAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.0) ?? UIFont.systemFont(ofSize: 11), NSAttributedString.Key.foregroundColor: UIColor.black]
        let denomStr = NSMutableAttributedString(string: amountStr, attributes: denomAttr)
        let amountStr    = NSMutableAttributedString.init(string: " MMK", attributes: amntAttr)
        
        let amountAttr = NSMutableAttributedString()
        amountAttr.append(denomStr)
        amountAttr.append(amountStr)
        return amountAttr
    }
    
    private func generateQRCode() {
        var dateStr = ""
        if let date = valueDictionary["date"] as? String {
            if let dateFormat = date.components(separatedBy: " ").first?.dateValue(dateFormatIs: "dd-MMM-yyyy") {
                dateStr = dateFormat.stringValue(dateFormatIs: "yyyyMMdd")
            }
        }
        let encryptedStr = "\(UserModel.shared.mobileNo)|\(valueDictionary["destination"] as? String ?? "")|\(dateStr)|\(valueDictionary["transId"] as? String ?? "")|\(valueDictionary["ReferenceNumber"] as? String ?? "")|\(valueDictionary["amount"] as? String ?? "0")|V5POS"
        let qrImageObject = PTQRGenerator()
        let qrImage =  qrImageObject.getQRImage(stringQR: encryptedStr, withSize: 1)
        self.mitQRImage.image = qrImage
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.titleTextAttributes = PaytoConstants.global.attributesForTitle()
        self.title = "Receipt".localized
        self.navigationController?.navigationBar.backgroundColor = MyNumberTopup.OperatorColorCode.okDefault
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationBar.isHidden = false
    }
    
    private func home_recipt() {
        performSegue(withIdentifier: "unwindSegueToVCHme", sender: self)
    }
    
    private func share_recipt() {
        self.balanceLbl.isHidden = true
        self.balanceVal.isHidden = true
        self.balanceView.isHidden = true
        let imageShare = self.view.snapshotOfCustomeView
        self.balanceLbl.isHidden = false
        self.balanceVal.isHidden = false
        self.balanceView.isHidden = false
        let vc = UIActivityViewController(activityItems: [imageShare], applicationActivities: [])
        DispatchQueue.main.async {self.present(vc, animated: true)}
    }
    
    func changeMobFormat(_ number : String) -> String {
        var numberVal = ""
        if number.hasPrefix("0095") {
            let num = number.deletingPrefix("0095")
            numberVal = "(+95) 0" + num
        } else {
            let num = number.dropFirst(4)
            var country = String(number.prefix(4))
            if country.hasPrefix("00") {
                country = country.deletingPrefix("00")
                country = "(+\(country)) "
            }
            numberVal = country + num
        }
        return numberVal
    }
}


//MARK:- TabOptionProtocol
extension MITReceiptViewController: TabOptionProtocol {
    func addFavorite() {
        
    }
    
    func addContact() {
        
    }
    
    func goHome() {
        self.home_recipt()
    }
    
    func showMore() {
        self.share_recipt()
    }
}
