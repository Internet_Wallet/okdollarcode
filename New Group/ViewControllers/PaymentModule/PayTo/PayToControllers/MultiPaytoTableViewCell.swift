//
//  MultiPaytoTableViewCell.swift
//  OK
//
//  Created by Ashish on 8/10/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol MultiPaytoTableViewCellDelegate : class {
    func didDeleteCell(_ cell: MultiPaytoTableViewCell)
}

class MultiPaytoTableViewCell: UITableViewCell {
    
    weak var delegate : MultiPaytoTableViewCellDelegate?
    
    var indexPath : IndexPath?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    @IBOutlet weak var containerView : UIView!
    
    @IBOutlet weak var headerUpdate: UILabel!{
        didSet{
        self.headerUpdate.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBAction func deleteButtonAction(_ sender: UIButton) {
        guard let delegate = self.delegate else { return }
        delegate.didDeleteCell(self)
    }
    
    func updateHeader(_ header: String) {
        self.headerUpdate.text = header
    }
    
    func updateCell(_ update: UIView) {
        containerView.addSubview(update)
        update.translatesAutoresizingMaskIntoConstraints = false
        containerView.addConstraints([
            NSLayoutConstraint(item: update, attribute: .top, relatedBy: .equal, toItem: containerView, attribute: .top, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: update, attribute: .leading, relatedBy: .equal, toItem: containerView, attribute: .leading, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: update, attribute: .bottom, relatedBy: .equal, toItem: containerView, attribute: .bottom, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: update, attribute: .trailing, relatedBy: .equal, toItem: containerView, attribute: .trailing, multiplier: 1.0, constant: 0),
            ])
        self.layoutIfNeeded()
    }


}
