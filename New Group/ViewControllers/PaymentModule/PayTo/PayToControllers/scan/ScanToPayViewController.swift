//
//  ScanToPayViewController.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/27/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import PhotosUI
import AVFoundation
import AudioToolbox

struct PTScanObject {
    var amount  = ""
    var name    = "Unknown"
    var agentNo = ""
    
    init(amt: String, nme: String, agent: String) {
        self.amount  = amt
        self.name    = nme
        self.agentNo = agent
    }
}

protocol ScanObject_MultiPayDelegate {
    func navigateToMulti(obj: PTScanObject)
    func selfUpdateScan(obj: PTScanObject)
}


class ScanToPayViewController: PayToBaseViewController, AVCaptureMetadataOutputObjectsDelegate, PTIndicatorInfoProvider {
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PTPagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo.init(title: "Scan To Pay")
    }
    

    let systemSoundId : SystemSoundID = 1016
    
   weak  var delegate  : ScrollChangeDelegate?
    
    var mDelegate : ScanObject_MultiPayDelegate?
    
    var scanObj  : PTScanObject?
    
    var scanDad  : PayToViewController?
    
    var screen = MultiSelectionType.payto
    
    var popBackScreen : PaySendViewController?
    
    @IBOutlet weak var containerViews: UIView!
    @IBOutlet weak var buttonContainerView: CardDesignView!
    
    //captureSession manages capture activity and coordinates between input device and captures outputs
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    
    //Empty Rectangle with green border to outline detected QR or BarCode
    let codeFrame:UIView = {
        let codeFrame = UIView()
        codeFrame.layer.borderColor =  UIColor.init(hex: "F3C632").cgColor
        codeFrame.layer.borderWidth = 2.5
        codeFrame.frame = CGRect.zero
        codeFrame.translatesAutoresizingMaskIntoConstraints = false
        return codeFrame
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setPOPButton()
        
        self.title = "Scan QR Code"
        
        if self.scanDad != nil {
            println_debug("Scan Code Parent Found : Party")
        }
    }
    
    private func loadCamera() {
        DispatchQueue.global(qos: .background).async {
            //AVCaptureDevice allows us to reference a physical capture device (video in our case)
            DispatchQueue.main.async {
                self.okActivityIndicator(view: self.view)
            }
            let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
            if let captureDevice = captureDevice {
                do {
                    self.captureSession = AVCaptureSession()
                    // CaptureSession needs an input to capture Data from
                    let input = try AVCaptureDeviceInput(device: captureDevice)
                    self.captureSession?.addInput(input)
                    // CaptureSession needs and output to transfer Data to
                    let captureMetadataOutput = AVCaptureMetadataOutput()
                    self.captureSession?.addOutput(captureMetadataOutput)
                    //We tell our Output the expected Meta-data type
                    captureMetadataOutput.setMetadataObjectsDelegate(self , queue: DispatchQueue.main)
                    captureMetadataOutput.metadataObjectTypes = [.code128, .qr,.ean13, .ean8, .code39, .upce, .aztec, .pdf417] //AVMetadataObject.ObjectType
                    self.captureSession?.startRunning()
                    //The videoPreviewLayer displays video in conjunction with the captureSession
                    self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession!)
                    self.videoPreviewLayer?.videoGravity = .resizeAspectFill
                    DispatchQueue.main.async {
                        self.videoPreviewLayer?.frame = self.view.layer.bounds
                        self.view.layer.addSublayer(self.videoPreviewLayer!)
                        self.view.bringSubview(toFront: self.containerViews)
                        self.view.bringSubview(toFront: self.buttonContainerView)
                    }
                }
                catch {
                    println_debug("Error")
                    DispatchQueue.main.async {
                        self.removeActivityIndicator(view: self.view)
                    }
                }
            }
            DispatchQueue.main.async {
                self.removeActivityIndicator(view: self.view)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if AVCaptureDevice.authorizationStatus(for: .video) == AVAuthorizationStatus.authorized {
            self.loadCamera()
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted) in
                switch granted {
                case true:
                    self.loadCamera()
                case false:
                    self.requestAlertForPermissions(withBody: "OK$ needs camera permisson to Scan the QR Code", handler: { (cancelled) in
                        
                    })
                }
            })
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        captureSession?.stopRunning()
    }

    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects.count == 0 {
            println_debug("no objects returned")
            return
        }
        
        let metaDataObject = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        guard let StringCodeValue = metaDataObject.stringValue else {
            return
        }
        
        view.addSubview(codeFrame)

        guard let metaDataCoordinates = videoPreviewLayer?.transformedMetadataObject(for: metaDataObject) else {
            return
        }
        
        codeFrame.frame = metaDataCoordinates.bounds
        let feedbackGenerator = UIImpactFeedbackGenerator(style: .heavy)
        feedbackGenerator.impactOccurred()
        captureSession?.stopRunning()
        println_debug(StringCodeValue)
        DispatchQueue.main.async {
            self.scannedString(str: StringCodeValue)
        }
    }

    func scannedString(str: String) {
        

        _ = AESCrypt.decrypt(str.components(separatedBy: "---")[0], password: "m2n1shlko@$p##d")
        
        var sttt: String = ""
        println_debug(AESCrypt.decrypt(str, password: "m2n1shlko@$p##d"))
        if let stringQR = AESCrypt.decrypt(str, password: "m2n1shlko@$p##d") {
            sttt = stringQR
        } else {
            self.showToast(message: "Invalid QR Code")
        }
        
        if sttt != "" {
            
            if sttt.contains("α") {
                if sttt.contains("-1") {
                    sttt = ""
                }
                else {
                }
                if sttt.contains("`") {
                }
                else {
                    // sttt=@"";
                }
                println_debug("contains yes")
            }
            else {
                sttt = ""
            }
            
        }
        
        if(sttt.count>0){
            
//            var cellId: String = ""
            var businessName: String = ""
            var businessMobile: String = ""
            
            var myArray: [Any] = sttt.components(separatedBy: "#")
            _ = myArray[0] as! String
            
            var seconArray: [Any] = (myArray[1] as AnyObject).components(separatedBy: "-")
            businessName = seconArray[0] as! String
            
            var thirdArray: [Any] = (seconArray[1] as AnyObject).components(separatedBy: "@")
            businessMobile = thirdArray[0] as! String
            
            var fourthArray: [Any] = (thirdArray[1] as AnyObject).components(separatedBy: "&")
            let amt : String = fourthArray[0] as! String
            
            self.scanObj = PTScanObject.init(amt: amt, nme: businessName, agent: businessMobile)
            
            if screen == .payto {
                if let delegate = self.delegate {
                    delegate.changeScrollPage(point: 0, withScanObject: self.scanObj!)
                }
            } else if screen == .multi {
                screen = .payto
                if let delegate = mDelegate {
                    delegate.navigateToMulti(obj: self.scanObj!)
                    self.navigationController?.popViewController(animated: false)
                }
            } else if screen == .selfUpdate {
                screen = .payto
                if let delegate = mDelegate {
                    delegate.selfUpdateScan(obj: self.scanObj!)
                    self.navigationController?.popViewController(animated: false)
                }
            }
        } else {
             captureSession?.startRunning()
            return
        }
        codeFrame.removeFromSuperview()
        captureSession?.stopRunning()
    }
    
    @IBOutlet var flashImage: UIImageView!
    @IBAction func camera_flash(_ sender: UIButton) {
        if let device = AVCaptureDevice.default(for: AVMediaType.video) {
            if (device.hasTorch) {
                do {
                    try device.lockForConfiguration()
                    if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                        self.flashImage.isHighlighted = false
                        device.torchMode = AVCaptureDevice.TorchMode.off
                    } else {
                         self.flashImage.isHighlighted = true
                        do {
                            try device.setTorchModeOn(level: 1.0)
                        } catch {
                            println_debug(error)
                        }
                    }
                    device.unlockForConfiguration()
                } catch {
                    println_debug(error)
                }
            }
        }
    }

    @IBAction func scanFromGallery(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "QRPhotoScanner") as? QRPhotoScanner
        vc?.scanQRController = self
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    deinit {
        println_debug("deinit called scan qr code in payto")
    }

}
