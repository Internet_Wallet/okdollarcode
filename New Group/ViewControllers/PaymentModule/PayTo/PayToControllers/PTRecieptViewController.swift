//
//  PTRecieptViewController.swift
//  OK
//
//  Created by Ashish on 12/14/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI
import CoreLocation
import Rabbit_Swift

protocol DissmissDelegate: class {
    func paytoDismissEvent()
}

enum recieptType {
    case topupMyNumber, topupOtherNumber, payto, overseas
}

var CategoryStr: String = ""

class PTRecieptViewController: PayToBaseViewController, RecieptCellDelegate {

    //MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewTab: UIView!
    var businessNameNew = ""
    //MARK: - Properties
    var shownAlert: Bool? = false
    var transferToAlertShown: Bool = false
    //instapay
    var instaPay = false
    var instaRemarks = ""
    var agentModel: PTAgentModel?
    var requestMoney = false
    var cashInCashOut = ""
    
    var AccountTypeCheck = ""
    
    var instaAccount = [String]()
    //
    var viewModel = [PayToUIViewController]()
    var typeResponse = PayToType(type: .payTo)
    weak var delegate: DissmissDelegate?
    
    var viewOptions: TabOptionsOnReceipt?
    var cellA: [RecieptCell] = [RecieptCell]() {
        didSet {
            for (_,element) in cellA.enumerated() {
                let snapImage = element.snapshotOfCustomeView
                if let lastObject = self.cellA.last {
                    lastObject.viewRating.isHidden = true
                    lastObject.qrImageView.isHidden = false
                    lastObject.receiptCount.isHidden = true
                    UIImageWriteToSavedPhotosAlbum(lastObject.viewCard.snapshotOfCustomeView, nil, nil, nil)
                    lastObject.viewRating.isHidden = false
                    lastObject.qrImageView.isHidden = true
                    lastObject.receiptCount.isHidden = false
                }
                println_debug("category string-----\(CategoryStr)")
                if CategoryStr == "TICKET" || CategoryStr == "TOLL"  {
                    saveImageIntoDocument(finalImage: snapImage)
                }
            }
        }
    }
    var transactionDetails = [Dictionary<String,Any>]()
    var displayResponse = [Dictionary<String,Any>]()
    var recieptType: recieptType = .payto
    var printSnapShot: Int = 0
    let optionTextDefaultColor = UIColor.init(hex: "162D9F")
    var isInFavArray = [Bool]()
    var isInContactArray = [Bool]()
    var currentIndex = 0
    
    //MARK: - Views Method
    override func viewDidLoad() {
        // super.viewDidLoad()
        geoLocManager.startUpdateLocation()
        if #available(iOS 13.0, *) {
            self.changeCancelImplementation()
        }
        if transactionDetails.count > 1 {
            let payDetailsStr = "Payment".localized
            let size  = "\(payDetailsStr) \(transactionDetails.count)".size(withAttributes: [.font: UIFont.init(name: appFont, size: 13.0) ?? UIFont.systemFont(ofSize: 13.0)])
            let label = UILabel(frame: .init(origin: CGPoint.zero, size: size))
            label.textColor = .white
            label.font = UIFont.init(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: 17.0)
            label.text = "\(payDetailsStr) 1"
            self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: label)
        }
        
        for records in transactionDetails {
            let number   = records.safeValueForKey("destination") as? String ?? ""
            let type     = "PAYTO"
            var amount = "0"
            if self.typeResponse.type == .transferTo {
                if let model = self.viewModel.first {
                    amount   = model.totalAmountTxt.text?.replacingOccurrences(of: ",", with: "") ?? ""
                }
            } else {
                amount   = records.safeValueForKey("amount") as? String ?? ""
            }
            let model = PaymentTransactions.init(number, type: type, name: "Unknown", amount: amount, transID: "", bonus: "0.0")
            PaymentVerificationManager.storeTransactions(model: model)
            let isInFav = checkForFavorites(number, type: "PAYTO").isInFavorite
            let isInCont = checkForContact(number).isInContact
            isInFavArray.append(isInFav)
            isInContactArray.append(isInCont)
        }
        GetProfile.updateBalance()
        
        guard let bView = Bundle.main.loadNibNamed("TabOptionsOnReceipt", owner: self, options: nil)?.first as? TabOptionsOnReceipt else { return }
        viewOptions = bView
        bView.delegate = self
        viewOptions?.frame = CGRect(x: 0, y: 0, width: viewTab.frame.size.width, height: viewTab.frame.size.height)
        if let _ = viewOptions {
            viewTab.addSubview(viewOptions!)
            viewOptions!.lblHome.text = "HomePagePT".localized
            viewOptions!.lblMore.text = "More".localized
            viewOptions!.lblAddContact.text = "TabbarAddContact".localized
            viewOptions!.lblAddFav.text = "Add Favorite".localized
            
            viewOptions!.imgHome.tintColor = UIColor.gray
            viewOptions!.lblHome.textColor = optionTextDefaultColor
            
            viewOptions!.imgMore.tintColor = UIColor.gray
            viewOptions!.lblMore.textColor = optionTextDefaultColor
            
            viewOptions!.imgAddFav.tintColor = UIColor.darkGray
            viewOptions!.lblAddFav.textColor = optionTextDefaultColor
            
            viewOptions!.imgAddContact.tintColor = UIColor.darkGray
            viewOptions!.lblAddContact.textColor = optionTextDefaultColor
            
            
            
            if isInContactArray.count > 0 && isInContactArray[0] == true {
                addContactUI(isInContact: true)
            }
            if isInFavArray.count > 0 && isInFavArray[0] == true {
                addFavUI(isInFav: true)
            }
            viewOptions!.layoutIfNeeded()
        }
        if self.typeResponse.type == .cashIn || self.typeResponse.type == .cashOut || self.typeResponse.type == .transferTo || self.viewModel.first?.cashInCashOut != "" {
            self.updateCashInOutToServer()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.titleTextAttributes = PaytoConstants.global.attributesForTitle()
        if transactionDetails.count > 1 {
            self.title = "Multiple Payment".localized
        } else {
            self.title = "Receipt".localized
        }
        
        self.navigationController?.navigationBar.backgroundColor = MyNumberTopup.OperatorColorCode.okDefault
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.typeResponse.type == .transferTo {
            if let alertShow = shownAlert, alertShow == false {
                self.shownAlert = true
                let alertImage = UIImage(named: "dashboard_transfer_to_payto")
                alertViewObj.wrapAlert(title: "".localized, body: "Do you want to Check the Near By Merchant?".localized, img: alertImage)
                alertViewObj.addAction(title: "Cancel".localized, style: .cancel , action: {

                })
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    let scanPromotionObj = ["status": false, "beforePromotion": "", "afterPromotion": ""] as [String : Any]
                    UserDefaults.standard.set(scanPromotionObj, forKey: "ScanQRPromotion")
                    self.showNearByAgents()
                })
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:"OK$ would like to use your Current Location. Please turn on the Location Services for your device.".localized, img:#imageLiteral(resourceName: "map"))
        
        alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
            
        })
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: self.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            }
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }
    
    private func showNearByAgents() {
        println_debug("navigateToNearByOKServices")
        
        if(appDel.checkNetworkAvail()) {
            //Check location access
            
            if CLLocationManager.locationServicesEnabled() {
                switch(CLLocationManager.authorizationStatus()) {
                case .notDetermined:
                    println_debug("notDetermined")
                    loadNearByServicesView()
                    break
                case .denied :
                    println_debug("denied")
                    showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                    break
                case .restricted :
                    println_debug("restricted")
                    showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                    break
                case .authorizedAlways, .authorizedWhenInUse:
                    loadNearByServicesView()
                    break
                }
            } else {
                alertViewObj.wrapAlert(title:"", body: "Please Enable Location Service".localized, img: #imageLiteral(resourceName: "location"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    
                })
                DispatchQueue.main.async {
                    alertViewObj.showAlert(controller: self)
                }
            }
        } else {
            self.noInternetAlert()
        }
    }
    
    func loadNearByServicesView() {
        let story: UIStoryboard = UIStoryboard(name: "NBSMain", bundle: nil)
        let nearByOkServicesView = story.instantiateViewController(withIdentifier: "NearByServicesMainVC") as! NearByServicesMainVC
        nearByOkServicesView.statusScreen = "NearBy"
        nearByOkServicesView.isPushed = true
        self.navigationController?.pushViewController(nearByOkServicesView, animated: true)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        geoLocManager.stopUpdateLocation()
        self.navigationItem.hidesBackButton = false
    }
    
    //MARK:- CashInOutToServer
    private func updateCashInOutToServer() {
        guard appDel.checkNetworkAvail() else {
            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.noInternet.localized)
            return
        }
        let dict = self.transactionDetails[currentIndex]
        let url = getUrl(urlStr: "CompletePaymentForOkDollar", serverType: .cashInOutPayto)
        //        let paramsDict = self.getParamsForCashinCashout(dict: dict)
        guard let model = self.viewModel.first else { return }
        let request = CICOUpdateToServer(dict: dict, model: model, typeRes: self.typeResponse)
        
        let web      = WebApiClass()
        web.delegate = self
        let params = [String: String]() as AnyObject
        let encoder = JSONEncoder()
        do {
            let jsonData = try encoder.encode(request)
            web.genericClass(url: url, param: params , httpMethod: "POST", mScreen: "CICOUpdateReport", hbData: jsonData)
        } catch {
            PTLoader.shared.hide()
        }
        //        let paramsDict = []
        //
        //                TopupWeb.genericClass(url: url, param: paramsDict as AnyObject, httpMethod: "POST", isCicoPayTo: true, handle: { [weak self](resp, success) in
        //            if success {
        //                DispatchQueue.main.async {
        //                    self?.parseMultiPaymentResult(data: resp)
        //                }
        //            }
        //        })
    }
    
    private func getParamsForCashinCashout(dict: Dictionary<String, Any>) -> Dictionary<String,Any> {
        var paramsDict = Dictionary<String, Any>()
        paramsDict["senderPhoneNumber"] = UserModel.shared.mobileNo
        paramsDict["receiverPhoneNumber"] = dict.safeValueForKey("destination") as? String
        paramsDict["rechargeAmount"] = dict.safeValueForKey("amount") as? String
        paramsDict["okDollarTransactionId"] = dict.safeValueForKey("transid") as? String
        paramsDict["bonusPoints"] = dict.safeValueForKey("loyaltypoints") as? String
        paramsDict["cashBack"] = dict.safeValueForKey("kickvalue") as? String
        paramsDict["openingBalance"] = dict["prewalletbalance"] as? String ?? "0"
        paramsDict["closingBalance"] = dict["walletbalance"] as? String ?? "0"
        paramsDict["okDollarTransactionTime"] = dict.safeValueForKey("responsects") as? String ?? ""
        paramsDict["TotalAmount"]  = dict.safeValueForKey("amount") as? String ?? "0"
        if let model = self.viewModel.first {
            paramsDict["cashTransactionType"] = (model.cashInCashOut == "Cash In") ? 0 : 1
            paramsDict["commissionPercent"] = model.commissionTxt.text ?? ""
            paramsDict["comments"] = (model.cashInCashOut == "Cash In") ? "Sent Digital Money" : "Received Digital Money"
        }
        var type = "PAYTO"
        if self.typeResponse.transType == .payToID {
            type = "PAYTOID"
        }
        var receiverName: String? = nil
        if let com = dict.safeValueForKey("comments") as? String {
            let recieverMerchantName = com.components(separatedBy: "##")
            if recieverMerchantName.count >= 2 {
                let recieverName = recieverMerchantName[1]
                if recieverName == "Unregistered User" {
                    receiverName =  "Unknown"
                } else {
                    receiverName = recieverName
                }
            }
        } else {
            receiverName    =  dict.safeValueForKey("merchantname") as? String ?? ""
        }
        
        receiverName    =  dict.safeValueForKey("merchantname") as? String ?? ""
        paramsDict["paymentType"]  = type
        paramsDict["senderName"]  = UserModel.shared.name
        paramsDict["receiverName"]  = receiverName ?? ""
        paramsDict["receiverClosingBalance"]  = dict.safeValueForKey("unregipostbalance") as? String ??  "0"
        paramsDict["remark"]  = self.viewModel.first?.remarks.text ?? ""
        return paramsDict
    }
    
    private func parseMultiPaymentResult(data: Any) {
        if let dict = data as? Dictionary<String,Any> {
            guard let success = dict.safeValueForKey("StatusCode") as? Int, success == 200 else { return }
        }
    }
    
    //MARK: - Methods
    func addFavUI(isInFav: Bool) {
        if let _ = viewOptions {
            viewOptions?.imgAddFav.image = viewOptions?.imgAddFav.image?.withRenderingMode(.alwaysTemplate)
            if isInFav {
                viewOptions?.imgAddFav.tintColor = kYellowColor
                viewOptions!.lblAddFav.textColor = kYellowColor
            } else {
                viewOptions?.imgAddFav.tintColor = UIColor.darkGray
                viewOptions!.lblAddFav.textColor = optionTextDefaultColor
            }
            viewOptions!.layoutIfNeeded()
        }
    }
    
    func addContactUI(isInContact: Bool) {
        if let _ = viewOptions {
            viewOptions?.imgAddContact.image = viewOptions?.imgAddContact.image?.withRenderingMode(.alwaysTemplate)
            if isInContact {
                viewOptions?.imgAddContact.tintColor = kYellowColor
                viewOptions!.lblAddContact.textColor = kYellowColor
            } else {
                viewOptions?.imgAddContact.tintColor = UIColor.darkGray
                viewOptions!.lblAddContact.textColor = optionTextDefaultColor
            }
            viewOptions!.layoutIfNeeded()
        }
    }
    
    func snapAllRows() {
        for (_,element) in cellA.enumerated() {
            let snapImage = element.snapshotOfCustomeView
            println_debug("SnapShot Taken")
            UIImageWriteToSavedPhotosAlbum(snapImage, nil, nil, nil)
        }
    }
    
    func snapshotrow(cell: RecieptCell) {
        //get the cell
        guard let imageView = cell.snapshotView(afterScreenUpdates: true) else { return }
        let image = UIImage.init(view: imageView)
        UIImageWriteToSavedPhotosAlbum(image, self, nil, nil)
    }
    
    //MARK: PrabusnapshotstoreintoDocument
    func saveImageIntoDocument(finalImage: UIImage) {
        //////////////////
        //Sote image into document path with new Directory
        
        let filemgr = FileManager.default
        let dirPaths = filemgr.urls(for: .documentDirectory, in: .userDomainMask)
        let docsURL = dirPaths[0]
        let newDir = docsURL.appendingPathComponent("LocalCache").path
        do {
            try filemgr.createDirectory(atPath: newDir,
                                        withIntermediateDirectories: true, attributes: nil)
            let getImagePath = newDir.appending("/\(createUUID()).png")
            let theFileName = (getImagePath as NSString).lastPathComponent
            println_debug("sub path \(theFileName)")
            let checkValidation = FileManager.default
            if (checkValidation.fileExists(atPath: getImagePath))
            {
                //remove file as its already existed
                try!  checkValidation.removeItem(atPath: getImagePath)
            } else {
                //write file as its not available
                //  let imageData =  UIImageJPEGRepresentation(UIImage(named: "account_number.png")!, 1.0)
                let imageData =  finalImage.jpegData(compressionQuality: 1.0)
                try! imageData?.write(to: URL.init(fileURLWithPath: getImagePath), options: .atomicWrite)
                saveUserdefaults(imageName: theFileName , imageUrl: getImagePath)
            }
        } catch let error as NSError {
            println_debug("Error: \(error.localizedDescription)")
        }
    }
    
    func saveUserdefaults(imageName: String , imageUrl: String) {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let result = formatter.string(from: date)
        println_debug("Date string------\(result)")
        println_debug("Image name and url string------\(imageName)---\(imageUrl)")
        
        // println_debug("Date string------\(date)")
        let strval = TicketGallery(tearing: false, signature: false, phonegallery: false, timer: false, identify: false, timerdate: result, image: imageUrl)
        let kUserDefault = UserDefaults.standard
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: strval)
        kUserDefault.set(encodedData, forKey: imageName)
        kUserDefault.synchronize()
        retrieveUserdefaults(selectedImageName: imageName)
    }
    
    func retrieveUserdefaults (selectedImageName: String) {
        /////Retrieve stored object
        let defaultval = UserDefaults.standard
        let decoded  = defaultval.object(forKey: selectedImageName) as! Data
        if let object  = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? TicketGallery {
            userdefaultObject = object
        } else { return }
        
        //        println_debug("All values------>\(userdefaultObject.tearingstatus)--->\(userdefaultObject.signaturestatus)---\(userdefaultObject.phonegallerystatus)---\(userdefaultObject.timerstatus)---\(userdefaultObject.identifystatus)---\(userdefaultObject.timervalue)----\(userdefaultObject.imageUrl)")
        
    }
    
    func createUUID() -> String {
        let uuid = NSUUID().uuidString
        println_debug("UUDID----->\(uuid)")
        return uuid
    }
    
    func showToastlocal(message: String) {
        let toastLabel = UILabel()
        toastLabel.frame = CGRect(x: 15, y: 50, width: self.view.frame.width - 30 , height: 60)
        toastLabel.text = message
        toastLabel.numberOfLines = 0
        toastLabel.font = UIFont(name: appFont,size: 15.0)
        toastLabel.textAlignment = .center
        toastLabel.numberOfLines = 0
        toastLabel.layer.cornerRadius = 5
        toastLabel.layer.masksToBounds = true
        toastLabel.textColor = .white
        toastLabel.backgroundColor = UIColor.red
        self.view.bringSubviewToFront(toastLabel)
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 1.2, delay: 0.1, options: .curveEaseOut, animations: {
            //toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    fileprivate  func favorite_recipt() {
        currentIndex = (currentIndex == 0) ? 1 : currentIndex
        if let cell = self.getCellFromDisplay() {
            for (_,modelCell) in self.cellA.enumerated() {
                if cell == modelCell {
                    let favorite = FavoriteDBManager.init()
                    let contacts = favorite.fetchRecordsForFavoriteContactsEntity()
                    let dict = self.transactionDetails[currentIndex-1]
                    if contacts.filter({($0.phone.safelyWrappingString() == (dict.safeValueForKey("destination") as? String) ?? "" && ($0.type.safelyWrappingString() == (dict.safeValueForKey("responsetype") as? String) ?? ""))}).count > 0 {
                        PaytoConstants.alert.showToast(msg: "Contact  Already Added".localized)
                    } else {
                        let dict = self.transactionDetails[currentIndex-1]
                        let number = dict.safeValueForKey("destination") as? String
                        var nameValue = ""
                        
                        if let com = dict.safeValueForKey("comments") {
                            nameValue = (com as! String).components(separatedBy: "##")[1]
                            if nameValue == "Unregistered User" {
                                let msg = "Sorry, favorite not available for unregister number".localized
                                PaytoConstants.alert.showToast(msg: msg)
                                return
                            }
                        }
                        var name = ""
//                        if UitilityClass.getAccountType(accountType: self.AccountTypeCheck) != "Personal"{
//                            name   = dict.safeValueForKey("merchantname") as? String ?? "Unknown"//nameValue
//                        }
//                        else
//                        {
//                             name   = nameValue
//                        }
                        
                        if UitilityClass.getAccountType(accountType: self.AccountTypeCheck) != "Personal"{
                            
                            if self.AccountTypeCheck == "DUMMY"{
                               
                                    name = nameValue
                                
                            }else if self.AccountTypeCheck == "AGENT"{
                               
                                    name = nameValue
                            }
                            else{
                                name   = dict.safeValueForKey("merchantname") as? String ?? "Unknown"//nameValue
                            }
                            
                        }
                        else
                        {
                            name   = nameValue
                        }
                        let amount = dict.safeValueForKey("amount") as? String
                        var type = "PAYTO"
                        switch self.typeResponse.transType {
                        case .payTo:
                            type = "PAYTO"
//                            switch self.typeResponse.categoryType {
//                            case .entrance:
//                                type = "ENTRANCE"
//                            case .parking:
//                                type = "PARKING"
//                            case .fuel:
//                                type = "FUEL"
//                            case .restaurant:
//                                type = "RESTAURANT"
//                            case .shop:
//                                type = "SHOP"
//                            case .superMarket:
//                                type = "SUPERMARKET"
//                            default:
//                                break
//                            }
                        case .payToID:
                            type = "PAYTOID"
                        }
                        
                        
                       
                        let vc = self.addFavoriteController(withName: displayResponse[currentIndex-1].safeValueForKey("destinationName") as? String ?? "", favNum: number ?? "", type: type, amount: amount ?? "")
                        if let vcs = vc {
                            vcs.delegate = self
                            self.navigationController?.present(vcs, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    func didSelectRateOption(cell: RecieptCell, ratingDone: Bool) {
        if !ratingDone {
            let dict = transactionDetails[safe: cell.indexPath?.row ?? 0]
            let story = UIStoryboard.init(name: "Topup", bundle: nil)
            guard let ratingScreen = story.instantiateViewController(withIdentifier: String(describing: PaymentRateScreenViewController.self)) as? PaymentRateScreenViewController else { return }
            ratingScreen.modalPresentationStyle = .overCurrentContext
            ratingScreen.delegate = cell
            ratingScreen.confirmationCellScreen = ""
            ratingScreen.destinationNumber = (dict?.safeValueForKey("destination") as? String != nil) ? dict?.safeValueForKey("destination") as? String: ""
//            ratingScreen.modalPresentationStyle = .fullScreen
            self.navigationController?.present(ratingScreen, animated: true, completion: nil)
        } else {
            self.showToast(message: "Already feedback submitted".localized)
        }
    }
    
    fileprivate func getNameFrom(dict: [String: Any]) -> String {
        var nameValue = ""
        if let com = dict.safeValueForKey("comments") {
            let fullNameArr = (com as! String).components(separatedBy: "##")
            if fullNameArr[1] == "Unregistered User"{
                nameValue = ""
            } else {
                nameValue = fullNameArr[1]
            }
        }
        return nameValue
    }
    
    fileprivate  func addContact_recipt() {
        if #available(iOS 9.0, *) {
            guard let cell = self.getCellFromDisplay() else { return }
            for (index,modelCell) in self.cellA.enumerated() {
                if cell == modelCell {
                    let dict = self.transactionDetails[index]
                    let store     = CNContactStore()
                    let contact   = CNMutableContact()
                    let phone = dict.safeValueForKey("destination") as? String
                    let nameValue = getNameFrom(dict: dict)
                    let name  = dict.safeValueForKey("merchantname") as? String ?? nameValue
                    let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue: wrapFormattedNumber(phone ?? "") ))
                    contact.phoneNumbers = [homePhone]
                    contact.givenName = name
                    let controller = CNContactViewController.init(forNewContact: contact)
                    controller.contactStore = store
                    controller.delegate     = self
                    controller.title        = "Add Contact".localized
                    self.navigationController?.setNavigationBarHidden(false, animated: true)
                    self.navigationController?.navigationBar.isHidden = false
                    self.navigationController?.navigationBar.backgroundColor = UIColor.init(hex: "F3C632")
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }
        }
    }
    
    fileprivate func home_recipt() {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadNotificationVC"), object: nil)
        UserDefaults.standard.set(true, forKey: "popPayTo")
//        self.navigationController?.setNavigationBarHidden(false, animated: true)
//        self.navigationController?.navigationBar.isHidden = false
    //    self.navigationController?.popToRootViewController(animated: true)
  //      performSegue(withIdentifier: "unwindSegueToVC10", sender: self)
        performSegue(withIdentifier: "unwindSegueToVC10", sender: self)
        
    }
    
    fileprivate func more_recipt() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PTMoreViewController") as? PTMoreViewController
        addChild(vc!)
        vc?.isFromTopup = false
        vc?.view.frame = self.view.bounds
        vc?.didMove(toParent: self)
        vc?.delegate = self
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        
        vc?.view.layer.add(transition, forKey: nil)
        view.addSubview(vc!.view)
    }
}

//MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension PTRecieptViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecieptCell", for: indexPath) as! RecieptCell
        cell.layoutIfNeeded()
      //  cell.wrapData(dict: transactionDetails[indexPath.section], paytoType: self.typeResponse, accountType: self.viewModel[indexPath.row].accountType ?? "")
        
        cell.wrapData(dict: transactionDetails[indexPath.section], paytoType: self.typeResponse, accountType: self.instaAccount[indexPath.section],displayResponse: displayResponse[indexPath.section])
        
        cell.businessNameMer.text = displayResponse[indexPath.section].safeValueForKey("businessName") as? String ?? ""
        
        self.AccountTypeCheck = self.instaAccount[indexPath.section]
        
        if UitilityClass.getAccountType(accountType: self.instaAccount[indexPath.section]) != "Personal"{
            cell.starView.isHidden = true
            cell.rateService.isHidden = true
        }else{
            cell.starView.isHidden = false
            cell.rateService.isHidden = false
        }
        
        cell.wrapConditions(type: self.recieptType, payToType: self.typeResponse, cico: self.viewModel.first?.cashInCashOut ?? "")
        if transactionDetails.count > 1 {
            cell.wrapCount(txt: "\(indexPath.section + 1)")
        } else {
            cell.wrapCount(txt: "")
        }
        func catTypeUpdate(catTypeStr: String) {
            switch catTypeStr {
            case "restaurant":
                cell.forRestaurant()
                self.typeResponse.categoryType = .restaurant
            case "fuel":
                cell.forFeul()
                self.typeResponse.categoryType = .fuel
            case "shop":
                self.typeResponse.categoryType = .shop
            case "supermarket":
                self.typeResponse.categoryType = .superMarket
            case "entrance":
                self.typeResponse.categoryType = .entrance
            case "parking":
                self.typeResponse.categoryType = .parking
            default:
                break
            }
        }
        if instaPay {
            cell.forRemarks(str: instaRemarks)
            if let categoryType = self.agentModel?.localTransType {
                catTypeUpdate(catTypeStr: categoryType.lowercased())
            }
        } else {
            if let element = self.viewModel[safe: indexPath.section] {
                if let remark = element.remarks {
                    cell.forRemarks(str: remark.text ?? "")
                }
                if let type  = element.userType, let safeText = type.text?.lowercased() {
                    catTypeUpdate(catTypeStr: safeText)
                }
            }
        }
        if !instaPay, !requestMoney && self.typeResponse.type == .payTo {
            cell.forVehicle(model: self.viewModel[indexPath.section])
        }
        cell.forCashBack(str: transactionDetails[indexPath.section]["kickvalue"] as? String ?? "")
        cell.forBonusPoints(str: transactionDetails[indexPath.section]["loyaltypoints"] as? String ?? "")
        if let model = self.viewModel.first {
            if self.typeResponse.type == .cashIn || self.typeResponse.type == .cashOut || self.typeResponse.type == .transferTo || model.cashInCashOut != "" {
                cell.forCICOAmount(str: (model.amount.text ?? ""), commission: (model.commissionTxt.text ?? ""), totalAmount: (model.totalAmountTxt.text ?? ""), type: (model.cashInCashOut == "Cash In" || self.typeResponse.type == .transferTo) ? "Cash-In Amount" : "Cash-Out Amount")
                cell.forCICOCommssion(str: (model.commissionTxt.text ?? ""), type: (model.cashInCashOut == "Cash In") ? "Cash In Fee" : "Cash Out Fee")
                cell.forTotalAmount(str: model.totalAmountTxt.text ?? "", type: (model.cashInCashOut == "Cash In") ? "Cash In" : "Cash Out")
            }
        }
        cell.qrImageView.image = self.getQRImageForReceiptandPDF(from: transactionDetails[indexPath.section])
        cell.indexPath = indexPath
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if !self.cellA.contains(cell as! RecieptCell) {
            self.cellA.append(cell as! RecieptCell)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return transactionDetails.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.bounds.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
}

//MARK: - UIScrollViewDelegate
extension PTRecieptViewController: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let cell = self.getCellFromDisplay() {
            for (index,modelCell) in self.cellA.enumerated() {
                if cell == modelCell {
                    if let rCell = cell as? RecieptCell {
                        currentIndex = Int(rCell.receiptCount.text ?? "") ?? index
                    } else {
                        currentIndex = index
                    }
                    self.navigationItem.rightBarButtonItem = nil
                    let payDetailsStr = "Payment".localized
                    let size  = "\(payDetailsStr) \(transactionDetails.count)".size(withAttributes: [.font: UIFont.init(name: appFont, size: 13.0) ?? UIFont.systemFont(ofSize: 13.0)])
                    let label = UILabel(frame: .init(origin: CGPoint.zero, size: size))
                    label.textColor = .white
                    label.text = "\(payDetailsStr) \(currentIndex)"
                    label.font = UIFont.init(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: 17.0)
                    self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: label)
                    if currentIndex == 0 {
                        currentIndex = 1
                    }
                    if (currentIndex - 1) < isInFavArray.count && isInFavArray.count > 0 {
                        addFavUI(isInFav: isInFavArray[currentIndex - 1])
                    }
                    if (currentIndex - 1) < isInContactArray.count && isInContactArray.count > 0 {
                        addContactUI(isInContact: isInContactArray[currentIndex - 1])
                    }
                }
            }
        }
    }
}

//MARK: - CNContactViewControllerDelegate
extension PTRecieptViewController: CNContactViewControllerDelegate {

    func changeCancelImplementation() {
        let originalSelector = Selector(("editCancel:"))
        let swizzledSelector = #selector(CNContactViewController.cancelHack)
        if let originalMethod = class_getInstanceMethod(object_getClass(CNContactViewController()), originalSelector),
           let swizzledMethod = class_getInstanceMethod(object_getClass(CNContactViewController()), swizzledSelector) {
            method_exchangeImplementations(originalMethod, swizzledMethod)
        }
    }
    
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        if let newContact = contact {
            
            if isInContactArray.count == 1{
                
                if isInContactArray.indices.contains(currentIndex){
                    
                    if !isInContactArray[currentIndex]{
                        
                        isInContactArray[currentIndex] = true
                    }
                }else{
                    if !isInContactArray[0]{
                        
                        isInContactArray[0] = true
                    }

                }
                
                
            }else{
                for i in 0..<isInContactArray.count{
                    if !isInContactArray[i] {
                        isInContactArray[i] = true
                        break
                    }
                }
            }
            addContactUI(isInContact: true)
            okContacts.append(newContact)
            
            //if isInContactArray.count >= currentIndex {
             //   isInContactArray[currentIndex-1] = true
                
          //  }
        }
        viewController.delegate = nil
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - MoreControllerActionDelegate
extension PTRecieptViewController: MoreControllerActionDelegate {
    func actionFor(option: MorePaymentAction) {
        switch option {
        case .morePayment: self.morePayment()
        case .repeatPayment: self.repeatPayment()
        case .invoice: self.invoice()
        case .share: self.share()
        }
    }
    
    func morePayment() {
        if instaPay {
            NotificationCenter.default.post(name: NSNotification.Name("InstaPayMore"), object: nil, userInfo: nil)
            self.dismiss(animated: true, completion: nil)
        } else {
            UserDefaults.standard.set(true, forKey: "morePaymentPayto")
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.navigationController?.navigationBar.tintColor = .white
            self.navigationController?.popToRootViewController(animated: true)
            NotificationCenter.default.post(name: NSNotification.Name("ReloadNotificationVC"), object: nil, userInfo: nil)
        }
    }
    
    func repeatPayment() {
        if instaPay {
            NotificationCenter.default.post(name: NSNotification.Name("InstaPayRepeat"), object: nil, userInfo: nil)
            self.navigationController?.popToRootViewController(animated: true)
        } else {
        }
        if let cell = self.getCellFromDisplay() {
            for (index,modelCell) in self.cellA.enumerated() {
                if cell == modelCell {
                    let dict = self.transactionDetails[index]
                    var number =  dict.safeValueForKey("destination") as? String
                    let operatorVal =  dict.safeValueForKey("operator") as? String
                    if operatorVal?.count ?? 0 > 0 {
                        number =  dict.safeValueForKey("operator") as? String
                    }
                    UserDefaults.standard.set(number, forKey: "stringPaymentPayto")
                    
                    var recieverName = ""
                   // let accountType = ""
                    let comments = dict.safeValueForKey("comments") as? String ?? ""
                    let recieverMerchantName = comments.components(separatedBy: "##")
                    
                    if UitilityClass.getAccountType(accountType: self.AccountTypeCheck) != "Personal"{
                        
                        if self.AccountTypeCheck == "DUMMY"{
                            if recieverMerchantName.count>0 && recieverMerchantName.indices.contains(1){
                                recieverName = recieverMerchantName[1]
                            }
                        }else if self.AccountTypeCheck == "AGENT"{
                            if recieverMerchantName.count>0 && recieverMerchantName.indices.contains(1){
                                recieverName = recieverMerchantName[1]
                            }
                        }
                        else{
                            recieverName = dict.safeValueForKey("merchantname") as? String ?? ""
                        }
                        
                    }
                    else
                    {
                        recieverName = recieverMerchantName[1]
                    }
                    
                    
                    
                    let nameValue = recieverName//getNameFrom(dict: dict)
                    UserDefaults.standard.set(nameValue, forKey: "PaytoName")
                    UserDefaults.standard.set(true, forKey: "repeatPaymentPayto")
                    self.navigationController?.setNavigationBarHidden(false, animated: true)
                    self.navigationController?.navigationBar.tintColor = .white
                    self.navigationController?.popToRootViewController(animated: true)
                    NotificationCenter.default.post(name: NSNotification.Name("ReloadNotificationVCRepeat"), object: nil, userInfo: nil)
                }
            }
        }
    }
    
    func getCellFromDisplay() -> UICollectionViewCell? {
        let visibleCell = CGRect.init(origin: self.collectionView.contentOffset, size: self.collectionView.bounds.size)
        let visiblePoint = CGPoint.init(x: visibleCell.midX, y: visibleCell.midY)
        let indexPath = self.collectionView.indexPathForItem(at: visiblePoint)
        if let path = indexPath {
            let cell = self.collectionView.cellForItem(at: path)
            return cell
        }
        return nil
    }
    
    func getQRImageForReceiptandPDF(from dict: [String: Any]) -> UIImage? {
        var phNumber = ""
        if instaAccount.count>0{
            if instaAccount[0] == "DUMMY"{
              phNumber   = dict.safeValueForKey("operator") as? String ?? ""
            }else{
                phNumber = dict.safeValueForKey("destination") as? String ?? ""
            }
        }else{
            phNumber = dict.safeValueForKey("destination") as? String ?? ""
        }
        
       
        var transDateStr = ""
        if let trDateStr = dict.safeValueForKey("responsects") as? String {
            if let trDateValue = trDateStr.dateValue(dateFormatIs: "d-MMM-yyyy HH:mm:ss") {
                transDateStr = trDateValue.stringValue(dateFormatIs: "dd/MMM/yyyy HH:mm:ss")
            }
        }
        let senderName = UserModel.shared.name
        let senderNumber = UserModel.shared.mobileNo
        var receiverName: String? = nil
        var receiverBusinessName = dict.safeValueForKey("merchantname")
        if let com = dict.safeValueForKey("comments") as? String {
            let rNameArray = com.components(separatedBy: "##")
            if rNameArray.count >= 2 {
                if rNameArray[2] != "" {
                    receiverName = rNameArray[2]
                } else {
                    receiverBusinessName = rNameArray[1]
                }
            } else if rNameArray.count >= 1 {
                receiverBusinessName = rNameArray[1]
            }
        }
        
        let receiverNumber = phNumber
        let firstPartToEncrypt = "\(transDateStr)----\(phNumber)"
        let transID = dict.safeValueForKey("transid") ?? ""
        let trasnstype = dict.safeValueForKey("responsetype") ?? ""
        let lattitude = geoLocManager.currentLatitude ?? ""
        let longitude = geoLocManager.currentLongitude ?? ""
        let firstPartEncrypted = AESCrypt.encrypt(firstPartToEncrypt, password: "m2n1shlko@$p##d")
        let amount = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: (dict.safeValueForKey("amount") as? String ?? "0")) + " MMK"
        let state = UserModel.shared.state
        let gender = (UserModel.shared.gender == "1") ? "M" : "F"/////
        let age = UserModel.shared.ageNow /////
        let balance = "" /////
        let cashBackAmount = dict.safeValueForKey("kickvalue") as? String /////
        let bonus = dict.safeValueForKey("loyaltypoints") as? String /////
        
        let str2 = "OK-\(senderName)-\(amount)-\(transDateStr)-\(transID)-\(senderNumber)-\(balance)-des-\(receiverNumber)-\(trasnstype)-\(cashBackAmount ?? "")-\(bonus ?? "")-\(lattitude),\(longitude)-0-\(gender)-\(age)-\(state)-\(receiverName ?? "")-\(receiverBusinessName ?? "")"
        
        let secondEncrypted = AESCrypt.encrypt(str2, password: firstPartToEncrypt)
        
        var firstPartEncryptedSafeValue = ""
        var secondEncryptedSafeValue = ""
        if let firstEncrypted = firstPartEncrypted {
            firstPartEncryptedSafeValue = firstEncrypted
        }
        if let secondEncrypt = secondEncrypted {
            secondEncryptedSafeValue = secondEncrypt
        }
        let finalEncyptFormation = "\(firstPartEncryptedSafeValue)---\(secondEncryptedSafeValue)"
        
        let qrImageObject = PTQRGenerator()
        let qrImage =  qrImageObject.getQRImage(stringQR: finalEncyptFormation, withSize: 1)
        return qrImage
    }
    
    func changeMobFormat(_ number : String) -> String {
        var numberVal = ""
        if number.hasPrefix("0095") {
            let num = number.deletingPrefix("0095")
            numberVal = "(+95) 0" + num
        } else {
            let num = number.dropFirst(4)
            var country = String(number.prefix(4))
            if country.hasPrefix("00") {
                country = country.deletingPrefix("00")
                country = "(+\(country)) "
            }
            numberVal = country + num
        }
        return numberVal
    }
    
    func append09Format(_ dest: String) -> String {
        if dest.hasPrefix("0095") {
            var number = dest.deletingPrefix("00")
            number = "+" + number
            let country = identifyCountry(withPhoneNumber: number)
            let final = number.replacingOccurrences(of: country.countryCode, with: "0")
            return final
        } else {
            if dest.hasPrefix("00") {
                var number = dest.deletingPrefix("00")
                number = "+" + number
                let country = identifyCountry(withPhoneNumber: number)
                let final = number.replacingOccurrences(of: country.countryCode, with: "(\(country.countryCode))")
                return final
            }
            return ""
        }
    }
    
    func invoice() {
        //   Passing dictionary values:
        if let cell = self.getCellFromDisplay() {
            for (index,modelCell) in self.cellA.enumerated() {
                if cell == modelCell {
                    let dict = self.transactionDetails[index]
                    println_debug(dict)
                    var pdfDictionary = Dictionary<String,Any>()
                    // pdfDictionary["businessName"]       = dict.safeValueForKey("businessName") ?? dict.safeValueForKey("merchantname") ?? "Unknown" as String
                    let number = self.changeMobFormat(UserModel.shared.mobileNo)
//                    _ = number.remove(at: String.Index.init(encodedOffset: 0))
//                    _ = number.remove(at: String.Index.init(encodedOffset: 0))
//                    number  = "+" + number
//                    number  = number.replacingOccurrences(of: "+95", with: "(+95) 0")
                    
//                    let amount = (dict.safeValueForKey("amount") as? String ?? "0") + " MMK"
                    
//                    var dest = dict.safeValueForKey("destination") as? String ?? ""
//                    _ = dest.remove(at: String.Index.init(encodedOffset: 0))
//                    _ = dest.remove(at: String.Index.init(encodedOffset: 0))
//                    dest  = "+" + dest
//                    dest  = dest.replacingOccurrences(of: "+95", with: "(+95) 0")
                    
                    let dest = self.changeMobFormat(dict.safeValueForKey( "destination") as? String ?? "")
                    
                    pdfDictionary["senderAccName"]      = UserModel.shared.name
                    pdfDictionary["senderAccNo"]        = number
                    
                    //var fullNameArr = ""
                    let nameValue: String = ""
                    let value = dict.safeValueForKey("merchantname")
                    var receiverName: String? = nil
                    if let com = dict.safeValueForKey("comments") as? String {
                        let recieverMerchantName = com.components(separatedBy: "##")

                        if recieverMerchantName.count >= 2 {
                            let recieverName = recieverMerchantName[1]
                            let recieverMerchantName = (recieverMerchantName.count > 1) ? recieverMerchantName[2] : ""
                            if recieverName == "Unregistered User" {
                                pdfDictionary["receiverAccName"] =  "Unregistered User"
                            } else {
                                
                                if UitilityClass.getAccountType(accountType:self.AccountTypeCheck) != "Personal"{
                                    if instaAccount.count>0{
                                        if instaAccount[0] == "DUMMY"{
                                            
                                           pdfDictionary["receiverAccName"] = displayResponse[index].safeValueForKey("destinationName") as? String ?? "" //recieverName
                                        }else{
                                            pdfDictionary["receiverAccName"] = displayResponse[index].safeValueForKey("destinationName") as? String ?? "" //recieverName//dict.safeValueForKey("merchantname") as? String ?? ""
                                        }
                                    }else{
                                        pdfDictionary["receiverAccName"] = displayResponse[index].safeValueForKey("destinationName") as? String ?? "" //recieverMerchantName
                                    }
                                }else{
                                    pdfDictionary["receiverAccName"] = displayResponse[index].safeValueForKey("destinationName") as? String ?? "" //recieverName
                                }
                                
//                                if recieverMerchantName == "" {
//
//                                } else {
//
//                                }
                            }
                        }
                    } else {
                        if value != nil {
                            receiverName    =  dict.safeValueForKey("merchantname") as? String ?? nameValue
                        } else {
                            receiverName   =  nameValue
                        }
                        pdfDictionary["receiverAccName"] = displayResponse[index].safeValueForKey("destinationName") as? String ?? "" //receiverName
                    }
                    
                    if instaAccount.count>0{
                        if instaAccount[0] == "DUMMY"{
                        pdfDictionary["receiverAccNo"]  = self.changeMobFormat(dict.safeValueForKey( "operator") as? String ?? "")
                        }else{
                            pdfDictionary["receiverAccNo"]      = dest
                        }
                    }else{
                        pdfDictionary["receiverAccNo"]      = dest
                    }
                    
                    
                    pdfDictionary["transactionID"]      = dict.safeValueForKey("transid") ?? ""
                    
                    func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
                        
                        let inputFormatter = DateFormatter.init()
                        inputFormatter.dateFormat = "d-MMM-yyyy HH:mm:ss"
                        inputFormatter.setLocale()
                        if let date = inputFormatter.date(from: dateString) {
                            
                            let outputFormatter = DateFormatter.init()
                            outputFormatter.dateFormat = format
                            outputFormatter.setLocale()
                            return outputFormatter.string(from: date)
                        }
                        
                        return nil
                    }
                    let transactionDate = formattedDateFromString(dateString: dict.safeValueForKey("responsects") as? String ?? "", withFormat: "EEE, d-MMM-yyyy HH:mm:ss") ?? ""
                    pdfDictionary["transactionDate"]    = transactionDate
                    pdfDictionary["amount"]           = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: (dict.safeValueForKey("amount") as? String ?? "0"))
                    if !instaPay, !requestMoney && self.typeResponse.type == .payTo {
                        pdfDictionary["transactionType"]    = (self.viewModel[index].addVehicle.text != "") ? "Fuel" : dict.safeValueForKey("responsetype") ?? ""
                    } else {
                        pdfDictionary["transactionType"]    =  dict.safeValueForKey("responsetype") ?? ""
                    }
                    
                    if self.typeResponse.type == .cashIn || self.typeResponse.type == .cashOut || self.typeResponse.type == .transferTo || self.viewModel.first?.cashInCashOut != "" {
                        if let model = self.viewModel.first {
                            pdfDictionary["invoiceTitle"]       = (model.cashInCashOut == "Cash In") ? "Sent Digital Money Transaction Receipt".localized : "Receive Digital Money Transaction Receipt".localized
                            if self.typeResponse.type == .transferTo || model.cashInCashOut == "Cash In" {
                                pdfDictionary["paymentType"]    = "Cash In"
                            } else if self.typeResponse.type == .cashOut {
                                pdfDictionary["paymentType"]    = "Cash Out"
                            }
                            pdfDictionary["cicoAmount"] = model.amount.text ?? "0"
                            pdfDictionary["commissionCICO"] = model.commissionTxt.text ?? "0"
                            pdfDictionary["totalAmount"] = model.totalAmountTxt.text ?? "0"
                        }
                    } else {
                        pdfDictionary["invoiceTitle"]       = "Transaction Receipt".localized
                    }
                    pdfDictionary["logoName"]           = "appIcon_Ok"
                    //For qr scan
                    pdfDictionary["qrImage"] = self.getQRImageForReceiptandPDF(from: dict)
                    guard let pdfUrl = CodeSnippets.createInvoicePDF(dictValues: pdfDictionary, pdfFile: "OK$ Receipt-Invoice") else {
                        println_debug("Error - pdf not generated")
                        return
                    }
                    var pdfUrlWithCashback: URL?
                    if let cashBack = dict.safeValueForKey("kickvalue") as? String {
                        if cashBack != "0.0", cashBack.count > 0 {
                            let cashBackValue = "\(cashBack.replacingOccurrences(of: ".00", with: "")) MMK"
                            pdfDictionary["cashback"] = cashBackValue
                            guard let withCashback = CodeSnippets.createInvoicePDF(dictValues: pdfDictionary, pdfFile: "OK$ Receipt-Invoice") else {
                                println_debug("Error - pdf not generated")
                                return
                            }
                            pdfUrlWithCashback = withCashback
                        } else {
                            pdfUrlWithCashback = pdfUrl
                        }
                    } else {
                        pdfUrlWithCashback = pdfUrl
                    }
                    let story = UIStoryboard.init(name: "Topup", bundle: nil)
                    guard let vc = story.instantiateViewController(withIdentifier: "SharePDFViewController") as? SharePDFViewController else { return }
                    vc.url = pdfUrl
                    vc.urlWithCashBack = pdfUrlWithCashback
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    func share() {
        if let cell = self.getCellFromDisplay() as? RecieptCell {
            cell.viewRating.isHidden = true
            cell.qrImageView.isHidden = false
            cell.receiptCount.isHidden = true
            cell.balance.isHidden = true
            cell.balance_val.isHidden = true
            cell.mainBalHeight.constant = 0
            let frame = cell.frame
            cell.frame = CGRect(x: cell.frame.origin.x, y: cell.frame.origin.y, width: cell.frame.width, height: cell.frame.height + 80)
            let imageShare = cell.snapshotOfCustomeView
            cell.frame = frame
            cell.viewRating.isHidden = false
            cell.qrImageView.isHidden = true
            cell.receiptCount.isHidden = false
            cell.balance.isHidden = false
            cell.balance_val.isHidden = false
            cell.mainBalHeight.constant = 35.0
            
            let vc = UIActivityViewController(activityItems: [imageShare], applicationActivities: [])
            DispatchQueue.main.async {self.present(vc, animated: true)}
        }
    }
}

//MARK:- TabOptionProtocol
extension PTRecieptViewController: TabOptionProtocol {
    func addFavorite() {
        if let _ = viewOptions {
            if viewOptions!.lblAddFav.textColor != kYellowColor {
                self.favorite_recipt()
            } else {
                PaytoConstants.alert.showToast(msg: "Contact already added in Favorites".localized)
            }
        }
    }
    
    func addContact() {
        if let _ = viewOptions {
            if viewOptions!.lblAddContact.textColor != kYellowColor {
                self.addContact_recipt()
            } else {
                PaytoConstants.alert.showToast(msg: "Contact Already Added".localized)
            }
        }
    }
    
    
    
    func goHome() {
        if let promotionObj = UserDefaults.standard.value(forKey: "ScanQRPromotion") as? [String: Any] {
            if let status = promotionObj["status"] as? Bool, status == true {
                if let afterPromoText = promotionObj["afterPromotion"] as? String, afterPromoText != "" {
                    self.showScanQRAlert(msg: afterPromoText)
                } else {
                    self.home_recipt()
                }
            } else {
                self.home_recipt()
            }
        } else {
            self.home_recipt()
        }
    }
    
    func showMore() {
        self.more_recipt()
    }
    
    private func showScanQRAlert(msg: String) {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "".localized, body: msg.localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                let scanPromotionObj = ["status": false, "beforePromotion": "", "afterPromotion": ""] as [String : Any]
                UserDefaults.standard.set(scanPromotionObj, forKey: "ScanQRPromotion")
                self.home_recipt()
            })
            alertViewObj.showAlert(controller: self)
        }
    }
}

//MARK:- PTFavoriteActionDelegate
extension PTRecieptViewController: PTFavoriteActionDelegate {
    func favoriteAdded() {
        if isInFavArray.count >= currentIndex {
        isInFavArray[currentIndex-1] = true
        addFavUI(isInFav: true)
        }
    }
}

protocol RecieptCellDelegate: class {
    func didSelectRateOption(cell: RecieptCell, ratingDone: Bool)
}

//MARK: - RecieptCell
class RecieptCell: UICollectionViewCell, PaymentRateDelegate {
    
    //MARK: - Outlets
    @IBOutlet weak var nameLbl: UILabel!{
        didSet{
            nameLbl.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var receiptCount: UILabel!{
        didSet{
            receiptCount.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var businessName: UILabel!{
        didSet{
            businessName.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var paymentCat: UILabel!{
        didSet{
            paymentCat.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var patmentCategoryHeight: NSLayoutConstraint!
    @IBOutlet weak var balance: UILabel!{
        didSet{
            balance.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var viewCard: CardDesignView!
    var ratingDone = false
    @IBOutlet weak var transactId: UILabel!
        {
        didSet
        {
            transactId.font = UIFont(name: appFont, size: appTitleButton)
            transactId.text = "Transaction ID".localized
        }
    
    }
    @IBOutlet weak var transactType: UILabel!{
        didSet
        {
            transactType.font = UIFont(name: appFont, size: appTitleButton)
        }
    
    }
    @IBOutlet weak var rateService: UILabel!
        {
        didSet
        {
            rateService.font = UIFont(name: appFont, size: appTitleButton)
            rateService.text = "Rate OK$ Services".localized
        }
    }
    @IBOutlet weak var date_Val: UILabel!{
        didSet
        {
            date_Val.font = UIFont(name: appFont, size: appTitleButton)
        }
    
    }
    @IBOutlet weak var time_Val: UILabel!{
        didSet
        {
            time_Val.font = UIFont(name: appFont, size: appTitleButton)
        }
    
    }
    @IBOutlet weak var bName_Val: UILabel!{
        didSet
        {
           // bName_Val.font = UIFont(name: appFont, size: appFontSize)
        }
    
    }
    @IBOutlet weak var PaymentCt_Val: UILabel!{
        didSet
        {
          //  PaymentCt_Val.font = UIFont(name: appFont, size: appFontSize)
        }
    
    }
    @IBOutlet weak var balance_val: UILabel!{
        didSet
        {
            balance_val.font = UIFont(name: appFont, size: appTitleButton)
        }
    
    }
    @IBOutlet var transact_Val: UILabel!{
        didSet
        {
            transact_Val.font = UIFont(name: appFont, size: appTitleButton)
        }
    
    }
    @IBOutlet var transact_Value: UILabel!{
        didSet
        {
            transact_Value.font = UIFont(name: appFont, size: appTitleButton)
        }
    
    }
    @IBOutlet weak var businessNameMer: UILabel!{
        didSet
        {
            businessNameMer.font = UIFont(name: appFont, size: appTitleButton)
        }
    
    }
    @IBOutlet weak var remarksKey: UILabel! {
        didSet {
            self.remarksKey.font = UIFont(name: appFont, size: appTitleButton)
            self.remarksKey.text  = "Remarks".localized
        }
    }
    @IBOutlet weak var remarksValue: UILabel!{
        didSet {
            self.remarksValue.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    @IBOutlet weak var imageViewtype: UIImageView!
    @IBOutlet weak var stackViews: UIStackView!
    
    @IBOutlet weak var cicoCommissonHeightConstraint: NSLayoutConstraint! {
        didSet {
            self.cicoCommissonHeightConstraint.constant = 0.0
        }
    }
    @IBOutlet weak var totalAmountHeightConstraint: NSLayoutConstraint! {
        didSet {
            self.totalAmountHeightConstraint.constant = 0.0
        }
    }
    //Cash Back and bonus Points Cell
    @IBOutlet weak var remarksHeightConstraint: NSLayoutConstraint! {
        didSet {
            self.remarksHeightConstraint.constant = 0.0
        }
    }
    @IBOutlet weak var cashbackHeightConstraint: NSLayoutConstraint! {
        didSet {
            self.cashbackHeightConstraint.constant = 0.0
        }
    }
    @IBOutlet weak var bonusPointHeightConstraint: NSLayoutConstraint! {
        didSet {
            self.bonusPointHeightConstraint.constant = 0.0
        }
    }
    @IBOutlet weak var cashBackLabel: UILabel! {
        didSet {
            cashBackLabel.font = UIFont(name: appFont, size: appTitleButton)
            cashBackLabel.text = "Cash Back".localized
        }
    }
    @IBOutlet weak var cashBackvalue: UILabel!{
        didSet {
            cashBackvalue.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    @IBOutlet weak var bonusPointlabel: UILabel! {
        didSet {
            bonusPointlabel.font = UIFont(name: appFont, size: appTitleButton)
            bonusPointlabel.text = "Bonus".localized
        }
    }
    @IBOutlet weak var bonusPointValue: UILabel!{
        didSet {
            bonusPointValue.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    @IBOutlet weak var cicoFeeLabel: UILabel!{
        didSet {
            cicoFeeLabel.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var cicoFeeValue: UILabel!{
        didSet {
            cicoFeeValue.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var paymentTypeLabel: UILabel! {
        didSet {
            paymentTypeLabel.font = UIFont(name: appFont, size: appTitleButton)
            paymentTypeLabel.text = "Payment Type".localized
            paymentTypeLabel.isHidden = true
        }
    }
    @IBOutlet weak var paymentValueLabel: UILabel! {
        didSet {
            paymentValueLabel.font = UIFont(name: appFont, size: appTitleButton)
            paymentValueLabel.isHidden = true
        }
    }
    @IBOutlet weak var paymentTypeHeight: NSLayoutConstraint! {
        didSet {
            paymentTypeHeight.constant = 0
        }
    }
    
    @IBOutlet weak var bNameHeight: NSLayoutConstraint! {
        didSet {
            bNameHeight.constant = 35.0
        }
    }
    
    @IBOutlet weak var mainBalHeight: NSLayoutConstraint! {
        didSet {
            mainBalHeight.constant = 35.0
        }
    }
    
    @IBOutlet weak var cicoAmountLabel: UILabel!{
        didSet {
            cicoAmountLabel.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var cicoAmountValue: UILabel!{
        didSet {
            cicoAmountValue.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var cicoAmountHeightConstraint: NSLayoutConstraint! {
        didSet {
            self.cicoAmountHeightConstraint.constant = 0.0
        }
    }
    
    @IBOutlet weak var totAmountLabel: UILabel! {
        didSet {
            totAmountLabel.font = UIFont(name: appFont, size: appTitleButton)
            self.totAmountLabel.text = "Net Paid Digital Amount".localized
        }
    }
    @IBOutlet weak var totAmountValue: UILabel!{
        didSet {
            totAmountValue.font = UIFont(name: appFont, size: appTitleButton)
            self.totAmountValue.text = "Net Paid Digital Amount".localized
        }
    }
    @IBOutlet weak var qrImageView: UIImageView!
    @IBOutlet weak var viewRating: UIView!
    
    @IBOutlet weak var vehicleNumLbl: UILabel! {
        didSet {
            vehicleNumLbl.font = UIFont(name: appFont, size: appTitleButton)
            vehicleNumLbl.text = "Vehicle No.".localized
            vehicleNumLbl.isHidden = true
        }
    }
    @IBOutlet weak var vehicleNumVal: MarqueeLabel! {
        didSet {
            self.vehicleNumVal.animationDelay = 0.5
            vehicleNumVal.isHidden = true
        }
    }
    @IBOutlet weak var vehicleNumHeight: NSLayoutConstraint! {
        didSet {
            vehicleNumHeight.constant = 0
        }
    }
    
    @IBOutlet var starView: UIView!
    //MARK: - Properties
    var indexPath: IndexPath?
    weak var delegate: RecieptCellDelegate?
    
    //MARK: - Views Method
    override func awakeFromNib() {
        
    }
    
    //MARK: - Button Action Methods
    @IBAction func actionButton(_ sender: UIButton) {
        self.delegate?.didSelectRateOption(cell: self, ratingDone: ratingDone)
    }
    
    //MARK: - Methods
    func changeButton() {
        for views in self.stackViews.subviews {
            if let imageV = views as? UIImageView {
                imageV.isHighlighted = false
            }
        }
    }
    
    func wrapData (dict: Dictionary<String,Any>, paytoType: PayToType, accountType: String = "",displayResponse: Dictionary<String,Any>) {
        print(dict)
        print(accountType)
        
        let comments = dict.safeValueForKey("comments") as? String ?? ""
        let recieverMerchantName = comments.components(separatedBy: "##")
        if paytoType.type == .transferTo {
            paymentTypeHeight.constant = 35.0
            paymentTypeLabel.isHidden = false
            paymentValueLabel.isHidden = false
            paymentValueLabel.text = "Cash In".localized
            patmentCategoryHeight.constant = 0.0
            paymentCat.isHidden = true
            PaymentCt_Val.isHidden = true
        }
        
        if recieverMerchantName.count >= 2 {
            var recieverName = ""
            if UitilityClass.getAccountType(accountType: accountType) != "Personal"{
                
                if accountType == "DUMMY"{
                    if recieverMerchantName.count>0 && recieverMerchantName.indices.contains(1){
                        recieverName = recieverMerchantName[1]
                        
                    }
                } else if accountType == "AGENT"{
                    
                    if recieverMerchantName.count>0 && recieverMerchantName.indices.contains(1){
                        recieverName = recieverMerchantName[1]
                    }
                }
                else{
                    recieverName = recieverMerchantName[1]//dict.safeValueForKey("merchantname") as? String ?? ""
                }
                
            }
            else
            {
                recieverName = recieverMerchantName[1]
            }
            
            
            _ = (recieverMerchantName.count > 1) ? recieverMerchantName[2] : ""
            if recieverName == "Unregistered User" {
                self.businessName.text =  "Personal"
//                if appDel.currentLanguage == "uni" {
//                    self.bName_Val.text = recieverName
//                } else {
//                    self.bName_Val.text = Rabbit.uni2zg(recieverName)
//                }
                
                
                self.bName_Val.text = displayResponse.safeValueForKey("destinationName") as? String ?? ""
//                self.businessNameMer.text = recieverName
            } else {
                if accountType == "DUMMY" {
                    self.businessName.text = "Safety Cashier"
                } else {
                  //  self.businessName.text = (recieverMerchantName == "") ? "Personal": "Merchant"
                    self.businessName.text =  UitilityClass.getAccountType(accountType: accountType)
                }
//                if appDel.currentLanguage == "uni" {
//                    self.bName_Val.text = recieverName
//                } else {
//                    self.bName_Val.text = Rabbit.uni2zg(recieverName)
//                }
                
                self.bName_Val.text = displayResponse.safeValueForKey("destinationName") as? String ?? ""
               // self.bName_Val.text  = viewModel.merchant.text
//                if UitilityClass.getAccountType(accountType: accountType) != "Personal"{
//                    self.businessNameMer.text = recieverMerchantName
//                }else{
//                    self.businessNameMer.text = ""
//                }
                
            }
        }
        
        if accountType == "DUMMY"{
            self.nameLbl.text =  self.append09Format(dict.safeValueForKey("operator") as? String ?? "")
        }else{
            self.nameLbl.text =  self.append09Format(dict.safeValueForKey("destination") as? String ?? "")
        }
        
       
        //
        //if let type = dict.safeValueForKey("") as?
        let amount  = self.wrapAmount(key: dict.safeValueForKey("amount") as? String ?? "0")
        self.amount.text = amount
        if let date = dict["responsects"] as? String {
            if let dateFormat = date.components(separatedBy: " ").first?.dateValue(dateFormatIs: "dd-MMM-yyyy") {
                self.date_Val.text = dateFormat.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy")
            }
            self.time_Val.text = date.components(separatedBy: " ").last
        }
        
        self.PaymentCt_Val.text = dict["responsetype"] as? String
        let bal = self.wrapAmount(key: dict["walletbalance"] as? String ?? "0")
        let denomAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 15.0) ?? UIFont.systemFont(ofSize: 15), NSAttributedString.Key.foregroundColor: UIColor.red]
        let amntAttr = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.0) ?? UIFont.systemFont(ofSize: 11), NSAttributedString.Key.foregroundColor: UIColor.black]
        let denomStr = NSMutableAttributedString(string: bal, attributes: denomAttr)
        let amountStr    = NSMutableAttributedString.init(string: " MMK", attributes: amntAttr)
        
        let amountAttr = NSMutableAttributedString()
        amountAttr.append(denomStr)
        amountAttr.append(amountStr)
        self.balance_val.attributedText   = amountAttr
//        self.balance_val.textColor = UIColor.red
        self.transact_Val.text  = dict["transid"] as? String
        self.transact_Value.text  = dict["responsetype"] as? String
        
        //Prabu Category for Ticket Gallery
        CategoryStr = (dict["responsetype"] as? String) ?? ""
        businessName.text = businessName.text?.localized
        paymentCat.text  = paymentCat.text?.localized
        balance.text = balance.text?.localized
        transactId.text = "Transaction ID".localized
        transactType.text = transactType.text?.localized
        rateService.text = "Rate OK$ Service".localized
    }
    
    func wrapCount(txt: String) {
        self.receiptCount.text = txt
    }
    
    func append09Format(_ dest: String) -> String {
        if dest.hasPrefix("0095") {
            var number = dest.deletingPrefix("00")
            number = "+" + number
            let country = identifyCountry(withPhoneNumber: number)
            let final = number.replacingOccurrences(of: country.countryCode, with: "0")
            return final
        } else {
            if dest.hasPrefix("00") {
                var number = dest.deletingPrefix("00")
                number = "+" + number
                let country = identifyCountry(withPhoneNumber: number)
                let final = number.replacingOccurrences(of: country.countryCode, with: "(\(country.countryCode))")
                return final
            }
            return ""
        }
    }
    
    fileprivate func wrapAmount(key: String) -> String {
        let number = NSDecimalNumber(string: key)
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        if let num = formatter.string(from: number) {
            if (num == "NaN") {
                return ""
            } else {
                return num.replacingOccurrences(of: ".00", with: "")
            }
        }
        return ""
    }
    
    
    func ratingShow(_ rate: Int, commentText: String?) {
        DispatchQueue.main.async {
            for view in self.stackViews.subviews {
                if view.tag <= rate {
                    if let img = view as? UIImageView {
                        img.isHighlighted = true
                    }
                } else {
                    if let img = view as? UIImageView {
                        img.isHighlighted = false
                    }
                }
            }
        }
        ratingDone = true
    }
    
    func forRestaurant() {
        self.PaymentCt_Val.text = "Restaurant"
        self.imageViewtype.image = #imageLiteral(resourceName: "payto_receipt_restaurant")
    }
    
    func forFeul() {
        self.PaymentCt_Val.text = "FUEL"
        self.imageViewtype.image = #imageLiteral(resourceName: "pt_Receipt_fuel.png")
    }
    
    func forRemarks(str: String) {
        if str.count > 0 {
            self.remarksHeightConstraint.constant = 35.0
            self.remarksKey.isHidden = false
            self.remarksValue.isHidden = false
            self.remarksValue.text = str
        } else {
            self.remarksHeightConstraint.constant = 0.0
        }
    }
    
    
    func forCICOAmount(str: String, commission: String, totalAmount: String, type: String) {
        patmentCategoryHeight.constant = 0.0
        paymentCat.isHidden = true
        PaymentCt_Val.isHidden = true
        paymentTypeHeight.constant = 35.0
        paymentTypeLabel.isHidden = false
        paymentValueLabel.isHidden = false
        paymentValueLabel.text = (type.contains(find: "Cash-In") ? "Cash-In".localized : "Cash-Out".localized)
        if str != "0.0", str.count > 0 {
            self.cicoAmountHeightConstraint.constant = 35.0
            self.cicoAmountLabel.isHidden = false
            self.cicoAmountLabel.text = (type.contains(find: "Cash-In") ? type.localized : "Paid Digital Amount".localized)
            self.cicoAmountValue.isHidden = false
//            if type.contains(find: "Cash-In") {
            let bal = str.replacingOccurrences(of: ".00", with: "")
            self.cicoAmountValue.attributedText   = wrapAmountWithMMK(bal: bal)
            
//                self.cicoAmountValue.text = "\(str.replacingOccurrences(of: ".00", with: "")) MMK"
//            } else {
//                var amount = 0
//                let commission = commission.replacingOccurrences(of: ",", with: "")
//                let totAmount = totalAmount.replacingOccurrences(of: ",", with: "")
//                let cmsn = (commission as NSString).intValue
//                let tot = (totAmount as NSString).intValue
//                amount = Int(tot + cmsn)
//                self.cicoAmountValue.text = self.wrapAmount(key: "\(amount)") + " " + "MMK"
//            }
        } else {
            self.cicoAmountHeightConstraint.constant = 0.0
        }
    }
    
    func forCICOCommssion(str: String, type: String) {
        if str != "0.0", str.count > 0 {
            self.cicoCommissonHeightConstraint.constant = 35.0
            self.cicoFeeLabel.isHidden = false
            self.cicoFeeLabel.text = "Agent Commission".localized
            self.cicoFeeValue.isHidden = false
            
            let bal = str.replacingOccurrences(of: ".00", with: "")
            self.cicoFeeValue.attributedText   = wrapAmountWithMMK(bal: bal)
//            self.cicoFeeValue.text = "\(str.replacingOccurrences(of: ".00", with: "")) MMK"
        } else {
            self.cicoCommissonHeightConstraint.constant = 0.0
        }
    }
    
    func forTotalAmount(str: String, type: String) {
        if str != "0.0", str.count > 0 {
            self.totalAmountHeightConstraint.constant = 35.0
            self.totAmountLabel.isHidden = false
            self.totAmountLabel.text = (type == "Cash In") ? "Net Paid Digital Amount".localized : "Net Receive Amount".localized
            self.totAmountValue.isHidden = false
            let bal = str.replacingOccurrences(of: ".00", with: "")
            self.totAmountValue.attributedText   = wrapAmountWithMMK(bal: bal)
//            self.totAmountValue.text = "\(str.replacingOccurrences(of: ".00", with: "")) MMK"
        } else {
            self.totalAmountHeightConstraint.constant = 0.0
        }
    }
    
    func forVehicle(model: PayToUIViewController) {
        if model.addVehicle.text != "" {
            self.vehicleNumVal.text = model.addVehicle.text
            self.vehicleNumVal.isHidden = false
            self.vehicleNumLbl.isHidden = false
            self.vehicleNumHeight.constant = 35.0
        }
    }
    
    func forCashBack(str: String) {
        if str != "0.0", str.count > 0 {
            self.cashbackHeightConstraint.constant = 35.0
            self.cashBackLabel.isHidden = false
            self.cashBackvalue.isHidden = false
            let strCashBack = "\(str.replacingOccurrences(of: ".00", with: ""))"
            self.cashBackvalue.attributedText = wrapAmountWithMMK(bal: strCashBack)
        } else {
            self.cashbackHeightConstraint.constant = 0.0
        }
    }
    
    func forBonusPoints(str: String) {
        if str != "0", str.count > 0 {
            self.bonusPointHeightConstraint.constant = 35.0
            self.bonusPointlabel.isHidden = false
            self.bonusPointValue.isHidden = false
            self.bonusPointValue.text = wrapAmount(key: str)
        } else {
            self.bonusPointHeightConstraint.constant = 0.0
        }
    }
    
    
    func wrapConditions(type: recieptType, payToType: PayToType, cico: String) {
        if cico != "" {
            if cico == "Cash In" {
                self.imageViewtype.image = UIImage(named: "transfer_to_receipt")
            } else if cico == "Cash Out" {
                self.imageViewtype.image = UIImage(named: "cash_out_receipt")
            } else {
                self.imageViewtype.image = #imageLiteral(resourceName: "dashboard_pay_send")
            }
        } else {
            switch type {
            case .topupMyNumber:
                self.imageViewtype.image = #imageLiteral(resourceName: "dashboard_my_number")
            case .topupOtherNumber:
                self.imageViewtype.image = #imageLiteral(resourceName: "dashboard_other_number")
            case .payto:
                self.imageViewtype.image = #imageLiteral(resourceName: "dashboard_pay_send")
            case .overseas:
                self.imageViewtype.image = #imageLiteral(resourceName: "dashboard_overseas_recharge")
            }
        }
    }
}

extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: image!.cgImage!)
    }
}


//MARK: - AvailableAgentsListDTO Request
struct CICOUpdateToServer: Codable {
    var senderPhoneNumber, receiverPhoneNumber, rechargeAmount: String?
    let okDollarTransactionId, bonusPoints, cashBack, openingBalance, closingBalance, okDollarTransactionTime: String?
    let TotalAmount, commissionPercent, comments, paymentType, senderName, receiverName, receiverClosingBalance, remark: String?
    let cashTransactionType: Int
    
    enum CodingKeys: String, CodingKey {
        case senderPhoneNumber = "senderPhoneNumber"
        case receiverPhoneNumber = "receiverPhoneNumber"
        case rechargeAmount = "rechargeAmount"
        case okDollarTransactionId = "okDollarTransactionId"
        case bonusPoints = "bonusPoints"
        case cashBack = "cashBack"
        case openingBalance = "openingBalance"
        case closingBalance = "closingBalance"
        case okDollarTransactionTime = "okDollarTransactionTime"
        case TotalAmount = "TotalAmount"
        case cashTransactionType = "cashTransactionType"
        case commissionPercent = "commissionPercent"
        case comments = "comments"
        case paymentType = "paymentType"
        case senderName = "senderName"
        case receiverName = "receiverName"
        case receiverClosingBalance = "receiverClosingBalance"
        case remark = "remark"
    }
    
    init(dict: Dictionary<String, Any>, model: PayToUIViewController, typeRes: PayToType) {
        self.senderPhoneNumber = UserModel.shared.mobileNo
        self.receiverPhoneNumber = dict.safeValueForKey("destination") as? String
        //self.rechargeAmount = dict.safeValueForKey("amount") as? String
        self.okDollarTransactionId = dict.safeValueForKey("transid") as? String
        self.bonusPoints = dict.safeValueForKey("loyaltypoints") as? String
        self.cashBack = dict.safeValueForKey("kickvalue") as? String
        self.openingBalance = dict["prewalletbalance"] as? String ?? "0"
        self.closingBalance = dict["walletbalance"] as? String ?? "0"
        self.okDollarTransactionTime = dict.safeValueForKey("responsects") as? String ?? ""
        //self.TotalAmount  = dict.safeValueForKey("amount") as? String ?? "0"
        
        if model.cashInCashOut == "Cash In" {
            self.TotalAmount  = model.amount.text ?? "0"
            
//            var result = 0
//            let intCommission = Int(model.commissionTxt.text ?? "") ?? 0
//            let intAmount = Int(model.amount.text ?? "") ?? 0
//            result  = (intAmount - intCommission)
            rechargeAmount = model.totalAmountTxt.text
        }
        else {
            
            self.rechargeAmount = model.totalAmountTxt.text
//            var result = 0
//            //self.commissionPercent = model.commissionTxt.text ?? ""
//            let intCommission = Int(model.commissionTxt.text ?? "") ?? 0
//            let intAmount = Int(model.amount.text ?? "") ?? 0
//            result  = (intAmount - intCommission)
            TotalAmount = dict.safeValueForKey("amount") as? String
        }
        
        //self.TotalAmount  = model.amount.text ?? "0"
        self.cashTransactionType = (model.cashInCashOut == "Cash In") ? 0 : 1
        self.commissionPercent = model.commissionTxt.text ?? ""
        self.comments = (model.cashInCashOut == "Cash In") ? "Sent Digital Money" : "Received Digital Money"
        var type = "PAYTO"
        if typeRes.transType == .payToID {
            type = "PAYTOID"
        }
        var receiverName: String? = nil
        if let com = dict.safeValueForKey("comments") as? String {
            let recieverMerchantName = com.components(separatedBy: "##")
            if recieverMerchantName.count >= 2 {
                let recieverName = recieverMerchantName[1]
                if recieverName == "Unregistered User" {
                    receiverName =  "Unknown"
                } else {
                    receiverName = recieverName
                }
            }
        } else {
            receiverName    =  dict.safeValueForKey("merchantname") as? String ?? ""
        }
        self.paymentType  = type
        self.senderName  = UserModel.shared.name
        self.receiverName  = receiverName ?? ""
        self.receiverClosingBalance  = dict.safeValueForKey("unregipostbalance") as? String ??  "0"
        self.remark  = model.remarks.text ?? ""
    }
    
}



extension PTRecieptViewController: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        PTLoader.shared.hide()
        if  screen == "CICOUpdateReport" {
            guard let castedData = json as? Data else {
                PTLoader.shared.hide()
                println_debug("Data error")
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: castedData, options: .allowFragments)
                println_debug(json)
            } catch _ {
                //cashInCashOutDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed, type: type)
                PTLoader.shared.hide()
            }
        }
    }
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        PTLoader.shared.hide()
    }
}
