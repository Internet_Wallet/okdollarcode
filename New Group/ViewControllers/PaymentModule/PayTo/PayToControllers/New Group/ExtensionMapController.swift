//
//  ExtensionMapController.swift
//  OK
//
//  Created by Ashish on 6/26/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import Rabbit_Swift

extension MapPayToViewController : MKMapViewDelegate {
    
    func locationAuthorisation() {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            self.alertViewPermission.isHidden = false
            break
        case .restricted:
            self.alertViewPermission.isHidden = false
            break
        case .denied:
            self.alertViewPermission.isHidden = false
            break
        case .authorizedAlways:
            GeoLocationManager.shared.startUpdateLocation()
            self.alertViewPermission.isHidden = true
            break
        case .authorizedWhenInUse:
            GeoLocationManager.shared.startUpdateLocation()
            self.alertViewPermission.isHidden = true
            break
        }
    }
    
    func addClustersToMap(list: [NearByServicesNewModel]) {

        if list.count <= 0 {
            PaytoConstants.alert.showErrorAlert(title: nil, body: "No Records Found!".localized)
            self.listViewContainer.isHidden = true
            self.listView.isHidden = false
            self.mapView.isHidden = true
            return
        } else {
            self.listViewContainer.isHidden = true
            self.listView.isHidden = false
            self.mapView.isHidden = true
            
            merchantModel.removeAll()
            merchantModel = list
            listTableView.reloadData()
        }
        
        removeAllAnnotationsInMap()
        listData = list
        var clusterArray: [FBAnnotation] = []
        var value = 0
        while value < list.count {
            if let model = list[value] as? NearByServicesNewModel {
                
                //print("AnotationLatitudeandlogtitude-----\(model.UserContactData?.LocationNewData?.Latitude)----\(model.UserContactData?.LocationNewData?.Longitude)")
                
                
                if let lat = model.UserContactData?.LocationNewData?.Latitude , let long = model.UserContactData?.LocationNewData?.Longitude {
                    print("AnotationLatitudeandlogtitude-----")
                    
                    let annotation = FBAnnotation()
                    annotation.coordinate = CLLocationCoordinate2D(latitude:  lat, longitude: long)
                    
                    if model.UserContactData?.BusinessName == "" {
                        
                        if model.UserContactData?.FirstName != "" {
                            //print("AnotationFirstname-----\(model.UserContactData?.FirstName)")
                            annotation.title = Rabbit.uni2zg(String.init(format: "%@", model.UserContactData?.FirstName ?? ""))
                            //annotation.title = model.UserContactData?.FirstName
                        }
                        
                    } else {
                        //print("Anotationbusiness-----\(model.UserContactData?.BusinessName)")
                        //annotation.title = model.UserContactData?.BusinessName
                        annotation.title = Rabbit.uni2zg(String.init(format: "%@", model.UserContactData?.BusinessName ?? ""))
                    }
                    let pinImage = UIImage(named: "ok_logo.png")
                    annotation.image = pinImage
                    annotation.userData = model
                    clusterArray.append(annotation)
                    
                    if value == list.count-2 {
                        let initialLocation = CLLocation(latitude: Double(lat), longitude: Double(long))
                        centerMapOnLocation(location: initialLocation)
                    }
                }
            }
            value += 1
        }
        clusterManager.add(annotations: clusterArray)
        reloadMap()
    }
    
    func centerMapOnLocation(location: CLLocation) {
        
        let coordinateRegion = MKCoordinateRegion.init(center: location.coordinate,latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        
        self.map.setRegion(coordinateRegion, animated: true)
    }
    
    func removeAllAnnotationsInMap() {
        clusterManager.removeAll()
        reloadMap()
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        self.reloadMap()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var reuseId = ""
        if annotation is FBAnnotationCluster {
            reuseId = "Cluster"
            var clusterView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
            if clusterView == nil {
                clusterView = FBAnnotationClusterView(annotation: annotation, reuseIdentifier: reuseId, configuration: FBAnnotationClusterViewConfiguration.default())
            } else {
                clusterView?.annotation = annotation
            }
            return clusterView
        } else {
            
            print("NBSCustomAnnotation called----\(annotation)")
            
            var annotationView = MKMarkerAnnotationView()
            let annotation = annotation as? NBSCustomAnnotation
            let identifier = "NBSCustomAnnotation"
            
            if let dequedView = mapView.dequeueReusableAnnotationView(
                withIdentifier: identifier)
                as? MKMarkerAnnotationView {
                annotationView = dequedView
            } else{
                annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            }
            annotationView.glyphImage = UIImage(named: "ok_logo.png")
            annotationView.image = UIImage(named: "ok_logo.png")
            annotationView.backgroundColor = .clear
            annotationView.markerTintColor = .clear
            annotationView.glyphTintColor = .clear
            annotationView.clusteringIdentifier = identifier
            return annotationView
        
          /*  let annotationIdentifier = "SomeCustomIdentifier"
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
            
            if annotationView == nil {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
                //annotationView?.canShowCallout = false
                annotationView!.clusteringIdentifier = annotationIdentifier

                annotationView?.image = UIImage(named: "ok_logo.png") */
                
                /*
                    for promotion in merchantModel {
                        if let promo = promotion as? PTMapNearbyMerchant {
                            if promo.shopID == annotation.title {
                                
                                    let emoji1 = (promo.businessCategory?.logo)!
                                    let emoji2 = (promo.businessCategory?.businessType?.logo)!
                                    let topImage = emoji1.emojiToImage()
                                    let bottomImage = emoji2.emojiToImage()
                                    annotationView?.image = topImage?.combineWith(image: bottomImage!)
                                
                                break
                            }
                        }
                    }
                */
           /* } else {
                annotationView?.annotation = annotation
            }
            return annotationView */
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation else { return }
        
        if let cluster = annotation as? FBAnnotationCluster {
            var zoomRect = MKMapRect.null
            for annotation in cluster.annotations {
                
                let annotationPoint = MKMapPoint.init(annotation.coordinate)
                let pointRect = MKMapRect.init(x: annotationPoint.x, y: annotationPoint.y, width: 0, height: 0)
                if zoomRect.isNull {
                    zoomRect = pointRect
                } else {
                    zoomRect = zoomRect.union(pointRect)
                }
            }
            mapView.setVisibleMapRect(zoomRect, animated: true)
        }else {
            
            print("Else payto map icon  called")
            
            if let eachAnnotation = annotation as? FBAnnotation {
                if let userdata = eachAnnotation.userData as? NearByServicesNewModel {
                    
                    print("inside payto map icon  called")

                    //println_debug("user each data details :::: \(userdata.shop_Name)  ,, \(userdata.shop_Address?.addressLine1) ,, \(userdata.shop_InMeter)")
                    
                    let promotion = PromotionsModel.init(model: userdata)
                    self.showPromotionInfoView(promotion: promotion)

                    //self.showPromotionInfoView(promotion: userdata)
                }
                
            }

           /* if let eachAnnotation = annotation as? FBAnnotation {
                if let userdata = eachAnnotation.userData as? PTMapNearbyMerchant {
                    
                    print("payto map icon  called")

                    let nearby = NearByServicesModel.init(model: userdata)
                    let promotion = PromotionsModel.init(model: nearby)
                    self.showPromotionInfoView(promotion: promotion , merchantnew:userdata)
                }
            } */
        }
    }
    
   
    //MARK:-  Popupview Action
    func showPromotionInfoView(promotion: PromotionsModel) {
        self.promotionInfoView.isHidden = false
        self.promotionInfoView.center.x -= self.view.bounds.width
        if let timer = infoViewTimer {
            timer.invalidate()
        }
        loadInfoViewData(info: promotion)
        UIView.animate(withDuration: 1.5, delay: 0.5,
                       usingSpringWithDamping: 0.3,
                       initialSpringVelocity: 0.5,
                       options: [], animations: {
                        self.promotionInfoView.center.x += self.view.bounds.width
                        
        }, completion:{ _ in
            self.infoViewTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.update), userInfo: nil, repeats: false)
        })
    }
    
    @objc func update() {
        infoViewTimer.invalidate()
        removeTheInfoView()
    }
    
    func removeTheInfoView() {
        UIView.animate(withDuration: 0.1,
                       delay: 0.1,
                       options: UIView.AnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.promotionInfoView.center.x -= self.view.bounds.width
        }, completion: { (finished) -> Void in
            self.promotionInfoView.isHidden = true
            self.promotionInfoView.center.x += self.view.bounds.width
        })
    }
    
    func loadInfoViewData(info: PromotionsModel) {
        self.selectedpromotionData = nil
        self.selectedpromotionData = info
        promotionImgView.image = UIImage.init()
        promotionNameLbl.text = info.shop_Name
        var mobileNumber = "NA"
        if info.shop_Phonenumber!.count > 0 {
            mobileNumber = getPhoneNumberByFormat(phoneNumber: info.shop_Phonenumber!)
        }
        contactNoLbl.text = "Contact: \(mobileNumber)"
        
        var distance = "0 Km"
        if let shopDistance = info.shop_InMeter {
            distance = String(format: "%.2f Km", shopDistance)
        }
        distanceLbl.text = "Distance: \(distance)"
        
        if info.shop_Templates.count > 0 {
            let promoOffer = info.shop_Templates[0]
            remarksLbl.text = promoOffer.template_Header
        }else {
            remarksLbl.text = "NA"
        }
        
         promotionImgView.image = UIImage.init(named: "btc")

       /* let emoji1 = (merchant.businessCategory?.logo!)!
        let emoji2 = (merchant.businessCategory?.businessType?.logo!)!
        let topImage = emoji1.emojiToImage()
        let bottomImage = emoji2.emojiToImage()
        promotionImgView.image = topImage?.combineWith(image: bottomImage!) */
        
        
//        let emoji1 = (info.shop_Category?.categoryLogo!)!
//        let emoji2 = (info.shop_Category?.businessType?.businessType_Logo!)!
//        let topImage = emoji1.emojiToImage()
//        let bottomImage = emoji2.emojiToImage()
//         promotionImgView.image = topImage?.combineWith(image: bottomImage!)
        //promotionImgView.image = bottomImage
        
        promotionInfoView.backgroundColor = info.isAgent ? kYellowColor : UIColor.white
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

    func reloadMap() {
        DispatchQueue.main.async {
            let mapBoundsWidth = Double(self.map.bounds.size.width)
            let mapRectWidth = self.map.visibleMapRect.size.width
            let scale = mapBoundsWidth / mapRectWidth
            let annotationArray = self.clusterManager.clusteredAnnotations(withinMapRect: self.map.visibleMapRect, zoomScale:scale)
            self.clusterManager.display(annotations: annotationArray, onMapView:self.map)
        }
    }
    
    @IBAction func PopupAction(_ sender: Any) {
        
        let promotion = selectedpromotionData
        //print("PAYTO MAP action called\(promotion)")
        
      /*   let storyboard: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
        let protionDetailsView = storyboard.instantiateViewController(withIdentifier: "PromotionDetailView_ID") as! PromotionDetailViewController
        
        if let model = selectedpromotionData {
            if let distance =  model.shopInMeter {
                let roundValue = (distance as NSString).floatValue
                if roundValue > 1000 {
                    let stringDistance = String.init(format: "%.2f Km", roundValue/1000)
                    protionDetailsView.distance = stringDistance
                } else {
                    let stringDistance = String.init(format: "%.2f m", roundValue)
                    protionDetailsView.distance = stringDistance
                }
            }
        }
        
        let promotion = NearByServicesModel.init(model: model)
        protionDetailsView.selectedNearByServices = promotion
        protionDetailsView.strNearByPromotion = "NearBy"
        
        //            self.navigationController?.pushViewController(protionDetailsView, animated: true)
        self.navigation?.pushViewController(protionDetailsView, animated: true) */

        let storyboard: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
        let protionDetailsView = storyboard.instantiateViewController(withIdentifier: "PromotionDetailView_ID") as! PromotionDetailViewController
        protionDetailsView.selectedPromotion = promotion
        protionDetailsView.strNearByPromotion = "Promotion"
        self.navigation?.pushViewController(protionDetailsView, animated: true)
        
        
    }

}
