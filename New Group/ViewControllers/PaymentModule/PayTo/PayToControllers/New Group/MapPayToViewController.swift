//
//  MapPayToViewController.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/27/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import MapKit
import Rabbit_Swift

enum SelectLocationEnum {
    case currentLocation, division, nearbyMerchants, allMerchants, merchantWithPromotions
}

protocol SelectLocationDelegate : class {
    func didSelectLocation(withOptions option: SelectLocationEnum)
}


class MapPayToViewController: PayToBaseViewController, CategoriesSelectionDelegate, SelectLocationDelegate, PTMapListTableViewCellDelegate, TownshipSelectionDelegate, NRCDivisionStateDeletage, MainNearByLocationViewDelegate , CurrentLocationViewControllerDelegate {
    
    
    fileprivate var paytoMenu : PaytoScroller?
    @IBOutlet weak var alertViewPermission: UIView!
    @IBOutlet weak var mapViewContainer: UIView!
    @IBOutlet weak var listDisplayButton: ButtonWithBages!
    
    //prabu
    let defaults = UserDefaults.standard
    @IBOutlet weak var promotionInfoView: UIView!
    @IBOutlet weak var promotionImgView: UIImageView!
    @IBOutlet weak var promotionNameLbl: UILabel!
    @IBOutlet weak var contactNoLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var remarksLbl: MarqueeLabel!
    var infoViewTimer: Timer!
    var listData: [Any]?
    let regionRadius: CLLocationDistance = 500
    var checkurl:String = ""
    
    //MapView
    @IBOutlet weak var mapView: UIView! {
        
        didSet {
            
            self.mapView.clipsToBounds = true
            self.mapView.layer.borderWidth = 0.8
            self.mapView.layer.borderColor = UIColor.lightGray.cgColor
            self.mapView.layer.cornerRadius = 20.0
            
            self.mapView.layer.shadowColor = UIColor.lightGray.cgColor
            self.mapView.layer.shadowOpacity = 0.8
            self.mapView.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
            self.mapView.layer.shadowRadius = 5.0

          
        }
    }
    
    //Filter
    @IBOutlet  weak var mapfilterTitileLbl : UILabel! {
        didSet {
            self.mapfilterTitileLbl.text = "Filter".localized
        }
    }
    @IBOutlet  weak var mapfilterBtnOut : UIButton!
    
    @IBOutlet  weak var newmapTitileLbl : UILabel! {
        didSet {
            self.newmapTitileLbl.text = "Map".localized
        }
    }
    @IBOutlet  weak var newmapBtnOut : UIButton!
    
    
    //Listview
    @IBOutlet weak var listView: UIView! {
        
        didSet {
            
            self.listView.clipsToBounds = true
            self.listView.layer.borderWidth = 0.8
            self.listView.layer.borderColor = UIColor.lightGray.cgColor
            self.listView.layer.cornerRadius = 20.0
            
            self.listView.layer.shadowColor = UIColor.lightGray.cgColor
            self.listView.layer.shadowOpacity = 0.8
            self.listView.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
            self.listView.layer.shadowRadius = 5.0
        }
    }
    
    @IBOutlet  weak var newlistTitileLbl : UILabel! {
        didSet {
            self.newlistTitileLbl.text = "List".localized
        }
    }
    @IBOutlet  weak var newlistBtnOut : UIButton!
    
    //Filter
    @IBOutlet  weak var listfilterTitileLbl : UILabel! {
        didSet {
            self.listfilterTitileLbl.text = "Filter".localized
        }
    }
    @IBOutlet  weak var listfilterBtnOut : UIButton!
    
    @IBOutlet  weak var lblCurrentLoc      : UILabel!
        {
        didSet
        {
            lblCurrentLoc.text = lblCurrentLoc.text?.localized
        }
    }
    @IBOutlet  weak var lblList      : UILabel!
        {
        didSet
        {
            lblList.text = lblList.text?.localized
        }
    }
    @IBOutlet  weak var lblCategories      : UILabel!
        {
        didSet
        {
            lblCategories.text = lblCategories.text?.localized
        }
    }
    @IBOutlet  weak var lblSatView      : UILabel!
        {
        didSet
        {
            lblSatView.text = lblSatView.text?.localized
        }
    }
    
    var navigation : UINavigationController?
    
    var pulsingEffect : PulseManager?
    
    @IBOutlet weak var listViewContainer: UIView!
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var heightListViewContainer: NSLayoutConstraint!
    @IBOutlet weak var heightUpDownImage: UIImageView!
    @IBOutlet weak var merchantCountglobalLabel: UILabel!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var locationOutlet: UILabel!
    var selectedpromotionData : PromotionsModel?
    
    // cluster manager
    let clusterManager = FBClusteringManager()
    
    var merchantModel = [NearByServicesNewModel]()
    var merchantModelBackup = [NearByServicesNewModel]()
    var selectedFilterData      : NearByServicesNewModel?
    var selectedMerchant : NearByServicesNewModel?
    
    //////////////////
    
    // list view data source
    var listMerchantModel : [String]?
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        listView.isHidden = false
        mapView.isHidden = true
        self.listViewContainer.isHidden = false
        self.view.backgroundColor = .clear
        
//        merchantModelBackup = merchantModel
//
//        self.promotionInfoView.isHidden = true
//        pulsingEffect = PulseManager.updateView()
//        pulsingEffect?.startPulsing(distance: 0)
//
//        self.map.delegate = self
//
//        //prabu
//        defaults.set(nil, forKey: "PTMsortBy")
//        defaults.set(nil, forKey: "PTMfilterBy")
//        defaults.set(nil, forKey: "PTMcategoryBy")
//        defaults.set(nil, forKey: "PTMNearBy")
//
//      ///// // slider.isContinuous = false
//        listMerchantModel = ["",""]
//        self.listTableView.tableFooterView = UIView.init()
//        self.listViewContainer.isHidden = true
//        self.heightUpDownImage.isHighlighted = true
//
//        self.map.showsUserLocation = false
//
//
//         NotificationCenter.default.addObserver(self, selector: #selector(reloadViewData), name: NSNotification.Name(rawValue: "MapPayToLocation"), object: nil)
    }
    
   
    
    @objc func reloadViewData() {
        println_debug("reloadViewData")
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: String(describing: CurrentLocationViewController.self)) as? CurrentLocationViewController else { return }
        vc.delegateData = self
        self.navigation?.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       self.navigation = PaytoConstants.global.navigation
        self.locationAuthorisation()
        if self.alertViewPermission.isHidden {
            ////self.initialSetupForMap()
            //self.initialSetupForMapSlider()
            
        }
       self.currentLocationUpdate()
    }
        
       //prabu
////        self.heightListViewContainer.constant = 0.00
////        self.listViewContainer.isHidden=true
////        self.listView.isHidden = false
////        self.mapView.isHidden = true
//    }
    
    @IBAction func goToSettings(_ sender: UIButton) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:])
                , completionHandler: nil)
        } else {
            UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func initialSetupForMap() {
        let locationLat  = CLLocationDegrees.init(GeoLocationManager.shared.currentLatitude)
        let locationLong = CLLocationDegrees.init(GeoLocationManager.shared.currentLongitude)
        
        let location = CLLocationCoordinate2D.init(latitude: locationLat ?? 0.0, longitude: locationLong ?? 0.0)
        let distance = 1.0//self.slider.value / 3.2808
        let coordinateRegion = MKCoordinateRegion.init(center: location, latitudinalMeters: CLLocationDistance(distance), longitudinalMeters: CLLocationDistance(distance))
        
        map.setRegion(coordinateRegion, animated: true)
        
        let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
        let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
        self.callNearbyMerchantWithCustomLocation(lat: lat, long: long)

    }
    
    func initialSetupForMapSlider() {
        let locationLat  = CLLocationDegrees.init(GeoLocationManager.shared.currentLatitude)
        let locationLong = CLLocationDegrees.init(GeoLocationManager.shared.currentLongitude)
        
        let location = CLLocationCoordinate2D.init(latitude: locationLat ?? 0.0, longitude: locationLong ?? 0.0)
        let distance = 100.0//self.slider.value / 3.2808
        let coordinateRegion = MKCoordinateRegion.init(center: location, latitudinalMeters: CLLocationDistance(distance), longitudinalMeters: CLLocationDistance(distance))
        
        map.setRegion(coordinateRegion, animated: true)
        
        let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
        let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
        self.callNearbyMerchantWithCustomLocation(lat: lat, long: long)

    }
    
    
    // MARK: Categories Helpers
    private func showCategoriesView() {
        let storyBoardName = UIStoryboard(name: "Promotion", bundle: nil)
        guard let categoriesView = storyBoardName.instantiateViewController(withIdentifier: "CategoriesListView_ID") as? CategoriesListViewController else { return }
        categoriesView.delegate = self
        categoriesView.modalPresentationStyle = .overCurrentContext
        self.navigation?.present(categoriesView, animated: true, completion: nil)
    }
    
    func didSelectLocationByCategory(category: SubCategoryDetail?) {
        if let cat = category {
            
            var subCategoryCode = cat.subCategoryCode
            if subCategoryCode.length == 1 {
                subCategoryCode = "0\(cat.subCategoryCode)"
            }
            
            let array = self.merchantModelBackup.filter({$0.UserContactData?.BusinessName == subCategoryCode})
            
            defaults.set(cat.mainCategoryName + "," + cat.subCategoryName, forKey: "PTMcategoryBy")

            self.addClustersToMap(list: array)
        } else {
            
            let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
            let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
            self.callNearbyMerchantWithCustomLocation(lat: lat, long: long)
            
        }
    }
    
    // MARK: Township
    func didSelectTownship(location: LocationDetail, township: TownShipDetail) {
        self.locationOutlet.text = township.cityNameEN
        
        let array = self.merchantModel.filter({$0.UserContactData?.Township == township.cityNameEN})
        print("Township----+++++\(township.cityNameEN)******\(array.count)")
        self.addClustersToMap(list: array)
    }
    
   
    
    //MARK: - Location Manager
    
    func didSelectLocationByCurrentLocation() {
        self.currentLocationUpdate()
    }
    
    // MARK: Filterview CurrentLocation
    func didSelectNearByLocation() {
        
        self.currentLocationUpdate()
    }
    
    // MARK: List and Map Action
    @IBAction func mapfilterBtnAction(_ sender: UIButton) {
        /*let storyBoardName = UIStoryboard(name: "Promotion", bundle: nil)
        guard let categoriesView = storyBoardName.instantiateViewController(withIdentifier: "CategoriesListView_ID") as? CategoriesListViewController else { return }
        categoriesView.delegate = self
        categoriesView.modalPresentationStyle = .overCurrentContext
        self.present(categoriesView, animated: true, completion: nil)*/
        guard let sortViewController = UIStoryboard(name: "NBSMain", bundle: nil).instantiateViewController(withIdentifier: "FilterNearByServiceViewController") as? FilterNearByServiceViewController else { return }
        sortViewController.categoryDelegate = self
        sortViewController.applybuttondelegate = self
        sortViewController.nearbylocationdelegate = self
        sortViewController.filterDataList = merchantModelBackup
        sortViewController.previousSelectedFilterData = self.selectedFilterData
        
        sortViewController.findviewStr = "PaytoMap"
        if (defaults.value(forKey: "PTMsortBy") != nil) {
            sortViewController.previousSelectedSortOptionnew = defaults.value(forKey: "PTMsortBy") as! String
        }else {
            sortViewController.previousSelectedSortOptionnew = nil
        }
        if (defaults.value(forKey: "PTMNearBy") != nil) {
            sortViewController.previousSelectedNearbyOptionnew = defaults.value(forKey: "PTMNearBy") as! String
        }else {
            sortViewController.previousSelectedNearbyOptionnew = nil
        }
        
        sortViewController.checkMap = false
        sortViewController.modalPresentationStyle = .overCurrentContext
        self.navigation?.present(sortViewController, animated: true, completion: nil)
    }
    
    @IBAction func mapBtnAction(_ sender: UIButton) {
      
       // print("Map Action called----")

//        self.heightUpDownImage.isHighlighted = false
//      //  self.heightListViewContainer.constant = 0.00
//        self.listViewContainer.isHidden=false
//        self.promotionInfoView.isHidden = true

     //   self.listView.isHidden = false
      //  self.mapView.isHidden = true
        
        
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Map", bundle: nil)
        let vc : MapPayToVC = storyboard.instantiateViewController(withIdentifier: "MapPayToVC") as! MapPayToVC
        vc.merchantModel = self.merchantModel
        vc.merchantModelBackup = self.merchantModelBackup
        let navigationController = UINavigationController(rootViewController: vc)
        self.presentDetail(navigationController)
    
    }
    
    @IBAction func listfilterBtnAction(_ sender: UIButton) {
        /*
        let storyBoardName = UIStoryboard(name: "Promotion", bundle: nil)
        guard let categoriesView = storyBoardName.instantiateViewController(withIdentifier: "CategoriesListView_ID") as? CategoriesListViewController else { return }
        categoriesView.delegate = self
        categoriesView.modalPresentationStyle = .overCurrentContext
        self.present(categoriesView, animated: true, completion: nil)
        */
        
        guard let sortViewController = UIStoryboard(name: "NBSMain", bundle: nil).instantiateViewController(withIdentifier: "FilterNearByServiceViewController") as? FilterNearByServiceViewController else { return }
        sortViewController.categoryDelegate = self
        sortViewController.nearbylocationdelegate = self
        sortViewController.applybuttondelegate = self
        sortViewController.filterDataList = merchantModelBackup
        sortViewController.previousSelectedFilterData = self.selectedFilterData

        sortViewController.findviewStr = "PaytoMap"
        if (defaults.value(forKey: "PTMsortBy") != nil) {
            sortViewController.previousSelectedSortOptionnew = defaults.value(forKey: "PTMsortBy") as! String
        }else {
            sortViewController.previousSelectedSortOptionnew = nil
        }
        
        if (defaults.value(forKey: "PTMNearBy") != nil) {
            sortViewController.previousSelectedNearbyOptionnew = defaults.value(forKey: "PTMNearBy") as! String
        }else {
            sortViewController.previousSelectedNearbyOptionnew = nil
        }
        
        sortViewController.checkMap = false
        sortViewController.modalPresentationStyle = .overCurrentContext
        self.navigation?.present(sortViewController, animated: true, completion: nil)
        
    }
    
    @IBAction func listBtnAction(_ sender: UIButton) {
        
       // print("List Action called----")
        
        self.heightUpDownImage.isHighlighted = true
        self.listViewContainer.isHidden=false
        let height = self.mapViewContainer.bounds.height
     //   self.heightListViewContainer.constant = height
        
    //    self.listView.isHidden = true
     //   self.mapView.isHidden = false
        
    }
    
    
    
    //MARK:- Slider Actions

    //MARK:- Button Actions
    @IBAction func sliderAction(_ sender: UISlider) {
        //        let valueChnaged = sender.value
        self.slider.minimumTrackTintColor = UIColor.blue

        listViewContainer.isHidden = false
        //self.initialSetupForMap()
       /// self.initialSetupForMapSlider()
        
        
    }
    
    @IBAction func listAction(_ sender: UIButton) {
        if listViewContainer.isHidden {
            listViewContainer.isHidden = false
        } else {
            listViewContainer.isHidden = false
        }
    }
    
    
    @IBAction func currentLocationAction(_ sender: UIButton) {
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: String(describing: CurrentLocationViewController.self)) as? CurrentLocationViewController else { return }
        vc.delegate = self
        self.navigation?.pushViewController(vc, animated: true)
    }
    
    @IBAction func openCategoriesAction(_ sender: UIButton) {
        self.showCategoriesView()
    }
    
    @IBAction func mapViewChangeAction(_ sender: UIButton) {
        if map.mapType != .satellite {
            map.mapType = .satellite
            self.lblSatView.text = "Map View".localized
        } else {
            map.mapType = .standard
            self.lblSatView.text = "Satellite View".localized
        }
    }
    
    //MARK:- List Container View Button Action
    @IBAction func listViewUpDownAction(_ sender: UIButton) {
        if self.heightUpDownImage.isHighlighted {
            self.heightUpDownImage.isHighlighted = false
         //   self.heightListViewContainer.constant = 0.00
            self.listViewContainer.isHidden = true
         //   self.listView.isHidden = false
            self.mapView.isHidden = true

        } else {
            self.heightUpDownImage.isHighlighted = true
            let height = self.mapViewContainer.bounds.height
         //   self.heightListViewContainer.constant = height
            self.listViewContainer.isHidden = false
            //self.listView.isHidden = true
           // self.mapView.isHidden = false
            self.mapView.isHidden = true
        }
        
        UIView.animate(withDuration: 0.1) {
            self.view.layoutIfNeeded()
        }
        
    }
   
    //MARK:- Select Location Delegate
    func didSelectLocation(withOptions option: SelectLocationEnum) {
        switch option {
        case .currentLocation:
            self.currentLocationUpdate()
            break
        case .division:
            self.merchantWithDivisions()
            break
        case .nearbyMerchants:
            //print("Nearbymerchant called")
        notfDef.post(name:Notification.Name(rawValue:"PayToStateDivision"),
                         object: nil,
                         userInfo: ["Name":  Rabbit.uni2zg(String.init(format: "%@","Near By".localized))])
            
            let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
            let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
            self.callNearbyMerchantWithCustomLocation(lat: lat, long: long)
            
            break
        case .allMerchants:
            let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
            let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
            self.callNearbyMerchantWithCustomLocation(lat: lat, long: long)
            
            break
        case .merchantWithPromotions:
            let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
            let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
            self.callNearbyMerchantWithCustomLocation(lat: lat, long: long)
            break
        }
    }
    
    private func merchantWithDivisions() {
        let viewController = UIStoryboard(name: "TaxPayment", bundle: Bundle.main).instantiateViewController(withIdentifier: "NRCDivisionRoot")
        if let nav = viewController as? UINavigationController {
            for views in nav.viewControllers {
                if let vc = views as? TaxNRCDivisionState {
                    vc.townShipDelegate = self
                    vc.fromPaytoMap = ""
                    break
                }
            }
        }
        self.navigation?.present(viewController, animated: true, completion: nil)
    }
    
    //State Division delegate
    func setStateDivisionDetails(division: LocationDetail) {
        println_debug(division.stateOrDivitionCode)
    }
    
    //func setDivisionDetails(divisionName: String, township: String) {
    func setDivisionDetails(stateDivCode: LocationDetail, divisionName: String, township: String) {
        self.locationOutlet.text = township + "," + divisionName
    notfDef.post(name:Notification.Name(rawValue:"PayToStateDivision"),
                object: nil,
                userInfo: ["Name":  divisionName + "," + township])
        
        geoLocManager.getLatLongByName(cityname: township) { (isSuccess, lat, long) in
            guard isSuccess, let latti = lat, let longi = long  else {
                self.showErrorAlert(errMessage: "Try Again".localized)
                return
            }
            
            DispatchQueue.main.async {
                
                let locationLat  = CLLocationDegrees.init(Double(latti) ?? 0.0)
                let locationLong = CLLocationDegrees.init(Double(longi) ?? 0.0)
                
                let locationData = CLLocationCoordinate2D.init(latitude: locationLat , longitude: locationLong)
                let distance = 1.0 //self.slider.value / 3.2808
                let coordinateRegion = MKCoordinateRegion.init(center: locationData, latitudinalMeters: CLLocationDistance(distance), longitudinalMeters: CLLocationDistance(distance))
                
                self.map.setRegion(coordinateRegion, animated: true)
                
                self.callNearbyMerchantWithCustomLocation(lat: latti, long: longi)

               // self.callNearbyMerchantWithCustomLocation(lat: latti, long: longi , business: stateDivCode.stateOrDivitionCode)
            }
        }
    }
    
    func merchantWithPromotions() {
        self.currentLocationUpdate()
    }
    
    func currentLocationUpdate() {
        geoLocManager.getAddressFrom(lattitude: GeoLocationManager.shared.currentLatitude ?? "0.0" , longitude: GeoLocationManager.shared.currentLongitude ?? "0.0", language: appDel.currentLanguage, completionHandler: { [weak self](success, address) in
            DispatchQueue.main.async {
                if success {
                    if let adrs = address {
                        if let dictionary = adrs as? Dictionary<String,Any> {
                            let street = dictionary.safeValueForKey("street") as? String ?? ""
                            let township = dictionary.safeValueForKey("township") as? String ?? ""
                            let city = dictionary.safeValueForKey("region") as? String ?? ""
                            //self?.locationOutlet.text = Rabbit.uni2zg(String.init(format: "%@ ,%@ ,%@", street , township + " " + "Township".localized, city))
                            self?.locationOutlet.text = Rabbit.uni2zg(String.init(format: "%@ ,%@ ,%@", street , township + " ", city))
                            //print("Current location called")

                            notfDef.post(name:Notification.Name(rawValue:"PayToStateDivision"),
                                         object: nil,
                                         userInfo: ["Name":  Rabbit.uni2zg(String.init(format: "%@ ,%@ ,%@", street , township + " ", city))])
                            
                        }
                    }
                }
            }
        })
        
        let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
        let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
        self.callNearbyMerchantWithCustomLocation(lat: lat, long: long)
        
    }
    
    
}


extension MapPayToViewController: MainFilterApplyViewDelegate {
    
 func didSelectApplyFilter(category: SubCategoryDetail? , index:Int , str:Int){
    
        print("PAYTO MAP===\(str)---\(index)")
        
            let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
            let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
            self.callNearbyMerchantWithCustomLocationNew(lat: lat, long: long, str: str , index:index)
    
        
    }
    
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
