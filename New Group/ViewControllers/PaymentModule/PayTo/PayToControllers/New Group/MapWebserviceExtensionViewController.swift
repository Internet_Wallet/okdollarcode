//
//  MapWebserviceExtensionViewController.swift
//  OK
//
//  Created by Ashish on 6/25/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

extension MapPayToViewController {
    
  /*  func callNearbyMerchant(distance : String = "" ) {
        let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
        let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
      
        let business: String = ""
        var tempDist = ""
        if distance == "NearBy" {
            tempDist = "200"
        } else {
            tempDist = "\(Int(self.slider.value / 3.2808))"
        }
        
        let strUrl = String.init(format: Url.nearbyMerchantWithPromotions, getMsid(), tempDist,long, business, lat)
        guard let url = URL.init(string: strUrl) else { return }
  
        TopupWeb.genericClass(url: url, param: Dictionary<String,Any>() as AnyObject, httpMethod: "GET") { [weak self](response, success) in
            if success {
                if let jsonData = response as? Dictionary<String,Any> {
                    if let code = jsonData.safeValueForKey("Code") as? Int, code == 200 {
                        if let stringData = jsonData.safeValueForKey("Data") as? String {
                            if let parse = stringData.parseJSONString {
                                if let arrDict = parse as? [Dictionary<String,Any>] {
                                 if arrDict.count > 0 {
                                    self?.clusterManager.removeAll()
                                    self?.merchantModel.removeAll()
                                    for dict in arrDict {
                                        if let jsonData = dict.jsonString().data(using: .utf8) {
                                            if let mapper = try? JSONDecoder().decode(PTMapNearbyMerchant.self, from: jsonData) {
                                                self?.merchantModel.append(mapper)
                                                self?.merchantModelBackup.append(mapper)
                                                
                                            }
                                        }
                                    }
                                    DispatchQueue.main.async {
                                        if let weak = self {
                                            let text = "Near By Merchants".localized
                                            weak.merchantCountglobalLabel.text = "\(text) \(weak.merchantModel.count)"
                                            
                                            weak.listTableView.reloadData()
                                            weak.listViewContainer.isHidden = false
                                            weak.addClustersToMap(modelArray: weak.merchantModel)
                                        }
                                    }
                                 } else
                                 {
                                    //self.mapView.isHidden = true
                                    self!.showErrorAlert(errMessage: "No Record Found".localized)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func callNearbyMerchantWithCustomLocation(lat: String, long: String, business: String = "" ) {
        let distance = "\(Int(self.slider.value / 3.2808))"
        print("callNearbyMerchantWithCustomLocation lat----\(lat)long--\(long)")
        
        let strUrl = String.init(format: Url.nearbyMerchantWithPromotions, getMsid(), distance,long, business, lat)
        guard let url = URL.init(string: strUrl) else { return }
        
        TopupWeb.genericClassWithoutLoader(url: url, param: Dictionary<String,Any>() as AnyObject, httpMethod: "GET") { [weak self](response, success) in
            if success {
                if let jsonData = response as? Dictionary<String,Any> {
                    print("callNearbyMerchantWithCustomLocation----\(jsonData)")
                    if let code = jsonData.safeValueForKey("Code") as? Int, code == 200 {
                        if let stringData = jsonData.safeValueForKey("Data") as? String {
                            if let parse = stringData.parseJSONString {
                                if let arrDict = parse as? [Dictionary<String,Any>] {
                                    if arrDict.count > 0 {
                                        self?.clusterManager.removeAll()
                                        self?.merchantModel.removeAll()
                                        for dict in arrDict {
                                            if let jsonData = dict.jsonString().data(using: .utf8) {
                                                if let mapper = try? JSONDecoder().decode(PTMapNearbyMerchant.self, from: jsonData) {
                                                    self?.merchantModelBackup.append(mapper)
                                                    self?.merchantModel.append(mapper)
                                                    
                                                    
                                                }
                                            }
                                        }
                                        DispatchQueue.main.async {
                                            if let weak = self {
                                                
                                                let text = "Near By Merchants".localized
                                                weak.merchantCountglobalLabel.text = "\(text) \(weak.merchantModel.count)"
                                                weak.listTableView.reloadData()
                                                weak.addClustersToMap(modelArray: weak.merchantModel)
                                            }
                                        }
                                    } else
                                    {
                                        //self.mapView.isHidden = true
                                        self!.showErrorAlert(errMessage: "No Record Found".localized)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
    } */
    
    
    //MARK:- NEW API CALL
    
    func callNearbyMerchantWithCustomLocation(lat: String, long: String)
    {
        println_debug("call ok offices api ")
        
        let url = getUrl(urlStr: "", serverType: .nearByServicesUrl)
        
        //let jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 0, "Latitude" : lat, "Longitude": long, "PhoneNumber" : UserModel.shared.mobileNo, "UserIdNotToConsider": ""] as [String : Any]
        
        let jsonDic:[String : Any]
        
               // var lat = "16.81668401110357"
               // var long = "96.13187085779862"
        
        
        if checkurl ==  "NearBy" {
            
            // jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 300, "Latitude" : lat, "Longitude": long, "PhoneNumber" : UserModel.shared.mobileNo, "UserIdNotToConsider": ""] as [String : Any]
            
            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1500, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":[2,7,8,9,10], "phoneNumberNotToConsider":""] as [String : Any]
            
            
        } else if checkurl ==  "Township"{
            
            // jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 0, "Latitude" : lat, "Longitude": long,"optionalSearchParams":["limit":100,"offset":0,"type":intType], "PhoneNumber" : UserModel.shared.mobileNo,"searchAdditionalParams":["divisionName":selectedLocationDetailStr,"townshipname":selectedTownshipDetailStr] ,"UserIdNotToConsider": ""] as [String : Any]
            
            
            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 3000, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":[2,7,8,9,10], "phoneNumberNotToConsider":""] as [String : Any]
            
        } else {
            
            // jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 0, "Latitude" : lat, "Longitude": long, "PhoneNumber" : UserModel.shared.mobileNo, "UserIdNotToConsider": ""] as [String : Any]
            
            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1000, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":[2,7,8,9,10], "phoneNumberNotToConsider":""] as [String : Any]
            
        }
        
        print("OK$ServiceurlRequest========\(jsonDic)")
        
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url,methodType: kMethod_Post, contentType: kContentType_Json, inputVal: jsonDic, authStr: nil)
        print("OK$ServiceurlRequest========\(urlRequest)")
        
       
        
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { [weak self](isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    //println_debug("remove progress view called inside cashin offices api main call with error")
                }
                //self.showErrorAlert(errMessage: "Try Again".localized)
                return
            }
            //println_debug("response dict for get all offices list :: \(response ?? "")")
            
            var cashArray = [NearByServicesNewModel]()
            DispatchQueue.main.async {
                cashArray = ParserAttributeClass.parseNearbyServiceJSONNew(anyValue: response as AnyObject) as [NearByServicesNewModel]
                //println_debug("response count for ok offices :::: \(cashArray.count)")
                
                if cashArray.count > 0 {
                    self?.clusterManager.removeAll()
                    self?.merchantModel.removeAll()
                    self?.merchantModelBackup.removeAll()

                    //Seperate mechant =2,agent=1,office=7
                    for indexVal in 0..<cashArray.count {
                        let currentModel = cashArray[indexVal]
                    
                        
                        self?.merchantModel.append(currentModel)
                        self?.merchantModelBackup.append(currentModel)
                        
                      /*  if self.intType == currentModel.UserContactData!.type! {
                        
                            print("innertype-------\(currentModel.UserContactData!.type)")
                            self.nearByServicesList.append(currentModel)
                            self.nearByServicesListBackup.append(currentModel)
                            self.cashInListBackUPByCurLoc.append(currentModel)
                        
                        } */
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(8), execute: {
                            progressViewObj.removeProgressView()
                            
                        })
                        
                    }
                    
                    DispatchQueue.main.async {
                        
                       
                        
                        
                        if let weak = self {
                            let text = "Near By Merchants".localized
                           weak.merchantCountglobalLabel.text = "\(text) \(weak.merchantModel.count)"
                          weak.listTableView.reloadData()
                            //weak.addClustersToMap(list: weak.merchantModel)
//                            weak.listViewContainer.isHidden = false
//                            weak.mapView.isHidden = false
//                            weak.listView.isHidden = true
                            
//                            print("**********")
//                            let storyboard : UIStoryboard = UIStoryboard(name: "Map", bundle: nil)
//                            let vc : MapViewController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
//                            vc.merchantModel = weak.merchantModel
//
//                            let navigationController = UINavigationController(rootViewController: vc)
                            
                          //  weak.presentDetail(navigationController)
                         //  weak.present(navigationController, animated: true, completion: nil)
                            
                            
                        }
                    }
                    
                   
                }
                else
                {
                    self?.showErrorAlert(errMessage: "No Record Found".localized)
                    //self.showHideInfoLabel(isShow: true, content: "No Record Found".localized)
                }
            }
        })
    }
    
    
    func callNearbyMerchantWithCustomLocationFirst(lat: String, long: String , str:Int, index:Int)
    {
        
        var type : [Int] = [2,7,8,9,10]
        
        if str == 0 {
            type =  [2,7,8,9,10]
        }
        else if str == 1 {
            type = [8]
        }
        else if str == 2 {
            type = [7]
        }
        else if str == 3 {
            type = [2]

        }
        else if str == 4 {
            type = [9]
            
        }
        else if str == 5 {
            type = [10]
            
        }

        println_debug("call ok offices api ---\(type)")
        
        let url = getUrl(urlStr: "", serverType: .nearByServicesUrl)
        
        //let jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 0, "Latitude" : lat, "Longitude": long, "PhoneNumber" : UserModel.shared.mobileNo, "UserIdNotToConsider": ""] as [String : Any]
        
        let jsonDic:[String : Any]
        
//                var lat = "16.81668401110357"
//                var long = "96.13187085779862"
        
        
        if checkurl ==  "NearBy" {
            
            // jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 300, "Latitude" : lat, "Longitude": long, "PhoneNumber" : UserModel.shared.mobileNo, "UserIdNotToConsider": ""] as [String : Any]
            
            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1500, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":type, "phoneNumberNotToConsider":""] as [String : Any]
            
            
        } else if checkurl ==  "Township"{
            
            // jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 0, "Latitude" : lat, "Longitude": long,"optionalSearchParams":["limit":100,"offset":0,"type":intType], "PhoneNumber" : UserModel.shared.mobileNo,"searchAdditionalParams":["divisionName":selectedLocationDetailStr,"townshipname":selectedTownshipDetailStr] ,"UserIdNotToConsider": ""] as [String : Any]
            
            
            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 3000, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":type, "phoneNumberNotToConsider":""] as [String : Any]
            
        } else {
            
            // jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 0, "Latitude" : lat, "Longitude": long, "PhoneNumber" : UserModel.shared.mobileNo, "UserIdNotToConsider": ""] as [String : Any]
            
            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1000, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":type, "phoneNumberNotToConsider":""] as [String : Any]
            
        }
        
        print("OK$ServiceurlRequest========\(jsonDic)")
        
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url,methodType: kMethod_Post, contentType: kContentType_Json, inputVal: jsonDic, authStr: nil)
        print("OK$ServiceurlRequest========\(urlRequest)")
        
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
        
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { [weak self](isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    //println_debug("remove progress view called inside cashin offices api main call with error")
                }
                //self.showErrorAlert(errMessage: "Try Again".localized)
                return
            }
            println_debug("response dict for get all offices list :: \(response ?? "")")
            progressViewObj.removeProgressView()
            
            var cashArray = [NearByServicesNewModel]()
            DispatchQueue.main.async {
                cashArray = ParserAttributeClass.parseNearbyServiceJSONNew(anyValue: response as AnyObject) as [NearByServicesNewModel]
                println_debug("response count for ok offices :::: \(cashArray.count)")
                progressViewObj.removeProgressView()
                
                if cashArray.count > 0 {
                    self?.clusterManager.removeAll()
                    self?.merchantModel.removeAll()                    //
                    self?.merchantModelBackup.removeAll()

                    //Seperate mechant =2,agent=1,office=7
                    for indexVal in 0..<cashArray.count {
                        let currentModel = cashArray[indexVal]
                        
                        self?.merchantModel.append(currentModel)
                        self?.merchantModelBackup.append(currentModel)
                        
                        
                       
                        
                        /*  if self.intType == currentModel.UserContactData!.type! {
                         
                         print("innertype-------\(currentModel.UserContactData!.type)")
                         self.nearByServicesList.append(currentModel)
                         self.nearByServicesListBackup.append(currentModel)
                         self.cashInListBackUPByCurLoc.append(currentModel)
                         
                         } */
                    }
                    
                    DispatchQueue.main.async {
                        if let weak = self {
                            println_debug("Reload called")
                            
                            switch index {
                            case 0:
                                println_debug("default case falling")
                                
                                //                            self.merchantModel = merchantModelBackup
                                //                            print("datalist list11----\(merchantModel.count)")
                                //                            listTableView.reloadData()
                                
                            case 1:
                                println_debug("Name A to Z")
                                if let list = self!.merchantModel as? [NearByServicesNewModel] {
                                    self!.merchantModel = list.sorted(by: { $0.UserContactData!.BusinessName! <  $1.UserContactData!.BusinessName! })
                                    // merchantModel = list.sorted(by: {$0.UserContactData?.BusinessName < $1.UserContactData?.BusinessName})
                                    print("datalist list11----\(self!.merchantModel.count)")
                                    // self.listTableView.reloadData()
                                }
                                
                            case 2:
                                println_debug("Name Z to A")
                                if let list = self!.merchantModel as? [NearByServicesNewModel] {
                                    self!.merchantModel = list.sorted(by: { $0.UserContactData!.BusinessName! >  $1.UserContactData!.BusinessName! })
                                    // listTableView.reloadData()
                                }
                                
                            default:
                                break
                            }

                            let text = "Near By Merchants".localized
                            weak.merchantCountglobalLabel.text = "\(text) \(weak.merchantModel.count)"
                            
                            weak.listTableView.reloadData()
                            weak.listViewContainer.isHidden = false
                            weak.addClustersToMap(list: weak.merchantModel)
                        }
                    }
                    
                    
                }
                else
                {
                    self?.showErrorAlert(errMessage: "No Record Found".localized)
                    //self.showHideInfoLabel(isShow: true, content: "No Record Found".localized)
                }
            }
        })
    }
    
    
    func callNearbyMerchantWithCustomLocationNew(lat: String, long: String , str:Int, index:Int)
    {
        
        var type : [Int] = [2,7,8,9,10]
        
        if str == 0 {
            type =  [2,7,8,9,10]
        }
        else if str == 1 {
            type = [8]
        }
        else if str == 2 {
            type = [7]
        }
        else if str == 3 {
            type = [2]
            
        }
        else if str == 4 {
            type = [9]
            
        }
        else if str == 5 {
            type = [10]
            
        }
        
        println_debug("call ok offices api ---\(type)")
        
        if self.merchantModelBackup.count > 0 {
            self.clusterManager.removeAll()
            self.merchantModel.removeAll()
            
            //Seperate mechant =2,agent=1,office=7
            for indexVal in 0..<self.merchantModelBackup.count {
                
                let currentModel = self.merchantModelBackup[indexVal]
                
                if str == 0 {
                
                self.merchantModel.append(currentModel)

                } else {
                    
                var value = type[0]
                  if value == currentModel.UserContactData!.type! {
                 
                 print("innertype-------\(currentModel.UserContactData!.type)")
                    self.merchantModel.append(currentModel)
                 
                 }
                    
                }
            }
            
            DispatchQueue.main.async {
                    println_debug("Reload called")
                    
                    switch index {
                    case 0:
                        println_debug("default case falling")
                        
                        //                            self.merchantModel = merchantModelBackup
                        //                            print("datalist list11----\(merchantModel.count)")
                        //                            listTableView.reloadData()
                        
                    case 1:
                        println_debug("Name A to Z")
                        if let list = self.merchantModel as? [NearByServicesNewModel] {
                            
                            var name1 : String = ""
                            var name2 : String = ""
                            
                            self.merchantModel = list.sorted {
                                
                                if $0.UserContactData!.BusinessName != "" {
                                    name1 = $0.UserContactData!.BusinessName!
                                } else {
                                    name1 = $0.UserContactData!.FirstName!
                                }
                                
                                if $1.UserContactData!.BusinessName != "" {
                                    name2 = $1.UserContactData!.BusinessName!
                                } else {
                                    name2 = $1.UserContactData!.FirstName!
                                }
                                return name1.compare(name2) == .orderedAscending
                            }
                         
                        }
                        
                    case 2:
                        println_debug("Name Z to A")
                        if let list = self.merchantModel as? [NearByServicesNewModel] {
                            
                            var name1 : String = ""
                            var name2 : String = ""
                            
                            self.merchantModel = list.sorted {
                                
                                if $0.UserContactData!.BusinessName != "" {
                                    name1 = $0.UserContactData!.BusinessName!
                                } else {
                                    name1 = $0.UserContactData!.FirstName!
                                }
                                
                                if $1.UserContactData!.BusinessName != "" {
                                    name2 = $1.UserContactData!.BusinessName!
                                } else {
                                    name2 = $1.UserContactData!.FirstName!
                                }
                                return name1.compare(name2) == .orderedDescending
                            }
                            
                           
                        }
                        
                    default:
                        break
                    }
                    
                    let text = "Near By Merchants".localized
                    self.merchantCountglobalLabel.text = "\(text) \(self.merchantModel.count)"
                    
                    self.listTableView.reloadData()
                    self.listViewContainer.isHidden = false
                    self.addClustersToMap(list: self.merchantModel)
                
            }
            
            
        }
        else
        {
            self.showErrorAlert(errMessage: "No Record Found".localized)
            //self.showHideInfoLabel(isShow: true, content: "No Record Found".localized)
        }
        
        
        
    }
    
}

extension String {
    var parseJSONString: AnyObject? {
        let data = self.data(using: .utf8)
        if let jsonData = data {
            if let json = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) {
                return json as AnyObject
            }
            return nil
        } else {
            return nil
        }
    }
}

