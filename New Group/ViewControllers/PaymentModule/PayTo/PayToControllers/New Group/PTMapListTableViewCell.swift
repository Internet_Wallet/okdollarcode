//
//  PTMapListTableViewCell.swift
//  OK
//
//  Created by Ashish on 6/25/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol PTMapListTableViewCellDelegate: class {
    func callToMerchant(index: IndexPath)
}

class PTMapListTableViewCell: UITableViewCell {

    @IBOutlet weak var distance: UILabel!{
        didSet{
            textSecond.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var name: UILabel!{
        didSet{
            name.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var textFirst: UILabel!{
        didSet{
            textSecond.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var textSecond: UILabel!{
        didSet{
            textSecond.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var textThird: UILabel!{
        didSet{
            textSecond.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var callBtn: UIButton!
    
    var index : IndexPath!
    
    weak var delegate : PTMapListTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func callButtonAction(_ sender: UIButton) {
        if index != nil {
            delegate?.callToMerchant(index: index)
        }
    }
    
    func wrapModel(promotion: NearByServicesNewModel) {
        
       /*
        if let name = model.UserContactData?.BusinessName {
            self.name.text = name
        }
        
        if let distance =  model.distanceInKm {
           // let roundValue = (distance as NSString).floatValue
            let roundValue = distance
            if roundValue > 1000 {
                let stringDistance = String.init(format: "%.2f Km", roundValue/1000)
                self.distance.text = stringDistance
            } else {
                let stringDistance = String.init(format: "%.2f m", roundValue)
                self.distance.text = stringDistance
            }
        }
        
      /*  if let address = model.UserContactData?.Address, let town = address.townShip, let name = town.name {
            self.textFirst.text = name
        }
        if model.shopOpenTime == "<null>" {
            self.textSecond.text =  "NA"
        } else {
            self.textSecond.text = model.shopOpenTime ?? "NA"
        }
        if let promo = model.promotion, promo.count > 0, let promoObj = promo.first {
            if promoObj.header.safelyWrappingString() == "<null>" {
                self.textThird.text = "NA"
            } else {
                self.textThird.text = promoObj.header ?? "NA"
            }
        } */
        
        if let addressnew = model.UserContactData?.Address {
            if addressnew.count > 0 {
                
                self.textFirst.text = "\(addressnew)"
                
                // listCell.locationLbl.text = "\(addressnew),\( promotion.UserContactData!.Township!),\( promotion.UserContactData!.Division!)"
            }else {
                self.textFirst.text = "\(model.UserContactData!.Township!),\(model.UserContactData!.Division!)"
            }
        }
        
        if model.UserContactData!.OfficeHourFrom == "<null>" {
            self.textSecond.text =  "NA"
        } else {
            self.textSecond.text = model.UserContactData!.OfficeHourFrom ?? "NA"
        } */
        
        
        
        
        
    }
        
        
        
        
    

}


//MARK:- List TableView Datasource & Delegate
extension MapPayToViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return merchantModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       // let listCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: MapListCell.self), for: indexPath) as! MapListCell
        
         //let listCell = tableView.dequeueReusableCell(withIdentifier: "maplistcellidentifier", for: indexPath) as! MapListCell
        
        let identifier = "maplistcellidentifier"
        var listCell: MapListCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? MapListCell
        if listCell == nil {
            tableView.register (UINib(nibName: "MapListCell", bundle: nil), forCellReuseIdentifier: identifier)
            listCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? MapListCell
        }

        let promotion = self.merchantModel[indexPath.row]
        listCell.callBtn.addTarget(self, action: #selector(callBtnAction(_:)), for: UIControl.Event.touchUpInside)
        listCell.photoImgView.image = UIImage(named: "cicoCamera")
        if let imageStr = promotion.UserContactData?.ImageUrl, let imageUrl = URL(string: imageStr) {
            listCell.photoImgView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "cicoCamera"))
        }
        //listCell.photoImgView.image = UIImage.init(named: "cicoCamera")
//        listCell.photoImgView.layer.borderWidth = 1.0
//        listCell.photoImgView.layer.masksToBounds = false
//        listCell.photoImgView.layer.borderColor = UIColor(red: 246/255, green: 198/255, blue: 0/225, alpha: 1).cgColor
//        listCell.photoImgView.layer.cornerRadius = listCell.photoImgView.frame.size.width / 2//
    //    listCell.photoImgView.clipsToBounds = true
        
        listCell.nameLbl.text = promotion.UserContactData?.BusinessName
        
        //        let str = promotion.phoneNumber!
        //        let agentCode = str.replacingOccurrences(of: "0095", with: "+95")
        //        listCell.phonenumberLbl.text = agentCode
        
        
        /* let agentCodenew  = getPlusWithDestinationNum(promotion.phoneNumber!, withCountryCode: "+95")
         listCell.phonenumberLbl.text = agentCodenew */
        
        let agentCodenew  = getPlusWithDestinationNum((promotion.UserContactData!.PhoneNumber)!, withCountryCode: "+95")
        listCell.phonenumberLbl.text = agentCodenew
        
        //listCell.phonenumberLbl.text = promotion.UserContactData?.PhoneNumber
        
        
        if(promotion.UserContactData?.BusinessName!.count == 0)
        {
            listCell.nameLbl.text = promotion.UserContactData?.FirstName
        }
        
        if let openTime = promotion.UserContactData!.OfficeHourFrom , let closeTime = promotion.UserContactData!.OfficeHourTo {
            
            //            if(promotion.isOpenAlways == true)
            //            {
            //                listCell.timeImgView.isHidden = false
            //                listCell.timeLbl.isHidden = false
            //                listCell.timeLblHeightConstraint.constant = 20
            //                listCell.timeImgView.image = UIImage.init(named: "time")
            //                listCell.timeLbl.text = "Open 24/7"
            //            }
            //            else
            //            {
            if openTime.count > 0 && closeTime.count > 0 {
                listCell.timeImgView.isHidden = false
                listCell.timeLbl.isHidden = false
                listCell.timeLblHeightConstraint.constant = 20
                //listCell.timeImgView.image = UIImage.init(named: "time")
                listCell.timeLbl.text = "\(listCell.getTime(timeStr: openTime)) - \(listCell.getTime(timeStr: closeTime))"
            }else {
                listCell.timeImgView.isHidden = true
                listCell.timeLbl.isHidden = true
                listCell.timeLblHeightConstraint.constant = 0
            }
            // }
        }
        listCell.locationImgView.image = UIImage.init(named: "location.png")
        if let addressnew = promotion.UserContactData?.Address {
            if addressnew.count > 0 {
                
                listCell.locationLbl.text = "\(addressnew)"
                
                
                // listCell.locationLbl.text = "\(addressnew),\( promotion.UserContactData!.Township!),\( promotion.UserContactData!.Division!)"
            }else {
                listCell.locationLbl.text = "\(promotion.UserContactData!.Township!),\(promotion.UserContactData!.Division!)"
            }
        }
        
        //CashIn And CashOut
        
        if let cashinamount = promotion.UserContactData?.CashInLimit , let cashoutamount = promotion.UserContactData?.CashOutLimit {
            
            if cashinamount > 0 {
                let phone =  self.getNumberFormat("\(promotion.UserContactData!.CashInLimit!)").replacingOccurrences(of: ".00", with: "")
                listCell.cashInLbl.text = "Maximum Cash In Amount".localized + " : \(phone)" + " MMK"
                listCell.cashInLblHeightConstraint.constant = 20
            } else {
                listCell.cashInLblHeightConstraint.constant = 0
            }
            
            if cashoutamount > 0 {
                let phonenew =  self.getNumberFormat("\(promotion.UserContactData!.CashOutLimit!)").replacingOccurrences(of: ".00", with: "")
                listCell.cashOutLbl.text = "Maximum Cash Out Amount".localized + " : \(phonenew)" + " MMK"
                listCell.cashOutLblHeightConstraint.constant = 20
            } else{
                listCell.cashOutLblHeightConstraint.constant = 0
            }
        }
        
        // listCell.remarksLbl.text = ""
        listCell.callBtn.setImage(UIImage.init(named: "call.png"), for: UIControl.State.normal)
        listCell.distanceLblKm.text = "(0 Km)"
        if let shopDistance = promotion.distanceInKm {
            listCell.distanceLblKm.text = String(format: "(%.2f Km)", shopDistance).replacingOccurrences(of: ".00", with: "")
        }
        
        listCell.durationLbl.text = listCell.durationFromDistance(distance: promotion.distanceInKm) as String

        return listCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 250
        tableView.rowHeight = UITableView.automaticDimension
        return tableView.rowHeight
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if let model = merchantModel[safe: indexPath.row] {
//            let storyboard: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
//            let protionDetailsView = storyboard.instantiateViewController(withIdentifier: "PromotionDetailView_ID") as! PromotionDetailViewController
//
//            //prabu
//            if let model = self.merchantModel[safe: indexPath.row] {
//                if let distance =  model.distanceInKm {
//                   // let roundValue = (distance as NSString).floatValue
//                    let roundValue = distance
//                    if roundValue > 1000 {
//                        let stringDistance = String.init(format: "%.2f Km", roundValue/1000)
//                        protionDetailsView.distance = stringDistance
//                    } else {
//                        let stringDistance = String.init(format: "%.2f m", roundValue)
//                        protionDetailsView.distance = stringDistance
//                    }
//                }
//            }
//
//            protionDetailsView.selectedNearByServicesNew = model
//            protionDetailsView.strNearByPromotion = "NearByService"
//
////            self.navigationController?.pushViewController(protionDetailsView, animated: true)
//            self.navigation?.pushViewController(protionDetailsView, animated: true)
////            PaytoConstants.global.navigation?.pushViewController(protionDetailsView, animated: true)
//        }
        
        
        
        
        
        
     
  //   self.navigationController?.viewControllers.append(vc)
    //  self.presentDetail(vc)
//      let navigationController = UINavigationController(rootViewController: vc)
//      self.presentDetail(navigationController)
        
        
        
    }
    
    // call to number
    func callToMerchant(index: IndexPath) {
        if let model = self.merchantModel[safe:index.row] {
            var number = model.UserContactData?.PhoneNumber.safelyWrappingString()
            if number!.hasPrefix("00") {
                number = "+" + number!
            }
            let phone = "tel://\(number)"
            if let url = URL.init(string: phone) {
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
            
        }
    }
    
    
    @objc func callBtnAction(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to: self.listTableView)
        if let indexPath = self.listTableView.indexPathForRow(at: buttonPosition), self.merchantModel.count > 0 {
            let selectedpromotion = self.merchantModel[indexPath.row]
            let promotion = selectedpromotion
            if let phonenumber = promotion.UserContactData?.PhoneNumber {
                if phonenumber.count > 0 && phonenumber.count < 20 {
                    var mobNumber = getPhoneNumberByFormat(phoneNumber: phonenumber)
                    mobNumber = mobNumber.removingWhitespaces()
                    let phone = "tel://\(mobNumber)"
                    if let url = URL.init(string: phone) {
                        UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    }
                }
            }
        }else {
            alertViewObj.wrapAlert(title: nil, body: "No Phonenumber Exists".localized, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    
}




// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
