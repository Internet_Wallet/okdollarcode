//
//  CurrentLocationViewController.swift
//  OK
//
//  Created by Ashish on 6/24/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol CurrentLocationViewControllerDelegate  {
    func didSelectLocation(withOptions option: SelectLocationEnum)
}

class CurrentLocationViewController: PayToBaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var currentTableView: UITableView!
    
    weak var delegate : SelectLocationDelegate?
    var delegateData : CurrentLocationViewControllerDelegate?

    var imageArray : [UIImage]?
    var labelArray : [String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Select Location".localized
        
//        imageArray = [#imageLiteral(resourceName: "current_location"), #imageLiteral(resourceName: "divisionmap"), #imageLiteral(resourceName: "findmap"), #imageLiteral(resourceName: "promotionmap"),#imageLiteral(resourceName: "destination-iconmap")]
//        labelArray = ["Current Location", "State/Division", "Find Nearby Merchants", "Find All Merchants Promotion", "Find All Merchants"]
        
        imageArray = [#imageLiteral(resourceName: "current_location"), #imageLiteral(resourceName: "divisionmap"), #imageLiteral(resourceName: "findmap")]
        labelArray = ["Current Location", "State/Division", "Find Nearby Merchants"]
        
        self.currentTableView.tableFooterView = UIView.init()

        
        let buttonIcon      = UIImage(named: "tabBarBack")
        let leftBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: self, action: #selector(CurrentLocationViewController.popVC(_:)))
        leftBarButton.image = buttonIcon
        self.navigationItem.leftBarButtonItem = leftBarButton

        // Do any additional setup after loading the view.
    }
    
    @objc func popVC(_ sender:UIBarButtonItem!) {
        self.navigationController?.popViewController(animated: true)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (imageArray == nil) ? 0 : imageArray!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: CurrentLocationCell.self), for: indexPath) as? CurrentLocationCell
        if let label = labelArray, let images = imageArray {
            cell?.wrapData(string: label[indexPath.row], withImage: images[indexPath.row])
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.00
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            switch indexPath.row {
            case 0:
                self.delegateData?.didSelectLocation(withOptions: .currentLocation)
            case 1:
                self.delegateData?.didSelectLocation(withOptions: .division)
            case 2:
                self.delegateData?.didSelectLocation(withOptions: .nearbyMerchants)
            case 3:
                self.delegateData?.didSelectLocation(withOptions: .merchantWithPromotions)
            case 4:
                self.delegateData?.didSelectLocation(withOptions: .allMerchants)
            default:
                self.delegateData?.didSelectLocation(withOptions: .currentLocation)
            }
            self.navigationController?.popViewController(animated: true)
    }

}

//MARK:- Current Location cell

class CurrentLocationCell : UITableViewCell {
    @IBOutlet weak var labelKey: UILabel!
    @IBOutlet weak var imageViewKey: UIImageView!
    
    func wrapData(string: String, withImage img: UIImage) {
        self.labelKey.text = string.localized
        self.imageViewKey.image = img
    }
    
    
}
