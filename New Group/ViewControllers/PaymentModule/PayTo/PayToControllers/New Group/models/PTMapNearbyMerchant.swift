//
//	PTMapNearbyMerchant.swift
//
//	Create by Ashish Kumar on 28/6/2018
//	Copyright © 2018. All rights reserved.

import Foundation

struct PTMapNearbyMerchant: Codable {
    let address: PTMapNearbyMerchantAddress?
    let agentID, agentName, agentSettingsCashInCashOut, agentType: String?
    let businessCategory: PTMapNearbyMerchantBusiness?
    let geoLocation, geoLocationShop: PTMapNearbyMerchantGeoLocation?
    let isActive, isPromotion, phoneNumber: String?
    let promotion: [PTMapNearbyMerchantPromotion]?
    let shopCity, shopCloseTime, shopDivision: String?
    let shopHolidays: [String]?
    let shopID, shopInMeter, shopName, shopOpenTime: String?
    let shopTownShip: String?
    
    enum CodingKeys: String, CodingKey {
        case address
        case agentID = "agentId"
        case agentName, agentSettingsCashInCashOut, agentType, businessCategory, geoLocation, geoLocationShop, isActive, isPromotion, phoneNumber, promotion, shopCity, shopCloseTime, shopDivision, shopHolidays
        case shopID = "shopId"
        case shopInMeter, shopName, shopOpenTime, shopTownShip
    }
}

struct PTMapNearbyMerchantAddress: Codable {
    let addressLine1, addressLine2, country: String?
    let state, townShip: PTMapNearbyMerchantState?
}

struct PTMapNearbyMerchantState: Codable {
    let burmeseName, code, isDivision, name: String?
}

class PTMapNearbyMerchantBusiness: Codable {
    let burmeseName: String?
    let businessType: PTMapNearbyMerchantBusiness?
    let code, logo, name: String?
    
    init(burmeseName: String?, businessType: PTMapNearbyMerchantBusiness?, code: String?, logo: String?, name: String?) {
        self.burmeseName = burmeseName
        self.businessType = businessType
        self.code = code
        self.logo = logo
        self.name = name
    }
}

struct PTMapNearbyMerchantGeoLocation: Codable {
    let latitude, longitude: String?
}

struct PTMapNearbyMerchantPromotion: Codable {
    let header, id, templateID, templateName: String?
    
    enum CodingKeys: String, CodingKey {
        case header, id
        case templateID = "templateId"
        case templateName
    }
}


