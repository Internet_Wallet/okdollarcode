//
//  MapPayToViewController.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/27/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit


class MapPayToViewController: UIViewController, PTIndicatorInfoProvider {
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PTPagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo.init(title: "Map")
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
