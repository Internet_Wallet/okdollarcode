//
//  PaySendPaymentExtension.swift
//  OK
//
//  Created by Ashish on 8/7/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

extension PaySendViewController {
    //MARK:- CashBack Payment
    
    func initiateCashbackPayment() {
        guard appDel.checkNetworkAvail() else {
            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.noInternet.localized)
            return
        }
       guard let url = PTManagerClass.createUrlComponents(base: PaytoConstants.url.multiCashback.safelyWrappingString()) else { return }
        self.cashbackApi(url)
        
    }
    
    //
    func cashbackApi(_ url: URL) {
        //
        let params = self.multiCashbackParameters()
        println_debug("Check Agent url : \(url)")
        println_debug(String(data: try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted), encoding: .utf8 )!)

        TopupWeb.genericClass(url: url, param: params as AnyObject, httpMethod: "POST") { (response, success) in
            DispatchQueue.main.sync {
                if success {
                    guard let arrayResponse = response as? [Any]  else { return }
                    for (index,subItem) in arrayResponse.enumerated() {
                        guard let dictionary = subItem as? Dictionary<String,Any> else { return }
                        if dictionary.safeValueForKey("Code") as? Int == 200 {
                            guard let xmlString = dictionary.safeValueForKey("Data") as? String else { return }
                            self.parsingCashbackData(xml: xmlString, withIndex: index)
                        }
                    }
                    DispatchQueue.main.async {
                        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "PayToTransactionConfirmationVC") as? PayToTransactionConfirmationVC else { return }
                        vc.displayResponse = self.transactionArray
                        if let mode = self.model, let object = mode.getObjects().first {
                            vc.varType = object.getTransactionType
                        } else {
                            vc.varType = PayToType(type: .payTo)
                        }
                        
                        vc.viewModel = self.model!.getObjects()
                        if self.transactionArray.count > 0 {
                            PaytoConstants.global.navigation?.pushViewController(vc, animated: false)
                            self.transactionArray.removeAll()
                        }
                    }
                }
            }
        }
    }
    
    func parsingCashbackData(xml: String, withIndex index: Int) {
        let xmlIndexer = SWXMLHash.parse(xml)
        self.enumerate(indexer: xmlIndexer) { (dictionary) in
            DispatchQueue.main.async {
                let selfModel = self.model?.getObjects()[index]
                var transaction = dictionary
                transaction["businessName"]    = selfModel?.merchant.text ?? ""
                transaction["destinationName"]  = selfModel?.subscriber.text ?? ""
                
                if (selfModel?.subscriber.text ?? "").lowercased() == "Unregistered User".lowercased() {
                    transaction["businessName"] = ""
                }
                if selfModel?.transType.type == .cashOut {
                    transaction["amount"] = (selfModel?.amount.text ?? "").replacingOccurrences(of: ",", with: "")
                }
                transaction["commission"] = (selfModel?.commissionTxt.text ?? "").replacingOccurrences(of: ",", with: "")
                transaction["totalAmount"] = (selfModel?.totalAmountTxt.text ?? "").replacingOccurrences(of: ",", with: "")
                transaction["localRemark"]     = selfModel?.remarks.text ?? ""
                transaction["CICOPayTo"] = self.transType
                if (transaction["resultdescription"] as? String)?.lowercased() == "Transaction Successful".lowercased() {
                    self.transactionArray.append(transaction)
//                    selfModel?.merchant.text = ""
                } else {
                    let msgs = transaction.safeValueForKey("resultdescription") as? String
                    PaytoConstants.alert.showErrorAlert(title: nil, body: msgs.safelyWrappingString().localized)
                }
            }
        }
    }
    
    func enumerate(indexer: XMLIndexer, handle :@escaping (_ dict: Dictionary<String,Any>) -> Void) {
        var dictionary : Dictionary<String,Any> = Dictionary<String,Any>()

        func indexing(indexer: XMLIndexer) {
            for child in indexer.children {
                guard let element = child.element else { return }
                dictionary[element.name] = element.text
                indexing(indexer: child)
            }
        }
        
        indexing(indexer: indexer)
        handle(dictionary)
    }

    
    func multiCashbackParameters() -> Dictionary<String,Any> {
        var finalObject =  Dictionary<String,AnyObject>()
        var dict        = [Dictionary<String,String>]()
        
        guard let model = self.model else { return finalObject }
        
        let objects = model.getObjects()
        
        for dataVC in objects {
            var cashbackobj = Dictionary<String,String>()
            cashbackobj["Agentcode"]     = UserModel.shared.mobileNo
            if dataVC.transType.type == .cashOut {
                cashbackobj["Amount"]       = dataVC.formattedTotalAmount
            } else {
                cashbackobj["Amount"]       = dataVC.formattedAmount
            }
            cashbackobj["Clientip"]       = OKBaseController.getIPAddress()
            cashbackobj["Clientos"]       = "iOS"
            cashbackobj["Clienttype"]      = "GPRS"
            cashbackobj["Destination"]     = dataVC.formattedNumber
            cashbackobj["Pin"]           = ok_password ?? ""
            cashbackobj["Requesttype"]    = "FEELIMITKICKBACKINFO"
            cashbackobj["Securetoken"]    = UserLogin.shared.token
            cashbackobj["Transtype"]      = self.getTransactionString(dataVC.getTransactionType)
            cashbackobj["Vendorcode"]     = "IPAY"
            dict.append(cashbackobj)
        }
        
        var log = Dictionary<String,Any>()
        log["MobileNumber"] = UserModel.shared.mobileNo
        log["Msid"]        = getMsid()
        log["Ostype"]      = 1
        log["Otp"]        = ""
        log["SimId"]      = uuid
        
        finalObject["CashBackRequestList"] = dict as AnyObject
        finalObject["Login"] = log as AnyObject
        return finalObject
    }
    
    
    //MARK:- Check Agent code
    func checkAgentCode(with agentCode: String) {
        
        if agentCode.hasPrefix(UserModel.shared.mobileNo) {
            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.ownMobileNumber.localized)
            return
        }
        
        guard appDelegate.checkNetworkAvail() else {
            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.noInternet.localized)
            return
        }
        
        let baseString = PaytoConstants.url.checkAgentCode.safelyWrappingString()
        
        let mobile = URLQueryItem.init(name: "MobileNumber", value: agentCode)
        let source = URLQueryItem.init(name: "SourceNumber", value: UserModel.shared.mobileNo)
        let transactionType = URLQueryItem.init(name: "TransType", value: "PAYTO")
        let appBuildNumber = URLQueryItem.init(name: "AppBuildNumber", value: buildNumber)
        let osType  = URLQueryItem.init(name: "OsType", value: "1")
        
        let itemsArray = [mobile, source, transactionType, appBuildNumber, osType]
        
        guard let url = PTManagerClass.createUrlComponents(base: baseString, queryItems: itemsArray) else {
            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.pleaseTryAgain.localized)
            return
        }
        
        TopupWeb.genericClass(url: url, param: Dictionary<String,Any>() as AnyObject, httpMethod: "GET") { (response, success) in
            if success {
                DispatchQueue.main.async {
                    self.formatAgentCodeResponse(response, agent: agentCode)
                }
            } else {
                PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.pleaseTryAgain.localized)
            }
        }
        
    }
    
    func formatAgentCodeResponse(_ response: Any, agent: String) {
        guard let dictionary = response as? Dictionary<String,Any> else { return }
        guard let dataString = dictionary.safeValueForKey("Data") as? String else { return }
        guard dataString.count > 0 else { return }
        guard let dictionaryJson = OKBaseController.convertToDictionary(text: dataString) else { return }
        
        guard let vc = self.model?.getObjects().first else { return }
        
        let agentModel = PTAgentModel(withDictionary: dictionaryJson)
       
        let number   = agent
        
        var category = "Payto"

        if agentModel.localTransType.safelyWrappingString().lowercased() == "Restaurant".lowercased() {
            category = "Restaurant"
        }
        
        let merchant = agentModel.mercantName ?? "Unknown"
        let business = agentModel.businessName ?? "Unknown"
        let amount   = vc.amount.text?.replacingOccurrences(of: ",", with: "")
        let remarks  = vc.remarks.text
        //let accountType = vc.accountType
        let accountType  = UitilityClass.returnAccountType(type: vc.accountType)

        let confirmationModel = OfferConfirmationScreen(mobileNumber: number, category: category, merchant: merchant, businessName: business, amount: amount, remarks: remarks, accountType: accountType,  offers: self.offerModel)
        
        let offerStory = UIStoryboard(name: "Offers", bundle: Bundle.main)
        guard let offerConfirm = offerStory.instantiateViewController(withIdentifier: "OffersConfirmationViewController") as? OffersConfirmationViewController else { return }
        offerConfirm.offerConfirmModel = confirmationModel
        PaytoConstants.global.navigation?.pushViewController(offerConfirm, animated: true)
        
    }
    
}
