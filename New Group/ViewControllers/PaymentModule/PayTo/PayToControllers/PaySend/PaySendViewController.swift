//
//  PaySendViewController.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/27/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//
        
import UIKit

enum selectionType {
    case name
    case number
}

enum MultiSelectionType {
    case payto, multi, selfUpdate
}

//enum PaytoIDTransactionType {
//    case paytoWithID, paytoWithoutID, taxi, bus, toll, ferry, transferTo, cashInPayto, cashOutPayto
//}

//These all are the types of pay to
class PayToType {
    enum ModuleType {
        case payTo
        case cashIn
        case cashOut
        case transferTo
        case toll
        case ticket
        case cashInFree
        case cashOutFree
    }
    
    //hide my number then only payto id
    enum TransType {
        case payTo, payToID
    }
    enum Category: String {
        case payTo = "PayTo", restaurant = "Restaurant", superMarket = "SuperMarket"
        case shop = "Shop", fuel = "Fuel", entrance = "Entrance", parking = "Parking"
        case taxi = "Taxi", toll = "Toll", bus = "Bus", ferry = "Ferry"///removed items but still keeping
        case none = ""
    }
    
    var type: ModuleType
    
    var transType = TransType.payTo
    var paymentType: String {
        switch self.type {
        case .payTo, .toll, .ticket:
            return ""
        case .cashIn:
            return "Cash In"
        case .cashOut:
            return "Cash Out"
        case .transferTo:
            return "Transfer To"
        case .cashInFree:
            return "Cash In"
        case .cashOutFree:
            return "Cash Out"
        }
    }
    var categoryType = Category.none
    
    init(type: ModuleType, transType: TransType = .payTo) {
        self.type = type
    }
}

class PaySendViewController: OKBaseController, UITableViewDelegate, UITableViewDataSource, PaytoHeightChanged, TipMerchantViewControllerDelegate, ScrollChangeDelegate, ContactPickerDelegate, FavoriteSelectionDelegate, BioMetricLoginDelegate, MultiPaymentDelegate, submitTextChange {
    
    func checkForSameNumberSameAmount() {
        self.submitBtn.isHidden = true
    }
    
    var nearByServicesList = [NearByServicesNewModel]()
    @IBOutlet weak var nearByAgentTableView: UITableView! {
        didSet {
            nearByAgentTableView.isHidden = true
        }
    }
    @IBOutlet weak var nearbyAgentBtn: UIButton! {
        didSet {
            self.nearbyAgentBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.nearbyAgentBtn.isHidden = true
        }
    }
    
    @IBOutlet weak var nearbyAgentView: UIView! {
        didSet {
            self.nearbyAgentView.isHidden = true
            self.nearbyAgentView.layer.cornerRadius = 20.0
            self.nearbyAgentView.layer.masksToBounds = true
        }
    }
    
    @IBOutlet weak var nearbyAgentLbl: UILabel! {
        didSet {
            self.nearbyAgentLbl.font = UIFont(name: appFont, size: appFontSize)
            self.nearbyAgentLbl.textColor = .white
            self.nearbyAgentLbl.text = "Nearby OK $ Agents".localized
        }
    }
    
    @IBOutlet weak var nearbyAgentImageView: UIImageView!
    
    @IBOutlet weak var nearByAgentTableViewHeight: NSLayoutConstraint! {
        didSet {
           nearByAgentTableViewHeight.constant = 150.0
        }
    }
    @IBOutlet weak var nearbyAgentBtnHeight: NSLayoutConstraint! {
        didSet {
            nearbyAgentBtnHeight.constant = 40.0
        }
    }

    //these will be main table view
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var submitBtn: UIButton! {
        didSet {
            self.submitBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.submitBtn.setTitle("Submit".localized, for: .normal)
        }
    }
    
    var submitView: UIView {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 50))
        let btn = UIButton.init(frame: view.frame)
        btn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        btn.backgroundColor = kYellowColor
        if self.transType.type == .cashIn || self.transType.type == .cashInFree {
            btn.setTitle("Generate Cash In QR".localized, for: .normal)
        } else {
            btn.setTitle("Submit".localized, for: .normal)
        }
        if let myFont = UIFont(name: appFont, size: 18) {
            btn.titleLabel?.font =  myFont
        }
        btn.setTitleColor(UIColor.init(hex: "#0E10CB"), for: .normal)
        btn.addTarget(self, action: #selector(submit_PayAction(_:)), for: UIControl.Event.touchUpInside)
        view.addSubview(btn)
        return view
    }
    
    var multiBtnYPos: CGFloat = 0.0
  
    //cell object
    var model : PaytoObject<PayToUIViewController>?
    
    var cells : PaytoObject<PaysendHolderCell>?
    
    //var transType : PaytoIDTransactionType = PaytoIDTransactionType.paytoWithID
    var transType: PayToType = PayToType(type: .payTo)
    
    var cashbackModel : [PTCashbackModel] = [PTCashbackModel]()
    
    var transactionArray = [Dictionary<String,Any>]()
    
    var multiButton : ActionButton?

    var otherRequirement : (number: String, name: String, amount: String, remark: String, cashInQR: Bool, referenceNumber: String)?
    
    var isOfferEnabled: Bool = false
    var offerModel: OffersListModel?
    var oldOfferModel: OffersListModel?
    var offerListModel: [OffersListModel]?
    var offerFilterListModel: [OffersListModel]?

    //MARK: Ads Image
    @IBOutlet weak var advView: UIView!
    @IBOutlet weak var adViewImage: UIImageView!
    @IBOutlet weak var heightConstraints: NSLayoutConstraint!
    
    //MARK:- offer list view
    @IBOutlet weak var offerListView: CardDesignView!
    @IBOutlet weak var offerTableView: UITableView!
    @IBOutlet weak var btnOfferBack: UIButton!{
        didSet{
            btnOfferBack.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnOfferBack.setTitle("Back".localized, for: .normal)
        }
    }
    
    @IBOutlet weak var lblOfferCode: UILabel!{
        didSet{
            self.lblOfferCode.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
     //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
   //     NotificationCenter.default.addObserver(self, selector: #selector(self.bioAuth), name: NSNotification.Name(rawValue: "PAYTO_LOGIN"), object: nil)
        
        
        
        offerFilterListModel = [OffersListModel]()

        let className = String(describing: OffersTableViewCell.self)
        self.offerTableView.register(UINib.init(nibName: className, bundle: Bundle.main), forCellReuseIdentifier: className)
        self.offerTableView.delegate = self
        self.offerTableView.tableFooterView = UIView()
        
        oldOfferModel = offerModel
        freeQty = offerModel?.freeQty ?? 0
        buyQty = offerModel?.buyingQty ?? 0
        discountAmount = offerModel?.value ?? 0
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "PayToUIViewController") as? PayToUIViewController else { return }
        vc.delegate = self
        vc.transType      = self.transType
        vc.isOfferEnabled = isOfferEnabled
        vc.offerModel     = offerModel
        
        vc.view.frame = vc.view.frame
        model = PaytoObject<PayToUIViewController>(initial: vc)
        
        // cell initialisation ()
        let indexPath  = IndexPath.init(row: 0, section: 0)
        guard let cell = self.mainTableView.dequeueReusableCell(withIdentifier: "identifierPaytoCell", for: indexPath) as? PaysendHolderCell else { return }
        model!.getObjects()[indexPath.row].indexPath = indexPath
        cell.updateCell(model!.getObjects()[indexPath.row].view)
        self.cells = PaytoObject<PaysendHolderCell>(initial: cell)
        
        self.mainTableView.tableFooterView = UIView.init()
        
        self.submitBtn.isHidden = true
        if self.transType.type == .cashIn || self.transType.type == .cashInFree {
            self.submitBtn.setTitle("Generate Cash In QR".localized, for: .normal)
        }
        
        if isOfferEnabled {
            self.multiButton?.hide()
        } else {
            self.multiButton?.show()
        }
        
        if let required = otherRequirement {
            let object = PTScanObject.init(amt: required.amount, nme: required.name, agent: required.number, remark: required.remark, cashInQR: required.cashInQR, referenceNumber: required.referenceNumber)
            
            vc.changeScrollPage(point: 0, withScanObject: object)
        }
        self.addingFloatingButtons()

        if !isOfferEnabled {
            vc.mobileNumber.becomeFirstResponder()
        } else {
            if self.offerModel?.paymentType == "0" {//Direct
                vc.amount.becomeFirstResponder()
            } else {
                   self.lblOfferCode.text = "Cannot proceed with this promocode".localized + " " + "\(self.offerModel?.promotionCode ?? "")" + " " + "please select another promocode for your enter amount".localized
               // vc.mobileNumber.becomeFirstResponder()
            }
        }
        //vc.amount.inputAccessoryView = self.submitView
        //vc.amount.inputAccessoryView?.isHidden = true
        //vc.remarks.inputAccessoryView = self.submitView
        
        NotificationCenter.default.addObserver(self, selector: #selector(PaySendViewController.morePayment), name: NSNotification.Name(rawValue: "ReloadNotificationVC"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PaySendViewController.moreRepeatPayment1), name: NSNotification.Name(rawValue: "ReloadNotificationVCRepeat"), object: nil)
        
        if isOfferEnabled {
            self.multiButton?.hide()
            self.heightOffersHeader.constant = 50.00
            self.containerOfferHeader.isHidden = false
            
            if let offer = self.offerModel {
                if offer.paymentType == "0" {
                    self.scanToPay.isHidden = true
                } else {
                    
                    self.scanToPay.isHidden = false
                }
            }
        } else {
            self.heightOffersHeader.constant = 0.00
            self.containerOfferHeader.isHidden = true
        }
        
        if transType.type == .transferTo || transType.type == .cashIn || transType.type == .cashOut {
            self.multiButton?.hide()
        }
        self.loadAdvertisement()
        self.didShowAdvertisement(show: true)
        
        println_debug(NSUUID().uuidString.lowercased())
        println_debug(uuid)
        multiBtnYPos = self.multiButton?.floatButton.frame.origin.y ?? 0
        //self.hideOfferList()
//        NotificationCenter.default.addObserver(
//            self,
//            selector: #selector(keyboardWillShow),
//            name: UIResponder.keyboardWillShowNotification,
//            object: nil
//        )
//        NotificationCenter.default.addObserver(
//            self,
//            selector: #selector(keyboardWillHide),
//            name: UIResponder.keyboardWillHideNotification,
//            object: nil
//        )
        if self.transType.type == .cashIn || self.transType.type == .cashOut {
            self.loadInitialize()
            self.getNearByAgentsList()
            self.nearbyAgentBtn.isHidden = true
            self.nearbyAgentView.isHidden = true
            self.didShowAdvertisement(show: false)
            self.nearByAgentTableView.delegate = self
            self.nearByAgentTableView.dataSource = self
            let image = UIImage(named: "upArrow")?.withRenderingMode(.alwaysTemplate).tintImage(tintColor: .white)
            self.nearbyAgentImageView.image = image
        } else {
            self.nearbyAgentBtn.isHidden = true
            self.nearbyAgentView.isHidden = true
            self.nearByAgentTableView.isHidden = true
        }
    }
    
//    @objc func bioAuth() {
//        OKPayment.main.authenticate(screenName: "caskbackPayto", delegate: self)
//        }
    
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            self.multiButton?.setMultiBtnPosition(keyboardHide: false, height: keyboardHeight)
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        self.multiButton?.setMultiBtnPosition(keyboardHide: true, height: multiBtnYPos)
    }
    
    
    //MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.hideOfferList()
    }
    
    func loadAdvertisement() {
        let randomIndex = Int(arc4random_uniform(UInt32(advImagesUrl.AdImageData.count)))
        if let urlString = advImagesUrl.AdImageData[safe: randomIndex] {
            DispatchQueue.main.async {
                self.adViewImage.image = UIImage(data: urlString)
                self.adViewImage.contentMode = .scaleAspectFill
                //                self.adImage.setImage(url: urlString, contentMode: UIView.ContentMode.scaleAspectFit, placeholderImage: nil)
            }
        }
    }
    
    func didShowAdvertisement(show: Bool) { // show means false execution
        self.advView.isHidden = !show
        self.adViewImage.isHidden = !show
        self.heightConstraints.constant = (show) ? 120.00 : 0.00
        self.view.layoutIfNeeded()
    }
    
    func backToSinglePayment(vc: PayToUIViewController) {
        self.didShowAdvertisement(show: false)
        vc.delegate = self
        vc.transType = self.transType
        model = PaytoObject<PayToUIViewController>(initial: vc)
        // cell initialisation()
        let indexPath  = IndexPath.init(row: 0, section: 0)
        guard let cell = self.mainTableView.dequeueReusableCell(withIdentifier: "identifierPaytoCell", for: indexPath) as? PaysendHolderCell else { return }
        model!.getObjects()[indexPath.row].indexPath = indexPath
        cell.updateCell(model!.getObjects()[indexPath.row].view)
        self.cells = PaytoObject<PaysendHolderCell>(initial: cell)
        
        self.mainTableView.reloadData()
    }
    
    //MARK:- Multi Action Button
    func addingFloatingButtons() {
        let scanPay = ActionButtonItem(title: PaytoConstants.messages.scantoPay.localized, image: #imageLiteral(resourceName: "scan"))
        scanPay.action = { [weak self] item in
            guard let model = self?.model?.getObjects().first else { return }
            if let txt = model.userType.text, txt.lowercased().hasPrefix("FUEL".lowercased()) || txt.lowercased().hasPrefix("PARKING".lowercased()) || txt.lowercased().hasPrefix("TOLL".lowercased()) {
//                guard let text = model.addVehicle.text else { return }
//                guard text.count > 0 else {
//                    PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.addVehicle.localized)
//                    return
//                }
                
                self?.multiButton?.toggleMenu()
                guard let scanController = self?.storyboard?.instantiateViewController(withIdentifier: "secondVC") as? ScanToPayViewController else { return }
                scanController.delegate = self
                PaytoConstants.global.navigation?.pushViewController(scanController, animated: true)
                
            } else {
                self?.multiButton?.toggleMenu()
                guard let scanController = self?.storyboard?.instantiateViewController(withIdentifier: "secondVC") as? ScanToPayViewController else { return }
                scanController.delegate = self
                PaytoConstants.global.navigation?.pushViewController(scanController, animated: true)
            }
        }
        
        let conatct = ActionButtonItem(title: PaytoConstants.messages.payFromContact.localized, image: #imageLiteral(resourceName: "add_pay"))
        conatct.action = { [weak self] item in
            
            guard let model = self?.model?.getObjects().first else { return }
            if let txt = model.userType.text, txt.lowercased().hasPrefix("FUEL".lowercased()) || txt.lowercased().hasPrefix("PARKING".lowercased()) || txt.lowercased().hasPrefix("TOLL".lowercased()) {
//                guard let text = model.addVehicle.text else { return }
//                guard text.count > 0 else {
//                    PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.addVehicle.localized)
//                    return
//                }
                self?.multiButton?.toggleMenu()
                let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
                PaytoConstants.global.navigation?.present(nav, animated: true, completion: nil)
            } else {
                self?.multiButton?.toggleMenu()
                let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
                PaytoConstants.global.navigation?.present(nav, animated: true, completion: nil)
            }
        }
        
        let favorite = ActionButtonItem(title: PaytoConstants.messages.payFromFav.localized, image: #imageLiteral(resourceName: "favourit"))
        favorite.action = { [weak self] item in
            
            guard let model = self?.model?.getObjects().first else { return }
            if let txt = model.userType.text, txt.lowercased().hasPrefix("FUEL".lowercased()) || txt.lowercased().hasPrefix("PARKING".lowercased()) || txt.lowercased().hasPrefix("TOLL".lowercased()) {
//                guard let text = model.addVehicle.text else { return }
//                guard text.count > 0 else {
//                    PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.addVehicle.localized)
//                    return
//                }
                self?.multiButton?.toggleMenu()
                guard let weakself = self else { return }
                let nav = weakself.openFavoriteFromNavigation(weakself, withFavorite: .payto)
                PaytoConstants.global.navigation?.present(nav, animated: true, completion: nil)
            } else {
                self?.multiButton?.toggleMenu()
                guard let weakself = self else { return }
                let nav = weakself.openFavoriteFromNavigation(weakself, withFavorite: .payto, modelPayTo: model)
                PaytoConstants.global.navigation?.present(nav, animated: true, completion: nil)
            }
        }
        
        let mobile = ActionButtonItem(title: PaytoConstants.messages.addMobNumberToPay.localized, image: #imageLiteral(resourceName: "payto"))
        mobile.action = { [weak self] item in
            
            guard let model = self?.model?.getObjects().first else { return }
            if let txt = model.userType.text, txt.lowercased().hasPrefix("FUEL".lowercased()) || txt.lowercased().hasPrefix("PARKING".lowercased()) || txt.lowercased().hasPrefix("TOLL".lowercased()) {
//                guard let text = model.addVehicle.text else { return }
//                guard text.count > 0 else {
//                    PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.addVehicle.localized)
//                    return
//                }
                self?.multiButton?.toggleMenu()
                self?.addNumberToPay()
            } else {
                
                //when i do multi payto and click on add mobile number to pay this will be called
                self?.multiButton?.toggleMenu()
                self?.addNumberToPay()
            }
        }
        
        multiButton = ActionButton(attachedToView: self.view, items: [mobile, favorite, scanPay, conatct])
        multiButton?.action = { button in button.toggleMenu() }
        multiButton?.setTitle("+", forState: UIControl.State())
        multiButton?.backgroundColor = UIColor.init(hex: "F3C632")
        
        self.multiButton?.hide()
    }

    func didChangeHeight(height: CGFloat, withIndex path: IndexPath?) {
        UIView.setAnimationsEnabled(false)
        self.mainTableView.beginUpdates()
        self.mainTableView.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    func didResetUI(path: IndexPath?) {
        guard let index = path else { return }
        self.model?.remove(index: index.row)
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "PayToUIViewController") as? PayToUIViewController else { return }
        vc.delegate = self
        vc.transType = self.transType
        model = PaytoObject<PayToUIViewController>(initial: vc)
        self.mainTableView.reloadData()
        self.submitBtn.isHidden = true
        self.multiButton?.hide()
    }
    
   @objc func morePayment() {
    //this will be used for multi payto
        self.addNewObject()
    }
    
    @objc func moreRepeatPayment1() {
        DispatchQueue.main.async {
            self.moreRepeatPayment()
        }
    }
    
    @objc func moreRepeatPayment() {
        model?.removeObjects()
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "PayToUIViewController") as? PayToUIViewController else { return }
        vc.delegate = self
        vc.view.frame = vc.view.frame
        vc.transType = self.transType
        if self.transType.type == .transferTo || self.transType.type == .cashIn {
            vc.cashInCashOut = "Cash In"
        } else if self.transType.type == .cashOut {
            vc.cashInCashOut = "Cash Out"
        }
        model = PaytoObject<PayToUIViewController>(initial: vc)
        
        // cell initialisation ()
        let indexPath  = IndexPath.init(row: 0, section: 0)
        guard let cell = self.mainTableView.dequeueReusableCell(withIdentifier: "identifierPaytoCell") as? PaysendHolderCell else { return }
        model!.getObjects()[indexPath.row].indexPath = indexPath
        cell.updateCell(model!.getObjects()[indexPath.row].view)
        self.cells = PaytoObject<PaysendHolderCell>(initial: cell)
        self.mainTableView.reloadData()
        
        self.didShowSubmitButton(show: false, path: nil)
        if self.transType.type == .cashIn || self.transType.type == .cashInFree || self.transType.type == .cashOutFree || self.transType.type == .cashOut {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddScanButtonInMobile"), object: nil)
        }
        if self.transType.transType == .payToID {
            self.transType.transType = .payTo
        }
        
        if let numberData = UserDefaults.standard.string(forKey: "stringPaymentPayto") {
            //UserDefaults.standard.set("", forKey: "stringPaymentPayto")
            if numberData.count > 0 {
                var name = "Unknown"
                if numberData.hasPrefix("0095") {
                    let validNumber = "0\(numberData.substring(from: 4))"
                    let dict = self.contactSuggesstion(validNumber)
                    if dict.count > 0 {
                        name = dict[0]["name"].safelyWrappingString()
                    }
                } else {
                    let dict = self.contactSuggesstion(numberData)
                    if dict.count > 0 {
                        name = dict[0]["name"].safelyWrappingString()
                    }
                }
                
                if name == "Unknown" || name == "" {
                    if let nameVal = UserDefaults.standard.string(forKey: "PaytoName") {
                       // UserDefaults.standard.set("", forKey: "PaytoName")
                        name = nameVal.count > 0 ? nameVal : "Unknown"
                    }
                } else {
                    UserDefaults.standard.set("", forKey: "PaytoName")
                }
                UserDefaults.standard.set(numberData, forKey: "RepeatPayTo")
                let mobileObject = PTManagerClass.decodeMobileNumber(phoneNumber: numberData)
                model?.getObjects()[indexPath.row].mobileNumber.text = mobileObject.number
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    vc.contactUpgrade(number: numberData, name: (name != "") ? name : "Unknown", repeatPayTo: true)
                }
            }
        }
        vc.view.endEditing(true)
    }
    
    func addNewObject() {
        
        model?.removeObjects()
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "PayToUIViewController") as? PayToUIViewController else { return }
        vc.delegate = self
        vc.titleDelegate = self
        vc.view.frame = vc.view.frame
        vc.transType = self.transType
        if self.transType.type == .transferTo || self.transType.type == .cashIn {
            vc.cashInCashOut = "Cash In"
        } else if self.transType.type == .cashOut {
            vc.cashInCashOut = "Cash Out"
        }

        model = PaytoObject<PayToUIViewController>(initial: vc)
        
        // cell initialisation ()
        let indexPath  = IndexPath.init(row: 0, section: 0)
        guard let cell = self.mainTableView.dequeueReusableCell(withIdentifier: "identifierPaytoCell") as? PaysendHolderCell else { return }
        model!.getObjects()[indexPath.row].indexPath = indexPath
        if self.transType.type == .cashIn || self.transType.type == .cashInFree || self.transType.type == .cashOutFree || self.transType.type == .cashOut {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddScanButtonInMobile"), object: nil)
        }
        cell.updateCell(model!.getObjects()[indexPath.row].view)
        self.cells = PaytoObject<PaysendHolderCell>(initial: cell)
        self.mainTableView.reloadData()
        
        if let firstObj = model!.getObjects().first {
            firstObj.mobileNumber.text = "09"
            firstObj.mobileNumber.hideClear?.hideClearButton(hide: true)
        }
        self.didShowSubmitButton(show: false, path: nil)
        vc.view.endEditing(true)
    }
    
    func didShowSubmitButton(show: Bool, path: IndexPath?) {

        self.submitBtn.isHidden = !show
        if self.submitBtn.isHidden {
            self.multiButton?.hide()
        } else {
            if isOfferEnabled || transType.type == .transferTo || transType.type == .cashIn || transType.type == .cashOut {
                self.multiButton?.hide()
            } else {
                self.multiButton?.show()
            }
        }
        
        guard let firstModel = model!.getObjects().first else { return }
        guard model!.getObjects().count < 2 else { return }

        if firstModel.subscriber.text!.lowercased() == "Unregistered User".lowercased() {
            self.multiButton?.hide()
        }
        firstModel.amount.inputAccessoryView?.isHidden = !show
    }
    
    func changeSubmitBtnTitle() {
        self.submitBtn.setTitle("Submit".localized, for: .normal)
    }
    
    //MARK:- Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == nearByAgentTableView {
            return (nearByServicesList.count > 0) ? 1 : 0
        }
        if tableView.tag == 2 {
            return offerFilterListModel?.count ?? 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == nearByAgentTableView {
            let listCell = tableView.dequeueReusableCell(withIdentifier: "maplistcellidentifier", for: indexPath) as! MapListCell
            let promotion = nearByServicesList[indexPath.row]
            listCell.callBtn.addTarget(self, action: #selector(callBtnAction(_:)), for: UIControl.Event.touchUpInside)
            listCell.wrapData(promotion: promotion)
            return listCell
        } else  if tableView.tag == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: OffersTableViewCell.self)) as! OffersTableViewCell
            if let model = self.offerFilterListModel {
                if let dataModel = model[safe: indexPath.row] {
                    cell.wrapCellData(dataModel)
                }
            }
            return cell
        } else {
        let cell = self.cells!.getObjects()[indexPath.row]
        return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == nearByAgentTableView {
            tableView.estimatedRowHeight = 250
            tableView.rowHeight = UITableView.automaticDimension
            return tableView.rowHeight
        }
        if tableView.tag == 2 {
            return 215.00
        } else {
            if model != nil, cells != nil  {
                if let object = model!.getObjects()[safe: indexPath.row] {
                    return object.heightUpdates
                } else {
                    return 50.00
                }
            } else {
                return 50.00
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == nearByAgentTableView {
            let promotion = nearByServicesList[indexPath.row]
            if let number = promotion.UserContactData?.PhoneNumber {
                let object = PTScanObject.init(amt: "", nme: promotion.UserContactData?.FirstName ?? "", agent: number, remark: "")
                self.nearByAgentTableView.isHidden = true
                self.nearbyAgentBtn.isHidden = true
                self.nearbyAgentView.isHidden = true
                self.selfUpdateObject(object: object)
            }
        } else if tableView.tag == 2 {
            self.didSelectListObject(model: offerFilterListModel![indexPath.row])
        }
    }
    
    //MARK:- Cell Delegate
    func didSelectListObject(model: OffersListModel) {
        self.offerModel = model
        self.submitModelData()
    }
    
    //MARK:- Offer List Action
    @IBAction func btnBackAction(_ sender: Any) {
        offerModel = oldOfferModel
        self.hideOfferList()
    }
    
    func hideOfferList() {
        self.offerListView.isHidden = true
    }
    
    func showOfferList() {
        self.offerListView.isHidden = false
        self.offerTableView.reloadData()
    }
    
    func submitModelData()
    {
        guard let controllerModel = self.model else { return }
        guard let controller = controllerModel.getObjects().first else { return }
        
        let number     = controller.mobileNumber.text
        let category = controller.userType.text
        let merchant = controller.subscriber.text
        let business = controller.merchant.text
        let amount   = controller.amount.text?.replacingOccurrences(of: ",", with: "")
        let remarks  = controller.remarks.text
        let accountType  = UitilityClass.returnAccountType(type: controller.accountType)

        let confirmationModel = OfferConfirmationScreen(mobileNumber: number, category: category, merchant: merchant, businessName: business, amount: amount, remarks: remarks, accountType: accountType, offers: self.offerModel)
        
        let offerStory = UIStoryboard(name: "Offers", bundle: Bundle.main)
        guard let offerConfirm = offerStory.instantiateViewController(withIdentifier: "OffersConfirmationViewController") as? OffersConfirmationViewController else { return }
        offerConfirm.offerConfirmModel = confirmationModel
        PaytoConstants.global.navigation?.pushViewController(offerConfirm, animated: true)
    }
    
    func filterOfferList()
    {
        offerFilterListModel?.removeAll()
        guard let controllerModel = self.model else { return }
        guard let controller = controllerModel.getObjects().first else { return }
        
        let promocode = offerModel?.promotionCode
        let min = offerModel?.minimumPayableAmount ?? 0

        for element in offerListModel! {
            println_debug(element)
            if (element.promotionCode != promocode &&
                element.paymentType == "1" &&
                element.isPayableLimit! && element.categoryID == offerModel?.categoryID && element.amountType == "0" || element.amountType == "1") {
                
                let minimumAmount = element.minimumPayableAmount ?? 0
                let maximumAmount = element.maximumPayableAmount ?? 0

                if (minimumAmount > 0 && minimumAmount > min) {
                     let actualAmount   = Int(controller.amount.text?.replacingOccurrences(of: ",", with: "") ?? "0") ?? 0
                    if (actualAmount >= minimumAmount && actualAmount <= maximumAmount) {
                        offerFilterListModel?.append(element)
                    }
                }
            }
        }
    }
    
    //MARK:- Submit Button Action
    func navigateToConfirmation() {
        
        guard let controllerModel = self.model else { return }
        guard let controller = controllerModel.getObjects().first else { return }
        let number     = controller.mobileNumber.text
        let category = controller.userType.text
        let merchant = controller.subscriber.text
        let business = controller.merchant.text
        let amount   = controller.amount.text?.replacingOccurrences(of: ",", with: "")
        let remarks  = controller.remarks.text
        let accountType  = UitilityClass.returnAccountType(type: controller.accountType)

        if self.offerModel?.productValidationRuleValue == 2 { //Check product value multiple
            if self.offerModel?.amountType == "3"{
                
                let  dataSaleFree =   CodeSnippets.getDiscountSaleFreeQty(discountAmount: Int(discountAmount) ,totalAmount: Int(amount ?? "0") ?? 0, productValue: Int( self.offerModel?.productValue ?? 0) , saleQty:  buyQty, freeQty: freeQty)
                self.offerModel?.buyingQty = dataSaleFree.saleQuantity
                self.offerModel?.freeQty = dataSaleFree.freeQuantity
                self.offerModel?.value = dataSaleFree.discountAmount
            } else {
            let  dataSaleFree =   CodeSnippets.getSaleFreeQty(totalAmount: Int(amount ?? "0") ?? 0, productValue: Int( self.offerModel?.productValue ?? 0) , saleQty:  buyQty, freeQty:  freeQty)
                self.offerModel?.buyingQty = dataSaleFree.saleQuantity
                self.offerModel?.freeQty = dataSaleFree.freeQuantity
            }
        }
        
        if self.offerModel?.promotionConditionalValue == 0 {//Conditional value
            guard let offerData = self.offerModel else {
                return
            }
            let agentNo = getAgentCode(numberWithPrefix: number.safelyWrappingString(), havingCountryPrefix: controller.mobileNumber.country.dialCode)
            OffersManager.getConditionalMappingByPromotinId(amount: amount ?? "", model: offerData, agentNo: agentNo, completion: { (collections) in
                if collections == nil {
                    let confirmationModel = OfferConfirmationScreen(mobileNumber: number, category: category, merchant: merchant, businessName: business, amount: amount, remarks: remarks, accountType: accountType, offers: offerData)
                    
                    let offerStory = UIStoryboard(name: "Offers", bundle: Bundle.main)
                    guard let offerConfirm = offerStory.instantiateViewController(withIdentifier: "OffersConfirmationViewController") as? OffersConfirmationViewController else { return }
                    offerConfirm.offerConfirmModel = confirmationModel
                PaytoConstants.global.navigation?.pushViewController(offerConfirm, animated: true)
                } else {
                    let confirmationModel = OfferConfirmationScreen(mobileNumber: number, category: category, merchant: merchant, businessName: business, amount: amount, remarks: remarks, accountType: accountType, offers: collections)
                    
                    let offerStory = UIStoryboard(name: "Offers", bundle: Bundle.main)
                    guard let offerConfirm = offerStory.instantiateViewController(withIdentifier: "OffersConfirmationViewController") as? OffersConfirmationViewController else { return }
                    offerConfirm.offerConfirmModel = confirmationModel
                    PaytoConstants.global.navigation?.pushViewController(offerConfirm, animated: true)
                }
            })
            
        } else {
            
            let confirmationModel = OfferConfirmationScreen(mobileNumber: number, category: category, merchant: merchant, businessName: business, amount: amount, remarks: remarks, accountType: accountType, offers: self.offerModel)
            
            let offerStory = UIStoryboard(name: "Offers", bundle: Bundle.main)
            guard let offerConfirm = offerStory.instantiateViewController(withIdentifier: "OffersConfirmationViewController") as? OffersConfirmationViewController else { return }
            offerConfirm.offerConfirmModel = confirmationModel
            PaytoConstants.global.navigation?.pushViewController(offerConfirm, animated: true)
        }
    }
    
    private func cashInMoveto() {
        guard let controllerModel = self.model else { return }
        guard let controller = controllerModel.getObjects().first else { return }
        controller.amount.resignFirstResponder()
        let model: Dictionary<String, Any> = ["number": UserModel.shared.mobileNo, "name": UserModel.shared.name, "merchant": "", "amount": controller.amount.text ?? "", "commission": controller.commissionTxt.text ?? "", "totalAmount": controller.totalAmountTxt.text ?? "", "remark": controller.remarks.text ?? "", "destinationNumber": controller.mobileNumber.text ?? "", "countryCode": controller.mobileNumber.country.dialCode]
        guard let cashInQR = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "CashInQRViewController") as? CashInQRViewController else { return }
        cashInQR.model = model
        PaytoConstants.global.navigation?.pushViewController(cashInQR, animated: true)
    }
    
    //this will help to pay
    @IBAction func submit_PayAction(_ sender: UIButton) {
        self.view.endEditing(true)
        UserDefaults.standard.set("", forKey: "stringPaymentPayto")
        UserDefaults.standard.set("", forKey: "PaytoName")
        
        if (transType.type == .cashIn || self.transType.type == .cashInFree) && submitBtn.titleLabel?.text == "Generate Cash In QR".localized {
            guard let controllerModel = self.model else { return }
            guard let controller = controllerModel.getObjects().first else { return }
            let userRegisterInformation = controller.subscriber.text ?? ""
            if userRegisterInformation.lowercased() == "Unregistered User".lowercased() {
                alertViewObj.wrapAlert(title: nil, body: PaytoConstants.messages.unregisterUser.localized, img: #imageLiteral(resourceName: "dashboard_pay_send"))
                
                alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {}
                
                alertViewObj.addAction(title: "OK".localized, style: .target) {
                    self.cashInMoveto()
                }
                
                alertViewObj.showAlert()
                
                return
            } else {
                self.cashInMoveto()
            }
        } else {
            
            guard let controllerModel = self.model else { return }
            guard let controller = controllerModel.getObjects().first else { return }
            if transType.type == .cashIn {
                let amount = controller.totalAmountTxt.text?.replacingOccurrences(of: ",", with: "") ?? "0"
                if (amount as NSString).floatValue > (UserLogin.shared.walletBal as NSString).floatValue {
                    PaytoConstants.alert.showToast(msg: PaytoConstants.messages.lessBalance.localized)
                    return
                }
            }
            if isOfferEnabled {
                if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "offersPayto", delegate: self)
                } else {
                    self.moveOfferConfirmation()
                }
            } else {
                
                //this will call for payto
                let transactionType = controller.localTransactionType.lowercased()
                
                let userRegisterInformation = controller.subscriber.text ?? ""
                
                if userRegisterInformation.lowercased() == "Unregistered User".lowercased() {
                    alertViewObj.wrapAlert(title: nil, body: PaytoConstants.messages.unregisterUser.localized, img: #imageLiteral(resourceName: "dashboard_pay_send"))
                    
                    alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {}
                    
                    alertViewObj.addAction(title: "OK".localized, style: .target) {
                        OKPayment.main.authenticate(screenName: "caskbackPayto", delegate: self)
                    }
                    
                    alertViewObj.showAlert()
                    
                    return
                }
                if transType.type == .cashIn || transType.type == .cashOut || transType.type == .transferTo {
                    
                } else {
                    
                    if transactionType.hasPrefix("Restaurant".lowercased()) || transactionType.hasPrefix("Bill".lowercased())  {
                        let amount = (controller.amount.text!.replacingOccurrences(of: ",", with: "") as NSString).doubleValue
                        if !controller.remarks.text!.hasPrefix("BILL") && amount > 4999.0 {
                            guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "TipMerchantViewController") as? TipMerchantViewController else { return }
                            vc.modalPresentationStyle = .overCurrentContext
                            vc.delegate = self
                            vc.amount = controller.amount.text!.replacingOccurrences(of: ",", with: "")
                            PaytoConstants.global.navigation?.present(vc, animated: true, completion: nil)
                            return
                        }
                    } else if transactionType.lowercased().hasPrefix("FUEL".lowercased()) || transactionType.lowercased().hasPrefix("PARKING".lowercased()) || transactionType.lowercased().hasPrefix("TOLL".lowercased()) { //}|| self.transType.categoryType == .toll {
                        //                guard let text = controller.addVehicle.text else { return }
                        //                guard text.count > 0 else {
                        //                    PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.addVehicle.localized)
                        //                    return
                        //                }
                    }
                }
                OKPayment.main.authenticate(screenName: "caskbackPayto", delegate: self)
            }
        }
        
    }
    
    func moveOfferConfirmation() {
        
        guard let controllerModel = self.model else { return }
        guard let controller = controllerModel.getObjects().first else { return }
        
        let amount   = controller.amount.text?.replacingOccurrences(of: ",", with: "")
        
        if let offer = self.offerModel, offer.paymentType == "0" {//Direct
            if self.offerModel?.isPayableLimit ?? true { //Unlimited Amount
                if Int(amount ?? "0") ?? 0 < self.offerModel?.minimumPayableAmount ?? 0 {
                    
                    alertViewObj.wrapAlert(title: nil, body: "This promo code offer not valid for your entered amount.".localized, img: #imageLiteral(resourceName: "NoOffersFound"))
                    
                    alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        
                    }
                    alertViewObj.showAlert()
                } else if self.offerModel?.minimumPayableAmount ?? 0 == 0 {
                    if Int(amount ?? "0") ?? 0 > self.offerModel?.maximumPayableAmount ?? 0 {
                        alertViewObj.wrapAlert(title: nil, body: "This promo code offer not valid for your entered amount.".localized, img: #imageLiteral(resourceName: "NoOffersFound"))
                        
                        alertViewObj.addAction(title: "OK".localized, style: .cancel) {}
                        
                        alertViewObj.showAlert()
                    } else {
                        if let mobileNumber = offer.mobilenumber, var agent = mobileNumber.components(separatedBy: ",").first {
                            agent = agent.trimmingCharacters(in: .whitespacesAndNewlines)
                            self.checkAgentCode(with: agent)
                        }
                    }
                } else if self.offerModel?.minimumPayableAmount ?? 0 > 0 {
                    
                    if Int(amount ?? "0") ?? 0 >
                        self.offerModel?.maximumPayableAmount ?? 0 {
                        alertViewObj.wrapAlert(title: nil, body: "This promo code offer not valid for your entered amount.".localized, img: #imageLiteral(resourceName: "NoOffersFound"))
                        
                        alertViewObj.addAction(title: "OK".localized, style: .cancel) {}
                        
                        alertViewObj.showAlert()
                    } else {
                        
                        if let mobileNumber = offer.mobilenumber, var agent = mobileNumber.components(separatedBy: ",").first {
                            agent = agent.trimmingCharacters(in: .whitespacesAndNewlines)
                            self.checkAgentCode(with: agent)
                        }
                    }
                }
            }
        } else {//Indirect offers
            
            if (self.offerModel?.isPayableLimit ?? true == false) { //Unlimited Amount
                 self.navigateToConfirmation()
            }
            else {
            if self.offerModel?.productValidationRuleValue == 0 || self.offerModel?.productValidationRuleValue == -1
            {
                //For fixed amount&percentage with max min
                //Check condition for minimum amount
                if Int(amount ?? "0") ?? 0 < self.offerModel?.minimumPayableAmount ?? 0 {
                    
                    alertViewObj.wrapAlert(title: nil, body: "This promo code offer not valid for your entered amount.".localized, img: #imageLiteral(resourceName: "NoOffersFound"))
                    
                    alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        
                    }
                    alertViewObj.showAlert()
                }
                else if (self.offerModel?.minimumPayableAmount ?? 0 == self.offerModel?.maximumPayableAmount ?? 0) {
                    self.navigateToConfirmation()
                }
                else if self.offerModel?.minimumPayableAmount ?? 0 == 0 {
                    if Int(amount ?? "0") ?? 0 > self.offerModel?.maximumPayableAmount ?? 0 {
                        alertViewObj.wrapAlert(title: nil, body: "This promo code offer not valid for your entered amount.".localized, img: #imageLiteral(resourceName: "NoOffersFound"))
                        
                        alertViewObj.addAction(title: "OK".localized, style: .cancel) {}
                        
                        alertViewObj.showAlert()
                    } else {
                        self.navigateToConfirmation()
                    }
                }else if (self.offerModel?.maximumPayableAmount ?? 0 == 0) {
                    if Int(amount ?? "0") ?? 0 > self.offerModel?.minimumPayableAmount ?? 0 {
                        alertViewObj.wrapAlert(title: nil, body: "This promo code offer not valid for your entered amount.".localized, img: #imageLiteral(resourceName: "NoOffersFound"))
                        
                        alertViewObj.addAction(title: "OK".localized, style: .cancel) {}
                        
                        alertViewObj.showAlert()
                    } else {
                        self.navigateToConfirmation()
                    }
                }
                else if self.offerModel?.minimumPayableAmount ?? 0 > 0 {
                    
                    if Int(amount ?? "0") ?? 0 >
                        self.offerModel?.maximumPayableAmount ?? 0 {
                        alertViewObj.wrapAlert(title: nil, body: "This promo code offer not valid for your entered amount.".localized, img: #imageLiteral(resourceName: "NoOffersFound"))
                        
                        alertViewObj.addAction(title: "OK".localized, style: .cancel) {}
                        
                        alertViewObj.showAlert()
                    } else {
                        self.filterOfferList()
                        
                        if offerFilterListModel?.count ?? 0 > 0
                        {
                            self.showOfferList()
                        } else {
                            self.navigateToConfirmation()
                        }
                    }
                }
                else {
                    self.navigateToConfirmation()
                }
            } else {//For free and quantity
                
                //Check for qty product validation rule= 2
                if self.offerModel?.productValidationRuleValue == 2 { //Check product value multiple
                    let amountValue = Int(amount ?? "0") ?? 0
                    let productValue = self.offerModel?.productValue ?? 0
                    guard productValue != 0 else { return }
                    if Int(amountValue % productValue) == 0 {
                        self.navigateToConfirmation()
                    }
                    else {
                        //Show alert msg
                        alertViewObj.wrapAlert(title: nil, body: "This promo code offer not valid for your entered amount.".localized, img: #imageLiteral(resourceName: "NoOffersFound"))
                        alertViewObj.addAction(title: "OK".localized, style: .cancel) {}
                        alertViewObj.showAlert()
                    }
                } else {
                    self.navigateToConfirmation()
                }
            }
        }
        }
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
       
        if screen == "offersPayto" {
            if isSuccessful{
                self.moveOfferConfirmation()
            }
        } else {
        if isSuccessful {
            self.initiateCashbackPayment()
        }
        }
    }
    
    //MARK:- Tip Callbacks
    func didSelectOK(amount: String) {
      
        
        
        guard let controllerModel = self.model else { return }
        guard let controller = controllerModel.getObjects().first else { return }

        if let amountStr = controller.amount.text, amount.count > 0 {
            let amnt = amountStr.replacingOccurrences(of: ",", with: "")
            controller.amount.text = "\((amnt as NSString).floatValue + (amount as NSString).floatValue)"
            controller.remarks.text = String.init(format: "BILL %@ MMK + Tip %@ MMK", amnt, amount)
            OKPayment.main.authenticate(screenName: "caskbackPayto", delegate: self)
        }else{
            //OIO-I5722
            //Payto-for restaurant number-in tips pop up if I give ok without amount it is not moving to next screen it being in same screen
            OKPayment.main.authenticate(screenName: "caskbackPayto", delegate: self)
        }
    }
    
    func didselectNoThnx() {
         OKPayment.main.authenticate(screenName: "caskbackPayto", delegate: self)
    }
    
    //MARK:- MultiPayto navigations && Delegate Callback
    
    // contacts delegate
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        guard let multipay = self.storyboard?.instantiateViewController(withIdentifier: "MultiPaytoViewController") as? MultiPaytoViewController else { return }
        multipay.multipayModel = self.model
        multipay.delegate = self
        guard let numbers = contact.phoneNumbers.first else {
            multipay.generateCell("", name: nil, amount: nil)
            PaytoConstants.global.navigation?.pushViewController(multipay, animated: true)
            return
        }
        multipay.generateCell(numbers.phoneNumber, name: contact.displayName(), amount: nil)
        PaytoConstants.global.navigation?.pushViewController(multipay, animated: true)
    }
    
    // scan qr delegate
    func changeScrollPage(point: Int, withScanObject object: PTScanObject) {
        if !isOfferEnabled {
            guard let multipay = self.storyboard?.instantiateViewController(withIdentifier: "MultiPaytoViewController") as? MultiPaytoViewController else { return }
            multipay.multipayModel = self.model
            multipay.delegate = self
            multipay.generateCell(object.agentNo, name: object.name, amount: object.amount, remark: object.remarks)
            PaytoConstants.global.navigation?.pushViewController(multipay, animated: true)
        }
    }
    
    func scanningFailed() {
        
    }
    // self update
    func selfUpdateObject(object: PTScanObject) {
        if let singlePay = self.model?.getObjects().first {
            if object.amount != "" {
                //singlePay.amount.inputAccessoryView = self.submitView
            }
            singlePay.changeScrollPage(point: 0, withScanObject: object)
        }
    }
    
    //offer update
    func offerUpdateThroughScan(number: String) {
        if let singlePay = self.model?.getObjects().first {
            singlePay.offerParsing(number : number)
        }
    }
    
    // favorite delegate
    func selectedFavoriteObject(obj: FavModel) {
        guard let multipay = self.storyboard?.instantiateViewController(withIdentifier: "MultiPaytoViewController") as? MultiPaytoViewController else { return }
        multipay.multipayModel = self.model
        multipay.delegate = self
        multipay.generateCell(obj.phone, name: obj.name, amount: nil)
        PaytoConstants.global.navigation?.pushViewController(multipay, animated: true)
    }
    
    // add number to multipayto
    func addNumberToPay() {
        guard let multipay = self.storyboard?.instantiateViewController(withIdentifier: "MultiPaytoViewController") as? MultiPaytoViewController else { return }
        multipay.multipayModel = self.model
        multipay.delegate = self
        multipay.generateCell("", name: nil, amount: nil, checkApi: false)
        PaytoConstants.global.navigation?.pushViewController(multipay, animated: true)
        guard let lastObj = multipay.multipayModel?.getObjects().last else { return }
        UIView.animate(withDuration: 0.2) {
            lastObj.mobileNumber.becomeFirstResponder()
        }
    }
    
    deinit {
        println_debug(PaySendViewController.self)
    }
    
    //MARK:- Offers Extra Layouting
    @IBOutlet weak var heightOffersHeader: NSLayoutConstraint!
    
     @IBOutlet weak var containerOfferHeader: UIView!
    
    @IBOutlet weak var addMoney: UIButton! {
        didSet {
            self.addMoney.setTitle("Add Money".localized, for: .normal)
             self.addMoney.layer.cornerRadius = 25.0
             self.addMoney.layer.masksToBounds = true
             self.addMoney.layer.borderColor = UIColor.red.cgColor
             self.addMoney.layer.borderWidth = 1.0
        }
    }
    
    @IBOutlet weak var scanToPay: UIButton! {
        didSet {
            self.scanToPay.setTitle("Scan QR Code".localized, for: .normal)
            self.scanToPay.layer.cornerRadius = 25.0
            self.scanToPay.layer.masksToBounds = true
            self.scanToPay.layer.borderColor = UIColor.red.cgColor
            self.scanToPay.layer.borderWidth = 1.0
        }
    }
    
    @IBAction func offerHeaderAction(_ sender: UIButton) {
        let nav = PaytoConstants.global.navigation
        if sender == addMoney {
            
            let addMoneyStoryboard = UIStoryboard.init(name: "AddWithdraw", bundle: nil)
            guard let firstView_addMoneyVC = addMoneyStoryboard.instantiateViewController(withIdentifier: "AddWithdrawMoneyVC") as? AddWithdrawMoneyVC else { return }
            firstView_addMoneyVC.screenFrom.0 = "DashBoard"
            firstView_addMoneyVC.screenFrom.1 = "Cash In"
            nav?.pushViewController(firstView_addMoneyVC, animated: true)
            
            /*
            if let addWithdrawView = UIStoryboard(name: "AddWithdrawMain", bundle: nil).instantiateViewController(withIdentifier: "AddWithdrawView_ID") as? AddWithdrawViewController {
                let aObjNav = UINavigationController(rootViewController: addWithdrawView)
            
                nav?.present(aObjNav, animated: true, completion: nil)
            }
             */
        } else {
            guard let scan = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "secondVC") as? ScanToPayViewController else { return }
            scan.delegate = self
            if isOfferEnabled {
                scan.isOfferEnabled = true
                PaytoConstants.global.navigation?.pushViewController(scan, animated: true)
            } else {
                if let nav = PaytoConstants.global.navigation {
                    if nav.viewControllers.contains(where: {$0.isKind(of: MultiPaytoViewController.self)}) {
                        scan.screen = .payto
                    }
                }
                if let _ = PaytoConstants.global.navigation {
                    PaytoConstants.global.navigation?.pushViewController(scan, animated: true)
                }
            }
           //  nav?.pushViewController(scan, animated: true)
         //    nav?.present(scan, animated: true, completion: nil)
           
//            let navController = UINavigationController(rootViewController: scan)
//            self.present(navController, animated:true, completion: nil)
        }
    }
    
    func dismissScanOffer(object: PTScanObject, controller: ScanToPayViewController) {
        if isOfferEnabled {
            self.offerUpdateThroughScan(number: object.agentNo)
            
        }
    }

}

//MARK:- TableView Cell
class PaysendHolderCell : UITableViewCell {
    
    @IBOutlet weak var containerView : UIView!
    
    func updateCell(_ update: UIView) {
        containerView.addSubview(update)
        update.translatesAutoresizingMaskIntoConstraints = false
        containerView.addConstraints([
            NSLayoutConstraint(item: update, attribute: .top, relatedBy: .equal, toItem: containerView, attribute: .top, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: update, attribute: .leading, relatedBy: .equal, toItem: containerView, attribute: .leading, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: update, attribute: .bottom, relatedBy: .equal, toItem: containerView, attribute: .bottom, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: update, attribute: .trailing, relatedBy: .equal, toItem: containerView, attribute: .trailing, multiplier: 1.0, constant: 0),
            ])
        self.layoutIfNeeded()
    }
}



extension PaySendViewController: SelectedAgentPayToProtocol {
    
    func selectedAgent(object: PTScanObject) {
        self.selfUpdateObject(object: object)
        self.nearByAgentTableView.isHidden = true
        self.nearbyAgentBtn.isHidden = true
        self.nearbyAgentView.isHidden = true
    }
    
    @IBAction func agentBtnAction(_ sender: UIButton) {
        let paytoStory = UIStoryboard(name: "PayTo", bundle: nil)
        guard let nearByVC = paytoStory.instantiateViewController(withIdentifier: "PaytoNearByListViewController") as? PaytoNearByListViewController else { return }
        nearByVC.delegate = self
        nearByVC.nearByServicesList = nearByServicesList
        PaytoConstants.global.navigation?.present(nearByVC, animated: true, completion: nil)
    }
    
    func didShowAgentButton(hide: Bool) {
        if transType.type == .cashIn || transType.type == .cashOut {
            if hide {
                self.nearbyAgentBtn.isHidden = true
                self.nearbyAgentView.isHidden = true
                self.nearByAgentTableView.isHidden = true
            } else {
                self.nearbyAgentBtn.isHidden = false
                self.nearbyAgentView.isHidden = false
                self.nearByAgentTableView.isHidden = false
            }
        } else {
            self.nearbyAgentBtn.isHidden = true
            self.nearbyAgentView.isHidden = true
            self.nearByAgentTableView.isHidden = true
        }
    }
    
    
    func loadInitialize() {
        let nib = UINib.init(nibName: "MapListCell", bundle: nil)
        nearByAgentTableView.register(nib, forCellReuseIdentifier: "maplistcellidentifier")
        nearByAgentTableView.tableFooterView = UIView(frame: CGRect.zero)
        nearByAgentTableView.rowHeight = UITableView.automaticDimension
        nearByAgentTableView.estimatedRowHeight = 250
        nearByAgentTableView.reloadData()
    }
    
    @objc func callBtnAction(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to: self.nearByAgentTableView)
        if let indexPath = self.nearByAgentTableView.indexPathForRow(at: buttonPosition), nearByServicesList.count >= 0 {
            let selectedpromotion = nearByServicesList[indexPath.row]
            let promotion = selectedpromotion
            if let phonenumber = promotion.UserContactData?.PhoneNumber {
                if phonenumber.count > 0 {
                    var mobNumber = getPhoneNumberByFormat(phoneNumber: phonenumber)
                    mobNumber = mobNumber.removingWhitespaces()
                    let phone = "tel://\(mobNumber)"
                    if let url = URL.init(string: phone) {
                        UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    }
                }
            }
        }else {
            alertViewObj.wrapAlert(title: nil, body: "No Phonenumber Exists".localized, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func getNearByAgentsList() {
        let url = getUrl(urlStr: "", serverType: .nearByServicesUrl)
        
        guard let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude else {
            return
        }
        
        let jsonDic:[String : Any]
        jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1000, "cashType" : (self.transType.type == .cashOut) ? 1 : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":100],"types":[11,8,9], "phoneNumberNotToConsider":""] as [String : Any]
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url,methodType: kMethod_Post, contentType: kContentType_Json, inputVal: jsonDic, authStr: nil)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                }
                return
            }
            progressViewObj.removeProgressView()
            var cashArray = [NearByServicesNewModel]()
            DispatchQueue.main.async {
                cashArray = ParserAttributeClass.parseNearbyServiceJSONNew(anyValue: response as AnyObject) as [NearByServicesNewModel]
                progressViewObj.removeProgressView()
                
                if cashArray.count > 0 {
                   self.nearByServicesList = cashArray
                    DispatchQueue.main.async {
                        self.nearbyAgentBtn.isHidden = false
                        self.nearbyAgentView.isHidden = false
                        self.nearByAgentTableView.isHidden = false
                        self.nearByAgentTableView.reloadData()
                    }
                } else  {
                    //self.showErrorAlert(errMessage: "No Record Found".localized)
                }
            }
        })
    }
    
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
