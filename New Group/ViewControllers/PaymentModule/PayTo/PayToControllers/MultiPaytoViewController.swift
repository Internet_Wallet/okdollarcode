//
//  MultiPaytoViewController.swift
//  OK
//
//  Created by Ashish on 8/10/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol MultiPaymentDelegate : class {
    func backToSinglePayment(vc: PayToUIViewController)
}

class MultiPaytoViewController: OKBaseController, UITableViewDelegate, UITableViewDataSource, PaytoHeightChanged, MultiPaytoTableViewCellDelegate, BioMetricLoginDelegate {
    func didShowAdvertisement(show: Bool) {
        
    }
    
    func didShowAgentButton(hide: Bool) {
        
    }
    
    
    @IBOutlet weak var multiPaytable: UITableView!
    @IBOutlet weak var submitBtn: UIButton!
    {
      didSet
      {
        submitBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        submitBtn.setTitle("Submit".localized, for: .normal)
      }
  }
    @IBOutlet weak var addBtnContainer: CardDesignView!
    @IBOutlet weak var rightBarView: UIView! {
        didSet {
            self.rightBarView.isHidden = true
        }
    }
    @IBOutlet weak var rightBarMicImage: UIImageView! {
        didSet {
            self.rightBarMicImage.image = UIImage(named: "act_speaker")
        }
    }
    @IBOutlet weak var rightBarSoundLabel: UILabel! {
        didSet {
            self.rightBarSoundLabel.font = UIFont(name: appFont, size: appFontSize)
            self.rightBarSoundLabel.text = "Sound On".localized
        }
    }
    @IBOutlet weak var rightBarSwitchButton: UISwitch!
    var amountNullStatus = true
    
    var multipayModel : PaytoObject<PayToUIViewController>? {
        didSet {
            guard let model = multipayModel else { return }
            let objects = model.getObjects()
            if !isFromFavorites {
                for object in objects {
                    if let last = objects.last, object == last {
                        object.view.isUserInteractionEnabled = true
                    } else {
                        object.view.isUserInteractionEnabled = false
                    }
                }
            } else {
                for object in objects {
                    if let amount = object.amount.text, amount.count > 0 {
                        object.view.isUserInteractionEnabled = false
                    } else {
                        object.view.isUserInteractionEnabled = true
                    }
                }
            }
        }
    }
    var isFromFavorites : Bool = false
    
    var transactionArray = [Dictionary<String,Any>]()
    
    weak var delegate : MultiPaymentDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backButton()
        self.rightButton()
        loadCell()
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideRightView))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func hideRightView() {
        rightBarView.isHidden = true
    }
    
  
    
    
    func generateCell(_ number: String, name: String?, amount: String?, checkApi: Bool = true, remark: String? = "") {
        self.view.frame = self.view.frame
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "PayToUIViewController") as? PayToUIViewController else { return }
        vc.delegate = self
        vc.view.frame = .zero
        if checkApi {
            vc.contactUpgrade(number: number, name: name ?? "Unknown", amount: amount, remark: remark)
        } else {
            vc.contactUpgrade(number: number, name: name ?? "Unknown", checkApi: false, amount: amount, remark: remark)
        }
        
        
        if let amountUpdate = amount {
            if (amountUpdate as NSString).floatValue > 0.00 {
                if (amountUpdate as NSString).floatValue > (UserLogin.shared.walletBal as NSString).floatValue {
                    PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.lessBalance.localized)
                } else {
                    if (amountUpdate as NSString).floatValue > (UserLogin.shared.walletBal as NSString).floatValue {
                        PaytoConstants.alert.showToast(msg: PaytoConstants.messages.lessBalance.localized)
                    } else {
                        vc.amount.text = amountUpdate
                    }
                }
            } else {
                vc.amount.text = ""
            }
        }

        self.multipayModel?.add(object: vc)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.setAnimationsEnabled(false)
        self.navigationController?.navigationBar.titleTextAttributes = PaytoConstants.global.attributesForTitle()
        self.title = PaytoConstants.headers.controllersTitles.multiPayto.localized //.safelyWrappingString()
        guard self.multiPaytable != nil else { return }
        self.loadCell()
        self.multiPaytable.reloadData()
        
//        self.multiPaytable.reloadData {
//            guard let multiModel = self.multipayModel else { return }
//            for (index, payToUi) in multiModel.getObjects().enumerated() {
//                if payToUi.amount.text?.count == 0 {
//                    self.multiPaytable.scrollToRow(at: IndexPath(row: index, section: 0), at: .top, animated: true)
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
//                        payToUi.becomeFirstResponder()
//                    }
//                    break
//                }
//            }
//        }
        
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        guard let model = multipayModel?.getObjects() else { return }
//        for (_, modelA) in model.enumerated() {
//            if modelA.amount.text?.count == 0 {
//                modelA.amount.becomeFirstResponder()
//                break
//            }
//        }
//    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIView.setAnimationsEnabled(true)
    }
    
    private func backButton() {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "tabBarBack"), for: UIControl.State.normal)
        button.tintColor = UIColor.white
        button.sizeToFit()
        button.addTargetClosure { [weak self](button) in
            guard let weakSelf = self else { return }
            //MARK:- changed from popViewController to popToRootViewController
            if let paysend = weakSelf.navigationController?.viewControllers.first as? PayToViewController {
                weakSelf.navigationController?.popToViewController(paysend, animated: true)
            } else {
                weakSelf.navigationController?.popViewController(animated: true)
            }
//            guard let firstObject = weakSelf.multipayModel?.getObjects().first else { return }
//            firstObject.view.isUserInteractionEnabled = true
//            weakSelf.delegate?.backToSinglePayment(vc: firstObject)
//            if let _ = weakSelf.delegate {
//
//            } else {
//                if let nav = PaytoConstants.global.navigation {
//                    if let payToVC = nav.viewControllers.first as? PayToViewController {
//                        payToVC.updatePayToObject(vc: firstObject)
//                    }
//                }
//            }
        }
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    private func rightButton() {
        let button = UIButton(frame: CGRect(x: 0.0, y: 0.0, width: 45.0, height: 30.0))
        button.setImage(UIImage(named: "menu_white_payto"), for: UIControl.State.normal)
        button.tintColor = UIColor.white
        button.sizeToFit()
        button.addTargetClosure { [weak self](button) in
            self?.showSoundOnOffView()
        }
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    private func showSoundOnOffView() {
        if rightBarView.isHidden {
            rightBarView.isHidden = false
        } else {
            rightBarView.isHidden = true
        }
    }
    
    @IBAction func rightBarSwitchAction(_ sender: UISwitch) {
        if sender.isOn {
            self.rightBarMicImage.image = UIImage(named: "act_speaker")
            self.rightBarSoundLabel.text = "Sound On".localized
            self.updateMultiPayObjects(status: true)
        } else {
            self.rightBarMicImage.image = UIImage(named: "speaker")
            self.rightBarSoundLabel.text = "Sound Off".localized
            self.updateMultiPayObjects(status: false)
        }
    }
    
    fileprivate func updateMultiPayObjects(status: Bool) {
        guard let model = self.multipayModel else { return }
        
        let objects = model.getObjects()
        
        // check for amount field
        for object in objects {
            object.doChangeVoiceImage(on: status)
            object.objectSound.didChangeVoice()
        }
    }

    
    func appendObjectFromPayto(number: String, name: String?) {
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "PayToUIViewController") as? PayToUIViewController else { return }
        vc.delegate = self
        if let _ = name {
            vc.contactUpgrade(number: number)
        } else {
            vc.contactUpgrade(number: number, name: name ?? "Unknown")
        }
        self.multipayModel?.add(object: vc)
    }
    
    //MARK:- Height Delegates
    func didChangeHeight(height: CGFloat, withIndex path: IndexPath?) {
      
        if self.multiPaytable != nil {
            self.multiPaytable.beginUpdates()
            self.multiPaytable.endUpdates()
        }
    }
    
    func didResetUI(path: IndexPath?) {
        
    }
    
    func didShowSubmitButton(show: Bool, path: IndexPath?) {
        self.submitBtn.isHidden = !show
        guard let model = self.multipayModel else { return }
        if model.getObjects().count <= 4 {
            self.addBtnContainer.isHidden = !show
        }
        self.checkAmountFieldNull()
    }
    

    //MARK:- Add Button
    
    @IBAction func submitAction(_ sender: UIButton) {
        
        if isFromFavorites {
            guard let controllerModel = self.multipayModel else { return }
            
            for object in controllerModel.getObjects() {
                if let amount = object.amount.text, amount.count <= 0 {
                    object.amount.becomeFirstResponder()
                    return
                }
            }
        }
        
        guard let controllerModel = self.multipayModel else { return }
        
        for controller in controllerModel.getObjects() {
            let transactionType = controller.localTransactionType.lowercased()
            
            if transactionType.hasPrefix("FUEL".lowercased()) || transactionType.hasPrefix("PARKING".lowercased()) || transactionType.hasPrefix("TOLL".lowercased()) {
                guard let text = controller.addVehicle.text else { return }
                guard text.count > 0 else {
                    PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.addVehicle.localized)
                    return
                }
            }
        }

        if appDelegate.checkNetworkAvail() {
            OKPayment.main.authenticate(screenName: "MultiCashback", delegate: self)
        } else {
            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.noInternet.localized)
        }
        
    }
    
    
    @IBAction func addNewObject(_ sender: UIButton) {
        
        if isFromFavorites {
            guard let controllerModel = self.multipayModel else { return }
            
            for object in controllerModel.getObjects() {
                if let amount = object.amount.text, amount.count <= 0 {
                    object.amount.becomeFirstResponder()
                    return
                }
            }
        }
        guard let model = self.multipayModel else { return }
        
         let objects = model.getObjects()
        
        // check for amount field
        guard let object = model.getObjects().last else { return }

        guard let mobileNumber = object.mobileNumber.text else { return }

        if mobileNumber.count > 4 {

            guard let amount = object.amount.text else { return }

            if (amount as NSString).floatValue > 0 {
                guard let paytoUI = self.storyboard?.instantiateViewController(withIdentifier: "PayToUIViewController") as? PayToUIViewController else { return }
                paytoUI.delegate = self
                paytoUI.view.frame = .zero
                model.add(object: paytoUI)
                self.multipayModel = PaytoObject<PayToUIViewController>.init(objects: model.getObjects())
                self.loadCell()
                self.multiPaytable.reloadData {
                    paytoUI.mobileNumber.becomeFirstResponder()
                }
                self.didShowSubmitButton(show: true, path: nil)
                self.didChangeHeight(height: 0.00, withIndex: nil)
            } else {
                PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.selectMobileNo.localized)
            }

            if objects.count >= 4 {
                self.addBtnContainer.isHidden = true
            }

            self.didShowSubmitButton(show: false, path: nil)
            
        } else {
            if objects.count >= 4 {
                self.addBtnContainer.isHidden = true
            }

            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.selectMobileNo.localized)
        }
        
        self.multiPaytable.scrollToBottom(animated: false)
        
    }
    
    var cellArray : [MultiPaytoTableViewCell] = [MultiPaytoTableViewCell]()
    //MARK:- MultiPay Cell Generation
    func loadCell() {
        self.checkAmountFieldNull()
        guard let multiModel = self.multipayModel else { return }
        
        cellArray.removeAll()
    UserDefaults.standard.removeObject(forKey: "MultiplePayTOAmount")
       var amountArray = [Double]()
    amountArray.removeAll()
    for (index, _) in multiModel.getObjects().enumerated() {
        let cell = multiPaytable.dequeueReusableCell(withIdentifier: "MultiPaytoTableViewCell") as? MultiPaytoTableViewCell
        cell?.indexPath = IndexPath.init(row: index, section: 0)
        let title = "Payment Details".localized + " " + "\(index + 1)"
        cell?.updateHeader(title)
        cell?.updateCell(multiModel.getObjects()[index].view)
        let newValue = multiModel.getObjects()[index].amount.text ?? ""
        let secondValue = newValue.replacingOccurrences(of: ",", with: "")
        amountArray.append(Double(secondValue) ?? 0.0)
        cell?.delegate = self
        cellArray.append(cell!)
    }
    
    UserDefaults.standard.setValue(amountArray, forKey: "MultiplePayTOAmount")
    
        if multiModel.getObjects().count>2{
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                self.multiPaytable.setContentOffset(.zero, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                    
                    if !self.isFromFavorites {
                        
                        if  multiModel.getObjects()[multiModel.getObjects().count - 1].mobileNumber.text == ""{
                            multiModel.getObjects()[multiModel.getObjects().count - 1].mobileNumber.becomeFirstResponder()
                        }else{
                            multiModel.getObjects()[multiModel.getObjects().count - 1].amount.becomeFirstResponder()
                        }
                    }else{
                        if  multiModel.getObjects()[0].mobileNumber.text == ""{
                            multiModel.getObjects()[0].mobileNumber.becomeFirstResponder()
                        }else{
                            multiModel.getObjects()[0].amount.becomeFirstResponder()
                        }
                    }
                    
                    
                })
            })
        }else if  multiModel.getObjects().count == 2{
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                    if multiModel.getObjects()[0].amount.text == "0"{
                        multiModel.getObjects()[0].amount.text = ""
                        
                        if !self.isFromFavorites {
                            if multiModel.getObjects()[multiModel.getObjects().count - 1].mobileNumber.text == ""{
                                multiModel.getObjects()[multiModel.getObjects().count - 1].mobileNumber.becomeFirstResponder()
                            }else{
                                multiModel.getObjects()[multiModel.getObjects().count - 1].amount.becomeFirstResponder()
                            }
                        }else{
                            if  multiModel.getObjects()[0].mobileNumber.text == ""{
                                multiModel.getObjects()[0].mobileNumber.becomeFirstResponder()
                            }else{
                                multiModel.getObjects()[0].amount.becomeFirstResponder()
                            }
                        }
                    
                    } else {
                        if !self.isFromFavorites {
                            if multiModel.getObjects()[multiModel.getObjects().count - 1].mobileNumber.text == ""{
                                multiModel.getObjects()[multiModel.getObjects().count - 1].mobileNumber.becomeFirstResponder()
                            }else{
                                multiModel.getObjects()[multiModel.getObjects().count - 1].amount.becomeFirstResponder()
                            }
                        }else{
                            if  multiModel.getObjects()[0].mobileNumber.text == ""{
                                multiModel.getObjects()[0].mobileNumber.becomeFirstResponder()
                            }else{
                                multiModel.getObjects()[0].amount.becomeFirstResponder()
                            }
                        }
                        
                    }
            })
        }
        
        

        
    }
    
    private func checkAmountFieldNull() {
        guard let model = self.multipayModel else { return }
        var ishidden = false
        let objects = model.getObjects()
        
        // check for amount field
        for object in objects {
            if object.amount.text == "" {
                amountNullStatus = true
                ishidden = true
                self.addBtnContainer.isHidden = true
                self.submitBtn.isHidden = true
                break
            }
        }
        if !ishidden {
            self.addBtnContainer.isHidden = false
            self.submitBtn.isHidden = false
        }
        if objects.count > 4 {
            self.addBtnContainer.isHidden = true
        }
    }

    
    //MARK:- MultiPay TableView Delegates & Datasources
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cellArray[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if multipayModel != nil  {
            guard let model = self.multipayModel else { return 0.00 }
            return model.getObjects()[indexPath.row].heightUpdates + 90.00
        } else {
            return 90.0
        }
    }
    
    func didDeleteCell(_ cell: MultiPaytoTableViewCell) {
        guard let model = self.multipayModel else { return }
        if model.getObjects().count == 2 {
            
            var index = 0
            index = cell.indexPath?.row ?? 0
            
            let num = index + 1
            
            let messages = "Do you want to delete Payment Details".localized + " \(num)" + " ?"
            alertViewObj.wrapAlert(title: nil, body: messages, img: #imageLiteral(resourceName: "dashboard_pay_send"))
            alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
            }
            alertViewObj.addAction(title: "OK".localized, style: .target) {
                guard let firstObject = self.multipayModel?.getObjects().first else { return }
                firstObject.view.isUserInteractionEnabled = true
                self.delegate?.backToSinglePayment(vc: firstObject)
                self.navigationController?.popViewController(animated: true)
                //self.navigationController?.popToRootViewController(animated: true)
            }
            alertViewObj.showAlert()

        } else {
            
            var index = 0
            index = cell.indexPath?.row ?? 0
            
            let num = index + 1
            
            let messages = "Do you want to delete Payment Details".localized + " \((num))" + " ?"
            alertViewObj.wrapAlert(title: nil, body: messages, img: #imageLiteral(resourceName: "dashboard_pay_send"))
            alertViewObj.addAction(title: "OK".localized, style: .target) {
                guard let indexPath = cell.indexPath else { return }
                guard var objects = self.multipayModel?.getObjects() else { return }
                if objects.count > indexPath.row {
                    let removedObj = objects.remove(at: indexPath.row)
                    removedObj.view.removeFromSuperview()
                    self.multipayModel = PaytoObject<PayToUIViewController>.init(objects: objects)
                    self.loadCell()
                    self.multiPaytable.reloadData()
                    self.multiPaytable.scrollToTop()
                    guard let firstObject = self.multipayModel?.getObjects().last else { return }
                    
                    if firstObject.amount.text!.count > 0 {
                        self.addBtnContainer.isHidden = false
                        self.submitBtn.isHidden = false
                    } else {
                        self.addBtnContainer.isHidden = true
                        self.submitBtn.isHidden = true
                    }

                }
            }
            alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
            }
            alertViewObj.showAlert()
        }
    }
    
    
    //MARK:- Web Api Calls
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful {
            self.initiateCashbackPayment()
        }
    }
    
    func initiateCashbackPayment() {
        guard appDel.checkNetworkAvail() else {
            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.noInternet.localized)
            return
        }
        guard let url = PTManagerClass.createUrlComponents(base: PaytoConstants.url.multiCashback.safelyWrappingString()) else { return }
        self.cashbackApi(url)
    }
    
    func cashbackApi(_ url: URL) {
        let params = self.multiCashbackParameters()
        TopupWeb.genericClass(url: url, param: params as AnyObject, httpMethod: "POST") { (response, success) in
            DispatchQueue.main.sync {
                if success {
                    guard let arrayResponse = response as? [Any]  else { return }
                    for (index,subItem) in arrayResponse.enumerated() {
                        guard let dictionary = subItem as? Dictionary<String,Any> else { return }
                        if dictionary.safeValueForKey("Code") as? Int == 200 {
                            guard let xmlString = dictionary.safeValueForKey("Data") as? String else { return }
                            self.parsingCashbackData(xml: xmlString, withIndex: index)
                        }
                    }
                    DispatchQueue.main.async {
                        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "PayToTransactionConfirmationVC") as? PayToTransactionConfirmationVC else { return }
                        vc.displayResponse = self.transactionArray
                        vc.varType = PayToType(type: .payTo)
                        guard let model = self.multipayModel else { return }
                        vc.viewModel = model.getObjects()
                        if self.transactionArray.count > 0 {
                            self.navigationController?.pushViewController(vc, animated: false)
                            self.transactionArray.removeAll()
                        }
                    }
                }
            }
        }
    }
    
    func parsingCashbackData(xml: String, withIndex index: Int) {
        let xmlIndexer = SWXMLHash.parse(xml)
        self.enumerate(indexer: xmlIndexer) { (dictionary) in
            DispatchQueue.main.async {
                let selfModel = self.multipayModel?.getObjects()[index]
                var transaction = dictionary
                transaction["businessName"]    = selfModel?.merchant.text ?? ""
                transaction["destinationName"]  = selfModel?.subscriber.text ?? ""
                transaction["localRemark"]     = selfModel?.remarks.text ?? ""
                if (transaction["resultdescription"] as? String)?.lowercased() == "Transaction Successful".lowercased() {
                    self.transactionArray.append(transaction)
                }
            }
        }
    }
    
    func enumerate(indexer: XMLIndexer, handle :@escaping (_ dict: Dictionary<String,Any>) -> Void) {
        var dictionary : Dictionary<String,Any> = Dictionary<String,Any>()
        
        func indexing(indexer: XMLIndexer) {
            for child in indexer.children {
                guard let element = child.element else { return }
                dictionary[element.name] = element.text
                indexing(indexer: child)
            }
        }
        
        indexing(indexer: indexer)
        handle(dictionary)
    }
    
    func checkForSameNumberSameAmount() {
        
        guard let model = self.multipayModel else { return }
        
        let objects = model.getObjects()
        
        var number : String = ""
        var amount : String = ""
        
        for controller in objects {
            
            number = controller.mobileNumber.text.safelyWrappingString()
            
            amount = controller.amount.text.safelyWrappingString()
            
            for subController in objects {
                
                if controller != subController {
                    
                    if subController.mobileNumber.text.safelyWrappingString() == number, subController.amount.text.safelyWrappingString() == amount {
                        
                        subController.amount.text = ""
                        subController.burmeseAmount.text = ""
                        subController.amountClearButton()
                        if let rightView = subController.amount.rightView as? AmountRightView {
                            rightView.hideClearButton(hide: true)
                        }
                        subController.remarks.hide(); subController.amount.placeholder = "Enter Amount".localized;
                        
                        
                        
                        subController.amount.becomeFirstResponder()
                        subController.commissionTxt.text = ""
                        subController.totalAmountTxt.text = ""
                        subController.commissionTxt.hide(); subController.totalAmountTxt.hide()
                        subController.burmeseAmount.hide()
                
                        
                        PaytoConstants.alert.showErrorAlert(title: nil, body: "You cannot pay to same mobile number with same amount".localized)
                        
                        self.submitBtn.isHidden = true
                        
                        self.addBtnContainer.isHidden = true
                        
                        return
                    }

                }
                
            }
            
        }

        
    }
    
    
    func multiCashbackParameters() -> Dictionary<String,Any> {
        var finalObject =  Dictionary<String,AnyObject>()
        var dict        = [Dictionary<String,String>]()
        
        guard let model = self.multipayModel else { return finalObject }
        
        let objects = model.getObjects()
        
        for dataVC in objects {
            var cashbackobj = Dictionary<String,String>()
            cashbackobj["Agentcode"]     = UserModel.shared.mobileNo
            cashbackobj["Amount"]       = dataVC.formattedAmount
            cashbackobj["Clientip"]         = OKBaseController.getIPAddress()
            cashbackobj["Clientos"]        = "iOS"
            cashbackobj["Clienttype"]      = "GPRS"
            cashbackobj["Destination"]     = dataVC.formattedNumber
            cashbackobj["Pin"]           = ok_password ?? ""
            cashbackobj["Requesttype"]    = "FEELIMITKICKBACKINFO"
            cashbackobj["Securetoken"]    = UserLogin.shared.token
            cashbackobj["Transtype"]      = self.getTransactionString(dataVC.getTransactionType)
            cashbackobj["Vendorcode"]     = "IPAY"
            dict.append(cashbackobj)
        }
        
        var log = Dictionary<String,Any>()
        log["MobileNumber"] = UserModel.shared.mobileNo
        log["Msid"]        = getMsid()
        log["Ostype"]      = 1
        log["Otp"]        = ""
        log["SimId"]      = uuid
        
        finalObject["CashBackRequestList"] = dict as AnyObject
        finalObject["Login"] = log as AnyObject
        return finalObject
    }


}
