//
//  PaytoViews.swift
//  OK
//
//  Created by Ashish Kumar Singh on 11/10/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

 enum ButtonType {
    case countryView
    case contactView
}

protocol PTClearButtonDelegate : class {
    func clearField(strTag: String)
}

 protocol CountryLeftViewDelegate : class {
     func openAction(type: ButtonType)
     func clearActionType()
}

protocol CICOScanQROpenDelegate: class {
    func openQRToScanToPay()
}

protocol HideMyNumberDelegate : class {
    func hideMyNumberDelegate(hide: Bool) -> Void
}

protocol HideClearButtonDelegate : class {
    func hideClearButton(hide: Bool)
}


class PaytoViews: UIView {
    
    @IBOutlet var countryImage: UIImageView!
    @IBOutlet var countryLabel: UILabel!
    
   weak var delegate : CountryLeftViewDelegate?
    
    class func updateView() -> PaytoViews {
        let nib   =  UINib(nibName: "PayToLeftView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as! PaytoViews
        nib.frame = .init(x: 0.00, y: 0.00, width: 110.00, height: 40.00)
        nib.layoutUpdates()
        return nib
    }
    
    class func confupdateView() -> PaytoViews {
           let nib   =  UINib(nibName: "PayToLeftView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[0] as! PaytoViews
           nib.frame = .init(x: 0.00, y: 0.00, width: 110.00, height: 40.00)
           nib.layoutUpdates()
           return nib
       }
    func layoutUpdates() {
        self.layoutIfNeeded()
    }
    
    func wrapCountryViewData(img: String, str: String) {
        self.countryImage.image = UIImage.init(named: img)
        self.countryImage.contentMode = .scaleToFill
        if str.contains(find: "(") {
            self.countryLabel.text  = str
        } else {
            self.countryLabel.text  = "(\(str))"
        }
        
        
        let size   = self.countryLabel.text?.size(withAttributes: [.font: self.countryLabel.font]) ?? .zero
        let vWidth = 55 + size.width
        self.frame = .init(x: 0, y: 0, width: vWidth, height: 40.00)
        self.layoutIfNeeded()
    }
    
    func wrapinternationalCountryViewData(img: String, str: String) {
        self.countryImage.image = UIImage.init(named: img)
        self.countryImage.contentMode = .scaleAspectFit
        if str.contains(find: "(") {
            self.countryLabel.text  = str
        } else {
            self.countryLabel.text  = "(\(str))"
        }
        
        
        let size   = self.countryLabel.text?.size(withAttributes: [.font: self.countryLabel.font]) ?? .zero
        let vWidth = 55 + size.width
        self.frame = .init(x: 0, y: 0, width: vWidth, height: 40)
        self.layoutIfNeeded()
    }
   
    @IBAction func countryLeftViewAction(_ sender: UIButton) {
        if let delegate = delegate {
            delegate.openAction(type: ButtonType.countryView)
        }
    }
    
    deinit {
        self.delegate = nil
    }
}

protocol cashBackMobClearProtocol: class {
    func mobClearfield()
}

class ContactRightView : UIView, HideClearButtonDelegate {
    
    @IBOutlet weak var imgClearButton : UIImageView!
    @IBOutlet weak var buttonClear    : UIButton!
    
    @IBOutlet var scanQRImage : UIImageView! {
        didSet {
            self.scanQRImage.isHidden = true
        }
    }
    @IBOutlet var scanQRButton    : UIButton! {
        didSet {
             self.scanQRButton.isHidden = true
        }
    }
    @IBOutlet var scanQRImageWidth : NSLayoutConstraint! {
        didSet {
            scanQRImageWidth.constant = 0
        }
    }
    @IBOutlet var scanQRButtonWidth    : NSLayoutConstraint! {
        didSet {
            scanQRButtonWidth.constant = 0
        }
    }
    
    weak var delegate : CountryLeftViewDelegate?
    weak var cashBackDelegate: cashBackMobClearProtocol?
    weak var scanQRDelegate: CICOScanQROpenDelegate?


    class func updateView(show: Bool = false) -> ContactRightView {
        let nib   =  UINib(nibName: "PayToLeftView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[1] as! ContactRightView
        if show {
            nib.frame = .init(x: 0.00, y: 0.00, width: 120.00, height: 50.00)
        } else {
            nib.frame = .init(x: 0.00, y: 0.00, width: 80.00, height: 50.00)
        }
        nib.layoutUpdates()
        return nib
    }
    
    func layoutUpdates() {
        self.layoutIfNeeded()
    }
    

    func hideClearButton(hide: Bool) {
        self.imgClearButton.isHidden = hide
        self.buttonClear.isHidden = hide
    }
    
    @IBAction func clearMobileNumberAction(_ sender: UIButton) {
        if let delegate = self.delegate  {
            delegate.clearActionType()
        }
        self.cashBackDelegate?.mobClearfield()
    }
    
    @IBAction func countryLeftViewAction(_ sender: UIButton) {
        if let delegate = delegate {
            delegate.openAction(type: ButtonType.contactView)
        }
    }
    
    @IBAction func scanQRBtnAction(_ sender: UIButton) {
        if let delegate = scanQRDelegate {
            delegate.openQRToScanToPay()
        }
    }
    
    deinit {
        self.delegate = nil
    }

}

class HideMyView : UIView {
    @IBOutlet weak var imageCheckBox: UIImageView!
    @IBOutlet weak var mobileNumber: UILabel!
    @IBOutlet weak var hideMyNumber: UILabel!
        {
        didSet
        {
            hideMyNumber.font = UIFont(name: appFont, size: appTitleButton)
            hideMyNumber.text = "Hide My Number".localized + " " + "-"
        }
    }
    weak var delegate : HideMyNumberDelegate?
    
    var formattedNumber : String {
        var number = UserModel.shared.mobileNo
        let object = PTManagerClass.decodeMobileNumber(phoneNumber: number)
        number = String.init(format: "(%@) %@", object.country.dialCode ?? "+95", object.number.deletingPrefix("0"))
        return number
    }
    
    let hiddenString = "XXXXXXXXXX"
    
    class func updateView(_ fr: CGRect, delegate: HideMyNumberDelegate) -> HideMyView {
        let nib   =  UINib(nibName: "PayToLeftView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[4] as! HideMyView
        nib.frame = fr
        nib.delegate = delegate
        nib.layoutUpdates()
        return nib
    }
    
    @IBAction func HideMyNumberAction(_ sender: UIButton) {
        if let delegate = self.delegate {
            
            UIView.animate(withDuration: 0.2, animations: {
                self.imageCheckBox.isHighlighted = !self.imageCheckBox.isHighlighted
                
                self.mobileNumber.text = (!self.imageCheckBox.isHighlighted) ? self.formattedNumber : self.hiddenString
                
                delegate.hideMyNumberDelegate(hide: self.imageCheckBox.isHighlighted)
            })
            
        } else {
            
            self.imageCheckBox.isHighlighted = false
            
            self.mobileNumber.text = UserModel.shared.formattedNumber
        }
    }
    
    func fireDelegate() {
        self.imageCheckBox.isHighlighted = false
        
        self.mobileNumber.text = (!self.imageCheckBox.isHighlighted) ? UserModel.shared.formattedNumber : self.hiddenString
        
        delegate?.hideMyNumberDelegate(hide: self.imageCheckBox.isHighlighted)
    }
    
    func layoutUpdates() {
        self.mobileNumber.text = formattedNumber
        self.layoutIfNeeded()
    }
    
}

class TimerView : UIView {
    
    @IBOutlet weak var timerLaber: UILabel!{
        didSet{
            self.timerLaber.font = UIFont(name: appFont, size: 8.0)
        }
    }
    @IBOutlet weak var circularView: CardDesignView!
    @IBOutlet weak var timerHeight: NSLayoutConstraint!
    @IBOutlet weak var timerWidth: NSLayoutConstraint!
    @IBOutlet weak var secondsLeftString: UILabel!{
        didSet{
            self.secondsLeftString.font = UIFont(name: appFont, size: 8.0)
        }
    }
    
    @IBOutlet weak var secondstrailingcontraint: NSLayoutConstraint!
    class func updateView() -> TimerView {
        let nib = UINib.init(nibName: "PayToLeftView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[6]  as! TimerView
//        if UIDevice.current.type == Model.iPhoneX {
//            nib.frame = .init(x: screenWidth/2 - 30.00, y: 30.00, width: 106.00, height: 64.00)
//        } else {
//            nib.frame = .init(x: screenWidth/2 - 65.00, y: 20.00, width: 106.00, height: 64.00)
//        }
        
        if UIDevice.current.type == Model.iPhoneX || UIDevice.current.type == Model.iPhoneXS || UIDevice.current.type == Model.iPhoneXSMax || UIDevice.current.type == Model.iPhone11 || UIDevice.current.type == Model.iPhone11Pro || UIDevice.current.type == Model.iPhone11ProMax{
            nib.frame = .init(x: screenWidth - 115.00, y: 33.0, width: 112.00, height: 64.00)
        }
        else if UIDevice.current.type == Model.iPhone12ProMax {
            nib.frame = .init(x: screenWidth - 115.00, y: 43.00, width: 112.00, height: 64.00)
        }
        else {
            nib.frame = .init(x: screenWidth - 115.00, y: 20.00, width: 112.00, height: 64.00)
        }
        nib.layoutUpdates()
        return nib
    }
    
    func layoutUpdates() {
        self.layoutIfNeeded()
    }
    
    func wrapTimerView(timer: String) {
        self.timerLaber.text = timer
    }
    
    func changeFrameForMulti() {
        
        self.circularView.cornerRadius = 20.00
        self.timerHeight.constant = 40
        self.timerWidth.constant = 40
        
        if UIDevice.current.type == Model.iPhoneX || UIDevice.current.type == Model.iPhoneXR || UIDevice.current.type == Model.iPhoneXS || UIDevice.current.type == Model.iPhoneXSMax || UIDevice.current.type == Model.iPhone11 || UIDevice.current.type == Model.iPhone11Pro || UIDevice.current.type == Model.iPhone11ProMax {
            self.frame = .init(x: screenWidth - 115.00, y: 44.0, width: 112.00, height: 64.00)
        }
        else if UIDevice.current.type == Model.iPhone12ProMax {
            self.frame = .init(x: screenWidth - 115.00, y: 43.00, width: 112.00, height: 64.00)
        }
        else {
            self.frame = .init(x: screenWidth - 115.00, y: 20.00, width: 112.00, height: 64.00)
        }

       // guard let secfont = UIFont(name: appFont, size: 11.00) else { return }
     //   guard let timerfont = UIFont(name: appFont, size: 12.00) else { return }
        self.secondsLeftString.font = UIFont(name: appFont, size: 11.00)
        self.timerLaber.font =  UIFont(name: appFont, size: 12.00)
        
        if appFont == "Myanmar3"{
            self.secondsLeftString.text = "ကျန်ရှိအချိန် (စက္ကန့်)"
        }else{
            self.secondsLeftString.text = "Seconds left".localized
        }
        
        
        
       
        
        
        self.layoutIfNeeded()
    }
    
    func wrapTimerViewUpdate(color: UIColor = UIColor.black, alpha: CGFloat = 1.00) {
        self.timerLaber.textColor = color
        self.timerLaber.alpha = alpha
        
    }
    
    func wrapOrigin(point: CGPoint, size: CGSize) {
        self.frame.origin = point
        self.frame.size   = size
        self.layoutUpdates()
    }
}

class PTClearButton : UIView, HideClearButtonDelegate {
    weak var delegate : PTClearButtonDelegate?
    
    @IBOutlet weak var imgClearButton : UIImageView!
    @IBOutlet weak var buttonClear    : UIButton!

    
    var stringTag : String?
    
    class func updateView(strTag: String, delegate: PTClearButtonDelegate) -> PTClearButton {
        let nib   =  UINib(nibName: "PayToLeftView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[5] as! PTClearButton
            nib.frame = .init(x: 0.00, y: 0.00, width: 50.00, height: 40.00)
        nib.stringTag = strTag
        nib.delegate  = delegate
        nib.layoutUpdates()
        return nib
    }
    
    func updateView1(strTag: String, delegate: PTClearButtonDelegate) -> PTClearButton {
        let nib   =  UINib(nibName: "PayToLeftView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[5] as! PTClearButton
        nib.frame = .init(x: 0.00, y: 0.00, width: 50.00, height: 40.00)
        nib.stringTag = strTag
        nib.delegate  = delegate
        nib.layoutUpdates()
        return nib
    }
    
    func layoutUpdates() {
        self.layoutIfNeeded()
    }
    
    func hideClearButton(hide: Bool) {
        self.imgClearButton.isHidden = hide
        self.buttonClear.isHidden = hide
    }
    
    @IBAction func clearButtonAction(_ sender: UIButton) {
        if let delegate = self.delegate, let str = stringTag {
            delegate.clearField(strTag: str)
        }
    }

}

protocol AmountRightViewDelegate : class {
    func amountClearButton()
    func soundOffOn(sender: UIButton)
}

class AmountRightView : UIView, HideClearButtonDelegate {
    
    @IBOutlet weak var amountClearButton : UIButton!
    @IBOutlet weak var soundButton : UIButton!
    @IBOutlet weak var amountClearImageView : UIImageView!
    
    weak var delegate : AmountRightViewDelegate?

    class func updateView(_ image: String) -> AmountRightView {
        let nib = UINib(nibName: "PayToLeftView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[7] as! AmountRightView
        nib.frame = .init(x: 0, y: 0, width: 80.00, height: 47)
        nib.soundButton.setImage(UIImage.init(named: image), for: .normal)
        nib.layoutIfNeeded()
        return nib
    }
    
    func hideClearButton(hide: Bool) {
        if hide {
            self.amountClearImageView.image = UIImage(named: "")
           self.amountClearButton.isUserInteractionEnabled = false
//            self.amountClearButton.setImage(UIImage(named: ""), for: .normal)
        } else {
            self.amountClearButton.isUserInteractionEnabled = true
            self.amountClearImageView.image = UIImage(named: "close_grey")
//            self.amountClearButton.setImage(UIImage(named: "crossCircle"), for: .normal)
        }
        
        //self.soundButton.isHidden = hide
    }
    
    @IBAction func clearButtonAction(_ sender: UIButton) {
        if sender == amountClearButton {
            self.delegate?.amountClearButton()
        } else {
            self.delegate?.soundOffOn(sender: sender)
        }
    }

}

class DefaultIconView : UIView {
    
    @IBOutlet var iconImageView: UIImageView!

    class func updateView(icon: String) -> DefaultIconView {
        let nib   =  UINib(nibName: "PayToLeftView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[2] as! DefaultIconView
        if icon == "pt_categories" {
            nib.frame = .init(x: 0.00, y: 0.00, width: 43 + 15, height: 40.00)
        } else {
            nib.frame = .init(x: 0.00, y: 0.00, width: 50.00 + 15, height: 40.00)
        }
        nib.layoutUpdates(img: icon)
        return nib
    }
    
    class func updateViews(icon: String) -> DefaultIconView {
        let nib   =  UINib(nibName: "PayToLeftView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[2] as! DefaultIconView
        if icon == "pt_categories" {
            nib.frame = .init(x: 0.00, y: 0.00, width: 43, height: 40.00)
        } else {
            nib.frame = .init(x: 0.00, y: 0.00, width: 50.00, height: 40.00)
        }
        nib.layoutUpdates(img: icon)
        return nib
    }

    
    class func updateView(icon: UIImage) -> DefaultIconView {
        let nib   =  UINib(nibName: "PayToLeftView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[2] as! DefaultIconView
        nib.frame = .init(x: 0.00, y: 0.00, width: 50.00 + 15, height: 40.00)
        return nib
    }
    
    class func mobileupdateView(icon: String) -> DefaultIconView {
        let nib   =  UINib(nibName: "PayToLeftView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[2] as! DefaultIconView
        if icon == "pt_categories" {
            nib.frame = .init(x: 0.00, y: 0.00, width: 30, height: 30.00)
        } else {
            nib.frame = .init(x: 0.00, y: 0.00, width: 30, height: 30.00)
        }
        nib.layoutUpdates(img: icon)
        return nib
    }
    
    func classLayout(ing: UIImage) {
        self.iconImageView.image = ing
        self.layoutIfNeeded()
    }

    
    func layoutUpdates(img: String) {
        self.iconImageView.image = UIImage.init(named: img)
        self.iconImageView.contentMode = .scaleAspectFit
        self.layoutIfNeeded()
    }

}

class ExpandCollapseIconView : UIView {
    
    @IBOutlet var iconImageView: UIImageView!
    
    
    class func updateView(icon: String) -> ExpandCollapseIconView {
        let nib   =  UINib(nibName: "PayToLeftView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[3] as! ExpandCollapseIconView
        nib.frame = .init(x: 0.00, y: 0.00, width: 50, height: 40.00)
        nib.layoutUpdates(img: icon)
        return nib
    }
    
    
    
    func layoutUpdates(img: String) {
        self.iconImageView.image = UIImage.init(named: img)
        self.layoutIfNeeded()
    }
    
}

//MARK:- Tab bar view for reciept

enum RecieptTabBarButton {
    case addFav, addContact, home, share
}

protocol ReceiptTabBarDelegate : class {
    func didSelectTabbarButton(id: RecieptTabBarButton)
    func addedToFavorite(flag: Bool, number: String)
    func addedToContact(flag: Bool, number: String)

}

@IBDesignable class TabFooterView : UIView {
    
    weak var delegate : ReceiptTabBarDelegate?
    
    @IBOutlet weak var addFav : UIButton! {
        didSet {
            
            if appFont == "Myanmar3"{
                addFav.titleLabel?.font = UIFont(name: appFont, size: 12.0)
            }else{
                addFav.titleLabel?.font = UIFont(name: appFont, size: 13.0)
            }
            
            addFav.setTitle("Add Favorite".localized, for: .normal)
        }
    }
    @IBOutlet weak var addContact : UIButton! {
        didSet {
            
            if appFont == "Myanmar3"{
                addContact.titleLabel?.font = UIFont(name: appFont, size: 12.0)
            }else{
                addContact.titleLabel?.font = UIFont(name: appFont, size: 13.0)
            }
            
            addContact.setTitle("Add Contact".localized, for: .normal)
        }
    }
    @IBOutlet weak var home : UIButton! {
        didSet {
            
            if appFont == "Myanmar3"{
                home.titleLabel?.font = UIFont(name: appFont, size: 12.0)
            }else{
                home.titleLabel?.font = UIFont(name: appFont, size: 13.0)
            }
            
            home.setTitle("Home".localized, for: .normal)
        }
    }
    @IBOutlet weak var more : UIButton! {
        didSet {
            
            if appFont == "Myanmar3"{
                more.titleLabel?.font = UIFont(name: appFont, size: 12.0)
            }else{
                more.titleLabel?.font = UIFont(name: appFont, size: 13.0)
            }
            
            more.setTitle("More".localized, for: .normal)
        }
    }
    
     func updateView() -> TabFooterView {
        let nib   =  UINib(nibName: "PayToLeftView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil)[8] as! TabFooterView
        nib.frame = .init(x: 0.00, y: 0.00, width: screenWidth, height: 50.00)
        nib.layoutUpdates()
        return nib
    }
    
    func layoutUpdates() {
        self.addFav.centerVertically()
        self.addContact.centerVertically()
        self.home.centerVertically()
        self.more.centerVertically()
        self.layoutIfNeeded()
    }
    
    @IBAction func btnAction(_ sender: UIButton) {
        guard let delegate = self.delegate else { return }
        if sender.tag == 1 {
            delegate.didSelectTabbarButton(id: .addFav)
        } else if sender.tag == 2 {
            delegate.didSelectTabbarButton(id: .addContact)
        } else if sender.tag == 3 {
            delegate.didSelectTabbarButton(id: .home)
        } else if sender.tag == 4 {
            delegate.didSelectTabbarButton(id: .share)
        }
    }
    
    func checkNumber(_ adentCode: String, isContact: Bool) {
        switch isContact {
        case true:
            self.checkForContact(adentCode)
        case false:
            self.checkForFavorites(adentCode)
        }
    }
    
    private func checkForContact(_ mobile: String) {
        
        guard let contactDelegate = self.delegate else { return }
        
        let isContactPresent = ContactManager().agentgentCodePresentinContactsLoc(number: mobile)
        
        contactDelegate.addedToContact(flag: isContactPresent, number: mobile)
        
    }
    
    private func checkForFavorites(_ number: String) {
        guard let favDel   = self.delegate else { return }
        let decodedNumber  = PTManagerClass.decodeMobileNumber(phoneNumber: number)
        let favoritesArray = favoriteManager.fetchRecordsForFavoriteContactsEntity()
        for favorite in favoritesArray {
            let numberTuple = PTManagerClass.decodeMobileNumber(phoneNumber: favorite.phone.safelyWrappingString())
            if numberTuple.number == decodedNumber.number {
                favDel.addedToFavorite(flag: true, number: number)
                break
            } else {
                println_debug("Star Settings")
            }
        }
    }
    
}
