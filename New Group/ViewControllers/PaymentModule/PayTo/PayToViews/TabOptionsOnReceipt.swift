//
//  TabOptionsOnReceipt.swift
//  OK
//
//  Created by E J ANTONY on 15/11/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit

protocol TabOptionProtocol: class {
    func addFavorite()
    func addContact()
    func goHome()
    func showMore()
}

class TabOptionsOnReceipt: UIView {
    
    //MARK: - Outlets
    //ImageView
    @IBOutlet weak var imgAddFav: UIImageView!
    @IBOutlet weak var imgAddContact: UIImageView!
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var imgMore: UIImageView!
    
    //Label
    @IBOutlet weak var lblAddFav: MarqueeLabel!{
        didSet{
            
            if appFont == "Myanmar3"{
                lblAddFav.font = UIFont(name: appFont, size: 12.0)
            }else{
                lblAddFav.font = UIFont(name: appFont, size: 13.0)
            }
            
            
        }
    }
    @IBOutlet weak var lblAddContact: MarqueeLabel!{
        didSet{
            
            if appFont == "Myanmar3"{
                lblAddContact.font = UIFont(name: appFont, size: 12.0)
            }else{
                lblAddContact.font = UIFont(name: appFont, size: 13.0)
            }
            
            
            
        }
    }
    @IBOutlet weak var lblHome: MarqueeLabel!{
        didSet{
            
            if appFont == "Myanmar3"{
                lblHome.font = UIFont(name: appFont, size: 12.0)
            }else{
                lblHome.font = UIFont(name: appFont, size: 13.0)
            }
            
        }
    }
    @IBOutlet weak var lblMore: MarqueeLabel!{
        didSet{
            
            if appFont == "Myanmar3"{
                lblMore.font = UIFont(name: appFont, size: 12.0)
            }else{
                lblMore.font = UIFont(name: appFont, size: 13.0)
            }
            
        }
    }
    
    //views
    @IBOutlet weak var viewAddFav: UIView!
    @IBOutlet weak var viewAddContact: UIView!
    @IBOutlet weak var viewHome: UIView!
    @IBOutlet weak var viewMore: UIView!

    
    
    //MARK: - Properties
    var delegate: TabOptionProtocol?
    
    //MARK: - Button Actions
    @IBAction func addFavorite(_ sender: UIButton) {
        delegate?.addFavorite()
    }
    
    @IBAction func addContact(_ sender: UIButton) {
        delegate?.addContact()
    }
    
    @IBAction func goHome(_ sender: UIButton) {
        delegate?.goHome()
    }
    
    @IBAction func showMore(_ sender: UIButton) {
        delegate?.showMore()
    }
}
