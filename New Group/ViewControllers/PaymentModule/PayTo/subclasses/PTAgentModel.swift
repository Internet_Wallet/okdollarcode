//
//  PTAgentModel.swift
//  OK
//
//  Created by Ashish on 8/4/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct PTAgentModel {
    let mercantName, merchantCatName: String?
    let status: Bool?
    let localTransType, targeReturnURL, accountType, businessName, payToPhotoCapurePicAmountLimit: String?
    let transAmountLimit, transStatus, profilePic: String?
    let destinationNumberRegStatus: Bool?
    var isOfferApplicable: Bool?
    var photoCaptureStatus: Int?

    init(withDictionary dictionary: Dictionary<String,Any>) {
        self.mercantName = dictionary.safeValueForKey("MercantName") as? String
        self.merchantCatName = dictionary.safeValueForKey("MerchantCatName") as? String
        self.status = dictionary.safeValueForKey("status") as? Bool
        self.localTransType = dictionary.safeValueForKey("LocalTransType") as? String
        self.targeReturnURL = dictionary.safeValueForKey("TargeReturnUrl") as? String
        self.accountType = dictionary.safeValueForKey("AccountType") as? String
        self.businessName = dictionary.safeValueForKey("BusinessName") as? String
        self.transAmountLimit = dictionary.safeValueForKey("transAmountLimit") as? String
        self.transStatus = dictionary.safeValueForKey("TransStatus") as? String
        self.profilePic  = dictionary.safeValueForKey("profilePic") as? String
        self.destinationNumberRegStatus = dictionary.safeValueForKey("DestinationNumberRegStatus") as? Bool
        self.isOfferApplicable = dictionary.safeValueForKey("IsOfferApplicable") as? Bool
        self.payToPhotoCapurePicAmountLimit = dictionary.safeValueForKey("PayToPhotoCapurePicAmountLimit") as? String
        self.photoCaptureStatus = dictionary.safeValueForKey("PhotoCaptureStatus") as? Int
    }
}

struct PTCashbackModel : Codable {
    let agentcode, agentname, amount, clientip: String?
    let clientos, clienttype, comments, destination: String?
    let estel, fee, header, kickback: String?
    let limit, limittype, requestcts, response: String?
    let responsetype, resultcode, resultdescription, securetoken: String?
    let source, transid, transtype, vendorcode: String?
}

struct PTConfirmationModel {
    let receiver, name, businessName, amount : String?
    let cashback : String?
    let rateService : Bool? = false
    let isCashback  : Bool? = false
}

struct PaySendModel {
    let number : String?
    let cnfNumber : String?
    let name : String?
}




















