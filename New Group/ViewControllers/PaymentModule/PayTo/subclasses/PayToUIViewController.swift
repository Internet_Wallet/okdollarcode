//
//  PayToUIViewController.swift
//  OK
//
//  Created by Ashish on 7/31/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol PaytoHeightChanged : class {
    func didChangeHeight(height: CGFloat, withIndex path: IndexPath?)
    func didResetUI(path: IndexPath?)
    func didShowSubmitButton(show: Bool, path: IndexPath?)
    func checkForSameNumberSameAmount()
    func didShowAdvertisement(show: Bool)
    func didShowAgentButton(hide: Bool)
}

protocol DeviceSoundDelegateBurmeseVoice : class {
    func didChangeVoice()
    func didChangeVoice(status: Bool)
}

protocol submitTextChange: class {
    func changeSubmitBtnTitle()
}


class PayToUIViewController: OKBaseController, PTTextFieldSubclassDelegate, PTMultiSelectionDelegate, CountryViewControllerDelegate, CountryLeftViewDelegate, FavoriteSelectionDelegate, ContactPickerDelegate, ScrollChangeDelegate, AmountRightViewDelegate, BurmeseSoundObjectDelegate, HideMyNumberDelegate, PTClearButtonDelegate, UITextFieldDelegate, PTWebResponseDelegate {
    
    var balanceListArray = [Dictionary<String,Any>]()
    var balanceDict = Dictionary<String,Any>()
    
    @IBOutlet weak var walletSizeView: UIView!
    @IBOutlet weak var walletSizeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var maxWalletSizeLbl: UILabel!
    @IBOutlet weak var perDayTxnLbl: UILabel!
    @IBOutlet weak var monthlyTxtLbl: UILabel!
    @IBOutlet weak var remPerDayTxtLbl: UILabel!
    @IBOutlet weak var remMonthlyTxnLbl: UILabel!
    
    var agentPayToModel: PTAgentModel?
    var offerArrModel: [OffersListModel]?
    var mitQRCode = false
    var referenceNumberMIT = ""
    var favNav: UINavigationController?
   // var isComingFromMultipayTo = false
    
    //var amountCheck = 0
    
    
    @IBOutlet weak var offersTextField: PTOfferSubclass!
    @IBOutlet weak var offersPromoCodeLabel: PTPromoSubclass!

    @IBOutlet weak var mobileNumber: PTMobileNumberSubclass! {
        didSet {
            self.mobileNumber.font = UIFont(name: appFont, size: 16.0)
            self.mobileNumber.placeholder = "Enter Mobile Number".localized
        }
    }
    @IBOutlet weak var cnfMobileNumber: PTMobileNumberSubclass! {
        didSet {
            self.cnfMobileNumber.font = UIFont(name: appFont, size: 16.0)
            self.cnfMobileNumber.placeholder = "Confirm Mobile Number".localized
        }
    }
    @IBOutlet weak var name: PTTextFieldSubclass! {
        didSet {
            self.name.font = UIFont(name: appFont, size: 15.5)
            self.name.placeholder = "Name".localized
        }
    }
    @IBOutlet weak var userType: PTTextFieldSubclass! {
        didSet {
            self.userType.font = UIFont(name: appFont, size: 15.5)
            self.userType.placeholder = "Category".localized
        }
    }
    @IBOutlet weak var addVehicle: PTAddVehicleSubclass! {
        didSet {
            self.addVehicle.font = UIFont(name: appFont, size: 15.5)
            self.addVehicle.placeholder = "Select or Add Vehicle".localized
        }
    }
    @IBOutlet weak var subscriber: PTTextFieldSubclass! {
        didSet {
         //   self.subscriber.font = UIFont(name: appFont, size: appFontSize)
            self.subscriber.placeholder = "Subscriber".localized
        }
    }
    @IBOutlet weak var merchant: PTTextFieldSubclass! {
        didSet {
            self.merchant.font = UIFont(name: appFont, size: 15.5)
            self.merchant.placeholder = "Business Name".localized
        }
    }
    
    @IBOutlet var merchantLabel: MarqueeLabel!{
        didSet{
            merchantLabel.font = UIFont(name: appFont, size: 15.5)
        }
    }
    
    @IBOutlet weak var amount: PTAmountValidation! {
        didSet {
            self.amount.font = UIFont(name: appFont, size: 15.5)
            self.amount.placeholder = "Enter Amount".localized
        }
    }
    @IBOutlet weak var burmeseAmount: PTTextFieldSubclass! {
        didSet {
            self.burmeseAmount.font = UIFont(name: appFont, size: 15.5)
            self.burmeseAmount.placeholder = "Burmese Amount".localized
        }
    }
    @IBOutlet weak var remarks: PTTextFieldSubclass! {
        didSet {
            self.remarks.font = UIFont(name: appFont, size: 15.5)
            self.remarks.placeholder = "Enter Remarks".localized
        }
    }
    @IBOutlet weak var commissionTxt: PTTextFieldSubclass! {
        didSet {
            self.commissionTxt.font = UIFont(name: appFont, size: 15.5)
            self.commissionTxt.placeholder = "Agent Commission".localized
            self.commissionTxt.isUserInteractionEnabled = false
        }
    }
    @IBOutlet weak var totalAmountTxt: PTTextFieldSubclass! {
        didSet {
            self.totalAmountTxt.font = UIFont(name: appFont, size: 15.5)
            self.totalAmountTxt.placeholder = "Cash InOut Total Amount".localized
            self.totalAmountTxt.isUserInteractionEnabled = false
        }
    }
    
    @IBOutlet weak var agentCodeTxt: PTTextFieldSubclass! {
        didSet {
            self.agentCodeTxt.font = UIFont(name: appFont, size: 15.5)
            self.agentCodeTxt.isHidden = true
            self.agentCodeTxt.placeholder = "Agent Code".localized
            self.agentCodeTxt.isUserInteractionEnabled = false
        }
    }
    var cashInLimit = 0.0
    var cashOutLimit = 0.0
    var cashInfree = false
    var cashOutFree = false
    var cashInFreeWithCommission = false
    var transferToWithCommission = false
    var needToTakePhoto = false
    var takePhotoAmount = ""
    var accountType: String?
    @IBOutlet weak var hideMyNumber: PTHideMyNumberClass!
    
    var cnfRightView : PTClearButton?
    
    @IBOutlet weak var holderView: CardDesignView!
    
    weak var delegate : PaytoHeightChanged?
    
    weak var titleDelegate: submitTextChange?

    weak var hideDelegate : HideClearButtonDelegate?
    
    weak var deviceSound : DeviceSoundDelegateBurmeseVoice?
    
    var indexPath : IndexPath?
    
    var amountCheckingTimer: Timer?
    
    let objectSound = BurmeseSoundObject()
    
    //var transType : PaytoIDTransactionType = PaytoIDTransactionType.paytoWithID
    var transType: PayToType = PayToType(type: .payTo)
    
    var isOfferEnabled: Bool = false // for direct offer validation
    
    var isInDirectOffer: Bool = false
    
    var offerModel: OffersListModel?
    
    var localTransactionType : String {
        let text = self.userType.text
        return text.safelyWrappingString()
    }
    
    var formattedAmount : String {
        guard var amount = self.amount.text else { return "" }
        amount = amount.replacingOccurrences(of: ",", with: "")
        return amount
    }
    
    var formattedTotalAmount : String {
        guard var amount = self.totalAmountTxt.text else { return "" }
        amount = amount.replacingOccurrences(of: ",", with: "")
        return amount
    }
    
    var formattedNumber : String {
        guard var mobile = self.amount.text else { return "" }
         mobile = self.getAgentCode(numberWithPrefix: self.mobileNumber.text ?? "", havingCountryPrefix: self.mobileNumber.country.dialCode)
        return mobile
    }
    
    var cashInCashOut = ""
    
    //    var getTransactionType : PaytoIDTransactionType {
    //        return transType
    //    }
    var getTransactionType : PayToType {
        return transType
    }
    
    var heightUpdates : CGFloat {
        
        self.view.frame = CGRect.zero
        
        let promoHeight      = self.offersPromoCodeLabel.constraints.first(where: {$0.identifier == "heightConstraints"})?.constant ?? 0.0
        let offerHeight      = self.offersTextField.constraints.first(where: {$0.identifier == "heightConstraints"})?.constant ?? 0.0
        let mobHeight        = self.mobileNumber.constraints.first(where: {$0.identifier == "heightConstraints"})?.constant ?? 0.0
        let cnfHeight        = self.cnfMobileNumber.constraints.first(where: {$0.identifier == "heightConstraints"})?.constant ?? 0.0
        let nameHeight       = self.name.constraints.first(where: {$0.identifier == "heightConstraints"})?.constant ?? 0.0
        let typeHeight       = self.userType.constraints.first(where: {$0.identifier == "heightConstraints"})?.constant ?? 0.0
        let addVehicleHeight = self.addVehicle.constraints.first(where: {$0.identifier == "heightConstraints"})?.constant ?? 0.0
        let subsHeight       = self.subscriber.constraints.first(where: {$0.identifier == "heightConstraints"})?.constant ?? 0.0
        let merHeight        = self.merchant.constraints.first(where: {$0.identifier == "heightConstraints"})?.constant ?? 0.0
        let amountHeight     = self.amount.constraints.first(where: {$0.identifier == "heightConstraints"})?.constant ?? 0.0
        let burHeight        = self.burmeseAmount.constraints.first(where: {$0.identifier == "heightConstraints"})?.constant ?? 0.0
        let comsnHeight      = self.commissionTxt.constraints.first(where: {$0.identifier == "heightConstraints"})?.constant ?? 0.0
        let comsnBurHeight   = self.totalAmountTxt.constraints.first(where: {$0.identifier == "heightConstraints"})?.constant ?? 0.0
        let remarkHeight     = self.remarks.constraints.first(where: {$0.identifier == "heightConstraints"})?.constant ?? 0.0
        let hideHeight       = self.hideMyNumber.constraints.first(where: {$0.identifier == "heightConstraints"})?.constant ?? 0.0
        let first  = promoHeight + offerHeight + mobHeight + cnfHeight + nameHeight
        let second = subsHeight + merHeight + amountHeight + burHeight + comsnHeight + comsnBurHeight
        let third  = typeHeight + addVehicleHeight + remarkHeight + hideHeight
        var final  = first + second + third + 16.00 + self.heightContactSuggestion.constant
        if cnfHeight != 0.0 && self.heightContactSuggestion.constant != 0.0 {
            final = final - cnfHeight
        }
        let walletSize: CGFloat = (self.walletSizeView.isHidden) ? 0.0 : 190.0
        final = final + walletSize
        self.view.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: final)
        self.view.layoutIfNeeded()
        return final
    }
    
   

    // contact suggestion view
    @IBOutlet weak var contactSuggestion: CardDesignView!
    @IBOutlet weak var contactTableView: UITableView!
    @IBOutlet weak var heightContactSuggestion: NSLayoutConstraint!
    var contactSearchedDict = [Dictionary<String,Any>]()
    
    var hideMyView : HideMyView?
    
    //MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     
        
//        self.amountClearButton()
//        self.mobileNumber.clearText()
//        self.mobileNumber.resignFirstResponder()
//        self.hidelAllFields()
        
        
        self.commissionTxt.hide()
        self.totalAmountTxt.hide()
        self.cnfMobileNumber.leftView?.isUserInteractionEnabled = false
//
        self.mobileNumber.hideClear?.hideClearButton(hide: true)
//        cnfMobileNumber.hideClear?.hideClearButton(hide: true)
        if let mobileRightView = self.mobileNumber.rightView as? ContactRightView {
            mobileRightView.hideClearButton(hide: true)
        }
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.endEditing(true)
        self.mobileNumber.hideClear?.hideClearButton(hide: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        offerArrModel = [OffersListModel]()
        
        if self.transType.type == .payTo && !isOfferEnabled {
            self.getWalletSize()
        }
        addVehicle.resignTextFieldDelegate = self
        self.cnfMobileNumber.hide()
        self.name.hide()
        self.userType.hide()
        self.addVehicle.hide()
        self.subscriber.hide()
        self.merchant.hide()
        self.merchantLabel.isHidden = true
        self.amount.hide()
        self.commissionTxt.hide()
        self.totalAmountTxt.hide()
        self.burmeseAmount.hide()
        self.remarks.hide()
        self.hideMyNumber.hide()
        
        contactTableView.tableFooterView = UIView()
        cnfRightView = PTClearButton.updateView(strTag: "cleaCNFr", delegate: self)
        
        remarks.delegate = self
        
        self.mobileNumber.subDelegate = self
        self.cnfMobileNumber.subDelegate = self
        self.amount.subDelegate = self
        if self.transType.type == .cashIn || self.transType.type == .cashOut || transType.type == .cashInFree || self.transType.type == .cashOutFree || cashInfree == true || cashOutFree == true {
            self.mobileNumber.updateRightView(self, show: true)
            self.mobileNumber.subScanQRDelegate = self
        } else {
            self.mobileNumber.updateRightView(self)
        }
        
        self.initLeftViews()
        self.hideContactSuggestion()
        objectSound.mappingDelegate(self)
        objectSound.delegate = self
        if transType.type == .cashInFree {
            cashInfree = true
            transType.type = .cashIn
        } else if transType.type == .cashOutFree {
            cashOutFree = true
            transType.type = .cashOut
        }
        if transType.type == .transferTo || transType.type == .cashIn {
            cashInCashOut = "Cash In"
        } else if transType.type == .cashOut {
            cashInCashOut = "Cash Out"
        }
        
        if isOfferEnabled {
            self.mobileNumber.leftView?.isUserInteractionEnabled = true
            self.mobileNumber.hideClear?.hideClearButton(hide: true)

            self.offersPromoCodeLabel.show()
            self.offersTextField.show()
            guard let offer = self.offerModel else { return }
            // payment type property of offer 2 - Direct, 1 indirect
            self.isInDirectOffer = offer.paymentType?.lowercased() == "0".lowercased()
            
            if self.isInDirectOffer {//Direct offer
                self.mobileNumber.hide()
                self.cnfMobileNumber.hide()
                self.userType.hide()
                self.addVehicle.hide()
                self.name.hide()
                self.merchant.hide()
                self.merchantLabel.isHidden = true
                self.subscriber.hide()
                self.burmeseAmount.hide()
                self.remarks.hide()
                self.amount.show()
                self.offersTextField.show()
                self.offersPromoCodeLabel.hide()
                self.hideMyNumber.hide()
                self.walletSizeView.isHidden = true
                self.displayOfferText(offer: offer)
            } else {
                //Show offer text
                self.displayOfferText(offer: offer)
            }
            
            self.offersTextField.text = " " + "Promo Code".localized + " = " + offer.promotionCode.safelyWrappingString()
            self.walletSizeView.isHidden = true

        } else {
            self.offersTextField.hide()
            self.offersPromoCodeLabel.hide()
        }
        self.cnfMobileNumber.leftView?.isUserInteractionEnabled = false
        if let nav = favNav {
            PaytoConstants.global.navigation = nav
        }
        //self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""

        //Reload offers view from Notification
        NotificationCenter.default.addObserver(self, selector: #selector(updateMobileNumberField), name: NSNotification.Name(rawValue: "AddScanButtonInMobile"), object: nil)
        notfDef.addObserver(forName:NSNotification.Name(rawValue: "OffersReloadUI"), object:nil, queue:nil, using:reloadNewView)
        self.amount.selectedLineHeight = 0.5
        self.remarks.selectedLineHeight = 0.5
        //self.mobileNumber.becomeFirstResponder()
    }
    
    //Display offer text
    func displayOfferText(offer: OffersListModel) {
        let minAmtStr = self.getDigitDisplay(String(describing: offer.minimumPayableAmount ?? 0))
        let maxAmtStr = self.getDigitDisplay(String(describing: offer.maximumPayableAmount ?? 0))
        let disctAmtStr = self.getDigitDisplay(String(describing: offer.value ?? 0))
        
        if appDel.currentLanguage == "my" {
            
            if offer.amountType == "0"{ //Fixed Amount Offers Type
                if (offer.isPayableLimit ?? true == false) {
                    let urlStr = String.init(format: OffersConstantBurmish.offer_type_fixed_discount_unlimited_purchase,disctAmtStr)
                    self.offersPromoCodeLabel.text = urlStr
                } else  {
                    if (offer.minimumPayableAmount ?? 0 > 0 && offer.maximumPayableAmount ?? 0 > 0  && offer.minimumPayableAmount ?? 0 != offer.maximumPayableAmount ?? 0) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_type_fixed_discount_min_max_purchase,disctAmtStr,minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.maximumPayableAmount ?? 0 == 0) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_type_fixed_discount_min_purchase,disctAmtStr,minAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.minimumPayableAmount ?? 0 > 0 && offer.maximumPayableAmount ?? 0 > 0 && offer.minimumPayableAmount ?? 0 == offer.maximumPayableAmount ?? 0) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_type_fixed_discount_fixed_purchase,disctAmtStr,minAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                }
            } else if offer.amountType == "1"{ //Percentage Offers Type
                if (offer.isPayableLimit ?? true == false) {
                    let urlStr = String.init(format: OffersConstantBurmish.offer_type_fixed_discount_per_unlimited_purchase,disctAmtStr)
                    self.offersPromoCodeLabel.text = urlStr
                } else  {
                    if (offer.minimumPayableAmount ?? 0 > 0 && offer.maximumPayableAmount ?? 0 > 0 && offer.minimumPayableAmount ?? 0 != offer.maximumPayableAmount ?? 0) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_type_fixed_discount_per_min_max_purchase,disctAmtStr,minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.minimumPayableAmount ?? 0 == 0) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_type_fixed_discount_per_max_purchase,disctAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.maximumPayableAmount ?? 0 == 0) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_type_fixed_discount_per_min_purchase,disctAmtStr,minAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else  if (offer.minimumPayableAmount ?? 0 > 0 && offer.maximumPayableAmount ?? 0 > 0
                        && offer.minimumPayableAmount ?? 0 == offer.maximumPayableAmount ?? 0) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_type_fixed_discount_per_fixed_purchase,disctAmtStr,minAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                }
            }  else if offer.amountType == "2" || offer.amountType == "3" || offer.amountType == "4" || offer.amountType == "5" || offer.amountType == "6" || offer.amountType == "7" { //Free & Buy Offers Type
                //Free & Buy
                let buyingQty = self.getDigitDisplay(String(describing: offer.buyingQty ?? 0))
                let freeQty = self.getDigitDisplay(String(describing: offer.freeQty ?? 0))
                let productValue = self.getDigitDisplay(String(describing: offer.productValue ?? 0))
                let enterValue = self.getDigitDisplay(String(describing: offer.value ?? 0))
                
                if offer.amountType == "2" { //Free Offer Type
                    if (offer.productValidationRuleValue == 0) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_free_min_max_purchase,buyingQty,freeQty,minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 1) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_free_fixed_purchase,buyingQty,freeQty,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 2) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_free_fixed_purchase_accumulated_quantity_amount,buyingQty,freeQty,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                } else if offer.amountType == "3" {//Free with fixed amount
                    if (offer.productValidationRuleValue == 0) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_free_min_max_purchase_discont_amount,buyingQty,freeQty,enterValue,minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 1) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_free_fixed_purchase_discont_amount,buyingQty,freeQty,enterValue,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 2) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_free_fixed_purchase_accumulated_quantity_amount_discont_amount,buyingQty,freeQty,enterValue,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                }  else if offer.amountType == "4" {//Free With Percentage
                    if (offer.productValidationRuleValue == 0) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_free_min_max_purchase_discont_per,buyingQty,freeQty,enterValue,minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 1) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_free_fixed_purchase_discont_per,buyingQty,freeQty,enterValue,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 2) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_free_fixed_purchase_accumulated_quantity_amount_discont_per,buyingQty,freeQty,enterValue,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                } else if offer.amountType == "5" {//Product
                    if (offer.productValidationRuleValue == 0) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_buy_min_max_purchase,buyingQty,minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 1) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_buy_fixed_purchase,buyingQty,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 2) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_buy_fixed_purchase_accumulated_quantity_amount,buyingQty,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                } else if offer.amountType == "6" {//Product with Fixed Amount
                    if (offer.productValidationRuleValue == 0) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_buy_min_max_purchase_discont_amount,buyingQty,enterValue,minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 1) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_buy_fixed_purchase_discont_amount,buyingQty,enterValue,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 2) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_buy_fixed_purchase_accumulated_quantity_amount_discont_amount,buyingQty,enterValue,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                    
                } else if offer.amountType == "7" {//Product with Fixed Percentage
                    if (offer.productValidationRuleValue == 0) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_buy_min_max_purchase_discont_per,buyingQty,enterValue,minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 1) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_buy_fixed_purchase_discont_per,buyingQty,enterValue,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 2) {
                        let urlStr = String.init(format: OffersConstantBurmish.offer_buy_fixed_purchase_accumulated_quantity_amount_discont_per,buyingQty,enterValue,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                }
            }
        }
        else if appDel.currentLanguage == "uni" {
            
            if offer.amountType == "0"{ //Fixed Amount Offers Type
                if (offer.isPayableLimit ?? true == false) {
                    let urlStr = String.init(format: OffersConstantUnicode.offer_type_fixed_discount_unlimited_purchase,disctAmtStr)
                    self.offersPromoCodeLabel.text = urlStr
                } else  {
                    if (offer.minimumPayableAmount ?? 0 > 0 && offer.maximumPayableAmount ?? 0 > 0  && offer.minimumPayableAmount ?? 0 != offer.maximumPayableAmount ?? 0) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_type_fixed_discount_min_max_purchase,disctAmtStr,minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.maximumPayableAmount ?? 0 == 0) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_type_fixed_discount_min_purchase,disctAmtStr,minAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.minimumPayableAmount ?? 0 > 0 && offer.maximumPayableAmount ?? 0 > 0 && offer.minimumPayableAmount ?? 0 == offer.maximumPayableAmount ?? 0) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_type_fixed_discount_fixed_purchase,disctAmtStr,minAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                }
            } else if offer.amountType == "1"{ //Percentage Offers Type
                if (offer.isPayableLimit ?? true == false) {
                    let urlStr = String.init(format: OffersConstantUnicode.offer_type_fixed_discount_per_unlimited_purchase,disctAmtStr)
                    self.offersPromoCodeLabel.text = urlStr
                } else  {
                    if (offer.minimumPayableAmount ?? 0 > 0 && offer.maximumPayableAmount ?? 0 > 0 && offer.minimumPayableAmount ?? 0 != offer.maximumPayableAmount ?? 0) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_type_fixed_discount_per_min_max_purchase,disctAmtStr,minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.minimumPayableAmount ?? 0 == 0) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_type_fixed_discount_per_max_purchase,disctAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.maximumPayableAmount ?? 0 == 0) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_type_fixed_discount_per_min_purchase,disctAmtStr,minAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else  if (offer.minimumPayableAmount ?? 0 > 0 && offer.maximumPayableAmount ?? 0 > 0
                        && offer.minimumPayableAmount ?? 0 == offer.maximumPayableAmount ?? 0) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_type_fixed_discount_per_fixed_purchase,disctAmtStr,minAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                }
            }  else if offer.amountType == "2" || offer.amountType == "3" || offer.amountType == "4" || offer.amountType == "5" || offer.amountType == "6" || offer.amountType == "7" { //Free & Buy Offers Type
                //Free & Buy
                let buyingQty = self.getDigitDisplay(String(describing: offer.buyingQty ?? 0))
                let freeQty = self.getDigitDisplay(String(describing: offer.freeQty ?? 0))
                let productValue = self.getDigitDisplay(String(describing: offer.productValue ?? 0))
                let enterValue = self.getDigitDisplay(String(describing: offer.value ?? 0))
                
                if offer.amountType == "2" { //Free Offer Type
                    if (offer.productValidationRuleValue == 0) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_free_min_max_purchase,buyingQty,freeQty,minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 1) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_free_fixed_purchase,buyingQty,freeQty,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 2) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_free_fixed_purchase_accumulated_quantity_amount,buyingQty,freeQty,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                } else if offer.amountType == "3" {//Free with fixed amount
                    if (offer.productValidationRuleValue == 0) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_free_min_max_purchase_discont_amount,buyingQty,freeQty,enterValue,minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 1) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_free_fixed_purchase_discont_amount,buyingQty,freeQty,enterValue,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 2) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_free_fixed_purchase_accumulated_quantity_amount_discont_amount,buyingQty,freeQty,enterValue,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                }  else if offer.amountType == "4" {//Free With Percentage
                    if (offer.productValidationRuleValue == 0) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_free_min_max_purchase_discont_per,buyingQty,freeQty,enterValue,minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 1) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_free_fixed_purchase_discont_per,buyingQty,freeQty,enterValue,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 2) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_free_fixed_purchase_accumulated_quantity_amount_discont_per,buyingQty,freeQty,enterValue,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                } else if offer.amountType == "5" {//Product
                    if (offer.productValidationRuleValue == 0) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_buy_min_max_purchase,buyingQty,minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 1) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_buy_fixed_purchase,buyingQty,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 2) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_buy_fixed_purchase_accumulated_quantity_amount,buyingQty,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                } else if offer.amountType == "6" {//Product with Fixed Amount
                    if (offer.productValidationRuleValue == 0) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_buy_min_max_purchase_discont_amount,buyingQty,enterValue,minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 1) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_buy_fixed_purchase_discont_amount,buyingQty,enterValue,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 2) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_buy_fixed_purchase_accumulated_quantity_amount_discont_amount,buyingQty,enterValue,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                    
                } else if offer.amountType == "7" {//Product with Fixed Percentage
                    if (offer.productValidationRuleValue == 0) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_buy_min_max_purchase_discont_per,buyingQty,enterValue,minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 1) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_buy_fixed_purchase_discont_per,buyingQty,enterValue,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 2) {
                        let urlStr = String.init(format: OffersConstantUnicode.offer_buy_fixed_purchase_accumulated_quantity_amount_discont_per,buyingQty,enterValue,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                }
            }
        }
        else {//English text display
            
            if offer.amountType == "0"{ //Fixed Amount Offers Type
                if (offer.isPayableLimit ?? true == false) {
                    let urlStr = String.init(format: OffersConstant.offer_type_fixed_discount_unlimited_purchase,disctAmtStr)
                    self.offersPromoCodeLabel.text = urlStr
                } else  {
                    if (offer.minimumPayableAmount ?? 0 > 0 && offer.maximumPayableAmount ?? 0 > 0 && offer.minimumPayableAmount ?? 0 != offer.maximumPayableAmount ?? 0) {
                        let urlStr = String.init(format: OffersConstant.offer_type_fixed_discount_min_max_purchase,disctAmtStr,minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.maximumPayableAmount ?? 0 == 0) {
                        let urlStr = String.init(format: OffersConstant.offer_type_fixed_discount_min_purchase,disctAmtStr,minAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.minimumPayableAmount ?? 0 > 0 && offer.maximumPayableAmount ?? 0 > 0 && offer.minimumPayableAmount ?? 0 == offer.maximumPayableAmount ?? 0) {
                        let urlStr = String.init(format: OffersConstant.offer_type_fixed_discount_fixed_purchase,disctAmtStr,minAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                }
            } else if offer.amountType == "1"{ //Percentage Offers Type
                if (offer.isPayableLimit ?? true == false) {
                    let urlStr = String.init(format: OffersConstant.offer_type_fixed_discount_per_unlimited_purchase,disctAmtStr,"%")
                    self.offersPromoCodeLabel.text = urlStr
                } else  {
                    if (offer.minimumPayableAmount ?? 0 > 0 && offer.maximumPayableAmount ?? 0 > 0 && offer.minimumPayableAmount ?? 0 != offer.maximumPayableAmount ?? 0) {
                        let urlStr = String.init(format: OffersConstant.offer_type_fixed_discount_per_min_max_purchase,disctAmtStr,"%",minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    }else if (offer.minimumPayableAmount ?? 0 == 0) {
                        let urlStr = String.init(format: OffersConstant.offer_type_fixed_discount_per_max_purchase,disctAmtStr,"%",maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    }else if (offer.maximumPayableAmount ?? 0 == 0) {
                        let urlStr = String.init(format: OffersConstant.offer_type_fixed_discount_per_min_purchase,disctAmtStr,"%",minAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    }else if (offer.minimumPayableAmount ?? 0 > 0 && offer.maximumPayableAmount ?? 0 > 0
                        && offer.minimumPayableAmount ?? 0 == offer.maximumPayableAmount ?? 0) {
                        let urlStr = String.init(format: OffersConstant.offer_type_fixed_discount_per_fixed_purchase,disctAmtStr,"%",minAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                }
            }  else if offer.amountType == "2" || offer.amountType == "3" || offer.amountType == "4" || offer.amountType == "5" || offer.amountType == "6" || offer.amountType == "7" { //Free & Buy Offers Type
                //Free & Buy
                let buyingQty = self.getDigitDisplay(String(describing: offer.buyingQty ?? 0))
                let freeQty = self.getDigitDisplay(String(describing: offer.freeQty ?? 0))
                let productValue = self.getDigitDisplay(String(describing: offer.productValue ?? 0))
                let enterValue = self.getDigitDisplay(String(describing: offer.value ?? 0))
                
                if offer.amountType == "2" { //Free Offer Type
                    if (offer.productValidationRuleValue == 0) {
                        let urlStr = String.init(format: OffersConstant.offer_free_min_max_purchase,buyingQty,freeQty,minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 1) {
                        let urlStr = String.init(format: OffersConstant.offer_free_fixed_purchase,buyingQty,freeQty,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 2) {
                        let urlStr = String.init(format: OffersConstant.offer_free_fixed_purchase_accumulated_quantity_amount,buyingQty,freeQty,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                } else if offer.amountType == "3" {//Free with fixed amount
                    if (offer.productValidationRuleValue == 0) {
                        let urlStr = String.init(format: OffersConstant.offer_free_min_max_purchase_discont_amount,buyingQty,freeQty,enterValue,minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 1) {
                        let urlStr = String.init(format: OffersConstant.offer_free_fixed_purchase_discont_amount,buyingQty,freeQty,enterValue,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 2) {
                        let urlStr = String.init(format: OffersConstant.offer_free_fixed_purchase_accumulated_quantity_amount_discont_amount,buyingQty,freeQty,enterValue,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                }  else if offer.amountType == "4" {//Free With Percentage
                    if (offer.productValidationRuleValue == 0) {
                        let urlStr = String.init(format: OffersConstant.offer_free_min_max_purchase_discont_per,buyingQty,freeQty,enterValue,"%",minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 1) {
                        let urlStr = String.init(format: OffersConstant.offer_free_fixed_purchase_discont_per,buyingQty,freeQty,enterValue,"%",productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 2) {
                        let urlStr = String.init(format: OffersConstant.offer_free_fixed_purchase_accumulated_quantity_amount_discont_per,buyingQty,freeQty,enterValue,"%",productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                } else if offer.amountType == "5" {//Product
                    if (offer.productValidationRuleValue == 0) {
                        let urlStr = String.init(format: OffersConstant.offer_buy_min_max_purchase,buyingQty,minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 1) {
                        let urlStr = String.init(format: OffersConstant.offer_buy_fixed_purchase,buyingQty,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 2) {
                        let urlStr = String.init(format: OffersConstant.offer_buy_fixed_purchase_accumulated_quantity_amount,buyingQty,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                } else if offer.amountType == "6" {//Product with Fixed Amount
                    if (offer.productValidationRuleValue == 0) {
                        if (offer.minimumPayableAmount ?? 0 == offer.maximumPayableAmount ?? 0) {
                            let urlStr = String.init(format: OffersConstant.offer_buy_fixed_purchase_discont_amount,buyingQty,enterValue,productValue)
                            self.offersPromoCodeLabel.text = urlStr
                        } else {
                        let urlStr = String.init(format: OffersConstant.offer_buy_min_max_purchase_discont_amount,buyingQty,enterValue,minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                        }
                    } else if (offer.productValidationRuleValue == 1) {
                        let urlStr = String.init(format: OffersConstant.offer_buy_fixed_purchase_discont_amount,buyingQty,enterValue,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 2) {
                        let urlStr = String.init(format: OffersConstant.offer_buy_fixed_purchase_accumulated_quantity_amount_discont_amount,buyingQty,enterValue,productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                    
                } else if offer.amountType == "7" {//Product with Fixed Percentage
                    if (offer.productValidationRuleValue == 0) {
                        let urlStr = String.init(format: OffersConstant.offer_buy_min_max_purchase_discont_per,buyingQty,enterValue,"%",minAmtStr,maxAmtStr)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 1) {
                        let urlStr = String.init(format: OffersConstant.offer_buy_fixed_purchase_discont_per,buyingQty,enterValue,"%",productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    } else if (offer.productValidationRuleValue == 2) {
                        let urlStr = String.init(format: OffersConstant.offer_buy_fixed_purchase_accumulated_quantity_amount_discont_per,buyingQty,enterValue,"%",productValue)
                        self.offersPromoCodeLabel.text = urlStr
                    }
                }
            }
        }
        
        //Display the promo code label
        self.offersPromoCodeLabel.show()
    }
    
    @objc func updateMobileNumberField() {
        DispatchQueue.main.async {
            self.mobileNumber.hideClear?.hideClearButton(hide: true)
            self.mobileNumber.updateRightView(self, show: true)
            self.mobileNumber.subScanQRDelegate = self
            if self.transType.type == .cashInFree {
                self.cashInfree = true
                self.transType.type = .cashIn
            } else if self.transType.type == .cashOutFree {
                self.cashOutFree = true
                self.transType.type = .cashOut
            }
            if self.transType.type == .transferTo || self.transType.type == .cashIn {
                self.cashInCashOut = "Cash In"
            } else if self.transType.type == .cashOut {
                self.cashInCashOut = "Cash Out"
            }
        }
    }
    
    fileprivate func updateAllLabels() {
        DispatchQueue.main.async {
            self.maxWalletSizeLbl.text = "Max Wallet Size".localized +  "\((self.balanceDict["Maximumwalletsize"] as? String ?? "").replacingOccurrences(of: ".00", with: "").alignWithCurrencyFormat()) \("MMK".localized)"
            self.perDayTxnLbl.text = "Per day limit".localized + "\((self.balanceDict["Perdaylimitamount"] as? String ?? "").replacingOccurrences(of: ".00", with: "").alignWithCurrencyFormat()) \("MMK".localized)"
            self.monthlyTxtLbl.text = "Monthly limit".localized + "\((self.balanceDict["Monthlimitamount"] as? String ?? "").replacingOccurrences(of: ".00", with: "").alignWithCurrencyFormat()) \("MMK".localized)"
            self.remPerDayTxtLbl.text = "Per day limit remain".localized + "\((self.balanceDict["Perdayremaingamount"] as? String ?? "").replacingOccurrences(of: ".00", with: "").alignWithCurrencyFormat()) \("MMK".localized)"
            self.remMonthlyTxnLbl.text = "Monthly limit remain".localized + "\((self.balanceDict["Permonthremaingamount"] as? String ?? "").replacingOccurrences(of: ".00", with: "").alignWithCurrencyFormat()) \("MMK".localized)"
            if self.isOfferEnabled {
                self.walletSizeView.isHidden = true
            } else if self.mobileNumber.text?.count ?? 0 > 3 || self.transType.type != .payTo {
                self.walletSizeView.isHidden = true
            } else {
                if let nav = PaytoConstants.global.navigation {
                    if nav.viewControllers.contains(where: {$0.isKind(of: MultiPaytoViewController.self)}) {
                        self.walletSizeView.isHidden = true
                    } else {
                        self.walletSizeView.isHidden = true // false
                    }
                } else {
                    self.walletSizeView.isHidden = true // false
                }
            }
            self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
        }
    }
    
    //Kethan - changes
    fileprivate func getTheWalletSizeParams() -> Dictionary<String, Any> {
        var params = Dictionary<String, Any>()
        params["AppId"] = UserModel.shared.appID
        params["Limit"] = 0
        params["MobileNumber"] = UserModel.shared.mobileNo
        params["Msid"] = UserModel.shared.msid
        params["Offset"] = 0
        params["Ostype"] = 1
        params["Otp"] = ""
        params["Simid"] = ""
        return params
    }
    
    private func getWalletSize() {
        if appDelegate.checkNetworkAvail() {
            let web      = PayToWebApi()
            web.delegate = self
            
            let url = getUrl(urlStr: Url.okWalletSizeURL, serverType: .serverApp)
//            guard let url   = URL(string: "http://69.160.4.151:8001/RestService.svc/GetWalletsizeapiByMobilenumber") else { return }
            let pRam = self.getTheWalletSizeParams()
            web.genericClass(url: url, param: pRam as AnyObject, httpMethod: "POST", mScreen: "mWalletSize", loader: false)
        }
    }
    
    //PTWebresponseDelegate
    func webSuccessResult(data: Any, screen: String) {
        PTLoader.shared.hide()
        if screen == "mWalletSize" {
//            if let xmlString = data as? String {
//                let xml = SWXMLHash.parse(xmlString)
//                balanceDict.removeAll()
//                self.enumerateBalance(indexer: xml)
//                self.updateAllLabels()
//            }
            
            if let dataDict = data as? Dictionary<String, Any> {
                println_debug(dataDict)
                if let dataStr = dataDict["Data"] as? String {
                    if let dataDict = dataStr.parseJSONString as? Dictionary<String, Any> {
                        balanceDict["Monthlimitamount"] = dataDict["Monthlimitamount"] as? String ?? ""
                        balanceDict["Permonthremaingamount"] = dataDict["Permonthremaingamount"] as? String ?? ""
                        balanceDict["Permonthusedamount"] = dataDict["Permonthusedamount"] as? String ?? ""
                        balanceDict["Perdaylimitamount"] = dataDict["Perdaylimitamount"] as? String ?? ""
                        balanceDict["Perdayremaingamount"] = dataDict["Perdayremaingamount"] as? String ?? ""
                        balanceDict["Perdayusedamount"] = dataDict["Perdayusedamount"] as? String ?? ""
                        balanceDict["Maximumwalletsize"] = dataDict["Maximumwalletsize"] as? String ?? ""
                        self.updateAllLabels()
                    }
                }
            }
        }
    }
    
    func webFailureResult(screen: String) {
        println_debug("Failure")
    }
    
    //MARK:- Post Notification Offers
    func reloadNewView (notification:Notification) -> Void {
        self.delegate?.didShowAdvertisement(show: false)

        let mobileNo = notification.userInfo?["text"] as? String ?? ""

        self.name.text = "Unknown"
        self.cnfMobileNumber.hide()
        self.name.show()
        self.userType.show()
        self.addVehicle.hide()
        self.subscriber.hide()
        self.merchant.show()
       self.merchantLabel.isHidden = false
        self.commissionTxt.hide()
        self.totalAmountTxt.hide()
        self.burmeseAmount.hide()
        self.remarks.hide()
        self.hideMyNumber.hide()
        self.offersPromoCodeLabel.show()
        self.offersTextField.show()
        
        guard let offer = self.offerModel else { return }
        
        self.displayOfferText(offer: offer)
        
        self.offersTextField.text = " " + "Promo Code".localized + " = " + offer.promotionCode.safelyWrappingString()
        
        self.checkAgentCode(with: mobileNo)
    }
    func reloadNewView () {
        self.delegate?.didShowAdvertisement(show: false)
         self.mobileNumber.leftView?.isUserInteractionEnabled = false
        let mobileNo = self.mobileNumber.text ?? ""
        
        let s = mobileNo
        
        let s2 = s.replacingCharacters(in: ...s.startIndex, with: "0095")
        
        self.name.text = "Unknown"
        self.cnfMobileNumber.hide()
        self.name.show()
        self.userType.show()
        self.addVehicle.hide()
        self.subscriber.hide()
        self.merchant.show()
        self.merchantLabel.isHidden = false
        self.commissionTxt.hide()
        self.totalAmountTxt.hide()
        self.burmeseAmount.hide()
        self.remarks.hide()
        self.hideMyNumber.hide()
        self.offersPromoCodeLabel.show()
        self.offersTextField.show()
       
        guard let offer = self.offerModel else { return }
        
        self.displayOfferText(offer: offer)
        
        self.offersTextField.text = " " + "Promo Code".localized + " = " + offer.promotionCode.safelyWrappingString()
        
        self.checkAgentCode(with: s2)
    }
    
    
    func initLeftViews() {
        let nameLeftView        = DefaultIconView.updateView(icon: "name_bill")
        let categoryLeftView    = DefaultIconView.updateView(icon: "categories")
        let merchantLeftView    = DefaultIconView.updateView(icon: "r_business_name")
        let businessLeftView    = DefaultIconView.updateView(icon: "r_user")
        let amountLeftView      = DefaultIconView.updateView(icon: "amount")
        let burAmountLeftView   = DefaultIconView.updateView(icon: "amount")
        let commissionLeftView  = DefaultIconView.updateView(icon: "amount")
        let totAmountLeftView   = DefaultIconView.updateView(icon: "amount")
        let remarkLeftView      = DefaultIconView.updateView(icon: "remark")
        let addVehicleView      = DefaultIconView.updateView(icon: "add_car")
        
        self.name.leftView      = nameLeftView
        self.name.leftViewMode  = UITextField.ViewMode.always

        self.userType.leftView      = categoryLeftView
        self.userType.leftViewMode  = UITextField.ViewMode.always
        
        self.merchant.leftView      = merchantLeftView
        self.merchant.leftViewMode  = UITextField.ViewMode.always
        
        self.burmeseAmount.leftView      = burAmountLeftView
        self.burmeseAmount.leftViewMode  = UITextField.ViewMode.always
        
        self.amount.leftView      = amountLeftView
        self.amount.leftViewMode  = UITextField.ViewMode.always
        
        self.commissionTxt.leftView      = commissionLeftView
        self.commissionTxt.leftViewMode  = UITextField.ViewMode.always
        
        self.totalAmountTxt.leftView      = totAmountLeftView
        self.totalAmountTxt.leftViewMode  = UITextField.ViewMode.always
        
        let amountRightView       = AmountRightView.updateView("act_speaker")
        amountRightView.delegate  = self
        self.amount.rightView     = amountRightView
        self.amount.rightViewMode = .always
        
        if let text = self.amount.text, text.count == 0 {
            amountRightView.hideClearButton(hide: true)
        }
       
        self.remarks.leftView      = remarkLeftView
        self.remarks.leftViewMode  = UITextField.ViewMode.always
        
        self.addVehicle.leftViewMode = .always
        self.addVehicle.leftView = addVehicleView
        
        self.subscriber.leftViewMode = .always
        self.subscriber.leftView = businessLeftView
        
        //cnfRightView?.buttonClear.isHidden = true
        self.cnfMobileNumber.rightViewMode = .whileEditing
        self.cnfMobileNumber.rightView     = cnfRightView
        cnfRightView?.hideClearButton(hide: true)
        self.updateHideMyNumberView()

    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.walletSizeView.isHidden = true
        self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
        //self.walletSizeViewHeight.constant = 0
        if textField == remarks {
            if textField.text!.count > 0 {
                textField.placeholder = "Remarks".localized
            } else {
                textField.placeholder = "Enter Remarks".localized
            }
        } else if textField == amount {
            self.updateTextColor()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
     
        if textField == remarks {
            guard let tfText   = textField.text else { return false }
            let text     = (tfText as NSString)
            let txt      = text.replacingCharacters(in: range, with: string)
            if txt.count > 0 {
                textField.placeholder = "Remarks".localized
            } else {
                textField.placeholder = "Enter Remarks".localized
            }
            let containsEmoji: Bool = txt.containsEmoji
            if (containsEmoji){
                return false
            }
            if txt.count > 80 {
                return false
            }
        }
        return true
    }
    
    // Amount Clear Field Delegate
    func amountClearButton() {
        self.amount.text = ""
        self.remarks.text = ""
        self.remarks.placeholder = "Enter Remarks".localized
        if let rightView = self.amount.rightView as? AmountRightView {
            rightView.hideClearButton(hide: true)
        }
        self.amount.textColor = .black
        self.remarks.hide(); self.amount.placeholder = "Enter Amount".localized;
        
        
        
        self.amount.becomeFirstResponder()
        self.commissionTxt.text = ""
        self.totalAmountTxt.text = ""
        self.commissionTxt.hide(); self.totalAmountTxt.hide()
        self.burmeseAmount.hide()
        self.hideMyNumber.hide()
        self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
        self.delegate?.didShowSubmitButton(show: false, path: self.indexPath)
    }
    
    func clearField(strTag: String) {
        if strTag.lowercased() == "cleaCNFr".lowercased() {
            self.cnfMobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
            self.hidelAllFields(withConfirmationField: false)
            self.amount.text = ""
            self.remarks.text = ""
            self.remarks.placeholder = "Enter Remarks".localized
            self.cnfRightView?.hideClearButton(hide: true)
        }
    }
    
    func mobClearfield() {
        self.mitQRCode = false
        if self.transType.type == .payTo && !isOfferEnabled {
            if let nav = PaytoConstants.global.navigation {
                if nav.viewControllers.contains(where: {$0.isKind(of: MultiPaytoViewController.self)}) {
                    self.walletSizeView.isHidden = true
                } else {
                    self.walletSizeView.isHidden = true // false
                }
            }
            self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
        }
        self.delegate?.didShowAgentButton(hide: false)
        self.commissionTxt.text = ""
        self.totalAmountTxt.text = ""
        self.commissionTxt.hide(); self.totalAmountTxt.hide()
        self.remarks.text = ""
        self.remarks.placeholder = "Enter Remarks".localized
    }
    
    func hideContactSuggestionWhenEndEditing() {
        self.hideContactSuggestion()
        self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
    }
    
    func soundOffOn(sender: UIButton) {
        self.deviceSound?.didChangeVoice()
    }
    
    func soundOnOff(status: Bool) {
        self.deviceSound?.didChangeVoice(status: status)
    }
    
    func updateHideMyNumberView() {
        let framHideMyView = CGRect.init(x: 0, y: 0, width: screenWidth, height: 50.00)
        hideMyView = HideMyView.updateView(framHideMyView, delegate: self)
        self.hideMyNumber.leftViewMode = .always
        self.hideMyNumber.leftView = nil
        self.hideMyNumber.leftView = hideMyView
        
        //self.hideMyNumber.addSubview(hideMyView!)

    }
    
    func hideMyNumberDelegate(hide: Bool) {
//        if self.transType != .cashInPayto || self.transType == .cashOutPayto {
//            //self.transType = hide ? .paytoWithoutID : .paytoWithID
//        }
        self.transType.transType = hide ? .payToID : .payTo
    }
    
    func doChangeVoiceImage(on: Bool) {
       // self.amount.rightView     = nil

        if on {
//            if let sound = self.deviceSound {
//                sound.didChangeVoice(status: true)
//            }
            let amountRightView       = AmountRightView.updateView("act_speaker")
            amountRightView.delegate  = self
            self.amount.rightView     = amountRightView
            self.amount.rightViewMode = .always
            if let text = self.amount.text, text.count == 0 {
                amountRightView.hideClearButton(hide: true)
            }
        } else {
//            if let sound = self.deviceSound {
//                sound.didChangeVoice(status: false)
//            }
            let amountRightView       = AmountRightView.updateView("speaker")
            amountRightView.delegate  = self
            self.amount.rightView     = amountRightView
            self.amount.rightViewMode = .always
            if let text = self.amount.text, text.count == 0 {
                amountRightView.hideClearButton(hide: true)
            }
        }
       
    }
    
    func hidelAllFields(withConfirmationField flag: Bool = true, textField: UITextField? = nil) {
        self.amount.textColor = .black
        transferToWithCommission = false
        cashInFreeWithCommission = false
        if !isOfferEnabled {
            offersTextField.hide()
            offersPromoCodeLabel.hide()
        }
        
        if flag {
            cnfMobileNumber.hide()
            self.cnfMobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""

        } else {
//            let confirmMobileNumber = String(self.mobileNumber.text?.dropLast() ?? "")
//            self.cnfMobileNumber.text = confirmMobileNumber
            if let txtCnf = textField, txtCnf != self.cnfMobileNumber {
                self.cnfMobileNumber.show()
                self.cnfMobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
            }
        }
        
        name.hide()
        name.text = ""
        userType.hide()
        userType.text = ""
        addVehicle.hide()
        addVehicle.text = ""
        subscriber.hide()
        subscriber.text = ""
        merchant.hide()
        self.merchantLabel.isHidden = true
        merchant.text = ""
        self.merchantLabel.text = ""
        amount.hide()
        amount.text = ""
        if let rightView = self.amount.rightView as? AmountRightView {
            rightView.hideClearButton(hide: true)
        }
        commissionTxt.text = ""
        totalAmountTxt.text = ""
        commissionTxt.hide()
        totalAmountTxt.hide()
        burmeseAmount.hide()
        burmeseAmount.text = ""
        remarks.hide()
        remarks.text = ""
        hideMyNumber.hide()
        
        self.updateHideMyNumberView()
        
        self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
        self.delegate?.didShowSubmitButton(show: false, path: self.indexPath)
    }
    
    func hideCnfMobileClear(view: UITextField) {
        if view == self.cnfMobileNumber {
            if self.mobileNumber.country.dialCode == "+95" {
                if view.text == "09" {
                    self.cnfRightView?.hideClearButton(hide: true)
                } else {
                    self.cnfRightView?.hideClearButton(hide: false)
                }
            } else {
                if view.text == "" {
                    self.cnfRightView?.hideClearButton(hide: true)
                } else {
                    self.cnfRightView?.hideClearButton(hide: false)
                }
            }
        }
    }
    
    func response(text: String, view: UITextField) -> Bool {
        
        let validations = PayToValidations().getNumberRangeValidation(text)
        print("validations---------\(validations)")
        self.walletSizeView.isHidden = true
        self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
        if view == self.mobileNumber {
            self.mitQRCode = false
            if validations.isRejected {
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                self.mobileNumber.hideClear?.hideClearButton(hide: true)
                if self.transType.type == .payTo && !isOfferEnabled {
                    if let nav = PaytoConstants.global.navigation {
                        if nav.viewControllers.contains(where: {$0.isKind(of: MultiPaytoViewController.self)}) {
                            self.walletSizeView.isHidden = true
                        } else {
                            self.walletSizeView.isHidden = true // false
                        }
                    }
                    self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
                }
                return false
            }
            
            if isOfferEnabled {
                //print("validations.min---------\(validations.min)")

                if text.count >= validations.min {
                    self.delegate?.didShowAdvertisement(show: false)
                   // self.cnfMobileNumber.show()
                    
                    self.cnfMobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                    if self.mobileNumber.country.dialCode.hasPrefix("+95") {
                        if validations.max == text.count {
                            //print("validations equal max---------\(validations.max)")

                            self.mobileNumber.text = text
                            let flagOffer = self.mobileNumber.offerModelParsing(self.offerModel, text: text)
                            
                            if !flagOffer {
                                self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.offerInvalidNumber.localized)
                                self.mobileNumber.hideClear?.hideClearButton(hide: true)
                                return false
                            }
                            self.cnfMobileNumber.show()
                            self.cnfMobileNumber.becomeFirstResponder()
                            self.hideContactSuggestion()
                            self.delegate?.didChangeHeight(height: heightUpdates, withIndex: self.indexPath)
                            return false
                        }
                        if text.count > validations.max {
                            //print("validations greater max---------\(validations.max)")

                            self.delegate?.didShowAdvertisement(show: false)
                            let flagOffer = self.mobileNumber.offerModelParsing(self.offerModel, text: text)
                            
                            if !flagOffer {
                                self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.offerInvalidNumber.localized)
                                self.mobileNumber.hideClear?.hideClearButton(hide: true)
                                return false
                            }
                            self.cnfMobileNumber.show()
                            self.cnfMobileNumber.becomeFirstResponder()
                            self.hideContactSuggestion()
                            self.delegate?.didChangeHeight(height: heightUpdates, withIndex: self.indexPath)
                            return false
                        }
                        if (text.count > validations.min && text.count < validations.max){
                           // print("validations equal and less max---------\(validations.max)")
                            
                            self.delegate?.didShowAdvertisement(show: false)
                            let flagOffer = self.mobileNumber.offerModelParsing(self.offerModel, text: text)
                            
                            if !flagOffer {
                                self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.offerInvalidNumber.localized)
                                self.mobileNumber.hideClear?.hideClearButton(hide: true)
                                return false
                            }
                            self.cnfMobileNumber.show()
                            self.cnfMobileNumber.becomeFirstResponder()
                            self.hideContactSuggestion()
                            self.delegate?.didChangeHeight(height: heightUpdates, withIndex: self.indexPath)
                            return false
                        }
                    }
                    
                    self.delegate?.didChangeHeight(height: heightUpdates, withIndex: self.indexPath)
                    return true
                } else {
                    self.hideContactSuggestion()
                    self.cnfMobileNumber.hide()
                    self.delegate?.didChangeHeight(height: heightUpdates, withIndex: self.indexPath)
                }
            }
            
            if text.count > 3 {
                if validations.max == validations.min {
                    self.loadContacts(txt: text, maxReached: (text.count >= validations.max))
                } else {
                    if text.count == validations.min {
                        self.loadContacts(txt: text, maxReached: true)
                    } else {
                        self.loadContacts(txt: text, maxReached: (text.count >= validations.max))
                    }
                }
            } else {
                let char = text.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) && (text.count == 0) {
                    mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                    self.hideContactSuggestion()
                    return false
                }
                self.hideContactSuggestion()
            }

            
            if self.mobileNumber.country.dialCode.hasPrefix("+95") {
                
                if text.count >= validations.min {
                    self.delegate?.didShowAgentButton(hide: true)
                    if self.mobileNumber.country.dialCode.hasPrefix("+95") {
                        
                        if validations.max == text.count {
                            self.cnfMobileNumber.show()
                            self.cnfRightView?.hideClearButton(hide: true)
                            self.hidelAllFields(withConfirmationField: false)
                            self.cnfMobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                            self.mobileNumber.text = text
                            self.delegate?.didShowAdvertisement(show: false)
                            self.hideContactSuggestion()
                            self.delegate?.didChangeHeight(height: heightUpdates, withIndex: self.indexPath)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { // Change `2.0` to the desired number of seconds.
                               // Code you want to be delayed
                                self.cnfMobileNumber.becomeFirstResponder()
                            }
                            return false
                        }
                        if text.count > validations.max {
                            self.delegate?.didShowAdvertisement(show: false)
                            //self.cnfMobileNumber.becomeFirstResponder()
                            self.hideContactSuggestion()
                            self.delegate?.didChangeHeight(height: heightUpdates, withIndex: self.indexPath)
                            return false
                        } else {
                            self.cnfMobileNumber.show()
                            self.cnfRightView?.hideClearButton(hide: true)
                            self.hidelAllFields(withConfirmationField: false)
                            self.cnfMobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                        }
                    }
                    self.hideContactSuggestion()
                    self.delegate?.didChangeHeight(height: heightUpdates, withIndex: self.indexPath)
                    return true
                } else {
                    self.delegate?.didShowAgentButton(hide: false)
                    self.cnfMobileNumber.hide()
                    self.delegate?.didChangeHeight(height: heightUpdates, withIndex: self.indexPath)
                }
                
                if text.count > validations.max {
                    return false
                }

            } else {
                if text.count > 3 {
                    self.delegate?.didShowAgentButton(hide: true)
                    if text.count >= 13 {
                        self.hideContactSuggestion()
                        if text.count <= 13 {
                            self.mobileNumber.text = text
                        }
                        self.cnfMobileNumber.becomeFirstResponder()
                        self.cnfMobileNumber.show()
                        self.cnfRightView?.hideClearButton(hide: true)
                        self.cnfMobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                        self.delegate?.didChangeHeight(height: heightUpdates, withIndex: self.indexPath)
                        return false
                    }
                    if text.count > 8 {
                        self.loadContacts(txt: text, maxReached: true, otherCountry: true)
                    } else {
                        self.loadContacts(txt: text, otherCountry: true)
                    }
                    self.hidelAllFields(withConfirmationField: false)
                    self.cnfMobileNumber.show()
                    self.cnfRightView?.hideClearButton(hide: true)
                    self.cnfMobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                    
                    self.delegate?.didChangeHeight(height: heightUpdates, withIndex: self.indexPath)
                    return true
                } else if text.count == 13 {
                    self.hideContactSuggestion()
                    self.mobileNumber.text = text
                    self.cnfMobileNumber.becomeFirstResponder()
                    self.delegate?.didChangeHeight(height: heightUpdates, withIndex: self.indexPath)
                    return false
                } else if text.count > 13 {
                    self.hideContactSuggestion()
                    self.cnfMobileNumber.becomeFirstResponder()
                    self.delegate?.didChangeHeight(height: heightUpdates, withIndex: self.indexPath)
                    return false
                } else if text.count < 4 {
                    self.delegate?.didShowAgentButton(hide: false)
                }
            }

        } else if view == self.cnfMobileNumber {
            if (self.mobileNumber.country.dialCode == "+95") {
                if text != "09" {
                    self.cnfRightView?.hideClearButton(hide: false)
                } else {
                    self.cnfRightView?.hideClearButton(hide: true)
                }
            } else {
                if text.count > 0 {
                    self.cnfRightView?.hideClearButton(hide: false)
                } else {
                    self.cnfRightView?.hideClearButton(hide: true)
                }
            }
            if !self.mobileNumber.text!.hasPrefix(text) {
                if (self.mobileNumber.country.dialCode == "+95"), self.cnfMobileNumber.text == "09" {
                    self.cnfRightView?.hideClearButton(hide: true)
                }
                if text.count == 1 {
                   self.cnfRightView?.hideClearButton(hide: true)
                }
                return false
            }
            
            if self.mobileNumber.text?.count == text.count, String(describing: self.mobileNumber.text!.last!) == String(describing:text.last!) {
                self.cnfMobileNumber.text = self.mobileNumber.text
                let agentCode = self.getAgentCode(numberWithPrefix: self.mobileNumber.text!, havingCountryPrefix: self.mobileNumber.country.dialCode)
                //check agent this will called when mobile number and confirm number will match
                self.checkAgentCode(with: agentCode)
                if isOfferEnabled {
                    if offerModel?.amountType == "0" || offerModel?.amountType == "1" {
                        if (offerModel?.minimumPayableAmount ?? 0 == offerModel?.maximumPayableAmount ?? 0) {
                            let minAmtStr = self.getDigitDisplay(String(describing: offerModel?.minimumPayableAmount ?? 0))

                            self.amount.text =  "\(minAmtStr)"//self.getNumberFormat("\(offerModel?.productValue ?? 0)")
                            let amountRightView       = AmountRightView.updateView("act_speaker")
                            amountRightView.delegate  = self
                            self.amount.rightView     = amountRightView
                            self.amount.rightViewMode = .always
                            amountRightView.hideClearButton(hide: true)
                            self.amount.isUserInteractionEnabled = false
                            
                            if var burmese = self.amount.text {
                                burmese = burmese.replacingOccurrences(of: ",", with: "")
                                if let ch = burmese.last, ch == "." {
                                    
                                } else {
                                    let text = objectSound.getBurmeseSoundText(text: burmese)
                                    self.burmeseAmount.text = text
                                }
                                
                                self.burmeseAmount.placeholder = ""
                                self.delegate?.didShowSubmitButton(show: true, path: self.indexPath)
                            }
                            
                        }

                    } else {
                    if offerModel?.productValidationRuleValue == 1 {
                        //self.amount.text =  "\(offerModel?.productValue ?? 0)"
                        self.amount.text =  self.getNumberFormat("\(offerModel?.productValue ?? 0)")
                        let amountRightView       = AmountRightView.updateView("act_speaker")
                        amountRightView.delegate  = self
                        self.amount.rightView     = amountRightView
                        self.amount.rightViewMode = .always
                        amountRightView.hideClearButton(hide: true)
                        self.amount.isUserInteractionEnabled = false
                        
                        if var burmese = self.amount.text {
                            burmese = burmese.replacingOccurrences(of: ",", with: "")
                            if let ch = burmese.last, ch == "." {
                                
                            } else {
                                let text = objectSound.getBurmeseSoundText(text: burmese)
                                self.burmeseAmount.text = text
                            }
                            
                            self.burmeseAmount.placeholder = ""
                            self.delegate?.didShowSubmitButton(show: true, path: self.indexPath)
                        }
                        
                    }
                    }
                }
                let char = text.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) && (text.count == 0) {
                    self.cnfMobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                    return false
                }
            }
        }
        
        return true
    }
    
    func hideHideMySuggestionContacts() {
//        self.hideHideMySuggestionContacts()
    }
    
    @objc func checkCashOutAmount() {
        if ((amount.text ?? "").replacingOccurrences(of: ",", with: "") as NSString).floatValue < ("1000" as NSString).floatValue {
            PaytoConstants.alert.showToast(msg: "Minimum transfer amount should be 1,000 MMK".localized)
        }
    }
    
    @objc func checkCashInAmount() {
        let amt = ((amount.text ?? "").replacingOccurrences(of: ",", with: "") as NSString).floatValue
        if amt < ("500" as NSString).floatValue || (self.cashInLimit != 0 && amt >= Float(self.cashInLimit)) {
            PaytoConstants.alert.showToast(msg: "Minimum transfer amount should be 500 MMK".localized)
        }
    }
    
    private func hideTextFields() {
        self.commissionTxt.text = ""
        self.totalAmountTxt.text = ""
        self.burmeseAmount.text = ""
        self.burmeseAmount.hide()
        self.remarks.hide()
        self.commissionTxt.hide()
        self.totalAmountTxt.hide()
        self.hideMyNumber.hide()
        self.delegate?.didShowSubmitButton(show: false, path: self.indexPath)
    }
    
    func backspaceHandler(string: String, view: UITextField) {
        self.amount.isUserInteractionEnabled = true
        if view == self.mobileNumber {
            self.mitQRCode = false
            self.cnfMobileNumber.text = ""
            var otherCountry = false
            if self.mobileNumber.country.dialCode != "+95" {
                if string.count == 0 {
                    if let nav = PaytoConstants.global.navigation {
                        if nav.viewControllers.contains(where: {$0.isKind(of: MultiPaytoViewController.self)}) {
                            self.walletSizeView.isHidden = true
                        } else {
                            if !isOfferEnabled {
                            self.walletSizeView.isHidden = true // false
                            }
                        }
                    }
                }
                otherCountry = true
                if view.text!.count < 5 {
                    self.delegate?.didShowAgentButton(hide: false)
                    self.hidelAllFields()
                    mobileNumber.hideClear?.hideClearButton(hide: true)
                } else {
                    self.hidelAllFields(withConfirmationField: false)
                }
            } else {
                if string == "09" {
                    if let nav = PaytoConstants.global.navigation {
                        if nav.viewControllers.contains(where: {$0.isKind(of: MultiPaytoViewController.self)}) {
                            self.walletSizeView.isHidden = true
                        } else {
                            if !isOfferEnabled {
                            self.walletSizeView.isHidden = true // false
                            }
                        }
                    }
                }
                let validations = PayToValidations().getNumberRangeValidation(string)
                if view.text!.count <= validations.min {
                    self.delegate?.didShowAgentButton(hide: false)
                    self.hidelAllFields()
                } else {
                    if view.text!.count >= validations.min {
                        self.hidelAllFields(withConfirmationField: false, textField: view)
                    } else {
                        self.hidelAllFields(withConfirmationField: false)
                    }
                }
            }
            if view.text!.count <= 5 {
                self.hideContactSuggestion()
            } else {
                let validations = PayToValidations().getNumberRangeValidation(string)
                self.loadContacts(txt: string, maxReached: (string.count >= validations.max), otherCountry: otherCountry)
            }
            self.commissionTxt.hide()
            self.totalAmountTxt.hide()
            
        } else if view == self.cnfMobileNumber {
            if (self.mobileNumber.country.dialCode == "+95") {
                if string == "09" || string == "0" {
                    self.cnfRightView?.hideClearButton(hide: true)
                } else {
                    self.cnfRightView?.hideClearButton(hide: false)
                }
            } else {
                if string.count > 0 {
                    self.cnfRightView?.hideClearButton(hide: false)
                } else {
                    self.cnfRightView?.hideClearButton(hide: true)
                }
            }
            self.hidelAllFields(withConfirmationField: false,  textField: view)
            self.commissionTxt.hide()
            self.totalAmountTxt.hide()
        }
        
        self.amount.text = ""
        self.remarks.text = ""
        self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
    }
    
    func amountResponse(text: String, view: UITextField) -> Bool {
            self.delegate?.didShowAdvertisement(show: false)
                if let rightView = view.rightView as? AmountRightView {
                    if text.count > 0 {
                        rightView.hideClearButton(hide: false)
                    } else {
                        rightView.hideClearButton(hide: true)
                    }
                }
            
            // if a timer is already active, prevent it from firing
            if amountCheckingTimer != nil {
                amountCheckingTimer?.invalidate()
                amountCheckingTimer = nil
            }
            
            if text.hasPrefix("0"), text.count == 1 {
                return false
            }

            
            // reschedule the search: in 1.0 second, call the searchForKeyword method on the new textfield content
            if transType.type == .cashOut || transType.type == .cashIn || transType.type == .transferTo {
                
            } else {
                self.amountCheckingTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(searchForKeyword(_:)), userInfo: view.text ?? "", repeats: false)
            }

            
            if transType.type == .cashIn  || transType.type == .transferTo {
                if (text.replacingOccurrences(of: ",", with: "") as NSString).floatValue < ("499" as NSString).floatValue {
                    self.hideTextFields()
                    self.amount.inputAccessoryView?.isHidden = false
                    let _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(checkCashInAmount), userInfo: view.text ?? "", repeats: false)
                    self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
                    return true
                } else {
                    self.commissionTxt.show(); self.totalAmountTxt.show()
                    self.amountCheckingTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(getCommissionForCICOFromApi), userInfo: view.text ?? "", repeats: false)            }
            } else if transType.type == .cashOut {
                if (text.replacingOccurrences(of: ",", with: "") as NSString).floatValue < ("1000" as NSString).floatValue {
                    self.hideTextFields()
                    self.amount.inputAccessoryView?.isHidden = false
                    let _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(checkCashOutAmount), userInfo: view.text ?? "", repeats: false)
                    self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
                    return true
                } else {
                    self.commissionTxt.show(); self.totalAmountTxt.show()
                    self.amountCheckingTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(getCommissionForCICOFromApi), userInfo: view.text ?? "", repeats: false)
                }
            }
            
            if text.count > 0 {
                if transType.type != .cashIn {
                    
                    
                    if let hasValue = UserDefaults.standard.value(forKey: "MultiplePayTOAmount") as? [Double]{
                        var amountFloat = 0.0
                        for i in 0..<hasValue.count{
                         amountFloat = amountFloat + hasValue[i]
                        }
                        
                        let floatAmount = Float(amountFloat) + (text.replacingOccurrences(of: ",", with: "") as NSString).floatValue
                        print(floatAmount)
                        
                        
                        if floatAmount > (UserLogin.shared.walletBal as NSString).floatValue {
                            self.amount.text = ""
                            self.remarks.hide()
                            if transType.type == .cashOut  {
                                self.amount.placeholder = "Enter Receive Amount".localized
                            } else if transType.type == .transferTo {
                                self.amount.placeholder = "Enter Receive Digital Amount".localized
                            } else {
                                self.amount.placeholder = "Enter Amount".localized
                            }
                            self.burmeseAmount.hide()
                            self.amountClearButton()
                            
                            PaytoConstants.alert.showToast(msg: PaytoConstants.messages.lessBalance.localized)
                            self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
                            self.delegate?.didShowSubmitButton(show: false, path: self.indexPath)
                            return false
                        } else {
                            if transType.type != .cashOut {
                                if (text.replacingOccurrences(of: ",", with: "") as NSString).floatValue > ("999999999" as NSString).floatValue {
                                    PaytoConstants.alert.showToast(msg: "Maximum Cash Out Amount Reached".localized)
                                    self.amount.placeholder = "Enter Cash-Out Amount".localized
                                    self.amountClearButton()
                                    self.amount.textColor = .black
                                    return false
                                }
                            }
                        }
                        
                    }else{
                        if (text.replacingOccurrences(of: ",", with: "") as NSString).floatValue > (UserLogin.shared.walletBal as NSString).floatValue {
                            self.amount.text = ""
                            self.remarks.hide()
                            if transType.type == .cashOut  {
                                self.amount.placeholder = "Enter Receive Amount".localized
                            } else if transType.type == .transferTo {
                                self.amount.placeholder = "Enter Receive Digital Amount".localized
                            } else {
                                self.amount.placeholder = "Enter Amount".localized
                            }
                            self.burmeseAmount.hide()
                            self.amountClearButton()
                            
                            PaytoConstants.alert.showToast(msg: PaytoConstants.messages.lessBalance.localized)
                            self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
                            self.delegate?.didShowSubmitButton(show: false, path: self.indexPath)
                            return false
                        } else {
                            if transType.type != .cashOut {
                                if (text.replacingOccurrences(of: ",", with: "") as NSString).floatValue > ("999999999" as NSString).floatValue {
                                    PaytoConstants.alert.showToast(msg: "Maximum Cash Out Amount Reached".localized)
                                    self.amount.placeholder = "Enter Cash-Out Amount".localized
                                    self.amountClearButton()
                                    self.amount.textColor = .black
                                    return false
                                }
                            }
                        }
                        
                        
                    }
                    
                    
                    
                    
                    
                    
                } else {
                    if (text.replacingOccurrences(of: ",", with: "") as NSString).floatValue > ("999999999" as NSString).floatValue {
                        PaytoConstants.alert.showToast(msg: "Maximum Cash In Amount Reached".localized)
                        self.amount.placeholder = "Enter Cash-In Amount".localized
                        self.amountClearButton()
                        self.amount.textColor = .black
                        return false
                    }
                }
                
                self.remarks.show()
                self.burmeseAmount.show()
                if !isOfferEnabled {
                    self.hideMyNumber.show()
                }
                if self.transType.type == .cashIn || self.transType.type == .transferTo {
                    self.hideMyNumber.hide()
                }
                if transType.type == .cashOut || transType.type == .cashIn || transType.type == .transferTo {
                    self.amount.inputAccessoryView?.isHidden = false
                } else {
                    if text.hasPrefix("."), text.count < 2 {
                        self.delegate?.didShowSubmitButton(show: false, path: self.indexPath)
                    } else {
                        if text == ".0" || text == ".00" || text == ".000" {
                            self.delegate?.didShowSubmitButton(show: false, path: self.indexPath)
                        } else {
                            self.delegate?.didShowSubmitButton(show: true, path: self.indexPath)
                        }
                    }
                }
                if transType.type == .cashIn || transType.type == .transferTo {
                    self.amount.placeholder = "Cash-In Amount".localized
                } else if transType.type == .cashOut {
                    self.amount.placeholder = "Payable Digital Amount".localized
                } else {
                    self.amount.placeholder = "Amount".localized
                }

            } else {
                if transType.type == .cashIn || transType.type == .transferTo {
                    self.amount.placeholder = "Enter Cash-In Amount".localized
                } else if transType.type == .cashOut {
                    self.amount.placeholder = "Enter Payable Digital Amount".localized
                } else {
                    self.amount.placeholder = "Enter Amount".localized
                }
                self.remarks.hide()
                self.commissionTxt.text = ""
                self.totalAmountTxt.text = ""
                self.commissionTxt.hide(); self.totalAmountTxt.hide()
                self.burmeseAmount.hide()
                self.hideMyNumber.hide()
                self.delegate?.didShowSubmitButton(show: false, path: self.indexPath)
            }
            self.updateTextColor()
            self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
            return true
        }
    
    func updateTextColor() {
        if var text = self.amount.text {
            text = text.replacingOccurrences(of: ",", with: "")
            if (text as NSString).floatValue > 9999999.0 {
                 self.amount.textColor = .red
            } else if (text as NSString).floatValue > 999999.0 && (text as NSString).floatValue <= 9999999.0 {
                 self.amount.textColor = .green
            } else {
                self.amount.textColor = .black
            }
        } else {
            self.amount.textColor = .black
        }
    }

    func openContacts(selection: ButtonType) {
        self.hideContactSuggestion()
        self.view.endEditing(true)
        switch selection {
        case .countryView:
            let nav = self.countryViewController(delegate: self)
            if let _ = PaytoConstants.global.navigation {
                PaytoConstants.global.navigation?.present(nav, animated: true, completion: nil)
            }
            break
        case .contactView:
            if let nav = PaytoConstants.global.navigation {
                if nav.viewControllers.contains(where: {$0.isKind(of: MultiPaytoViewController.self)}) {
                    PTManagerClass.openMultiContactSelection(self, hideScan: false)
                } else {
                    PTManagerClass.openMultiContactSelection(self, hideScan: true)
                }
            } else {
                PTManagerClass.openMultiContactSelection(self, hideScan: true)
            }
            
        }
    }
    
    
    
    func amountForCICO(text: Int?) {
        if  transType.type == .cashIn || transType.type == .transferTo || transType.type == .cashOut {
            if let cnt = text, cnt > 0 {
                self.amount.placeholder = (transType.type == .cashOut) ? "Payable Digital Amount".localized : "Cash-In Amount".localized
                self.commissionTxt.placeholder = "Agent Commission".localized
                if transType.type == .cashIn {
                    self.totalAmountTxt.placeholder = "Net Receive Digital Amount".localized
                } else if transType.type == .cashOut {
                    self.totalAmountTxt.placeholder = "Net Receive Amount".localized
                } else {
                    self.totalAmountTxt.placeholder = "Net Payable Digital Amount".localized
                }
            } else {
                self.amount.placeholder = (transType.type == .cashOut) ? "Enter Payable Digital Amount".localized : "Enter Cash-In Amount".localized
            }
            //self.getCommissionForCICOFromApi()
        } else {
            
            getSameNumberSameAmount()
            
            if let cnt = text, cnt > 0 {
                self.amount.placeholder = "Amount".localized
            } else {
                self.amount.placeholder = "Enter Amount".localized
            }
        }
    }
    
    //MARK:- CashIn and CashOut Commission Api
    @objc func getCommissionForCICOFromApi() {
        if var amt = self.amount.text {
            amt = amt.replacingOccurrences(of: ",", with: "")
            if self.transType.type == .cashIn || self.transType.type == .transferTo, (amt as NSString).floatValue > 499 {
                println_debug("Cash In")
            } else  if self.transType.type == .cashOut, (amt as NSString).floatValue > 999 {
                println_debug("Cash Out")
            } else {
                return
            }
        } else {
            return
        }
        println_debug(self.amount.text)
        guard appDel.checkNetworkAvail() else {
            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.noInternet.localized)
            return
        }
        let url = getUrl(urlStr: "GetCashSettings", serverType: .cashInOutPayto)
        var paramsDict = Dictionary<String, Any>()
        paramsDict["PhoneNumber"] = (self.transType.type == .transferTo) ? formattedNumber : UserModel.shared.mobileNo
        paramsDict["Amount"] = (self.amount.text ?? "0").replacingOccurrences(of: ",", with: "")
        paramsDict["CashIn"] = (transType.type == .cashOut) ? false : true
        paramsDict["ReceiverPhoneNumber"] = (self.transType.type == .transferTo) ? UserModel.shared.mobileNo : formattedNumber
        let params = self.JSONStringFromAnyObject(value: paramsDict as AnyObject)
        println_debug(paramsDict)
        TopupWeb.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", isCicoPayTo: true, handle: { [weak self](resp, success) in
            if success {
                DispatchQueue.main.async {
                    self?.parseCommissonPaymentResult(data: resp)
                }
            }
        })
           
    }
    
    private func parseCommissonPaymentResult(data: Any) {
     
        if let dict = data as? Dictionary<String,Any> {
            cashInFreeWithCommission = false
            transferToWithCommission = false
            guard let success = dict.safeValueForKey("StatusCode") as? Int, success == 200 else {
                self.amountClearButton()
                self.delegate?.didShowAgentButton(hide: false)
                PaytoConstants.alert.showErrorAlert(title: "", body: "Please try with some other amount".localized)
                return }
            if let commissionDict = dict.safeValueForKey("Content") as? Dictionary<String, Any> {
                if let cashInLmt = commissionDict.safeValueForKey("CashInLimit") as? Int, let cashOutLmt = commissionDict.safeValueForKey("CashOutLimit") as? Int {
                    self.cashInLimit = Double(cashInLmt)
                    self.cashOutLimit = Double(cashOutLmt)
                    if cashInfree || self.transType.type == .cashIn, cashInLmt != 0 {
                        if let amt = self.amount.text?.replacingOccurrences(of: ",", with: "") {
                            let doubleVal = (amt as NSString).intValue
                            if doubleVal > cashInLmt {
                                DispatchQueue.main.async {
                                    let amountFormat = wrapAmountWithCommaDecimal(key: "\(cashInLmt)")
                                    alertViewObj.wrapAlert(title: nil, body: "Maximum cash-in amount for this Agent is \(amountFormat) MMK".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                                    alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                                        self.amountClearButton()
                                        self.delegate?.didShowAgentButton(hide: false)
                                    })
                                    alertViewObj.showAlert()
                                }
                                return
                            }
                        }
                    } else if cashOutFree ||  self.transType.type == .cashOut, cashOutLmt != 0 {
                        if let amt = self.amount.text?.replacingOccurrences(of: ",", with: "") {
                            let doubleVal = (amt as NSString).intValue
                            if doubleVal > cashOutLmt {
                                DispatchQueue.main.async {
                                    let amountFormat = wrapAmountWithCommaDecimal(key: "\(cashOutLmt)")
                                    alertViewObj.wrapAlert(title: nil, body: "Maximum cash-out amount for this Agent is \(amountFormat) MMK".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                                    alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                                        self.amountClearButton()
                                        self.delegate?.didShowAgentButton(hide: false)
                                    })
                                    alertViewObj.showAlert()
                                }
                                return
                            }
                        }
                    }
                }
                
                self.commissionTxt.placeholder = "Agent Commission".localized
//                self.totalAmountTxt.placeholder = "Net Receive Digital Amount".localized
                if (cashInfree == true || cashOutFree == true), let commission = commissionDict.safeValueForKey("Commission") as? Double, commission > 0.0 {
                    cashInFreeWithCommission = true
                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: nil, body: "This Agent have Paid Commission.".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                        alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                        })
                        alertViewObj.showAlert()
                    }
                }
                if !cashOutFree, !cashInfree, (self.transType.type == .cashIn || self.transType.type == .cashOut), let commission = commissionDict.safeValueForKey("Commission") as? Double, commission <= 0.0 {
                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: nil, body: "Please enter correct agent Number".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                        alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                            self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                            self.mobileNumber.hideClear?.hideClearButton(hide: true)
                            self.hidelAllFields()
                            self.delegate?.didShowAgentButton(hide: false)
                        })
                        alertViewObj.showAlert()
                    }
                } else {
                    if transType.type == .transferTo, let commission = commissionDict.safeValueForKey("Commission") as? Double, commission > 0.0 {
                        transferToWithCommission = true
                    }
                    self.commissionTxt.text = self.amountFormatter(text: "\(commissionDict.safeValueForKey("Commission") as? Double ?? 0)")
                    //                if transType.type == .cashIn || transType.type == .transferTo {
                    let removeSpace = self.amount.text?.replacingOccurrences(of: ",", with: "")
                    let commisson = Double(removeSpace ?? "0")! - (commissionDict.safeValueForKey("Commission") as? Double ?? 0)
                    self.totalAmountTxt.text = self.amountFormatter(text: "\(commisson)")
                    //                } else {
                    //                    self.totalAmountTxt.text = self.amount.text ?? ""
                    //                }
                    self.delegate?.didShowSubmitButton(show: true, path: self.indexPath)
                    if transType.type == .cashIn || transType.type == .transferTo {
                        _ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(searchForKeyword(_:)), userInfo: self.totalAmountTxt.text ?? "", repeats: false)
                    } else {
                        _ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(searchForKeyword(_:)), userInfo: self.amount.text ?? "", repeats: false)
                    }
                }
            }
        }
    }
    
    private func amountFormatter(text: String) -> String  {
        let mystring = text.replacingOccurrences(of: ",", with: "")
        let number = NSDecimalNumber(string: mystring)
        
        let formatter = NumberFormatter()
        
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        var num = formatter.string(from: number)
        if num == "NaN" {
            num = ""
        }
        return num ?? ""
    }
    //MARK:- Animate END EDITING

    //MARK:- Contact Delegates
    
    //MARK:- Country Delegates
    func countryViewController(_ list: CountryViewController, country: Country) {
      
        self.mobileNumber.updateLeftView(country: country)
        self.cnfMobileNumber.updateLeftView(country: country)
        self.hidelAllFields()
        self.mobileNumber.text = ""
        self.amount.text = ""
        self.delegate?.didShowSubmitButton(show: false, path: self.indexPath)
        list.dismiss(animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if self.transType.type == .cashIn || self.transType.type == .cashOut || self.cashInfree || self.cashOutFree {
                if (self.mobileNumber.country.dialCode != "+95") {
                    self.mobileNumber.placeholder = "Enter Mobile No".localized
                } else {
                    self.mobileNumber.placeholder = "Enter Mobile Number".localized
                }
            }
//            else if self.isOfferEnabled
//            {
//               if (self.mobileNumber.country.dialCode != "+95") {
//
//                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
//                }
//            }
            
           self.mobileNumber.becomeFirstResponder()
        }
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Contact Updation
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        self.amount.text = ""
        guard let numbers = contact.phoneNumbers.first, numbers.phoneNumber.count > 0 else {
            self.hidelAllFields()
            self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
            self.mobileNumber.hideClear?.hideClearButton(hide: true)
            self.amount.text = ""
            self.mobileNumber.leftView?.isUserInteractionEnabled = true
            PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
            self.mobileNumber.becomeFirstResponder()
            return
        }
        if isOfferEnabled {
            // self.mobileNumber.text = PTManagerClass.decodeMobileNumber(phoneNumber: numbers.phoneNumber)
            
            let destinationNum = numbers.phoneNumber
            var remainNum = ""
            
            if destinationNum.hasPrefix("0"){
                //destinationNum =  String(destinationNum.dropFirst())
                remainNum = destinationNum
            }
            if destinationNum.hasPrefix("+95")
            {
                remainNum  = destinationNum.replacingOccurrences(of: "+95", with: "0")
                
            }
            
            
            let flagOffer = self.mobileNumber.offerModelParsing(self.offerModel, text: remainNum)
            if !flagOffer {
                DispatchQueue.main.async {
                self.hidelAllFields()
                }
                self.mobileNumber.leftView?.isUserInteractionEnabled = true
                self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
        
                return
            }
            else
            {
                if offerModel?.productValidationRuleValue == 1 {
                    self.contactUpgrade(number: numbers.phoneNumber.validateContact(), name: contact.displayName())

                    //self.amount.text =  "\(offerModel?.productValue ?? 0)"
                    self.amount.text =  self.getNumberFormat("\(offerModel?.productValue ?? 0)")
                    let amountRightView       = AmountRightView.updateView("act_speaker")
                    amountRightView.delegate  = self
                    self.amount.rightView     = amountRightView
                    self.amount.rightViewMode = .always
                    amountRightView.hideClearButton(hide: true)
                    self.amount.isUserInteractionEnabled = false
                    
                    if var burmese = self.amount.text {
                        burmese = burmese.replacingOccurrences(of: ",", with: "")
                        if let ch = burmese.last, ch == "." {
                            
                        } else {
                            let text = objectSound.getBurmeseSoundText(text: burmese)
                            self.burmeseAmount.text = text
                        }
                        
                        self.burmeseAmount.placeholder = ""
                        self.delegate?.didShowSubmitButton(show: true, path: self.indexPath)
                    }
                    
                } else {
                    self.contactUpgrade(number: numbers.phoneNumber.validateContact(), name: contact.displayName())
                }
            }
        } else {
            self.contactUpgrade(number: numbers.phoneNumber.validateContact(), name: contact.displayName())
        }
        
        self.remarks.text = ""
       
    }
    
    func contactUpgrade(number: String, name: String = "Unknown", checkApi: Bool = true, amount: String? = nil, remark: String? = "", cashInQR: Bool? = false, repeatPayTo: Bool? = false, cashinLimit: Double? = 0, cashOutLimit: Double? = 0, referenceNumber: String = "") {
        self.walletSizeView.isHidden = true
        if remark ?? "" == "V5POS" {
            self.mitQRCode = true
            self.referenceNumberMIT = referenceNumber
        } else {
            self.mitQRCode = false
        }
        self.cashInLimit = cashinLimit ?? 0
        self.cashOutLimit = cashOutLimit ?? 0
        self.hideContactSuggestion()
        self.delegate?.didShowAdvertisement(show: false)
        self.delegate?.didShowAgentButton(hide: true)
        if let nav = favNav {
            PaytoConstants.global.navigation = nav
        }
        self.walletSizeView.isHidden = true
        var num = number
        
        if num == "" && repeatPayTo == true {
            num = userDef.value(forKey: "RepeatPayTo") as? String ?? ""
        }
        
      //  print(isComingFromMultipayTo)
       // if isComingFromMultipayTo{
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
               // self.amount.backgroundColor = .red
               // print("********************amount tag value \(self.amount.tag)")
                self.amount.becomeFirstResponder()
            }
      // }
        
        
        if num == "" && repeatPayTo == true {
            self.hidelAllFields()
            self.amount.text = ""
            self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "+95" : ""
            self.mobileNumber.hideClear?.hideClearButton(hide: true)
            PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
            return
        }
        var mobileObject = PTManagerClass.decodeMobileNumber(phoneNumber: num)
        
        if mobileObject.country.dialCode == "+95" {
            
            let numberValid = PayToValidations().getNumberRangeValidation(mobileObject.number)
            
            if numberValid.isRejected {
                if let nav = PaytoConstants.global.navigation {
                    if nav.viewControllers.contains(where: {$0.isKind(of: MultiPaytoViewController.self)}) {
                        self.walletSizeView.isHidden = true
                    } else {
                        self.walletSizeView.isHidden = true // false
                    }
                }
                self.hidelAllFields()
                self.mobileNumber.hideClear?.hideClearButton(hide: true)
                self.amount.text = ""
                self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                return
            }
            
            if mobileObject.number.count < numberValid.min && mobileObject.number != "" {
                if let nav = PaytoConstants.global.navigation {
                    if nav.viewControllers.contains(where: {$0.isKind(of: MultiPaytoViewController.self)}) {
                        self.walletSizeView.isHidden = true
                    } else {
                        self.walletSizeView.isHidden = true // false
                    }
                }
                self.hidelAllFields()
                self.amount.text = ""
                self.mobileNumber.hideClear?.hideClearButton(hide: true)
                self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                return
            }
            
            if mobileObject.number.count > numberValid.max {
                self.hidelAllFields()
                self.amount.text = ""

                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                return
            }

        } else {
            if  mobileObject.number.count > 13 {
                self.hidelAllFields()
                self.amount.text = ""
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                return
            }
        }
        if mobileObject.number == "" && repeatPayTo == true {
            num = userDef.value(forKey: "RepeatPayTo") as? String ?? ""
            mobileObject = PTManagerClass.decodeMobileNumber(phoneNumber: num)
            self.mobileNumber.text = mobileObject.number
        }
        userDef.removeObject(forKey: "RepeatPayTo")
        
        let number = self.getAgentCode(numberWithPrefix: mobileObject.number, havingCountryPrefix: mobileObject.country.dialCode)
        if number == UserModel.shared.mobileNo {
            if let nav = PaytoConstants.global.navigation {
                if nav.viewControllers.contains(where: {$0.isKind(of: MultiPaytoViewController.self)}) {
                    self.walletSizeView.isHidden = true
                } else {
                    self.walletSizeView.isHidden = true // false
                }
            }
            self.mobileNumber.clearText()
//            self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
            self.hidelAllFields()
            self.amount.text = ""
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                self.mobileNumber.becomeFirstResponder()
            })
            PaytoConstants.alert.showToast(msg: PaytoConstants.messages.ownMobileNumber.localized)
            return
        }
        if checkApi {
            self.checkAgentCode(with: number, repeatPay: repeatPayTo ?? false, mobileObjNum: mobileObject.number)
            self.mobileNumber.text = mobileObject.number
            self.mobileNumber.updateLeftView(country: mobileObject.country)
            self.cnfMobileNumber.updateLeftView(country: mobileObject.country)
            self.mobileNumber.leftView?.isUserInteractionEnabled = true
            mobileNumber.hideClear?.hideClearButton(hide: false)
            self.hidelAllFields()
            self.name.show()
            if let amt = amount, amt.count > 0 {
                if let rightView = self.amount.rightView as? AmountRightView {
                    rightView.hideClearButton(hide: false)
                }
                let amountShow = amount?.replacingOccurrences(of: ",", with: "")
                self.amount.text = self.amountFormatter(text: amountShow ?? "")
                if self.transType.type == .cashIn || transType.type == .cashOut || transType.type == .transferTo {
                    
                } else {
                    self.searchForKeyword(Timer())
                    self.burmeseAmount.show()
                }
            }
            self.name.text = (name != "") ? name : "Unknown"
            DispatchQueue.global(qos: .background).async {
                self.getNameFromContact(number: number) { (nameStr) in //need to check for more than 1000 contacts
                    DispatchQueue.main.async {
                        if nameStr != "" {
                            self.name.text = nameStr
                        } else {
                            self.name.text = (name != "") ? name : "Unknown"
                        }
                    }
                }
            }
            self.remarks.text = (remark != "V5POS") ? remark : ""
            if let status = cashInQR, status == true {
                self.transType = PayToType(type: .transferTo)
                self.amount.show()
//                self.mobileNumber.isUserInteractionEnabled = false
//                self.cnfMobileNumber.isUserInteractionEnabled = false
                self.amount.placeholder = "Cash-In Amount".localized
                self.commissionTxt.placeholder = "Agent Commission".localized
                if transType.type == .cashOut {
                    self.amount.placeholder = "Payable Digital Amount".localized
                    self.totalAmountTxt.placeholder = "Net Receive Amount".localized
                } else if transType.type == .cashIn {
                    self.totalAmountTxt.placeholder = "Net Receive Digital Amount".localized
                } else {
                    self.totalAmountTxt.placeholder = "Net Payable Digital Amount".localized
                }
                self.commissionTxt.show()
                self.totalAmountTxt.show()
//                self.amount.isUserInteractionEnabled = false
               // self.getCommissionForCICOFromApi()
            }
        } else {
            if transType.type == .cashOut {
                self.amount.placeholder = "Enter Payable Digital Amount".localized
                self.totalAmountTxt.placeholder = "Net Receive Amount".localized
            } else if transType.type == .cashIn {
                self.amount.placeholder = "Enter Cash-In Amount".localized
                self.totalAmountTxt.placeholder = "Net Receive Digital Amount".localized
            } else {
                
            }
            self.hidelAllFields()
        }
        
        if let _ = amount {
            if let status = cashInQR, status == true {
                self.titleDelegate?.changeSubmitBtnTitle()
            }
            self.delegate?.didShowSubmitButton(show: true, path: self.indexPath)
        } else {
            self.delegate?.didShowSubmitButton(show: false, path: self.indexPath)
        }
        self.walletSizeView.isHidden = true
        self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
    }
    
    func openAction(type: ButtonType) {
        self.hideContactSuggestion()
        self.view.endEditing(true)
        switch type {
        case .countryView:
            let nav = self.countryViewController(delegate: self)
            if let _ = PaytoConstants.global.navigation {
                PaytoConstants.global.navigation?.present(nav, animated: true, completion: nil)
            }
            break
        case .contactView:
            if let nav = PaytoConstants.global.navigation {
                if nav.viewControllers.contains(where: {$0.isKind(of: MultiPaytoViewController.self)}) {
                    PTManagerClass.openMultiContactSelection(self, hideScan: false)
                } else {
                    PTManagerClass.openMultiContactSelection(self, hideScan: true)

                }
            } else {
                PTManagerClass.openMultiContactSelection(self, hideScan: true)
            }

//            if favoriteManager.count(.payto) > 0 {
//            } else {
//                let nav = UitilityClass.openContact(multiSelection: false, self)
//                PaytoConstants.global.navigation?.present(nav, animated: true, completion: nil)
//            }
        }
    }
    
    func clearActionType() {
        self.amount.text = ""
        self.hidelAllFields()
        self.mobileNumber.clearText()
        self.amount.isUserInteractionEnabled = true
        self.mobileNumber.becomeFirstResponder()
        self.amount.text = ""
        self.contactSuggestion.isHidden = true
        self.heightContactSuggestion.constant = 0.0
        self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
        self.delegate?.didShowSubmitButton(show: false, path: self.indexPath)
        if transType.type == .cashIn || transType.type == .cashOut {
            
        } else {
            self.delegate?.didShowAdvertisement(show: true)
        }
    }


    //MARK:- Favorite Delegate
    func selectedFavoriteObject(obj: FavModel) {
        self.amount.text = ""
        self.remarks.text = ""
        if isOfferEnabled {
            
            let destinationNum = obj.phone
            var remainNum = ""
            
            if destinationNum.hasPrefix("0"){
                //destinationNum =  String(destinationNum.dropFirst())
                remainNum  = destinationNum.replacingOccurrences(of: "0095", with: "0")

                //remainNum = destinationNum
            }
            if destinationNum.hasPrefix("+95")
            {
                remainNum  = destinationNum.replacingOccurrences(of: "+95", with: "0")
                
            }
            
          

            
            let flagOffer = self.mobileNumber.offerModelParsing(self.offerModel, text: remainNum)
            if !flagOffer {
                DispatchQueue.main.async {
                    self.hidelAllFields()
                }
                self.mobileNumber.leftView?.isUserInteractionEnabled = true
                self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.offerInvalidNumber.localized)
                return
            }
            else
            {
                if offerModel?.productValidationRuleValue == 1 {
                    //self.amount.text =  "\(offerModel?.productValue ?? 0)"
                    
                      self.contactUpgrade(number: obj.phone, name: obj.name)
                    self.amount.text =  self.getNumberFormat("\(offerModel?.productValue ?? 0)")
                    let amountRightView       = AmountRightView.updateView("act_speaker")
                    amountRightView.delegate  = self
                    self.amount.rightView     = amountRightView
                    self.amount.rightViewMode = .always
                    amountRightView.hideClearButton(hide: true)
                    self.amount.isUserInteractionEnabled = false
                    
                    if var burmese = self.amount.text {
                        burmese = burmese.replacingOccurrences(of: ",", with: "")
                        if let ch = burmese.last, ch == "." {
                            
                        } else {
                            let text = objectSound.getBurmeseSoundText(text: burmese)
                            self.burmeseAmount.text = text
                        }
                        
                        self.burmeseAmount.placeholder = ""
                        self.delegate?.didShowSubmitButton(show: true, path: self.indexPath)
                    }
                    
                }
                else {
                     self.contactUpgrade(number: obj.phone, name: obj.name)
                }
            }
        }
        else
        {
              self.contactUpgrade(number: obj.phone, name: obj.name)
        }
    }
    
    //MARK:- Scan QR Code
    
    func offerParsing(number: String) {
      
        self.mobileNumber.text = PTManagerClass.decodeMobileNumber(phoneNumber: number).number
        let flagOffer = self.mobileNumber.offerModelParsing(self.offerModel, text: self.mobileNumber.text ?? "")
         self.mobileNumber.leftView?.isUserInteractionEnabled = false
        if !flagOffer {
         //   self.cnfMobileNumber.isHidden = true
            self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
            PaytoConstants.alert.showToast(msg: PaytoConstants.messages.offerInvalidNumber.localized)
            return
        }
        else
        {
        //    self.cnfMobileNumber.isHidden = false
            self.cnfMobileNumber.becomeFirstResponder()
           self.hideContactSuggestion()
           self.reloadNewView()
            self.delegate?.didChangeHeight(height: heightUpdates, withIndex: self.indexPath)
        }

    }
    
    func changeScrollPage(point: Int, withScanObject object: PTScanObject) {
     
        
        DispatchQueue.main.async {
            self.delegate?.didShowAdvertisement(show: false)
            if self.isOfferEnabled {
                
            } else {
                if (object.amount as NSString).floatValue > 0.00 {
                    if (object.amount.replacingOccurrences(of: ",", with: "") as NSString).floatValue > (UserLogin.shared.walletBal as NSString).floatValue {
                        if self.transType.type == .cashIn {
                            self.remarks.show()
                            self.burmeseAmount.show()
                            self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
                            self.delegate?.didShowSubmitButton(show: true, path: self.indexPath)
                            self.amount.text = object.amount
                            if object.remarks != "" {
                                self.remarks.placeholder = "Remarks".localized
                            }
                            self.remarks.text = ""
                            self.remarks.text = object.remarks
                            //self.searchForKeyword(Timer())
                            self.contactUpgrade(number: object.agentNo, name: object.name, amount: self.amount.text, remark: object.remarks, cashInQR: object.cashInQR, cashinLimit: object.cashInLimit, cashOutLimit: object.cashoutLimit, referenceNumber: object.referenceNumberMIT)
                        } else {
                            self.amount.text = ""
                            PaytoConstants.alert.showToast(msg: PaytoConstants.messages.lessBalance.localized)
                            self.remarks.hide()
                            self.amount.placeholder = "Enter Amount".localized
                            self.burmeseAmount.hide()
                            self.hideMyNumber.hide()
                            self.remarks.text = ""
                            self.remarks.text = object.remarks
                            self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
                            self.delegate?.didShowSubmitButton(show: false, path: self.indexPath)
                            self.contactUpgrade(number: object.agentNo, name: object.name, cashinLimit: object.cashInLimit, cashOutLimit: object.cashoutLimit, referenceNumber: object.referenceNumberMIT)
                        }
                    } else {
                        if self.transType.type == .transferTo || self.transType.type == .cashIn {
                            self.amount.placeholder = "Cash-In Amount".localized
                        } else if self.transType.type == .cashOut {
                            self.amount.placeholder = "Payable Digital Amount".localized
                        } else {
                            self.amount.placeholder = "Amount".localized
                        }
                        self.remarks.show()
                        self.burmeseAmount.show()
                        if !self.isOfferEnabled {
                            self.hideMyNumber.show()
                        }
                        if self.transType.type == .cashIn {
                            self.hideMyNumber.hide()
                        }
                        self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
                        self.delegate?.didShowSubmitButton(show: true, path: self.indexPath)
                        self.amount.text = object.amount
                        if object.remarks != "" {
                            self.remarks.placeholder = "Remarks".localized
                        }
                        self.remarks.text = ""
                        self.remarks.text = object.remarks
                        //self.searchForKeyword(Timer())
                        self.contactUpgrade(number: object.agentNo, name: object.name, amount: self.amount.text, remark: object.remarks, cashInQR: object.cashInQR, cashinLimit: object.cashInLimit, cashOutLimit: object.cashoutLimit, referenceNumber: object.referenceNumberMIT)
                    }
                } else {
                    self.amount.text = ""
                    self.remarks.text = ""
                    self.contactUpgrade(number: object.agentNo, name: object.name, remark: object.remarks, referenceNumber: object.referenceNumberMIT)
                    self.delegate?.didShowSubmitButton(show: false, path: self.indexPath)
                }
            }

        }
        
    }
    
    func scanningFailed() {
        println_debug(scanningFailed)
    }
    private func getSameNumberSameAmount(){
        
        let amout = self.amount.text?.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: ".", with: "")
        let tempAmnt = Int(amout ?? "") ?? 0
        if tempAmnt >= 1{
            var showSubmitButton = true
            if let twoMinutesRef = (transType.type == .transferTo || transType.type == .cashIn) ? self.totalAmountTxt.text : amout {
                
                if var uiNumber = self.mobileNumber.text {
                    if uiNumber.hasPrefix("0") {
                        uiNumber = uiNumber.deletingPrefix("0")
                    }
                    
                    uiNumber = getAgentCode(numberWithPrefix: self.mobileNumber.text ?? "", havingCountryPrefix: self.mobileNumber.country.dialCode)
                    if transType.type != .cashIn {
                        if !PaymentVerificationManager.isValidPaymentTransactions(uiNumber, twoMinutesRef) {
                            showSubmitButton = false
                            
                            if !showSubmitButton{
                                self.delegate?.checkForSameNumberSameAmount()
                            }

                            DispatchQueue.main.async {
                                alertViewObj.wrapAlert(title: nil, body: "Same Number with Same Amount can not be paid before 2 minutes".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                                alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                                    self.amount.isUserInteractionEnabled = true
                                    self.amountClearButton()
                                })
                                alertViewObj.showAlert()
                                return
                            }
                            
                            
                        }
                        
                    }
                }
            }
            
        }
    }
    
    
    
    private func playburmeseSound() {
        
        if var burmese = (transType.type == .transferTo || transType.type == .cashIn) ? totalAmountTxt.text : amount.text {
            burmese = burmese.replacingOccurrences(of: ",", with: "")
            if let ch = burmese.last, ch == "." {
                
            } else {
                let text = objectSound.getBurmeseSoundText(text: burmese)
                self.burmeseAmount.text = text
                //self.objectSound.stopPlay()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.objectSound.playSound(fileName: "commissionAmount")
                    if var burmeseCommission = self.commissionTxt.text {
                        burmeseCommission = burmeseCommission.replacingOccurrences(of: ",", with: "")
                        if let ch = burmeseCommission.last, ch == "." {
                            
                        } else {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                let _ = self.objectSound.getBurmeseSoundText(text: burmeseCommission)
                            }
                        }
                    }
                }
            }
            
            self.burmeseAmount.placeholder = ""
                      
        }
    }

    // amount update delegate fire for pay send view controller
    @objc func searchForKeyword(_ timer: Timer) {
      //  getSameNumberSameAmount()
        if (transType.type == .transferTo && transferToWithCommission) {
            self.playburmeseSound()
        } else if (cashInfree && cashInFreeWithCommission) || (cashOutFree && cashInFreeWithCommission) {
            self.playburmeseSound()
        } else if transType.type == .cashOut || (transType.type == .cashIn && !cashInfree) {
            self.playburmeseSound()
        } else {
            
//            var showSubmitButton = true
//             let amout = self.amount.text?.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: ".", with: "")
//            let tempAmnt = Int(amout ?? "") ?? 0
//            if tempAmnt >= 1{
//                if let twoMinutesRef = amout {
//
//                    if var uiNumber = self.mobileNumber.text {
//                        if uiNumber.hasPrefix("0") {
//                            uiNumber = uiNumber.deletingPrefix("0")
//                        }
//
//                        uiNumber = getAgentCode(numberWithPrefix: self.mobileNumber.text ?? "", havingCountryPrefix: self.mobileNumber.country.dialCode)
//                        if !PaymentVerificationManager.isValidPaymentTransactions(uiNumber, twoMinutesRef) {
//
//                            showSubmitButton = false
//
//                            if !showSubmitButton{
//                                self.delegate?.checkForSameNumberSameAmount()
//                            }
//
//                          DispatchQueue.main.async {
//                                alertViewObj.wrapAlert(title: nil, body: "Same Number with Same Amount can not be paid before 2 minutes".localized, img: #imageLiteral(resourceName: "enter_mobile"))
//                                alertViewObj.addAction(title: "OK".localized, style: .target, action: {
//                                    self.amountClearButton()
//                                })
//                                alertViewObj.showAlert()
//                                return
//                            }
//
//                       }
//                        //else{
////
////                            let totaldigit = amountCheck  + text
////
////                            if (text.replacingOccurrences(of: ",", with: "") as NSString).floatValue > (UserLogin.shared.walletBal as NSString).floatValue {
////                                self.amount.text = ""
////                                self.remarks.hide()
////                                if transType.type == .cashOut  {
////                                    self.amount.placeholder = "Enter Receive Amount".localized
////                                } else if transType.type == .transferTo {
////                                    self.amount.placeholder = "Enter Receive Digital Amount".localized
////                                } else {
////                                    self.amount.placeholder = "Enter Amount".localized
////                                }
////                                self.burmeseAmount.hide()
////                                self.amountClearButton()
////
////                                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.lessBalance.localized)
////                                self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
////                                self.delegate?.didShowSubmitButton(show: false, path: self.indexPath)
////                                return false
////                            }
////
////
////                        }
//
//                    }
//                }
//
//            }
           
            if var burmese = self.amount.text {
                burmese = burmese.replacingOccurrences(of: ",", with: "")
                if let ch = burmese.last, ch == "." {
                    
                } else {
                    let text = objectSound.getBurmeseSoundText(text: burmese)
                    self.burmeseAmount.text = text
                }
                
                self.burmeseAmount.placeholder = ""
                
                
            }
        }
    }

    //MARK:- PTMultiSelectionDelegate
    func didSelectOption(option: PTMultiSelectionOption) {
    
        
        switch option {
        case .favorite:
            var selectionType = false
            var multiPay = false
            var initialCount = 0
            
            if self.transType.type == .transferTo || self.transType.type == .cashIn || self.transType.type == .cashOut {
                selectionType = true
            }
            if let nav = PaytoConstants.global.navigation {
                if nav.viewControllers.contains(where: {$0.isKind(of: MultiPaytoViewController.self)}) {
                    multiPay = true
                    if let multiPayVC = nav.viewControllers.last as? MultiPaytoViewController {
                        initialCount = multiPayVC.multipayModel?.getObjects().count ?? 1
                    }
                }
            }
            let nav = openFavoriteFromNavigation(self, withFavorite: .payto, selectionType: selectionType, multiPay: multiPay, payToCount: initialCount - 1)
            if let _ = PaytoConstants.global.navigation {
                PaytoConstants.global.navigation?.present(nav, animated: true, completion: nil)
            }
            break
        case .contact:
            let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: true, isComingFromOtherNumberTopUp: false)
            if let _ = PaytoConstants.global.navigation {
                PaytoConstants.global.navigation?.present(nav, animated: true, completion: nil)
            }
            break
        case .scanQR:
            guard let scanController = self.storyboard?.instantiateViewController(withIdentifier: "secondVC") as? ScanToPayViewController else { return }
            scanController.delegate = self
            if let nav = PaytoConstants.global.navigation {
                if nav.viewControllers.contains(where: {$0.isKind(of: MultiPaytoViewController.self)}) {
                    scanController.screen = .multi
                }
            }
            if let _ = PaytoConstants.global.navigation {
                PaytoConstants.global.navigation?.pushViewController(scanController, animated: true)
            }
            break
        }
    }
    
    deinit {
        println_debug(PayToUIViewController.self)
    }

}

extension PayToUIViewController: CICOScanQROpenDelegate {
    func openQRToScanToPay() {
        guard let scanController = self.storyboard?.instantiateViewController(withIdentifier: "secondVC") as? ScanToPayViewController else { return }
        scanController.delegate = self
        scanController.transType = self.transType
        scanController.cicoQRScreen = true
        if let _ = PaytoConstants.global.navigation {
            PaytoConstants.global.navigation?.pushViewController(scanController, animated: true)
        }
    }
}


extension PayToUIViewController : ResignAddVehicleTFDelegate {
    func resignTextField() {
        self.view.endEditing(true)
    }
}
