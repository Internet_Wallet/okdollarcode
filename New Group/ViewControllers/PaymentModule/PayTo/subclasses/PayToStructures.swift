//
//  PayToStructures.swift
//  OK
//
//  Created by Ashish on 7/31/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct PaytoConstants {
    
    struct url {
        static let checkAgentCode : String? = "RestService.svc/GetCategoryAndMercNameByMobileNumber?"
        static let multiCashback : String? = "RestService.svc/MultiCashBack"

    }
    
    struct alert {
        static func showErrorAlert(title: String? = nil, body: String = "") {
            alertViewObj.wrapAlert(title: title, body: body, img: #imageLiteral(resourceName: "dashboard_pay_send"))
            alertViewObj.addAction(title: "OK".localized, style: .target) {}
            alertViewObj.showAlert()
        }
        
        static func showToast(msg: String) {
            guard let keyWinndow = UIApplication.shared.keyWindow else { return }
            keyWinndow.makeToast(msg.localized, point: keyWinndow.center, title: nil, image: nil, completion: nil)
        }
    }
    
    struct messages {
        static let ownMobileNumber : String = "You can't make payment to own Mobile Number"
        static let noInternet      : String = "No Internet Connection"
        static let pleaseTryAgain  : String = "Please Try Again"
        static let lessBalance     : String = "Insufficient Balance"
        static let addVehicle      : String = "Please select vehicle"
        static let scantoPay       : String = "Scan To Pay"
        static let payFromContact      : String = "Add Pay From Contact"
        static let payFromFav          : String = "Add Pay From Favorite"
        static let addMobNumberToPay   : String = "Add Mobile Number to Pay"
        static let selectMobileNo       : String = "Please select Mobile Number"
        static let selectAmount          : String = "Please enter Amount"
        static let payButton            : String = "Pay"
        static let limitReached         : String = "Maximum 5 transactions are allowed"
        static let invalidNumber         : String = "Invalid Mobile Number"
        static let noSubscriber            : String = "Subscriber not allowed"
        static let offerInvalidNumber       : String = "Number not correct"
        static let unregisterUser           : String = "Are you sure want to payto Unregistered number?"
    }
    
    struct paytoSetup {
        static var isFromOffer = false
    }
    
    struct global {
       static func storyboard() -> UIStoryboard {
            return UIStoryboard.init(name: "PayTo", bundle: nil)
        }
        
        static func attributesForTitle() -> [NSAttributedString.Key : Any] {
          return [NSAttributedString.Key.font: global.zawgyiFont, NSAttributedString.Key.foregroundColor: UIColor.white]
        }
        
        static var navigation : UINavigationController?
        static var zawgyiFont : UIFont = UIFont(name: appFont, size: 20.00) ?? UIFont.systemFont(ofSize: 17.00)
    }
    
    
    
    
    struct headers {
        struct controllersTitles {
            static var payto : String = "Pay To"
            static let scan  : String = "Scan To Pay"
            static let map   : String = "Map"
            static let recievedRequest : String = "Received Payment Request"
            static let sentRequest : String = "Sent Payment Request"
            static let multiPayto  : String = "Multiple Payment"
            static let mobileNumber : String = "Mobile Number"
        }
        static let title : String = "Pay / Send".localized
    }
}

class PaytoObject <PT: Equatable> {
    
    private var objects : [PT]

    init(initial obj: PT) {
        self.objects = [obj]
    }
    
    init(objects: [PT]) {
        self.objects = objects
    }
    
    func add(object: PT) {
        self.objects.append(object)
    }
    
    func remove(index: Int) {
        self.objects.remove(at: index)
    }
    
     func remove(object: PT) -> [PT] {
        if let index = self.objects.index(of: object) {
            self.objects.remove(at: index)
        }
        return self.objects
    }
    
     func getObjects() -> [PT] {
        return self.objects
    }
    
     func removeObjects() {
        self.objects.removeAll()
    }
    
    deinit {
        println_debug("deinit called in ui of payto structure")
        self.objects.removeAll()
    }
    
}





