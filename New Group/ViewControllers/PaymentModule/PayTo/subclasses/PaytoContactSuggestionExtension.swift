//
//  TableViewCell.swift
//  OK
//
//  Created by Ashish on 8/15/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

extension PayToUIViewController : UITableViewDelegate, UITableViewDataSource {
    
    func hideContactSuggestion() {
        self.contactSuggestion.isHidden = true
        self.contactTableView.isHidden  = true
        self.heightContactSuggestion.constant = 0.0
        let yHeight = self.heightUpdates
        self.delegate?.didChangeHeight(height: yHeight, withIndex: nil)
    }
    
    func showContactSuggestion() {
        if isOfferEnabled { return }
        self.contactSuggestion.isHidden = false
        self.contactTableView.isHidden  = false
        if self.contactSearchedDict.count == 1 {
            self.heightContactSuggestion.constant = 50.00
        } else if self.contactSearchedDict.count == 2 {
            self.heightContactSuggestion.constant = 100.00
        } else {
            self.heightContactSuggestion.constant = 150.00
        }
        let yHeight = self.heightUpdates
        self.delegate?.didChangeHeight(height: yHeight, withIndex: nil)
    }
    
    func loadContacts(txt: String, maxReached: Bool = false, otherCountry: Bool = false) {
        DispatchQueue.main.async {
            self.contactSuggesstionCompletion(number: txt) { (contacts) in
                DispatchQueue.main.async {
                    guard contacts.count > 0 else {
                        self.hideContactSuggestion()
                        self.heightContactSuggestion.constant = 0.00
                        return
                    }
//                    if otherCountry {
//                        self.cnfMobileNumber.hide()
//                    }
                    self.contactSearchedDict = contacts
                    if maxReached {
                        self.hideContactSuggestion()
                        self.heightContactSuggestion.constant = 0.00
                    } else {
                        self.showContactSuggestion()
                    }
                    self.contactTableView.reloadData()
                }
            }
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactSearchedDict.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.00
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchingCell", for: indexPath)
        
        let dict = contactSearchedDict[indexPath.row]
        
        let name  = dict.safeValueForKey(ContactSuggessionKeys.contactname) as? String ?? "Unknown"
        let phone = dict.safeValueForKey(ContactSuggessionKeys.phonenumber_onscreen) as? String ?? "Unknown"
        
        cell.textLabel?.text = name
        println_debug(dict.safeValueForKey(ContactSuggessionKeys.operatorname) as? String)
        if let operatorName = dict.safeValueForKey(ContactSuggessionKeys.operatorname) as? String {
            if phone.hasPrefix("+95") || phone.first == "0" {
                cell.detailTextLabel?.text = phone + " " + "(\(operatorName))"
            } else {
                cell.detailTextLabel?.text = phone 
            }
        } else {
            cell.detailTextLabel?.text = phone
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.hideContactSuggestion()
        
        let dict = self.contactSearchedDict[indexPath.row]
        let mobileNumber = dict[ContactSuggessionKeys.phonenumber_backend] as? String ?? ""
        let name = dict[ContactSuggessionKeys.contactname] as? String ?? "Unknown"
        
        var selectedNumber = ""
        
        if let number = mobileNumber.addingPercentEncoding(withAllowedCharacters: .decimalDigits) {
            let phones  = number.replacingOccurrences(of: "%20", with: "")
            let phones2 = phones.replacingOccurrences(of: "%C2%A0", with: "")
            if let final = phones2.removingPercentEncoding {
                selectedNumber = final
                let fav = FavModel.init("", phone: selectedNumber, name: name, createDate: "", agentID: "", type: "")
                self.remarks.text = ""
                self.contactUpgrade(number: selectedNumber, name: fav.name)
                self.hideContactSuggestion()
            }
        }
    }

    
}
