//
//  PTWebCalls.swift
//  OK
//
//  Created by Ashish on 8/1/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

extension  PayToUIViewController {
    
    //MARK:- Agent Code Validations
    func checkAgentCode(with agentCode: String, repeatPay: Bool = false, mobileObjNum: String = "") {
        
        if agentCode.hasPrefix(UserModel.shared.mobileNo) {
            self.sameNumberAlert()
//            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.ownMobileNumber.localized)
            self.mobileNumber.text = ""
            self.cnfMobileNumber.hide()
            self.name.hide()
            self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
//            self.mobileNumber.becomeFirstResponder()
            return
        } else {
            self.cnfMobileNumber.endEditing(true)
        }
        
        guard appDelegate.checkNetworkAvail() else {
            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.noInternet.localized)
            return
        }

        let baseString = PaytoConstants.url.checkAgentCode.safelyWrappingString()
        
        let mobile = URLQueryItem.init(name: "MobileNumber", value: agentCode)
        let source = URLQueryItem.init(name: "SourceNumber", value: UserModel.shared.mobileNo)
        let transactionType = URLQueryItem.init(name: "TransType", value: "PAYTO")
        let appBuildNumber = URLQueryItem.init(name: "AppBuildNumber", value: buildNumber)
        let osType  = URLQueryItem.init(name: "OsType", value: "1")
        
        let itemsArray = [mobile, source, transactionType, appBuildNumber, osType]
        
        guard let url = PTManagerClass.createUrlComponents(base: baseString, queryItems: itemsArray) else {
            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.pleaseTryAgain.localized)
            return
        }
        
        TopupWeb.genericClass(url: url, param: Dictionary<String,Any>() as AnyObject, httpMethod: "GET") { (response, success) in
            if success {
                DispatchQueue.main.async {
                    println_debug("*****************************")
                    println_debug(response)
                    self.formatAgentCodeResponse(response, repeatPay, mobileObjNum)
                    println_debug("*****************************")
                }
            } else {
                DispatchQueue.main.async {
                PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.pleaseTryAgain.localized)
                }
            }
        }
 
    }
    
    func formatAgentCodeResponse(_ response: Any, _ repeatPay: Bool, _ mobileNumObj: String) {
        guard let dictionary = response as? Dictionary<String,Any> else { return }
        if let code = dictionary.safeValueForKey("Code") as? Int, code != 200  {
            PaytoConstants.alert.showErrorAlert(title: nil, body: dictionary.safeValueForKey("Msg") as? String ?? "")
            self.hidelAllFields()
            let country  = Country(name: "Myanmar", code: "myanmar", dialCode: "+95")
            mobileNumber.updateLeftView(country: country)
            self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
            return
        }
        guard let dataString = dictionary.safeValueForKey("Data") as? String else { return }
        guard dataString.count > 0 else { return }
        guard let dictionaryJson = OKBaseController.convertToDictionary(text: dataString) else { return }

        let agentModel = PTAgentModel(withDictionary: dictionaryJson)
                
        if self.transType.categoryType == .taxi || self.transType.categoryType == .toll || self.transType.categoryType == .bus ||  self.transType.categoryType == .ferry {
            if agentModel.accountType.safelyWrappingString().lowercased() == "SUBSBR".lowercased() {
                self.hidelAllFields()
                self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                //PaytoConstants.alert.showToast(msg: PaytoConstants.messages.noSubscriber.safelyWrappingString())
                noSubscriber()
                return
            }
        }
        
        self.generalizingUIAgainst(agentModel: agentModel, repeatPay, mobileNumObj)
    }
    func noSubscriber(){
        DispatchQueue.main.async {
        alertViewObj.wrapAlert(title: nil, body: "Subscriber Account cannot be used".localized, img:#imageLiteral(resourceName: "dashboard_pay_send.png") )
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            
        })
        
        alertViewObj.showAlert(controller: self)
        }
    }
    
    func sameNumberAlert() {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "", body: PaytoConstants.messages.ownMobileNumber.localized, img: #imageLiteral(resourceName: "dashboard_pay_send"))
            alertViewObj.addAction(title: "OK".localized, style: .target) {
                self.mobileNumber.becomeFirstResponder()
            }
            alertViewObj.showAlert()
        }
    }
    
    func generalizingUIAgainst(agentModel model: PTAgentModel, _ repeatPay: Bool, _ mobileNumObj: String ) {
        self.delegate?.didShowAgentButton(hide: true)
        self.userType.show()
        self.userType.text = model.localTransType.safelyWrappingString().count > 0 ? model.localTransType.safelyWrappingString() : "Unknown"
        
        if model.merchantCatName.safelyWrappingString().count > 0 {
            self.subscriber.show()
            self.subscriber.text = model.mercantName.safelyWrappingString()
        } else {
            self.subscriber.show()
            self.subscriber.text = "Unregistered User"
            self.delegate?.didShowSubmitButton(show: false, path: nil)
            
            if let nav = PaytoConstants.global.navigation {
                if nav.viewControllers.contains(where: {$0.isKind(of: MultiPaytoViewController.self)}) {
                    if self.subscriber.text!.lowercased() == "Unregistered User".lowercased() {
                        self.hidelAllFields()
                        self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                        //PaytoConstants.alert.showToast(msg: "Invalid OK$ Account".localized)
                        invalidNumber()
                        return
                    }
                }
            }
        }
                
        
        if model.businessName.safelyWrappingString().count > 0 {
            self.merchant.text = model.businessName.safelyWrappingString()
            self.merchantLabel.text = model.businessName.safelyWrappingString()
            //print(model.businessName)
            self.merchant.show()
            self.merchantLabel.isHidden = false
        } else {
            self.merchant.hide()
        }
        
        self.amount.show()
        
        if model.accountType.safelyWrappingString().lowercased() == "SUBSBR".lowercased() {
            self.merchant.hide()
            self.subscriber.placeholder = "Subscriber".localized
        } else if model.accountType.safelyWrappingString().lowercased() == "DUMMY".lowercased() {
            self.subscriber.placeholder = "Safety Cashier".localized
        } else if model.accountType.safelyWrappingString().lowercased() == "ADVMER".lowercased(){
            self.subscriber.placeholder = "Advance Merchant".localized
        } else if model.accountType.safelyWrappingString().lowercased() == "AGENT".lowercased(){
            self.subscriber.placeholder = "Agent".localized
        } else if model.accountType.safelyWrappingString().lowercased() == "MER".lowercased() {
            self.subscriber.placeholder = "Merchant".localized
        } else {
            self.subscriber.placeholder = "Subscriber".localized
        }
        accountType = model.accountType
        needToTakePhoto = (model.photoCaptureStatus ?? 1 == 0) ? true : false
        takePhotoAmount = model.payToPhotoCapurePicAmountLimit ?? "0"
        
         if self.transType.type == .cashIn || self.transType.type == .transferTo  || self.transType.type == .cashOut {
            if self.subscriber.text!.lowercased() == "Unregistered User".lowercased() {
                self.hidelAllFields()
                self.mobileNumber.text = (self.mobileNumber.country.dialCode == "+95") ? "09" : ""
                //PaytoConstants.alert.showToast(msg: "Invalid OK$ Account".localized)
                unregistedNumber()
                return
            }
         } else {
            if model.localTransType.safelyWrappingString().lowercased() == "TOLL".lowercased() || model.localTransType.safelyWrappingString().lowercased() == "PARKING".lowercased() || model.localTransType.safelyWrappingString().lowercased() == "FUEL".lowercased() || self.transType.categoryType == .toll {
                self.addVehicle.show()
            }
        }
        
        if model.localTransType.safelyWrappingString().lowercased() == "Restaurant".lowercased() {
            self.userType.text = "Restaurant"
        }
        if self.mobileNumber.text == "" && repeatPay == true {
            self.mobileNumber.text = mobileNumObj
        }
        
        if let amountText = self.amount.text, amountText.count > 0 {
            self.burmeseAmount.show()
            if self.transType.type == .cashIn || self.transType.type == .transferTo {
                let amt = (amountText.replacingOccurrences(of: ",", with: "") as NSString).floatValue
                if amt > ("999999999" as NSString).floatValue || (self.cashInLimit != 0 && amt >= Float(self.cashInLimit)) {
                    PaytoConstants.alert.showToast(msg: "Maximum amount for Cash-In Amount is 999,999,999".localized)
                    self.amount.text = ""
                    self.burmeseAmount.hide()
                    self.remarks.hide()
                    self.hideMyNumber.hide()
                    self.delegate?.didShowSubmitButton(show: false, path: self.indexPath)
                    self.amount.placeholder = "Enter Cash-In Amount".localized
                    if let rightView = self.amount.rightView as? AmountRightView {
                        rightView.hideClearButton(hide: true)
                    }
                } else if amt > ("499" as NSString).floatValue || (self.cashOutLimit != 0 && amt >= Float(self.cashInLimit)) {
                    self.commissionTxt.show()
                    self.totalAmountTxt.show()
                    self.remarks.show()
                    self.hideMyNumber.hide()
                    self.amountCheckingTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(getCommissionForCICOFromApi), userInfo: self.amount.text ?? "", repeats: false)
                    self.amount.placeholder = "Cash-In Amount".localized
                } else {
                    self.checkCashInAmount()
                    self.amount.text = ""
                    self.checkCashInAmount()
                    self.burmeseAmount.hide()
                    self.remarks.hide()
                    self.hideMyNumber.hide()
                    self.delegate?.didShowSubmitButton(show: false, path: self.indexPath)
                    self.amount.placeholder = "Cash-In Amount".localized
                    if let rightView = self.amount.rightView as? AmountRightView {
                        rightView.hideClearButton(hide: true)
                    }
                    self.updateTextColor()
                }
            } else if self.transType.type == .cashOut {
                 let amt = (amountText.replacingOccurrences(of: ",", with: "") as NSString).floatValue
                if amt > ("999999999" as NSString).floatValue || (self.cashOutLimit != 0 && amt >= Float(self.cashOutLimit)) {
                    PaytoConstants.alert.showToast(msg: "Maximum amount for Cash-Out Amount is 999,999,999".localized)
                    self.amount.text = ""
                    self.burmeseAmount.hide()
                    self.remarks.hide()
                    self.hideMyNumber.hide()
                    self.delegate?.didShowSubmitButton(show: false, path: self.indexPath)
                    self.amount.placeholder = "Enter Cash-Out Amount".localized
                    if let rightView = self.amount.rightView as? AmountRightView {
                        rightView.hideClearButton(hide: true)
                    }
                } else  if amt > ("999" as NSString).floatValue || (self.cashOutLimit != 0 && amt >= Float(self.cashOutLimit)) {
                    self.commissionTxt.show()
                    self.totalAmountTxt.show()
                    self.remarks.show()
                    self.hideMyNumber.show()
                    self.amountCheckingTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(getCommissionForCICOFromApi), userInfo: self.amount.text ?? "", repeats: false)
                    self.amount.placeholder = "Cash-Out Amount".localized
                } else {
                    self.checkCashOutAmount()
                    self.amount.text = ""
                    self.checkCashOutAmount()
                    self.burmeseAmount.hide()
                    self.remarks.hide()
                    self.hideMyNumber.hide()
                    self.delegate?.didShowSubmitButton(show: false, path: self.indexPath)
                    self.amount.placeholder = "Cash-Out Amount".localized
                    if let rightView = self.amount.rightView as? AmountRightView {
                        rightView.hideClearButton(hide: true)
                    }
                }
            } else {
                if !isOfferEnabled {
                    self.hideMyNumber.show()
                }
                self.remarks.show()
                self.amount.placeholder = "Amount".localized
                if self.remarks.text == "V5POS" || (offerModel?.minimumPayableAmount ?? 0 > 0 && offerModel?.maximumPayableAmount ?? 0 > 0 && offerModel?.minimumPayableAmount ?? 0 == offerModel?.maximumPayableAmount ?? 0) {
                    self.amount.resignFirstResponder()
                    self.amount.isUserInteractionEnabled = false
                    if let rightView = self.amount.rightView as? AmountRightView {
                        rightView.hideClearButton(hide: true)
                    }
                    self.remarks.text = ""
                }
                self.delegate?.didShowSubmitButton(show: true, path: self.indexPath)
            }
            self.updateTextColor()
        } else {
            self.burmeseAmount.hide()
            self.remarks.hide();
            if transType.type == .cashOut {
                self.amount.placeholder = "Enter Payable Amount".localized
                self.totalAmountTxt.placeholder = "Net Receive Amount".localized
            } else if transType.type == .cashIn {
                self.amount.placeholder = "Enter Cash-In Amount".localized
                self.totalAmountTxt.placeholder = "Net Receive Digital Amount".localized
            } else {
                self.amount.placeholder = "Enter Amount".localized
            }
            self.hideMyNumber.hide()
        }
        
        self.amount.becomeFirstResponder()
        self.delegate?.didChangeHeight(height: self.heightUpdates, withIndex: self.indexPath)
        
        if let nav = PaytoConstants.global.navigation {
            if nav.viewControllers.contains(where: {$0.isKind(of: MultiPaytoViewController.self)}) {
                return
            }
        }
        if isPayToDashboard ?? false {
            if model.isOfferApplicable ?? false {
                println_debug("Call offer list API")
                if appDelegate.checkNetworkAvail() {
                    let urlStr   = Url.offersPromotionByMerchant
                    let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
                    
                    var params = Dictionary<String,Any>()
                    let number     = self.mobileNumber.text
                    
                    let agentNo = getAgentCode(numberWithPrefix: number.safelyWrappingString(), havingCountryPrefix: self.mobileNumber.country.dialCode)
                    params["MerchantMobileNumber"] = agentNo
                    params["CustomerMobileNumber"] = UserModel.shared.mobileNo
                    params["BalanceAmount"] = UserLogin.shared.walletBal
                    params["Limit"] = 100
                    params["OffSet"] = 0
                    
                    TopupWeb.genericClass(url: ur, param: params as AnyObject, httpMethod: "POST", handle: { (response, success) in
                        DispatchQueue.main.async {
                            if !success { return }
                            if let valueDictionary = response as? Dictionary<String, Any> {
                                if valueDictionary.safeValueForKey("Code") as? Int == 200 {
                                    if let stringCollections = valueDictionary.safeValueForKey("Data") as? String {
                                        if let collections = OKBaseController.convertToArrDictionary(text: stringCollections) as? [Dictionary<String,Any>] {
                                            for element in collections {
                                                let jsonString = OKBaseController().JSONStringFromAnyObject(value: element as AnyObject)
                                                if let dataJson = jsonString.data(using: .utf8) {
                                                    do {
                                                        let categoryModel = try JSONDecoder().decode(OffersListModel.self, from: dataJson)
                                                        self.offerArrModel?.append(categoryModel)
                                                    } catch {
                                                        println_debug(error.localizedDescription)
                                                    }
                                                }
                                            }
                                            
                                            if self.offerArrModel?.count ?? 0 > 0 {
                                                isPayToDashboard = false
                                                let myDict = ["number": agentNo, "arrOffer": self.offerArrModel as Any]
                                                notfDef.post(name:Notification.Name(rawValue:"OffersHeaderTitle"),
                                                             object: myDict)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    })
                }
            }
        }
    }
    
    func invalidNumber(){
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: nil, body: "Invalid OK$ Account".localized, img:#imageLiteral(resourceName: "dashboard_pay_send.png") )
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func unregistedNumber(){
        DispatchQueue.main.async {
            var message = ""
            if self.transType.type == .cashIn {
                message = "Can't generate QR to Unregister Number".localized
            } else {
                message = "Can't make payment to Unregister Number".localized
            }
            alertViewObj.wrapAlert(title: nil, body: message, img:#imageLiteral(resourceName: "alert-icon") )
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        }
    }

}








