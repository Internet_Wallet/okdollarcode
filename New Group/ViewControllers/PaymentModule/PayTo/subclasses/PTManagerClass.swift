//
//  PTManagerClass.swift
//  OK
//
//  Created by Ashish on 7/31/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PTManagerClass {
    class func decodeMobileNumber(phoneNumber mobile: String) -> (country: Country, number: String) {
        
        var country : Country = Country(name: "Myanmar", code: "myanmar", dialCode: "+95")
        guard  mobile.count > 0 else { return (country, "") }
        
        // treating 09 prefix as for myanmar itself
        if mobile.hasPrefix("09") {
            return (country, mobile)
        }
        
        // init prefix number
        var prefixedNumber = ""
        if mobile.hasPrefix("00+") {
            prefixedNumber = mobile
            _ = prefixedNumber.remove(at: String.Index.init(encodedOffset: 0))
            _ = prefixedNumber.remove(at: String.Index.init(encodedOffset: 0))
        }
        // operation for mobile number having + prefix
        func plusPrefix(mobile number: String) -> (country: Country, number: String) {
            let countryObj = identifyCountry(withPhoneNumber: number)
            if number.hasPrefix(countryObj.countryCode) {
                prefixedNumber = number.deletingPrefix(countryObj.countryCode)
            }
            if prefixedNumber.hasPrefix("+") {
                prefixedNumber.remove(at: String.Index.init(encodedOffset: 0))
            }
            if countryObj.countryCode == "+95" {
                if !prefixedNumber.hasPrefix("0") {
                    prefixedNumber = "0" + prefixedNumber
                }
            }
            if countryObj.countryCode == "" {
                country  = Country(name: "Myanmar", code: "myanmar", dialCode: "+95")
            } else {
                country  = Country(name: "", code: countryObj.countryFlag, dialCode: countryObj.countryCode)
            }
            return (country, prefixedNumber)
        }
        if mobile.hasPrefix("+") {
            return plusPrefix(mobile: mobile)
        }
        if mobile.hasPrefix("00") {
            prefixedNumber = mobile
            prefixedNumber = prefixedNumber.deletingPrefix("00")
            prefixedNumber = "+" + prefixedNumber
            return plusPrefix(mobile: prefixedNumber)
        }
        if !mobile.hasPrefix("+") && !mobile.hasPrefix("00") {
            let otherNum = "+" + mobile
            return plusPrefix(mobile: otherNum)
        }
        if mobile.hasPrefix("0") {
            return (country, mobile)
        } else {
            var number = mobile
            number = "0" + number
            return (country, number)
        }
    }
    
    class func openMultiContactSelection(_ delegate: PTMultiSelectionDelegate, hideScan: Bool = true) {
        let story = PaytoConstants.global.storyboard()
        if let vc = story.instantiateViewController(withIdentifier: "PayToMultiSelection") as? PayToMultiSelection {
            vc.delegate = delegate // self as? PTMultiSelectionDelegate
            if let keyWindow = UIApplication.shared.keyWindow {
                vc.hideScan = hideScan
                vc.view.frame = keyWindow.bounds
                vc.view.tag = 4354
                UIView.transition(with: keyWindow, duration: 1.5, options: UIView.AnimationOptions.curveEaseIn,
                                  animations:
                    {keyWindow.addSubview(vc.view)}
                    , completion: nil)
                keyWindow.makeKeyAndVisible()
            }
        }
    }
    
    class func contactAccess(_ delegate: ContactPickerDelegate) {
        let nav = UitilityClass.openContact(multiSelection: false, delegate, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
        PaytoConstants.global.navigation?.present(nav, animated: true, completion: nil)
    }
    
    class func createUrlComponents(base: String, queryItems: [URLQueryItem] = [URLQueryItem](), isEstel: Bool = false) -> URL? {
        struct UrlConfig {
            static let appProdUrl = "https://www.okdollar.co/"
            static let appTestUrl = "http://69.160.4.151:8001/"
            
            static let estelProdUrl = "http://www.okdollar.net/"
            static let estelTestUrl = "http://120.50.43.150:8090/"
        }
        #if DEBUG
        let serverApp    = serverUrl == .productionUrl ? UrlConfig.appProdUrl : UrlConfig.appTestUrl
        let serverEstel  = serverUrl == .productionUrl ? UrlConfig.estelProdUrl : UrlConfig.estelTestUrl
        #else
        let serverApp    = UrlConfig.appProdUrl
        let serverEstel  = UrlConfig.estelProdUrl
        #endif
        let urlString = String.init(format: "%@%@", (isEstel) ? serverEstel : serverApp, base)
        guard var urlComponents = URLComponents.init(string: urlString) else { return nil }
        if queryItems.count > 0 {
            urlComponents.queryItems = queryItems
        }
        return urlComponents.url
    }
}
