//
//  PTAmountValidations.swift
//  OK
//
//  Created by Ashish on 8/1/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit


class PTAmountValidation : PTTextFieldSubclass, UITextFieldDelegate {
    
    private let amountAcceptableCharacters = ",0123456789."
    
    weak var subDelegate : PTTextFieldSubclassDelegate?
    
    weak var clearDelegate : HideClearButtonDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.clearDelegate?.hideClearButton(hide: true)
        self.delegate = self
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }
    
    override var text: String? {
        didSet {
            _ = self.text?.amountOnly
        }
    }
    
    @objc func clearAmountHide() {
        self.clearDelegate?.hideClearButton(hide: true)
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x:self.frame.size.width - 82 , y: 0, width: 82 , height: 46)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    
        if textField.text!.count > 0 {
            textField.placeholder = "Amount".localized
            if let amountDelegate = self.subDelegate {
                amountDelegate.amountForCICO(text: 1)
            }
        } else {
            textField.placeholder = "Enter Amount".localized
            if let amountDelegate = self.subDelegate {
                amountDelegate.amountForCICO(text: 0)
            }
        }
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        self.clearDelegate?.hideClearButton(hide: true)
        
        if textField.text!.count > 0 {
            textField.placeholder = "Amount".localized
            if let amountDelegate = self.subDelegate {
                amountDelegate.amountForCICO(text: 1)
            }
        } else {
            textField.placeholder = "Enter Amount".localized
            if let amountDelegate = self.subDelegate {
                amountDelegate.amountForCICO(text: 0)
            }
        }

        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
            let endOfDocument = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
            return false
        }
        
        if string == "0", textField.text!.count <= 0 {
            self.clearDelegate?.hideClearButton(hide: true)
            return false
        }
        if string == ".", textField.text!.count <= 0 {
            self.clearDelegate?.hideClearButton(hide: true)
            return false
        }
        
        let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: amountAcceptableCharacters).inverted
        let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
        
        if string != filteredSet {
            return false
        }
        
        guard let tfText = textField.text else { return false }
        let text     = (tfText as NSString)
        let txt      = text.replacingCharacters(in: range, with: string)
        if txt.count > 0 {
            self.clearDelegate?.hideClearButton(hide: false)
        }
        
        let flag = self.isValidAmount(text: txt)
        
        guard let amountDelegate = self.subDelegate else { return flag }
        
        return amountDelegate.amountResponse(text: txt, view: self) && flag
    }
    
    func isValidAmount(text :String) -> Bool {
        
        if text.contains(".") {
            let amountArray = text.components(separatedBy: ".")

            if amountArray.count > 2 {
                return false
            }
            
            if let last = amountArray.last {
                if last.count > 2 {
                    return false
                } else {
                    return true
                }
            }
            return true
        } else {
            let mystring = text.replacingOccurrences(of: ",", with: "")
            let number = NSDecimalNumber(string: mystring)
            
            let formatter = NumberFormatter()

            formatter.groupingSeparator = ","
            formatter.groupingSize = 3
            formatter.usesGroupingSeparator = true
            var num = formatter.string(from: number)
            if num == "NaN" {
                num = ""
            }
            self.text = num
            return false
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("HidePaytoAmountClearBtn"), object: nil)
    }
    
}

