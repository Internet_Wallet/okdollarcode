//
//  PTHideMyNumberClass.swift
//  OK
//
//  Created by Ashish on 8/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PTHideMyNumberClass: PTTextFieldSubclassNumber, UITextFieldDelegate {
    
    var payWithoutID : Bool? = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.placeholder = ""
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.delegate = self
    }
    
    override func show() {
        super.show()
    }
    
    override func hide() {
        super.hide()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }

}
