//
//  PTAddVehicleSubclass.swift
//  OK
//
//  Created by Ashish on 8/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol ResignAddVehicleTFDelegate: class {
    func resignTextField()
}

class PTAddVehicleSubclass: PTTextFieldSubclass, UITextFieldDelegate, defaultVehicleDelegate, BioMetricLoginDelegate {
    weak var resignTextFieldDelegate: ResignAddVehicleTFDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.placeholder = "Select or Add Vehicle".localized
        self.delegate = self
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }
    
    override func show() {
        super.show()
        self.callForVehicleUpdate()
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x:0 , y: 0, width: 65 , height: 40)
    }
    
    func callForVehicleUpdate() {
        if appDelegate.checkNetworkAvail() {
            let type:Int = 1
            let mobStr:String = UserModel.shared.mobileNo
            let urlString =  "\(Url.GetAllVehicleAPI)MobileNumber=\(mobStr)&SimId=\(uuid)&MsId=\(msid)&OsType=\(type)&Otp=\(uuid)"
            
            guard let urlStr = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else { return }
            let url = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug(url)
            let params = [String: AnyObject]() as AnyObject
            TopupWeb.genericClass(url: url, param: params, httpMethod: "GET", handle: { (response, success) in
                if success {
                    let jsonData = JSON(response)
                    let categoryData = jsonData["Data"].stringValue
                    guard let categorydDict = OKBaseController.convertToArrDictionary(text: categoryData) else { return }
                    let categoryjsonData = JSON(categorydDict)
                    println_debug(categoryjsonData)
                    for i in 0..<categoryjsonData.count {
                        if categoryjsonData[i]["IsDefaultCar"].boolValue {
                            DispatchQueue.main.async {
                                self.placeholder = ""
                                self.text = "\(categoryjsonData[i]["DivisionCode"].stringValue)-\(categoryjsonData[i]["CarNumber"].stringValue)-\(categoryjsonData[i]["CarName"].stringValue)"
                                self.updateLeftViewForVehicle(categoryId: categoryjsonData[i]["CategoryId"].stringValue)
                            }
                        }
                    }
                }
            })
        }
    }
    
    func updateLeftViewForVehicle(categoryId: String) {
        if categoryId.contains(find: "b43e2875-23e3-46ff-a994-2b6f6a3808af") {
            let addVehicleView      = DefaultIconView.updateView(icon: "add_Vech_Truck")
            self.leftView = addVehicleView
           // cell.vehicleImgView.image = #imageLiteral(resourceName: "add_Vech_Truck")
        }
        else if categoryId.contains(find: "44c5a536-60dc-45d4-95dd-98ba8e6cc32f") {
            let addVehicleView      = DefaultIconView.updateView(icon: "add_Vech_Bus")
            self.leftView = addVehicleView
            //cell.vehicleImgView.image = #imageLiteral(resourceName: "add_Vech_Bus")
        }
        else if categoryId.contains(find: "a0eedf0b-7c03-4aac-b0a5-ff45b8106651") {
            let addVehicleView      = DefaultIconView.updateView(icon: "add_Vech_Bike")
            self.leftView = addVehicleView
            //cell.vehicleImgView.image = #imageLiteral(resourceName: "add_Vech_Bike")
        }
        else {
            let addVehicleView      = DefaultIconView.updateView(icon: "add_Vech_Car")
            self.leftView = addVehicleView
           // cell.vehicleImgView.image = #imageLiteral(resourceName: "add_Vech_Car")
        }
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        resignTextFieldDelegate?.resignTextField()
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "AddYourVehicle", delegate: self)
        } else {
            self.callAddVehicleScreen()
        }
        return false
    }
    
    func slelectedVehicle(defaultVehicleStr: String, categoryId: String = "") {
        self.placeholder = ""
        self.text = defaultVehicleStr
        self.updateLeftViewForVehicle(categoryId: categoryId)
    }
    
    func noVehicleFound() {
        self.placeholder = "Select or Add Vehicle".localized
        self.text = ""
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if screen == "AddYourVehicle" {
            if isSuccessful{
                self.callAddVehicleScreen()
            }
        }
    }
    
   private func callAddVehicleScreen() {
        let viewController = UIStoryboard(name: "AddVehicle", bundle: Bundle.main).instantiateViewController(withIdentifier: "addVehicleRoot")
        
        if let nav = viewController as? UINavigationController {
            for views in nav.viewControllers {
                if let vc = views as? AddVehicleVC {
                    vc.delegate = self
                    vc.vehicleDetail = self.text
                    vc.isFromPayto = true
                }
            }
        }
    
        let navigation = PaytoConstants.global.navigation
        navigation?.present(viewController, animated: true, completion: nil)
    }
    
}








