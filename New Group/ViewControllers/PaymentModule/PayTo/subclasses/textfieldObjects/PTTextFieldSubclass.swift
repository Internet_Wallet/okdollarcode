//
//  PTMobileNumberSubclass.swift
//  OK
//
//  Created by Ashish on 7/30/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

//mobile nnumber

class PTTextFieldSubclassNumber: RestrictedCursorMovement {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor.darkGray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
    }
    
    
        @IBInspectable public var leftPadding: CGFloat = 0 {
           didSet {
               let padding = UIView(frame: CGRect(x: 0, y: 0, width: leftPadding, height: 0))
               
               leftViewMode = UITextField.ViewMode.always
               leftView = padding
           }
       }
       
       @IBInspectable public var rightPadding: CGFloat = 0 {
           didSet {
               let padding = UIView(frame: CGRect(x: 0, y: 0, width: rightPadding, height: 0))
               
               rightViewMode = UITextField.ViewMode.always
               rightView = padding
           }
       }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        if self.tag != 511 { // hide my number tag
            self.lowerBorder()
        }
        
        if self.tag == 501 {
            self.show()
        } else {
            self.hide()
        }
        
    }
    
    override func clearButtonRect(forBounds bounds: CGRect) -> CGRect {
        super.clearButtonRect(forBounds: bounds)
        return CGRect.init(x: 0, y: 0, width: 50, height: 50)
    }
    
    func show() {
        self.updateConstraint(constant: 50.00)
        self.isHidden = false
    }
    
    func hide() {
        self.text = ""
        self.updateConstraint(constant: 0.00)
        self.isHidden = true
    }
    
    private func updateConstraint(constant: CGFloat) -> Void {
        if let constraint = self.constraints.filter({$0.identifier == "heightConstraints"}).first {
            constraint.constant = constant
            self.layoutIfNeeded()
        }
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x:leftPadding , y: 0, width: 110 , height: 40)
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.width - 80  , y: 0, width: 82 , height: bounds.height)
    }

}


// other
class PTLabelSubclass: UILabel {
    
    func lowerBorder() {
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0.0, y: self.frame.size.height - width, width:  self.frame.size.width + 100, height: 1.0)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        if self.tag == 499 { // hide my number tag
            self.lowerBorder()
        }
        
        if self.tag == 499 {
            self.show()
        } else {
            self.hide()
        }

    }
   
    func show() {
        self.updateConstraint(constant: 50.00)
        self.isHidden = false
    }
    
    func hide() {
        self.updateConstraint(constant: 0.00)
        self.isHidden = true
    }
    
    private func updateConstraint(constant: CGFloat) -> Void {
        if let constraint = self.constraints.filter({$0.identifier == "heightConstraints"}).first {
            constraint.constant = constant
            self.layoutIfNeeded()
        }
    }
    
}

class PTTextFieldSubclass: RestrictedCursorMovementWithFloat {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        switch self.tag {
        case 511, 508: // hide my number tag and amount
            break
        default:
            self.lowerBorder()
        }

        self.selectedTitleColor = UIColor.colorWithRedValue(redValue: 1, greenValue: 145, blueValue: 85, alpha: 1.0)
        
        if self.tag == 501 {
            self.show()
        } else {
            self.hide()
        }
        
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x:0 , y: 0, width: 65 , height: 40)
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x:bounds.width - 66 , y: 0, width: 65 , height: 40)
    }
    
    override func clearButtonRect(forBounds bounds: CGRect) -> CGRect {
        super.clearButtonRect(forBounds: bounds)
        return CGRect.init(x: 0, y: 0, width: 50, height: 40)
    }
    
    func show() {
        self.updateConstraint(constant: 50.00)
        self.isHidden = false
    }
    
    
   
    
    func hide() {
        //self.text = ""
        self.updateConstraint(constant: 0.00)
        self.isHidden = true
    }
    
  private func updateConstraint(constant: CGFloat) -> Void {
        if let constraint = self.constraints.filter({$0.identifier == "heightConstraints"}).first {
            constraint.constant = constant
            self.layoutIfNeeded()
        }
    }

}




