//
//  PTMobileNumberSubclass.swift
//  OK
//
//  Created by Ashish on 7/31/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol PTTextFieldSubclassDelegate : class {
    func backspaceHandler(string: String, view: UITextField)
    func response(text: String, view: UITextField) -> Bool
    func amountResponse(text: String, view: UITextField) -> Bool
    func amountForCICO(text: Int?)
    func openContacts(selection: ButtonType)
    func hideHideMySuggestionContacts()
    func mobClearfield()
    func hideCnfMobileClear(view: UITextField)
    func hideContactSuggestionWhenEndEditing()
}

class PTMobileNumberSubclass: PTTextFieldSubclassNumber, UITextFieldDelegate, CountryLeftViewDelegate, CICOScanQROpenDelegate {

   private let mobileNumberAcceptableCharacters = "0123456789"
    
    weak var subDelegate : PTTextFieldSubclassDelegate?
    
    weak var subScanQRDelegate: CICOScanQROpenDelegate?
    
    weak var hideClear : HideClearButtonDelegate?
    
    var country : Country = Country(name: "Myanmar", code: "myanmar", dialCode: "+95")

    var showQR = false
    
    var cnfMob = true
    var codeCountry = 5
    
    func amountForCICO(text: Int?) {
        
    }
    override var text: String? {
        didSet {
          _ = self.text?.numbersOnly
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setBottomBorder()
        self.lowerBorder()

    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.setBottomBorder()
        self.lowerBorder()
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
        self.delegate = self
        self.updateLeftView(country: self.country)
        self.setBottomBorder()
        self.lowerBorder()
    }
    
    func updateLeftView(country: Country) {
        self.country = country
        self.leftViewMode = .always
        self.leftView = nil
        
        let view = PaytoViews.updateView()
        let code = String.init(format: "(%@)", country.dialCode)
        codeCountry = code.count
        view.wrapCountryViewData(img: country.code, str: code)
        view.delegate = self
//        if code.count == 7{
//            self.leftView?.frame = CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: 120, height: view.frame.size.height)
//            self.leftView = view
//        }else if code.count == 6{
//            self.leftView?.frame = CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: 95, height: view.frame.size.height)
//            self.leftView = view
//        }else if code.count == 5{
//            self.leftView?.frame = CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: 90, height: view.frame.size.height)
//            self.leftView = view
//        }
       self.leftView?.frame = view.frame
        self.leftView = view
    }
    
    func clearText() {
        if self.country.dialCode == "+95" {
            self.text = "09"
        } else {
            self.text = ""
        }
        self.hideClear?.hideClearButton(hide: true)
        self.subDelegate?.mobClearfield()
    }
    
    func updateRightView(_ delegate: CountryLeftViewDelegate, show: Bool = false, cnfMob: Bool = false)  {
        self.showQR = show
        self.cnfMob = false
        let contact = ContactRightView.updateView(show: show)
        contact.delegate = delegate
        contact.frame.origin.x = self.frame.size.width - contact.frame.size.width - 2
        self.rightViewMode = .always
        // avaneesh
        self.rightView = contact
       
        self.hideClear?.hideClearButton(hide: true)
        if show {
            contact.scanQRImage.isHidden = false
            contact.scanQRButton.isHidden = false
            contact.scanQRImageWidth.constant = 40.0
            contact.scanQRButtonWidth.constant = 40.0
            contact.scanQRDelegate = self
        }
        self.hideClear = contact
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        let width: CGFloat = (self.cnfMob) ? 40 : ((self.showQR) ? 120.0 : 80.0)
        return CGRect(x: bounds.width - width, y: 0, width: width + 2 , height: 50)
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
                if codeCountry == 7{
                    return CGRect(x:0 , y: 3, width: 120 , height: 42)
                }else if codeCountry == 6{
                    return CGRect(x:0 , y: 3, width: 100 , height: 42)
                }else if codeCountry == 5{
                    return CGRect(x:0 , y: 3, width: 95 , height: 42)
                }
        return CGRect(x:0 , y: 3, width: 95 , height: 42)
    }
    
    func openAction(type: ButtonType) {
        self.subDelegate?.openContacts(selection: type)
    }
    
    func clearActionType() {
        
    }
    
    func openQRToScanToPay() {
        self.subScanQRDelegate?.openQRToScanToPay()
    }
    
    override func show() {
        super.show()
        self.setBottomBorder()
        self.lowerBorder()
    }
    
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.becomeFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
      
        
        if let text = textField.text, text.count > 2 {
            self.hideClear?.hideClearButton(hide: false)
        } else {
            if let text = textField.text, text.count <= 2, country.dialCode == "+95" {
                textField.text = "09"
                self.hideClear?.hideClearButton(hide: true)
            }
        }
        self.subDelegate?.hideCnfMobileClear(view: textField)
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.hideClear?.hideClearButton(hide: true)
        self.subDelegate?.hideContactSuggestionWhenEndEditing()
        return true
    }

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        let scanPromotionObj = ["status" :false, "beforePromotion" : "", "afterPromotion": ""] as [String : Any]
        UserDefaults.standard.set(scanPromotionObj, forKey: "ScanQRPromotion")
        if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
            let endOfDocument = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
            return false
        }
        let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: mobileNumberAcceptableCharacters).inverted
        let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
        
        if string != filteredSet { return false }
        
        guard let encoded = string.cString(using: String.Encoding.utf8) else { return false }
        let isBackSpace = strcmp(encoded, "\\b")
        
        guard let tfText   = textField.text else { return false }
        let text     = (tfText as NSString)
        let txt      = text.replacingCharacters(in: range, with: string)

        if (isBackSpace == -92) {

            if textField.text == "09" {
                self.subDelegate?.backspaceHandler(string: txt, view: textField)
                self.hideClear?.hideClearButton(hide: true)
                return (self.country.dialCode == "+95") ? false : true
            } else if txt == "" {
                let flag = self.subDelegate?.response(text: txt, view: textField)
                self.hideClear?.hideClearButton(hide: true)
                return flag ?? false
            } else {
                self.subDelegate?.backspaceHandler(string: txt, view: textField)
                if txt == "09" {
                   self.hideClear?.hideClearButton(hide: true)
                }
                
                return true
            }
        }
        
        if textField.text == "09"{
        
        if txt.count > 2 {
            self.hideClear?.hideClearButton(hide: false)
        }
        } else {
            
            if txt.count > 3 {
                self.hideClear?.hideClearButton(hide: false)
            }else {
                self.hideClear?.hideClearButton(hide: true)
            }
        }
        
        if self.country.dialCode == "+95" {
            if !txt.hasPrefix("09") {
                return false
            }
        }

        //this will go to paytouiview contorller
        let flag = self.subDelegate?.response(text: txt, view: textField)
        
        return flag ?? false
    }
    
    //MARK:- Offer MobileNumberParsing
    func offerModelParsing(_ model: OffersListModel?, text: String) -> Bool {
        
        guard let offer = model else { return false }
        
        let mobileNumbers = offer.mobilenumber.safelyWrappingString()
        
        guard mobileNumbers.count > 0 else { return false }
        
        let mobileNumberArray = mobileNumbers.components(separatedBy: ",")
        
        var flag : Bool = false
        
        for number in mobileNumberArray {
            let agentCodeNumber = PTManagerClass.decodeMobileNumber(phoneNumber: number)
            if agentCodeNumber.number == text {
                flag = true
            }
            //flag = agentCodeNumber.number.hasPrefix(text)
            if flag {
                break
            }
        }

        return flag
    }

}

//MARK:- Offers Delegate
class PTPromoSubclass: PTLabelSubclass {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if self.tag == 499 { // hide my number tag
            self.lowerBorder()
        }
    }
    
    override func show() {
        let height = self.text?.height(withConstrainedWidth: self.frame.width, font: UIFont(name: appFont, size: 12) ?? UIFont.systemFont(ofSize: 12)) ?? 0.0
//        if height < 60 {
//            self.updateConstraint(constant: 50)
//        } else {
//            self.updateConstraint(constant: 80)
//        }
        
        self.updateConstraint(constant: height+20.0)
        self.isHidden = false
    }
    
    override func hide() {
        self.text = ""
        self.updateConstraint(constant: 0.00)
        self.isHidden = true
    }
    
    private func updateConstraint(constant: CGFloat) -> Void {
        if let constraint = self.constraints.filter({$0.identifier == "heightConstraints"}).first {
            constraint.constant = constant
            self.layoutIfNeeded()
        }
    }
    
}

class PTOfferSubclass: PTTextFieldSubclass {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.placeholder = ""
        if self.tag != 500 { // hide my number tag
            self.lowerBorder()
        }
    }

    override func show() {
        self.updateConstraint(constant: 50.00)
        self.isHidden = false
    }
    
    override func hide() {
        self.text = ""
        self.updateConstraint(constant: 0.00)
        self.isHidden = true
    }
    
    private func updateConstraint(constant: CGFloat) -> Void {
        if let constraint = self.constraints.filter({$0.identifier == "heightConstraints"}).first {
            constraint.constant = constant
            self.layoutIfNeeded()
        }
    }

}
