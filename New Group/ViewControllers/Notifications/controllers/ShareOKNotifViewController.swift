//
//  ShareOKNotifViewController.swift
//  OK
//
//  Created by Ashish on 2/20/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class ShareOKNotifViewController: OKBaseController,UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var btnShare : UIButton!
        {
        didSet
        {
            self.btnShare.setTitle("SHARE".localized, for: .normal)
        }
    }
    
    @IBOutlet weak var lblOR : UILabel!
        {
        didSet
        {
            self.lblOR.text = "or".localized
        }
    }
    
    let imageArray  = [#imageLiteral(resourceName: "tbNotif_facebook"), #imageLiteral(resourceName: "tbNotif_gplus"), #imageLiteral(resourceName: "tbNotif_twitter"), #imageLiteral(resourceName: "tbNotif_whatsapp"), #imageLiteral(resourceName: "tbNotif_mail"), #imageLiteral(resourceName: "tbNotif_linked")]
    let stringArray = ["FACEBOOK", "GOOGLE", "TWITTER", "WHATSAPP", "MAIL", "LINKEDIN"]

    @IBOutlet weak var shareCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.shareCollectionView.allowsSelection = true
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return stringArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String.init(describing: ShareCollectionCell.self), for: indexPath) as? ShareCollectionCell
        cell?.wrapCell(imageArray[indexPath.row], str: stringArray[indexPath.row])
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 135, height: 45)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        println_debug("index:::::\(indexPath.row)")
        
        switch indexPath.row {
            
        case 0:
            self.faceBookAction()
            
        case 1:
            self.googlePlusAction()
            
        case 2:
            self.twitterAction()
            
        case 3:
            self.whatsAppAction()
            
        case 4:
            self.emailAction()
            
        case 5:
            self.linkedInAction()
            
        default:
            break
            
        }
        
    }
    
    func faceBookAction() {
        
        if UIApplication.shared.canOpenURL(URL(string: "fb://")!) {
            let url = URL(string: "fb://profile?app_okdollar")
            UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            
        } else {
            let url = URL(string: "http://www.facebook.com/okdollarapp/")
            UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            
        }
        
    }
    
    func googlePlusAction(){
        
        if UIApplication.shared.canOpenURL(URL(string: "gplus://")!) {
            let url = URL(string: "gplus://plus.google.com/+WpguruTv")
            UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        }
        
        else {
            let url = URL(string: "http://plus.google.com/+WpguruTv")
            UIApplication.shared.open(url!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        }
        
    }
    func twitterAction(){
        let twitterURL = URL(string: "twitter://user?screen_name=username")
        if let aURL = twitterURL {
            if UIApplication.shared.canOpenURL(aURL) {
                if let aURL = twitterURL {
                    UIApplication.shared.open(aURL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                }
            }
            else {
                if let aString = URL(string: "http://www.twitter.com/username") {
                    UIApplication.shared.open(aString, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                }
            }
        }
        
    }
    func whatsAppAction(){
        
        let urlWhats = "whatsapp://send?phone=+959454994404&abid=12354&text=Hello"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL) {
                    UIApplication.shared.open(whatsappURL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    
                } else {
                    println_debug("Install Whatsapp")
                }
            }
        }

        
    }
    
    func emailAction(){
        let email = "customercare@okdollar.com"
        if let url = URL(string: "mailto:\(email)") {
            UIApplication.shared.open(url)
        }
        else{
            println_debug("No Mail Configured")
        }

    }
    
    func linkedInAction(){
        
        let webURL = URL(string: "https://www.linkedin.com/in/yourName-yourLastName-yourID/")!
        
        let appURL = URL(string: "linkedin://profile/yourName-yourLastName-yourID")!
        
        if UIApplication.shared.canOpenURL(appURL) {
            UIApplication.shared.open(appURL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        } else {
            UIApplication.shared.open(webURL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        }
    }

    @IBAction func shareAction(_ sender: Any) {
        
        let textToShare: String = "Share OK$ application to your family & Friends"
        let link: String = "https://itunes.apple.com/us/app/ok-$/id1067828611?ls=1&mt=8"
        
        let url:NSURL = NSURL.init(string: link)!
        var activityItems = [Any]()
        activityItems.append(textToShare)
        activityItems.append(url)
        
        let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activityViewController.setValue("Hey this is an awsome app.You also download and enjoy the features", forKey: "subject")
        activityViewController.excludedActivityTypes = [.assignToContact, .print]
        DispatchQueue.main.async { self.present(activityViewController, animated: true, completion: nil) }

    }
}

class ShareCollectionCell : UICollectionViewCell {

    @IBOutlet weak var imageV  : UIImageView!
    @IBOutlet weak var lblNames: UILabel!
    
    func wrapCell(_ img: UIImage, str: String) -> Void {
        self.imageV.image = img
        self.lblNames.text = str
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
