//
//  AppUpdateNotifViewController.swift
//  OK
//
//  Created by Ashish on 2/20/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class AppUpdateNotifViewController: UIViewController {

    @IBOutlet weak var btnAppInfo : UIButton!
        {
        didSet
        {
            self.btnAppInfo.titleLabel?.font = UIFont(name: appFont, size: appTitleButton)
            self.btnAppInfo.setTitle("APP INFO".localized, for: .normal)
        }
    }
    @IBOutlet weak var lblWhatsNew : UILabel!
        {
        didSet
        {
            self.lblWhatsNew.font = UIFont(name: appFont, size: appFontSize)
            self.lblWhatsNew.text = "WHAT'S NEW".localized
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK:- IBActions
    @IBAction func appInfoAction(_ sender: UIButton) {
        if let appSettings = URL(string: UIApplication.openSettingsURLString + Bundle.main.bundleIdentifier!) {
            if UIApplication.shared.canOpenURL(appSettings) {
                UIApplication.shared.open(appSettings)
            }
        }
    }
    
    @IBAction func appUpdateAction(_ sender: Any) {
        
        let urlStr = "https://itunes.apple.com/us/app/ok-$/id1067828611?ls=1&mt=8"
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: urlStr)!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            
        } else {
            UIApplication.shared.openURL(URL(string: urlStr)!)
        }
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
