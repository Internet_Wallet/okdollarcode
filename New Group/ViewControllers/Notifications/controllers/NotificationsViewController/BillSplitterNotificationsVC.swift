//
//  BillSplitterNotificationsVC.swift
//  OK
//
//  Created by PC on 3/29/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SDWebImage
import Foundation

class BillSplitterNotificationsVC: OKBaseController,PTWebResponseDelegate {
    
    @IBOutlet weak var tblList: UITableView!
    let refreshControl = UIRefreshControl()
    weak var delegate : ShowHideSearchButtonProtocol?
    var delegateInBox : InBoxightBarShow?

    var navigationObject : UINavigationController?

    var arrPendingList = [BillSplitterAllRequestModel]()
    var intRowPayment : Int  = 0
    var transaction      = Dictionary<String,Any>()
    var processedTransaction = [Dictionary<String,Any>]()
    var agentType : String = ""
    var businessNameNew = ""
    var mobileNumberString : String = ""
    
    override func viewWillAppear(_ animated: Bool) {
       
        self.getDataFromTheServer()

           // self.multiButton?.hide()
          //  self.delegate?.showHideSearchButton(status: false)
            
        //DispatchQueue.main.async(execute: {

        self.delegate?.showHideSearchButton(status: false)
        NotificationCenter.default.post(name: NSNotification.Name("inboxRightBarHide"), object: nil, userInfo: ["HideSearch": true])
        //self.delegateInBox?.showInBoxRightBar(sttaus: false, getIndex: 4)

        //NotificationCenter.default.post(name: NSNotification.Name("inboxRightBarHidesent"), object: nil, userInfo: ["HideSearch": true])
            
        //});
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 10.0, *) {
            tblList.refreshControl = refreshControl
        } else {
            tblList.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshBillNotification(_:)), for: .valueChanged)
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTableView), name: NSNotification.Name("ReloadNotificationVC"), object: nil)
        
    }
    
    @objc private func refreshBillNotification(_ sender: Any) {
            self.getDataFromTheServer()
    }
    
    @objc func reloadTableView() {
        self.getDataFromTheServer()
    }

    func getDataFromTheServer() {
        
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            showProgressView()
            //https://www.okdollar.co/RestService.svc/RequestMoneyViewByReciever?MobileNumber=00959969948254&Simid=89950510000137550532&MSID=414051013755053&OSType=0&OTP=&Status=Requested&LocalTransType=ALL&Limit=5&OffSet=0&RequestModuleType=1
            
            let buldApi = Url.billSplitterNotification + "MobileNumber=\(UserModel.shared.mobileNo)&Simid=\(UserModel.shared.simID)&MSID=\(msid)&OSType=1&OTP=\(uuid)&Status=Requested&LocalTransType=ALL&Limit=100&OffSet=0&RequestModuleType=1"
            
            let url = getUrl(urlStr: buldApi, serverType: .serverApp)
            println_debug(url)
            
            let params = Dictionary<String,String>()
            
            web.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", mScreen: "mBillSplitterNotification")
        } else {
            self.refreshControl.endRefreshing()
        }
    }
    
    func getCategoryList(number: String) {
        
         if appDelegate.checkNetworkAvail() {
             let web      = WebApiClass()
             web.delegate = self
             showProgressView()
             
             let buldApi = Url.billSplitterMobileValidation + "MobileNumber=\(number)"
             let url = getUrl(urlStr: buldApi, serverType: .serverApp)
             println_debug(url)
             let params = Dictionary<String,String>()
             
             web.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", mScreen: "mMobileValidation")
           }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


//MARK: - Web Response Delegate


extension BillSplitterNotificationsVC : WebServiceResponseDelegate {
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        self.removeProgressView()
        
        
        if screen == "mBillSplitterNotification" {
            
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dataDict = dict as? Dictionary<String,AnyObject> {
                        println_debug(dataDict)
                        if let dicData = dataDict["Data"] as? String {
                            let arrData = OKBaseController.convertToArrDictionary(text: dicData)
                            
                            if arrData == nil { return }
                            
                            if arrData?.count == 0
                            {
                                 DispatchQueue.main.async {
                                self.refreshControl.endRefreshing()
                                let nib = Bundle.main.loadNibNamed("NoRecordsView", owner: self, options: nil)?[0] as! NoRecordsView
                                self.tblList.backgroundView = nib
                                }
                            }
                            
                            arrPendingList.removeAll()
                        
                            for element in arrData! {
                                
                                println_debug(element)
                                let notificationModel =  BillSplitterAllRequestModel()
                                notificationModel.id = "\(element["RequestId"]! ?? "")"
                                
                                if !(element["AgentName"] is NSNull) {
                                    notificationModel.name = "\(element["AgentName"]! ?? "")"
                                    if(notificationModel.name == "Not Exist")
                                    {
                                        notificationModel.name = "Unknown".localized
                                    }
                                }
                                else
                                {
                                    notificationModel.name = "Unknown".localized
                                    
                                }
                                
                                if !(element["CategoryName"] is NSNull) {
                                    notificationModel.category = "\(element["CategoryName"]! ?? "")"
                                }
                                else
                                {
                                    notificationModel.category = "Not Registered".localized
                                }
                                notificationModel.requestType = "OK$ Money"
                                notificationModel.number = "\(element["Source"]! ?? "")"
                                notificationModel.localTransactionType = "\(element["LocalTransactionType"]! ?? "")"
                                notificationModel.commanTransactionType = "\(element["CommanTransactionType"]! ?? "")"
                                
                                if let ary = element["AttachmentPath"] as? [String] {
                                    if(ary.count > 0)
                                    {
                                        let x: String = ary[0]
                                        println_debug(x)
                                        notificationModel.attachedFile = "Yes".localized
                                        notificationModel.attachedFileURL = x
                                    }
                                    else{
                                        notificationModel.attachedFile = "No".localized
                                    }
                                }
                                
                                notificationModel.profileImage = "\(element["ProfileImage"]! ?? "")"

                                let intdata = "\(element["Amount"]! ?? "")"
                                notificationModel.amount = Int(intdata)!
                                
                                notificationModel.dateTime = "\(element["Scheduledtime"]! ?? "")"
                                arrPendingList.append(notificationModel)
                            }
                             DispatchQueue.main.async {
                            self.refreshControl.endRefreshing()
                            self.tblList.reloadData()
                            }
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                                       self.delegate?.showHideSearchButton(status: false)

                                print("Bill Splitter Web responce called-------")
                    
                                NotificationCenter.default.post(name: NSNotification.Name("inboxRightBarHide"), object: nil, userInfo: ["HideSearch": true])
                            }
                                 
                        }
                        
                    }
                    
                }
            } catch {
                
            }
        }
        //mBillSplitterCancel
        if (screen == "mBillSplitterNotificationCancel")
        {
            
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dataDict = dict as? Dictionary<String,AnyObject> {
                        println_debug(dataDict)
                        if dataDict["Code"]! as! Int == 200
                        {
                            
                            DispatchQueue.main.async {
                                
                                alertViewObj.wrapAlert(title: nil, body:"Your Request Cancelled Successfully".localized, img: #imageLiteral(resourceName: "bank_success"))
                                
                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                    self.getDataFromTheServer()
                                }
                                alertViewObj.showAlert(controller: self)
                                
                            }
                        }
                        else{
                             DispatchQueue.main.async {
                            alertViewObj.wrapAlert(title: nil, body:"\(dataDict["Msg"]!)", img: #imageLiteral(resourceName: "bill_splitter"))
                            
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                
                            }
                            alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                    
                }
            } catch {
                
            }
        }
        //mBillSplitterPay
        if (screen == "mBillSplitterNotificationPay")
        {
            
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dataDict = dict as? Dictionary<String,AnyObject> {
                        println_debug(dataDict)
                        
                        if dataDict["Code"]! as! Int == 200
                        {
                            
                            DispatchQueue.main.async {
                                
                                alertViewObj.wrapAlert(title: nil, body:"Bill Splitter Reminder Successfully.".localized, img: #imageLiteral(resourceName: "bill_splitter"))
                                
                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                    
                                }
                                alertViewObj.showAlert(controller: self)
                                
                            }
                        }
                        else if dataDict["Code"]! as! Int == 311
                        {
                            DispatchQueue.main.async {
                                alertViewObj.wrapAlert(title: nil, body:"You can not request to same destination mobile number with in 5 minutes.".localized, img: #imageLiteral(resourceName: "bill_splitter"))
                                
                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                    
                                }
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                alertViewObj.wrapAlert(title: nil, body:"\(dataDict["Msg"]!)", img: #imageLiteral(resourceName: "bill_splitter"))
                                
                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                    
                                }
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                }
            } catch {
                
            }
        }
         if screen == "mMobileValidation"
                {
                    do {
                        if let data = json as? Data {
                            let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                            if let dic = dict as? Dictionary<String,AnyObject> {
                                println_debug(dic)
                                print("BankCounterDepositDetailsVC Dic----\(dic)")

                                if dic["Code"]! as! Int == 200
                                {
                                    
                                    DispatchQueue.main.async {
                                       
                                        let dataDict = dic["Data"] as? String
                                        let tempData = OKBaseController.convertToDictionary(text: dataDict!)
                                        //print(tempData)
                                       // self.agentType.append(tempData!["AccountType"]! as! String)
//                                        print(tempData?["AccountType"] as! String)
//                                        print(tempData?["BusinessName"] as! String)
                                        
                                         self.agentType = tempData?["AccountType"] as! String
                                        self.businessNameNew = tempData?["BusinessName"] as! String
                                       // self.count = self.count + 1
                                        
        //                                if self.count == self.mobileNumberArray.count{
        //                                    self.tableV.delegate = self
        //                                    self.tableV.dataSource = self
        //
        //
        //                                }
                                      //  DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
                                           //
                                       // })
             
                                    }
                                }
                                else {
                                    DispatchQueue.main.async {
                                        
                                        alertViewObj.wrapAlert(title: nil, body:"This Number is not registered with OK$".localized, img: UIImage(named: "phone_alert"))
                                        
                                        alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                            
                                        }
                                        alertViewObj.showAlert(controller: self)
                                        
                                    }
                                }
                            }
                            
                        }
                    } catch {
                        
                    }
                }
    }
}

extension BillSplitterNotificationsVC : UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 334.0;//Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPendingList.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "BillSplitterNotificationCell"
        var cell: BillSplitterNotificationCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BillSplitterNotificationCell
        if cell == nil {
            tableView.register(UINib(nibName: "BillSplitterNotificationCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? BillSplitterNotificationCell
        }
      
        var notificationModel: BillSplitterAllRequestModel!
        notificationModel = arrPendingList[indexPath.row]
        
        cell.merchantValue.text =  notificationModel.name
        cell.categoriesValue.text =  notificationModel.category
        cell.requestTypeValue.text = "OK$ Money".localized
        cell.numberValue.text = self.getActualNum(notificationModel.number,withCountryCode: "+95")//remove 0095 with 0
        cell.attachedValue.text =  notificationModel.attachedFile
        
        cell.amountValue.text = self.getDigitDisplay("\(notificationModel.amount)") + " MMK"
        
        cell.dateValue.text =  self.getDisplayDate(notificationModel.dateTime)
        
        cell.imageView_profile.sd_setImage(with: self.getImageURLFromString(notificationModel.profileImage), placeholderImage: UIImage(named: "avatar"))
        
        cell.btnCancel.tag = indexPath.row
        cell.btnCancel.addTarget(self, action:#selector(self.cancelAction(sender:)), for: .touchUpInside)
        
        cell.btnPay.tag = indexPath.row
        cell.btnPay.addTarget(self, action:#selector(self.payAction(sender:)), for: .touchUpInside)
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var notificationModel: BillSplitterAllRequestModel!
        notificationModel = arrPendingList[indexPath.row]
        
        if (notificationModel.attachedFile == "Yes")
        {
            let imagePreviewVC = UIStoryboard(name: "BillSplitterView", bundle: Bundle.main).instantiateViewController(withIdentifier: "ImagePreviewVC") as! ImagePreviewVC
            imagePreviewVC.strURL = (notificationModel.attachedFileURL as? String)!
            self.navigationObject?.pushViewController(imagePreviewVC, animated: true)
        }
        
    }
    
    //To select Cancel
    @objc func cancelAction(sender:UIButton){
        //println_debug("cancelAction")
        
        alertViewObj.wrapAlert(title: "", body: "Do you want to Cancel Payment Request?".localized , img: #imageLiteral(resourceName: "dashboard_requestmoney"))
        alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
            
        }
        alertViewObj.addAction(title: "OK".localized, style: .target) {
            self.intRowPayment = sender.tag
            OKPayment.main.authenticate(screenName: "BillSplitterNotificationsCancelVC", delegate: self)
        }
        alertViewObj.showAlert(controller: self)
    }
    
    @objc func payAction(sender:UIButton){
        //println_debug("payAction")
        
        
        // Payment Button Action
        //Check money in account
        var notificationModel: BillSplitterAllRequestModel!
        notificationModel = self.arrPendingList[sender.tag]
        
        if NSString(string: UserLogin.shared.walletBal).intValue < notificationModel.amount  {
            
            alertViewObj.wrapAlert(title: "", body: "Your account have insufficient balance. Please recharge OK$ Wallet money at nearest OK$ Service Counter.".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "Add Money".localized, style: AlertStyle.target, action: {
                 DispatchQueue.main.async {
            let addWithdrawView = UIStoryboard(name: "AddWithdrawMain", bundle: nil).instantiateViewController(withIdentifier: "AddWithdrawView_ID") as! AddWithdrawViewController
                    addWithdrawView.navigationStatus = true
                self.navigationObject?.pushViewController(addWithdrawView, animated: true)
                }
            })
            
            alertViewObj.showAlert(controller: self)
        }
        else{
            
            //Pay Money
            alertViewObj.wrapAlert(title: "", body: "Are you sure you want to pay request?".localized , img: #imageLiteral(resourceName: "dashboard_requestmoney"))
            alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
                
            }
            alertViewObj.addAction(title: "OK".localized, style: .target)
            {
                self.intRowPayment = sender.tag
                OKPayment.main.authenticate(screenName: "BillSplitterNotificationsVC", delegate: self)
                var mobileNo =  notificationModel.number
                if mobileNo.hasPrefix("09"){
                    mobileNo = "0095\((mobileNo).substring(from: 1))"
                }
                self.getCategoryList(number: mobileNo)
            }
            
            alertViewObj.showAlert(controller: self)
        }
        
        
    }
    
}

extension BillSplitterNotificationsVC: BioMetricLoginDelegate{
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String)
    {
        println_debug("Authorised")
        if isSuccessful{
        //https://www.okdollar.net/WebServiceIpay/services/request;requesttype=FEELIMITKICKBACKINFO;agentcode=00959769335942;destination=00959421899924;amount=1;pin=B6ID6aF5HJ9Jd8a;vendorcode=IPAY;clientip=;clientos=ios;transtype=PAYTO;clienttype=GPRS;securetoken=Q4bAn5hHgL
        
        if screen == "BillSplitterNotificationsCancelVC"
        {
            if appDelegate.checkNetworkAvail() {
                let web      = WebApiClass()
                web.delegate = self
                self.showProgressView()
                
                var notificationModel: BillSplitterAllRequestModel!
                notificationModel = self.arrPendingList[intRowPayment]
                
                let buldApi = Url.billSplitterCancelNotification + "MobileNumber=\(UserModel.shared.mobileNo)&Simid=\(UserModel.shared.simID)&MSID=\(msid)&OSType=1&OTP=\(uuid)&RequestId=\(notificationModel.id)&Status=Rejected"
                
                let url = getUrl(urlStr: buldApi, serverType: .serverApp)
                println_debug(url)
                
                let params = Dictionary<String,String>()
                
                web.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", mScreen: "mBillSplitterNotificationCancel")
            }
        }
        else
        {
        if appDelegate.checkNetworkAvail() {
            println_debug("Single Payment Initiated")
            let web = PayToWebApi()
            web.delegate = self
            
            var notificationModel: BillSplitterAllRequestModel!
            notificationModel = self.arrPendingList[intRowPayment]
            
            var mobileNo =  notificationModel.number//getAgentCode(numberWithPrefix: notificationModel.number, havingCountryPrefix: "+95")

            if mobileNo.hasPrefix("09"){
                mobileNo = "0095\((mobileNo).substring(from: 1))"
            }
            self.mobileNumberString = mobileNo
            
            var ip = ""
            if let ipAdd = OKBaseController.getIPAddress() {
                ip = ipAdd
            }
            
            let amount = String(notificationModel.amount).replacingOccurrences(of: ",", with: "")
            
            let urlString = String.init(format: PTHelper.confirmPay, UserModel.shared.mobileNo,mobileNo,amount,ok_password ?? "","IPAY",ip,"ios","PAYTO","GPRS",UserLogin.shared.token)
            println_debug(urlString)
            let url = URL.init(string: urlString)
            let pRam = Dictionary<String,String>()
            web.genericClassXML(url: url!, param: pRam as AnyObject, httpMethod: "POST", mScreen: "ConfirmationMovement")
        }
        }}
    }
    
    func webSuccessResult(data: Any, screen: String) {

        //MARK:- ConfirmationMovement
        if screen == "ConfirmationMovement"
        {
            DispatchQueue.main.async {
                if let xmlString = data as? String {
                    let xml = SWXMLHash.parse(xmlString)
                    self.transaction.removeAll()
                    self.enumerate(indexer: xml)
                    self.processedTransaction.removeAll()
                    
                    var notificationModel: BillSplitterAllRequestModel!
                    notificationModel = self.arrPendingList[self.intRowPayment]
                    
                    if self.agentType == "SUBSBR"{
                        self.transaction["businessName"] = ""
                    }else{
                        self.transaction["businessName"] = self.businessNameNew
                    }
                    
                    self.transaction["destinationName"]  = notificationModel.name
                    self.transaction["localRemark"]     = "Bill Splitter Request"
                    self.transaction["requestId"]     = notificationModel.id

                    self.processedTransaction.append(self.transaction)
                    if self.transaction["resultdescription"] as? String == "Transaction Successful" {
                        DispatchQueue.main.async {
                            let vc =  UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PayToTransactionConfirmationVC") as? PayToTransactionConfirmationVC
                            vc?.displayResponse = self.processedTransaction
                            vc?.agentTypeNew = self.agentType
                            vc?.requestMoney = true
                            self.navigationObject?.pushViewController(vc!, animated: true)
                        }
                    }
                    else
                    {
                        alertViewObj.wrapAlert(title: "", body: self.transaction["resultdescription"] as? String ?? "", img: #imageLiteral(resourceName: "dashboard_requestmoney"))
                        
                        alertViewObj.addAction(title: "OK".localized, style: .target)
                        {
                            println_debug(self.transaction["resultdescription"] as? String ?? "")
                        }
                        
                        alertViewObj.showAlert(controller: self)
                    }
                }
            }
            println_debug(data)
        }
    }
    
    func webFailureResult(screen: String) {
        println_debug("Something went wrong")
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            transaction[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
}
    

//MARK:- Cell
class BillSplitterNotificationCell : UITableViewCell {
    
    @IBOutlet var merchantKey     : UILabel!
    {
        didSet
        {
            merchantKey.font = UIFont(name: appFont, size: 15)
            merchantKey.text = "Name".localized
        }
    }
    @IBOutlet var categoriesKey   : UILabel!
        {
        didSet
        {
            categoriesKey.font = UIFont(name: appFont, size: 15)
            categoriesKey.text = "Categories".localized
        }
    }
    @IBOutlet var numberKey       : UILabel!
        {
        didSet
        {
            numberKey.font = UIFont(name: appFont, size: 15)
            numberKey.text = "Number".localized
        }
    }
    @IBOutlet var attachedFileKey : UILabel!
        {
        didSet
        {
            attachedFileKey.font = UIFont(name: appFont, size: 15)
            attachedFileKey.text = "Attached File".localized
        }
    }
    @IBOutlet var amountKey       : UILabel!
        {
        didSet
        {
            amountKey.font = UIFont(name: appFont, size: 15)
            amountKey.text = "Amount".localized
        }
    }
    @IBOutlet var dateKey         : UILabel!
        {
        didSet
        {
            dateKey.font = UIFont(name: appFont, size: 15)
            dateKey.text = "Date & Time".localized
        }
    }
    @IBOutlet var requestTypeKey : UILabel!
        {
        didSet
        {
            requestTypeKey.font = UIFont(name: appFont, size: 15)
            requestTypeKey.text = "Request Type".localized
        }
    }

    @IBOutlet var merchantValue    : UILabel!{
        didSet
        {
            merchantValue.font = UIFont(name: appFont, size: 15)
        }
    }
    @IBOutlet var categoriesValue  : UILabel!{
        didSet
        {
            categoriesValue.font = UIFont(name: appFont, size: 15)
        }
    }
    @IBOutlet var numberValue      : UILabel!{
        didSet
        {
            numberValue.font = UIFont(name: appFont, size: 15)
        }
    }
    @IBOutlet var attachedValue    : UILabel!{
        didSet
        {
            attachedValue.font = UIFont(name: appFont, size: 15)
        }
    }
    @IBOutlet var amountValue      : UILabel!{
        didSet
        {
            amountValue.font = UIFont(name: appFont, size: 15)
        }
    }
    @IBOutlet var dateValue        : UILabel!{
        didSet
        {
            dateValue.font = UIFont(name: appFont, size: 15)
        }
    }
    @IBOutlet var requestTypeValue : UILabel!{
        didSet
        {
            requestTypeValue.font = UIFont(name: appFont, size: 15)
        }
    }

    @IBOutlet var imageView_profile: UIImageView!
    @IBOutlet var btnPay: UIButton!
    {
        didSet
        {
            btnPay.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnPay.setTitle("Pay".localized, for: .normal)
        }
    }
    @IBOutlet var btnCancel: UIButton!
        {
        didSet
        {
            btnCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnCancel.setTitle("Cancel".localized, for: .normal)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageView_profile.cornerEdge(radius: self.imageView_profile.frame.size.height / 2)
    }
}
