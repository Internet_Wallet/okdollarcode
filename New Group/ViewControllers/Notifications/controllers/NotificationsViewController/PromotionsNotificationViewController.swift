//
//  PromotionsNotificationViewController.swift
//  OK
//
//  Created by Ashish on 2/23/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import UserNotifications

class PromotionsNotificationViewController: OKBaseController, UNUserNotificationCenterDelegate {

    @IBOutlet weak var _switch: UISwitch!
    @IBOutlet weak var lblTitle: UILabel!
    {
        didSet
        {
           lblTitle.font = UIFont(name: appFont, size: appFontSize)
           lblTitle.text = "If you turn off our push notification it will off promotion and offer notification".localized
        }
    }

    @IBOutlet weak var lblSubTitle: UILabel!
        {
        didSet
        {
            lblSubTitle.font = UIFont(name: appFont, size: appFontSize)
            lblSubTitle.text = "Push Notification".localized
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkAuthorisationForPushNotification()
    }
    
     // MARK:- Check Initial 
    private func checkAuthorisationForPushNotification() {
        let isRegisteredForPN = UIApplication.shared.isRegisteredForRemoteNotifications
        self._switch.isOn     = isRegisteredForPN
    }
    
    @IBAction func promotionsSwitchAction(_ sender: UISwitch) {
        self.checkSwitchStatus(sender.isOn)
    }
    
    // MARK:- Check PN Authorisation
    private func checkSwitchStatus(_ status: Bool) {
        switch status {
        case true:
            self.authorisePushNotification()
        case false:
            OKSessionValidation.shared.unregisterForRemoteNotifications()
            self.checkAuthorisationForPushNotification()
        }
    }
    
    
    func authorisePushNotification() {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil{
                    DispatchQueue.main.async(execute: {
                        UIApplication.shared.registerForRemoteNotifications()
                        self.checkAuthorisationForPushNotification()
                    })
                }
            }
        } else {
            let settings = UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        println_debug(error)
    }
}





