//
//  AllNotificationsViewController.swift
//  OK
//
//  Created by Ashish on 2/23/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class AllNotificationsViewController: OKBaseController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableAllNotif: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: AllNotificationCell.self), for: indexPath) as? AllNotificationCell
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 175.0
    }

}

//MARK:- Notification Cell
class AllNotificationCell : UITableViewCell {
    
}
