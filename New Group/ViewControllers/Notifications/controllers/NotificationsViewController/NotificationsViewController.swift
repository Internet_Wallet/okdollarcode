//
//  NotificationsViewController.swift
//  OK
//
//  Created by Ashish on 2/20/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class NotificationsViewController: OKBaseController, PaytoScrollerDelegate {
    
    fileprivate var paytoMenu : PaytoScroller?
    fileprivate var controllerArray : [UIViewController] = []
    
    var navigation : UINavigationController?
    var delegate: InBoxightBarShow?
    var selectedIndex = 0
    var checkloader : Bool = true


    override func viewDidLoad() {
        super.viewDidLoad()
        print("NotificationsViewController viewDidLoad called------")

        self.setupTabViewControllers()

//        let paySend = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: AllNotificationsViewController.self))
//        paySend?.title = "All"
    }
    
    private func setupTabViewControllers() {
        print("setupTabViewControllers called------")
        let story = UIStoryboard(name: "Notifications", bundle: nil)
        let allNotify = story.instantiateViewController(withIdentifier: String.init(describing: TBAllNotificationsTabViewController.self)) as? TBAllNotificationsTabViewController
        allNotify?.vcTitle = "All"
        allNotify?.called = false
        allNotify?.title = "All Inbox".localized
        
        let transactNotify = story.instantiateViewController(withIdentifier: String.init(describing: TBAllNotificationsTabViewController.self)) as? TBAllNotificationsTabViewController
        transactNotify?.vcTitle = "Payment Transaction"
        transactNotify?.called = true
        transactNotify?.title = "Payment Transaction".localized
        
        let fourth = UIStoryboard(name: "PendingRequestSB", bundle: nil).instantiateViewController(withIdentifier: "pending_approval_request_result") as! PendingApprovalRequestVC
        fourth.vcTitle = "Received Payment Request"
        fourth.title = "Received Payment Request".localized
        fourth.FromScanQR = "ReceivedPayment"
        fourth.navigationObject = navigation
        fourth.selectedItemIndex = 4

        let fifth = UIStoryboard(name: "PendingRequestSB", bundle: nil).instantiateViewController(withIdentifier: "pending_request_result") as! PendingRequestVC
        fifth.title = "Sent Payment Request".localized
        fifth.FromScanQR = "SentPayment"
        fifth.navigationObject = navigation
        fifth.selectedItemIndex = 1
        
        let billSplitter = UIStoryboard(name: "Notifications", bundle: nil).instantiateViewController(withIdentifier: "BillSplitterNotificationsVC") as? BillSplitterNotificationsVC
        billSplitter?.title = "Bill Splitter".localized
        billSplitter?.navigationObject = navigation
        
        let promo = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: PromotionsNotificationViewController.self))
        promo?.title = "Promotions".localized
        
        controllerArray = [allNotify!, transactNotify!, fourth, fifth, billSplitter!, promo!]
        
        let parameters = [PaytoScrollerOptionMenuItemSeparatorWidth: 0, PaytoScrollerOptionMenuItemSeparatorPercentageHeight: 0.1,
                          PaytoScrollerOptionScrollMenuBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectedMenuItemLabelColor: MyNumberTopup.OperatorColorCode.okDefault,
                          PaytoScrollerOptionViewBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectionIndicatorColor: MyNumberTopup.OperatorColorCode.okDefault,
                          PaytoScrollerOptionUnselectedMenuItemLabelColor:UIColor.gray,
                          PaytoScrollerOptionMenuHeight: 50,
                          PaytoScrollerOptionEnableHorizontalBounce: false,
                          PaytoScrollerOptionMenuItemWidthBasedOnTitleTextWidth: true,
                          PaytoScrollerOptionAddBottomMenuHairline: true,
                          PaytoScrollerOptionMenuItemFont: "Zawgyi-One",
                          PaytoScrollerOptionBottomMenuHairlineColor: UIColor.lightGray] as [String : Any]
        
        paytoMenu = PaytoScroller(viewControllers: controllerArray, frame: self.view.bounds, options: parameters)
        
        paytoMenu?.delegate = self
        
        paytoMenu?.scrollingEnable(true)
        
        guard let viewMenu = paytoMenu else { return }
        self.view.addSubview(viewMenu.view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        paytoMenu?.selectedMenuItemLabelColor = MyNumberTopup.OperatorColorCode.okDefault
//        appDel.floatingButtonControl?.button.isHidden = true
//        appDel.floatingButtonControl?.closeButton.isHidden = true
        if selectedIndex == 2 || selectedIndex == 3 {
            self.delegate?.showInBoxRightBar(sttaus: true, getIndex: selectedIndex)
        } else {
            self.delegate?.showInBoxRightBar(sttaus: false, getIndex: selectedIndex)
        }
        
        self.navigationController?.navigationBar.backgroundColor = kYellowColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //self.setupTabViewControllers()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.paytoMenu?.selectedMenuItemLabelColor = .black
        }
    }
    
    //MARK:- Delegates for scrolling
    func willMove(toPage controller: UIViewController!, index: Int) {
        
        println_debug("Notification willMove----\(index)")
        println_debug("Notification didMove----\(self.controllerArray[index])")
        UserDefaults.standard.set("\(index)", forKey: "SearchButtonHideShow")

        selectedIndex = index
        if index == 2 || index == 3 {
            self.delegate?.showInBoxRightBar(sttaus: true, getIndex: index)
        }
        else  if index == 0 || index == 1 || index == 4 || index == 5 {
            self.delegate?.showInBoxRightBar(sttaus: false, getIndex: index)
        }
        
    }
    
    func didMove(toPage controller: UIViewController!, index: Int) {
        
        println_debug("Notification didMove----\(index)")
        println_debug("Notification didMove----\(self.controllerArray[index])")
        
        UserDefaults.standard.set("\(index)", forKey: "SearchButtonHideShow")
        
        if index == 2 || index == 3 {
            self.delegate?.showInBoxRightBar(sttaus: true, getIndex: index)
        }
        else if index == 0 || index == 1 || index == 4 || index == 5 {
            self.delegate?.showInBoxRightBar(sttaus: false, getIndex: index)
        }
        
        /*  if index == 3 {
         
         DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
         self.delegate?.showInBoxRightBar(sttaus: true, getIndex: index)
         });
         } */
        
        
        if  self.controllerArray[2].isEqual(controller) || self.controllerArray[3].isEqual(controller) || self.controllerArray[4].isEqual(controller) {
            print("Notification loader----\(checkloader)")
            if checkloader == true {
                checkloader = false
                DispatchQueue.main.async {
                    progressViewObj.showProgressView()
                }
                
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: languageKeyDashboard), object: nil)
    }


}


