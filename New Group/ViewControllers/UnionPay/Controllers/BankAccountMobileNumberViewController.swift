//
//  BankAccountMobileNumberViewController.swift
//  OK
//
//  Created by iMac on 05/09/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Contacts

protocol BankMobileNumberViewControllerDelegate {
   
    func changeColor(color: UIColor,name: String)
    func singleObjectPayment()
    func singleObjectWithCustomMsg(msg: String)
    func didShowAdvertisement(show: Bool)
    func showAddObjectButton(show: Bool)
}

class BankAccountMobileNumberViewController: UIViewController {
    @IBOutlet var suggestionTableHeight: NSLayoutConstraint!
    @IBOutlet var navigationView: UIView!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var navigationTitleLabel: UILabel!
    @IBOutlet var suggestionTable: UITableView!
    @IBOutlet var submitButton: UIButton!{
       didSet{
            //nextButton.setTitle(nextButton.titleLabel?.text?.localized, for: .normal)
             submitButton.titleLabel?.font = UitilityClass.getZwagiFontWithSize(size: appButtonSize)
             submitButton.setTitle("Submit".localized, for: .normal)
        }
    }
    @IBOutlet var fieldLabel: UILabel!{
        didSet {
            fieldLabel.font = UitilityClass.getZwagiFontWithSize(size: 20.0)
           // fieldLabel.text = "Select Transferer".localized
        }
    }
    @IBOutlet var userdetailFieldLabel: UILabel!{
        didSet {
           // userdetailFieldLabel.font = UitilityClass.getZwagiFontWithSize(size: 20.0)
            userdetailFieldLabel.text = "Enter Beneficiary Details".localized
        }
    }
    
    @IBOutlet var namechoosenfromLabel: UILabel!
    @IBOutlet var bankaccounttitleLabel: UILabel!
    
    @IBOutlet var accountSegement: UISegmentedControl!{
        didSet {
            accountSegement?.font(name: appFont, size: 15)
            accountSegement?.setTitle("My Self".localized, forSegmentAt: 0)
            accountSegement?.setTitle("Other Person".localized, forSegmentAt: 1)
        }
    }
    @IBOutlet var checkmarkButton: UIButton!
    @IBOutlet var navigationImageView: UIImageView!
    
    @IBOutlet var infoButton: UIButton!
    @IBOutlet var InfoLabel: UILabel!
    
    @IBOutlet var BankTititleLabel: UILabel!
    @IBOutlet var mobilefiledsView: UIView!
    @IBOutlet var MobileNumber: BankMobileRightViewTextField!
    
    @IBOutlet var userNameTF: BankMobileRightViewTextField!
    @IBOutlet var userNameTFHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var mobileFiledsViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var trnasferByLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet var segmentHeigthConstraint: NSLayoutConstraint!
    
    
    @IBOutlet var ChoosenFromLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet var contentViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var contentScroll: UIScrollView!
    
    //MARK: - Properties
       let mobileNumberView =  ContactRightView.updateView()
    
       // mobileNumberView.delegate = self
       private let mobileNumberAcceptableCharacters = "0123456789"
     private var mobileNumberLeftView    =  DefaultIconView.updateView(icon: "topup_number")
    var userLeftView    =  DefaultIconView.updateView(icon: "merchantIcon")
    
    var delegate: BankMobileNumberViewControllerDelegate?
     var contactsArray = [CNContact]()
     var confMobile = UIView()
    
    var projectid = ""
    var bankName = ""
    var updatedText = String()
    var parameters:[String : Any] = [:]
    var mobilenumFormate = ""
    var limit = 0
    var usertype = 0
    var persontype = 0
    var walletImage = ""
    var valueamount = ""
    var minimumvalueamount = ""
    
     var unchecked = true
    
    var selectstr = ""
    var myself = ""
    var otherperson = ""
    var fromFavorite = ""
    var fromContact = ""
    var selectOther = ""
    var walletArray = [Wallet]()
    var walletArrayCopy = [Wallet]()
    var tableViewHeight: CGFloat = 0
    var isComingFromContact = false
    
    var contactnameself = ""
    var contactnameother = ""
    
    var selectionDic = NSMutableDictionary()
    //From Recent Data
    var strStatus = ""
    var strMobileNo = ""
    var isComingFromRecent = false
    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationController?.navigationBar.isHidden = true
        // fieldLabel.text = "Enter \(bankName) Details".localized
        suggestionTable.isHidden = true
        self.suggestionTable.register(UINib(nibName: "SuggestionCell", bundle: nil), forCellReuseIdentifier: "BankSuggestionCell")
        self.suggestionTable.tableFooterView = UIView()
        
       
        getsuggestionArray()
        
        bankaccounttitleLabel.attributedText = self.returnAttributtedAmount()
        self.subscribeToShowKeyboardNotifications()
        
        if bankName == "MAB Bank"{
            let fullName    = bankName
            let fullNameArr = fullName.components(separatedBy: " ")

            let name    = fullNameArr[0]
         //   let surname = fullNameArr[1]
            navigationTitleLabel.text = name
            BankTititleLabel.text = "Myanma Apex Bank"
        }
        else{
            navigationTitleLabel.text = bankName
            BankTititleLabel.text = bankName
        }
    
        usertype = UserModel.shared.agentType.intValue
        if usertype == 6 {
           
            self.personalinitialUpdates()
            usertype = 0
            persontype = 1
        }
        else if usertype == 1 {
           
            usertype = 2
            self.initialUpdates()
            
        }
        else if usertype == 2 {
            usertype = 1
           
            self.initialUpdates()
        }
        
        if appDelegate.checkNetworkAvail(){
           
        }else{
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
                
            })
            alertViewObj.showAlert(controller:self)
        }
        
        if strStatus == "RecentTx"{
            MobileNumber.resignFirstResponder()
            bankDetailsAPI()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
       // self.contentViewHeightConstraint.constant = 750
    }
    
    
    func subscribeToShowKeyboardNotifications() {
           NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
           NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
       }
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            //            self.adImageConstraint.constant = 0 - keyboardHeight
            
            if device.type == .iPhoneX || device.type == .iPhoneXR || device.type == .iPhoneXS || device.type == .iPhoneXSMax || device.type == .iPhone11 || device.type == .iPhone11Pro || device.type == .iPhone11ProMax || device.type == .iPhone12 || device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                    if let value = self{
                       // value.contentScroll.isScrollEnabled = false
                        value.contentViewHeightConstraint.constant = 550
                    }
                    
                }
            }
            else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                    if let value = self{
                       // value.contentScroll.isScrollEnabled = true
                        value.contentViewHeightConstraint.constant = 600
                    }
                    
                }
            }
            
            
        })
    }
    @objc func keyboardWillHide(_ notification: Notification) {
        //        self.adImageConstraint.constant = 0
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            if let value = self{
               // value.contentScroll.isScrollEnabled = true
                value.contentViewHeightConstraint.constant = 750
            }
           
            
        }
        self.suggestionTable.isHidden = true
    }
    
    private func applySearch(with searchText: String) {
        walletArrayCopy.removeAll()
        let tempRecords = walletArray
        
        if searchText.count > 0 {
             //let newValue = searchText
            //let finalsearch = newValue.prefix(9).dropFirst(1)
            walletArrayCopy = tempRecords.filter({($0.number.lowercased().contains(find: searchText.lowercased()))})
        }
        
        if walletArrayCopy.count>0{
            suggestionTable.isHidden = false
            self.suggestionTable.reloadData()
        }else{
            suggestionTable.isHidden = true
        }
        

    }
        
    func getsuggestionArray(){
        do {
            if UserDefaults.standard.object(forKey: "WALLET") != nil{
                let storedObjItem = UserDefaults.standard.object(forKey: "WALLET")
                let storedItems = try JSONDecoder().decode([Wallet].self, from: storedObjItem as! Data)
                print(storedItems)
                walletArray.removeAll()
                for i in 0..<storedItems.count{
                    if projectid == storedItems[i].projectID{
                        walletArray.append(Wallet(number: storedItems[i].number, name: storedItems[i].name, projectID: storedItems[i].projectID))
                    }
                }
                if walletArray.count>0{
                    self.suggestionTable.delegate = self
                    self.suggestionTable.dataSource  = self
                    self.suggestionTable.reloadData()
    }
            }
        } catch let err {
            print(err)
        }
        
       // suggestionTable.isHidden = false

    }
    
    
    private func personalinitialUpdates() {
        self.MobileNumber.placeholder = "Please Enter \(bankName) Registered Number".localized
        MobileNumber.delegate = self
        mobileNumberView.delegate = self
        self.MobileNumber.rightViewMode = .always
        self.MobileNumber.rightView = mobileNumberView
        mobileNumberView.hideClearButton(hide: true)
        
        self.MobileNumber.leftViewMode = .always
        //        self.mobileNumber.editingRect(forBounds: CGRect(x: 58, y: 0, width: self.mobileNumber.frame.size.width - 185, height: 50))
        
       // selectstr = "0"
        
        mobilefiledsView.isHidden = false
        self.MobileNumber.leftView  = mobileNumberLeftView
      //  self.userNameTF.leftViewMode = .always
      //  self.userNameTF.leftView = userLeftView
        
        let urlFront = walletImage
        let imageUrl = URL(string: urlFront )
        self.navigationImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "bank"))
        
        //self.MobileNumber.text = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "0")
        
        if let text = self.MobileNumber.text, text.count < 3 {
            MobileNumber.text = "09"
            mobileNumberView.hideClearButton(hide: true)
            submitButton.isUserInteractionEnabled = false
            submitButton.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 178.0/255.0, alpha: 1.0)
            userdetailFieldLabel.text = "Enter Beneficiary Details".localized
            bankaccounttitleLabel.attributedText = self.returnAttributtedAmount()
        }
        else{
            mobileNumberView.hideClearButton(hide: false)
        }
        MobileNumber.becomeFirstResponder()
        namechoosenfromLabel.isHidden = true
        ChoosenFromLabelHeightConstraint.constant = 0
        fieldLabel.isHidden = true
        trnasferByLabelHeightConstraint.constant = 0
        
      
        
        accountSegement.isHidden = true
        segmentHeigthConstraint.constant = 0
        userNameTFHeightConstraint.constant = 0
        mobileFiledsViewHeightConstraint.constant = 220
        
        self.MobileNumber.addTarget(self, action: #selector(BankAccountMobileNumberViewController.textFieldDidChange(_:)),
                                    for: UIControl.Event.editingChanged)
        
        
    }
    
    func returnAttributtedAmount() -> NSAttributedString {

    let customAttribute1 = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 16) ?? UIFont.systemFont(ofSize: 16),
    NSAttributedString.Key.foregroundColor: UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)]
        let attributedAmountString = NSMutableAttributedString.init(string: "Account Number (Mobile)".localized, attributes: customAttribute1)

    let customAttribute2 = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 15) ?? UIFont.systemFont(ofSize: 15),
    NSAttributedString.Key.foregroundColor: UIColor.red ]
    let mmkString = NSMutableAttributedString.init(string: " * ", attributes: customAttribute2)
        
    let customAttribute3 = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 10) ?? UIFont.systemFont(ofSize: 10),
    NSAttributedString.Key.foregroundColor: UIColor.red ]
        let requireString = NSMutableAttributedString.init(string: "(Required)".localized, attributes: customAttribute3)

    attributedAmountString.append(mmkString)
    attributedAmountString.append(requireString)

    return attributedAmountString

    //self.amountTF.attributedPlaceholder = attributedAmountString
    }
    
    func returnAttributtedString() -> NSAttributedString {

    let customAttribute1 = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 16) ?? UIFont.systemFont(ofSize: 16),
    NSAttributedString.Key.foregroundColor: UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)]
        let attributedAmountString = NSMutableAttributedString.init(string: "Account Number (Mobile)".localized, attributes: customAttribute1)

        return attributedAmountString

    //self.amountTF.attributedPlaceholder = attributedAmountString
    }
    
    func returnAttributtedmerchantagent() -> NSAttributedString {

    let customAttribute1 = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 16) ?? UIFont.systemFont(ofSize: 16),
    NSAttributedString.Key.foregroundColor: UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)]
        let attributedAmountString = NSMutableAttributedString.init(string: "Select Transfer For".localized, attributes: customAttribute1)

    let customAttribute2 = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 15) ?? UIFont.systemFont(ofSize: 15),
    NSAttributedString.Key.foregroundColor: UIColor.red ]
    let mmkString = NSMutableAttributedString.init(string: " * ", attributes: customAttribute2)
        
    let customAttribute3 = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 10) ?? UIFont.systemFont(ofSize: 10),
    NSAttributedString.Key.foregroundColor: UIColor.red ]
        let requireString = NSMutableAttributedString.init(string: "(Required)".localized, attributes: customAttribute3)

    attributedAmountString.append(mmkString)
    attributedAmountString.append(requireString)

    return attributedAmountString

    //self.amountTF.attributedPlaceholder = attributedAmountString
    }
    
    
    private func initialUpdates() {
        self.MobileNumber.placeholder = "Please Enter \(bankName) Registered Number".localized
        MobileNumber.delegate = self
        mobileNumberView.delegate = self
        self.MobileNumber.rightViewMode = .always
        self.MobileNumber.rightView = mobileNumberView
        mobileNumberView.hideClearButton(hide: true)
        
        self.MobileNumber.leftViewMode = .always
        //        self.mobileNumber.editingRect(forBounds: CGRect(x: 58, y: 0, width: self.mobileNumber.frame.size.width - 185, height: 50))
        
       // selectstr = "0"
        
        mobilefiledsView.isHidden = true
        self.MobileNumber.leftView  = mobileNumberLeftView
      //  self.userNameTF.leftViewMode = .always
      //  self.userNameTF.leftView = userLeftView
        
        let urlFront = walletImage
        let imageUrl = URL(string: urlFront )
        self.navigationImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "bank"))
        
        self.MobileNumber.text = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "0")
        
        if let text = self.MobileNumber.text, text.count < 3 {
            MobileNumber.text = "09"
            mobileNumberView.hideClearButton(hide: true)
            submitButton.isUserInteractionEnabled = false
            submitButton.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 178.0/255.0, alpha: 1.0)
            userdetailFieldLabel.text = "Enter Beneficiary Details".localized
            bankaccounttitleLabel.attributedText = self.returnAttributtedAmount()
        }
        else{
            mobileNumberView.hideClearButton(hide: false)
        }
        fieldLabel.isHidden = false
        trnasferByLabelHeightConstraint.constant = 40
        fieldLabel.attributedText = self.returnAttributtedmerchantagent()
      //  userdetailFieldLabel.text = "Enter Beneficiary Details".localized
        namechoosenfromLabel.isHidden = true
        ChoosenFromLabelHeightConstraint.constant = 0
        segmentHeigthConstraint.constant = 45
        
     //   accountSegement?.setTitle("My Self".localized, forSegmentAt: 0)
    //    accountSegement?.setTitle("Other Person".localized, forSegmentAt: 1)
        
        accountSegement.setTitleTextAttributes( [NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        
        
        self.MobileNumber.addTarget(self, action: #selector(BankAccountMobileNumberViewController.textFieldDidChange(_:)),
                                    for: UIControl.Event.editingChanged)
        
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
          
      }
    
   
    func newActionSheet(){
        
//
//        let alert = UIAlertController(
//            title: "Choose From".localized,
//            message: "",
//            preferredStyle: .actionSheet)
        let alert:UIAlertController = UIAlertController(title: nil ,message: nil, preferredStyle: .actionSheet)
        
        let titleFont = [NSAttributedString.Key.font: UIFont(name: appFont, size: 15.0)!]
      
        
    //    let titleAttrString = NSMutableAttributedString(string: "Favorite".localized, attributes: titleFont)


     //   alert.setValue(titleAttrString, forKey:"attributedTitle")  //"attributedMessage"

        
   
        let title = UIAlertAction(
            title: "Chose From",
            style: .default,
            handler: { action in
                //Do some thing here
            })
        
        let online = UIAlertAction(
            title: "Favorite",
            style: .default,
            handler: { action in
                //Do some thing here
                
                let nav = self.openFavoriteFromNavigation(self, withFavorite: .payto, selectionType: true,  multiPay: false, payToCount: MultiTopupArray.main.controllers.count - 1)
                self.navigationController?.present(nav, animated: true, completion: nil)
                
                if self.usertype == 0 {
                    self.fromFavorite = "Fav"
                }
                else{
                    if self.selectstr == "0"{
                        self.selectionDic.setValue(false, forKey: "0")
                    }
                    else{
      
                        self.selectionDic.setValue(false, forKey: "1")
                    }
                }
                
                
               
                alert.dismiss(animated: true)
            })
        let offline = UIAlertAction(
            title: "Contacts",
            style: .default,
            handler: { action in
                let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
                nav.modalPresentationStyle = .fullScreen
                self.navigationController?.present(nav, animated: true, completion: nil)
                if self.usertype == 0 {
                    self.fromFavorite = "con"
                }
                else{
                    if self.selectstr == "0"{
                        self.selectionDic.setValue(true, forKey: "0")
                    }
                    else{
      
                        self.selectionDic.setValue(true, forKey: "1")
                    }
                }
               
                alert.dismiss(animated: true)
            })
        let cancel = UIAlertAction(
            title: "Cancel",
            style: .cancel,
            handler: { action in
            })
        
     //   alert.setValue(NSAttributedString(string: "Chose From", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15),NSAttributedString.Key.foregroundColor : UIColor.red]), forKey: "attributedTitle")
//        let font = [kCTFontAttributeName: UIFont(name: appFont, size: 22.0)!]
//        let buttontitleAttrString = NSMutableAttributedString(string: online.title!, attributes: font as [NSAttributedString.Key : Any])
//        online.setValue(buttontitleAttrString, forKey: "attributedTitleForAction")
     
        online.setValue(UIImage(named: "unionpayFav")?.withRenderingMode(.alwaysOriginal), forKey: "image")
        online.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        offline.setValue(UIImage(named: "unionpaycontact")?.withRenderingMode(.alwaysOriginal), forKey: "image")
        offline.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        
        
       
       // alert.setTitlet(font: UIFont.boldSystemFont(ofSize: 25), color: UIColor.red)
       
        
        alert.view.tintColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
        
      //  alert.applyContacts()
      //  alert.applyFavorites()
        alert.addAction(title)
        alert.addAction(online)
        alert.addAction(offline)
        alert.addAction(cancel)
        
      

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    func actionSheet() {
        let alert = UIAlertController(title: "Choose From", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Favorites", style: .default , handler:{ (UIAlertAction)in
            let nav = self.openFavoriteFromNavigation(self, withFavorite: .topup, selectionType: false,  multiPay: true, payToCount: MultiTopupArray.main.controllers.count - 1)
            self.navigationController?.present(nav, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Contacts", style: .default , handler:{ (UIAlertAction)in
        let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
        nav.modalPresentationStyle = .fullScreen
        self.navigationController?.present(nav, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func contact(contact: ContactPicker) {
       
        self.MobileNumber.text = "09"
        self.MobileNumber.becomeFirstResponder()
        
    }
    func decodeContact(ph: String,name: String, amount: String = "", hide: Bool = false) {
      
        var str = ""
        str = ph
        if str.count > 0 {
            str = str.replacingOccurrences(of: "(", with: "")
            str = str.replacingOccurrences(of: ")", with: "")
            str = str.trimmingCharacters(in: .whitespaces)
        }
        if str.hasPrefix("00") {
            _ = str.remove(at: String.Index.init(encodedOffset: 0))
            _ = str.remove(at: String.Index.init(encodedOffset: 0))
            str = "+" + str
        }
        let country = self.identifyCountry(withPhoneNumber: str)
        if country.0 == "+95" ||  country.1 == "myanmar" ||  country.0 == "" ||  country.1 == "" {
            let countryObject = Country.init(name: "", code: "myanmar", dialCode: "+95")
            if str.hasPrefix(countryObject.dialCode) {
                str = str.replacingOccurrences(of: countryObject.dialCode, with: "")
                str = "0" + str
            } else {
                if str.hasPrefix("0") {
                    
                } else {
                    str = "0" + str
                }
            }
           
            self.MobileNumber.text = str
            submitButton.isUserInteractionEnabled = true
            submitButton.backgroundColor = UIColor.systemYellow
           
            self.mobileNumberView.hideClearButton(hide: false)
            let rangeCheck = PayToValidations().getNumberRangeValidation(str)
        } else {
            self.delegate?.showAddObjectButton(show: false)
            guard let keyWinndow = UIApplication.shared.keyWindow else { return }
            keyWinndow.makeToast("You Selected Other Country Number".localized, point: keyWinndow.center, title: nil, image: nil, completion: nil)
        }
    }
    
    func identifyCountry(withPhoneNumber prefix: String) -> (String, String) {
           let countArray = revertCountryArray() as! Array<NSDictionary>
           var finalCodeString = ""
           var flagCountry = ""
           for dic in countArray {
               if let code = dic .object(forKey: "CountryCode") as? String, let flag = dic.object(forKey: "CountryFlagCode") as? String{
                   if prefix.hasPrefix(code) {
                       finalCodeString = code
                       flagCountry = flag
                   }
               }
           }
           return (finalCodeString,flagCountry)
       }
    func revertCountryArray() -> NSArray {
        do {
            if let file = Bundle.main.url(forResource: "mcc_mnc", withExtension: "txt") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? NSDictionary {
                    if let arr = object.object(forKey: "data") as? NSArray {
                        arrCountry = arr
                        return arrCountry
                    }
                } else if let object = json as? [Any] {
                    println_debug(object)
                } else {
                    println_debug("JSON is invalid")
                }
            } else {
                println_debug("no file")
            }
        } catch {
            println_debug(error.localizedDescription)
        }
        return arrCountry
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func infoButtonClick(_ sender: Any) {
        
        alertViewObj.wrapAlert(title: "OK$", body: "You will earn comission as Cash Back", img: nil)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        alertViewObj.showAlert(controller: self)
        
    }
    
    @IBAction func accounsegmentClick(_ sender: Any) {
        fieldLabel.text = "Transfer For".localized
        if accountSegement.selectedSegmentIndex == 0{
            persontype = 1
            selectOther = "myself"
            selectstr = "0"
            if myself == ""{
               self.MobileNumber.text = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "0")
                submitButton.isUserInteractionEnabled = true
                submitButton.backgroundColor = UIColor.systemYellow
                namechoosenfromLabel.isHidden = true
                ChoosenFromLabelHeightConstraint.constant = 0
                userNameTFHeightConstraint.constant = 0
                mobileFiledsViewHeightConstraint.constant = 220
                self.userNameTF.leftViewMode = .always
                self.userNameTF.leftView = nil
                self.userNameTF.text = ""
                userdetailFieldLabel.text = "Beneficiary Details".localized
                bankaccounttitleLabel.attributedText = self.returnAttributtedString()
            }
            else{
                self.MobileNumber.text = myself
                submitButton.isUserInteractionEnabled = true
                submitButton.backgroundColor = UIColor.systemYellow
                userdetailFieldLabel.text = "Beneficiary Details".localized
                bankaccounttitleLabel.attributedText = self.returnAttributtedString()
                if contactnameself == ""{
                    namechoosenfromLabel.isHidden = true
                    ChoosenFromLabelHeightConstraint.constant = 0
                    userNameTFHeightConstraint.constant = 0
                    mobileFiledsViewHeightConstraint.constant = 220
                    self.userNameTF.leftViewMode = .always
                    self.userNameTF.leftView = nil
                    self.userNameTF.text = ""
                }
                else{
                    self.userNameTF.text = contactnameself
                    namechoosenfromLabel.isHidden = false
                    ChoosenFromLabelHeightConstraint.constant = 23.33
                    userNameTFHeightConstraint.constant = 50
                    mobileFiledsViewHeightConstraint.constant = 265
                    self.userNameTF.leftViewMode = .always
                    self.userNameTF.leftView = userLeftView

                    
                    let va = selectionDic.value(forKey: selectstr) as! Bool
                    
                    if va{
                        namechoosenfromLabel.text = "Name From Contact".localized
                    }else{
                        namechoosenfromLabel.text = "Name From Favorite".localized
                    }
                }
            }

            if let text = self.MobileNumber.text, text.count < 3 {
                MobileNumber.text = "09"
                mobileNumberView.hideClearButton(hide: true)
                submitButton.isUserInteractionEnabled = false
                submitButton.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 178.0/255.0, alpha: 1.0)
                userdetailFieldLabel.text = "Enter Beneficiary Details".localized
                bankaccounttitleLabel.attributedText = self.returnAttributtedAmount()
            }
            else{
                mobileNumberView.hideClearButton(hide: false)
            }
            
            
//            self.checkmarkButton.setImage( UIImage(named:"check_box_Blue"), for: .normal)
//             self.InfoLabel.textColor = UIColor.darkGray
//            unchecked = true
        }
        else
        {
            MobileNumber.becomeFirstResponder()
            persontype = 0
            selectOther = "other"
             selectstr = "1"
            if otherperson == ""{
                 MobileNumber.text = "09"
                submitButton.isUserInteractionEnabled = false
                submitButton.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 178.0/255.0, alpha: 1.0)
                namechoosenfromLabel.isHidden = true
                ChoosenFromLabelHeightConstraint.constant = 0
                userNameTFHeightConstraint.constant = 0
                mobileFiledsViewHeightConstraint.constant = 220
                self.userNameTF.leftViewMode = .always
                self.userNameTF.leftView = nil
                self.userNameTF.text = ""
                userdetailFieldLabel.text = "Enter Beneficiary Details".localized
                bankaccounttitleLabel.attributedText = self.returnAttributtedAmount()
            }
            else{
               self.MobileNumber.text = otherperson
                submitButton.isUserInteractionEnabled = true
                submitButton.backgroundColor = UIColor.systemYellow
                userdetailFieldLabel.text = "Beneficiary Details".localized
                bankaccounttitleLabel.attributedText = self.returnAttributtedString()
                if contactnameother == ""{
                    namechoosenfromLabel.isHidden = true
                    ChoosenFromLabelHeightConstraint.constant = 0
                    userNameTFHeightConstraint.constant = 0
                    mobileFiledsViewHeightConstraint.constant = 220
                    self.userNameTF.leftViewMode = .always
                    self.userNameTF.leftView = nil
                    self.userNameTF.text = ""
                }
                else{
                    self.userNameTF.text = contactnameother
                    namechoosenfromLabel.isHidden = false
                    ChoosenFromLabelHeightConstraint.constant = 23.33
                    userNameTFHeightConstraint.constant = 50
                    mobileFiledsViewHeightConstraint.constant = 265
                    self.userNameTF.leftViewMode = .always
                    self.userNameTF.leftView = userLeftView

                    let va = selectionDic.value(forKey: selectstr) as! Bool
                    
                    if va{
                        namechoosenfromLabel.text = "Name From Contact".localized
                    }else{
                        namechoosenfromLabel.text = "Name From Favorite".localized
                    }

                }
                
            }
            
            if let text = self.MobileNumber.text, text.count < 3 {
                //MobileNumber.text = "09"
                mobileNumberView.hideClearButton(hide: true)
                submitButton.isUserInteractionEnabled = false
                submitButton.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 178.0/255.0, alpha: 1.0)
                userdetailFieldLabel.text = "Enter Beneficiary Details".localized
                bankaccounttitleLabel.attributedText = self.returnAttributtedAmount()
            }
            else{
                mobileNumberView.hideClearButton(hide: false)
            }
           
//            self.checkmarkButton.setImage(UIImage(named:"checkboxTick"), for: .normal)
//             self.InfoLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
//            unchecked = false
            
        }
        mobilefiledsView.isHidden = false
        
        
    }
//    @IBAction func checkMarkClick(_ sender: Any) {
//       // checkboxTick
//      // check_box_Blue
//        if unchecked {
//            (sender as AnyObject).setImage(UIImage(named:"checkboxTick"), for: .normal)
//            self.InfoLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
//            unchecked = false
//        }
//        else {
//            (sender as AnyObject).setImage( UIImage(named:"check_box_Blue"), for: .normal)
//            self.InfoLabel.textColor = UIColor.darkGray
//            unchecked = true
//        }
//
//    }
    
    
    @IBAction func submitClick(_ sender: Any) {
        if MobileNumber.text == "09" || MobileNumber.text == "" {
            alertViewObj.wrapAlert(title: "OK$", body: "Enter Mobile Number".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        }
        else{
            MobileNumber.resignFirstResponder()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                if self?.MobileNumber.text == "09" || self?.MobileNumber.text == "" {
                    alertViewObj.wrapAlert(title: "OK$", body: "Enter Mobile Number".localized, img: #imageLiteral(resourceName: "alert-icon"))
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    })
                    alertViewObj.showAlert(controller: self)
                }
                else{
                    self?.apicall()
                }
               
            }
        }
     
    }
    
    func apicall(){
        progressViewObj.showProgressView()
        let apiReqObj = ApiRequestClass()
        apiReqObj.customDelegate = self
          parameters = [
            "BankAccountNumber": MobileNumber.text ?? "", //"0830131083000106018",
            "OKNumber": UserModel.shared.mobileNo, //"00959966646970",//mobilenumFormate,
            "ProjectId": projectid,
            "UserType": usertype,//UserModel.shared.agentType.intValue,
            "Channel": "0" ,//"1"
            "OSType": UserModel.shared.osType,
            "_v":"V2"
            ]
        
         print("*****",parameters)
       // http://api.sandbox.okdollarapp.com/okdollar/v1/Wallet/VerifyWalletNumber
        let bankverifywalletnumurl = getUrl(urlStr:Url.bankVerifyWalletNumber , serverType: .UnionPayBankDetailsapi)
           DispatchQueue.main.async{
            apiReqObj.sendHttpRequest(requestUrl: bankverifywalletnumurl, requestData: self.parameters, requestType: RequestTypeObj.RequestTypeBankAccount, httpMethodName: "POST")
        }
    }
    
    func bankDetailsAPI(){
        
        progressViewObj.showProgressView()
        
        let apiReqObj = ApiRequestClass()
        apiReqObj.customDelegate = self
        let bankdetailsurl = getUrl(urlStr:Url.bankdetailsUrl , serverType: .UnionPayBankDetailsapi)
        DispatchQueue.main.async{
            apiReqObj.sendHttpRequest(requestUrl: bankdetailsurl, requestData: "", requestType: RequestTypeObj.RequestTypeBankDetails, httpMethodName: "GET")
        }
    }
        
}

extension BankAccountMobileNumberViewController: ApiRequestProtocol{
        func httpResponse(responseObj: Any, reqType: RequestTypeObj) {
            DispatchQueue.main.async {
                progressViewObj.removeProgressView()
                if let respMsg = responseObj as? String {
                    alertViewObj.wrapAlert(title: "OK$", body: respMsg, img: nil)
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    })
                    alertViewObj.showAlert(controller: self)
                }else {
                   // var respDict = [String:Any]()
    //                if responseObj is [Any] {
    //                    // self.tokenarray = responseObj as! [Any]
    //                } else if responseObj is [String:Any] {
    //                    respDict = responseObj as! Dictionary<String,Any>
    //                }
                    var respDict = [String:Any]()
                    if let resposneDataFromServer = responseObj as? [String : Any] {
                        respDict = resposneDataFromServer
                    }
                    if reqType == RequestTypeObj.RequestTypeBankAccount {
                        if respDict.count > 0 {
                            
                            print ("Response", respDict)
                            let code = respDict["code"] as? Int
                            print("#####",code as Any)
                            
                            if let isValidCode = code {
                                if isValidCode == 200 {
                                   
                                    DispatchQueue.main.async {
                                        var homeVC: BankAccountViewController?
                                        homeVC = UIStoryboard(name: "UnionPay", bundle: nil).instantiateViewController(withIdentifier: "BankAccountViewController") as? BankAccountViewController
                                        homeVC!.projectid = self.projectid
                                        homeVC!.bankName = self.bankName
                                        //  homeVC!.limitdict = limitDict
                                        homeVC!.walletImage = self.walletImage
                                        homeVC!.mobilenumdisplay = self.MobileNumber.text ?? ""
                                        homeVC!.selectedother = self.selectOther
                                        homeVC!.persontype = self.persontype
                                        //            userDef.set(banklogo, forKey: "UnionPayLogo")
                                        //            userDef.synchronize()
                                        
                                        if let homeVC = homeVC {
                                            self.navigationController?.pushViewController(homeVC, animated: true)
                                        }
                                    }
                                
                                    print ("&&&&&",respDict["data"] ?? "")
                                }else if isValidCode == 302 {
                                    alertViewObj.wrapAlert(title: "", body: "Number not registered in \(self.bankName)".localized, img: #imageLiteral(resourceName: "alert-icon"))
                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                    self.MobileNumber.text = ""
                                    self.submitButton.isUserInteractionEnabled = false
                                    self.submitButton.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 178.0/255.0, alpha: 1.0)
                                   // self.fieldLabel.isHidden = true
                                    self.MobileNumber.becomeFirstResponder()
                                    
                                })
                                    alertViewObj.showAlert(controller: self)
                                }
                                else {
                                    alertViewObj.wrapAlert(title: "", body: (respDict["message"] as? String) ?? "", img: #imageLiteral(resourceName: "alert-icon"))
                                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                        self.MobileNumber.text = ""
                                        self.submitButton.isUserInteractionEnabled = false
                                        self.submitButton.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 178.0/255.0, alpha: 1.0)
                                       // self.fieldLabel.isHidden = true
                                        self.MobileNumber.becomeFirstResponder()
                                    })
                                    alertViewObj.showAlert(controller: self)
                                }
                            }else {
                                alertViewObj.wrapAlert(title: "", body: (respDict["message"] as? String) ?? "", img: #imageLiteral(resourceName: "alert-icon"))
                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                })
                                alertViewObj.showAlert(controller: self)
                            }
                        }else {
                            alertViewObj.wrapAlert(title: "", body: (respDict["message"] as? String) ?? "", img: #imageLiteral(resourceName: "alert-icon"))
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                            })
                            alertViewObj.showAlert(controller: self)
                        }
                    }
                    if reqType == RequestTypeObj.RequestTypeBankDetails {
                                        if respDict.count > 0
                                        {
                                            let code = respDict["code"] as? Int
                                            if let isValidCode = code {
                                                if isValidCode == 200 {
                                                    var bankDetailsArray = [[String : Any]]()
                                                    if respDict.count>0{
                                                        
                                                        if let isValidData = respDict["data"] as? [[String : Any]]{
                                                            bankDetailsArray = isValidData
                                                        }
                                                        if bankDetailsArray.count>0{
                                                            for i in 0..<bankDetailsArray.count{
                                                                if bankDetailsArray.indices.contains(i){
                                                                    if let validID = bankDetailsArray[i]["ProjectId"] as? String{
                                                                        if self.projectid == validID{
                                                                            self.walletImage = bankDetailsArray[i]["Logo"] as? String ?? ""
                                                                            self.projectid = bankDetailsArray[i]["ProjectId"] as? String ?? ""
                                                                            self.bankName = bankDetailsArray[i]["Name"] as? String ?? ""
                                                                            self.MobileNumber.text = self.strMobileNo

                                                                            self.submitButton.isUserInteractionEnabled = true
                                                                            self.submitButton.backgroundColor = UIColor.systemYellow
                                                                            //self.setUI()
                                                                            if self.bankName == "MAB Bank"{
                                                                                let fullName    = self.bankName
                                                                                       let fullNameArr = fullName.components(separatedBy: " ")

                                                                                       let name    = fullNameArr[0]
                                                                                self.navigationTitleLabel.text = name
                                                                                self.BankTititleLabel.text = "Myanma Apex Bank"
                                                                                   }
                                                                                   else{
                                                                                self.navigationTitleLabel.text = self.bankName
                                                                                self.BankTititleLabel.text = self.bankName
                                                                                   }
                                                                            self.navigationImageView.sd_setImage(with: URL(string: bankDetailsArray[i]["Logo"] as? String ?? ""), placeholderImage: UIImage(named: "bank"))
                                                                            break
                                                                        }
                                                                        
                                                                    }
                                                                    
                    //
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    alertViewObj.wrapAlert(title: "", body: (respDict["message"] as? String) ?? "", img: #imageLiteral(resourceName: "alert-icon"))
                                                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                                    })
                                                    alertViewObj.showAlert(controller: self)
                                                }
                                            }
                                            else
                                            {
                                                alertViewObj.wrapAlert(title: "", body: (respDict["message"] as? String) ?? "", img: #imageLiteral(resourceName: "alert-icon"))
                                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                                })
                                                alertViewObj.showAlert(controller: self)
                                            }
                                            
                                            
                                        }
                                        else
                                        {
                                            alertViewObj.wrapAlert(title: "", body: (respDict["message"] as? String) ?? "", img: #imageLiteral(resourceName: "alert-icon"))
                                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                            })
                                            alertViewObj.showAlert(controller: self)
                                        }
                                    }
                }
            }
          }
}

//MARK: - PTClearButtonDelegate
extension BankAccountMobileNumberViewController: PTClearButtonDelegate {
    func clearField(strTag: String) {
        if strTag.hasPrefix("confMobile") {
            self.delegate?.showAddObjectButton(show: false)
//            if let cnfView = confimMobile.rightView as? PTClearButton {
//                cnfView.hideClearButton(hide: true)
//            }
        }
    }
}

extension BankAccountMobileNumberViewController: UITextFieldDelegate {
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let rangeCheck = PayToValidations().getNumberRangeValidation(text)
            if !text.hasPrefix("09") {
                return false
            }
            if rangeCheck.isRejected {
                textField.text = "09"
               
                mobileNumberView.hideClearButton(hide: true)
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                return false
            }
            
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    self.delegate?.showAddObjectButton(show: false)
                    if textField == self.MobileNumber {
                        if textField.text == "09" {
                            println_debug(textField.text)
                            return false
                        }
                        if text == "09" {
                            suggestionTable.isHidden = true
                            mobileNumberView.hideClearButton(hide: true)
                            submitButton.isUserInteractionEnabled = false
                            submitButton.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 178.0/255.0, alpha: 1.0)
                            namechoosenfromLabel.isHidden = true
                            ChoosenFromLabelHeightConstraint.constant = 0
                            self.userNameTF.leftView = nil
                            userNameTFHeightConstraint.constant = 0
                            mobileFiledsViewHeightConstraint.constant = 220
                            userdetailFieldLabel.text = "Enter Beneficiary Details".localized
                            bankaccounttitleLabel.attributedText = self.returnAttributtedAmount()
                        }else{
                            
                            applySearch(with: text)
                           // suggestionTable.isHidden = false
                            namechoosenfromLabel.isHidden = true
                            ChoosenFromLabelHeightConstraint.constant = 0
                            self.userNameTF.leftView = nil
                            userNameTFHeightConstraint.constant = 0
                            mobileFiledsViewHeightConstraint.constant = 220
                        }
                        if text.count < rangeCheck.min {
                            
                            self.submitButton.isUserInteractionEnabled = false
                            submitButton.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 178.0/255.0, alpha: 1.0)
                            namechoosenfromLabel.isHidden = true
                            ChoosenFromLabelHeightConstraint.constant = 0
                            self.userNameTF.leftView = nil
                            userNameTFHeightConstraint.constant = 0
                            mobileFiledsViewHeightConstraint.constant = 220
                            userdetailFieldLabel.text = "Enter Beneficiary Details".localized
                            bankaccounttitleLabel.attributedText = self.returnAttributtedAmount()
                        }
                        if text.count < 3 {
                            
                        }
                        
                        
                        self.MobileNumber.text = text
                        return false
                    }
                }
            }
            
            if textField == self.MobileNumber {
                
               // suggestionTable.isHidden = false
               // let removedigits = textField.text?.replacingOccurrences(of: "09", with: "")
                
                let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: mobileNumberAcceptableCharacters).inverted
                let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
                
                if string != filteredSet { return false }
             
                
//                if text.count < rangeCheck.min {
//                    self.submitButton.isUserInteractionEnabled = false
//                    submitButton.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 178.0/255.0, alpha: 1.0)
//                } else if (text.count >= rangeCheck.min && text.count < rangeCheck.max) {
//                    self.submitButton.isUserInteractionEnabled = true
//                    submitButton.backgroundColor = UIColor.systemYellow
//
//                } else if (text.count == rangeCheck.max) {
//                    textField.text = text
//
//                    submitButton.isUserInteractionEnabled = true
//                    submitButton.backgroundColor = UIColor.systemYellow
//
//                    return false
//                }
                
                
                if text.count > 2 {
                    applySearch(with: text)
                    mobileNumberView.hideClearButton(hide: false)
                }else {
                    submitButton.isUserInteractionEnabled = false
                    submitButton.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 178.0/255.0, alpha: 1.0)
                    mobileNumberView.hideClearButton(hide: true)
                    namechoosenfromLabel.isHidden = true
                    ChoosenFromLabelHeightConstraint.constant = 0
                    self.userNameTF.leftView = nil
                    userNameTFHeightConstraint.constant = 0
                    mobileFiledsViewHeightConstraint.constant = 220
                    userdetailFieldLabel.text = "Enter Beneficiary Details".localized
                    bankaccounttitleLabel.attributedText = self.returnAttributtedAmount()
                }
                
              
                
                if text.count > 3 {
                   
                } else {
                   
                }
                
              
                if text.count > rangeCheck.max {
                    self.MobileNumber.resignFirstResponder()
                    userdetailFieldLabel.text = "Beneficiary Details".localized
                    bankaccounttitleLabel.attributedText = self.returnAttributtedString()
                    suggestionTable.isHidden = true
                    return false
                }
                
                
                if text.count >= rangeCheck.min {
    //                self.amountConst.constant = 0.00
    //                self.bonusPointHeightConstraints.constant = 0.0
    //                self.amount.isHidden = true
    //                self.bonusPointTextField.isHidden = true
    //                if let cnfRightView = self.confimMobile.rightView as? PTClearButton {
    //                    cnfRightView.hideClearButton(hide: true)
    //                }
                    
                    self.delegate?.showAddObjectButton(show: true)
                  
    //                if self.confimMobile.isHidden {
    //                    self.contactSuggestionHeightConstraint.constant = 0
    //                    self.confirmMobConst.constant = 50.00
    //                    self.animateView {
    //                        self.confimMobile.isHidden = false
    //                    }
    //                    self.delegate?.scrollEnable(show: true)
    //                    self.delegate?.showAddObjectButton(show: false)
    //                    self.delegate?.updateHeightTableView()
    //                }
                    if text.count == rangeCheck.max {
                        if selectstr == "0"{
                            myself = text
                        }
                        else
                        {
                            otherperson = text
                        }
                        
                        self.MobileNumber.text = text
                        submitButton.isUserInteractionEnabled = true
                        submitButton.backgroundColor = UIColor.systemYellow
                       
    //                    self.confimMobile.becomeFirstResponder()
                        self.MobileNumber.resignFirstResponder()
                        userdetailFieldLabel.text = "Beneficiary Details".localized
                        bankaccounttitleLabel.attributedText = self.returnAttributtedString()
                        suggestionTable.isHidden = true
                        return false
                    } else if text.count > rangeCheck.max {
                        self.MobileNumber.resignFirstResponder()
                        userdetailFieldLabel.text = "Beneficiary Details".localized
                        bankaccounttitleLabel.attributedText = self.returnAttributtedString()
                        suggestionTable.isHidden = true
                        return false
                    }
                    self.MobileNumber.text = text
                    submitButton.isUserInteractionEnabled = true
                    submitButton.backgroundColor = UIColor.systemYellow
                    return false
                } else {
    //                self.confimMobile.text = ""
    //                self.amount.text = ""
    //                self.amount.rightView = nil
    //                self.confimMobile.isUserInteractionEnabled = true
    //                self.confirmMobConst.constant = 0.00
    //                self.amountConst.constant = 0.00
    //                self.bonusPointHeightConstraints.constant = 0.0
    //                self.confimMobile.isHidden = true
    //                self.amount.isHidden = true
    //                self.bonusPointTextField.isHidden = true
    //                self.heightChanges = true
                    
                    self.delegate?.showAddObjectButton(show: false)
                }
            }
            return true
        }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.MobileNumber {
            if textField == self.MobileNumber {
                if let text = self.MobileNumber.text, text.count < 3 {
                    textField.text = "09"
                    mobileNumberView.hideClearButton(hide: true)
                    namechoosenfromLabel.isHidden = true
                    ChoosenFromLabelHeightConstraint.constant = 0
                    self.userNameTF.leftView = nil
                    userNameTFHeightConstraint.constant = 0
                    mobileFiledsViewHeightConstraint.constant = 220
                 //   userdetailFieldLabel.text = "Enter Beneficiary Details".localized
                }
                else{
                    mobileNumberView.hideClearButton(hide: false)
                //    userdetailFieldLabel.text = "Beneficiary Details".localized
                }
            }
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == self.MobileNumber {
            //mobileNumberView.hideClearButton(hide: true)
            mobileNumberView.hideClearButton(hide: true)
        }
        return true
    }
}


//MARK: - CountryLeftViewDelegate
extension BankAccountMobileNumberViewController: CountryLeftViewDelegate {
    func openAction(type: ButtonType) {
        self.view.endEditing(true)
        switch type {
        case .countryView:
            break
        case .contactView:
           // PTManagerClass.openMultiContactSelection(self, hideScan: true)
         //   self.actionSheet()
            self.newActionSheet()
        }
    }
    
    func clearActionType() {
        
        if   selectstr == "0" {
             myself = ""
            contactnameself = ""
        }
        else{
            otherperson = ""
            contactnameother = ""
        }
        suggestionTable.isHidden  = true
        self.MobileNumber.text = "09"
        submitButton.isUserInteractionEnabled = false
        submitButton.backgroundColor = UIColor(red: 174.0/255.0, green: 174.0/255.0, blue: 178.0/255.0, alpha: 1.0)
        userdetailFieldLabel.text = "Enter Beneficiary Details".localized
        bankaccounttitleLabel.attributedText = self.returnAttributtedAmount()
        self.MobileNumber.becomeFirstResponder()
        
     //   self.delegate?.changeColor(color: MyNumberTopup.OperatorColorCode.okDefault, name: "")
    //    self.delegate?.showAddObjectButton(show: false)
       
        mobileNumberView.hideClearButton(hide: true)
        namechoosenfromLabel.isHidden = true
        ChoosenFromLabelHeightConstraint.constant = 0
        self.userNameTF.leftView = nil
        userNameTFHeightConstraint.constant = 0
        mobileFiledsViewHeightConstraint.constant = 220
    }
}



//MARK: - PTMultiSelectionDelegate
extension BankAccountMobileNumberViewController: PTMultiSelectionDelegate {
    func didSelectOption(option: PTMultiSelectionOption) {
        switch option {
        case .favorite:
            let nav = openFavoriteFromNavigation(self, withFavorite: .payto, selectionType: false,  multiPay: true, payToCount: MultiTopupArray.main.controllers.count - 1)
            self.navigationController?.present(nav, animated: true, completion: nil)

            
        case .contact:
            
            let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: true)
         //   let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false)
            self.navigationController?.present(nav, animated: true, completion: nil)
        case .scanQR:
            break
        }
    }
    
    func openFavoriteFromNavigation(_ delegate: FavoriteSelectionDelegate, withFavorite type: FavCaseSelection, selectionType: Bool = false, modelPayTo : PayToUIViewController? = nil, multiPay: Bool? = false, payToCount: Int? = 0) -> UINavigationController {
        let navigation = UIStoryboard(name: "Favorite", bundle: nil).instantiateViewController(withIdentifier: "favoriteNav") as! UINavigationController
        navigation.modalPresentationStyle = .fullScreen
        for controllers in navigation.viewControllers {
            if let fav = controllers as? FavoriteViewController {
                fav.isFromModules  = true
                fav.directSegue    = type
                fav.moduleDelegate = delegate
                fav.singleSelection = selectionType
                fav.modelPayTo = modelPayTo
                fav.multiPay = multiPay ?? false
                fav.payToCount = payToCount ?? 0
            }
        }
        return navigation
    }
    
}

//MARK: - FavoriteSelectionDelegate
extension BankAccountMobileNumberViewController: FavoriteSelectionDelegate {
    func selectedFavoriteObject(obj: FavModel) {
        let contact = ContactPicker.init(object: obj)
        self.contact(ContactPickersPicker(), didSelectContact: contact)
    }
}

class BankMobileRightViewTextField: SearchTextFieldTopUp {
    var cnf = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
       super.init(coder: coder)
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 0, y: 3, width:  58 , height: 44)
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: (cnf) ? (self.frame.size.width - 42) : (self.frame.size.width - 82), y: 0, width: (cnf) ? 42 : 82 , height: 50)
    }
    override func closestPosition(to point: CGPoint) -> UITextPosition? {
              let beginning = self.beginningOfDocument
              let end = self.position(from: beginning, offset: self.text?.count ?? 0)
              return end
          }
    
//    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
//         if action == #selector(paste(_:)) || action == #selector(cut(_:)) || action == #selector(copy(_:)) || action == #selector(select(_:)) || action == #selector(selectAll(_:)) {
//             return false
//         }
//         return super.canPerformAction(action, withSender: sender)
//     }
    
    override public func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}

extension UIAlertController {
    func setTitlet(font: UIFont?, color: UIColor?) {
         guard let title = self.title else { return }
         let attributeString = NSMutableAttributedString(string: title)//1
         if let titleFont = font {
             attributeString.addAttributes([NSAttributedString.Key.font : titleFont],//2
                                           range: NSMakeRange(0, title.utf8.count))
         }
         
         if let titleColor = color {
             attributeString.addAttributes([NSAttributedString.Key.foregroundColor : titleColor],//3
                                           range: NSMakeRange(0, title.utf8.count))
         }
         self.setValue(attributeString, forKey: "attributedTitle")//4
     }
    
    func applyFavorites() {
          let font = [kCTFontAttributeName: UIFont(name: appFont, size: 22.0)!]
          for actionButton in actions {
              let titleAttrString = NSMutableAttributedString(string: actionButton.title!, attributes: font as [NSAttributedString.Key : Any])
              actionButton.setValue(titleAttrString, forKey: "attributedTitleForAction")
          }
      }
      
      func applyContacts() {
        let font = [kCTFontAttributeName: UIFont(name: appFont, size: 22.0)!]
          for actionButton in actions {
              let titleAttrString = NSMutableAttributedString(string: actionButton.title!, attributes: font as [NSAttributedString.Key : Any])
              actionButton.setValue(titleAttrString, forKey: "attributedTitleForAction")
          }
      }
    
    
}

extension UISegmentedControl {
    func font(name:String?, size:CGFloat?) {
        let attributedSegmentFont = NSDictionary(object: UIFont(name: name!, size: size!)!, forKey: NSAttributedString.Key.font as NSCopying)
        setTitleTextAttributes(attributedSegmentFont as [NSObject : AnyObject] as [NSObject : AnyObject] as? [NSAttributedString.Key : Any], for: .normal)
    }
}


extension BankAccountMobileNumberViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if walletArrayCopy.count>0{
            return walletArrayCopy.count
        }else{
            return walletArray.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BankSuggestionCell") as? BankSuggestionCell else{
            return UITableViewCell()
        }
        
        if walletArrayCopy.count>0{
            cell.mobileLabel.text = walletArrayCopy[indexPath.row].name
            cell.nameLabel.text = walletArrayCopy[indexPath.row].number
        }else{
            cell.mobileLabel.text = walletArray[indexPath.row].name
            cell.nameLabel.text = walletArray[indexPath.row].number
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return suggestionTable.estimatedRowHeight
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.tableViewHeight += cell.frame.size.height
        suggestionTableHeight.constant = self.tableViewHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if walletArrayCopy.count>0{
            MobileNumber.text = walletArrayCopy[indexPath.row].number
        }else{
            MobileNumber.text = walletArray[indexPath.row].number
        }
        MobileNumber.resignFirstResponder()
        userdetailFieldLabel.text = "Beneficiary Details".localized
        bankaccounttitleLabel.attributedText = self.returnAttributtedString()
        submitButton.isUserInteractionEnabled = true
        submitButton.backgroundColor = UIColor.systemYellow
        suggestionTable.isHidden  = true
    }

    
}
