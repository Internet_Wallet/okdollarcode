//
//  BankDetailsTableViewCell.swift
//  OK
//
//  Created by vamsi on 7/27/20.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class BankDetailsTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet var bankNameLAbel: UILabel!
    @IBOutlet var bankImageView: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
