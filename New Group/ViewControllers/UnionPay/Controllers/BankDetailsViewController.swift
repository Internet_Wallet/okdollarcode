//
//  BankDetailsViewController.swift
//  OK
//
//  Created by vamsi on 7/27/20.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit
import Reachability

class BankDetailsViewController: UIViewController, ApiRequestProtocol {
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet var navigationTitleLabel: UILabel! {
        didSet {
             navigationTitleLabel.font = UitilityClass.getZwagiFontWithSize(size: 17.0)
            navigationTitleLabel.text = "Mobile Wallet | Mobile Pay and Bank".localized //"Select Wallet".localized
        }
    }
    @IBOutlet var backButton: UIButton!
    @IBOutlet var bankdetailsTableView: UITableView!
    
    @IBOutlet var selectwalletserivceLabel: UILabel! {
        didSet {
            selectwalletserivceLabel.font = UitilityClass.getZwagiFontWithSize(size: 17.0)
            selectwalletserivceLabel.text = "     Select Wallet".localized
        }
    }
    
    var bankDetailsArray = [[String : Any]]()
     var banklogos = [Any]()
     var limitDict = [String : Any]()
    
    var netcheck = Utilities()
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
        self.bankdetailsTableView.tableFooterView = UIView()
        
        banklogos = ["CBBank","MABBank","UABBank"]
        
        if appDelegate.checkNetworkAvail(){
            print("Internet Connection Available!")
            apicall()
        }else{
            print("Internet Connection not Available!")
           alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
           alertViewObj.addAction(title: "OK".localized, style: .target , action: {
              
               
           })
           alertViewObj.showAlert(controller:self)
        }
        
        
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    func apicall(){
           
         progressViewObj.showProgressView()
        
           let apiReqObj = ApiRequestClass()
           apiReqObj.customDelegate = self
         let bankdetailsurl = getUrl(urlStr:Url.bankdetailsUrl , serverType: .UnionPayBankDetailsapi)
        DispatchQueue.main.async{
              //  https://www.okdollarapp.com/okdollar/v1/Wallet/BankDetails
           // http://api.sandbox.okdollarapp.com/okdollar/v1/Wallet/BankDetails

            apiReqObj.sendHttpRequest(requestUrl:bankdetailsurl, requestData: "", requestType: RequestTypeObj.RequestTypeBankDetails, httpMethodName: "GET")
           }
       }
          func httpResponse(responseObj: Any, reqType: RequestTypeObj)
          {
              DispatchQueue.main.async{
                  
                progressViewObj.removeProgressView()
                  
                  if let respMsg = responseObj as? String
                  {
                      
                    alertViewObj.wrapAlert(title: "OK$", body: respMsg, img: nil)
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {

                        })
                    alertViewObj.showAlert(controller: self)
                    
                  }
                  else
                  {
                      
                      var respDict = [String:Any]()
                      if responseObj is [Any]
                      {
                         // self.tokenarray = responseObj as! [Any]
                      }
                      else if responseObj is [String:Any]
                      {
                          respDict = responseObj as! Dictionary<String,Any>
                          
                      }
                      
                    if reqType == RequestTypeObj.RequestTypeBankDetails
                      {
                          if respDict.count > 0
                          {
                          let code = respDict["code"] as? Int
                           print("#####",code as Any)
                           if let isValidCode = code {
                             if isValidCode == 200 {
                                self.bankDetailsArray = respDict["data"] as! [[String : Any]]
                              // self.bankDetailsArray.remove(at: 1)
                              //  let itemDict:[String:Any] = respDict["data"] as!  Dictionary<String,Any>
                               
                            
//                                for i in 0..<self.bankDetailsArray.count
//                                    {
//                                        let itemDict:[String:Any] = self.bankDetailsArray[i]["Limit"] as!  Dictionary<String,Any>
//                                        self.limitDict = itemDict
//                                  }
//                                print("######", self.limitDict)
                                
                               self.bankdetailsTableView.reloadData()
                            }
                            else
                            {
                                alertViewObj.wrapAlert(title: "", body: (respDict["message"] as? String)!, img: #imageLiteral(resourceName: "alert-icon"))
                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                    })
                                alertViewObj.showAlert(controller: self)
                              }
                            }
                            else
                             {
                              alertViewObj.wrapAlert(title: "", body: (respDict["message"] as? String)!, img: #imageLiteral(resourceName: "alert-icon"))
                              alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                  })
                              alertViewObj.showAlert(controller: self)
                            }
                            
                             
                          }
                          else
                          {
                             alertViewObj.wrapAlert(title: "", body: (respDict["message"] as? String)!, img: #imageLiteral(resourceName: "alert-icon"))
                              alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                  })
                              alertViewObj.showAlert(controller: self)
                          }
                      }
                  }
              }
          }
   
}
