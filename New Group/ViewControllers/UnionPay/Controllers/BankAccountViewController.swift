//
//  BankAccountViewController.swift
//  OK
//
//  Created by vamsi on 7/27/20.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

protocol FeesCheckFailure {
    func callFeesCheckApi()
}

class BankAccountViewController: UIViewController, CountryLeftViewDelegate, PhValidationProtocol, ApiRequestProtocol, BioMetricLoginDelegate {
    var strStatus = ""
    var strMobileNo = ""

    @IBOutlet weak var contentScroll: UIScrollView!
    @IBOutlet var contentView: UIView!
    
    @IBOutlet var totalAmountLabelHeight: NSLayoutConstraint!
    @IBOutlet var navigationView: UIView!
    @IBOutlet var navigationTitleLabel : UILabel!
    
    @IBOutlet var remarkLabelHeight: NSLayoutConstraint!
    @IBOutlet var navigationImageView: UIImageView!
    
    @IBOutlet var backButton: UIButton!
    @IBOutlet var mobileNumberTF: RightViewSubClassTextFieldSecond! {
        didSet {
             mobileNumberTF.placeholder = "Please Enter Sai Sai Pay Registered Number".localized
         }
    }
    @IBOutlet var inFoView: CardDesignView!
    @IBOutlet var backView: UIView!
    
    @IBOutlet var inFOFeestitleLabel: UILabel!{
        didSet{
            inFOFeestitleLabel.font = UitilityClass.getZwagiFontWithSize(size: 18.0)
            inFOFeestitleLabel.text = "Fees".localized
        }
    }
    
    @IBOutlet var inFOFeesLabel: UILabel!{
        didSet{
            inFOFeesLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
            inFOFeesLabel.text = "OK$".localized
        }
    }
    @IBOutlet var agentMerchantFeesLabel: UILabel!{
        didSet{
            agentMerchantFeesLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
            agentMerchantFeesLabel.text = "Agent / Merchant ".localized
        }
    }
    
    @IBOutlet var INFOtotalFeesLabel: UILabel!{
        didSet{
            INFOtotalFeesLabel.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
            INFOtotalFeesLabel.text = "Total ".localized
        }
    }
    @IBOutlet var inFofeesValueLabel: UILabel!
    @IBOutlet var agentMerchantValueLabel: UILabel!
    
    @IBOutlet var infoOkButton: UIButton!{
        didSet{
            infoOkButton.titleLabel?.font = UitilityClass.getZwagiFontWithSize(size: appButtonSize)
            infoOkButton.setTitle("OK".localized, for: .normal)
        }
    }
    @IBOutlet var agentMerchantTitleLable: UILabel!{
        didSet{
            agentMerchantTitleLable.font = UitilityClass.getZwagiFontWithSize(size: 13.0)
            agentMerchantTitleLable.text = "Agent / Merchant will receive Fees as Cash Back from OK$".localized
        }
    }
    @IBOutlet var totalFeesValueLabel: UILabel!
    
    
    @IBOutlet var confirmMobileNumberTF: RightViewSubClassTextFieldSecond!{
        didSet {
             confirmMobileNumberTF.placeholder = "Please Enter Sai Sai Pay Registered Number".localized
         }
    }
    
    @IBOutlet var fieldLabel: UILabel! {
        didSet {
            fieldLabel.font = UitilityClass.getZwagiFontWithSize(size: 20.0)
            fieldLabel.text = "Sai Sai Pay A/C No.".localized
        }
    }
    @IBOutlet var useraccountnameView: UIView!
    
    @IBOutlet var confirmTfTopConst: NSLayoutConstraint!
    
    @IBOutlet var contentViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var userdetailsView: UIView!
    @IBOutlet var nameView: UIView!
    @IBOutlet var amountView: UIView!
    @IBOutlet var accountlevelView: UIView!
    
    @IBOutlet var userdetailsHeightConstraint: NSLayoutConstraint!
    @IBOutlet var nrcViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var amountTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var nextbuttonBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var nextButtonHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var nrcView: UIView!
    @IBOutlet var nameLabel: UILabel!{
        didSet {
            nameLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
            nameLabel.text = "Name".localized
        }
    }
    
    @IBOutlet var userNameLabel: UILabel!
    @IBOutlet var accountLabel: UILabel!{
        didSet {
            accountLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
            accountLabel.text = "Account Number".localized
        }
    }
    
    
    
    @IBOutlet var remarkVertical: NSLayoutConstraint!
    @IBOutlet var useraccountLabel: UILabel!
    
    @IBOutlet var nrcLabel: UILabel! {
        didSet {
            nrcLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
            nrcLabel.text = "NRC Number".localized
        }
    }
    @IBOutlet var userNrcLabel: UILabel!
    @IBOutlet var amountTF: SkyFloatingLabelTextField! {
        didSet {
             amountTF.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
             amountTF.placeholder = "Enter Transfer Amount".localized
        }
    }
    @IBOutlet var remarkViewHeight: NSLayoutConstraint!
    @IBOutlet var accountLevelLabel: UILabel! {
        didSet {
            accountLevelLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
            accountLevelLabel.text = "Account Level".localized
        }
    }
    @IBOutlet var benficiaryLabel: UILabel! {
        didSet {
            benficiaryLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
            benficiaryLabel.text = "Beneficiary Details".localized
        }
    }
    @IBOutlet var feesheadingLabel: UILabel! {
        didSet {
            feesheadingLabel.font = UitilityClass.getZwagiFontWithSize(size: 14.0)
            feesheadingLabel.text = "Fees".localized
        }
    }
    
    @IBOutlet var totalamountheadingLabel: UILabel! {
        didSet {
            totalamountheadingLabel.font = UitilityClass.getZwagiFontWithSize(size: 14.0)
            totalamountheadingLabel.text = "Total Amount".localized
        }
    }
    @IBOutlet var agentmerchantinfoLabel: UILabel! {
        didSet {
            agentmerchantinfoLabel.font = UitilityClass.getZwagiFontWithSize(size: 12.0)
            agentmerchantinfoLabel.text = "Get this Total Amount from Customer.".localized
        }
    }
    @IBOutlet var remarkheadingLabel: UILabel! {
        didSet {
            remarkheadingLabel.font = UitilityClass.getZwagiFontWithSize(size: 14.0)
            remarkheadingLabel.text = "Remarks :".localized
        }
    }
    
    @IBOutlet var transferheadingLabel: UILabel!{
        didSet {
            transferheadingLabel.text = "Transfer Details".localized
        }
    }
    
    var stractiveField = ""
    
    @IBOutlet var feesTopConstraint: NSLayoutConstraint!
    @IBOutlet var feesHeightContraint: NSLayoutConstraint!
    @IBOutlet var feesicon: UIImageView!
    @IBOutlet var feesMMKLabel: UILabel!
    
    @IBOutlet var useraccountLevelLabel: UILabel!
    
    @IBOutlet var amountViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet var limitViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet var closeButton: UIButton!
    @IBOutlet var confrimCloseButton: UIButton!
    @IBOutlet var lblconfirmSeprator: UILabel!
    @IBOutlet var amountCloseButton: UIButton!
    @IBOutlet var nextButton: UIButton! {
       didSet{
            //nextButton.setTitle(nextButton.titleLabel?.text?.localized, for: .normal)
             nextButton.titleLabel?.font = UitilityClass.getZwagiFontWithSize(size: appButtonSize)
             nextButton.setTitle("Next".localized, for: .normal)
        }
    }
    @IBOutlet var maximumamountLabel: UILabel!{
        didSet{
              maximumamountLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
        }
    }
    var contactSuggestionView = Bundle.main.loadNibNamed("SAConatactListView", owner: self, options: nil)?[0] as! SAConatactListView
    var projectid = ""
    var bankName = ""
    var updatedText = String()
    var parameters:[String : Any] = [:]
    var mobilenumFormate = ""
    var limit = 0
    var usertype = 0
    var limitdict: [String : Any]?
    var walletImage = ""
    var valueamount = ""
    var minimumvalueamount = ""
    var merchantlimit = 0
    var OKFeesTransId = ""
    var mobilenumdisplay = ""
    var persontype = 0
    
    var totalamount = ""
    var feeamount = ""
    var feetransactionid = ""
    var transactionrefrence = ""
    var bankUserType = ""
    var nrcpassport  = ""
    var okFeeAmount = ""
    
    var minlimit = ""
    var maxlimit = ""
    
    var block1: DispatchWorkItem?
    var KeyboardSize : CGFloat = 0.0
    
    var countryView   : PaytoViews?
    var country       : Country?
    let validObj = PayToValidations.init()
    var isAPICalled = false
    public var currentSelectedCountry : countrySelected = .myanmarCountry
       enum countrySelected {
           case indiaCountry, thaiCountry, chinaCountry, myanmarCountry, other
       }
    private var mobileNumberLeftView = DefaultIconView.mobileupdateView(icon: "accountHolder")
    private var connfrimMobileNumberLeftView = DefaultIconView.updateView(icon: "topup_number")
    var isSelectedFromContact = false
    
    @IBOutlet var feesCheckView: UIView!
    @IBOutlet var totalamountViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var feesCheckHeightConstraint: NSLayoutConstraint!
    @IBOutlet var remarkCloseButton: UIButton!
   // @IBOutlet var remarkTF: SkyFloatingLabelTextField!
    @IBOutlet var fessamountLabel: UILabel!
    @IBOutlet var totalamountLabel: UILabel!
    
    @IBOutlet var infoButton: UIButton!
    
    
    @IBOutlet var remarkTextView: UITextView!
    @IBOutlet var remarkView: UIView!
    
    var remarkHeightContraint = 0.0
    var selectedother = ""
    
    override func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(animated)
        
        if strStatus == "RecentTx"{
        self.navigationController?.navigationBar.isHidden = false
        }
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification,object : nil)
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        inFoView.isHidden = true
        backView.isHidden = true
        totalAmountLabelHeight.constant = 0.0
        totalamountViewHeightConstraint.constant = 60.0
        if remarkTextView.attributedText == returnAttributtedtextview() {
            remarkVertical.constant = 0.0
            remarkLabelHeight.constant = 0.0
        }else{
            remarkVertical.constant = 8.0
            remarkLabelHeight.constant = 15.0
        }
        
//        if device.type == .iPhoneX || device.type == .iPhoneXR || device.type == .iPhoneXS || device.type == .iPhoneXSMax || device.type == .iPhone11 {
//            self.contentViewHeightConstraint.constant = 600
//           }
//           else {
//            self.contentViewHeightConstraint.constant = 750
//            }
       
       // self.navigationController?.navigationBar.isHidden = true
        confirmMobileNumberTF.delegate = self
       
        feesCheckView.isHidden = true
        remarkTextView.delegate = self
       
        remarkTextView.attributedText = self.returnAttributtedtextview()
        // Do any additional setup after loading the view.
        
        let urlFront = walletImage
        let imageUrl = URL(string: urlFront )
        self.navigationImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "bank"))

      //  navigationImageView.transform = navigationImageView.transform.rotated(by: .pi)
        
        if bankName == "MAB Bank"{
            let fullName    = bankName
            let fullNameArr = fullName.components(separatedBy: " ")

            let name    = fullNameArr[0]
          //  let surname = fullNameArr[1]
            navigationTitleLabel.text = name
            fieldLabel.text = "Myanma Apex Bank"
          
        }
        else{
            navigationTitleLabel.text = bankName
            fieldLabel.text = bankName
        }
        
        
        mobilenumFormate = UserModel.shared.mobileNo
        
        self.userdetailsView.isHidden = true
        self.useraccountnameView.isHidden = true
        self.remarkCloseButton.isHidden = true
        
        usertype = UserModel.shared.agentType.intValue
        
        if usertype == 6 {
          limit = limitdict?["Subscriber"] as? Int ?? 0
//          let valuelimit  = String(self.limit)
//          let valueAmountStr = wrapAmountWithCommaDecimal(key: valuelimit)
//           valueamount = valueAmountStr
//          maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(valueAmountStr) \("MMK".localized)"
//
          usertype = 0
            infoButton.isHidden = true
            feesicon.isHidden = false
            feesMMKLabel.isHidden = false
            feesTopConstraint.constant = 0
            feesHeightContraint.constant = 60
            feesCheckHeightConstraint.constant = 147//87
            totalAmountLabelHeight.constant = 0.0
            totalamountViewHeightConstraint.constant = 60.0
            
        }
        else if usertype == 1 {
             limit = limitdict?["Agent"] as? Int ?? 0
//            let valuelimit  = String(self.limit)
//          let valueAmountStr = wrapAmountWithCommaDecimal(key: valuelimit)
//            valueamount = valueAmountStr
//            maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(valueAmountStr) \("MMK".localized)"
             usertype = 2
            if selectedother == "other"{
                infoButton.isHidden = false
                feesicon.isHidden = false
                feesMMKLabel.isHidden = false
                feesTopConstraint.constant = 0
                feesHeightContraint.constant = 60
                feesCheckHeightConstraint.constant = 147
                totalAmountLabelHeight.constant = 18.0
                totalamountViewHeightConstraint.constant = 80.0
            }
            else{
                infoButton.isHidden = true
                feesicon.isHidden = false
                feesMMKLabel.isHidden = false
                feesTopConstraint.constant = 0
                feesHeightContraint.constant = 60
                feesCheckHeightConstraint.constant = 147
            }
            
        }
      else if usertype == 2 {
           limit = limitdict?["Merchant"] as? Int ?? 0
//            let valuelimit  = String(self.limit)
//            let valueAmountStr = wrapAmountWithCommaDecimal(key: valuelimit)
//            valueamount = valueAmountStr
//            maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(valueAmountStr) \("MMK".localized)"
              usertype = 1
        if selectedother == "other"{
            infoButton.isHidden = false
            feesicon.isHidden = false
            feesMMKLabel.isHidden = false
            feesTopConstraint.constant = 0
            feesHeightContraint.constant = 60
            feesCheckHeightConstraint.constant = 147
            totalAmountLabelHeight.constant = 18.0
            totalamountViewHeightConstraint.constant = 80.0
        }
        else{
            infoButton.isHidden = true
            feesicon.isHidden = false
            feesMMKLabel.isHidden = false
            feesTopConstraint.constant = 0
            feesHeightContraint.constant = 60
            feesCheckHeightConstraint.constant = 147
        }
      }
        if appDelegate.checkNetworkAvail(){
           // mobileNumberTF.becomeFirstResponder()
            
            mobileNumberTF.isUserInteractionEnabled = false
        }else{
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
               
                
            })
            alertViewObj.showAlert(controller:self)
        }
        
        println_debug("project id \(projectid) mobile no \(strMobileNo) screen\(strStatus)")
        //Manage From Recent View
               if strStatus == "RecentTx"{
                mobileNumberTF.resignFirstResponder()
                   bankDetailsAPI()
               }
              else
               {
                   setUI()
               }
     //   self.automaticallyAdjustsScrollViewInsets = false
        self.nextButton.isHidden = true
        nextbuttonBottomConstraint.constant = 0
       // self.view.layoutIfNeeded()
        subscribeToShowKeyboardNotifications()
    }
    
    
    @IBAction func onClickInfoOK(_ sender: Any) {
        inFoView.isHidden = true
        backView.isHidden = true
        nextButton.isUserInteractionEnabled = true
    }
    
    
    func subscribeToShowKeyboardNotifications() {
           NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
           NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
       }
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            //            self.adImageConstraint.constant = 0 - keyboardHeight
//            if  self.stractiveField == "AMOUNTTF" {
//                self.contentViewHeightConstraint.constant = 450
//            }
//            else{
//                self.contentViewHeightConstraint.constant = 550
//            }
            self.contentViewHeightConstraint.constant = 580
            
        if device.type == .iPhoneX || device.type == .iPhoneXR || device.type == .iPhoneXS || device.type == .iPhoneXSMax {
                    self.nextbuttonBottomConstraint.constant = keyboardHeight+10
                }
            else {
                    self.nextbuttonBottomConstraint.constant = keyboardHeight
                }
          //  self.bottomGetBillBtnConstraint.constant = keyboardHeight
            if self.nextButton.isHidden {
                self.nextButtonHeightConstraint.constant = 0
                self.nextButton.setTitle("".localized, for: .normal)
                                          
            } else {
                self.nextButtonHeightConstraint.constant = 45
                self.nextButton.setTitle("Next".localized, for: .normal)
            }
            
        })
    }
    @objc func keyboardWillHide(_ notification: Notification) {
        //        self.adImageConstraint.constant = 0
       self.stractiveField = ""
               
       self.contentViewHeightConstraint.constant = 750
        self.nextbuttonBottomConstraint.constant = 0
        self.nextButton.setTitle("".localized, for: .normal)
        if self.nextButton.isHidden {
            self.nextButtonHeightConstraint.constant = 0
            self.nextButton.setTitle("".localized, for: .normal)
        } else {
            self.nextButtonHeightConstraint.constant = 45
            self.nextButton.setTitle("Next".localized, for: .normal)
        }
    }
    
    func setUI() {
       //  headingLabel.setPropertiesForLabel()
         mobileNumberTF.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
         confirmMobileNumberTF.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
         amountTF.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
         closeButton.isHidden = true
        // fieldLabel.isHidden = true
         phNumValidationsFile = getDataFromJSONFile() ?? []
         self.country = Country.init(name: "Myanmar", code: "myanmar", dialCode: "+95")
         countryView = PaytoViews.updateView()
         let countryCode = String(format: "(%@)", "+95")
         countryView?.wrapCountryViewData(img: "myanmar", str: countryCode)
         countryView?.delegate = self
       //  self.mobileNumberTF.leftViewMode = .always
       //  self.mobileNumberTF.leftView    = mobileNumberLeftView
         self.mobileNumberTF.text = mobilenumdisplay
        useraccountLabel.text = mobilenumdisplay
         self.apicall()
               
        //Hide confrim Number
        self.confirmMobileNumberTF.isHidden = false
        self.hideConfirmNumber()
        
        if strStatus == "RecentTx" {
            mobileNumberTF.resignFirstResponder()
            mobileNumberTF.text = strMobileNo
            useraccountLabel.text = strMobileNo
            closeButton.isHidden = false
            //Hide confrim Number
            self.confirmMobileNumberTF.isHidden = false
            self.hideConfirmNumber()
            isSelectedFromContact = false
            self.confirmTfTopConst.constant = -44
            self.apicall()
        }
     }
    
    func showConfrimNumber(isShowKeyboard: Bool) {
        if self.confirmMobileNumberTF.isHidden == true {
            if isShowKeyboard {
               self.confirmMobileNumberTF.becomeFirstResponder()
            }
            self.confirmMobileNumberTF.leftViewMode = .always
            self.confirmMobileNumberTF.leftView = connfrimMobileNumberLeftView
            self.confirmMobileNumberTF.isHidden = false
            self.confrimCloseButton.isHidden = true
            self.lblconfirmSeprator.isHidden = false
            self.confirmMobileNumberTF.text = "09"
        }
    }
    
    func hideConfirmNumber() {
        if self.confirmMobileNumberTF.isHidden == false {
            self.confirmMobileNumberTF.leftView = nil
            self.confirmMobileNumberTF.text = ""
            self.confirmMobileNumberTF.isHidden = true
            self.confrimCloseButton.isHidden = true
            self.lblconfirmSeprator.isHidden = true
            self.userdetailsView.isHidden = true
            self.useraccountnameView.isHidden = true
            self.amountTF.text = ""
            self.amountCloseButton.isHidden = true
            self.nextButton.isHidden = true
            self.nextButtonHeightConstraint.constant = 0
            self.subscribeToShowKeyboardNotifications()
        }
        if isSelectedFromContact {
            self.isSelectedFromContact = false
            self.confirmTfTopConst.constant = 0
            self.confirmMobileNumberTF.leftView = nil
            self.confirmMobileNumberTF.text = ""
            self.confirmMobileNumberTF.isHidden = true
            self.confrimCloseButton.isHidden = true
            self.lblconfirmSeprator.isHidden = true
            self.userdetailsView.isHidden = true
            self.useraccountnameView.isHidden = true
            self.amountTF.text = ""
            self.amountCloseButton.isHidden = true
            self.nextButton.isHidden = true
            self.nextButtonHeightConstraint.constant = 0
            self.subscribeToShowKeyboardNotifications()
        }
        if strStatus == "RecentTx" {
            self.confirmTfTopConst.constant = 0
            self.confirmMobileNumberTF.leftView = nil
            self.confirmMobileNumberTF.text = ""
            self.confirmMobileNumberTF.isHidden = true
            self.confrimCloseButton.isHidden = true
            self.lblconfirmSeprator.isHidden = true
            self.userdetailsView.isHidden = true
            self.useraccountnameView.isHidden = true
            self.amountTF.text = ""
            self.amountCloseButton.isHidden = true
            self.nextButton.isHidden = true
            self.nextButtonHeightConstraint.constant = 0
            self.subscribeToShowKeyboardNotifications()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        if amountTF.text != ""{
            self.nextButton.isHidden = false
            self.nextButtonHeightConstraint.constant = 45
            self.nextButton.setTitle("Next".localized, for: .normal)
            self.subscribeToShowKeyboardNotifications()
        }
       else
        {
            self.nextButton.isHidden = true
            self.nextButtonHeightConstraint.constant = 0
            nextButton.setTitle("".localized, for: .normal)
            self.subscribeToShowKeyboardNotifications()

        }
        if NSString(string: UserLogin.shared.walletBal).floatValue == 0.0{
            self.addMoney()
            self.mobileNumberTF.resignFirstResponder()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden(notification:)) , name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillChangeHeight(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
   
    
    
   // wallet balance if zero
    func addMoney() {
           alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
            alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                    self.modalPresentationStyle = .fullScreen
                    self.present(addWithdrawView, animated: true, completion: nil)
                }
            })
            alertViewObj.showAlert()
       }
  
    //Function to hide show button
      func clearButtonShowHide(count: Int) {
          if count > 0 {
              self.closeButton.isHidden = false
              // fieldLabel.isHidden = false
          } else {
              self.closeButton.isHidden = true
             // fieldLabel.isHidden = true
               self.userdetailsView.isHidden = true
            self.useraccountnameView.isHidden = true
          }
      }
    
    func openAction(type: ButtonType) {
           // no need in this view
        self.openCountryScreen()
    }
       
    func clearActionType() {
        // no need in this view
    }
    func openCountryScreen() {
        var countryVC: CountryViewController?
        countryVC = UIStoryboard(name: "Country", bundle: nil).instantiateViewController(withIdentifier: "CountryViewController") as? CountryViewController
        countryVC?.delegate = self
        countryVC?.modalPresentationStyle = .fullScreen
        if let countryVC = countryVC {
            navigationController?.present(countryVC, animated: true)
        }
    }
    
    func FeeCheckApi(){
        progressViewObj.showProgressView()
              let apiReqObj = ApiRequestClass()
              apiReqObj.customDelegate = self
                parameters = [
                  "WalletNumber": mobileNumberTF.text ?? "",
                  "OKNumber": UserModel.shared.mobileNo,
                  "ProjectId": projectid,
                  "UserType": usertype,//UserModel.shared.agentType.intValue,
                  "Amount":amountTF.text ?? "",
                  "Channel": "0", //"1"
                  "BankUserType":bankUserType,
                  "OSType":UserModel.shared.osType,
                  "BankMobileNumber": mobileNumberTF.text ?? "",
                  "_v":"V2",
                  "MySelf": persontype
                  ]
        
          let bankfeeschecksurl = getUrl(urlStr:Url.bankfeescheck , serverType: .UnionPayBankDetailsapi)
              
          DispatchQueue.main.async{
            apiReqObj.sendHttpRequest(requestUrl: bankfeeschecksurl, requestData: self.parameters, requestType: RequestTypeObj.RequestTypeFeesCheck, httpMethodName: "POST")
        }
    }
    
    func apicall(){
        progressViewObj.showProgressView()
        let apiReqObj = ApiRequestClass()
        apiReqObj.customDelegate = self
          parameters = [
            "BankAccountNumber": mobileNumberTF.text ?? "", //"0830131083000106018",
            "OKNumber": UserModel.shared.mobileNo, //"00959966646970",//mobilenumFormate,
            "ProjectId": projectid,
            "UserType": usertype,//UserModel.shared.agentType.intValue,
            "Channel": "0", //"1"
            "_v":"V2",
            "OSType": UserModel.shared.osType,
            ]
        
         print("*****",parameters)
       
        let bankverifywalletnumurl = getUrl(urlStr:Url.bankVerifyWalletNumber , serverType: .UnionPayBankDetailsapi)
           DispatchQueue.main.async{
            apiReqObj.sendHttpRequest(requestUrl:bankverifywalletnumurl, requestData: self.parameters, requestType: RequestTypeObj.RequestTypeBankAccount, httpMethodName: "POST")
        }
    }
    
    func bankDetailsAPI(){
               
             progressViewObj.showProgressView()
            
               let apiReqObj = ApiRequestClass()
               apiReqObj.customDelegate = self
             let bankdetailsurl = getUrl(urlStr:Url.bankdetailsUrl , serverType: .UnionPayBankDetailsapi)
            DispatchQueue.main.async{
                apiReqObj.sendHttpRequest(requestUrl: bankdetailsurl, requestData: "", requestType: RequestTypeObj.RequestTypeBankDetails, httpMethodName: "GET")
               }
           }
     
    func httpResponse(responseObj: Any, reqType: RequestTypeObj) {
        DispatchQueue.main.async {
            progressViewObj.removeProgressView()
            if let respMsg = responseObj as? String {
                alertViewObj.wrapAlert(title: "OK$", body: respMsg, img: nil)
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                alertViewObj.showAlert(controller: self)
            }else {
               // var respDict = [String:Any]()
//                if responseObj is [Any] {
//                    // self.tokenarray = responseObj as! [Any]
//                } else if responseObj is [String:Any] {
//                    respDict = responseObj as! Dictionary<String,Any>
//                }
                var respDict = [String:Any]()
                if let resposneDataFromServer = responseObj as? [String : Any] {
                    respDict = resposneDataFromServer
                }
                if reqType == RequestTypeObj.RequestTypeBankAccount {
                    if respDict.count > 0 {
                        
                        print ("Response", respDict)
                        let code = respDict["code"] as? Int
                        print("#####",code as Any)
                        
                        if let isValidCode = code {
                            if isValidCode == 200 {
                                
                                if self.isSelectedFromContact {
                                    self.confrimCloseButton.isHidden = true
                                    self.confirmMobileNumberTF.isHidden = true
                                    self.lblconfirmSeprator.isHidden = true
                                    self.confirmTfTopConst.constant = -44
                                }
                              
                                if let itemDict:[String:Any] = respDict["data"] as? [String : Any] {
                                    
                                    self.userdetailsView.isHidden = false
                                    self.useraccountnameView.isHidden = false
                                    
                                    self.userNameLabel.text = itemDict["Name"] as? String
                                    //  self.userNrcLabel.text = itemDict["BankAccountNumber"] as? String
                                    //  self.limit = itemDict["Limit"]  as? Int ?? 0
                                    self.bankUserType = itemDict["BankUserType"] as? String ?? ""
                                   // self.nrcpassport = itemDict["NRCPassport"] as? String ?? ""
                                    self.minlimit = "\(itemDict["MinLimit"] as? Int ?? 0)"
                                    self.maxlimit = "\(itemDict["Limit"] as? Int ?? 0)"
                                }
                               // let itemDict:[String:Any] = respDict["data"] as!  Dictionary<String,Any>
                               
                                if self.nrcpassport != ""{
                                    self.nrcView.isHidden = false
                                    self.userNrcLabel.text = self.nrcpassport
                                    self.nrcViewHeightConstraint.constant = 60
                                   // self.amountTopConstraint.constant = 1
                                    self.userdetailsHeightConstraint.constant = 305
                                }else {
                                    self.nrcView.isHidden = true
                                    self.nrcViewHeightConstraint.constant = 0
                                   // self.amountTopConstraint.constant = 0
                                    self.amountViewTopConstraint.constant = 0
                                    self.userdetailsHeightConstraint.constant = 245
                                    
                                }
                              
                                let minimumvalueAmountStr = wrapAmountWithCommaDecimal(key: self.minlimit)
                                self.minimumvalueamount = minimumvalueAmountStr
                                let valueAmountStr = wrapAmountWithCommaDecimal(key: self.maxlimit)
                                self.valueamount = valueAmountStr
                               // self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(valueAmountStr) \("MMK".localized)"
                              //  self.maximumamountLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
                                self.maximumamountLabel.text = "\("*(")\("Enter Amount".localized) \(minimumvalueAmountStr) \(" ~ ") \(valueAmountStr) \("MMK".localized) \(")")"
                                self.maximumamountLabel.textColor = UIColor.red
                                
                                self.useraccountLevelLabel.attributedText = self.returnAttributtedmmk()
                                //self.amountTF.title = "Amount".localized
                                self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                                self.amountTF.placeholder = "Enter Transfer Amount".localized
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                              //   self.contentScroll.isScrollEnabled = false
                                    self?.amountTF.becomeFirstResponder()
                                   }
                              //  scrollview setContentOffset:scrollview.contentOffset animated:NO];
                                
                                
                                print ("&&&&&",respDict["data"] ?? "")
                            }else if isValidCode == 302 {
                                alertViewObj.wrapAlert(title: "", body: "Number not registered in \(self.bankName)".localized, img: #imageLiteral(resourceName: "alert-icon"))
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                self.mobileNumberTF.text = ""
                                self.closeButton.isHidden = true
                               // self.fieldLabel.isHidden = true
                                self.mobileNumberTF.becomeFirstResponder()
                                self.hideConfirmNumber()
                               // self.nextButton.isHidden = false
                            })
                                alertViewObj.showAlert(controller: self)
                            }
                            else {
                                alertViewObj.wrapAlert(title: "", body: (respDict["message"] as? String) ?? "", img: #imageLiteral(resourceName: "alert-icon"))
                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                    self.mobileNumberTF.text = ""
                                    self.closeButton.isHidden = true
                                   // self.fieldLabel.isHidden = true
                                    self.mobileNumberTF.becomeFirstResponder()
                                    self.hideConfirmNumber()
                                })
                                alertViewObj.showAlert(controller: self)
                            }
                        }else {
                            alertViewObj.wrapAlert(title: "", body: (respDict["message"] as? String) ?? "", img: #imageLiteral(resourceName: "alert-icon"))
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                            })
                            alertViewObj.showAlert(controller: self)
                        }
                    }else {
                        alertViewObj.wrapAlert(title: "", body: (respDict["message"] as? String) ?? "", img: #imageLiteral(resourceName: "alert-icon"))
                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        })
                        alertViewObj.showAlert(controller: self)
                    }
                }
                if reqType == RequestTypeObj.RequestTypeFeesCheck {
                    if respDict.count > 0 {
                        let code = respDict["code"] as? Int
                        print("#####%%%",code as Any)
                                                
                        if let isValidCode = code {
                            if isValidCode == 200 {
                                
//                                DispatchQueue.main.async {
//                                    self.totalamountViewHeightConstraint.constant = 80.0
//                                    self.totalAmountLabelHeight.constant = 18.0
//                                }
                                var agentMerchantFees = ""
                                if let itemDict:[String:Any] = respDict["data"] as? [String : Any] {
                                    self.totalamount = "\(itemDict["TotalAmount"] as? Double ?? 0.0)"
                                    self.feeamount = "\(itemDict["FeeAmount"] as? Double ?? 0.0)"
                                    self.feetransactionid = itemDict["FeeTransactionId"] as? String ?? ""
                                    self.transactionrefrence = itemDict["FeeRefferenceId"] as? String ?? ""
                                    self.okFeeAmount = "\(itemDict["OKFeeAmount"] as? Double ?? 0.0)"
                                    agentMerchantFees = "\(itemDict["AgentFees"] as? Double ?? 0.0)"
                                    self.OKFeesTransId = itemDict["OKFeesTransId"] as? String ?? ""
                                }
                                self.feesCheckView.isHidden = false
                                self.nextButton.isHidden = false
                                self.nextButtonHeightConstraint.constant = 45
                                self.nextButton.setTitle("Next".localized, for: .normal)
                                self.subscribeToShowKeyboardNotifications()
                               // self.remarkTF.keyboardType = .default
                               // self.remarkTF.becomeFirstResponder()
//                                if self.usertype == 0 {
//                                    self.infoButton.isHidden = true
//                                    self.totalamountViewHeightConstraint.constant = 48
//                                    self.feesCheckHeightConstraint.constant = 147
//                                }
//                                else
//                                {
//                                    self.infoButton.isHidden = false
//                                    self.totalamountViewHeightConstraint.constant = 80
//                                    self.feesCheckHeightConstraint.constant = 179
//                                }
                                
//                                 let amout = self.amountTF.text?.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: ".", with: "")
//
//                                let resultfee = self.feeamount.replacingOccurrences(of: ",", with: "")
//
//                                let result = Double(String(amout ?? ""))! + Double(String(resultfee))!
//                                
                                let valueAmountStr = wrapAmountWithCommaDecimal(key: self.totalamount)
                                
                               // self.remarkCloseButton.isHidden = true
                             //   self.fessamountLabel.text = wrapAmountWithCommaDecimal(key: self.feeamount)//self.feeamount
                                
                                
                                self.inFofeesValueLabel.attributedText  =  self.returnAttributtedtextview(textOne: ": " + wrapAmountWithCommaDecimal(key: self.okFeeAmount), textTwo: " MMK", sizeOne: 15.0, sizeTwo: 12.0, color: .black)
                                self.agentMerchantValueLabel.attributedText  =  self.returnAttributtedtextview(textOne: ": " + wrapAmountWithCommaDecimal(key: agentMerchantFees), textTwo: " MMK", sizeOne: 15.0, sizeTwo: 12.0, color: .black)
                                let data = Double(agentMerchantFees) ?? 0.0
                                let value = Double(self.okFeeAmount) ?? 0.0
                                let feevalue   = Double(self.feeamount) ?? 0.0
                                let new = data + value + feevalue
                                let newValue = wrapAmountWithCommaDecimal(key: "\(new)").replacingOccurrences(of: ",", with: "")
                                self.totalFeesValueLabel.attributedText = self.returnAttributtedtextview(textOne: ": " + wrapAmountWithCommaDecimal(key: newValue), textTwo: " MMK", sizeOne: 15.0, sizeTwo: 12.0, color: .black)

                                
                                
                             //   self.inFofeesValueLabel.text =  ": " + wrapAmountWithCommaDecimal(key: self.okFeeAmount)
                               // self.agentMerchantValueLabel.text = ": "  + "0"
                              //  self.totalFeesValueLabel.text = ": " + wrapAmountWithCommaDecimal(key: self.okFeeAmount)
                                
                                let result = Double(String(new))! + Double(String(self.totalamount))!
                                self.fessamountLabel.text = wrapAmountWithCommaDecimal(key: "\(new)")
                                
                                self.totalamountLabel.text = wrapAmountWithCommaDecimal(key: "\(result)")//valueAmountStr
                                
                                //self.contentScroll.isScrollEnabled = true
//                                if self.totalamount != ""{
//                                   OKPayment.main.authenticate(screenName: "BankAccountViewController", delegate: self)
//                                }
//                                else
//                                {
//                                    alertViewObj.wrapAlert(title: "OK$", body: (respDict["message"] as? String) ?? "", img: nil)
//                                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                                    })
//                                    alertViewObj.showAlert(controller: self)
//                                }
                                
                                
                            }else {
                                alertViewObj.wrapAlert(title: "OK$", body: (respDict["message"] as? String) ?? "", img: nil)
                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                    self.amountTF.text = ""
                                    self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                                    self.amountTF.placeholder = "Enter Transfer Amount".localized
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                                      //  self?.contentScroll.isScrollEnabled = false
                                        self?.amountTF.becomeFirstResponder()
                                    }
                                    self.nextButton.isHidden = true
                                    self.nextButtonHeightConstraint.constant = 0
                                    self.nextButton.setTitle("".localized, for: .normal)
                                    self.subscribeToShowKeyboardNotifications()
                                
                                })
                                alertViewObj.showAlert(controller: self)
                            }
                        }else {
                            alertViewObj.wrapAlert(title: "OK$", body: (respDict["message"] as? String) ?? "", img: nil)
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                            })
                            alertViewObj.showAlert(controller: self)
                        }
                    }else {
                        alertViewObj.wrapAlert(title: "", body: (respDict["message"] as? String) ?? "", img: #imageLiteral(resourceName: "alert-icon"))
                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        })
                        alertViewObj.showAlert(controller: self)
                    }
                }
                if reqType == RequestTypeObj.RequestTypeBankDetails {
                    if respDict.count > 0
                    {
                        let code = respDict["code"] as? Int
                        if let isValidCode = code {
                            if isValidCode == 200 {
                                var bankDetailsArray = [[String : Any]]()
                                if respDict.count>0{
                                    
                                    if let isValidData = respDict["data"] as? [[String : Any]]{
                                        bankDetailsArray = isValidData
                                    }
                                    if bankDetailsArray.count>0{
                                        for i in 0..<bankDetailsArray.count{
                                            if bankDetailsArray.indices.contains(i){
                                                if let validID = bankDetailsArray[i]["ProjectId"] as? String{
                                                    if self.projectid == validID{
                                                        self.walletImage = bankDetailsArray[i]["Logo"] as? String ?? ""
                                                        self.projectid = bankDetailsArray[i]["ProjectId"] as? String ?? ""
                                                        self.bankName = bankDetailsArray[i]["Name"] as? String ?? ""
                                                        self.setUI()
                                                        self.navigationImageView.sd_setImage(with: URL(string: bankDetailsArray[i]["Logo"] as? String ?? ""), placeholderImage: UIImage(named: "bank"))
                                                        break
                                                    }
                                                    
                                                }
                                                
//                                                if self.projectid  == String(describing: sampleDict["ProjectId"]!) {
//                                                    self.walletImage = String(describing: sampleDict["Logo"]!)
//                                                    self.projectid = String(describing: sampleDict["ProjectId"]!)
//                                                    self.bankName = String(describing: sampleDict["Name"]!)
//
//                                                    // let itemDict:[String:Any] = sampleDict["Limit"] as!  Dictionary<String,Any>
//                                                    //  self.limitdict = itemDict
//                                                    self.setUI()
//
//                                                    self.navigationImageView.sd_setImage(with: URL(string: String(describing: sampleDict["Logo"]!)), placeholderImage: UIImage(named: "bank"))
//
//                                                    //                                        if UserModel.shared.agentType.intValue == 6 {
//                                                    ////                                            self.limit = self.limitdict?["Subscriber"] as? Int ?? 0
//                                                    ////                                            let valuelimit  = String(self.limit)
//                                                    ////                                            let valueAmountStr = wrapAmountWithCommaDecimal(key: valuelimit)
//                                                    ////                                            self.valueamount = valueAmountStr
//                                                    ////                                            self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(self.valueamount) \("MMK".localized)"
//                                                    //                                            usertype = 0
//                                                    //                                        }
//                                                    //                                        else if UserModel.shared.agentType.intValue == 1 {
//                                                    ////                                            self.limit = self.limitdict?["Agent"] as? Int ?? 0
//                                                    ////                                            let valuelimit  = String(self.limit)
//                                                    ////                                            let valueAmountStr = wrapAmountWithCommaDecimal(key: valuelimit)
//                                                    ////                                            self.valueamount = valueAmountStr
//                                                    ////                                            self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(self.valueamount) \("MMK".localized)"
//                                                    //                                            usertype = 2
//                                                    //                                        }
//                                                    //                                        else if UserModel.shared.agentType.intValue == 2 {
//                                                    ////                                            self.limit = self.limitdict?["Merchant"] as? Int ?? 0
//                                                    ////                                            let valuelimit  = String(self.limit)
//                                                    ////                                            let valueAmountStr = wrapAmountWithCommaDecimal(key: valuelimit)
//                                                    ////                                            self.valueamount = valueAmountStr
//                                                    ////                                            self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(self.valueamount) \("MMK".localized)"
//                                                    //                                            usertype = 1
//                                                    //                                        }
//                                                    break
//                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                alertViewObj.wrapAlert(title: "", body: (respDict["message"] as? String) ?? "", img: #imageLiteral(resourceName: "alert-icon"))
                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                })
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                        else
                        {
                            alertViewObj.wrapAlert(title: "", body: (respDict["message"] as? String) ?? "", img: #imageLiteral(resourceName: "alert-icon"))
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                            })
                            alertViewObj.showAlert(controller: self)
                        }
                        
                        
                    }
                    else
                    {
                        alertViewObj.wrapAlert(title: "", body: (respDict["message"] as? String) ?? "", img: #imageLiteral(resourceName: "alert-icon"))
                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        })
                        alertViewObj.showAlert(controller: self)
                    }
                }
            }
        }
      }
    
    @IBAction func backButtonClick(_ sender: Any) {
         if strStatus == "RecentTx"{
           self.dismiss(animated: true, completion: nil)
        }
        else
         {
            self.navigationController?.popViewController(animated: true)
        }
       
        
    }
    @IBAction func closeButtonClick(_ sender: Any) {
       // self.fieldLabel.isHidden = true
        self.mobileNumberTF.text =  (self.country?.dialCode == "+95") ? "09" : ""
        self.closeButton.isHidden = true
        self.userdetailsView.isHidden = true
        self.useraccountnameView.isHidden = true
        self.amountTF.text = ""
        self.amountView.layer.borderWidth = 0
        self.amountView.layer.borderColor = UIColor.gray.cgColor
        if self.nrcpassport == ""{
            self.amountViewTopConstraint.constant = 0
            self.limitViewTopConstraint.constant = 1
        }
        else{
            self.amountViewTopConstraint.constant = 1
            self.limitViewTopConstraint.constant = 1
        }
         self.amountCloseButton.isHidden = true
        self.nextButton.isHidden = true
        self.nextButtonHeightConstraint.constant = 0
        self.nextButton.setTitle("".localized, for: .normal)
        self.subscribeToShowKeyboardNotifications()
        //self.amountTF.title = "Amount".localized
        self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
        self.mobileNumberTF.becomeFirstResponder()
        self.removeContactList()
        self.hideConfirmNumber()
        
        if strStatus == "RecentTx" {
        
            //Hide confrim Number
            self.confirmMobileNumberTF.isHidden = false
            self.hideConfirmNumber()
            isSelectedFromContact = false
            self.confirmTfTopConst.constant = 0
        }
        
        
    }
    
    
    func returnAttributtedtextview(textOne: String,textTwo: String,sizeOne:CGFloat,sizeTwo: CGFloat,color: UIColor) -> NSMutableAttributedString {
        //theme setting
        let customAttribute1 = [ NSMutableAttributedString.Key.font: UIFont(name: appFont, size: sizeOne) ?? UIFont.systemFont(ofSize: sizeOne),
                                 NSMutableAttributedString.Key.foregroundColor: color]
        let customAttribute2 = [ NSMutableAttributedString.Key.font: UIFont(name: appFont, size: sizeTwo) ?? UIFont.systemFont(ofSize: sizeTwo),
                                 NSMutableAttributedString.Key.foregroundColor: color ]
        
        
        //adding attributes and text
        let attributedAmountString = NSMutableAttributedString.init(string: textOne, attributes: customAttribute1)
        
        let mmkString = NSMutableAttributedString.init(string: textTwo, attributes: customAttribute2)
       
        attributedAmountString.append(mmkString)

        return attributedAmountString
        
    }
    
    
    
    
    @IBAction func onClickConfrimCloseAction(_ sender: UIButton) {
        self.confrimCloseButton.isHidden = true
        self.confirmMobileNumberTF.text =  (self.country?.dialCode == "+95") ? "09" : ""
        self.userdetailsView.isHidden = true
        self.useraccountnameView.isHidden = true
        self.amountTF.text = ""
         self.amountView.layer.borderWidth = 0
               self.amountView.layer.borderColor = UIColor.gray.cgColor
               if self.nrcpassport == ""{
                   self.amountViewTopConstraint.constant = 0
                   self.limitViewTopConstraint.constant = 1
               }
               else{
                   self.amountViewTopConstraint.constant = 1
                   self.limitViewTopConstraint.constant = 1
               }
                self.amountCloseButton.isHidden = true
               self.nextButton.isHidden = true
               self.nextButtonHeightConstraint.constant = 0
               self.nextButton.setTitle("".localized, for: .normal)
               self.subscribeToShowKeyboardNotifications()
             //  self.amountTF.title = "Amount".localized
        self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
        self.confirmMobileNumberTF.becomeFirstResponder()
    }
      
    
    @IBAction func amountCloseButtonClick(_ sender: Any) {
         self.amountCloseButton.isHidden = true
         amountTF.text = ""
        self.amountView.layer.borderWidth = 0
        self.amountView.layer.borderColor = UIColor.gray.cgColor
        self.feesCheckView.isHidden = true
        if self.nrcpassport == ""{
            self.amountViewTopConstraint.constant = 0
            self.limitViewTopConstraint.constant = 0
        }
        else{
            self.amountViewTopConstraint.constant = 1
            self.limitViewTopConstraint.constant = 1
        }
        self.nextButton.isHidden = true
        self.nextButtonHeightConstraint.constant = 0
        self.nextButton.setTitle("".localized, for: .normal)
        self.subscribeToShowKeyboardNotifications()
       // self.amountTF.title = "Amount".localized
        self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
        self.amountTF.placeholder = "Enter Transfer Amount".localized
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
          //  self?.contentScroll.isScrollEnabled = false
            self?.amountTF.becomeFirstResponder()
        }
        
    }
    @IBAction func remarkCloseButtonClick(_ sender: Any) {
        remarkTextView.text = ""
        remarkTextView.becomeFirstResponder()
        remarkCloseButton.isHidden = true
        remarkheadingLabel.attributedText = self.returnAttributtedtextview()
    }
    
    
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful {
            if(screen == "BankAccountViewController") {
                var homeVC: BankAccountConfirmationViewController?
                homeVC = UIStoryboard(name: "UnionPay", bundle: nil).instantiateViewController(withIdentifier: "BankAccountConfirmationViewController") as? BankAccountConfirmationViewController
                homeVC!.bankname = bankName
                homeVC!.name = userNameLabel.text ?? ""
                homeVC!.nrcnum = nrcpassport
                homeVC!.amount = amountTF.text ?? ""
                homeVC!.accountnum = mobileNumberTF.text ?? ""
                homeVC!.projectid = projectid
                homeVC!.walletmobilenum = mobileNumberTF.text ?? ""
                homeVC!.feeamount = feeamount
                homeVC!.walletTotalAmount = totalamount
                homeVC!.feetransactionid = feetransactionid
                homeVC!.transactionrefernce = transactionrefrence
                homeVC!.okFeeAmount = okFeeAmount
                homeVC!.bankusertype = bankUserType
                homeVC!.usertype = usertype
                homeVC!.remarkHeight = remarkHeightContraint
                homeVC!.OKFeesTransId = OKFeesTransId
                homeVC!.displaytotalamount = self.totalamountLabel.text ?? ""
                homeVC!.feedisplayamount =  self.fessamountLabel.text ?? ""
                homeVC!.selectedType = self.selectedother
                homeVC!.feesDelegate = self
                if remarkTextView.text == "Remark (Optional)".localized{
                    homeVC!.remark = ""
                }
                else{
                    homeVC!.remark = remarkTextView.text ?? ""
                }
                
                if let homeVC = homeVC {
                    navigationController?.pushViewController(homeVC, animated: true)
                }
            }
        }
   }
    
    @IBAction func infoButtonClick(_ sender: Any) {
        
        
        inFoView.isHidden = false
        backView.isHidden = false
        nextButton.isUserInteractionEnabled = false
    }
    
    
    @IBAction func nextButtonClick(_ sender: Any) {
        
        if mobileNumberTF.text == "09" || mobileNumberTF.text == "" {
            alertViewObj.wrapAlert(title: "OK$", body: "Enter Mobile Number".localized, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        }
        else if amountTF.text == ""{
            alertViewObj.wrapAlert(title: "OK$", body: "Enter Tranasfer Amount".localized, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        }
        else{
            let amout = amountTF.text?.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: ".", with: "") ?? "0"
            let intAmout = Int(amout) ?? 0
            if intAmout < Int(minlimit) ?? 0 {
                alertViewObj.wrapAlert(title: nil, body: "\("Please enter minimum amount".localized) \(minlimit) \("MMK".localized)", img: #imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    self.amountTF.text = ""
                    self.amountCloseButton.isHidden = true
                    self.feesCheckView.isHidden = true
                    self.nextButton.isHidden = true
                    self.nextButtonHeightConstraint.constant = 0
                    self.nextButton.setTitle("".localized, for: .normal)
                    self.subscribeToShowKeyboardNotifications()
                    self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                    self.amountTF.placeholder = "Enter Transfer Amount".localized
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                  //   self.contentScroll.isScrollEnabled = false
                        self?.amountTF.becomeFirstResponder()
                       }
                })
                alertViewObj.showAlert(controller: self)
            }else if intAmout > Int(maxlimit) ?? 0 {
                alertViewObj.wrapAlert(title: nil, body: "\("Maximum You can transfer up to".localized) \(valueamount) \("MMK".localized)", img: #imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    self.amountTF.text = ""
                    self.amountCloseButton.isHidden = true
                    self.feesCheckView.isHidden = true
                    self.nextButton.isHidden = true
                    self.nextButtonHeightConstraint.constant = 0
                    self.nextButton.setTitle("".localized, for: .normal)
                    self.subscribeToShowKeyboardNotifications()
                    self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                    self.amountTF.placeholder = "Enter Transfer Amount".localized
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                  //   self.contentScroll.isScrollEnabled = false
                        self?.amountTF.becomeFirstResponder()
                       }
                })
                alertViewObj.showAlert(controller: self)
                
            }else if NSString(string: self.totalamountLabel.text?.replacingOccurrences(of: ".", with: "").replacingOccurrences(of: ",", with: "") ?? "0").doubleValue > NSString(string: UserLogin.shared.walletBal).doubleValue {
                alertViewObj.wrapAlert(title: nil, body: "Insufficient balance".localized, img: #imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    
                    self.amountTF.text = ""
                    self.amountCloseButton.isHidden = true
                    self.feesCheckView.isHidden = true
                    self.nextButton.isHidden = true
                    self.nextButtonHeightConstraint.constant = 0
                    self.nextButton.setTitle("".localized, for: .normal)
                    self.subscribeToShowKeyboardNotifications()
                    self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                    self.amountTF.placeholder = "Enter Transfer Amount".localized
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                  //   self.contentScroll.isScrollEnabled = false
                        self?.amountTF.becomeFirstResponder()
                       }
                    
                })
                alertViewObj.showAlert(controller: self)
            }
            
            else {
               
                mobileNumberTF.resignFirstResponder()
                confirmMobileNumberTF.resignFirstResponder()
                amountTF.resignFirstResponder()
                remarkTextView.resignFirstResponder()
                //self.FeeCheckApi()
                OKPayment.main.authenticate(screenName: "BankAccountViewController", delegate: self)
            }
        }
        
    }
}


extension BankAccountViewController : SAConatactListViewDelegate {
    
    func showContactList(yAsix: CGFloat, position: String, contactFor: String) {
        if !(self.view.contains(self.contactSuggestionView)) {
            DispatchQueue.main.async {
                self.contactSuggestionView.contactTableView.register(UINib.init(nibName: "ContactSuggestionCell", bundle: Bundle.main), forCellReuseIdentifier: "ContactSuggestionCell")
                self.contactSuggestionView.frame = CGRect(x: 5, y: yAsix, width: (self.view.frame.width - 10) , height: 150)
                self.contactSuggestionView.reduceFrom = position
                self.contactSuggestionView.yAxis = yAsix
                self.contactSuggestionView.contactFor = contactFor
                self.contactSuggestionView.delegate = self
                self.view.bringSubviewToFront(self.contactSuggestionView)
                self.view.addSubview(self.contactSuggestionView)
            }
        }
    }
    
    func removeContactViewFromSurperView() {
        removeContactList()
    }
    
    func removeContactList() {
        if self.view.contains(self.contactSuggestionView) {
            self.contactSuggestionView.removeFromSuperview()
        }
    }
    
    func selectedNumber(flag: String, countryCode: String, number: String, isValid: Bool, contactFor: String) {
        removeContactList()
        println_debug("Number \(number) Country code \(countryCode) Flag\(flag) isValid: \(isValid)")
        if isValid {
            if countryCode == "+95" {
                self.closeButton.isHidden = false
                var myString = number
                myString.remove(at: myString.startIndex)
                mobilenumFormate = "0095"+myString
                print("@@@@@",mobilenumFormate)
                self.mobileNumberTF.text = number
                self.mobileNumberTF.resignFirstResponder()
                self.isSelectedFromContact = true
                self.apicall()
            }else {
                // Invalid Number
            }
        }else {
            self.isSelectedFromContact = false
            // Invlaid Number
        }
        
    }
 
}

extension BankAccountViewController{
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        self.KeyboardSize = (keyboardSize?.height)!
        //let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize!.height + 45, right: 0.0)
      //  self.contentScroll.contentInset = contentInsets
       // self.contentScroll.scrollIndicatorInsets = contentInsets
//        var aRect : CGRect = self.view.frame
//        aRect.size.height -= keyboardSize!.height
//        if amountTF != nil{
//            if (!aRect.contains(amountTF!.frame.origin)){
//                self.contentScroll.scrollRectToVisible(amountTF!.frame, animated: true)
//            }
//            else
//            {
//                if (!aRect.contains(remarkView!.frame.origin)){
//                    self.contentScroll.scrollRectToVisible(remarkView!.frame, animated: true)
//
//                }
//            }
//        }

    }
    @objc func keyboardWillChangeHeight(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        self.KeyboardSize = (keyboardSize?.height)!
        //println_debug("Keyboard changed \(KeyboardSize,keyboardSize?.height))")
        //        classActionButton?.frame.origin.y = self.view.frame.height - KeyboardSize - 54
        if device.type == .iPhoneX || device.type == .iPhoneXR || device.type == .iPhoneXS || device.type == .iPhoneXSMax {
               self.nextbuttonBottomConstraint.constant = KeyboardSize+10
           }
           else {
                 self.nextbuttonBottomConstraint.constant = KeyboardSize
            }
        
       // bottomGetBillBtnConstraint.constant = KeyboardSize
       // self.view.layoutIfNeeded()
    }
    
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.contentScroll.contentInset = contentInsets
        self.contentScroll.scrollIndicatorInsets = contentInsets
        //        classActionButton?.frame.origin.y = self.view.frame.height - 54
        
//        let info : NSDictionary = notification.userInfo! as NSDictionary
//        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
//        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
//            self.contentScroll.contentInset = contentInsets
//            self.contentScroll.scrollIndicatorInsets = contentInsets
//            self.view.endEditing(true)
//        self.contentScroll.isScrollEnabled = false
        
        nextbuttonBottomConstraint.constant = 0
    }
}



extension BankAccountViewController: FeesCheckFailure {
    func callFeesCheckApi(){
      //  FeeCheckApi()
        self.amountCloseButton.isHidden = true
        amountTF.text = ""
       self.amountView.layer.borderWidth = 0
       self.amountView.layer.borderColor = UIColor.gray.cgColor
       self.feesCheckView.isHidden = true
       if self.nrcpassport == ""{
           self.amountViewTopConstraint.constant = 0
           self.limitViewTopConstraint.constant = 0
       }
       else{
           self.amountViewTopConstraint.constant = 1
           self.limitViewTopConstraint.constant = 1
       }
       self.nextButton.isHidden = true
       self.nextButtonHeightConstraint.constant = 0
       self.nextButton.setTitle("".localized, for: .normal)
       self.subscribeToShowKeyboardNotifications()
      // self.amountTF.title = "Amount".localized
       self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
       self.amountTF.placeholder = "Enter Transfer Amount".localized
       DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
         //  self?.contentScroll.isScrollEnabled = false
           self?.amountTF.becomeFirstResponder()
       }
    }
}
