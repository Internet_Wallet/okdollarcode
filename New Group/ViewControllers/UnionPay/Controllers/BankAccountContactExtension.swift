//
//  BankAccountContactExtension.swift
//  OK
//
//  Created by iMac on 14/09/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit


//MARK: - ContactPickerDelegate
extension BankAccountMobileNumberViewController: ContactPickerDelegate {
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        let phoneCount = contact.phoneNumbers.count
        var phoneNumber = ""
        if phoneCount > 0 {
            phoneNumber = contact.phoneNumbers[0].phoneNumber
        }
        
        if phoneNumber.count <= 0 {
           
            self.MobileNumber.text = "09"
            
            mobileNumberView.hideClearButton(hide: true)
            PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
            // self.showErrorAlert(errMessage: "Rejected Number".localized)
          //  self.delegate?.changeColor(color: MyNumberTopup.OperatorColorCode.okDefault, name: "")
            self.delegate?.showAddObjectButton(show: false)
            return
        }
        var str = ""
        str = phoneNumber
        if str.count > 0 {
            str = str.replacingOccurrences(of: "(", with: "")
            str = str.replacingOccurrences(of: ")", with: "")
            str = str.trimmingCharacters(in: .whitespaces)
        }
        if str.hasPrefix("00") {
            _ = str.remove(at: String.Index.init(encodedOffset: 0))
            _ = str.remove(at: String.Index.init(encodedOffset: 0))
            str = "+" + str
        }
        
        let country = self.identifyCountry(withPhoneNumber: str)
        if country.0 == "+95" ||  country.1 == "myanmar" ||  country.0 == "" ||  country.1 == "" {
            let countryObject = Country.init(name: "", code: "myanmar", dialCode: "+95")
            if str.hasPrefix(countryObject.dialCode) {
                str = str.replacingOccurrences(of: countryObject.dialCode, with: "")
                str = "0" + str
            } else {
                if str.hasPrefix("0") {
                    
                } else {
                    str = "0" + str
                }
            }
            let number = PayToValidations().getNumberRangeValidation(str)

            if str.count > number.max || str.count < number.min || number.isRejected {
                mobileNumberView.hideClearButton(hide: true)
               
                self.MobileNumber.text = "09"
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                self.delegate?.showAddObjectButton(show: false)
                self.clearActionType()
                return
            }
            if selectstr == "0"{
                myself = str
                contactnameself = contact.displayName()
            }
            else
            {
                otherperson = str
                contactnameother = contact.displayName()
            }
            
            self.MobileNumber.text = str
            submitButton.isUserInteractionEnabled = true
            submitButton.backgroundColor = UIColor.systemYellow
            self.userNameTF.leftViewMode = .always
            self.userNameTF.leftView = userLeftView
            self.userNameTF.text = contact.displayName()
            namechoosenfromLabel.isHidden = false
            ChoosenFromLabelHeightConstraint.constant = 23.33
            userdetailFieldLabel.text = "Beneficiary Details".localized
            bankaccounttitleLabel.attributedText = self.returnAttributtedString()
            if usertype == 0 {
                if fromFavorite == "Fav"{
                    namechoosenfromLabel.text = "Name From Favorite".localized
                }
                else{
                    namechoosenfromLabel.text = "Name From Contact".localized
                }
                
            }
            else{
                let va = selectionDic.value(forKey: selectstr) as! Bool
                
                if va{
                    namechoosenfromLabel.text = "Name From Contact".localized
                }else{
                    namechoosenfromLabel.text = "Name From Favorite".localized
                }
            }
            
            suggestionTable.isHidden = true
            
//            if fromFavorite == "Faviorite"{
//
//                namechoosenfromLabel.text = "Name From Favorite"
//            }
//            else {
//                namechoosenfromLabel.text = "Name From Contact"
//            }

            userNameTFHeightConstraint.constant = 50
            mobileFiledsViewHeightConstraint.constant = 265
            
//            if str.count > number.max || str.count < number.min || number.isRejected {
//                self.amountConst.constant = 0.00
//                self.bonusPointHeightConstraints.constant = 0.0
//                self.confirmMobConst.constant = 0.00
//                self.amount.text = ""
//                self.mobileNumber.text = "09"
//                self.confimMobile.text = "09"
//                self.amount.rightView = nil
//                self.amount.isHidden = true
//                self.bonusPointTextField.isHidden = true
//                self.confimMobile.isHidden = true
//                self.heightChanges = true
//                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
//                // self.showErrorAlert(errMessage: "Rejected Number".localized)
//                self.delegate?.showAddObjectButton(show: false)
//                return
//            }
            
//            if str.count < number.min {
//                self.amountConst.constant = 0.00
//                self.bonusPointHeightConstraints.constant = 0.0
//                self.confirmMobConst.constant = 0.00
//                self.amount.text = ""
//                self.mobileNumber.text = "09"
//                self.confimMobile.text = "09"
//                self.amount.rightView = nil
//                self.amount.isHidden = true
//                self.bonusPointTextField.isHidden = true
//                self.confimMobile.isHidden = true
//                self.heightChanges = true
//                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
//                // self.showErrorAlert(errMessage: "Rejected Number".localized)
//                self.delegate?.showAddObjectButton(show: false)
//                return
//            }
//
//            if number.isRejected {
//                self.amountConst.constant = 0.00
//                self.bonusPointHeightConstraints.constant = 0.0
//                self.confirmMobConst.constant = 0.00
//                self.amount.text = ""
//                self.mobileNumber.text = "09"
//                self.confimMobile.text = "09"
//                self.amount.rightView = nil
//                self.amount.isHidden = true
//                self.bonusPointTextField.isHidden = true
//                self.confimMobile.isHidden = true
//                self.heightChanges = true
//                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
//                //                self.showErrorAlert(errMessage: "Rejected Number".localized)
//                self.delegate?.showAddObjectButton(show: false)
//                return
//            }
           
            self.mobileNumberView.hideClearButton(hide: true)
        } else {
            self.delegate?.showAddObjectButton(show: false)
            guard let keyWinndow = UIApplication.shared.keyWindow else { return }
            keyWinndow.makeToast("You Selected Other Country Number".localized, point: keyWinndow.center, title: nil, image: nil, completion: nil)
        }
    }
}
