//
//  BankReceiptViewController.swift
//  OK
//
//  Created by vamsi on 7/27/20.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

class BankReceiptViewController: OKBaseController{

    @IBOutlet var navigationView: UIView!
    @IBOutlet var navigationImageView: UIImageView!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var navigationTitleLabel: UILabel!{
        didSet {
            navigationTitleLabel.font = UitilityClass.getZwagiFontWithSize(size: 22.0)
            navigationTitleLabel.text = "Receipt".localized
        }
    }
    
    @IBOutlet var paymentSuccessLabel: UILabel!
    {
        didSet {
            paymentSuccessLabel.font = UitilityClass.getZwagiFontWithSize(size: 17.0)
            paymentSuccessLabel.text = "Transfer Successful".localized
        }
    }
    
    var yAxis : CGFloat = 0.0
    var pdfview = UIView()
    var listTitleValue : [Dictionary<String,String>]?
    
    @IBOutlet var bgView: UIView!
    
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    
    @IBOutlet var bankImageView: UIImageView!
    @IBOutlet var bankNameLabel: UILabel!
    
    @IBOutlet var receiptTabelView: UITableView!
    
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var detailLabel: UILabel!
    
    @IBOutlet var shareButton: UIButton!
    
    @IBOutlet var homeButton: UIButton!
//    {
//         didSet {
//                homeButton.setTitle("DONE".localized, for: .normal)
//            }
//       }
    
    @IBOutlet var favouriteButton: UIButton!
    
    @IBOutlet var contactButton: UIButton!
    
    @IBOutlet var userdetailView: UIView!
    
    @IBOutlet var transferbyLabel: UILabel!{
        didSet{
            transferbyLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
            transferbyLabel.text = "Transfer By".localized
        }
    }
    
    @IBOutlet var okdollarNameView: UIView!
    @IBOutlet var okdollarnumView: UIView!
    
    @IBOutlet var benficaryLabel: UILabel!{
        didSet{
            benficaryLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
            benficaryLabel.text = "Beneficiary Details".localized
        }
    }
    
    
    @IBOutlet var nameView: UIView!
    @IBOutlet var accountView: UIView!
    
    @IBOutlet var transactiondetailsLabel: UILabel!{
        didSet{
            transactiondetailsLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
            transactiondetailsLabel.text = "Transaction Details".localized
        }
    }
    
    @IBOutlet var transactionView: UIView!
    @IBOutlet var amountView: UIView!
    @IBOutlet var feesView: UIView!
    @IBOutlet var totalamountView: UIView!
    @IBOutlet var remarkView: UIView!
    
    @IBOutlet var okdollarnameLabel: UILabel!{
        didSet {
            okdollarnameLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
            okdollarnameLabel.text = "Name".localized
        }
    }
    @IBOutlet var okdollarusernameLabel: UILabel!
    
    
    @IBOutlet var okdollarnumberLabel: UILabel!{
        didSet {
            okdollarnumberLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
            okdollarnumberLabel.text = "OK$ Mobile".localized
        }
    }
    @IBOutlet var okdollarusermobileLabel: UILabel!
    
    
    @IBOutlet var nameLabel: UILabel!
    {
        didSet {
            nameLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
            nameLabel.text = "Name".localized
        }
    }
    @IBOutlet var userNameLabel: UILabel!
    @IBOutlet var receiptSavedLabel: UILabel!
       {
           didSet {
               receiptSavedLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
               receiptSavedLabel.text = "Receipt already saved in gallery".localized
           }
       }
    @IBOutlet var accountNumberLabel: UILabel!
    {
        didSet {
            accountNumberLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
            accountNumberLabel.text = "A/C Number.".localized
        }
    }
    @IBOutlet var userAccountnumLabel: UILabel!
    
//    @IBOutlet var nrcNumberLabel: UILabel!
//    {
//        didSet {
//            nrcNumberLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
//            nrcNumberLabel.text = "NRC Number".localized
//        }
//    }
  //  @IBOutlet var userNrcNumLabel: UILabel!
    
    @IBOutlet var transactionLabel: UILabel!
    {
           didSet {
               transactionLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
               transactionLabel.text = "OK$ Txn ID".localized
           }
       }
//    @IBOutlet var banktransactionLAbel: UILabel!{
//        didSet {
//            banktransactionLAbel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
//            banktransactionLAbel.text = "Bank Transaction ID".localized
//        }
//    }
    
    @IBOutlet var userTransactionLabel: UILabel!
    
  //  @IBOutlet var userbanktrasactionLabel: UILabel!
    
    @IBOutlet var amountLabel: UILabel!
    {
        didSet {
             amountLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
            amountLabel.text = "Amount".localized
        }
    }
    @IBOutlet var userAmountLabel: UILabel!
    
    @IBOutlet var feesLabel: UILabel!{
        didSet {
            feesLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
            feesLabel.text = "Fees".localized
        }
    }
    
    @IBOutlet var userfeesLabel: UILabel!
    
    @IBOutlet var totalamountLabel: UILabel!{
        didSet {
            totalamountLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
            totalamountLabel.text = "Total Amount".localized
        }
    }
    
    @IBOutlet var usertotalamountLabel: UILabel!
    
    @IBOutlet var remarksLabel: UILabel!{
        didSet {
            remarksLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
            remarksLabel.text = "Remarks :".localized
        }
    }
    
    @IBOutlet var userremarkslabel: UILabel!
    
    @IBOutlet var okdollarBalnceLabel: UILabel!
    
    @IBOutlet var userremarkHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var rmearkViewHeightContraint: NSLayoutConstraint!
    //    @IBOutlet var userdetailsHeightConstraint: NSLayoutConstraint!
//    @IBOutlet var nrcViewHeightConstraint: NSLayoutConstraint!
//    @IBOutlet var transactionViewtopConstraint: NSLayoutConstraint!
//
    @IBOutlet var debitedamountLabel: UILabel!
    
    @IBOutlet var feesheightConstraint: NSLayoutConstraint!
    
    @IBOutlet var userdetailsHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var feesTopConstraint: NSLayoutConstraint!
    
    var remarkHeight = 0.0
    
    @IBOutlet  var viewReceipt: UIView!
    var transactionid = ""
    var banktransactionId = ""
    var amount = ""
    var name = ""
    var nrcnumber = ""
    var accountnumber = ""
    var date = ""
    var time = ""
    var bankname = ""
    
    var fees = ""
    var totalamount = ""
    var remarks = ""
    
    var isInFav = false
    var isInContact = false
    
    var buttonselection = ""
    var SelectionType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
        self.bgView.frame =  CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0)
        
        self.backButton.isHidden = false
        
      

        let urlFront = userDef.value(forKey: "UnionPayLogo")
        let imageUrl = URL(string: urlFront as! String)
        self.bankImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "bank"))
        
      //  self.navigationImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "bank"))
        
         accountNumberLabel.text = "A/C Number\nMobile".localized
        
     //   if SelectionType == "other"{
            feesView.isHidden = false
            feesTopConstraint.constant = 1
            feesheightConstraint.constant = 48
            userdetailsHeightConstraint.constant = 477
//        }
//        else{
//            feesView.isHidden = true
//            feesTopConstraint.constant = 0
//            feesheightConstraint.constant = 0
//            userdetailsHeightConstraint.constant = 428
//        }
        
        
        
        self.userdetailView.layer.cornerRadius = 2.0
      //  self.userdetailView.layer.shadowColor = UIColor(red: 75/255, green: 129/255, blue: 198/255, alpha: 1.0).cgColor
      //  self.userdetailView.layer.shadowOffset = CGSize(width: 2, height: 2)
      //  self.userdetailView.layer.shadowOpacity = 1.0
        self.userdetailView.layer.shadowRadius = 1.0
        self.userdetailView.layer.masksToBounds = false
        
        
        let dateStr = date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        let currentDate = dateFormatter.date(from: dateStr)
        dateFormatter.dateFormat = "EEE, dd-MMM-yyyy"
        let finalDate = dateFormatter.string(from: currentDate ?? Date())
        println_debug(finalDate)
        
        
     //   bankNameLabel.text = bankname
        
        if bankname == "MAB Bank"{
            let fullName    = bankname
            let fullNameArr = fullName.components(separatedBy: " ")

            let name    = fullNameArr[0]
         //   let surname = fullNameArr[1]
            
            bankNameLabel.text = "Myanma Apex Bank"
        }
        else{
            bankNameLabel.text = bankname
        }
        
    
        dateLabel.text = finalDate
        timeLabel.text = time
        
        userNameLabel.text = name
        userAccountnumLabel.text = accountnumber.replacingCharacters(in: ...accountnumber.startIndex, with: "+95")
        userTransactionLabel.text = transactionid
     //   userbanktrasactionLabel.text = banktransactionId
      //  userAmountLabel.text = wrapAmountWithCommaDecimal(key: amount) + " " + "MMK"
        
        isInFav = checkForFavorites(accountnumber, type: "PAYTO").isInFavorite
        isInContact = checkForContact(accountnumber).isInContact
        
        self.addFavUI(isInFav: isInFav)
        self.addContactUI(isInContact: isInContact)
        
        
        let amountNew = wrapAmountWithCommaDecimal(key: totalamount)//wrapAmountWithCommaDecimal(key: totalamount)
        let finalAmount = amountNew.components(separatedBy: ".")
        
        if finalAmount.count>0{
            if finalAmount.indices.contains(0) && finalAmount.indices.contains(1){
                debitedamountLabel.attributedText = returnAttributtedtextview(nameFont: "HelveticaNeue-Bold",textOne: finalAmount[0], textTwo: ".\(finalAmount[1]) MMK" ,textThree: "", textfourth: "", sizeOne: 17, sizeTwo: 13, color: .red)
                usertotalamountLabel.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: finalAmount[0], textTwo: "" + "",textThree: " MMK", textfourth: "", sizeOne: 15, sizeTwo: 12, color: .black)
                
            }else{
                debitedamountLabel.attributedText = returnAttributtedtextview(nameFont: "HelveticaNeue-Bold",textOne: finalAmount[0], textTwo: " MMK",textThree: "", textfourth: "", sizeOne: 17, sizeTwo: 13, color: .red)
                usertotalamountLabel.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: finalAmount[0], textTwo: "",textThree: " MMK", textfourth: "", sizeOne: 15, sizeTwo: 12, color: .black)
            }
            
        }
        
        
        
        let amountData = wrapAmountWithCommaDecimal(key: amount)
        let finalAmountCopy = amountData.components(separatedBy: ".")
        
        if finalAmountCopy.count>0{
            if finalAmountCopy.indices.contains(0) && finalAmountCopy.indices.contains(1){
                userAmountLabel.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: finalAmountCopy[0], textTwo: "" ,textThree: " MMK", textfourth: "", sizeOne: 15, sizeTwo: 12, color: .black)
            }else{
                userAmountLabel.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: finalAmountCopy[0], textTwo: "",textThree: " MMK", textfourth: "", sizeOne: 15, sizeTwo: 12, color: .black)
            }
            
        }
        
        
        if wrapAmountWithCommaDecimal(key: fees) == "0"{
            userfeesLabel.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: wrapAmountWithCommaDecimal(key: fees), textTwo: "",textThree: " MMK", textfourth: "", sizeOne: 15, sizeTwo: 12, color: .black)
            
        }else{
            let amountData = wrapAmountWithCommaDecimal(key: fees)
            let finalAmountCopy = amountData.components(separatedBy: ".")
            if finalAmountCopy.count>0{
                if finalAmountCopy.indices.contains(0) && finalAmountCopy.indices.contains(1){
                    userfeesLabel.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: finalAmountCopy[0], textTwo: "" ,textThree: " MMK", textfourth: "", sizeOne: 15, sizeTwo: 12, color: .black)
                }else{
                    userfeesLabel.attributedText = returnAttributtedtextview(nameFont: appFont,textOne: finalAmountCopy[0], textTwo: "",textThree: " MMK", textfourth: "", sizeOne: 15, sizeTwo: 12, color: .black)
                }
                
            }
            
        }
        
        
      //  userfeesLabel.text = wrapAmountWithCommaDecimal(key: fees) + " " + "MMK"
      //  usertotalamountLabel.text = wrapAmountWithCommaDecimal(key: totalamount) + " " + "MMK"
        userremarkslabel.text = remarks
        
        //debitedamountLabel.text = wrapAmountWithCommaDecimal(key: totalamount)
        
        okdollarusernameLabel.text = UserModel.shared.name
        okdollarusermobileLabel.text = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "+95")
        
        
        
            
        let okBal = Double(UserLogin.shared.walletBal) ?? 0.0
        let Dtotalamount = Double(totalamount) ?? 0.0
          let finalbalance = okBal - Dtotalamount
        let okBalance = String(format:"%.02f", finalbalance).components(separatedBy: ".")
            if okBalance.count>0 {
                if okBalance.indices.contains(0) && okBalance.indices.contains(1){
                    okdollarBalnceLabel.attributedText =   returnAttributtedtextview(nameFont: appFont,textOne: wrapAmountWithCommaDecimal(key:okBalance[0]), textTwo: "." + okBalance[1],textThree: " MMK", textfourth: "OK$ Balance : ".localized, sizeOne: 18, sizeTwo: 15, color: UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0))
                }else{
                    
                    okdollarBalnceLabel.attributedText =    returnAttributtedtextview(nameFont: appFont,textOne: wrapAmountWithCommaDecimal(key:okBalance[0]), textTwo: ".00",textThree: " MMK", textfourth: "OK$ Balance : ".localized, sizeOne: 18, sizeTwo: 15, color: UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0))
                }
                
            }
            
       
        
        
       // okdollarBalnceLabel.text =  "OK$ Balance :" + UserLogin.shared.walletBal + " MMK"
        
        if remarks != ""{
            userremarkHeightConstraint.constant = CGFloat(remarkHeight)
            rmearkViewHeightContraint.constant = CGFloat(remarkHeight + 50)
            
        }else{
            //
        }
        
       
        
//        if nrcnumber != ""{
//            userNrcNumLabel.text = nrcnumber
//            nrcNumberLabel.isHidden = false
//            userNrcNumLabel.isHidden = false
//            nrcView.isHidden = false
//            nrcViewHeightConstraint.constant = 48
//            transactionViewtopConstraint.constant = 2
//            userdetailsHeightConstraint.constant = 298
//        }
//        else
//        {
//            nrcNumberLabel.isHidden = true
//            userNrcNumLabel.isHidden = true
//            nrcView.isHidden = true
//            nrcViewHeightConstraint.constant = 0
//            transactionViewtopConstraint.constant = 0
//            userdetailsHeightConstraint.constant = 248
//        }
        
        //Generate PDF
        pdfview.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30)
        self.pdfview.backgroundColor = #colorLiteral(red: 0.9285323024, green: 0.9334669709, blue: 0.9376094937, alpha: 1)
        self.bgView.backgroundColor = .white
       
        //Create PDF
        self.createPDF()
        
    }
    
    
   
    override func viewDidAppear(_ animated: Bool)  {
        super.viewDidAppear(true)
//        if device.type == .iPhoneX || device.type == .iPhoneXR || device.type == .iPhoneXS || device.type == .iPhoneXSMax || device.type == .iPhone11 {
//            bottomViewHeightConstraint.constant = 250
//        }else {
//            bottomViewHeightConstraint.constant = 90
//        }
        
        let image = captureScreen(view: viewReceipt)
         UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)

    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            alertViewObj.wrapAlert(title: "Error".localized, body: error.localizedDescription, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        } else {
            println_debug("Your altered image has been saved to your photos.")
        }
    }
    func shareQRWithDetail() {
          let image = captureScreen(view: viewReceipt)
          let imageToShare = [image]
          let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
          activityViewController.popoverPresentationController?.sourceView = self.view
          DispatchQueue.main.async {
              self.present(activityViewController, animated: true, completion: nil)
          }
      }
    
    func sharePdfWithDetail() {
            let image = captureScreen(view: bgView)
            let imageToShare = [image]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            DispatchQueue.main.async {
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
    
    @IBAction func doneButtonClick(_ sender: Any) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func sharebuttonClick(_ sender: Any) {
          //  self.shareQRWithDetail()
            self.sharePdfWithDetail()
    }
    
    @IBAction func favrouiteClick(_ sender: Any) {
        self.addFavorite()
    }
    @IBAction func contactClick(_ sender: Any) {
        self.addContact()
    }
    
    
    @IBAction func onClickBackAction(_: UIButton) {
        
        for controller in self.navigationController!.viewControllers as Array {
                 if controller.isKind(of: AddWithdrawMoneyVC.self) {
                     self.navigationController!.popToViewController(controller, animated: true)
                     break
                 }
             }
    }
    
    
    func addFavUI(isInFav: Bool) {
            if isInFav {
                favouriteButton.setImage(UIImage(named:"overseas_heart_w"), for: .normal)
            } else {
                favouriteButton.setImage(UIImage(named:"overseas_heart"), for: .normal)
            }
        
    }
    
    func addContactUI(isInContact: Bool) {
            if isInContact {
                contactButton.setImage(UIImage(named:"overseas_contact_w"), for: .normal)
            } else {
                contactButton.setImage(UIImage(named:"overseas_contact"), for: .normal)
            }
        
    }
    
    
    func addFavorite() {
        if isInFav {
          //  PaytoConstants.alert.showToast(msg: "Contact already added in Favorites".localized)
            //
            alertViewObj.wrapAlert(title: nil, body: "Contact already added in Favorites".localized, img: #imageLiteral(resourceName: "topup_number"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
            
        } else {
            self.favorite_recipt()
        }
    }
    
    fileprivate  func favorite_recipt() {
        
        //let favorite = FavoriteDBManager.init()
        //let contacts = favorite.fetchRecordsForFavoriteContactsEntity()
      
            //let dict = "comments"
            let number = userAccountnumLabel.text ?? ""//dict.safeValueForKey("destination") as? String
            let name   = userNameLabel.text ?? ""
            let amount = self.amount//dict.safeValueForKey("amount") as? String
            let type = "PAYTO"
           
        let vc = self.addFavoriteController(withName: name, favNum: number , type: type, amount: amount)
            if let vcs = vc {
                vcs.delegate = self
                vcs.modalPresentationStyle = .overCurrentContext
                self.present(vcs, animated: true, completion: nil)
            }
        
    }
    
    func addContact() {
        if isInContact {
           // PaytoConstants.alert.showToast(msg: "Contact Already Added".localized)
            alertViewObj.wrapAlert(title:nil, body: "Contact Already Added".localized, img: #imageLiteral(resourceName: "topup_number"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        } else {
            self.addContact_recipt()
        }
    }
       
    fileprivate  func addContact_recipt() {
        
            let store     = CNContactStore()
            let contact   = CNMutableContact()
            let phone     = accountnumber  //dict.safeValueForKey("destination") as? String
            //let name      = "Change Name from comment"//dict.safeValueForKey("merchantname") as? String ?? nameValue
            let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue: phone))
            contact.phoneNumbers = [homePhone]
            contact.givenName = userNameLabel.text ?? ""
           
            let controller = CNContactViewController.init(forNewContact: contact)
            controller.contactStore = store
            controller.delegate     = self
            controller.title        = "Add Contact".localized
            let navControler = UINavigationController.init(rootViewController: controller)
            navControler.modalPresentationStyle = .overCurrentContext
            self.present(navControler, animated: true, completion: nil)
    }
    
    

}


//MARK:- InternationalReceiptDelegate
extension BankReceiptViewController: PTFavoriteActionDelegate {
    func favoriteAdded() {
//        if isInFavArray.count >= currentIndex {
//        isInFavArray[currentIndex-1] = true
//        addFavUI(isInFav: true)
  
        
        self.isInFav = true
        addFavUI(isInFav: true)
    }
}

//MARK: - CNContactViewControllerDelegate
extension BankReceiptViewController: CNContactViewControllerDelegate {

    func changeCancelImplementation() {
        let originalSelector = Selector(("editCancel:"))
        let swizzledSelector = #selector(CNContactViewController.cancelHack)
        if let originalMethod = class_getInstanceMethod(object_getClass(CNContactViewController()), originalSelector),
           let swizzledMethod = class_getInstanceMethod(object_getClass(CNContactViewController()), swizzledSelector) {
            method_exchangeImplementations(originalMethod, swizzledMethod)
        }
    }
    
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        if let newContact = contact {
            
//            if isInContactArray.count == 1{
//                if !isInContactArray[currentIndex]{
//                    isInContactArray[currentIndex] = true
//                }
//            }else{
//                for i in 0..<isInContactArray.count{
//                    if !isInContactArray[i] {
//                        isInContactArray[i] = true
//                        break
//                    }
//                }
//            }
            //addContactUI(isInContact: true)
            //UPdate button color with selected
         //   contactButton.setImage(UIImage(named:"overseas_contact_w"), for: .normal)
            self.isInContact = true
            addContactUI(isInContact: true)
            okContacts.append(newContact)
            
            //if isInContactArray.count >= currentIndex {
             //   isInContactArray[currentIndex-1] = true
                
          //  }
        }
        viewController.delegate = nil
        self.dismiss(animated: true, completion: nil)
        //self.navigationController?.popViewController(animated: true)
    }
}




