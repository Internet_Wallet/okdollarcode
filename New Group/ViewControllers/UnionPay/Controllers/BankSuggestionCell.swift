//
//  BankSuggestionCell.swift
//  OK
//
//  Created by Tushar Lama on 23/10/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class BankSuggestionCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var mobileLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
