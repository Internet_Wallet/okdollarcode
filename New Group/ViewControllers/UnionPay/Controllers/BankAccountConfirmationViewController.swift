//
//  BankAccountConfirmationViewController.swift
//  OK
//
//  Created by vamsi on 7/27/20.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit
import SDWebImage

class BankAccountConfirmationViewController: UIViewController,  ApiRequestProtocol{
    var feesDelegate: FeesCheckFailure?
    @IBOutlet var navigationView: UIView!
    @IBOutlet var navigationImageView: UIImageView!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var payButton: UIButton!
    {
        didSet {
            payButton.titleLabel?.font = UitilityClass.getZwagiFontWithSize(size: appButtonSize)
            payButton.setTitle("TransferSaiSai".localized, for: .normal)
        }
    }
    @IBOutlet var bankLogoImageView: UIImageView!
    @IBOutlet var bankNameLabel: UILabel!
    
    @IBOutlet var userDetailsView: UIView!
    
    
    @IBOutlet var nameLabel: UILabel!
    {
          didSet {
            nameLabel.font = UitilityClass.getZwagiFontWithSize(size: 16.0)
            nameLabel.text = "Name".localized
          }
      }
    @IBOutlet var userNameLabel: UILabel!
    
    @IBOutlet var accountLabel: UILabel!
    {
        didSet {
             accountLabel.font = UitilityClass.getZwagiFontWithSize(size: 14.0)
             accountLabel.text = "Account Number".localized
        }
    }
    @IBOutlet var accountNumLabel: UILabel!
    
    @IBOutlet var amountLabel: UILabel!{
        didSet {
            amountLabel.font = UitilityClass.getZwagiFontWithSize(size: 14.0)
            amountLabel.text = "Transfer Amount".localized
        }
    }
    
    @IBOutlet var transferdetailLabel: UILabel!{
        didSet {
            transferdetailLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
            transferdetailLabel.text = "Transfer Details".localized
        }
    }
    
    @IBOutlet var amountNumLabel: UILabel!
    
    @IBOutlet var navigationtitleLabel: UILabel!
    {
        didSet {
            navigationtitleLabel.font = UitilityClass.getZwagiFontWithSize(size: 18.0)
            navigationtitleLabel.text = "Check Details".localized //"Confirmation UnionPay".localized
        }
    }
    
    @IBOutlet var feesLabel: UILabel!{
        didSet {
            feesLabel.font = UitilityClass.getZwagiFontWithSize(size: 14.0)
            feesLabel.text = "Fees".localized
        }
    }
    
    @IBOutlet var feesNumLabel: UILabel!
    
    @IBOutlet var totalAmountLabel: UILabel!{
        didSet {
            totalAmountLabel.font = UitilityClass.getZwagiFontWithSize(size: 14.0)
            totalAmountLabel.text = "Total Amount".localized
        }
    }
    
    @IBOutlet var totalamountNumLabel: UILabel!
    
    @IBOutlet var remarksLabel: UILabel!{
        didSet {
            remarksLabel.font = UitilityClass.getZwagiFontWithSize(size: 14.0)
            remarksLabel.text = "Remarks :".localized
        }
    }
    
    @IBOutlet var benficairyLabel: UILabel!{
        didSet {
            benficairyLabel.font = UitilityClass.getZwagiFontWithSize(size: 15.0)
            benficairyLabel.text = "Beneficiary Details".localized
        }
    }
    
    @IBOutlet var userremarksLabel: UILabel!
    
    @IBOutlet var remarkLabelHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var remarksViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var userdetailsHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var feesTopConstraint: NSLayoutConstraint!
    @IBOutlet var feesheightConstraint: NSLayoutConstraint!
    @IBOutlet var feesMMkLabel: UILabel!
    @IBOutlet var feesicon: UIImageView!
    
    var name = ""
    var accountnum = ""
    var nrcnum = ""
    var amount = ""
    var bankname = ""
    var projectid = ""
    var walletmobilenum = ""
    var OKFeesTransId = ""
    var walletTotalAmount = ""
    var feeamount = ""
    var feetransactionid = ""
    var transactionrefernce = ""
    var bankusertype = ""
    var okFeeAmount = ""
    
    var comments = ""
    var walletamount = ""
    var parameters:[String : Any] = [:]
    
    var usertransactionId = ""
    var userbanktransactionId = ""
    var username = ""
    var usernrc = ""
    var useramount = ""
    var useraccountNum = ""
    var transaciondate = ""
    var trasactiontime = ""
    var usertype = 0
    var displaytotalamount = ""
    var feedisplayamount = ""
    
    var datetime = ""
    
    var remark = ""
    var remarkHeight = 0.0
    
    var selectedType = ""
    
    override func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
        
        let urlFront = userDef.value(forKey: "UnionPayLogo")
        let imageUrl = URL(string: urlFront as! String)
        self.bankLogoImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "bank"))
       
    //    bankNameLabel.text = bankname
        
        
        if bankname == "MAB Bank"{
            let fullName    = bankname
            let fullNameArr = fullName.components(separatedBy: " ")

            let name    = fullNameArr[0]
         //   let surname = fullNameArr[1]
            
            bankNameLabel.text = "Myanma Apex Bank"
        }
        else{
            bankNameLabel.text = bankname
        }
        

      //  bankNameLabel.text = bankname
        userNameLabel.text = name
        accountNumLabel.text = accountnum
        amountNumLabel.text = amount
        
      //  okdollarNameLabel.text = UserModel.shared.name
     //   okdollaruserNumLabel.text = UserModel.shared.mobileNo
        
        feesNumLabel.text = feedisplayamount//wrapAmountWithCommaDecimal(key: feeamount)
        totalamountNumLabel.text = displaytotalamount //wrapAmountWithCommaDecimal(key: displaytotalamount)
        userremarksLabel.text = remark
        
        if remark != ""{
            if remark == "Enter Remarks  (optional)"{
                userremarksLabel.text = ""
                remark = ""
                remarksViewHeightConstraint.constant = 60
                remarkLabelHeightConstraint.constant = 22
            }
            else{
                userremarksLabel.text = remark
                remarkLabelHeightConstraint.constant = CGFloat(remarkHeight)
                remarksViewHeightConstraint.constant = CGFloat(remarkHeight + 30)
            }
           
        }
        else{
            userremarksLabel.text = ""
            remarksViewHeightConstraint.constant = 60
            remarkLabelHeightConstraint.constant = 22
           
        }
        
 //       if selectedType == "other"{
            userdetailsHeightConstraint.constant = 279
            feesTopConstraint.constant = 1
            feesheightConstraint.constant = 60
            feesMMkLabel.isHidden = false
            feesicon.isHidden = false
   //     }
//        else{
//            userdetailsHeightConstraint.constant = 218
//            feesTopConstraint.constant = 0
//            feesheightConstraint.constant = 0
//            feesMMkLabel.isHidden = true
//            feesicon.isHidden = true
//        }
       
        
       
       // comments = "SEND MONEY TO WALLET: Sai Sai Payment Wallet, \(walletmobilenum), \(UserModel.shared.name), “SAV”, “No.459, Ground Floor, Merchant Road,"
        comments = "OKDollar to \(bankname)"
        
        walletamount = wrapAmountWithCommaDecimal(key: walletTotalAmount).replacingOccurrences(of: ",", with: "")
            
          //  NSString(string: (walletTotalAmount.replacingOccurrences(of: ",", with: "")).replacingOccurrences(of: ".", with: "")) as String
        
        if appDelegate.checkNetworkAvail(){
            print("Internet Connection Available!")
        }else{
            print("Internet Connection not Available!")
             alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
             alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
                 
             })
             alertViewObj.showAlert(controller:self)
        }
        
        
    }
    @IBAction func backButtonClick(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
//    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
//           if isSuccessful {
//              if(screen == "BankAccountConfirmation") {
//                   if appDelegate.checkNetworkAvail(){
//                        self.apicall()
//                    }else{
//                        print("Internet Connection not Available!")
//                    alertViewObj.wrapAlert(title: "OK$", body: "check internet connection".localized, img: nil)
//                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                    })
//                    alertViewObj.showAlert(controller: self)
//                    }
//               }
//         }
//       }
    
    @IBAction func payButtonClick(_ sender: Any) {
     
     //   let appVersion = String.init(format: "Version %@, Build (\(buildNumber))", Bundle.main.releaseVersionNumber)
        
        
//            CurrentLong = geoLocManager.currentLongitude ?? "0.0"
//            currentLat = geoLocManager.currentLatitude ?? "0.0"
        
        parameters = ["AppInfo": [
                        "OSType": UserModel.shared.osType,
                        "AppVersion": buildVersion,
                        "CellId": "00",
                        "Latitude": geoLocManager.currentLongitude ?? "0.0",
                        "Longitude": geoLocManager.currentLatitude ?? "0.0",
                        "MobileNumber": UserModel.shared.mobileNo,
                        "MsId": msid,
                        "ProfileImg": UserModel.shared.proPic,
                        "SecureToken": UserLogin.shared.token,
                        "SimId": UserModel.shared.simID
                      ],
                      "BankAcount":[
                        "Amount":walletamount,
                        "BankAccountNumber":walletmobilenum,
                        "Comments":comments,
                        "Name": name,
                        "Nrc": nrcnum,
                        "OkAccountNumber": UserModel.shared.mobileNo,
                        "BankMobileNumber": walletmobilenum,
                        "Password": ok_password ?? "",
                        "UserType":usertype,
                        "TotalAmount":walletamount,
                        "FeeAmount":feeamount,
                        "FeeTransactionRefference":transactionrefernce,
                        "TransactionId": feetransactionid,
                        "BankUserType":bankusertype,
                        "Channel":"0",
                        "OKFeeAmount":okFeeAmount,
                        "ProjectId": projectid,
                        //"OKFeesTransId": OKFeesTransId,
                        "_v":"V2"
                   ]
                 ]
        
        
      
       
        print("&&&&&",parameters)
         self.apicall()
       /// if UserLogin.shared.loginSessionExpired {
         //   OKPayment.main.authenticate(screenName: "BankAccountConfirmation", delegate: self)
//        }
//        else{
//
//            if appDelegate.checkNetworkAvail(){
//
//            }else{
//                print("Internet Connection not Available!")
//                alertViewObj.wrapAlert(title: "OK$", body: "check internet connection".localized, img: nil)
//                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                })
//                alertViewObj.showAlert(controller: self)
//            }
//
//        }
        
        
//        var homeVC: BankReceiptViewController?
//
//       homeVC = UIStoryboard(name: "UnionPay", bundle: nil).instantiateViewController(withIdentifier: "BankReceiptViewController") as? BankReceiptViewController
//       homeVC!.name = self.name
//       homeVC!.nrcnumber = self.nrcnum
//       homeVC!.accountnumber = accountnum
//       homeVC!.amount = amount
//       homeVC!.transactionid = "2009467742"
//       homeVC!.banktransactionId = "2009467742"
//       homeVC!.date = ""
//       homeVC!.time = ""
//       homeVC!.bankname = self.bankname
//       homeVC!.fees = self.feeamount
//       homeVC!.totalamount = self.walletamount
//       homeVC!.remarks = self.remark
//       homeVC!.remarkHeight = self.remarkHeight
         
        
        do {
            if UserDefaults.standard.object(forKey: "WALLET") != nil{
                let storedObjItem = UserDefaults.standard.object(forKey: "WALLET")
                let storedItems = try JSONDecoder().decode([Wallet].self, from: storedObjItem as! Data)
                var walletArray = [Wallet]()
                for i in 0..<storedItems.count{
                    walletArray.append(storedItems[i])
                }
                
                var contain = false
                for i in 0..<walletArray.count{
                    if walletArray[i].number == accountnum{
                        contain = true
                        break
                    }
                    contain = false
                }
                
                if !contain{
                    walletArray.append(Wallet.init(number: accountnum, name: self.name, projectID: projectid))
                }
                
                if let encoded = try? JSONEncoder().encode(walletArray) {
                    UserDefaults.standard.set(encoded, forKey: "WALLET")
                }else{
                    print("not able to store")
                }
            }else{
                //this will work for first time
                let itemA: Wallet = Wallet.init(number: accountnum, name: self.name, projectID: projectid)
                //Storing Items
                var obj = [Wallet]()
                obj.append(itemA)
                if let encoded = try? JSONEncoder().encode(obj) {
                    UserDefaults.standard.set(encoded, forKey: "WALLET")
                }else{
                    print("not able to store")
                }
            }
        } catch let err {
            print(err)
        }
        
//           if let homeVC = homeVC {
//               self.navigationController?.pushViewController(homeVC, animated: true)
//       }

         
    }
    
    func apicall(){
           
         progressViewObj.showProgressView()
        
        let apiReqObj = ApiRequestClass()
        apiReqObj.customDelegate = self
        let bankwalletTopUpurl = getUrl(urlStr:Url.bankwalletTopup , serverType: .UnionPayBankDetailsapi)
        
        DispatchQueue.main.async{
                
            apiReqObj.sendHttpRequest(requestUrl:bankwalletTopUpurl, requestData: self.parameters, requestType: RequestTypeObj.RequestTypePayWallet, httpMethodName: "POST")
           }
       }
    func httpResponse(responseObj: Any, reqType: RequestTypeObj)
        {
            DispatchQueue.main.async{
                  
            progressViewObj.removeProgressView()
                  
             if let respMsg = responseObj as? String
               {
               alertViewObj.wrapAlert(title: "OK$", body: respMsg, img: nil)
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                alertViewObj.showAlert(controller: self)
               }
             else
              {
               var respDict = [String:Any]()
                      if responseObj is [Any]
                      {
                         // self.tokenarray = responseObj as! [Any]
                      }
                      else if responseObj is [String:Any]
                      {
                          respDict = responseObj as! Dictionary<String,Any>
                          
                      }
                      
                    if reqType == RequestTypeObj.RequestTypePayWallet
                      {
                          if respDict.count > 0
                          {
                            print ("%%%%%",respDict)
                          let code = respDict["code"] as? Int
                           print("#####",code as Any)
                           if let isValidCode = code {
                             if isValidCode == 200 {
                              let itemDict:[String:Any] = respDict["data"] as!  Dictionary<String,Any>
                                
                                self.usertransactionId = itemDict["OkTransactionId"] as? String ?? ""
                                self.userbanktransactionId = itemDict["BankTransactionId"] as? String ?? ""
                                self.useramount = itemDict["Amount"] as? String ?? ""
                                self.useraccountNum = itemDict["BankAccountNumber"] as? String ?? ""
                                self.datetime = itemDict["ResponseCts"] as? String ?? ""
                                let myString: String = self.datetime
                                let myStringArr = myString.components(separatedBy: " ")//componentsSeparatedByString(" ")
                                
                                let date: String = myStringArr [0]
                                let time: String = myStringArr [1]
                                
                                 var homeVC: BankReceiptViewController?

                                homeVC = UIStoryboard(name: "UnionPay", bundle: nil).instantiateViewController(withIdentifier: "BankReceiptViewController") as? BankReceiptViewController
                                homeVC!.name = self.name
                                homeVC!.nrcnumber = self.nrcnum
                                homeVC!.accountnumber = self.useraccountNum
                                homeVC!.amount = self.walletamount//self.useramount
                                homeVC!.transactionid = self.usertransactionId
                                homeVC!.banktransactionId =  self.userbanktransactionId
                                homeVC!.date = date
                                homeVC!.time = time
                                homeVC!.bankname = self.bankname
                                homeVC!.fees = self.feedisplayamount//self.feeamount
                                homeVC!.totalamount = self.useramount//self.walletamount
                                homeVC!.remarks = self.remark
                                homeVC!.remarkHeight = self.remarkHeight
                                homeVC!.SelectionType = self.selectedType

                                    if let homeVC = homeVC {
                                        self.navigationController?.pushViewController(homeVC, animated: true)
                                }
                                
                              
                                
                            }
                            else
                            {
                               alertViewObj.wrapAlert(title: "", body: (respDict["message"] as? String)!, img: #imageLiteral(resourceName: "alert-icon"))
                               alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                self.feesDelegate?.callFeesCheckApi()
                                self.navigationController?.popViewController(animated: true)
                               })
                               alertViewObj.showAlert(controller: self)
                              }
                            }
                            else
                             {
                              alertViewObj.wrapAlert(title: "", body: (respDict["message"] as? String)!, img: #imageLiteral(resourceName: "alert-icon"))
                              alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                self.feesDelegate?.callFeesCheckApi()
                                self.navigationController?.popViewController(animated: true)
                              })
                              alertViewObj.showAlert(controller: self)
                            }
                            
                             
                          }
                          else
                          {
                              alertViewObj.wrapAlert(title: "", body: (respDict["message"] as? String)!, img: #imageLiteral(resourceName: "alert-icon"))
                              alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                self.feesDelegate?.callFeesCheckApi()
                                self.navigationController?.popViewController(animated: true)
                              })
                              alertViewObj.showAlert(controller: self)
                          }
                      }
                  }
              }
          }
    
    

}
