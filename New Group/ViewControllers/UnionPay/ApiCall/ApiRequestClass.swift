//
//  ApiRequestClass.swift


import UIKit
import Reachability

enum RequestTypeObj {
    case RequestTypeBankDetails
    case RequestTypeBankAccount
    case RequestTypePayWallet
    case RequestTypeBankReceipt
    case RequestTypeFeesCheck
    case RequestTypeGetCompanyList
    case RequestTypeGetCompanyPrice
    case RequestTypeTicketByNumber
    case ReserverLuckyNumByUser
    case VerifyallNumbers
    case RequestTypePayment
    case RequestTypeVerifyWinningSheet
}



protocol ApiRequestProtocol {
    func httpResponse(responseObj:Any,reqType:RequestTypeObj) -> Void
}

class ApiRequestClass: NSObject {
    let alertViewObj     = AlertView()
    
    var customDelegate:ApiRequestProtocol?
    
    func sendHttpRequest(requestUrl:URL,requestData:Any,requestType:RequestTypeObj,httpMethodName:String) -> Void
    {
       // let reacabilityObj = try Reachability()
        
        if  appDelegate.checkNetworkAvail()  //AppUtility.isConnectedToNetwork()
        {
            var request = URLRequest(url: requestUrl , cachePolicy: .useProtocolCachePolicy, timeoutInterval: 40.0)
            
            if httpMethodName == "POST"
            {
                let jsonData = try! JSONSerialization.data(withJSONObject: requestData, options: JSONSerialization.WritingOptions.prettyPrinted)
                let jsonString = String(data: jsonData, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as String
                let dataToSend = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                request.httpBody = dataToSend
            }
            
          //  let authtoken = UserDefaults.standard.value(forKey: "auth")
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
           // request.setValue(authtoken as? String, forHTTPHeaderField: "Authorization")
            request.httpMethod = httpMethodName
            
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest) {data,response,error in
                
                guard error == nil else
                {
                    self.customDelegate?.httpResponse(responseObj: (error?.localizedDescription)! as String, reqType: requestType)
                    return
                }
                
                do
                {
                    var responseArr = [Any]()
                    var responseArr1 = [Any]()
                    var responseDict = [String:Any]()
                    let result = try JSONSerialization.jsonObject(with: data!, options: [])
                    if result is [Any]
                    {
                       responseArr = result as! [Any]
                        
                        for i in 0..<responseArr.count
                        {
                            let sampleDict:[String:Any] = self.recursiveNullRemove(responseArr[i] as! [String : Any])
                            responseArr1.append(sampleDict)
                        }
                        self.customDelegate?.httpResponse(responseObj: responseArr1, reqType: requestType)
                    }
                    else if result is [String:Any]
                    {
                       responseDict = result as! Dictionary<String,Any>
                        let finalObj = self.recursiveNullRemove(responseDict)
                        self.customDelegate?.httpResponse(responseObj: finalObj, reqType: requestType)
                    }

                  //  let respDict = result as! Dictionary<String,Any>
                  //  let finalObj = self.recursiveNullRemove(respDict)
                  //  self.customDelegate?.httpResponse(responseObj: result, reqType: requestType)
                }
                catch
                {
                    self.customDelegate?.httpResponse(responseObj: error.localizedDescription, reqType: requestType)
                }
            }
            
            dataTask.resume()
            
        }
        else
        {
            let networkAlert = UIAlertController(title: "OK$", message: "check your network connection".localized, preferredStyle: UIAlertController.Style.alert)
            networkAlert.addAction(UIAlertAction(title: "Ok".localized, style: UIAlertAction.Style.default, handler: nil))
               progressViewObj.removeProgressView()

              UIApplication.shared.keyWindow?.rootViewController?.present(networkAlert, animated: true, completion: nil)
            var topController:UIViewController = UIApplication.shared.keyWindow!.rootViewController!
            while ((topController.presentedViewController) != nil) {
                topController = topController.presentedViewController!;
            }
           topController.present(networkAlert, animated:true, completion:nil)
//            progressViewObj.removeProgressView()
//            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
//            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                print("Ok call is done")
//
//            })
//            alertViewObj.showAlert(controller: topController.self)
//
            
        }
    }
 
    func recursiveNullRemove(_ dictionaryResponse: [String: Any]) -> [String: Any]
    {
        var dictionary: [String: Any] = dictionaryResponse 
        let emptyString: String = ""
     
        for key: String in dictionary.keys
        {
            let value: Any? = dictionary[key]
            if (value is [String: Any])
            {
                dictionary[key] = recursiveNullRemove((value as? [String: Any])!)
            }
            else if (value is [Any])
            {
                var valueArray: [Any] = value as! [Any]
                for i in 0..<valueArray.count {
                    let valueForKey: Any? = valueArray[i]
                    if (valueForKey is [String: Any]) {
                        valueArray[i] = recursiveNullRemove((valueForKey as? [String: Any])!)
                    }
                    else if (valueForKey is NSNull) {
                        valueArray[i] = emptyString
                    }
                }
                dictionary[key] = valueArray
            }
            else if (value is NSNull)
            {
                dictionary[key] = emptyString
            }
        }
        return dictionary
    }
}
