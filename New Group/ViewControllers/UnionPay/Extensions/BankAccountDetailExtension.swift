//
//  BankAccountDetailExtension.swift
//  OK
//
//  Created by vamsi on 7/27/20.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

//MARK:- TextField Delegate & Functions
extension BankAccountViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == mobileNumberTF {
            if  currentSelectedCountry == .myanmarCountry  {
                if let text = textField.text, text.count < 3 {
                    textField.text = "09"
                    self.closeButton.isHidden = true
                    self.userdetailsView.isHidden = true
                    self.amountTF.text = ""
                    self.amountTF.borderStyle = .none
                    self.amountCloseButton.isHidden = true
                   // self.fieldLabel.isHidden = false
                }
            }
            let maximumlimit = self.valueamount
         //   self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(maximumlimit) \("MMK".localized)"
         //   self.maximumamountLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
           
        }else if textField == confirmMobileNumberTF {
            if self.userdetailsView.isHidden {
                textField.text = "09"
                self.confrimCloseButton.isHidden = true
            }else {
                
            }
            let maximumlimit = self.valueamount
          //  self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(maximumlimit) \("MMK".localized)"
          //  self.maximumamountLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
           
        }else if textField == amountTF {
            remarkCloseButton.isHidden = true
            remarkVertical.constant = 0.0
            remarkLabelHeight.constant = 0.0
            if let text = textField.text{
               // amountTF.selectedTitle = "Amount".localized
                self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
               // self.amountCloseButton.isHidden = true
                textField.text = text
                if textField.text != ""{
                    self.amountCloseButton.isHidden = false
                    self.amountTF.placeholder = ""
                    self.stractiveField = "AMOUNTTF"
                  //  self.contentViewHeightConstraint.constant = 550
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
//                     self.contentScroll.isScrollEnabled = false
//                    }
                   let amout = textField.text?.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: ".", with: "") ?? "0"
                    let intAmout = Int(amout) ?? 0
                    
                    let maximumlimit = self.valueamount.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: ".", with: "")
                    
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
//                     self.contentScroll.isScrollEnabled = true
//                    }
                    if intAmout >= Int(minlimit) ?? 0{
                       // self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(self.valueamount) \("MMK".localized)"
                       // self.maximumamountLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
                        if intAmout > Int(maximumlimit) ?? 0 {
                          //  self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(self.valueamount) \("MMK".localized)"
                          //  self.maximumamountLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
//                            self.maximumamountLabel.isHidden = true
//                            self.amountTF.selectedTitleNew = self.returnAttributtedTitle()
//                            self.userdetailsHeightConstraint.constant = 224
                            self.maximumamountLabel.isHidden = false
                            self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                            self.userdetailsHeightConstraint.constant = 245
                            self.nextButton.isHidden = true
                            self.nextButtonHeightConstraint.constant = 0
                            self.nextButton.setTitle("".localized, for: .normal)
                            self.subscribeToShowKeyboardNotifications()
                        }
                        else
                        {
                            self.maximumamountLabel.isHidden = true
                            self.amountTF.selectedTitleNew = self.returnAttributtedTitle()
                            self.userdetailsHeightConstraint.constant = 224
//                            self.nextButton.isHidden = false
//                            self.nextButtonHeightConstraint.constant = 45
//                            self.nextButton.setTitle("Next".localized, for: .normal)
                           // self.subscribeToShowKeyboardNotifications()
                        }
                       
                    }
                    else
                    {
                     //   self.maximumamountLabel.text = "\("Please enter minimum amount".localized) \(self.minlimit) \("MMK".localized)"
                     //   self.maximumamountLabel.textColor = .red
                        self.maximumamountLabel.isHidden = false
                        self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                        self.userdetailsHeightConstraint.constant = 245
                        self.nextButton.isHidden = true
                        self.nextButtonHeightConstraint.constant = 0
                        self.nextButton.setTitle("".localized, for: .normal)
                        self.subscribeToShowKeyboardNotifications()
                    }
                }
                else{
                    self.maximumamountLabel.isHidden = false
                    self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                    self.userdetailsHeightConstraint.constant = 245
                    self.nextButton.isHidden = true
                    self.nextButtonHeightConstraint.constant = 0
                    self.nextButton.setTitle("".localized, for: .normal)
                    self.subscribeToShowKeyboardNotifications()
                }
                
            }
            
           // let maximumlimit = self.valueamount
          //  self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(maximumlimit) \("MMK".localized)"
           // self.maximumamountLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
        }
//        else if textField == remarkTF{
//                if textField.text != ""{
//                    remarkCloseButton.isHidden = false
//                }
//                else{
//                     remarkCloseButton.isHidden = true
//                }
//
//        }
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
       // if self.country?.dialCode == "+95" {
        if textField == mobileNumberTF{
            textField.text = "09"
            self.userdetailsView.isHidden = true
            self.amountTF.text = ""
              self.amountTF.borderStyle = .none
            self.amountCloseButton.isHidden = true
          //   self.fieldLabel.isHidden = false
             self.nextButton.isHidden = true
            self.nextButtonHeightConstraint.constant = 0
            self.nextButton.setTitle("".localized, for: .normal)
            self.subscribeToShowKeyboardNotifications()
        }
        else if textField == amountTF{
             self.amountCloseButton.isHidden = true
             textField.text = ""
            self.maximumamountLabel.isHidden = false
            self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
            self.userdetailsHeightConstraint.constant = 245
             self.feesCheckView.isHidden = true
             self.nextButton.isHidden = true
            self.nextButtonHeightConstraint.constant = 0
            self.nextButton.setTitle("".localized, for: .normal)
            self.subscribeToShowKeyboardNotifications()
        }
        
//        else if textField == remarkTF{
//            self.remarkCloseButton.isHidden = true
//            textField.text = ""
//        }
        
     //   }
      //  else {
      //      textField.text = ""
      //  }
        return false
    }
    
    func isValidAmount(text :String) -> Bool {
        
        if text.contains(".") {
            let amountArray = text.components(separatedBy: ".")
            
            if amountArray.count > 2 {
                return false
            }
            
            if let last = amountArray.last {
                if last.count > 2 {
                    return false
                } else {
                    return true
                }
            }
            return true
        } else {
            let mystring = text.replacingOccurrences(of: ",", with: "")
            let number = NSDecimalNumber(string: mystring)
            
            let formatter = NumberFormatter()
            
            formatter.groupingSeparator = ","
            formatter.groupingSize = 3
            formatter.usesGroupingSeparator = true
            var num = formatter.string(from: number)
            if num == "NaN" {
                num = ""
            }
           
            self.amountTF.text = num
            self.maximumamountLabel.isHidden = true
            self.amountTF.selectedTitleNew = self.returnAttributtedTitle()
            self.userdetailsHeightConstraint.constant = 224
            self.amountCloseButton.isHidden = false
           //  self.nextButton.isHidden = false
           //  self.nextButtonHeightConstraint.constant = 45
           // self.subscribeToShowKeyboardNotifications()
            return false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == mobileNumberTF {
         //   let object = validObj.getNumberRangeValidation(textField.text ?? "")
          //  self.removeContactList()
         //   if (textCount >= object.min && textCount <= object.max) {
//                    var myString = textField.text ?? ""
//                    myString.remove(at: myString.startIndex)
//                    mobilenumFormate = "0095"+myString
//                    print("@@@@@",mobilenumFormate)
//                    self.mobileNumberTF.resignFirstResponder()
//                    self.apicall()
           // }
        }else if textField == amountTF{
           
          if textField.text != ""{
          // let maximumlimit = self.valueamount
         //  self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(maximumlimit) \("MMK".localized)"
         //  self.maximumamountLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
         //   amountTF.title = "Amount".localized
            self.amountTF.placeholder = ""
            self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
            let amout = textField.text?.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: ".", with: "") ?? "0"
            let intAmout = Int(amout) ?? 0
            let maximumlimit = self.valueamount.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: ".", with: "")
            
            if intAmout >= Int(minlimit) ?? 0 {
               // self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(self.valueamount) \("MMK".localized)"
              //  self.maximumamountLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
             //   self.maximumamountLabel.isHidden = true
                if intAmout > Int(maximumlimit) ?? 0 {
                 //   self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(self.valueamount) \("MMK".localized)"
                 //   self.maximumamountLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
                //    self.maximumamountLabel.isHidden = true
                    self.maximumamountLabel.isHidden = false
                    self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                    self.userdetailsHeightConstraint.constant = 245
                    self.nextButton.isHidden = true
                    self.nextButtonHeightConstraint.constant = 0
                    self.nextButton.setTitle("".localized, for: .normal)
                    self.subscribeToShowKeyboardNotifications()
                }
                else
                {
                    self.maximumamountLabel.isHidden = true
                    self.amountTF.selectedTitleNew = self.returnAttributtedTitle()
                    self.userdetailsHeightConstraint.constant = 224
//                    self.nextButton.isHidden = false
//                    self.nextButtonHeightConstraint.constant = 45
//                    self.nextButton.setTitle("Next".localized, for: .normal)
//                    self.subscribeToShowKeyboardNotifications()
                }
                
               
            }
            else
            {
              //  self.maximumamountLabel.text = "\("Please enter minimum amount".localized) \(self.minlimit) \("MMK".localized)"
              //  self.maximumamountLabel.textColor = .red
                self.maximumamountLabel.isHidden = false
                self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                self.userdetailsHeightConstraint.constant = 245
                self.nextButton.isHidden = true
                self.nextButtonHeightConstraint.constant = 0
                self.nextButton.setTitle("".localized, for: .normal)
                self.subscribeToShowKeyboardNotifications()
            }
//            self.amountCloseButton.isHidden = false
//             self.nextButton.isHidden = false
//             self.nextButton.setTitle("Next".localized, for: .normal)
////             self.nextButtonHeightConstraint.constant = 45
//            self.subscribeToShowKeyboardNotifications()
            if intAmout >= Int(minlimit) ?? 0 {
                if intAmout > Int(maximumlimit) ?? 0 {
                    alertViewObj.wrapAlert(title: nil, body: "\("Maximum You can transfer up to".localized) \(valueamount) \("MMK".localized)", img: #imageLiteral(resourceName: "alert-icon"))
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                      //   self.contentScroll.isScrollEnabled = false
                            self?.amountTF.becomeFirstResponder()
                           }
                    })
                    alertViewObj.showAlert(controller: self)
                    
                    self.maximumamountLabel.isHidden = false
                    self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                    self.userdetailsHeightConstraint.constant = 245
                }else{
                    amountCloseButton.isHidden = true
                    if appDelegate.checkNetworkAvail(){
                        self.maximumamountLabel.isHidden = true
                        self.FeeCheckApi()
                    }else{
                        print("Internet Connection not Available!")
                    }
                }
          
            }
            }
           else
             {
                self.amountTF.placeholder = "Enter Transfer Amount".localized
             //   self.maximumamountLabel.text = "Please enter minimum amount 500 MMK".localized
             //   self.maximumamountLabel.textColor = .red
                let maximumlimit = self.valueamount
             //   self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(maximumlimit) \("MMK".localized)"
            //    self.maximumamountLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
                self.maximumamountLabel.isHidden = false
                self.userdetailsHeightConstraint.constant = 245
                print( "nodataaaaa")
            }
      
        }
        
//        else if textField == remarkTF{
//            if textField.text != ""{
//                remarkCloseButton.isHidden = false
//
//            }
//            else{
//                remarkCloseButton.isHidden = true
//
//            }
//        }
    }
    
    @objc func textFieldDidEditChanged(_ textField : UITextField) {
        
        if textField == mobileNumberTF {
            if textField.text?.count ?? 0 >= 3 {
                println_debug(mobileNumberTF.frame.origin.y)
                let yAxis : CGFloat = navigationView.frame.origin.y + navigationView.frame.height + mobileNumberTF.frame.origin.y + mobileNumberTF.frame.height + 1
                showContactList(yAsix: yAxis , position: "BOTTOM", contactFor: "AccountNumber")
                contactSuggestionView.loadContacts(txt: textField.text ?? "0", axis: yAxis, position: "BOTTOM")
            }else {
                self.removeContactList()
               // self.fieldLabel.isHidden = false
            }
            let object = validObj.getNumberRangeValidation(textField.text ?? "")
            let textCount = textField.text?.count ?? 0
            
         
            if textCount < object.min {
                self.hideConfirmNumber()
            }else if (textCount >= object.min) {
                if object.min == object.max {
                    self.removeContactList()
                    self.showConfrimNumber(isShowKeyboard: true)
                }else {
                    if (textCount >= object.max) {
                        self.confirmMobileNumberTF.becomeFirstResponder()
                    }else {
                        self.showConfrimNumber(isShowKeyboard: false)
                    }
                }
            }
        }else if textField == confirmMobileNumberTF {
            if confirmMobileNumberTF.text == mobileNumberTF.text {
                var myString = textField.text ?? ""
                myString.remove(at: myString.startIndex)
                mobilenumFormate = "0095" + myString
                self.mobileNumberTF.resignFirstResponder()
                self.apicall()
            }else {
                if !self.userdetailsView.isHidden {
                    self.userdetailsView.isHidden = true
                    self.amountTF.text = ""
                    self.amountTF.borderStyle = .none
                }
            }
        }else if textField == amountTF {
            let strMainTxt = textField.text?.replacingOccurrences(of: ",", with: "") ?? ""
            if strMainTxt.count > 10 {
                textField.text?.removeLast()
            }
        }
//        else if textField == remarkTF{
//            if textField.text != ""{
//                remarkCloseButton.isHidden = false
//            }
//            else{
//                remarkCloseButton.isHidden = true
//
//            }
//        }

        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var flag = true
        if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
            let endOfDocument = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
            return false
        }
        if textField == mobileNumberTF {
            if self.isSelectedFromContact {
                self.hideConfirmNumber()
            }
            if currentSelectedCountry == .myanmarCountry {
                if range.location <= 1 {
                    let newPosition = textField.endOfDocument
                    textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
                    return false
                }
            }
            
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "0123456789").inverted).joined(separator: "")) { return false }
            if (range.location == 0 && range.length == 0) || (range.location == 1 && range.length == 0) {
                return false
            }
            
            if range.location == 0 && range.length > 1 {
                textField.text = "09"
                self.closeButton.isHidden = true
                self.userdetailsView.isHidden = true
                self.amountTF.text = ""
                  self.amountTF.borderStyle = .none
                self.amountCloseButton.isHidden = true
               // self.fieldLabel.isHidden = true
                self.nextButton.isHidden = true
                self.nextButtonHeightConstraint.constant = 0
                nextButton.setTitle("".localized, for: .normal)
                return false
            }
            
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let textCount = text.count
            if textCount > 2 {
                self.closeButton.isHidden = false
               // self.fieldLabel.isHidden = false
            }else {
                self.closeButton.isHidden = true
               // self.fieldLabel.isHidden = true
                self.userdetailsView.isHidden = true
                self.amountTF.text = ""
                self.amountTF.borderStyle = .none
                self.amountCloseButton.isHidden = true
                self.nextButton.isHidden = true
                self.nextButtonHeightConstraint.constant = 0
                nextButton.setTitle("".localized, for: .normal)
                self.subscribeToShowKeyboardNotifications()
            }
            
            let object = validObj.getNumberRangeValidation(text)
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            
            if (isBackSpace == -92) {
                if textField.text == "09" {
                    textField.text = "09"
                    self.closeButton.isHidden = true
                    self.userdetailsView.isHidden = true
                    self.amountTF.text = ""
                     self.amountTF.borderStyle = .none
                    self.amountCloseButton.isHidden = true
                  //  self.fieldLabel.isHidden = true
                    self.nextButton.isHidden = true
                    self.nextButtonHeightConstraint.constant = 0
                    nextButton.setTitle("".localized, for: .normal)
                    self.subscribeToShowKeyboardNotifications()
                    return false
                }
            }
            
            if object.isRejected == true {
                textField.text = "09"
                self.closeButton.isHidden = true
                self.userdetailsView.isHidden = true
                self.amountTF.text = ""
                self.amountTF.borderStyle = .none
                self.amountCloseButton.isHidden = true
               // self.fieldLabel.isHidden = true
                self.nextButton.isHidden = true
                self.nextButtonHeightConstraint.constant = 0
                nextButton.setTitle("".localized, for: .normal)
                self.subscribeToShowKeyboardNotifications()
                textField.resignFirstResponder()
                DispatchQueue.main.async {
                    self.showToast(message: "Invalid Number".localized, align: .top)
                }
                return false
            }
            if textCount > object.max {
                return false
            }
            return true
        }else if textField == confirmMobileNumberTF {
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "0123456789").inverted).joined(separator: "")) { return false }
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            if (range.location < 2 && isBackSpace == -92) {
                return false
            }
            if validObj.checkMatchingNumber(string: text, withString: mobileNumberTF.text ?? "") {
                if text.count > 2 {
                    self.confrimCloseButton.isHidden = false
                }else {
                    self.confrimCloseButton.isHidden = true
                }
                return true
            }else {
                return false
            }
        }
        else if textField == amountTF {
          
            let minimumlimit = self.minlimit
            let maximumlimit = self.valueamount.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: ".", with: "")
           // self.maximumamountLabel.text = "\("Please enter minimum amount".localized) \(minimumlimit) \("MMK".localized)"
          //  self.maximumamountLabel.textColor = .red
            
            var strMainTxt = ""
            var txtMain = ""
            guard let textFieldText = textField.text else { return false}
            guard let textRange = Range(range, in: textFieldText) else { return false}
            txtMain = textFieldText.replacingCharacters(in: textRange, with: string)
            
            if (range.location == 0 && txtMain == "0") {
                let maximumlimit = self.valueamount
             //   self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(maximumlimit) \("MMK".localized)"
             //   self.maximumamountLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
            //    self.maximumamountLabel.isHidden = true
            //    self.amountTF.selectedTitleNew = self.returnAttributtedTitle()
               return false
            }
            if txtMain.count > 11 {
                print("exicute")
                return false
            }
                updatedText = textFieldText.replacingCharacters(in: textRange, with: string)
                print(updatedText)
            
        //    if updatedText.count == self.minlimit.count{
            
            
                
          //  }
            self.amountTF.placeholder = ""
            self.maximumamountLabel.isHidden = false
            self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
            self.userdetailsHeightConstraint.constant = 245
            
            if NSString(string: self.updatedText.replacingOccurrences(of: ".", with: "").replacingOccurrences(of: ",", with: "")).doubleValue > NSString(string: UserLogin.shared.walletBal).doubleValue{
                            self.block1?.cancel()
                            self.amountTF.text = ""
                            let maximumlimit = self.valueamount
                        //   self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(maximumlimit) \("MMK".localized)"
                        //   self.maximumamountLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
                            self.maximumamountLabel.isHidden = true
                            self.amountTF.selectedTitleNew = self.returnAttributtedTitle()
                            self.userdetailsHeightConstraint.constant = 224
                            self.amountView.layer.borderWidth = 0
                            self.amountView.layer.borderColor = UIColor.gray.cgColor
                            if self.nrcpassport == ""{
                              self.amountViewTopConstraint.constant = 0
                              self.limitViewTopConstraint.constant = 0
                            }
                            else{
                                self.amountViewTopConstraint.constant = 1
                                self.limitViewTopConstraint.constant = 1
                             }
                           alertViewObj.wrapAlert(title: nil, body: "Insufficient balance".localized, img: #imageLiteral(resourceName: "alert-icon"))
                           alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                          //  self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(maximumlimit) \("MMK".localized)"
                         //   self.maximumamountLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
                            self.maximumamountLabel.isHidden = false
                            self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                            self.userdetailsHeightConstraint.constant = 245
                            self.amountTF.text = ""
                            self.amountTF.placeholder = "Enter Transfer Amount".localized
                            self.amountTF.becomeFirstResponder()
                            self.amountCloseButton.isHidden = true
                            self.feesCheckView.isHidden = true
                            self.nextButton.isHidden = true
                            self.nextButtonHeightConstraint.constant = 0
                            self.nextButton.setTitle("".localized, for: .normal)
                            self.subscribeToShowKeyboardNotifications()

                           })
                           alertViewObj.showAlert(controller: self)
                           return false
                       }
                let numberCharSet = "1234567890."
                if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: numberCharSet).inverted).joined(separator: "")) { return false }
                flag = self.isValidAmount(text: updatedText)
                
                strMainTxt = textField.text?.replacingOccurrences(of: ",", with: "") ?? ""
                  if strMainTxt.count > 9 {
                               print ("Count",strMainTxt)
                     }
                let amountArray = strMainTxt.components(separatedBy: ".")
                let mystring = amountArray[0]
                if(mystring.length > minimumlimit.length) && (mystring.length <= maximumlimit.length) {
                  //  textField.textColor = UIColor.green
                    //amountTF.borderStyle = .roundedRect
                    self.amountView.layer.borderWidth = 0
                    self.amountView.layer.borderColor = UIColor.gray.cgColor
                    if self.nrcpassport == ""{
                        self.amountViewTopConstraint.constant = 0
                        self.limitViewTopConstraint.constant = 0
                        self.userdetailsHeightConstraint.constant = 245
                    }
                    else{
                        self.amountViewTopConstraint.constant = 1
                        self.limitViewTopConstraint.constant = 1
                    }
                } else if(mystring.length > maximumlimit.length)  {
                   // textField.textColor = UIColor.red
                   //  amountTF.setBottomLine(borderColor: UIColor.red)
                    self.amountView.layer.borderWidth = 0
                    self.amountView.layer.borderColor = UIColor.gray.cgColor
                    if self.nrcpassport == ""{
                        self.amountViewTopConstraint.constant = 0
                        self.limitViewTopConstraint.constant = 0
                        self.userdetailsHeightConstraint.constant = 245
                    }
                    else{
                        self.amountViewTopConstraint.constant = 1
                        self.limitViewTopConstraint.constant = 1
                    }
                }
                else if (mystring.length == minimumlimit.length){
                    let valuelimit  = self.minlimit
                   // let minimumlimit = self.minimumvalueamount
                    let valueFloat: Float =  NSString(string: (self.updatedText.replacingOccurrences(of: ",", with: "")).replacingOccurrences(of: ".", with: "")).floatValue
                    print(valueFloat)
                    if valueFloat < (valuelimit as NSString).floatValue{
                      //   textField.textColor = UIColor.red
                        self.amountView.layer.borderWidth = 0
                        self.amountView.layer.borderColor = UIColor.gray.cgColor
                        if self.nrcpassport == ""{
                            self.amountViewTopConstraint.constant = 0
                            self.limitViewTopConstraint.constant = 0
                             self.userdetailsHeightConstraint.constant = 245
                        }
                        else{
                            self.amountViewTopConstraint.constant = 1
                            self.limitViewTopConstraint.constant = 1
                        }
                        self.maximumamountLabel.isHidden = false
                        self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                        self.amountCloseButton.isHidden = true
                        self.nextButton.isHidden = true
                        self.nextButtonHeightConstraint.constant = 0
                        self.nextButton.setTitle("".localized, for: .normal)
                        self.subscribeToShowKeyboardNotifications()
                    }
                    else if valueFloat > (maximumlimit as NSString).floatValue{
                       // textField.textColor = UIColor.red
                        self.amountView.layer.borderWidth = 0
                        self.amountView.layer.borderColor = UIColor.gray.cgColor
                        if self.nrcpassport == ""{
                            self.amountViewTopConstraint.constant = 0
                            self.limitViewTopConstraint.constant = 0
                            self.userdetailsHeightConstraint.constant = 245
                        }
                        else{
                            self.amountViewTopConstraint.constant = 1
                            self.limitViewTopConstraint.constant = 1
                        }
                        self.maximumamountLabel.isHidden = true
                        self.amountTF.selectedTitleNew = self.returnAttributtedTitle()
                        self.userdetailsHeightConstraint.constant = 224
                        self.amountCloseButton.isHidden = true
                        self.nextButton.isHidden = true
                        self.nextButtonHeightConstraint.constant = 0
                        self.nextButton.setTitle("".localized, for: .normal)
                        self.subscribeToShowKeyboardNotifications()
                    }
                    else
                    {
//                        textField.textColor = UIColor.green
                        self.amountView.layer.borderWidth = 0
                        self.amountView.layer.borderColor = UIColor.gray.cgColor
                        if self.nrcpassport == ""{
                            self.amountViewTopConstraint.constant = 0
                            self.limitViewTopConstraint.constant = 0
                            self.userdetailsHeightConstraint.constant = 245
                        }
                        else{
                            self.amountViewTopConstraint.constant = 1
                            self.limitViewTopConstraint.constant = 1
                        }
                    }

                }
                else if (mystring.length < minimumlimit.length){
//                    textField.textColor = UIColor.red
                    self.amountView.layer.borderWidth = 0
                    self.amountView.layer.borderColor = UIColor.gray.cgColor
                    if self.nrcpassport == ""{
                        self.amountViewTopConstraint.constant = 0
                        self.limitViewTopConstraint.constant = 0
                        self.userdetailsHeightConstraint.constant = 245
                    }
                    else{
                        self.amountViewTopConstraint.constant = 1
                        self.limitViewTopConstraint.constant = 0
                    }
                    self.maximumamountLabel.isHidden = false
                    self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                    self.amountCloseButton.isHidden = true
                    self.nextButton.isHidden = true
                    self.nextButtonHeightConstraint.constant = 0
                    self.nextButton.setTitle("".localized, for: .normal)
                    self.subscribeToShowKeyboardNotifications()
                    
                }
                else if (mystring.length > maximumlimit.length){
//                    textField.textColor = UIColor.red
                    self.amountView.layer.borderWidth = 0
                    self.amountView.layer.borderColor = UIColor.gray.cgColor
                    if self.nrcpassport == ""{
                        self.amountViewTopConstraint.constant = 0
                        self.limitViewTopConstraint.constant = 0
                        self.userdetailsHeightConstraint.constant = 245
                    }
                    else{
                        self.amountViewTopConstraint.constant = 1
                        self.limitViewTopConstraint.constant = 1
                    }
                    self.maximumamountLabel.isHidden = true
                    self.amountTF.selectedTitleNew = self.returnAttributtedTitle()
                    self.userdetailsHeightConstraint.constant = 224
                    self.amountCloseButton.isHidden = true
                    self.nextButton.isHidden = true
                    self.nextButtonHeightConstraint.constant = 0
                    self.nextButton.setTitle("".localized, for: .normal)
                    self.subscribeToShowKeyboardNotifications()
                    
                }
                
                else {
//                    textField.textColor = UIColor.red
                    self.amountView.layer.borderWidth = 0
                    self.amountView.layer.borderColor = UIColor.gray.cgColor
                    if self.nrcpassport == ""{
                        self.amountViewTopConstraint.constant = 0
                        self.limitViewTopConstraint.constant = 0
                         self.userdetailsHeightConstraint.constant = 245
                    }
                    else{
                        self.amountViewTopConstraint.constant = 1
                        self.limitViewTopConstraint.constant = 1
                    }
                }
                if let amount = textField.text {
                    if amount == "" {
                        self.amountTF.placeholder = "Enter Transfer Amount".localized
                        // button.isHidden = true
                      //  self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(self.valueamount) \("MMK".localized)"
                     //   self.maximumamountLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
                        self.maximumamountLabel.isHidden = false
                        self.userdetailsHeightConstraint.constant = 245
                        self.amountView.layer.borderWidth = 0
                        self.amountView.layer.borderColor = UIColor.gray.cgColor
                        if self.nrcpassport == ""{
                            self.amountViewTopConstraint.constant = 0
                            self.limitViewTopConstraint.constant = 0
                        }
                        else{
                            self.amountViewTopConstraint.constant = 1
                            self.limitViewTopConstraint.constant = 1
                        }
                        self.amountCloseButton.isHidden = true
                        // self.viewNext.isHidden = true
                        self.feesCheckView.isHidden = true
                        self.nextButton.isHidden = true
                       self.nextButtonHeightConstraint.constant = 0
                       self.nextButton.setTitle("".localized, for: .normal)
                       self.subscribeToShowKeyboardNotifications()
                        
                    }
                    else
                    {
                        self.amountTF.placeholder = ""
                        //   button.isHidden = false
                        self.amountCloseButton.isHidden = false
                       
                       //  self.nextButtonHeightConstraint.constant = 45
                       // self.subscribeToShowKeyboardNotifications()
                        //  self.viewNext.isHidden = false
                        let amout = textField.text?.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: ".", with: "") ?? "0"
                        let intAmout = Int(amout) ?? 0
                        if intAmout >= Int(minlimit) ?? 0{
                         //   self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(self.valueamount) \("MMK".localized)"
                         //   self.maximumamountLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
                            self.maximumamountLabel.isHidden = true
                            self.amountTF.selectedTitleNew = self.returnAttributtedTitle()
                            self.userdetailsHeightConstraint.constant = 224
                            if intAmout > Int(maximumlimit) ?? 0{
                                 self.nextButton.isHidden = true
                                self.maximumamountLabel.isHidden = true
                                self.amountTF.selectedTitleNew = self.returnAttributtedTitle()
                                self.userdetailsHeightConstraint.constant = 224
                            }
                            else
                            {
                                self.maximumamountLabel.isHidden = false
                                self.feesCheckView.isHidden = true
                                self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                                self.userdetailsHeightConstraint.constant = 245
                              // self.nextButton.isHidden = false
                            }
                            
                        }
                        else
                        {
                            self.maximumamountLabel.isHidden = true
                            self.amountTF.selectedTitleNew = self.returnAttributtedTitle()
                            self.userdetailsHeightConstraint.constant = 224
                            self.nextButton.isHidden = true
                        }
                        
                        
                    }
                }
                if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
                    let endOfDocument = textField.endOfDocument
                    textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
                    flag = false
                }
                else {
                        self.block1?.cancel()
                        self.block1 = DispatchWorkItem {
                         
                            if let text = textField.text, text.count > 0 {
                                let textAfterRemovingComma = text.replacingOccurrences(of: ",", with: "")
                                
                                if self.updatedText.count < 3 {
                                    let valuelimit  = self.minlimit
                                    let minimumlimit = self.minimumvalueamount
                                    let valueFloat: Float =  NSString(string: (self.updatedText.replacingOccurrences(of: ",", with: "")).replacingOccurrences(of: ".", with: "")).floatValue
                                    print(valueFloat)
                                    if valueFloat < (valuelimit as NSString).floatValue{
                                      // let minimumlimit = self.minimumvalueamount
                                      //  self.maximumamountLabel.text = "\("Please enter minimum amount".localized) \(minimumlimit) \("MMK".localized)"
                                      //  self.maximumamountLabel.textColor = .red
                                        self.maximumamountLabel.isHidden = false
                                        self.feesCheckView.isHidden = true
                                        self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                                        self.userdetailsHeightConstraint.constant = 245
                                       
//                                        alertViewObj.wrapAlert(title: nil, body: self.maximumamountLabel.text ?? "", img: #imageLiteral(resourceName: "alert-icon"))
//                                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                                            self.amountTF.text = ""
//                                            self.amountView.layer.borderWidth = 0
//                                            self.amountView.layer.borderColor = UIColor.gray.cgColor
//                                            self.amountCloseButton.isHidden = true
//                                            self.nextButton.isHidden = true
//                                            self.nextButtonHeightConstraint.constant = 0
//                                            self.nextButton.setTitle("".localized, for: .normal)
//                                            self.subscribeToShowKeyboardNotifications()
//                                        })
//                                        alertViewObj.showAlert(controller: self)
                                    }
                                }else if self.updatedText.count == 3 {
                                    ///compare weith min amount
                                    
                                    let valuelimit  = self.minlimit
                                    let minimumlimit = self.minimumvalueamount
                                    let valueFloat: Float =  NSString(string: (self.updatedText.replacingOccurrences(of: ",", with: "")).replacingOccurrences(of: ".", with: "")).floatValue

                                    print(valueFloat)
                                    if valueFloat < (valuelimit as NSString).floatValue {
                                        // let minimumlimit = self.minimumvalueamount
                                     //    self.maximumamountLabel.text = "\("Please enter minimum amount".localized) \(minimumlimit) \("MMK".localized)"
                                     //   self.maximumamountLabel.textColor = .red
                                        self.maximumamountLabel.isHidden = false
                                        self.feesCheckView.isHidden = true
                                        self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                                        self.userdetailsHeightConstraint.constant = 245
//                                        alertViewObj.wrapAlert(title: nil, body: self.maximumamountLabel.text ?? "", img: #imageLiteral(resourceName: "alert-icon"))
//                                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                                            self.amountTF.text = ""
//                                            self.amountView.layer.borderWidth = 0
//                                            self.amountView.layer.borderColor = UIColor.gray.cgColor
//                                            self.amountCloseButton.isHidden = true
//                                            self.nextButton.isHidden = true
//                                            self.nextButtonHeightConstraint.constant = 0
//                                            self.nextButton.setTitle("".localized, for: .normal)
//                                            self.subscribeToShowKeyboardNotifications()
//                                        })
//                                        alertViewObj.showAlert(controller: self)
                                    }
                                }
                                    else if self.updatedText.count > 3{
                                         let minvaluelimit  = self.minlimit
                                         let minimumlimit = self.minimumvalueamount
                                         let valuelimit  = self.maxlimit
                                        // let maximumlimit = self.valueamount
                                         let valueFloat: Float =  NSString(string: (self.updatedText.replacingOccurrences(of: ",", with: "")).replacingOccurrences(of: ".", with: "")).floatValue
                                            print(valueFloat)
                                        if valueFloat > (valuelimit as NSString).floatValue{
                                        //    self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(self.valueamount) \("MMK".localized)"
                                       //     self.maximumamountLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
                                            self.maximumamountLabel.isHidden = true
                                            self.amountTF.selectedTitleNew = self.returnAttributtedTitle()
                                            self.userdetailsHeightConstraint.constant = 224
//                                              alertViewObj.wrapAlert(title: nil, body: "\("Maximum You can transfer up to".localized) \(maximumlimit) \("MMK".localized)", img: #imageLiteral(resourceName: "alert-icon"))
//                                       alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//
//                                            textField.textColor = UIColor.red
                                            self.amountView.layer.borderWidth = 0
                                            self.amountView.layer.borderColor = UIColor.gray.cgColor
                                            if self.nrcpassport == ""{
                                                self.amountViewTopConstraint.constant = 0
                                                self.limitViewTopConstraint.constant = 0
                                                self.userdetailsHeightConstraint.constant = 245
                                            }
                                            else{
                                                self.amountViewTopConstraint.constant = 1
                                                self.limitViewTopConstraint.constant = 1
                                            }
                                        self.nextButton.isHidden = true
                                        self.nextButtonHeightConstraint.constant = 0
                                        self.nextButton.setTitle("".localized, for: .normal)
                                        self.subscribeToShowKeyboardNotifications()
//
//                                         })
//                                        alertViewObj.showAlert(controller: self)
                                    }
                                    if valueFloat < (minvaluelimit as NSString).floatValue {
                                        // let minimumlimit = self.minimumvalueamount
                                      //  self.maximumamountLabel.text = "\("Please enter minimum amount".localized) \(minimumlimit) \("MMK".localized)"
                                      //  self.maximumamountLabel.textColor = .red
                                        self.maximumamountLabel.isHidden = false
                                        self.feesCheckView.isHidden = true
                                        self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
//                                        alertViewObj.wrapAlert(title: nil, body: self.maximumamountLabel.text ?? "", img: #imageLiteral(resourceName: "alert-icon"))
//                                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                                            self.amountTF.text = ""
//                                            self.amountView.layer.borderWidth = 0
//                                            self.amountView.layer.borderColor = UIColor.gray.cgColor
//                                            self.amountCloseButton.isHidden = true
//                                            self.nextButton.isHidden = true
//                                            self.nextButtonHeightConstraint.constant = 0
//                                            self.nextButton.setTitle("".localized, for: .normal)
//                                            self.subscribeToShowKeyboardNotifications()
//                                        })
//                                        alertViewObj.showAlert(controller: self)
                                    }
                                }
                                else {
                                    if let amount = textField.text {
                                        let amout = textField.text?.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: ".", with: "") ?? "0"
                                         let intAmout = Int(amout) ?? 0
                                        if intAmout >= Int(self.minlimit) ?? 0 {
                                          //  self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(self.valueamount) \("MMK".localized)"
                                         //   self.maximumamountLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
                                            
                                            if intAmout > Int(maximumlimit) ?? 0 {
                                                self.maximumamountLabel.isHidden = false
                                                self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                                                self.userdetailsHeightConstraint.constant = 245
                                                self.nextButton.isHidden = true
                                                self.nextButtonHeightConstraint.constant = 0
                                                self.nextButton.setTitle("".localized, for: .normal)
                                                self.subscribeToShowKeyboardNotifications()
                                                
                                            }
                                            else
                                            {
                                                self.maximumamountLabel.isHidden = true
                                                self.amountTF.selectedTitleNew = self.returnAttributtedTitle()
                                                self.userdetailsHeightConstraint.constant = 224
//                                                self.nextButton.isHidden = false
//                                                self.nextButtonHeightConstraint.constant = 45
//                                                self.nextButton.setTitle("Next".localized, for: .normal)
//                                                self.subscribeToShowKeyboardNotifications()
                                            }
                                        }
                                        else
                                        {
                                         //   self.maximumamountLabel.text = "\("Please enter minimum amount".localized) \(self.minlimit) \("MMK".localized)"
                                         //   self.maximumamountLabel.textColor = .red
                                            self.maximumamountLabel.isHidden = false
                                            self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                                            self.userdetailsHeightConstraint.constant = 245
                                            self.nextButton.isHidden = true
                                            self.nextButtonHeightConstraint.constant = 0
                                            self.nextButton.setTitle("".localized, for: .normal)
                                            self.subscribeToShowKeyboardNotifications()
                                        }
                                        
                                    }
                                }
                                if NSString(string: UserLogin.shared.walletBal).floatValue < NSString(string: text.replacingOccurrences(of: ",", with: "")).floatValue {
                                    // let maximumlimit = self.valueamount
                                   // self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(self.valueamount) \("MMK".localized)"
                                  //  self.maximumamountLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
                                    self.maximumamountLabel.isHidden = false
                                    self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                                    self.userdetailsHeightConstraint.constant = 245
                                    self.amountView.layer.borderWidth = 0
                                    self.amountView.layer.borderColor = UIColor.gray.cgColor
                                    if self.nrcpassport == ""{
                                        self.amountViewTopConstraint.constant = 0
                                        self.limitViewTopConstraint.constant = 0
                                    }
                                    else{
                                        self.amountViewTopConstraint.constant = 1
                                        self.limitViewTopConstraint.constant = 1
                                    }
//                                    alertViewObj.wrapAlert(title: nil, body: "Insufficient Balance".localized, img: #imageLiteral(resourceName: "alert-icon"))
//                                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                                        self.amountTF.text = ""
//                                        self.amountView.layer.borderWidth = 0
//                                        self.amountView.layer.borderColor = UIColor.gray.cgColor
//                                        self.amountCloseButton.isHidden = true
//                                        self.nextButton.isHidden = true
//                                        self.nextButtonHeightConstraint.constant = 0
//                                        self.nextButton.setTitle("".localized, for: .normal)
//                                        self.subscribeToShowKeyboardNotifications()
//
//                                    })
//                                    alertViewObj.showAlert(controller: self)
                                    //       return flag == false
                                }
                                else if NSString(string: UserLogin.shared.walletBal).doubleValue < NSString(string: text.replacingOccurrences(of: ".", with: "")).doubleValue {
                                     //let maximumlimit = self.valueamount
                                  //  self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(self.valueamount) \("MMK".localized)"
                                  //  self.maximumamountLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
                                    self.maximumamountLabel.isHidden = false
                                    self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                                    self.userdetailsHeightConstraint.constant = 245
                                    self.amountView.layer.borderWidth = 0
                                    self.amountView.layer.borderColor = UIColor.gray.cgColor
                                    if self.nrcpassport == ""{
                                        self.amountViewTopConstraint.constant = 0
                                        self.limitViewTopConstraint.constant = 0
                                    }
                                    else{
                                        self.amountViewTopConstraint.constant = 1
                                        self.limitViewTopConstraint.constant = 1
                                    }
//                                    alertViewObj.wrapAlert(title: nil, body: "Insufficient Balance".localized, img: #imageLiteral(resourceName: "alert-icon"))
//                                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                                        self.amountTF.text = ""
//                                        self.amountView.layer.borderWidth = 0
//                                        self.amountView.layer.borderColor = UIColor.gray.cgColor
//                                        self.amountCloseButton.isHidden = true
//                                        self.nextButton.isHidden = true
//                                        self.nextButtonHeightConstraint.constant = 0
//                                        self.nextButton.setTitle("".localized, for: .normal)
//                                        self.subscribeToShowKeyboardNotifications()
//                                    })
//                                    alertViewObj.showAlert(controller: self)
                                    // return flag == false
                                }
                                else {
                                    if let amount = textField.text {
                                        let amout = textField.text?.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: ".", with: "") ?? "0"
                                        let intAmout = Int(amout) ?? 0
                                        if intAmout >= Int(self.minlimit) ?? 0  {
                                       //     self.maximumamountLabel.text = "\("Maximum You can transfer up to".localized) \(self.valueamount) \("MMK".localized)"
                                      //      self.maximumamountLabel.textColor = UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)
                                           
                                            if intAmout > Int(maximumlimit) ?? 0 {
                                                self.maximumamountLabel.isHidden = false
                                                self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                                                self.userdetailsHeightConstraint.constant = 245
                                                self.nextButton.isHidden = true
                                                self.nextButtonHeightConstraint.constant = 0
                                                self.nextButton.setTitle("".localized, for: .normal)
                                                self.subscribeToShowKeyboardNotifications()
                                                
                                            }
                                            else
                                            {
                                                self.maximumamountLabel.isHidden = true
                                                self.amountTF.selectedTitleNew = self.returnAttributtedTitle()
                                                self.userdetailsHeightConstraint.constant = 224
//                                                self.nextButton.isHidden = false
//                                                self.nextButtonHeightConstraint.constant = 45
//                                                self.nextButton.setTitle("Next".localized, for: .normal)
//                                                self.subscribeToShowKeyboardNotifications()
                                            }
                                            
                                            
                                        }
                                        else
                                        {
//                                            self.maximumamountLabel.text = "\("Please enter minimum amount".localized) \(self.minlimit) \("MMK".localized)"
//                                            self.maximumamountLabel.textColor = .red
                                            self.maximumamountLabel.isHidden = false
                                            self.amountTF.selectedTitleNew = self.returnAttributtedAmount()
                                            self.userdetailsHeightConstraint.constant = 245
                                            self.nextButton.isHidden = true
                                            self.nextButtonHeightConstraint.constant = 0
                                            self.nextButton.setTitle("".localized, for: .normal)
                                            self.subscribeToShowKeyboardNotifications()
                                        }
                                    
                                    }
                                       
                                    }
                                if textAfterRemovingComma.count > 9 {
                                    textField.text = String((textField.text?.dropLast())!)
                                }
                            }
                        }
                        // execute task in 2 seconds
                         DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(0), execute: self.block1!)
                 }
          }
//        else if textField == remarkTF{
//            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
//            let textCount = text.count
//            if textCount > 0 {
//                remarkCloseButton.isHidden = false
//            }
//            else{
//                 remarkCloseButton.isHidden = true
//            }
//            if string == " " {
//                return false
//            }
//        }
      return flag
    }
    
}
extension BankAccountViewController:UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.stractiveField = "REMARKTEXTVIEW"
        remarkVertical.constant = 8.0
        remarkLabelHeight.constant = 15.0
        remarkheadingLabel.attributedText = self.returnAttributtedtextview()
        
        if textView.attributedText == returnAttributtedtextview() {
            textView.attributedText = NSAttributedString(string: "")
            textView.textColor = UIColor.black
            remarkCloseButton.isHidden = true
        }
        else{
            if textView.text == "" {
                remarkCloseButton.isHidden = true
            }
            else{
                remarkCloseButton.isHidden = false
                remarkheadingLabel.text = "Remarks :".localized
            }
        }
      
            textView.textColor = UIColor.black
       
        textView.becomeFirstResponder()       
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.attributedText = self.returnAttributtedtextview()
            remarkCloseButton.isHidden = true
            remarkVertical.constant = 0.0
            remarkLabelHeight.constant = 0.0
           // textView.textColor = UIColor.black
        }
        else
        {   remarkCloseButton.isHidden = true
            textView.textColor = UIColor.black
            remarkheadingLabel.text = "Remarks :".localized
        }
      //
        textView.resignFirstResponder()
      
    }
    
    func textViewDidChange(_ textView: UITextView) {
                let fixedWidth = remarkTextView.frame.size.width
                remarkTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                let newSize = remarkTextView.sizeThatFits(CGSize(width: fixedWidth, height: feesCheckView.frame.size.height
                ))
                var newFrame = remarkTextView.frame
                newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                remarkTextView.frame = newFrame;
               
               remarkHeightContraint = Double(remarkTextView.frame.size.height)
               print ("Height", remarkHeightContraint)
                
              
        
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

        var CHARSET = ""
        if appDel.currentLanguage == "my" {
            CHARSET = NAME_CHAR_SET_My
        }else if appDel.currentLanguage == "en" {
              CHARSET = NAME_CHAR_SET_En
        }else {
             CHARSET = NAME_CHAR_SET_Uni
        }
        let updatedText = (textView.text! as NSString).replacingCharacters(in: range, with: text)
        if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: CHARSET).inverted).joined(separator: "")) { return false }
      
        if updatedText == " " || updatedText == "  "{
            return false
        }
        if (updatedText.contains("  ")){
            return false
        }
        if updatedText.count > 30 {
            return false
        }
        
        if textView.text.count <= 29{
            remarkViewHeight.constant = 60.0
        }else{
            let fixedWidth = remarkTextView.frame.size.width
            remarkTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = remarkTextView.sizeThatFits(CGSize(width: fixedWidth, height: feesCheckView.frame.size.height
            ))
            var newFrame = remarkTextView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            remarkTextView.frame = newFrame;
            var local = 0.0
            local = Double(remarkTextView.frame.size.height)
            remarkViewHeight.constant =  CGFloat(local) + 30.0
           print ("Height", remarkHeightContraint)
        }
        
        if updatedText.count > 0{
            remarkCloseButton.isHidden = false
            remarkheadingLabel.text = "Remarks :".localized
        }
        else{
            remarkCloseButton.isHidden = true
            remarkheadingLabel.attributedText = self.returnAttributtedtextview()
        }
//
//        if remarkTextView.text.count > 0 && remarkTextView.text != ""{
//
//        }
//        else{
////            if remarkTextView.text.count == 1 && remarkTextView.text != ""{
////
////                remarkCloseButton.isHidden = false
////            }else{
//                remarkCloseButton.isHidden = true
//        //    }
//        }
        return true
    }
    
}

extension BankAccountViewController{
    func returnAttributtedAmount() -> NSAttributedString {

    let customAttribute1 = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 16) ?? UIFont.systemFont(ofSize: 16),
    NSAttributedString.Key.foregroundColor: UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)]
        let attributedAmountString = NSMutableAttributedString.init(string: "Amount".localized, attributes: customAttribute1)

    let customAttribute2 = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 15) ?? UIFont.systemFont(ofSize: 15),
    NSAttributedString.Key.foregroundColor: UIColor.red ]
    let mmkString = NSMutableAttributedString.init(string: " * ", attributes: customAttribute2)
        
    let customAttribute3 = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 10) ?? UIFont.systemFont(ofSize: 10),
    NSAttributedString.Key.foregroundColor: UIColor.red ]
        let requiredString = NSMutableAttributedString.init(string: "(Required)".localized, attributes: customAttribute3)

    attributedAmountString.append(mmkString)
    attributedAmountString.append(requiredString)

    return attributedAmountString

    //self.amountTF.attributedPlaceholder = attributedAmountString
    }
    
    func returnAttributtedTitle() -> NSAttributedString {

    let customAttribute1 = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 16) ?? UIFont.systemFont(ofSize: 16),
    NSAttributedString.Key.foregroundColor: UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)]
        let attributedAmountString = NSMutableAttributedString.init(string: "Amount".localized, attributes: customAttribute1)


    return attributedAmountString

    //self.amountTF.attributedPlaceholder = attributedAmountString
    }
    
    func returnAttributtedmmk() -> NSAttributedString {
        
        let customAttribute1 = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 16) ?? UIFont.systemFont(ofSize: 16),
                                 NSAttributedString.Key.foregroundColor: UIColor.black]
        let attributedAmountString = NSMutableAttributedString.init(string:"\("Level - 1 ")\("                 ") \("Max".localized) \(self.valueamount)" , attributes: customAttribute1)

    let customAttribute2 = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 14) ?? UIFont.systemFont(ofSize: 14),
    NSAttributedString.Key.foregroundColor: UIColor.black ]
        let mmkString = NSMutableAttributedString.init(string: " MMK".localized, attributes: customAttribute2)
        
       attributedAmountString.append(mmkString)
    return attributedAmountString


    }
    
    func returnAttributtedtextview() -> NSAttributedString {
        
    //    "\("Level - 1 ")\("     ") \("Max".localized) \(valueamount)
        
        let customAttribute1 = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 16) ?? UIFont.systemFont(ofSize: 16),
                                 NSAttributedString.Key.foregroundColor: UIColor(red: 1.0/255.0, green: 25.0/255.0, blue: 147.0/255.0, alpha: 1.0)]
        let attributedAmountString = NSMutableAttributedString.init(string:"Enter Remarks ".localized , attributes: customAttribute1)

    let customAttribute2 = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 14) ?? UIFont.systemFont(ofSize: 14),
    NSAttributedString.Key.foregroundColor: UIColor.darkGray ]
        let mmkString = NSMutableAttributedString.init(string: " (optional)".localized, attributes: customAttribute2)
        
       attributedAmountString.append(mmkString)
    return attributedAmountString


    }
}





extension BankAccountViewController: CountryViewControllerDelegate{
    
    func countryViewController(_ list: CountryViewController, country: Country) {
        self.country = country
        self.mobileNumberTF.leftView = nil
        
        let countryCode = String.init(format: "(%@)", country.dialCode)
        countryView?.wrapCountryViewData(img: country.code, str: countryCode)
        self.mobileNumberTF.leftView = countryView
        list.dismiss(animated: true, completion: nil)
        
        switch self.country?.dialCode {
        case "+91":
            currentSelectedCountry = .indiaCountry
        case "+95":
            currentSelectedCountry = .myanmarCountry
        case "+66":
            currentSelectedCountry = .thaiCountry
        case "+86":
            currentSelectedCountry = .chinaCountry
        default:
            currentSelectedCountry  = .other
        }
        
        self.mobileNumberTF.text = (self.country?.dialCode == "+95") ? "09" : ""
        self.mobileNumberTF.becomeFirstResponder()
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: true, completion: nil)
    }
    
  func publicCountryView() {
//        guard let countryVC = countryViewController(delegate: self) else { return }
//        countryVC.modalPresentationStyle = .fullScreen
//        self.present(countryVC, animated: true, completion: nil)
         let sb = UIStoryboard(name: "Country", bundle: nil)
        let countryVC = sb.instantiateViewController(withIdentifier: "CountryViewController") as! CountryViewController
        countryVC.delegate = self
        countryVC.modalPresentationStyle = .fullScreen
        self.navigationController?.present(countryVC, animated: false, completion: nil)
    
    }
    
}
