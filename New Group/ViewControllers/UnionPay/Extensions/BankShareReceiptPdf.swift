//
//  BankShareReceiptPdf.swift
//  OK
//
//  Created by iMac on 12/01/2021.
//  Copyright © 2021 Vinod Kumar. All rights reserved.
//

import UIKit

extension BankReceiptViewController {
    
    func dynamicArray() {
        //True means hide that
          listTitleValue = [
                    ["title" : "Transfer By" ,"value" : ""],
                    ["title" : "Name" ,"value" : okdollarusernameLabel.text ?? ""],
                    ["title" : "OK$ Mobile" ,"value" : okdollarusermobileLabel.text ?? ""],
                    ["title" : "Beneficiary Details" ,"value" : ""],
                    ["title" : "Name","value" : userNameLabel.text ?? ""],
                    ["title" : "A/C Number Mobile" ,"value" : userAccountnumLabel.text ?? ""],
                    ["title" : "Transaction Details" ,"value" : ""],
                    ["title" : "OK$ Txn ID","value" : userTransactionLabel.text ?? ""],
                    ["title" : "Amount" ,"value" :  wrapAmountWithCommaDecimal(key: amount)],
                    ["title" : "Fees" ,"value" : wrapAmountWithCommaDecimal(key: fees)],
                    ["title" : "Total Amount" ,"value" : wrapAmountWithCommaDecimal(key: totalamount)],
                    ["title" : "Remarks" ,"value" : remarks]
                ]
         
    }
    

    func createPDF() {
           //Image Icon
            self.dynamicArray()
        
           yAxis = 30
           let imgAppIcon = UIImageView(frame: CGRect(x: (self.view.frame.width/2) - 40, y: yAxis, width: 80, height: 80))
           imgAppIcon.image = UIImage(named: "appIcon_Ok")
           self.pdfview.addSubview(imgAppIcon)
           //Y= 120
           yAxis = 120
           let lblPaymentReceipt = UILabel(frame: CGRect(x: 0, y: yAxis, width: self.view.frame.width, height: 40))
           lblPaymentReceipt.text = "PAYMENT RECEIPT".localized
           lblPaymentReceipt.backgroundColor = #colorLiteral(red: 0.1096028909, green: 0.2039180398, blue: 0.5867625475, alpha: 1)
           lblPaymentReceipt.textAlignment = .center
           lblPaymentReceipt.textColor = .white
           lblPaymentReceipt.font = UitilityClass.getZwagiFontWithSize(size: 16.0)//UIFont.systemFont(ofSize: 16)
           self.pdfview.addSubview(lblPaymentReceipt)
           //Y= 160
           yAxis = 160
           let dateView = UILabel(frame: CGRect(x: 0, y: yAxis, width: self.view.frame.width, height: 50))
           dateView.backgroundColor = .clear
           
           let imgDate = UIImageView(frame: CGRect(x: 10, y: 15, width: 20, height: 20))
           imgDate.image = UIImage(named: "merchantCalendar")
           dateView.addSubview(imgDate)
           
           let lblDate = UILabel()
           lblDate.text = self.getDate()
           lblDate.font = UitilityClass.getZwagiFontWithSize(size: 12.0)//UIFont.systemFont(ofSize: 12)
           lblDate.textAlignment = .left
           lblDate.sizeToFit()
           lblDate.frame = CGRect(x: 35, y: 0, width: lblDate.frame.width, height: 50)
           dateView.addSubview(lblDate)
           
           let lblTime = UILabel()
           lblTime.text = self.getTime()
           lblTime.font = UitilityClass.getZwagiFontWithSize(size: 12.0)//UIFont.systemFont(ofSize: 12)
           lblTime.textAlignment = .left
           lblTime.sizeToFit()
           lblTime.frame = CGRect(x: self.view.frame.width - 65, y: 0, width: lblTime.frame.width, height: 50)
           dateView.addSubview(lblTime)
           
           let imgTime = UIImageView(frame: CGRect(x: lblTime.frame.origin.x - 30, y: 15, width: 20, height: 20))
           imgTime.image = UIImage(named: "merchantClock")
           dateView.addSubview(imgTime)
                 
           yAxis = 210
           let upiView = UILabel(frame: CGRect(x: 10, y: yAxis, width: self.view.frame.width - 20, height: 60))
           upiView.backgroundColor = .clear
        
        let urlFront = userDef.value(forKey: "UnionPayLogo")
        let imageUrl = URL(string: urlFront as! String)
        let imgUPI = UIImageView(frame: CGRect(x:10, y: 10, width: 40, height: 40))
        //imgUPI.backgroundColor = .red
        imgUPI.contentMode = .scaleAspectFit
        imgUPI.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "bank"))
        //imgUPI.layer.cornerRadius = 20
        //imgUPI.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        //imgUPI.layer.borderWidth = 1.0
        //imgUPI.layer.masksToBounds = true
        upiView.addSubview(imgUPI)
           
           let lblUPI = UILabel()
           lblUPI.text = bankNameLabel.text
           lblUPI.font = UitilityClass.getZwagiFontWithSize(size: 17.0)//UIFont.systemFont(ofSize: 17)
           lblUPI.textAlignment = .center
           lblUPI.sizeToFit()
           lblUPI.frame = CGRect(x: 60, y: 0, width: lblUPI.frame.width, height: 60)
           upiView.addSubview(lblUPI)
        
           let lblUPISep = UILabel()
           lblUPISep.text = ""
           lblUPISep.frame = CGRect(x: 0, y: 59, width: upiView.frame.width, height: 1)
           lblUPISep.backgroundColor = #colorLiteral(red: 0.8273780942, green: 0.8274977207, blue: 0.8273518085, alpha: 1)
           upiView.addSubview(lblUPISep)
           //Y = 270
           yAxis = 270
           
           self.pdfview.addSubview(dateView)
           self.pdfview.addSubview(upiView)
                   
        if let list = listTitleValue {
            for (index, _) in list.enumerated() {
                let dicTitle = list[index]
                let title = dicTitle["title"]
               
                let containerView = UIView(frame: CGRect(x: 10, y: yAxis, width: self.view.frame.width - 20, height: 0))
                if title == "Transfer By" || title == "Beneficiary Details" || title == "Transaction Details" {
                    let lblTitle = UILabel()
                    let strTitle = title?.localized ??  ""
                    lblTitle.text = " " + strTitle
                    lblTitle.textAlignment = .left
                    lblTitle.font = UitilityClass.getZwagiFontWithSize(size: 15.0)//UIFont.systemFont(ofSize: 15)
                    lblTitle.numberOfLines = 0
                    lblTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
                    lblTitle.textColor = #colorLiteral(red: 0.1096028909, green: 0.2039180398, blue: 0.5867625475, alpha: 1)
                    lblTitle.backgroundColor = #colorLiteral(red: 0.8273780942, green: 0.8274977207, blue: 0.8273518085, alpha: 1)
                    lblTitle.frame = CGRect(x: 10, y: 10, width: containerView.frame.width-20, height: 40)
                    containerView.addSubview(lblTitle)
                 
                    containerView.frame = CGRect(x: 10, y: yAxis, width: self.view.frame.width - 20, height: lblTitle.frame.height + 20)
                   
                } else {
                    let lblTitle = UILabel()
                    lblTitle.text = title?.localized  ?? ""
                lblTitle.textAlignment = .left
                lblTitle.font = UitilityClass.getZwagiFontWithSize(size: 15.0)//UIFont.systemFont(ofSize: 15)
                lblTitle.numberOfLines = 0
                lblTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
                lblTitle.frame = CGRect(x: 10, y: 10, width: 140, height: heightForViewUPI(text: title ?? "", font: UIFont.systemFont(ofSize: 15), width: 140))
                containerView.addSubview(lblTitle)
                
                let lblColon = UILabel()
                lblColon.text = ":"
                lblColon.textAlignment = .center
                lblColon.font = UIFont.systemFont(ofSize: 15)
                lblColon.numberOfLines = 0
                lblColon.frame = CGRect(x: 150, y: 8, width: 10, height: heightForViewUPI(text:  ":", font: UIFont.systemFont(ofSize: 15), width: 10))
                containerView.addSubview(lblColon)
             
             let lineLabel = UILabel()
             lineLabel.textAlignment = .center
             lineLabel.font = UIFont.systemFont(ofSize: 15)
             lineLabel.numberOfLines = 0
             lineLabel.frame = CGRect(x: 150, y: 8, width: 2, height: 8)
             containerView.addSubview(lineLabel)
                
                let dicValue = list[index]
                let value = dicValue["value"]
                let lblValue = UILabel()
                lblValue.text = value ?? ""
                lblValue.textAlignment = .left
                lblValue.font = UitilityClass.getZwagiFontWithSize(size: 15.0)//UIFont.systemFont(ofSize: 15)
                lblValue.numberOfLines = 0
                lblValue.lineBreakMode = NSLineBreakMode.byWordWrapping
                lblValue.frame = CGRect(x: 180, y: 10, width: containerView.frame.width - 165, height: heightForViewUPI(text: value ?? "", font: UIFont.systemFont(ofSize: 15) , width: containerView.frame.width - 160))
                containerView.addSubview(lblValue)
                    
                    
             if lblTitle.text == "Amount".localized || lblTitle.text == "Fees".localized || lblTitle.text == "Total Amount".localized {
                 
                 let mmkLabel = UILabel()
                 mmkLabel.textAlignment = .center
                 mmkLabel.font = UitilityClass.getZwagiFontWithSize(size: 10.0)//UIFont.systemFont(ofSize: 10)
                 mmkLabel.text = "MMK"
                 mmkLabel.frame = CGRect(x: lblValue.frame.size.width + 5 , y: 7 , width: 30, height: 24)
                 containerView.addSubview(mmkLabel)
               
                 
             }
               
             
                if (lblTitle.frame.height == lblValue.frame.height) || (lblTitle.frame.height > lblValue.frame.height) {
                    containerView.frame = CGRect(x: 10, y: yAxis, width: self.view.frame.width - 20, height: lblTitle.frame.height + 20)
                }else {
                    containerView.frame = CGRect(x: 10, y: yAxis, width: self.view.frame.width - 20, height: lblValue.frame.height + 20)
                }
                }
                let lblSap = UILabel()
                lblSap.text = ""
                lblSap.backgroundColor = #colorLiteral(red: 0.8273780942, green: 0.8274977207, blue: 0.8273518085, alpha: 1)
                lblSap.frame = CGRect(x: 10, y: containerView.frame.height - 1, width: containerView.frame.width-20, height: 1)
                containerView.addSubview(lblSap)
                
                containerView.backgroundColor = #colorLiteral(red: 0.9638236165, green: 0.9687631726, blue: 0.9728998542, alpha: 1)
                
                 pdfview.addSubview(containerView)
                
                print("Height: \(yAxis)")
                yAxis = yAxis + containerView.frame.height
          
            }
        }
        
        
//        let imgFooter = UIImageView(frame: CGRect(x: 0, y: yAxis + 30, width: self.view.frame.width, height: 30))
//        imgFooter.image = UIImage(named: "footerSMB")
//        self.pdfview.addSubview(imgFooter)
//
           pdfview.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: yAxis + 50)
           self.bgView.frame.size.height = yAxis + 50
           self.bgView.addSubview(pdfview)
           
       }
       
       func heightForViewUPI(text:String, font:UIFont, width:CGFloat) -> CGFloat {
            let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
            label.numberOfLines = 0
            label.lineBreakMode = NSLineBreakMode.byWordWrapping
            label.font = font
            label.text = text
            label.sizeToFit()
            return label.frame.height
        }
    
    func getDate() -> String {
         let date = Date()
         let formatter = DateFormatter()
         formatter.calendar = Calendar(identifier: .gregorian)
         formatter.dateFormat = "EEE, dd-MMM-yyyy"
         return formatter.string(from: date)
     }
     
     func getTime() -> String {
         let date = Date()
         let formatter = DateFormatter()
         formatter.calendar = Calendar(identifier: .gregorian)
         formatter.dateFormat = "HH:mm:ss"
         return formatter.string(from: date)
     }
}
