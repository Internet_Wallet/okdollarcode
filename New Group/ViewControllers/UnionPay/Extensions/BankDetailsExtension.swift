//
//  BankDetailsExtension.swift
//  OK
//
//  Created by vamsi on 7/27/20.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

extension BankDetailsViewController : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
           return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if bankDetailsArray.count > 0 {
//            return 1
//        }
        return bankDetailsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier: String = "Cell"
        
        
              
              var cell:BankDetailsTableViewCell? = self.bankdetailsTableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? BankDetailsTableViewCell
              
            //  cell?.separatorInset = UIEdgeInsets.zero
              
              if cell == nil {
                  cell = BankDetailsTableViewCell(style: .default, reuseIdentifier: cellIdentifier)
              }

        let sampleDict = bankDetailsArray[indexPath.row] 

               let banklogo = String(describing: sampleDict["Logo"]!)
              cell?.bankNameLAbel.text = String(describing: sampleDict["Name"]!)
             // cell?.bankImageView.image = UIImage(named: banklogos[indexPath.row] as! String)
              let urlFront = URL(string: banklogo)
              cell?.bankImageView.sd_setImage(with: urlFront, placeholderImage: nil)
              cell?.selectionStyle = .none
              
              return cell!
        
    }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            let sampleDict = bankDetailsArray[indexPath.row] as! [String: Any]
            let banklogo = String(describing: sampleDict["Logo"]!)
            
          //  if bankname == "UAB Bank"{
                var homeVC: BankAccountMobileNumberViewController?
                 homeVC = UIStoryboard(name: "UnionPay", bundle: nil).instantiateViewController(withIdentifier: "BankAccountMobileNumberViewController") as? BankAccountMobileNumberViewController
                   homeVC!.projectid = String(describing: sampleDict["ProjectId"]!)
                   homeVC!.bankName = String(describing: sampleDict["Name"]!)
                  // homeVC!.limitdict = limitDict
                   homeVC!.walletImage = banklogo
                   userDef.set(banklogo, forKey: "UnionPayLogo")
                   userDef.synchronize()
            
                   if let homeVC = homeVC {
                        navigationController?.pushViewController(homeVC, animated: true)
                    }
          //  }
            else
            {
               alertViewObj.wrapAlert(title: "OK$", body: "ComingSoon!!!".localized, img: #imageLiteral(resourceName: "alert-icon"))
               alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
               alertViewObj.showAlert(controller: self)
            }
            
          
            
        }
    
    
    
}




extension UILabel {

    @IBInspectable
    var letterSpace: CGFloat {
        set {
            let attributedString: NSMutableAttributedString!
            if let currentAttrString = attributedText {
                attributedString = NSMutableAttributedString(attributedString: currentAttrString)
            } else {
                attributedString = NSMutableAttributedString(string: text ?? "")
                text = nil
            }
            attributedString.addAttribute(NSAttributedString.Key.kern,
                                          value: newValue,
                                          range: NSRange(location: 0, length: attributedString.length))
            attributedText = attributedString
        }

        get {
            if let currentLetterSpace = attributedText?.attribute(NSAttributedString.Key.kern, at: 0, effectiveRange: .none) as? CGFloat {
                return currentLetterSpace
            } else {
                return 0
            }
        }
    }
}
