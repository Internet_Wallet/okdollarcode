//
//  UnionHelperCustomObj.swift
//  OK
//
//  Created by Tushar Lama on 23/10/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation

struct Wallet: Codable {
    var number : String
    var name: String
    var projectID: String
}
