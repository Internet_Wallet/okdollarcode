//
//  FavouriteListController.swift
//  OK
//
//  Created by iMac on 10/2/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import CoreData

class FavouriteListController: OKBaseController {

    @IBOutlet weak var newBackGroundView: UIView!
    @IBOutlet weak var tablBackGroundView: UIView!
    @IBOutlet weak var favouriteTable: UITableView!
     var favouriteResult: [NSManagedObject] = []
    var promotionModelObj = [PromotionsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tablBackGroundView.giveBorderShadowCornerRadiusToView()
        
        
        //for table view border
        favouriteTable.layer.borderColor = UIColor.clear.cgColor
        favouriteTable.layer.borderWidth = 1.0
        
        //for shadow
        let containerView:UIView = UIView(frame:self.favouriteTable.frame)
        containerView.backgroundColor = UIColor.clear
        containerView.layer.shadowColor = UIColor.lightGray.cgColor
        containerView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowRadius = 2
        
        //for rounded corners
        favouriteTable.layer.cornerRadius = 10
        favouriteTable.layer.masksToBounds = true
        self.view.addSubview(containerView)
        containerView.addSubview(favouriteTable)
        
       
      self.navigationController?.navigationBar.isHidden = true
        for i in 0..<favouriteResult.count{
            if let archiveData = favouriteResult[i].value(forKey: "promotionData") {
                if let archiveArray = NSKeyedUnarchiver.unarchiveObject(with: archiveData as! Data) as? PromotionsModel {
                    promotionModelObj.append(archiveArray)
                }
            }
        }
        favouriteTable.separatorStyle = .none
        if promotionModelObj.count>0{
            favouriteTable.delegate  = self
            favouriteTable.dataSource = self
            favouriteTable.reloadData()
        }
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }
    
    @IBAction func onClickCrossButton(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

//
extension FavouriteListController : UITableViewDelegate,UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return promotionModelObj.count
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FavouriteCellNew") as? FavouriteCellNew else {
            return UITableViewCell()
        }
   let mobileNumber = getPlusWithDestinationNum(promotionModelObj[indexPath.row].shop_Phonenumber ?? "", withCountryCode: "+95")
        
        //Made this check to avoid crash in future
        if mobileNumber[0] == "+" &&  mobileNumber[1] == "9" && mobileNumber[2] == "5"{
            cell.agentPhoneNumber.text = mobileNumber.replacingOccurrences(of: "+95", with: "(+95) 0")
        }else{
            cell.agentPhoneNumber.text = mobileNumber
        }
        cell.contactView.tag = indexPath.row
        cell.delegate = self
       cell.agentNameLabel.text = promotionModelObj[indexPath.row].shop_AgentName
       cell.agentShopNameLabel.text = promotionModelObj[indexPath.row].shop_Name
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140.0
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            
            let alertController = UIAlertController(title: "", message: "Do You Want To Delete This User", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "Yes", style: .default){
                UIAlertAction in
                //
                self.delete(userId: self.promotionModelObj[indexPath.row].shop_Id ?? "")
            }  
            
            let noAction = UIAlertAction(title: "No", style: .default){
                UIAlertAction in
                
            }
            
            alertController.addAction(okAction)
            alertController.addAction(noAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isComingFromMapMakePayment = true
        self.navigateToPayto(phone: promotionModelObj[indexPath.row].shop_Phonenumber ?? "" , amount: "", name: promotionModelObj[indexPath.row].shop_AgentName ?? "", fromMerchant: true)

    }


}

extension FavouriteListController: ContactProtocol {
    func getPhoneNumber(index: Int) {
        let number = promotionModelObj[index].shop_Phonenumber ?? ""
        
        let phone = "tel://\(getPlusWithDestinationNum(number, withCountryCode: "+95"))"
        if let url = URL.init(string: phone) {
            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        }
        else {
            alertViewObj.wrapAlert(title: nil, body: "No Phonenumber Exists".localized, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        }
        
    }
}

extension FavouriteListController{
    
        func delete(userId: String) {
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FavoritePromotion")
            let predicate = NSPredicate(format: "promotionId == %@", userId)
            fetchRequest.predicate = predicate
            do{
                let request = try context.fetch(fetchRequest)
                if let data = request as? [NSManagedObject]{
                    if data.count>0{
                        context.delete(data[0])
                        try context.save()
                        //fetch data again from Core data
                        self.fetch()
                    }
                }
            }catch{
                print(error)
            }
        }
        
        
        func fetch(){
            favouriteResult.removeAll()
            guard let appdelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            let managedContext = appdelegate.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FavoritePromotion")
            do{
                let result = try managedContext.fetch(fetchRequest)
                for data in result as! [NSManagedObject]{
                    favouriteResult.append(data)
                }
                
                if favouriteResult.count>0{
                    promotionModelObj.removeAll()
                    for i in 0..<favouriteResult.count{
                        if let archiveData = favouriteResult[i].value(forKey: "promotionData") {
                            if let archiveArray = NSKeyedUnarchiver.unarchiveObject(with: archiveData as! Data) as? PromotionsModel {
                                promotionModelObj.append(archiveArray)
                            }
                        }
                    }
                    favouriteTable.reloadData()
                }else{
                   self.dismiss(animated: true, completion: nil)
                }
            }catch{
                print("Error")
            }
            
            
            
        }
        
        
    

    
    
}

