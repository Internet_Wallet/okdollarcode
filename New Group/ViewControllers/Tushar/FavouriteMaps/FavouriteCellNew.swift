//
//  FavouriteCell.swift
//  OK
//
//  Created by iMac on 10/2/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol ContactProtocol {
    func getPhoneNumber(index: Int)
}

class FavouriteCellNew: UITableViewCell {

    @IBOutlet weak var contactView: UIView!
    @IBOutlet weak var agentPhoneNumber: UILabel!
    @IBOutlet weak var agentShopNameLabel: UILabel!
    @IBOutlet weak var agentNameLabel: UILabel!
    var delegate: ContactProtocol?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
      contactView.addGestureRecognizer(tap)
        
        contactView.isUserInteractionEnabled = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        delegate?.getPhoneNumber(index: contactView.tag)
    }
    
}
