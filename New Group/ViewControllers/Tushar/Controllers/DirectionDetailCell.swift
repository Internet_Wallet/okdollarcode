//
//  DirectionDetailCell.swift
//  OK
//
//  Created by iMac on 8/27/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class DirectionDetailCell: UITableViewCell {
    @IBOutlet weak var directionButtonOutlet: UIButton!
        @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var shopAddressLabel: UILabel!{
        didSet{
            shopAddressLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var shopNameLabel: UILabel!{
        didSet{
            shopNameLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var shopLabel: UILabel!{
        didSet{
            shopLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var addressLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var address: UILabel!{
        didSet{
            address.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
