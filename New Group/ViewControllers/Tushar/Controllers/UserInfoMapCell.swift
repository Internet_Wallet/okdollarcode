//
//  UserInfoMapCell.swift
//  OK
//
//  Created by iMac on 8/27/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class UserInfoMapCell: UITableViewCell {

    @IBOutlet weak var nameLabel: MarqueeLabel!{
        didSet{
            nameLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var companyNameLabel: UILabel!{
        didSet{
            companyNameLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var favouriteButtonOutlet: UIButton!
    //@IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!{
        didSet{
            categoryLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var categoryDetailLabel: UILabel!{
        didSet{
            categoryDetailLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var subCategoryLable: UILabel!{
        didSet{
            subCategoryLable.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var subCategoryDetailLabel: UILabel!{
        didSet{
            subCategoryDetailLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var shopNameLabel: UILabel!{
        didSet {
            shopNameLabel.font = UIFont(name: appFont, size: appFontSize)
            shopNameLabel.text = "Shop Name".localized
        }
    }
    
    @IBOutlet weak var phoneNumberLabel: UILabel!{
        didSet{
            phoneNumberLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var starOne: UIImageView!
    @IBOutlet weak var starTwo: UIImageView!
    @IBOutlet weak var starThree: UIImageView!
    @IBOutlet weak var starFour: UIImageView!
    @IBOutlet weak var starFive: UIImageView!
    
    @IBOutlet weak var phoneView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
