//
//  ShopImageViewCell.swift
//  OK
//
//  Created by iMac on 8/27/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class ShopImageViewCell: UITableViewCell {

    @IBOutlet weak var shopImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
