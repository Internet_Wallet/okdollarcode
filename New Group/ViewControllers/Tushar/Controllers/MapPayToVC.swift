//
//  MapPayToVC.swift
//  OK
//
//  Created by iMac on 8/23/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import MapKit
import Rabbit_Swift
import ParallaxHeader
import CoreData
import CoreLocation
import GoogleMaps


class MapPayToVC: PayToBaseViewController, CategoriesSelectionDelegate, SelectLocationDelegate,NRCDivisionStateDeletage,CurrentLocationViewControllerDelegate  {
    
    @IBOutlet weak var shopMakePaymentButton: UIButton!{
        didSet{
            self.shopMakePaymentButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
            self.shopMakePaymentButton.setTitle("Make Payment".localized, for: .normal)
        }
    }
    @IBOutlet weak var agentLabel: UILabel!{
        didSet{
            self.agentLabel.text = "Agents List".localized
            self.agentLabel.font = UIFont(name: appFont, size: appFontSize)
    }
    }
    @IBOutlet weak var shopTopView: UIView!
    @IBOutlet weak var shopViewHeight: NSLayoutConstraint!
    @IBOutlet weak var shopDetailView: UIView!
    @IBOutlet weak var shopTableView: UITableView!
    
    var infoBase = UIView()
    var photoBase = UIView()
    var descriptionBase = UIView()
    var infoButton = UIButton()
    var photoButton = UIButton()
    var descriptionButton = UIButton()
    var loaderView = UIView()
    
    ///Initial View
    
    @IBOutlet weak var initialCrossButton: UIButton!
    @IBOutlet weak var initialView: UIView!
    @IBOutlet weak var initialViewCrossButton: UIButton!
    @IBOutlet weak var initialNearByView: UIView!
    @IBOutlet weak var initialTableView: UITableView!
    @IBOutlet weak var initialViewAgentLabel: UILabel!
    @IBOutlet weak var initialFilterView: UIView!
    
    var translation: CGPoint!
    var startPosition: CGPoint! //Start position for the gesture transition
    var originalHeight: CGFloat = 0 // Initial Height for the UIView
    var difference: CGFloat!
    var height: CGFloat!
    var shopImageView = UIImageView()
    var showAgentShopView = false
    var headerButton = UIButton()
    var isPlayingScroll = false
    var checkIfComingFromOption = "info"
    var isComingFromInitialView = false
    
    var selectedLocationDetailStr : String?
    var selectedTownshipDetailStr : String?
    
    @IBOutlet weak var mapCrossButton: UIButton!
    @IBOutlet weak var alertViewPermission: UIView!
    @IBOutlet weak var mapViewContainer: UIView!
    @IBOutlet weak var listDisplayButton: ButtonWithBages!
    
    //This view is attached to the view
    @IBOutlet weak var listBackGroundView: UIView!
    
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var topBackGroundView: UIView!
    
    //prabu
    let defaults = UserDefaults.standard
    @IBOutlet weak var promotionInfoView: UIView!
    @IBOutlet weak var promotionImgView: UIImageView!
    @IBOutlet weak var promotionNameLbl: UILabel!
    @IBOutlet weak var contactNoLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var remarksLbl: MarqueeLabel!
    var infoViewTimer: Timer!
    var listData: [Any]?
    let regionRadius: CLLocationDistance = 500
    var checkurl  = "NearBy".localized
    
    //This will be used for animation view to play with view height
    var click = 0
    
    //This is for promotion
    var selectedNearByServicesNew: NearByServicesNewModel?
    var selectedPromotion: PromotionsModel?
    var iscategoryAPICall: Bool = false
    var categorylistArr = [String:Any]()
    var mainCatName: String = ""
    var strTimeDuration: String = ""
    var isfavoritePromotion: Bool = false
    
    @IBOutlet weak var shopDetailTopDownArrowButton: UIButton!
    //MapView
    @IBOutlet weak var mapView: UIView!
    
    //Filter
    @IBOutlet  weak var mapfilterTitileLbl : UILabel! {
        didSet {
            
            self.mapfilterTitileLbl.text = "Filter".localized
        }
    }
    @IBOutlet  weak var mapfilterBtnOut : UIButton!
    
    @IBOutlet  weak var newmapTitileLbl : UILabel! {
        didSet {
            self.newmapTitileLbl.text = "Map".localized
        }
    }
    @IBOutlet  weak var newmapBtnOut : UIButton!
    
    //Listview
    @IBOutlet weak var listView: UIView!
    
    @IBOutlet  weak var newlistTitileLbl : UILabel! {
        didSet {
            self.newlistTitileLbl.text = "List".localized
        }
    }
    @IBOutlet  weak var newlistBtnOut : UIButton!
    
    //Filter
    @IBOutlet  weak var listfilterTitileLbl : UILabel! {
        didSet {
            self.listfilterTitileLbl.text = "Filter".localized
        }
    }
    @IBOutlet  weak var listfilterBtnOut : UIButton!
    
    @IBOutlet  weak var lblCurrentLoc : UILabel!
        {
        didSet
        {
            lblCurrentLoc.text = lblCurrentLoc.text?.localized
        }
    }
    @IBOutlet  weak var lblList: UILabel!
        {
        didSet
        {
            lblList.text = lblList.text?.localized
        }
    }
    @IBOutlet  weak var lblCategories : UILabel!
        {
        didSet
        {
            lblCategories.text = lblCategories.text?.localized
        }
    }
    @IBOutlet  weak var lblSatView : UILabel!
        {
        didSet
        {
            lblSatView.text = lblSatView.text?.localized
        }
    }
    
    var navigation : UINavigationController?
    
    var pulsingEffect : PulseManager?
    
    @IBOutlet weak var crossButtonOutLet: UIButton!
    @IBOutlet weak var top: NSLayoutConstraint!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var listViewContainer: UIView!
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var heightListViewContainer: NSLayoutConstraint!
    @IBOutlet weak var heightUpDownImage: UIImageView!
    @IBOutlet weak var merchantCountglobalLabel: UILabel!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var locationOutlet: UILabel!
    var selectedpromotionData : PromotionsModel?
    
    @IBOutlet weak var shopDetailTopShareButton: UIButton!
    @IBOutlet weak var topHeight: MKMapView!
    // cluster manager
    let clusterManager = FBClusteringManager()
    
    var merchantModel = [NearByServicesNewModel]()
    var merchantModelBackup = [NearByServicesNewModel]()
    var selectedFilterData      : NearByServicesNewModel?
    var selectedMerchant : NearByServicesNewModel?
    var distance: String = ""
    var blueColor = UIColor(red: 28.0/255.0, green: 44.0/255.0, blue: 153.0/255.0, alpha: 1.0)
    //////////////////
    
    var defaultView = 0.0
    // list view data source
    var listMerchantModel : [String]?
    var selectedIndex = 0
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
     
        //This condition will execute only when we click on the agent list cell to show the details of the particular agent
        if isComingFromInitialView{
           loaderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height:  self.view.frame.size.height)
            loaderView.backgroundColor = .white
            self.view.addSubview(loaderView)
           self.view.bringSubviewToFront(loaderView)
        }else{
            //Else show the map view with agent list
            showMapView()
        }
    }
    
//    @objc func tappedMe(){
//
//    }
    
    @IBAction func onClikcMap(_ sender: Any) {
        showMapView()
    }

//    @objc func nearbyTap(){
//
//    }
    
    @IBAction func onClickinitialViewCrossButton(_ sender: Any) {
       // self.dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func shopDetailArrowButtonClick(_ sender: Any) {
          //  shopTopView.isHidden = true
        //    self.shopTopView.frame.origin.y = 0
            showAgentShopView = true
            self.headerButton.setImage(UIImage(named: "blueCross.png"), for: .normal)
            animationViewHeight(top: CGFloat((self.view.frame.size.height)/2 + 100))
    }
    
    @IBAction func shopDetailShareButtonClick(_ sender: Any) {
        shareAction()
    }
    
    @IBAction func onClickShopMakePayment(_ sender: Any) {
        isComingFromMapMakePayment = true
        self.navigateToPayto(phone: self.selectedPromotion?.shop_Phonenumber ?? "" , amount: "", name: self.selectedPromotion?.shop_AgentName ?? "", fromMerchant: true)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer){
        
        if let tag = sender.view?.tag{
            let phone = "tel://\(getPlusWithDestinationNum(selectedPromotion?.shop_Phonenumber ?? "", withCountryCode: "+95"))"
            if let url = URL.init(string: phone) {
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
            else {
                alertViewObj.wrapAlert(title: nil, body: "No Phonenumber Exists".localized, img: nil)
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                alertViewObj.showAlert(controller: self)
            }
            
            
        }
        
    }
    
    
  
    
    @objc func reloadViewData() {
        println_debug("reloadViewData")
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: String(describing: CurrentLocationViewController.self)) as? CurrentLocationViewController else { return }
        vc.delegateData = self
        self.navigation?.pushViewController(vc, animated: true)
    }
    
    func designView(animateView: UIView,cornerRadius: CGFloat,borderWidth: CGFloat){
       animateView.layer.cornerRadius = cornerRadius
       animateView.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigation = PaytoConstants.global.navigation
        self.navigationController?.isNavigationBarHidden = true
        self.locationAuthorisation()
        
        if isComingFromInitialView{
            if let model = self.merchantModel[safe: selectedIndex] {
                selectedNearByServicesNew = model
                if let distance =  model.distanceInKm {
                    let roundValue = distance
                    if roundValue > 1000 {
                        let stringDistance = String.init(format: "%.2f Km", roundValue/1000)
                        self.distance = stringDistance
                    } else {
                        let stringDistance = String.init(format: "%.2f m", roundValue)
                        self.distance = stringDistance
                    }
                }
            }
            //This method will give u the detail of particular agent on shopTableView
            callPromotionDetails()
        }else{
            if merchantModel.count == 0 && merchantModelBackup.count == 0{
                self.currentLocationUpdate()
            }else{
                self.addClustersToMap(list: merchantModel)
            }
            
        }
    }
    
    
    @IBAction func onClickMapCross(_ sender: Any) {
        
       /// if isComingFromInitialView{
        //This notification will help to pop the agent list back to the dashboard Vc
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PopToDashboard"), object: nil)
             self.dismiss(animated: true, completion: nil)
      //  }else{
        //     self.dismiss(animated: true, completion: nil)
      //  }
       
      // setViewWithAnimation(view: initialView, hidden: false)
    }
    
    
    
    @objc func handleTapCall(_ sender: UITapGestureRecognizer){
        
        if let tag = sender.view?.tag{
            let phone = "tel://\(getPlusWithDestinationNum(selectedPromotion?.shop_Phonenumber ?? "", withCountryCode: "+95"))"
            if let url = URL.init(string: phone) {
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
            else {
                alertViewObj.wrapAlert(title: nil, body: "No Phonenumber Exists".localized, img: nil)
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                alertViewObj.showAlert(controller: self)
            }
            
            
        }
        
    }
    
    
    @IBAction func goToSettings(_ sender: UIButton) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:])
                , completionHandler: nil)
        } else {
            UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
        }
    }
    
    // Helper function inserted by Swift 4.2 migrator.
    fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onClickCross(_ sender: Any) {
        animationViewHeight(top: CGFloat((self.view.frame.size.height)/2 + 100))
        click = 2
    }
    
    
    func didSelectLocationByCategory(category: SubCategoryDetail?) {
        if let cat = category {
            var subCategoryCode = cat.subCategoryCode
            if subCategoryCode.length == 1 {
                subCategoryCode = "0\(cat.subCategoryCode)"
            }
            
            let array = self.merchantModelBackup.filter({$0.UserContactData?.BusinessName == subCategoryCode})
            defaults.set(cat.mainCategoryName + "," + cat.subCategoryName, forKey: "PTMcategoryBy")
            self.addClustersToMap(list: array)
        } else {
            let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
            let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
            self.callNearbyMerchantWithCustomLocation(lat: lat, long: long)
        }
    }
    
    
    //MARK:- Select Location Delegate
    func didSelectLocation(withOptions option: SelectLocationEnum) {
        switch option {
        case .currentLocation:
            self.currentLocationUpdate()
            break
        case .division:
            self.merchantWithDivisions()
            break
        case .nearbyMerchants:
            //print("Nearbymerchant called")
            notfDef.post(name:Notification.Name(rawValue:"PayToStateDivision"),
                         object: nil,
                         userInfo: ["Name":  Rabbit.uni2zg(String.init(format: "%@","Near By".localized))])
            
            let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
            let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
            self.callNearbyMerchantWithCustomLocation(lat: lat, long: long)
            
            break
        case .allMerchants:
            let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
            let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
            self.callNearbyMerchantWithCustomLocation(lat: lat, long: long)
            
            break
        case .merchantWithPromotions:
            let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
            let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
            self.callNearbyMerchantWithCustomLocation(lat: lat, long: long)
            break
        }
    }
    
    private func merchantWithDivisions() {
        let viewController = UIStoryboard(name: "TaxPayment", bundle: Bundle.main).instantiateViewController(withIdentifier: "NRCDivisionRoot")
        if let nav = viewController as? UINavigationController {
            for views in nav.viewControllers {
                if let vc = views as? TaxNRCDivisionState {
                    vc.townShipDelegate = self
                    vc.fromPaytoMap = ""
                    break
                }
            }
        }
        viewController.modalPresentationStyle = .fullScreen
        self.navigation?.present(viewController, animated: true, completion: nil)
    }
    
    //State Division delegate
    func setStateDivisionDetails(division: LocationDetail) {
        println_debug(division.stateOrDivitionCode)
    }
    
    //func setDivisionDetails(divisionName: String, township: String) {
    func setDivisionDetails(stateDivCode: LocationDetail, divisionName: String, township: String) {
        self.locationOutlet.text = township + "," + divisionName
        notfDef.post(name:Notification.Name(rawValue:"PayToStateDivision"),
                     object: nil,
                     userInfo: ["Name":  divisionName + "," + township])
        
        geoLocManager.getLatLongByName(cityname: township) { (isSuccess, lat, long) in
            guard isSuccess, let latti = lat, let longi = long  else {
                self.showErrorAlert(errMessage: "Try Again".localized)
                return
            }
            
            DispatchQueue.main.async {
                let locationLat  = CLLocationDegrees.init(Double(latti) ?? 0.0)
                let locationLong = CLLocationDegrees.init(Double(longi) ?? 0.0)
                
                let locationData = CLLocationCoordinate2D.init(latitude: locationLat , longitude: locationLong)
                let distance = 1.0 //self.slider.value / 3.2808
                let coordinateRegion = MKCoordinateRegion.init(center: locationData, latitudinalMeters: CLLocationDistance(distance), longitudinalMeters: CLLocationDistance(distance))
                self.map.setRegion(coordinateRegion, animated: true)
                self.callNearbyMerchantWithCustomLocation(lat: latti, long: longi)
                
            }
        }
    }
    
    func merchantWithPromotions() {
        self.currentLocationUpdate()
    }
    
    func currentLocationUpdate() {
        geoLocManager.getAddressFrom(lattitude: GeoLocationManager.shared.currentLatitude ?? "0.0" , longitude: GeoLocationManager.shared.currentLongitude ?? "0.0", language: appDel.currentLanguage, completionHandler: { [weak self](success, address) in
            DispatchQueue.main.async {
                if success {
                    if let adrs = address {
                        if let dictionary = adrs as? Dictionary<String,Any> {
                            let street = dictionary.safeValueForKey("street") as? String ?? ""
                            let township = dictionary.safeValueForKey("township") as? String ?? ""
                            let city = dictionary.safeValueForKey("region") as? String ?? ""
                            self?.locationOutlet.text = Rabbit.uni2zg(String.init(format: "%@ ,%@ ,%@", street , township + " ", city))
                            notfDef.post(name:Notification.Name(rawValue:"PayToStateDivision"),
                                         object: nil,
                                         userInfo: ["Name":  Rabbit.uni2zg(String.init(format: "%@ ,%@ ,%@", street , township + " ", city))])
                            
                        }
                    }
                }
            }
        })
        
        let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
        let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
        self.callNearbyMerchantWithCustomLocation(lat: lat, long: long)
        
    }
    
    
    @objc func buttonAction(sender: UIButton!) {
        checkIfComingFromOption = "info"
        changeButtonColor(backGroundColor: blueColor, textColor: .black, button: infoButton, baseView: infoBase)
        changeButtonColor(backGroundColor: .white, textColor: .lightGray, button: photoButton, baseView: photoBase)
        changeButtonColor(backGroundColor: .white, textColor: .lightGray, button: descriptionButton, baseView: descriptionBase)
       
        if isComingFromInitialView {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PopToDashboard"), object: nil)
           self.dismiss(animated: true, completion: nil)
        }else{
            height = CGFloat((self.view.frame.size.height)/2 + 100) - 50
            isPlayingScroll = false
            
            //This bool is used for animation when the shop view will come and when its top axis is zero that time we are setting this bool so that scroll view delegate wil not call again
            if headerButton.currentImage == UIImage(named: "arrow_down.png"){
                showAgentShopView = true
                self.headerButton.setImage(UIImage(named: "blueCross.png"), for: .normal)
                animationViewHeight(top: CGFloat((self.view.frame.size.height)/2 + 100))
            }else{
                showAgentShopView = false
                setViewWithAnimation(view: shopDetailView, hidden: true)
                setViewWithAnimation(view: topBackGroundView, hidden: false)
                setViewWithAnimation(view: listViewContainer, hidden: false)
                self.animationViewHeight(top: CGFloat((self.view.frame.size.height)/2 + 100))
 
            }
        }
    }
    
    @objc func buttonActionMap(sender: UIButton!){
        shareAction()
    }
}

extension MapPayToVC: MainFilterApplyViewDelegate {
    func didSelectApplyFilter(category: SubCategoryDetail? , index:Int , str:Int){
        let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
        let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
        self.callNearbyMerchantWithCustomLocationNew(lat: lat, long: long, str: str , index:index)
    }
}


extension MapPayToVC : MKMapViewDelegate {
    func locationAuthorisation() {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            self.alertViewPermission.isHidden = false
            break
        case .restricted:
            self.alertViewPermission.isHidden = false
            break
        case .denied:
            self.alertViewPermission.isHidden = false
            break
        case .authorizedAlways:
            GeoLocationManager.shared.startUpdateLocation()
            self.alertViewPermission.isHidden = true
            break
        case .authorizedWhenInUse:
            GeoLocationManager.shared.startUpdateLocation()
            self.alertViewPermission.isHidden = true
            break
        }
    }
    
    func addClustersToMap(list: [NearByServicesNewModel]) {
        
        if list.count <= 0 {
            PaytoConstants.alert.showErrorAlert(title: nil, body: "No Records Found!".localized)
            return
        } else {
            
            merchantModel.removeAll()
            merchantModel = list
            listTableView.reloadData()
        }
        
        removeAllAnnotationsInMap()
        listData = list
        var clusterArray: [FBAnnotation] = []
        var value = 0
        while value < list.count {
            if let model = list[value] as? NearByServicesNewModel {
           
                if let lat = model.UserContactData?.LocationNewData?.Latitude , let long = model.UserContactData?.LocationNewData?.Longitude {
                   // print("AnotationLatitudeandlogtitude-----")
                    let annotation = FBAnnotation()
                    annotation.coordinate = CLLocationCoordinate2D(latitude:  lat, longitude: long)
                    if model.UserContactData?.BusinessName == "" {
                        if model.UserContactData?.FirstName != "" {
                            annotation.title = model.UserContactData?.FirstName
                        }
                    } else {
                        annotation.title = model.UserContactData?.BusinessName
                    }
                    
                    let pinImage = UIImage(named: "ok_logo.png")
                    annotation.image = pinImage
                    annotation.userData = model
                    clusterArray.append(annotation)
                    
//                    if value == list.count-2 {
//                        let initialLocation = CLLocation(latitude: Double(lat), longitude: Double(long))
//                        //centerMapOnLocation(location: initialLocation)
//                    }
                }
            }
            value += 1
        }
       
        if clusterArray.count<=3{
           
            let latitude:CLLocationDegrees =  list[0].UserContactData?.LocationNewData?.Latitude ?? 0.0
            
            let longitude:CLLocationDegrees = list[0].UserContactData?.LocationNewData?.Longitude ?? 0.0
            
            let latDelta:CLLocationDegrees = Double(clusterArray.count) * 0.2
            
            let lonDelta:CLLocationDegrees = Double(clusterArray.count) * 0.2
            
            let span = MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: lonDelta)
            
            let location = CLLocationCoordinate2DMake(latitude, longitude)
            
            let region = MKCoordinateRegion(center: location, span: span)
            
            map.setRegion(region, animated: false)
            
            
            
        }
      
        clusterManager.add(annotations: clusterArray)
        reloadMap()
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion.init(center: location.coordinate,latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        self.map.setRegion(coordinateRegion, animated: true)
    }
    
    func removeAllAnnotationsInMap() {
        clusterManager.removeAll()
        reloadMap()
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        self.reloadMap()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var reuseId = ""
        if annotation is FBAnnotationCluster {
            reuseId = "o"
            var clusterView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
            
            if clusterView == nil {
                clusterView = FBAnnotationClusterView(annotation: annotation, reuseIdentifier: reuseId, configuration: FBAnnotationClusterViewConfiguration.default())
            } else {
                clusterView?.annotation = annotation
                
            }
            return clusterView
        } else {
            
            var annotationView = MKMarkerAnnotationView()
            let annotation = annotation as? NBSCustomAnnotation
            let identifier = "NBSCustomAnnotation"
            
            if let dequedView = mapView.dequeueReusableAnnotationView(
                withIdentifier: identifier)
                as? MKMarkerAnnotationView {
                annotationView = dequedView
            } else{
                annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            }
            annotationView.glyphImage = UIImage(named: "yellowMarker")
            annotationView.image = UIImage(named: "yellowMarker")
            annotationView.backgroundColor = .clear
            annotationView.markerTintColor = .clear
            annotationView.glyphTintColor = .clear
            annotationView.clusteringIdentifier = identifier
            return annotationView
            
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
     
        guard let annotation = view.annotation else { return }
       
        if let cluster = annotation as? FBAnnotationCluster {
            var zoomRect = MKMapRect.null
            for annotation in cluster.annotations {
                
                let annotationPoint = MKMapPoint.init(annotation.coordinate)
                let pointRect = MKMapRect.init(x: annotationPoint.x, y: annotationPoint.y, width: 0, height: 0)
                if zoomRect.isNull {
                    zoomRect = pointRect
                } else {
                    zoomRect = zoomRect.union(pointRect)
                }
            }
            mapView.setVisibleMapRect(zoomRect, animated: true)
        }else {
            if let eachAnnotation = annotation as? FBAnnotation {
                if let userdata = eachAnnotation.userData as? NearByServicesNewModel {
                    self.showPromotionInfoView(promotion: PromotionsModel.init(model: userdata))
                }
            }
        }
    }
    
    //MARK:-  Popupview Action
    func showPromotionInfoView(promotion: PromotionsModel) {
      
        for i in 0..<merchantModel.count{
            if let obj = merchantModel[i].UserContactData{
                if obj.Id  == promotion.shop_Id{
                    if let model = self.merchantModel[safe: i] {
                        selectedNearByServicesNew = model
                        if let distance =  model.distanceInKm {
                            let roundValue = distance
                            if roundValue > 1000 {
                                let stringDistance = String.init(format: "%.2f Km", roundValue/1000)
                                self.distance = stringDistance
                            } else {
                                let stringDistance = String.init(format: "%.2f m", roundValue)
                                self.distance = stringDistance
                            }
                        }
                    }
                    
                    //This method will give u the detail of particular agent on shopTableView
                    callPromotionDetails()
                    break
                }
                
            }
        }
        
       

        
        
//        self.promotionInfoView.isHidden = false
//        self.promotionInfoView.center.x -= self.view.bounds.width
//        if let timer = infoViewTimer {
//            timer.invalidate()
//        }
//        loadInfoViewData(info: promotion)
//        UIView.animate(withDuration: 1.5, delay: 0.5,
//                       usingSpringWithDamping: 0.3,
//                       initialSpringVelocity: 0.5,
//                       options: [], animations: {
//                        self.promotionInfoView.center.x += self.view.bounds.width
//
//        }, completion:{ _ in
//            self.infoViewTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.update), userInfo: nil, repeats: false)
//        })
    }
    
    @objc func update() {
        infoViewTimer.invalidate()
        removeTheInfoView()
    }
    
    func removeTheInfoView() {
        UIView.animate(withDuration: 0.1,
                       delay: 0.1,
                       options: UIView.AnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.promotionInfoView.center.x -= self.view.bounds.width
        }, completion: { (finished) -> Void in
            self.promotionInfoView.isHidden = true
            self.promotionInfoView.center.x += self.view.bounds.width
        })
    }
    
    func loadInfoViewData(info: PromotionsModel) {
        self.selectedpromotionData = nil
        self.selectedpromotionData = info
        promotionImgView.image = UIImage.init()
        promotionNameLbl.text = info.shop_Name
        var mobileNumber = "NA"
        if info.shop_Phonenumber!.count > 0 {
            mobileNumber = getPhoneNumberByFormat(phoneNumber: info.shop_Phonenumber!)
        }
        contactNoLbl.text = "Contact: \(mobileNumber)"
        
        var distance = "0 Km"
        if let shopDistance = info.shop_InMeter {
            distance = String(format: "%.2f Km", shopDistance)
        }
        distanceLbl.text = "Distance: \(distance)"
        
        if info.shop_Templates.count > 0 {
            let promoOffer = info.shop_Templates[0]
            remarksLbl.text = promoOffer.template_Header
        }else {
            remarksLbl.text = "NA"
        }
        
        promotionImgView.image = UIImage.init(named: "btc")
        promotionInfoView.backgroundColor = info.isAgent ? kYellowColor : UIColor.white
        
    }
    
    
    func reloadMap() {
        DispatchQueue.main.async {
            let mapBoundsWidth = Double(self.map.bounds.size.width)
            let mapRectWidth = self.map.visibleMapRect.size.width
            let scale = mapBoundsWidth / mapRectWidth
            let annotationArray = self.clusterManager.clusteredAnnotations(withinMapRect: self.map.visibleMapRect, zoomScale:scale)
            self.clusterManager.display(annotations: annotationArray, onMapView:self.map)
        }
    }
    
    @IBAction func PopupAction(_ sender: Any) {
        let promotion = selectedpromotionData
        let storyboard: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
        let protionDetailsView = storyboard.instantiateViewController(withIdentifier: "PromotionDetailView_ID") as! PromotionDetailViewController
        protionDetailsView.selectedPromotion = promotion
        protionDetailsView.strNearByPromotion = "Promotion"
        self.navigation?.pushViewController(protionDetailsView, animated: true)
    }
    
}




//MARK: - UIScrollViewDelegate
extension MapPayToVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if shopDetailView.isHidden == false{
            // Exit early if swiping up (scrolling down)
            if scrollView.contentOffset.y > 0 {
                if !isPlayingScroll{
                    height = height - 20
                }
            }else{
                //TusharLama
                    var scale = 1.0 + abs(scrollView.contentOffset.y)  / scrollView.frame.size.height
                    scale = max(0.0, scale)
                    // Set the scale to the imageView on shopTable HEader
                    self.shopImageView.transform = CGAffineTransform(scaleX: scale, y: scale)
            }
        }
    }
}


//this is for promotions
extension MapPayToVC{
    
     func callPromotionDetails(){
            if ((selectedNearByServicesNew?.UserContactData?.PhoneNumber?.count)!) < 20 {
                
                iscategoryAPICall = true
                
                getCategoryList()
               // makePaymentBtn.isHidden = false
            }else {
               iscategoryAPICall = false
               // makePaymentBtn.isHidden = true
            }
            
            if let modelNearBy = selectedNearByServicesNew {
                selectedPromotion = PromotionsModel.init(model: modelNearBy)
                isfavoritePromotion = favoriteExist()
            }
            
            if self.iscategoryAPICall == false {
                loadDistanceInMinute()
              isfavoritePromotion = favoriteExist()
            }
        
    }
    
    func favoriteExist() -> Bool {
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FavoritePromotion")
        do {
            let shopId = self.selectedPromotion?.shop_Id ?? ""
            fetchRequest.predicate = NSPredicate(format: "promotionId == %@", shopId)
            let fetchedResults = try managedContext.fetch(fetchRequest) as! [FavoritePromotion]
            if let _ = fetchedResults.first {
                return true
            }else {
                return false
            }
        }
        catch {
            print ("fetch task failed", error)
        }
        return false
        
    }
    
    func getTime(timeStr: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "HH:mm:ss"
        //        if(strNearByPromotion == "NearBy")
        //        {
        //            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        //        }
        //dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let time = dateFormatter.date(from: timeStr)
        dateFormatter.dateFormat = "hh:mm a"
        if let t = time  {
            let hour = dateFormatter.string(from: t)
            return hour
        } else {
            return ""
        }
    }
    
    func getCategoryList() {
        
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self as WebServiceResponseDelegate
            showProgressView()
            
            let buldApi = Url.billSplitterMobileValidation + "MobileNumber=\(selectedNearByServicesNew?.UserContactData?.PhoneNumber ?? "")"
            
            
            let url = getUrl(urlStr: buldApi, serverType: .serverApp)
            println_debug(url)
            
            let params = Dictionary<String,String>()
            
            web.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", mScreen: "PromotionDetail")
        }
        
    }
    
   
    
    @objc func favoriteBtnAction() {
        //        if(strNearByPromotion == "Promotion" || strNearByPromotion == "NearBy")
        //        {
        addDeletePromotionInFavorite()
        //        }
    }
    
    func addDeletePromotionInFavorite() {
        guard (self.selectedPromotion != nil) else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FavoritePromotion")
        do {
            let shopId = self.selectedPromotion?.shop_Id ?? ""
            
            fetchRequest.predicate = NSPredicate(format: "promotionId == %@", shopId)
            let fetchedResults = try managedContext.fetch(fetchRequest) as! [FavoritePromotion]
            if let aFavorite = fetchedResults.first {
                alertViewObj.wrapAlert(title: "", body: "Do you want to remove from favorite ?".localized, img: #imageLiteral(resourceName: "promotion_star"))
                alertViewObj.addAction(title: "Yes,Remove".localized, style: .target , action: {
                    // here need to delete the favorite from db
                    managedContext.delete(aFavorite)
                    do {
                        try managedContext.save()
                        self.isfavoritePromotion = false
                        if self.checkIfComingFromOption == "info"{
                            let indexPath = IndexPath(item: 0, section: 0)
                            self.shopTableView.reloadRows(at: [indexPath], with: .fade)
                        }else if self.checkIfComingFromOption == "photo"{
                            let indexPath = IndexPath(item: 2, section: 0)
                            self.shopTableView.reloadRows(at: [indexPath], with: .fade)
                        }else{
                            let indexPath = IndexPath(item: 1, section: 0)
                            self.shopTableView.reloadRows(at: [indexPath], with: .fade)
                        }
                        
                    } catch let error as NSError {
                        println_debug("Could not save. \(error), \(error.userInfo)")
                    }
                    
                })
                
                alertViewObj.showAlert(controller: self)
            }else {
                // here need to add the favorite
                alertViewObj.wrapAlert(title: "", body: "Do you want to add as favorite ?".localized, img: #imageLiteral(resourceName: "promotion_star"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    // here need to add the favorite from db
                    guard let promotion = self.selectedPromotion else {
                        return
                    }
                    let saveData = NSKeyedArchiver.archivedData(withRootObject: promotion)
                    let managedContext = appDelegate.persistentContainer.viewContext
                    let entity = NSEntityDescription.entity(forEntityName: "FavoritePromotion",
                                                            in: managedContext)!
                    let favPromotion = NSManagedObject(entity: entity,
                                                       insertInto: managedContext)
                    favPromotion.setValue(saveData, forKeyPath: "promotionData")
                    favPromotion.setValue(Date(), forKeyPath: "addedDateTime")
                    favPromotion.setValue(self.selectedPromotion?.shop_Id, forKeyPath: "promotionId")
                    do {
                        try managedContext.save()
                        self.isfavoritePromotion = true
                        if self.checkIfComingFromOption == "info"{
                            let indexPath = IndexPath(item: 0, section: 0)
                            self.shopTableView.reloadRows(at: [indexPath], with: .fade)
                        }else if self.checkIfComingFromOption == "photo"{
                            let indexPath = IndexPath(item: 2, section: 0)
                            self.shopTableView.reloadRows(at: [indexPath], with: .fade)
                        }else{
                            let indexPath = IndexPath(item: 1, section: 0)
                            self.shopTableView.reloadRows(at: [indexPath], with: .fade)
                        }
                        
                        
                    } catch let error as NSError {
                        println_debug("Could not save. \(error), \(error.userInfo)")
                    }
                })
                
                alertViewObj.showAlert(controller: self)
            }
            
        }
        catch {
            print ("fetch task failed", error)
        }
        
    }
    ///
    @objc func directionBtnAction() {
        guard let promotion = self.selectedPromotion else {
            return
        }
        if let curLat = geoLocManager.currentLatitude, let curLong = geoLocManager.currentLongitude {
            
            var urlStr = ""
        
        //    if strNearByPromotion == "NearBy" {
             //   urlStr = "http://maps.google.com/maps?saddr=\(curLat),\(curLong)&daddr=\(promotion.shop_GeoLocation?.lattitude ?? 0.0),\(promotion.shop_GeoLocation?.longitude ?? 0.0)"
         //   }
            
//            else {
               urlStr = "http://maps.google.com/maps?saddr=\(curLat),\(curLong)&daddr=\(promotion.shop_Latitude ?? 0.0),\(promotion.shop_Longtitude ?? 0.0)"
//            }
            
            if let url = URL.init(string: urlStr) {
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        }
        
    }
    
    
    
    
    
    /////
//    @objc func directionBtnAction() {
//        guard let promotion = self.selectedPromotion else {
//            return
//        }
//        if let curLat = geoLocManager.currentLatitude, let curLong = geoLocManager.currentLongitude {
//
//           let mapViewObj = self.storyboard?.instantiateViewController(withIdentifier: "DirectionMapVC") as? DirectionMapVC
//
//           if let obj = mapViewObj{
//               obj.source = CLLocationCoordinate2D(latitude: Double(curLat) ?? 0.0  , longitude: Double(curLong) ?? 0.0)
//               obj.url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(curLat),\(curLong)&destination=\(promotion.shop_Latitude ?? 0.0),\(promotion.shop_Longtitude ?? 0.0)&sensor=false&mode=driving&key=AIzaSyB8SzL-QxjqrTHt7eqNA6D4zU4ayHTJO8k"
//                   obj.destination = CLLocationCoordinate2D(latitude: promotion.shop_Latitude ?? 0.0, longitude: promotion.shop_Longtitude ?? 0.0)
//
//               self.presentDetail(obj)
//
//           }
//
//        }
//
//    }
    
    func shareAction() {
        guard let promotion = self.selectedPromotion else {
            return
        }
        
        var mobileNumber = ""
        if let phonenumber = promotion.shop_Phonenumber {
            if phonenumber.count > 0 {
                mobileNumber = getPlusWithDestinationNum (phonenumber, withCountryCode: "+95")
            }
        }
        
        let textToShare: String = "OK$\n\(promotion.shop_Name!)\n\(promotion.shop_AgentName!)\n\(mobileNumber)\n"
        var link: String = "https://www.google.com/maps/search/?api=1&query=\(promotion.shop_GeoLocation?.lattitude ?? 0.0),\(promotion.shop_GeoLocation?.longitude ?? 0.0)"
        
            link = "https://www.google.com/maps/search/?api=1&query=\(promotion.shop_Latitude ?? 0.0),\(promotion.shop_Longtitude ?? 0.0)"
        
        
        let url:NSURL = NSURL.init(string: link)!
        var activityItems = [Any]()
        activityItems.append(textToShare)
        activityItems.append(url)
        
        let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activityViewController.setValue("Information".localized, forKey: "subject")
        activityViewController.excludedActivityTypes = [.assignToContact, .print]
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
        
    }
    
    @objc func navigateToMapDetailViewAction() {
        
        changeButtonColor(backGroundColor: blueColor, textColor: .black, button: infoButton, baseView: infoBase)
        changeButtonColor(backGroundColor: .white, textColor: .lightGray, button: photoButton, baseView: photoBase)
        changeButtonColor(backGroundColor: .white, textColor: .lightGray, button: descriptionButton, baseView: descriptionBase)
        checkIfComingFromOption = "info"
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
        let mapDetailsView = storyboard.instantiateViewController(withIdentifier: "MapDetailView_ID") as! MapDetailViewController
            mapDetailsView.selectedPromotion = self.selectedPromotion
            mapDetailsView.isFavorite = self.isfavoritePromotion
            mapDetailsView.screencheck = "Promotion"
            mapDetailsView.nearbyLatitude = selectedNearByServicesNew?.UserContactData?.LocationNewData?.Latitude
            mapDetailsView.nearbyLongtitude = selectedNearByServicesNew?.UserContactData?.LocationNewData?.Longitude
        self.navigationController?.pushViewController(mapDetailsView, animated: true)
        
    }
    
    func loadDistanceInMinute()
    {
        //http://maps.googleapis.com/maps/api/directions/json?origin=16.8166773,96.131959&destination=16.8092851,96.1350552&mode=driving&sensor=false
        
        if let curLat = geoLocManager.currentLatitude, let curLong = geoLocManager.currentLongitude {
            geoLocManager.getDurationData(sourceLatitude: curLat, sourceLongitude: curLong, destinationLatitude: curLat, destinationLongitude: curLong) { (_, address) in
                DispatchQueue.main.async {
                    if let addressUnwrapped = address as? String {
                        println_debug(addressUnwrapped)
                        self.strTimeDuration = addressUnwrapped
                        
                        self.shopTableView.reloadData()
                    }
                }
            }
        }
    }
}



extension MapPayToVC{
    
    func showMapView(){
        self.setViewWithAnimation(view: self.initialView, hidden: true)
        self.setViewWithAnimation(view: self.shopDetailView, hidden: true)
        self.setViewWithAnimation(view: self.topBackGroundView, hidden: true)
        self.setViewWithAnimation(view: self.listViewContainer, hidden: false)
        self.setViewWithAnimation(view: self.mapViewContainer, hidden: false)
        self.animationViewHeight(top: CGFloat((self.view.frame.size.height)/2 + 100))
    }
    
    func setViewWithAnimation(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.2, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
    
    //This will be called when clicked on nearby service and when we select town and all
    func updateOtherLocationToUINew(location: String, township: String) {
        selectedLocationDetailStr = location
        selectedTownshipDetailStr = township
        
    }
    
}


//This is called when user click on nearby and call the current location tap

extension MapPayToVC: MainLocationViewDelegateRegistration{
    
    func didSelectLocationByTownshipRegistration(location: LocationDetail, township: TownShipDetailForAddress) {
        
        self.checkurl = "Township"
//
       if appDel.getSelectedLanguage() == "my" {
           updateOtherLocationToUINew(location: location.stateOrDivitionNameMy, township: township.townShipNameMY)
        } else {
           updateOtherLocationToUINew(location: location.stateOrDivitionNameEn, township: township.townShipNameEN)
       }
        let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
        let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
        self.callNearbyMerchantWithCustomLocation(lat: lat, long: long)
        
    }
    
    
    func didSelectLocationByCurrentLocation() {
        self.currentLocationUpdate()
    }
    
}




