////
////  MapViewController.swift
////  OK
////
////  Created by iMac on 8/22/19.
////  Copyright © 2019 Vinod Kumar. All rights reserved.
////
//
//import UIKit
//import Rabbit_Swift
//import MapKit
//import CoreLocation
//
//
//
//class MapViewController: OKBaseController , CategoriesSelectionDelegate, SelectLocationDelegate, PTMapListTableViewCellDelegate, TownshipSelectionDelegate, NRCDivisionStateDeletage, MainNearByLocationViewDelegate , CurrentLocationViewControllerDelegate {
//    
//    @IBOutlet weak var mapView: MKMapView!
//    @IBOutlet weak var listTableView: UITableView!
//    @IBOutlet weak var topBackGroundView: UIView!
//    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
//    @IBOutlet weak var animationView: UIView!
//    @IBOutlet weak var heightTopView: NSLayoutConstraint!
//    @IBOutlet weak var dropDownView: UIView!
//    @IBOutlet weak var agentListDesign: UIView!
//    var click  = 0
//    let defaults = UserDefaults.standard
//    var merchantModel = [NearByServicesNewModel]()
//    var pulsingEffect : PulseManager?
//    var checkurl:String = ""
//    
//    var selectedpromotionData : PromotionsModel?
//    
//    // cluster manager
//    let clusterManager = FBClusteringManager()
//    
//    var merchantModelBackup = [NearByServicesNewModel]()
//    var selectedFilterData      : NearByServicesNewModel?
//    var selectedMerchant : NearByServicesNewModel?
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.navigationController?.isNavigationBarHidden = true
//        topViewHeight.constant = 0
//        
//        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
//        swipeUp.direction = .up
//        self.animationView.addGestureRecognizer(swipeUp)
//        
//        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
//        swipeDown.direction = .down
//        self.animationView.addGestureRecognizer(swipeDown)
//        
//        let tap = UITapGestureRecognizer(target: self, action: #selector(playWithView))
//        self.dropDownView.addGestureRecognizer(tap)
//        
//        let tapOne = UITapGestureRecognizer(target: self, action: #selector(playWithTopBackGround))
//        self.topBackGroundView.addGestureRecognizer(tapOne)
//        
//        
//        mapView.delegate = self
//        
//        self.animationView.layer.cornerRadius = CGFloat(20)
//        self.animationView.layer.borderWidth = 2.0
//        self.animationView.layer.borderColor = UIColor.lightGray.cgColor
//        self.animationView.clipsToBounds = true
//        animationViewHeight(top: 400)
//        listTableView.delegate = self
//        listTableView.dataSource = self
//        listTableView.reloadData()
//        
//        
//    }
//    
//    func initialSetupForMap() {
//        let locationLat  = CLLocationDegrees.init(GeoLocationManager.shared.currentLatitude)
//        let locationLong = CLLocationDegrees.init(GeoLocationManager.shared.currentLongitude)
//        
//        let location = CLLocationCoordinate2D.init(latitude: locationLat ?? 0.0, longitude: locationLong ?? 0.0)
//        let distance = 1.0//self.slider.value / 3.2808
//        let coordinateRegion = MKCoordinateRegion.init(center: location, latitudinalMeters: CLLocationDistance(distance), longitudinalMeters: CLLocationDistance(distance))
//        
//        mapView.setRegion(coordinateRegion, animated: true)
//        
//        let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
//        let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
//        self.callNearbyMerchantWithCustomLocation(lat: lat, long: long)
//        
//    }
//    
//    func didSelectLocationByCategory(category: SubCategoryDetail?) {
//        if let cat = category {
//            
//            var subCategoryCode = cat.subCategoryCode
//            if subCategoryCode.length == 1 {
//                subCategoryCode = "0\(cat.subCategoryCode)"
//            }
//            
//            let array = self.merchantModelBackup.filter({$0.UserContactData?.BusinessName == subCategoryCode})
//            
//            defaults.set(cat.mainCategoryName + "," + cat.subCategoryName, forKey: "PTMcategoryBy")
//            
//            self.addClustersToMap(list: array)
//        } else {
//            
//            let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
//            let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
//            self.callNearbyMerchantWithCustomLocation(lat: lat, long: long)
//            
//        }
//    }
//    
//    // MARK: Township
//    func didSelectTownship(location: LocationDetail, township: TownShipDetail) {
//        //self.locationOutlet.text = township.cityNameEN
//        
//        let array = self.merchantModel.filter({$0.UserContactData?.Township == township.cityNameEN})
//        print("Township----+++++\(township.cityNameEN)******\(array.count)")
//        self.addClustersToMap(list: array)
//    }
//    
//    
//    private func showCategoriesView() {
////        let storyBoardName = UIStoryboard(name: "Promotion", bundle: nil)
////        guard let categoriesView = storyBoardName.instantiateViewController(withIdentifier: "CategoriesListView_ID") as? CategoriesListViewController else { return }
////        categoriesView.delegate = self
////        categoriesView.modalPresentationStyle = .overCurrentContext
////        self.navigation?.present(categoriesView, animated: true, completion: nil)
//    }
//    
//    
//    override func viewWillAppear(_ animated: Bool) {
//        self.locationAuthorisation()
//        self.currentLocationUpdate()
//    }
//    
//    //MARK: - Location Manager
//    
//    func didSelectLocationByCurrentLocation() {
//        self.currentLocationUpdate()
//    }
//    
//    // MARK: Filterview CurrentLocation
//    func didSelectNearByLocation() {
//        
//        self.currentLocationUpdate()
//    }
//    
//    
//    
//   
//    
//    private func merchantWithDivisions() {
////        let viewController = UIStoryboard(name: "TaxPayment", bundle: Bundle.main).instantiateViewController(withIdentifier: "NRCDivisionRoot")
////        if let nav = viewController as? UINavigationController {
////            for views in nav.viewControllers {
////                if let vc = views as? TaxNRCDivisionState {
////                    vc.townShipDelegate = self
////                    vc.fromPaytoMap = ""
////                    break
////                }
////            }
////        }
////        self.navigation?.present(viewController, animated: true, completion: nil)
//    }
//    
//    //MARK:- Select Location Delegate
//    func didSelectLocation(withOptions option: SelectLocationEnum) {
//        switch option {
//        case .currentLocation:
//            self.currentLocationUpdate()
//            break
//        case .division:
//            self.merchantWithDivisions()
//            break
//        case .nearbyMerchants:
//            //print("Nearbymerchant called")
//            notfDef.post(name:Notification.Name(rawValue:"PayToStateDivision"),
//                         object: nil,
//                         userInfo: ["Name":  Rabbit.uni2zg(String.init(format: "%@","Near By".localized))])
//            
//            let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
//            let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
//            self.callNearbyMerchantWithCustomLocation(lat: lat, long: long)
//            
//            break
//        case .allMerchants:
//            let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
//            let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
//            self.callNearbyMerchantWithCustomLocation(lat: lat, long: long)
//            
//            break
//        case .merchantWithPromotions:
//            let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
//            let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
//            self.callNearbyMerchantWithCustomLocation(lat: lat, long: long)
//            break
//        }
//    }
//    
//    func currentLocationUpdate() {
//        geoLocManager.getAddressFrom(lattitude: GeoLocationManager.shared.currentLatitude ?? "0.0" , longitude: GeoLocationManager.shared.currentLongitude ?? "0.0", language: appDel.currentLanguage, completionHandler: { [weak self](success, address) in
//            DispatchQueue.main.async {
//                if success {
//                    if let adrs = address {
//                        if let dictionary = adrs as? Dictionary<String,Any> {
//                            let street = dictionary.safeValueForKey("street") as? String ?? ""
//                            let township = dictionary.safeValueForKey("township") as? String ?? ""
//                            let city = dictionary.safeValueForKey("region") as? String ?? ""
//                            //self?.locationOutlet.text = Rabbit.uni2zg(String.init(format: "%@ ,%@ ,%@", street , township + " " + "Township".localized, city))
//                           // self?.locationOutlet.text = Rabbit.uni2zg(String.init(format: "%@ ,%@ ,%@", street , township + " ", city))
//                            //print("Current location called")
//                            
//                            notfDef.post(name:Notification.Name(rawValue:"PayToStateDivision"),
//                                         object: nil,
//                                         userInfo: ["Name":  Rabbit.uni2zg(String.init(format: "%@ ,%@ ,%@", street , township + " ", city))])
//                            
//                        }
//                    }
//                }
//            }
//        })
//        
//        let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
//        let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
//        self.callNearbyMerchantWithCustomLocation(lat: lat, long: long)
//        
//    }
//    
//    
//    @IBAction func onClickCrossButton(_ sender: Any) {
//        
//        self.dismiss(animated: true, completion: nil)
////        if let vc = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PayToViewController") as? PayToViewController {
////
////            let navigation = UINavigationController(rootViewController: vc)
////            self.navigationController?.popToViewController(navigation, animated: true)
////
////        }
//        
//    }
//    
//    
//    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
//        
//        if gesture.direction == .right {
//            print("Swipe Right")
//        }
//        else if gesture.direction == .left {
//            print("Swipe Left")
//        }
//        else if gesture.direction == .up {
//            playWithUpGesture()
//        }
//        else if gesture.direction == .down {
//            playWithDownGesture()
//        }
//    }
//    
//    func playWithUpGesture(){
//        if heightTopView.constant ==  400{
//            animationViewHeight(top: 0)
//            click = 1
//        }else if heightTopView.constant ==  550{
//            animationViewHeight(top: 400)
//            click = 2
//        }
//        
//    }
//    
//    func playWithDownGesture(){
//        if heightTopView.constant == 0{
//            animationViewHeight(top: 400)
//            click = 2
//        }else if heightTopView.constant == 400{
//            animationViewHeight(top: 550)
//            click = 0
//        }
//    }
//    
//    
//    @objc func playWithView(gesture: UITapGestureRecognizer) -> Void {
//        if click == 0 {
//            animationViewHeight(top: 0)
//            click = 1
//        }else if click == 1{
//            animationViewHeight(top: 400)
//            click = 2
//        }else if click == 2{
//            animationViewHeight(top: 550)
//            click = 0
//        }
//    }
//    
//    
//    @objc func playWithTopBackGround(gesture: UITapGestureRecognizer) -> Void {
//        animationViewHeight(top: 400)
//        click = 2
//    }
//    
//    
//    
//    func animationViewHeight(top: Int){
//        
//        if top == 0{
//            topViewHeight.constant = 57
//        }else{
//            topViewHeight.constant = 0
//        }
//        
//        UIView.animate(withDuration: 1.0, animations: { () -> Void in
//            self.heightTopView.constant  = CGFloat(top)
//            self.view.layoutIfNeeded()
//        })
//    }
//    
//    
//}
//
//
////MARK:- List TableView Datasource & Delegate
//extension MapViewController: UITableViewDelegate, UITableViewDataSource {
//    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return merchantModel.count
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        // let listCell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: MapListCell.self), for: indexPath) as! MapListCell
//        
//        //let listCell = tableView.dequeueReusableCell(withIdentifier: "maplistcellidentifier", for: indexPath) as! MapListCell
//        
//        let identifier = "maplistcellidentifier"
//        var listCell: MapListCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? MapListCell
//        if listCell == nil {
//            tableView.register (UINib(nibName: "MapListCell", bundle: nil), forCellReuseIdentifier: identifier)
//            listCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? MapListCell
//        }
//        
//        let promotion = self.merchantModel[indexPath.row]
//        listCell.callBtn.addTarget(self, action: #selector(callBtnAction(_:)), for: UIControl.Event.touchUpInside)
//        listCell.photoImgView.image = nil
//        if let imageStr = promotion.UserContactData?.ImageUrl, let imageUrl = URL(string: imageStr) {
//            listCell.photoImgView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "cicoCamera"))
//        }
//        //listCell.photoImgView.image = UIImage.init(named: "cicoCamera")
//        listCell.photoImgView.layer.borderWidth = 1.0
//        listCell.photoImgView.layer.masksToBounds = false
//        listCell.photoImgView.layer.borderColor = UIColor(red: 246/255, green: 198/255, blue: 0/225, alpha: 1).cgColor
//        listCell.photoImgView.layer.cornerRadius = listCell.photoImgView.frame.size.width / 2
//        listCell.photoImgView.clipsToBounds = true
//        
//        listCell.nameLbl.text = promotion.UserContactData?.BusinessName
//        
//        //        let str = promotion.phoneNumber!
//        //        let agentCode = str.replacingOccurrences(of: "0095", with: "+95")
//        //        listCell.phonenumberLbl.text = agentCode
//        
//        
//        /* let agentCodenew  = getPlusWithDestinationNum(promotion.phoneNumber!, withCountryCode: "+95")
//         listCell.phonenumberLbl.text = agentCodenew */
//        
//        let agentCodenew  = getPlusWithDestinationNum((promotion.UserContactData!.PhoneNumber)!, withCountryCode: "+95")
//        listCell.phonenumberLbl.text = agentCodenew
//        
//        //listCell.phonenumberLbl.text = promotion.UserContactData?.PhoneNumber
//        
//        
//        if(promotion.UserContactData?.BusinessName!.count == 0)
//        {
//            listCell.nameLbl.text = promotion.UserContactData?.FirstName
//        }
//        
//        if let openTime = promotion.UserContactData!.OfficeHourFrom , let closeTime = promotion.UserContactData!.OfficeHourTo {
//            
//            //            if(promotion.isOpenAlways == true)
//            //            {
//            //                listCell.timeImgView.isHidden = false
//            //                listCell.timeLbl.isHidden = false
//            //                listCell.timeLblHeightConstraint.constant = 20
//            //                listCell.timeImgView.image = UIImage.init(named: "time")
//            //                listCell.timeLbl.text = "Open 24/7"
//            //            }
//            //            else
//            //            {
//            if openTime.count > 0 && closeTime.count > 0 {
//                listCell.timeImgView.isHidden = false
//                listCell.timeLbl.isHidden = false
//                listCell.timeLblHeightConstraint.constant = 20
//                //listCell.timeImgView.image = UIImage.init(named: "time")
//                listCell.timeLbl.text = "\(listCell.getTime(timeStr: openTime)) - \(listCell.getTime(timeStr: closeTime))"
//            }else {
//                listCell.timeImgView.isHidden = true
//                listCell.timeLbl.isHidden = true
//                listCell.timeLblHeightConstraint.constant = 0
//            }
//            // }
//        }
//        listCell.locationImgView.image = UIImage.init(named: "location.png")
//        if let addressnew = promotion.UserContactData?.Address {
//            if addressnew.count > 0 {
//                
//                listCell.locationLbl.text = "\(addressnew)"
//                
//                
//                // listCell.locationLbl.text = "\(addressnew),\( promotion.UserContactData!.Township!),\( promotion.UserContactData!.Division!)"
//            }else {
//                listCell.locationLbl.text = "\(promotion.UserContactData!.Township!),\(promotion.UserContactData!.Division!)"
//            }
//        }
//        
//        //CashIn And CashOut
//        
//        if let cashinamount = promotion.UserContactData?.CashInLimit , let cashoutamount = promotion.UserContactData?.CashOutLimit {
//            
//            if cashinamount > 0 {
//                let phone =  self.getNumberFormat("\(promotion.UserContactData!.CashInLimit!)").replacingOccurrences(of: ".00", with: "")
//                listCell.cashInLbl.text = "Maximum Cash In Amount".localized + " : \(phone)" + " MMK"
//                listCell.cashInLblHeightConstraint.constant = 20
//            } else {
//                listCell.cashInLblHeightConstraint.constant = 0
//            }
//            
//            if cashoutamount > 0 {
//                let phonenew =  self.getNumberFormat("\(promotion.UserContactData!.CashOutLimit!)").replacingOccurrences(of: ".00", with: "")
//                listCell.cashOutLbl.text = "Maximum Cash Out Amount".localized + " : \(phonenew)" + " MMK"
//                listCell.cashOutLblHeightConstraint.constant = 20
//            } else{
//                listCell.cashOutLblHeightConstraint.constant = 0
//            }
//        }
//        
//        // listCell.remarksLbl.text = ""
//        listCell.callBtn.setImage(UIImage.init(named: "call.png"), for: UIControl.State.normal)
//        listCell.distanceLblKm.text = "(0 Km)"
//        if let shopDistance = promotion.distanceInKm {
//            listCell.distanceLblKm.text = String(format: "(%.2f Km)", shopDistance).replacingOccurrences(of: ".00", with: "")
//        }
//        
//        listCell.durationLbl.text = listCell.durationFromDistance(distance: promotion.distanceInKm) as String
//        
//        return listCell
//    }
//    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        tableView.estimatedRowHeight = 250
//        tableView.rowHeight = UITableView.automaticDimension
//        return tableView.rowHeight
//        
//    }
//    
////    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
////        if let model = merchantModel[safe: indexPath.row] {
////            let storyboard: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
////            let protionDetailsView = storyboard.instantiateViewController(withIdentifier: "PromotionDetailView_ID") as! PromotionDetailViewController
////
////            //prabu
////            if let model = self.merchantModel[safe: indexPath.row] {
////                if let distance =  model.distanceInKm {
////                    // let roundValue = (distance as NSString).floatValue
////                    let roundValue = distance
////                    if roundValue > 1000 {
////                        let stringDistance = String.init(format: "%.2f Km", roundValue/1000)
////                        protionDetailsView.distance = stringDistance
////                    } else {
////                        let stringDistance = String.init(format: "%.2f m", roundValue)
////                        protionDetailsView.distance = stringDistance
////                    }
////                }
////            }
////
////            protionDetailsView.selectedNearByServicesNew = model
////            protionDetailsView.strNearByPromotion = "NearByService"
////
////            //            self.navigationController?.pushViewController(protionDetailsView, animated: true)
////            self.navigation?.pushViewController(protionDetailsView, animated: true)
////            //            PaytoConstants.global.navigation?.pushViewController(protionDetailsView, animated: true)
////        }
////    }
//    
//    // call to number
//    func callToMerchant(index: IndexPath) {
//        if let model = self.merchantModel[safe:index.row] {
//            var number = model.UserContactData?.PhoneNumber.safelyWrappingString()
//            if number!.hasPrefix("00") {
//                number = "+" + number!
//            }
//            let phone = "tel://\(number)"
//            if let url = URL.init(string: phone) {
//                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
//            }
//            
//        }
//    }
//    
//    
//    @objc func callBtnAction(_ sender: UIButton) {
//        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to: self.listTableView)
//        if let indexPath = self.listTableView.indexPathForRow(at: buttonPosition), self.merchantModel.count > 0 {
//            let selectedpromotion = self.merchantModel[indexPath.row]
//            let promotion = selectedpromotion
//            if let phonenumber = promotion.UserContactData?.PhoneNumber {
//                if phonenumber.count > 0 && phonenumber.count < 20 {
//                    var mobNumber = getPhoneNumberByFormat(phoneNumber: phonenumber)
//                    mobNumber = mobNumber.removingWhitespaces()
//                    let phone = "tel://\(mobNumber)"
//                    if let url = URL.init(string: phone) {
//                        UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
//                    }
//                }
//            }
//        }else {
//            alertViewObj.wrapAlert(title: nil, body: "No Phonenumber Exists".localized, img: nil)
//            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
//                
//            })
//            alertViewObj.showAlert(controller: self)
//        }
//    }
//    
//    
//    
//}
//
//
//// Helper function inserted by Swift 4.2 migrator.
//fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
//    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
//}
//
//
//extension MapViewController : MKMapViewDelegate {
//    
//    func locationAuthorisation() {
//        switch CLLocationManager.authorizationStatus() {
//        case .notDetermined:
//           // self.alertViewPermission.isHidden = false
//            break
//        case .restricted:
//          //  self.alertViewPermission.isHidden = false
//            break
//        case .denied:
//          //  self.alertViewPermission.isHidden = false
//            break
//        case .authorizedAlways:
//            GeoLocationManager.shared.startUpdateLocation()
//          //  self.alertViewPermission.isHidden = true
//            break
//        case .authorizedWhenInUse:
//            GeoLocationManager.shared.startUpdateLocation()
//          //  self.alertViewPermission.isHidden = true
//            break
//        }
//    }
//    
//    func addClustersToMap(list: [NearByServicesNewModel]) {
//        
//        if list.count <= 0 {
//            PaytoConstants.alert.showErrorAlert(title: nil, body: "No Records Found!".localized)
//            self.listViewContainer.isHidden = true
//            self.listView.isHidden = false
//            self.mapView.isHidden = true
//            return
//        } else {
//            self.listViewContainer.isHidden = true
//            self.listView.isHidden = false
//            self.mapView.isHidden = true
//            
//            merchantModel.removeAll()
//            merchantModel = list
//            listTableView.reloadData()
//        }
//        
//        removeAllAnnotationsInMap()
//        listData = list
//        var clusterArray: [FBAnnotation] = []
//        var value = 0
//        while value < list.count {
//            if let model = list[value] as? NearByServicesNewModel {
//                
//                print("AnotationLatitudeandlogtitude-----\(model.UserContactData?.LocationNewData?.Latitude)----\(model.UserContactData?.LocationNewData?.Longitude)")
//                
//                
//                if let lat = model.UserContactData?.LocationNewData?.Latitude , let long = model.UserContactData?.LocationNewData?.Longitude {
//                    print("AnotationLatitudeandlogtitude-----")
//                    
//                    let annotation = FBAnnotation()
//                    annotation.coordinate = CLLocationCoordinate2D(latitude:  lat, longitude: long)
//                    
//                    if model.UserContactData?.BusinessName == "" {
//                        
//                        if model.UserContactData?.FirstName != "" {
//                            print("AnotationFirstname-----\(model.UserContactData?.FirstName)")
//                            
//                            annotation.title = model.UserContactData?.FirstName
//                        }
//                        
//                    } else {
//                        print("Anotationbusiness-----\(model.UserContactData?.BusinessName)")
//                        annotation.title = model.UserContactData?.BusinessName
//                        
//                    }
//                    let pinImage = UIImage(named: "ok_logo.png")
//                    annotation.image = pinImage
//                    annotation.userData = model
//                    clusterArray.append(annotation)
//                    
//                    if value == list.count-2 {
//                        let initialLocation = CLLocation(latitude: Double(lat), longitude: Double(long))
//                        centerMapOnLocation(location: initialLocation)
//                    }
//                }
//            }
//            value += 1
//        }
//        clusterManager.add(annotations: clusterArray)
//        reloadMap()
//    }
//    
//    func centerMapOnLocation(location: CLLocation) {
//        
//        let coordinateRegion = MKCoordinateRegion.init(center: location.coordinate,latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
//        
//        self.map.setRegion(coordinateRegion, animated: true)
//    }
//    
//    func removeAllAnnotationsInMap() {
//        clusterManager.removeAll()
//        reloadMap()
//    }
//    
//    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
//        self.reloadMap()
//    }
//    
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        var reuseId = ""
//        if annotation is FBAnnotationCluster {
//            reuseId = "Cluster"
//            var clusterView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
//            if clusterView == nil {
//                clusterView = FBAnnotationClusterView(annotation: annotation, reuseIdentifier: reuseId, configuration: FBAnnotationClusterViewConfiguration.default())
//            } else {
//                clusterView?.annotation = annotation
//            }
//            return clusterView
//        } else {
//            
//            print("NBSCustomAnnotation called----\(annotation)")
//            
//            var annotationView = MKMarkerAnnotationView()
//            let annotation = annotation as? NBSCustomAnnotation
//            let identifier = "NBSCustomAnnotation"
//            
//            if let dequedView = mapView.dequeueReusableAnnotationView(
//                withIdentifier: identifier)
//                as? MKMarkerAnnotationView {
//                annotationView = dequedView
//            } else{
//                annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
//            }
//            annotationView.glyphImage = UIImage(named: "ok_logo.png")
//            annotationView.image = UIImage(named: "ok_logo.png")
//            annotationView.backgroundColor = .clear
//            annotationView.markerTintColor = .clear
//            annotationView.glyphTintColor = .clear
//            annotationView.clusteringIdentifier = identifier
//            return annotationView
//            
//            /*  let annotationIdentifier = "SomeCustomIdentifier"
//             var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
//             
//             if annotationView == nil {
//             annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
//             //annotationView?.canShowCallout = false
//             annotationView!.clusteringIdentifier = annotationIdentifier
//             
//             annotationView?.image = UIImage(named: "ok_logo.png") */
//            
//            /*
//             for promotion in merchantModel {
//             if let promo = promotion as? PTMapNearbyMerchant {
//             if promo.shopID == annotation.title {
//             
//             let emoji1 = (promo.businessCategory?.logo)!
//             let emoji2 = (promo.businessCategory?.businessType?.logo)!
//             let topImage = emoji1.emojiToImage()
//             let bottomImage = emoji2.emojiToImage()
//             annotationView?.image = topImage?.combineWith(image: bottomImage!)
//             
//             break
//             }
//             }
//             }
//             */
//            /* } else {
//             annotationView?.annotation = annotation
//             }
//             return annotationView */
//        }
//    }
//    
//    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
//        guard let annotation = view.annotation else { return }
//        
//        if let cluster = annotation as? FBAnnotationCluster {
//            var zoomRect = MKMapRect.null
//            for annotation in cluster.annotations {
//                
//                let annotationPoint = MKMapPoint.init(annotation.coordinate)
//                let pointRect = MKMapRect.init(x: annotationPoint.x, y: annotationPoint.y, width: 0, height: 0)
//                if zoomRect.isNull {
//                    zoomRect = pointRect
//                } else {
//                    zoomRect = zoomRect.union(pointRect)
//                }
//            }
//            mapView.setVisibleMapRect(zoomRect, animated: true)
//        }else {
//            
//            print("Else payto map icon  called")
//            
//            if let eachAnnotation = annotation as? FBAnnotation {
//                if let userdata = eachAnnotation.userData as? NearByServicesNewModel {
//                    
//                    print("inside payto map icon  called")
//                    
//                    //println_debug("user each data details :::: \(userdata.shop_Name)  ,, \(userdata.shop_Address?.addressLine1) ,, \(userdata.shop_InMeter)")
//                    
//                    let promotion = PromotionsModel.init(model: userdata)
//                    self.showPromotionInfoView(promotion: promotion)
//                    
//                    //self.showPromotionInfoView(promotion: userdata)
//                }
//                
//            }
//            
//            /* if let eachAnnotation = annotation as? FBAnnotation {
//             if let userdata = eachAnnotation.userData as? PTMapNearbyMerchant {
//             
//             print("payto map icon  called")
//             
//             let nearby = NearByServicesModel.init(model: userdata)
//             let promotion = PromotionsModel.init(model: nearby)
//             self.showPromotionInfoView(promotion: promotion , merchantnew:userdata)
//             }
//             } */
//        }
//    }
//    
//    
//    //MARK:-  Popupview Action
//    func showPromotionInfoView(promotion: PromotionsModel) {
//        self.promotionInfoView.isHidden = false
//        self.promotionInfoView.center.x -= self.view.bounds.width
//        if let timer = infoViewTimer {
//            timer.invalidate()
//        }
//        loadInfoViewData(info: promotion)
//        UIView.animate(withDuration: 1.5, delay: 0.5,
//                       usingSpringWithDamping: 0.3,
//                       initialSpringVelocity: 0.5,
//                       options: [], animations: {
//                        self.promotionInfoView.center.x += self.view.bounds.width
//                        
//        }, completion:{ _ in
//            self.infoViewTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.update), userInfo: nil, repeats: false)
//        })
//    }
//    
//    func setStateDivisionDetails(division: LocationDetail) {
//        println_debug(division.stateOrDivitionCode)
//    }
//    
//    //func setDivisionDetails(divisionName: String, township: String) {
//    func setDivisionDetails(stateDivCode: LocationDetail, divisionName: String, township: String) {
//        self.locationOutlet.text = township + "," + divisionName
//        notfDef.post(name:Notification.Name(rawValue:"PayToStateDivision"),
//                     object: nil,
//                     userInfo: ["Name":  divisionName + "," + township])
//        
//        geoLocManager.getLatLongByName(cityname: township) { (isSuccess, lat, long) in
//            guard isSuccess, let latti = lat, let longi = long  else {
//                self.showErrorAlert(errMessage: "Try Again".localized)
//                return
//            }
//            
//            DispatchQueue.main.async {
//                
//                let locationLat  = CLLocationDegrees.init(Double(latti) ?? 0.0)
//                let locationLong = CLLocationDegrees.init(Double(longi) ?? 0.0)
//                
//                let locationData = CLLocationCoordinate2D.init(latitude: locationLat , longitude: locationLong)
//                let distance = 1.0 //self.slider.value / 3.2808
//                let coordinateRegion = MKCoordinateRegion.init(center: locationData, latitudinalMeters: CLLocationDistance(distance), longitudinalMeters: CLLocationDistance(distance))
//                
//                self.map.setRegion(coordinateRegion, animated: true)
//                
//                self.callNearbyMerchantWithCustomLocation(lat: latti, long: longi)
//                
//                // self.callNearbyMerchantWithCustomLocation(lat: latti, long: longi , business: stateDivCode.stateOrDivitionCode)
//            }
//        }
//    }
//    
//    func merchantWithPromotions() {
//        self.currentLocationUpdate()
//    }
//    
//    func currentLocationUpdate() {
//        geoLocManager.getAddressFrom(lattitude: GeoLocationManager.shared.currentLatitude ?? "0.0" , longitude: GeoLocationManager.shared.currentLongitude ?? "0.0", language: appDel.currentLanguage, completionHandler: { [weak self](success, address) in
//            DispatchQueue.main.async {
//                if success {
//                    if let adrs = address {
//                        if let dictionary = adrs as? Dictionary<String,Any> {
//                            let street = dictionary.safeValueForKey("street") as? String ?? ""
//                            let township = dictionary.safeValueForKey("township") as? String ?? ""
//                            let city = dictionary.safeValueForKey("region") as? String ?? ""
//                            //self?.locationOutlet.text = Rabbit.uni2zg(String.init(format: "%@ ,%@ ,%@", street , township + " " + "Township".localized, city))
//                            self?.locationOutlet.text = Rabbit.uni2zg(String.init(format: "%@ ,%@ ,%@", street , township + " ", city))
//                            //print("Current location called")
//                            
//                            notfDef.post(name:Notification.Name(rawValue:"PayToStateDivision"),
//                                         object: nil,
//                                         userInfo: ["Name":  Rabbit.uni2zg(String.init(format: "%@ ,%@ ,%@", street , township + " ", city))])
//                            
//                        }
//                    }
//                }
//            }
//        })
//        
//        let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
//        let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
//        self.callNearbyMerchantWithCustomLocation(lat: lat, long: long)
//        
//    }
//    
//    @objc func update() {
//        infoViewTimer.invalidate()
//        removeTheInfoView()
//    }
//    
//    func removeTheInfoView() {
//        UIView.animate(withDuration: 0.1,
//                       delay: 0.1,
//                       options: UIView.AnimationOptions.curveEaseIn,
//                       animations: { () -> Void in
//                        self.promotionInfoView.center.x -= self.view.bounds.width
//        }, completion: { (finished) -> Void in
//            self.promotionInfoView.isHidden = true
//            self.promotionInfoView.center.x += self.view.bounds.width
//        })
//    }
//    
//    func loadInfoViewData(info: PromotionsModel) {
////        self.selectedpromotionData = nil
////        self.selectedpromotionData = info
////        promotionImgView.image = UIImage.init()
////        promotionNameLbl.text = info.shop_Name
////        var mobileNumber = "NA"
////        if info.shop_Phonenumber!.count > 0 {
////            mobileNumber = getPhoneNumberByFormat(phoneNumber: info.shop_Phonenumber!)
////        }
////        contactNoLbl.text = "Contact: \(mobileNumber)"
////
////        var distance = "0 Km"
////        if let shopDistance = info.shop_InMeter {
////            distance = String(format: "%.2f Km", shopDistance)
////        }
////        distanceLbl.text = "Distance: \(distance)"
////
////        if info.shop_Templates.count > 0 {
////            let promoOffer = info.shop_Templates[0]
////            remarksLbl.text = promoOffer.template_Header
////        }else {
////            remarksLbl.text = "NA"
////        }
////
////        promotionImgView.image = UIImage.init(named: "btc")
////
////        /* let emoji1 = (merchant.businessCategory?.logo!)!
////         let emoji2 = (merchant.businessCategory?.businessType?.logo!)!
////         let topImage = emoji1.emojiToImage()
////         let bottomImage = emoji2.emojiToImage()
////         promotionImgView.image = topImage?.combineWith(image: bottomImage!) */
////
////
////        //        let emoji1 = (info.shop_Category?.categoryLogo!)!
////        //        let emoji2 = (info.shop_Category?.businessType?.businessType_Logo!)!
////        //        let topImage = emoji1.emojiToImage()
////        //        let bottomImage = emoji2.emojiToImage()
////        //         promotionImgView.image = topImage?.combineWith(image: bottomImage!)
////        //promotionImgView.image = bottomImage
////
////        promotionInfoView.backgroundColor = info.isAgent ? kYellowColor : UIColor.white
//        
//    }
//    
//    
//    /*
//     // MARK: - Navigation
//     
//     // In a storyboard-based application, you will often want to do a little preparation before navigation
//     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//     // Get the new view controller using segue.destinationViewController.
//     // Pass the selected object to the new view controller.
//     }
//     */
//    
//    func reloadMap() {
//        DispatchQueue.main.async {
//            let mapBoundsWidth = Double(self.map.bounds.size.width)
//            let mapRectWidth = self.map.visibleMapRect.size.width
//            let scale = mapBoundsWidth / mapRectWidth
//            let annotationArray = self.clusterManager.clusteredAnnotations(withinMapRect: self.map.visibleMapRect, zoomScale:scale)
//            self.clusterManager.display(annotations: annotationArray, onMapView:self.map)
//        }
//    }
//    
//    
//    
//}
//
//extension MapViewController {
//
//    
//    
//    //MARK:- NEW API CALL
//    
//    func callNearbyMerchantWithCustomLocation(lat: String, long: String)
//    {
//        println_debug("call ok offices api ")
//        
//        let url = getUrl(urlStr: "", serverType: .nearByServicesUrl)
//        
//        //let jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 0, "Latitude" : lat, "Longitude": long, "PhoneNumber" : UserModel.shared.mobileNo, "UserIdNotToConsider": ""] as [String : Any]
//        
//        let jsonDic:[String : Any]
//        
//        // var lat = "16.81668401110357"
//        // var long = "96.13187085779862"
//        
//        
//        if checkurl ==  "NearBy" {
//            
//            // jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 300, "Latitude" : lat, "Longitude": long, "PhoneNumber" : UserModel.shared.mobileNo, "UserIdNotToConsider": ""] as [String : Any]
//            
//            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1500, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":[2,7,8,9,10], "phoneNumberNotToConsider":""] as [String : Any]
//            
//            
//        } else if checkurl ==  "Township"{
//            
//            // jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 0, "Latitude" : lat, "Longitude": long,"optionalSearchParams":["limit":100,"offset":0,"type":intType], "PhoneNumber" : UserModel.shared.mobileNo,"searchAdditionalParams":["divisionName":selectedLocationDetailStr,"townshipname":selectedTownshipDetailStr] ,"UserIdNotToConsider": ""] as [String : Any]
//            
//            
//            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 3000, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":[2,7,8,9,10], "phoneNumberNotToConsider":""] as [String : Any]
//            
//        } else {
//            
//            // jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 0, "Latitude" : lat, "Longitude": long, "PhoneNumber" : UserModel.shared.mobileNo, "UserIdNotToConsider": ""] as [String : Any]
//            
//            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1000, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":[2,7,8,9,10], "phoneNumberNotToConsider":""] as [String : Any]
//            
//        }
//        
//        print("OK$ServiceurlRequest========\(jsonDic)")
//        
//        
//        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url,methodType: kMethod_Post, contentType: kContentType_Json, inputVal: jsonDic, authStr: nil)
//        print("OK$ServiceurlRequest========\(urlRequest)")
//        
//        DispatchQueue.main.async {
//            progressViewObj.showProgressView()
//        }
//        
//        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { [weak self](isSuccess, response) in
//            
//            guard isSuccess else {
//                DispatchQueue.main.async {
//                    progressViewObj.removeProgressView()
//                    //println_debug("remove progress view called inside cashin offices api main call with error")
//                }
//                //self.showErrorAlert(errMessage: "Try Again".localized)
//                return
//            }
//            println_debug("response dict for get all offices list :: \(response ?? "")")
//            progressViewObj.removeProgressView()
//            
//            var cashArray = [NearByServicesNewModel]()
//            DispatchQueue.main.async {
//                cashArray = ParserAttributeClass.parseNearbyServiceJSONNew(anyValue: response as AnyObject) as [NearByServicesNewModel]
//                println_debug("response count for ok offices :::: \(cashArray.count)")
//                progressViewObj.removeProgressView()
//                
//                if cashArray.count > 0 {
//                    self?.clusterManager.removeAll()
//                    self?.merchantModel.removeAll()
//                    self?.merchantModelBackup.removeAll()
//                    
//                    //Seperate mechant =2,agent=1,office=7
//                    for indexVal in 0..<cashArray.count {
//                        let currentModel = cashArray[indexVal]
//                        
//                        
//                        self?.merchantModel.append(currentModel)
//                        self?.merchantModelBackup.append(currentModel)
//                        
//                        /*  if self.intType == currentModel.UserContactData!.type! {
//                         
//                         print("innertype-------\(currentModel.UserContactData!.type)")
//                         self.nearByServicesList.append(currentModel)
//                         self.nearByServicesListBackup.append(currentModel)
//                         self.cashInListBackUPByCurLoc.append(currentModel)
//                         
//                         } */
//                    }
//                    
//                    DispatchQueue.main.async {
//                        
//                        
//                        
//                        
//                        if let weak = self {
//                            let text = "Near By Merchants".localized
//                            weak.merchantCountglobalLabel.text = "\(text) \(weak.merchantModel.count)"
//                            weak.listTableView.reloadData()
//                            weak.listViewContainer.isHidden = false
//                            weak.addClustersToMap(list: weak.merchantModel)
//                            
//                            //                            print("**********")
//                            //                            let storyboard : UIStoryboard = UIStoryboard(name: "Map", bundle: nil)
//                            //                            let vc : MapViewController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
//                            //                            vc.merchantModel = weak.merchantModel
//                            //
//                            //                            let navigationController = UINavigationController(rootViewController: vc)
//                            
//                            //  weak.presentDetail(navigationController)
//                            //  weak.present(navigationController, animated: true, completion: nil)
//                            
//                            
//                        }
//                    }
//                    
//                    
//                }
//                else
//                {
//                    self?.showErrorAlert(errMessage: "No Record Found".localized)
//                    //self.showHideInfoLabel(isShow: true, content: "No Record Found".localized)
//                }
//            }
//        })
//    }
//    
//    
//    func callNearbyMerchantWithCustomLocationFirst(lat: String, long: String , str:Int, index:Int)
//    {
//        
//        var type : [Int] = [2,7,8,9,10]
//        
//        if str == 0 {
//            type =  [2,7,8,9,10]
//        }
//        else if str == 1 {
//            type = [8]
//        }
//        else if str == 2 {
//            type = [7]
//        }
//        else if str == 3 {
//            type = [2]
//            
//        }
//        else if str == 4 {
//            type = [9]
//            
//        }
//        else if str == 5 {
//            type = [10]
//            
//        }
//        
//        println_debug("call ok offices api ---\(type)")
//        
//        let url = getUrl(urlStr: "", serverType: .nearByServicesUrl)
//        
//        //let jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 0, "Latitude" : lat, "Longitude": long, "PhoneNumber" : UserModel.shared.mobileNo, "UserIdNotToConsider": ""] as [String : Any]
//        
//        let jsonDic:[String : Any]
//        
//        //                var lat = "16.81668401110357"
//        //                var long = "96.13187085779862"
//        
//        
//        if checkurl ==  "NearBy" {
//            
//            // jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 300, "Latitude" : lat, "Longitude": long, "PhoneNumber" : UserModel.shared.mobileNo, "UserIdNotToConsider": ""] as [String : Any]
//            
//            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1500, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":type, "phoneNumberNotToConsider":""] as [String : Any]
//            
//            
//        } else if checkurl ==  "Township"{
//            
//            // jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 0, "Latitude" : lat, "Longitude": long,"optionalSearchParams":["limit":100,"offset":0,"type":intType], "PhoneNumber" : UserModel.shared.mobileNo,"searchAdditionalParams":["divisionName":selectedLocationDetailStr,"townshipname":selectedTownshipDetailStr] ,"UserIdNotToConsider": ""] as [String : Any]
//            
//            
//            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 3000, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":type, "phoneNumberNotToConsider":""] as [String : Any]
//            
//        } else {
//            
//            // jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 0, "Latitude" : lat, "Longitude": long, "PhoneNumber" : UserModel.shared.mobileNo, "UserIdNotToConsider": ""] as [String : Any]
//            
//            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1000, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":type, "phoneNumberNotToConsider":""] as [String : Any]
//            
//        }
//        
//        print("OK$ServiceurlRequest========\(jsonDic)")
//        
//        
//        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url,methodType: kMethod_Post, contentType: kContentType_Json, inputVal: jsonDic, authStr: nil)
//        print("OK$ServiceurlRequest========\(urlRequest)")
//        
//        DispatchQueue.main.async {
//            progressViewObj.showProgressView()
//        }
//        
//        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { [weak self](isSuccess, response) in
//            
//            guard isSuccess else {
//                DispatchQueue.main.async {
//                    progressViewObj.removeProgressView()
//                    //println_debug("remove progress view called inside cashin offices api main call with error")
//                }
//                //self.showErrorAlert(errMessage: "Try Again".localized)
//                return
//            }
//            println_debug("response dict for get all offices list :: \(response ?? "")")
//            progressViewObj.removeProgressView()
//            
//            var cashArray = [NearByServicesNewModel]()
//            DispatchQueue.main.async {
//                cashArray = ParserAttributeClass.parseNearbyServiceJSONNew(anyValue: response as AnyObject) as [NearByServicesNewModel]
//                println_debug("response count for ok offices :::: \(cashArray.count)")
//                progressViewObj.removeProgressView()
//                
//                if cashArray.count > 0 {
//                    self?.clusterManager.removeAll()
//                    self?.merchantModel.removeAll()                    //
//                    self?.merchantModelBackup.removeAll()
//                    
//                    //Seperate mechant =2,agent=1,office=7
//                    for indexVal in 0..<cashArray.count {
//                        let currentModel = cashArray[indexVal]
//                        
//                        self?.merchantModel.append(currentModel)
//                        self?.merchantModelBackup.append(currentModel)
//                        
//                        
//                        
//                        
//                        /*  if self.intType == currentModel.UserContactData!.type! {
//                         
//                         print("innertype-------\(currentModel.UserContactData!.type)")
//                         self.nearByServicesList.append(currentModel)
//                         self.nearByServicesListBackup.append(currentModel)
//                         self.cashInListBackUPByCurLoc.append(currentModel)
//                         
//                         } */
//                    }
//                    
//                    DispatchQueue.main.async {
//                        if let weak = self {
//                            println_debug("Reload called")
//                            
//                            switch index {
//                            case 0:
//                                println_debug("default case falling")
//                                
//                                //                            self.merchantModel = merchantModelBackup
//                                //                            print("datalist list11----\(merchantModel.count)")
//                                //                            listTableView.reloadData()
//                                
//                            case 1:
//                                println_debug("Name A to Z")
//                                if let list = self!.merchantModel as? [NearByServicesNewModel] {
//                                    self!.merchantModel = list.sorted(by: { $0.UserContactData!.BusinessName! <  $1.UserContactData!.BusinessName! })
//                                    // merchantModel = list.sorted(by: {$0.UserContactData?.BusinessName < $1.UserContactData?.BusinessName})
//                                    print("datalist list11----\(self!.merchantModel.count)")
//                                    // self.listTableView.reloadData()
//                                }
//                                
//                            case 2:
//                                println_debug("Name Z to A")
//                                if let list = self!.merchantModel as? [NearByServicesNewModel] {
//                                    self!.merchantModel = list.sorted(by: { $0.UserContactData!.BusinessName! >  $1.UserContactData!.BusinessName! })
//                                    // listTableView.reloadData()
//                                }
//                                
//                            default:
//                                break
//                            }
//                            
//                            let text = "Near By Merchants".localized
//                            weak.merchantCountglobalLabel.text = "\(text) \(weak.merchantModel.count)"
//                            
//                            weak.listTableView.reloadData()
//                            weak.listViewContainer.isHidden = false
//                            weak.addClustersToMap(list: weak.merchantModel)
//                        }
//                    }
//                    
//                    
//                }
//                else
//                {
//                    self?.showErrorAlert(errMessage: "No Record Found".localized)
//                    //self.showHideInfoLabel(isShow: true, content: "No Record Found".localized)
//                }
//            }
//        })
//    }
//    
//    
//    func callNearbyMerchantWithCustomLocationNew(lat: String, long: String , str:Int, index:Int)
//    {
//        
//        var type : [Int] = [2,7,8,9,10]
//        
//        if str == 0 {
//            type =  [2,7,8,9,10]
//        }
//        else if str == 1 {
//            type = [8]
//        }
//        else if str == 2 {
//            type = [7]
//        }
//        else if str == 3 {
//            type = [2]
//            
//        }
//        else if str == 4 {
//            type = [9]
//            
//        }
//        else if str == 5 {
//            type = [10]
//            
//        }
//        
//        println_debug("call ok offices api ---\(type)")
//        
//        if self.merchantModelBackup.count > 0 {
//            self.clusterManager.removeAll()
//            self.merchantModel.removeAll()
//            
//            //Seperate mechant =2,agent=1,office=7
//            for indexVal in 0..<self.merchantModelBackup.count {
//                
//                let currentModel = self.merchantModelBackup[indexVal]
//                
//                if str == 0 {
//                    
//                    self.merchantModel.append(currentModel)
//                    
//                } else {
//                    
//                    var value = type[0]
//                    if value == currentModel.UserContactData!.type! {
//                        
//                        print("innertype-------\(currentModel.UserContactData!.type)")
//                        self.merchantModel.append(currentModel)
//                        
//                    }
//                    
//                }
//            }
//            
//            DispatchQueue.main.async {
//                println_debug("Reload called")
//                
//                switch index {
//                case 0:
//                    println_debug("default case falling")
//                    
//                    //                            self.merchantModel = merchantModelBackup
//                    //                            print("datalist list11----\(merchantModel.count)")
//                    //                            listTableView.reloadData()
//                    
//                case 1:
//                    println_debug("Name A to Z")
//                    if let list = self.merchantModel as? [NearByServicesNewModel] {
//                        
//                        var name1 : String = ""
//                        var name2 : String = ""
//                        
//                        self.merchantModel = list.sorted {
//                            
//                            if $0.UserContactData!.BusinessName != "" {
//                                name1 = $0.UserContactData!.BusinessName!
//                            } else {
//                                name1 = $0.UserContactData!.FirstName!
//                            }
//                            
//                            if $1.UserContactData!.BusinessName != "" {
//                                name2 = $1.UserContactData!.BusinessName!
//                            } else {
//                                name2 = $1.UserContactData!.FirstName!
//                            }
//                            return name1.compare(name2) == .orderedAscending
//                        }
//                        
//                    }
//                    
//                case 2:
//                    println_debug("Name Z to A")
//                    if let list = self.merchantModel as? [NearByServicesNewModel] {
//                        
//                        var name1 : String = ""
//                        var name2 : String = ""
//                        
//                        self.merchantModel = list.sorted {
//                            
//                            if $0.UserContactData!.BusinessName != "" {
//                                name1 = $0.UserContactData!.BusinessName!
//                            } else {
//                                name1 = $0.UserContactData!.FirstName!
//                            }
//                            
//                            if $1.UserContactData!.BusinessName != "" {
//                                name2 = $1.UserContactData!.BusinessName!
//                            } else {
//                                name2 = $1.UserContactData!.FirstName!
//                            }
//                            return name1.compare(name2) == .orderedDescending
//                        }
//                        
//                        
//                    }
//                    
//                default:
//                    break
//                }
//                
//                let text = "Near By Merchants".localized
//                self.merchantCountglobalLabel.text = "\(text) \(self.merchantModel.count)"
//                
//                self.listTableView.reloadData()
//                self.listViewContainer.isHidden = false
//                self.addClustersToMap(list: self.merchantModel)
//                
//            }
//            
//            
//        }
//        else
//        {
//            self.showErrorAlert(errMessage: "No Record Found".localized)
//            //self.showHideInfoLabel(isShow: true, content: "No Record Found".localized)
//        }
//        
//        
//        
//    }
//    
//}
//
//
//
//extension MapViewController: MainFilterApplyViewDelegate {
//    
//    func didSelectApplyFilter(category: SubCategoryDetail? , index:Int , str:Int){
//        
//        print("PAYTO MAP===\(str)---\(index)")
//        
//        let long = GeoLocationManager.shared.currentLongitude ?? "0.0"
//        let lat  = GeoLocationManager.shared.currentLatitude ?? "0.0"
//        self.callNearbyMerchantWithCustomLocationNew(lat: lat, long: long, str: str , index:index)
//        
//        
//    }
//    
//}
