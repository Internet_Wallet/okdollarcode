//
//  ExtensionMapShopUI.swift
//  OK
//
//  Created by iMac on 8/31/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation


extension MapPayToVC{
    
    func createHeader() -> UIView{
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: shopTableView.frame.width, height: 200))
        let mapShareButton = UIButton.init(frame: CGRect.init(x: shopTableView.frame.width - 80 , y: 10, width: 30, height: 30))
        headerButton.frame = CGRect(x: 20, y: 10, width: 30, height: 30)
        mapShareButton.backgroundColor = .clear
        headerButton.backgroundColor = .black
        headerButton.setImage(UIImage(named: "blueCross.png"), for: .normal)
        mapShareButton.setImage(UIImage(named: "mapShare.png"), for: .normal)
        headerButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        mapShareButton.addTarget(self, action: #selector(buttonActionMap), for: .touchUpInside)
        headerButton.layer.cornerRadius = 15
        mapShareButton.layer.cornerRadius = 15
        headerButton.clipsToBounds = true
        mapShareButton.clipsToBounds = true
        shopImageView.frame = CGRect.init(x: 5, y: 0, width: headerView.frame.width-10, height: headerView.frame.height-10)
        shopImageView.image = UIImage(named: "newBetterthan.png")
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(viewDidDrag(_:)))
        shopImageView.addGestureRecognizer(panGesture)
        headerView.addSubview(mapShareButton)
        headerView.addSubview(headerButton)
        headerView.addSubview(shopImageView)
        headerView.bringSubviewToFront(headerButton)
        headerView.bringSubviewToFront(mapShareButton)
        return headerView
 } 
    
    func createFooter() -> UIView{
        
        
//        let detailsLabel = UILabel.init(frame: CGRect.init(x: headerView.frame.width/4 + 10, y: headerViewBackGround.center.y + 5 , width: 150, height: 25))
//        detailsLabel.text = "Merchant Details".localized
//        detailsLabel.textAlignment = .center
//        detailsLabel.font = UIFont.init(name: "Zawgyi-One", size: 20)
//        detailsLabel.textColor = blueColor
//        detailsLabel.center = headerViewBackGround.center
//
        
        let footerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: shopTableView.frame.width, height: 50))
        footerView.backgroundColor = .lightGray
        let footerButton = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 100, height: 50))
        footerButton.backgroundColor = .yellow
      //  footerButton.titleLabel?.textAlignment = .center
        footerButton.setTitleColor(.white, for: .normal)
        footerButton.setTitle("Make Payment".localized, for: .normal)
        footerButton.addTarget(self, action: #selector(footerAction), for: .touchUpInside)
        if iscategoryAPICall{
            footerButton.isHidden = false
        }else{
            footerButton.isHidden = true
        }
        
       // footerButton.center = footerView.center
        footerView.addSubview(footerButton)
        return footerView
    }
    
    @objc func footerAction(sender: UIButton!){
       isComingFromMapMakePayment = true
//     //   self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
//         self.navigationController?.popToRootViewController(animated: true)
//
////        guard let vc = self.presentingViewController else { return }
////
////        while (vc.presentingViewController != nil) {
////            vc.dismiss(animated: true, completion: nil)
////        }
        self.navigateToPayto(phone: self.selectedPromotion?.shop_Phonenumber ?? "" , amount: "", name: self.selectedPromotion?.shop_AgentName ?? "", fromMerchant: true)
      //  self.presentingViewController?.dismiss(animated: true, completion: {
//            self.dismiss(animated: true, completion: {
//                self.navigateToPayto(phone: self.selectedPromotion?.shop_Phonenumber ?? "" , amount: "", name: self.selectedPromotion?.shop_AgentName ?? "", fromMerchant: true)
//            })
//        })
       
    }
    
    @objc func infoAction(sender: UIButton!){
        checkIfComingFromOption = "info"
        changeButtonColor(backGroundColor: blueColor, textColor: .black, button: infoButton, baseView: infoBase)
        changeButtonColor(backGroundColor: .white, textColor: .lightGray, button: photoButton, baseView: photoBase)
        changeButtonColor(backGroundColor: .white, textColor: .lightGray, button: descriptionButton, baseView: descriptionBase)
        
    }
    
    @objc func photoAction(sender: UIButton!){
        checkIfComingFromOption = "photo"
        changeButtonColor(backGroundColor: blueColor, textColor: .black, button: photoButton, baseView: photoBase)
        changeButtonColor(backGroundColor: .white, textColor: .lightGray, button: infoButton, baseView: infoBase)
        changeButtonColor(backGroundColor: .white, textColor: .lightGray, button: descriptionButton, baseView: descriptionBase)
        
        
    }
    
    @objc func descriptionAction(sender: UIButton!){
        checkIfComingFromOption = ""
        changeButtonColor(backGroundColor: blueColor, textColor: .black, button: descriptionButton, baseView: descriptionBase)
        changeButtonColor(backGroundColor: .white, textColor: .lightGray, button: photoButton, baseView: photoBase)
        changeButtonColor(backGroundColor: .white, textColor: .lightGray, button: infoButton, baseView: infoBase)
        
    }
    
    func changeButtonColor(backGroundColor: UIColor, textColor: UIColor, button: UIButton, baseView: UIView){
        button.setTitleColor(textColor, for: .normal)
        baseView.backgroundColor = backGroundColor
         shopTableView.reloadData()
        if checkIfComingFromOption == "info"{
            shopTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        }else if checkIfComingFromOption == "photo"{
            shopTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        }else{
            shopTableView.scrollToRow(at: IndexPath(row: 0 , section: 0), at: .top, animated: true)
        }
       
       
    }
    
    //TusharLama
    //This will work when you click agent details and you drag the view from bottom
    @objc private func viewDidDrag(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: self.view)
        if sender.state == UIGestureRecognizer.State.began {
            print("Began")
        }
        
        if sender.state == UIGestureRecognizer.State.ended {
            print("Ended")
            if !isComingFromInitialView{
                self.headerButton.setImage(UIImage(named: "arrow_down.png"), for: .normal)
            }
            animationViewHeight(top: -20)
        }
        
        if sender.state == UIGestureRecognizer.State.changed {
            print("Changed")
            if self.shopViewHeight.constant != -20{
                animationViewHeight(top: height + translation.y)
            }
        } else {
            print("Dragging")
        }
    }

}
