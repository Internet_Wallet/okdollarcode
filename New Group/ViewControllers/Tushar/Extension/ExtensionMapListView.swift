//
//  ExtensionMapListView.swift
//  OK
//
//  Created by iMac on 8/31/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation


extension MapPayToVC{
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        
        if gesture.direction == .right {
            print("Swipe Right")
        }
        else if gesture.direction == .left {
            print("Swipe Left")
        }
        else if gesture.direction == .up {
            playWithUpGesture()
        }
        else if gesture.direction == .down {
            playWithDownGesture()
        }
    }
    func playWithUpGesture(){
        if top.constant ==  CGFloat((self.view.frame.size.height)/2 + 100){
            designView(animateView: listViewContainer, cornerRadius: 0.0, borderWidth: 0.0)
            animationViewHeight(top: -44)
            click = 1
        }else  if UIScreen.main.bounds.size.height >= 812{
            if  top.constant ==  CGFloat((self.view.frame.size.height) - 120){
                designView(animateView: listViewContainer, cornerRadius: 20.0, borderWidth: 2.0)
                animationViewHeight(top: CGFloat((self.view.frame.size.height)/2 + 100))
                click = 2
            }
        }else{
            if  top.constant ==  CGFloat((self.view.frame.size.height) - 85){
                designView(animateView: listViewContainer, cornerRadius: 20.0, borderWidth: 2.0)
                animationViewHeight(top: CGFloat((self.view.frame.size.height)/2 + 100))
                click = 2
            }
        }
            
        
    }
    func playWithDownGesture(){
        if top.constant == -44{
            designView(animateView: listViewContainer, cornerRadius: 20.0, borderWidth: 2.0)
            animationViewHeight(top: CGFloat((self.view.frame.size.height)/2 + 100))
            click = 2
        }else if top.constant == CGFloat((self.view.frame.size.height)/2 + 100){
            designView(animateView: listViewContainer, cornerRadius: 20.0, borderWidth: 2.0)
            if UIScreen.main.bounds.size.height >= 812{
                animationViewHeight(top: CGFloat((self.view.frame.size.height) - 120))
            }else{
                animationViewHeight(top: CGFloat((self.view.frame.size.height) - 85))
            }
            click = 0
        }
    }
    @objc func playWithView(gesture: UITapGestureRecognizer) -> Void {
        if click == 0 {
            designView(animateView: listViewContainer, cornerRadius: 0.0, borderWidth: 0.0)
            animationViewHeight(top: -44)
            click = 1
        }else if click == 1{
            designView(animateView: listViewContainer, cornerRadius: 20.0, borderWidth: 2.0)
            animationViewHeight(top: CGFloat((self.view.frame.size.height)/2 + 100))
            click = 2
        }else if click == 2{
            designView(animateView: listViewContainer, cornerRadius: 20.0, borderWidth: 2.0)
            if UIScreen.main.bounds.size.height >= 812{
                animationViewHeight(top: CGFloat((self.view.frame.size.height) - 120))
            }else{
                animationViewHeight(top: CGFloat((self.view.frame.size.height) - 85))
            }
            
            
            click = 0
        }
    }
    @objc func playWithTopBackGround(gesture: UITapGestureRecognizer) -> Void {
        animationViewHeight(top: CGFloat((self.view.frame.size.height)/2 + 100))
        click = 2
    }
    
    func animationViewHeight(top: CGFloat){
        
        if showAgentShopView{
            
        }else{
            if top == -44{
                mapCrossButton.isHidden = true
                topBackGroundView.isHidden = false
                topViewHeight.constant = 101
                crossButtonOutLet.isHidden = false
            }else{
                mapCrossButton.isHidden = false
                topBackGroundView.isHidden = false
                topViewHeight.constant = 0
                crossButtonOutLet.isHidden = true
            }
        }
        
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            if self.showAgentShopView{
                self.shopViewHeight.constant  = CGFloat(top)
            }else{
                self.top.constant  = CGFloat(top)
            }
            self.view.layoutIfNeeded()
        })
    }
    
    func createShopTableHeader() -> UIView{
    
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: 120))
        headerView.backgroundColor = .white
        let headerViewBackGround = UIView.init(frame: CGRect.init(x: 0, y: 0, width:  self.view.frame.width, height: 64))
        headerViewBackGround.backgroundColor = kYellowColor
        
        descriptionBase.frame = CGRect(x: 0, y: headerView.frame.origin.y + headerView.frame.size.height , width: headerView.frame.size.width/3 - 5, height: 5)
        infoButton.frame =  CGRect(x: 0, y: 65, width: headerView.frame.size.width/3 - 5, height: 40)
        infoBase.frame = CGRect(x: 0, y: infoButton.frame.origin.y + infoButton.frame.size.height , width: headerView.frame.size.width/3 - 5, height: 5)
        infoButton.setTitle("Info".localized, for: .normal)
        infoButton.setTitleColor(.black, for: .normal)
        infoButton.addTarget(self, action: #selector(infoAction), for: .touchUpInside)
        
        photoButton.frame = CGRect(x: infoButton.frame.origin.x + infoButton.frame.size.width + 5, y: 65, width: headerView.frame.size.width/3 - 5, height: 40)
        photoBase.frame = CGRect(x: infoButton.frame.origin.x + infoButton.frame.size.width + 5, y: photoButton.frame.origin.y + photoButton.frame.size.height , width: headerView.frame.size.width/3 - 5, height: 5)
        photoButton.setTitle("Photo".localized, for: .normal)
        photoButton.setTitleColor(.lightGray, for: .normal)
        photoButton.addTarget(self, action: #selector(photoAction), for: .touchUpInside)
        
        descriptionButton.frame = CGRect(x: photoButton.frame.origin.x + photoButton.frame.size.width + 5, y: 65, width: headerView.frame.size.width/3 , height: 40)
        descriptionButton.setTitleColor(.lightGray, for: .normal)
        descriptionButton.setTitle("Direction".localized, for: .normal)
        descriptionButton.addTarget(self, action: #selector(descriptionAction), for: .touchUpInside)
        descriptionBase.frame = CGRect(x: photoButton.frame.origin.x + photoButton.frame.size.width + 5, y: descriptionButton.frame.origin.y + descriptionButton.frame.size.height , width: headerView.frame.size.width/3 - 5, height: 5)
        
        let detailsLabel = UILabel.init(frame: CGRect.init(x: 0, y: 30 , width: self.view.frame.width, height: 25))
        detailsLabel.text = "Details".localized
        detailsLabel.textAlignment = .center
        detailsLabel.font = UIFont.init(name: appFont, size: 20)
        detailsLabel.textColor = blueColor
       // detailsLabel.center = headerViewBackGround.center
        let mapShareButton = UIButton.init(frame: CGRect.init(x: detailsLabel.frame.origin.x + self.view.frame.width - 50 , y: 30, width: 25, height: 25))
        headerButton.frame = CGRect(x: 20, y: 30, width: 25, height: 25)
        mapShareButton.backgroundColor = .clear
        headerButton.backgroundColor = .clear
        headerButton.setImage(UIImage(named: "blueCross.png"), for: .normal)
        mapShareButton.setImage(UIImage(named: "blueShare.png"), for: .normal)
        headerButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        mapShareButton.addTarget(self, action: #selector(buttonActionMap), for: .touchUpInside)
        headerButton.layer.cornerRadius = 15
        mapShareButton.layer.cornerRadius = 15
        headerButton.clipsToBounds = true
        mapShareButton.clipsToBounds = true
        
        headerViewBackGround.addSubview(detailsLabel)
        headerView.addSubview(headerViewBackGround)
        headerView.addSubview(infoButton)
        headerView.addSubview(photoButton)
        headerView.addSubview(descriptionButton)
        headerView.addSubview(photoBase)
        headerView.addSubview(infoBase)
        headerView.addSubview(descriptionBase)
        headerView.addSubview(mapShareButton)
        headerView.addSubview(headerButton)
        headerView.bringSubviewToFront(headerButton)
        headerView.bringSubviewToFront(mapShareButton)
        return headerView
        
    }
}
