//
//  ExtensionMapService.swift
//  OK
//
//  Created by iMac on 8/31/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation


extension MapPayToVC: WebServiceResponseDelegate{
    
    func webResponse(withJson json: AnyObject, screen: String) {
        self.removeProgressView()
        do {
            if let data = json as? Data {
                let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                print("promotiondetail category----\(dict)")
                if let dic = dict as? Dictionary<String,AnyObject> {
                    if dic["Code"]! as! Int == 200 {
                        
                        DispatchQueue.main.async {
                            self.loaderView.isHidden = true
                            let dataDict = dic["Data"] as? String
                            print(dataDict)
                            self.categorylistArr = OKBaseController.convertToDictionary(text: dataDict!)!
                            print(self.categorylistArr)
                            self.mainCatName = self.categorylistArr.safeValueForKey("MerchantCatName") as! String
                            print(self.mainCatName)
                           // self.shopTableView.tableFooterView = self.createFooter()
                            self.shopTableView.delegate = self
                            self.shopTableView.dataSource = self
                          self.animationViewHeight(top: 0)
                          //  self.shopTableView.setContentOffset(CGPoint(x: 0, y: 20), animated: false)
                           // self.shopTableView.scrollToTop()
                          self.shopTableView.reloadData()
                            self.showAgentShopView = true
                            
                            

//                            if self.isComingFromInitialView{
//                                self.animationViewHeight(top: -20)
//                            }else{
//                                //This will be uncommented if you want flow like trivago
//                                //self.animationViewHeight(top: CGFloat((self.view.frame.size.height)/2 + 100))
//                            }
                            self.setViewWithAnimation(view: self.shopDetailView, hidden: false)
                            self.setViewWithAnimation(view: self.topBackGroundView, hidden: true)
                            self.setViewWithAnimation(view: self.listViewContainer, hidden: true)
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.loaderView.isHidden = true
                           
                            // self.shopTableView.tableFooterView = self.createFooter()
                            self.shopTableView.delegate = self
                            self.shopTableView.dataSource = self
                            self.animationViewHeight(top: 0)
                            //  self.shopTableView.setContentOffset(CGPoint(x: 0, y: 20), animated: false)
                            // self.shopTableView.scrollToTop()
                            self.shopTableView.reloadData()
                            self.showAgentShopView = true
                            
                            
                            
                            //                            if self.isComingFromInitialView{
                            //                                self.animationViewHeight(top: -20)
                            //                            }else{
                            //                                //This will be uncommented if you want flow like trivago
                            //                                //self.animationViewHeight(top: CGFloat((self.view.frame.size.height)/2 + 100))
                            //                            }
                            self.setViewWithAnimation(view: self.shopDetailView, hidden: false)
                            self.setViewWithAnimation(view: self.topBackGroundView, hidden: true)
                            self.setViewWithAnimation(view: self.listViewContainer, hidden: true)
                        }
                    }
                }
            }
        } catch {
            
        }
    }
}

extension MapPayToVC {
    
    func callNearbyMerchantWithCustomLocation(lat: String, long: String)
    {
        println_debug("call ok offices api ")
        
        let url = getUrl(urlStr: "", serverType: .nearByServicesUrl)
        let jsonDic:[String : Any]
        
        if checkurl ==  "NearBy" {
            
            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1500, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":[2,7,8,9,10], "phoneNumberNotToConsider":""] as [String : Any]
        } else if checkurl ==  "Township"{
            
              jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 3000, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":selectedTownshipDetailStr,"cityName":"","businessName":"","country":"","divisionName":selectedLocationDetailStr,"line1":""],"paginationParams": ["offset":0,"limit":0],"types":[7,2,8,9,11], "phoneNumberNotToConsider":""] as [String : Any]
        } else {
            
            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1000, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":[2,7,8,9,10], "phoneNumberNotToConsider":""] as [String : Any]
            
        }
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url,methodType: kMethod_Post, contentType: kContentType_Json, inputVal: jsonDic, authStr: nil)
        
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
        
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { [weak self](isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                }
                return
            }
            
            var cashArray = [NearByServicesNewModel]()
            DispatchQueue.main.async {
                cashArray = ParserAttributeClass.parseNearbyServiceJSONNew(anyValue: response as AnyObject) as [NearByServicesNewModel]
                if cashArray.count > 0 {
                    self?.clusterManager.removeAll()
                    self?.merchantModel.removeAll()
                    self?.merchantModelBackup.removeAll()
                    for indexVal in 0..<cashArray.count {
                        let currentModel = cashArray[indexVal]
                        self?.merchantModel.append(currentModel)
                        self?.merchantModelBackup.append(currentModel)
                    }
                    
                    self?.initialTableView.delegate = self
                    self?.initialTableView.dataSource = self
                    self?.initialTableView.reloadData()
                    self?.listTableView.reloadData()
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(8), execute: {
                        progressViewObj.removeProgressView()
                        
                    })
                    
                    if let weak = self {
                        let text = "Near By Merchants".localized
                        weak.merchantCountglobalLabel.text = "\(text) \(weak.merchantModel.count)"
                        weak.listTableView.reloadData()
                        weak.addClustersToMap(list: weak.merchantModel)
                    }
                    
                }else{
                    self?.showErrorAlert(errMessage: "No Record Found".localized)
                }
            }
        })
    }
    
    
    func callNearbyMerchantWithCustomLocationFirst(lat: String, long: String , str:Int, index:Int){
        
        var type : [Int] = [2,7,8,9,10]
        
        if str == 0 {
            type =  [2,7,8,9,10]
        }
        else if str == 1 {
            type = [8]
        }
        else if str == 2 {
            type = [7]
        }
        else if str == 3 {
            type = [2]
            
        }
        else if str == 4 {
            type = [9]
            
        }
        else if str == 5 {
            type = [10]
            
        }
        
        println_debug("call ok offices api ---\(type)")
        
        let url = getUrl(urlStr: "", serverType: .nearByServicesUrl)
        
        let jsonDic:[String : Any]
        
        if checkurl ==  "NearBy" {
            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1500, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":type, "phoneNumberNotToConsider":""] as [String : Any]
            
            
        } else if checkurl ==  "Township"{
            
            
            
            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 3000, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":type, "phoneNumberNotToConsider":""] as [String : Any]
            
        } else {
            
            
            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1000, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":type, "phoneNumberNotToConsider":""] as [String : Any]
            
        }
        
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url,methodType: kMethod_Post, contentType: kContentType_Json, inputVal: jsonDic, authStr: nil)
        
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
        
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { [weak self](isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                }
                return
            }
            println_debug("response dict for get all offices list :: \(response ?? "")")
            progressViewObj.removeProgressView()
            
            var cashArray = [NearByServicesNewModel]()
            DispatchQueue.main.async {
                cashArray = ParserAttributeClass.parseNearbyServiceJSONNew(anyValue: response as AnyObject) as [NearByServicesNewModel]
                println_debug("response count for ok offices :::: \(cashArray.count)")
                progressViewObj.removeProgressView()
                
                if cashArray.count > 0 {
                    self?.clusterManager.removeAll()
                    self?.merchantModel.removeAll()
                    self?.merchantModelBackup.removeAll()
                    
                    for indexVal in 0..<cashArray.count {
                        let currentModel = cashArray[indexVal]
                        
                        self?.merchantModel.append(currentModel)
                        self?.merchantModelBackup.append(currentModel)
                    }
                    
                    DispatchQueue.main.async {
                        if let weak = self {
                            println_debug("Reload called")
                            
                            switch index {
                            case 0:
                                println_debug("default case falling")
                                
                            case 1:
                                println_debug("Name A to Z")
                                if let list = self!.merchantModel as? [NearByServicesNewModel] {
                                    self!.merchantModel = list.sorted(by: { $0.UserContactData!.BusinessName! <  $1.UserContactData!.BusinessName! })
                                    // merchantModel = list.sorted(by: {$0.UserContactData?.BusinessName < $1.UserContactData?.BusinessName})
                                    print("datalist list11----\(self!.merchantModel.count)")
                                    // self.listTableView.reloadData()
                                }
                                
                            case 2:
                                println_debug("Name Z to A")
                                if let list = self!.merchantModel as? [NearByServicesNewModel] {
                                    self!.merchantModel = list.sorted(by: { $0.UserContactData!.BusinessName! >  $1.UserContactData!.BusinessName! })
                                    // listTableView.reloadData()
                                }
                                
                            default:
                                break
                            }
                            
                            let text = "Near By Merchants".localized
                            weak.merchantCountglobalLabel.text = "\(text) \(weak.merchantModel.count)"
                            weak.listTableView.reloadData()
                            weak.addClustersToMap(list: weak.merchantModel)
                        }
                    }
                    
                    
                } else{
                    self?.showErrorAlert(errMessage: "No Record Found".localized)
                }
            }
        })
    }
    
    
    func callNearbyMerchantWithCustomLocationNew(lat: String, long: String , str:Int, index:Int){
        
        var type : [Int] = [2,7,8,9,10]
        
        if str == 0 {
            type =  [2,7,8,9,10]
        }
        else if str == 1 {
            type = [8]
        }
        else if str == 2 {
            type = [7]
        }
        else if str == 3 {
            type = [2]
            
        }
        else if str == 4 {
            type = [9]
            
        }
        else if str == 5 {
            type = [10]
            
        }
        
        println_debug("call ok offices api ---\(type)")
        
        if self.merchantModelBackup.count > 0 {
            self.clusterManager.removeAll()
            self.merchantModel.removeAll()
            
            //Seperate mechant =2,agent=1,office=7
            for indexVal in 0..<self.merchantModelBackup.count {
                
                let currentModel = self.merchantModelBackup[indexVal]
                
                if str == 0 {
                    
                    self.merchantModel.append(currentModel)
                    
                } else {
                    
                    let value = type[0]
                    if value == currentModel.UserContactData!.type! {
                        self.merchantModel.append(currentModel)
                    }
                    
                }
            }
            
            DispatchQueue.main.async {
                println_debug("Reload called")
                
                switch index {
                case 0:
                    println_debug("default case falling")
                case 1:
                    println_debug("Name A to Z")
                    if let list = self.merchantModel as? [NearByServicesNewModel] {
                        
                        var name1 : String = ""
                        var name2 : String = ""
                        
                        self.merchantModel = list.sorted {
                            
                            if $0.UserContactData!.BusinessName != "" {
                                name1 = $0.UserContactData!.BusinessName!
                            } else {
                                name1 = $0.UserContactData!.FirstName!
                            }
                            
                            if $1.UserContactData!.BusinessName != "" {
                                name2 = $1.UserContactData!.BusinessName!
                            } else {
                                name2 = $1.UserContactData!.FirstName!
                            }
                            return name1.compare(name2) == .orderedAscending
                        }
                        
                    }
                    
                case 2:
                    println_debug("Name Z to A")
                    if let list = self.merchantModel as? [NearByServicesNewModel] {
                        
                        var name1 : String = ""
                        var name2 : String = ""
                        
                        self.merchantModel = list.sorted {
                            
                            if $0.UserContactData!.BusinessName != "" {
                                name1 = $0.UserContactData!.BusinessName!
                            } else {
                                name1 = $0.UserContactData!.FirstName!
                            }
                            
                            if $1.UserContactData!.BusinessName != "" {
                                name2 = $1.UserContactData!.BusinessName!
                            } else {
                                name2 = $1.UserContactData!.FirstName!
                            }
                            return name1.compare(name2) == .orderedDescending
                        }
                        
                        
                    }
                    
                default:
                    break
                }
                
                let text = "Near By Merchants".localized
                self.merchantCountglobalLabel.text = "\(text) \(self.merchantModel.count)"
                
                self.listTableView.reloadData()
                //   self.listViewContainer.isHidden = false
                self.addClustersToMap(list: self.merchantModel)
                
            }
            
            
        }
        else {
            self.showErrorAlert(errMessage: "No Record Found".localized)
        }
        
    }
    
}


extension MapPayToVC {
//This will be called when the user will click nearby and
func getOfficeAPI(lat: String, long: String){
    println_debug("call ok offices api ")
    
  
    
    let url = getUrl(urlStr: "", serverType: .nearByServicesUrl)
    
    let jsonDic:[String : Any]
    
     /// checkurl ==  "Township"{
            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 3000, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":selectedTownshipDetailStr,"cityName":"","businessName":"","country":"","divisionName":selectedLocationDetailStr,"line1":""],"paginationParams": ["offset":0,"limit":0],"types":[7,2,8,9,11], "phoneNumberNotToConsider":""] as [String : Any]
    
    
    let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url,methodType: kMethod_Post, contentType: kContentType_Json, inputVal: jsonDic, authStr: nil)
    
    print("Url Request--------\(jsonDic)")
    print("Url Request11--------\(urlRequest)")
    
    DispatchQueue.main.async {
        progressViewObj.showProgressView()
    }
    JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
        
        guard isSuccess else {
            DispatchQueue.main.async {
                progressViewObj.removeProgressView()
            }
            
            return
        }
        progressViewObj.removeProgressView()
        
        var cashArray = [NearByServicesNewModel]()
        DispatchQueue.main.async {
            cashArray = ParserAttributeClass.parseNearbyServiceJSONNew(anyValue: response as AnyObject) as [NearByServicesNewModel]
            progressViewObj.removeProgressView()
            
           // self.cashInListRecentFromServer = cashArray
            
            if cashArray.count > 0 {
                
                self.mapView.isHidden = false
                
                
                self.merchantModel.removeAll()
               self.merchantModelBackup.removeAll()
                
                for i in 0..<cashArray.count {
                    self.merchantModel.append(cashArray[i])
                    self.merchantModelBackup.append(cashArray[i])
                }
                
                
                self.initialTableView.delegate = self
                self.initialTableView.dataSource = self
                self.initialTableView.reloadData()
                
//                let nc = NotificationCenter.default
//
//                nc.post(name:Notification.Name(rawValue:"NearByAllDataNotification"),
//                        object: nil,
//                        userInfo: ["NearByData": self.arrAllList , "Latitude":lat , "Logtitude":long , "urlstring":self.checkurl])
//                nc.post(name:Notification.Name(rawValue:"NearByMerchantNotification"),
//                        object: nil,
//                        userInfo: ["NearByData": self.arrMerchantList , "Latitude":lat , "Logtitude":long , "urlstring":self.checkurl])
//                nc.post(name:Notification.Name(rawValue:"NearByAgentNotification"),
//                        object: nil,
//                        userInfo: ["NearByData": self.arrAgentsList  , "Latitude":lat , "Logtitude":long , "urlstring":self.checkurl])
//                nc.post(name:Notification.Name(rawValue:"NearByOfficeNotification"),
//                        object: nil,
//                        userInfo: ["NearByData": self.arrOfficeList , "Latitude":lat , "Logtitude":long , "urlstring":self.checkurl])
                
            }
            else
            {
               
                self.showErrorAlert(errMessage: "No Record Found".localized)
               // self.reloadListEmptyViewAfterSwitch()
                
            }
        }
    })
}
}
