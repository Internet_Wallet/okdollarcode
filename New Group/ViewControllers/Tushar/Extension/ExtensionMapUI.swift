//
//  ExtensionMapUI.swift
//  OK
//
//  Created by iMac on 8/31/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation


extension MapPayToVC{
    
    
    func setUI(){
      

        shopTableView.separatorStyle = .none
        self.crossButtonOutLet.isHidden = true
        //Setting UI for Intial View in which agent list is coming in initial
        initialTableView.isHidden = true
        initialTableView.separatorColor = .clear
        initialNearByView.layer.cornerRadius = 10.0
        initialNearByView.clipsToBounds = true
        initialNearByView.layer.borderWidth = 1.0
        initialNearByView.layer.borderColor = UIColor.white.cgColor
        initialFilterView.layer.cornerRadius = 10.0
        initialFilterView.clipsToBounds = true
        initialFilterView.layer.borderWidth = 1.0
        initialFilterView.layer.borderColor = UIColor.lightGray.cgColor
        
     
        shopDetailTopShareButton.layer.cornerRadius = 15
        shopDetailTopDownArrowButton.layer.cornerRadius = 15
        shopDetailTopShareButton.clipsToBounds = true
        shopDetailTopDownArrowButton.clipsToBounds = true
        infoBase.backgroundColor = blueColor
        
        
        height = CGFloat((self.view.frame.size.height)/2 + 100) - 50
        
        designView(animateView: listViewContainer, cornerRadius: 20.0, borderWidth: 2.0)
      
        
        shopDetailView.isHidden = true
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeUp.direction = .up
        self.listViewContainer.addGestureRecognizer(swipeUp)
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeDown.direction = .down
        self.listViewContainer.addGestureRecognizer(swipeDown)
        let tap = UITapGestureRecognizer(target: self, action: #selector(playWithView))
        self.dropDownView.addGestureRecognizer(tap)
        let tapOne = UITapGestureRecognizer(target: self, action: #selector(playWithTopBackGround))
        self.topBackGroundView.addGestureRecognizer(tapOne)
        topViewHeight.constant = 0
        animationViewHeight(top: CGFloat((self.view.frame.size.height)/2 + 100))
        
        mapView.isHidden = true
        listView.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        merchantModelBackup = merchantModel
        
        self.promotionInfoView.isHidden = true
        pulsingEffect = PulseManager.updateView()
        pulsingEffect?.startPulsing(distance: 0)
        
        self.map.delegate = self
        
        //prabu
        defaults.set(nil, forKey: "PTMsortBy")
        defaults.set(nil, forKey: "PTMfilterBy")
        defaults.set(nil, forKey: "PTMcategoryBy")
        defaults.set(nil, forKey: "PTMNearBy")
        
        listMerchantModel = ["",""]
        self.listTableView.tableFooterView = UIView.init()
        self.heightUpDownImage.isHighlighted = true
        
        self.map.showsUserLocation = false
        NotificationCenter.default.addObserver(self, selector: #selector(reloadViewData), name: NSNotification.Name(rawValue: "MapPayToLocation"), object: nil)
        infoButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        photoButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        descriptionButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        
    }
    
    
    
}
