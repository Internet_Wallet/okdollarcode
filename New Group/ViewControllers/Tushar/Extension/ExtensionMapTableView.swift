//
//  ExtensionMapTableView.swift
//  OK
//
//  Created by iMac on 8/30/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation


extension MapPayToVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
      //Shop TablewView is the detail view which will be show when you press agent list cell
        if tableView == shopTableView{
            return 1
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView == shopTableView{
            if section == 0{
                let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 120))
                headerView.backgroundColor = .white
                let headerViewBackGround = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 64))
                headerViewBackGround.backgroundColor = UIColor(red: 245.0/255.0, green: 198.0/255.0, blue: 0/255.0, alpha: 1.0)
                
                descriptionBase.frame = CGRect(x: 0, y: headerView.frame.origin.y + headerView.frame.size.height , width: headerView.frame.size.width/3 - 5, height: 5)
                infoButton.frame =  CGRect(x: 0, y: 65, width: headerView.frame.size.width/3 - 5, height: 40)
                infoBase.frame = CGRect(x: 0, y: infoButton.frame.origin.y + infoButton.frame.size.height , width: headerView.frame.size.width/3 - 5, height: 5)
                infoButton.setTitle("Info".localized, for: .normal)
                
                
                if checkIfComingFromOption == "info"{
                    infoButton.setTitleColor(.black, for: .normal)
                    photoButton.setTitleColor(.lightGray, for: .normal)
                    descriptionButton.setTitleColor(.lightGray, for: .normal)
                }
                
//                else if checkIfComingFromOption == "photo"{
//                    infoButton.setTitleColor(.lightGray, for: .normal)
//                    photoButton.setTitleColor(.black, for: .normal)
//                    descriptionButton.setTitleColor(.lightGray, for: .normal)
//                }else{
//                    infoButton.setTitleColor(.lightGray, for: .normal)
//                    photoButton.setTitleColor(.lightGray, for: .normal)
//                    descriptionButton.setTitleColor(.black, for: .normal)
//                }
                
                
                infoButton.addTarget(self, action: #selector(infoAction), for: .touchUpInside)
                
                photoButton.frame = CGRect(x: infoButton.frame.origin.x + infoButton.frame.size.width + 5, y: 65, width: headerView.frame.size.width/3 - 5, height: 40)
                photoBase.frame = CGRect(x: infoButton.frame.origin.x + infoButton.frame.size.width + 5, y: photoButton.frame.origin.y + photoButton.frame.size.height , width: headerView.frame.size.width/3 - 5, height: 5)
                photoButton.setTitle("Photo".localized, for: .normal)
                photoButton.addTarget(self, action: #selector(photoAction), for: .touchUpInside)
                descriptionButton.frame = CGRect(x: photoButton.frame.origin.x + photoButton.frame.size.width + 5, y: 65, width: headerView.frame.size.width/3 , height: 40)
                descriptionButton.setTitle("Direction".localized, for: .normal)
                descriptionButton.addTarget(self, action: #selector(descriptionAction), for: .touchUpInside)
                descriptionBase.frame = CGRect(x: photoButton.frame.origin.x + photoButton.frame.size.width + 5, y: descriptionButton.frame.origin.y + descriptionButton.frame.size.height , width: headerView.frame.size.width/3 - 5, height: 5)
                
                let detailsLabel = UILabel.init(frame: CGRect.init(x: headerView.frame.width/4 + 10, y: headerViewBackGround.center.y + 5 , width: 200, height: 25))
                detailsLabel.text = "Merchant Details".localized
                detailsLabel.textAlignment = .center
                detailsLabel.font = UIFont.init(name: appFont, size: 18)
               detailsLabel.textColor = blueColor
               detailsLabel.center = headerViewBackGround.center
                let mapShareButton = UIButton.init(frame: CGRect.init(x: shopTableView.frame.width - 50 , y: 15, width: 25, height: 25))
                headerButton.frame = CGRect(x: 20, y: 15, width: 25, height: 25)
                mapShareButton.backgroundColor = .clear
                headerButton.backgroundColor = .clear
                headerButton.setImage(UIImage(named: "blueCross.png"), for: .normal)
               mapShareButton.setImage(UIImage(named: "blueShare.png"), for: .normal)
                headerButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
                mapShareButton.addTarget(self, action: #selector(buttonActionMap), for: .touchUpInside)
               headerButton.layer.cornerRadius = 15
                mapShareButton.layer.cornerRadius = 15
                headerButton.clipsToBounds = true
               mapShareButton.clipsToBounds = true
    
                headerViewBackGround.addSubview(detailsLabel)
                headerView.addSubview(headerViewBackGround)
                headerView.addSubview(infoButton)
                headerView.addSubview(photoButton)
                headerView.addSubview(descriptionButton)
                headerView.addSubview(photoBase)
                headerView.addSubview(infoBase)
                headerView.addSubview(descriptionBase)
                headerView.addSubview(mapShareButton)
                headerView.addSubview(headerButton)
                headerView.bringSubviewToFront(headerButton)
                headerView.bringSubviewToFront(mapShareButton)
                return headerView
            }
    
        }
        return UIView()
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == shopTableView{
           if section == 0{
                return 120
            }else{
                return 0
            }
        }else{
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == shopTableView{
            return 3
        }else{
            return merchantModel.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == shopTableView{
            if indexPath.section == 0 {
                if checkIfComingFromOption == "info"{
                    
                    if indexPath.row == 0{
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserInfoMapCell") as? UserInfoMapCell else{
                            return UITableViewCell()
                        }
                        
                        cell.selectionStyle = .none
                        if isfavoritePromotion {
                            
                            let origImage = UIImage(named: "act_favorite")
                            let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
                            cell.favouriteButtonOutlet.setImage(tintedImage, for: .normal)
                            cell.favouriteButtonOutlet.tintColor = blueColor
                            
                            
                        }else{
                            let origImage = UIImage(named: "favorite")
                            let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
                            cell.favouriteButtonOutlet.setImage(tintedImage, for: .normal)
                            cell.favouriteButtonOutlet.tintColor = blueColor
                        }
                        
                        if let value = selectedPromotion?.shop_Name{
                            if value == "" {
                                cell.companyNameLabel.text = "Not available".localized
                            }else{
                                cell.companyNameLabel.text = value
                            }
                        }
                        
                        if let name = selectedPromotion?.shop_AgentName{
                            if name == ""{
                                cell.nameLabel.text = "Not available".localized
                            }else{
                                cell.nameLabel.text = name
                            }
                        }
                        
                        cell.favouriteButtonOutlet.addTarget(self, action: #selector(favoriteBtnAction), for: UIControl.Event.touchUpInside)
                        
                        cell.categoryLabel.text = "Category".localized
                        cell.subCategoryLable.text = "Sub-Category".localized
                        
                        let mobileNumber = getPlusWithDestinationNum(selectedPromotion?.shop_Phonenumber ?? "", withCountryCode: "+95")
                        if mobileNumber.length > 15 {
                            cell.phoneNumberLabel.text =  "Not available".localized
                            cell.phoneView.isUserInteractionEnabled = false
                            // cell.callButton.isUserInteractionEnabled = false
                        } else {
                            
                            //Made this check to avoid crash in future
                            if mobileNumber[0] == "+" &&  mobileNumber[1] == "9" && mobileNumber[2] == "5"{
                                cell.phoneNumberLabel.text = mobileNumber.replacingOccurrences(of: "+95", with: "(+95) 0")
                            }else{
                                cell.phoneNumberLabel.text = mobileNumber
                            }
                            
                            
                            //  cell.phoneNumberLabel.text = mobileNumber
                            
                            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
                            let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.handleTapCall(_:)))
                            cell.callButton.addGestureRecognizer(tap2)
                            cell.callButton.tag = indexPath.row
                            cell.callButton.isUserInteractionEnabled = true
                            cell.phoneView.addGestureRecognizer(tap1)
                            cell.phoneView.tag = indexPath.row
                            cell.phoneView.isUserInteractionEnabled = true
                            // cell.callButton.addTarget(self, action: #selector(callBtnAction), for: UIControl.Event.touchUpInside)
                            // cell.callButton.isUserInteractionEnabled = true
                        }
                        
                        
                        if(appDelegate.currentLanguage == "my")
                        {
                            if (selectedPromotion?.shop_Category?.categoryNameMY) != "" {
                                cell.categoryDetailLabel.text = selectedPromotion?.shop_Category?.categoryNameMY ?? "Not available".localized
                            } else {
                                if (categorylistArr.count > 0)  {
                                    cell.categoryDetailLabel.text = self.categorylistArr.safeValueForKey("MerchantCatName") as? String
                                } else {
                                    cell.categoryDetailLabel.text = "Not available".localized
                                }
                            }
                            
                            if ((selectedPromotion?.shop_Category?.businessType?.businessType_NameMY) != "") {
                                cell.subCategoryDetailLabel.text = selectedPromotion?.shop_Category?.businessType?.businessType_NameMY ?? "Not available".localized
                            } else {
                                cell.subCategoryDetailLabel.text = "Not available".localized
                            }
                        } else {
                            if (selectedPromotion?.shop_Category?.categoryNameEN) != "" {
                                cell.categoryDetailLabel.text = selectedPromotion?.shop_Category?.categoryNameEN ?? "Not available".localized
                            } else {
                                if (categorylistArr.count > 0)  {
                                    cell.categoryDetailLabel.text = self.categorylistArr.safeValueForKey("MerchantCatName") as? String
                                } else {
                                    cell.categoryDetailLabel.text = "Not available".localized
                                }
                            }
                            
                            if (selectedPromotion?.shop_Category?.businessType?.businessType_NameEN) != "" {
                                cell.subCategoryDetailLabel.text = selectedPromotion?.shop_Category?.businessType?.businessType_NameEN ?? "Not available".localized
                            } else {
                                cell.subCategoryDetailLabel.text = "Not available".localized
                            }
                        }
                        return cell
                        
                    }else if indexPath.row == 1{
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ShopImageViewCell") as? ShopImageViewCell else{
                            return UITableViewCell()
                        }
                        cell.selectionStyle = .none
                        return cell
                        
                    }else{
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DirectionDetailCell") as? DirectionDetailCell else{
                            return UITableViewCell()
                        }
                        cell.selectionStyle = .none
                        cell.shopLabel.text = "Opening Time".localized
                        if let openTime = selectedPromotion?.shop_OpenTime , let closeTime = selectedPromotion?.shop_CloseTime {
                            if openTime.count > 0 && closeTime.count > 0 {
                                if openTime == "00:00:00" && closeTime == "00:00:00" {
                                    cell.shopNameLabel.text = "Open 24/7".localized
                                } else {
                                    cell.shopNameLabel.text = "\(self.getTime(timeStr: openTime)) - \(self.getTime(timeStr: closeTime))"
                                }
                            } else {
                                cell.shopNameLabel.text = "Open 24/7".localized
                               // cell.mapButton.isUserInteractionEnabled = false
                            }
                        }
                        
                        if let shopDistance = selectedNearByServicesNew?.distanceInKm {
                            cell.time.text = String(format: "%.2f Km", shopDistance).replacingOccurrences(of: ".00", with: "")
                        }else{
                            cell.time.text = "0 km"
                        }
                        
                        cell.mapButton.addTarget(self, action: #selector(navigateToMapDetailViewAction), for: UIControl.Event.touchUpInside)
                        cell.directionButtonOutlet.addTarget(self, action: #selector(directionBtnAction), for: UIControl.Event.touchUpInside)
                        
                        
                        
                        if let address = selectedPromotion?.shop_Address?.addressLine1{
                            if address.isEmpty{
                                cell.address.isHidden = true
                                cell.addressLabelHeight.constant = 0
                            }else{
                                cell.addressLabelHeight.constant = 50
                                cell.address.isHidden = false
                                cell.address.text = "Address".localized
                                
                                cell.shopAddressLabel.text =   "\(selectedPromotion?.shop_Address?.addressLine2 ?? "") , \(selectedPromotion?.shop_Address?.addressLine1 ?? "")"
                            }
                            
                        }else{
                            cell.address.isHidden = true
                            cell.addressLabelHeight.constant = 0
                        }
                        
                        
                        
                        return cell
                    }
                    
                    
                }else if checkIfComingFromOption == "photo"{
                    
                    if indexPath.row == 2{
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserInfoMapCell") as? UserInfoMapCell else{
                            return UITableViewCell()
                        }
                        
                        cell.selectionStyle = .none
                        if isfavoritePromotion {
                            let origImage = UIImage(named: "act_favorite")
                            let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
                            cell.favouriteButtonOutlet.setImage(tintedImage, for: .normal)
                            cell.favouriteButtonOutlet.tintColor = blueColor
                        }else{
                            let origImage = UIImage(named: "favorite")
                            let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
                            cell.favouriteButtonOutlet.setImage(tintedImage, for: .normal)
                            cell.favouriteButtonOutlet.tintColor = blueColor
                        }
                        
                        if let value = selectedPromotion?.shop_Name{
                            if value == "" {
                                cell.companyNameLabel.text = "Not available".localized
                            }else{
                                cell.companyNameLabel.text = value
                            }
                        }
                        
                        if let name = selectedPromotion?.shop_AgentName{
                            if name == ""{
                                cell.nameLabel.text = "Not available".localized
                            }else{
                                cell.nameLabel.text = name
                            }
                        }
                        
                        cell.favouriteButtonOutlet.addTarget(self, action: #selector(favoriteBtnAction), for: UIControl.Event.touchUpInside)
                        
                        cell.categoryLabel.text = "Category".localized
                        cell.subCategoryLable.text = "Sub-Category".localized
                        
                        let mobileNumber = getPlusWithDestinationNum(selectedPromotion?.shop_Phonenumber ?? "", withCountryCode: "+95")
                        if mobileNumber.length > 15 {
                            cell.phoneNumberLabel.text =  "Not available".localized
                            cell.phoneView.isUserInteractionEnabled = false
                            // cell.callButton.isUserInteractionEnabled = false
                        } else {
                            
                            //Made this check to avoid crash in future
                            if mobileNumber[0] == "+" &&  mobileNumber[1] == "9" && mobileNumber[2] == "5"{
                                cell.phoneNumberLabel.text = mobileNumber.replacingOccurrences(of: "+95", with: "(+95) 0")
                            }else{
                                cell.phoneNumberLabel.text = mobileNumber
                            }
                            
                            
                            //  cell.phoneNumberLabel.text = mobileNumber
                            
                            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
                            let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.handleTapCall(_:)))
                            cell.callButton.addGestureRecognizer(tap2)
                            cell.callButton.tag = indexPath.row
                            cell.callButton.isUserInteractionEnabled = true
                            cell.phoneView.addGestureRecognizer(tap1)
                            cell.phoneView.tag = indexPath.row
                            cell.phoneView.isUserInteractionEnabled = true
                            // cell.callButton.addTarget(self, action: #selector(callBtnAction), for: UIControl.Event.touchUpInside)
                            // cell.callButton.isUserInteractionEnabled = true
                        }
                        
                        
                        if(appDelegate.currentLanguage == "my")
                        {
                            if (selectedPromotion?.shop_Category?.categoryNameMY) != "" {
                                cell.categoryDetailLabel.text = selectedPromotion?.shop_Category?.categoryNameMY ?? "Not available".localized
                            } else {
                                if (categorylistArr.count > 0)  {
                                    cell.categoryDetailLabel.text = self.categorylistArr.safeValueForKey("MerchantCatName") as? String
                                } else {
                                    cell.categoryDetailLabel.text = "Not available".localized
                                }
                            }
                            
                            if ((selectedPromotion?.shop_Category?.businessType?.businessType_NameMY) != "") {
                                cell.subCategoryDetailLabel.text = selectedPromotion?.shop_Category?.businessType?.businessType_NameMY ?? "Not available".localized
                            } else {
                                cell.subCategoryDetailLabel.text = "Not available".localized
                            }
                        } else {
                            if (selectedPromotion?.shop_Category?.categoryNameEN) != "" {
                                cell.categoryDetailLabel.text = selectedPromotion?.shop_Category?.categoryNameEN ?? "Not available".localized
                            } else {
                                if (categorylistArr.count > 0)  {
                                    cell.categoryDetailLabel.text = self.categorylistArr.safeValueForKey("MerchantCatName") as? String
                                } else {
                                    cell.categoryDetailLabel.text = "Not available".localized
                                }
                            }
                            
                            if (selectedPromotion?.shop_Category?.businessType?.businessType_NameEN) != "" {
                                cell.subCategoryDetailLabel.text = selectedPromotion?.shop_Category?.businessType?.businessType_NameEN ?? "Not available".localized
                            } else {
                                cell.subCategoryDetailLabel.text = "Not available".localized
                            }
                        }
                        return cell
                        
                    }else if indexPath.row == 0{
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ShopImageViewCell") as? ShopImageViewCell else{
                            return UITableViewCell()
                        }
                        cell.selectionStyle = .none
                        return cell
                        
                    }else{
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DirectionDetailCell") as? DirectionDetailCell else{
                            return UITableViewCell()
                        }
                        cell.selectionStyle = .none
                        cell.shopLabel.text = "Opening Time".localized
                        if let openTime = selectedPromotion?.shop_OpenTime , let closeTime = selectedPromotion?.shop_CloseTime {
                            if openTime.count > 0 && closeTime.count > 0 {
                                if openTime == "00:00:00" && closeTime == "00:00:00" {
                                    cell.shopNameLabel.text = "Open 24/7".localized
                                } else {
                                    cell.shopNameLabel.text = "\(self.getTime(timeStr: openTime)) - \(self.getTime(timeStr: closeTime))"
                                }
                            } else {
                                cell.shopNameLabel.text = "Open 24/7".localized
                               // cell.mapButton.isUserInteractionEnabled = false
                            }
                        }
                        
                        if let shopDistance = selectedNearByServicesNew?.distanceInKm {
                            cell.time.text = String(format: "%.2f Km", shopDistance).replacingOccurrences(of: ".00", with: "")
                        }else{
                            cell.time.text = "0 km"
                        }
                        
                        cell.mapButton.addTarget(self, action: #selector(navigateToMapDetailViewAction), for: UIControl.Event.touchUpInside)
                        cell.directionButtonOutlet.addTarget(self, action: #selector(directionBtnAction), for: UIControl.Event.touchUpInside)
                        
                        
                        
                        if let address = selectedPromotion?.shop_Address?.addressLine1{
                            if address.isEmpty{
                                cell.address.isHidden = true
                                cell.addressLabelHeight.constant = 0
                            }else{
                                cell.addressLabelHeight.constant = 50
                                cell.address.isHidden = false
                                cell.address.text = "Address".localized
                                
                                cell.shopAddressLabel.text =   "\(selectedPromotion?.shop_Address?.addressLine2 ?? "") , \(selectedPromotion?.shop_Address?.addressLine1 ?? "")"
                            }
                            
                        }else{
                            cell.address.isHidden = true
                            cell.addressLabelHeight.constant = 0
                        }
                        
                        
                        
                        return cell
                    }
                    
                    
                }else{
                    
                    if indexPath.row == 1{
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserInfoMapCell") as? UserInfoMapCell else{
                            return UITableViewCell()
                        }
                        
                        cell.selectionStyle = .none
                        if isfavoritePromotion {
                            let origImage = UIImage(named: "act_favorite")
                            let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
                            cell.favouriteButtonOutlet.setImage(tintedImage, for: .normal)
                            cell.favouriteButtonOutlet.tintColor = blueColor
                        }else{
                            let origImage = UIImage(named: "favorite")
                            let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
                            cell.favouriteButtonOutlet.setImage(tintedImage, for: .normal)
                            cell.favouriteButtonOutlet.tintColor = blueColor
                        }
                        
                        if let value = selectedPromotion?.shop_Name{
                            if value == "" {
                                cell.companyNameLabel.text = "Not available".localized
                            }else{
                                cell.companyNameLabel.text = value
                            }
                        }
                        
                        if let name = selectedPromotion?.shop_AgentName{
                            if name == ""{
                                cell.nameLabel.text = "Not available".localized
                            }else{
                                cell.nameLabel.text = name
                            }
                        }
                        
                        cell.favouriteButtonOutlet.addTarget(self, action: #selector(favoriteBtnAction), for: UIControl.Event.touchUpInside)
                        
                        cell.categoryLabel.text = "Category".localized
                        cell.subCategoryLable.text = "Sub-Category".localized
                        
                        let mobileNumber = getPlusWithDestinationNum(selectedPromotion?.shop_Phonenumber ?? "", withCountryCode: "+95")
                        if mobileNumber.length > 15 {
                            cell.phoneNumberLabel.text =  "Not available".localized
                            cell.phoneView.isUserInteractionEnabled = false
                            // cell.callButton.isUserInteractionEnabled = false
                        } else {
                            
                            //Made this check to avoid crash in future
                            if mobileNumber[0] == "+" &&  mobileNumber[1] == "9" && mobileNumber[2] == "5"{
                                cell.phoneNumberLabel.text = mobileNumber.replacingOccurrences(of: "+95", with: "(+95) 0")
                            }else{
                                cell.phoneNumberLabel.text = mobileNumber
                            }
                            
                            
                            //  cell.phoneNumberLabel.text = mobileNumber
                            
                            let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
                            let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.handleTapCall(_:)))
                            cell.callButton.addGestureRecognizer(tap2)
                            cell.callButton.tag = indexPath.row
                            cell.callButton.isUserInteractionEnabled = true
                            cell.phoneView.addGestureRecognizer(tap1)
                            cell.phoneView.tag = indexPath.row
                            cell.phoneView.isUserInteractionEnabled = true
                            // cell.callButton.addTarget(self, action: #selector(callBtnAction), for: UIControl.Event.touchUpInside)
                            // cell.callButton.isUserInteractionEnabled = true
                        }
                        
                        
                        if(appDelegate.currentLanguage == "my")
                        {
                            if (selectedPromotion?.shop_Category?.categoryNameMY) != "" {
                                cell.categoryDetailLabel.text = selectedPromotion?.shop_Category?.categoryNameMY ?? "Not available".localized
                            } else {
                                if (categorylistArr.count > 0)  {
                                    cell.categoryDetailLabel.text = self.categorylistArr.safeValueForKey("MerchantCatName") as? String
                                } else {
                                    cell.categoryDetailLabel.text = "Not available".localized
                                }
                            }
                            
                            if ((selectedPromotion?.shop_Category?.businessType?.businessType_NameMY) != "") {
                                cell.subCategoryDetailLabel.text = selectedPromotion?.shop_Category?.businessType?.businessType_NameMY ?? "Not available".localized
                            } else {
                                cell.subCategoryDetailLabel.text = "Not available".localized
                            }
                        } else {
                            if (selectedPromotion?.shop_Category?.categoryNameEN) != "" {
                                cell.categoryDetailLabel.text = selectedPromotion?.shop_Category?.categoryNameEN ?? "Not available".localized
                            } else {
                                if (categorylistArr.count > 0)  {
                                    cell.categoryDetailLabel.text = self.categorylistArr.safeValueForKey("MerchantCatName") as? String
                                } else {
                                    cell.categoryDetailLabel.text = "Not available".localized
                                }
                            }
                            
                            if (selectedPromotion?.shop_Category?.businessType?.businessType_NameEN) != "" {
                                cell.subCategoryDetailLabel.text = selectedPromotion?.shop_Category?.businessType?.businessType_NameEN ?? "Not available".localized
                            } else {
                                cell.subCategoryDetailLabel.text = "Not available".localized
                            }
                        }
                        return cell
                        
                    }else if indexPath.row == 2{
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ShopImageViewCell") as? ShopImageViewCell else{
                            return UITableViewCell()
                        }
                        cell.selectionStyle = .none
                        return cell
                        
                    }else{
                        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DirectionDetailCell") as? DirectionDetailCell else{
                            return UITableViewCell()
                        }
                        cell.selectionStyle = .none
                        cell.shopLabel.text = "Opening Time".localized
                        if let openTime = selectedPromotion?.shop_OpenTime , let closeTime = selectedPromotion?.shop_CloseTime {
                            if openTime.count > 0 && closeTime.count > 0 {
                                if openTime == "00:00:00" && closeTime == "00:00:00" {
                                    cell.shopNameLabel.text = "Open 24/7".localized
                                } else {
                                    cell.shopNameLabel.text = "\(self.getTime(timeStr: openTime)) - \(self.getTime(timeStr: closeTime))"
                                }
                            } else {
                                cell.shopNameLabel.text = "Open 24/7".localized
                              //  cell.mapButton.isUserInteractionEnabled = false
                            }
                        }
                        
                        if let shopDistance = selectedNearByServicesNew?.distanceInKm {
                            cell.time.text = String(format: "%.2f Km", shopDistance).replacingOccurrences(of: ".00", with: "")
                        }else{
                            cell.time.text = "0 km"
                        }
                        
                        cell.mapButton.addTarget(self, action: #selector(navigateToMapDetailViewAction), for: UIControl.Event.touchUpInside)
                        cell.directionButtonOutlet.addTarget(self, action: #selector(directionBtnAction), for: UIControl.Event.touchUpInside)
                        
                        
                        
                        if let address = selectedPromotion?.shop_Address?.addressLine1{
                            if address.isEmpty{
                                cell.address.isHidden = true
                                cell.addressLabelHeight.constant = 0
                            }else{
                                cell.addressLabelHeight.constant = 50
                                cell.address.isHidden = false
                                cell.address.text = "Address".localized
                                
                                cell.shopAddressLabel.text =   "\(selectedPromotion?.shop_Address?.addressLine2 ?? "") , \(selectedPromotion?.shop_Address?.addressLine1 ?? "")"
                            }
                            
                        }else{
                            cell.address.isHidden = true
                            cell.addressLabelHeight.constant = 0
                        }
                        
                        
                        
                        return cell
                    }
                    
                    
                }
            }
        }else{
            
            
            let identifier = "maplistcellidentifier"
            var listCell: MapListCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? MapListCell
            if listCell == nil {
                tableView.register (UINib(nibName: "MapListCell", bundle: nil), forCellReuseIdentifier: identifier)
                listCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? MapListCell
            }
             listCell.selectionStyle = .none
            let promotion = self.merchantModel[indexPath.row]
            listCell.callBtn.isHidden = true
            listCell.distanceLblKm.isHidden = false
            //listCell.distanceLblMile.isHidden = true
            
          //  listCell.callBtn.addTarget(self, action: #selector(callBtnAction(_:)), for: UIControl.Event.touchUpInside)
            //listCell.photoImgView.image = nil
            if let imageStr = promotion.UserContactData?.ImageUrl, let imageUrl = URL(string: imageStr) {
                listCell.photoImgView.isHidden = false
                listCell.photoImgView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "cicoCamera"))
            }else{
                listCell.photoImgView.image = UIImage(named: "btc.png")
            }
            
            listCell.nameLbl.text = promotion.UserContactData?.BusinessName
            
            
            let agentCodenew  = getPlusWithDestinationNum((promotion.UserContactData!.PhoneNumber)!, withCountryCode: "+95")
            listCell.phonenumberLbl.text = agentCodenew
            
            //Made this check to avoid crash in future
            if agentCodenew[0] == "+" &&  agentCodenew[1] == "9" && agentCodenew[2] == "5"{
               listCell.phonenumberLbl.text = agentCodenew.replacingOccurrences(of: "+95", with: "(+95) 0")
            }else{
                listCell.phonenumberLbl.text = agentCodenew
            }
            
            
            
            if(promotion.UserContactData?.BusinessName!.count == 0)
            {
                listCell.nameLbl.text = promotion.UserContactData?.FirstName
            }
            
            if let openTime = promotion.UserContactData!.OfficeHourFrom , let closeTime = promotion.UserContactData!.OfficeHourTo {
                
                if openTime.count > 0 && closeTime.count > 0 {
                    listCell.timeImgView.isHidden = false
                    listCell.timeLbl.isHidden = false
                    listCell.timeLblHeightConstraint.constant = 20
                    //listCell.timeImgView.image = UIImage.init(named: "time")
                    listCell.timeLbl.text = "\(listCell.getTime(timeStr: openTime)) - \(listCell.getTime(timeStr: closeTime))"
                }else {
                    listCell.timeImgView.isHidden = true
                    listCell.timeLbl.isHidden = true
                    listCell.timeLblHeightConstraint.constant = 0
                }
                // }
            }
            listCell.locationImgView.image = UIImage.init(named: "location.png")
            if let addressnew = promotion.UserContactData?.Address {
                if addressnew.count > 0 {
                    
                    listCell.locationLbl.text = "\(addressnew)"
                    
                    
                    // listCell.locationLbl.text = "\(addressnew),\( promotion.UserContactData!.Township!),\( promotion.UserContactData!.Division!)"
                }else {
                    listCell.locationLbl.text = "\(promotion.UserContactData!.Township!),\(promotion.UserContactData!.Division!)"
                }
            }
            
            //CashIn And CashOut
            
            if let cashinamount = promotion.UserContactData?.CashInLimit , let cashoutamount = promotion.UserContactData?.CashOutLimit {
                
                if cashinamount > 0 {
                    let phone =  self.getNumberFormat("\(promotion.UserContactData!.CashInLimit!)").replacingOccurrences(of: ".00", with: "")
                    listCell.cashInLbl.text = "Maximum Cash In Amount".localized + " : \(phone)" + " MMK"
                    listCell.cashInLblHeightConstraint.constant = 20
                } else {
                    listCell.cashInLblHeightConstraint.constant = 0
                }
                
                if cashoutamount > 0 {
                    let phonenew =  self.getNumberFormat("\(promotion.UserContactData!.CashOutLimit!)").replacingOccurrences(of: ".00", with: "")
                    listCell.cashOutLbl.text = "Maximum Cash Out Amount".localized + " : \(phonenew)" + " MMK"
                    listCell.cashOutLblHeightConstraint.constant = 20
                } else{
                    listCell.cashOutLblHeightConstraint.constant = 0
                }
            }
            
            // listCell.remarksLbl.text = ""
            listCell.callBtn.setImage(UIImage.init(named: "call.png"), for: UIControl.State.normal)
            listCell.distanceLblKm.text = "(0 Km)"
            if let shopDistance = promotion.distanceInKm {
                listCell.distanceLblKm.text = String(format: "(%.2f Km)", shopDistance).replacingOccurrences(of: ".00", with: "")
            }
            
            listCell.durationLbl.text = listCell.durationFromDistance(distance: promotion.distanceInKm) as String
            
            return listCell
        }
        
        return UITableViewCell()
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == shopTableView{
            var rowHeight: CGFloat = 0.0
            if checkIfComingFromOption == "info"{
                if indexPath.row == 0 {
                    rowHeight = 349
                }else if indexPath.row == 1{
                    rowHeight = 166
                }else{
                    if let _ = selectedPromotion?.shop_NewAddress{
                        rowHeight = 244
                    }else{
                        rowHeight = 194
                    }
                }
                
            }else if checkIfComingFromOption == "photo"{
                if indexPath.row == 0 {
                    rowHeight = 166
                }else if indexPath.row == 1{
                    if let _ = selectedPromotion?.shop_NewAddress{
                        rowHeight = 244
                    }else{
                        rowHeight = 194
                    }
                }else  {
                    rowHeight = 349
                    
                }
                
            }else{
                if indexPath.row == 0 {
                    if let _ = selectedPromotion?.shop_NewAddress{
                        rowHeight = 244
                    }else{
                        rowHeight = 194
                    }
                }else if indexPath.row == 1{
                    rowHeight = 349
                }else  {
                    rowHeight = 166
                    
                }
                
                
            }
            
            return rowHeight
        }else{
            tableView.estimatedRowHeight = 250
            tableView.rowHeight = UITableView.automaticDimension
            return tableView.rowHeight
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == shopTableView{
        }else{
            if let model = self.merchantModel[safe: indexPath.row] {
                
                selectedNearByServicesNew = model
                if let distance =  model.distanceInKm {
                    let roundValue = distance
                    if roundValue > 1000 {
                        let stringDistance = String.init(format: "%.2f Km", roundValue/1000)
                        self.distance = stringDistance
                    } else {
                        let stringDistance = String.init(format: "%.2f m", roundValue)
                        self.distance = stringDistance
                    }
                }
            }
            
            //This method will give u the detail of particular agent on shopTableView
            callPromotionDetails()
        }
    }
}
