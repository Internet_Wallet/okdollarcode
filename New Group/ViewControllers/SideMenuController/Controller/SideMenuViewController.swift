//
//  SideMenuViewController.swift
//  OK
//
//  Created by Ashish Kr Singh on 25/08/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import RAMAnimatedTabBarController

class SideMenuViewController: OKBaseController {

    @IBOutlet var sideMenuTable: UITableView!
    
    var sideLabelArr = [String]()
    var sideImgsArr  = [UIImage]()

  fileprivate  var sideMenuHeaderV = SideMenuHeaderView.updateViewResources(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 255))
    
    var collectView = UIView()

    fileprivate let cellId = "SideMenuTableViewCell"

    
    override func viewDidLoad() {
        super.viewDidLoad()
        //UserModel.shared.accountType = "1"
        let tap = UITapGestureRecognizer.init()
        view.addGestureRecognizer(tap)
        
        tap.cancelsTouchesInView = false

        
    sideLabelArr = ["Change Language",
                    "My Profile",
                    "Verify Payment & My QR Code",
                    "Ticket Gallery",
                    "Recieved Money",
                    "Favorites List",
                    "Report",
                    "Recents",
                    "Add Your Vehicle",
                    "settings",
                    "Share OK $ App",
                    "Help & Support"]
    
    sideImgsArr  =  [#imageLiteral(resourceName: "change_language"),
                     #imageLiteral(resourceName: "my_profile"),
                     #imageLiteral(resourceName: "verify_payment"),
                     #imageLiteral(resourceName: "ticketg_gallery"),
                     #imageLiteral(resourceName: "received_money"),
                     #imageLiteral(resourceName: "favorite_list"),
                     #imageLiteral(resourceName: "report"),
                     #imageLiteral(resourceName: "recents"),
                     #imageLiteral(resourceName: "add_car"),
                     #imageLiteral(resourceName: "setting"),
                     #imageLiteral(resourceName: "share_ok"),
                     #imageLiteral(resourceName: "help")]
        self.sideMenuHeaderV.layoutIfNeeded()
        sideMenuTable.tableHeaderView = sideMenuHeaderV
        
        //Tap gesture
        //Handled Attach file selection
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.moveToMyProfileAction(sender:)))
        sideMenuHeaderV.addGestureRecognizer(gesture)
        
    }
    
    @objc func moveToMyProfileAction(sender : UITapGestureRecognizer) {
        let story = UIStoryboard.init(name: "MyProfile", bundle: nil)
        
        if(UserModel.shared.accountType == "0")
        {
            let vc = story.instantiateViewController(withIdentifier: "UpdateProfile") as? UpdateProfile
            let navController = UpdateProfileNavController.init(rootViewController: vc!)
        
            self.present(navController, animated: true, completion: nil)
        }
        else
        {
            let vc = story.instantiateViewController(withIdentifier: "PersonalAccountVC") as? PersonalAccountVC
            let navController = UpdateProfileNavController.init(rootViewController: vc!)
            
            self.present(navController, animated: true, completion: nil)
            //let personalDetailVC = UIStoryboard(name: "MyProfile", bundle: Bundle.main).instantiateViewController(withIdentifier: "PersonalAccountVC")
            //self.navigationController?.pushViewController(personalDetailVC, animated: true)
        }
    }
    
   
    func switchFromSideMenu(index : Int) {
        
    }
}

//Mark: TableViewDataSources
extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SideMenuTableViewCell
        let imgs  = sideImgsArr[indexPath.row]
        let name  = sideLabelArr[indexPath.row]
        cell.wrapData(img: imgs, name: name)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideLabelArr.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.moveToDashboardAction(index: indexPath.row)
    }
    
    func moveToDashboardAction(index: Int) {
        
        switch index {
            
        case 1:
            print("call my profile function")
/*
            let story = UIStoryboard.init(name: "MyProfile", bundle: nil)
            
            let vc = story.instantiateViewController(withIdentifier: "UpdateProfile") as? UpdateProfile
            let navController = UpdateProfileNavController.init(rootViewController: vc!)
            
            self.present(navController, animated: true, completion: nil)
            */
            let story = UIStoryboard.init(name: "MyProfile", bundle: nil)
            
            if(UserModel.shared.accountType == "0")
            {
                let vc = story.instantiateViewController(withIdentifier: "UpdateProfile") as? UpdateProfile
                let navController = UpdateProfileNavController.init(rootViewController: vc!)
                
                self.present(navController, animated: true, completion: nil)
            }
            else
            {
                let vc = story.instantiateViewController(withIdentifier: "PersonalAccountVC") as? PersonalAccountVC
                let navController = UpdateProfileNavController.init(rootViewController: vc!)
                
                self.present(navController, animated: true, completion: nil)
            }
            break

        
        case 5:
            let vc = UIStoryboard.init(name: "Favorite", bundle: Bundle.main).instantiateViewController(withIdentifier: "favoriteNav")
            self.present(vc, animated: true, completion: nil)
            
        case 7:
            
            let viewController = UIStoryboard(name: "Recent", bundle: Bundle.main).instantiateViewController(withIdentifier: "recentRoot")
            self.present(viewController, animated: true, completion: nil)
            break
            
        case 9:
            
            let payment = PaymentAuthorisation()
            payment.authenticate()
            payment.delegate = self
            break
            
        case 10:
            
            let textToShare: String = "Share OK$ application to your family"
            let link: String = "https://itunes.apple.com/us/app/ok-$/id1067828611?ls=1&mt=8"
            
            let url:NSURL = NSURL.init(string: link)!
            var activityItems = [Any]()
            activityItems.append(textToShare)
            activityItems.append(url)
            
            let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
            activityViewController.setValue("Hey this is an awsome app.You also download and enjoy the features", forKey: "subject")
            activityViewController.excludedActivityTypes = [.assignToContact, .print]
            self.present(activityViewController, animated: true, completion: nil)
            
            break
        case 11:
            
            let viewController = UIStoryboard(name: "HelpSupport", bundle: Bundle.main).instantiateViewController(withIdentifier: "HelpNavigation")
            self.present(viewController, animated: true, completion: nil)
            break
            
        default:
            break
        }
    }

}

extension SideMenuViewController: PaymentValidationDelegate{
    func paymentAuthorisationSuccess() {
        print("Authorised")
         
        let viewController = UIStoryboard(name: "Setting", bundle: Bundle.main).instantiateViewController(withIdentifier: "myid")
        self.present(viewController, animated: true, completion: nil)
    }
}
