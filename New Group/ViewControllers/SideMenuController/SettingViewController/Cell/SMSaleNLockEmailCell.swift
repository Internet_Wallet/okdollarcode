//
//  SMSaleNLockEmailCell.swift
//  OK
//
//  Created by OK$ on 6/27/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class SMSaleNLockEmailCell: UITableViewCell {

    @IBOutlet var txtTitle: SkyFloatingLabelTextField!
    @IBOutlet var imgIcon: UIImageView!
    @IBOutlet weak var plusMinusIcon: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
