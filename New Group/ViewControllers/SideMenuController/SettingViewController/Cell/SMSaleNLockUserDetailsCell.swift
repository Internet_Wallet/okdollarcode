//
//  SMSaleNLockUserDetailsCell.swift
//  OK
//
//  Created by gauri OK$ on 6/1/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class SMSaleNLockUserDetailsCell: UITableViewCell {

    @IBOutlet var userImg: UIImageView!
    @IBOutlet weak var btnStatusActiveInActive: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        userImg.layer.cornerRadius = userImg.frame.width / 2
        userImg?.layer.masksToBounds = true
        userImg.layer.borderColor = UIColor.lightGray.cgColor
        userImg.layer.borderWidth = 1.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func wrapData(dataModel: SaleNLockList,indexTag: Int){
        if dataModel.status == 0 {
            btnStatusActiveInActive.isHidden = true
        } else {
            btnStatusActiveInActive.isHidden = false
            if(dataModel.isActive?.caseInsensitiveCompare("Active") == .orderedSame){
                btnStatusActiveInActive.setBackgroundImage( #imageLiteral(resourceName: "SNL_Active"), for: .normal)
            } else {
                btnStatusActiveInActive.setBackgroundImage( #imageLiteral(resourceName: "SNL_InActive"), for: .normal)
            }
        }
        
        //profilePic
        if let urlStr = dataModel.profilePic {
            if urlStr.contains(find: " ") {
                if let imagePEUrl = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                    let imageURL = URL(string: imagePEUrl)
                    userImg.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "userTRN"))
                    
                } else {
                    let imageURL = URL(string: urlStr)
                    userImg.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "userTRN"))
                }
            } else {
                let imageURL = URL(string: urlStr)
                userImg.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "userTRN"))
            }
        }
    }
    
}
