

import UIKit
import SkyFloatingLabelTextField

class SMTopUpMultiCell2: UITableViewCell {
    
    var parentVC:SMAddFiveNumberViewController! = nil
    var indexPath:IndexPath! = nil
    let validObj = PayToValidations()
    var holdMobileNumberStr:String = ""
    
    @IBOutlet weak var tfConfirmMb: SkyFloatingLabelTextField!{
        didSet
        {
            tfConfirmMb.titleLabel?.font = UIFont(name: appFont, size: 17.0)
            tfConfirmMb.placeholderFont = UIFont(name: appFont, size: 17.0)
            tfConfirmMb.placeholder = tfConfirmMb.placeholder?.localized
        }
    }
    @IBOutlet weak var btnCross: UIButton!{
        didSet
        {
            btnCross.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnCross.setTitle(btnCross.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var ivConfirmMb: UIImageView!
    
    let app_delegate = UIApplication.shared.delegate as! AppDelegate
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.btnCross.isHidden = true
        self.tfConfirmMb.resignFirstResponder()
        
        //self.tfConfirmMb.placeholder = "Enter  Confirmation Number"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    @IBAction func onClickCrossBtn(_ sender: Any) {
        //remove secton's obj at selected section
        
        let rowCount = self.parentVC.sections[self.indexPath.section].rows.count
        
        if rowCount > 2 {
            //remove 2 rows (amount & confirm mb no)
            self.parentVC.sections[self.indexPath.section].rows.removeLastObject()
            
            self.parentVC.sections[self.indexPath.section].rows.removeLastObject()
        }else{
            //only remove confirm mb no
            self.parentVC.sections[self.indexPath.section].rows.removeLastObject()
            self.parentVC.sections[self.indexPath.section].rows[1] = "09"
        }
        
        
        //add confirm mb no row
        
        //        self.parentVC.sections[self.indexPath.section].rows =  self.parentVC.addNewRowInSection(rows: self.parentVC.sections[self.indexPath.section].rows)
        //        self.parentVC.tv.reloadData();
        
        
        self.parentVC.btnPlus.isHidden = true
        self.parentVC.btnSubmit.isHidden = true
        self.parentVC.isBothNumbersEntered = false
        
        self.btnCross.isHidden = true
        self.tfConfirmMb.becomeFirstResponder()
        if let cell:SMTopUpMultiCell2 = self.parentVC.tv.cellForRow(at: IndexPath(row: 1, section: indexPath.section)) as? SMTopUpMultiCell2 {
            cell.tfConfirmMb.text = "09"
        }
        
        //        let deadlineTime = DispatchTime.now() + .milliseconds(3)
        //        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
        //            self.tfConfirmMb.becomeFirstResponder()
        //        }
    }
    
    @IBAction func confirmMobileNumberTFEditingChanged(_ sender: Any) {
        let chars = tfConfirmMb.text!
        //let mbLength = validObj.getNumberRangeValidation(chars).min //10
        
        if chars.count > 2 {
            self.btnCross.isHidden = false
            
        }else {
            self.btnCross.isHidden = true
        }
        
        if self.holdMobileNumberStr == chars {
            if self.parentVC.sections.count == 5 {
                self.parentVC.btnPlus.isHidden = true
            }
            else {
                self.parentVC.btnPlus.isHidden = false
            }
            self.parentVC.isBothNumbersEntered = true
            //            self.parentVC.isShowPlusButton = true
            self.parentVC.btnSubmit.isHidden = false
        }
        else {
            self.parentVC.isBothNumbersEntered = false
            //            self.parentVC.isShowPlusButton = false
            self.parentVC.btnPlus.isHidden = true
            self.parentVC.btnSubmit.isHidden = true
        }
        
        
        //        if chars.count > mbLength - 1 {
        //            if self.parentVC.sections.count == 5 {
        //                self.parentVC.btnPlus.isHidden = true
        //            }
        //            else {
        //                self.parentVC.btnPlus.isHidden = false
        //            }
        //            self.parentVC.btnSubmit.isHidden = false
        //        }
        //        else {
        //            self.parentVC.btnPlus.isHidden = true
        //            self.parentVC.btnSubmit.isHidden = true
        //        }
        
    }
    
}


extension SMTopUpMultiCell2:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let lastSection =  self.parentVC.sections.count - 1
        //let lastSectionRowCount = self.parentVC.sections[lastSection].rows.count
        
        if range.location < 2 {
            return false
        }
        // back pressed
        if string == ""  && textField.text?.count != 2 {
            let rowsCount = self.parentVC.sections[self.indexPath.section].rows.count;
            // delete the below rows
            
            if rowsCount == 3 {
                // delete  row
                self.parentVC.deleteTvLastRow()
                self.parentVC.btnPlus.isHidden = true
            }
            
        }
        else if string == "" && textField.text?.count == 2 {
            return false
        }
        
        let chars = textField.text! + string;
        let nsString = textField.text as NSString?
        let newString = nsString?.replacingCharacters(in: range, with: string)
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        //      let mbLength = Int(HelperObjC.getMbLength(chars))
        
        //let mbLength = (validObj.getNumberRangeValidation(prefix: chars)) //10
        
        //plus btn
        //        if chars.count > mbLength - 1 {
        //            self.parentVC.btnPlus.isHidden = false
        //            self.parentVC.btnSubmit.isHidden = false
        //        }
        if (chars.hasPrefix("090")) || (chars.hasPrefix("091")){
            return false
        }
        //        else{
        //            self.parentVC.btnPlus.isHidden = true
        //        }
        
        //show plus button but do n't show if count 5
        if self.parentVC.sections.count == 5 {
            
            //            self.parentVC.btnPlus.isHidden = true
        }
        
        //btn cross
        if chars.count > 3 {
            self.btnCross.isHidden = false
        }else {
            self.btnCross.isHidden = true
        }
        
        //confirm mb
        let enteredMbText = self.parentVC.sections[lastSection].rows[0] as! String
        self.holdMobileNumberStr = enteredMbText
        let count:Int = (textField.text?.count)! + 1
        
        if isBackSpace == -92 {
            parentVC.sections[self.indexPath.section].rows.replaceObject(at: 1, with: newString!)
            parentVC.tv.beginUpdates()
            parentVC.tv.endUpdates()
            return true
        }
        
        // First n chars
        let firstChars = String(enteredMbText.prefix(count)) // first2Chars = "My"
        //enter cha is equal to mb no
        if string != "" && (textField.text! + string == firstChars) {
            //replace row value with new value
            parentVC.sections[self.indexPath.section].rows.replaceObject(at: 1, with: newString!)
            return true
        }else {
            //not matching with entered mb no
            return false
        }
        //return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
}
