//
//  SMSaleNLockListCell.swift
//  OK
//
//  Created by gauri OK$ on 6/1/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class SMSaleNLockListCell: UITableViewCell{
    @IBOutlet var viewRequestApprove: UIView!
    @IBOutlet var viewInActiveSendOTP: UIView!
    
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var nameTitle: UILabel!{
        didSet
        {
            nameTitle.font = UIFont(name: appFont, size: appFontSize)
            nameTitle.text = nameTitle.text?.localized
        }
    }
    @IBOutlet var nameValue: UILabel!{
        didSet
        {
           // nameValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var mobNumTitle: UILabel!{
        didSet
        {
            mobNumTitle.font = UIFont(name: appFont, size: appFontSize)
            mobNumTitle.text = mobNumTitle.text?.localized
        }
    }
    @IBOutlet var mobNumValue: UILabel!{
        didSet
        {
            mobNumValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var divisionTitle: UILabel!{
        didSet
        {
            divisionTitle.font = UIFont(name: appFont, size: appFontSize)
            divisionTitle.text = divisionTitle.text?.localized
        }
    }
    @IBOutlet var divisionValue: UILabel!{
        didSet
        {
            divisionValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var townshipTitle: UILabel!{
        didSet
        {
            townshipTitle.font = UIFont(name: appFont, size: appFontSize)
            townshipTitle.text = townshipTitle.text?.localized
        }
    }
    @IBOutlet var townshipValue: UILabel!{
        didSet
        {
            townshipValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var statusTitle: UILabel!{
        didSet
        {
            statusTitle.font = UIFont(name: appFont, size: appFontSize)
            statusTitle.text = statusTitle.text?.localized
        }
    }
    @IBOutlet var statusValue: UILabel!{
        didSet
        {
            statusValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var cardView: CardDesignView!
    @IBAction func activeInActiveClick(_ sender: UIButton) {
        
        if (sender.isSelected){
            sender.isSelected=false;
        }
        else{
            sender.isSelected=true;
        }
    }
    @IBOutlet weak var btnReqApprove: UIButton!{
        didSet{
            btnReqApprove.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnReqApprove.setTitle(btnReqApprove.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var btnInActive: UIButton!{
        didSet{
            btnInActive.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnInActive.setTitle(btnInActive.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var btnSendOTP: UIButton!{
        didSet{
            btnSendOTP.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnSendOTP.setTitle("Send OTP".localized, for: .normal)
        }
    }
    @IBOutlet weak var btnStatusActiveInActive: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgUser.layer.cornerRadius = imgUser.frame.width / 2
        imgUser?.layer.masksToBounds = true
        imgUser.layer.borderColor = UIColor.lightGray.cgColor
        imgUser.layer.borderWidth = 1.0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func wrapData(arrayValue: SaleNLockList,indexTag: Int){

        //profilePic
        if let urlStr = arrayValue.profilePic {
            if urlStr.contains(find: " ") {
                if let imagePEUrl = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                    let imageURL = URL(string: imagePEUrl)
                    self.imgUser.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "userTRN"))
                    
                } else {
                    let imageURL = URL(string: urlStr)
                    self.imgUser.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "userTRN"))
                }
            } else {
                let imageURL = URL(string: urlStr)
                self.imgUser.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "userTRN"))
            }
        }
        
        nameValue.text = ": " + arrayValue.name!
        mobNumValue.text = ": " + arrayValue.phoneNumber!.replacingOccurrences(of: "0095", with: "(+95)0")
        let divisionTownshipName = getDivisionAndTownShip(divisionCode: arrayValue.state ?? "", townshipCode: arrayValue.township ?? "").split(separator: ",")
        if divisionTownshipName.count > 0 {
        divisionValue.text = ": " + divisionTownshipName[1]
        townshipValue.text = ": " + divisionTownshipName[0]
        } else {
            divisionValue.text = ": "
            townshipValue.text = ": "
        }
        if arrayValue.status == 0 {
            btnStatusActiveInActive.isHidden = true
            viewRequestApprove.isHidden = false
            viewInActiveSendOTP.isHidden = true
            btnReqApprove.setTitle("Pending Approval".localized, for: .normal)
            statusValue.text = ": " + "Pending Approval".localized
            statusValue.textColor = .red
            //btnReqApprove.tag = indexTag
        } else {
            btnStatusActiveInActive.isHidden = false

            if(arrayValue.isActive?.caseInsensitiveCompare("Active") == .orderedSame){
               
                statusValue.text = ": " + "Active".localized
                statusValue.textColor = UIColor(red: (0/255), green: (119/255), blue: (0/255), alpha: 1.0)
                //btnReqApprove.tag = 100 + indexTag
                btnStatusActiveInActive.setBackgroundImage( #imageLiteral(resourceName: "SNL_Active"), for: .normal)

                if arrayValue.forgotFlag == true {
                    viewRequestApprove.isHidden = true
                    viewInActiveSendOTP.isHidden = false
                    btnInActive.setTitle("Edit".localized, for: .normal)
                    //btnInActive.tag = 100 + indexTag
                } else {
                    viewRequestApprove.isHidden = false
                    viewInActiveSendOTP.isHidden = true
                    btnReqApprove.setTitle("Edit".localized, for: .normal)
                }
            }else{
                statusValue.text = ": " + "InActive".localized
                statusValue.textColor = .blue
                //btnReqApprove.tag = 200 + indexTag
                btnStatusActiveInActive.setBackgroundImage( #imageLiteral(resourceName: "SNL_InActive"), for: .normal)

                if arrayValue.forgotFlag == true {
                    viewRequestApprove.isHidden = true
                    viewInActiveSendOTP.isHidden = false
                    btnInActive.setTitle("Edit".localized, for: .normal)
                    //btnInActive.tag = 200 + indexTag
                } else {
                    viewRequestApprove.isHidden = false
                    viewInActiveSendOTP.isHidden = true
                    btnReqApprove.setTitle("Edit".localized, for: .normal)
                }
            }
        }
    }

}


