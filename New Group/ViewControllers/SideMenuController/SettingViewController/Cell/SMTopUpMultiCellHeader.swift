//
//  TopUpMultiCellHeader.swift
//  OK
//
//  Created by Badru on 8/14/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit

class SMTopUpMultiCellHeader: UITableViewCell {
    
    var parentVC:SMAddFiveNumberViewController! = nil
    var section:Int! = nil
    
    @IBOutlet weak var lblTitle: UILabel!{
        didSet
        {
            lblTitle.text = lblTitle.text?.localized
        }
    }
    @IBOutlet weak var lblOperatorName: UILabel!{
        didSet
        {
            lblOperatorName.text = lblOperatorName.text?.localized
        }
    }
    @IBOutlet weak var btnDelete: UIButton!{
        didSet
        {
            btnDelete.setTitle(btnDelete.titleLabel?.text?.localized, for: .normal)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func onClickDeleteBtn(_ sender: Any) {
        self.parentVC.btnPlus.isHidden = true
        self.parentVC.btnSubmit.isHidden = true
        self.parentVC.sections.remove(at: section)
        self.parentVC.tv.reloadData()
        self.parentVC.isDeletedSection = true
        self.parentVC.viewWillAppear(true)
    }
}
