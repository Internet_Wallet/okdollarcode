

import UIKit
import SkyFloatingLabelTextField

class SMTopUpMultiCell1: UITableViewCell{
    
    var parentVC: SMAddFiveNumberViewController! = nil
    var indexPath: IndexPath! = nil
    let validObj              = PayToValidations()
    
    @IBOutlet weak var layoutConstraintHeight_rechargeHeader: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!{
        didSet
        {
            lblTitle.font = UIFont(name: appFont, size: appFontSize)
            lblTitle.text = lblTitle.text?.localized
        }
    }
    
    @IBOutlet weak var lblOperatorName: UILabel!{
        didSet
        {
            lblOperatorName.font = UIFont(name: appFont, size: appFontSize)
            lblOperatorName.text = lblOperatorName.text?.localized
        }
    }
    
    @IBOutlet weak var btnDelete: UIButton!{
        didSet
        {
            btnDelete.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnDelete.setTitle(btnDelete.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var tfMb: SkyFloatingLabelTextField!{
        didSet
        {
            tfMb.font = UIFont(name: appFont, size: 17.0)
            tfMb.placeholder = tfMb.placeholder?.localized
        }
    }
    
    @IBOutlet weak var btnCross: UIButton!{
        didSet
        {
            btnCross.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnCross.setTitle(btnCross.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var btnContact: UIButton!{
        didSet
        {
            btnContact.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnContact.setTitle(btnContact.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var viewBg: UIView!
    
    let app_delegate = UIApplication.shared.delegate as! AppDelegate
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.btnCross.isHidden = true
        self.tfMb.resignFirstResponder()
        let longPress = UILongPressGestureRecognizer.init(target: self, action: #selector(deleteBtnLongPressed(_:)))
        btnDelete.addGestureRecognizer(longPress)
        
        //        self.tfMb.placeholder = app_delegate.getlocaLizationLanguage("Enter Mobile Number")
        //        self.tfMb.placeholder = "Enter Mobile Number"        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    override func didMoveToSuperview() {
        
    }
    
    @objc func deleteBtnLongPressed(_ sender: UILongPressGestureRecognizer) {
        if sender.state == .began {
            btnDelete.isUserInteractionEnabled = false
        }
        else if sender.state == .ended {
            btnDelete.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func onClickDeleteBtn(_ sender: Any){
        btnDelete.isUserInteractionEnabled = false
        self.parentVC.tfFalse.resignFirstResponder()
        var isCheckBothCellValueSame:Bool = false
        var totalSections:Int = 0
        
        //        var valueInCell2 = ""
        //        let valueInCell1 = self.parentVC.sections[self.parentVC.sections.count-1].rows[0] as! String
        //
        //        if self.parentVC.sections[self.parentVC.sections.count-1].rows.count > 1 {
        //            println_debug("kkkkkkkkkkkk ---- \n \(self.parentVC.sections[self.parentVC.sections.count-1].rows[1])")
        //            valueInCell2 = self.parentVC.sections[self.parentVC.sections.count-1].rows[1] as! String
        //        }
        //
        //        if valueInCell1 == valueInCell2 {
        //            isCheckBothCellValueSame = true
        //        }
        for data in self.parentVC.sections {
            println_debug("Beforeee --------- \(data.isSelectContact)")
        }
        totalSections = self.parentVC.sections.count-1
        
        if (indexPath.section == self.parentVC.sections.count-1) && self.parentVC.isBothNumbersEntered {
            isCheckBothCellValueSame = true
        } else {
            isCheckBothCellValueSame = false
        }
        
        if totalSections == 4 {
            if self.parentVC.isBothNumbersEntered {
                isCheckBothCellValueSame = true
            } else {
                isCheckBothCellValueSame = false
            }
        }
        
        if self.indexPath.section <= totalSections {
            self.parentVC.sections.remove(at: self.indexPath.section)
            self.parentVC.tv.reloadData()
        }
        btnDelete.isUserInteractionEnabled = true
        
        //        if totalSections == 1 {
        //            self.parentVC.btnPlus.isHidden = true
        //            self.parentVC.btnSubmit.isHidden = true
        //            self.parentVC.sections.append(MultiTopUpSection.init(name: "Recharge 1".localized, rows:self.parentVC.addNewRowInSection(rows: NSMutableArray())))
        //        }
        
        for data in self.parentVC.sections {
            println_debug("Afterrrr --------- \(data.isSelectContact)")
        }
        
        if self.parentVC.sections.count == 0 {
            self.parentVC.initload()
            return
        }
        if (self.parentVC.totalMobileNumbers == self.parentVC.sections.count) {
            self.parentVC.btnPlus.isHidden = false
            return
        }
        //        if (indexPath.section < totalSections) {
        //            if isCheckBothCellValueSame {
        //                self.parentVC.btnPlus.isHidden = false
        //            }
        //        }
        if isCheckBothCellValueSame {
            self.parentVC.btnPlus.isHidden = false
            self.parentVC.btnSubmit.isHidden = false
        }
        else if (indexPath.section == totalSections) {
            self.parentVC.btnPlus.isHidden = false
            self.parentVC.btnSubmit.isHidden = false
        }
            //        else if self.parentVC.isShowPlusButton {
            //            self.parentVC.btnPlus.isHidden = false
            //        }
        else {
            if (indexPath.section <= totalSections) {
                if isCheckBothCellValueSame {
                    self.parentVC.btnPlus.isHidden = false
                    self.parentVC.btnSubmit.isHidden = false
                }
                else {
                    if totalSections == 4 {
                        if isCheckBothCellValueSame || self.parentVC.sections[totalSections-1].isSelectContact {
                            self.parentVC.btnPlus.isHidden = false
                            self.parentVC.btnSubmit.isHidden = false
                            return
                        } else {
                            self.parentVC.btnPlus.isHidden = true
                            return
                        }
                    }
                    if self.parentVC.btnPlus.isHidden {
                        self.parentVC.btnPlus.isHidden = true
                    }
                    else {
                        self.parentVC.btnPlus.isHidden = false
                        self.parentVC.btnSubmit.isHidden = false
                    }
                }
            }
        }
        
        /*
         alertViewObj.wrapAlert(title: nil, body: "Do you want to delete Recharge detail" + " " + String(self.indexPath.section + 1) + " ?", img: nil)
         
         alertViewObj.addAction(title: "OK", style: .target , action: {
         
         self.parentVC.btnPlus.isHidden = false
         self.parentVC.btnSubmit.isHidden = false
         self.parentVC.sections.remove(at: self.indexPath.section)
         self.parentVC.tv.reloadData()
         
         })
         
         alertViewObj.showAlert(controller: parentVC)
         
         */
    }
    
    
    
    
    @IBAction func onClickContactBtn(_ sender: Any) {
        self.parentVC.view.endEditing(true)
        let nav = UitilityClass.openContact(multiSelection: false, self.parentVC, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
        self.parentVC.present(nav, animated: true, completion: nil)
        //        self.parentVC.tv.keyboardDismissMode = .interactive
    }
    
    
    
    @IBAction func onClickCrossBtn(_ sender: Any){
        
        //remove secton's obj at selected section
        self.parentVC.sections.remove(at: self.indexPath.section);
        
        let sectionName = "Recharge"
        
        //add new section with 1 row
        self.parentVC.sections.append(MultiTopUpSection.init(name: sectionName , rows:self.parentVC.addNewRowInSection(rows: NSMutableArray())))
        
        self.parentVC.tv.reloadData();
        
        //update header color to default
        self.parentVC.updateColor(mb: "09")
        self.parentVC.btnPlus.isHidden = true
        self.parentVC.btnSubmit.isHidden = true
        
        //   self.parentVC.setAdv(isBackPressed: false)
        
        self.btnCross.isHidden = true
        self.tfMb.becomeFirstResponder()
        
    }
    
    @IBAction func mobileNumberTFEditingChanged(_ sender: Any) {
        let chars = tfMb.text!
        
        if chars.count > 1 && chars.count < 6 {
            self.parentVC.updateColor(mb: chars)
        }
        if chars.count > 2 {
            self.btnCross.isHidden = false
        }else {
            self.btnCross.isHidden = true
            //            self.parentVC.updateColor(mb: chars)
        }
    }
}

extension SMTopUpMultiCell1: UITextFieldDelegate {
    
    func deleteRow(rowsCount:Int) {
        
        // delete the below rows
        
        if rowsCount == 3 {
            // delete 2 rows
            self.parentVC.deleteTvLastRow()
            self.parentVC.deleteTvLastRow()
            
        }else if rowsCount == 2 {
            // delete 1 row
            self.parentVC.deleteTvLastRow()
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let chars = textField.text! + string;
        let nsString = textField.text as NSString?
        let newString = nsString?.replacingCharacters(in: range, with: string)
        
        // back pressed
        if range.location < 2 {
            return false
        }
        
        if validObj.getNumberRangeValidation(chars).isRejected == true {
            return false
        }
        
        if validObj.getNumberRangeValidation(chars).isRejected == false {
            let mbLength = validObj.getNumberRangeValidation(chars).min
            let maxLength = validObj.getNumberRangeValidation(chars).max
            if chars.count > mbLength { //11
                if maxLength > mbLength {
                    if chars.count > maxLength { //11
                        return false
                    }
                    //                    else {
                    //                        return true
                    //                    }
                }
                else {
                    return false
                }
            }
        } else {
            self.parentVC.updateColor(mb: "09")
            self.parentVC.btnPlus.isHidden = true
            self.parentVC.btnSubmit.isHidden = true
            self.btnCross.isHidden = true
            self.parentVC.showToast(message: "Invalid Number".localized, align: .center)
            return false
        }
        
        if string == ""  && textField.text?.count != 2 {
            self.parentVC.setAdv(isBackPressed: true)
            let rowsCount = self.parentVC.sections[self.indexPath.section].rows.count;
            if rowsCount > 1 {
                //                self.parentVC.isShowPlusButton = false
                self.parentVC.btnPlus.isHidden = true
                self.parentVC.btnSubmit.isHidden = true
                self.deleteRow(rowsCount: rowsCount)
            }
            //update header color to default
            //            if (textField.text?.count)! < 5 {
            //
            //                self.parentVC.updateColor(mb: "09")
            //            }
        }
        
        let rowsCount = self.parentVC.sections[self.indexPath.section].rows.count;
        
        if rowsCount > 1 {
            self.deleteRow(rowsCount: rowsCount)
        }
        if (chars.hasPrefix("090")) || (chars.hasPrefix("091")){
            return false
        }
        
        if validObj.getNumberRangeValidation(chars).isRejected == false {
            let mbLength = validObj.getNumberRangeValidation(chars).min //10
            let maxLength = validObj.getNumberRangeValidation(chars).max //10
            
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            parentVC.sections[self.indexPath.section].rows.replaceObject(at: 0, with: newString!)
            
            if chars.count >= mbLength && !(isBackSpace == -92) { //11
                if maxLength > mbLength {
                    //                    parentVC.sections[self.indexPath.section].rows.replaceObject(at: 0, with: chars)
                    self.parentVC.reloadTV2AddNewRow(indexPath: self.indexPath, willReloadTv: false)
                    self.parentVC.btnPlus.isHidden = true
                    self.parentVC.btnSubmit.isHidden = true
                    
                    if chars.count == maxLength {
                        //become  next cell first responder
                        self.parentVC.tv.scrollToRow(at: IndexPath(row: 1, section: self.indexPath.section), at: UITableView.ScrollPosition.bottom, animated: false)
                        let sectionCount = self.parentVC.sections.count - 1;
                        let lastSectionRowCount = (self.parentVC.sections[sectionCount].rows.count - 1)
                        
                        if lastSectionRowCount == 1 {
                            if let cell:SMTopUpMultiCell2 = self.parentVC.tv.cellForRow(at: IndexPath(row: lastSectionRowCount, section: sectionCount)) as? SMTopUpMultiCell2 {
                                let deadlineTime = DispatchTime.now() + .milliseconds(1)
                                DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                                    cell.tfConfirmMb.becomeFirstResponder()
                                }
                            }
                        }
                    }
                    self.btnCross.isHidden = false
                }
                else {
                    //                    parentVC.sections[self.indexPath.section].rows.replaceObject(at: 0, with: chars)
                    self.parentVC.reloadTV2AddNewRow(indexPath: self.indexPath, willReloadTv: false)
                    self.parentVC.tv.scrollToRow(at: IndexPath(row: 1, section: self.indexPath.section), at: UITableView.ScrollPosition.bottom, animated: false)
                    
                    let sectionCount = self.parentVC.sections.count - 1;
                    let lastSectionRowCount = (self.parentVC.sections[sectionCount].rows.count - 1)
                    if lastSectionRowCount == 1 {
                        if let cell:SMTopUpMultiCell2 = self.parentVC.tv.cellForRow(at: IndexPath(row: lastSectionRowCount, section: sectionCount)) as? SMTopUpMultiCell2 {
                            let deadlineTime = DispatchTime.now() + .milliseconds(1)
                            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                                cell.tfConfirmMb.becomeFirstResponder()
                            }
                        }
                    }
                    self.btnCross.isHidden = false
                }
            }
            if chars.count > mbLength && isBackSpace == -92 { //11
                if maxLength > mbLength {
                    //                    parentVC.sections[self.indexPath.section].rows.replaceObject(at: 0, with: newString!)
                    self.parentVC.reloadTV2AddNewRow(indexPath: self.indexPath, willReloadTv: false)
                    self.parentVC.btnPlus.isHidden = true
                    self.parentVC.btnSubmit.isHidden = true
                    self.btnCross.isHidden = false
                }
                else {
                    //                    parentVC.sections[self.indexPath.section].rows.replaceObject(at: 0, with: newString!)
                    self.parentVC.reloadTV2AddNewRow(indexPath: self.indexPath, willReloadTv: false)
                    self.parentVC.btnPlus.isHidden = true
                    self.parentVC.btnSubmit.isHidden = true
                    
                    let sectionCount = self.parentVC.sections.count - 1;
                    let lastSectionRowCount = (self.parentVC.sections[sectionCount].rows.count - 1)
                    if lastSectionRowCount == 1 {
                        if let cell:SMTopUpMultiCell2 = self.parentVC.tv.cellForRow(at: IndexPath(row: lastSectionRowCount, section: sectionCount)) as? SMTopUpMultiCell2 {
                            let deadlineTime = DispatchTime.now() + .milliseconds(1)
                            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                                cell.tfConfirmMb.becomeFirstResponder()
                            }
                        }
                    }
                    self.btnCross.isHidden = false
                }
            }
        }
        
        return true
    }
    
    //        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    //
    //            // back pressed
    //            if string == ""  && textField.text?.count != 2 {
    //
    //                self.parentVC.setAdv(isBackPressed: true)
    //
    //                let rowsCount = self.parentVC.sections[self.indexPath.section].rows.count;
    //
    //                if rowsCount > 1 {
    //
    //                    self.deleteRow(rowsCount: rowsCount)
    //
    //                }
    //
    //                //update header color to default
    //    //            if (textField.text?.count)! < 5 {
    //    //
    //    //                self.parentVC.updateColor(mb: "09")
    //    //            }
    //
    //                return true
    //
    //            }else if string == "" && textField.text?.count == 2 {
    //                return false
    //            }
    //
    //
    //            let chars = textField.text! + string;
    //
    //            //========= to avoid crash
    //            let rowsCount = self.parentVC.sections[self.indexPath.section].rows.count;
    //
    //            if rowsCount > 1 {
    //
    //                self.deleteRow(rowsCount: rowsCount)
    //
    //            }
    //            else if (chars.hasPrefix("090")) || (chars.hasPrefix("091")){
    //                return false
    //            }
    //
    //            let mbLength = (validObj.getNumberRangeValidation(prefix: chars)) //10
    //
    //            if chars.count == mbLength { //11
    //
    //                //remove  row value as containg "" value
    //
    //
    //                parentVC.sections[self.indexPath.section].rows.replaceObject(at: 0, with: newString!)
    //
    //                self.parentVC.reloadTV2AddNewRow(indexPath: self.indexPath, willReloadTv: false)
    //
    //
    //                //become  next cell first responder
    //                let sectionCount = self.parentVC.sections.count - 1;
    //                let lastSectionRowCount = (self.parentVC.sections[sectionCount].rows.count - 1)
    //
    //
    //                if lastSectionRowCount == 1 {
    //
    //                    let cell:SMTopUpMultiCell2 = self.parentVC.tv.cellForRow(at: IndexPath(row: lastSectionRowCount, section: sectionCount  )) as! SMTopUpMultiCell2
    //
    //                    let deadlineTime = DispatchTime.now() + .milliseconds(1)
    //                    DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
    //
    //                        cell.tfConfirmMb.becomeFirstResponder()
    //
    //                    }
    //                }
    //
    //                //            self.btnCross.isHidden = false
    //
    //            }
    //
    //                //        else if chars.count < mbLength {
    //                //
    //                //            self.btnCross.isHidden = true
    //                //
    //                //        }
    //                //        else if chars.count > mbLength {
    //                //
    //                //            // don't allow to add chars
    //                //            return false
    //                //
    //                //        }
    //
    //                //btn cross
    //            else if chars.count > 3 {
    //
    //                self.btnCross.isHidden = false
    //
    //            }else {
    //
    //                self.btnCross.isHidden = true
    //
    //            }
    //
    //
    //    //        if chars.count == 4 || chars.count > 4  {
    //    //            //chage nav color and plus btn
    //    //
    //    //            self.parentVC.updateColor(mb: chars)
    //    //
    //    //        } else if chars.count < 4 {
    //    //            // set default color
    //    //
    //    //        }
    //
    //            return true
    //        }
    
}












