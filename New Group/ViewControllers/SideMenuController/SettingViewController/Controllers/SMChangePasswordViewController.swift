//
//  changePasswordViewController.swift
//  SettingVC
//
//  Created by Mohit on 10/16/17.
//  Copyright © 2017 Mohit. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class SMChangePasswordViewController: OKBaseController {
    
    var changePasswordDict = Dictionary<String, Any>()
    var buttonConstraint: NSLayoutConstraint!
    let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    let notificationCenter = NotificationCenter.default
    
    @IBOutlet var label: UILabel!{
        didSet
        {
            label.font = UIFont(name: appFont, size: appFontSize)
            label.text = label.text?.localized
        }
    }
    @IBOutlet weak var btnReset: UIButton!{
        didSet
        {
            btnReset.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnReset.setTitle(btnReset.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var btnNext: UIButton!{
        didSet
        {
            btnNext.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnNext.setTitle(btnNext.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var btnReset1: UIButton!{
        didSet
        {
            btnReset1.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnReset1.setTitle(btnReset1.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var btnNext1: UIButton!{
        didSet
        {
            btnNext1.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnNext1.setTitle(btnNext1.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var btnShowPwd: UIButton!{
        didSet
        {
            btnShowPwd.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnShowPwd.setTitle(btnShowPwd.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var normalPasswordView: UIView!
    @IBOutlet weak var changePasswordView: UIView!
    @IBOutlet weak var tfPassword: UITextField!{
        didSet
        {
            tfPassword.font = UIFont(name: appFont, size: 17.0)
            tfPassword.placeholder = tfPassword.placeholder?.localized
            //tfPassword.becomeFirstResponder()
        }
    }
    @IBOutlet weak var tfRePassword: UITextField!{
        didSet
        {
            tfRePassword.font = UIFont(name: appFont, size: 17.0)
            tfRePassword.placeholder = tfRePassword.placeholder?.localized
        }
    }
    @IBOutlet weak var tfCurrentPassword: UITextField!{
        didSet
        {
            tfCurrentPassword.font = UIFont(name: appFont, size: 17.0)
            tfCurrentPassword.placeholder = tfCurrentPassword.placeholder?.localized
             //tfCurrentPassword.becomeFirstResponder()
        }
    }
    let validObj       = PayToValidations()
    override func viewDidLoad(){
         super.viewDidLoad()
     
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Password".localized
        if ok_password_type {
            normalPasswordView.isHidden = false
            changePasswordView.isHidden = true
        } else {
            normalPasswordView.isHidden = true
            changePasswordView.isHidden = false
        }
        buttonConstraint = btnNext.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        buttonConstraint = btnReset.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        buttonConstraint = btnNext1.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        buttonConstraint = btnReset1.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        buttonConstraint.isActive = true
        self.view.layoutIfNeeded()
        subscribeToShowKeyboardNotifications()
        
    }
    
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.buttonConstraint.constant = 0 - keyboardHeight
        })
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        buttonConstraint.constant = 0
    }
    
    func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notificationCenter.addObserver(self,  selector: #selector(SessionAlert),   name: .appTimeout,   object: nil)
        
        IQKeyboardManager.sharedManager().enable = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = false
        IQKeyboardManager.sharedManager().previousNextDisplayMode = .alwaysHide
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        
        IQKeyboardManager.sharedManager().previousNextDisplayMode = .Default
        notificationCenter.removeObserver(self, name: .appTimeout, object: nil)
        
    }
    
    @objc func SessionAlert()  {
        DispatchQueue.main.async {
            
            alertViewObj.wrapAlert(title: "", body: "Application Session expired login again".localized, img:  #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){
                if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    
    override func didMove(toParent parent: UIViewController?){
        
        if parent == nil {
            if changePasswordView.isHidden == true{
                normalPasswordView.isHidden = true
                changePasswordView.isHidden = false
            }
            else{ self.navigationController?.popViewController(animated: true) }
        }
    }
    
    
    
    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func nextClick(_ sender: UIButton){
        
        if tfCurrentPassword.text == ""{
            alertViewObj.wrapAlert(title: "", body: "Please enter minimum 6 digits password.".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: { })
            alertViewObj.showAlert(controller: self)
        }
            
        else if (tfCurrentPassword.text?.length)! < 6{
            self.customAlert(msg: "Please enter minimum 6 digits password.")
        }
            
        else if tfCurrentPassword.text != ok_password{
            
            alertViewObj.wrapAlert(title: "", body: "You have entered Incorrect Password. Please try again".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: { })
            alertViewObj.showAlert(controller: self)
        }
        else{
            tfCurrentPassword.resignFirstResponder()
            let defValue = userDef.value(forKey: "statedef") as? String ?? ""
            println_debug(defValue)
            //tfPassword.becomeFirstResponder()
            if !ok_password_type == true && defValue == "Password".localized{
                normalPasswordView.isHidden = false
                changePasswordView.isHidden = true
            }
            else{
                self.tfCurrentPassword.text = ""
                self.tfCurrentPassword.resignFirstResponder()
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "SMPatternViewController") as? SMPatternViewController
                self.navigationController?.pushViewController(VC!, animated: true)
            }
            
        }
    }
    
    func customAlert(msg:String){
        
        
        alertViewObj.wrapAlert(title: "", body: msg.localized, img: #imageLiteral(resourceName: "alert-icon"))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {
            //            self.tfPassword.text = ""
            //            self.tfRePassword.text = ""
        })
        alertViewObj.showAlert(controller: self)
        
    }
    
    @IBAction func showPasswordClick(_ sender: UIButton){
        
        if sender.isSelected {
            sender.isSelected = false
            tfPassword.isSecureTextEntry = true
            tfRePassword.isSecureTextEntry = true
        } else {
            sender.isSelected = true
            tfPassword.isSecureTextEntry = false
            tfRePassword.isSecureTextEntry = false
        }
    }
    
    
    @IBAction func resetClick(_ sender: Any){
        self.tfCurrentPassword.text = ""
    }
    
    
    @IBAction func patternClick(_ sender: Any){
        self.tfPassword.text = ""
        self.tfRePassword.text = ""
    }
    
    
    @IBAction func continueClick(_ sender: Any){
        if tfPassword.text != tfRePassword.text {
            alertViewObj.wrapAlert(title: "", body: "Confirm Password and Password is not matched".localized, img: #imageLiteral(resourceName: "r_lock"))
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {
                
            })
            alertViewObj.showAlert(controller: self)
        }
//        else if  (tfRePassword.text?.length)! > 6 {
//            alertViewObj.wrapAlert(title: "", body: "Please enter minimum 6 digits password.".localized, img: #imageLiteral(resourceName: "r_lock"))
//            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {
//
//            })
//            alertViewObj.showAlert(controller: self)
//        }
            
        else if tfPassword.text == "" &&  tfRePassword.text == ""{
            alertViewObj.wrapAlert(title: "", body: "Please enter minimum 6 digits password.".localized, img: #imageLiteral(resourceName: "r_lock"))
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {
                
            })
            alertViewObj.showAlert(controller: self)
        }
            
        else if (tfPassword.text?.length)! < 6 || (tfRePassword.text?.length)! < 6{
            self.customAlert(msg: "Please enter minimum 6 digits password.")
        }
        else if tfRePassword.text == ok_password{
            
            alertViewObj.wrapAlert(title: "", body: "Current Password/Pattern and New Password/Pattern cannot be same".localized, img: #imageLiteral(resourceName: "r_lock"))
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {})
            alertViewObj.showAlert(controller: self)
        }
        else{ self.changePasswordApiCall(newPass: tfPassword.text!, confmPass: tfRePassword.text!) }
        
    }
    
    
    func changePasswordApiCall(newPass: String,confmPass: String) {
        
        if appDelegate.checkNetworkAvail(){
            
            showProgressView()
            let web = WebApiClass()
            web.delegate = self
            let secureToken = UserLogin.shared.token
            let urlS = String.init(format: Url.changePassword, UserModel.shared.mobileNo,ok_password!,UserModel.shared.mobileNo,newPass,secureToken)
            let url =   getUrl(urlStr: urlS, serverType: .serverEstel)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "POST", mScreen: "changePassword")
            
        }else{
            alertViewObj.wrapAlert(title: "", body: "Network Not Available.".localized, img: #imageLiteral(resourceName: "setting"))
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {})
            alertViewObj.showAlert(controller: self)
        }
        
    }
    
}

extension SMChangePasswordViewController: UITextFieldDelegate{
    
    // change the kayBoard type here - Avaneesh
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tfCurrentPassword {
            if userDef.bool(forKey: "passKeyBoardType") {
                tfCurrentPassword.keyboardType = .numberPad
            }else {
                tfCurrentPassword.keyboardType = .default
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == tfPassword{
            tfRePassword.becomeFirstResponder()
        }
        else{
            tfRePassword.resignFirstResponder()
        }
        return false
        
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        
        let text: String? = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if textField == self.tfCurrentPassword{
            if (text?.count ?? 0) >= 30{
                return false
            }
        }
        if textField == self.tfPassword{
            if (text?.count ?? 0)  >= 30{
                return false
            }
        }
        if textField == self.tfRePassword{
            if (text?.count ?? 0)  >= 30{
                return false
            }
        }
        
        
        
        if (isBackSpace == -92){
            if textField == self.tfCurrentPassword{
                if (text?.count ?? 0) <  30{
                    return true
                }
            }
            if textField == self.tfPassword{
                if (text?.count ?? 0)  < 30{
                    return true
                }
            }
            if textField == self.tfRePassword{
                if (text?.count ?? 0)  < 30{
                    return true
                }
            }
        }
        
        if textField != self.tfCurrentPassword {
            
            let chars = textField.text! + string;
            if (chars.hasPrefix("11")) || (chars.hasPrefix("22")) || (chars.hasPrefix("00")) || (chars.hasPrefix("33")) || (chars.hasPrefix("44")) || (chars.hasPrefix("55")) || (chars.hasPrefix("66")) || (chars.hasPrefix("77")) || (chars.hasPrefix("88")) || (chars.hasPrefix("99")) || (chars.hasSuffix("11")) || (chars.hasSuffix("22")) || (chars.hasSuffix("33")) || (chars.hasSuffix("44")) || (chars.hasSuffix("55")) || (chars.hasSuffix("66")) || (chars.hasSuffix("77")) || (chars.hasSuffix("88")) || (chars.hasSuffix("99")) {
                self.customAlert(msg: "Please enter valid password. Password like 111111, 112233 and your mobile number is not allowed for security reasons")
                return false
            }
        }
        
        

        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        
        if textField == tfRePassword{
            if validObj.checkMatchingNumber(string: text ?? "", withString: self.tfPassword.text!) {
               
                self.tfRePassword.text = text
                return false
            } else {  return false  }
            
        }
        
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        return (string == filtered)
        
       
        
    }
    
    
    
}

extension SMChangePasswordViewController:WebServiceResponseDelegate{
    
    func webResponse(withJson json: AnyObject, screen: String) {
        self.changePasswordParsing(data: json)
    }
    
    func changePasswordParsing(data: AnyObject) {
        let responseString = String(data: data as! Data, encoding: .utf8)
        let xml            = SWXMLHash.parse(responseString!)
        println_debug(xml)
        self.enumerate(indexer: xml)
        println_debug(self.changePasswordDict)
        if  self.changePasswordDict["resultdescription"] as? String ?? "" == "Records Not Found" || self.changePasswordDict["resultdescription"] as? String ?? "" == "Transaction Successful" || self.changePasswordDict["resultdescription"] as? String ?? "" == "Invalid Secure Token" || self.changePasswordDict["resultdescription"] as? String ?? "" == "Secure Token Expire" || self.changePasswordDict["resultdescription"] as? String ?? "" == "Invalid Request (Schema Validation Failed)" || self.changePasswordDict["resultdescription"] as? String ?? "" == "Invalid PIN"{
            
            DispatchQueue.main.async{
                //var str =    self.changePasswordDict["resultdescription"] as? String ?? ""
                userDef.set(self.tfPassword.text, forKey: "passwordLogin_local")
                userDef.set(false, forKey: "passwordLoginType")
                userDef.synchronize()
                
                if let pass = self.tfPassword.text {
                    if pass.isNumeric {
                        userDef.set(true, forKey: "passKeyBoardType")
                    }else {
                        userDef.set(false, forKey: "passKeyBoardType")
                    }
                }
                
                
                ok_password         = userDef.string(forKey: "passwordLogin_local")
                ok_password_type    = userDef.bool(forKey: "passwordLoginType")
                profileObj.callLoginApi(aCode: UserModel.shared.mobileNo, withPassword: ok_password! , handler: { (success
                    ) in
                    if success {
                        profileObj.callUpdateProfileApi(aCode: UserModel.shared.mobileNo, handler: { (status) in
                            if status {
                                DispatchQueue.main.async {
                                    self.removeProgressView()
                                    alertViewObj.wrapAlert(title: "", body: "Your password has been changed successfully.".localized, img:UIImage.init(named: "bank_success"))
                                    alertViewObj.addAction(title: "Done".localized, style: AlertStyle.target, action: {
                                        UserLogin.shared.loginSessionExpired = true
                                        self.navigationController?.popToRootViewController(animated: true)
                                        //NotificationCenter.default.post(name: .goDashboardScreen, object: nil)
                                    })
                                    alertViewObj.showAlert(controller: self)
                                }
                            }
                        })
                    }
                })
            }
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            self.changePasswordDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
}

extension SMChangePasswordViewController: BioMetricLoginDelegate {
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if isSuccessful {
            println_debug("Authorised")
        }
        else{
            if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
        }
    }
}
