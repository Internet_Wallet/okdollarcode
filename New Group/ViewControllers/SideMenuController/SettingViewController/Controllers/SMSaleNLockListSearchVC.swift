//
//  SMSaleNLockListSearchVC.swift
//  OK
//
//  Created by OK$ on 7/17/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class SMSaleNLockListSearchVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SMSaleNLockListVC {
    
    //MARK: - Methods
    func setUpNavigation() {
        self.navigationItem.titleView = nil
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 21.0) ?? UIFont.systemFont(ofSize: 21.0), NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "Safety Cashier".localized
        //self.navigationItem.rightBarButtonItem = nil
        //self.setDismissButton()
    }
    
    func setDismissButton() {
        let buttonIcon      = UIImage(named: "tabBarBack")
        let leftBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: self, action: #selector(FavoriteSelectedViewController.backActions))
        leftBarButton.image = buttonIcon
        self.navigationItem.leftBarButtonItem = leftBarButton
    }
    
    func setSearchButton() {
        let buttonIcon      = UIImage(named: "offer_search_icon")
        let rightBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: self, action: #selector(showSearchView))
        rightBarButton.image = buttonIcon
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    func setupSearchBar() {
        searchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.tintColor = UIColor.white
        searchBar.setTextColor(color: UIColor.white)
        searchBar.placeholder = "Search".localized
        self.navigationItem.titleView = searchBar
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
               
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.keyboardType = .asciiCapable
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
            }
        }
        let view = self.searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = UIColor.white
            }
        }
        
        searchBar.becomeFirstResponder()
    }
    
    @objc func showSearchView() {
        searchBar.placeholder = "Search".localized
        searchBar.text = ""
        searchBar.setTextColor(color: UIColor.white)
        searchBar.delegate = self
        self.navigationItem.titleView = searchBar
        //self.navigationItem.rightBarButtonItem = nil
        searchBar.becomeFirstResponder()
    }
}

extension SMSaleNLockListVC: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            uiButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize) ?? UIFont.systemFont(ofSize: 14)
            if appDel.currentLanguage == "my" {
                uiButton.setTitle("မလုပ်ပါ", for:.normal)
            } else if appDel.currentLanguage == "uni" {
                uiButton.setTitle("မလုပ္ပါ", for:.normal)
            } else {
                uiButton.setTitle("Cancel".localized, for:.normal)
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let totalCharacters = (searchBar.text?.appending(text).count ?? 0) - range.length
        return  totalCharacters <= 30
    }
    
    // called whenever text is changed.
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchBar.text = searchText
        boolSearch = true
        didSelectSearchWithKey(key: searchBar.text!)
    }
    
    func didSelectSearchWithKey(key: String) {
        
        if key.count > 0 {
            
            let filteredArray = recData.filter { ($0.name)?.range(of: key, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
            
            if((filteredArray.count) > 0)
            {
                reloadTableData(arrayList: filteredArray)
            }
            else
            {
                let filteredArray = recData.filter { ($0.phoneNumber)?.range(of: key, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
                
                reloadTableData(arrayList: filteredArray)
            }
        } else {
            println_debug("Text field is cleared")
            reloadTableData(arrayList: recData)
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        boolSearch = false
        setUpNavigation()
        saleNLockTbl.reloadData()
    }
    
    func reloadTableData(arrayList : [SaleNLockList])
    {
        searchResults = arrayList
        saleNLockTbl.reloadData()
    }
    
}
