//
//  ApprovalSaleNLockViewController.swift
//  OK
//  It shows the list of user request to become advance merchant and the current status
//  Created by Mohit on 10/25/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Foundation

class SMApprovalSaleNLockViewController: OKBaseController {
    private let cellId = "SMApprovalSaleNLockViewCell"
    private var saleNLockUserList = [SaleNLockUserList]()
    let notificationCenter = NotificationCenter.default
    private var selectedIndex: Int?
    
    @IBOutlet weak var tableViewApprovalSaleNLock: UITableView!
    @IBOutlet var name: UILabel!{
        didSet
        {
            name.font = UIFont(name: appFont, size: appFontSize)
            name.text = name.text?.localized
        }
    }
    @IBOutlet var status: UILabel!{
        didSet
        {
            status.font = UIFont(name: appFont, size: appFontSize)
            status.text = status.text?.localized
        }
    }
    @IBOutlet var phone: UILabel!{
        didSet
        {
            phone.font = UIFont(name: appFont, size: appFontSize)
            phone.text = phone.text?.localized
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        setUpNavigation()
        self.getSaleNLockUsersList(mobileNo: UserModel.shared.mobileNo)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notificationCenter.addObserver(self,  selector: #selector(SessionAlert),  name: .appTimeout,   object: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notificationCenter.removeObserver(self, name: .appTimeout, object: nil)
    }
    @objc func SessionAlert()  {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "", body: "Application Session expired login again".localized, img:  #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){
                if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getSaleNLockUsersList(mobileNo: String) {
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let web = WebApiClass()
            web.delegate = self
            let urlS = String.init(format: Url.ApproveSaleNLock, mobileNo, uuid)
            let url =   getUrl(urlStr: urlS, serverType: .serverApp)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "GetSaleNLockUsers")
        }
    }
    
    func customAlert(msg:String, title:String) {
        alertViewObj.wrapAlert(title: "", body: msg, img: #imageLiteral(resourceName: "setting"))
        alertViewObj.addAction(title: "Done".localized, style: .target, action:{})
        alertViewObj.showAlert(controller: self)
    }
    
    @objc func activeApproveSaleNLockApiCall(_ sender: UIButton) {
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let web = WebApiClass()
            web.delegate = self
            selectedIndex = sender.tag
            let user = self.saleNLockUserList[sender.tag]
            let strId = user.id ?? ""
            let urlS = String.init(format: Url.RequsteApprovalDummyMerchant_API, UserModel.shared.mobileNo, uuid, uuid, strId, "")//,UserLogin.shared.token)
            let url =   getUrl(urlStr: urlS, serverType: .serverApp)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "ActivateUser")
        }
    }
}

extension SMApprovalSaleNLockViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.saleNLockUserList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SMApprovalSaleNLockViewCell
        let user  = self.saleNLockUserList[indexPath.row]
        cell.wrapData(name: user.name, number: user.phoneNumber, status: user.status)
        cell.btnStatus.tag = indexPath.row
        
        if user.status == 0 {
            cell.btnStatus.addTarget(self, action: #selector(self.activeApproveSaleNLockApiCall(_:)), for: .touchUpInside)
        } else {
            cell.btnStatus.removeTarget(nil, action: nil, for: .allEvents)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.backgroundColor = UIColor.clear
    }
}

extension SMApprovalSaleNLockViewController: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        if screen == "GetSaleNLockUsers" {
            self.usersListResponseParsing(data: json)
        } else if screen == "ActivateUser" {
            self.userActivationResponseParsing(data: json)
        } else if screen == "GetAddedBankID" {
            handleBankIDListResponse(data: json)
        } else if screen == "ClearAddedBanks" {
            handleClearAddedBanksResponse(data: json)
        }
    }
    
    func usersListResponseParsing(data: AnyObject) {
        do {
            if let safeData = data as? Data {
                let decoder = JSONDecoder()
                self.removeProgressView()
                let saleNLockUserListResponse = try decoder.decode(SaleNLockUserListResponse.self, from: safeData)
                if let code = saleNLockUserListResponse.code, code == 200 {
                    guard let userListHolderStr = saleNLockUserListResponse.userListHolder else { return }
                    guard let userListHolderData = userListHolderStr.data(using: .utf8) else { return }
                    let userListHolder = try decoder.decode(SaleNLockUserListHolder.self, from: userListHolderData)
                    saleNLockUserList = userListHolder.userList ?? []
                    DispatchQueue.main.async {
                        self.tableViewApprovalSaleNLock.reloadData()
                    }
                } else {
                    DispatchQueue.main.async {
                        if let msg = saleNLockUserListResponse.msg {
                            self.customAlert(msg: msg.localized, title: "")
                        }
                    }
                }
            }
        } catch let error {
            println_debug(error.localizedDescription)
        }
    }
    
    func userActivationResponseParsing(data: AnyObject) {
        self.removeProgressView()
        do {
            if let safeData = data as? Data {
                let decoder = JSONDecoder()
                let approveSaleNLockUserResponse = try decoder.decode(ApproveSaleNLockUserResponse.self, from: safeData)
                if let code = approveSaleNLockUserResponse.code, code == 200 {
                    DispatchQueue.main.async {
                        if let indexOfItem = self.selectedIndex {
                            self.saleNLockUserList[indexOfItem].status = 1
                            let indexPath = IndexPath(item: indexOfItem, section: 0)
                            self.tableViewApprovalSaleNLock.reloadRows(at: [indexPath], with: .top)
                            self.selectedIndex = nil
                        }
                        DispatchQueue.main.async {
                            CoreDataHelper.shared.clearSMBFromDB()
                            profileObj.callUpdateProfileApi(aCode: UserModel.shared.mobileNo, handler: { (isSuccess) in
                                println_debug("Handle success and failure case")
                            })
                        }
                    }
                    if !UserDefaults.standard.bool(forKey: "IsAlreadyAdvMer") {
                        self.getAllAddedBankID()
                    }
                } else {
                    self.selectedIndex = nil
                    DispatchQueue.main.async {
                        if let msg = approveSaleNLockUserResponse.msg {
                            self.customAlert(msg: msg, title: "")
                        }
                    }
                }
            }
        } catch let error {
            println_debug(error.localizedDescription)
        }
    }
    
    func getAllAddedBankID() {
        if appDelegate.checkNetworkAvail() {
            showProgressView()
            let web      = WebApiClass()
            web.delegate = self
            let mobNo = UserModel.shared.mobileNo
            guard mobNo.count > 0 else { return }
            let simID = UserModel.shared.simID
            guard simID.count > 0 else { return }
            let otp = ""
            let urlStr   = String.init(format: Url.getAddedBanksID, mobNo, simID, otp)
            let getAddedBankIDUrl = getUrl(urlStr: urlStr, serverType: .serverApp)
            let params = [String: String]()
            web.genericClass(url: getAddedBankIDUrl, param: params as AnyObject, httpMethod: "GET", mScreen: "GetAddedBankID")
        }
    }
    
    func handleBankIDListResponse(data: AnyObject) {
        guard let castedData = data as? Data else {
            self.removeProgressView()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let addedBankIDResponse = try decoder.decode(AddedBankIDResponse.self, from: castedData)
            guard let msg = addedBankIDResponse.msg, msg.count > 0 else {
                self.removeProgressView()
                return
            }
            guard let commentData = msg.data(using: .utf8) else {
                self.removeProgressView()
                return
            }
            let addedBankIdList = try decoder.decode([AddedBankIDList].self, from: commentData)
            let bankIDLst = addedBankIdList.map { $0.id }
            self.clearAddedBanksFromServer(bankIDs: bankIDLst)
        } catch let error {
            println_debug(error.localizedDescription)
            self.removeProgressView()
        }
    }
    
    func clearAddedBanksFromServer(bankIDs: [String]) {
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            let urlStr   = Url.clearerAddedBanks
            let removeAddedBanksUrl = getUrl(urlStr: urlStr, serverType: .serverApp)
            let params = [String: String]()
            let encoder = JSONEncoder()
            do {
                let logn = ClearAddedBankLogin(appID: 50,
                                               mobileNumber: UserModel.shared.mobileNo,
                                               simID: UserModel.shared.simID)
                let request = ClearAddedBanksRequest(login: logn, bankIDS: bankIDs)
                let jsonData = try encoder.encode(request)
                web.genericClass(url: removeAddedBanksUrl, param: params as AnyObject, httpMethod: "POST", mScreen: "ClearAddedBanks", hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
           self.removeProgressView()
        }
    }
    
    func handleClearAddedBanksResponse(data: AnyObject) {
        guard let castedData = data as? Data else {
            self.removeProgressView()
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let clearBankResponse = try decoder.decode(ClearAddedBanksResponse.self, from: castedData)
            if let resCode = clearBankResponse.code, resCode == 200 {
                println_debug("List is cleared")
                UserDefaults.standard.set(true, forKey: "IsAlreadyAdvMer")
            } else {
                println_debug("Added Bank List is not cleared - \(clearBankResponse.msg ?? "")")
            }
            self.removeProgressView()
        } catch let error {
            println_debug(error.localizedDescription)
            self.removeProgressView()
        }
    }
}

extension SMApprovalSaleNLockViewController: BioMetricLoginDelegate {
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if isSuccessful {
            println_debug("Authorised")
        }
        else{
            if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
        }
    }
}


// MARK: - Additional Methods
extension SMApprovalSaleNLockViewController: NavigationSetUpProtocol {
    /// It set up the navigation bar
    func setUpNavigation() {
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: titleViewWidth, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        let ypos = (titleViewSize.height - 30 ) / 2
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text = "ApproveSaleNLock User".localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}

class SMApprovalSaleNLockViewCell: UITableViewCell {
    
    // MARK:- Outlets
    @IBOutlet weak var lblName: MarqueeLabel!{
        didSet{
            lblName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblMobileNumber: MarqueeLabel!{
        didSet{
            lblMobileNumber.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnStatus: UIButton!{
        didSet
        {
            self.btnStatus.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.btnStatus.setTitle(self.btnStatus.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var btnActiveInActive: UIButton!{
        didSet
        {
            self.btnActiveInActive.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.btnActiveInActive.setTitle(self.btnActiveInActive.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    
    @IBOutlet weak var cardView: CardDesignView!
    
    // MARK:- Button Actions
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK:- Methods
    func wrapData(name: String?, number: String?, status: Int?) {
        lblName.text = name ?? ""
        lblMobileNumber.text = number ?? ""
        var statusString = "Inactive".localized
        if let statusValue = status, statusValue == 1 {
            statusString = "Active".localized
        } else {
            statusString = "Inactive".localized
        }
        btnStatus.setTitle(statusString, for: .normal)
    }
}

struct SaleNLockUserListResponse: Codable {
    var msg: String?
    var code: Int?
    var userListHolder: String?
    private enum CodingKeys: String, CodingKey {
        case msg = "Msg"
        case code = "Code"
        case userListHolder = "Data"
    }
}

struct SaleNLockUserListHolder: Codable {
    var userList: [SaleNLockUserList]?
    
    private enum CodingKeys: String, CodingKey {
        case userList = "DummyProfile"
    }
}

struct SaleNLockUserList: Codable {
    var id: String?
    var name: String?
    var phoneNumber: String?
    var status: Int?
    
    private enum CodingKeys: String, CodingKey {
        case id = "ID"
        case name = "Name"
        case phoneNumber = "PhoneNumber"
        case status = "Status"
    }
}

struct ApproveSaleNLockUserResponse: Codable {
    var msg: String?
    var code: Int?
    var response: String?
    private enum CodingKeys: String, CodingKey {
        case msg = "Msg"
        case code = "Code"
        case response = "Data"
    }
}

struct AddedBankIDResponse: Codable {
    let code: Int
    let data, msg: String?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case msg = "Msg"
    }
}

struct AddedBankIDList: Codable {
    let id: String
    let assetID: String?
    let bankID: String?
    let bankName, bankBurmeseName, branchName, branchBurmeseName: String?
    let accountNumber, accountType, isActive, branchid: String?
    let idType, idNo, bankPhoneNumber, accountName: String?
    let accountModel, branchAddress, state, township: String?
    let beneficiaryMobNumber, divisionBurmeseName, townshipBurmeseName, remarks: String?
    let emailID, bankBrachBurmessAddress: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case assetID = "AssetId"
        case bankID = "BankId"
        case bankName = "BankName"
        case bankBurmeseName = "BankBurmeseName"
        case branchName = "BranchName"
        case branchBurmeseName = "BranchBurmeseName"
        case accountNumber = "AccountNumber"
        case accountType = "AccountType"
        case isActive = "IsActive"
        case branchid = "Branchid"
        case idType = "IdType"
        case idNo = "IdNo"
        case bankPhoneNumber = "BankPhoneNumber"
        case accountName = "AccountName"
        case accountModel = "AccountModel"
        case branchAddress = "BranchAddress"
        case state = "State"
        case township = "Township"
        case beneficiaryMobNumber = "\nBeneficiaryMobNumber"
        case divisionBurmeseName = "DivisionBurmeseName"
        case townshipBurmeseName = "TownshipBurmeseName"
        case remarks = "Remarks"
        case emailID = "EmailId"
        case bankBrachBurmessAddress = "BankBrachBurmessAddress"
    }
}

struct ClearAddedBanksRequest: Codable {
    let login: ClearAddedBankLogin
    let bankIDS: [String]
    
    enum CodingKeys: String, CodingKey {
        case login = "Login"
        case bankIDS = "BankIds"
    }
}

struct ClearAddedBankLogin: Codable {
    let appID: Int
    let mobileNumber, simID: String
    let msid = ""
    let ostype = 1
    let otp = ""
    let limit = 0, offset = 0
    
    enum CodingKeys: String, CodingKey {
        case appID = "AppId"
        case mobileNumber = "MobileNumber"
        case simID = "SimId"
        case msid = "Msid"
        case ostype = "Ostype"
        case otp = "Otp"
        case limit = "Limit"
        case offset = "Offset"
    }
}

struct ClearAddedBanksResponse: Codable {
    let code: Int?
    let data, msg: String?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case msg = "Msg"
    }
}

