//
//  getOTPViewController.swift
//  SettingVC
//
//  Created by Mohit on 10/19/17.
//  Copyright © 2017 Mohit. All rights reserved.
//

import UIKit

class SMGetOTPViewController: OKBaseController {
    
    let notificationCenter = NotificationCenter.default
    var getOtpDict = Dictionary<String, Any>()
    @IBOutlet var mobileNum: UILabel!{
        didSet
        {
            mobileNum.font = UIFont(name: appFont, size: appFontSize)
            mobileNum.text = mobileNum.text?.localized
        }
    }
    @IBOutlet var desclbl: UILabel!{
        didSet
        {
            desclbl.font = UIFont(name: appFont, size: appFontSize)
            desclbl.text = desclbl.text?.localized
        }
    }
    @IBOutlet weak var lblMobileNumber: UILabel!{
        didSet
        {
            lblMobileNumber.font = UIFont(name: appFont, size: appFontSize)
            lblMobileNumber.text = lblMobileNumber.text?.localized
        }
    }
    @IBOutlet weak var lblOTP: UILabel!{
        didSet
        {
            lblOTP.font = UIFont(name: appFont, size: appFontSize)
            lblOTP.text = lblOTP.text?.localized
        }
    }
    @IBOutlet var btnTryAgain: UIButton!{
        didSet{
            btnTryAgain.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnTryAgain.setTitle(btnTryAgain.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Get OTP".localized
        self.lblMobileNumber.text = self.getActualNum(UserModel.shared.mobileNo,withCountryCode: "+95")
        self.GetOTPApiCall()
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notificationCenter.addObserver(self,selector: #selector(SessionAlert), name: .appTimeout,   object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notificationCenter.removeObserver(self, name: .appTimeout, object: nil)
    }
    
    @objc func SessionAlert()  {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "", body: "Application Session expired login again".localized, img:  #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){
                if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func TryAgainClick(_ sender: UIButton){
        self.GetOTPApiCall()
    }
    
    func GetOTPApiCall() {
        
        if appDelegate.checkNetworkAvail() {
            showProgressView()
            let web = WebApiClass()
            web.delegate = self
        let urlS = String.init(format: Url.getOTP, UserModel.shared.mobileNo,UserModel.shared.simID,msid,"1",uuid,UserModel.shared.name)
           // let urlS = String.init(format: Url.getOTP, UserModel.shared.mobileNo,UserModel.shared.simID,UserModel.shared.msid,"1","",UserModel.shared.name)
            if let urlAbsolute = urlS.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                let url =   getUrl(urlStr: urlAbsolute, serverType: .serverApp)
                let param = [String:String]() as AnyObject
                web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "generateAuthCode")
            }
        }
        
    }
    
    
    
    func customAlert(msg:String, title:String, image:UIImage?) {
        
        alertViewObj.wrapAlert(title: "", body: msg.localized, img: image)
        alertViewObj.addAction(title: "Done".localized, style: .target, action:{})
        alertViewObj.showAlert(controller: self)
        
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            println_debug(child)
            self.getOtpDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
}


extension SMGetOTPViewController: BioMetricLoginDelegate {
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if isSuccessful {
            println_debug("Authorised")
        }
        else{
            if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
        }
    }
}




extension SMGetOTPViewController : WebServiceResponseDelegate{
    
    func webResponse(withJson json: AnyObject, screen: String) {
        
        if screen == "generateAuthCode"{
            do {
                if let data = json as? Data {
                    
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(dic)
                        if dic["Code"] as? Int == 200 {
                            if let login = dic["Data"] as? String, login != "" {
                                let dic = OKBaseController.convertToDictionary(text: login)
                                if let log = dic!["Table"] as? Array<Any> {
                                    if let logi = log.first as? Dictionary<String,Any> {
                                        if ((logi["OTP"] as? String) != nil){
                                            
                                            DispatchQueue.main.async {
                                                
                                                //self.customAlert(msg: (logi["OTP"] as? String)!, title: "")
                                                self.lblOTP.text = (logi["OTP"] as? String)!.localized
                                                
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if dic["Code"] as? Int == 300{
                            DispatchQueue.main.async {
                                self.customAlert(msg: (dic["Msg"] as? String)!.localized, title: "", image: #imageLiteral(resourceName: "get_otp_st"))
                                
                            }
                            
                        }
                    }
                }
                self.removeProgressView()
            } catch {
                
            }
        }
        
    }
    
    func GetOTParsing(data: AnyObject) {
        
        let responseString = String(data: data as! Data, encoding: .utf8)
        let xml            = SWXMLHash.parse(responseString!)
        println_debug(xml)
        self.enumerate(indexer: xml)
        
        if self.getOtpDict.keys.contains("resultdescription"){
            
            if self.getOtpDict["resultdescription"] as? String ?? "" == "Transaction Successful" || self.getOtpDict["resultdescription"] as? String ?? "" == "The OTP Not Found"{
                
                DispatchQueue.main.async{
                    let msg  = self.getOtpDict["resultdescription"] as? String ?? ""
                    self.customAlert(msg: msg.localized, title: "", image: #imageLiteral(resourceName: "get_otp_st"))
                    self.removeProgressView()
                }
            }
        }
    }
}
