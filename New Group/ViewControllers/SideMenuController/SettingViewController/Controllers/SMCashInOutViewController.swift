//
//  CashInOutViewController.swift
//  SettingVC
//
//  Created by Mohit on 10/16/17.
//  Copyright © 2017 Mohit. All rights reserved.
//

import UIKit
class SMCashInOutViewController: OKBaseController{
    
    var tblRateArr = [String]()
    var tblMaxCashArr = [String]()
    var dataStateForSelection = Bool()
    var selectedValue = Int()
    let notificationCenter = NotificationCenter.default
    fileprivate let cellIdentifier = "Cell"
    
    @IBOutlet weak var tblData: UITableView!
    @IBOutlet weak var tfTopPercent: UITextField!{
        didSet
        {
            self.tfTopPercent.font = UIFont(name: appFont, size: 17.0)
            tfTopPercent.placeholder = tfTopPercent.placeholder?.localized
            tfTopPercent.text = tfTopPercent.text?.localized
        }
    }
    @IBOutlet weak var tfTopRange: UITextField!{
        didSet
        {
            self.tfTopRange.font = UIFont(name: appFont, size: 17.0)
            tfTopRange.placeholder = tfTopRange.placeholder?.localized
            tfTopRange.text = tfTopRange.text?.localized
        }
    }
    @IBOutlet weak var tfDownRange: UITextField!{
        didSet
        {
            self.tfDownRange.font = UIFont(name: appFont, size: 17.0)
            tfDownRange.placeholder = tfDownRange.placeholder?.localized
            tfDownRange.text = tfDownRange.text?.localized
        }
    }
    @IBOutlet weak var tfDownPercent: UITextField!{
        didSet
        {
            self.tfDownPercent.font = UIFont(name: appFont, size: 17.0)
            tfDownPercent.placeholder = tfDownPercent.placeholder?.localized
            tfDownPercent.text = tfDownPercent.text?.localized
        }
    }
    @IBOutlet weak var listView : UIView?
    @IBOutlet weak var TopView: UIView!
    @IBOutlet weak var DownView: UIView!
    @IBOutlet weak var setBtn: UIButton!{
        didSet
        {
            self.setBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            setBtn.setTitle(setBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var lblCashIn: UILabel!{
        didSet
        {
            self.lblCashIn.font = UIFont(name: appFont, size: appFontSize)
            lblCashIn.text = lblCashIn.text?.localized
        }
    }
    @IBOutlet weak var lblCashOut: UILabel!{
        didSet
        {
            self.lblCashOut.font = UIFont(name: appFont, size: appFontSize)
            lblCashOut.text = lblCashOut.text?.localized
        }
    }
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        
        
        
        self.title = "Update Cash In | Out".localized
        tblData.isHidden = true
        TopView.isUserInteractionEnabled = false
        DownView.isUserInteractionEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.blue]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        notificationCenter.addObserver(self, selector: #selector(SessionAlert), name: .appTimeout, object: nil)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notificationCenter.removeObserver(self, name: .appTimeout, object: nil)
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    @objc func SessionAlert()  {
        DispatchQueue.main.async {
            
            alertViewObj.wrapAlert(title: "", body: "Application Session expired login again".localized, img:  #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){
                if UserLogin.shared.loginSessionExpired { OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    @objc func rightBtnClick(){
        
        self.lblCashIn.backgroundColor = UIColor(red: 253.0/255.0, green: 185.0/255.0, blue: 9.0/255.0, alpha: 1.0)
        self.lblCashOut.backgroundColor = UIColor(red: 253.0/255.0, green: 185.0/255.0, blue: 9.0/255.0, alpha: 1.0)
        self.setBtn.isHidden = false
        self.view.isUserInteractionEnabled = true
        
    }
    
    @IBAction func editClick(_ sender: UIBarButtonItem) {
        
        self.lblCashIn.backgroundColor = UIColor(red: 253.0/255.0, green: 185.0/255.0, blue: 9.0/255.0, alpha: 1.0)
        self.lblCashOut.backgroundColor = UIColor(red: 253.0/255.0, green: 185.0/255.0, blue: 9.0/255.0, alpha: 1.0)
        self.setBtn.isHidden = false
        self.view.isUserInteractionEnabled = true
        
    }
    
    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func cashInOutClick(_ sender: UIButton) {
        if sender.tag == 100{
            if sender.isSelected{
                sender.isSelected = false
                TopView.isUserInteractionEnabled = false
            }else{
                sender.isSelected = true
                TopView.isUserInteractionEnabled = true
            }
        }
            
        else{
            if sender.isSelected{
                sender.isSelected = false
                DownView.isUserInteractionEnabled = false
            }else{
                sender.isSelected = true
                DownView.isUserInteractionEnabled = true
            }
        }
        
    }
    
    
    
    func initalizeArray(stateForSelection:Bool){
        tblData.isHidden = false
        dataStateForSelection = stateForSelection
        if stateForSelection == true{
            tblRateArr = [
                "Select Rate Cash-In %".localized,
                "1%".localized,
                "2%".localized,
                "3%".localized,
                "4%".localized,
                "5%".localized,
                "6%".localized,
                "7%".localized,
                "8%".localized,
                "9%".localized,
                "10%".localized,
                "11%".localized,
                "12%".localized,
                "13%".localized,
                "14%".localized,
                "15%".localized
            ]
            
        }
        else{
            
            tblMaxCashArr = [
                "Maximum Cash In Amount".localized,
                "0-10,000".localized,
                "0-100,000".localized,
                "0-1,00,0000".localized,
                "1,00,0000-Above".localized
            ]
        }
        self.tblData.reloadData()
    }
    
    
    @IBAction func setClick(_ sender: UIButton){
        if self.tfTopPercent.text == ("Select Rate Cash-In %").localized ||  self.tfTopRange.text == ("Maximum Cash In Amount").localized{
            self.customAlert(msg: "Select in Cash-In".localized)
        }
        else if self.tfDownRange.text == ("Select Rate Cash-In %").localized ||  self.tfDownPercent.text == ("Maximum Cash In Amount").localized{
            self.customAlert(msg: "Select in Cash-Out".localized)
        }
        else{
            self.CashInOutApiCall()
        }
        
    }
    
    
    func customAlert(msg:String){
        alertViewObj.wrapAlert(title: "", body: msg.localized, img: #imageLiteral(resourceName: "setting"))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {
        })
        alertViewObj.showAlert(controller: self)
    }
    
    
    @IBAction func topPercentSelectionClick(_ sender: UIButton){
        
        selectedValue = 0
        tblData.frame = CGRect(x: 0.0, y: 182.0, width: 200.0, height: 305.0)
        self.initalizeArray(stateForSelection: true)
    }
    
    
    @IBAction func topRangeSelectionClick(_ sender: UIButton){
        
        selectedValue = 1
        tblData.frame = CGRect(x: 0.0, y: 237.0, width: 200.0, height: 305.0)
        self.initalizeArray(stateForSelection: false)
    }
    
    
    
    @IBAction func downPercentSelectionClick(_ sender: UIButton){
        
        selectedValue = 2
        tblData.frame = CGRect(x: 0.0, y: 403.0, width: 200.0, height: 305.0)
        self.initalizeArray(stateForSelection: true)
    }
    
    
    @IBAction func downRangeSelectionClick(_ sender: UIButton){
        
        
        selectedValue = 3
        tblData.frame = CGRect(x: 0.0, y: 457.0, width: 200.0, height: 305.0)
        self.initalizeArray(stateForSelection: false)
    }
    
    
    
    
    
    func CashInOutApiCall(){
        
        let params = LoginParams.getParamsCheckInOut(inamount: tfTopRange.text!, outamount: tfDownRange.text!, inrateamount: tfTopPercent.text!, outrateamount: tfDownPercent.text!, preObj: UserModel.shared.mobileNo as NSString)
        println_debug(params)
        
        if appDelegate.checkNetworkAvail(){
            
            let web = WebApiClass()
            web.delegate = self
            self.showProgressView()
            let urlS = String.init(format: Url.CashInCashOut)
            let url =   getUrl(urlStr: urlS, serverType: .serverApp)
            let param = params
            web.genericClass(url: url, param: param as AnyObject, httpMethod: "POST", mScreen: "CashInCashOut")
        }
        else{
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
        }
    }
}


extension SMCashInOutViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        var name = String()
        if dataStateForSelection == true{
            name  = tblRateArr[indexPath.row]
        }
        else{
            name  = tblMaxCashArr[indexPath.row]
        }
        cell.textLabel?.font = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: 17)
        cell.textLabel?.text = name
       // cell.textLabel?.font = UIFont(name: appFont, size: 17) ?? UIFont.systemFont(ofSize: 17)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if dataStateForSelection == true {
            return tblRateArr.count
        }
        else{
            return tblMaxCashArr.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        tblData.isHidden = true
        switch selectedValue{
        case 0:
            self.tfTopPercent.text = tblRateArr[indexPath.row]
            break
            
        case 1:
            self.tfTopRange.text = tblMaxCashArr[indexPath.row]
            break
            
        case 2:
            self.tfDownPercent.text = tblRateArr[indexPath.row]
            break
            
        default:
            self.tfDownRange.text = tblMaxCashArr[indexPath.row]
            break
        }
    }
    
}



extension SMCashInOutViewController : WebServiceResponseDelegate{
    
    func webResponse(withJson json: AnyObject, screen: String){
        if screen == "CashInCashOut"
        {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(dic)
                        
                        
                        if dic["Msg"] as? String ?? "" == "The OTP Not Found" || dic["Msg"] as? String ?? "" == "Success"{
                            
                            DispatchQueue.main.async {
                                let msg  = dic["Msg"] as? String ?? ""
                                self.customAlert(msg: msg.localized)
                                self.removeProgressView()
                            }
                        }
                    }
                }
            } catch {
                
            }
        }
    }
    
    
    
    func CashInOutParsing(data: AnyObject){
        
        let responseString = String(data: data as! Data, encoding: .utf8)
        let xml            = SWXMLHash.parse(responseString!)
        println_debug(xml)
        
    }
    
}

extension SMCashInOutViewController: BioMetricLoginDelegate {
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if isSuccessful {
            println_debug("Authorised")
        }
        else{
            if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
        }
    }
}
