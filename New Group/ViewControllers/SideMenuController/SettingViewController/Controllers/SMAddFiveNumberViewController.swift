
//  AddFiveNumberViewController.swift
//  SettingVC

//  Created by Mohit on 10/20/17.
//  Copyright © 2017 Mohit. All rights reserved.


import UIKit


struct MultiTopUpSection {
    var name: String!
    var customerName: String = ""
    var customerMb: String = ""
    var rows: NSMutableArray!
    var planType:String = ""
    var isSelectContact:Bool = false
    
    init(name: String, rows: NSMutableArray){
        self.name = name
        self.rows = rows
    }
}


class SMAddFiveNumberViewController: OKBaseController {
    
    @IBOutlet weak var ScrollerView: UIScrollView!
    @IBOutlet weak var constraintBottom_ivAdv: NSLayoutConstraint!
    @IBOutlet weak var tv: UITableView!
    @IBOutlet weak var btnPlus: UIButton!{
        didSet
        {
            self.btnPlus.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.btnPlus.setTitle(self.btnPlus.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var btnSubmit: UIButton!{
        didSet
        {
            self.btnSubmit.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.btnSubmit.setTitle(self.btnSubmit.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var constraintHeight_ivAdv: NSLayoutConstraint!
    
    let notificationCenter = NotificationCenter.default
    let SectionHeaderHeight: CGFloat = 25
    let dictResponse = NSMutableDictionary()
    var NofItem = 0
    var state = Bool()
    var sections = [MultiTopUpSection]()
    var tfFalse:UITextField = UITextField.init()
    var tblList = [String]()
    var sectionList = [String]()
    var defaultColor = UIColor(red: 247.0 / 255.0, green: 196.0 / 255.0, blue: 39.0 / 255.0, alpha: 1)
    var isShowingKeyBoard:Bool = false
    var isDeletedSection = false
    var AddFiveNumberDict = Dictionary<String, Any>()
    var topConstraint : NSLayoutConstraint?
    var height = 0.0
    var submitButtonConstraint: NSLayoutConstraint!
    var plusButtonConstraint: NSLayoutConstraint!
    var totalMobileNumbers = 0
    let validObj = PayToValidations()
    var rowssss: NSMutableArray!
    var isContactSelected:Bool = false
    var comeFirstTIme:Bool = true
    var isShowPlusButton:Bool = false
    var isBothNumbersEntered:Bool = false
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        
        self.initload()
    }
    
    func initload(){
        // Do any additional setup after loading the view.
        self.title = "Add 5 OK$ Numbers".localized
        //        sections.append(MultiTopUpSection.init(name: "Recharge 1", rows:self.addNewRowInSection(rows: NSMutableArray())))
        //        let deadlineTime = DispatchTime.now() + .milliseconds(3)
        //        println_debug(sections)
        //        println_debug(sections.count)
        //        println_debug(sections[0].rows)
        //        println_debug(sections[0].rows.count)
        rowssss = []
        self.loadefaultData()
        
        self.tfFalse.resignFirstResponder()
        
        //        DispatchQueue.main.asyncAfter(deadline: deadlineTime){
        //            println_debug(self)
        //            self.loadefaultData()
        //        }
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        submitButtonConstraint.constant = 0 - keyboardHeight
        plusButtonConstraint.constant = -64 - keyboardHeight
        isShowingKeyBoard = true
        //        self.setAdv(isBackPressed: false)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        submitButtonConstraint.constant = 0
        plusButtonConstraint.constant = 64
        isShowingKeyBoard = false
        //        self.setAdv(isBackPressed: false)
    }
    
    
    func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func loadefaultData() {
        self.tfFalse.resignFirstResponder()
        self.btnSubmit.isHidden = true
        if let decoded = userDef.object(forKey: "msnidDetails") as? Data {
            let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded as Data) as! [MsnIdDetail]
            //            println_debug("sssssss",decodedTeams[0].mobileNumber)
            for obj in decodedTeams {
                let anotherDict = obj.mobileNumber
                if anotherDict == "" {
                    self.btnPlus.isHidden = true
                    self.btnSubmit.isHidden = true
                    sections.append(MultiTopUpSection.init(name: "Recharge 1".localized, rows:self.addNewRowInSection(rows: NSMutableArray())))
                    DispatchQueue.main.async {
                        self.tv.reloadData()
                    }
                    return
                }
            }
            
            totalMobileNumbers = decodedTeams.count
            if totalMobileNumbers > 0 {
                self.btnPlus.isHidden = false
                self.btnSubmit.isHidden = true
            }
            else{
                self.btnPlus.isHidden = true
                self.btnSubmit.isHidden = true
            }

            for obj in decodedTeams {
                let anotherDict = obj.mobileNumber
                sections.append(MultiTopUpSection.init(name: "Recharge 1", rows:rowssss))
                self.sections[sections.count - 1].customerName = anotherDict
                self.sections[sections.count - 1].customerMb = anotherDict
                self.sections[sections.count - 1].rows[0] = anotherDict
            }
            self.reloadTV2AddNewRow(indexPath: IndexPath.init(row: 1, section: self.sections.count - 1), willReloadTv: true)
            
            if self.sections.count > 4 {
                self.btnPlus.isHidden = true
            }
        }
        else {
            self.btnPlus.isHidden = true
            self.btnSubmit.isHidden = true
            sections.append(MultiTopUpSection.init(name: "Recharge 1".localized, rows:self.addNewRowInSection(rows: NSMutableArray())))
            DispatchQueue.main.async {
                self.tv.reloadData()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let cell:SMTopUpMultiCell1 = self.tv.cellForRow(at: IndexPath(row: 0, section: 0)) as? SMTopUpMultiCell1 {
            DispatchQueue.main.async {
                cell.tfMb.becomeFirstResponder()
            }
        }
    }
    
    func checkNumberAlreadyExist(mobileStr: String) -> Bool{
        var statusValue = false
        
        if let decoded = userDef.object(forKey: "msnidDetails") as? Data {
            let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded as Data) as! [MsnIdDetail]
            //            println_debug("sssssss",decodedTeams[0].mobileNumber)
            
            for obj in decodedTeams {
                let value = obj
                let anotherDict = value.mobileNumber
                if mobileStr == anotherDict {
                    statusValue =  true
                }
            }
        }
        return statusValue
    }
}

extension SMAddFiveNumberViewController {
    
    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //    @objc func keyboardWillAppear(notification: NSNotification) {
    //        isShowingKeyBoard = true
    //        self.setAdv(isBackPressed: false)
    //        let keyboardSize:CGSize = (notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue.size
    //        height = Double(min(keyboardSize.height, keyboardSize.width))
    //        topConstraint = NSLayoutConstraint(
    //            item: btnPlus,
    //            attribute: NSLayoutAttribute.top,
    //            relatedBy: NSLayoutRelation.equal,
    //            toItem: self.view,
    //            attribute: NSLayoutAttribute.top,
    //            multiplier: 1,
    //            constant: CGFloat(Int(height) + Int(btnPlus.frame.size.height))
    //        )
    //        //self.view.addConstraint(topConstraint!)
    //    }
    
    //    @objc func keyboardWillDisappear() {
    //        isShowingKeyBoard = false
    //        self.setAdv(isBackPressed: false)
    //        //self.view.removeConstraint(topConstraint!)
    //    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        notificationCenter.addObserver(self,selector: #selector(SessionAlert), name: .appTimeout, object: nil)
        
        submitButtonConstraint = btnSubmit.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        plusButtonConstraint = btnPlus.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 64)
        submitButtonConstraint.isActive = true
        plusButtonConstraint.isActive = true
        submitButtonConstraint.constant = 0
        subscribeToShowKeyboardNotifications()
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: Notification.Name.UIKeyboardWillHide, object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: Notification.Name.UIKeyboardWillShow, object: nil)
        // if TopUpDefines.sharedInstance.isBackFromConfirmationPage == t
        let deadlineTime = DispatchTime.now() + .milliseconds(3)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            self.setAdv(isBackPressed: false)
        }
        self.setLng()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        notificationCenter.removeObserver(self, name: .appTimeout, object: nil)
    }
    
    @objc func SessionAlert()  {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "", body: "Application Session expired login again".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){
                if UserLogin.shared.loginSessionExpired {
                    OKPayment.main.authenticate(screenName: "SMAddFiveNumberViewController", delegate: self as BioMetricLoginDelegate)
                }
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    //    override func viewWillDisappear(_ animated: Bool) {
    //        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardWillHide, object: nil)
    //    }
    
    func setLng() {
        self.btnSubmit.setTitle("Submit".localized, for: .normal)
    }
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func AddClick(_ sender: UIButton) {
        if NofItem < 5 {
            let customView = Bundle.main.loadNibNamed("SMAddFiveView", owner: self, options: nil)?[0] as! SMAddFiveView
            customView.frame = CGRect(x: 0, y: (NofItem*160)+2, width: 375, height: 160)
            self.ScrollerView.contentSize = CGSize(width: customView.frame.size.width, height: 800.0)
            self.ScrollerView.addSubview(customView)
        }
        NofItem = NofItem + 1
    }
    
    func updateColor(mb:String) {
        // set default value
        var operatorName = ""
        var color = defaultColor
        if mb.count > 2 {
            color = UIColor.black
        }
        if self.sections.count > 1 {
            color = defaultColor
            operatorName = ""
        }
        println_debug(color)
        let lastSectionIndex = self.sections.count - 1
        if let cell1:SMTopUpMultiCell1 = self.tv.cellForRow(at: IndexPath.init(row: 0, section:lastSectionIndex)) as? SMTopUpMultiCell1 {
            if mb.count > 2 {
                operatorName = validObj.getNumberRangeValidation(mb).operator
            }
            cell1.lblOperatorName.text = operatorName.localized
            cell1.lblOperatorName.textColor = UIColor.gray
        }
    }
    
    func deleteTvLastRow() {
        let sectionCount = self.sections.count - 1;
        self.sections[sectionCount].rows.removeLastObject()
        let lastSectionRowCount = (self.sections[sectionCount].rows.count)
        self.tv.beginUpdates()
        self.tv.deleteRows(at: [IndexPath(row: lastSectionRowCount , section: sectionCount)], with: .fade)
        self.tv.endUpdates()
        //        self.tv.scrollToRow(at: IndexPath(row: 0, section: sectionCount), at: UITableViewScrollPosition.bottom, animated: false)
    }
    
    func setAdv(isBackPressed:Bool) {
        let deadlineTime = DispatchTime.now() + .milliseconds(2)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            if self.sections.count == 1 {
                let cell1:SMTopUpMultiCell1 = self.tv.cellForRow(at: IndexPath.init(row: 0, section:0)) as! SMTopUpMultiCell1
                let tfText = cell1.tfMb.text
                if isBackPressed == true {
                    self.calculateADVtextCount(count: 5, tfText: tfText!)
                } else {
                    self.calculateADVtextCount(count: 3, tfText: tfText!)
                }
            }
        }
    }
    
    func calculateADVtextCount(count:Int, tfText:String) {
        if tfText.count < count{
            
        }
    }
    
    func reloadTV2AddNewRow(indexPath:IndexPath, willReloadTv:Bool) -> Void {
        //        self.btnPlus.isHidden = false
        var rows = self.sections[indexPath.section].rows;
        //let lastRowValue = self.sections[indexPath.section].rows[(rows?.count)! - 1] as! String ?? ""
        if (rows?.count == 1)  {
            //add one more row
            rows =  self.addNewRowInSection(rows: rows!)
            self.sections[indexPath.section].rows = rows
            if (rows?.count == 2) {
                //                if totalMobileNumbers > 1 {
                //                    self.btnPlus.isHidden = false
                //                    self.btnSubmit.isHidden = true
                //                }
                //                else{
                //                    self.btnPlus.isHidden = true
                //                    self.btnSubmit.isHidden = true
                //                }
                // show plus btn
                //                if comeFirstTIme {
                //                    self.btnPlus.isHidden = false
                //                    comeFirstTIme = false
                //                }
                //                else {
                //                    self.btnPlus.isHidden = true
                //                }
            }
        }
        else if(rows?.count == 2) {
            //add one more section and 1 row in new section
            if indexPath.section != 4 {
                //create  new row
                if !isContactSelected {
                    let row = self.addNewRowInSection(rows: NSMutableArray())
                    let title = "Recharge " + String(indexPath.section + 2)
                    //add in section
                    let section = MultiTopUpSection.init(name: title , rows:row)
                    self.sections.append(section)
                }
            }
        }
        if willReloadTv {
            self.tv.reloadData()
        } else {
            let sectionCount = self.sections.count - 1;
            let lastSectionRowCount = (self.sections[sectionCount].rows.count - 1)
            self.tv.beginUpdates()
            self.tv.insertRows(at: [IndexPath(row: lastSectionRowCount , section: sectionCount)], with: .fade)
            self.tv.endUpdates()
        }
        //        self.tv.setContentOffset( CGPoint(x: 0, y: 10 * (self.sections.count - 1)) , animated: true)
        //        let customView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        //        self.tv.tableFooterView = customView
    }
    
    func addNewRowInSection(rows:NSMutableArray) -> NSMutableArray {
        if (rows.count == 0) || (rows.count == 1) {
            rows.insert("09".localized, at:rows.count)
        }
        else {
            rows.insert("Enter or Select Amount".localized, at: rows.count)
        }
        return rows
    }
    
    @IBAction func onClickPlusBtn(_ sender: Any) {
        self.btnPlus.isHidden = true
        isContactSelected = false
        //        isShowPlusButton = true
        isBothNumbersEntered = false
        if self.sections.count > 0 {
            if (self.sections[self.sections.count - 1].rows.count == 2)  && (String(describing: self.sections[self.sections.count - 1].rows[1]) != "Enter or Select Amount"){
                // add one more section
                self.btnSubmit.isHidden = true
                self.reloadTV2AddNewRow(indexPath: IndexPath.init(row: 0, section:self.sections.count - 1), willReloadTv: true)
                self.btnPlus.isHidden = true
            } else {
                let totSection = self.sections.count
                // set confirm mb name to section row
                if self.sections[totSection - 1].rows.count == 2 {
                    let cell2:SMTopUpMultiCell2 = self.tv.cellForRow(at: IndexPath.init(row: 1, section:totSection - 1)) as! SMTopUpMultiCell2
                    self.sections[totSection - 1].rows[1] = cell2.tfConfirmMb.text!
                }
                self.tfFalse.resignFirstResponder()
            }
        } else {
            //show initial view
            self.initload()
            self.tfFalse.resignFirstResponder()
        }
        return
    }
    
    func AddFiveNumberApiCall(paramDict: Dictionary<String,Any>) {
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let web = WebApiClass()
            web.delegate = self
            let urlS = String.init(format: Url.addFiveNumber)
            let url =   getUrl(urlStr: urlS, serverType: .serverApp)
            let param = paramDict
            web.genericClass(url: url, param: param as AnyObject , httpMethod: "POST", mScreen: "AddFiveNumber")
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            self.AddFiveNumberDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    @IBAction func btnSubmit(_ sender: UIButton) {
        self.view.endEditing(true)
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "SMAddFiveNumberViewController", delegate: self)
        } else {
            self.callAfterAuthenticate()
        }
    }
    
    func callAfterAuthenticate() {
        var data = Array<AnyObject>()
        var value = String()
        for object in sections {
            if object.customerMb == "" {
                value = object.rows[0] as? String ?? ""
                value.insert("0", at: value.startIndex)
                value.insert("9", at: value.index(value.startIndex, offsetBy: 2))
                value.insert("5", at: value.index(value.startIndex, offsetBy: 3))
                if checkNumberAlreadyExist(mobileStr: value) {
                    self.customAlert(msg: "Please enter unique OK$ number".localized, title: "")
                    return
                }
            }
            else {
                value = object.customerMb
//                if checkNumberAlreadyExist(mobileStr: value) {
//                    self.customAlert(msg: "Please enter unique OK$ number".localized, title: "")
//                    return
//                }
            }
            
            if !(value.contains(find: "0095")) {
                if value.contains(find: "+95") {
                    value = value.replacingOccurrences(of: "+95", with: "0095")
                }
                else {
                    value.insert("0", at: value.startIndex)
                    value.insert("9", at: value.index(value.startIndex, offsetBy: 2)) // prints hel!lo
                    value.insert("5", at: value.index(value.startIndex, offsetBy: 3))
                }
            }
            
            //            var strCheck = sections[sections.count-1].rows[0] as? String ?? ""
            //            strCheck.insert("0", at: value.startIndex)
            //            strCheck.insert("9", at: value.index(value.startIndex, offsetBy: 2))
            //            strCheck.insert("5", at: value.index(value.startIndex, offsetBy: 3))
            //            println_debug("stststtsts",strCheck)
            //            if checkNumberAlreadyExist(mobileStr: strCheck){
            //                self.customAlert(msg: "Please enter unique OK$ number".localized, title: "")
            //                return
            //            }
            
            if value.count > 2 {
                let localdict = NSMutableDictionary()
                localdict["Countrycode"] = "+95"
                localdict["Msisidn"] = value
                data.append(localdict)
            }
        }
        
        if data.count > 2 {
            
        }
        
        for index in 0..<data.count {
            let dict = data[index]
            let number = dict["Msisidn"] as! String
            for index2 in 0..<data.count {
                let dict = data[index2]
                let num = dict["Msisidn"] as! String
                if index != index2 {
                    if num == number {
                        self.customAlert(msg: "Please enter unique OK$ number".localized, title: "")
                        return
                    }
                }
            }
        }
        
        dictResponse["Address1"] = UserModel.shared.address1.localized
        dictResponse["BusinessCategory"] = UserModel.shared.businessCate.localized
        dictResponse["BusinessType"] = UserModel.shared.businessType.localized
        dictResponse["DateOfBirth"] = UserModel.shared.dob.localized
        dictResponse["EmailId"] = UserModel.shared.email.localized
        dictResponse["Father"] = UserModel.shared.fatherName.localized
        dictResponse["Gender"] = UserModel.shared.gender.localized
        dictResponse["LType"] = "".localized
        dictResponse["Language"] = UserModel.shared.language.localized
        dictResponse["MerBenefNumberList"] = data
        dictResponse["MobileNumber"] = UserModel.shared.mobileNo.localized
        dictResponse["NRC"] = UserModel.shared.nrc.localized
        dictResponse["Name"] = UserModel.shared.name.localized
        dictResponse["OSType"] = UserModel.shared.osType.localized
        dictResponse["Password"] = ok_password?.localized
        dictResponse["Phone"] = UserModel.shared.phoneNumber.localized
        dictResponse["SecureToken"] = UserLogin.shared.token.localized
        dictResponse["State"] = UserModel.shared.state.localized
        dictResponse["Township"] = UserModel.shared.township.localized
        println_debug(data)
        println_debug(dictResponse)
        println_debug(UserLogin.shared.token)
        self.AddFiveNumberApiCall(paramDict: dictResponse as! Dictionary<String, Any>)
    }
    
    func customAlert(msg:String, title:String) {
        alertViewObj.wrapAlert(title: "", body: msg, img: #imageLiteral(resourceName: "setting"))
        alertViewObj.addAction(title: "OK".localized, style: .target, action:{})
        alertViewObj.showAlert(controller: self)
    }
}

extension SMAddFiveNumberViewController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section < totalMobileNumbers {
            if indexPath.row == 1 {
                return 0
            }
        }
        if indexPath.row == 0 {
            return 110.0
        } else {
            return 60.0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let rowItem = self.sections[indexPath.section].rows[indexPath.row]
        let rowCount = self.sections[indexPath.section].rows.count
        let sectionCount = self.sections.count
        
        if indexPath.row == 0 {
            let cell:SMTopUpMultiCell1 = tableView.dequeueReusableCell(withIdentifier: "SMTopUpMultiCell1", for: indexPath) as! SMTopUpMultiCell1
            cell.btnCross.isHidden = true
            
            if self.sections[indexPath.section].customerMb.count > 2 {
                let s = self.sections[indexPath.section].customerMb
                let result = s.replacingOccurrences(of: "0095", with: "0")
                let operatorName = validObj.getNumberRangeValidation(result).operator
                cell.lblOperatorName.text = operatorName.localized
                cell.tfMb.text = result
            } else {
                let operatorName = validObj.getNumberRangeValidation(rowItem as! String).operator
                cell.lblOperatorName.text = operatorName.localized
                cell.tfMb.text = String(describing: rowItem)
            }
            cell.parentVC = self
            cell.indexPath = indexPath
            if rowCount == 1 {
                //as only sigel row
                cell.btnCross.isHidden = true
            } else if indexPath.section + 1 < sectionCount {
                //have more section in its below
                cell.btnCross.isHidden = true
            } else {
                //   last section
                //                let tfCharsCount = cell.tfMb.text?.count
                //                if tfCharsCount == 2 {
                //                    cell.btnCross.isHidden = true
                //                } else {
                //                    cell.btnCross.isHidden = false
                //                }
            }
            //header  height
            if self.sections.count == 1 {
                // cell.layoutConstraintHeight_rechargeHeader.constant = 0
            } else {
                // cell.layoutConstraintHeight_rechargeHeader.constant = 45
            }
            //header value
            // cell.lblTitle.text = app_delegate.getlocaLizationLanguage("Recharge") +  " " + String(indexPath.section + 1)
            cell.lblTitle.text = ("OK$ Number".localized + String(indexPath.section + 1)).localized
            if self.sections[indexPath.section].rows.count > 1 {
                //                let s = self.sections[indexPath.section].customerMb
                //                let result = s.replacingOccurrences(of: "0095", with: "0")
                //                let operatorName = validObj.getNumberRangeValidation(rowItem as! String).operator
                // let color = self.updateColorsBasedOnOperator(operatorName: operatorName)
                let color = UIColor.gray
                //                cell.lblOperatorName.text = operatorName.localized
                // cell.lblOperatorName.textColor = color.0
                cell.lblOperatorName.textColor = color
            } else {
                cell.lblOperatorName.text = ""
            }
            // have below cell
            if totalMobileNumbers > 0 {
                if (indexPath.section < totalMobileNumbers) {
                    cell.btnDelete.isHidden = true
                }
                else {
                    cell.btnDelete.isHidden = false
                }
            }
            else {
                if self.sections.count == 1 {
                    if sections[indexPath.section].isSelectContact {
                        cell.btnDelete.isHidden = false
                    } else {
                        cell.btnDelete.isHidden = true
                    }
                    //                    cell.btnDelete.isHidden = true
                }
                else {
                    cell.btnDelete.isHidden = false
                }
            }
            
            if self.sections.count == totalMobileNumbers {
                if (indexPath.section) < self.sections.count {
                    println_debug("SHOBHIT \(indexPath.section)")
                    cell.btnContact.isHidden = true
                    cell.btnCross.isHidden = true
                    cell.btnContact.isHidden = true
                    cell.tfMb.isUserInteractionEnabled = false
                    cell.btnCross.isUserInteractionEnabled = false
                    self.btnSubmit.isHidden = true
                }
                else {
                    cell.contentView.isUserInteractionEnabled = true
                    cell.tfMb.isUserInteractionEnabled = true
                    cell.btnCross.isUserInteractionEnabled = true
                    cell.btnContact.isUserInteractionEnabled = true
                    cell.btnContact.isHidden = false
                    cell.btnCross.isHidden = false
                }
            }
            else {
                if (indexPath.section) < self.sections.count - 1 {
                    cell.btnContact.isHidden = true
                    cell.btnCross.isHidden = true
                    cell.btnContact.isHidden = true
                    cell.tfMb.isUserInteractionEnabled = false
                    cell.btnCross.isUserInteractionEnabled = false
                }
                else {
                    println_debug("ggggggg \(indexPath.section)")
                    if sections[indexPath.section].isSelectContact {
                        //                        if indexPath.section == 0 {
                        //                            cell.btnDelete.isHidden = false
                        //                        }
                        cell.tfMb.isUserInteractionEnabled = false
                        cell.btnContact.isUserInteractionEnabled = true
                        cell.btnContact.isHidden = true
                        cell.btnCross.isHidden = true
                    } else {
                        cell.contentView.isUserInteractionEnabled = true
                        cell.tfMb.isUserInteractionEnabled = true
                        cell.btnCross.isUserInteractionEnabled = true
                        cell.btnContact.isUserInteractionEnabled = true
                        cell.btnContact.isHidden = false
                        if cell.tfMb.text!.count > 2 {
                            cell.btnCross.isHidden = false
                        } else {
                            cell.btnCross.isHidden = true
                        }
                    }
                }
            }
            
            //section 5
            if  (self.sections.count == 5 && self.sections[4].rows.count > 2 &&  self.sections[4].rows[2] as? String ?? "" != "Enter or Select Amount") {
                cell.btnCross.isHidden = true
                cell.btnContact.isHidden = true
                cell.tfMb.isUserInteractionEnabled = false
                cell.btnCross.isUserInteractionEnabled = false
                cell.btnContact.isUserInteractionEnabled = false
                if totalMobileNumbers  > indexPath.row {
                    cell.btnDelete.isUserInteractionEnabled = false
                }
                else {
                    cell.btnDelete.isUserInteractionEnabled = true
                }
            }
            self.tfFalse = cell.tfMb
            self.tfFalse.resignFirstResponder()
            cell.tfMb.resignFirstResponder()
            
            return cell
        }
        else if indexPath.row == 1 {
            if (indexPath.section < totalMobileNumbers) {
                let cell:SMTopUpBlankMultiCell = tableView.dequeueReusableCell(withIdentifier: "SMTopUpBlankMultiCell", for: indexPath) as! SMTopUpBlankMultiCell
                return cell
            }
            else {
                let cell:SMTopUpMultiCell2 = tableView.dequeueReusableCell(withIdentifier: "SMTopUpMultiCell2", for: indexPath) as! SMTopUpMultiCell2
                if self.sections[indexPath.section].customerMb.count > 2 {
                    cell.tfConfirmMb.text = self.sections[indexPath.section].customerName
                    cell.ivConfirmMb.image = UIImage.init(named: "other_number")
                    cell.btnCross.isHidden = false
                } else {
                    cell.tfConfirmMb.text = String(describing: rowItem)
                    cell.ivConfirmMb.image = UIImage.init(named: "other_number")
                    cell.btnCross.isHidden = true
                }
                cell.parentVC = self
                cell.indexPath = indexPath
                //  cell.contentView.dropShadow()
                if rowCount == 2 {
                    //as only 2 rows
                    cell.btnCross.isHidden = true
                } else if indexPath.section + 1 < sectionCount {
                    //have more section in its below
                    cell.btnCross.isHidden = true
                } else {
                    cell.btnCross.isHidden = false
                }
                
                if self.sections.count == totalMobileNumbers {
                    if (indexPath.section) < self.sections.count {
                        cell.contentView.isUserInteractionEnabled = false
                        cell.btnCross.isHidden = true
                    } else {
                        cell.contentView.isUserInteractionEnabled = true
                        cell.tfConfirmMb.isUserInteractionEnabled = true
                        cell.btnCross.isHidden = false
                    }
                }
                else {
                    if (indexPath.section) < self.sections.count - 1 {
                        cell.contentView.isUserInteractionEnabled = false
                        cell.btnCross.isHidden = true
                    } else {
                        if sections[indexPath.section].isSelectContact {
                            cell.contentView.isUserInteractionEnabled = false
                            cell.btnCross.isHidden = true
                        } else {
                            cell.contentView.isUserInteractionEnabled = true
                            cell.tfConfirmMb.isUserInteractionEnabled = true
                            if cell.tfConfirmMb.text!.count > 2 {
                                cell.btnCross.isHidden = false
                            } else {
                                cell.btnCross.isHidden = true
                            }
                        }
                    }
                }
                
                if sections[indexPath.section].isSelectContact {
                    cell.tfConfirmMb.placeholder = "Name".localized
                    cell.ivConfirmMb.image = UIImage(named: "name_bill")
                } else {
                    cell.tfConfirmMb.placeholder = "Confirm Mobile Number".localized
                    cell.ivConfirmMb.image = UIImage(named: "other_number")
                }
                
                //                if (indexPath.section + 1) < self.sections.count {
                //                    // not touchable
                //                    cell.contentView.isUserInteractionEnabled = false
                //                    cell.btnCross.isHidden = true
                //                } else {
                //                    cell.contentView.isUserInteractionEnabled = true
                //                    cell.btnCross.isHidden = false
                //                }
                //section 5
                //                if  (self.sections.count == 5 && self.sections[4].rows.count > 2 &&  self.sections[4].rows[2] as! String ?? "" != "Enter or Select Amount") {
                //                    cell.contentView.isUserInteractionEnabled = false
                //                    cell.btnCross.isHidden = true
                //                }
                self.tfFalse = cell.tfConfirmMb
                self.tfFalse.resignFirstResponder()
                cell.tfConfirmMb.resignFirstResponder()
                return cell
            }
        }
        else {
            let cell:SMTopUpMultiCell1 = tableView.dequeueReusableCell(withIdentifier: "SMTopUpMultiCell1", for: indexPath) as! SMTopUpMultiCell1
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2 {
            
        }
    }
}


extension SMAddFiveNumberViewController : WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        //        self.generateAuthCodeParsing(data: json)
        if screen == "AddFiveNumber" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(dic)
                        if dic["Code"] as! Int == 200 || dic["Code"] as! Int == 940 || dic["Code"] as! Int == 151 || dic["Code"] as! Int == 929 {
                            DispatchQueue.main.async {
                                //                                println_debug("mooooooooohhhhh")
                                if dic["Code"] as! Int == 200 {
                                    //                                    println_debug("shobhittttt")
                                    let profileObj   = GetProfile()
                                    profileObj.callUpdateProfileApi(aCode: UserModel.shared.mobileNo, handler: { (isSuccess) in
                                    })
                                    self.navigationController?.popViewController(animated: true)
                                    DispatchQueue.main.async {
                                        alertViewObj.wrapAlert(title: "".localized, body:"OK numbers have been updated".localized, img: #imageLiteral(resourceName: "Contact Mobile Number"))
                                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                        })
                                        DispatchQueue.main.async {
                                            alertViewObj.showAlert(controller: self)
                                        }
                                    }
                                }
                                else {
                                    DispatchQueue.main.async {
                                        alertViewObj.wrapAlert(title: "", body: dic["Msg"] as! String, img: #imageLiteral(resourceName: "Contact Mobile Number"))
                                        alertViewObj.addAction(title: "OK".localized, style: .target, action:{})
                                        alertViewObj.showAlert(controller: self)
                                    }
                                }
                                self.removeProgressView()
                            }
                        }
                        else {
                            DispatchQueue.main.async {
                                alertViewObj.wrapAlert(title: "", body: dic["Msg"] as! String, img: #imageLiteral(resourceName: "Contact Mobile Number"))
                                alertViewObj.addAction(title: "OK".localized, style: .target, action:{})
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                }
            } catch {
            }
        }
    }
    
    func generateAuthCodeParsing(data: AnyObject) {
        let responseString = String(data: data as! Data, encoding: .utf8)
        let xml = SWXMLHash.parse(responseString!)
        println_debug(xml)
        self.enumerate(indexer: xml)
        println_debug(self.AddFiveNumberDict)
        if self.AddFiveNumberDict.keys.contains("resultdescription") {
            if self.AddFiveNumberDict["resultdescription"] as? String ?? "" == "Records Not Found" || self.AddFiveNumberDict["resultdescription"] as? String == "Transaction Successful" || self.AddFiveNumberDict["resultdescription"] as? String ?? "" == "Invalid Secure Token" || self.AddFiveNumberDict["resultdescription"] as? String ?? "" == "Secure Token Expire" || self.AddFiveNumberDict["resultdescription"] as? String ?? "" == "Invalid Request (Schema Validation Failed)" {
                
                DispatchQueue.main.async {
                    self.removeProgressView()
                }
            }
        }
    }
    
}

extension SMAddFiveNumberViewController : ContactPickerDelegate {
    //    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]) {
    //        for contact in contacts {
    //            decodeContact(contact: contact, isMulti: true)
    //        }
    //    }
    
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        println_debug(contact)
        decodeContact(contact: contact, isMulti: false)
    }
    
    func decodeContact(contact: ContactPicker, isMulti: Bool) {
//        self.btnSubmit.isHidden = true
        println_debug(contact.firstName)
        println_debug(contact.phoneNumbers[0].phoneNumber)
        let phoneNumberCount = contact.phoneNumbers.count
        if phoneNumberCount >= 1 {
            if !contact.phoneNumbers[0].phoneNumber.hasPrefix("+95") && !contact.phoneNumbers[0].phoneNumber.hasPrefix("09") && !contact.phoneNumbers[0].phoneNumber.hasPrefix("9") {
                if btnSubmit.isHidden {
                    self.btnSubmit.isHidden = true
                } else {
                    self.btnSubmit.isHidden = false
                }
                self.customAlert(msg: "You entered wrong mobile number. Please enter correct mobile number.".localized, title: "")
            }
            else{
                isContactSelected = true
                //                isShowPlusButton = true
                var number = "\(contact.phoneNumbers[0].phoneNumber)"
                
                if validObj.getNumberRangeValidation(number).isRejected == true {
                    if btnSubmit.isHidden {
                        self.btnSubmit.isHidden = true
                    } else {
                        self.btnSubmit.isHidden = false
                    }
                    self.customAlert(msg: "You entered wrong mobile number. Please enter correct mobile number.".localized, title: "")
                    return
                }
                if validObj.getNumberRangeValidation(number).isRejected == false {
                    if number.count < validObj.getNumberRangeValidation(number).min {
                        if btnSubmit.isHidden {
                            self.btnSubmit.isHidden = true
                        } else {
                            self.btnSubmit.isHidden = false
                        }
                        self.customAlert(msg: "You entered wrong mobile number. Please enter correct mobile number.".localized, title: "")
                        return
                    }
                }
                
                if number.hasPrefix("9") {
                    number.insert("0", at: number.startIndex)
                }
                let phNumber = number.replacingOccurrences(of: "+95", with: "0")
                //add it last element
                
                for object in sections {
                    if object.customerMb == "" {
                        if phNumber == object.rows[0] as? String ?? "" {
                            if btnSubmit.isHidden {
                                self.btnSubmit.isHidden = true
                            } else {
                                self.btnSubmit.isHidden = false
                            }
                            self.customAlert(msg: "Please enter unique OK$ number".localized, title: "")
                            return
                        }
                    } else {
                        if phNumber == object.customerMb.replacingOccurrences(of: "0095", with: "0") {
                            if btnSubmit.isHidden {
                                self.btnSubmit.isHidden = true
                            } else {
                                self.btnSubmit.isHidden = false
                            }
                            self.customAlert(msg: "Please enter unique OK$ number".localized, title: "")
                            return
                        }
                    }
                }
                
                self.sections[self.sections.count - 1].customerName = contact.firstName.localized
                self.sections[self.sections.count - 1].customerMb = phNumber
                self.sections[self.sections.count - 1].rows[0] = phNumber
                self.sections[self.sections.count - 1].rows[1] = contact.firstName.localized
                self.reloadTV2AddNewRow(indexPath: IndexPath.init(row: 2, section: self.sections.count - 1), willReloadTv: true)
                if self.sections.count == 1 {
                    if let cell1:SMTopUpMultiCell1 = self.tv.cellForRow(at: IndexPath.init(row: 0, section:0)) as? SMTopUpMultiCell1 {
                        cell1.btnDelete.isHidden = false
                    }
                }
                let lastSectionIndex = self.sections.count - 1
                if let cell1:SMTopUpMultiCell1 = self.tv.cellForRow(at: IndexPath.init(row: 0, section:lastSectionIndex)) as? SMTopUpMultiCell1 {
                    cell1.tfMb.isUserInteractionEnabled = false
                    cell1.btnCross.isHidden = true
                    cell1.btnContact.isHidden = true
                    sections[lastSectionIndex].isSelectContact = true
                    for data in sections {
                        println_debug("cccccccccccccccccccc --------- \(data.isSelectContact)")
                    }
                }
                
                if let cell2:SMTopUpMultiCell2 = self.tv.cellForRow(at: IndexPath.init(row: 1, section:lastSectionIndex)) as? SMTopUpMultiCell2 {
                    cell2.tfConfirmMb.placeholder = "Name".localized
                    cell2.ivConfirmMb.image = UIImage(named: "name_bill")
                    cell2.tfConfirmMb.isUserInteractionEnabled = false
                    cell2.btnCross.isHidden = true
                }
                
                if self.sections.count == 5 {
                    //last section
                    self.btnPlus.isHidden = true
                    self.btnSubmit.isHidden = false
                }
                else {
                    self.btnPlus.isHidden = false
                    self.btnSubmit.isHidden = false
                }
                
            }
        }
            //        else if phoneNumberCount > 1 {
            //            _ = "\(contact.phoneNumbers[0].phoneNumber) and \(contact.phoneNumbers.count-1) more"
            //        }
        else {
            _ = ContactGlobalConstants.Strings.phoneNumberNotAvaialable
        }
    }
    
    func createMultiContactView(contact: ContactPicker) {
        
    }
}

extension SMAddFiveNumberViewController: BioMetricLoginDelegate {
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if isSuccessful {
            println_debug("Authorised")
            DispatchQueue.main.async {
                self.callAfterAuthenticate()
            }
            
        }
        //        else{
        
        //            if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
        
        //            OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)
        //        }
    }
}
