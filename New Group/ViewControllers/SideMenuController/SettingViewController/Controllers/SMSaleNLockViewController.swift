//
//  saleNLockViewController.swift
//  OK
//
//  Created by Mohit on 10/25/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit


struct SaleNLockData: XMLIndexerDeserializable{
    
    var name = ""
    var mobileno = ""
    var email = ""
    var country = ""
    var state = ""
    var city = ""
    var status = ""
    var idproof = ""
    var idprooftype = ""
    
    static func deserialize(_ node: XMLIndexer) throws -> SaleNLockData {
        
        return try SaleNLockData(name: node["name"].value(),mobileno: node["mobileno"].value(),email: node["email"].value(),country: node["country"].value(),state: node["state"].value(),city: node["city"].value(),status: node["status"].value(),idproof: node["idproof"].value(),idprooftype: node["idprooftype"].value())
    }
}

class SMSaleNLockViewController: OKBaseController {
    
    var saleNLockDict = Dictionary <String, Any> ()
    var dataArr = [NSDictionary]()
    var NofItem = Int()
    var recData = [SaleNLockData]()
    var searchResults = [SaleNLockData]()
    let sortView        = SortViewController()
    let notificationCenter = NotificationCenter.default
    fileprivate let cellId = "SMSaleNLockViewCell"
    var intFilterStatus = 0
    var intSearchStatus = 0
    var arrSortList: [String] = ["Default".localized,"Name A to Z".localized,"Name Z to A".localized,"Active".localized,"In Active".localized]
    
    @IBOutlet weak var lblStatus: UILabel!{
        didSet
        {
            lblStatus.font = UIFont(name: appFont, size: appFontSize)
            lblStatus.text = lblStatus.text?.localized
        }
    }
    @IBOutlet weak var saleNLockTbl: UITableView!
    @IBOutlet weak var filtertbl: UITableView!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var NoResultsLabel: UILabel!{
        didSet{
            NoResultsLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnDone: UIButton!{
        didSet{
            self.btnDone.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.btnDone.setTitle(self.btnDone.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var lblFilter: UILabel!{
        didSet
        {
            lblFilter.font = UIFont(name: appFont, size: appFontSize)
            lblFilter.text = lblFilter.text?.localized
        }
    }
    
    
    struct Section {
        
        var name: String!
        var items: [String]!
        var collapsed: Bool!
        
        init(name: String, items: [String], collapsed: Bool = false) {
            
            self.name = name
            self.items = items
            self.collapsed = collapsed
            
        }
        
    }
    
    var sections = [Section]()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        
        searchBar.delegate = self
        
        sections = [
            Section(name: "Type", items: ["Normal"]),
            Section(name: "Categories", items: ["Personal", "Automotive"])
        ]
        self.title = "Safety Cashier Configuration".localized
        
        let secureToken = UserLogin.shared.token
        self.saleNLockApiCall(mobileNo: UserModel.shared.mobileNo, password: ok_password!, secureToken: secureToken)
        self.topView.isHidden = false
        self.searchView.isHidden = true
        self.filterView.isHidden = true
        self.filtertbl.tableFooterView = UIView.init()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customSearch()
        notificationCenter.addObserver(self, selector: #selector(SessionAlert), name: .appTimeout, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notificationCenter.removeObserver(self, name: .appTimeout, object: nil)
    }
    
    @objc func SessionAlert()  {
        DispatchQueue.main.async {
            
            alertViewObj.wrapAlert(title: "", body: "Application Session expired login again".localized, img:  #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){
                
                if UserLogin.shared.loginSessionExpired { OKPayment.main.authenticate(screenName: "Dashboard", delegate: self) }
                
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    
    @IBAction func filterOptionsAction(_ sender: UIButton) {
        
        if sender.tag == 1{
            
            intFilterStatus = 1
            filterView.isHidden = false
            filtertbl.reloadData()
        }
        else if sender.tag == 2{
            
            intFilterStatus = 2
            filterView.isHidden = false
            filtertbl.reloadData()
        }
    }
    
    func FilterFunction(sender: Bool){
        println_debug("buttonAction2 tapped")
        if sender {
            self.recData =  self.recData.sorted(by: { ($0.name) > ($1.name) })
        }else {
            self.recData =  self.recData.sorted(by: { ($0.name) > ($1.name) })
        }
        self.saleNLockTbl.reloadData()
    }
    @IBAction func searchClick(_ sender: UIButton) {
        self.topView.isHidden = true
        self.searchView.isHidden = false
    }
    
    @IBAction func filterClick(_ sender: UIButton) {
        self.filterView.isHidden = false
        filtertbl.reloadData()
    }
    
    @IBAction func doneClick(_ sender: UIButton) {
        self.filterView.isHidden = true
    }
    
    func customAlert(msg:String, title:String) {
        
        alertViewObj.addTextField(title: "", body: msg.localized, img:  #imageLiteral(resourceName: "website"))
        //alertViewObj.addAction(title: "NO", style: .cancel){}
        alertViewObj.addAction(title: "Done".localized, style: .target){
        }
        alertViewObj.showAlert(controller: self)
    }
    
    func loadData(arr:[SaleNLockData]) {
        
        DispatchQueue.main.async {
            self.saleNLockTbl.reloadData()
        }
    }
    
    
}

extension SMSaleNLockViewController: BioMetricLoginDelegate {
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if isSuccessful {
            println_debug("Authorised")
        }
        else{
            if UserLogin.shared.loginSessionExpired { OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
        }
    }
}


extension SMSaleNLockViewController: SortViewDelegate{
    func didSelectSortOption(sortedList: [Any], selectedSortOption: Int) {
        
    }
    
    func showSortListView() {
        
        sortView.delegate = self
        sortView.viewFrom = .promotion
        sortView.dataList = arrSortList
        sortView.modalPresentationStyle = .overCurrentContext
        self.present(sortView, animated: true, completion: nil)
    }
}

extension SMSaleNLockViewController {
    
    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func saleNLockApiCall(mobileNo: String, password: String, secureToken: String) {
        
        if appDelegate.checkNetworkAvail(){
            
            self.showProgressView()
            
            let web = WebApiClass()
            web.delegate = self
            let localWifiAdd = ""
            let urlS = String.init(format: Url.saleNLock,mobileNo,password,localWifiAdd,secureToken)
            let url =   getUrl(urlStr: urlS, serverType: .serverEstel)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "POST", mScreen: "saleNLock")
            
        }
        else{
            
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
            
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            self.saleNLockDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    
    @objc @IBAction func statusChange(_ sender : UIButton){
        
        alertViewObj.wrapAlert(title: "", body: "Do you want to change status?".localized, img: #imageLiteral(resourceName: "alert-icon"))
        alertViewObj.addAction(title: "Cancel".localized, style: .target){}
        alertViewObj.addAction(title: "OK".localized, style: .target){ self.ActiveInactiveSaleNLockApiCall(sender)}
        alertViewObj.showAlert(controller: self)
        
    }
    
    @objc @IBAction func ActiveInactiveSaleNLockApiCall(_ sender: UIButton) {
        
        if appDelegate.checkNetworkAvail(){
            
            self.showProgressView()
            
            let web = WebApiClass()
            web.delegate = self
            var activeInactive = ""
            
            if (sender.isSelected){
                sender.isSelected=false
                activeInactive = "ACTIVE"
            }
            else{
                sender.isSelected=true
                activeInactive = "INACTIVE"
            }
            
            let object = self.recData[sender.tag]
            let urlS = String.init(format: Url.DummyMerchantListStatusUpdate_API,UserModel.shared.mobileNo,object.mobileno,activeInactive,ok_password ?? "",UserLogin.shared.token)
            let url =   getUrl(urlStr: urlS, serverType: .serverEstel)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "POST", mScreen: "ActiveInactiveSaleNLock")
            
        }
        else{
            
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
            
        }
    }
}

extension SMSaleNLockViewController : WebServiceResponseDelegate {
    
    func webResponse(withJson json: AnyObject, screen: String) {
        self.saleNLockParsing(data: json, mscreen: screen)
    }
    
    func saleNLockParsing(data: AnyObject, mscreen: String){
        
        let responseString = String(data: data as! Data, encoding: .utf8)
        let xml            = SWXMLHash.parse(responseString!)
        
        println_debug(xml)
        self.enumerate(indexer: xml)
        println_debug(self.saleNLockDict)
        
        if self.saleNLockDict.keys.contains("resultdescription"){
            
            
            //            if self.saleNLockDict["resultdescription"] as? String ?? "" == "Records Not Found" || self.saleNLockDict["resultdescription"] as? String ?? "" == "Transaction Successful" || self.saleNLockDict["resultdescription"] as? String ?? "" == "Invalid Secure Token" || self.saleNLockDict["resultdescription"] as? String ?? "" == "Secure Token Expire" || self.saleNLockDict["resultdescription"] as? String ?? "" == "Invalid Request (Schema Validation Failed)" {
            
            DispatchQueue.main.async {
                
                if self.saleNLockDict["resultdescription"] as? String ?? "" == "Records Not Found"{
                    let msg = self.saleNLockDict["resultdescription"] as? String ?? ""
                    self.customAlert(msg: msg.localized, title: "")
                }
                else if mscreen == "ActiveInactiveSaleNLock"{
                    if let message = self.saleNLockDict.safeValueForKey("resultdescription") as? String, message == "Transaction Successful" {
                        alertViewObj.wrapAlert(title: "", body: "Updated successfully".localized, img: #imageLiteral(resourceName: "info_success"))
                        alertViewObj.addAction(title: "OK".localized, style: .target){}
                        alertViewObj.showAlert(controller: self)
                    } else {
                        if let message = self.saleNLockDict.safeValueForKey("resultdescription") as? String {
                            alertViewObj.wrapAlert(title: "", body: message.localized, img: #imageLiteral(resourceName: "alert-icon"))
                            alertViewObj.addAction(title: "OK".localized, style: .target){}
                            alertViewObj.showAlert(controller: self)
                        }
                    }
                }
                else{
                    do {
                        self.recData = try xml["estel"]["response"]["records"]["record"].value()
                    } catch {
                        println_debug(error)
                    }
                    self.loadData(arr: self.recData)
                }
                self.removeProgressView()
            }
            //            }
        }
    }
}

class SMSaleNLockViewCell: UITableViewCell{
    
    @IBOutlet var name: UILabel!{
        didSet
        {
            name.font = UIFont(name: appFont, size: appFontSize)
            name.text = name.text?.localized
        }
    }
    @IBOutlet var mobNum: UILabel!{
        didSet
        {
            mobNum.font = UIFont(name: appFont, size: appFontSize)
            mobNum.text = mobNum.text?.localized
        }
    }
    @IBOutlet var division: UILabel!{
        didSet
        {
            division.font = UIFont(name: appFont, size: appFontSize)
            division.text = division.text?.localized
        }
    }
    @IBOutlet var status: UILabel!{
        didSet
        {
            status.font = UIFont(name: appFont, size: appFontSize)
            status.text = status.text?.localized
        }
    }
    @IBOutlet var township: UILabel!{
        didSet
        {
            township.font = UIFont(name: appFont, size: appFontSize)
            township.text = township.text?.localized
        }
    }
    
    @IBOutlet weak var lblName: UILabel!{
        didSet
        {
            lblName.font = UIFont(name: appFont, size: appFontSize)
            lblName.text = lblName.text?.localized
        }
    }
    
    @IBOutlet weak var lblMobileNumber: UILabel!{
        didSet
        {
            lblMobileNumber.font = UIFont(name: appFont, size: appFontSize)
            lblMobileNumber.text = lblMobileNumber.text?.localized
        }
    }
    
    @IBOutlet weak var lblState: UILabel!{
        didSet
        {
            lblState.font = UIFont(name: appFont, size: appFontSize)
            lblState.text = lblState.text?.localized
        }
    }
    
    @IBOutlet weak var lblTownShip: UILabel!{
        didSet
        {
            lblTownShip.font = UIFont(name: appFont, size: appFontSize)
            lblTownShip.text = lblTownShip.text?.localized
        }
    }
    
    @IBOutlet weak var btnStatus: UIButton!{
        didSet{
            btnStatus.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnStatus.setTitle(btnStatus.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var cardView: CardDesignView!
    @IBAction func activeInActiveClick(_ sender: UIButton) {
        
        if (sender.isSelected){
            sender.isSelected=false;
        }
        else{
            sender.isSelected=true;
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func wrapData(name: String, number: String, state: String, township: String, staus: String){
        
        lblName.text = name.localized
        lblMobileNumber.text = number.localized
        lblState.text = state.localized
        lblTownShip.text = township.localized
    }
    
    func wrapData(arrayValue: SaleNLockData){
        
        lblName.text = arrayValue.name
        lblMobileNumber.text = arrayValue.mobileno
        lblState.text = arrayValue.state
        lblTownShip.text = arrayValue.city
        if arrayValue.status == "ACTIVE"{
            btnStatus.isSelected = false
        }else{
            btnStatus.isSelected = true
        }
    }
}




// MARK: -UISearchBar

extension SMSaleNLockViewController : UISearchBarDelegate {
    
    func customSearch(){
        
        searchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.tintColor = UIColor.black
        searchBar.placeholder = "Search".localized
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 16) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
               
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.keyboardType = .numberPad
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
            }
        }
        let view = self.searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = UIColor.gray
            }
        }
        
       if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            uiButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize) ?? UIFont.systemFont(ofSize: 14)
            if appDel.currentLanguage == "my" {
                uiButton.setTitle("မလုပ်ပါ", for:.normal)
            } else if appDel.currentLanguage == "uni" {
                uiButton.setTitle("မလုပ္ပါ", for:.normal)
            } else {
                uiButton.setTitle("Cancel".localized, for:.normal)
            }
        }
        
    }
    
    func filterContentForSearchText(searchText : String){
        
        println_debug(searchText)
        
        searchResults.removeAll()
        if searchText.isEmpty && searchText == ""{
            
            saleNLockTbl.isHidden = false
            NoResultsLabel?.isHidden = true
            NoResultsLabel?.text = "No record found".localized
            //     searchResults.removeAll()
            
            saleNLockTbl.reloadData()
            
            
        }
        else {
            
            
            saleNLockTbl.isHidden = false
            NoResultsLabel?.isHidden = true
            
            //      searchResults.removeAll()
            
            var demoArry = [SaleNLockData]()
            var i = 0
            
            
            while i < (recData.count) {
                
                let modelObj = recData[i]
                if modelObj.name.containsIgnoringCase(find: searchText) || modelObj.mobileno.containsIgnoringCase(find: searchText) {
                    self.saleNLockTbl.isHidden = false
                    demoArry.append(modelObj)
                }
                i = i + 1
            }
            if demoArry.count > 0 {
                saleNLockTbl.isHidden = false
                NoResultsLabel?.isHidden = true
                searchResults = demoArry
                saleNLockTbl.reloadData()
            } else {
                saleNLockTbl.isHidden = true
                NoResultsLabel?.text = "No record found".localized
                NoResultsLabel?.isHidden = false
            }
        }
        
        
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchBar.text = searchText
        
        if searchBar.text != "" && searchBar.text != "\n" {
            self.filterContentForSearchText(searchText: searchText)
        }
    }
    
    // called when cancel button is clicked
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.text = ""
        searchView.isHidden = true
        topView.isHidden = false
        //self.searchResults.removeAll()
        self.saleNLockTbl.reloadData()
        searchBar.resignFirstResponder()
        
    }
    
    // called when search button is clicked
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //searchBar.text = "Searching"
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
}

// MARK: - extension of Table View

extension SMSaleNLockViewController : UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if tableView == saleNLockTbl{
            return 1
        }
        else{
            if (intFilterStatus == 1){
                return 1
            }
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if tableView == saleNLockTbl{
            return 200.0
        }
        else{
            if indexPath.section == 0 {
                return tableView.rowHeight
            }
            let section = getSectionIndex(indexPath.row)
            let row = getRowIndex(indexPath.row)
            if row == 0 {
                return 50.0
            }
            return sections[section].collapsed! ? 0 : 44.0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == saleNLockTbl{
            
            if searchResults.count != 0 {
                return (searchResults.count)
            }
            else{
                return self.recData.count
                
            }
            
            
        }
        else{
            if (intFilterStatus == 1){
                return arrSortList.count
            }
            
            if (intFilterStatus == 2){
                if section == 0 {
                    return 1
                }
                var count = sections.count
                for section in sections {
                    count += section.items.count
                }
                return count
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == saleNLockTbl{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SMSaleNLockViewCell
            if searchResults.count != 0{
                //            cell.wrapData(name: object.name, number: object.mobileno, state: object.state, township: object.city, staus: object.state)
                cell.wrapData(arrayValue: searchResults[indexPath.row])
                
            }
            else{
                //            cell.wrapData(name: object.name, number: object.mobileno, state: object.state, township: object.city, staus: object.state)
                cell.wrapData(arrayValue: self.recData[indexPath.row])
                
            }
            cell.btnStatus.tag = indexPath.row
            cell.btnStatus.addTarget(self, action: #selector(self.statusChange(_:)), for: .touchUpInside)
            return cell
        }
            
        else{
            // create a new cell if needed or reuse an old one
            let cell:UITableViewCell=UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "cell")
            
            if (intFilterStatus == 1) {
                
                cell.textLabel?.font = UIFont.init(name: appFont, size: 17)
                cell.textLabel?.text = self.arrSortList[indexPath.row]
            }
            else
            {
                if indexPath.section == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "title")
                    cell?.textLabel?.font = UIFont.init(name: appFont, size: 17)
                    cell?.textLabel?.text = "Default"
                    return cell!
                }
                // Calculate the real section index and row index
                let section = getSectionIndex(indexPath.row)
                let row = getRowIndex(indexPath.row)
                
                if row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! HeaderCell
                    cell.textLabel?.font = UIFont.init(name: appFont, size: 17)
                    cell.titleLabel.text = sections[section].name
                    cell.toggleButton.tag = section
                    cell.toggleButton.setImage(sections[section].collapsed! ?   #imageLiteral(resourceName: "downArrow") :   #imageLiteral(resourceName: "upArrow"), for: UIControl.State())
                    cell.toggleButton.addTarget(self, action: #selector(self.toggleCollapse), for: .touchUpInside)
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
                    cell?.textLabel?.font = UIFont.init(name: appFont, size: 17)
                    cell?.textLabel?.text = sections[section].items[row - 1]
                    return cell!
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        tableView.deselectRow(at: indexPath, animated: true)
        if tableView == filtertbl{
            
            self.filterView.isHidden = true
            switch indexPath.row {
            case 0:
                break
            case 1: self.recData =  self.recData.sorted(by: { ($0.name) < ($1.name) })
            self.saleNLockTbl.reloadData()
                break
                
            case 2:  self.recData =  self.recData.sorted(by: { ($0.name) > ($1.name) })
            self.saleNLockTbl.reloadData()
                break
                
            case 3: self.recData =  self.recData.sorted(by: { ($0.status) < ($1.status) })
            self.saleNLockTbl.reloadData()
                break
                
            case 4:  self.recData =  self.recData.sorted(by: { ($0.status) > ($1.status) })
            self.saleNLockTbl.reloadData()
                break
            default:
                break
            }
            
        }
    }
    
    //
    // MARK: - Event Handlers
    //
    @objc func toggleCollapse(_ sender: UIButton) {
        
        let section = sender.tag
        let collapsed = sections[section].collapsed
        
        // Toggle collapse
        sections[section].collapsed = !collapsed!
        
        let indices = getHeaderIndices()
        
        let start = indices[section]
        let end = start + sections[section].items.count
        
        filtertbl.beginUpdates()
        for i in start ..< end + 1 {
            filtertbl.reloadRows(at: [IndexPath(row: i, section: 1)], with: .automatic)
        }
        filtertbl.endUpdates()
    }
    
    //
    // MARK: - Helper Functions
    //
    func getSectionIndex(_ row: NSInteger) -> Int {
        let indices = getHeaderIndices()
        
        for i in 0..<indices.count {
            if i == indices.count - 1 || row < indices[i + 1] {
                return i
            }
        }
        
        return -1
    }
    
    func getRowIndex(_ row: NSInteger) -> Int {
        var index = row
        let indices = getHeaderIndices()
        
        for i in 0..<indices.count {
            if i == indices.count - 1 || row < indices[i + 1] {
                index -= indices[i]
                break
            }
        }
        
        return index
    }
    
    func getHeaderIndices() -> [Int] {
        var index = 0
        var indices: [Int] = []
        
        for section in sections {
            indices.append(index)
            index += section.items.count + 1
        }
        
        return indices
    }
}



