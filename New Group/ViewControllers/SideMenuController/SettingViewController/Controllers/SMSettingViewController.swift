 //
 //  SettingViewController.swift
 //  OK
 //
 //  Created by Mohit on 10/16/17.
 //  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
 //
 
 import UIKit
 import CoreLocation
 
 //MARK: step 1 Add Protocol here.
 protocol SMSettingDelegate: class {
    func settingCloseClick(sender: UIBarButtonItem?)
    func changeNavigationController(controller: UIViewController)
 }
 struct BlockdataHeader: XMLIndexerDeserializable{
    
    var responsetype = ""
    static func deserialize(_ node: XMLIndexer) throws -> BlockdataHeader {
        return try BlockdataHeader(responsetype: node["responsetype"].value())
    }
 }
 
 struct Blockdata: XMLIndexerDeserializable{
    
    var responsetype = ""
    var agentcode = ""
    var transid = ""
    var resultcode = ""
    var resultdescription = ""
    var requestcts = ""
    var responsects = ""
    var clienttype = ""
    var responsevalue = ""
    var vendorcode = ""
    var comments = ""
    
    static func deserialize(_ node: XMLIndexer) throws -> Blockdata {
        
        return try Blockdata(responsetype: node["responsetype"].value(), agentcode: node["agentcode"].value(), transid: node["transid"].value(), resultcode: node["resultcode"].value(), resultdescription: node["resultdescription"].value(), requestcts: node["requestcts"].value(), responsects: node["responsects"].value(), clienttype: node["clienttype"].value(), responsevalue: node["responsevalue"].value(), vendorcode: node["vendorcode"].value(), comments: node["comments"].value())
    }
 }
 
 class SMSettingViewController: OKBaseController,SMViewOneDelegate,SMViewTwoDelegate,SMViewThreeDelegate {
    
    var navigation: UINavigationController?
    var tblLabelArr = [String]()
    var tblIconArr  = [UIImage]()
    var privilageUser = AgentType.user
    var getPaymentOtpDict = Dictionary<String, Any>()
    var dict = Dictionary<String,String>()
    var strCatName = String()
    var strCatImg = String()
    var strSubCatName = String()
    var strSubCatImg = String()
    var rec = Blockdata()
    var header = BlockdataHeader()
    var settingDelegate: SMSettingDelegate?
    fileprivate let cellId = "SettingTableViewCell"
    @IBOutlet weak var settingTbl: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Settings".localized
        privilageUser = UserModel.shared.agentType
        println_debug(UserModel.shared.agentType)
        println_debug(privilageUser)
        navigation = self.navigationController
        self.navigationController?.navigationItem.rightBarButtonItem = nil
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        
        self.initalizeArray()
    }
    
    
    func initalizeArray(){
        
        if privilageUser == .merchant {
            
            tblLabelArr = [
                
                "Security Lock Type".localized,
                "Add & View CashBack".localized,
                "Setup Safety Cashier".localized,
                "Transfer OK$ Main Balance to Cashback".localized,
                "Add & View Promotion".localized,
                "Get OTP for Web Admin".localized,
                "Get Payment Gateway OTP".localized,
                "Generate Agent Code".localized,
                "User Block".localized
                
            ]
            
            tblIconArr  =  [
                #imageLiteral(resourceName: "security_lock"),
                #imageLiteral(resourceName: "new_cashback"),
                #imageLiteral(resourceName: "safety_cashier_st"),
                #imageLiteral(resourceName: "mainbalance_cashback_st"),
                #imageLiteral(resourceName: "advertisement_st"),
                #imageLiteral(resourceName: "get_otp_st"),
                #imageLiteral(resourceName: "payment_gateway_st"),
                #imageLiteral(resourceName: "otp"),
                #imageLiteral(resourceName: "agent_block"),
                
            ]
        }
        else if privilageUser == .advancemerchant{
            tblLabelArr = [
                "Security Lock Type".localized,
                "Add & View CashBack".localized,
                "Setup Safety Cashier".localized,
                "Transfer OK$ Main Balance to Cashback".localized,
                "Add 5 Other OK$ Mobile Numbers".localized,
                "Add & View Promotion".localized,
                "Get OTP for Web Admin".localized,
                "Get Payment Gateway OTP".localized,
                "Generate Agent Code".localized,
                "User Block".localized
            ]
            
            tblIconArr  =  [
                #imageLiteral(resourceName: "security_lock"),
                #imageLiteral(resourceName: "new_cashback"),
                #imageLiteral(resourceName: "safety_cashier_st"),
                #imageLiteral(resourceName: "mainbalance_cashback_st"),
                #imageLiteral(resourceName: "add_number_st"),
                #imageLiteral(resourceName: "advertisement_st"),
                #imageLiteral(resourceName: "get_otp_st"),
                #imageLiteral(resourceName: "payment_gateway_st"),
                #imageLiteral(resourceName: "otp"),
                #imageLiteral(resourceName: "agent_block"),
                
            ]
            
        }
            
        else  if privilageUser == .user{
            
            tblLabelArr = [
                "Security Lock Type".localized,
                "Get OTP for Web Admin".localized,
                "Get Payment Gateway OTP".localized,
                "User Block".localized
            ]
            
            tblIconArr  =  [
                
                #imageLiteral(resourceName: "security_lock"),
                #imageLiteral(resourceName: "get_otp_st"),
                #imageLiteral(resourceName: "payment_gateway_st"),
                #imageLiteral(resourceName: "agent_block"),
                
            ]
        }
        else{
            tblLabelArr = [
                
                "Security Lock Type".localized,
                "Add & View Promotion".localized,
                "Get OTP for Web Admin".localized,
                "Get Payment Gateway OTP".localized,
                "Generate Agent Code".localized,
                //"Cash in|Out rate & Amount".localized,
                "User Block".localized
            ]
            tblIconArr  =  [
                #imageLiteral(resourceName: "security_lock"),
                #imageLiteral(resourceName: "advertisement_st"),
                #imageLiteral(resourceName: "get_otp_st"),
                #imageLiteral(resourceName: "payment_gateway_st"),
                #imageLiteral(resourceName: "payment_gateway_st"),
                //#imageLiteral(resourceName: "cash_in_out"),
                #imageLiteral(resourceName: "agent_block")
            ]
        }
        
        self.settingTbl.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData(arr:Blockdata){
        
        DispatchQueue.main.async {
            
            alertViewObj.wrapAlert(title: "", body: arr.resultdescription, img: #imageLiteral(resourceName: "setting"))
            alertViewObj.addAction(title: "OK".localized, style: .target, action:{
                self.navigationController?.popToRootViewController(animated: true)
                NotificationCenter.default.post(name: .goDashboardScreen, object: nil)
            })
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func blockUserAlert(){
        let str = String.init("Are you sure want to block your account?".localized)
        alertViewObj.wrapAlert(title: "Block account".localized, body: str.localized, img: nil)
        alertViewObj.addAction(title: "NO".localized, style: .target, action:{})
        alertViewObj.addAction(title: "YES".localized, style: .target, action:{
            self.BlockUserApiCall()
        })
        alertViewObj.showAlert(controller: self)
    }
    
    func switchtoViewController(index : Int){
        
        
        if privilageUser == .merchant{
            
            switch index{
                
            case 0:
                
                self.moveToController(identifier: "SMLockTypeViewController")
                
                break
                
            case 1:
                
                self.moveToController(identifier: "SMCashBackViewController")
                
                break
                
            case 2:
                
                self.moveToController(identifier: "SMSaleNLockListVC")
                
                break
                
            case 3:
                
                self.moveToController(identifier: "SMTransferCashBackViewController")
                
                break
                //            case 4:
                //
                //
                //                self.moveToController(identifier: "SMAddFiveNumberViewController")
                //
                //                break
                
            case 4:
                
                if (UserDefaults.standard.object(forKey: "shopID") as? String) != nil{
                    
                    self.moveToController(identifier: "SMAddPromotionViewController")
                    
                }else{
                    
                    self.customAlert1(msg: "Update Business area details and Add Promotion Offers.".localized, title: "Add/View Promotion".localized)
                    
                }
                break
            case 5:
                
                self.moveToController(identifier: "SMGetOTPViewController")
                
                break
                
            case 6:
                
                self.paymentGatewayOTPApiCall()
                
                break
                
            case 7:
                let str = String.init("You don't have permission".localized)
                self.customAlert(msg: str, title: "", image: #imageLiteral(resourceName: "otp"))
                break
                
            case 8:
                
                self.blockUserAlert()
                break
                
            default:
                break
                
            }
        }
            
        else if privilageUser == .advancemerchant{
            
            switch index{
                
            case 0:
                
                self.moveToController(identifier: "SMLockTypeViewController")
                
                break
                
            case 1:
                
                self.moveToController(identifier: "SMCashBackViewController")
                
                break
                
            case 2:
                
                self.moveToController(identifier: "SMSaleNLockListVC")
                
                break
                
            case 3:
                
                self.moveToController(identifier: "SMTransferCashBackViewController")
                
                break
            case 4:
                
                
                self.moveToController(identifier: "SMAddFiveNumberViewController")
                
                break
                
            case 5:
                
                if (UserDefaults.standard.object(forKey: "shopID") as? String) != nil{
                    
                    self.moveToController(identifier: "SMAddPromotionViewController")
                    
                }else{
                    
                    self.customAlert1(msg: "Update Business area details and Add Promotion Offers.".localized, title: "Add/View Promotion".localized)
                    
                }
                break
            case 6:
                
                self.moveToController(identifier: "SMGetOTPViewController")
                
                break
                
            case 7:
                
                self.paymentGatewayOTPApiCall()
                
                break
                
            case 8:
                let str = String.init("You don't have permission".localized)
                self.customAlert(msg: str, title: "", image: #imageLiteral(resourceName: "otp"))
                break
                
            case 9:
                
                self.blockUserAlert()
                break
                
            default:
                break
                
            }
        }
        else if privilageUser == .user{
            
            switch index{
                
            case 0:
                
                self.moveToController(identifier: "SMLockTypeViewController")
                
                break
                
            case 1:
                self.moveToController(identifier: "SMGetOTPViewController")
                
                break
                
            case 2:
                self.paymentGatewayOTPApiCall()
                break
                
            default:
                self.blockUserAlert()
                break
            }
        }
            
        else{
            switch index{
            case 0:
                self.moveToController(identifier: "SMLockTypeViewController")
                break
            case 1:
                if (UserDefaults.standard.object(forKey: "shopID") as? String) != nil{
                    self.moveToController(identifier: "SMAddPromotionViewController")
                }else{
                    self.customAlert1(msg: "Update Business area details and Add Promotion Offers.".localized, title: "Add/View Promotion".localized)
                }
                break
            case 2:
                self.moveToController(identifier: "SMGetOTPViewController")
                break
            case 3:
                self.paymentGatewayOTPApiCall()
                break
            case 4:
                let str = String.init("You don't have permission".localized)
                self.customAlert(msg: str.localized, title: "Response", image: #imageLiteral(resourceName: "otp"))
                break
//            case 5:
//                self.moveToController(identifier: "SMCashInOutViewController")
//                break
            default:
                self.blockUserAlert()
                break
            }
        }
    }
    
    func customAlert(msg:String, title:String, image:UIImage?) {
        
        alertViewObj.wrapAlert(title: "", body: msg.localized, img: image)
        alertViewObj.addAction(title: "OK".localized, style: .target, action:{})
        alertViewObj.showAlert(controller: self)
    }
    
    @IBAction func closeBtn(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func customAlert1(msg:String,title:String) {
        
        alertViewObj.addTextField(title: title, body: msg, img:  nil)
        alertViewObj.addAction(title: "NO".localized, style: .cancel){}
        alertViewObj.addAction(title: "YES".localized, style: .target){
            
            let viewController = UIStoryboard(name: "Setting", bundle: Bundle.main).instantiateViewController(withIdentifier: "customLayerVC") as? customLayerVC
            viewController?.modalPresentationStyle = .overCurrentContext
            
            println_debug(UserDefaults.standard.object(forKey: "shopID"))
            
            if (UserDefaults.standard.object(forKey: "shopID") as? String) != nil{
                self.strSubCatName = "Delete".localized
            }
            else{
                self.getCategoaryName()
            }
            
            if (self.strSubCatName == "Bus") || (self.strSubCatName == "Ticketing") {
                
                let ScreenView = Bundle.main.loadNibNamed("SMAddFiveView", owner: self, options: nil)?[3] as! SMViewThree
                let labelTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismiss))
                labelTapGesture.numberOfTapsRequired = 1
                ScreenView.addGestureRecognizer(labelTapGesture)
                ScreenView.delegate = self
                ScreenView.txt_busniessname.text = UserModel.shared.businessName.localized
                ScreenView.frame = CGRect(x: 10, y:((screenHeight/2)-130) , width: screenWidth-20, height: 450)
                viewController?.view.addSubview(ScreenView)
                
            }
                
            else if (self.strSubCatName == "Taxi") {
                
                let ScreenView = Bundle.main.loadNibNamed("SMAddFiveView", owner: self, options: nil)?[2] as! SMViewOne
                let labelTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismiss))
                labelTapGesture.numberOfTapsRequired = 1
                ScreenView.addGestureRecognizer(labelTapGesture)
                ScreenView.delegate = self
                ScreenView.txt_busniessname.text = UserModel.shared.businessName.localized
                ScreenView.frame = CGRect(x: 10, y:((screenHeight/2)-130) , width: screenWidth-20, height: 535)
                viewController?.view.addSubview(ScreenView)
                
            }
            else{
                
                let ScreenView = Bundle.main.loadNibNamed("SMAddFiveView", owner: self, options: nil)?[4] as! SMViewTwo
                let labelTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismiss))
                labelTapGesture.numberOfTapsRequired = 1
                ScreenView.addGestureRecognizer(labelTapGesture)
                ScreenView.delegate = self
                ScreenView.txt_busniessname.text = UserModel.shared.businessName.localized
                ScreenView.frame = CGRect(x: 10, y:((screenHeight/2)-130) , width: screenWidth-20, height: 260)
                viewController?.view.addSubview(ScreenView)
            }
            self.present(viewController!, animated:true, completion:nil)
        }
        
        alertViewObj.showAlert(controller: self)
        
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismiss(sender:UITapGestureRecognizer) {
        
        sender.view?.isHidden = true
        sender.view?.removeFromSuperview()
        
    }
    
    func getCategoaryName(){
        
        let categoriesList = CategoriesManager.categoriesList
        println_debug(categoriesList)
        
        var i = 0
        
        while i < (categoriesList.count){
            
            let category = categoriesList[i]
            
            if (category.categoryCode.localizedCaseInsensitiveContains(UserModel.shared.businessCate)){
                
                strCatName = category.categoryName
                strCatImg = category.categoryEmoji
                
                //fetch subcat
                let subCatList = category.subCategoryList
                var subCat = 0
                
                while subCat < (subCatList.count){
                    
                    let subCategory = subCatList[subCat]
                    
                    if (subCategory.subCategoryCode.localizedCaseInsensitiveContains(UserModel.shared.businessSubCate)){
                        
                        strSubCatName = subCategory.subCategoryName
                        strSubCatImg = subCategory.subCategoryEmoji
                        break
                        
                    }
                    subCat = subCat + 1
                }
                
                break
            }
            i = i + 1
        }
        
        println_debug(strCatName)
        println_debug(strSubCatName)
    }
    
    func updateCurrentLocationToUI(alerText: String){
        
        geoLocManager.startUpdateLocation()
        guard appDelegate.checkNetworkAvail() else{
            self.showErrorAlert(errMessage: "Network Not Available")
            
            return
        }
        
        if let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude{
            
            self.showProgressView()
            
            geoLocManager.getLocationNameByLatLong(lattitude: lat, longitude: long) {(isSuccess, currentAddress) in
                if isSuccess {
                    
                    DispatchQueue.main.async{
                        
                        if let curAddress = currentAddress as? String{
                            
                            self.AfterBusinessNavigate(lat: lat, long: long, curadd: curAddress, alerText: alerText)
                            
                        }else{
                            println_debug("Loading location.....")
                        }
                        self.removeProgressView()
                        geoLocManager.stopUpdateLocation()
                    }
                }else{
                    DispatchQueue.main.async {
                        println_debug("something went wrong when get current address from google api")
                        //                    if let curAddress = currentAddress as? String{
                        //                        SingletonClass.sharedInstance.getAdress(completion: { (address, error) in
                        //                            println_debug(address!)
                        //                            println_debug(error!)
                        //                        })
                        self.AfterBusinessNavigate(lat: UserModel.shared.lat, long: UserModel.shared.long, curadd: "\(UserModel.shared.address1),\(UserModel.shared.address2),\(UserModel.shared.address1)", alerText: alerText)
                        //                }
                        self.removeProgressView()
                        geoLocManager.stopUpdateLocation()
                    }
                    return
                }
            }
        }
    }
    
    
    func AfterBusinessNavigate(lat: String,long: String,curadd:String,alerText:String){
        
        //                        self.dict["lat"] = lat
        //                        self.dict["long"] = long
        //                        self.dict["curAddress"] = curAddress
        //                        self.dict["alerText"] = alerText
        
        UserDefaults.standard.set(lat, forKey: "lat")
        UserDefaults.standard.set(long, forKey: "long")
        // for distance purpose
        
        
        var dataVal: CLLocationCoordinate2D!
        var lat : Double!
        var lon : Double!
        
        dataVal = geoLocManager.currentLocation.coordinate
        
        if dataVal == nil{
            
            lat = 0.0
            lon = 0.0
        }
        else{
            
            lat = dataVal.latitude
            lon = dataVal.longitude 
        }
        let userLocation = ["lat": lat, "long": lon]
        UserDefaults.standard.set(userLocation, forKey: "BussinessDoneCurrentLocation")
        //
        UserDefaults.standard.set(curadd, forKey: "curAddress")
        UserDefaults.standard.set(alerText, forKey: "alerText")
        UserDefaults.standard.synchronize()
        
        
        let story = UIStoryboard.init(name: "Setting", bundle: nil)
        let VC = story.instantiateViewController(withIdentifier: "SMAddPromotionViewController") as! SMAddPromotionViewController
        VC.accessAddressFromGEOLocation =  self.dict
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    func moveToController(identifier: String){
        
        let VC = self.storyboard?.instantiateViewController(withIdentifier: identifier)
        settingDelegate?.changeNavigationController(controller: getViewController(sbName: identifier, vcName: identifier))
        self.navigation?.pushViewController(VC!, animated: true)
    }
    
    func getViewController<T: UIViewController>(sbName: String, vcName: String) -> T {
        let sb = UIStoryboard(name: sbName, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: vcName) as! T
        return vc
    }
    
    func paymentGatewayOTPApiCall(){
        if appDelegate.checkNetworkAvail(){
            
            showProgressView()
            
            let web = WebApiClass()
            web.delegate = self
            
            
            let urlS = String.init(format: Url.paymentGatewayOTP, UserModel.shared.mobileNo,UserModel.shared.simID,msid,"1","",UserModel.shared.name)
            
            
            let allowedCharacterSet = (CharacterSet(charactersIn: " ").inverted)
            
            if let escapedString = urlS.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) {
                
                let url =   getUrl(urlStr: escapedString, serverType: .serverApp)
                let param = [String:String]() as AnyObject
                web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "paymentGatewayOTP")
                
            }
        }
        else{
            
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func BlockUserApiCall(){
        
        if appDelegate.checkNetworkAvail(){
            
            showProgressView()
            
            let web = WebApiClass()
            web.delegate = self
            
            var userID = ""
            
            if  UserModel.shared.idType == "01"{
                userID = "NRC".localized
            }
            else if UserModel.shared.idType == "04"{
                userID = "passport".localized
            }
            else{
                userID = UserModel.shared.phoneNumber
            }
            let urlS = String.init(format: Url.BlockUser, UserModel.shared.mobileNo,"Block Api Coding",ok_password!,userID)
            
            let allowedCharacterSet = (CharacterSet(charactersIn: " ").inverted)
            
            if let escapedString = urlS.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) {
                
                let url =   getUrl(urlStr: escapedString, serverType: .serverEstel)
                let param = [String:String]() as AnyObject
                web.genericClass(url: url, param: param, httpMethod: "POST", mScreen: "BlockUser")
                
            }
        }
        else{
            
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
            
        }
    }
    
    func generateAuthCodeApiCall() {
        
        if appDelegate.checkNetworkAvail() {
            
            showProgressView()
            let web = WebApiClass()
            web.delegate = self
            
            let urlS = String.init(format: Url.generateAuthCode, UserModel.shared.mobileNo,UserModel.shared.simID,"01","1","")
            let url  =   getUrl(urlStr: urlS, serverType: .serverApp)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "POST", mScreen: "generateAuthCode")
            
        }
        
    }
    
    
    func enumerate(indexer: XMLIndexer){
        for child in indexer.children {
            self.getPaymentOtpDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func doneClick(sender: UIButton?){
        
        //        self.updateCurrentLocationToUI(alerText: "Testing")
    }
    
    func doneClick(sender: UIButton?, txtValue: String) {
        self.dismiss(animated: true, completion: nil)
        UserDefaults.standard.setValue(txtValue, forKey: "txtValue")
        self.updateCurrentLocationToUI(alerText: txtValue)
        
    }
    
 }
 
 extension SMSettingViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SettingTableViewCell
        let icon  = tblIconArr[indexPath.row]
        let name  = tblLabelArr[indexPath.row]
        cell.wrapData(img: icon, name: name)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tblLabelArr.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.switchtoViewController(index: indexPath.row)
    }
    
 }
 
 
 class SettingTableViewCell: UITableViewCell {
    
    @IBOutlet var lblName: UILabel!{
        didSet{
            lblName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var icon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func wrapData(img: UIImage, name: String){
        icon.image    = img
        lblName.text = name
    }
    
 }
 
 extension SMSettingViewController : WebServiceResponseDelegate {
    
    func webResponse(withJson json: AnyObject, screen: String) {
        if screen == "BlockUser"{
            self.generateAuthCodeParsing(data: json, screen: screen)
        }
        
        if screen == "paymentGatewayOTP"{
            
            do {
                if let data = json as? Data {
                    
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(dic)
                        
                        
                        
                        if dic["Code"] as? Int == 200 {
                            
                            
                            if let login = dic["Data"] as? String, login != "" {
                                let dic = OKBaseController.convertToDictionary(text: login)
                                if let log = dic!["Table"] as? Array<Any> {
                                    if let logi = log.first as? Dictionary<String,Any> {
                                        if ((logi["OTP"] as? String) != nil){
                                            DispatchQueue.main.async {
                                                
                                                //let str = logi["OTP"]
                                                self.customAlert(msg: "The OTP is \(logi["OTP"] as? String ?? "" )", title: "", image: #imageLiteral(resourceName: "payment_gateway_st"))
                                            }
                                            
                                        }
                                    }
                                }
                            }
                        }
                            
                        else if dic["Code"] as? Int == 300{
                            DispatchQueue.main.async {
                                self.customAlert(msg: (dic["Msg"] as? String)!.localized, title: "", image: #imageLiteral(resourceName: "payment_gateway_st"))
                            }
                            
                        }
                        else{
                            
                        }
                        
                        
                    }
                }
                self.removeProgressView()
            } catch {
                
            }
        }
        
    }
    
    func generateAuthCodeParsing(data: AnyObject, screen: String){
        
        let responseString = String(data: data as! Data, encoding: .utf8)
        let xml            = SWXMLHash.parse(responseString!)
        
        self.enumerate(indexer: xml)
        
        println_debug(self.getPaymentOtpDict)
        
        
        if screen == "BlockUser"{
            
            DispatchQueue.main.async {
                self.removeProgressView()
            }
            do{
                rec = try xml["estel"]["response"].value()
                header = try xml["estel"]["header"].value()
                self.loadData(arr: self.rec)
            }
            catch{
                println_debug(error)
            }
        }
            
        else{
            
            if self.getPaymentOtpDict.keys.contains("resultdescription"){
                
                if self.getPaymentOtpDict["Msg"] as? String ?? "" == "The OTP Not Found" || self.getPaymentOtpDict["resultdescription"] as? String ?? "" == "Transaction Successful"{
                    
                    if self.getPaymentOtpDict["Msg"] as? String ?? "" == "The OTP Not Found" || self.getPaymentOtpDict["resultdescription"] as? String ?? "" == "Transaction Successful"{
                        
                        DispatchQueue.main.async {
                            self.removeProgressView()
                        }
                    }
                }
            }
        }
    }
 }
 
 class customLayerVC: OKBaseController{
    
    @IBOutlet weak var layerView: UIView!
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let labelTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.removeSubView))
        labelTapGesture.numberOfTapsRequired = 1
        self.layerView.addGestureRecognizer(labelTapGesture)
    }
    
    @IBAction func dismissTapAction(_ sender: Any) {
        // print(self.children[0])
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func removeSubView(VC: customLayerVC){
        //print(self.children[0])
        print("touch begins")
        dismiss(animated: false, completion: nil)
        //
        //        VC.view.removeFromSuperview()
    }
    
 }
