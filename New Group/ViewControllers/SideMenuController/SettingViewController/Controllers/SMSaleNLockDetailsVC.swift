//
//  SMSaleNLockDetailsVC.swift
//  OK
//
//  Created by gauri OK$ on 5/29/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol SMSaleNLockDetailsVCDelegate : class {
    func reloadListView()
}


class SMSaleNLockDetailsVC: OKBaseController, UITextFieldDelegate {

    @IBOutlet weak var saleNLockTbl: UITableView!
    var dataModel = SaleNLockList()
    @IBOutlet weak var btnApproveUpdate: UIButton!{
        didSet{
            btnApproveUpdate.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }

    var saleNLockDict = Dictionary <String, Any> ()
    var emailArr : [String] = []
    var emailListData = [SaleNLockEmailList]()
    var delegate : SMSaleNLockDetailsVCDelegate?
    var strMsg = ""

    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Safety Cashier".localized
        //Manage email ID
        if emailListData.count > 0 {
           
            let filteredArray = self.emailListData.filter({ $0.dummyMerOkAccNum?.contains(find: dataModel.phoneNumber ?? "") ?? true})
            
            if filteredArray.count > 0 {
            //Display Data on view
            let emailID = filteredArray[0].emailId ?? ""
            let tmpArr = emailID.components(separatedBy: ",")
            if tmpArr.count > 1 {
                emailArr.append(tmpArr[0])
                emailArr.append(tmpArr[1])
            } else {
                emailArr.append(tmpArr[0])
            }
            } else {
                emailArr.append("")
            }
        } else {
            emailArr.append("")
        }
        
        saleNLockTbl.reloadData()
        
        //Show Bottom button text
        if dataModel.status == 0 {
            btnApproveUpdate.setTitle("Approve".localized, for: .normal)
        } else {
            btnApproveUpdate.setTitle("Update".localized, for: .normal)
        }
        
    }
    
    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnApproveUpdateAction(_ sender: UIBarButtonItem) {
        println_debug("btnApproveUpdateAction")
        //Call Update API
        var emailID = ""
        if emailArr.count > 1 {
            if emailArr[0].count > 0 && emailArr[1].count > 0 {
                emailID = emailArr[0] + "," + emailArr[1]
            } else if emailArr[0].count > 0{
                emailID = emailArr[0]
            } else if emailArr[1].count > 0{
                emailID = emailArr[1]
            }
        } else {
            emailID = emailArr[0]
        }
        
        if emailID.length > 0 {
        //Request Approve
       if dataModel.status == 0 {
        
        self.activeApproveSaleNLockApiCall()
        } else { //Update email API
            
            self.updateEmailApiCall()
        }
        } else {
            alertViewObj.wrapAlert(title: "", body: "Please enter atleast one Email Id!".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){
                
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func isValidEmail(email:String) -> Bool {
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let pred = NSPredicate(format:"SELF MATCHES %@", regEx)
        return pred.evaluate(with: email)
    }
    
    //MARK:- Text Field Delegate
    
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let textString = textField.text! + string
        if (textString.count) > 0 {
            textField.placeholder = "Email Id".localized
            if textString.count == 1 {
                if string.count == 0 {
                    textField.placeholder = "Enter Email Id".localized
                }
            }
        } else {
            textField.placeholder = "Enter Email Id".localized
        }
    
         if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: EMAILCHARSET).inverted).joined(separator: "")) { return false }
        
        if textField.text!.length > 64 {
            textField.text?.removeLast()
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if isValidEmail(email : textField.text!){
            if dataModel.villageName?.count == 0 {
                if(textField.tag == 10) {
                    
                    emailArr.remove(at: 0)
                    
                    emailArr.insert(textField.text ?? "", at: 0)
                }else {
                    
                    emailArr.insert(textField.text ?? "", at: 1)
                }
            } else {
                if(textField.tag == 11) {
//                    if emailArr[0].count > 0 {
                        emailArr.remove(at: 0)
                    //}
                    emailArr.insert(textField.text ?? "", at: 0)
                }else {
                    if emailArr[1].count > 0 {
                        emailArr.remove(at: 1)
                    }
                    emailArr.insert(textField.text ?? "", at: 1)
                }
            }
            
        }else {
            alertViewObj.wrapAlert(title: nil, body: "Please enter valid EMail ID".localized, img: #imageLiteral(resourceName: "info_one"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                self.saleNLockTbl.reloadData()
            }
            alertViewObj.showAlert(controller: self)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


// MARK: - extension of Table View

extension SMSaleNLockDetailsVC : UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 102.0
        }
        return 52.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if dataModel.villageName?.count == 0 {
            if emailArr.count > 1 {
                return 12
            }
            return 11
        } else {
        if emailArr.count > 1 {
            return 13
        }
        return 12
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        if dataModel.villageName?.count == 0 {
            //Set Records
            if indexPath.row == 0 {
                let cellId1 = "SMSaleNLockUserDetailsCell"
                
                let cellUser = tableView.dequeueReusableCell(withIdentifier: cellId1, for: indexPath) as! SMSaleNLockUserDetailsCell
                //Show image icon
                cellUser.userImg.layer.cornerRadius =  cellUser.userImg.frame.size.width/2
                cellUser.userImg.layer.masksToBounds = true
                cellUser.wrapData(dataModel: dataModel, indexTag: indexPath.row)
                //Active&InActive status
                cellUser.btnStatusActiveInActive.addTarget(self, action: #selector(self.btnInActiveAction(_:)), for: .touchUpInside)
                cellUser.selectionStyle = .none
                return cellUser
            } else if indexPath.row == 10 {
                let cellId = "SMSaleNLockEmailCell"
                
                let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SMSaleNLockEmailCell
                cell.imgIcon.image = #imageLiteral(resourceName: "n_email")
                if emailArr.count > 1 {
                    cell.plusMinusIcon.setBackgroundImage(#imageLiteral(resourceName: "delet"), for: .normal)
                } else {
                    cell.plusMinusIcon.setBackgroundImage(#imageLiteral(resourceName: "add"), for: .normal)
                }
                
                //Button status update
                cell.plusMinusIcon.addTarget(self, action: #selector(self.plusMinusEmailAction(_:)), for: .touchUpInside)
                cell.plusMinusIcon.tag = indexPath.row
                //Email Handle
                cell.txtTitle.tag = indexPath.row
                cell.txtTitle.delegate = self
                if emailArr.count > 0 {
                    if emailArr[0].length > 0 {
                        cell.txtTitle.placeholder = "Email Id".localized
                        cell.txtTitle.text = emailArr[0]
                    } else {
                        cell.txtTitle.placeholder = "Enter Email Id".localized
                        cell.txtTitle.text = ""
                    }
                }
                return cell
            } else if indexPath.row == 11 {
                let cellId = "SMSaleNLockEmailCell"
                
                let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SMSaleNLockEmailCell
                
                cell.imgIcon.image = #imageLiteral(resourceName: "n_email")
                cell.plusMinusIcon.setBackgroundImage(#imageLiteral(resourceName: "delet"), for: .normal)
                cell.plusMinusIcon.addTarget(self, action: #selector(self.plusMinusEmailAction(_:)), for: .touchUpInside)
                cell.plusMinusIcon.tag = indexPath.row
                //Email Handle
                cell.txtTitle.tag = indexPath.row
                cell.txtTitle.delegate = self
                if emailArr[1].count > 0 {
                    cell.txtTitle.placeholder = "Email Id".localized
                    cell.txtTitle.text = emailArr[1]
                } else {
                    cell.txtTitle.placeholder = "Enter Email Id".localized
                    cell.txtTitle.text = ""
                }
                
                return cell
            } else {
                let cellId = "SMSaleNLockDetailsCell"
                
                let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SMSaleNLockDetailsCell
                
                if indexPath.row == 1 {
                    cell.txtTitle.placeholder = "Safety Cashier Mobile Number".localized
                    cell.txtTitle.text = dataModel.phoneNumber!.replacingOccurrences(of: "0095", with: "(+95)0")
                    cell.imgIcon.image = #imageLiteral(resourceName: "myanmar")
                } else if indexPath.row == 2 {
                    cell.txtTitle.placeholder = "Name".localized
                    cell.txtTitle.text = dataModel.name
                    if dataModel.gender == true {
                        cell.imgIcon.image = #imageLiteral(resourceName: "r_male")
                    } else {
                        cell.imgIcon.image = #imageLiteral(resourceName: "r_female")
                    }
                } else if indexPath.row == 3 {
                    cell.txtTitle.placeholder = "NRC Number".localized
                    cell.txtTitle.text = dataModel.NRC
                    cell.imgIcon.image = #imageLiteral(resourceName: "nrc")
                } else if indexPath.row == 4 {
                    cell.txtTitle.placeholder = "Joined On".localized
                    println_debug(dataModel.joiningDate)
                    cell.txtTitle.text = self.getJoiningDateTime(digit:dataModel.joiningDate ?? "") //self.getJoiningDate(dataModel.joiningDate ?? "")
                    cell.imgIcon.image = #imageLiteral(resourceName: "r_user")
                }  else if indexPath.row == 5 {
                    cell.txtTitle.placeholder = "Date of Birth".localized
                    cell.txtTitle.text = dataModel.DOB
                    cell.imgIcon.image = #imageLiteral(resourceName: "dob")
                } else if indexPath.row == 6 {
                    let divisionTownshipName = getDivisionAndTownShip(divisionCode: dataModel.state ?? "", townshipCode: dataModel.township ?? "").split(separator: ",")
                    cell.txtTitle.placeholder = "Division / State".localized
                    if divisionTownshipName.count > 0 {
                        cell.txtTitle.text = "" + divisionTownshipName[1]
                    } else {
                        cell.txtTitle.text = ""
                    }
                    cell.imgIcon.image = #imageLiteral(resourceName: "website")
                } else if indexPath.row == 7 {
                    let divisionTownshipName = getDivisionAndTownShip(divisionCode: dataModel.state ?? "", townshipCode: dataModel.township ?? "").split(separator: ",")
                    cell.txtTitle.placeholder = "Township".localized
                    if divisionTownshipName.count > 0 {
                        cell.txtTitle.text = "" + divisionTownshipName[0]
                    } else {
                        cell.txtTitle.text = ""
                    }
                    cell.imgIcon.image = #imageLiteral(resourceName: "r_township")
                } else if indexPath.row == 8 {
                    cell.txtTitle.textColor = .black
                    cell.txtTitle.placeholder = "City".localized
                    cell.txtTitle.text = dataModel.cityName
                    cell.imgIcon.image = #imageLiteral(resourceName: "r_city")
                }
                else if indexPath.row == 9 {
                    cell.txtTitle.placeholder = "Status".localized
                    cell.txtTitle.text = dataModel.isActive
                    if dataModel.status == 0 {
                        cell.txtTitle.textColor = .red
                        cell.txtTitle.text = "Pending Approval".localized
                    } else {
                        if(dataModel.isActive?.caseInsensitiveCompare("Active") == .orderedSame){
                            cell.txtTitle.textColor = UIColor(red: (0/255), green: (119/255), blue: (0/255), alpha: 1.0)
                            
                            cell.txtTitle.text = "Active"
                        } else {
                            cell.txtTitle.textColor = .blue
                            cell.txtTitle.text = "InActive"
                        }
                    }
                    cell.imgIcon.image = #imageLiteral(resourceName: "status")
                }
                cell.selectionStyle = .none
                return cell
            }
        } else {
        //Set Records
        if indexPath.row == 0 {
            let cellId1 = "SMSaleNLockUserDetailsCell"
            
            let cellUser = tableView.dequeueReusableCell(withIdentifier: cellId1, for: indexPath) as! SMSaleNLockUserDetailsCell
            //Show image icon
            cellUser.userImg.layer.cornerRadius =  cellUser.userImg.frame.size.width/2
            cellUser.userImg.layer.masksToBounds = true
            cellUser.wrapData(dataModel: dataModel, indexTag: indexPath.row)
            //Active&InActive status
            cellUser.btnStatusActiveInActive.addTarget(self, action: #selector(self.btnInActiveAction(_:)), for: .touchUpInside)
            cellUser.selectionStyle = .none
            return cellUser
        } else if indexPath.row == 11 {
            let cellId = "SMSaleNLockEmailCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SMSaleNLockEmailCell
            cell.imgIcon.image = #imageLiteral(resourceName: "n_email")
            if emailArr.count > 1 {
            cell.plusMinusIcon.setBackgroundImage(#imageLiteral(resourceName: "delet"), for: .normal)
            } else {
                cell.plusMinusIcon.setBackgroundImage(#imageLiteral(resourceName: "add"), for: .normal)
            }
            
            //Button status update
            cell.plusMinusIcon.addTarget(self, action: #selector(self.plusMinusEmailAction(_:)), for: .touchUpInside)
            cell.plusMinusIcon.tag = indexPath.row
            //Email Handle
            cell.txtTitle.tag = indexPath.row
            cell.txtTitle.delegate = self
            if emailArr.count > 0 {
                if emailArr[0].length > 0 {
                    cell.txtTitle.placeholder = "Email Id".localized
                    cell.txtTitle.text = emailArr[0]
                } else {
                    cell.txtTitle.placeholder = "Enter Email Id".localized
                    cell.txtTitle.text = ""
                }
            }
            return cell
        } else if indexPath.row == 12 {
            let cellId = "SMSaleNLockEmailCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SMSaleNLockEmailCell
            
                cell.imgIcon.image = #imageLiteral(resourceName: "n_email")
                cell.plusMinusIcon.setBackgroundImage(#imageLiteral(resourceName: "delet"), for: .normal)
                cell.plusMinusIcon.addTarget(self, action: #selector(self.plusMinusEmailAction(_:)), for: .touchUpInside)
                cell.plusMinusIcon.tag = indexPath.row
            //Email Handle
            cell.txtTitle.tag = indexPath.row
            cell.txtTitle.delegate = self
            if emailArr[1].count > 0 {
                cell.txtTitle.placeholder = "Email Id".localized
                cell.txtTitle.text = emailArr[1]
            } else {
                cell.txtTitle.placeholder = "Enter Email Id".localized
                cell.txtTitle.text = ""
            }

            return cell
        } else {
            let cellId = "SMSaleNLockDetailsCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SMSaleNLockDetailsCell
            
            if indexPath.row == 1 {
                cell.txtTitle.placeholder = "Safety Cashier Mobile Number".localized
                cell.txtTitle.text = dataModel.phoneNumber!.replacingOccurrences(of: "0095", with: "(+95)0")
                cell.imgIcon.image = #imageLiteral(resourceName: "myanmar")
            } else if indexPath.row == 2 {
                cell.txtTitle.placeholder = "Name".localized
                cell.txtTitle.text = dataModel.name
                if dataModel.gender == true {
                cell.imgIcon.image = #imageLiteral(resourceName: "r_male")
                } else {
                    cell.imgIcon.image = #imageLiteral(resourceName: "r_female")
                }
            } else if indexPath.row == 3 {
                cell.txtTitle.placeholder = "NRC Number".localized
                cell.txtTitle.text = dataModel.NRC
                cell.imgIcon.image = #imageLiteral(resourceName: "nrc")
            } else if indexPath.row == 4 {
                cell.txtTitle.placeholder = "Joined On".localized
                if dataModel.joiningDate!.length > 0 {
                    cell.txtTitle.text = self.getJoiningDateTime(digit:dataModel.joiningDate ?? "")
                } else {
                    cell.txtTitle.text = ""
                }
                cell.imgIcon.image = #imageLiteral(resourceName: "r_user")
            }  else if indexPath.row == 5 {
                cell.txtTitle.placeholder = "Date of Birth".localized
                cell.txtTitle.text = dataModel.DOB
                cell.imgIcon.image = #imageLiteral(resourceName: "dob")
            } else if indexPath.row == 6 {
                let divisionTownshipName = getDivisionAndTownShip(divisionCode: dataModel.state ?? "", townshipCode: dataModel.township ?? "").split(separator: ",")
                cell.txtTitle.placeholder = "Division / State".localized
                if divisionTownshipName.count > 0 {
                    cell.txtTitle.text = "" + divisionTownshipName[1]
                } else {
                    cell.txtTitle.text = ""
                }
                cell.imgIcon.image = #imageLiteral(resourceName: "website")
            } else if indexPath.row == 7 {
                let divisionTownshipName = getDivisionAndTownShip(divisionCode: dataModel.state ?? "", townshipCode: dataModel.township ?? "").split(separator: ",")
                cell.txtTitle.placeholder = "Township".localized
                if divisionTownshipName.count > 0 {
                    cell.txtTitle.text = "" + divisionTownshipName[0]
                } else {
                    cell.txtTitle.text = ""
                }
                cell.imgIcon.image = #imageLiteral(resourceName: "r_township")
            } else if indexPath.row == 8 {
                cell.txtTitle.textColor = .black
                cell.txtTitle.placeholder = "City".localized
                cell.txtTitle.text = dataModel.cityName
                cell.imgIcon.image = #imageLiteral(resourceName: "r_city")
            }else if indexPath.row == 9 {
                cell.txtTitle.placeholder = "Village".localized
                cell.txtTitle.text = dataModel.villageName
                cell.imgIcon.image = #imageLiteral(resourceName: "village_track")
            }
            else if indexPath.row == 10 {
                cell.txtTitle.placeholder = "Status".localized
                cell.txtTitle.text = dataModel.isActive
                if dataModel.status == 0 {
                    cell.txtTitle.textColor = .red
                    cell.txtTitle.text = "Pending Approval".localized
                } else {
                    if(dataModel.isActive?.caseInsensitiveCompare("Active") == .orderedSame){
                        cell.txtTitle.textColor = UIColor(red: (0/255), green: (119/255), blue: (0/255), alpha: 1.0)
                        
                        cell.txtTitle.text = "Active"
                    } else {
                        cell.txtTitle.textColor = .blue
                        cell.txtTitle.text = "InActive"
                    }
                }
                cell.imgIcon.image = #imageLiteral(resourceName: "status")
            }
            cell.selectionStyle = .none
            return cell
        }
        }
    }
    
    //MARK: Custom Methods Action
    //Joining Date
    func getJoiningDateTime(digit: String) -> String {
        println_debug(digit)
        var FormateStr: String
        
        //calculate duration since join
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)

        // initially set the format based on your datepicker date
        formatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss.SS"
        formatter.setLocale()
        // convert your string to date
        let yourDate = formatter.date(from: digit)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MMM-yyyy"
        formatter.setLocale()
        // again convert your date to string
        FormateStr = formatter.string(from: yourDate!)
        
        return FormateStr
    }
    
    @objc @IBAction func plusMinusEmailAction(_ sender : UIButton){
        let intData = sender.tag
        if dataModel.villageName?.count == 0 {
            if intData == 10 {
                println_debug("first email")
                if emailArr.count > 1 {
                    emailArr.remove(at: 0)
                } else {
                    emailArr.append("")
                }
            } else {
                println_debug("2nd email")
                emailArr.removeLast()
            }
        }  else {
        if intData == 11 {
            println_debug("first email")
            if emailArr.count > 1 {
                emailArr.remove(at: 0)
            } else {
                emailArr.append("")
            }
        } else {
            println_debug("2nd email")
            emailArr.removeLast()
        }
        }
        saleNLockTbl.reloadData()
    }
    
    @objc @IBAction func btnInActiveAction(_ sender : UIButton){

        //self.ActiveInactiveSaleNLockApiCall(sender)

        if(dataModel.isActive?.caseInsensitiveCompare("Active") == .orderedSame){
            strMsg = "InActive"
        } else {
            strMsg = "Active"
        }
        alertViewObj.wrapAlert(title: "", body: "Do you want to change status to ".localized + strMsg + "?", img: #imageLiteral(resourceName: "alert-icon"))
        alertViewObj.addAction(title: "Cancel".localized, style: .target){}
        alertViewObj.addAction(title: "OK".localized, style: .target){
                self.ActiveInactiveSaleNLockApiCall(sender)
        }
        alertViewObj.showAlert(controller: self)
    }
    
    //MARK: API Request
    //UpdateEmail
    @objc func updateEmailApiCall() {
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let web = WebApiClass()
            web.delegate = self

            var emailID = ""
            if emailArr.count > 1 {
                if emailArr[0].count > 0 && emailArr[1].count > 0 {
                emailID = emailArr[0] + "," + emailArr[1]
                } else if emailArr[0].count > 0{
                    emailID = emailArr[0]
                } else if emailArr[1].count > 0{
                    emailID = emailArr[1]
                }
            } else {
                emailID = emailArr[0]
            }
            
            var arrEmailAPI = Array<Dictionary<String,Any>>()
            
            let dictEmailInfo = NSMutableDictionary()
            dictEmailInfo["EmailId"] = emailID
            dictEmailInfo["ExistingEmailId"] = dataModel.emailId ?? ""
            dictEmailInfo["OkAccNumber"] = dataModel.phoneNumber
            arrEmailAPI.append(dictEmailInfo as! Dictionary<String, Any>)

            let paramString : [String:Any] = [
                "LoginRequest": self.loginInfoDictionary(),
                "EmailId" : emailID,
                "ExistingEmailId" : dataModel.emailId ?? "",
                "AccountEmaiId": arrEmailAPI
            ]
            
            let urlS = String.init(format: Url.UpdateEmailDummyMerchant_API)
            let url =   getUrl(urlStr: urlS, serverType: .serverApp)
            //let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: paramString as AnyObject, httpMethod: "POST", mScreen: "UpdateEmail")
        }
    }
    
    //Reuqest for Approval
    @objc func activeApproveSaleNLockApiCall() {
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let web = WebApiClass()
            web.delegate = self
            let strId = dataModel.id ?? ""
            let urlS = String.init(format: Url.RequsteApprovalDummyMerchant_API, UserModel.shared.mobileNo, uuid, uuid, strId, "")
            let url =   getUrl(urlStr: urlS, serverType: .serverApp)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "ActivateUser")
        }
    }
    
    //Active & InActive request on estel server
    func ActiveInactiveSaleNLockApiCall(_ sender: UIButton) {
        
        if appDelegate.checkNetworkAvail(){
            
            self.showProgressView()
            
            let web = WebApiClass()
            web.delegate = self
            var activeInactive = ""
            
            if(dataModel.isActive?.caseInsensitiveCompare("Active") == .orderedSame){
                activeInactive = "INACTIVE"
                let object = dataModel.phoneNumber ?? ""
                let urlS = String.init(format: Url.DummyMerchantListStatusUpdate_API,UserModel.shared.mobileNo,object,activeInactive,ok_password ?? "",UserLogin.shared.token)
                let url =   getUrl(urlStr: urlS, serverType: .SaleNLockServerEstel)
                let param = [String:String]() as AnyObject
                web.genericClass(url: url, param: param, httpMethod: "POST", mScreen: "ActiveInactiveSaleNLock")
            } else {
                activeInactive = "ACTIVE"
                let object = dataModel.phoneNumber ?? ""
                let urlS = String.init(format: Url.DummyMerchantListStatusUpdate_API,UserModel.shared.mobileNo,object,activeInactive,ok_password ?? "",UserLogin.shared.token)
                let url =   getUrl(urlStr: urlS, serverType: .SaleNLockServerEstel)
                let param = [String:String]() as AnyObject
                web.genericClass(url: url, param: param, httpMethod: "POST", mScreen: "ActiveInactiveSaleNLock")
            }
            
        }
        else{
            
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
            
        }
    }
}

extension SMSaleNLockDetailsVC: WebServiceResponseDelegate {
    
    func webResponse(withJson json: AnyObject, screen: String) {
       if screen == "ActiveInactiveSaleNLock" {
            self.saleNLockParsing(data: json, mscreen: screen)
        } else if screen == "ActivateUser" {
            self.userActivationResponseParsing(data: json)
       } else if screen == "UpdateEmail" {
            self.updateEmailResponseParsing(data: json)
        }
    }
    
    //Email Update Parsing
    func updateEmailResponseParsing(data: AnyObject) {
        self.removeProgressView()
        do {
            if let safeData = data as? Data {
                let decoder = JSONDecoder()
                let approveSaleNLockUserResponse = try decoder.decode(ApproveSaleNLockUserResponse.self, from: safeData)
                if let code = approveSaleNLockUserResponse.code, code == 200 {
                    DispatchQueue.main.async {
                        
                        //if let msg = approveSaleNLockUserResponse.msg {
                            alertViewObj.wrapAlert(title: "", body: "Updated Successfully!".localized, img: #imageLiteral(resourceName: "check_sucess_bill"))
                            alertViewObj.addAction(title: "OK".localized, style: .target){
                                self.delegate?.reloadListView()
                                self.navigationController?.popViewController(animated: true)
                        }
                            alertViewObj.showAlert(controller: self)
                        //}
                    
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        if let msg = approveSaleNLockUserResponse.msg {
                            alertViewObj.wrapAlert(title: "", body: msg.localized, img: #imageLiteral(resourceName: "alert-icon"))
                            alertViewObj.addAction(title: "OK".localized, style: .target){}
                            alertViewObj.showAlert(controller: self)
                        }
                    }
                }
            }
        } catch let error {
            println_debug(error.localizedDescription)
        }
    }
    
    //User Activation Response Parsing
    func userActivationResponseParsing(data: AnyObject) {
        self.removeProgressView()
        do {
            if let safeData = data as? Data {
                let decoder = JSONDecoder()
                let approveSaleNLockUserResponse = try decoder.decode(ApproveSaleNLockUserResponse.self, from: safeData)
                if let code = approveSaleNLockUserResponse.code, code == 200 {
                    DispatchQueue.main.async {
                    
                        if let msg = approveSaleNLockUserResponse.msg {
                            self.updateEmailApiCall()

//                            alertViewObj.wrapAlert(title: "", body: msg.localized, img: #imageLiteral(resourceName: "check_sucess_bill"))
//                            alertViewObj.addAction(title: "OK".localized, style: .target){
//                                self.updateEmailApiCall()
//                            }
//                            alertViewObj.showAlert(controller: self)
                        }
                        DispatchQueue.main.async {
                            CoreDataHelper.shared.clearSMBFromDB()
                            profileObj.callUpdateProfileApi(aCode: UserModel.shared.mobileNo, handler: { (isSuccess) in
                                println_debug("Handle success and failure case")
                            })
                        }
                    }
                    if !UserDefaults.standard.bool(forKey: "IsAlreadyAdvMer") {
                        //self.getAllAddedBankID()
                    }
                } else {
                    DispatchQueue.main.async {
                        if let msg = approveSaleNLockUserResponse.msg {
                            alertViewObj.wrapAlert(title: "", body: msg.localized, img: #imageLiteral(resourceName: "alert-icon"))
                            alertViewObj.addAction(title: "OK".localized, style: .target){}
                            alertViewObj.showAlert(controller: self)
                        }
                    }
                }
            }
        } catch let error {
            println_debug(error.localizedDescription)
        }
    }
    
    //Active & InActive Response
    func saleNLockParsing(data: AnyObject, mscreen: String){
        
        let responseString = String(data: data as! Data, encoding: .utf8)
        let xml            = SWXMLHash.parse(responseString!)
        
        println_debug(xml)
        self.enumerateOld(indexer: xml)
        println_debug(self.saleNLockDict)
        
        if self.saleNLockDict.keys.contains("resultdescription"){
            
            DispatchQueue.main.async {
                
            if mscreen == "ActiveInactiveSaleNLock"{
                    if let message = self.saleNLockDict.safeValueForKey("resultdescription") as? String, message == "Transaction Successful" {
                        alertViewObj.wrapAlert(title: "", body: self.strMsg + " successfully".localized, img: #imageLiteral(resourceName: "info_success"))
                        alertViewObj.addAction(title: "OK".localized, style: .target){
                            self.delegate?.reloadListView()
                            self.navigationController?.popViewController(animated: true)
                        }
                        alertViewObj.showAlert(controller: self)
                    } else {
                        if let message = self.saleNLockDict.safeValueForKey("resultdescription") as? String {
                            alertViewObj.wrapAlert(title: "", body: message.localized, img: #imageLiteral(resourceName: "alert-icon"))
                            alertViewObj.addAction(title: "OK".localized, style: .target){}
                            alertViewObj.showAlert(controller: self)
                        }
                    }
                }
                self.removeProgressView()
            }
        }
    }
    
    //MARK:- Enumerate data from XML
    func enumerateOld(indexer: XMLIndexer) {
        for child in indexer.children {
            self.saleNLockDict[child.element!.name] = child.element!.text
            enumerateOld(indexer: child)
        }
    }
}


