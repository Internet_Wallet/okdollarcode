//
//  TransferCashBackViewController.swift
//  SettingVC
//
//  Created by Mohit on 10/19/17.
//  Copyright © 2017 Mohit. All rights reserved.
//

import UIKit



class SMTransferCashBackViewController: OKBaseController {
    
    var transferCBResponse = Dictionary<String, Any>()
    
    @IBOutlet weak var tfMobileNumber: UITextField! {
        didSet {
            tfMobileNumber.font = UIFont(name: appFont, size: 17.0)
            tfMobileNumber.placeholder = tfMobileNumber.placeholder?.localized
        }
    }
    @IBOutlet weak var tfAmount: UITextField! {
        didSet {
            tfAmount.font = UIFont(name: appFont, size: 17.0)
            tfAmount.placeholder = tfAmount.placeholder?.localized
        }
    }
    @IBOutlet weak var tfRemark: UITextField! {
        didSet {
            tfRemark.font = UIFont(name: appFont, size: 17.0)
            tfRemark.placeholder = tfRemark.placeholder?.localized
            //tfRemark.placeholder = NSLocalizedString("Type here", comment: "Type here")
            tfRemark.placeholder = tfRemark.placeholder?.localized
        }
    }
    @IBOutlet weak var btnSubmit: UIButton! {
        didSet {
            btnSubmit.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnSubmit.setTitle(btnSubmit.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var bottomBtnConstraint: NSLayoutConstraint!
    var buttonConstraint: NSLayoutConstraint!
    let notificationCenter = NotificationCenter.default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        borderView.layer.borderWidth = 1
        borderView.layer.borderColor = UIColor.colorWithRedValue(redValue: 200, greenValue: 200, blueValue: 200, alpha: 1).cgColor
        
        self.title = "Transfer CashBack".localized
        let mobileNumber = UserModel.shared.mobileNo
        var chnageNo = UserModel.shared.mobileNo
        chnageNo.remove(at: chnageNo.index(chnageNo.startIndex, offsetBy: 1))
        chnageNo.remove(at: chnageNo.index(chnageNo.startIndex, offsetBy: 1))
        chnageNo.remove(at: chnageNo.index(chnageNo.startIndex, offsetBy: 1))
        self.tfMobileNumber.text = chnageNo
        subscribeToShowKeyboardNotifications()
        tfAmount.addTarget(self, action: #selector(ResaleMyNumViewController.textFieldDidChange(_:)),
                           for: UIControl.Event.editingChanged)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        UIView.animate(withDuration: 1, animations: { () -> Void in
            self.bottomBtnConstraint.constant = keyboardHeight
        })
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        bottomBtnConstraint.constant = 0
    }
    
    func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if self.tfAmount.text != "" {
            self.btnSubmit.isHidden = false
        }else{
            self.btnSubmit.isHidden = true
        }
        self.tfAmount.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (UserDefaults.standard.value(forKey: "TextField_Clear") as? Bool) != nil{
            self.tfAmount.text = ""
            self.tfRemark.text = ""
            UserDefaults.standard.set(nil, forKey: "TextField_Clear")
        }
        
        if tfAmount.text == "" {
            self.btnSubmit.isHidden = true
        }else{
            self.btnSubmit.isHidden = false
        }
        notificationCenter.addObserver(self,
                                       selector: #selector(SessionAlert),
                                       name: .appTimeout,
                                       object: nil)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notificationCenter.removeObserver(self, name: .appTimeout, object: nil)
    }
    
    @objc func SessionAlert()  {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "", body: "Application Session expired login again".localized, img:  #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func submitClick(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.tfAmount.text == "" {
            self.customAlert(msg: "Please enter valid amount.".localized, title: "")
        }else {
            OKPayment.main.authenticate(screenName: "SMTransferCashBackViewController", delegate: self)
        }
    }
    
    
    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func moveToAnotherScreen() {
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "SMConfirmationViewController") as! SMConfirmationViewController
        let dict = NSMutableDictionary()
        dict["amount"] = self.tfAmount.text?.localized
        dict["remark"] = self.tfRemark.text?.localized
        println_debug(dict)
        VC.dataDict = dict
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    func transferCashBackApiCall(mobileNo: String) {
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let web = WebApiClass()
            web.delegate = self
            let urlS = String.init(format: Url.AgentInfo_API,mobileNo)
            let url = getUrl(urlStr: urlS, serverType: .serverEstel)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "POST", mScreen: "saleNLock")
        }else {
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func customAlert(msg:String) {
        let alert = UIAlertController(title: "ImageIcon", message: msg.localized, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    func customAlert(msg:String, title:String) {
        alertViewObj.wrapAlert(title: "", body: msg.localized, img: #imageLiteral(resourceName: "setting"))
        alertViewObj.addAction(title: "Done".localized, style: .target, action:{})
        alertViewObj.showAlert(controller: self)
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children{
            self.transferCBResponse[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    @IBAction func amountTFEditingChanged(_ sender: UITextField) {
        if tfAmount.text == "" {
            self.btnSubmit.isHidden = true
        }else {
            self.btnSubmit.isHidden = false
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if textField == tfAmount {
            
            if let text = textField.text, text.count > 0 {
                let formatter = NumberFormatter()
                formatter.numberStyle = NumberFormatter.Style.decimal
                
                //remove any existing commas
                let textAfterRemovingComma = text.replacingOccurrences(of: ",", with: "")
                
                let formattedNum = formatter.string(from: NSNumber(value: Int(textAfterRemovingComma)!))
                textField.text = formattedNum!
                
                if NSString(string: UserLogin.shared.walletBal).floatValue < NSString(string: text.replacingOccurrences(of: ",", with: "")).floatValue {
                    self.tfAmount.text = ""
                    self.btnSubmit.isHidden = true
                    alertViewObj.wrapAlert(title: "", body: "Insufficent Balance".localized, img: #imageLiteral(resourceName: "setting"))
                    alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {
                    })
                    alertViewObj.showAlert(controller: self)
                }
                
                if textAfterRemovingComma.count > 9 {
                    textField.text = String((textField.text?.dropLast())!)
                }
            }
        }
    }
    
}


extension SMTransferCashBackViewController: BioMetricLoginDelegate {
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful {
            self.transferCashBackApiCall(mobileNo: UserModel.shared.mobileNo)
        }
    }
}


extension SMTransferCashBackViewController : WebServiceResponseDelegate {
    
    func webResponse(withJson json: AnyObject, screen: String) {
        self.generateAuthCodeParsing(data: json)
    }
    
    func generateAuthCodeParsing(data: AnyObject) {
        
        let responseString = String(data: data as! Data, encoding: .utf8)
        let xml = SWXMLHash.parse(responseString!)
        println_debug(xml)
        self.enumerate(indexer: xml)
        println_debug(self.transferCBResponse)
        if let safeValue = transferCBResponse["resultdescription"], let safeResultDesc = safeValue as? String {
            if safeResultDesc == "Records Not Found" || safeResultDesc == "Transaction Successful" || safeResultDesc == "Invalid Secure Token" || safeResultDesc == "Secure Token Expire" || safeResultDesc == "Invalid Request (Schema Validation Failed)" || safeResultDesc == "Invalid PIN" {
                DispatchQueue.main.async {
                    self.removeProgressView()
                    self.moveToAnotherScreen()
                }
            }
        }else {
            //Handle your case
        }
    }
}

extension SMTransferCashBackViewController: UITextFieldDelegate {
    
    // MARK:  textfield delegate method
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.tfAmount {
            textField.keyboardType = .numberPad
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.tfAmount{
            textField.keyboardType = .numberPad
        }
        return true
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let _: String? = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if textField == self.tfAmount {
            
            if let text = textField.text, let _ = Range(range, in: text) {
                if (isBackSpace == -92) {
                    return true
                }else if string == "0" && (textField.text! + string).count <= 1 {
                    return false
                }else if string == "." {
                    return false
                }
                let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
                let compSepByCharInSet = string.components(separatedBy: aSet)
                let numberFiltered = compSepByCharInSet.joined(separator: "")
                return string == numberFiltered
            }
        }else if textField == self.tfRemark {
            //            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            //            let filteredStr = string.components(separatedBy: cs).joined(separator: "")
            //            guard string == filteredStr else {
            //                return false
            //            }
            if let text = textField.text, let _ = Range(range, in: text) {
                if (isBackSpace == -92){
                    return true
                }else if text.length >= 80  && text.count >= 80{
                    return false
                }
            }
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    
}


