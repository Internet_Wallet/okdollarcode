//
//  LockTypeViewController.swift
//  SettingVC
//
//  Created by Mohit on 10/16/17.
//  Copyright © 2017 Mohit. All rights reserved.
//

import UIKit

class SMLockTypeViewController: OKBaseController {
    
    fileprivate let cellId = "LockTypeViewCell"
    let notificationCenter = NotificationCenter.default
    var tblLabelArr = [String]()
    var tblIconArr = [UIImage]()
    @IBOutlet weak var SMLockTypeTbl: UITableView!
    @IBOutlet var btnPassword: UIButton!{
        didSet
        {
            btnPassword.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnPassword.setTitle(btnPassword.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var btnPattern: UIButton!{
        didSet
        {
            btnPattern.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnPattern.setTitle(btnPattern.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var btnViewPwdPattern: UIButton!{
        didSet
        {
            btnViewPwdPattern.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnViewPwdPattern.setTitle(btnViewPwdPattern.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Select Lock Type".localized
        tblLabelArr = [
            "Password".localized,
            "Pattern".localized,
            "View Pattern OR Password".localized
        ]
        tblIconArr  =  [
            #imageLiteral(resourceName: "password"),
            #imageLiteral(resourceName: "pattern"),
            #imageLiteral(resourceName: "viewpattern_password"),
        ]
        self.SMLockTypeTbl.tableFooterView = UIView.init()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notificationCenter.addObserver(self,   selector: #selector(SessionAlert), name: .appTimeout,   object: nil)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notificationCenter.removeObserver(self, name: .appTimeout, object: nil)
    }
    
    @objc func SessionAlert()  {
        DispatchQueue.main.async {
            
            alertViewObj.wrapAlert(title: "", body: "Application Session expired login again".localized, img:  #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){
                if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    @IBAction func patternClick(_ sender: UIButton) {
        self.moveToController(identifier: "SMPatternViewController")
    }
    
    
    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //    @IBAction func passwordClick(_ sender: UIButton) {
    func passwordClick(Title: NSString) {
        userDef.set(Title, forKey: "statedef")
        userDef.synchronize()
        
        if !ok_password_type{
            // password
            self.moveToController(identifier: "SMChangePasswordViewController")
        }
        else{
            // pattern
            self.moveToController(identifier: "SMPatternViewController")
        }
    }
    
    
    @IBAction func viewPatternAndPasswordClick(_ sender: Any) {
        alertViewObj.wrapAlert(title: "", body: ("Your password is".localized + " \(ok_password!)").localized, img: #imageLiteral(resourceName: "r_lock"))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {})
        alertViewObj.showAlert(controller: self)
    }
    
    
    func moveToController(identifier: String){
        let VC = self.storyboard?.instantiateViewController(withIdentifier: identifier)
        self.navigationController?.pushViewController(VC!, animated: true)
    }
    
}

extension SMLockTypeViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SettingTableViewCell
        let icon  = tblIconArr[indexPath.row]
        let name  = tblLabelArr[indexPath.row]
        cell.wrapData(img: icon, name: name)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tblLabelArr.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.switchtoViewController(index: indexPath.row)
    }
    
    
    func switchtoViewController(index : Int){
        
        let strTitle = self.tblLabelArr[index] as NSString
        
        switch index{
            
        case 0:
            
            self.passwordClick(Title: strTitle)
            
            break
            
        case 1:
            
            self.passwordClick(Title: strTitle)
            
            break
            
        default:
            
            alertViewObj.wrapAlert(title: "", body: ("Your password is".localized + " \(ok_password!)").localized, img: #imageLiteral(resourceName: "r_lock"))
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: { })
            
            alertViewObj.showAlert(controller: self)
            break
        }
    }
}

extension SMLockTypeViewController: BioMetricLoginDelegate {
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if isSuccessful {
            println_debug("Authorised")
        }
        else{
            if UserLogin.shared.loginSessionExpired { OKPayment.main.authenticate(screenName: "Dashboard", delegate: self) }
        }
    }
}
