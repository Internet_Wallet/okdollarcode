//
//  ConfirmationViewController.swift
//  OK
//
//  Created by Mohit on 10/30/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class SMConfirmationViewController: OKBaseController {
    var dataDict = NSMutableDictionary()
    var confirmDict = Dictionary<String, Any>()
    var seconds        = 60
    var timer          = Timer()
    var isTimerRunning = false
    let notificationCenter = NotificationCenter.default
    
    
    @IBOutlet var rmkImg: UIImageView!
    
    @IBOutlet var btnpay: UIButton! {
        didSet {
            btnpay.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnpay.setTitle(btnpay.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var receiver: UILabel! {
        didSet {
            receiver.font = UIFont(name: appFont, size: appFontSize)
            receiver.text = receiver.text?.localized
        }
    }
    @IBOutlet var remark: UILabel! {
        didSet {
            remark.font = UIFont(name: appFont, size: appFontSize)
            remark.text = remark.text?.localized
        }
    }
    @IBOutlet var amount: UILabel! {
        didSet {
            amount.font = UIFont(name: appFont, size: appFontSize)
            amount.text = amount.text?.localized
        }
    }
    @IBOutlet var name: UILabel! {
        didSet {
            name.font = UIFont(name: appFont, size: appFontSize)
            name.text = name.text?.localized
        }
    }
    
    @IBOutlet var second: UILabel! {
        didSet {
            second.font = UIFont(name: appFont, size: appFontSize)
            second.text = second.text?.localized
        }
    }
    @IBOutlet weak var lblMobileNumber: UILabel! {
        didSet {
            lblMobileNumber.font = UIFont(name: appFont, size: appFontSize)
            lblMobileNumber.text = lblMobileNumber.text?.localized
        }
    }
    @IBOutlet weak var lblName: UILabel! {
        didSet {
            lblName.font = UIFont(name: appFont, size: appFontSize)
            lblName.text = lblName.text?.localized
        }
    }
    @IBOutlet weak var lblAmountValue: UILabel! {
        didSet {
            lblAmountValue.font = UIFont(name: appFont, size: appFontSize)
            lblAmountValue.text = lblAmountValue.text?.localized
        }
    }
    @IBOutlet weak var lblRemark: UILabel! {
        didSet {
            lblRemark.font = UIFont(name: appFont, size: appFontSize)
            lblRemark.text = lblRemark.text?.localized
        }
    }
    @IBOutlet var timerLabel: UILabel! {
        didSet {
            timerLabel.font = UIFont(name: appFont, size: appFontSize)
            timerLabel.text = timerLabel.text?.localized
        }
    }
    @IBOutlet var timerSuperview: UIView!
    @IBOutlet var mainViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var infoViewHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.timerLabel.clipsToBounds = true
        self.timerLabel.layer.cornerRadius = self.timerLabel.frame.width / 2
        self.timerLabel.layer.borderWidth = 1
        self.timerLabel.layer.borderColor = UIColor.black.cgColor
        self.title = "Confirmation".localized
        var chnageNo = UserModel.shared.mobileNo
        chnageNo.remove(at: chnageNo.index(chnageNo.startIndex, offsetBy: 0))
        chnageNo.remove(at: chnageNo.index(chnageNo.startIndex, offsetBy: 0))
        self.lblMobileNumber.text = "+" + chnageNo
        self.lblName.text = UserModel.shared.name
        self.lblAmountValue.text = (dataDict["amount"] as? String ?? "")
        self.lblRemark.text = (dataDict["remark"] as? String ?? "")
        self.lblAmountValue.text = (dataDict["amount"] as? String ?? "")
        
        if dataDict["remark"] as? String == "" {
            infoViewHeightConstraint.constant = 150
            remark.isHidden = true
            rmkImg.isHidden = true
            lblRemark.isHidden = true
        }else {
            self.lblRemark.text = (dataDict["remark"] as? String ?? "")
            let font = UIFont.init(name: appFont, size: 17)
            let remarkLabelHeight = heightForView(text: self.lblRemark.text!, font: font!, width: screenWidth - 160)
            if remarkLabelHeight > 30 {
                mainViewHeightConstraint.constant = screenHeight + 220
                infoViewHeightConstraint.constant = remarkLabelHeight + 220
            } else {
                infoViewHeightConstraint.constant = 220
            }
        }
        mainViewHeightConstraint.constant = screenHeight
        NotificationCenter.default.addObserver(self, selector: #selector(getCurrentTimer(notification:)), name: .updateTimer, object: nil)
    }
    
    
    @objc func getCurrentTimer(notification: NSNotification) {
        GlobalConstants.timerValue = seconds
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.singlePay()
        notificationCenter.addObserver(self, selector: #selector(SessionAlert), name: .appTimeout, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notificationCenter.removeObserver(self, name: .appTimeout, object: nil)
        timer.invalidate()
        println_debug("Timer Invalidated")
    }
    
    
    @objc func SessionAlert()  {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "", body: "Application Session expired login again".localized, img:  #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){                
                self.navigationController?.popToRootViewController(animated: true)
                //NotificationCenter.default.post(name: .goDashboardScreen, object: nil)
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    func singlePay() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(SMConfirmationViewController.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        seconds -= 1
        timerLabel.text = "0:" + "\(seconds)"
        if seconds == 0 {
            if timer.isValid {
                self.navigationController?.popViewController(animated: true)
                timer.invalidate()
            }
        } else if seconds < 10 {
            timerSuperview.layer.borderColor = UIColor.red.cgColor
            self.timerLabel.textColor = UIColor.red
            UIView.animate(withDuration: 0.2, animations: {
                self.timerLabel.alpha = 0.0
            }) { (bool) in
                self.timerLabel.alpha = 1.0
            }
        }
    }
    
    func viewLayouting(onView v : UIView) {
        v.layer.cornerRadius = v.frame.width / 2
        v.clipsToBounds = true
        v.layer.borderColor = UIColor.init(hex: "F3C632").cgColor
        v.layer.borderWidth = 1.5
        v.layer.shadowColor = UIColor.lightGray.cgColor
        v.layer.shadowOpacity = 0.8
        v.layer.shadowRadius = 2.0
        v.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
    }
    
    
    
}

extension SMConfirmationViewController {
    
    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func payClick(_ sender: UIButton) {
        self.PayApiCall(mobileNo: UserModel.shared.mobileNo)
    }
    
    func customAlert(msg:String){
        let alert = UIAlertController(title: "ImageIcon", message: msg.localized, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func PayApiCall(mobileNo: String){
        
        if appDelegate.checkNetworkAvail(){
            self.showProgressView()
            let web = WebApiClass()
            web.delegate = self
            println_debug(Url.TransferToKickBack_APi)
            println_debug(mobileNo)
            println_debug(self.lblAmountValue.text!)
            println_debug(self.lblRemark.text!)
            println_debug(ok_password!)
            println_debug(appDelegate.getWiFiAddress()!)
            println_debug(UserLogin.shared.token)
            let strAmount = self.lblAmountValue.text?.replacingOccurrences(of: ",", with: "") ?? "0"
            let iostype = "IOS%3Dhtc_a5mgp_dug"
            var urlS = String.init(format: Url.TransferToKickBack_APi,mobileNo,mobileNo,strAmount,self.lblRemark.text!,ok_password!,appDelegate.getWiFiAddress()!,iostype,UserLogin.shared.token)
            urlS = urlS.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            println_debug(urlS)
            let url =   getUrl(urlStr: urlS, serverType: .serverEstel)
            println_debug(url)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "POST", mScreen: "Pay")
        }
        else{
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    func enumerate(indexer: XMLIndexer){
        
        for child in indexer.children{
            self.confirmDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
        
    }
}

extension SMConfirmationViewController : WebServiceResponseDelegate {
    
    func webResponse(withJson json: AnyObject, screen: String){
        self.generateAuthCodeParsing(data: json)
    }
    
    func generateAuthCodeParsing(data: AnyObject) {
        let responseString = String(data: data as! Data, encoding: .utf8)
        let xml = SWXMLHash.parse(responseString!)
        println_debug(xml)
        self.enumerate(indexer: xml)
        println_debug(self.confirmDict)
        if self.confirmDict.keys.contains("resultdescription") {
            DispatchQueue.main.async {
                if  self.confirmDict["resultdescription"] as? String ?? "" == "Transaction Successful" {
                    self.removeProgressView()
                    let VC = self.storyboard?.instantiateViewController(withIdentifier: "SMPaymentSuccessFullViewController") as! SMPaymentSuccessFullViewController
                    VC.dataDict = self.confirmDict
                    self.navigationController?.pushViewController(VC, animated: true)
                }else {
                    alertViewObj.wrapAlert(title: self.confirmDict["resultdescription"] as? String ?? "", body: "Please Try Again Later.".localized, img: #imageLiteral(resourceName: "alert-icon"))
                    alertViewObj.addAction(title: "OK".localized, style: .target){}
                    alertViewObj.showAlert(controller: self)
                }
            }
        }
    }
}

extension String {
    func getIPAddress() -> String? {
        var address : String?
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return nil }
        guard let firstAddr = ifaddr else { return nil }
        
        // For each interface ...
        for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let interface = ifptr.pointee
            
            // Check for IPv4 or IPv6 interface:
            let addrFamily = interface.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                // Check interface name:
                let name = String(cString: interface.ifa_name)
                if  name == "en0" || name == "pdp_ip0" {
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                                &hostname, socklen_t(hostname.count),
                                nil, socklen_t(0), NI_NUMERICHOST)
                    address = String(cString: hostname)
                }
            }
        }
        freeifaddrs(ifaddr)
        return address
    }
}

extension SMConfirmationViewController: BioMetricLoginDelegate {
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful {
            println_debug("Authorised")
        }else {
            if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
        }
    }
}

