//
//  AddPromotionViewController.swift
//  SettingVC
//
//  Created by Mohit on 10/20/17.
//  Copyright © 2017 Mohit. All rights reserved.
//

import UIKit

let REGEX_PHONE_DEFAULT = "[0-9]{3}\\-[0-9]{3}\\-[0-9]{4}"
let REGEX_USER_NAME_LIMIT = "^.{3,10}$"

class SMAddPromotionViewController: OKBaseController {
    
    let notificationCenter = NotificationCenter.default
    var responseDict = Dictionary<String, Any>()
    var accessAddressFromGEOLocation = Dictionary <String,String>()
    
    @IBOutlet weak var tfPromotionTitle: TextFieldValidator!{
        didSet
        {
            tfPromotionTitle.font = UIFont(name: appFont, size: 17.0)
            tfPromotionTitle.placeholder = tfPromotionTitle.placeholder?.localized
        }
    }
    @IBOutlet weak var tfDescription: UITextView!{
        didSet
        {
            tfDescription.font = UIFont(name: appFont, size: 17.0)
            tfDescription.text = tfDescription.text?.localized
        }
    }
    @IBOutlet weak var tfContactPerson: UITextField!{
        didSet
        {
            tfContactPerson.font = UIFont(name: appFont, size: 17.0)
            tfContactPerson.placeholder = tfContactPerson.placeholder?.localized
        }
    }
    @IBOutlet weak var tfMobileNo: UITextField!{
        didSet
        {
            tfMobileNo.font = UIFont(name: appFont, size: 17.0)
            tfMobileNo.placeholder = tfMobileNo.placeholder?.localized
        }
    }
    @IBOutlet weak var tfContactEmail: UITextField!{
        didSet
        {
            tfContactEmail.font = UIFont(name: appFont, size: 17.0)
            tfContactEmail.placeholder = tfContactEmail.placeholder?.localized
        }
    }
    @IBOutlet weak var tfCompanyWebsite: UITextField!{
        didSet
        {
            tfCompanyWebsite.font = UIFont(name: appFont, size: 17.0)
            tfCompanyWebsite.placeholder = tfCompanyWebsite.placeholder?.localized
        }
    }
    @IBOutlet weak var tfacebookPage: UITextField!{
        didSet
        {
            tfacebookPage.font = UIFont(name: appFont, size: 17.0)
            tfacebookPage.placeholder = tfacebookPage.placeholder?.localized
        }
    }
    @IBOutlet weak var btnSubmit: UIButton!{
        didSet
        {
            self.btnSubmit.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.btnSubmit.setTitle(self.btnSubmit.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var lblastUpdated: UILabel!{
        didSet
        {
            self.lblastUpdated.font = UIFont(name: appFont, size: appFontSize)
            //            lblastUpdated.text =  (lblastUpdated.text?.localized)!
            lblastUpdated.textAlignment = .center
           // lblastUpdated.lineBreakMode = .byWordWrapping
          //  lblastUpdated.numberOfLines = 4
        }
    }
    @IBOutlet var protitle: UILabel!{
        didSet
        {
            self.protitle.font = UIFont(name: appFont, size: appFontSize)
            protitle.text = "  " + (protitle.text?.localized)!
        }
    }
    @IBOutlet var descr: UILabel!{
        didSet
        {
            self.descr.font = UIFont(name: appFont, size: appFontSize)
            descr.text = "  " + (descr.text?.localized)!
        }
    }
    @IBOutlet var contactInfo: UILabel!{
        didSet
        {
            self.contactInfo.font = UIFont(name: appFont, size: appFontSize)
            contactInfo.text = "  " + (contactInfo.text?.localized)!
        }
    }
    @IBOutlet weak var lblastheightContraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 15) ?? UIFont.systemFont(ofSize: 15)]
        self.title = "Add/View Promotion".localized
        println_debug("Value Access by previous Controller \(accessAddressFromGEOLocation)")
        self.tfContactEmail.delegate = self
        tfDescription.delegate = self
        tfDescription.text = "Write your promotion description (max 500 chars)".localized
        tfDescription.textColor = UIColor.lightGray
        self.view.isUserInteractionEnabled = true
       
        if (UserDefaults.standard.object(forKey: "shopID") as? String) != nil{
            self.PromotionViewApiCall()
        }
        else{
            self.getShopIdApiCall()
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notificationCenter.addObserver(self,selector: #selector(SessionAlert),name: .appTimeout,object: nil)
        
        self.tfCompanyWebsite.text = ""
        self.tfacebookPage.text = ""
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notificationCenter.removeObserver(self, name: .appTimeout, object: nil)
    }
    
    @objc func SessionAlert()  {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "", body: "Application Session expired login again".localized, img:  #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){
                if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    @IBAction func btnEditClick(_ sender: UIBarButtonItem){
        
        lblastheightContraint.constant = 0
        self.lblastUpdated.isHidden = true
        self.btnSubmit.isHidden = false
        self.tfCompanyWebsite.text = "http://www."
        self.tfacebookPage.text = "http://www.facebook.com/"
        self.view.isUserInteractionEnabled = true
        self.tfCompanyWebsite.isUserInteractionEnabled = true
        self.tfacebookPage.isUserInteractionEnabled = true
        self.tfPromotionTitle.isUserInteractionEnabled = true
        self.tfDescription.isUserInteractionEnabled = true
        self.tfContactPerson.isUserInteractionEnabled = true
        self.tfMobileNo.isUserInteractionEnabled = true
        self.tfContactEmail.isUserInteractionEnabled = true
        
        sender.isEnabled = false
        sender.tintColor = UIColor.clear
    }
    
    
    @objc func rightBtnClick(){
         lblastheightContraint.constant = 0
        self.lblastUpdated.isHidden = true
        self.btnSubmit.isHidden = false
        self.view.isUserInteractionEnabled = true
        self.tfCompanyWebsite.isUserInteractionEnabled = true
        self.tfacebookPage.isUserInteractionEnabled = true
        self.tfPromotionTitle.isUserInteractionEnabled = true
        self.tfDescription.isUserInteractionEnabled = true
        self.tfContactPerson.isUserInteractionEnabled = true
        self.tfMobileNo.isUserInteractionEnabled = true
        self.tfContactEmail.isUserInteractionEnabled = true
    }
    
    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func submitClick(_ sender: UIButton){
        
        if self.tfPromotionTitle.text == ""{
            self.tfPromotionTitle.isMandatory = true
            self.tfPromotionTitle.validate()
        }
        else if !isValidEmail(testStr : tfContactEmail.text!){
            alertViewObj.wrapAlert(title: "", body: "Invalid Email", img: #imageLiteral(resourceName: "setting"))
            alertViewObj.addAction(title: "OK".localized, style: .target, action:{})
            alertViewObj.showAlert(controller: self)
        }
        else{
            self.addPromotionApiCall(title: self.tfPromotionTitle.text!, description: self.tfDescription.text!, contactName: self.tfContactPerson.text!, mobileNo: self.tfMobileNo.text!, Email: self.tfContactEmail.text!, website: self.tfCompanyWebsite.text!, fbpage: self.tfacebookPage.text!)
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    func getShopIdApiCall(){
        self.showProgressView()
        if appDelegate.checkNetworkAvail(){
            let web = WebApiClass()
            web.delegate = self
            guard let sayHello = UserDefaults.standard.object(forKey: "curAddress") as? String else { return }
            let result = sayHello.split(separator: ",")
            let long = UserDefaults.standard.string(forKey: "long")
            let lat =  UserDefaults.standard.string(forKey: "lat")
            println_debug(result)
            let urlS = String.init(format: Url.getShopId,UserModel.shared.mobileNo,UserModel.shared.simID,lat ?? "0.0",long ?? "0.0","",UserDefaults.standard.string(forKey: "alerText") ?? "","","1",uuid,result[safe: 2] as CVarArg? ?? "",result[safe: 1] as CVarArg? ?? "","",result[safe: 1] as CVarArg? ?? "")
            let allowedCharacterSet = (CharacterSet(charactersIn: " ").inverted)
            if let escapedString = urlS.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet){
                let url =   getUrl(urlStr: escapedString, serverType: .serverApp)
                let param = [String:String]() as AnyObject
                web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "getShopId")
            }
        }
        else{
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    func BroadCastGPSApiCall(){
        self.showProgressView()
        if appDelegate.checkNetworkAvail(){
            let web = WebApiClass()
            web.delegate = self
            let sayHello = UserDefaults.standard.object(forKey: "curAddress") as? String
            let result = sayHello?.split(separator: ",")
            //println_debug(result!)
            let urlS = String.init(format: Url.BroadcastGPS,UserModel.shared.mobileNo,UserModel.shared.simID,UserDefaults.standard.object(forKey: "lat") as! CVarArg,UserDefaults.standard.object(forKey: "long") as! CVarArg,"","1",UserDefaults.standard.object(forKey: "shopID") as! CVarArg,result![2] as CVarArg,result![1] as CVarArg,result![1] as CVarArg)
            let allowedCharacterSet = (CharacterSet(charactersIn: " ").inverted)
            if let escapedString = urlS.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) {
                let url =   getUrl(urlStr: escapedString, serverType: .serverApp)
                let param = [String:String]() as AnyObject
                web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "BroadcastGPS")
            }
        }
        else{
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func getPromotionIdApiCall(title:String,description:String,contactName:String,mobileNo:String,Email:String,website:String,fbpage:String){
        self.showProgressView()
        if appDelegate.checkNetworkAvail(){
            let web = WebApiClass()
            web.delegate = self
            let urlS = String.init(format: Url.getPromotionId,UserModel.shared.mobileNo,UserModel.shared.simID,"","1",uuid,title,description,website,mobileNo,fbpage,UserDefaults.standard.object(forKey: "shopID") as! CVarArg,false as CVarArg,contactName,Email)
            let allowedCharacterSet = (CharacterSet(charactersIn: " ").inverted)
            if let escapedString = urlS.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet){
                let url =   getUrl(urlStr: escapedString, serverType: .serverApp)
                let param = [String:String]() as AnyObject
                web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "getPromotionId")
            }
        } else {
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func PromotionViewApiCall(){
        self.showProgressView()
        if appDelegate.checkNetworkAvail() {
            let web = WebApiClass()
            web.delegate = self
            let urlS = String.init(format: Url.promotionView,UserModel.shared.mobileNo,UserModel.shared.simID,"","1",uuid,UserDefaults.standard.object(forKey: "PromotionId") as! CVarArg)
            let allowedCharacterSet = (CharacterSet(charactersIn: " ").inverted)
            if let escapedString = urlS.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) {
                let url =   getUrl(urlStr: escapedString, serverType: .serverApp)
                let param = [String:String]() as AnyObject
                web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "PromotionView")
            }
        } else {
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func addPromotionApiCall(title:String,description:String,contactName:String,mobileNo:String,Email:String,website:String,fbpage:String){
        
        println_debug(title)
        println_debug(description)
        println_debug(contactName)
        println_debug(mobileNo)
        println_debug(Email)
        println_debug(website)
        println_debug(fbpage)
        
        self.showProgressView()
        
        if appDelegate.checkNetworkAvail(){
            
            let web = WebApiClass()
            web.delegate = self
            var urlS = String()
            if UserDefaults.standard.object(forKey: "PromotionId") == nil{
                urlS = String.init(format: Url.addPromotion, UserModel.shared.mobileNo,UserModel.shared.simID,"","1",uuid,title,description,website,mobileNo,fbpage,UserDefaults.standard.object(forKey: "shopID") as! CVarArg,"false",contactName,Email)
            }
            else{
                urlS = String.init(format: Url.addPromotionwithID, UserModel.shared.mobileNo,UserModel.shared.simID,"","1",uuid,title,description,website,mobileNo,fbpage,UserDefaults.standard.object(forKey: "shopID") as! CVarArg,"false",UserDefaults.standard.object(forKey: "PromotionId") as! CVarArg,contactName,Email)
            }
            
            let allowedCharacterSet = (CharacterSet(charactersIn: " ").inverted)
            if let escapedString = urlS.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) {
                let url =   getUrl(urlStr: escapedString, serverType: .serverApp)
                let param = [String:String]() as AnyObject
                web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "addPromotion")
            }
        }
        else{
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    
    func enumerate(indexer: XMLIndexer){
        
        for child in indexer.children{
            self.responseDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
        
    }
    
    
    func isValid(_ email: String) -> Bool {
        
        let emailRegEx = "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"+"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"+"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"+"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"+"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"+"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"+"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: email)
        
    }
    
    
    
    func customAlert(msg:String, title:String) {
        
        alertViewObj.wrapAlert(title: "", body: msg, img: #imageLiteral(resourceName: "setting"))
        alertViewObj.addAction(title: "Done".localized, style: .target, action:{
            if UserDefaults.standard.object(forKey: "PromotionId") != nil{
                self.PromotionViewApiCall()
            }
        })
        alertViewObj.showAlert(controller: self)
        
    }
    
    func updateDataInOutlet(object: Dictionary<String,Any>){
        println_debug(object)
        self.tfPromotionTitle.text = (object["PromotionHeader"] as? String)?.localized
        
        let currentStr = UserDefaults.standard.string(forKey: "currentLanguage")
        let str = object["Description"] as? String
    
        if currentStr == "my" {
            if str == "Write your promotion description (max 500 chars)" {
            self.tfDescription.text = "ပ႐ိုမိုးရွင္း အစီအစဥ္ ရွင္းလင္းခ်က္ ႐ိုက္ထည့္ပါ (စာလံုးေရ ၅၀၀ လံုး)"
            }
            else {
                self.tfDescription.text = object["Description"] as? String
            }
        } else if currentStr == "en"{
            if str == "ပ႐ိုမိုးရွင္း အစီအစဥ္ ရွင္းလင္းခ်က္ ႐ိုက္ထည့္ပါ (စာလံုးေရ ၅၀၀ လံုး)" {
                self.tfDescription.text = "Write your promotion description (max 500 chars)"
            }
            else {
               self.tfDescription.text = object["Description"] as? String
            }
        }
        else {
            self.tfDescription.text = object["Description"] as? String
        }
        //self.tfDescription.text = (object["Description"] as? String)?.localized
        self.tfContactPerson.text = (object["ContactPersonName"] as? String)?.localized
        self.tfMobileNo.text = (object["PhoneNumber"] as? String)?.localized
        self.tfContactEmail.text = (object["EmailId"] as? String)?.localized
        self.tfacebookPage.text = (object["FacebookId"] as? String)?.localized
        self.tfCompanyWebsite.text = (object["WebUrl"] as? String)?.localized
        
    }
}


extension SMAddPromotionViewController: UITextFieldDelegate,UITextViewDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        
        textField.resignFirstResponder()
        
        if textField == self.tfContactEmail{
            if isValid(self.tfContactEmail.text!){
                return false
            }
            else{
                self.showErrorAlert(errMessage: "Validate email".localized)
            }
        }
        
        return true
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if tfDescription.textColor == UIColor.lightGray {
            
            tfDescription.text = ""
            tfDescription.textColor = UIColor.black
            
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if tfDescription.text == "" {
            
            tfDescription.text = "Write your promotion description (max 500 chars)".localized
            tfDescription.textColor = UIColor.lightGray
            
        }
    }
    
}

extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.setLocale()
        return dateFormatter.string(from: self)
    }
    
}

extension SMAddPromotionViewController : WebServiceResponseDelegate {
    
    func webResponse(withJson json: AnyObject, screen: String){
        
        println_debug("Screen :- \(screen)")
        if screen == "addPromotion"{
            do{
                if let data = json as? Data{
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject>{
                        println_debug(dic)
                        if dic["Code"] as! Int == 200{
                            DispatchQueue.main.async{
                                if UserDefaults.standard.object(forKey: "PromotionId") != nil{
                                    alertViewObj.wrapAlert(title: "", body: "Promotion successfully added".localized, img: #imageLiteral(resourceName: "h_postpaid"))
                                    alertViewObj.addAction(title: "OK".localized, style: .target){}
                                    alertViewObj.showAlert(controller: self)
                                    
                                    self.lblastUpdated.text = "Last Updated:".localized.appending(UserDefaults.standard.value(forKey: "dateAndTime") as! String)
                              //      self.lblastUpdated.lineBreakMode = .byWordWrapping
                              //      self.lblastUpdated.numberOfLines = 4
                                    self.lblastUpdated.textAlignment = .center
                                    // change to desired number of seconds (in this case 5 seconds)
                                    let when = DispatchTime.now() + 1
                                    DispatchQueue.main.asyncAfter(deadline: when){
                                        self.navigationController?.popToRootViewController(animated: true)
                                        //NotificationCenter.default.post(name: .goDashboardScreen, object: nil)
                                    }
                                }
                                guard let msg = dic["Data"] as? String else {
                                    return
                                }
                                if let promoDetail =  OKBaseController.convertToDictionary(text: msg ) {
                                    println_debug(promoDetail)
                                    println_debug("Promotion det \(String(describing: promoDetail["PromotionId"]))")
                                    guard let arrDict = promoDetail["Promotion Details"] as? Array<Dictionary<String,Any>> else {
                                        return
                                    }
                                    for element in arrDict {
                                        self.accessAddressFromGEOLocation["PromotionId"] = element["PromotionId"] as? String
                                        UserDefaults.standard.set((element["PromotionId"] as? String), forKey: "PromotionId")
                                        UserDefaults.standard.synchronize()
                                        if UserDefaults.standard.object(forKey: "PromotionId") != nil{
                                            self.PromotionViewApiCall()
                                        }
                                        break
                                    }
                                }
                                self.removeProgressView()
                            }
                        }
                    }
                }
            } catch{
                
            }
        }
        
        if screen == "getShopId"{
            
            do{
                if let data = json as? Data{
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject>{
                        let data  = dic["Data"] as! String
                        println_debug(dic)
                        if dic["Msg"] as? String ?? "" == "success"{
                            DispatchQueue.main.async{
                                self.accessAddressFromGEOLocation["shopID"] = data
                                UserDefaults.standard.set(data, forKey: "shopID")
                                userDef.set(true, forKey: "ShopOnOff")
                                userDef.synchronize()
                                
                                let now = Date()

                                let formatter = DateFormatter()

                                formatter.timeZone = TimeZone.current

                                formatter.dateFormat = "EE, dd-MMM-yyyy, HH:mm:ss"

                                let dateString = formatter.string(from: now)
                                
                                print(dateString)
                                
//                                let someOtherDateTime = Date()
//                                let dateAndTime = someOtherDateTime.toString(dateFormat: "EE, dd-MMM-yyyy, HH:mm:ss")
                                UserDefaults.standard.set(dateString, forKey: "dateAndTime")
                                UserDefaults.standard.synchronize()
                                self.lblastUpdated.textAlignment = .center
                       //         self.lblastUpdated.lineBreakMode = .byWordWrapping
                         //       self.lblastUpdated.numberOfLines = 4
                                self.lblastUpdated.text = "Last Updated:".localized.appending(UserDefaults.standard.value(forKey: "dateAndTime") as! String)
                                
                                self.BroadCastGPSApiCall()
                                self.removeProgressView()
                            }
                        }
                        else if dic["Code"] as? Int == 302{
                            DispatchQueue.main.async{
                                
                                alertViewObj.wrapAlert(title: "", body: (dic["Msg"] as? String)!, img: #imageLiteral(resourceName: "setting"))
                                alertViewObj.addAction(title: "Done".localized, style: .target, action:{
                                    self.navigationController?.popToRootViewController(animated: true)
                                    //NotificationCenter.default.post(name: .goDashboardScreen, object: nil)
                                    
                                })
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                        else{
                            DispatchQueue.main.async{
                                self.removeProgressView()
                            }
                        }
                    }
                    
                }
            } catch{
                
            }
        }
        
        
        if screen == "getPromotionId"{
            
            self.PromotionViewApiCall()
            
        }
        
        
        if screen == "BroadcastGPS"{
            
            do {
                
                if UserDefaults.standard.object(forKey: "PromotionId") == nil{
                    
                    self.addPromotionApiCall(title: "No promotion set".localized, description: "Write your promotion description (max 500 chars)".localized, contactName: UserModel.shared.name, mobileNo: UserModel.shared.mobileNo, Email: UserModel.shared.email, website: UserModel.shared.website, fbpage: UserModel.shared.fbEmailId)
                }
                else{
                    self.addPromotionApiParsing(data: json)
                }
                
            }
        }
        
        
        if screen == "PromotionView"{
            do{
                if let data = json as? Data{
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject>{
                        println_debug(dic)
                        if dic["Code"] as! Int == 200{
                            DispatchQueue.main.async{
                                
                                self.lblastUpdated.textAlignment = .center
                            //    self.lblastUpdated.lineBreakMode = .byWordWrapping
                            //    self.lblastUpdated.numberOfLines = 4
                                self.lblastUpdated.text = "Last Updated:".localized.appending(UserDefaults.standard.value(forKey: "dateAndTime") as! String)
                                
                                
                                guard let msg = dic["Data"] as? String else {
                                    return
                                }
                                if let promoDetail =  OKBaseController.convertToDictionary(text: msg ) {
                                    
                                    println_debug(promoDetail)
                                    println_debug("Promotion det \(String(describing: promoDetail["PromotionId"]))")
                                    
                                    guard let arrDict = promoDetail["PromotionDetails"] as? Array<Dictionary<String,Any>> else {
                                        return
                                    }
                                    
                                    for element in arrDict {
                                        println_debug("Mohit Promotion Details here:- \(element)")
                                        self.updateDataInOutlet(object: element)
                                        break
                                    }
                                }
                                self.removeProgressView()
                                
                            }
                        }
                    }
                }
            } catch{
            }
        }
    }
    
    func addPromotionApiParsing(data: AnyObject) {
        
        let responseString = String(data: data as! Data, encoding: .utf8)
        let xml            = SWXMLHash.parse(responseString!)
        println_debug(xml)
        self.enumerate(indexer: xml)
        let Dvalue = self.responseDict["string"]
        let tempData = OKBaseController.convertToDictionary(text: Dvalue as? String ?? "")
        let dict = tempData!["status"] as AnyObject
        if Int(dict["code"] as? String ?? "") == 200{
            
            DispatchQueue.main.async {
                
                if dict["msg"] as? String ?? "" == "success" || dict["msg"] as? String ?? "" == "Records Not Found" || dict["msg"] as? String ?? "" == "No Data found" {
                    self.getPromotionIdApiCall(title: self.tfPromotionTitle.text!, description: self.tfDescription.text!, contactName: self.tfContactPerson.text!, mobileNo: self.tfMobileNo.text!, Email: UserModel.shared.mobileNo, website: self.tfCompanyWebsite.text!, fbpage: self.tfacebookPage.text!)
                    self.removeProgressView()
                    
                    
                }
            }
        }
    }
}


extension SMAddPromotionViewController: BioMetricLoginDelegate {
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if isSuccessful {
            
            println_debug("Authorised")
            
        }
        else{
            
            if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
        }
    }
}


