//
//  paymentSuccessFullViewController.swift
//  OK
//
//  Created by Mohit on 10/30/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

fileprivate enum TabBarItem : Int {
    case favorite = 900
    case contact
    case home
    case more
}


class SMPaymentSuccessFullViewController: OKBaseController, UITabBarDelegate {
    
    var dataDict = Dictionary<String, Any>()
    var qrImageObject :  PTQRGenerator?
    let notificationCenter = NotificationCenter.default
    
    @IBOutlet var transId: UILabel! {
        didSet {
            transId.font = UIFont(name: appFont, size: appFontSize)
            transId.text = transId.text?.localized
        }
    }
    
    @IBOutlet var tranId: UILabel! {
        didSet {
            tranId.font = UIFont(name: appFont, size: appFontSize)
            tranId.text = tranId.text?.localized
        }
    }
    
    @IBOutlet var transType: UILabel! {
        didSet {
            transType.font = UIFont(name: appFont, size: appFontSize)
            transType.text = transType.text?.localized
        }
    }
    
    @IBOutlet var tranType: UILabel! {
        didSet {
            tranType.font = UIFont(name: appFont, size: appFontSize)
            tranType.text = tranType.text?.localized
        }
    }
    
    @IBOutlet var bal: UILabel! {
        didSet {
            bal.font = UIFont(name: appFont, size: appFontSize)
            bal.text = bal.text?.localized
        }
    }
    
    @IBOutlet weak var lblName: UILabel! {
        didSet {
            lblName.font = UIFont(name: appFont, size: appFontSize)
            lblName.text = lblName.text?.localized
        }
    }
    @IBOutlet weak var lblNumber: UILabel! {
        didSet {
            lblNumber.font = UIFont(name: appFont, size: appFontSize)
            lblNumber.text = lblNumber.text?.localized
        }
    }
    @IBOutlet weak var lblAmount: UILabel! {
        didSet {
            lblAmount.font = UIFont(name: appFont, size: appFontSize)
            lblAmount.text = lblAmount.text?.localized
        }
    }
    @IBOutlet weak var lblDate: UILabel!{
        didSet
        {
            lblDate.font = UIFont(name: appFont, size: appFontSize)
            lblDate.text = lblDate.text?.localized
        }
    }
    @IBOutlet weak var lblTime: UILabel! {
        didSet {
            lblTime.font = UIFont(name: appFont, size: appFontSize)
            lblTime.text = lblTime.text?.localized
        }
    }
    @IBOutlet weak var lblBalance: UILabel! {
        didSet {
            lblBalance.font = UIFont(name: appFont, size: appFontSize)
            lblBalance.text = lblBalance.text?.localized
        }
    }
    @IBOutlet weak var lblTransactionId: UILabel! {
        didSet {
            lblTransactionId.font = UIFont(name: appFont, size: appFontSize)
            lblTransactionId.text = lblTransactionId.text?.localized
        }
    }
    @IBOutlet weak var lbltransactionType: UILabel!{
        didSet
        {
            lbltransactionType.font = UIFont(name: appFont, size: appFontSize)
            lbltransactionType.text = lbltransactionType.text?.localized
        }
    }
    @IBOutlet weak var lblCategory: UILabel! {
        didSet {
            lblCategory.font = UIFont(name: appFont, size: appFontSize)
            lblCategory.text = lblCategory.text?.localized
        }
    }
    @IBOutlet weak var childView: UIView!
    @IBOutlet weak var transactionView: UIView!
    @IBOutlet weak var pdfView: UIView!
    
    @IBOutlet weak var lblamount: UILabel! {
        didSet {
            lblamount.font = UIFont(name: appFont, size: appFontSize)
            lblamount.text = lblamount.text?.localized
        }
    }
    @IBOutlet weak var lbldate: UILabel! {
        didSet {
            lbldate.font = UIFont(name: appFont, size: appFontSize)
            lbldate.text = lbldate.text?.localized
        }
    }
    @IBOutlet weak var lbltime: UILabel! {
        didSet {
            lbltime.font = UIFont(name: appFont, size: appFontSize)
            lbltime.text = lbltime.text?.localized
        }
    }
    @IBOutlet weak var lbltransactionid: UILabel! {
        didSet {
            lbltransactionid.font = UIFont(name: appFont, size: appFontSize)
            lbltransactionid.text = lbltransactionid.text?.localized
        }
    }
    @IBOutlet weak var lbltransactiontype: UILabel!{
        didSet
        {
            lbltransactiontype.font = UIFont(name: appFont, size: appFontSize)
            lbltransactiontype.text = lbltransactiontype.text?.localized
        }
    }
    @IBOutlet weak var lblmobileno: UILabel! {
        didSet {
            lblmobileno.font = UIFont(name: appFont, size: appFontSize)
            lblmobileno.text = lblmobileno.text?.localized
        }
    }
    @IBOutlet weak var lblcategoary: UILabel! {
        didSet {
            lblcategoary.font = UIFont(name: appFont, size: appFontSize)
            lblcategoary.text = lblcategoary.text?.localized
        }
    }
    @IBOutlet weak var lblname: UILabel! {
        didSet {
            lblname.font = UIFont(name: appFont, size: appFontSize)
            lblname.text = lblname.text?.localized
        }
    }
    
    @IBOutlet var btnRepeatPay: UIButton! {
        didSet {
            btnRepeatPay.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnRepeatPay.setTitle(btnRepeatPay.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet var btnInvoice: UIButton! {
        didSet {
            btnInvoice.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnInvoice.setTitle(btnInvoice.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var btnShare: UIButton! {
        didSet {
            btnShare.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnShare.setTitle(btnShare.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var imgScanQR: UIImageView!
    @IBOutlet var rTabBar: UITabBar!
    @IBOutlet var fav: UITabBarItem!
    @IBOutlet var addContact: UITabBarItem!
    @IBOutlet var home: UITabBarItem!
    @IBOutlet var more: UITabBarItem!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let color = UIColor.init(hex: "162D9F")
        let attrFont = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.0) ?? UIFont.systemFont(ofSize: 11.0), NSAttributedString.Key.foregroundColor: color]
        
        self.home.setTitleTextAttributes(attrFont, for: .normal)
        self.more.setTitleTextAttributes(attrFont, for: .normal)
        
        self.home.title = "HomePagePT".localized
        self.more.title = "More".localized
        
        UITabBar.appearance().tintColor = UIColor.gray
        UITabBar.appearance().unselectedItemTintColor = UIColor.gray
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        
        self.title = "Receipt".localized
        println_debug(dataDict)
        self.initValue()
        self.signOutAction()
        
        self.generateQRCodeAction(self.getQRCodeImage()!)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .normal)
        UITabBar.appearance().unselectedItemTintColor = .lightGray
    }
    
    fileprivate func signOutAction() {
        UserDefaults.standard.set(true, forKey: "isLogOut")
        UserLogin.shared.loginSessionExpired = true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Tabbar Delegate
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch(item) {
        case home : self.btnHomeClick(item)
            break
        case more : self.btnMoreClick(item)
            break
        default:
            break
        }
    }
    
    
    
    func generateQRCodeAction(_ str: String) {
        qrImageObject = PTQRGenerator()
        guard qrImageObject != nil , let qrImage = qrImageObject else {
            return
        }
        
        guard let image =  qrImage.getQRImage(stringQR: str, withSize: 10) else {
            return
        }
        self.imgScanQR.image = image
    }
    
    
    
    func getQRCodeImage() -> String? {
        
        let phNumber = UserModel.shared.mobileNo
        var transDateStr = ""
        let dFormatter = DateFormatter()
        
        dFormatter.calendar = Calendar(identifier: .gregorian)
        dFormatter.dateFormat = "dd/MMM/yyyy HH:mm:ss"
        dFormatter.setLocale()
        if let trDate = dataDict["requestcts"] {
            transDateStr = dFormatter.string(from: dFormatter.date(from: trDate as! String)!)
        }
        let senderName = UserModel.shared.name
        let senderNumber = (dataDict["destination"] as? String)!
        let receiverName = UserModel.shared.name
        let receiverBusinessName = UserModel.shared.businessName
        let receiverNumber = (dataDict["destination"] as? String)!
        let firstPartToEncrypt = "\(transDateStr)----\(phNumber)"
        let transID = (dataDict["transid"] as? String)!
        let trasnstype = (dataDict["responsetype"] as? String)!
        var gender = ""
        if UserModel.shared.gender == "1"{gender = "M"}
        else{gender = "F"}
        let age = UserModel.shared.ageNow
        let lattitude = UserModel.shared.lat
        let longitude = UserModel.shared.long
        let firstPartEncrypted = AESCrypt.encrypt(firstPartToEncrypt, password: "m2n1shlko@$p##d")
        let balance = (dataDict["walletbalance"] as? String)!
        let cashBackAmount = "0"
        let amount = (dataDict["amount"] as? String)!
        let bonus = "0"
        let state = UserModel.shared.state
        let str2 = "OK-\(senderName)-\(amount)-\(transDateStr)-\(transID)-\(senderNumber)-\(balance)-des-\(receiverNumber)-\(trasnstype)-\(cashBackAmount)-\(bonus)-\(lattitude),\(longitude)-0-\(gender)-\(age)-\(state)-\(receiverName)-\(receiverBusinessName)"
        let secondEncrypted = AESCrypt.encrypt(str2, password: firstPartToEncrypt)
        var firstPartEncryptedSafeValue = ""
        var secondEncryptedSafeValue = ""
        if let firstEncrypted = firstPartEncrypted {
            firstPartEncryptedSafeValue = firstEncrypted
        }
        if let secondEncrypt = secondEncrypted {
            secondEncryptedSafeValue = secondEncrypt
        }
        let finalEncyptFormation = "\(firstPartEncryptedSafeValue)---\(secondEncryptedSafeValue)"
        return finalEncyptFormation
        
    }
    
}

extension SMPaymentSuccessFullViewController{
    
    func initValue(){
        
        self.lblName.text = dataDict["agentname"] as? String
        var chnageNo = dataDict["destination"] as? String ?? ""
        chnageNo.remove(at: chnageNo.index(chnageNo.startIndex, offsetBy: 1))
        chnageNo.remove(at: chnageNo.index(chnageNo.startIndex, offsetBy: 1))
        chnageNo.remove(at: chnageNo.index(chnageNo.startIndex, offsetBy: 1))
        self.lblNumber.text = chnageNo.localized
        self.lblCategory.text = "CashBack".localized
        self.lbltransactionType.text = "CASHBACK".localized
        self.lblAmount.text = self.getDigitDisplay((dataDict["amount"] as? String)!)
        let dateTime = (dataDict["responsects"] as! String ?? "").components(separatedBy: " ")
        self.lblDate.text = dateTime[0].localized
        self.lblTime.text = dateTime[1].localized
        self.lblBalance.text = self.getDigitDisplay((dataDict["walletbalance"] as? String)!).localized
        self.lblTransactionId.text = (dataDict["transid"] as? String)?.localized
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissView))
        tap.numberOfTapsRequired = 1
        self.childView.addGestureRecognizer(tap)
        self.childView.isHidden = true
        self.lblname.text = (dataDict["agentname"] as? String)?.localized
        var chnageNo1 = dataDict["destination"] as? String ?? ""
        chnageNo1.remove(at: chnageNo1.index(chnageNo1.startIndex, offsetBy: 1))
        chnageNo1.remove(at: chnageNo1.index(chnageNo1.startIndex, offsetBy: 1))
        chnageNo1.remove(at: chnageNo1.index(chnageNo1.startIndex, offsetBy: 1))
        
        self.lblmobileno.text = chnageNo1.localized
        self.lbltransactiontype.text = "CASHBACK".localized
        self.lblamount.text = self.getDigitDisplay((dataDict["amount"] as? String)!)
        let dateTime1 = (dataDict["responsects"] as! String ?? "").components(separatedBy: " ")
        self.lbldate.text = dateTime1[0].localized
        self.lbltime.text = dateTime1[1].localized
        self.lbltransactionid.text = (dataDict["transid"] as? String)?.localized
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.dismissView))
        tap1.numberOfTapsRequired = 1
        self.childView.addGestureRecognizer(tap1)
    }
    
    
    
    func captureScreen() -> UIImage? {
        let bounds = pdfView.bounds
        UIGraphicsBeginImageContextWithOptions(bounds.size, true, 0.0)
        pdfView.drawHierarchy(in: bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notificationCenter.addObserver(self,selector: #selector(SessionAlert),name: .appTimeout,object: nil)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notificationCenter.removeObserver(self, name: .appTimeout, object: nil)
    }
    
    @objc func SessionAlert() {
        
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "", body: "Application Session expired login again".localized, img:  #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){
                if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)
                }
            }
            alertViewObj.showAlert(controller: self)
        }
        
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissView() {
        self.childView.isHidden = true
    }
    
    @IBAction func btnShareClick(_ sender: UIButton){
        self.childView.isHidden = true
        self.transactionView.isHidden = true
        self.pdfView.isHidden = false
        let activityItem = self.captureScreen()
        "".share(activityItem: (activityItem)!)
    }
    
    
    @IBAction func btnMorePaymentClick(_ sender: UIButton){
        
    }
    
    
    @IBAction func btnMoreClick(_ sender: UITabBarItem){
        self.transactionView.isHidden = false
        self.childView.isHidden = false
    }
    
    
    @IBAction func btnHomeClick(_ sender: UITabBarItem) {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: .selectHome, object: nil)
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    
    
    @IBAction func btnInvoiceClick(_ sender: UIButton){
        self.childView.isHidden = false
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "SMInvoiceReceipt") as! SMInvoiceReceipt
        VC.dataDict = dataDict
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    
    
    @IBAction func btnRepayment(_ sender: UIButton){
        UserDefaults.standard.set(true, forKey: "TextField_Clear")
        self.navigationController?.popToViewController((self.navigationController?.viewControllers[2])!, animated: true)        
    }
    
    
    @IBAction func btnAddFavoriteClick(_ sender: UITabBarItem){
        alertViewObj.addTextField(title: "", body: "", img: #imageLiteral(resourceName: "n_facebook"))
        alertViewObj.addAction(title: "Skip".localized, style: .cancel) {
        }
        
        alertViewObj.addAction(title: "Save".localized, style: .target) {
            self.dismiss(animated: true, completion: nil)
        }
        alertViewObj.showAlert(controller: self)
    }
    
    
    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddContactClick(_ sender: UITabBarItem) {
        showNewContactViewController()
    }
    
    
    func createPDFFromView(aView: UIView, fileName: String) {
        
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, aView.bounds, nil)
        UIGraphicsBeginPDFPage()
        
        guard let pdfContext = UIGraphicsGetCurrentContext() else { return }
        
        aView.layer.render(in: pdfContext)
        UIGraphicsEndPDFContext()
        
        if let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
            let documentsFileName = documentDirectories + "/" + fileName + ".pdf"
            pdfData.write(toFile: documentsFileName, atomically: true)
        }
    }
    
    
    func sharePdfFile(documentoPath: String){
        let documento = NSData(contentsOfFile: documentoPath)
        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [documento!], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView=self.view
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    
    
    @IBAction func morePayment(_ sender: UIButton){
        
    }
    
}

extension SMPaymentSuccessFullViewController: BioMetricLoginDelegate {
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful {
            println_debug("Authorised")
        }else {
            if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
        }
    }
}

extension SMPaymentSuccessFullViewController: CNContactViewControllerDelegate {
    
    func showNewContactViewController() {
        let contactViewController: CNContactViewController = CNContactViewController(forNewContact: nil)
        contactViewController.contactStore = CNContactStore()
        contactViewController.delegate = self
        let navigationController: UINavigationController = UINavigationController(rootViewController: contactViewController)
        present(navigationController, animated: false){
            println_debug("Present")
        }
        
    }
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?){
        self.dismiss(animated: true, completion: nil)
    }
    
    func contactViewController(_ viewController: CNContactViewController, shouldPerformDefaultActionFor property: CNContactProperty) -> Bool{
        return true
    }
}

extension SMPaymentSuccessFullViewController :  UIImagePickerControllerDelegate  {
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            alertViewObj.wrapAlert(title: "Error".localized, body: error.localizedDescription, img: #imageLiteral(resourceName: "alert-icon") )
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {})
            alertViewObj.showAlert(controller: self)
            
        }
    }
}


