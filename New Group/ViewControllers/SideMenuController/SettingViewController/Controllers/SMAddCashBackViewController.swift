//
//  AddCashBackViewController.swift
//  SettingVC
//
//  Created by Mohit on 10/19/17.
//  Copyright © 2017 Mohit. All rights reserved.
//

import UIKit


class SMAddCashBackViewController: OKBaseController,DossyTextLabelDelegate {
    
    @IBOutlet weak var f_radio: UIButton!
    @IBOutlet weak var p_radio: UIButton!
    @IBOutlet weak var r_radio: UIButton!
    @IBOutlet var fixed: UILabel!{
        didSet
        {
            fixed.font = UIFont(name: appFont, size: appFontSize)
            fixed.text = fixed.text?.localized
        }
    }
    @IBOutlet var percent: UILabel!{
        didSet
        {
            percent.font = UIFont(name: appFont, size: appFontSize)
            percent.text = percent.text?.localized
        }
    }
    @IBOutlet var ratio: UILabel!{
        didSet
        {
            ratio.font = UIFont(name: appFont, size: appFontSize)
            ratio.text = ratio.text?.localized
        }
    }
    
    @IBOutlet weak var tfMinAmount: UITextField!{
        didSet
        {
            tfMinAmount.font = UIFont(name: appFont, size: 17.0)
            
            tfMinAmount.placeholder = tfMinAmount.placeholder?.localized
        }
    }
    @IBOutlet weak var tfMaxAmount: UITextField!{
        didSet
        {
            tfMaxAmount.font = UIFont(name: appFont, size: 17.0)
            
            tfMaxAmount.placeholder = tfMaxAmount.placeholder?.localized
        }
    }
    @IBOutlet weak var tfCashBack: UITextField!{
        didSet
        {
            tfCashBack.font = UIFont(name: appFont, size: 17.0)
            tfCashBack.placeholder = tfCashBack.placeholder?.localized
        }
    }
    @IBOutlet weak var tfPerAmount: UITextField!{
        didSet
        {
            tfPerAmount.font = UIFont(name: appFont, size: 17.0)
            tfPerAmount.placeholder = tfPerAmount.placeholder?.localized
        }
    }
    @IBOutlet weak var lblCalcType: UILabel!{
        didSet
        {
            lblCalcType.font = UIFont(name: appFont, size: appFontSize)
            lblCalcType.text = lblCalcType.text?.localized
        }
    }
    @IBOutlet weak var minAmtView: UIView!
    @IBOutlet weak var radioView: UIView!
    @IBOutlet weak var selectCalcView: UIView!
    @IBOutlet weak var cashBackView: UIView!
    @IBOutlet weak var maxAmtView: UIView!
    @IBOutlet weak var perAmtView: UIView!
    @IBOutlet weak var lblAnimated: DossyTextLabel!
    @IBOutlet weak var btnSubmit: UIButton!{
        didSet{
            self.btnSubmit.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.btnSubmit.setTitle(self.btnSubmit.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var bottomBtnConstraint: NSLayoutConstraint!
    @IBOutlet weak var topCashBackConstraint: NSLayoutConstraint!
    @IBOutlet weak var mainViewHeightConstraint: NSLayoutConstraint!
    
    let notificationCenter = NotificationCenter.default
    var selectedRadioValue = String()
    var addCashBackViewDict = Dictionary<String, Any>()
    
    var textimer: Timer!
    var totalCount = 0
    var loopIndex = 0
    var charArray = [Character]()
    var fixedStr = "Example of Fixed type if you set range(Min and Max) of amount between 2000 to 5000. Your valuable customer will get cashback fixed amount whatever you entered.".localized
    var ratioStr = "Example of Ratio type if you set range(Min and Max) of amount between 2000 to 5000 and you define per 100 cashback is 1. Your valuable customer pay 3000 then he will get 3000/100=30 then 30*1=30 Cashback.".localized
    var percentStr = "Example of Percentage type if you set range(Min and Max) of amount between 2000 to 5000 and you define 2 % if cashback. Your valuable customer pay 3000 then he will get 3000*0.02=60 Cashback.".localized
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Add CashBack".localized
        UserDefaults.standard.set(true, forKey: "loopBreak")
        UserDefaults.standard.synchronize()
        // UIFrameManage()
        self.lblAnimated.font = UIFont(name: appFont, size: 20)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        
        self.btnSubmit.isHidden = false
        self.tfMinAmount.addTarget(self, action: #selector(SMAddCashBackViewController.textFieldDidChange(_:)),
                                   for: UIControl.Event.editingChanged)
        self.tfMaxAmount.addTarget(self, action: #selector(SMAddCashBackViewController.textFieldDidChange(_:)),
                                   for: UIControl.Event.editingChanged)
      
        self.tfCashBack.addTarget(self, action: #selector(SMAddCashBackViewController.textFieldDidChange(_:)),
                                  for: UIControl.Event.editingChanged)
        
        self.tfPerAmount.addTarget(self, action: #selector(SMAddCashBackViewController.textFieldDidChange(_:)),
                                   for: UIControl.Event.editingChanged)
        self.subscribeToShowKeyboardNotifications()
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notificationCenter.addObserver(self,selector: #selector(SessionAlert), name: .appTimeout, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notificationCenter.removeObserver(self, name: .appTimeout, object: nil)
    }
    
    @objc func SessionAlert()  {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "", body: "Application Session expired login again".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){
                if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self as BioMetricLoginDelegate)}
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        UIView.animate(withDuration: 1, animations: { () -> Void in
            print(keyboardHeight)
            self.bottomBtnConstraint.constant = keyboardHeight - 33
        })
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        bottomBtnConstraint.constant = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func textFieldDidChange(_ textField: UITextField){
        
        if r_radio.isSelected == true
        {
            if self.tfMinAmount.text == "" || self.tfMaxAmount.text == "" || self.tfCashBack.text == "" || self.tfPerAmount.text == ""{
                self.btnSubmit.isHidden = true
            }
            else{
                self.btnSubmit.isHidden = false
            }
            
        } else if f_radio.isSelected == true || f_radio.isSelected == true {
            if self.tfMinAmount.text == "" || self.tfMaxAmount.text == "" || self.tfCashBack.text == ""{
                self.btnSubmit.isHidden = true
            }
            else{
                self.btnSubmit.isHidden = false
            }
        }
        else{
            if self.tfMinAmount.text == "" || self.tfMaxAmount.text == "" || self.tfCashBack.text == ""{
                self.btnSubmit.isHidden = true
            }
            else{
                self.btnSubmit.isHidden = false
            }
        }
        
      
        
        if let text = textField.text, text.count > 1 {
            
            if !text.contains("."){
                let formatter = NumberFormatter()
                formatter.numberStyle = NumberFormatter.Style.decimal
                let textAfterRemovingComma = text.replacingOccurrences(of: ",", with: "")
                let formattedNum = formatter.string(from: NSNumber(value: Int(textAfterRemovingComma)!))
                textField.text = formattedNum!
                if textAfterRemovingComma.length >= 9  && textAfterRemovingComma.count >= 9{
                    textField.text = String((textField.text?.dropLast())!)
                }
            }
            
            
        }
    }
    @IBAction func imageStatusChange(_ sender: UIButton){
        if sender == r_radio{
            if sender.isSelected{
                r_radio.setImage(UIImage(named: "select_radio"), for: .selected)
                r_radio.isSelected = false
                r_radio.isEnabled = true
                f_radio.isSelected = true
                p_radio.isSelected = true
            }
            else{
                
                r_radio.setImage(UIImage(named: "r_Unradio"), for: .normal)
                r_radio.isSelected = true
                r_radio.isEnabled = false
                f_radio.isSelected = false
                p_radio.isSelected = false
            }
        }
        if sender == f_radio{
            if sender.isSelected{
                f_radio.setImage(UIImage(named: "select_radio"), for: .selected)
                f_radio.isSelected = false
                f_radio.isEnabled = true
                r_radio.isSelected = true
                p_radio.isSelected = true
            }
            else{
                f_radio.setImage(UIImage(named: "r_Unradio"), for: .normal)
                f_radio.isSelected = true
                f_radio.isEnabled = false
                r_radio.isSelected = false
                p_radio.isSelected = false
            }
        }
        if sender == p_radio{
            if sender.isSelected{
                p_radio.setImage(UIImage(named: "select_radio"), for: .selected)
                p_radio.isSelected = false
                p_radio.isEnabled = false
                r_radio.isSelected = true
                f_radio.isSelected = true
            }
            else{
                p_radio.setImage(UIImage(named: "r_Unradio"), for: .normal)
                p_radio.isSelected = true
                p_radio.isEnabled = true
                r_radio.isSelected = false
                f_radio.isSelected = false
            }
        }
    }
    
    
    
    deinit {
        if let _ = textimer {
            textimer.invalidate()
        }
        totalCount = 0
        loopIndex = 0
    }
}

extension SMAddCashBackViewController : UITextFieldDelegate{
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text: String? = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        let char = string.cString(using: String.Encoding.utf8)!
        
        if textField == tfCashBack{
            let value = tfCashBack.text
            if  let  data = value{
                if string == "." && data.contains("."){
                    return false
                }
            }
        }
        
    
        let isBackSpace = strcmp(char, "\\b")
        //        if (isBackSpace == -92){
        //            return true
        //        }
        if text == "0"{
            return true
        }
        else if (text?.length)! > 10{
            return false
        }
        else if textField == self.tfCashBack{
            if let textfieldInt = self.tfMaxAmount?.text {
                let textfield2Int = self.tfCashBack.text ?? "" + string
                let maxAmount = textfieldInt.replacingOccurrences(of: ",", with: "")
                let minAmount = textfield2Int.replacingOccurrences(of: ",", with: "")
                
                if string == "."{
                    
                    
                }else{
                    if maxAmount != "" && minAmount != "" {
                        if Double(maxAmount) ?? 0.0 <= Double(minAmount) ?? 0.0 {
                            DispatchQueue.main.async {
                                alertViewObj.wrapAlert(title: "", body: "Maximum Amount must be greater than Cashback".localized, img: #imageLiteral(resourceName: "alert-icon"))
                                alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {
                                    self.tfCashBack.text = ""
                                })
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                        else if self.tfMaxAmount.text == "" {
                            self.customAlert(title: "", msg: "Please enter minimum and maximum amount First.".localized)
                        }
                        
                    }
                    
                }
                
               
            }
        }
        else if textField == self.tfMaxAmount{
            println_debug(self.tfMaxAmount.text)
            if (isBackSpace == -92){
                self.tfCashBack.text = ""
            }
        }
        
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tfCashBack{
            let text = textField.text
            if text?.last == "."{
                let value = textField.text
                textField.text =  ""
                textField.text =  value ?? "" + "0"
            }
        }
    }
}

extension SMAddCashBackViewController {
    
    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        UserDefaults.standard.set(false, forKey: "loopBreak")
        UserDefaults.standard.synchronize()
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func submitClick(_ sender: UIButton){
        
        if (self.tfMinAmount.text != "")  && (self.tfMaxAmount.text != "") && (self.tfCashBack.text != "") && self.selectedRadioValue != ""{
            let min : Float = NSString(string: self.tfMinAmount.text!.replacingOccurrences(of: ",", with: "")).floatValue
            let max : Float = NSString(string: self.tfMaxAmount.text!.replacingOccurrences(of: ",", with: "")).floatValue
            if min < max{
                let secureToken = UserLogin.shared.token
                self.AddCashBackApiCall(mobileNo: UserModel.shared.mobileNo, password: ok_password!, secureToken: secureToken)
            }
            else{
                self.customAlert(title: "", msg: "Maximum amount must be greater than mimimum".localized)
            }
        }
        else{
            self.customAlert(title: "", msg: "Please Select Calculation Type".localized)
        }
    }
    
    func customAlert(title:String,msg:String){
        
        alertViewObj.wrapAlert(title: title, body: msg, img: #imageLiteral(resourceName: "alert-icon"))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {
        })
        alertViewObj.showAlert(controller: self)
    }
    
    fileprivate func radioButtonSelected(){
        if r_radio.isSelected == true
        {
            if tfMinAmount.text == "" || tfMaxAmount.text == "" || tfCashBack.text == "" || tfPerAmount.text == ""{
                self.btnSubmit.isHidden = true
            }
            else{
                self.btnSubmit.isHidden = false
            }
            
        } else{
            if tfMinAmount.text == "" || tfMaxAmount.text == "" || tfCashBack.text == ""{
                self.btnSubmit.isHidden = true
            }
            else {
                self.btnSubmit.isHidden = false
            }
        }
    }
    
    
    @IBAction func fixedClick(_ sender: UIButton){
        
        f_radio.isSelected = true
        r_radio.isSelected = false
        p_radio.isSelected = false
        radioButtonSelected()
        selectedRadioValue = "Fixed"
        topCashBackConstraint.constant = 2
        
        if let _ = textimer {
            textimer.invalidate()
        }
        showFixedString()
    }
    
    func showFixedString() {
        lblAnimated.text = ""
        charArray = Array(fixedStr)
        totalCount = charArray.count
        loopIndex = 0
        self.textimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.showNextAnimString), userInfo: nil, repeats: true)
    }
    
    @objc func showNextAnimString() {
        var previousStr = lblAnimated.text ?? ""
        previousStr += "\(charArray[loopIndex])"
        lblAnimated.text = previousStr
        mainViewHeightConstraint.constant = lblAnimated.frame.maxY + 20
        self.view.layoutIfNeeded()
        if loopIndex == totalCount - 1 {
            loopIndex = 0
            totalCount = 0
            charArray = []
            textimer.invalidate()
        }
        loopIndex += 1
    }
    
    @IBAction func radioClick(_ sender: UIButton){
        self.view.endEditing(true)
        r_radio.isSelected = true
        f_radio.isSelected = false
        p_radio.isSelected = false
        radioButtonSelected()
        selectedRadioValue = "Ration"
        self.perAmtView.isHidden = false
        topCashBackConstraint.constant = 59
        self.updatedImage(f_radio: f_radio, r_radio: r_radio, p_radio: p_radio)
        if let _ = textimer {
            textimer.invalidate()
        }
        showRatioString()
    }
    
    func showRatioString() {
        lblAnimated.text = ""
        charArray = Array(ratioStr)
        totalCount = charArray.count
        loopIndex = 0
        self.textimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.showNextAnimString), userInfo: nil, repeats: true)
    }
    
    func updatedImage(f_radio: UIButton,r_radio: UIButton,p_radio: UIButton){
        
        f_radio.setImage(UIImage(named: "r_Unradio"), for: .normal)
        r_radio.setImage(UIImage(named: "r_Unradio"), for: .normal)
        p_radio.setImage(UIImage(named: "r_Unradio"), for: .normal)
        f_radio.setImage(UIImage(named: "select_radio"), for: .disabled)
        r_radio.setImage(UIImage(named: "select_radio"), for: .disabled)
        p_radio.setImage(UIImage(named: "select_radio"), for: .disabled)
        f_radio.setImage(UIImage(named: "select_radio"), for: .highlighted)
        r_radio.setImage(UIImage(named: "select_radio"), for: .highlighted)
        p_radio.setImage(UIImage(named: "select_radio"), for: .highlighted)
        f_radio.setImage(UIImage(named: "select_radio"), for: .selected)
        r_radio.setImage(UIImage(named: "select_radio"), for: .selected)
        p_radio.setImage(UIImage(named: "select_radio"), for: .selected)
        
    }
    
    @IBAction func percentageClick(_ sender: UIButton){
        p_radio.isSelected = true
        f_radio.isSelected = false
        r_radio.isSelected = false
        radioButtonSelected()
        selectedRadioValue = "Percentage"
        topCashBackConstraint.constant = 2
        self.updatedImage(f_radio: f_radio, r_radio: r_radio, p_radio: p_radio)
        if let _ = textimer {
            textimer.invalidate()
        }
        showPercentString()
    }
    
    func showPercentString() {
        lblAnimated.text = ""
        charArray = Array(percentStr)
        totalCount = charArray.count
        loopIndex = 0
        self.textimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.showNextAnimString), userInfo: nil, repeats: true)
    }
    
    func UIFrameManage(){
        
        self.view.endEditing(true)
        self.perAmtView.isHidden = true
        self.cashBackView.frame = CGRect(x: 0.0, y: 124.0, width: self.view.frame.size.width, height: 55.0)
        self.selectCalcView.frame = CGRect(x: 0.0, y: 181.0, width: self.view.frame.size.width, height: 50.0)
        self.radioView.frame = CGRect(x: 0.0, y: 233.0, width: self.view.frame.size.width, height: 55.0)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    func AddCashBackApiCall(mobileNo: String, password: String, secureToken: String){
        
        if appDelegate.checkNetworkAvail(){
            
            showProgressView()
            let web = WebApiClass()
            web.delegate = self
            var urlS = String()
            let wifiAddress = appDelegate.getWiFiAddress() ?? ""
            
            let minAmount = self.tfMinAmount.text?.replacingOccurrences(of: ",", with: "") ?? "0"
            let maxAmount = self.tfMaxAmount.text?.replacingOccurrences(of: ",", with: "") ?? "0"
            let perAmount = self.tfPerAmount.text?.replacingOccurrences(of: ",", with: "") ?? "0"
            let cashBackAmount = self.tfCashBack.text?.replacingOccurrences(of: ",", with: "") ?? "0"
            
            if selectedRadioValue ==  "Fixed" || selectedRadioValue ==  "Percentage"{
                
                urlS = String.init(format: Url.addCashBack,cashBackAmount,mobileNo, wifiAddress ,password,selectedRadioValue,secureToken,minAmount,maxAmount)
            }
            else{
                
                
                urlS = String.init(format: Url.Add_Kickback_Ratio_api,cashBackAmount,mobileNo,perAmount,wifiAddress ,password,secureToken,minAmount,maxAmount)
            }
            
            let url =   getUrl(urlStr: urlS, serverType: .serverEstel)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "POST", mScreen: "addCashBack")
            
        }
        else{
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
        }
        
    }
    
    func enumerate(indexer: XMLIndexer){
        for child in indexer.children{
            self.addCashBackViewDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
}

extension SMAddCashBackViewController : WebServiceResponseDelegate {
    
    func webResponse(withJson json: AnyObject, screen: String) {
        self.AddCashBackParsing(data: json)
    }
    
    func AddCashBackParsing(data: AnyObject){
        
        let responseString = String(data: data as! Data, encoding: .utf8)
        let xml            = SWXMLHash.parse(responseString!)
        println_debug(xml)
        
        self.enumerate(indexer: xml)
        
        println_debug(self.addCashBackViewDict)
        
        if self.addCashBackViewDict.keys.contains("resultdescription"){
            if self.addCashBackViewDict["resultdescription"] as? String ?? "" == "Records Not Found" || self.addCashBackViewDict["resultdescription"] as? String ?? "" == "Transaction Successful" || self.addCashBackViewDict["resultdescription"] as? String ?? "" == "Invalid Secure Token" || self.addCashBackViewDict["resultdescription"] as? String ?? "" == "Secure Token Expire" || self.addCashBackViewDict["resultdescription"] as? String ?? "" == "Invalid Request (Schema Validation Failed)" || self.addCashBackViewDict["resultdescription"] as? String ?? "" == "Kick Back Slab OverLap " || self.addCashBackViewDict["resultdescription"] as? String ?? "" == "Invalid Amount"{
                
                DispatchQueue.main.async{
                    
                    self.removeProgressView()
                    if self.addCashBackViewDict["resultdescription"] as? String ?? "" == "Transaction Successful"{
                        
                        alertViewObj.wrapAlert(title: "", body: "Successfully Added".localized, img: #imageLiteral(resourceName: "new_cashback"))
                        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {
                            self.navigationController?.popViewController(animated: true)
                        })
                        alertViewObj.showAlert(controller: self)
                        
                    }
                    else{
                        let msg = self.addCashBackViewDict["resultdescription"] as? String ?? ""
                        self.customAlert(title: "", msg: msg.localized)
                        if self.addCashBackViewDict["resultdescription"] as? String ?? "" == "Transaction Successful"{
                            
                            alertViewObj.wrapAlert(title: "", body: "Successfully Added".localized, img: #imageLiteral(resourceName: "new_cashback"))
                            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {
                                self.navigationController?.popViewController(animated: true)
                            })
                            alertViewObj.showAlert(controller: self)
                            
                        }
                        else{
                            
                            let msg = self.addCashBackViewDict["resultdescription"] as? String ?? ""
                            self.customAlert(title: "", msg: msg)
                        }
                    }
                }
            }
        }
    }
}

extension SMAddCashBackViewController: BioMetricLoginDelegate {
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if isSuccessful {
            println_debug("Authorised")
        }
        else{
            
            if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
        }
    }
}
