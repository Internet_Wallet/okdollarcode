//
//  CashBackViewController.swift
//  SettingVC
//
//  Created by Mohit on 10/19/17.
//  Copyright © 2017 Mohit. All rights reserved.
//

import UIKit

struct Recordata: XMLIndexerDeserializable{
    var minrange = ""
    var maxrange = ""
    var disbtype = ""
    var peramt = ""
    var amount = ""
    var status = ""
    static func deserialize(_ node: XMLIndexer) throws -> Recordata {
        return try Recordata(minrange: node["minrange"].value(), maxrange: node["maxrange"].value(), disbtype: node["disbtype"].value(), peramt: node["peramt"].value(), amount: node["amount"].value(), status: node["status"].value())
    }
}
class SMCashBackViewController: OKBaseController {
    
    var rec = [Recordata]()
    var CashBackViewDict = Dictionary<String, Any>()
    var NofItem = Int()
    let notificationCenter = NotificationCenter.default
    @IBOutlet weak var ScrollerView: UIScrollView!
    @IBOutlet weak var lblRecord: MarqueeLabel!{
        didSet
        {
            self.lblRecord.font = UIFont(name: appFont, size: appFontSize)
            lblRecord.text = lblRecord.text?.localized
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Cashback".localized
        self.lblRecord.isHidden = true
    }
    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.IntialCallAPI()
        notificationCenter.addObserver(self,  selector: #selector(SessionAlert), name: .appTimeout, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notificationCenter.removeObserver(self, name: .appTimeout, object: nil)
    }
    
    @objc func SessionAlert()  {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "", body: "Application Session expired login again".localized, img:  #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){
                if UserLogin.shared.loginSessionExpired { OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
            }
            alertViewObj.showAlert(controller: self)
            
        }
    }
    
    
    func IntialCallAPI(){
        
        let secureToken = UserLogin.shared.token
        self.cashBackApiCall(mobileNo: UserModel.shared.mobileNo, password: ok_password!, secre_token: secureToken)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func longPressed(_ gestureRecognizer: UILongPressGestureRecognizer){
        
        if gestureRecognizer.state == .began {
            
            //        appDel.floatingButtonControl?.window.isHidden = true
            println_debug("long pressed")
            let currentIndex = (gestureRecognizer.view?.tag)
            DispatchQueue.main.async{
                alertViewObj.wrapAlert(title: "", body: "Do you want to delete?".localized, img: #imageLiteral(resourceName: "new_cashback"))
                alertViewObj.addAction(title: "Cancel".localized, style: .target, action: {})
                alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                    self.deleteApiCall(mobileNo: UserModel.shared.mobileNo, password: ok_password!, secureToken: UserLogin.shared.token,object: self.rec[currentIndex!])
                })
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    
    func loadData(arr:[Recordata]) {
        DispatchQueue.main.async {
            self.ScrollerView.contentSize = CGSize(width: self.view.frame.size.width, height: 0.0)
            for view in self.ScrollerView.subviews{
                view.removeFromSuperview()
            }
            self.NofItem = 0
            println_debug("Count: \(arr.count)")
            println_debug("Data \(arr)")
            for recortValue in arr {
                let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressed(_:)))
                let customView = Bundle.main.loadNibNamed("SMAddFiveView", owner: self, options: nil)?[1] as! SMCashBackView
                customView.frame = CGRect(x: 0, y: (self.NofItem * 215)+2, width: Int(self.view.frame.size.width), height:  215)
                customView.parentVC = self
                customView.layoutIfNeeded()
                customView.tag = self.NofItem
                let minrange = recortValue.minrange
                let maxrange = recortValue.maxrange
                var peramt = ""
                var amount = ""
                var ctype = ""
                if recortValue.disbtype == "F" { ctype = "Fixed"; peramt = recortValue.peramt; amount = recortValue.amount }
                else if recortValue.disbtype == "R" { ctype = "Ratio";  peramt = recortValue.peramt; amount = recortValue.amount }
                else { ctype = "Percentage"; peramt = recortValue.amount.appending("%") ; amount = recortValue.peramt }
                
                let min = minrange.components(separatedBy: ".")
                if min.count>0{
                    var value = ""
                    if min.indices.contains(0){
                        value = min[0]
                    }
                    
                    if min.indices.contains(1){
                        if min[1] != "00"{
                            value = min[0] + "." + min[1]
                        }
                    }
                    customView.minValue.text = wrapAmountWithCommaDecimal(key: value)
                }
                
                let max = maxrange.components(separatedBy: ".")
                if max.count>0{
                    var value = ""
                    if max.indices.contains(0){
                        value = max[0]
                    }
                    
                    if max.indices.contains(1){
                        if max[1] != "00"{
                            value = max[0] + "." + max[1]
                        }
                    }
                    customView.maxValue.text = wrapAmountWithCommaDecimal(key: value)
                }
                
                
                
                let perAmount = peramt.components(separatedBy: ".")
                if recortValue.disbtype == "P" {customView.peramt.text = "\(perAmount[0])%"}else{customView.peramt.text = "\(perAmount[0])"}
                let Amount = amount.components(separatedBy: ".")
//                if Amount[0] == "0"{
//                    customView.amount.text = "0"
//                } else {
                    let maxamount = amount.components(separatedBy: ".")
                    if maxamount.count>0{
                        var value = ""
                        if maxamount.indices.contains(0){
                            value = maxamount[0]
                        }
                        
                        if maxamount.indices.contains(1){
                            if maxamount[1] != "00"{
                                value = maxamount[0] + "." + maxamount[1]
                            }
                        }
                        customView.amount.text = wrapAmountWithCommaDecimal(key: value)  
                    }
                    
                    
                
                customView.type.text = ctype
                
                if ctype == "Ratio"{
                    customView.peramt.text = "0"
                }
                customView.status.tag = self.NofItem
                if recortValue.status == "INACTIVE" {customView.status.isSelected = true}else{customView.status.isSelected = false}
                customView.status.addTarget(self, action: #selector(self.activeInActiveClick(_:)), for: UIControl.Event.touchUpInside)
                customView.addGestureRecognizer(longPressRecognizer)
                self.ScrollerView.addSubview(customView)
                self.NofItem = self.NofItem + 1
            }
            
            DispatchQueue.main.async {
                self.ScrollerView.contentSize = CGSize(width: self.view.frame.size.width, height: CGFloat((self.NofItem)*225)+2)
                UIView.animate(withDuration: 0.5) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    @IBAction func activeInActiveClick(_ sender: UIButton){
        
        if (sender.isSelected){
            self.customAlert(msg: "Do you want to change status?".localized, sender: sender,status: "ACTIVE"
            )
        }
        else{
            self.customAlert(msg: "Do you want to change status?".localized, sender: sender,status: "INACTIVE"
            )
        }
    }
    
    
    @IBAction func addClick(_ sender: Any){
        self.lblRecord.isHidden = true
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "SMAddCashBackViewController") as? SMAddCashBackViewController
        self.navigationController?.pushViewController(VC!, animated: true)
    }
    
    func cashBackApiCall(mobileNo:String,password:String,secre_token:String) {
        
        if appDelegate.checkNetworkAvail(){
            showProgressView()
            let web = WebApiClass()
            web.delegate = self
            let urlS = String.init(format: Url.CashBack,mobileNo,password,secre_token)
            let url =   getUrl(urlStr: urlS, serverType: .serverEstel)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "POST", mScreen: "CashBack")
        }
        else{
            alertViewObj.wrapAlert(title: "", body: "Network Not Available.".localized, img: #imageLiteral(resourceName: "setting"))
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {})
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    func customAlert(msg:String, title:String) {
        alertViewObj.wrapAlert(title: "", body: msg.localized, img: #imageLiteral(resourceName: "new_cashback"))
        alertViewObj.addAction(title: "OK".localized, style: .target){}
        alertViewObj.showAlert(controller: self)
    }
    
    
    func customAlert(msg: String,sender: UIButton,status: String){
        alertViewObj.wrapAlert(title: "", body: msg.localized, img: #imageLiteral(resourceName: "new_cashback"))
        alertViewObj.addAction(title: "Cancel".localized, style: .cancel){}
        alertViewObj.addAction(title: "OK".localized, style: .target){
            if (sender.isSelected){
                sender.isSelected=false;
            }
            else{
                sender.isSelected=true;
            }
            self.ActiveInactiveApiCall(mobileNo: UserModel.shared.mobileNo, password: ok_password!, secre_token: UserLogin.shared.token,sender: sender,status: status)
        }
        alertViewObj.showAlert(controller: self)
    }
    
    
    func ActiveInactiveApiCall(mobileNo:String,password:String,secre_token:String,sender:UIButton,status: String){
        
        if appDelegate.checkNetworkAvail(){
            showProgressView()
            let web = WebApiClass()
            web.delegate = self
            let object = rec[sender.tag]
            println_debug(object)
            let urlS = String.init(format: Url.Viewkickbackstatuschange_API,mobileNo,password,object.minrange,object.maxrange,object.disbtype,object.amount,secre_token,status)
            let url =   getUrl(urlStr: urlS, serverType: .serverEstel)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "POST", mScreen: "Viewkickback")
        }
        else{
            alertViewObj.wrapAlert(title: "", body: "Network Not Available.".localized, img:#imageLiteral(resourceName: "setting"))
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: { })
            alertViewObj.showAlert(controller: self)
        }
        
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            self.CashBackViewDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func deleteApiCall(mobileNo: String, password: String, secureToken: String, object: Recordata) {
        
        if appDelegate.checkNetworkAvail(){
            showProgressView()
            let web = WebApiClass()
            web.delegate = self
            var urlS = String()
            urlS = String.init(format:Url.deleteCashback,mobileNo,password,object.minrange,object.maxrange,object.disbtype,object.amount,secureToken)
            let url =   getUrl(urlStr: urlS, serverType: .serverEstel)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "POST", mScreen: "delete")
        }
        else{
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
        }
    }
}

extension SMCashBackViewController : WebServiceResponseDelegate {
    
    func webResponse(withJson json: AnyObject, screen: String){
        self.cashBackParsing(data: json,screen: screen)
    }
    
    func cashBackParsing(data: AnyObject,screen: String) {
        
        let responseString = String(data: data as! Data, encoding: .utf8)
        let xml            = SWXMLHash.parse(responseString!)
        println_debug(xml)
        
        self.enumerate(indexer: xml)
        println_debug(self.CashBackViewDict)
        if self.CashBackViewDict.keys.contains("resultdescription") {
            if self.CashBackViewDict["resultdescription"] as? String ?? "" == "Records Not Found" || self.CashBackViewDict["resultdescription"] as? String ?? "" == "Transaction Successful" || self.CashBackViewDict["resultdescription"] as? String ?? "" == "Invalid Secure Token" || self.CashBackViewDict["resultdescription"] as? String ?? "" == "Secure Token Expire" || self.CashBackViewDict["resultdescription"] as? String ?? "" == "Invalid Request (Schema Validation Failed)" || self.CashBackViewDict["resultdescription"] as? String ?? "" == "INVALID REQUEST" {
                if self.CashBackViewDict["resultdescription"] as? String ?? "" == "Transaction Successful"{
                    do{
                        if screen == "CashBack"{
                            rec = try xml["estel"]["response"]["records"]["record"].value()
                            if rec.count == 0{
                                self.lblRecord.isHidden = false
                                self.lblRecord.text = (self.CashBackViewDict["resultdescription"] as? String)?.localized
                            }
                            self.loadData(arr: self.rec)
                        }
                            
                        else if screen == "delete"{
                            DispatchQueue.main.async{
                                alertViewObj.addTextField(title: "", body: "Deleted Successfully".localized, img:  #imageLiteral(resourceName: "new_cashback"))
                                alertViewObj.addAction(title: "OK".localized, style: .target){
                                    self.IntialCallAPI()
                                }
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                        else{
                            DispatchQueue.main.async{
                                self.customAlert(msg: "Status Changed Successfully".localized, title: "")
                            }
                        }
                    }catch {
                        println_debug(error)
                    }
                }
                else  if self.CashBackViewDict["resultdescription"] as? String ?? "" == "Records Not Found"{
                    DispatchQueue.main.async{
                        //self.IntialCallAPI()
                        if screen == "CashBack" {
                            self.lblRecord.isHidden = false
                            self.lblRecord.text = "No Records Found".localized
                            self.rec.removeAll()
                            self.loadData(arr: self.rec)
                        }
                        self.lblRecord.isHidden = false
                        self.lblRecord.text = (self.CashBackViewDict["resultdescription"] as? String)?.localized
                    }
                } else {
                    DispatchQueue.main.async{
                        alertViewObj.addTextField(title: "", body: "Try Again".localized, img:  #imageLiteral(resourceName: "new_cashback"))
                        alertViewObj.addAction(title: "OK".localized, style: .target){
                            self.IntialCallAPI()
                        }
                        alertViewObj.showAlert(controller: self)
                    }
                }
                DispatchQueue.main.async{
                    self.removeProgressView()
                }
            }
        }
    }
}

extension SMCashBackViewController: BioMetricLoginDelegate {
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if isSuccessful {
            println_debug("Authorised")
        }
        else{
            if UserLogin.shared.loginSessionExpired { OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
        }
    }
}
