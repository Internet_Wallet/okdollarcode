//
//  PatternViewController.swift
//  OK
//
//  Created by Mohit on 10/23/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class SMPatternViewController: OKBaseController {
    @IBOutlet var label: UILabel!{
        didSet
        {
            label.font = UIFont(name: appFont, size: appFontSize)
            label.text = label.text?.localized
            label.textColor = UIColor.init(hex: "0321AA")
        }
    }
    @IBOutlet var lockView: HUIPatternLockView!
    @IBOutlet weak var btnCancel: UIButton!{
        didSet
        {
            btnCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnCancel.setTitle(btnCancel.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var btnContinue: UIButton!{
        didSet
        {
            btnContinue.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnContinue.setTitle(btnContinue.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var lock: GesturePatternLock!
    private var passwordString = ""
    var oldPassword = String()
    var strPassword = String()
    var strConfirmPassword = String()
    var changePasswordDict = Dictionary<String, Any>()
    let notificationCenter = NotificationCenter.default
    var buttonConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Pattern".localized
        self.setuplock()
        self.lockView.isHidden = true
        if !ok_password_type{
            // password
            self.label.text = ("Draw Your New Pattern").localized
            self.btnCancel.setTitle("Reset".localized, for: .normal)
            self.btnContinue.setTitle("Continue".localized, for: .normal)
        }
        else{
            // pattern
            self.label.text = "Draw Your current Pattern".localized
            self.btnCancel.setTitle("Reset".localized, for: .normal)
            self.btnContinue.setTitle("Next".localized, for: .normal)
        }
        buttonConstraint = btnContinue.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        buttonConstraint = btnCancel.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        buttonConstraint.isActive = true
        self.view.layoutIfNeeded()
        subscribeToShowKeyboardNotifications()
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.buttonConstraint.constant = 0 - keyboardHeight
        })
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        buttonConstraint.constant = 0
    }
    
    func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.oldPassword.removeAll()
        notificationCenter.addObserver(self, selector: #selector(SessionAlert), name: .appTimeout, object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notificationCenter.removeObserver(self, name: .appTimeout, object: nil)
    }
    
    @objc func SessionAlert()  {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "", body: "Application Session expired login again".localized, img:  #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){
                if UserLogin.shared.loginSessionExpired {      OKPayment.main.authenticate(screenName: "Dashboard", delegate: self) }
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    private func setuplock() {
        // Set number of sensors
        lock.lockSize = (3, 3)
        // Sensor grid customisations
        lock.edgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        // Sensor point customisation (normal)
        lock.setSensorAppearance(
            type: .inner,
            radius: 5,
            width: 5,
            color: UIColor.init(hex: "0321AA"),
            forState: .normal
        )
        lock.setSensorAppearance(
            type: .outer,
            color: .gray,
            forState: .normal
        )
        // Sensor point customisation (selected)
        lock.setSensorAppearance(
            type: .inner,
            radius: 5, //6 6 white
            width: 5,
            color: UIColor.init(hex: "0321AA"),
            forState: .selected
        )
        lock.setSensorAppearance(
            type: .outer,
            radius: 40,
            width: 5,
            color: UIColor.init(hex: "0321AA"),//.gray,
            forState: .selected
        )
        // Sensor point customisation (wrong password)
        lock.setSensorAppearance(
            type: .inner,
            radius: 03,
            width: 15,
            color: .red,
            forState: .error
        )
        lock.setSensorAppearance(
            type: .outer,
            radius: 0,
            width: 1,
            color: .red,
            forState: .error
        )
        // Line connecting sensor points (normal/selected)
        [GesturePatternLock.GestureLockState.normal, GesturePatternLock.GestureLockState.selected].forEach { (state) in
            lock.setLineAppearance(
                width: 5.5, color: UIColor.init(hex: "0321AA"),
                //UIColor(red: 241/255, green: 188/255, blue: 20/255, alpha: 1.0), // UIColor.clear UIColor.yellow.withAlphaComponent(0.5)
                forState: state
            )
        }
        // Line connection sensor points (wrong password)
        lock.setLineAppearance(
            width: 5.5, color: UIColor.red.withAlphaComponent(0.5),
            forState: .error
        )
        lock.addTarget(
            self, action: #selector(gestureComplete),
            for: .gestureComplete
        )
    }
    
    func capturePasswordWithDetails(_ code: String) -> (password: String,type: Bool) {
        
        return (retainingEncryptedPassword(code),true)
        
    }
    
    private  func retainingEncryptedPassword(_ aCode: String) -> String {
        
        if passwordString == "" {
            
            alertViewObj.wrapAlert(title: "", body: "Draw the Pattern to Login".localized, img: #imageLiteral(resourceName: "setting"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {  })
            alertViewObj.showAlert(controller: self)
        }
        
        println_debug("Password pattern string to check --------------> \(passwordString)")
        return patternEncryption(passwordString, withnumber: aCode)
    }
    
    
    @objc func gestureComplete(gestureLock: GesturePatternLock){
        
        if gestureLock.lockSequence.count <= 3 {
            println_debug("More Than 3 counts needed")
            self.btnCancel.setTitle("Reset".localized, for: .normal)
            return
        }
        self.btnCancel.setTitle("Reset".localized, for: .normal)
        
        if !ok_password_type{               //password
            for element in gestureLock.lockSequence{
                
                if self.btnContinue.titleLabel!.text == "Continue".localized{
                    self.strPassword.append(element.stringValue)
                }
                else{
                    self.strConfirmPassword.append(element.stringValue)
                }
            }
        }
            
        else{           // pattern
            
            for element in gestureLock.lockSequence{
                
                if self.btnContinue.titleLabel!.text == "Next".localized{
                    self.oldPassword.append(element.stringValue)
                }
                    
                else if self.btnContinue.titleLabel!.text == "Continue".localized{
                    self.strPassword.append(element.stringValue)
                }
                else{
                    self.strConfirmPassword.append(element.stringValue)
                }
            }
        }
        println_debug("old pattern value is :- \(oldPassword)")
        println_debug("your pattern value is :- \(strPassword)")
        println_debug("strConfirmPassword pattern value is :- \(strConfirmPassword)")
    }
    
    
    func changePasswordApiCall(newPass: String,confmPass: String) {
        
        println_debug(newPass)
        println_debug(confmPass)
        
        if appDelegate.checkNetworkAvail(){
            showProgressView()
            let web = WebApiClass()
            web.delegate = self
            let secureToken = UserLogin.shared.token
            let urlS = String.init(format: Url.changePassword,UserModel.shared.mobileNo,ok_password!,UserModel.shared.mobileNo,newPass,secureToken)
            let url =   getUrl(urlStr: urlS, serverType: .serverEstel)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "POST", mScreen: "changePassword")
            
        }else{
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    @IBAction func clearClick(_ sender: UIButton){
        
        lock.gestureLockState = .normal
        if !ok_password_type{               // password
            
            if btnCancel.titleLabel!.text == "Reset".localized{
                
                if label.text == "Draw Your New Pattern".localized{
                    self.strPassword = ""
                    self.label.text = "Draw Your New Pattern".localized
                    self.btnCancel.setTitle("Reset".localized, for: .normal)
                    self.btnContinue.setTitle("Continue".localized, for: .normal)
                    return
                }
                else{
                    self.strConfirmPassword = ""
                    self.label.text = "Draw Your New Pattern Again to confirm".localized
                    self.btnCancel.setTitle("Reset".localized,for: .normal)
                    self.btnContinue.setTitle("Submit".localized,for: .normal)
                    return
                }
            }
            else{
                self.strPassword = ""
                self.strConfirmPassword = ""
                self.navigationController?.popViewController(animated: true)
            }
        }
        else{
            
            if btnCancel.titleLabel!.text == "Reset".localized{
                if self.label.text == "Draw Your current Pattern".localized{
                    self.oldPassword = ""
                    self.label.text = "Draw Your current Pattern".localized
                    self.btnCancel.setTitle("Reset".localized, for: .normal)
                    self.btnContinue.setTitle("Next".localized, for: .normal)
                    return
                }
                else if self.label.text == "Draw Your New Pattern".localized{
                    self.strPassword = ""
                    self.label.text = "Draw Your New Pattern".localized
                    self.btnCancel.setTitle("Reset".localized, for: .normal)
                    self.btnContinue.setTitle("Continue".localized, for: .normal)
                    return
                }
                else{
                    self.strConfirmPassword = ""
                    self.label.text = "Draw Your New Pattern Again to confirm".localized
                    self.btnCancel.setTitle("Reset".localized, for: .normal)
                    self.btnContinue.setTitle("Submit".localized, for: .normal)
                    return
                }
            }
            if btnCancel.titleLabel!.text == "Cancel"{
                
                self.oldPassword = ""
                self.strPassword = ""
                self.strConfirmPassword = ""
                self.label.text = "Draw Your current Pattern".localized
                self.btnCancel.setTitle("Reset".localized,for: .normal)
                self.btnContinue.setTitle("Next".localized,for: .normal)
            }
        }
        
    }
    
    
    @IBAction func nextClick(_ sender: UIButton) {
        
        lock.gestureLockState = .normal
        
        let defValue = userDef.value(forKey: "statedef") as? String ?? ""
        println_debug(defValue)
        
        
        if !ok_password_type{       //password
            
            
            if self.btnContinue.titleLabel!.text == "Continue".localized{
                
                if self.strPassword == "" && self.strConfirmPassword == ""{
                    
                    self.customAlert(msg: "Please Draw Minimum 4 Dots Pattern".localized)
                    
                }
                else{
                    
                    self.label.text = "Draw Your New Pattern Again to confirm".localized
                    self.btnCancel.setTitle("Cancel".localized,for: .normal)
                    self.btnContinue.setTitle("Submit".localized,for: .normal)
                }
                
                return
                
            }else {
                
                if strPassword == strConfirmPassword && strPassword != "" && strConfirmPassword != ""{
                    
                    self.changePasswordApiCall(newPass: self.patternEncryption(strPassword, withnumber: UserModel.shared.mobileNo), confmPass:self.patternEncryption(strPassword, withnumber: UserModel.shared.mobileNo))
                    return
                    
                }
                else{
                    
                    self.customAlert(msg: "You have Drawn Incorrect Pattern. Please try again.".localized)
                    self.strConfirmPassword = ""
                    return
                }
                
                
            }
            
        }
        else{
            
            if self.oldPassword != ""{
                
                if ok_password == self.patternEncryption(self.strPassword, withnumber: UserModel.shared.mobileNo){
                    self.customAlert(msg: "Current Password/Pattern and New Password/Pattern cannot be same".localized)
                    self.strPassword = ""
                }
                    
                else{
                    
                    let encrptPwd = self.patternEncryption(self.oldPassword , withnumber: UserModel.shared.mobileNo)
                    
                    if self.btnContinue.titleLabel!.text == "Next".localized {
                        
                        if encrptPwd != ok_password{
                            
                            self.customAlert(msg: "You have Drawn Incorrect Pattern. Please try again".localized)
                            self.oldPassword = ""
                            return
                            
                        }
                            
                        else if defValue == "Password".localized{
                            
                            let VC = self.storyboard?.instantiateViewController(withIdentifier: "SMChangePasswordViewController")
                            self.navigationController?.pushViewController(VC!, animated: true)
                            return
                        }
                        else{
                            
                            self.label.text = "Draw Your New Pattern".localized
                            self.btnCancel.setTitle("Reset".localized, for: .normal)
                            self.btnContinue.setTitle("Continue".localized, for: .normal)
                            return
                        }
                    }
                }
                if self.btnContinue.titleLabel!.text == "Continue".localized{
                    
                    if self.strPassword == "" && self.strConfirmPassword == ""{
                        return
                    }
                    else{
                        
                        self.label.text = "Draw Your New Pattern Again to confirm".localized
                        self.btnCancel.setTitle("Cancel".localized,for: .normal)
                        self.btnContinue.setTitle("Submit".localized,for: .normal)
                        return
                    }
                }else {
                    
                    if strPassword == strConfirmPassword && strPassword != "" && strConfirmPassword != ""{
                        
                        self.changePasswordApiCall(newPass: self.patternEncryption(strPassword, withnumber: UserModel.shared.mobileNo), confmPass:self.patternEncryption(strPassword, withnumber: UserModel.shared.mobileNo))
                        
                        
                    }
                    else{
                        
                        self.customAlert(msg: "You have Drawn Incorrect Pattern. Please try again.".localized)
                        self.strConfirmPassword = ""
                    }
                    
                    return
                }
            }
                
            else if self.oldPassword.count < 4 {
                self.customAlert(msg: "Please Draw Minimum 4 Dots Pattern".localized)
            }
            
        }
        
    }
    
    
    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func customAlert(msg:String){
        
        alertViewObj.wrapAlert(title: "", body: msg, img: #imageLiteral(resourceName: "alert-icon"))
        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {
            //            self.oldPassword = ""
            //            self.strPassword = ""
            //            self.strConfirmPassword = ""
        })
        alertViewObj.showAlert(controller: self)
        
    }
    
}


// MARK: - Custom LockView with images
extension SMPatternViewController{
    
    internal func configuareLockViewWithImages(){
        
        let defaultLineColor = HUIPatternLockView.defaultLineColor
        let correctLineColor = UIColor.green
        let wrongLineColor = UIColor.red
        
        let normalImage = UIImage(named: "dot")
        let highlightedImage = UIImage(named: "patterm")
        let correctImage = highlightedImage?.tintImage(tintColor: correctLineColor)
        let wrongImage = highlightedImage?.tintImage(tintColor: wrongLineColor)
        
        lockView.didDrawPatternPassword = { (lockView, count, password) -> Void in
            guard count > 0 else {
                return
            }
            
            let unlockPassword = "[0][3][6][7][8]"
            
            self.label.text = ("Got Password: " + password!).localized
            
            println_debug("Got Password: " + password!)
            
            println_debug("".patternEncryption(password!, withnumber: UserModel.shared.mobileNo))
            
            
            if password == unlockPassword {
                lockView.lineColor = correctLineColor
                lockView.normalDotImage = correctImage
                lockView.highlightedDotImage = correctImage
            }
            else {
                lockView.lineColor = wrongLineColor
                lockView.normalDotImage = wrongImage
                lockView.highlightedDotImage = wrongImage
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                lockView.resetDotsState()
                lockView.lineColor = defaultLineColor
                lockView.normalDotImage = normalImage
                lockView.highlightedDotImage = highlightedImage
            }
        }
    }
}

// MARK: - Custom LockView Drawing
extension SMPatternViewController {
    
    private enum LockViewPasswordState: Int{
        
        case Normal
        case Correct
        case Wrong
        
    }
    
    fileprivate func configuareLockViewWithCustomDrawingCodes(){
        
        lockView.drawLinePath = { [unowned self] (path, context) -> Void in
            self.drawLockViewLinePath(path, context: context)
        }
        
        lockView.drawDot = { [unowned self] (dot, context) -> Void in
            self.drawLockViewDot(dot, context: context)
        }
        
        lockView.didDrawPatternPassword = { [unowned self] (lockView, count, password) -> Void in
            self.handleLockViewDidDrawPassword(lockView: lockView, count: count, password: password)
        }
    }
    
    private func colorForLockViewState(_ state: LockViewPasswordState, useHighlightedColor: Bool) -> UIColor {
        
        switch state {
        case .Correct:
            return UIColor.green
        case .Wrong:
            return UIColor.red
        default:
            if useHighlightedColor {
                return HUIPatternLockView.defaultLineColor
            }
            else {
                return UIColor.black
            }
        }
    }
    
    private func resetLockView(_ lockView: HUIPatternLockView) {
        
        lockView.resetDotsState()
        
        lockView.drawLinePath = { [unowned self] (path, context) -> Void in
            self.drawLockViewLinePath(path, context: context)
        }
        lockView.isUserInteractionEnabled = true
        
    }
    
    private func handleLockViewDidDrawPassword(lockView: HUIPatternLockView, count: Int, password: String?) {
        
        guard count > 0 else {
            resetLockView(lockView)
            return
        }
        
        let unlockPassword = "[0][3][6][7][8]"
        var state = LockViewPasswordState.Wrong
        if password == unlockPassword {
            state = .Correct
        }
        
        self.label.text = ("Got Password: " + password!).localized
        
        println_debug("Got Password: " + password!)
        
        lockView.isUserInteractionEnabled = false
        
        lockView.drawLinePath = { [unowned self] (path, context) -> Void in
            self.drawLockViewLinePath(path, context: context, state: state)
        }
        
        lockView.drawDot = { [unowned self] (dot, context) -> Void in
            self.drawLockViewDot(dot, context: context, state: state)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [weak self] in
            self?.resetLockView(lockView)
        }
    }
    
    private func drawLockViewLinePath(_ path: Array<CGPoint>, context: CGContext?, state: LockViewPasswordState = .Normal){
        
        guard !path.isEmpty else{
            return
        }
        
        guard let context = context else{
            return
        }
        
        let color = colorForLockViewState(state, useHighlightedColor: true)
        let dashLengths: [CGFloat] = [5.0, 10.0, 5.0]
        context.setStrokeColor(color.cgColor)
        context.setLineWidth(3.0)
        context.setLineCap(.round)
        context.setLineJoin(.round)
        context.setLineDash(phase: 0.0, lengths: dashLengths)
        
        let fistPoint = path.first
        for point in path {
            
            if point == fistPoint{
                context.move(to: point)
            }
            else {
                context.addLine(to: point)
            }
        }
        
        context.drawPath(using: .stroke)
    }
    
    private func drawLockViewDot(_ dot: HUIPatternLockView.Dot, context: CGContext?, state: LockViewPasswordState = .Normal) {
        
        guard let context = context else {
            return
        }
        
        let dotCenter = dot.center
        let innerDotRadius: CGFloat = 15.0
        let color = colorForLockViewState(state, useHighlightedColor: dot.highlighted)
        
        context.setLineWidth(1.0)
        context.setFillColor(color.cgColor)
        context.setStrokeColor(color.cgColor)
        
        context.move(to: dotCenter)
        context.beginPath()
        context.addArc(center: dotCenter, radius: innerDotRadius, startAngle: 0, endAngle: CGFloat(2*Double.pi), clockwise: true)
        context.closePath()
        context.fillPath()
        context.strokeEllipse(in: dot.frame)
        
    }
}

extension UIImage{
    
    public func tintImage(tintColor: UIColor) -> UIImage? {
        return tintImage(tintColor, blendMode: .destinationIn)
    }
    
    public func gradientTintImage(tintColor: UIColor) -> UIImage? {
        return tintImage(tintColor, blendMode: .overlay)
    }
    
    public func tintImage(_ tintColor: UIColor, blendMode: CGBlendMode) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        
        let bounds = CGRect(origin: CGPoint.zero, size: size)
        tintColor.setFill()
        UIRectFill(bounds)
        
        draw(in: bounds, blendMode: blendMode, alpha: 1.0)
        
        //draw again to save alpha channel
        if blendMode != .destinationIn {
            draw(in: bounds, blendMode: .destinationIn, alpha: 1.0)
        }
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
}

//MARK: encryption password
extension SMPatternViewController{
    
    func patternEncryption(_ password: String, withnumber number: String) -> String {
        
        let object = PatternValidationClass()
        let finalString = object.patternEncryption(password, withnumber: number)
        return finalString!
    }
}

extension String {
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
}

extension SMPatternViewController:WebServiceResponseDelegate{
    
    func webResponse(withJson json: AnyObject, screen: String){
        self.changePasswordParsing(data: json)
        
    }
    
    func changePasswordParsing(data: AnyObject){
        
        let responseString = String(data: data as! Data, encoding: .utf8)
        let xml            = SWXMLHash.parse(responseString!)
        println_debug(xml)
        self.enumerate(indexer: xml)
        println_debug(self.changePasswordDict)
        if self.changePasswordDict["resultdescription"] as? String ?? "" == "Transaction Successful" {
            DispatchQueue.main.async{
                //var str =    self.changePasswordDict["resultdescription"] as? String ?? ""
                let defaults = UserDefaults.standard
                defaults.set(self.patternEncryption(self.strConfirmPassword, withnumber: UserModel.shared.mobileNo), forKey: "password")
                
                userDef.set(self.patternEncryption(self.strConfirmPassword, withnumber: UserModel.shared.mobileNo), forKey: "passwordLogin_local")
                userDef.set(true, forKey: "passwordLoginType")
                userDef.synchronize()
                ok_password         = userDef.string(forKey: "passwordLogin_local")
                ok_password_type    = userDef.bool(forKey: "passwordLoginType")
                if let myString = userDef.string(forKey: "passwordLogin_local"){
                    println_debug("Password: \(myString)")
                    ok_password = myString
                }
                profileObj.callLoginApi(aCode: UserModel.shared.mobileNo, withPassword: ok_password! , handler: { (success) in
                    if success {
                        profileObj.callUpdateProfileApi(aCode: UserModel.shared.mobileNo, handler: { (status) in
                            if status {
                                DispatchQueue.main.async {
                                    self.removeProgressView()
                                    
                                    alertViewObj.wrapAlert(title: "", body: "Your password has been changed successfully.".localized, img:UIImage.init(named: "bank_success"))
                                    alertViewObj.addAction(title: "Done".localized, style: AlertStyle.target, action: {
                                        UserLogin.shared.loginSessionExpired = true
                                        self.navigationController?.popToRootViewController(animated: true)
                                        //NotificationCenter.default.post(name: .goDashboardScreen, object: nil)
                                    })
                                    alertViewObj.showAlert(controller: self)
                                }
                            }
                        })
                    }
                })
            }
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            self.changePasswordDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
}
extension SMPatternViewController: BioMetricLoginDelegate {
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if isSuccessful {
            println_debug("Authorised")
        }
        else{
            if UserLogin.shared.loginSessionExpired { OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
        }
    }
}
