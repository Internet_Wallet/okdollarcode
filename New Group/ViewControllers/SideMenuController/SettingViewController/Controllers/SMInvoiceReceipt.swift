//
//  SMInvoiceReceipt.swift
//  OK
//
//  Created by Mohit on 12/2/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class SMInvoiceReceipt: OKBaseController {
    
    var dataDict = Dictionary<String, Any>()
    let notificationCenter = NotificationCenter.default
    
    @IBOutlet var senderAccName: UILabel!{
        didSet
        {
            senderAccName.font = UIFont(name: appFont, size: appFontSize)
            senderAccName.text = senderAccName.text?.localized
        }
    }
    @IBOutlet var senderAccNum: UILabel!{
        didSet
        {
            senderAccNum.font = UIFont(name: appFont, size: appFontSize)
            senderAccNum.text = senderAccNum.text?.localized
        }
    }
    @IBOutlet var receiverAccName: UILabel!{
        didSet
        {
            receiverAccName.font = UIFont(name: appFont, size: appFontSize)
            receiverAccName.text = receiverAccName.text?.localized
        }
    }
    @IBOutlet var receiverAccNum: UILabel!{
        didSet
        {
            receiverAccNum.font = UIFont(name: appFont, size: appFontSize)
            receiverAccNum.text = receiverAccNum.text?.localized
        }
    }
    @IBOutlet var transactionId: UILabel!{
        didSet
        {
            transactionId.font = UIFont(name: appFont, size: appFontSize)
            transactionId.text = transactionId.text?.localized
        }
    }
    @IBOutlet var transactionType: UILabel!{
        didSet
        {
            transactionType.font = UIFont(name: appFont, size: appFontSize)
            transactionType.text = transactionType.text?.localized
        }
    }
    @IBOutlet var dateTime: UILabel!{
        didSet
        {
            dateTime.font = UIFont(name: appFont, size: appFontSize)
            dateTime.text = dateTime.text?.localized
        }
    }
    @IBOutlet var remark: UILabel!{
        didSet
        {
            remark.font = UIFont(name: appFont, size: appFontSize)
            remark.text = remark.text?.localized
        }
    }
    
    @IBOutlet weak var Amount: UILabel!{
        didSet
        {
            Amount.font = UIFont(name: appFont, size: appFontSize)
            Amount.text = Amount.text?.localized
        }
    }
    
    
    @IBOutlet var receipt: UILabel!{
        didSet
        {
            receipt.font = UIFont(name: appFont, size: appFontSize)
            receipt.text = receipt.text?.localized
        }
    }
    
    @IBOutlet weak var lblSenderAccountName: UILabel!{
        didSet
        {
            lblSenderAccountName.font = UIFont(name: appFont, size: appFontSize)
            lblSenderAccountName.text = lblSenderAccountName.text?.localized
        }
    }
    
    @IBOutlet weak var lblSelectAccountNumber: UILabel!{
        didSet
        {
            lblSelectAccountNumber.font = UIFont(name: appFont, size: appFontSize)
            lblSelectAccountNumber.text = lblSelectAccountNumber.text?.localized
        }
    }
    
    @IBOutlet weak var lblReceiverAccountName: UILabel!{
        didSet
        {
            lblReceiverAccountName.font = UIFont(name: appFont, size: appFontSize)
            lblReceiverAccountName.text = lblReceiverAccountName.text?.localized
        }
    }
    
    @IBOutlet weak var lblReceiverAccountNumber: UILabel!{
        didSet
        {
            lblReceiverAccountNumber.font = UIFont(name: appFont, size: appFontSize)
            lblReceiverAccountNumber.text = lblReceiverAccountNumber.text?.localized
        }
    }
    
    @IBOutlet weak var lblTransactionId: UILabel!{
        didSet
        {
            lblTransactionId.font = UIFont(name: appFont, size: appFontSize)
            lblTransactionId.text = lblTransactionId.text?.localized
        }
    }
    
    @IBOutlet weak var lblTransactionType: UILabel!{
        didSet
        {
            lblTransactionType.font = UIFont(name: appFont, size: appFontSize)
            lblTransactionType.text = lblTransactionType.text?.localized
        }
    }
    @IBOutlet weak var lblDateandTime: UILabel!{
        didSet
        {
            lblDateandTime.font = UIFont(name: appFont, size: appFontSize)
            lblDateandTime.text = lblDateandTime.text?.localized
        }
    }
    @IBOutlet weak var lblRemark: UILabel!{
        didSet
        {
            lblRemark.font = UIFont(name: appFont, size: appFontSize)
            lblRemark.text = lblRemark.text?.localized
        }
    }
    @IBOutlet weak var lblAmount: UILabel!{
        didSet
        {
            lblAmount.font = UIFont(name: appFont, size: appFontSize)
            lblAmount.text = lblAmount.text?.localized
        }
    }
    
    @IBOutlet weak var imgQrScan: UIImageView!
    @IBOutlet weak var amountPaidView: UIView!
    @IBOutlet weak var transactionReceiptView: UIView!
    @IBOutlet var btnShare: UIButton!{
        didSet{
            btnShare.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnShare.setTitle(btnShare.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    
    var qrImageObject :  PTQRGenerator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "Invoice Receipt".localized;
        amountPaidView.layer.borderWidth = 1
        amountPaidView.layer.borderColor = UIColor.colorWithRedValue(redValue: 210, greenValue: 210, blueValue: 210, alpha: 1).cgColor
        println_debug(dataDict)
        self.initData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func initData(){
        
        self.lblSenderAccountName.text = dataDict["agentname"] as? String
        var chnageNo1 = (dataDict["destination"] as? String)!
        chnageNo1.remove(at: chnageNo1.index(chnageNo1.startIndex, offsetBy: 1))
        chnageNo1.remove(at: chnageNo1.index(chnageNo1.startIndex, offsetBy: 1))
        chnageNo1.remove(at: chnageNo1.index(chnageNo1.startIndex, offsetBy: 1))
        
        self.lblSelectAccountNumber.text = "(+95)\(chnageNo1)".localized
        self.lblReceiverAccountName.text = (dataDict["agentname"] as? String)?.localized
        var chnageNo = (dataDict["destination"] as? String)!
        chnageNo.remove(at: chnageNo.index(chnageNo.startIndex, offsetBy: 1))
        chnageNo.remove(at: chnageNo.index(chnageNo.startIndex, offsetBy: 1))
        chnageNo.remove(at: chnageNo.index(chnageNo.startIndex, offsetBy: 1))
        
        
        self.lblReceiverAccountNumber.text = "(+95)\(chnageNo)".localized
        self.lblTransactionId.text = (dataDict["transid"] as? String)?.localized
        if dataDict["responsetype"] as? String == "PAYTOKICKBACK" {
            self.lblTransactionType .text = "CASHBACK".localized
        }
        else {
            self.lblTransactionType .text = dataDict["responsetype"] as? String
        }
        
        self.lblDateandTime.text = self.getDateFormat(date: (dataDict["responsects"] as? String) ?? "")
        self.lblRemark.text = (dataDict["comments"] as? String)?.localized
        self.lblAmount.text = ((dataDict["amount"] as? String)! + " MMK").localized
        self.lblAmount.text = (self.getDigitDisplay((dataDict["amount"] as? String)!) + " MMK").localized
        
        //      self.generateQRCodeAction(self.qrStringGeneration(amount: (dataDict["amount"] as? String)!, beforePayment: "", afterPayment: "", remarks: self.lblRemark.text!))
        
        self.generateQRCodeAction(self.getQRCodeImage()!)
    }
    
    
    func getDateFormat(date: String) -> String {
        let dateFormat = date
        let dateF = DateFormatter()
        dateF.dateFormat = "dd-MMM-yyyy HH:mm:ss"
        dateF.setLocale()
        guard let dateLoc = dateF.date(from: date) else { return dateFormat }
        dateF.dateFormat = "EEE, dd-MMM-yyyy HH:mm:ss"
        dateF.setLocale()
        return dateF.string(from: dateLoc)
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notificationCenter.addObserver(self, selector: #selector(SessionAlert),       name: .appTimeout,   object: nil)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notificationCenter.removeObserver(self, name: .appTimeout, object: nil)
    }
    
    @objc func SessionAlert()  {
        DispatchQueue.main.async {
            
            alertViewObj.wrapAlert(title: "", body: ("Application Session expired login again").localized, img:  #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){
                if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
            }
            alertViewObj.showAlert(controller: self)
            
        }
    }
    
    
    @IBAction func btnShareClick(_ sender: UIButton) {
        let pdfUrl = self.createPdfFromView(aView: transactionReceiptView, saveToDocumentsWithFileName: "Transaction Receipt".localized)
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl!], applicationActivities: nil)
        DispatchQueue.main.async { self.present(activityViewController, animated: true, completion: nil) }
        
    }
    
    func createPdfFromView(aView: UIView, saveToDocumentsWithFileName pdfFileName: String) -> URL? {
        let render = UIPrintPageRenderer()
        let page = CGRect(x: 0, y: 0, width: 595.2, height: 841)
        let printable = page.insetBy(dx: 0, dy: 0)
        render.setValue(NSValue(cgRect: page), forKey: "paperRect")
        render.setValue(NSValue(cgRect: printable), forKey: "printableRect")
        let priorBounds = aView.bounds
        let fittedSize = aView.sizeThatFits(CGSize(width:priorBounds.size.width, height:aView.bounds.size.height))
        aView.bounds = CGRect(x:0, y:0, width:fittedSize.width, height:fittedSize.height)
        let pdfPageBounds = CGRect(x:0, y:0, width:aView.frame.width, height:self.view.frame.height)
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds,nil)
        let pageOriginY: CGFloat = 0
        UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
        UIGraphicsGetCurrentContext()!.saveGState()
        UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
        aView.layer.render(in: UIGraphicsGetCurrentContext()!)
        UIGraphicsGetCurrentContext()!.restoreGState()
        UIGraphicsEndPDFContext()
        aView.bounds = priorBounds
        
        guard let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else  { return nil}
        let properPDFFileName = pdfFileName.replacingOccurrences(of: ".pdf", with: "") + ".pdf"
        let pdfUrl = documentDirectories + "/" + properPDFFileName
        //Delete file if alreay exists
        do {
            try FileManager.default.removeItem(atPath: pdfUrl)
        } catch let error as NSError {
            println_debug("Error: \(error.domain)")
        }
        //Write file
        if pdfData.write(toFile: pdfUrl, atomically: true) {
            println_debug("Successfully wrote to \(pdfUrl)")
            
            let pathUrl = URL(fileURLWithPath: documentDirectories)
            let fileUrl = pathUrl.appendingPathComponent(properPDFFileName)
            return fileUrl
        }
        return nil
    }
    
    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func captureScreen() -> UIImage? {
        
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, UIScreen.main.scale)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    
    
    func qrStringGeneration(amount: String, beforePayment: String, afterPayment: String, remarks: String) -> String {
        self.lblAmount.text = amount + " " + "MMK"
        let lat  = geoLocManager.currentLatitude ?? ""
        let long = geoLocManager.currentLongitude ?? ""
        let currentDateTimeStr = getCurrentDateForQRCode()
        var name = ""
        if UserModel.shared.agentType == .merchant {
            name = UserModel.shared.businessName
        } else {
            name = UserModel.shared.name
        }
        let firstPart  = "00#" + "\(name)" + "-" + "\(UserModel.shared.mobileNo)" + "@" + "\(amount)" + "&"
        let secondPart =  "" + "β" + "\(lat)" + "γ" + "\(long)" + "α" + currentDateTimeStr + "`\(afterPayment),\(beforePayment),\(remarks)"
        let finalPart = String.init(format:"%@%@", firstPart,secondPart)
        println_debug("-------> \(finalPart)")
        guard let hashedQRKey = AESCrypt.encrypt(finalPart, password: "m2n1shlko@$p##d") else { return "" }
        return hashedQRKey
    }
    
    func generateQRCodeAction(_ str: String) {
        
        qrImageObject = PTQRGenerator()
        guard qrImageObject != nil , let qrImage = qrImageObject else {
            return
        }
        
        guard let image =  qrImage.getQRImage(stringQR: str, withSize: 10) else {
            return
        }
        self.imgQrScan.image = image
    }
    
    
    func getQRCodeImage() -> String? {
        
        let phNumber = UserModel.shared.mobileNo
        var transDateStr = ""
        let dFormatter = DateFormatter()
        dFormatter.calendar = Calendar(identifier: .gregorian)
        dFormatter.dateFormat = "dd/MMM/yyyy HH:mm:ss"
        dFormatter.setLocale()
        if let trDate = dataDict["requestcts"] {
            transDateStr = dFormatter.string(from: dFormatter.date(from: trDate as! String)!)
        }
        let senderName = UserModel.shared.name
        let senderNumber = (dataDict["destination"] as? String)!
        let receiverName = UserModel.shared.name
        let receiverBusinessName = UserModel.shared.businessName
        let receiverNumber = (dataDict["destination"] as? String)!
        let firstPartToEncrypt = "\(transDateStr)----\(phNumber)"
        let transID = (dataDict["transid"] as? String)!
        let trasnstype = (dataDict["responsetype"] as? String)!
        var gender = ""
        if UserModel.shared.gender == "1"{gender = "M"}
        else{gender = "F"}
        let age = UserModel.shared.ageNow
        let lattitude = UserModel.shared.lat
        let longitude = UserModel.shared.long
        let firstPartEncrypted = AESCrypt.encrypt(firstPartToEncrypt, password: "m2n1shlko@$p##d")
        let balance = (dataDict["walletbalance"] as? String)!
        let cashBackAmount = "0"
        let amount = (dataDict["amount"] as? String)!
        let bonus = "0"
        let state = UserModel.shared.state
        let str2 = "OK-\(senderName)-\(amount)-\(transDateStr)-\(transID)-\(senderNumber)-\(balance)-des-\(receiverNumber)-\(trasnstype)-\(cashBackAmount)-\(bonus)-\(lattitude),\(longitude)-0-\(gender)-\(age)-\(state)-\(receiverName)-\(receiverBusinessName)"
        let secondEncrypted = AESCrypt.encrypt(str2, password: firstPartToEncrypt)
        var firstPartEncryptedSafeValue = ""
        var secondEncryptedSafeValue = ""
        if let firstEncrypted = firstPartEncrypted {
            firstPartEncryptedSafeValue = firstEncrypted
        }
        if let secondEncrypt = secondEncrypted {
            secondEncryptedSafeValue = secondEncrypt
        }
        let finalEncyptFormation = "\(firstPartEncryptedSafeValue)---\(secondEncryptedSafeValue)"
        return finalEncyptFormation
        
    }
}

extension SMInvoiceReceipt: BioMetricLoginDelegate {
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if isSuccessful {
            println_debug("Authorised")
        }
        else{
            if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
        }
    }
}



extension UIApplication {
    
    class var topViewController: OKBaseController? {
        return getTopViewController() as? OKBaseController
    }
    
    private class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return getTopViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}

extension Equatable {
    
    func shareAnother(activityItem: UIImage){
        let activity = UIActivityViewController(activityItems:  [activityItem], applicationActivities: nil)
        DispatchQueue.main.async {UIApplication.topViewController?.present(activity, animated: true, completion: nil)}
    }
}

