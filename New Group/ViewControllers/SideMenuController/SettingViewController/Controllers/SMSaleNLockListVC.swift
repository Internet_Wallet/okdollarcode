//
//  SMSaleNLockListVC.swift
//  OK
//
//  Created by gauri OK$ on 5/29/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

struct SaleNLockListHolder: Codable {
    var userList: [SaleNLockList]?
    var emailList: [SaleNLockEmailList]?
    
    private enum CodingKeys: String, CodingKey {
        case userList = "DummyProfile"
        case emailList = "EmailList"
    }
}

struct SaleNLockEmailList: Codable {
    var dummyMerOkAccNum: String?
    var emailId: String?
   
    private enum CodingKeys: String, CodingKey {
        case dummyMerOkAccNum = "DummyMerOkAccNum"
        case emailId = "EmailId"
    }
}

struct SaleNLockList: Codable {
    var id: String?
    var name: String?
    var profilePic: String?
    var DOB: String?
    var NRC: String?
    var idPhoto: String?
    var emailId: String?
    var phoneNumber: String?
    var isActive: String?
    var state: String?
    var township: String?
    var forgotFlag: Bool?
    var status: Int?
    var cityName: String?
    var villageName: String?
    var gender: Bool?
    var joiningDate: String?

    private enum CodingKeys: String, CodingKey {
        case id = "ID"
        case name = "Name"
        case profilePic = "ProfilePic"
        case DOB = "DOB"
        case NRC = "NRC"
        case idPhoto = "IDPhoto"
        case emailId = "EmailId"
        case phoneNumber = "PhoneNumber"
        case isActive = "Is_Active"
        case state = "State"
        case township = "Township"
        case forgotFlag = "ForgotFlag"
        case status = "Status"
        case cityName = "CityName"
        case villageName = "VillageName"
        case gender = "Gender"
        case joiningDate = "JoiningDate"
    }
}

class SMSaleNLockListVC: OKBaseController, SMSaleNLockDetailsVCDelegate {

    @IBOutlet weak var saleNLockTbl: UITableView!
    var boolStatusEstel: Bool = false
    var saleNLockDict = Dictionary <String, Any> ()
    var dataArr = [NSDictionary]()
    var NofItem = Int()
    var recData = [SaleNLockList]()
    var searchResults = [SaleNLockList]()
    var searchBar = UISearchBar()
    var boolSearch: Bool = false
    
    var emailListData = [SaleNLockEmailList]()
    var recEstelArr = [SaleNLockData]()
    var recEstelDic = SaleNLockData()
    
    private var selectedIndex: Int?
    private let refreshControl = UIRefreshControl()

    var boolStatusRow: Bool = false
    var strStatusInActive: String = ""
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = nil

        self.setUpNavigation()
        // Do any additional setup after loading the view.
        self.title = "Safety Cashier".localized
        saleNLockTbl.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refereshAction), for: .valueChanged)
        //Get List User
        self.getSaleNLockUsersList()
    }
    
    @objc func refereshAction(refreshControl: UIRefreshControl) {
        self.getSaleNLockUsersList()
        refreshControl.endRefreshing()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        boolStatusRow = false
        boolSearch = false
        self.reloadListView()
    }
    
    func reloadListView() {
        self.getSaleNLockUsersList()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - extension of Table View

extension SMSaleNLockListVC : UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if boolSearch {
        if searchResults.count == 0 {
            let noDataLabel = UILabel(frame: tableView.bounds)
            noDataLabel.text = "No Records Found !!".localized
            noDataLabel.textAlignment = .center
            noDataLabel.font = UIFont(name: appFont, size: 18)
            tableView.backgroundView = noDataLabel
        } else {
            tableView.backgroundView = nil
        }
        } else {
            if recData.count == 0 {
                let noDataLabel = UILabel(frame: tableView.bounds)
                noDataLabel.text = "No Records Found !!".localized
                noDataLabel.textAlignment = .center
                noDataLabel.font = UIFont(name: appFont, size: 18)
                tableView.backgroundView = noDataLabel
            } else {
                tableView.backgroundView = nil
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
            return 400.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if boolSearch {
            return self.searchResults.count
        } else {
            return self.recData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cellId = "SMSaleNLockListCell"

            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SMSaleNLockListCell
        if boolSearch {
            cell.wrapData(arrayValue: self.searchResults[indexPath.row],indexTag: indexPath.row)
        } else {
            cell.wrapData(arrayValue: self.recData[indexPath.row],indexTag: indexPath.row)
        }
    
        //Button status update
        cell.btnReqApprove.addTarget(self, action: #selector(self.statusChange(_:)), for: .touchUpInside)
        cell.btnReqApprove.tag = indexPath.row
        
        //Sent OTP
        cell.btnInActive.addTarget(self, action: #selector(self.btnEditAction(_:)), for: .touchUpInside)
        cell.btnInActive.tag = indexPath.row
        cell.btnSendOTP.tag = indexPath.row
        cell.btnSendOTP.addTarget(self, action: #selector(self.btnSendOTPAction(_:)), for: .touchUpInside)
        
        //Active&InActive top status
        cell.btnStatusActiveInActive.addTarget(self, action: #selector(self.btnInActiveAction(_:)), for: .touchUpInside)
        cell.btnStatusActiveInActive.tag = indexPath.row
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        self.setUpNavigation()
        
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "SMSaleNLockDetailsVC") as! SMSaleNLockDetailsVC
        objVC.delegate = self
        if boolSearch {
            objVC.dataModel = self.searchResults[indexPath.row]
        } else {
            objVC.dataModel = self.recData[indexPath.row]
        }
        objVC.emailListData = emailListData
        self.navigationController?.pushViewController(objVC, animated: true)

    }
    
}

extension SMSaleNLockListVC {
    
      @objc func searchButtonAction() {
        println_debug("btnSearchClick")
        
        if boolSearch {
            boolSearch = false
            self.setUpNavigation()
            self.saleNLockTbl.reloadData()
        } else {
            //boolSearch = true
            searchBar.text = ""
            self.setupSearchBar()
        }
    }
    
    @IBAction func btnSearchClick(_ sender: UIBarButtonItem) {
        println_debug("btnSearchClick")
        
        if boolSearch {
            boolSearch = false
            self.setUpNavigation()
            self.saleNLockTbl.reloadData()
        } else {
            //boolSearch = true
            searchBar.text = ""
            self.setupSearchBar()
        }
    }
    
    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: API Request
    //Get App server list
    func getSaleNLockUsersList() {
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let web = WebApiClass()
            web.delegate = self
            //let urlS = String.init(format: Url.SaleNLockList, msid,simid,UserModel.shared.mobileNo)
            let urlS = String.init(format: Url.SaleNLockList, msid,simid,UserModel.shared.mobileNo)
            let url =   getUrl(urlStr: urlS, serverType: .serverApp)
            println_debug(url)
            //Static url
//            let url = URL(string: "http://69.160.4.151:8001/RestService.svc/RetrieveDummy?&msid=414061137451984&simid=89950616410374519848&phonenumber=00959763893150")
            //http://69.160.4.151:8001/RestService.svc/RetrieveDummy?&msid=01&simid=7A92A0FF-D422-4917-B382-8FEA04A4D58A&phonenumber=00959891497721
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "GetSaleNLockUsers")
        }
    }
    
    //Reuqest for Approval
    @objc func activeApproveSaleNLockApiCall(_ sender: UIButton) {
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let web = WebApiClass()
            web.delegate = self
            selectedIndex = sender.tag
            let user = self.recData[sender.tag]
            let strId = user.id ?? ""
            let urlS = String.init(format: Url.RequsteApprovalDummyMerchant_API, UserModel.shared.mobileNo, uuid, uuid, strId, "")
            let url =   getUrl(urlStr: urlS, serverType: .serverApp)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "ActivateUser")
        }
    }
    
    //Get Estel server record list
    func saleNLockApiCall(mobileNo: String, password: String, secureToken: String) {
        
        if appDelegate.checkNetworkAvail(){
            
            self.showProgressView()
            
            let web = WebApiClass()
            web.delegate = self
            let localWifiAdd = ""
            let urlS = String.init(format: Url.saleNLock,mobileNo,password,localWifiAdd,secureToken)
            let url =   getUrl(urlStr: urlS, serverType: .SaleNLockServerEstel)
            println_debug(url)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "POST", mScreen: "saleNLock")
            
        }
        else{
            
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
            
        }
    }
    
    //Active & InActive request on estel server
    func ActiveInactiveSaleNLockApiCall(_ sender: UIButton) {
        
        if appDelegate.checkNetworkAvail(){
            
            self.showProgressView()
            
            let web = WebApiClass()
            web.delegate = self
            var activeInactive = ""
            
            let intSenderTag = sender.tag
            let object = self.recData[intSenderTag]
            
            if(object.isActive?.caseInsensitiveCompare("Active") == .orderedSame){
                activeInactive = "INACTIVE"
                strStatusInActive = "InActive".localized
                
                let urlS = String.init(format: Url.DummyMerchantListStatusUpdate_API,UserModel.shared.mobileNo,object.phoneNumber ?? "",activeInactive,ok_password ?? "",UserLogin.shared.token)
                let url =   getUrl(urlStr: urlS, serverType: .SaleNLockServerEstel)
                let param = [String:String]() as AnyObject
                web.genericClass(url: url, param: param, httpMethod: "POST", mScreen: "ActiveInactiveSaleNLock")
            } else {
                activeInactive = "ACTIVE"
                strStatusInActive = "Active".localized

                let urlS = String.init(format: Url.DummyMerchantListStatusUpdate_API,UserModel.shared.mobileNo,object.phoneNumber ?? "",activeInactive,ok_password ?? "",UserLogin.shared.token)
                let url =   getUrl(urlStr: urlS, serverType: .SaleNLockServerEstel)
                let param = [String:String]() as AnyObject
                web.genericClass(url: url, param: param, httpMethod: "POST", mScreen: "ActiveInactiveSaleNLock")
            }
            
        }
        else{
            
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
            
        }
    }
    
    @objc @IBAction func statusChange(_ sender : UIButton){
        if boolStatusRow == false {
            boolStatusRow = true
        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "SMSaleNLockDetailsVC") as! SMSaleNLockDetailsVC
            objVC.delegate = self
        objVC.dataModel = self.recData[sender.tag]
            objVC.emailListData = emailListData

        self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @objc @IBAction func btnEditAction(_ sender : UIButton){
        
         if boolStatusRow == false {
            boolStatusRow = true

        let objVC = self.storyboard?.instantiateViewController(withIdentifier: "SMSaleNLockDetailsVC") as! SMSaleNLockDetailsVC
            objVC.delegate = self
        objVC.dataModel = self.recData[sender.tag]
            objVC.emailListData = emailListData

        self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @objc @IBAction func btnInActiveAction(_ sender : UIButton){
        
        //self.ActiveInactiveSaleNLockApiCall(sender)

        let intSenderTag = sender.tag
        let object = self.recData[intSenderTag]
        var strMsg = ""
        
        if(object.isActive?.caseInsensitiveCompare("Active") == .orderedSame){
            strMsg = "InActive".localized
        } else {
            strMsg = "Active".localized
        }
        alertViewObj.wrapAlert(title: "", body: "Do you want to change status to ".localized + strMsg + "?", img: #imageLiteral(resourceName: "alert-icon"))
        alertViewObj.addAction(title: "CANCEL".localized, style: .target){}
        alertViewObj.addAction(title: "OK".localized, style: .target){

                self.ActiveInactiveSaleNLockApiCall(sender)
        }
        alertViewObj.showAlert(controller: self)
    }
    
    @objc @IBAction func btnSendOTPAction(_ sender : UIButton){
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let web = WebApiClass()
            web.delegate = self
            selectedIndex = sender.tag
            let user = self.recData[sender.tag]
            let urlS = String.init(format: Url.SendForgotRequest, user.phoneNumber ?? "")
            let url =   getUrl(urlStr: urlS, serverType: .sendSaleNLockOTP)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "SendOTP")
        }
    }
}

extension SMSaleNLockListVC: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        if screen == "GetSaleNLockUsers" {
            self.usersListResponseParsing(data: json)
        } else if screen == "saleNLock" || screen == "ActiveInactiveSaleNLock" {
            self.saleNLockParsing(data: json, mscreen: screen)
        } else if screen == "ActivateUser" {
            self.userActivationResponseParsing(data: json)
        } else if screen == "SendOTP" {
            self.userSendOTP(data: json)
        }
    }
    
    func userSendOTP(data: AnyObject) {
        self.removeProgressView()
        do {
            if let safeData = data as? Data {
                let decoder = JSONDecoder()
                let approveSaleNLockUserResponse = try decoder.decode(ApproveSaleNLockUserResponse.self, from: safeData)
                if let code = approveSaleNLockUserResponse.code, code == 200 {
                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: "", body: "Safety Cashier Sent Otp Successfully".localized, img: #imageLiteral(resourceName: "info_success"))
                        alertViewObj.addAction(title: "OK".localized, style: .target){
                            self.getSaleNLockUsersList()
                        }
                        alertViewObj.showAlert(controller: self)
                    }
                } else {
                    DispatchQueue.main.async {
                        if let msg = approveSaleNLockUserResponse.msg {
                            alertViewObj.wrapAlert(title: "", body: msg.localized, img: #imageLiteral(resourceName: "alert-icon"))
                            alertViewObj.addAction(title: "OK".localized, style: .target){}
                            alertViewObj.showAlert(controller: self)
                        }
                    }
                }
            }
        } catch let error {
            println_debug(error.localizedDescription)
        }
    }
    
    func userActivationResponseParsing(data: AnyObject) {
        self.removeProgressView()
        do {
            if let safeData = data as? Data {
                let decoder = JSONDecoder()
                let approveSaleNLockUserResponse = try decoder.decode(ApproveSaleNLockUserResponse.self, from: safeData)
                if let code = approveSaleNLockUserResponse.code, code == 200 {
                    DispatchQueue.main.async {
                        if let indexOfItem = self.selectedIndex {
                            self.recData[indexOfItem].status = 1
                            let indexPath = IndexPath(item: indexOfItem, section: 0)
                            self.saleNLockTbl.reloadRows(at: [indexPath], with: .top)
                            self.selectedIndex = nil
                        }
                        DispatchQueue.main.async {
                            CoreDataHelper.shared.clearSMBFromDB()
                            profileObj.callUpdateProfileApi(aCode: UserModel.shared.mobileNo, handler: { (isSuccess) in
                                println_debug("Handle success and failure case")
                            })
                        }
                    }
                   
                } else {
                    self.selectedIndex = nil
                    DispatchQueue.main.async {
                        if let msg = approveSaleNLockUserResponse.msg {
                            alertViewObj.wrapAlert(title: "", body: msg.localized, img: #imageLiteral(resourceName: "alert-icon"))
                            alertViewObj.addAction(title: "OK".localized, style: .target){}
                            alertViewObj.showAlert(controller: self)
                            
                        }
                    }
                }
            }
        } catch let error {
            println_debug(error.localizedDescription)
        }
    }
    
    func setRightBarButtonItems() {
        let searchButton = UIBarButtonItem(image: UIImage(named:"r_search"), style: .plain, target: self, action:#selector(searchButtonAction))
        searchButton.tintColor = UIColor.gray
        self.navigationItem.rightBarButtonItem = searchButton
    }
    
    func usersListResponseParsing(data: AnyObject) {
        do {
            if let safeData = data as? Data {
                let decoder = JSONDecoder()
                self.removeProgressView()
                let saleNLockUserListResponse = try decoder.decode(SaleNLockUserListResponse.self, from: safeData)
                if let code = saleNLockUserListResponse.code, code == 200 {
                    guard let userListHolderStr = saleNLockUserListResponse.userListHolder else { return }
                    guard let userListHolderData = userListHolderStr.data(using: .utf8) else { return }
                    let userListHolder = try decoder.decode(SaleNLockListHolder.self, from: userListHolderData)
                    recData = userListHolder.userList ?? []
                    emailListData = userListHolder.emailList ?? []
                    
                   
                    for promotion in recData {
                        if let promo = promotion as? SaleNLockList {
                            if promo.status == 1 {
                                self.saleNLockApiCall(mobileNo: UserModel.shared.mobileNo, password: ok_password!, secureToken: UserLogin.shared.token)
                                break
                            }
                        }
                    }
                    
                    DispatchQueue.main.async {
                        if self.recData.count == 0 {
                            self.navigationItem.rightBarButtonItem = nil
                        } else {
                            self.setSearchButton()
                        }
                        
                        self.saleNLockTbl.reloadData()
                    }
                } else {
                    DispatchQueue.main.async {
                        if let msg = saleNLockUserListResponse.msg {
                            alertViewObj.wrapAlert(title: "", body: msg, img: #imageLiteral(resourceName: "safety_cashier_st"))
                            alertViewObj.addAction(title: "Done".localized, style: .target, action:{})
                            alertViewObj.showAlert(controller: self)
                        }
                    }
                }
            }
        } catch let error {
            println_debug(error.localizedDescription)
        }
    }
    
    func saleNLockParsing(data: AnyObject, mscreen: String){
        
        let responseString = String(data: data as! Data, encoding: .utf8)
        let xml            = SWXMLHash.parse(responseString!)
        
        println_debug(xml)
        self.enumerateOld(indexer: xml)
        println_debug(self.saleNLockDict)
        
        if self.saleNLockDict.keys.contains("resultdescription"){
            
            DispatchQueue.main.async {
                
                if self.saleNLockDict["resultdescription"] as? String ?? "" == "Records Not Found"{
                    let msg = self.saleNLockDict["resultdescription"] as? String ?? ""
                    alertViewObj.addTextField(title: "", body: msg.localized, img:  #imageLiteral(resourceName: "website"))
                    alertViewObj.addAction(title: "Done".localized, style: .target){
                    }
                    alertViewObj.showAlert(controller: self)
                }
                else if mscreen == "ActiveInactiveSaleNLock"{
                    if let message = self.saleNLockDict.safeValueForKey("resultdescription") as? String, message == "Transaction Successful" {
                        
                        alertViewObj.wrapAlert(title: "", body: self.strStatusInActive + " successfully".localized, img: #imageLiteral(resourceName: "info_success"))
                        alertViewObj.addAction(title: "OK".localized, style: .target){
                            self.saleNLockApiCall(mobileNo: UserModel.shared.mobileNo, password: ok_password!, secureToken: UserLogin.shared.token)
                        }
                        alertViewObj.showAlert(controller: self)
                    } else {
                        if let message = self.saleNLockDict.safeValueForKey("resultdescription") as? String {
                            alertViewObj.wrapAlert(title: "", body: message.localized, img: #imageLiteral(resourceName: "alert-icon"))
                            alertViewObj.addAction(title: "OK".localized, style: .target){}
                            alertViewObj.showAlert(controller: self)
                        }
                    }
                }
                else{
                    do {
                        self.recEstelArr.removeAll()
                        self.enumerateAlert(indexer: xml)
                        
                        println_debug(self.recEstelArr)
                        //Check with App Server Data & Merge Active status
                        var finalDataArr = [SaleNLockList]()
                        //var matchStatus = false
                        for appList in self.recData {
                            if var appModel = appList as? SaleNLockList {
                                println_debug(appModel)
                                
                                for promotion in self.recEstelArr {
                                    if let promo = promotion as? SaleNLockData {
                                        println_debug(promo)
                                        if appModel.phoneNumber == promo.mobileno {
                                            println_debug(promo.status)
                                            appModel.isActive = promo.status
                                            break
                                        }
                                    }
                                }//for-end of estel data
                                finalDataArr.append(appModel)
                            }
                        }//for-end of Main array
                        self.recData.removeAll()
                        self.recData = finalDataArr
                    } catch {
                        println_debug(error)
                    }
                    self.loadData(arr: self.recData)
                }
                self.removeProgressView()
            }
        }
    }
    
    
    //MARK:- Enumerate data from XML
    func enumerateOld(indexer: XMLIndexer) {
        for child in indexer.children {
            self.saleNLockDict[child.element!.name] = child.element!.text
            enumerateOld(indexer: child)
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            
            if child.element?.name == "name" {
                recEstelDic.name = child.element!.text
            } else if child.element?.name == "mobileno" {
                recEstelDic.mobileno = child.element!.text
            } else if child.element?.name == "email" {
                recEstelDic.email = child.element!.text
            } else if child.element?.name == "country" {
                recEstelDic.country = child.element!.text
            } else if child.element?.name == "state" {
                recEstelDic.state = child.element!.text
            } else if child.element?.name == "city" {
                recEstelDic.city = child.element!.text
            } else if child.element?.name == "status" {
                recEstelDic.status = child.element!.text
            } else if child.element?.name == "idproof" {
                recEstelDic.idproof = child.element!.text
            } else if child.element?.name == "idprooftype" {
                recEstelDic.idprooftype = child.element!.text
            }
            
            enumerate(indexer: child)
        }
    }
    
    func enumerateAlert(indexer: XMLIndexer) {
        for child in indexer.children {
            if child.element?.name == "record" {
                self.enumerate(indexer: child)
                recEstelArr.append(recEstelDic)
            }
            enumerateAlert(indexer: child)
        }
    }
    
    func loadData(arr:[SaleNLockList]) {
        
        DispatchQueue.main.async {
            self.saleNLockTbl.reloadData()
        }
    }
}
