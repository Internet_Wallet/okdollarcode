//
//  GenerateAgentCodeViewController.swift
//  OK
//
//  Created by Mohit on 10/24/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class SMGenerateAgentCodeViewController: OKBaseController {
    
    let notificationCenter = NotificationCenter.default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setTitle(title: "Generate Agent Code".localized)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notificationCenter.addObserver(self,   selector: #selector(SessionAlert),   name: .appTimeout,   object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notificationCenter.removeObserver(self, name: .appTimeout, object: nil)
    }
    
    @objc func SessionAlert()  {
        DispatchQueue.main.async {
            
            alertViewObj.wrapAlert(title: "", body: "Application Session expired login again".localized, img:  #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){
                if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
}


extension SMGenerateAgentCodeViewController: BioMetricLoginDelegate {
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if isSuccessful {
            println_debug("Authorised")
        }
        else{
            if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
        }
    }
}



extension UINavigationItem {
    
    func setTitle(title:String) {
        let one = UILabel()
        one.font = UIFont(name: appFont, size: appFontSize)
        one.text = title
        one.textColor = UIColor.white
        one.sizeToFit()
        UIView.animate(withDuration: 12.0, delay: 1, options: ([.curveLinear, .repeat]), animations: {() -> Void in
            one.center = CGPoint(x: 0 - one.bounds.size.width / 2, y: one.center.y)
        }, completion:  { _ in })
        let stackView = UIStackView(arrangedSubviews: [one])
        stackView.distribution = .equalCentering
        stackView.axis = .vertical
        stackView.frame = CGRect(x: 0, y: 0, width: 375, height: 35)
        one.sizeToFit()
        self.titleView = stackView
    }
}


