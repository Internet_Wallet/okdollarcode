//
//  SMViewThree.swift
//  OK
//
//  Created by Admin on 1/23/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit


protocol SMViewThreeDelegate: class {
    func doneClick(sender: UIButton?)
    func doneClick(sender: UIButton?, txtValue: String)
}


class SMViewThree: UIView {
    
    
    var delegate: SMViewThreeDelegate?
    var parentVCThree = TBMoreViewController()
    
    @IBOutlet weak var lbl_busniessname: UILabel!{
        didSet
        {
            lbl_busniessname.font = UIFont(name: appFont, size: appFontSize)
            lbl_busniessname.text = lbl_busniessname.text?.localized
        }
    }
    
    @IBOutlet weak var lbl_location: UILabel!{
        didSet
        {
            lbl_location.font = UIFont(name: appFont, size: appFontSize)
            lbl_location.text = lbl_location.text?.localized
        }
    }
    
    @IBOutlet weak var txt_busniessname: UITextField!{
        didSet
        {
            txt_busniessname.font = UIFont(name: appFont, size: appFontSize)
            txt_busniessname.text = txt_busniessname.text?.localized
        }
    }
    @IBOutlet weak var txt_vechicleRoute_addShop: UITextField!{
        didSet
        {
            txt_vechicleRoute_addShop.font = UIFont(name: appFont, size: 17.0)
            txt_vechicleRoute_addShop.text = txt_vechicleRoute_addShop.text?.localized
        }
    }
    @IBOutlet weak var txt_vechicleNumber_addShop: UITextField!{
        didSet
        {
            txt_vechicleNumber_addShop.font = UIFont(name: appFont, size: 17.0)
            txt_vechicleNumber_addShop.text = txt_vechicleNumber_addShop.text?.localized
        }
    }
    @IBOutlet weak var btnDone: UIButton!{
        didSet{
            self.btnDone.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.btnDone.setTitle(self.btnDone.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBAction func doneClick(_ sender: UIButton) {
        
        self.parentVCThree.dict["busniessname"] = self.txt_busniessname.text
        self.parentVCThree.dict["vechicleRoute"] = self.txt_vechicleRoute_addShop.text
        self.parentVCThree.dict["vechicleNumber"] = self.txt_vechicleNumber_addShop.text
        
        //        delegate?.doneClick(sender: sender)
        delegate?.doneClick(sender: sender, txtValue: self.txt_busniessname.text!)
        self.removeFromSuperview()
        //        let VC = self.storyboard?.instantiateViewController(withIdentifier: "SMAddPromotionViewController")
        //        self.navigationController?.pushViewController(VC!, animated: true)
    }
}
