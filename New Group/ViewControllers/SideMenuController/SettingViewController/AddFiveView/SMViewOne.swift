//
//  SMViewOne.swift
//  OK
//
//  Created by Admin on 1/23/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

//MARK: step 1 Add Protocol here.
protocol SMViewOneDelegate: class {
    func doneClick(sender: UIButton?)
    func doneClick(sender: UIButton?, txtValue: String)
}


class SMViewOne: UIView {
    
    var delegate: SMViewOneDelegate?
    var parentVCOne = TBMoreViewController()
    
    @IBOutlet weak var lbl_location: UILabel!{
        didSet
        {
            lbl_location.font = UIFont(name: appFont, size: appFontSize)
            lbl_location.text = lbl_location.text?.localized
        }
    }
    
    @IBOutlet weak var lbl_busniessname: UILabel!{
        didSet
        {
            lbl_busniessname.font = UIFont(name: appFont, size: appFontSize)
            lbl_busniessname.text = lbl_busniessname.text?.localized
        }
    }
    
    
    @IBOutlet weak var txt_busniessname: UITextField!{
        didSet
        {
            txt_busniessname.font = UIFont(name: appFont, size: 17.0)
            txt_busniessname.text = txt_busniessname.text?.localized
        }
    }
    @IBOutlet weak var txt_vechicleColor_addShop: UITextField!{
        didSet
        {
            txt_vechicleColor_addShop.font = UIFont(name: appFont, size: 17.0)
            txt_vechicleColor_addShop.text = txt_vechicleColor_addShop.text?.localized
        }
    }
    @IBOutlet weak var txt_vechicleNumber_addShop: UITextField!{
        didSet
        {
            txt_vechicleNumber_addShop.font = UIFont(name: appFont, size: 17.0)
            txt_vechicleNumber_addShop.text = txt_vechicleNumber_addShop.text?.localized
        }
    }
    @IBOutlet weak var txt_vechicleType_addShop: UITextField!{
        didSet
        {
            txt_vechicleType_addShop.font = UIFont(name: appFont, size: 17.0)
            txt_vechicleType_addShop.text = txt_vechicleType_addShop.text?.localized
        }
    }
    @IBOutlet weak var btnDone: UIButton!{
        didSet{
            self.btnDone.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.btnDone.setTitle(self.btnDone.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    
    @IBAction func doneClick(_ sender: UIButton) {
        
        println_debug(self.txt_busniessname.text)
        println_debug(self.txt_vechicleColor_addShop.text)
        println_debug(self.txt_vechicleNumber_addShop.text)
        println_debug(self.txt_vechicleType_addShop.text)
        
        self.parentVCOne.dict["busniessname"] = self.txt_busniessname.text
        self.parentVCOne.dict["vechicleColor"] = self.txt_vechicleColor_addShop.text
        self.parentVCOne.dict["vechicleNumber"] = self.txt_vechicleNumber_addShop.text
        self.parentVCOne.dict["vechicleType"] = self.txt_vechicleType_addShop.text
        
        println_debug(self.parentVCOne.dict)
        
        //        delegate?.doneClick(sender: sender)
        delegate?.doneClick(sender: sender, txtValue: self.txt_busniessname.text!)
        self.removeFromSuperview()
        
    }
    
}
