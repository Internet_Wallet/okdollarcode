//
//  SMViewTwo.swift
//  OK
//
//  Created by Admin on 1/23/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit


protocol SMViewTwoDelegate: class {
    func doneClick(sender: UIButton?)
    func doneClick(sender: UIButton?, txtValue: String)
}

class SMViewTwo: UIView {
    
    
    var delegate: SMViewTwoDelegate?
    var parentVCTwo = TBMoreViewController()
    
    
    @IBOutlet var lblBusiness: UILabel!{
        didSet
        {
            lblBusiness.font = UIFont (name: appFont, size: appFontSize)
            lblBusiness.text = lblBusiness.text?.localized
        }
    }
    
    @IBOutlet var lblValue: UILabel!{
        didSet
        {
            self.lblValue.font = UIFont(name: appFont, size: 14)
            lblValue.text = lblValue.text?.localized
        }
    }
    
    @IBOutlet weak var txt_busniessname: UITextField!{
        didSet
        {
            txt_busniessname.font = UIFont (name: appFont, size: 17.0)
            txt_busniessname.placeholder = txt_busniessname.placeholder?.localized
            //txt_busniessname.placeholder = NSLocalizedString("Type here", comment: "Type here")
            txt_busniessname.text = txt_busniessname.text?.localized
        }
    }
    @IBOutlet weak var btnDone: UIButton!{
        didSet{
            self.btnDone.titleLabel?.font = UIFont (name: appFont, size: appButtonSize)
            self.btnDone.setTitle(self.btnDone.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBAction func doneClick(_ sender: UIButton) {
        
        self.parentVCTwo.dict["busniessname"] = self.txt_busniessname.text?.localized
        //        delegate?.doneClick(sender: sender)
        delegate?.doneClick(sender: sender, txtValue: self.txt_busniessname.text!)
        self.removeFromSuperview()
        
    }
}



