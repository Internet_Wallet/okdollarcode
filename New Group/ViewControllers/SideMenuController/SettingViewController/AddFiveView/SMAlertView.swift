//
//  SMAlertView.swift
//  OK
//
//  Created by Mohit on 2/2/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

//MARK: step 1 Add Protocol here.
protocol SMAlertViewDelegate: class {
    func NoClick(sender: UIButton?)
    func YesClick(sender: UIButton?)
}

class SMAlertView: UIView {
    
    var delegate: SMAlertViewDelegate?
    var parentMoreVC = TBMoreViewController()
    
    @IBOutlet var label: UILabel!{
        didSet {
            label.text = label.text?.localized
        }
    }
    
    @IBAction func NoClick(_ sender: UIButton) {
        delegate?.NoClick(sender: sender)
        self.removeFromSuperview()
    }
    
    @IBAction func YesClick(_ sender: UIButton) {
        delegate?.YesClick(sender: sender)
        self.removeFromSuperview()
    }
    
}

//MARK: step 1 Add Protocol here.
protocol SMDeleteViewDelegate: class {
    func CloseClickDelegate(sender: UIButton?)
    func DeleteClickDelegate(sender: UIButton?)
    func SwitchClickDelegate(sender: UISwitch?)
}


class SMDeleteView: UIView {
    
    var delegate: SMDeleteViewDelegate?
    var parentdeleteVC = TBMoreViewController()
    
    @IBOutlet weak var lbl: UILabel!{
        didSet {
            lbl.font = UIFont(name: appFont, size: appFontSize)
            lbl.text = lbl.text?.localized
        }
    }
    
    @IBOutlet var switchStatus: UISwitch!
    @IBOutlet var Closebtn: UIButton!{
        didSet {
            self.Closebtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.Closebtn.setTitle(self.Closebtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var shopNamelbl: UILabel!{
        didSet {
            shopNamelbl.font = UIFont(name: appFont, size: appFontSize)
            shopNamelbl.text = shopNamelbl.text?.localized
        }
    }
    
    
    @IBAction func CloseClick(_ sender: UIButton) {
        delegate?.CloseClickDelegate(sender: sender)
        self.removeFromSuperview()
    }
    
    @IBAction func DeleteClick(_ sender: UIButton) {
        delegate?.DeleteClickDelegate(sender: sender)
        self.removeFromSuperview()
    }
    
    
    @IBAction func switchIsChanged(_ sender: UISwitch) {
        delegate?.SwitchClickDelegate(sender: sender)
    }
    
}



