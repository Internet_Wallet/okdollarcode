//
//  CashBackView.swift
//  OK
//
//  Created by Mohit on 10/25/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class SMCashBackView: UIView {
    var parentVC:SMCashBackViewController! = nil
    @IBOutlet weak var maxlbl: UILabel!{
        didSet
        {
            maxlbl.font  = UIFont(name: appFont, size: appFontSize)
            maxlbl.text = maxlbl.text?.localized
        }
    }
    @IBOutlet weak var minlbl: UILabel!{
        didSet
        {
            minlbl.font  = UIFont(name: appFont, size: appFontSize)
            minlbl.text = minlbl.text?.localized
        }
    }
    @IBOutlet weak var typelbl: UILabel!{
        didSet
        {
            typelbl.font  = UIFont(name: appFont, size: appFontSize)
            typelbl.text = typelbl.text?.localized
        }
    }
    @IBOutlet weak var amountlbl: UILabel!{
        didSet
        {
            amountlbl.font  = UIFont(name: appFont, size: appFontSize)
            amountlbl.text = amountlbl.text?.localized
        }
    }
    @IBOutlet weak var peramtlbl: UILabel!{
        didSet
        {
            peramtlbl.font  = UIFont(name: appFont, size: appFontSize)
            peramtlbl.text = peramtlbl.text?.localized
        }
    }
    @IBOutlet weak var statuslbl: UILabel!{
        didSet
        {
            statuslbl.font  = UIFont(name: appFont, size: appFontSize)
            statuslbl.text = statuslbl.text?.localized
        }
    }
    
    @IBOutlet weak var maxValue: UILabel!{
        didSet
        {
            maxValue.font  = UIFont(name: appFont, size: appFontSize)
            maxValue.text = maxValue.text?.localized
        }
    }
    @IBOutlet weak var minValue: UILabel!{
        didSet
        {
            minValue.font  = UIFont(name: appFont, size: appFontSize)
            minValue.text = minValue.text?.localized
        }
    }
    @IBOutlet weak var type: UILabel!{
        didSet
        {
            type.font  = UIFont(name: appFont, size: appFontSize)
            type.text = type.text?.localized
        }
    }
    @IBOutlet weak var amount: UILabel!{
        didSet
        {
            amount.font  = UIFont(name: appFont, size: appFontSize)
            amount.text = amount.text?.localized
        }
    }
    @IBOutlet weak var peramt: UILabel!{
        didSet
        {
            peramt.font  = UIFont(name: appFont, size: appFontSize)
            peramt.text = peramt.text?.localized
        }
    }
    @IBOutlet weak var status: UIButton!{
        didSet{
            status.titleLabel?.font  = UIFont(name: appFont, size: appButtonSize)
            self.status.setTitle(self.status.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var testBtn: UIButton!{
        didSet{
            testBtn.titleLabel?.font  = UIFont(name: appFont, size: appButtonSize)
            self.testBtn.setTitle(self.testBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var cardView: CardDesignView!
    @IBAction func activeInActiveClick(_ sender: UIButton) {
        
        if (sender.isSelected){
            sender.isSelected=false;
        }
        else{
            sender.isSelected=true;
        }
    }
    @IBAction func testBtnClick(_ sender: UIButton) {
        println_debug("testBtnClick")
        if (sender.isSelected){
            sender.isSelected=false;
        }
        else{
            sender.isSelected=true;
        }
    }
}
