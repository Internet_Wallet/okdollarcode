//
//  AddFiveView.swift
//  SettingVC
//
//  Created by Mohit on 10/20/17.
//  Copyright © 2017 Mohit. All rights reserved.
//

import UIKit

class SMAddFiveView: CardDesignView {
    
    @IBOutlet weak var headerlbl: UILabel!{
        didSet
        {
            headerlbl.text = headerlbl.text?.localized
        }
    }
    @IBOutlet weak var deleteBtn: UIButton!{
        didSet{
            self.deleteBtn.setTitle(self.deleteBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var contactBtn: UIButton!{
        didSet{
            self.contactBtn.setTitle(self.contactBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var tfContactPerson: UITextField!{
        didSet
        {
            tfContactPerson.text = tfContactPerson.text?.localized
        }
    }
    @IBOutlet weak var tfContactMobile: UITextField!{
        didSet
        {
            tfContactMobile.text = tfContactMobile.text?.localized
        }
    }
    
    @IBAction func activeInActiveClick(_ sender: UIButton) {
        
        if (sender.isSelected){
            sender.isSelected=false;
        }
        else{
            sender.isSelected=true;
        }
    }
}

extension NSAttributedString {
    
    func replacing(placeholder:String, with valueString:String) -> NSAttributedString {
        
        if let range = self.string.range(of:placeholder) {
            let nsRange = NSRange(range,in:valueString)
            let mutableText = NSMutableAttributedString(attributedString: self)
            mutableText.replaceCharacters(in: nsRange, with: valueString)
            return mutableText as NSAttributedString
        }
        return self
    }
}
