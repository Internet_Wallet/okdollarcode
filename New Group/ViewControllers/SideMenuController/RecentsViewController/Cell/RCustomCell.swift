//
//  customCell.swift
//  OK
//
//  Created by Admin on 12/5/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class RCustomCell: UITableViewCell {
    @IBOutlet var lblName: UILabel!{
        didSet{
            lblName.font = UIFont(name: appFont, size: appFontSize)
            lblName.text = lblName.text?.localized
            
        }
    }
    @IBOutlet var lblSecure: UILabel!{
        didSet{
            lblSecure.font = UIFont(name: appFont, size: appFontSize)
            lblSecure.text = lblSecure.text?.localized
        }
    }
    @IBOutlet var lblDateAndTime: UILabel!{
        didSet{
            lblDateAndTime.font = UIFont(name: appFont, size: appFontSize)
            lblDateAndTime.text = lblDateAndTime.text?.localized
        }
    }
    @IBOutlet var lblAmount: UILabel!{
        didSet{
            lblAmount.font = UIFont(name: appFont, size: appFontSize)
            lblAmount.text = lblAmount.text?.localized
        }
    }
    @IBOutlet var icon: UIImageView!
    @IBOutlet var circleLbl: UILabel!{
        didSet{
            circleLbl.font = UIFont(name: appFont, size: appFontSize)
            circleLbl.text = circleLbl.text?.localized
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func wrapData(img: UIImage, name: String, secure: String, dateandtime: String, amount: String, transType: String){
        
        icon.image    = img
        lblName.text = name.localized
        
        let number: String = secure
        let startIndex = number.index(number.startIndex, offsetBy: 2)
        let truncated  =  number.substring(from: startIndex)
        println_debug(number)      // "Dolphin"
        println_debug(truncated) // "Dolph"
        lblSecure.text = "+".appending(truncated).localized
        lblAmount.text = amount.replacingOccurrences(of: ".00", with: "").localized
        lblDateAndTime.text = dateandtime.localized
        circleLbl.text = transType.localized
        
    }
    func wrapData(object: DataModel){
        self.icon.layer.cornerRadius = icon.frame.size.height / 2
        //lblName.text = object.name.localized
        //Update Sender Name based on account type cr & dr
       if let com = object.comments as? String {
           let recieverMerchantName = com.components(separatedBy: "##")
           if recieverMerchantName.count >= 2 {
               let recieverName = recieverMerchantName[1]
               if recieverName == "Unregistered User" {
                   lblName.text =  object.name
               } else {
                   lblName.text = recieverName
               }
           }else {
            lblName.text =  "Unknown".localized
        }
       }
        if let name = lblName.text as? String , name == "Unknown" {
            if let name = object.name as? String , name != "" {
                lblName.text = name
            }
        }
        
        //
        var phNumber = ""
        let mNumber = object.email
            phNumber = mNumber.replaceFirstTwoChar(withStr: "+")
            phNumber.insert(")", at: phNumber.index(phNumber.startIndex, offsetBy: 3))
            phNumber.insert("(", at: phNumber.startIndex)
            if mNumber.hasPrefix("0095") {
                phNumber.insert("0", at: phNumber.index(phNumber.startIndex, offsetBy: 5))
            }
        
        lblAmount.text = object.amount.replacingOccurrences(of: ".00", with: "").localized
        if object.dateAndTime != "" {
            if let dateInDF = object.dateAndTime.dateValue(dateFormatIs: "dd-MMM-yyyy HH:mm:ss") {
                lblDateAndTime.text = dateInDF.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss")
            }
        }
        if object.comments.hasPrefix("#OK-00-") {
            let delimiter = "-"
            let newstr = object.comments
            var token = newstr.components(separatedBy: delimiter)
            circleLbl.text = token[2].uppercased()
            circleLbl.isHidden = true
            icon.backgroundColor = UIColor.clear
            if token[6].hasPrefix("Overseas"){
//                icon.backgroundColor = UIColor(red: 252.0/255.0, green: 202.0/255.0, blue: 37.0/255.0, alpha: 1.0)
                circleLbl.text = token[2]
                circleLbl.isHidden = true
                lblSecure.text = "(\(token[7]))\(token[3])"
            } else {
                token[3].remove(at: token[3].startIndex)
                lblSecure.text = "(+95)0".appending(token[3])
            }
        } else if object.comments.hasPrefix("OOK-00-") {
            let delimiter = "-"
            let newstr = object.comments
            var token = newstr.components(separatedBy: delimiter)
            circleLbl.text = token[2].uppercased()
            circleLbl.isHidden = true
            icon.backgroundColor = UIColor.clear
            
            token[3].remove(at: token[3].startIndex)
            lblSecure.text = "(+95)0".appending(token[3])
        } else {
            circleLbl.text = object.transtype.localized
            circleLbl.isHidden = true
//            icon.backgroundColor = UIColor(red: 252.0/255.0, green: 202.0/255.0, blue: 37.0/255.0, alpha: 1.0)
            if object.acctranstype == "Dr" {
                lblSecure.text = phNumber
            } else {
                if object.transtype == "PAYTO" {
                    lblSecure.text = phNumber
                } else {
                    lblSecure.text = "XXXXXXXXXX"
                }
            }
        }
        
        if object.comments.contains(find: "ELECTRICITY") {
            lblSecure.text = "XXXXXXXXXX"
        } else if object.comments.contains(find: "MPU")  {
            lblSecure.text = "XXXXXXXXXX"
        } else if object.comments.contains(find: "REFUND")  {
            lblSecure.text = "XXXXXXXXXX"
        } else if object.comments.contains(find: "POSTPAID")  || object.comments.contains(find: "MERCHANTPAY")  || object.comments.contains(find: "LANDLINE")  || object.comments.contains(find: "SUNKING")  || object.comments.contains(find: "INSURANCE")  {
            lblSecure.text = "XXXXXXXXXX"
        } else if object.comments.contains("#OK") {
            if  object.comments.contains(find: "PAYMENTTYPE:GiftCard")  {
                lblSecure.text = "XXXXXXXXXX"
            } else if object.comments.contains(find: "DTH") {
                lblSecure.text = "XXXXXXXXXX"
            }
        }  else if object.acctranstype == "" {
            lblSecure.text = "XXXXXXXXXX"
        } else if object.comments.contains(find: "Bonus Point Exchange Payment") {
            lblSecure.text = "XXXXXXXXXX"
        }
        else if object.comments.contains(find: "SEND MONEY TO BANK:") {
            lblSecure.text = "XXXXXXXXXX"
        } else if object.comments.contains(find: "DTH") {
            lblSecure.text = "XXXXXXXXXX"
        } else if object.comments.contains(find: "ProjectId") {
            var mobileNo = "XXXXXXXXXX"
            let value = object.comments
            if value.contains(find: "ProjectId"){
                let newValue = value.components(separatedBy: " Wallet Number :")
                if newValue.count > 2 {
                    if newValue.indices.contains(1){
                        let name = newValue[2].components(separatedBy: "ProjectId")
                        if name.indices.contains(0){
                            mobileNo = "(+95)".appending(name[0])
                        }
                    }
                }  else {
                    //OK Dollar Wallet To MAB , OK Dollar Wallet Number :  00959790681381 Account Number :09976669357ProjectIdMAB767898KUEOJBU_IEHK787UOEJDHF
                    let arrVal = value.components(separatedBy: "Account Number :")
                    if arrVal.indices.contains(1){
                        let name = arrVal[1].components(separatedBy: "ProjectId")
                        if name.indices.contains(0){
                            mobileNo = "(+95)".appending(name[0])
                        }
                    }
                }
            }
            lblSecure.text = mobileNo
        } else if object.comments.contains(find: "#Tax Code") {
            lblName.text = "Unknown"
            lblSecure.text = "XXXXXXXXXX"
        }
        
        self.setImageIconAsPerOperator(comments: object.comments, transactionType: object.transtype, record: object)
    }
    
    func setImageIconAsPerOperator(OprName: String,object: DataModel){
        switch OprName {
        case "OOREDOO":
            icon.image    = #imageLiteral(resourceName: "receiptOoredoo") ; break
        case "MPT":
            icon.image    = #imageLiteral(resourceName: "receiptMpt") ; break
        case "MECTEL":
            icon.image    = #imageLiteral(resourceName: "receiptMectel") ; break
        case "TELENOR":
            icon.image    = #imageLiteral(resourceName: "receiptTelenor") ; break
        case "MYTEL":
            icon.image = #imageLiteral(resourceName: "mytel_recent.png") ; break
        default:  break;
        }
    }
    
    
    func setImageIconAsPerOperator(comments: String, transactionType: String?, record: DataModel) {
        if comments.contains("#OK") || comments.contains("OOK") {
            switch comments {
            case let telenor where telenor.contains("-Telenor-"):
                icon.image = #imageLiteral(resourceName: "receiptTelenor").withRenderingMode(.alwaysOriginal)
            case let telenor where telenor.contains("-Mpt-"):
                icon.image = #imageLiteral(resourceName: "receiptMpt").withRenderingMode(.alwaysOriginal)
            case let telenor where telenor.contains("-Ooredoo-"):
                icon.image = #imageLiteral(resourceName: "receiptOoredoo").withRenderingMode(.alwaysOriginal)
            case let telenor where telenor.contains("-MecTel-") || telenor.contains("-Mectel-"):
                icon.image = #imageLiteral(resourceName: "receiptMectel").withRenderingMode(.alwaysOriginal)
            case let telenor where telenor.contains("#OK-voting"):
                icon.image = #imageLiteral(resourceName: "receiptVote").withRenderingMode(.alwaysOriginal)
            case let telenor where telenor.contains("MyTel") || telenor.contains("-Mytel-"):
                icon.image = #imageLiteral(resourceName: "mytel_recent.png").withRenderingMode(.alwaysOriginal)
            case let telenor where telenor.contains(find: "PAYMENTTYPE:GiftCard"):
                let image = UIImage(named: "gift_card_trans")?.withRenderingMode(.alwaysOriginal)
                icon.image = image
            default:
                if comments.contains(find: "#OK Wallet Topup") {
                    let image = UIImage(named: "send_money_bank_trans")?.withRenderingMode(.alwaysOriginal)
                    icon.image = image
                }  else if record.comments.contains(find: "MPU") {
                    let image = UIImage(named: "mpu_trans")?.withRenderingMode(.alwaysOriginal)
                    icon.image = image
                } else if record.comments.contains(find: "Overseas") {
                    let image = #imageLiteral(resourceName: "top_up_trans").withRenderingMode(.alwaysOriginal)
                    icon.image = image
                }else if comments.lowercased().contains("topup") {
                    let image = UIImage(named: "top_up_trans")?.withRenderingMode(.alwaysOriginal)
                    icon.image = image
                } else if comments.lowercased().contains(find: "dth") {
                    let image = UIImage(named: "dth_trans")?.withRenderingMode(.alwaysOriginal)
                    icon.image = image
                } else {
                    
                    if record.comments.contains(find: "##"){
                        let image = UIImage(named: "pay_trans")?.withRenderingMode(.alwaysOriginal)
                        icon.image = image
                    }else{
                        icon.image = #imageLiteral(resourceName: "receiptMpt").withRenderingMode(.alwaysOriginal)
                    }
                    
                }
                    
                 
            }
        }
        else {
            if let transType = transactionType {
                
                if transType == "PAYWITHOUT ID" {
                    let image = UIImage(named: "hide_num")?.withRenderingMode(.alwaysOriginal)
                    icon.image = image
                } else if transType == "TOLL" {
                    icon.image = #imageLiteral(resourceName: "receiptToll").withRenderingMode(.alwaysOriginal)
                } else if transType == "TICKET" {
                    icon.image = #imageLiteral(resourceName: "receiptTicket").withRenderingMode(.alwaysOriginal)
                } else {
                    icon.image = #imageLiteral(resourceName: "receiptPay").withRenderingMode(.alwaysOriginal)
                }
            } else {
                let image = UIImage(named: "pay_trans")?.withRenderingMode(.alwaysOriginal)
                icon.image = image
            }
            
            
            if record.comments.contains(find: "ELECTRICITY") {
                let image = UIImage(named: "electricity_trans")?.withRenderingMode(.alwaysOriginal)
                icon.image = image
            } else if record.comments.contains(find: "MPU")  {
                let image = UIImage(named: "send_money_bank_trans")?.withRenderingMode(.alwaysOriginal)
                icon.image = image
            } else if record.comments.contains(find: "-cashin")  {
                let image = UIImage(named: "transfer_to_trans")?.withRenderingMode(.alwaysOriginal)
                icon.image = image
            } else if record.comments.contains(find: "-cashout")  {
                let image = UIImage(named: "cash_out_trans")?.withRenderingMode(.alwaysOriginal)
                icon.image = image
            }else if record.comments.contains(find: "REFUND") {
                icon.image = #imageLiteral(resourceName: "receiptPay").withRenderingMode(.alwaysOriginal)
            } else if record.comments.contains(find: "POSTPAID")  {
                let image = UIImage(named: "postpaid_bill_trans")?.withRenderingMode(.alwaysOriginal)
                icon.image = image
            } else if record.comments.contains(find: "MERCHANTPAY")  {
                let image = UIImage(named: "pay_trans")?.withRenderingMode(.alwaysOriginal)
                icon.image = image
            } else if record.comments.contains(find: "LANDLINE")  {
                let image = UIImage(named: "landline_bill_trans")?.withRenderingMode(.alwaysOriginal)
                icon.image = image
            } else if record.comments.contains(find: "SUNKING")  {
                let image = UIImage(named: "insurance_trans")?.withRenderingMode(.alwaysOriginal)
                icon.image = image
            } else if record.comments.contains(find: "INSURANCE")  {
                let image = UIImage(named: "insurance_trans")?.withRenderingMode(.alwaysOriginal)
                icon.image = image
            } else if record.email == "" {
                let image = UIImage(named: "cash_back_trans")?.withRenderingMode(.alwaysOriginal)
                icon.image = image
            } else if record.comments.contains(find: "SEND MONEY TO BANK:") {
                let image = UIImage(named: "send_money_bank_trans")?.withRenderingMode(.alwaysOriginal)
                icon.image = image
            } else if record.comments.contains(find: "DTH") {
                let image = UIImage(named: "dth_trans")?.withRenderingMode(.alwaysOriginal)
                icon.image = image
            }  else if record.comments.contains(find: "ProjectId") {
                let image = UIImage(named: "send_money_bank_trans")?.withRenderingMode(.alwaysOriginal)
                icon.image = image
            }  else if record.comments.contains(find: "#Tax Code") {
                let image = UIImage(named: "tax_Logo")?.withRenderingMode(.alwaysOriginal)
                icon.image = image
            } else if record.comments.contains(find: "Bonus Point Exchange Payment") {
                let image = UIImage(named: "tr_earn_point")?.withRenderingMode(.alwaysOriginal)
                icon.image = image
            } else {
                if record.acctranstype == "Cr" {
                    let image = UIImage(named: "recieved_money_trans")?.withRenderingMode(.alwaysOriginal)
                    icon.image = image
                } else {
                    
                    if record.transtype == "PAYWITHOUT ID" {
                        let image = UIImage(named: "hide_num")?.withRenderingMode(.alwaysOriginal)
                        icon.image = image
                    }
                    else {
                        let image = UIImage(named: "pay_trans")?.withRenderingMode(.alwaysOriginal)
                        icon.image = image
                    }
                }
            }
        }
    }
}
