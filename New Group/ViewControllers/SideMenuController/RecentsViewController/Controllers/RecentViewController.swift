//
//  RecentViewController.swift
//  OK
//
//  Created by Admin on 12/5/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Contacts

struct DataModel {
    
    var name  = ""
    var email  = ""
    var amount  = ""
    var image : AnyObject?
    var dateAndTime = ""
    var transtype = ""
    var comments = ""
    var acctranstype = ""
}

struct AnotherRecord: XMLIndexerDeserializable{
    
    var amount = ""
    var destination = ""
    var transid = ""
    var transtype = ""
    var responsects = ""
    var operatorName = ""
    var walletbalance = ""
    var fee = ""
    var comments = ""
    var acctranstype = ""
    var FEE = ""
    
    static func deserialize(_ node: XMLIndexer) throws -> AnotherRecord{
        
        return try AnotherRecord(
        
            amount: node["amount"].value(),
            destination: node["destination"].value(),
            transid: node["transid"].value(),
            transtype: node["transtype"].value(),
            responsects: node["responsects"].value(),
            operatorName: node["operator"].value(),
            walletbalance: node["walletbalance"].value(),
            fee: node["fee"].value(),
            comments: node["comments"].value(),
            acctranstype: node["acctranstype"].value(),
            FEE: node["FEE"].value()
            
        )
    }
}


struct Record: XMLIndexerDeserializable {
    
    var amount : String?
    var destination : String?
    var transid : String?
    var transtype : String?
    var responsects : String?
    var operatorNumber : String?
    var walletbalance : String?
    var fee : String?
    var comments : String?
    var acctranstype : String?
    var FEE: String?
    
    //Contacts store
    public static var contactStore  = CNContactStore()
    static func deserialize(_ node: XMLIndexer) throws -> Record {
        
        return try Record(
            
            amount: node["amount"].value(),
            destination: node["destination"].value(),
            transid: node["transid"].value(),
            transtype: node["transtype"].value(),
            responsects: node["responsects"].value(),
            operatorNumber: node["operator"].value(),
            walletbalance: node["walletbalance"].value(),
            fee: node["fee"].value(),
            comments: node["comments"].value(),
            acctranstype: node["acctranstype"].value(),
            FEE: node["FEE"].value()
            
        )
    }
}

protocol MoveToDashboardFromTax {
    func moveToDashboard()
}

class RecentViewController: OKBaseController {
    
    var DataArr = [DataModel]()
    var confirmDict = Dictionary<String, Any>()
    var NofItem = Int()
    var filteredContacts = [CNContact]()
    var rec: [Record]!
    var Anotherec: [AnotherRecord]!
    let notificationCenter = NotificationCenter.default
    var delegate:MoveToDashboardFromTax?
    fileprivate let cellId = "RCustomCell"
    @IBOutlet weak var lbl: UILabel!{
        didSet{
            lbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var Tbl: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationController?.navigationBar.topItem?.title = ""

        self.lbl.isHidden = true
        
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "Dashboard", delegate: self as BioMetricLoginDelegate)
        } else {
            self.RecentApiCall()
        }
        
        //this notification will work when coming from tax
        NotificationCenter.default.addObserver(self, selector: #selector(self.moveToDashboard), name: NSNotification.Name(rawValue: "MoveToDashboardFromTax"), object: nil)
        
        //this notification will work when coming from tax
        NotificationCenter.default.addObserver(self, selector: #selector(self.moveToDashboardFromTopUp), name: NSNotification.Name(rawValue: "MoveToDashboardFromTopUp"), object: nil)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "MoveToDashboardFromTax"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "MoveToDashboardFromTopUp"), object: nil)
    }
    
    @objc func moveToDashboard() {
        self.dismiss(animated: true, completion: {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MoveToDashboardFromRecent"), object: nil)
        })
    }
    
    @objc func moveToDashboardFromTopUp() {
        UserDefaults.standard.setValue(false, forKey: "isComingfromRecentTopUp")
        self.dismiss(animated: false, completion: {
            self.dismiss(animated: false, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MoveToDashboardFromRecent"), object: nil)
            })
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
        
        /* If view controller is a direct child of UINavigationController */
       // self.title = "Recent Transactions".localized
        /* If view controller's parent is a direct child of UINavigationController e.g. a child of embedded tab view controller */
        //self.parent?.title = "Recent Transactions".localized
        self.navigationItem.title = "Recent Transactions".localized
        notificationCenter.addObserver(self,selector: #selector(SessionAlert), name: .appTimeout, object: nil)
        
        if DataArr.count > 0 {
            progressViewObj.removeProgressView()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notificationCenter.removeObserver(self, name: .appTimeout, object: nil)
    }
    
    @objc func SessionAlert()  {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "", body: "Application Session expired login again".localized, img:  #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){
                if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self as BioMetricLoginDelegate)}
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func viewLayouting(onView v : UIView) {
        
//        v.layer.masksToBounds = true
//        v.layer.cornerRadius = v.frame.width / 2
        //v.layer.cornerRadius = v.frame.size.height / 2
        // border
        //v.layer.borderColor = UIColor.init(hex: "F3C632").cgColor
        //v.layer.borderWidth = 1.5
        // drop shadow
        // v.layer.shadowColor = UIColor.lightGray.cgColor
        //v.layer.shadowOpacity = 0.8
        //v.layer.shadowRadius = 2.0
        //v.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
        
    }
}

extension RecentViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! RCustomCell
        let obj = DataArr[indexPath.row]
        self.viewLayouting(onView: cell.imageView!)
        // println_debug(obj)
        cell.wrapData(object: obj)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataArr.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let cell = tableView.cellForRow(at: indexPath) as? RCustomCell {
            let object  =  DataArr[indexPath.row]
            if !(cell.lblSecure.text?.contains(find: "XXXXXXX"))! {
                self.switchtoViewController(index: indexPath.row)
            } else if object.comments.contains(find: "ProjectId") {
                self.switchtoViewController(index: indexPath.row)
            } else if object.comments.contains(find: "#Tax Code") {
                self.switchtoViewController(index: indexPath.row)
            }
        }
    }
}

extension RecentViewController : WebServiceResponseDelegate{
    
    func webResponse(withJson json: AnyObject, screen: String) {
        self.generateAuthCodeParsing(data: json)
    }
    
    
    func generateAuthCodeParsing(data: AnyObject) {
        
        let responseString = String(data: data as! Data, encoding: .utf8)
        let xml            = SWXMLHash.parse(responseString!)
        println_debug(xml)
        
        self.enumerate(indexer: xml)
        
        println_debug(self.confirmDict)
        
        if self.confirmDict.keys.contains("resultdescription"){
            
            if self.confirmDict["resultdescription"] as! String == "Records Not Found" || self.confirmDict["resultdescription"] as! String == "Records Not found" || self.confirmDict["resultdescription"] as! String == "Transaction Successful" || self.confirmDict["resultdescription"] as! String == "Invalid Secure Token" || self.confirmDict["resultdescription"] as! String == "Secure Token Expire" || self.confirmDict["resultdescription"] as! String == "Invalid Request (Schema Validation Failed)" {
                
                if self.confirmDict["resultdescription"] as! String == "Transaction Successful"{
                    
                    do {
                        
                        rec = try xml["estel"]["response"]["records"]["record"].value()
                        println_debug(rec)
                        
                        //                        self.loadData(arr: rec)
                        self.makeDataArray(arr: rec)
                        
                    } catch {
                        println_debug(error)
                    }
                }
                
                DispatchQueue.main.async{
                    
                    if self.confirmDict["resultdescription"] as? String == "Invalid Secure Token" || self.confirmDict["resultdescription"] as! String == "Secure Token Expire"{
                        let msg  = self.confirmDict["resultdescription"] as? String
                        self.customAlert(msg: msg!.localized, title: "")
                    }
                    self.removeProgressView()
                    self.Tbl.reloadData()
                }
            }
        } else {
            DispatchQueue.main.async{
                self.removeProgressView()
            }
        }
    }
}

extension RecentViewController {
    
    
    func customAlert(msg:String, title:String) {
        
        alertViewObj.wrapAlert(title: "", body: msg.localized, img: #imageLiteral(resourceName: "recents"))
        alertViewObj.addAction(title: "Done".localized, style: .target, action:{})
        alertViewObj.showAlert(controller: self)
        
    }

    func switchtoViewController(index : Int){
        
        let object  =  DataArr[index]
        
        if object.comments.hasPrefix("#OK-00-"){
            let delimiter = "-"
            let newstr = object.comments
            var token = newstr.components(separatedBy: delimiter)
            let value = self.returnCorespondentName(value: "\(token[3])")
            if token[6].hasPrefix("Overseas"){
                let cDetails = identifyCountry(withPhoneNumber: token[7])
                println_debug(cDetails.countryFlag)
                if isOverseasShow {
                    self.overseasNavigation(countryObject: (code: token[7], name:cDetails.countryFlag), userObject: (phone: "(\(token[7]))\(token[3])", name: value["name"] as? String ?? "Unknown"))
                }
                return
            }
            token[3].remove(at: token[3].startIndex)
            
            let mob = "0095\(token[3])"
            
            if mob == UserModel.shared.mobileNo{
                self.navigateToMobileNumber()
                return
            }
            
            let vc = UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "TopupNavigation"))
            if let navigation = vc as? UINavigationController {
                for vc in navigation.viewControllers {
                    if let tHome = vc as? TopupHomeViewController {
                        //tHome.isComingFromRecent = true
                        tHome.shortcutMenu = 0
                        tHome.favoriteTouple = (mob,value["name"] as? String ?? "Unknown")
                    }
                }
              //  navigation.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
             //   navigation.pushViewController(vc, animated: true)
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
        }
            
            

            //vc.modalPresentationStyle = .fullScreen
           // self.present(vc, animated: true, completion: nil)
        }
        else if object.comments.hasPrefix("OOK-00-"){
            let delimiter = "-"
            let newstr = object.comments
            var token = newstr.components(separatedBy: delimiter)
            let value = self.returnCorespondentName(value: token[3])
            token[3].remove(at: token[3].startIndex)
            let mob = "0095\(token[3])"
            let vc = UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "TopupNavigation"))
            if let navigation = vc as? UINavigationController {
                for vc in navigation.viewControllers {
                    if let tHome = vc as? TopupHomeViewController {
                        tHome.favoriteTouple = (mob,value["name"] as? String ?? "Unknown")
                        //tHome.isComingFromRecent = true
                    }
                }
//                navigation.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
//                navigation.pushViewController(vc, animated: true)
                
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
            
            
            
            
            
            
          //  vc.modalPresentationStyle = .fullScreen
            
           // self.present(vc, animated: true, completion: nil)
        }
        else if object.comments.contains(find: "ProjectId") {
            
        var homeVC: BankAccountMobileNumberViewController?
        homeVC = UIStoryboard(name: "UnionPay", bundle: nil).instantiateViewController(withIdentifier: "BankAccountMobileNumberViewController") as? BankAccountMobileNumberViewController
          println_debug(object.comments)
            var mobileNo = ""
            let value = object.comments
                       if value.contains(find: "ProjectId"){
                           let newValue = value.components(separatedBy: " Wallet Number :")
                        if newValue.count > 2 {
                           if newValue.indices.contains(1){
                               let name = newValue[2].components(separatedBy: "ProjectId")
                               if name.indices.contains(0){
                                   mobileNo = name[0]
                               }
                           }
                        } else {
                            //OK Dollar Wallet To MAB , OK Dollar Wallet Number :  00959790681381 Account Number :09976669357ProjectIdMAB767898KUEOJBU_IEHK787UOEJDHF
                            let arrVal = value.components(separatedBy: "Account Number :")
                                if arrVal.indices.contains(1){
                                    let name = arrVal[1].components(separatedBy: "ProjectId")
                                    if name.indices.contains(0){
                                        mobileNo = name[0]
                                }
                            }
                        }
                       }
                       if value.contains(find: "UserType"){
                           let newValue = value.components(separatedBy: "ProjectId")
                           if newValue.indices.contains(1){
                               let name = newValue[1].components(separatedBy: "UserType")
                               if name.indices.contains(0){
                                   homeVC?.projectid  = name[0]
                               }
                           }
                       } else {
                           homeVC?.projectid = object.comments.components(separatedBy: "ProjectId").last ?? ""
                       }
            homeVC?.strMobileNo = mobileNo
            homeVC?.strStatus = "RecentTx"
            homeVC?.isComingFromRecent = true
            navigationController?.pushViewController(homeVC!, animated: true)

            /*
            var homeVC: BankAccountViewController?
            homeVC = UIStoryboard(name: "UnionPay", bundle: nil).instantiateViewController(withIdentifier: "BankAccountViewController") as? BankAccountViewController
            //homeVC?.projectid = object.comments.components(separatedBy: "ProjectId").last ?? ""
            
            homeVC?.strStatus = "RecentTx"
            var mobileNo = ""
            //"ProjectId6E98B9E69B3B769FD7F2EC7DD3BEB6F3E540CB961A5EEDBDE21004D815CDEA19595705D2B22371BDFF28F5534B62BA4BUserType2"

            let value = object.comments
            if value.contains(find: "ProjectId"){
                let newValue = value.components(separatedBy: " Wallet Number :")
                if newValue.indices.contains(1){
                    let name = newValue[2].components(separatedBy: "ProjectId")
                    if name.indices.contains(0){
                        mobileNo = name[0]
                    }
                }
            }
            if value.contains(find: "UserType"){
                let newValue = value.components(separatedBy: "ProjectId")
                if newValue.indices.contains(1){
                    let name = newValue[1].components(separatedBy: "UserType")
                    if name.indices.contains(0){
                        homeVC?.projectid  = name[0]
                    }
                }
            } else {
                homeVC?.projectid = object.comments.components(separatedBy: "ProjectId").last ?? ""
            }
            homeVC?.strMobileNo = mobileNo
            homeVC?.modalPresentationStyle = .fullScreen
            self.present(homeVC!, animated: true, completion: nil)
             */
        } else if object.comments.contains(find: "#Tax Code") {
            let viewController = UIStoryboard(name: "NearByModule", bundle: Bundle.main).instantiateViewController(withIdentifier: NearByConstant.kNearByController) as? NearByController
            viewController?.isComingFromTaxPayment = true
            viewController?.isComingFromRecent = true
            navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
            navigationController?.pushViewController(viewController!, animated: true)
        }
        else {
            //let value = self.returnCorespondentName(value: object.email)
            var payto = "payto"
            if object.comments.contains(find: "-cashin") {
                payto = "cashin"
            } else if object.comments.contains(find: "-cashout") {
                payto = "cashout"
            }
            
            var nameReciever = "Unknown"
            if let com = object.comments as? String {
                      let recieverMerchantName = com.components(separatedBy: "##")
                      if recieverMerchantName.count >= 2 {
                          let recieverName = recieverMerchantName[1]
                          if recieverName == "Unregistered User" {
                              nameReciever =  object.name.localized
                          } else {
                              nameReciever = recieverName
                          }
                      }
                  }
            
            if object.comments.contains(find: "Bonus Point Exchange Payment") {
                println_debug("Bonus Point Exchange Payment")
            } else {
                self.navigateToPayto(phone: object.email, amount: "", name: nameReciever, type: payto)
            }
        }
    }
    
    func navigateToMobileNumber() {
        if (UserLogin.shared.walletBal as NSString).floatValue < 100.00 {
            alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
            alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                TBHomeNavigations.main.navigate(.addWithdraw)
            })
            alertViewObj.showAlert(controller: self)
        } else {
//            let vc = UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "myNumberNavigation"))
//            UserDefaults.standard.setValue(true, forKey: "isComingfromRecentTopUp")
//            vc.modalPresentationStyle = .fullScreen
//            self.present(vc, animated: true, completion: nil)
            
            let vc = UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "myNumberNavigation"))
            if let navigation = vc as? UINavigationController {
                for vc in navigation.viewControllers {
                    if let tHome = vc as? TopupMyMainViewController {
                        tHome.isComingFromRecent = true
                    }
                }
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
        }
            
            
        }
    }
    
    func enumerate(indexer: XMLIndexer){
        
        for child in indexer.children{
            self.confirmDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
        
    }
    
    
    func RecentApiCall(){
        if appDelegate.checkNetworkAvail(){
            self.showProgressView()
            let web = WebApiClass()
            web.delegate = self
            let urlS = String.init(format: Url.Transaction_History_details_API,UserModel.shared.mobileNo,ok_password!,UserLogin.shared.token,0)
            let url =   getUrl(urlStr: urlS, serverType: .serverEstel)
            let param = [String:String]() as AnyObject
            web.genericClass(url: url, param: param, httpMethod: "POST", mScreen: "Recent")
        }
        else{
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
        }
    }
    
    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func makeDataArray(arr:[Record]){
        var LocalRec = [Record]()
        
        for recortValue in arr {
            println_debug(recortValue)
            if recortValue.acctranstype == "Dr"  {
                if recortValue.transtype != "PAYTOKICKBACK" && !((recortValue.comments ?? "").hasPrefix("Payment for SolarHome")) && !((recortValue.comments ?? "").hasPrefix("#point-redeempoints"))
                {
                    if (self.nullToNil(value: recortValue as AnyObject) != nil){
                        LocalRec.append(recortValue)
                    }
                }
            }
        }
        self.loadData(arr: LocalRec)
    }
    
    func loadData(arr:[Record]){
        
        DispatchQueue.main.async {
            
            if arr.count == 0{
                self.lbl.isHidden = false
                self.lbl.text = "No Record Found".localized
            }
            else{
                
                self.lbl.isHidden = true
                for recortValue in arr {
                    println_debug(recortValue)
                    
                    let mobileNumber = self.GetMobileNumberFromComent(coment: recortValue.comments ?? "")
                    var value : [String : AnyObject] = [String : AnyObject]()
                    if mobileNumber == "+950000000000" {
                        value = self.returnCorespondentName(value: recortValue.destination ?? "")
                    }
                    else {
                         value = self.returnCorespondentName(value: mobileNumber)
                    }
                    var obj = DataModel()
                    obj.transtype = recortValue.transtype ?? ""
                    obj.name = value["name"] as? String ?? "Unknown"
                    obj.email = recortValue.destination ?? ""
                    if recortValue.operatorNumber?.count ?? 0 > 0 {
                        obj.email = recortValue.operatorNumber ?? ""
                    }
                    obj.amount =  self.getNumberFormat(recortValue.amount ?? "0.0") + " MMK"
                    obj.dateAndTime = recortValue.responsects ?? ""
                    obj.comments = recortValue.comments ?? ""
                    obj.acctranstype = recortValue.acctranstype ?? ""
                    if (self.nullToNil(value: value["pic"]) != nil) {obj.image = UIImage(data: (value["pic"] as! NSData) as Data)}else{obj.image = nil}
                    println_debug(obj)
                    self.DataArr.append(obj)
                    
                    /*
                     if recortValue.acctranstype == "Dr"  {
                     let value = self.returnCorespondentName(value: recortValue.destination)
                     var obj = DataModel()
                     println_debug(value["name"] as? String ?? "")
                     if recortValue.transtype != "PAYTOKICKBACK" && !recortValue.comments.hasPrefix("Payment for SolarHome") {
                     obj.transtype = recortValue.transtype
                     obj.name = value["name"] as? String ?? "Unknown"
                     obj.email = recortValue.destination
                     obj.amount =  getNumberFormat(recortValue.amount) + " MMK"
                     obj.dateAndTime = recortValue.responsects
                     obj.comments = recortValue.comments
                     if (nullToNil(value: value["pic"]) != nil) {obj.image = UIImage(data: (value["pic"] as! NSData) as Data)}else{obj.image = nil}
                     println_debug(obj)
                     DataArr.append(obj)
                     }
                     }
                     */
                }
            }
            //        println_debug(DataArr)
        }
    }
}

extension RecentViewController: BioMetricLoginDelegate {
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if isSuccessful {
            self.RecentApiCall()
            println_debug("Authorised")
        }
        else{
            
            if UserLogin.shared.loginSessionExpired {OKPayment.main.authenticate(screenName: "Dashboard", delegate: self)}
        }
    }
}

extension String {
     func isEqualToString(find: String) -> Bool {
        return String(format: self) == find
    }
}
