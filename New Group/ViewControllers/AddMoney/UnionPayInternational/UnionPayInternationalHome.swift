//
//  UnionPayInternationalHome.swift
//  OK
//
//  Created by Vinod Kumar on 27/07/20.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class UnionPayInternationalHome: OKBaseController {

    @IBOutlet weak var tvUnionPay: UITableView!
    var iconList = ["payment_complete","payment_complete","payment_complete","payment_complete"]
    var titleList = ["Service Bill","Loan Repayment","Insurance Premium Payment","Union Pay International Payment"]
    override func viewDidLoad() {
        super.viewDidLoad()
        tvUnionPay.tableFooterView = UIView()
        self.configureBackButton(withTitle: "Bill Payment")
    }

}

extension UnionPayInternationalHome: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UnionPayIntHomeCell", for: indexPath) as! UnionPayIntHomeCell
        cell.selectionStyle = .none
        cell.imgIcon.image = UIImage(named: iconList[indexPath.row])
        cell.lblTitle.text = titleList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cardlessCashStory = UIStoryboard(name: "CardLessCashStoryBoard", bundle: nil)
        guard let cardlessCashVC = cardlessCashStory.instantiateViewController(withIdentifier: "UPayIntCheckDetails") as? UPayIntCheckDetails else { return }
        self.navigationController?.pushViewController(cardlessCashVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return createRequiredFieldLabel(headerTitle: "Select Payment Cetegories", reqTitle: "")
    }
    
    
    private func createRequiredFieldLabel(headerTitle:String, reqTitle: String) ->UIView? {
        let view1 = UIView(frame: CGRect(x: 15, y: 0, width:self.view.frame.width - 10, height: 35))
        view1.backgroundColor = hexStringToUIColor(hex: "#E0E0E0")
        let lblHeader = UILabel(frame: CGRect(x: 15, y: 0, width: (sizeOfString(myString: headerTitle)?.width)!, height:35))
        lblHeader.text = headerTitle
        lblHeader.textAlignment = .left
        lblHeader.font = UIFont(name: appFont, size: 14)
        lblHeader.backgroundColor = UIColor.clear
        lblHeader.textColor = UIColor.black
        
        let reqLabel = UILabel(frame: CGRect(x: self.view.frame.width - (sizeOfString(myString: reqTitle)?.width)! - 10, y: 0, width: (sizeOfString(myString: reqTitle)?.width)!, height:35))
        reqLabel.text = reqTitle
        reqLabel.textAlignment = .right
        reqLabel.font = UIFont(name: appFont, size: 14)
        reqLabel.backgroundColor = UIColor.clear
        reqLabel.textColor = UIColor.red
        view1.addSubview(lblHeader)
       // view1.addSubview(reqLabel)
        return view1
    }
    
    private func sizeOfString(myString : String) ->CGSize? {
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: appFont, size: 16) ?? UIFont.systemFont(ofSize: 16.0) ])
        return stringSize
    }
}



class UnionPayIntHomeCell: UITableViewCell {
    
@IBOutlet weak var imgIcon: UIImageView!
@IBOutlet weak var lblTitle: UILabel!
    
    
}
