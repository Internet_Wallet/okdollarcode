//
//  OtherOkAccountCell.swift
//  OK
//
//  Created by PC on 1/13/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class OtherOkAccountCell: UITableViewCell {

    @IBOutlet weak var btnOtherAccount: UIButton!{
        didSet{
            btnOtherAccount.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
