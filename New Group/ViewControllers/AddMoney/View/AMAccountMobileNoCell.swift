//
//  AMAccountMobileNoCell.swift
//  OK
//
//  Created by PC on 1/15/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class AMAccountMobileNoCell: UITableViewCell {

    @IBOutlet weak var btnContact: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var txtAccountMobNo: RestrictedCursorMovementWithFloat!{
        didSet{
            //txtAccountMobNo.font = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            txtAccountMobNo.placeholderFont  = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            txtAccountMobNo.titleFont  = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
