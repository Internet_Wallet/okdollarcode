//
//  SelectAccountHeaderCell.swift
//  OK
//
//  Created by PC on 1/13/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class SelectAccountHeaderCell: UITableViewCell {

    @IBOutlet weak var btnSelectAccount: UIButton!{
        didSet {
            btnSelectAccount.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgArrowIcon: UIImageView!
    @IBOutlet weak var imgUserIcon: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setArrow(_ collapsed: Bool) {
        
        if collapsed {
            
            self.imgArrowIcon.image = UIImage(named: "downArrow")
        }else{
            
            self.imgArrowIcon.image = UIImage(named: "upArrow")
        }
    }
    
}
