//
//  AMConfirmAccountMobNoCell.swift
//  OK
//
//  Created by PC on 1/15/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class AMConfirmAccountMobNoCell: UITableViewCell {

    @IBOutlet weak var btnConfirmCancel: UIButton!
    @IBOutlet weak var txtConfirmMobileNo: RestrictedCursorMovementWithFloat!{
        didSet{
            //txtConfirmMobileNo.font =  UIFont(name: appFont, size: appFontSize)
            txtConfirmMobileNo.titleFont = UIFont(name: appFont, size: 12) ?? UIFont.systemFont(ofSize: 12)
            txtConfirmMobileNo.placeholderFont = UIFont(name: appFont, size: 12) ?? UIFont.systemFont(ofSize: 12)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
