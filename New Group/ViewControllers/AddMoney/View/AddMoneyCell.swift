//
//  AddMoneyCell.swift
//  OK
//
//  Created by Badru on 7/29/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit

class AddMoneyCell: UITableViewCell {
    
    
    @IBOutlet weak var lblCardName: UILabel!{
        didSet{
            lblCardName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var lblFees: UILabel!
    @IBOutlet weak var lblCommingSoon: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func customCellUpdate(item : Item , indexPath: IndexPath) {
        
        lblCardName.text = item.cardName
        if UserModel.shared.agentType == .user {
            if(indexPath.section == 4 || indexPath.section == 5){//PayTo AWD
                if(item.cardName == "MPU") {
                    lblFees.text = item.fees.replacingOccurrences(of: ".00", with: "").replacingOccurrences(of: ".0", with: "") + " MMK"
                } else {
                    lblFees.text = item.fees.replacingOccurrences(of: ".00", with: "").replacingOccurrences(of: ".0", with: "") + " %"
                }
            }
            else if(indexPath.section == 6){//PayTo Online/Net Banking
                lblFees.text = item.fees.replacingOccurrences(of: ".00", with: "").replacingOccurrences(of: ".0", with: "") + " %"
            }
        } else {
            if(indexPath.section == 5 || indexPath.section == 6){//PayTo AWD
                if(item.cardName == "MPU") {
                    lblFees.text = item.fees.replacingOccurrences(of: ".00", with: "").replacingOccurrences(of: ".0", with: "") + " MMK"
                } else {
                    lblFees.text = item.fees.replacingOccurrences(of: ".00", with: "").replacingOccurrences(of: ".0", with: "") + " %"
                }
            }
            else if(indexPath.section == 7){//PayTo Online/Net Banking
                lblFees.text = item.fees.replacingOccurrences(of: ".00", with: "").replacingOccurrences(of: ".0", with: "") + " %"
            }
        }
    }
}
