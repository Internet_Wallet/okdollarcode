//
//  MyOkAccountCell.swift
//  OK
//
//  Created by PC on 1/13/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class MyOkAccountCell: UITableViewCell {

    @IBOutlet weak var lblMyNo: UILabel!
    @IBOutlet weak var btnMyAccount: UIButton!{
        didSet{
            btnMyAccount.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
