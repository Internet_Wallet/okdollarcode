//
//  AddMoneyHeaderCell.swift
//  OK
//
//  Created by Badru on 7/29/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit

class AddMoneyHeaderCell: UITableViewCell {
    
    @IBOutlet weak var iv: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!{
        didSet {
            lblTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var btnHeaderSelection: UIButton!
    @IBOutlet weak var ivArrow: UIImageView!
    
    @IBOutlet weak var ivSection: UIImageView!
    var section: Int = 0
    var collapsed = Bool()

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    
    func setCollapsed(_ collapsed: Bool) {
       
        self.collapsed = collapsed
    }
    
    func setArrow(_ collapsed: Bool) {
        
        if collapsed {
            self.ivArrow.image = UIImage(named: "downArrow")
        }else{
            self.ivArrow.image = UIImage(named: "upArrow")
        }
    }

}
