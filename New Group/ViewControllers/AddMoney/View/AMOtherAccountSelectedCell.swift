//
//  AMOtherAccountSelectedCell.swift
//  OK
//
//  Created by PC on 1/15/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class AMOtherAccountSelectedCell: UITableViewCell {

    @IBOutlet weak var txtAmount: SkyFloatingLabelTextField!{
        didSet {            
            txtAmount.titleFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            txtAmount.placeholderFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
        }
    }
    @IBOutlet weak var txtAccountName: SkyFloatingLabelTextField!{
        didSet {
            //txtAccountName.font = UIFont(name: "Zawgyi-One", size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            txtAccountName.titleFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            txtAccountName.placeholderFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
        }
    }
    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var btnOtherAmountCancel: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
