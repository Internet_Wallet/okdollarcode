//
//  AMRemarkCell.swift
//  OK
//
//  Created by PC on 1/15/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class AMRemarkCell: UITableViewCell {

    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var viewPercentage: UIView!
    @IBOutlet weak var lblPercentage: UILabel!
    @IBOutlet weak var txtTotalAmount: SkyFloatingLabelTextField!{
        didSet {
            txtTotalAmount.titleFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            txtTotalAmount.placeholderFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
        }
    }
    @IBOutlet weak var txtFees: SkyFloatingLabelTextField!{
        didSet {
            txtFees.titleFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            txtFees.placeholderFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
        }
    }
    @IBOutlet weak var txtBankCharges: SkyFloatingLabelTextField!{
        didSet {
            txtBankCharges.titleFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            txtBankCharges.placeholderFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
        }
    }
    @IBOutlet weak var txtRemark: SkyFloatingLabelTextField!{
        didSet {
            txtRemark.font = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            txtRemark.titleFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
            txtRemark.placeholderFont = UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
