//
//  AddWithdrawMoneyVC.swift
//  OK
//
//  Created by PC on 1/12/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

struct Item {
    var cardName: String
    var fees: String
    
    init(cardName: String, fees: String) {
        self.cardName = cardName
        self.fees = fees
    }
}

struct AddMoneySection {
    var sectionName: String
    var items: [Item]
    var collapsed: Bool
    
    init(sectionName: String, items: [Item], collapsed: Bool = false) {
        self.sectionName = sectionName
        self.items = items
        self.collapsed = collapsed
    }
}

class AddWithdrawMoneyVC: OKBaseController, WebServiceResponseDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var btnWalletBalance: UIButton! {
        didSet {
            btnWalletBalance.setImage(UIImage(named: "password_blue"), for: .normal)
            btnWalletBalance.tintColor = UIColor.init(hex: "0321AA")
        }
    }
    @IBOutlet weak var tblOptionsList: UITableView!
    @IBOutlet weak var tblAccountList: UITableView!
    @IBOutlet weak var lblBalance: UILabel!{
        didSet {
            lblBalance.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    var issound:Bool = true
    var amountCheckingTimer: Timer?
    @IBOutlet weak var submitButton: UIButton!{
        didSet {
            submitButton.setTitle("Submit".localized, for: .normal)
            submitButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    @IBOutlet weak var submitbuttonConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var soundView: UIView!{
        didSet{
            self.soundView.isHidden = true
        }
    }
    @IBOutlet weak var soundLabel: UILabel!{
        didSet{
            self.soundLabel.font  = UIFont(name: appFont, size: appFontSize)
            self.soundLabel.text = "Sound On".localized
        }
    }
    @IBOutlet weak var soundImageView: UIImageView!{
        didSet{
             self.soundImageView.image = UIImage(named: "act_speaker")
        }
    }
    
    @IBOutlet weak var soundSwitch: UISwitch!
    
    @IBAction func soundSwitchClick(_ sender: Any) {
        if (sender as AnyObject).isOn {
            self.soundImageView.image = UIImage(named: "act_speaker")
            self.soundLabel.text = "Sound On".localized
            issound = true
         //     self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: soundView)
        } else {
            self.soundImageView.image = UIImage(named: "speaker")
            self.soundLabel.text = "Sound Off".localized
             issound = false
         //     self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: soundView)
        }
        soundView.isHidden = true
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button1)
    }
    var scanDelegate: TBScanQRDelegate?

    var screenFrom = ("PAYTO" , "Cash In")
    let objectSound = BurmeseSoundObject()
    var playSound: Bool = true
    
    @IBOutlet weak var lblWalletBalance: UILabel! {
        didSet {
            self.lblWalletBalance.font = UIFont(name: appFont, size: appFontSize)
            self.lblWalletBalance.text = self.lblWalletBalance.text?.localized
        }
    }
    
    @IBOutlet weak var navigationview: UINavigationItem!
    //Manage Contact Suggestion
    var contactSuggessionView   : ContactSuggestionVC?
    var keyboardHeight : CGFloat?
    //Show User Balance
    var isViewBalance:Bool = false
    //Select Account Type
    var isSelectOkAccount:Bool = false
    //My Account Selection
    var isMyAccountSelected:Bool = false
    var isOtherAccountSelected:Bool = false
    //Other Account Selection
    var isContactSelected:Bool = false
    var isConfirmMobileNoShow:Bool = false
    var isOtherAccountShow:Bool = false
    var isRemark:Bool = false
    var sections = [AddMoneySection]()
    var bankResponseArr:[Any] = []
    var selectedDict = [String:String]()
    var selectedPaymentOption = ""
    var strEnterAmountValue = ""
    var strEnterRemarktValue = ""
     var strEnterMobileValue = ""
    var emptyArray = [String.Element]()
    //Manage Model Data
    let addMoneyModel =  AddMoneyModel()
    //Validation for Mobile numbers prefix
    let validObj  = PayToValidations()
    
    var lastCountIndex = 0
    
     let button1 = UIButton.init(type: .custom)
    
    //Manage JCB & Union Pay
    var isJCB:Bool = false
    var isUnionPay:Bool = false

    //Manage Navigation
    var selectedRowIndex = 0


    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        println_debug("AddWithdrawMoneyVC")
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "tabBarBack"), for: UIControl.State.normal)
        button.addTarget(self, action: #selector(dismissBackAction), for: .touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 50, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
      
        button1.setImage(#imageLiteral(resourceName: "menu_white_payto"), for: UIControl.State.normal)
        button1.frame = CGRect.init(x: 0, y: 0, width: 50, height: 30)
        
        button1.addTargetClosure { [weak self](button) in
            self?.showSoundOnOffView()
        }
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button1)
        
     //   soundView.frame = CGRect.init(x: screenWidth-10, y: 0, width: 185, height: 40)
        
        self.navigationItem.title = (screenFrom.1 == "Cash Out") ? "Cash Out".localized : "Cash In".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
        self.loadContactSuggessionView()
        tblAccountList.tableFooterView = UIView(frame: CGRect.zero)
        tblAccountList.sectionFooterHeight = 0.0
        //Manage Keyboard
        IQKeyboardManager.sharedManager().enable            = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        lblBalance.text = "xxxxxx"
        tblOptionsList.isHidden = false
        tblAccountList.isHidden = true
        submitbuttonConstraint.constant = 0
        
        soundView.isHidden = true
        
//        let reqCashStr = (screenFrom.1 == "Cash Out") ? "Request Cash Out_without_newline" : "Request Cash In_without_newline"
        
        if screenFrom.1 == "Cash Out" {
            sections = [
                
                AddMoneySection(sectionName:"Find OK$ Agent".localized, items:[
                Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                
                AddMoneySection(sectionName:"Paid Commission Agent Cash Out".localized, items:[
                    Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                
                AddMoneySection(sectionName:"Free Commission Agent Cash Out".localized, items:[
                    Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                
                AddMoneySection(sectionName:"Send Money To Bank".localized, items:[
                    Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                
                AddMoneySection(sectionName:"OK$ OfficeAW".localized, items:[
                    Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                
                AddMoneySection(sectionName:"Myanmar Post".localized, items:[
                    Item(cardName:"post".localized, fees:"Comming Soon".localized)], collapsed:true),
                
                AddMoneySection(sectionName:"Bank".localized, items:[
                    Item(cardName:"bank".localized, fees:"Comming Soon".localized)], collapsed:true),
                
                AddMoneySection(sectionName:"Mobile Wallet | Mobile Pay and Bank".localized, items:[
                                 Item(cardName:"Mobile Wallet | Mobile Pay and Bank".localized, fees:"Comming Soon".localized)], collapsed:true)
 
//                ,AddMoneySection(sectionName:"UPI".localized, items:[
//                                               Item(cardName:"UPI".localized, fees:"Comming Soon".localized)], collapsed:true)
            ]
        } else {

            if UserModel.shared.agentType == .user {
                sections = [
                    
                    AddMoneySection(sectionName:"Find OK$ Agent".localized, items:[
                        Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                    AddMoneySection(sectionName:"Paid Commission Agent Cash In".localized, items:[
                        Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                    AddMoneySection(sectionName:"Free Commission Agent Cash In".localized, items:[
                        Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                    AddMoneySection(sectionName:"123 Payment Code".localized, items:[
                        Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                    AddMoneySection(sectionName:"Credit Card".localized, items:[], collapsed:true),
                    
                    AddMoneySection(sectionName:"Debit Card".localized, items:[], collapsed:true),
                    
                    AddMoneySection(sectionName:"Online/Net Banking".localized, items:[], collapsed:true),
                    
                    AddMoneySection(sectionName:"Bank Counter Deposit Add".localized, items:[
                        Item(cardName:"KBZ Bank".localized, fees:"200 MMK (Fees)")], collapsed:true),
                    
                    
                    AddMoneySection(sectionName:"OK$ OfficeAW".localized, items:[
                        Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                    AddMoneySection(sectionName:"Myanmar Post".localized, items:[
                        Item(cardName:"post".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                    AddMoneySection(sectionName:"Bank".localized, items:[
                        Item(cardName:"bank".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                    AddMoneySection(sectionName:"Other Mobile Money Wallet".localized, items:[
                        Item(cardName:"Wave Money".localized, fees:"Comming Soon".localized),
                        Item(cardName:"True Money".localized, fees:"Comming Soon".localized)], collapsed:true)
                    
                ]
            } else {
                sections = [
                    
                    AddMoneySection(sectionName:"Find OK$ Agent".localized, items:[
                        Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                    AddMoneySection(sectionName:"Agent Transfer Digital Money".localized, items:[
                        Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                    AddMoneySection(sectionName:"Paid Commission Agent Cash In".localized, items:[
                        Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                    AddMoneySection(sectionName:"Free Commission Agent Cash In".localized, items:[
                        Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                    AddMoneySection(sectionName:"123 Payment Code".localized, items:[
                        Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                    AddMoneySection(sectionName:"Credit Card".localized, items:[], collapsed:true),
                    
                    AddMoneySection(sectionName:"Debit Card".localized, items:[], collapsed:true),
                    
                    AddMoneySection(sectionName:"Online/Net Banking".localized, items:[], collapsed:true),
                    
                    AddMoneySection(sectionName:"Bank Counter Deposit Add".localized, items:[
                        Item(cardName:"KBZ Bank".localized, fees:"200 MMK (Fees)")], collapsed:true),
                    
                    AddMoneySection(sectionName:"OK$ OfficeAW".localized, items:[
                        Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                    AddMoneySection(sectionName:"Myanmar Post".localized, items:[
                        Item(cardName:"post".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                    AddMoneySection(sectionName:"Bank".localized, items:[
                        Item(cardName:"bank".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                    AddMoneySection(sectionName:"Other Mobile Money Wallet".localized, items:[
                        Item(cardName:"Wave Money".localized, fees:"Comming Soon".localized),
                        Item(cardName:"True Money".localized, fees:"Comming Soon".localized)], collapsed:true)
                    
                ]
            }
            //Get Access Token For Bank
            self.getAccessToken()
            
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(popCurrentScreen), name: Notification.Name("CICOPopToroot"), object: nil)
    }
    
    @objc func popCurrentScreen() {
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func dismissBackAction() {
        if screenFrom.0 == "DashBoard/Payto" {
            let transition = CATransition()
            transition.duration = 0.3
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromLeft
            self.navigationController?.view.layer.add(transition, forKey: kCATransition)
            self.navigationController?.popViewController(animated: false)
        } else if screenFrom.0 != "PAYTO" {
            if selectedRowIndex < 101 {
                self.soundView.isHidden = true
                self.navigationController?.popViewController(animated: true)
            } else {
                selectedRowIndex = 100
                tblOptionsList.isHidden = false
                tblAccountList.isHidden = true
                tblOptionsList.reloadData()
            }
        } else {
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    
    private func showSoundOnOffView() {
        if soundView.isHidden {
      //      self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: soundView)
            soundView.isHidden = false
            
        } else {
            soundView.isHidden = true
          //   self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button1)
        }
    }
  
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        
        if UserLogin.shared.loginSessionExpired {
            let amountBal = Double(UserLogin.shared.walletBal) ?? 0.00
            if amountBal < 500000.00 {
                lblBalance.attributedText = self.walletBalance().replacing(placeholder: ".00", with: "")
                btnWalletBalance.isHidden = true
            } else {
                btnWalletBalance.isHidden = false
            }
        } else{
            lblBalance.attributedText = self.walletBalance().replacing(placeholder: ".00", with: "")
            btnWalletBalance.isHidden = true
        }
        
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        appDel.floatingButtonControl?.window.isHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    deinit {
        self.amountCheckingTimer?.invalidate()
    }
    
    @objc func searchForKeyword(_ timer: Timer) {
        if issound == true  {
            if let text = timer.userInfo as? String {
                var burmese = text
                burmese = burmese.replacingOccurrences(of: ",", with: "")
                if let ch = burmese.last, ch == "." {
                    
                } else {
                    let _ = objectSound.getBurmeseSoundText(text: burmese)
                }
            }
        }
    }
    
    func getHashValueForWebView() ->String {
        //addMoneyModel.depositorContactNumber
        //self.getConatctNum(addMoneyModel.depositorContactNumber,withCountryCode: "+95")
        var strHash = ""
        let PipeStr = "CustomerName=\(addMoneyModel.receiverName)|OkAccountNumber=\(UserModel.shared.mobileNo)|Amount=\(addMoneyModel.enteredAmount)|BeneficiaryAccountNumber=\(self.getConatctNum(addMoneyModel.depositorContactNumber,withCountryCode: "+95"))|BeneficiaryName=\(UserModel.shared.name)|GatewayConfigurationId=\(selectedDict["GatewayConfigurationId"]!)|PaymentGatewayId=\(selectedDict["PaymentGatewayId"]!)|PaymentGatewayName=\(selectedDict["PaymentGatewayName"]!)|BankCharges=\(selectedDict["BankCharges"]!)|AdminFee=\(addMoneyModel.percentage)|BankChargesType=\(selectedDict["BankChargesType"]!)|AdminFeeType=\(selectedDict["AdminFeeType"]!)|CalculatedAdminFee=\(addMoneyModel.fees)|CalculatedBankCharge=\(addMoneyModel.bankCharge)|TotalAmount=\(addMoneyModel.totalAmount)|Comments=\(addMoneyModel.remarks)"
        println_debug(PipeStr)
        strHash = PipeStr.hmac_SHA1(key: Url.sKey_AddMoneyPayment)
        println_debug(strHash)
        return strHash
    }
    
    //Hide/View Balance
    @IBAction func btnViewBalanceAction(_ sender: Any) {
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "AddWithdrawMoneyViewBalance", delegate: self)
        } else{
            lblBalance.attributedText = self.walletBalance()
            btnWalletBalance.isHidden = true
        }
    }
    
    func getBankResponseDict(PaymentGatewayName:String) -> [String:String] {
        print("bankResponseArr=====\(self.bankResponseArr)")

        for parseDict in self.bankResponseArr {
            if let dict = parseDict as? [String: Any], let gateWayName = dict["PaymentGatewayName"] as? String {
                if PaymentGatewayName == gateWayName {
                    var tempDict = [String:String]()
                    if let value = dict["AdminFee"] as? Int {
                        tempDict["AdminFee"] = String(value)
                    }
                    if let value = dict["AdminFeeType"] as? String {
                        tempDict["AdminFeeType"] = String(value)
                    }
                    if let value = dict["BankCharges"] as? Int {
                        tempDict["BankCharges"] = String(value)
                    }
                    if let value = dict["BankChargesType"] as? String {
                        tempDict["BankChargesType"] = String(value)
                    }
                    if let value = dict["GatewayConfigurationId"] as? String {
                        tempDict["GatewayConfigurationId"] = String(value)
                    }
                    if let value = dict["PaymentGatewayId"] as? String {
                        tempDict["PaymentGatewayId"] = String(value)
                    }
                    if let value = dict["PaymentGatewayName"] as? String {
                        tempDict["PaymentGatewayName"] = String(value)
                    }
                    if let value = dict["MaximumAmount"] as? Int {
                        tempDict["MaximumAmount"] = String(value)
                    }
                    if let value = dict["MinimumAmount"] as? Int {
                        tempDict["MinimumAmount"] = String(value)
                    }
                    return tempDict
                }
            }
        }
        return [String:String]()
    }
 
    //MARK:- Text Field Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if(textField.tag == 60) { //for Total Amount
            let str = (textField.text! + string)
            if str.count <= 13 {
                
                if issound == true
                {
                    if amountCheckingTimer != nil {
                        amountCheckingTimer?.invalidate()
                        amountCheckingTimer = nil
                    }
                    self.amountCheckingTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(searchForKeyword(_:)), userInfo: text, repeats: false)
                }
               
                return true
            }
            return false
        } else if(textField.tag == 62) { //for other user Total Amount
            let str = (textField.text! + string)
            if str.count <= 10 {
                return true
            }
            return false
        } else if(textField.tag == 63) { //for other user Total Amount
            let str = (textField.text! + string)
            if str.count <= 10 {
                return true
            }
            return false
        }  else if(textField.tag == 73) { //for Remarks limit
            let updatedText = (textField.text! + string)
           
            if updatedText == " " || updatedText == "  "{
                return false
            }
            if (updatedText.contains("  ")){
                return false
            }
            
            let containsEmoji: Bool = updatedText.containsEmoji
            if (containsEmoji){
                return false
            }
            
            if updatedText.count <= 50 {
                return true
            }
            return false
        }
        else {
            if(textField.tag == 50) {
               
                if range.location == 0 || range.location == 1 {
                    return false
                }
               
                    println_debug("Mobile number validation")
                    let strTotalLength = (textField.text! + string)
                
                    guard validObj.getNumberRangeValidation(strTotalLength).isRejected == false else {
                        alertViewObj.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: UIImage(named : "AppIcon"))
                        alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                            self.hideContactSuggesionView()
                            self.tblAccountList.reloadData()
                            let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? AMAccountMobileNoCell
                            cell?.txtAccountMobNo.becomeFirstResponder()
                            cell?.btnCancel.isHidden = true
                        })
                        alertViewObj.showAlert(controller: self)
                        return false
                    }

                if validObj.getNumberRangeValidation(strTotalLength).max >= strTotalLength.count {
                
                     return true
                }
                
                    /*
                    if validObj.getNumberRangeValidation(strTotalLength).max >= strTotalLength.count {
                        let str = (textField.text! + string)
                        if str.count > 7   {
                        //    addMoneyModel.depositorContactNumber = textField.text! + string
                            textField.text = str
                            isConfirmMobileNoShow = true
                            tblAccountList.reloadData()
                            
                            if str.count == validObj.getNumberRangeValidation(strTotalLength).max {
                            let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? AMConfirmAccountMobNoCell
                            cell?.txtConfirmMobileNo.becomeFirstResponder()
                            }
                        }
                        else{
                                 // nothing
                        }
                        
                        return true
                    }
                    */
                    return false
            } else if(textField.tag == 51) {
                
                if range.location == 0 || range.location == 1 {
                    return false
                }
                if validObj.checkMatchingNumber(string: text, withString: addMoneyModel.depositorContactNumber) {
                    addMoneyModel.depositorConfirmContactNumber = text
                    return true
                } else {
                    return false
                }
            }
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField.tag == 50) { //Other Account
//            contactSuggessionView?.view.isHidden = true
//            if(!isContactSelected) {
//                addMoneyModel.depositorContactNumber = textField.text!
//                if(addMoneyModel.depositorContactNumber.count > 7) {
//                    isConfirmMobileNoShow = true
//                }
//                tblAccountList.reloadData()
//

//                let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? AMConfirmAccountMobNoCell
//                cell?.txtConfirmMobileNo.becomeFirstResponder()
//            }
        } else if(textField.tag == 51) { //Confirm Number
            addMoneyModel.depositorConfirmContactNumber = textField.text!
            if(addMoneyModel.depositorConfirmContactNumber.count > 7) {
                //isOtherAccountShow = true
            }
            //tblAccountList.reloadData()
        } else if(textField.tag == 73){
            addMoneyModel.remarks = textField.text!
            self.tblAccountList.reloadData()
        } else if(textField.tag == 60) {//MyNumber Amount
            self.tblAccountList.reloadData()
        } else if(textField.tag == 63) {//OtherNumber Amount
            self.tblAccountList.reloadData()
        } else if(textField.tag == 62) {//OtherNumber Contact Amount
            self.tblAccountList.reloadData()
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField.tag == 50) {
            let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? AMAccountMobileNoCell

                textField.becomeFirstResponder()
                if let value = textField.text?.count{
                    if value  > 5 {
                        debugPrint("Entered Value")
                      cell?.btnCancel.isHidden = false
                    }else{
                        textField.text = "09"
                     // cell?.txtAccountMobNo.becomeFirstResponder()
                      cell?.btnCancel.isHidden = true
                    }
                }
            /*
            textField.becomeFirstResponder()
            if let value = textField.text?.count{
                if value  > 5 {
                    debugPrint("Entered Value")
                }else{
                    textField.text = "09"
                    let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? AMAccountMobileNoCell
                   // cell?.txtAccountMobNo.becomeFirstResponder()
                    cell?.btnCancel.isHidden = true
                    tblAccountList.reloadData()
                }
            }*/
            
        } else  if(textField.tag == 51) {
            textField.becomeFirstResponder()
            if let value = textField.text?.count{
                if value  > 5 {
                    debugPrint("Entered Value")
                }else{
                    textField.text = "09"
                }
            }
            //textField.text = "09"
            self.hideContactSuggesionView()
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? AMAccountMobileNoCell
        if(textField.tag == 50 || textField.tag == 51) {
            textField.text = "09"
            cell?.txtAccountMobNo.becomeFirstResponder()
            cell?.btnCancel.isHidden = true
            tblAccountList.reloadData()
            return false
        } else {
            if(textField.tag == 60) {
                isRemark = false
           
                submitbuttonConstraint.constant = 0
                tblAccountList.reloadData()
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
    
    
      
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        
             
        
        if(textField.tag == 50) {
           
            
            let startPosition: UITextPosition = textField.beginningOfDocument
            print(startPosition)
           
            let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? AMAccountMobileNoCell
            
            if((textField.text?.length)! > 2)
            {
                cell?.btnCancel.isHidden = false
            }
            else {
                cell?.btnCancel.isHidden = true
            }
            
            let contactList = self.contactSuggesstion(textField.text!)
            if contactList.count > 0 {
                // here show the contact suggession screen
           
                //let rect = self.getSuggessionviewFrame(arr: contactList.count, indexpath: IndexPath(item: 0, section: 1))
                let rect = UitilityClass.setFrameForContactSuggestion(arrayCount: contactList.count, indexpath: IndexPath(item: 0, section: 1),onTextField: cell!.txtAccountMobNo,tableView: tblAccountList)
                self.showContactSuggessionView(withFrame: rect, andContacts: contactList)
                
                //self.showContactSuggessionView(withFrame: rect, andContacts: contactList)
            } else {
                // here hide the contact suggession screen
                  strEnterMobileValue = ""
                self.hideContactSuggesionView()
            }
            if((textField.text?.length)! <= 2) {
                textField.text = "09"
                strEnterMobileValue = textField.text ?? ""
                self.hideContactSuggesionView()
            }
            if(textField.text!.count == 0) {
                strEnterMobileValue = ""
                
            } else {
                
                strEnterMobileValue = textField.text ?? ""
                addMoneyModel.depositorContactNumber =  textField.text ?? ""
                addMoneyModel.depositorConfirmContactNumber = "09"
                isOtherAccountShow = false
                isRemark = false
                strEnterAmountValue = ""

                let rangeCheck = validObj.getNumberRangeValidation(strEnterMobileValue)
                
                if strEnterMobileValue.count >= rangeCheck.min {
                    if !isConfirmMobileNoShow {
                        isConfirmMobileNoShow = true
                        self.hideContactSuggesionView()
                        tblAccountList.reloadData()
                        if self.strEnterMobileValue.count < rangeCheck.max {
                               let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? AMAccountMobileNoCell
                               cell?.txtAccountMobNo.becomeFirstResponder()
                        }
                        lastCountIndex = self.strEnterMobileValue.count
                    } else {
                        if strEnterMobileValue.count <= rangeCheck.max && strEnterMobileValue.count >= rangeCheck.min && strEnterMobileValue.count < lastCountIndex  {
                                isConfirmMobileNoShow = true
                                isRemark = false
                                strEnterAmountValue = ""
                                addMoneyModel.enteredAmount = ""
                                addMoneyModel.remarks = ""
                                isContactSelected = false
                                isRemark = false
                                submitbuttonConstraint.constant = 0
                                isOtherAccountShow = false
                                lastCountIndex = self.strEnterMobileValue.count
                                tblAccountList.reloadData()
                                let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? AMAccountMobileNoCell
                                cell?.txtAccountMobNo.becomeFirstResponder()
                                let cellX = tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? AMConfirmAccountMobNoCell
                                cellX?.txtConfirmMobileNo.text = "09"
                        }
                        else {
                            let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? AMConfirmAccountMobNoCell
                            cell?.txtConfirmMobileNo.text = "09"
                            lastCountIndex = self.strEnterMobileValue.count
                        }
                    }
                    if strEnterMobileValue.count >= rangeCheck.max {
                        let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? AMConfirmAccountMobNoCell
                        cell?.txtConfirmMobileNo.becomeFirstResponder()
                        lastCountIndex = self.strEnterMobileValue.count
                    }
                    
                } else {
                    if isConfirmMobileNoShow || isRemark || isContactSelected {
                        isConfirmMobileNoShow = false
                        isRemark = false
                        strEnterAmountValue = ""
                        addMoneyModel.enteredAmount = ""
                        addMoneyModel.remarks = ""
                        isContactSelected = false
                        isRemark = false
                        submitbuttonConstraint.constant = 0
                        isOtherAccountShow = false
                        tblAccountList.reloadData()
                        let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? AMAccountMobileNoCell
                        cell?.txtAccountMobNo.becomeFirstResponder()
                    }
                }
                
            }
        }
        if(textField.tag == 51) {
            
            let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? AMConfirmAccountMobNoCell
            
            if((textField.text?.length)! > 2)
            {
                cell?.btnConfirmCancel.isHidden = false
            }
            else {
                cell?.btnConfirmCancel.isHidden = true
            }
            
             self.hideContactSuggesionView()
            
            if((textField.text?.length)! <= 2) {
                textField.text = "09"
            }
            if(addMoneyModel.depositorContactNumber == textField.text) {
                addMoneyModel.depositorConfirmContactNumber = textField.text!
                self.validateOtherOkDollarNumberAPI(phoneNumber: self.getConatctNum(textField.text!,withCountryCode: "+95"))
            }
            
            if isRemark || isContactSelected || isOtherAccountShow {
                isOtherAccountShow = false
                isRemark = false
                strEnterAmountValue = ""
                addMoneyModel.enteredAmount = ""
                addMoneyModel.remarks = ""
                isContactSelected = false
                isRemark = false
                submitbuttonConstraint.constant = 0
                isOtherAccountShow = false
                tblAccountList.reloadData()
                let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? AMConfirmAccountMobNoCell
                cell?.txtConfirmMobileNo.becomeFirstResponder()
            }
        }
         if(textField.tag == 60) {//MyNumber account field
                   let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? AMMyAccountSelectedCell
                   if(textField.text!.count == 0) {
                       strEnterAmountValue = ""
                        addMoneyModel.remarks = ""
                       isRemark = false
                       submitbuttonConstraint.constant = 0
                    cell?.btnMyAmountCancel.isHidden = true
                   } else {
                       //btnSubmit.isHidden = false
                       isRemark = true
                       submitbuttonConstraint.constant = 55
                       //Display split amount equally on all person
                       let dataWithoutComma = self.removeCommaFromDigit(textField.text!)
                       addMoneyModel.enteredAmount = dataWithoutComma
                       strEnterAmountValue = self.getDigitDisplay(dataWithoutComma)
                     if(strEnterAmountValue.length == 9) {
                       cell?.txtAmount.textColor = UIColor.green
                                } else if(strEnterAmountValue.length >= 10) {
                              cell?.txtAmount.textColor = UIColor.red
                           } else {
                           cell?.txtAmount.textColor = UIColor.black
                       }
                     cell?.txtAmount.text = strEnterAmountValue
                    cell?.btnMyAmountCancel.isHidden = false
                   }
                   if textField.text?.length ?? 0 >= 1 {
                       tblAccountList.reloadData()
                   }
                   let cellX = self.tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? AMMyAccountSelectedCell
                   cellX?.txtAmount.becomeFirstResponder()
                   cellX?.txtAmount.keyboardType = .numberPad
               }
        if(textField.tag == 62) {
            let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? AMOtherAccountSelectedCell
            if(textField.text!.count == 0) {
                strEnterAmountValue = ""
                addMoneyModel.remarks = ""
                isRemark = false
                submitbuttonConstraint.constant = 0
                cell?.btnOtherAmountCancel.isHidden = true
            } else {
                isRemark = true
                submitbuttonConstraint.constant = 55
                //Display split amount equally on all person
                let dataWithoutComma = self.removeCommaFromDigit(textField.text!)
                addMoneyModel.enteredAmount = dataWithoutComma
                strEnterAmountValue = self.getDigitDisplay(dataWithoutComma)
              if(strEnterAmountValue.length == 9) {
                cell?.txtAmount.textColor = UIColor.green
                         } else if(strEnterAmountValue.length >= 10) {
                       cell?.txtAmount.textColor = UIColor.red
                    } else {
                    cell?.txtAmount.textColor = UIColor.black
                }
              cell?.txtAmount.text = strEnterAmountValue
                cell?.btnOtherAmountCancel.isHidden = false
            }
            if textField.text?.length ?? 0 >= 1 {
                tblAccountList.reloadData()
            }
            let cellX = self.tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? AMOtherAccountSelectedCell
            cellX?.txtAmount.becomeFirstResponder()
            cellX?.txtAmount.keyboardType = .numberPad
        }
        if(textField.tag == 63) {
           
                let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 2, section: 1)) as? AMOtherAccountSelectedCell
                if(textField.text!.count == 0) {
                    strEnterAmountValue = ""
                    addMoneyModel.remarks = ""
                    isRemark = false
                    submitbuttonConstraint.constant = 0
                    cell?.btnOtherAmountCancel.isHidden = true
                } else {
                    //btnSubmit.isHidden = false
                    isRemark = true
                    submitbuttonConstraint.constant = 55
                    //Display split amount equally on all person
                    let dataWithoutComma = self.removeCommaFromDigit(textField.text!)
                    addMoneyModel.enteredAmount = dataWithoutComma
                    strEnterAmountValue = self.getDigitDisplay(dataWithoutComma)
                  if(strEnterAmountValue.length == 9) {
                    cell?.txtAmount.textColor = UIColor.green
                             } else if(strEnterAmountValue.length >= 10) {
                           cell?.txtAmount.textColor = UIColor.red
                        } else {
                        cell?.txtAmount.textColor = UIColor.black
                    }
                  cell?.txtAmount.text = strEnterAmountValue
                    cell?.btnOtherAmountCancel.isHidden = false
                }
            if textField.text?.length ?? 0 >= 1 {
                    tblAccountList.reloadData()
                }
                let cellX = self.tblAccountList.cellForRow(at: IndexPath.init(row: 2, section: 1)) as? AMOtherAccountSelectedCell
                cellX?.txtAmount.becomeFirstResponder()
                cellX?.txtAmount.keyboardType = .numberPad
        }
        if(textField.tag == 73) {
            if(textField.text!.count == 0) {
                strEnterRemarktValue = ""
            } else {
                strEnterRemarktValue = textField.text ?? ""
            }
            //tblAccountList.reloadData()
            let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 2)) as? AMRemarkCell
            cell?.txtRemark.becomeFirstResponder()
        }
    }
    
    // MARK: - API Integration
    func getPaymentAccessToken() {
        if appDelegate.checkNetworkAvail() {
            showProgressView()
            let tokenUrl = getUrl(urlStr: "", serverType: .AddMoney_GetToken)
            let hashValue = Url.aKey_AddMoney.hmac_SHA1(key: Url.sKey_AddMoneyPayment)
            let inputValue = "password=\(hashValue)&grant_type=password"
            let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: "POST", contentType: LLBStrings.kContentType_urlencoded, inputVal: inputValue, authStr: nil)
            JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
                guard isSuccess else {
                    println_debug("Token API Fail")
                    self.removeProgressView()
                    return
                }
                guard let responseDic = response as? NSDictionary else {
                    self.removeProgressView()
                    return
                }
                guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                    self.removeProgressView()
                    return
                }
                //let authorizationString =  "\(tokentype) \(accesstoken)"
                self.removeProgressView()
                self.callBankChargesAll(tokenType:tokentype as! String,accesstoken:accesstoken as! String)
            })
        }
    }
    
    func getAccessToken() {
        
        if appDelegate.checkNetworkAvail() {
            showProgressView()
            let tokenUrl = getUrl(urlStr: "", serverType: .AddMoney_GetToken)//URL.init(string:  Url.AddMoney_GetToken)
            let hashValue = Url.aKey_AddMoney.hmac_SHA1(key: Url.sKey_AddMoney)
            let inputValue = "password=\(hashValue)&grant_type=password"
            let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: "POST", contentType: LLBStrings.kContentType_urlencoded, inputVal: inputValue, authStr: nil)
            JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
                guard isSuccess else  {
                    self.removeProgressView()
                    return
                }
                let responseDic = response as! NSDictionary
                guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                    self.removeProgressView()
                    return
                }
                let authorizationString =  "\(tokentype) \(accesstoken)"
                println_debug(authorizationString)
                self.removeProgressView()
                self.callBankChargesAll(tokenType:tokentype as! String,accesstoken:accesstoken as! String)
            })
        }
    }
    
    //Based on API response update the card details
    func generateNewArrayList () {
        
        var strVisaMasterPay = ""
        var strMPU = ""
        var strCbBANK = ""
        var strKbzBANK = ""
        var strJCB = "0"
        var strUnionPay = "0"

        for parseDict in self.bankResponseArr {
            if let dict = parseDict as? [String: Any], let gateWayName = dict["PaymentGatewayName"] as? String {
                let value = dict["AdminFee"] as? Double
                
                if gateWayName == "VisaMasterPay" {//Percentage
                    strVisaMasterPay = value?.stringWithoutZeroFraction ?? ""
                } else if gateWayName == "MPU " {//Fixed
                    strMPU = value?.stringWithoutZeroFraction ?? ""
                }  else if gateWayName == "CB BANK" {
                    strCbBANK = value?.stringWithoutZeroFraction ?? ""
                } else if gateWayName == "KBZ BANK " {
                   strKbzBANK = value?.stringWithoutZeroFraction ?? ""
                } else if gateWayName == "JCB" {
                    if dict["IsEnabled"] as? Int == 1 {
                        isJCB = true
                        strJCB = value?.stringWithoutZeroFraction ?? "0"
                    }
                } else if gateWayName == "UnionPay" {
                      if dict["IsEnabled"] as? Int == 1 {
                        isUnionPay = true
                        strUnionPay = value?.stringWithoutZeroFraction ?? "0"
                    }
                }
            }
        }
      
        //Create New Array
        if isJCB && isUnionPay {
            if UserModel.shared.agentType == .user {
                sections = [
                           
                           AddMoneySection(sectionName:"Find OK$ Agent".localized, items:[
                               Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Paid Commission Agent Cash In".localized, items:[
                               Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Free Commission Agent Cash In".localized, items:[
                               Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                           
                        AddMoneySection(sectionName:"123 Payment Code".localized, items:[
                        Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                           AddMoneySection(sectionName:"Credit Card".localized, items:[
                               Item(cardName:"MPU".localized, fees: strMPU),
                               Item(cardName:"Visa".localized, fees: strVisaMasterPay),
                               Item(cardName:"Master Card".localized, fees: strVisaMasterPay),
                               Item(cardName:"JCB".localized, fees: strJCB),
                               Item(cardName:"UnionPay".localized, fees: strUnionPay)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Debit Card".localized, items:[
                               Item(cardName:"MPU".localized, fees: strMPU),
                               Item(cardName:"Visa".localized, fees: strVisaMasterPay),
                               Item(cardName:"Master Card".localized, fees: strVisaMasterPay),
                               Item(cardName:"JCB".localized, fees: strJCB),
                               Item(cardName:"UnionPay".localized, fees: strUnionPay)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Online/Net Banking".localized, items:[
                               Item(cardName:"KBZ Bank".localized, fees: strKbzBANK),
                               Item(cardName:"CB Bank".localized, fees: strCbBANK)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Bank Counter Deposit Add".localized, items:[
                               Item(cardName:"KBZ Bank".localized, fees:"200 MMK (Fees)")], collapsed:true),
                           
                           AddMoneySection(sectionName:"OK$ OfficeAW".localized, items:[
                               Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Myanmar Post".localized, items:[
                               Item(cardName:"post".localized, fees:"Comming Soon".localized)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Bank".localized, items:[
                               Item(cardName:"bank".localized, fees:"Comming Soon".localized)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Other Mobile Money Wallet".localized, items:[
                               Item(cardName:"Wave Money".localized, fees:"Comming Soon".localized),
                               Item(cardName:"True Money".localized, fees:"Comming Soon".localized)], collapsed:true)
                           
                       ]
        } else {
        sections = [
            
            AddMoneySection(sectionName:"Find OK$ Agent".localized, items:[
                Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
            
            AddMoneySection(sectionName:"Agent Transfer Digital Money".localized, items:[
                Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
            
            AddMoneySection(sectionName:"Paid Commission Agent Cash In".localized, items:[
                Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
            
            AddMoneySection(sectionName:"Free Commission Agent Cash In".localized, items:[
                Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
            
            
         AddMoneySection(sectionName:"123 Payment Code".localized, items:[
         Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
            
            AddMoneySection(sectionName:"Credit Card".localized, items:[
                Item(cardName:"MPU".localized, fees: strMPU),
                Item(cardName:"Visa".localized, fees: strVisaMasterPay),
                Item(cardName:"Master Card".localized, fees: strVisaMasterPay),
                Item(cardName:"JCB".localized, fees: strJCB),
                Item(cardName:"UnionPay".localized, fees: strUnionPay)], collapsed:true),
            
            AddMoneySection(sectionName:"Debit Card".localized, items:[
                Item(cardName:"MPU".localized, fees: strMPU),
                Item(cardName:"Visa".localized, fees: strVisaMasterPay),
                Item(cardName:"Master Card".localized, fees: strVisaMasterPay),
                Item(cardName:"JCB".localized, fees: strJCB),
                Item(cardName:"UnionPay".localized, fees: strUnionPay)], collapsed:true),
            
            AddMoneySection(sectionName:"Online/Net Banking".localized, items:[
                Item(cardName:"KBZ Bank".localized, fees: strKbzBANK),
                Item(cardName:"CB Bank".localized, fees: strCbBANK)], collapsed:true),
            
            AddMoneySection(sectionName:"Bank Counter Deposit Add".localized, items:[
                Item(cardName:"KBZ Bank".localized, fees:"200 MMK (Fees)")], collapsed:true),
            
            AddMoneySection(sectionName:"OK$ OfficeAW".localized, items:[
                Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
            
            AddMoneySection(sectionName:"Myanmar Post".localized, items:[
                Item(cardName:"post".localized, fees:"Comming Soon".localized)], collapsed:true),
            
            AddMoneySection(sectionName:"Bank".localized, items:[
                Item(cardName:"bank".localized, fees:"Comming Soon".localized)], collapsed:true),
            
            AddMoneySection(sectionName:"Other Mobile Money Wallet".localized, items:[
                Item(cardName:"Wave Money".localized, fees:"Comming Soon".localized),
                Item(cardName:"True Money".localized, fees:"Comming Soon".localized)], collapsed:true)
            
        ] }
    }
        else if isJCB && isUnionPay {
            if UserModel.shared.agentType == .user {
                sections = [
                           
                           AddMoneySection(sectionName:"Find OK$ Agent".localized, items:[
                               Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Paid Commission Agent Cash In".localized, items:[
                               Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Free Commission Agent Cash In".localized, items:[
                               Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                 AddMoneySection(sectionName:"123 Payment Code".localized, items:[
                 Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                           AddMoneySection(sectionName:"Credit Card".localized, items:[
                               Item(cardName:"MPU".localized, fees: strMPU),
                               Item(cardName:"Visa".localized, fees: strVisaMasterPay),
                               Item(cardName:"Master Card".localized, fees: strVisaMasterPay),
                               Item(cardName:"JCB".localized, fees: strJCB),
                               Item(cardName:"UnionPay".localized, fees: strUnionPay)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Debit Card".localized, items:[
                               Item(cardName:"MPU".localized, fees: strMPU),
                               Item(cardName:"Visa".localized, fees: strVisaMasterPay),
                               Item(cardName:"Master Card".localized, fees: strVisaMasterPay),
                               Item(cardName:"JCB".localized, fees: strJCB),
                               Item(cardName:"UnionPay".localized, fees: strUnionPay)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Online/Net Banking".localized, items:[
                               Item(cardName:"KBZ Bank".localized, fees: strKbzBANK),
                               Item(cardName:"CB Bank".localized, fees: strCbBANK)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Bank Counter Deposit Add".localized, items:[
                               Item(cardName:"KBZ Bank".localized, fees:"200 MMK (Fees)")], collapsed:true),
                           
                           AddMoneySection(sectionName:"OK$ OfficeAW".localized, items:[
                               Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Myanmar Post".localized, items:[
                               Item(cardName:"post".localized, fees:"Comming Soon".localized)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Bank".localized, items:[
                               Item(cardName:"bank".localized, fees:"Comming Soon".localized)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Other Mobile Money Wallet".localized, items:[
                               Item(cardName:"Wave Money".localized, fees:"Comming Soon".localized),
                               Item(cardName:"True Money".localized, fees:"Comming Soon".localized)], collapsed:true)
                           
                       ]
        } else {
        sections = [
            
            AddMoneySection(sectionName:"Find OK$ Agent".localized, items:[
                Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
            
            AddMoneySection(sectionName:"Agent Transfer Digital Money".localized, items:[
                Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
            
            AddMoneySection(sectionName:"Paid Commission Agent Cash In".localized, items:[
                Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
            
            AddMoneySection(sectionName:"Free Commission Agent Cash In".localized, items:[
                Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
            
         AddMoneySection(sectionName:"123 Payment Code".localized, items:[
         Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
            
            AddMoneySection(sectionName:"Credit Card".localized, items:[
                Item(cardName:"MPU".localized, fees: strMPU),
                Item(cardName:"Visa".localized, fees: strVisaMasterPay),
                Item(cardName:"Master Card".localized, fees: strVisaMasterPay),
                Item(cardName:"JCB".localized, fees: strJCB),
                Item(cardName:"UnionPay".localized, fees: strUnionPay)], collapsed:true),
            
            AddMoneySection(sectionName:"Debit Card".localized, items:[
                Item(cardName:"MPU".localized, fees: strMPU),
                Item(cardName:"Visa".localized, fees: strVisaMasterPay),
                Item(cardName:"Master Card".localized, fees: strVisaMasterPay),
                Item(cardName:"JCB".localized, fees: strJCB),
                Item(cardName:"UnionPay".localized, fees: strUnionPay)], collapsed:true),
            
            AddMoneySection(sectionName:"Online/Net Banking".localized, items:[
                Item(cardName:"KBZ Bank".localized, fees: strKbzBANK),
                Item(cardName:"CB Bank".localized, fees: strCbBANK)], collapsed:true),
            
            AddMoneySection(sectionName:"Bank Counter Deposit Add".localized, items:[
                Item(cardName:"KBZ Bank".localized, fees:"200 MMK (Fees)")], collapsed:true),
            
            AddMoneySection(sectionName:"OK$ OfficeAW".localized, items:[
                Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
            
            AddMoneySection(sectionName:"Myanmar Post".localized, items:[
                Item(cardName:"post".localized, fees:"Comming Soon".localized)], collapsed:true),
            
            AddMoneySection(sectionName:"Bank".localized, items:[
                Item(cardName:"bank".localized, fees:"Comming Soon".localized)], collapsed:true),
            
            AddMoneySection(sectionName:"Other Mobile Money Wallet".localized, items:[
                Item(cardName:"Wave Money".localized, fees:"Comming Soon".localized),
                Item(cardName:"True Money".localized, fees:"Comming Soon".localized)], collapsed:true)
            
        ] }
    }
        else if isJCB {
        if UserModel.shared.agentType == .user {
            sections = [
                AddMoneySection(sectionName:"Find OK$ Agent".localized, items:[
                    Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),

                AddMoneySection(sectionName:"Paid Commission Agent Cash In".localized, items:[
                    Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                
                AddMoneySection(sectionName:"Free Commission Agent Cash In".localized, items:[
                    Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                
             AddMoneySection(sectionName:"123 Payment Code".localized, items:[
             Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                
                AddMoneySection(sectionName:"Credit Card".localized, items:[
                    Item(cardName:"MPU".localized, fees: strMPU),
                    Item(cardName:"Visa".localized, fees: strVisaMasterPay),
                    Item(cardName:"Master Card".localized, fees: strVisaMasterPay),
                    Item(cardName:"JCB".localized, fees: strJCB)], collapsed:true),
                
                AddMoneySection(sectionName:"Debit Card".localized, items:[
                    Item(cardName:"MPU".localized, fees: strMPU),
                    Item(cardName:"Visa".localized, fees: strVisaMasterPay),
                    Item(cardName:"Master Card".localized, fees: strVisaMasterPay),
                    Item(cardName:"JCB".localized, fees: strJCB)], collapsed:true),

                AddMoneySection(sectionName:"Online/Net Banking".localized, items:[
                    Item(cardName:"KBZ Bank".localized, fees: strKbzBANK),
                    Item(cardName:"CB Bank".localized, fees: strCbBANK)], collapsed:true),
                
                AddMoneySection(sectionName:"Bank Counter Deposit Add".localized, items:[
                    Item(cardName:"KBZ Bank".localized, fees:"200 MMK (Fees)")], collapsed:true),
 
                AddMoneySection(sectionName:"OK$ OfficeAW".localized, items:[
                    Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                
                AddMoneySection(sectionName:"Myanmar Post".localized, items:[
                    Item(cardName:"post".localized, fees:"Comming Soon".localized)], collapsed:true),
                
                AddMoneySection(sectionName:"Bank".localized, items:[
                    Item(cardName:"bank".localized, fees:"Comming Soon".localized)], collapsed:true),
                
                AddMoneySection(sectionName:"Other Mobile Money Wallet".localized, items:[
                    Item(cardName:"Wave Money".localized, fees:"Comming Soon".localized),
                    Item(cardName:"True Money".localized, fees:"Comming Soon".localized)], collapsed:true)
                
            ] }
        else {
            sections = [
                           
                           AddMoneySection(sectionName:"Find OK$ Agent".localized, items:[
                               Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),

                           AddMoneySection(sectionName:"Agent Transfer Digital Money".localized, items:[
                                             Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                                      
                           AddMoneySection(sectionName:"Paid Commission Agent Cash In".localized, items:[
                               Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Free Commission Agent Cash In".localized, items:[
                               Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                
             AddMoneySection(sectionName:"123 Payment Code".localized, items:[
             Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                
                           AddMoneySection(sectionName:"Credit Card".localized, items:[
                               Item(cardName:"MPU".localized, fees: strMPU),
                               Item(cardName:"Visa".localized, fees: strVisaMasterPay),
                               Item(cardName:"Master Card".localized, fees: strVisaMasterPay),
                               Item(cardName:"JCB".localized, fees: strJCB)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Debit Card".localized, items:[
                               Item(cardName:"MPU".localized, fees: strMPU),
                               Item(cardName:"Visa".localized, fees: strVisaMasterPay),
                               Item(cardName:"Master Card".localized, fees: strVisaMasterPay),
                               Item(cardName:"JCB".localized, fees: strJCB)], collapsed:true),

                           AddMoneySection(sectionName:"Online/Net Banking".localized, items:[
                               Item(cardName:"KBZ Bank".localized, fees: strKbzBANK),
                               Item(cardName:"CB Bank".localized, fees: strCbBANK)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Bank Counter Deposit Add".localized, items:[
                               Item(cardName:"KBZ Bank".localized, fees:"200 MMK (Fees)")], collapsed:true),
            
                           AddMoneySection(sectionName:"OK$ OfficeAW".localized, items:[
                               Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Myanmar Post".localized, items:[
                               Item(cardName:"post".localized, fees:"Comming Soon".localized)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Bank".localized, items:[
                               Item(cardName:"bank".localized, fees:"Comming Soon".localized)], collapsed:true),
                           
                           AddMoneySection(sectionName:"Other Mobile Money Wallet".localized, items:[
                               Item(cardName:"Wave Money".localized, fees:"Comming Soon".localized),
                               Item(cardName:"True Money".localized, fees:"Comming Soon".localized)], collapsed:true)
                           
                       ]
            }
        }
        else if isUnionPay {
               if UserModel.shared.agentType == .user {
                   sections = [
                       
                       AddMoneySection(sectionName:"Find OK$ Agent".localized, items:[
                           Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),

                       AddMoneySection(sectionName:"Paid Commission Agent Cash In".localized, items:[
                           Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                       
                       AddMoneySection(sectionName:"Free Commission Agent Cash In".localized, items:[
                           Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                       
                       AddMoneySection(sectionName:"Credit Card".localized, items:[
                           Item(cardName:"MPU".localized, fees: strMPU),
                           Item(cardName:"Visa".localized, fees: strVisaMasterPay),
                           Item(cardName:"Master Card".localized, fees: strVisaMasterPay),
                           Item(cardName:"UnionPay".localized, fees: strUnionPay)], collapsed:true),
                       
                       AddMoneySection(sectionName:"Debit Card".localized, items:[
                           Item(cardName:"MPU".localized, fees: strMPU),
                           Item(cardName:"Visa".localized, fees: strVisaMasterPay),
                           Item(cardName:"Master Card".localized, fees: strVisaMasterPay),
                           Item(cardName:"UnionPay".localized, fees: strUnionPay)], collapsed:true),

                       AddMoneySection(sectionName:"Online/Net Banking".localized, items:[
                           Item(cardName:"KBZ Bank".localized, fees: strKbzBANK),
                           Item(cardName:"CB Bank".localized, fees: strCbBANK)], collapsed:true),
                       
                       AddMoneySection(sectionName:"Bank Counter Deposit Add".localized, items:[
                           Item(cardName:"KBZ Bank".localized, fees:"200 MMK (Fees)")], collapsed:true),
        
                       AddMoneySection(sectionName:"OK$ OfficeAW".localized, items:[
                           Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                       
                       AddMoneySection(sectionName:"Myanmar Post".localized, items:[
                           Item(cardName:"post".localized, fees:"Comming Soon".localized)], collapsed:true),
                       
                       AddMoneySection(sectionName:"Bank".localized, items:[
                           Item(cardName:"bank".localized, fees:"Comming Soon".localized)], collapsed:true),
                       
                       AddMoneySection(sectionName:"Other Mobile Money Wallet".localized, items:[
                           Item(cardName:"Wave Money".localized, fees:"Comming Soon".localized),
                           Item(cardName:"True Money".localized, fees:"Comming Soon".localized)], collapsed:true)
                       
                   ] }
               else {
                   sections = [
                                  
                                  AddMoneySection(sectionName:"Find OK$ Agent".localized, items:[
                                      Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),

                                  AddMoneySection(sectionName:"Agent Transfer Digital Money".localized, items:[
                                                    Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                                             
                                  AddMoneySection(sectionName:"Paid Commission Agent Cash In".localized, items:[
                                      Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                                  
                                  AddMoneySection(sectionName:"Free Commission Agent Cash In".localized, items:[
                                      Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                    AddMoneySection(sectionName:"123 Payment Code".localized, items:[
                    Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                   
                                  AddMoneySection(sectionName:"Credit Card".localized, items:[
                                      Item(cardName:"MPU".localized, fees: strMPU),
                                      Item(cardName:"Visa".localized, fees: strVisaMasterPay),
                                      Item(cardName:"Master Card".localized, fees: strVisaMasterPay),
                                      Item(cardName:"UnionPay".localized, fees: strUnionPay)], collapsed:true),
                                  
                                  AddMoneySection(sectionName:"Debit Card".localized, items:[
                                      Item(cardName:"MPU".localized, fees: strMPU),
                                      Item(cardName:"Visa".localized, fees: strVisaMasterPay),
                                      Item(cardName:"Master Card".localized, fees: strVisaMasterPay),
                                      Item(cardName:"UnionPay".localized, fees: strUnionPay)], collapsed:true),

                                  AddMoneySection(sectionName:"Online/Net Banking".localized, items:[
                                      Item(cardName:"KBZ Bank".localized, fees: strKbzBANK),
                                      Item(cardName:"CB Bank".localized, fees: strCbBANK)], collapsed:true),
                                  
                                  AddMoneySection(sectionName:"Bank Counter Deposit Add".localized, items:[
                                      Item(cardName:"KBZ Bank".localized, fees:"200 MMK (Fees)")], collapsed:true),
                   
                                  AddMoneySection(sectionName:"OK$ OfficeAW".localized, items:[
                                      Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                                  
                                  AddMoneySection(sectionName:"Myanmar Post".localized, items:[
                                      Item(cardName:"post".localized, fees:"Comming Soon".localized)], collapsed:true),
                                  
                                  AddMoneySection(sectionName:"Bank".localized, items:[
                                      Item(cardName:"bank".localized, fees:"Comming Soon".localized)], collapsed:true),
                                  
                                  AddMoneySection(sectionName:"Other Mobile Money Wallet".localized, items:[
                                      Item(cardName:"Wave Money".localized, fees:"Comming Soon".localized),
                                      Item(cardName:"True Money".localized, fees:"Comming Soon".localized)], collapsed:true)
                                  
                              ]
                   }
               }
        else {
            if UserModel.shared.agentType == .user {
                sections = [
                    
                    AddMoneySection(sectionName:"Find OK$ Agent".localized, items:[
                        Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                    AddMoneySection(sectionName:"Paid Commission Agent Cash In".localized, items:[
                        Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                    AddMoneySection(sectionName:"Free Commission Agent Cash In".localized, items:[
                        Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                    AddMoneySection(sectionName:"123 Payment Code".localized, items:[
                    Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                   
                    AddMoneySection(sectionName:"Credit Card".localized, items:[
                        Item(cardName:"MPU".localized, fees: strMPU),
                        Item(cardName:"Visa".localized, fees: strVisaMasterPay),
                        Item(cardName:"Master Card".localized, fees: strVisaMasterPay)], collapsed:true),
                    
                    AddMoneySection(sectionName:"Debit Card".localized, items:[
                        Item(cardName:"MPU".localized, fees: strMPU),
                        Item(cardName:"Visa".localized, fees: strVisaMasterPay),
                        Item(cardName:"Master Card".localized, fees: strVisaMasterPay)], collapsed:true),

                    AddMoneySection(sectionName:"Online/Net Banking".localized, items:[
                        Item(cardName:"KBZ Bank".localized, fees: strKbzBANK),
                        Item(cardName:"CB Bank".localized, fees: strCbBANK)], collapsed:true),
                    
                    AddMoneySection(sectionName:"Bank Counter Deposit Add".localized, items:[
                        Item(cardName:"KBZ Bank".localized, fees:"200 MMK (Fees)")], collapsed:true),
                    
                    AddMoneySection(sectionName:"OK$ OfficeAW".localized, items:[
                        Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                    AddMoneySection(sectionName:"Myanmar Post".localized, items:[
                        Item(cardName:"post".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                    AddMoneySection(sectionName:"Bank".localized, items:[
                        Item(cardName:"bank".localized, fees:"Comming Soon".localized)], collapsed:true),
                    
                    AddMoneySection(sectionName:"Other Mobile Money Wallet".localized, items:[
                        Item(cardName:"Wave Money".localized, fees:"Comming Soon".localized),
                        Item(cardName:"True Money".localized, fees:"Comming Soon".localized)], collapsed:true)
                ]
            } else {
            sections = [
                
                AddMoneySection(sectionName:"Find OK$ Agent".localized, items:[
                    Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                
                AddMoneySection(sectionName:"Agent Transfer Digital Money".localized, items:[
                    Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                
                AddMoneySection(sectionName:"Paid Commission Agent Cash In".localized, items:[
                    Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                
                AddMoneySection(sectionName:"Free Commission Agent Cash In".localized, items:[
                    Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                AddMoneySection(sectionName:"123 Payment Code".localized, items:[
                Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
               
                AddMoneySection(sectionName:"Credit Card".localized, items:[
                    Item(cardName:"MPU".localized, fees: strMPU),
                    Item(cardName:"Visa".localized, fees: strVisaMasterPay),
                    Item(cardName:"Master Card".localized, fees: strVisaMasterPay),Item(cardName:"JCB".localized, fees: strJCB)], collapsed:true),
                
                AddMoneySection(sectionName:"Debit Card".localized, items:[
                    Item(cardName:"MPU".localized, fees: strMPU),
                    Item(cardName:"Visa".localized, fees: strVisaMasterPay),
                    Item(cardName:"Master Card".localized, fees: strVisaMasterPay)], collapsed:true),

                AddMoneySection(sectionName:"Online/Net Banking".localized, items:[
                    Item(cardName:"KBZ Bank".localized, fees: strKbzBANK),
                    Item(cardName:"CB Bank".localized, fees: strCbBANK)], collapsed:true),
                
                AddMoneySection(sectionName:"Bank Counter Deposit Add".localized, items:[
                    Item(cardName:"KBZ Bank".localized, fees:"200 MMK (Fees)")], collapsed:true),
                
                AddMoneySection(sectionName:"OK$ OfficeAW".localized, items:[
                    Item(cardName:"ok_office".localized, fees:"Comming Soon".localized)], collapsed:true),
                
                AddMoneySection(sectionName:"Myanmar Post".localized, items:[
                    Item(cardName:"post".localized, fees:"Comming Soon".localized)], collapsed:true),
                
                AddMoneySection(sectionName:"Bank".localized, items:[
                    Item(cardName:"bank".localized, fees:"Comming Soon".localized)], collapsed:true),
                
                AddMoneySection(sectionName:"Other Mobile Money Wallet".localized, items:[
                    Item(cardName:"Wave Money".localized, fees:"Comming Soon".localized),
                    Item(cardName:"True Money".localized, fees:"Comming Soon".localized)], collapsed:true)
            ]
            }
        }
        
         DispatchQueue.main.async {
        self.tblAccountList.reloadData()
        self.tblOptionsList.reloadData()
        }
    }
    
    func callBankChargesAll(tokenType:String,accesstoken:String) {
        let tokenUrl =  getUrl(urlStr: "", serverType: .AddMoney_GetCharges)//URL.init(string:  Url.AddMoney_GetCharges)
        let tokenType = tokenType
        let token =  accesstoken
        let header = tokenType + " " + token
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: "GET", contentType: LLBStrings.kContentType_Json, inputVal: nil, authStr: header)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            guard isSuccess else {
                self.removeProgressView()
                self.showToast(message: "Please try again later!")
                return
            }
            if let responseDic = response as? [Dictionary<String,Any>] {
                println_debug(responseDic)
                self.bankResponseArr = responseDic //response nil : Kethan commented b'coz app crashing
                //Update Main Array
               self.generateNewArrayList()
            }
            self.removeProgressView()
        })
    }
    
    func validateOkDollarNumberAPI(phoneNumber: Any) {   //Verify Mobile No
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            showProgressView()
            let buldApi = Url.billSplitterMobileValidation + "MobileNumber=\(phoneNumber)"
            let url = getUrl(urlStr: buldApi, serverType: .serverApp)
            let params = Dictionary<String,String>()
            web.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", mScreen: "mMobileValidation")
        }
    }
    
    func validateOtherOkDollarNumberAPI(phoneNumber: Any) {   //Verify Mobile No
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            showProgressView()
            let buldApi = Url.billSplitterMobileValidation + "MobileNumber=\(phoneNumber)"
            let url = getUrl(urlStr: buldApi, serverType: .serverApp)
            let params = Dictionary<String,String>()
            web.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", mScreen: "mOtherMobileValidation")
        }
    }
    
    //MARK:- API Response
    func webResponse(withJson json: AnyObject, screen: String) {
        self.removeProgressView()
        if screen == "mMobileValidation" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        if dic["Code"]! as! Int == 200 {
                            DispatchQueue.main.async {
                                self.isOtherAccountShow = true
                                self.isContactSelected = true
                                self.strEnterAmountValue = ""
                                self.strEnterRemarktValue = ""
                                self.isRemark = false
                                let dataDict = dic["Data"] as? String
                                let tempData = OKBaseController.convertToDictionary(text: dataDict!)
                                self.addMoneyModel.okACOwnername = tempData!["MercantName"]! as! String
                                self.tblAccountList.reloadData()
                            }
                        } else {
                            DispatchQueue.main.async {
                                self.isConfirmMobileNoShow = false
                                self.isRemark = false
                                self.isContactSelected = false
                                self.isOtherAccountShow = false
                                self.addMoneyModel.depositorContactNumber = ""
                                self.tblAccountList.reloadData()
                                alertViewObj.wrapAlert(title: nil, body:"Okdollar Account Number Not Exist. Please check!".localized, img:  #imageLiteral(resourceName: "phone_alert"))
                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                    self.tblAccountList.reloadData()
                                    let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? AMAccountMobileNoCell
                                    cell?.txtAccountMobNo.becomeFirstResponder()
                                }
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                }
            } catch {
                
            }
        } else if screen == "mOtherMobileValidation" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        if dic["Code"]! as! Int == 200 {
                            DispatchQueue.main.async {
                                self.isOtherAccountShow = true
                                self.isConfirmMobileNoShow = true
                                self.strEnterAmountValue = ""
                                self.strEnterRemarktValue = ""
                                self.isRemark = false
                                let dataDict = dic["Data"] as? String
                                let tempData = OKBaseController.convertToDictionary(text: dataDict!)
                                self.addMoneyModel.receiverName = tempData!["MercantName"]! as! String
                                self.tblAccountList.reloadData()
                            }
                        } else {
                            DispatchQueue.main.async {
                                self.isConfirmMobileNoShow = false
                                self.isRemark = false
                                self.isOtherAccountShow = false
                                self.addMoneyModel.depositorContactNumber = ""
                                self.addMoneyModel.depositorConfirmContactNumber = ""
                                self.addMoneyModel.receiverName = ""
                                self.tblAccountList.reloadData()
                                alertViewObj.wrapAlert(title: nil, body:"Okdollar Account Number Not Exist. Please check!".localized, img:  #imageLiteral(resourceName: "phone_alert"))
                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                    self.tblAccountList.reloadData()
                                    let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? AMAccountMobileNoCell
                                    cell?.txtAccountMobNo.becomeFirstResponder()
                                }
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                }
            } catch {
                
            }
        } else if(screen == "mAccessToken") {
            
        }
    }
    
    func loadContactSuggessionView() {
        contactSuggessionView  =  UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "ContactSugestView_ID") as? ContactSuggestionVC
        contactSuggessionView?.delegate   = self //as? ContactSuggestionDelegate
        contactSuggessionView?.view.frame = CGRect.zero
        addChild(contactSuggessionView!)
        self.view.addSubview(contactSuggessionView!.view)
        contactSuggessionView!.didMove(toParent: self)
        contactSuggessionView?.view.isHidden = true
    }
    
    fileprivate func getSuggessionviewFrame(arr: Int, indexpath: IndexPath) -> CGRect {
        let rect = tblAccountList.rectForRow(at: indexpath)
        var height          : CGFloat = CGFloat(Float(arr) * 70.0)
        let cellHgt         : CGFloat = 70.0
        let normalHeaderHgt : CGFloat = 40.0
        let maxHeight = screenHeight - 250 - cellHgt - normalHeaderHgt // - cellHgt
        if arr > 5 {
            height = maxHeight
        }
        let yPos = screenHeight - rect.origin.y - height
        if(screenHeight == 568) {
            return CGRect(x: 10, y: yPos-250 , width: screenWidth - 10, height: height)
        }
        return CGRect(x: 10, y: 0 , width: screenWidth - 10, height: 150)
    }
    
    func hideContactSuggesionView() {
        if let contactview = contactSuggessionView {
            contactview.view.isHidden = true
        }
    }
    
    func showContactSuggessionView(withFrame frame: CGRect, andContacts contactList: [Dictionary<String, Any>]) {
        contactSuggessionView?.view.isHidden    = false
        contactSuggessionView?.view.frame       = frame
        contactSuggessionView?.contactsList     = contactList
        contactSuggessionView?.contactsTable.reloadData()
        contactSuggessionView?.view.layoutIfNeeded()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        contactSuggessionView?.view.isHidden = true
        self.view.endEditing(true)
    }
}

extension AddWithdrawMoneyVC: BioMetricLoginDelegate {
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful {
            if(screen == "AddWithdrawMoneyVCSubmit"){
                println_debug((Double(addMoneyModel.totalAmount) ?? 0))
                println_debug((Double(addMoneyModel.enteredAmount) ?? 0))
               if (Double(addMoneyModel.totalAmount) ?? 0) >= (Double(addMoneyModel.enteredAmount) ?? 0) {
                   self.submitToServer()
               }
            }else if(screen == "AddWithdrawMoneyViewBalance") {
                lblBalance.attributedText = self.walletBalance()
                btnWalletBalance.isHidden = true
            }else if(screen == "NavigateToSendMoneyBank") {
                self.navToSendMoneyToBank()
            }
      }
    }
}

extension AddWithdrawMoneyVC : ContactSuggestionDelegate {
    func didSelectFromContactSuggession(number: Dictionary<String, Any>) {
        let strContactNumber = self.getConatctNum(number[ContactSuggessionKeys.phonenumber_backend] as! String,withCountryCode: "+95")
        addMoneyModel.depositorContactNumber = self.getActualNum(strContactNumber,withCountryCode: "+95")
        
        //Cal API for verification
        self.validateOkDollarNumberAPI(phoneNumber:strContactNumber)
        self.hideContactSuggesionView()
    }
}

extension AddWithdrawMoneyVC : ContactPickerDelegate {
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]) {
        for contact in contacts {
            decodeContact(contact: contact, isMulti: true)
        }
    }
    
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        decodeContact(contact: contact, isMulti: false)
    }
    
    func decodeContact(contact: ContactPicker, isMulti: Bool) {
        let phoneNumberCount = contact.phoneNumbers.count
        if phoneNumberCount >= 1, (contact.phoneNumbers.first?.phoneNumber.count ?? 0) > 0 {
            _ = "\(contact.phoneNumbers[0].phoneNumber)"
            let strMobNo = contact.phoneNumbers[0].phoneNumber
            let trimmed = String(strMobNo.filter { !" ".contains($0) })
            let strContactNumber = self.getConatctNum(trimmed,withCountryCode: "+95")
            addMoneyModel.depositorContactNumber = self.getActualNum(strContactNumber,withCountryCode: "+95")
            //Cal API for verification
            self.validateOkDollarNumberAPI(phoneNumber:strContactNumber)
        } else {
            _ = ContactGlobalConstants.Strings.phoneNumberNotAvaialable
        }
    }
}
