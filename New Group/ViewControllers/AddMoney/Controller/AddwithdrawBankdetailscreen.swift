//
//  AddwithdrawBankdetailscreen.swift
//  OK
//
//  Created by prabu on 17/06/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation

struct BankItem {
    var cardName: String
    var image: String
    
    init(cardName: String, img: String) {
        self.cardName = cardName
        self.image = img
    }
}

struct AddBankSection {
    var BankName: String
    var image: String
    var collapsed: Bool
    var items: [NearByServicesNewModel]
    
    init(sectionName: String, items: [NearByServicesNewModel], collapsed: Bool , img: String) {
        self.BankName = sectionName
        self.image = img
        self.collapsed = collapsed
        self.items = items

    }
}

private var longPressGestureRecognizer:UILongPressGestureRecognizer!

class AddwithdrawBankdetailscreen: OKBaseController ,MainNearByLocationViewDelegate,MainwheretoLocationViewDelegate,MainLocationViewDelegateRegistration{
    
    var arrAllBankList  = [NearByServicesNewModel]()
    var arrAllBankListsort  = [AddBankSection]()

    @IBOutlet weak var listTableView: UITableView!
    var intType : Int = 10
    var sections = [AddBankSection] ()

    var groupDic = Dictionary<String, Any>()
    let imageCache = NSCache<NSString, UIImage>()
    
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLabel: MarqueeLabel!
        {
        didSet
        {
            headerLabel.text = "Location".localized
            headerLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    //prabu
    let defaults = UserDefaults.standard
    var checkurl:String = ""
    var cashinoutstr:String = ""
    var cashType : Int = 0

    
    @IBOutlet weak var TopLocationBtn: UIButton!
        {
        didSet
        {
            self.TopLocationBtn.layer.masksToBounds = true
            self.TopLocationBtn.layer.borderWidth = 1.0
            self.TopLocationBtn.layer.borderColor = UIColor.white.cgColor
            self.TopLocationBtn.layer.cornerRadius = 20.0
            
        }
    }

    var selectedLocationDetailStr         : String?
    var selectedTownshipDetailStr         : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

       // print("cashinoutstr=======\(cashinoutstr)")
        if cashinoutstr == "CashIn"{
            cashType = 0
        }else {
            cashType = 1
        }
       // print("cashinoutstr=======\(cashType)")


        //Long Press
        longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGestureRecognizer.minimumPressDuration = 0.2
        longPressGestureRecognizer.numberOfTapsRequired = 1
        longPressGestureRecognizer.numberOfTouchesRequired = 1

        // longPressGestureRecognizer.delegate = self
        self.listTableView.addGestureRecognizer(longPressGestureRecognizer)
        
        self.didSelectLocationByCurrentLocation()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "Bank".localized
        
        self.navigationController?.navigationBar.isHidden = true
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    @IBAction func onClickBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handleLongPress(longPressGesture: UILongPressGestureRecognizer) {
        
        
        let p = longPressGesture.location(in: self.listTableView)
        let indexPath = self.listTableView.indexPathForRow(at: p)
        if indexPath == nil {
            print("Long press on table view, not row.")
        } else if longPressGesture.state == UIGestureRecognizer.State.began {
            print("Long press on row, at \(indexPath!.row)")
            
            let promotion = self.sections[indexPath!.section].items[indexPath!.row]
            let storyboard: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
            let protionDetailsView = storyboard.instantiateViewController(withIdentifier: "PromotionDetailView_ID") as! PromotionDetailViewController
            protionDetailsView.selectedNearByServicesNew = promotion
            protionDetailsView.strNearByPromotion = "NearByService"
          
            self.navigationController?.pushViewController(protionDetailsView, animated: true)

        }
    }
    

    
    @IBAction func topheaderLocationAction(_ sender: UIButton) {
        
        let storyBoardName = UIStoryboard(name: "Promotion", bundle: nil)
        guard let locationSelectionView = storyBoardName.instantiateViewController(withIdentifier: "SelecteMainLocationView_ID") as? SelectMainLocationViewController else { return }
        locationSelectionView.delegateRegistration = self
        locationSelectionView.delegatenearby = self
        locationSelectionView.delegatewhereto = self
        self.navigationController?.pushViewController(locationSelectionView, animated: true)
        
    }
    
    //MARK: - Location Manager Delegate
    
    func didSelectLocationByCurrentLocation() {
        updateCurrentLocationToUI()
        
        self.checkurl = "CurrentLocation"
        
        guard let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude else {
            // throw the error to the user
            return
        }
        
        self.getOfficeAPI(lat: lat , long: long )
        
    }
    
    func updateCurrentLocationToUI() {
        headerLabel.text = "\(userDef.value(forKey: "lastLoginKey") as? String ?? "")     "

        //headerLabel.text =  "          \(userDef.value(forKey: "lastLoginKey") as! String)          "
        defaults.set(userDef.value(forKey: "lastLoginKey") as? String ?? nil, forKey: "NBSCurrentLocation")

    }
    
    func didSelectNearByLocation() {
        
        self.checkurl = "NearBy"
        headerLabel.text = "Near By".localized
        
        
        guard let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude else {
            // throw the error to the user
            return
        }
        
        
        self.getOfficeAPI(lat: lat , long: long )
    }
    
    func didSelectwheretoLocation(streetName:String)
    {
        headerLabel.text = "     " + streetName + "     "
        
        geoLocManager.getLatLongByName(cityname: streetName) { (isSuccess, lat, long) in
            
            guard isSuccess, let latti = lat, let longi = long  else {
                //self.showErrorAlert(errMessage: "Try Again".localized)
                return
            }
            
            self.getOfficeAPI(lat:latti , long:longi)
            
        }
        
    }
    
    
    func didSelectLocationByTownshipRegistration(location: LocationDetail, township: TownShipDetailForAddress) {
        
        self.checkurl = "Township"
        
        if appDel.getSelectedLanguage() == "my" {
            headerLabel.text = township.townShipNameMY  + " , " + location.stateOrDivitionNameMy + "    "
            updateOtherLocationToUINew(location: location.stateOrDivitionNameMy, township: township.townShipNameMY)
        } else {
            headerLabel.text = location.stateOrDivitionNameEn + " , " + township.townShipNameEN + "    "
            updateOtherLocationToUINew(location: location.stateOrDivitionNameEn, township: township.townShipNameEN)
        }
        
        self.getOfficeAPI(lat: geoLocManager.currentLatitude , long: geoLocManager.currentLongitude)
        
    }
    
    
    func updateOtherLocationToUINew(location: String, township: String) {
        
        selectedLocationDetailStr = location
        selectedTownshipDetailStr = township
        
    }
    
    //MARK:- Get API Data
    
    func getOfficeAPI(lat: String, long: String)
    {
        println_debug("call ok offices api ")
        
        let url = getUrl(urlStr: "", serverType: .nearByServicesUrl)
    
        var jsonDic:[String : Any]
        
       // var lat = "16.81668401110357"
       // var long = "96.13187085779862"
        
        if checkurl ==  "NearBy" {
            
            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 300, "cashType" : cashType, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":[10], "phoneNumberNotToConsider":""] as [String : Any]
            
        } else if checkurl ==  "Township"{
            
            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1000, "cashType" : cashType, "amount" : 0, "searchAdditionalParams":["townShipName":selectedTownshipDetailStr,"cityName":"","businessName":"","country":"","divisionName":selectedLocationDetailStr,"line1":""],"paginationParams": ["offset":0,"limit":0],"types":[10], "phoneNumberNotToConsider":""] as [String : Any]
            
        } else {
            
            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1000, "cashType" : cashType, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":[10], "phoneNumberNotToConsider":""] as [String : Any]
            
        }
        print("OK$ServiceurlRequest Bankdetail========\(jsonDic)")
        
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url,methodType: kMethod_Post, contentType: kContentType_Json, inputVal: jsonDic, authStr: nil)
       /// print("OK$ServiceurlRequest========\(urlRequest)")
        
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
        
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                }
                return
            }
            
            println_debug("response dict for get all offices list :: \(response ?? "")")
            
            var cashArray = [NearByServicesNewModel]()
            DispatchQueue.main.async {
                cashArray = ParserAttributeClass.parseNearbyServiceJSONNew(anyValue: response as AnyObject) as [NearByServicesNewModel]
                println_debug("response count for ok offices :::: \(cashArray.count)")
                
                if cashArray.count > 0 {
                    self.arrAllBankList.removeAll()
                    
                    for indexVal in 0..<cashArray.count {
                        let currentModel = cashArray[indexVal]
    
                        if self.intType == currentModel.UserContactData!.type! {
                            
                            self.arrAllBankList.append(currentModel)
                            
                        }
                    }
                    print("innertype-------\(self.arrAllBankList.count)")
                    
                    if self.arrAllBankList.count > 0
                    {
                        
                        self.sections.removeAll()
                        self.groupDic.removeAll()
                        self.arrAllBankListsort.removeAll()
                        
                        self.groupDic = Dictionary(grouping: self.arrAllBankList, by: { $0.UserContactData!.BankName! })
                        
                        for indexValue in 0..<self.groupDic.count {
                            
                            let model = Array(self.groupDic.values)[indexValue] as! [NearByServicesNewModel]
                            let value =  AddBankSection(sectionName:Array(self.groupDic.keys)[indexValue], items:model, collapsed:true , img:model[0].UserContactData!.ImageUrl!)
                            self.arrAllBankListsort.append(value)
                            
                        }
                       /// print("arrAllBankListsort-------\(self.arrAllBankListsort.count)")
                        self.sections = self.arrAllBankListsort.sorted(by: { $0.BankName < $1.BankName })
                        
                       /// print("self.sections-------\(self.sections.count)")

                        self.listTableView.dataSource = self
                        self.listTableView.delegate = self
                        self.listTableView.reloadData()
                    }
                    else
                    {
                       // self.showHideInfoLabel(isShow: true, content: "")
                    }
                }
                else
                {
                    
                    
                    self.showErrorAlert(errMessage: "No Record Found".localized)
                    DispatchQueue.main.async {
                        progressViewObj.removeProgressView()
                    }
                    
                    self.arrAllBankListsort.removeAll()
                    self.sections.removeAll()
                    self.groupDic.removeAll()
                    self.listTableView.dataSource = self
                    self.listTableView.delegate = self
                    self.listTableView.reloadData()
                    
                   // self.showHideInfoLabel(isShow: true, content: "No Record Found".localized)
                }
            }
        })
    }
    
}


extension AddwithdrawBankdetailscreen:UITableViewDataSource, UITableViewDelegate{

    // Header
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 55.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
        let headerCell:AddMoneyHeaderCell = (tableView.dequeueReusableCell(withIdentifier: "AddMoneyHeaderCell") as? AddMoneyHeaderCell)!
        
        headerCell.lblTitle.text = self.sections[section].BankName + "     "
        
        let imageStr = self.sections[section].image
        let imageUrl = URL(string: imageStr)
        headerCell.ivSection.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "cicoCamera"))
        
        headerCell.btnHeaderSelection.tag = section
        headerCell.setCollapsed(self.sections[section].collapsed)
         headerCell.btnHeaderSelection.addTarget(self, action:#selector(self.headerSelectionAction(sender:)), for: .touchUpInside)
        
        headerCell.setArrow(self.sections[section].collapsed)
       
        return  headerCell.contentView
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if sections[(indexPath as NSIndexPath).section].collapsed {
            return 0
        } else {
            tableView.estimatedRowHeight = 250
            tableView.rowHeight = UITableView.automaticDimension
            return tableView.rowHeight
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print("numberofrow-------\(self.sections[section].items.count)")
        return self.sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "maplistcellidentifier"
        var listCell: MapListCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? MapListCell
        if listCell == nil {
            tableView.register (UINib(nibName: "MapListCell", bundle: nil), forCellReuseIdentifier: identifier)
            listCell = tableView.dequeueReusableCell(withIdentifier: identifier) as? MapListCell
        }
        
        let promotion = self.sections[indexPath.section].items[indexPath.row]
     
        listCell.callBtn.addTarget(self, action: #selector(callBtnAction(_:)), for: UIControl.Event.touchUpInside)
        
        listCell.photoImgView.image = nil
        if let imageStr = promotion.UserContactData?.ImageUrl, let imageUrl = URL(string: imageStr) {
            listCell.photoImgView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "cicoCamera"))
        }
        listCell.photoImgView.layer.borderWidth = 1.0
        listCell.photoImgView.layer.masksToBounds = false
        listCell.photoImgView.layer.borderColor = UIColor(red: 246/255, green: 198/255, blue: 0/225, alpha: 1).cgColor
        listCell.photoImgView.layer.cornerRadius = listCell.photoImgView.frame.size.width / 2
        listCell.photoImgView.clipsToBounds = true
        
        listCell.nameLbl.text = promotion.UserContactData?.BusinessName
        
        //print("phone number----\(promotion.UserContactData!.PhoneNumber)")
        let agentCodenew  = getPlusWithDestinationNum((promotion.UserContactData!.PhoneNumber)!, withCountryCode: "+95")
        if agentCodenew.length > 15 {
             listCell.phonenumberLbl.text = "NA"
        } else {
            listCell.phonenumberLbl.text = agentCodenew
        }
        if(promotion.UserContactData?.BusinessName!.count == 0)
        {
            listCell.nameLbl.text = promotion.UserContactData?.FirstName
        }
        
        if let openTime = promotion.UserContactData!.OfficeHourFrom , let closeTime = promotion.UserContactData!.OfficeHourTo {
            
            if openTime.count > 0 && closeTime.count > 0 {
                listCell.timeImgView.isHidden = false
                listCell.timeLbl.isHidden = false
                listCell.timeLblHeightConstraint.constant = 20
                listCell.timeLbl.text = "\(listCell.getTime(timeStr: openTime)) - \(listCell.getTime(timeStr: closeTime))"
            }else {
                listCell.timeImgView.isHidden = true
                listCell.timeLbl.isHidden = true
                listCell.timeLblHeightConstraint.constant = 0
            }
            
        }
        listCell.locationImgView.image = UIImage.init(named: "location.png")
        if let addressnew = promotion.UserContactData?.Address {
            if addressnew.count > 0 {
                listCell.locationLbl.text = "\(addressnew)"
            }else {
                listCell.locationLbl.text = "\(promotion.UserContactData!.Township!),\(promotion.UserContactData!.Division!)"
            }
        }
        
        //CashIn And CashOut
        
        if let cashinamount = promotion.UserContactData?.CashInLimit , let cashoutamount = promotion.UserContactData?.CashOutLimit {
            
            if cashinamount > 0 {
                let phone =  self.getNumberFormat("\(promotion.UserContactData!.CashInLimit!)").replacingOccurrences(of: ".00", with: "")
                listCell.cashInLbl.text = "Maximum Cash In Amount".localized + " : \(phone)" + " MMK"
                listCell.cashInLblHeightConstraint.constant = 20
            } else {
                listCell.cashInLblHeightConstraint.constant = 0
            }
            
            if cashoutamount > 0 {
                let phonenew =  self.getNumberFormat("\(promotion.UserContactData!.CashOutLimit!)").replacingOccurrences(of: ".00", with: "")
                listCell.cashOutLbl.text = "Maximum Cash Out Amount".localized + " : \(phonenew)" + " MMK"
                listCell.cashOutLblHeightConstraint.constant = 20
            } else{
                listCell.cashOutLblHeightConstraint.constant = 0
            }
        }
        
        listCell.callBtn.setImage(UIImage.init(named: "call.png"), for: UIControl.State.normal)
        listCell.distanceLblKm.text = "(0 Km)"
        if let shopDistance = promotion.distanceInKm {
            listCell.distanceLblKm.text = String(format: "(%.2f Km)", shopDistance).replacingOccurrences(of: ".00", with: "")
        }
        
        listCell.durationLbl.text = listCell.durationFromDistance(distance: promotion.distanceInKm) as String

        DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
        }
        return listCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let promotion = self.sections[indexPath.section].items[indexPath.row]
        let storyboard: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
        let protionDetailsView = storyboard.instantiateViewController(withIdentifier: "PromotionDetailView_ID") as! PromotionDetailViewController
        protionDetailsView.selectedNearByServicesNew = promotion
        protionDetailsView.strNearByPromotion = "NearByService"
        
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        
        print("cellforcontrollercount\(viewControllers)")
        print("cellforcontrollercount\(viewControllers.count)")
        
        if viewControllers.count < 4 {
        self.navigationController?.pushViewController(protionDetailsView, animated: true)
        }
    
    }
    
    @objc func headerSelectionAction(sender: UIButton!) {

        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
        
        self.toggleSection(section: sender.tag)
        
    }
    
   
    func toggleSection(section: Int) {
        
        let collapsed = !sections[section].collapsed
        //let collapsed = false
        // Toggle collapse
        self.sections[section].collapsed = collapsed
        // collapsed the opened section
        for i in 0..<self.sections.count {
            if i == section {
                continue
            }
            self.sections[i].collapsed = true
        }

        self.listTableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: UITableView.RowAnimation.fade)
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0, section: 0)
            self.listTableView.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    
   
    @objc func callBtnAction(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to: self.listTableView)
        if let indexPath = self.listTableView.indexPathForRow(at: buttonPosition), self.sections[indexPath.section].items.count > 0 {
            let selectedpromotion = self.sections[indexPath.section].items[indexPath.row]
            let promotion = selectedpromotion
            if let phonenumber = promotion.UserContactData?.PhoneNumber {
                if phonenumber.count > 0 && phonenumber.count < 15 {
                    var mobNumber = getPhoneNumberByFormat(phoneNumber: phonenumber)
                    mobNumber = mobNumber.removingWhitespaces()
                    let phone = "tel://\(mobNumber)"
                    if let url = URL.init(string: phone) {
                        UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    }
                }
            }
        }else {
            alertViewObj.wrapAlert(title: nil, body: "No Phonenumber Exists".localized, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            alertViewObj.showAlert(controller: self)
        }
    }


}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
