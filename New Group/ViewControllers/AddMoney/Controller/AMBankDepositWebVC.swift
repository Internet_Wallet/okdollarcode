//
//  AMBankDepositWebVC.swift
//  OK
//
//  Created by PC on 1/16/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import WebKit

class AMBankDepositWebVC: OKBaseController, WKNavigationDelegate,WKUIDelegate {
    
    @IBOutlet weak var viewLangOption: UIView!
 //   @IBOutlet weak var wv: UIWebView!
    
    @IBOutlet var wv: WKWebView!
    
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    
    var urlStr:String = ""
    var strTitleHeader:String = ""
    var boolUniZawg = false
    var boolLang = false
    var bankName = BankCounterDepositModel()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewLangOption.layer.cornerRadius = 10
        
        // border
        viewLangOption.layer.borderWidth = 1.0
        viewLangOption.layer.borderColor = UIColor.lightGray.cgColor
        
        // shadow
        viewLangOption.layer.shadowColor = UIColor.lightGray.cgColor
        viewLangOption.layer.shadowOffset = CGSize(width: 3, height: 3)
        viewLangOption.layer.shadowOpacity = 0.7
        viewLangOption.layer.shadowRadius = 10.0
        
            appDel.floatingButtonControl?.window.closeButton?.isHidden = true
            appDel.floatingButtonControl?.window.isHidden = true
        
        //Show change lang option
        viewLangOption.isHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        
         if #available(iOS 13, *) {
                  
                   UIApplication.statusBarBackgroundColor =  kYellowColor
                   
               } else {
                      let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
                      if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
                          statusBar.backgroundColor = kYellowColor//UIColor.init(hex: "F3C632")
                      }
               }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.helpSupportNavigationEnum = .Add_Withdraw
        wv.navigationDelegate = self
        wv.uiDelegate = self
        
       if #available(iOS 13, *) {
           
            UIApplication.statusBarBackgroundColor =  kYellowColor
            
        } else {
               let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
               if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
                   statusBar.backgroundColor = kYellowColor//UIColor.init(hex: "F3C632")
               }
        }
        
        self.title = strTitleHeader
        wv.load(URLRequest(url: URL(string: urlStr)!))
        
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
          showProgressView()
    }
  
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
          self.removeProgressView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- button Actionany
    
    @IBAction func onClickBackBtn(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickLangChangeBtn(_ sender: Any) {
        if boolLang {
            boolLang = false
            viewLangOption.isHidden = true
        } else {
            boolLang = true
            viewLangOption.isHidden = false
        }
        
    }
    
    @IBAction func selectedLanguage(_ sender: Any) {
        //self.overRideWebView()
        
        viewLangOption.isHidden = true

        let intSelection = (sender as AnyObject).tag
        boolLang = false
        viewLangOption.isHidden = true
        if intSelection == 0 {
            if strTitleHeader == "OK$ Deposit Money from Bank".localized
            {
                urlStr = "https://s3-ap-southeast-1.amazonaws.com/ok-dollar/bank-notes/how_to_topup_bank.html"
                wv.load(URLRequest(url: URL(string: urlStr)!))
            }
            else
            {
            urlStr = bankName.Notes
            wv.load(URLRequest(url: URL(string: urlStr)!))
            }
        } else if intSelection == 1 {
            
            if strTitleHeader == "OK$ Deposit Money from Bank".localized
            {
                urlStr = "https://s3-ap-southeast-1.amazonaws.com/ok-dollar/bank-notes/how_to_topup_bank_myanmar.html"
                wv.load(URLRequest(url: URL(string: urlStr)!))
            }
            else
            {
                urlStr = bankName.NotesBurmese
                wv.load(URLRequest(url: URL(string: urlStr)!))
            }
            
        } else {
            if strTitleHeader == "OK$ Deposit Money from Bank".localized
            {
                urlStr = "https://s3-ap-southeast-1.amazonaws.com/ok-dollar/bank-notes/how_to_topup_bank_myanmar.html"
                wv.load(URLRequest(url: URL(string: urlStr)!))
            }
            else
            {
                urlStr = bankName.NotesBurmese
                if urlStr.contains(find: "MAB") || urlStr.contains(find: "UAB") {
                    let tempStr = urlStr.replacingOccurrences(of: ".html", with: "__zw.html")
                    wv.load(URLRequest(url: URL(string: tempStr)!))
                } else {
                    let tempStr = urlStr.replacingOccurrences(of: ".html", with: "_zw.html")
                    wv.load(URLRequest(url: URL(string: tempStr)!))
                }
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
