//
//  AMPaymentDetailsVC.swift
//  OK
//
//  Created by PC on 1/18/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import WebKit

enum PaymentType {
    case MPU
    case VisaMasterPay
    case KBZBANK
    case CBBANK
    case JCB
    case UnionPay
    case OneTwoThree
}

class AMPaymentDetailsVC: OKBaseController {
    
    @IBOutlet weak var webView: UIWebView!
    
    var newWebViewShare: UIWebView!
    //@IBOutlet var webView: WKWebView!
    @IBOutlet var aView: UIView!
    var type : PaymentType?
    var dictPaymentDetails : Dictionary<String,Any>!
    var strWebUrl  = ""
    
   // MARK: -View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.title = "Payment Details".localized
        // Remove all cache
        URLCache.shared.removeAllCachedResponses()

        // Delete any associated cookies
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }
        }
        
        // Do any additional setup after loading the view.
        self.helpSupportNavigationEnum = .Add_Withdraw
        //println_debug(dictPaymentDetails)
        //         webView.navigationDelegate = self
        //         webView.uiDelegate = self
        
        webView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
             
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        
        view.bringSubviewToFront(aView)
        //println_debug(dictPaymentDetails)
        
        if #available(iOS 13, *) {
            UIApplication.statusBarBackgroundColor =  kYellowColor
        } else {
            let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
            if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
                statusBar.backgroundColor = kYellowColor//UIColor.init(hex: "F3C632")
            }
        }
        
//        webView.scalesPageToFit = true
//        webView.scrollView.bouncesZoom = false
//        webView.scrollView.bounces = false
//        webView.scrollView.contentSize = CGSize.init(width: screenWidth, height: webView.scrollView.contentSize.height)
        
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        appDel.floatingButtonControl?.window.isHidden = true
//        let urlPath = "https://www.bbc.com/"
//           let requestUrl = NSURL(string: urlPath)
//           let request = NSURLRequest(url: requestUrl! as URL)
//           webView.loadRequest(request as URLRequest)
//        webView.delegate = self
        
        self.loadUrl(urlType: type!)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        
        if #available(iOS 13, *) {
            UIApplication.statusBarBackgroundColor =  .clear
               } else {
                     if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                          statusbar.backgroundColor = UIColor.clear
                      }
               }
       }
    
    func loadUrl(urlType: PaymentType) {
        switch urlType {
            
        case .MPU:
            let url = getUrl(urlStr: "", serverType: .AddMoney_PaymentMPU)
            let request = self.giftCardLoader(urlString: url.absoluteString)
            println_debug(request.debugDescription)
            self.webView.loadRequest(request)
            break
            
        case .VisaMasterPay:
            let url = getUrl(urlStr: "", serverType: .AddMoney_PaymentVisaMaster)
            let request = self.giftCardLoader(urlString: url.absoluteString)
            println_debug(request.debugDescription)
            self.webView.loadRequest(request)
            break
            
        case .KBZBANK:
            let url = getUrl(urlStr: "", serverType: .AddMoney_PaymentKBZ)
            let request = self.giftCardLoader(urlString: url.absoluteString)
            println_debug(request.debugDescription)
            self.webView.loadRequest(request)
            break
            
        case .CBBANK:
            let url = getUrl(urlStr: "", serverType: .AddMoney_PaymentCB)
            let request = self.giftCardLoader(urlString: url.absoluteString)
            println_debug(request.debugDescription)
            self.webView.loadRequest(request)
            break
            
        case .JCB:
            let url = getUrl(urlStr: "", serverType: .AddMoney_PaymentVisaMaster)
            let request = self.giftCardLoader(urlString: url.absoluteString)
            println_debug(request.debugDescription)
            self.webView.loadRequest(request)
            break
            
        case .UnionPay:
            let url = getUrl(urlStr: "", serverType: .AddMoney_PaymentVisaMaster)
            let request = self.giftCardLoader(urlString: url.absoluteString)
            println_debug(request.debugDescription)
            self.webView.loadRequest(request)
            break
            
        case .OneTwoThree:
            let url = getUrl(urlStr: "", serverType: .AddMoney_PaymentOneTwoThree)
            let request = self.giftCardLoader(urlString: url.absoluteString)
            println_debug(request.debugDescription)
            self.webView.loadRequest(request)
            break
        }
    }
    
    func giftCardLoader(urlString: String) -> URLRequest {
        
        // Remove all cache
        URLCache.shared.removeAllCachedResponses()
        
        // Delete any associated cookies
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }
        }
        
        let url = URL.init(string: urlString)
        var request = URLRequest.init(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        if type == PaymentType.MPU {
            let param = dictPaymentDetails
            let postData: Data? = try? JSONSerialization.data(withJSONObject: param as Any, options: [])
            if postData != nil {    request.httpBody = postData }
            
        } else if type == PaymentType.VisaMasterPay {
            let param = dictPaymentDetails
            let postData: Data? = try? JSONSerialization.data(withJSONObject: param as Any, options: [])
            if postData != nil {    request.httpBody = postData
                
            }
        } else if type == PaymentType.KBZBANK {
            let param = dictPaymentDetails
            let postData: Data? = try? JSONSerialization.data(withJSONObject: param as Any, options: [])
            if postData != nil {    request.httpBody = postData
                
            }
        } else if type == PaymentType.JCB {
            let param = dictPaymentDetails
            let postData: Data? = try? JSONSerialization.data(withJSONObject: param as Any, options: [])
            if postData != nil {    request.httpBody = postData
                
            }
        } else {
            let param = dictPaymentDetails
            let postData: Data? = try? JSONSerialization.data(withJSONObject: param as Any, options: [])
            if postData != nil {    request.httpBody = postData }
        }
        return request
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        if strWebUrl.contains("FailureResult") || strWebUrl.contains("SuccessResult") || strWebUrl.contains("ErrorPage") //VISA
        {
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        if webView.canGoBack {
            webView.goBack()
        } else {
            webView.stopLoading()
            webView = nil
            //self.dismiss(animated: true, completion: nil)
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension AMPaymentDetailsVC: UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        println_debug("Started")
        PTLoader.shared.show()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        println_debug("Finished")
        PTLoader.shared.hide()
        
        if let text = webView.request?.url?.absoluteString{
            println_debug(text)
            strWebUrl = text
            if text.contains("FailureResult"){
                println_debug("Yes it contains 'help'")
                self.navigationController?.popViewController(animated: true)
            }
            else if text.contains("k=apphomedashboard")
            {
                println_debug("apphomedashboard")
                self.navigationController?.popViewController(animated: true)
            }
            else if text.contains("http://test.mobileview.okdollar.org/NetBanking/Mpu/LastGuide"){
                println_debug("screenfullview")
                shareWebURL()
            }
        }
    }
    
    func shareWebURL() {
        
        let webViewFrame = webView.frame
        
        webView.frame = CGRect(x: webViewFrame.origin.x, y: webViewFrame.origin.y, width: webView.scrollView.contentSize.width, height: webView.scrollView.contentSize.height)
        UIGraphicsBeginImageContextWithOptions(webView.scrollView.contentSize, false, 0);
        self.webView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image:UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
        UIImageWriteToSavedPhotosAlbum(image, self, nil, nil)
        
        var activityItems = [Any]()
        activityItems.append(image)
        let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo]
            self.present(activityViewController, animated: true, completion: {
            })
        
        //this code will work when i will press the cross button of activity view controller
        activityViewController.completionWithItemsHandler = { (activityType, completed:Bool, returnedItems:[Any]?, error: Error?) in
            if !completed {
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: false)
                }
            }
        }
        
        /*
         var activityItems = [Any]()
         //activityItems.append("Web Share")
         activityItems.append(image)
         let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
         activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo]
         
         DispatchQueue.main.async {
         self.present(activityViewController, animated: true, completion: {
         })
         }
         
         //this code will work when i will press the cross button of activity view controller
         activityViewController.completionWithItemsHandler = { (activityType, completed:Bool, returnedItems:[Any]?, error: Error?) in
         //if !completed {
         DispatchQueue.main.async {
         //self.navigationController?.popViewController(animated: false)
         self.webView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
         self.navigationController?.popViewController(animated: true)
         }
         //}
         }
         */
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        println_debug(error)
        PTLoader.shared.hide()
    }
    
    /*
     func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
     println_debug(error)
     PTLoader.shared.hide()
     }
     func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
     println_debug("Started")
     PTLoader.shared.show()
     }
     func webView(_ webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
     println_debug("Finished")
     PTLoader.shared.hide()
     
     //        if let text = webView.request?.url?.absoluteString{
     //            println_debug(text)
     //
     //            if text.contains("master"){
     //                println_debug("Yes it contains 'help'")
     //                //self.dismiss(animated: true, completion: nil)
     //            }
     //        }
     }
     
     
     
     func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
     return true
     }
     */
    
    /*
     private func webView(webView: WKWebView!, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError!) {
     let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
     alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
     present(alert, animated: true, completion: nil)
     }
     func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
     //   println_debug("Started")
     
     print("load")
     PTLoader.shared.show()
     }
     
     
     //        func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
     //            if let text = navigationAction.request.url?.absoluteString {
     //                       //urlStr is what you want
     //                       println_debug(text)
     //
     //                                   if text.contains("nativeappformultiple"){
     //                                       println_debug("Yes it contains 'help'")
     //                                       self.dismiss(animated: true, completion: nil)
     //                                   }
     //                   }
     //
     //                }
     
     func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
     //   println_debug("Finished")
     print("finish")
     PTLoader.shared.hide()
     
     
     
     }
     */
}

extension UIApplication {

class var statusBarBackgroundColor: UIColor? {
    get {
        return statusBarUIView?.backgroundColor
    } set {
        statusBarUIView?.backgroundColor = newValue
    }
}

class var statusBarUIView: UIView? {
    if #available(iOS 13.0, *) {
        let tag = 987654321

        if let statusBar = UIApplication.shared.keyWindow?.viewWithTag(tag) {
            return statusBar
        }
        else {
            let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
            statusBarView.tag = tag

            UIApplication.shared.keyWindow?.addSubview(statusBarView)
            return statusBarView
        }
    } else {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
    }
    return nil
}}

