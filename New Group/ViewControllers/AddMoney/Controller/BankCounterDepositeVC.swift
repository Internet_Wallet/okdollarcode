//
//  BankCounterDepositeVC.swift
//  OK
//
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit

class BankCounterDepositeVC:  OKBaseController {
    
    @IBOutlet weak var tblList: UITableView!
    let appDel = UIApplication.shared.delegate as! AppDelegate

    @IBOutlet weak var btnTopUpViaBank: UIButton!
    {
        didSet
        {
            self.btnTopUpViaBank.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.btnTopUpViaBank.setTitle("OK$ Deposit Money from Bank".localized, for: .normal)
        }
    }
    @IBOutlet weak var btnCodeGallery: UIButton!
        {
        didSet
        {
            self.btnCodeGallery.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.btnCodeGallery.setTitle(self.btnCodeGallery.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var viewCodeGallery: UIView!
    var arrStandardBankDeposit = [BankCounterDepositModel]()
    var arrGeneralBankDeposit = [BankCounterDepositModel]()
    var strBearer = ""
    var strHashValue = ""
    //Show&Hide More Option
    var isMoreStatus:Bool = false

  //MARK: - View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        self.title = "Bank Counter Deposit Add".localized
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
        
        viewCodeGallery.isHidden = true
        
            appDel.floatingButtonControl?.window.closeButton?.isHidden = true
            appDel.floatingButtonControl?.window.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.helpSupportNavigationEnum = .Add_Withdraw
        
      
        
        //self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        // Do any additional setup after loading the view.
        self.getAccessToken()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Button Action
    @IBAction func topUpOKAction(_ sender: Any) {
        
        let controller:AMBankDepositWebVC = UitilityClass.getStoryBoardAddWithdraw().instantiateViewController(withIdentifier: "AMBankDepositWebVC") as! AMBankDepositWebVC
        controller.strTitleHeader = "OK$ Deposit Money from Bank".localized
        if self.appDel.getSelectedLanguage() == "en"
        {
            controller.urlStr = "https://s3-ap-southeast-1.amazonaws.com/ok-dollar/bank-notes/how_to_topup_bank.html"
        }
        else
        {
            controller.urlStr = "https://s3-ap-southeast-1.amazonaws.com/ok-dollar/bank-notes/how_to_topup_bank_myanmar.html"
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func onClickBackBtn(_ sender: Any) {
        
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCodeGalleryAction(_ sender: Any) {
    
        viewCodeGallery.isHidden = true

        let controller:DepositCodeGalleryVC = UitilityClass.getStoryBoardAddWithdraw().instantiateViewController(withIdentifier: "DepositCodeGalleryVC") as! DepositCodeGalleryVC
        controller.strHeaderAuthorization = strBearer
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func onClickMoreBtn(_ sender: Any) {
        println_debug("onClickMoreBtn")
      if isMoreStatus == false
      {
        isMoreStatus = true
        viewCodeGallery.isHidden = false
      }
      else
      {
        isMoreStatus = false
        viewCodeGallery.isHidden = true
      }
    }
    
    // MARK: - API Integration
    
    func getAccessToken() {
        println_debug("getAccessToken")
        if appDelegate.checkNetworkAvail() {
            showProgressView()
           //let tokenUrl = URL.init(string:  Url.AddMoney_GetBanksToken)
            let urlStr   = Url.AddMoney_GetBanksToken
            let tokenUrl = getUrl(urlStr: urlStr, serverType: .bankCounterDepositUrl)
            let hashValue = Url.aKey_AddMoneyBank.hmac_SHA1(key: Url.sKey_AddMoneyBank)
            strHashValue = hashValue
            let inputValue = "password=\(hashValue)&grant_type=password"
            let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: "POST", contentType: LLBStrings.kContentType_urlencoded, inputVal: inputValue, authStr: nil)
            JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
                
                guard isSuccess else {
                    println_debug("Token API Fail")
                    self.removeProgressView()
                    return
                }
                println_debug("\n\n\n Token Response dict : \(response!)")
                let responseDic = response as! NSDictionary
                guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                    self.removeProgressView()
                    return
                }
                let authorizationString =  "\(tokentype) \(accesstoken)"
                println_debug(authorizationString)
                self.removeProgressView()
                self.callGetAllBanks(tokenType:tokentype as! String,accesstoken:accesstoken as! String)
            })
        }
    }
    
    func callGetAllBanks(tokenType:String,accesstoken:String) {
        
//        guard let tokenUrl = URL.init(string:  Url.AddMoney_GetBanks) else
//        {
//            return
//        }
        
        let urlStr   = Url.AddMoney_GetBanks
        let tokenUrl = getUrl(urlStr: urlStr, serverType: .bankCounterDepositUrl)
        
        let tokenType = tokenType
        let token =  accesstoken
        
        let header = tokenType + " " + token
        strBearer = header
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: "GET", contentType: LLBStrings.kContentType_Json, inputVal: nil, authStr: header)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else  {
                println_debug(" API Fail")
                self.removeProgressView()
                return
            }
             let dictTemp = response as! Dictionary<String, Any>
            
            if dictTemp["Message"] as? String ?? "" == "An error has occurred." {
                println_debug(" API Fail")
                self.removeProgressView()
                return
            }
            
            let arrTemp = response as! [Dictionary<String, Any>]
            println_debug("\n\n\n Array Data : \(arrTemp)")
            
            for y in 0..<arrTemp.count {
                
                let dict = arrTemp[y]
                
                println_debug(dict)
               let isManualDeposit = dict["IsManualDeposit"] as? Bool
                let bankModel = BankCounterDepositModel()
                
                bankModel.BankId = (dict["BankId"] as? String ?? "")
                bankModel.BankName = (dict["BankName"] as? String ?? "")
                bankModel.BankNameBurmese = (dict["BankNameBurmese"] as? String ?? "")
                bankModel.BankCode = (dict["BankCode"] as? String ?? "")
                bankModel.Notes = (dict["Notes"] as? String ?? "")
                bankModel.NotesBurmese = (dict["NotesBurmese"] as? String ?? "")
                bankModel.PersonalAmountLimit = (dict["PersonalAmountLimit"] as? Int ?? 0)
                bankModel.BusinessAmountLimit = (dict["BusinessAmountLimit"] as? Int ?? 0)
                bankModel.AdvanceMerchantLimit = (dict["AdvanceMerchantAmountLimit"] as? Int ?? 0)
                bankModel.AgentLimit = (dict["AgentAmountLimit"] as? Int ?? 0)
                
                if isManualDeposit! {

                        self.arrStandardBankDeposit.append(bankModel)
                } else {
                        self.arrGeneralBankDeposit.append(bankModel)
                }
            }
            
            self.arrGeneralBankDeposit = self.arrGeneralBankDeposit.sorted(by: { (amount1: BankCounterDepositModel, amount2: BankCounterDepositModel) -> Bool in
                return amount1.BankName < amount2.BankName
            })
            
            self.arrStandardBankDeposit = self.arrStandardBankDeposit.sorted(by: { (amount1: BankCounterDepositModel, amount2: BankCounterDepositModel) -> Bool in
                return amount1.BankName < amount2.BankName
            })
            
             DispatchQueue.main.async {
                    self.tblList.reloadData()
            }
            self.removeProgressView()
            
        })
    }
    
}

extension BankCounterDepositeVC:UITableViewDataSource, UITableViewDelegate{
    
    // Header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerCell = (tableView.dequeueReusableCell(withIdentifier: "bankCounterHeaderCell") )!
        let lblTitle = headerCell.contentView.viewWithTag(1) as? UILabel
        
        if section == 0{
            
            lblTitle?.text = "With Code".localized
            
        }else{
            lblTitle?.text = "Without Code".localized
        }
        
        return headerCell
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if section == 0{
            return arrGeneralBankDeposit.count
        }
        
        return arrStandardBankDeposit.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "bankCounterCell", for: indexPath as IndexPath)
        
        let lblBankname = cell.contentView.viewWithTag(1) as? UILabel
        let lblBankAbbriviation = cell.contentView.viewWithTag(2) as? UILabel
        let viewSeperator = cell.contentView.viewWithTag(3)

        var bankName = BankCounterDepositModel()
        
        if indexPath.section == 0 {
            bankName = arrGeneralBankDeposit[indexPath.row]
            lblBankname?.font = UIFont(name: appFont, size: appFontSize)
            
            if self.appDel.getSelectedLanguage() == "en"
            {
                lblBankname?.text = bankName.BankName
            }
            else
            {
                lblBankname?.text = bankName.BankNameBurmese
            }
            lblBankAbbriviation?.font = UIFont(name: appFont, size: appFontSize)
            lblBankAbbriviation?.text = "(" + bankName.BankCode + ")"
            if(indexPath.row == arrGeneralBankDeposit.count-1)
            {
                viewSeperator?.isHidden = true
            }
            else
            {
                viewSeperator?.isHidden = false
            }
        }else{
            bankName = arrStandardBankDeposit[indexPath.row]
            lblBankname?.font = UIFont(name: appFont, size: appFontSize)
            if self.appDel.getSelectedLanguage() == "en"
            {
                lblBankname?.text = bankName.BankName
            }
            else
            {
                lblBankname?.text = bankName.BankNameBurmese
            }
            lblBankAbbriviation?.font = UIFont(name: appFont, size: appFontSize)
            lblBankAbbriviation?.text = "(" + bankName.BankCode + ")"
            viewSeperator?.isHidden = false
        }
        cell.selectionStyle = .none

        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var bankName = BankCounterDepositModel()
        
        if indexPath.section == 0{
           //Load Bank deposit screen
            bankName = arrGeneralBankDeposit[indexPath.row]
            bankName.Bearer = strBearer
            bankName.HashValue = strHashValue

            let controller:BankCounterDepositDetailsVC = UitilityClass.getStoryBoardAddWithdraw().instantiateViewController(withIdentifier: "BankCounterDepositDetailsVC") as! BankCounterDepositDetailsVC
            controller.bankDetailsModel =  bankName
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else
        {
            bankName = arrStandardBankDeposit[indexPath.row]

            let controller:AMBankDepositWebVC = UitilityClass.getStoryBoardAddWithdraw().instantiateViewController(withIdentifier: "AMBankDepositWebVC") as! AMBankDepositWebVC
            controller.bankName = bankName
            if appDel.currentLanguage == "en"
            {
                controller.urlStr = bankName.Notes
            }
            else
            {
                controller.urlStr = bankName.NotesBurmese
            }
            controller.strTitleHeader = bankName.BankCode + " Bank".localized
            self.navigationController?.pushViewController(controller, animated: true)
                        
        }
    }
    
}
