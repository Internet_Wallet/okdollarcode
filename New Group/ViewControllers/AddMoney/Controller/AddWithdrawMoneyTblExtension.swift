//
//  AddWithdrawMoneyTblExtension.swift
//  OK
//
//  Created by SHUBH on 12/13/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit
import Rabbit_Swift
import CoreLocation

class AddWithdrawMoneyTblExtension: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        }

}

extension AddWithdrawMoneyVC: UITableViewDataSource, UITableViewDelegate {
    
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    if(tableView == tblOptionsList) {
      return 0.0
    }
    return 1.0
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if(tableView == tblOptionsList) {
      return 55.0
    } else {
      if(section != 2) {
        return 55.0
      } else {
        return 1
      }
    }
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if(tableView == tblOptionsList) {
      return sections[(indexPath as NSIndexPath).section].collapsed ? 0 : 55
    } else {
      if(indexPath.section == 1) {
        if(isSelectOkAccount) {
          if(isMyAccountSelected) {
            return 165
          } else {
            if(isContactSelected) {
              if(indexPath.row == 0) {
                return 55
              }
              return 110
            } else if(isConfirmMobileNoShow) {
              if(indexPath.row == 2) {
                return 110
              } else {
                return 56
              }
            } else if(isOtherAccountSelected) {
              return 55
            }
          }
          return 55
        }
        return 0
      } else if(indexPath.section == 2) {
        return 227
      } else {
        return 0
      }
    }
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    if(tableView == tblOptionsList) {
      return sections.count
    } else {
      if(isRemark) {
        return 3
      }
      return 2
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if(tableView == tblOptionsList) {
      return sections[section].collapsed ? 0 : sections[section].items.count
    } else {
      if(section == 1) {
        if(isSelectOkAccount) {
          if(isMyAccountSelected) {
            return 1
          } else {
            if(isContactSelected)
            {
              return 2
            } else if(isConfirmMobileNoShow)  {
              if(isOtherAccountShow)
              {
                return 3
              }
              return 2
            } else if(isOtherAccountSelected) {
              return 1
            }
          }
          return 2
        }
        
        return 0
      } else if(section == 2) {
        return 1
      }
      return 0
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if(tableView == tblOptionsList) {
        let cell:AddMoneyCell = tableView.dequeueReusableCell(withIdentifier: "AddMoneyCell", for: indexPath as IndexPath) as! AddMoneyCell
        let item: Item = sections[(indexPath as NSIndexPath).section].items[(indexPath as NSIndexPath).row]
        //Load data in custom cell
        cell.customCellUpdate(item: item, indexPath: indexPath)
        cell.selectionStyle = .none
        return cell
    } else {
      if(indexPath.section == 1) {
        
        if(isMyAccountSelected) {
          let identifier = "AMMyAccountSelectedCell"
          var cellView: AMMyAccountSelectedCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? AMMyAccountSelectedCell
          if cellView == nil {
            tableView.register (UINib(nibName: "AMMyAccountSelectedCell", bundle: nil), forCellReuseIdentifier: identifier)
            cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? AMMyAccountSelectedCell
          }
            cellView.btnMyAmountCancel.tag = 66
            cellView.btnMyAmountCancel.addTarget(self, action:#selector(self.btnMyAmountCancelAction(sender:)), for: .touchUpInside)
                         
          cellView.txtAccountNumber.text = self.getActualNum(UserModel.shared.mobileNo,withCountryCode: "+95")
          cellView.txtAccountName.text = UserModel.shared.name
            
          cellView.txtAmount.delegate = self
          cellView.txtAmount.tag = 60
          cellView.txtAmount.addTarget(self, action: #selector(AddWithdrawMoneyVC.textFieldDidChange(_:)),
                                       for: UIControl.Event.editingChanged)
          addMoneyModel.depositorContactNumber = UserModel.shared.mobileNo
          
          cellView.txtAccountNumber.placeholder = cellView.txtAccountNumber.placeholder?.localized
          cellView.txtAccountName.placeholder = cellView.txtAccountName.placeholder?.localized
          
          if(strEnterAmountValue.length > 0) {
            cellView.txtAmount.becomeFirstResponder()
            cellView.txtAmount.placeholder = "Amount".localized
            cellView.btnMyAmountCancel.isHidden = false
            if(strEnterAmountValue.length == 9) {
              cellView.txtAmount.textColor = UIColor.green
            } else if(strEnterAmountValue.length >= 10) {
              cellView.txtAmount.textColor = UIColor.red
            } else {
              cellView.txtAmount.textColor = UIColor.black
            }
          } else {
            cellView.txtAmount.placeholder = "Enter Amount".localized
            cellView.btnMyAmountCancel.isHidden = true
          }
          cellView.txtAmount.text = strEnterAmountValue
          cellView.selectionStyle = .none
          return cellView
        }
        else if(isOtherAccountSelected) {
      
            if(indexPath.row == 0)  {
            let identifier = "AMAccountMobileNoCell"
            var cellView: AMAccountMobileNoCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? AMAccountMobileNoCell
            if cellView == nil {
              tableView.register (UINib(nibName: "AMAccountMobileNoCell", bundle: nil), forCellReuseIdentifier: identifier)
              cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? AMAccountMobileNoCell
            }
            cellView.btnContact.tag = 73
            cellView.btnContact.addTarget(self, action:#selector(self.btnContactAction(sender:)), for: .touchUpInside)
            cellView.btnCancel.tag = 52
            cellView.btnCancel.addTarget(self, action:#selector(self.btnCancelAction(sender:)), for: .touchUpInside)
            
                cellView.txtAccountMobNo.delegate = self
                cellView.txtAccountMobNo.tag = 50
                cellView.txtAccountMobNo.becomeFirstResponder()
                cellView.txtAccountMobNo.addTarget(self, action: #selector(BankCounterDepositDetailsVC.textFieldDidChange(_:)),
                                                   for: UIControl.Event.editingChanged)
                cellView.txtAccountMobNo.text = addMoneyModel.depositorContactNumber
                
                if((cellView.txtAccountMobNo.text?.count)! < 2)
                {
                  cellView.txtAccountMobNo.placeholder = "Enter OK$ Account Mobile Number".localized
                  cellView.btnCancel.isHidden = true
                }
                else
                {
                  cellView.txtAccountMobNo.placeholder = "OK$ Account Mobile Number".localized
                  cellView.btnCancel.isHidden = true
                }
                cellView.txtAccountMobNo.becomeFirstResponder()
                
                /*
            cellView.txtAccountMobNo.placeholder = cellView.txtAccountMobNo.placeholder?.localized
            cellView.txtAccountMobNo.delegate = self
            cellView.txtAccountMobNo.tag = 50
            cellView.txtAccountMobNo.addTarget(self, action: #selector(AddWithdrawMoneyVC.textFieldDidChange(_:)),
                                             for: UIControl.Event.editingChanged)
            //cellView.txtAccountMobNo.text = strEnterMobileValue
            
            cellView.txtAccountMobNo.text = addMoneyModel.depositorContactNumber
                
            if(addMoneyModel.depositorContactNumber.length > 0) {
                cellView.txtAccountMobNo.placeholder = "OK$ Account Mobile Number".localized
            }
            else
            {
                cellView.txtAccountMobNo.placeholder = "Enter OK$ Account Mobile Number".localized
                cellView.txtAccountMobNo.text = "09"
            }
           
            cellView.txtAccountMobNo.becomeFirstResponder()
            cellView.txtAccountMobNo.isUserInteractionEnabled = true
                
            if(cellView.txtAccountMobNo.text?.length == 2 || cellView.txtAccountMobNo.text?.length == 0)
            {
                cellView.btnCancel.isHidden = true
            }
            else
            {
                cellView.btnCancel.isHidden = false
            }
            //cellView.txtAccountMobNo.text = addMoneyModel.depositorContactNumber
                 */
            cellView.selectionStyle = .none
            return cellView
          } else if(indexPath.row == 1) {
            //Display data while selected from contact list
            if(isContactSelected) {
              let identifier = "AMOtherAccountSelectedCell"
              var cellView: AMOtherAccountSelectedCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? AMOtherAccountSelectedCell
              if cellView == nil {
                tableView.register (UINib(nibName: "AMOtherAccountSelectedCell", bundle: nil), forCellReuseIdentifier: identifier)
                cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? AMOtherAccountSelectedCell
              }
              cellView.btnOtherAmountCancel.tag = 64
              cellView.btnOtherAmountCancel.addTarget(self, action:#selector(self.btnOtherAmountCancelAction(sender:)), for: .touchUpInside)
              cellView.txtAccountName.placeholder = cellView.txtAccountName.placeholder?.localized
              cellView.txtAccountName.delegate = self
              cellView.txtAccountName.tag = 61
              cellView.txtAccountName.text = addMoneyModel.okACOwnername
              cellView.txtAmount.delegate = self
              cellView.txtAmount.tag = 62
              cellView.txtAmount.addTarget(self, action: #selector(AddWithdrawMoneyVC.textFieldDidChange(_:)),
                                           for: UIControl.Event.editingChanged)
              cellView.txtAmount.text = strEnterAmountValue
              if(strEnterAmountValue.length > 0) {
                cellView.txtAmount.becomeFirstResponder()
                cellView.txtAmount.placeholder = "Amount".localized
                cellView.btnOtherAmountCancel.isHidden = false
                if(strEnterAmountValue.length == 9) {
                  cellView.txtAmount.textColor = UIColor.green
                } else if(strEnterAmountValue.length == 10) {
                  cellView.txtAmount.textColor = UIColor.red
                } else {
                  cellView.txtAmount.textColor = UIColor.black
                }
              } else {
                cellView.txtAmount.placeholder = "Enter Amount".localized
                cellView.btnOtherAmountCancel.isHidden = true
              }
              cellView.selectionStyle = .none
              return cellView
            } else { //Display confirmation Mobile number
              let identifier = "AMConfirmAccountMobNoCell"
              var cellView: AMConfirmAccountMobNoCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? AMConfirmAccountMobNoCell
              if cellView == nil {
                tableView.register (UINib(nibName: "AMConfirmAccountMobNoCell", bundle: nil), forCellReuseIdentifier: identifier)
                cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? AMConfirmAccountMobNoCell
              }
              cellView.btnConfirmCancel.tag = 53
              cellView.btnConfirmCancel.addTarget(self, action:#selector(self.btnCancelAction(sender:)), for: .touchUpInside)
                cellView.txtConfirmMobileNo.placeholder = cellView.txtConfirmMobileNo.placeholder?.localized
              cellView.txtConfirmMobileNo.delegate = self
              cellView.txtConfirmMobileNo.tag = 51
              cellView.txtConfirmMobileNo.becomeFirstResponder()
                
              cellView.txtConfirmMobileNo.addTarget(self, action: #selector(AddWithdrawMoneyVC.textFieldDidChange(_:)),
                                                    for: UIControl.Event.editingChanged)
              cellView.txtConfirmMobileNo.text = addMoneyModel.depositorConfirmContactNumber
              
                if(cellView.txtConfirmMobileNo.text?.length == 2 || cellView.txtConfirmMobileNo.text?.length == 0)
                {
                    cellView.btnConfirmCancel.isHidden = true
                }
                else
                {
                    cellView.btnConfirmCancel.isHidden = false
                }
                
                cellView.selectionStyle = .none
              return cellView
            }
          } else {
            let identifier = "AMOtherAccountSelectedCell"
            var cellView: AMOtherAccountSelectedCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? AMOtherAccountSelectedCell
            if cellView == nil {
              tableView.register (UINib(nibName: "AMOtherAccountSelectedCell", bundle: nil), forCellReuseIdentifier: identifier)
              cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? AMOtherAccountSelectedCell
            }
            cellView.btnOtherAmountCancel.tag = 65
            cellView.btnOtherAmountCancel.addTarget(self, action:#selector(self.btnOtherAmountCancelAction(sender:)), for: .touchUpInside)
                            
            cellView.txtAccountName.placeholder = cellView.txtAccountName.placeholder?.localized
            cellView.txtAccountName.delegate = self
            cellView.txtAccountName.tag = 61
            cellView.txtAccountName.text = addMoneyModel.receiverName
            cellView.txtAmount.delegate = self
            cellView.txtAmount.tag = 63
            cellView.txtAmount.addTarget(self, action: #selector(AddWithdrawMoneyVC.textFieldDidChange(_:)),
                                         for: UIControl.Event.editingChanged)
            cellView.txtAmount.text = strEnterAmountValue
            if(strEnterAmountValue.length > 0) {
              cellView.txtAmount.becomeFirstResponder()
              cellView.txtAmount.placeholder = "Amount".localized
                cellView.btnOtherAmountCancel.isHidden = false
              if(strEnterAmountValue.length == 9) {
                cellView.txtAmount.textColor = UIColor.green
              } else if(strEnterAmountValue.length == 10) {
                cellView.txtAmount.textColor = UIColor.red
              } else {
                cellView.txtAmount.textColor = UIColor.black
              }
            } else {
              cellView.txtAmount.placeholder = "Enter Amount".localized
                cellView.btnOtherAmountCancel.isHidden = true
            }
            cellView.selectionStyle = .none
            return cellView
          }
        } else  {
          if(indexPath.row == 0) {
            let identifier = "MyOkAccountCell"
            var cellView: MyOkAccountCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? MyOkAccountCell
            if cellView == nil {
              tableView.register (UINib(nibName: "MyOkAccountCell", bundle: nil), forCellReuseIdentifier: identifier)
              cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? MyOkAccountCell
            }
            cellView.btnMyAccount.setTitle(cellView.btnMyAccount.titleLabel?.text?.localized, for: .normal)
            cellView.btnMyAccount.addTarget(self, action:#selector(self.btnMyAccountAction(sender:)), for: .touchUpInside)
            cellView.lblMyNo.text = self.getActualNum(UserModel.shared.mobileNo,withCountryCode: "+95")
            addMoneyModel.receiverName = UserModel.shared.name
            cellView.selectionStyle = .none
            return cellView
          } else {
            let identifier = "OtherOkAccountCell"
            var cellView: OtherOkAccountCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? OtherOkAccountCell
            if cellView == nil {
              tableView.register (UINib(nibName: "OtherOkAccountCell", bundle: nil), forCellReuseIdentifier: identifier)
              cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? OtherOkAccountCell
            }
            cellView.btnOtherAccount.setTitle(cellView.btnOtherAccount.titleLabel?.text?.localized, for: .normal)
            cellView.btnOtherAccount.addTarget(self, action:#selector(self.btnOtherAccountAction(sender:)), for: .touchUpInside)
            cellView.selectionStyle = .none
            return cellView
          }
        }
      } else if(indexPath.section == 2) {
        let identifier = "AMRemarkCell"
        var cellView: AMRemarkCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? AMRemarkCell
        //cellView.txtRemark.delegate = self
        if cellView == nil {
          tableView.register (UINib(nibName: "AMRemarkCell", bundle: nil), forCellReuseIdentifier: identifier)
          cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? AMRemarkCell
        }

        cellView.txtRemark.placeholder = cellView.txtRemark.placeholder?.localized
        cellView.txtRemark.tag = 73
        cellView.txtRemark.delegate = self
        cellView.txtRemark.addTarget(self, action: #selector(AddWithdrawMoneyVC.textFieldDidChange(_:)),
                                     for: UIControl.Event.editingChanged)
       cellView.txtRemark.text = strEnterRemarktValue
        if(strEnterRemarktValue.length > 0) {
            cellView.txtRemark.becomeFirstResponder()
            cellView.txtRemark.placeholder = "Remark".localized
        }
        else
        {
            cellView.txtRemark.placeholder = "Enter Remark".localized
        }
        
        cellView.txtFees.placeholder = cellView.txtFees.placeholder?.localized
        if let adminFeeType = selectedDict["AdminFeeType"], adminFeeType == "Fixed" {
          cellView.viewPercentage.isHidden = true
          //calculate fees based on fixed or percentage
            addMoneyModel.fees = selectedDict["AdminFee"] ?? "0"
            cellView.txtFees.text = addMoneyModel.percentage + " MMK"
        }
        else {
          cellView.viewPercentage.isHidden = false
            cellView.lblPercentage.text = addMoneyModel.percentage + "%"
          //calculate fees based on fixed or percentage
          let perCentDivide = (Double(addMoneyModel.percentage) ?? 0.0)/100.0
          let perCentMult = (Double(addMoneyModel.enteredAmount) ?? 0.0) * perCentDivide
          addMoneyModel.fees = "\(perCentMult)"
          //Display Fees
          let arrTotalAmount = addMoneyModel.fees.components(separatedBy: ".")
          let firstValue = self.getDigitDisplay(arrTotalAmount[0] as String)
          let secondValue = arrTotalAmount[1] as String

          cellView.txtFees.text = "\(firstValue)" + "." + "\(secondValue.prefix(2))" + " MMK"
        }
        cellView.txtBankCharges.placeholder = cellView.txtBankCharges.placeholder?.localized

        if let bankcharge = selectedDict["BankCharges"] {
            if bankcharge.count == 0 {
                cellView.txtBankCharges.text = "0 MMK"
            } else {
                addMoneyModel.bankCharge = bankcharge
                cellView.txtBankCharges.text = addMoneyModel.bankCharge + " MMK"
            }
        }
        
        var tempAmount = 0.0
        tempAmount = tempAmount + (Double(addMoneyModel.enteredAmount) ?? 0.0)
        tempAmount = tempAmount + (Double(addMoneyModel.bankCharge) ?? 0.0)
        tempAmount = tempAmount + (Double(addMoneyModel.fees) ?? 0.0)
        addMoneyModel.totalAmount = "\(tempAmount)"
        
        //Display Amount
          let amont = wrapAmountWithCommaDecimal(key: "\(addMoneyModel.totalAmount)")
        cellView.txtTotalAmount.placeholder = cellView.txtTotalAmount.placeholder?.localized
        cellView.txtTotalAmount.text = amont + " MMK"
        cellView.txtRemark.text = addMoneyModel.remarks
        cellView.selectionStyle = .none
        return cellView
      } else { //Default return type-No Use
        let identifier = "OtherOkAccountCell"
        var cellView: OtherOkAccountCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? OtherOkAccountCell
        if cellView == nil {
          tableView.register (UINib(nibName: "OtherOkAccountCell", bundle: nil), forCellReuseIdentifier: identifier)
          cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? OtherOkAccountCell
        }
        cellView.btnOtherAccount.setTitle(cellView.btnOtherAccount.titleLabel?.text?.localized, for: .normal)
        cellView.btnOtherAccount.addTarget(self, action:#selector(self.btnOtherAccountAction(sender:)), for: .touchUpInside)
        cellView.selectionStyle = .none
        return cellView
      }
    }
  }
  
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    let view = UIView(frame: CGRect(x: 0, y: 0, width:200, height: 1))
    view.backgroundColor = .clear
    return view
  }
  
  // Header
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    if(tableView == tblOptionsList) {
      let headerCell:AddMoneyHeaderCell = (tableView.dequeueReusableCell(withIdentifier: "AddMoneyHeaderCell") as? AddMoneyHeaderCell)!
      headerCell.lblTitle.text = sections[section].sectionName
      headerCell.setArrow(sections[section].collapsed)
      headerCell.btnHeaderSelection.tag = section
      headerCell.btnHeaderSelection.addTarget(self, action:#selector(self.headerSelectionAction(sender:)), for: .touchUpInside)
      headerCell.section = section
        if screenFrom.1 == "Cash Out" {
            switch section {
            case 0:
                headerCell.ivSection.image = UIImage.init(named: "ok_agent_find")
                headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
            case 1:
                headerCell.ivSection.image = UIImage.init(named: "cashout_with_fee")
                headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
            case 2:
                headerCell.ivSection.image = UIImage.init(named: "cashout_okdollar")
                headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
            case 3:
                headerCell.ivSection.image = UIImage.init(named: "send_money")
                headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
            case 4:
                headerCell.ivSection.image = UIImage.init(named: "ok_office")
                headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
            case 5:
                headerCell.ivSection.image = UIImage.init(named: "post")
                headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
            case 6:
                headerCell.ivSection.image = UIImage.init(named: "bank_cash_icon")
                headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
            case 7:
                headerCell.ivSection.image = UIImage.init(named: "blue_wallet")
                headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
            default:
                println_debug("default")
            }
        }
        else {
            if UserModel.shared.agentType == .user {
                switch section {
                case 0:
                    headerCell.ivSection.image = UIImage.init(named: "ok_agent_find")
                    headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
                case 1:
                    headerCell.ivSection.image = UIImage.init(named: "cashin_with_fee")
                    headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
                case 2:
                    headerCell.ivSection.image = UIImage.init(named: "cashin_okdollar")
                    headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
                case 3:
                    headerCell.ivSection.image = UIImage.init(named: "123 logo")
                    headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
                case 4:
                    headerCell.ivSection.image = UIImage.init(named: "credit_card")
                case 5:
                    headerCell.ivSection.image = UIImage.init(named: "debit_card")
                case 6:
                    headerCell.ivSection.image = UIImage.init(named: "net_banking")
                case 7:
                    headerCell.ivSection.image = UIImage.init(named: "bank_counter_addMoney")
                    headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
                    
                case 8:
                    headerCell.ivSection.image = UIImage.init(named: "ok_office")
                    headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
                    
                case 9:
                    headerCell.ivSection.image = UIImage.init(named: "post")
                    headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
                    
                case 10:
                    headerCell.ivSection.image = UIImage.init(named: "bank_cash_icon")
                    headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
                    
                case 11:
                    headerCell.ivSection.image = UIImage.init(named: "blue_wallet")
                    headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
                    
                default:
                    println_debug("default")
                }
            }  else {
                    switch section {
                    case 0:
                        headerCell.ivSection.image = UIImage.init(named: "ok_agent_find")
                        headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
                    case 1:
                        headerCell.ivSection.image = UIImage.init(named: "agent_tranfer_digital")
                        headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
                    case 2:
                        headerCell.ivSection.image = UIImage.init(named: "cashin_with_fee")
                        headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
                    case 3:
                        headerCell.ivSection.image = UIImage.init(named: "cashin_okdollar")
                        headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
                    case 4:
                        headerCell.ivSection.image = UIImage.init(named: "123 logo")
                        headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
                    case 5:
                        headerCell.ivSection.image = UIImage.init(named: "credit_card")
                    case 6:
                        headerCell.ivSection.image = UIImage.init(named: "debit_card")
                    case 7:
                        headerCell.ivSection.image = UIImage.init(named: "net_banking")
                    case 8:
                        headerCell.ivSection.image = UIImage.init(named: "bank_counter_addMoney")
                        headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
                        
                    case 9:
                        headerCell.ivSection.image = UIImage.init(named: "ok_office")
                        headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
                        
                    case 10:
                        headerCell.ivSection.image = UIImage.init(named: "post")
                        headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
                        
                    case 11:
                        headerCell.ivSection.image = UIImage.init(named: "bank_cash_icon")
                        headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
                        
                    case 12:
                        headerCell.ivSection.image = UIImage.init(named: "blue_wallet")
                        headerCell.ivArrow.image = UIImage.init(named: "rightArrow")
                        
                    default:
                        println_debug("default")
                    }
                }
        }
      return headerCell
    } else {
      if(section == 0) {
        let identifier = "SelectionTypeHeaderCell"
        var cellView: SelectionTypeHeaderCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? SelectionTypeHeaderCell
        if cellView == nil {
          tableView.register (UINib(nibName: "SelectionTypeHeaderCell", bundle: nil), forCellReuseIdentifier: identifier)
          cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? SelectionTypeHeaderCell
        }
        cellView.btnSelectionType.addTarget(self, action:#selector(self.selectionTypeAction(sender:)), for: .touchUpInside)
        cellView.txtSelectionType.placeholder = addMoneyModel.paymentType
        cellView.txtSelectionType.placeholder = cellView.txtSelectionType.placeholder?.localized
        cellView.txtSelectionType.text = addMoneyModel.paymentTypeValue
        return cellView
      } else if(section == 1) {
        let identifier = "SelectAccountHeaderCell"
        var cellView: SelectAccountHeaderCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? SelectAccountHeaderCell
        if cellView == nil {
          tableView.register (UINib(nibName: "SelectAccountHeaderCell", bundle: nil), forCellReuseIdentifier: identifier)
          cellView = tableView.dequeueReusableCell(withIdentifier: identifier) as? SelectAccountHeaderCell
        }
        cellView.btnSelectAccount.addTarget(self, action:#selector(self.selectAccountAction(sender:)), for: .touchUpInside)
        cellView.btnSelectAccount.setTitle("Select OK$ Account".localized, for: .normal)
        //Arrow icon
        if(isSelectOkAccount) {
          cellView.imgArrowIcon.image = UIImage(named: "upArrow")
        } else {
          cellView.imgArrowIcon.image = UIImage(named: "downArrow")
        }
        if(isMyAccountSelected) {
          cellView.btnSelectAccount.setTitle("My OK$ Account".localized, for: .normal)
        }
        if(isOtherAccountSelected) {
          cellView.btnSelectAccount.setTitle("Other OK$ Account".localized, for: .normal)
        }
        return cellView
      } else {
        let view = UIView(frame: CGRect(x: 0, y: 0, width:200, height: 1))
        view.backgroundColor = .clear
        return view
      }
    }
  }
    
     func navToSendMoneyToBank() {
        let story = UIStoryboard(name: "SendMoney", bundle: nil)
        let sendMoneyToBankView = story.instantiateViewController(withIdentifier: "SendMoneyToBankView_ID") as! SendMoneyToBankViewController
        self.navigationController?.pushViewController(sendMoneyToBankView, animated: true)
    }
    
    private func navToBankDetail() {
        let story = UIStoryboard(name: "SendMoney", bundle: nil)
        let sendMoneyToBankView = story.instantiateViewController(withIdentifier: "SendMoneyToBankView_ID") as! SendMoneyToBankViewController
        self.navigationController?.pushViewController(sendMoneyToBankView, animated: true)
    }
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if(tableView == tblOptionsList) {
           
                if screenFrom.1 == "CashOut" {
                    switch indexPath.section {
                    case 0, 1, 2, 4, 3, 5,6:
                        tableView.deselectRow(at: indexPath, animated: true)
                        break
                    default:
                        println_debug("default")
                    }
                    
                    let item: Item = sections[indexPath.section].items[indexPath.row]
                    addMoneyModel.paymentType = sections[indexPath.section].sectionName
                    addMoneyModel.paymentTypeValue = item.cardName
                    addMoneyModel.percentage = item.fees
                    tblOptionsList.isHidden = true
                    tblAccountList.isHidden = false
                    button1.isHidden = false
                    tblAccountList.reloadData()
                }
                else {
                    if UserModel.shared.agentType == .user {
                        selectedRowIndex  = 100
                        switch indexPath.section {
                        case 0, 1, 2,3, 7, 8:
                            tableView.deselectRow(at: indexPath, animated: true)
                            break
                        case 4:
                            selectedRowIndex =  101 + indexPath.row
                            let gateWayName = sections[indexPath.section].items[indexPath.row].cardName
                            if gateWayName == "MPU" {
                                selectedDict = getBankResponseDict(PaymentGatewayName: "MPU ")
                                selectedPaymentOption = "MPU "
                            } else if gateWayName == "JCB" {
                                selectedDict = getBankResponseDict(PaymentGatewayName: "JCB")
                                selectedPaymentOption = "JCB"
                            } else if gateWayName == "UnionPay" {
                                selectedDict = getBankResponseDict(PaymentGatewayName: "UnionPay")
                                selectedPaymentOption = "UnionPay"
                            }  else if gateWayName == "1-2-3" {
                                selectedDict = getBankResponseDict(PaymentGatewayName: "1-2-3")
                                selectedPaymentOption = "1-2-3"
                            } else{
                                selectedDict = getBankResponseDict(PaymentGatewayName: "VisaMasterPay")
                                selectedPaymentOption = "VisaMasterPay"
                            }
                        case 5:
                            selectedRowIndex =  101 + indexPath.row
                            let gateWayName = sections[indexPath.section].items[indexPath.row].cardName
                            if gateWayName == "MPU" {
                                selectedDict = getBankResponseDict(PaymentGatewayName: "MPU ")
                                selectedPaymentOption = "MPU "
                            } else if gateWayName == "JCB" {
                                selectedDict = getBankResponseDict(PaymentGatewayName: "JCB")
                                selectedPaymentOption = "JCB"
                            } else if gateWayName == "UnionPay" {
                                selectedDict = getBankResponseDict(PaymentGatewayName: "UnionPay")
                                selectedPaymentOption = "UnionPay"
                            }  else if gateWayName == "1-2-3" {
                                selectedDict = getBankResponseDict(PaymentGatewayName: "1-2-3")
                                selectedPaymentOption = "1-2-3"
                            } else{
                                selectedDict = getBankResponseDict(PaymentGatewayName: "VisaMasterPay")
                                selectedPaymentOption = "VisaMasterPay"
                            }
                        case 6:
                            selectedRowIndex =  101 + indexPath.row
                            if indexPath.row == 0 {
                                //kbz bank
                                selectedDict = getBankResponseDict(PaymentGatewayName: "KBZ BANK ")
                                selectedPaymentOption = "KBZ BANK "
                            } else {
                                //cb bank
                                selectedDict = getBankResponseDict(PaymentGatewayName: "CB BANK")
                                selectedPaymentOption = "CB BANK"
                            }
                        case 7:
                            break
                            
                        case 10:
                            if indexPath.row == 0 {
                                selectedDict = getBankResponseDict(PaymentGatewayName: "MPU ")
                                selectedPaymentOption = "MPU "
                            } else{
                                selectedDict = getBankResponseDict(PaymentGatewayName: "VisaMasterPay")
                                selectedPaymentOption = "VisaMasterPay"
                            }
                        default:
                            println_debug("default")
                        }
                        
                    } else {
                        selectedRowIndex  = 100
                        switch indexPath.section {
                        case 0, 1, 2, 3,4, 8, 9:
                            tableView.deselectRow(at: indexPath, animated: true)
                            break
                        case 5:
                            selectedRowIndex =  101 + indexPath.row
                            let gateWayName = sections[indexPath.section].items[indexPath.row].cardName
                            if gateWayName == "MPU" {
                                selectedDict = getBankResponseDict(PaymentGatewayName: "MPU ")
                                selectedPaymentOption = "MPU "
                            } else if gateWayName == "JCB" {
                                selectedDict = getBankResponseDict(PaymentGatewayName: "JCB")
                                selectedPaymentOption = "JCB"
                            } else if gateWayName == "UnionPay" {
                                selectedDict = getBankResponseDict(PaymentGatewayName: "UnionPay")
                                selectedPaymentOption = "UnionPay"
                            }  else if gateWayName == "1-2-3" {
                                selectedDict = getBankResponseDict(PaymentGatewayName: "1-2-3")
                                selectedPaymentOption = "1-2-3"
                            } else{
                                selectedDict = getBankResponseDict(PaymentGatewayName: "VisaMasterPay")
                                selectedPaymentOption = "VisaMasterPay"
                            }
                        case 6:
                            selectedRowIndex =  101 + indexPath.row
                            let gateWayName = sections[indexPath.section].items[indexPath.row].cardName
                            if gateWayName == "MPU" {
                                selectedDict = getBankResponseDict(PaymentGatewayName: "MPU ")
                                selectedPaymentOption = "MPU "
                            } else if gateWayName == "JCB" {
                                selectedDict = getBankResponseDict(PaymentGatewayName: "JCB")
                                selectedPaymentOption = "JCB"
                            } else if gateWayName == "UnionPay" {
                                selectedDict = getBankResponseDict(PaymentGatewayName: "UnionPay")
                                selectedPaymentOption = "UnionPay"
                            }  else if gateWayName == "1-2-3" {
                                selectedDict = getBankResponseDict(PaymentGatewayName: "1-2-3")
                                selectedPaymentOption = "1-2-3"
                            } else{
                                selectedDict = getBankResponseDict(PaymentGatewayName: "VisaMasterPay")
                                selectedPaymentOption = "VisaMasterPay"
                            }
                        case 7:
                            selectedRowIndex =  101 + indexPath.row
                            if indexPath.row == 0 {
                                //kbz bank
                                selectedDict = getBankResponseDict(PaymentGatewayName: "KBZ BANK ")
                                selectedPaymentOption = "KBZ BANK "
                            } else {
                                //cb bank
                                selectedDict = getBankResponseDict(PaymentGatewayName: "CB BANK")
                                selectedPaymentOption = "CB BANK"
                            }
                        case 8:
                            break
                            
                        case 11:
                            if indexPath.row == 0 {
                                selectedDict = getBankResponseDict(PaymentGatewayName: "MPU ")
                                selectedPaymentOption = "MPU "
                            } else{
                                selectedDict = getBankResponseDict(PaymentGatewayName: "VisaMasterPay")
                                selectedPaymentOption = "VisaMasterPay"
                            }
                        default:
                            println_debug("default")
                        }
                    }
                    
                    let item: Item = sections[indexPath.section].items[indexPath.row]
                    addMoneyModel.paymentType = sections[indexPath.section].sectionName
                    addMoneyModel.paymentTypeValue = item.cardName
                    addMoneyModel.percentage = item.fees
                    tblOptionsList.isHidden = true
                    tblAccountList.isHidden = false
                    button1.isHidden = false
                    tblAccountList.contentSize.height += 100
                    tblAccountList.reloadData()
                }
                
            }
        
    }
  
  // MARK: - Custom Methods
  fileprivate func getSelectedPaymentType() -> PaymentType {
    switch selectedPaymentOption {
    case "MPU " :
      return PaymentType.MPU
    case "VisaMasterPay" :
      return PaymentType.VisaMasterPay
    case "KBZ BANK " :
      return  PaymentType.KBZBANK
    case "JCB" :
        return  PaymentType.JCB
    case "UnionPay" :
        return  PaymentType.UnionPay
    case "1-2-3" :
        return  PaymentType.OneTwoThree
    default:
      return PaymentType.CBBANK
    }
  }
  
  func submitToServer() {
    var customerName = ""
    var customerNumber = ""
    if(isContactSelected) {
        customerName = addMoneyModel.okACOwnername
        customerNumber = self.getConatctNum(addMoneyModel.depositorContactNumber,withCountryCode: "+95")
    } else {
        customerName = addMoneyModel.receiverName
        customerNumber = self.getConatctNum(addMoneyModel.depositorContactNumber,withCountryCode: "+95")
    }
    
    if isMyAccountSelected {
        customerName = UserModel.shared.name
        customerNumber = UserModel.shared.mobileNo
    }
    
    if(selectedDict["PaymentGatewayName"] == "VisaMasterPay") {
      let controller:AMPaymentDetailsVC = UitilityClass.getStoryBoardAddWithdraw().instantiateViewController(withIdentifier: "AMPaymentDetailsVC") as! AMPaymentDetailsVC
        
        var languageType = ""
         var language = ""
        if appDel.currentLanguage == "uni"{
            languageType = "1"
            language = "my-MM"
        }else if appDel.currentLanguage == "my"{
            languageType = ""
            language = "my-MM"
        }
        else {
            languageType = ""
            language = "en-GB"
        }
        
      let paramDict: Dictionary<String,Any> = ["Data": [["TotalAmount": Double(addMoneyModel.totalAmount)!,
                                                         "BeneficiaryAccountNumber":customerNumber, //String(addMoneyModel.depositorContactNumber),
                                                         "BankChargesType": selectedDict["BankChargesType"]!,
                                                         "BankCharges": Int(selectedDict["BankCharges"]!)!,
                                                         "GatewayConfigurationId": selectedDict["GatewayConfigurationId"]!,
                                                         "CalculatedBankCharges":  Int(addMoneyModel.bankCharge)!,
                                                         "OkAccountNumber": String(UserModel.shared.mobileNo),
                                                         "BeneficiaryName": Rabbit.zg2uni(addMoneyModel.receiverName),//addMoneyModel.receiverName,
                                                         "CustomerName": Rabbit.zg2uni(customerName),
                                                         "Comments": addMoneyModel.remarks,
                                                         "PaymentGatewayName": selectedDict["PaymentGatewayName"]!,
                                                         "AdminFee":  addMoneyModel.percentage,
                                                         "PaymentGatewayId": selectedDict["PaymentGatewayId"]!,
                                                         "Amount":   Int(addMoneyModel.enteredAmount)!,
                                                         "AdminFeeType": selectedDict["AdminFeeType"]!,
                                                         "CalculatedAdminFee":  Double(addMoneyModel.fees)!,
                                                         "HashValue":getHashValueForWebView(),
                                                         "Language": language,
                                                         "LanguageType": languageType]]]
      println_debug(paramDict)
      controller.type = self.getSelectedPaymentType()
      controller.dictPaymentDetails = paramDict as Dictionary<String,Any>
      self.navigationController?.pushViewController(controller, animated: true)
    } else {
      let vc:AMPaymentDetailsVC = UitilityClass.getStoryBoardAddWithdraw().instantiateViewController(withIdentifier: "AMPaymentDetailsVC") as! AMPaymentDetailsVC
        
        var languageType = ""
        var language = ""
        if appDel.currentLanguage == "uni"{
            languageType = "1"
            language = "my-MM"
        }else if appDel.currentLanguage == "my"{
            languageType = ""
            language = "my-MM"
        }
        else {
            languageType = ""
            language = "en"
        }
        
      let paramDict: Dictionary<String,Any> = ["Data": [["TotalAmount": Double(addMoneyModel.totalAmount)!,
                                                         "BeneficiaryAccountNumber": customerNumber, //String(addMoneyModel.depositorContactNumber),
                                                         "BankChargesType": selectedDict["BankChargesType"]!,
                                                         "BankCharges": Int(selectedDict["BankCharges"]!)!,
                                                         "GatewayConfigurationId": selectedDict["GatewayConfigurationId"]!,
                                                         "CalculatedBankCharges":  Int(addMoneyModel.bankCharge)!,
                                                         "OkAccountNumber": String(UserModel.shared.mobileNo),
                                                         "BeneficiaryName":Rabbit.zg2uni(customerName),
                                                         "CustomerName": Rabbit.zg2uni(UserModel.shared.name),
                                                         "Comments": addMoneyModel.remarks,
                                                         "PaymentGatewayName": selectedDict["PaymentGatewayName"]!,
                                                         "AdminFee":  addMoneyModel.percentage,
                                                         "PaymentGatewayId": selectedDict["PaymentGatewayId"]!,
                                                         "Amount":   Int(addMoneyModel.enteredAmount)!,
                                                         "AdminFeeType": selectedDict["AdminFeeType"]!,
                                                         "CalculatedAdminFee":  Double(addMoneyModel.fees)!,
                                                         "HashValue":getHashValueForWebView(),
                                                         "Language": language,
                                                         "LanguageType": languageType]]]
      println_debug(paramDict)
      vc.type = self.getSelectedPaymentType()
      vc.dictPaymentDetails = paramDict as Dictionary<String,Any>
      self.navigationController?.pushViewController(vc, animated: true)
    }
  }
  
 @IBAction func btnSubmitAction() {
    //(Double(addMoneyModel.enteredAmount) ?? 0.0)
    //  if(Int(addMoneyModel.enteredAmount)! >= Int(selectedDict["MinimumAmount"]!)! && Int(addMoneyModel.enteredAmount)! <= Int(selectedDict["MaximumAmount"]!)!)
    if((Int(addMoneyModel.enteredAmount) ?? 0) >= (Int(selectedDict["MinimumAmount"] ?? "0") ?? 0) && (Int(addMoneyModel.enteredAmount) ?? 0) <= (Int(selectedDict["MaximumAmount"] ?? "0") ?? 0)) {
      if UserLogin.shared.loginSessionExpired {
        OKPayment.main.authenticate(screenName: "AddWithdrawMoneyVCSubmit", delegate: self)
      } else {
        println_debug((Double(addMoneyModel.totalAmount) ?? 0))
        println_debug((Double(addMoneyModel.enteredAmount) ?? 0))

        if (Double(addMoneyModel.totalAmount) ?? 0) >= (Double(addMoneyModel.enteredAmount) ?? 0) {
            self.submitToServer()
        }
      }
    } else {
      if let minimumAmount = selectedDict["MinimumAmount"], let maximumAmount = selectedDict["MaximumAmount"] {
        let formatedMinAmount = self.getDigitDisplay(minimumAmount)
        let formatedMaxAmount = self.getDigitDisplay(maximumAmount)
        let formatedAlert = "You can transfer string1".localized + formatedMinAmount + "You can transfer string2".localized + formatedMaxAmount + "You can transfer string3".localized
        alertViewObj.wrapAlert(title: nil, body:formatedAlert, img: #imageLiteral(resourceName: "sm_add_money"))
        alertViewObj.addAction(title: "OK".localized, style: .cancel) {
          
        }
        alertViewObj.showAlert(controller: self)
      } else {
        //self.showAlert(alertTitle: "Please Try Again Later.".localized, alertBody: "", alertImage: #imageLiteral(resourceName: "sm_add_money"))
        }
    }
  }
  
  @objc func btnContactAction(sender: UIButton!) {
    println_debug("btnContactAction")
    self.hideContactSuggesionView()
    let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
    self.present(nav, animated: true, completion: nil)
  }
  
    @objc func btnOtherAmountCancelAction(sender: UIButton!) {
        
        if(sender.tag == 64) {
            let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? AMOtherAccountSelectedCell
            strEnterAmountValue = ""
            addMoneyModel.remarks = ""
            isRemark = false
            submitbuttonConstraint.constant = 0
            cell?.btnOtherAmountCancel.isHidden = true
            cell?.txtAmount.text = ""
            cell?.txtAmount.becomeFirstResponder()
            cell?.txtAmount.keyboardType = .numberPad
            tblAccountList.reloadData()
            
        } else {//tag = 65
            let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 2, section: 1)) as? AMOtherAccountSelectedCell
            strEnterAmountValue = ""
            addMoneyModel.remarks = ""
            isRemark = false
            submitbuttonConstraint.constant = 0
            cell?.btnOtherAmountCancel.isHidden = true
            cell?.txtAmount.text = ""
            cell?.txtAmount.becomeFirstResponder()
            cell?.txtAmount.keyboardType = .numberPad
            tblAccountList.reloadData()
        }
    }
    
    @objc func btnMyAmountCancelAction(sender: UIButton!) {
        
        let cell = self.tblAccountList.cellForRow(at: IndexPath.init(row: 0, section: 1)) as? AMMyAccountSelectedCell
        strEnterAmountValue = ""
        addMoneyModel.remarks = ""
        isRemark = false
        submitbuttonConstraint.constant = 0
        cell?.btnMyAmountCancel.isHidden = true
        cell?.txtAmount.text = ""
        cell?.txtAmount.becomeFirstResponder()
        cell?.txtAmount.keyboardType = .numberPad
        
        tblAccountList.reloadData()
        
    }
    
  @objc func btnCancelAction(sender: UIButton!) {
   
    if(sender.tag == 52) {
              self.hideContactSuggesionView()
            addMoneyModel.depositorContactNumber = ""
            addMoneyModel.depositorConfirmContactNumber = ""
              isConfirmMobileNoShow = false
                
          } else { //confirm
            addMoneyModel.depositorConfirmContactNumber = ""
              isConfirmMobileNoShow = true
//            tblAccountList.reloadData()
//              let cell = tblAccountList.cellForRow(at: IndexPath.init(row: 1, section: 1)) as? AMConfirmAccountMobNoCell
//              cell?.txtConfirmMobileNo.becomeFirstResponder()
          }
    
    strEnterAmountValue = ""
    addMoneyModel.enteredAmount = ""
    addMoneyModel.remarks = ""
    isContactSelected = false
    isRemark = false
    submitbuttonConstraint.constant = 0
    isOtherAccountShow = false
    tblAccountList.reloadData()
  }
  
  @objc func btnMyAccountAction(sender: UIButton!) {
    isMyAccountSelected = true
    isOtherAccountSelected = false
    tblAccountList.reloadData()
  }
  
  @objc func btnOtherAccountAction(sender: UIButton!) {
    println_debug("btnOtherAccountAction")
    isMyAccountSelected = false
    isOtherAccountSelected = true
    tblAccountList.reloadData()
   // let cellX = self.tblAccountList.cellForRow(at: IndexPath.init(row: 2, section: 1)) as? AMOtherAccountSelectedCell
//     let cellX = self.tblAccountList.cellForRow(at: IndexPath.init(row:0, section: 1)) as? AMAccountMobileNoCell
//    cellX?.txtAccountMobNo.delegate = self
//    cellX?.txtAccountMobNo.addTarget(self, action: #selector(AddWithdrawMoneyVC.textFieldDidChange(_:)),
//    for: UIControl.Event.editingChanged)
//    cellX?.txtAccountMobNo.becomeFirstResponder()
//    cellX?.txtAccountMobNo.keyboardType = .numberPad
  //  cellX?.txtAmount.becomeFirstResponder()
  //  cellX?.txtAmount.keyboardType = .numberPad
  }
  
  @objc func selectAccountAction(sender: UIButton!) {
    isMyAccountSelected = false
    isOtherAccountSelected = false
    isRemark = false
    submitbuttonConstraint.constant = 0
    addMoneyModel.enteredAmount = ""
    addMoneyModel.depositorContactNumber = ""
    strEnterAmountValue = ""
    addMoneyModel.remarks = ""
    //btnSubmit.isHidden = true
    isOtherAccountShow = false
    isConfirmMobileNoShow = false
    isContactSelected = false
    if(isSelectOkAccount) {
      isSelectOkAccount = false
    } else {
      isSelectOkAccount = true
    }
    tblAccountList.reloadData()
  }
  
  @objc func selectionTypeAction(sender: UIButton!) {
    println_debug("selectionTypeAction")
    self.view.endEditing(true)
    isSelectOkAccount = false
    isMyAccountSelected = false
    isOtherAccountSelected = false
    //btnSubmit.isHidden = true
    isRemark = false
    submitbuttonConstraint.constant = 0
    isConfirmMobileNoShow = false
    isOtherAccountShow = false
    isContactSelected = false
    addMoneyModel.enteredAmount = ""
    addMoneyModel.depositorContactNumber = ""
    strEnterAmountValue = ""
    addMoneyModel.remarks = ""
    tblOptionsList.isHidden = false
    tblAccountList.isHidden = true
     // button1.isHidden = true
     soundView.isHidden = true
    // self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button1)
    tblOptionsList.reloadData()
  }
    
    private func showNearByAgents() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined:
                println_debug("notDetermined")
                loadNearByServicesView()
                break
            case .denied :
                println_debug("denied")
                showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                break
            case .restricted :
                println_debug("restricted")
                showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                break
            case .authorizedAlways, .authorizedWhenInUse:
                loadNearByServicesView()
                break
            }
        } else {
            alertViewObj.wrapAlert(title:"", body: "Please Enable Location Service".localized, img: #imageLiteral(resourceName: "location"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func navigateToPayto(type: PayToType, headerTitle: String = "Pay / Send") {
        guard let payto = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PayToNavigation") as? UINavigationController else { return }
        isPayToDashboard = false
        for controller in payto.viewControllers {
            if let payToVC = controller as? PayToViewController {
                payToVC.headerTitle = headerTitle.localized
                payToVC.fromMerchant = true
                payToVC.transType = type
            }
        }
        payto.modalPresentationStyle = .fullScreen
        self.navigationController?.present(payto, animated: true, completion: nil)

        //self.present(payto, animated: true, completion: nil)
    }
    
    private func navigateToCashInScreen() {
        guard let payto = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PaytoCashInViewController") as? PaytoCashInViewController else { return }
        payto.modalPresentationStyle = .fullScreen
        self.present(payto, animated: true, completion: nil)

        //self.navigationController?.pushViewController(payto, animated: true)
    }
    
    
    private func navToCashInPaid() {
        if issound {
            objectSound.playSound(fileName: "Cash In with commission")
        }
        //        self.navigateToCashInScreen()
        self.navigateToPayto(type: PayToType.init(type: .cashIn), headerTitle: "Paid Commission Agent Cash In_header")
    }
    
    private func navToCashOutPaid() {
        if issound {
            objectSound.playSound(fileName: "Cash Out with commission")
        }
        self.navigateToPayto(type: PayToType.init(type: .cashOut), headerTitle: "Paid Commission Agent Cash Out_header")
    }
    
    private func navTo123PaymentCode() {
        //self.navigateToPayto(type: PayToType.init(type: .cashInFree), headerTitle: "Free Commission Agent Cash In_header")
            let story = UIStoryboard.init(name: "BookOnOKDollar", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
            vc?.type = WebType.pay123
            vc?.modalPresentationStyle = .fullScreen
            self.present(vc!, animated: true, completion: nil)
    }
    
    private func navToCashInNonPaid() {
        if issound {
            objectSound.playSound(fileName: "Cash In without commission in OK$ counter")
        }
        //        self.navigateToCashInScreen()
        self.navigateToPayto(type: PayToType.init(type: .cashInFree), headerTitle: "Free Commission Agent Cash In_header")
    }
    
    private func navToCashOutNonPaid() {
        if issound {
            objectSound.playSound(fileName: "Cash Out without commission in OK$ counter")
        }
        self.navigateToPayto(type: PayToType.init(type: .cashOutFree), headerTitle: "Free Commission Agent Cash Out_header")
    }
    
    
    private func navigateToCardlessCash() {
        let cardlessCashStory = UIStoryboard(name: "CardLessCashStoryBoard", bundle: nil)
        guard let cardlessCashVC = cardlessCashStory.instantiateViewController(withIdentifier: "CardlessCashHomeVC") as? CardlessCashHomeVC else { return }
        self.navigationController?.pushViewController(cardlessCashVC, animated: true)
    }
    
    private func navigateToUnionPay() {
        DispatchQueue.main.async {
            let cardlessCashStory = UIStoryboard(name: "CardLessCashStoryBoard", bundle: nil)
            guard let cardlessCashVC = cardlessCashStory.instantiateViewController(withIdentifier: "UPayIntScan") as? UPayIntScan else { return }
            self.navigationController?.pushViewController(cardlessCashVC, animated: true)
        }
     }
   
    func navToReqCashIn() {
        println_debug("navigateToCashIN")
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                self.showErrorAlert(errMessage: "Please Enable Location Service".localized)
            case .authorizedAlways, .authorizedWhenInUse:
                let story = UIStoryboard.init(name: cashInOutViewControllers.cashinoutStoryBoard, bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: cashInOutViewControllers.viewControllers.cioHomeVC) as? CIOHomeViewController
                vc?.screenName = "Cash In"
                let navController = UINavigationController(rootViewController: vc!)
                navController.modalPresentationStyle = .fullScreen
                self.present(navController, animated: true, completion: nil)
            }
        }else {
            println_debug("Please Enable Location Service".localized)
        }
    }
    
    func navToReqCashOut() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                self.showErrorAlert(errMessage: "Please Enable Location Service".localized)
            case .authorizedAlways, .authorizedWhenInUse:
                let story = UIStoryboard.init(name: cashInOutViewControllers.cashinoutStoryBoard, bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: cashInOutViewControllers.viewControllers.cioHomeVC) as? CIOHomeViewController
                vc?.screenName = "Cash Out"
                let navController = UINavigationController(rootViewController: vc!)
                navController.modalPresentationStyle = .fullScreen
                self.present(navController, animated: true, completion: nil)
            }
        } else {
            println_debug("Please Enable Location Service".localized)
        }
    }
    
    private func navigateToAgentTransfer() {
        let story = UIStoryboard(name: "Main", bundle: nil)
        guard let scanQRCode = story.instantiateViewController(withIdentifier: "TBScanToPayViewController") as? TBScanToPayViewController else { return }
        scanQRCode.scanDelegate = self
        scanQRCode.isFromTabBar = false
        scanQRCode.isFromTransferTo = true
        scanQRCode.isFromSideMenu = false
        self.navigationController?.pushViewController(scanQRCode, animated: true)
    }
    
    private func naviagteToAddWithdraw() {
        
        let sb = UIStoryboard(name: "AddWithDrawNew", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "AddWithdrawNavi")
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
  
    @objc func headerSelectionAction(sender: UIButton!) {
        if appDel.checkNetworkAvail() {
            
            if screenFrom.1 == "Cash Out" {
                if sender.tag == 0 {
                    self.showNearByAgents()
                } else if sender.tag == 1 {
                    self.navToCashOutPaid()
                } else if sender.tag == 2 {
                    self.navToCashOutNonPaid()
                }  else if sender.tag == 3 {
                    if UserLogin.shared.loginSessionExpired {
                        OKPayment.main.authenticate(screenName: "NavigateToSendMoneyBank", delegate: self)
                    } else {
                        self.navToSendMoneyToBank()
                    }
                }else if sender.tag == 4  {
                    if CLLocationManager.locationServicesEnabled() {
                        switch(CLLocationManager.authorizationStatus()) {
                        case .notDetermined:
                            println_debug("notDetermined")
                            showOKMoneyInViews(sender.tag)
                            break
                        case .denied :
                            println_debug("denied")
                            showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                            break
                        case .restricted :
                            println_debug("restricted")
                            showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                            break
                        case .authorizedAlways, .authorizedWhenInUse:
                            showOKMoneyInViews(sender.tag)
                            break
                        }
                    } else {
                        alertViewObj.wrapAlert(title:"", body: "Please Enable Location Service".localized, img: #imageLiteral(resourceName: "location"))
                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                            
                        })
                        DispatchQueue.main.async {
                            alertViewObj.showAlert(controller: self)
                        }
                    }
                }  else if sender.tag == 5 {
                    //self.showErrorAlert(errMessage: "Myanmar Post Coming Soon".localized)
                    showOKMoneyInViews(sender.tag)
                } else if sender.tag == 6 {
                    //self.showErrorAlert(errMessage: "Bank Coming Soon".localized)
                    self.loadBankDetailCashOutView()
                    
                }else if sender.tag == 7 {
                    // self.navigateToUnionPay()
                    var homeVC: BankDetailsViewController?
                    homeVC = UIStoryboard(name: "UnionPay", bundle: nil).instantiateViewController(withIdentifier: "BankDetailsViewController") as? BankDetailsViewController
                    if let homeVC = homeVC {
                        navigationController?.pushViewController(homeVC, animated: true)
                    }
                }  else if sender.tag == 8 {
                    //self.showErrorAlert(errMessage: "Bank Coming Soon".localized)
                    self.navigateToUnionPay()
                    //self.navigateToCardlessCash()
                } else {
                    self.toggleSection(section: sender.tag)
                }
            }
            else {
                if UserModel.shared.agentType == .user {
                    if sender.tag == 0 {
                        self.showNearByAgents()
                    } else if sender.tag == 1 {
                        self.navToCashInPaid()
                    } else if sender.tag == 2 {
                        self.navToCashInNonPaid()
                    }  else if sender.tag == 3 {
                        self.navTo123PaymentCode()
                    }
                    else if sender.tag == 7 {
                        
                        let controller:BankCounterDepositeVC = UitilityClass.getStoryBoardAddWithdraw().instantiateViewController(withIdentifier: "BankCounterDepositeVC") as! BankCounterDepositeVC
                        self.navigationController?.pushViewController(controller, animated: true)
                        
                    }  else if sender.tag == 8 || sender.tag == 9  {
                        if CLLocationManager.locationServicesEnabled() {
                            switch(CLLocationManager.authorizationStatus()) {
                            case .notDetermined:
                                println_debug("notDetermined")
                                showOKMoneyInViews(sender.tag)
                                break
                            case .denied :
                                println_debug("denied")
                                showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                                break
                            case .restricted :
                                println_debug("restricted")
                                showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                                break
                            case .authorizedAlways, .authorizedWhenInUse:
                                showOKMoneyInViews(sender.tag)
                                break
                            }
                        } else {
                            alertViewObj.wrapAlert(title:"", body: "Please Enable Location Service".localized, img: #imageLiteral(resourceName: "location"))
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                
                            })
                            DispatchQueue.main.async {
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                    }  else if sender.tag == 10 {
                        
                        self.loadBankDetailCashInView()
                        
                    } else if sender.tag == 11 {
                        alertViewObj.wrapAlert(title: nil, body:"Other Mobile Money Wallet Coming Soon".localized, img: #imageLiteral(resourceName: "ok_mobile_plus"))
                        alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        }
                        alertViewObj.showAlert(controller: self)
                        
                        
                    } else {
                        self.toggleSection(section: sender.tag)
                    }
                } else {
                    if sender.tag == 0 {
                        self.showNearByAgents()
                    } else if sender.tag == 1 {
                        self.navigateToAgentTransfer()
                    } else if sender.tag == 2 {
                        self.navToCashInPaid()
                    } else if sender.tag == 3 {
                        self.navToCashInNonPaid()
                    }
                    else if sender.tag == 4 {
                        self.navTo123PaymentCode()
                    }
                    else if sender.tag == 8 {
                        let controller:BankCounterDepositeVC = UitilityClass.getStoryBoardAddWithdraw().instantiateViewController(withIdentifier: "BankCounterDepositeVC") as! BankCounterDepositeVC
                        self.navigationController?.pushViewController(controller, animated: true)
                        
                    }  else if sender.tag == 9 || sender.tag == 10  {
                        if CLLocationManager.locationServicesEnabled() {
                            switch(CLLocationManager.authorizationStatus()) {
                            case .notDetermined:
                                println_debug("notDetermined")
                                showOKMoneyInViews(sender.tag)
                                break
                            case .denied :
                                println_debug("denied")
                                showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                                break
                            case .restricted :
                                println_debug("restricted")
                                showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                                break
                            case .authorizedAlways, .authorizedWhenInUse:
                                showOKMoneyInViews(sender.tag)
                                break
                            }
                        } else {
                            alertViewObj.wrapAlert(title:"", body: "Please Enable Location Service".localized, img: #imageLiteral(resourceName: "location"))
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                
                            })
                            DispatchQueue.main.async {
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                    }  else if sender.tag == 11 {
                        
                        self.loadBankDetailCashInView()
                        
                    } else if sender.tag == 12 {
                        alertViewObj.wrapAlert(title: nil, body:"Other Mobile Money Wallet Coming Soon".localized, img: #imageLiteral(resourceName: "ok_mobile_plus"))
                        alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        }
                        alertViewObj.showAlert(controller: self)
                        //                    var homeVC: BankDetailsViewController?
                        //                    homeVC = UIStoryboard(name: "UnionPay", bundle: nil).instantiateViewController(withIdentifier: "BankDetailsViewController") as? BankDetailsViewController
                        //                           if let homeVC = homeVC {
                        //                               navigationController?.pushViewController(homeVC, animated: true)
                        //                           }
                        
                    } else {
                        self.toggleSection(section: sender.tag)
                    }
                }
            }
        } else {
            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.noInternet.localized)
            if screenFrom.1 == "Cash Out" {
                alertViewObj.wrapAlert(title: "", body: PaytoConstants.messages.noInternet.localized, img: #imageLiteral(resourceName: "dashboard_cash_out_payto"))
            } else {
                alertViewObj.wrapAlert(title: "", body: PaytoConstants.messages.noInternet.localized, img: #imageLiteral(resourceName: "cashInDash"))
            }
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                self.navigationController?.popToRootViewController(animated: true)
            })
            
            alertViewObj.showAlert(controller: self)
            
        }
    }
  
  func toggleSection(section: Int) {
    let collapsed = !sections[section].collapsed
    // Toggle collapse
    self.sections[section].collapsed = collapsed
    // collapsed the opened section
    for i in 0..<self.sections.count {
      if i == section {
        continue
      }
      self.sections[i].collapsed = true
    }
    self.tblOptionsList.reloadData()
  }
    
    
    func showOKMoneyInViews(_ indexPath: Int) {
        
        print("showOKMoneyInViews Detail Called--------\(indexPath)")
        let story: UIStoryboard = UIStoryboard(name: "AddWithdrawMain", bundle: nil)
        let nearByOkServicesView = story.instantiateViewController(withIdentifier: "OK$ServicesViewController") as! OK_ServicesViewController
        //Cash IN
        if UserModel.shared.agentType == .user {
            if(indexPath == 8)
            {
                nearByOkServicesView.intType = 7//OK$ OFFICE
            }
            else if(indexPath == 9)
            {
                nearByOkServicesView.intType = 9//MYANMAR POST
            }
                //Cash Out
            else if(indexPath == 4)
            {
                nearByOkServicesView.intType = 7//OK$ office
            }else if(indexPath == 5)
            {
                nearByOkServicesView.intType = 9//MYANMAR POST
            }
        } else {
            if(indexPath == 9)
                   {
                       nearByOkServicesView.intType = 7//OK$ OFFICE
                   }
                   else if(indexPath == 10)
                   {
                       nearByOkServicesView.intType = 9//MYANMAR POST
                   }
            //Cash Out
            else if(indexPath == 4)
            {
                nearByOkServicesView.intType = 7//OK$ office
            }else if(indexPath == 5)
            {
                nearByOkServicesView.intType = 9//MYANMAR POST
            }
        }
      
        let aObjNav = UINavigationController(rootViewController: nearByOkServicesView)
        aObjNav.isNavigationBarHidden = true
        aObjNav.modalPresentationStyle = .fullScreen
    self.present(aObjNav, animated: true, completion: nil)
        
    }
    
    func loadNearByServicesView() {
        
        if screenFrom.1 == "Cash Out" {
            
            if issound{
                objectSound.playSound(fileName: "Near by Agent for Cash Out")
            }
            
        } else {
            
            if issound{
                objectSound.playSound(fileName: "Near by Agent for Cash In")
            }
            
        }
        let story: UIStoryboard = UIStoryboard(name: "NBSMain", bundle: nil)
        let nearByOkServicesView = story.instantiateViewController(withIdentifier: "NearByServicesMainVC") as! NearByServicesMainVC
        nearByOkServicesView.statusScreen = "Service"
        nearByOkServicesView.isComingFromMap = true
        nearByOkServicesView.modalPresentationStyle = .fullScreen
//        nearByOkServicesView.isPushed = true
        self.navigationController?.present(nearByOkServicesView, animated: true, completion: nil)
//        self.navigationController?.pushViewController(nearByOkServicesView, animated: true)
    }

    func loadBankDetailCashInView() {
        
        let controller:AddwithdrawBankdetailscreen = UitilityClass.getStoryBoardAddWithdraw().instantiateViewController(withIdentifier: "AddwithdrawBankdetailscreen") as! AddwithdrawBankdetailscreen
        controller.cashinoutstr = "CashIn"
        self.navigationController?.pushViewController(controller, animated: true)
    
    }
    
    func loadBankDetailCashOutView() {
        
        let controller:AddwithdrawBankdetailscreen = UitilityClass.getStoryBoardAddWithdraw().instantiateViewController(withIdentifier: "AddwithdrawBankdetailscreen") as! AddwithdrawBankdetailscreen
        controller.cashinoutstr = "CashOut"
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:"OK$ would like to use your Current Location. Please turn on the Location Services for your device.".localized, img:#imageLiteral(resourceName: "map"))
        
        alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
            
        })
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: self.convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            }
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }
    
}


////MARK: - TBScanQRDelegate
extension AddWithdrawMoneyVC: TBScanQRDelegate {
    func moveToPayto(withScanObject object: PTScanObject, fromTransfer: Bool? = false) {
        if let delegate = self.scanDelegate {
            delegate.moveToPayto(withScanObject: object, fromTransfer: fromTransfer)
        } else {
            self.navigateToPayto(phone: object.agentNo, amount: object.amount, name: object.name, remark: object.remarks, fromMerchant: true, fromTranfer: fromTransfer, cashInQR: object.cashInQR)
        }
    }

    func moveToPayTo(fromTransfer: Bool? = false) {
        if let delegate = self.scanDelegate {
            delegate.moveToPayTo(fromTransfer: fromTransfer)
        } else {
            self.navigatetoPayto(fromMerchant: true, fromTranfer: fromTransfer)
        }
    }
}
