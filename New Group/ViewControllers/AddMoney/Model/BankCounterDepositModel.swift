//
//  BankCounterDepositModel.swift
//  OK
//
//  Created by PC on 1/18/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BankCounterDepositModel: NSObject {

    var BankId:String = "",
    Bearer:String = "",
    HashValue:String = "",
    BankName:String = "",
    BankNameBurmese:String = "",
    BankCode:String = "",
    PersonalAmountLimit: Int = 0,
    BusinessAmountLimit: Int = 0,
    AdvanceMerchantLimit: Int = 0,
    AgentLimit: Int = 0,
    IsManualDeposit:Bool = false,
    Notes:String = "",
    NotesBurmese:String = ""
}
