//
//  AddMoneyModel.swift
//  OK
//
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit

class AddMoneyModel: NSObject {
    
    var bankName:String = ""
    var paymentType:String = ""
    var paymentTypeValue:String = ""
    var selectedOKAC:String = "" // my / other
    var otherOKACNumber:String = ""
    var okACOwnername:String = ""
    var businessName:String = ""
    var enteredAmount:String = ""
    var remarks:String = ""
    var IsMyOwnAccount:Bool = false
    var IsOtherContact:Bool = false

    //Depositor Details
    var depositeBy:String = "" //Myself // Others
    var depositorContactNumber:String = ""
    var depositorConfirmContactNumber:String = ""
    var depositorName:String = ""
    
    var depositorOtherContactNumber:String = ""
    var depositorOtherContactName:String = ""
    var IsDepositorOtherContact:Bool = false

    var bankCharge:String = ""
    var fees:String = ""
    var amount:String = ""
    var totalAmount:String = ""
    var percentage:String = ""
    var maxAmount:String = ""
    var minAmount:String = ""

    var receiverName:String = ""
    var receiverOKAC:String = ""
    
    var date:String = ""
    var time:String = ""
}
