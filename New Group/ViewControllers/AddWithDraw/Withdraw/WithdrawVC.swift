//
//  WithdrawVC.swift
//  OK
//
//  Created by Imac on 6/26/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import CoreLocation
import Rabbit_Swift
import AVFoundation

class WithdrawVC: OKBaseController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,BioMetricLoginDelegate,MyTextFieldDelegate,SAConatactListViewDelegate,SuccessViewAddWDDelegate {
    
    
    @IBOutlet weak var tfWithdraw: UITableView!
    var cellList = [UITableViewCell] ()
    @IBOutlet weak var tvBottom: NSLayoutConstraint!
    @IBOutlet weak var btnPay: UIButton!{
        didSet{
            self.btnPay.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    var model = WithDrawnodel()
    let validObj = PayToValidations()
    var allLocationsList = [LocationDetail]()
    var controller = UIImagePickerController()
    var countryListShownFor = ""
    var contactListShownFor = ""
    var isWithdrawNumberSelectedFromContact = false
    var isDepositeNumberSelectedFromContact = false
    var isWithdrawConrimHidden = true
    var isDepositeHidden = true
    var isDepositeConrimHidden = true
    var isOTPHidden = true
    var isWithdrawInfoHidden = true
    var isNumberHidden = true
    var isNameHidden = true
    var isNRCHidden = true
    var isAddressHidden = true
    var isDepositorMoreThanOne = false
    var currentLat = ""
    var currentLong = ""
    var isHideOTPView = false
    var otpLableStatus = "Enter Withdrawal OTP".localized
    var previousCCWithdraw = ""
    var previousCFWithdraw = ""
    var previousCCDeposite = ""
    var previousCFDeposite = ""
    var contactSuggestionView = Bundle.main.loadNibNamed("SAConatactListView", owner: self, options: nil)?[0] as! SAConatactListView
    var alertView = Bundle.main.loadNibNamed("SuccessViewAddWD", owner: self, options: nil)?[0] as! SuccessViewAddWD
    var Parms:[String : Any]?
    var failureViewModel = FailureViewModel()
    
    var strForAlert = ""
    var failureCount = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setMarqueLabelInNavigation()
        self.setDefalutValues()
        allLocationsList = TownshipManager.allDivisionList
        tfWithdraw.tableFooterView = UIView(frame: .zero)
        cellList.append(tfWithdraw.dequeueReusableCell(withIdentifier: "WithdrawContactCell") as! WithdrawContactCell)
        tfWithdraw.delegate = self
        tfWithdraw.dataSource = self
        tvBottom.constant = -50
        btnPay.isHidden = true
        getNumberToSendSMS()
        getAddressData()
        tfWithdraw.reloadData()
        if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 0, section: 0)) as? WithdrawContactCell {
            cell.tfNumber.becomeFirstResponder()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.view.endEditing(true)
        failureCount = 0
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    func setDefalutValues() {
        model.withdrawCountryCode = "+95"
        model.withdrawflag = "myanmar"
        model.depositeCountryCode = "+95"
        model.depositeflag = "myanmar"
        
        previousCCWithdraw = "+95"
        previousCFWithdraw = "myanmar"
        previousCCDeposite = "+95"
        previousCFDeposite = "myanmar"
    }
    
    func getNumberToSendSMS() {
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let web      = WebApiClass()
            web.delegate = self
            let ur = getUrl(urlStr: Url.URL_GetCategoryApi, serverType: .faceIDPay)
            let dic =  ["OsType":1,"DestinationNo":UserModel.shared.mobileNo,"SourceNo": UserModel.shared.mobileNo,"TransType":"PayTo","AppBuildNumber": buildNumber] as [String : Any]
            println_debug(ur)
            println_debug(dic)
            web.genericClass(url: ur, param: dic as AnyObject, httpMethod: "POST", mScreen: "NumberForSendSMS")
        }else {
            alertViewObj.wrapAlert(title: nil, body: "Please check your internet connection".localized, img: UIImage(named: "alert-icon")!)
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 18)
        lblMarque.text = "Withdrawal".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white//UIColor.init(hex: "#1C2C99")
        self.navigationItem.titleView = lblMarque
    }
    @IBAction func onClickBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickPayAction(_ sender: Any) {
          OKPayment.main.authenticate(screenName: "withdraw", delegate: self)
    }
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful{
            self.openCamera()
        }else {
            self.showAlert(alertTitle: "", alertBody: "Please Login Again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
        }
    }
    
    func payFinally(withdrawID: String) {
        var dicInfo = Dictionary<String,Any>()
            if let dic = model.withdrawServerList?[0] {
                dicInfo = dic
            }
        let appInfo = ["CellId":"","Lang": currentLong,"Lat": currentLat,"merchant_type": agentServiceTypeString(type: UserModel.shared.agentType),"MobileNo": UserModel.shared.mobileNo,"MsId": msid,"ProfileImg": UserModel.shared.proPic,"SimId": uuid,"TransType":"1","TransactionType":"PAYTO","Password": ok_password ?? ""] as [String : Any]
        let dic = ["MobileNo": dicInfo["MobileNo"] ?? "","WithdrawId": withdrawID, "DepositorId": dicInfo["UserId"] ?? "","Amt": dicInfo["Amount"] ?? "","ConfirmImg": self.model.monyWithdrawImage,"Otp": self.model.OTP,"AppInfo": appInfo] as [String : Any]
        println_debug( dic)
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let web      = WebApiClass()
            web.delegate = self
            let ur = getUrl(urlStr: Url.URL_FacePayPayment, serverType: .faceIDPay)
//            guard let ur = URL(string: "http://52.76.209.187:3002/withdraw/Payment") else {
//                return
//            }
            println_debug(ur)
            println_debug(dic)
            self.Parms = dic
            web.genericClass(url: ur, param: dic as AnyObject, httpMethod: "POST", mScreen: "payFinally")
        }else {
            alertViewObj.wrapAlert(title: nil, body: "Please check your internet connection".localized, img: UIImage(named: "alert-icon")!)
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }

        
    }
    
    func saveWithdraw() {
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let web      = WebApiClass()
            web.delegate = self
            let ur = getUrl(urlStr: Url.URL_FacePaySaveWithdraw, serverType: .faceIDPay)
            let dic = wrapSaveWithdraw()
            println_debug(ur)
            println_debug(dic)
            web.genericClass(url: ur, param: dic as AnyObject, httpMethod: "POST", mScreen: "SaveWithdraw")
        }else {
            alertViewObj.wrapAlert(title: nil, body: "Please check your internet connection".localized, img: UIImage(named: "alert-icon")!)
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }

    
    
    
    func wrapSaveWithdraw() -> Dictionary<String,Any> {
        var dicInfo = Dictionary<String,Any>()
        var fp = false
        if let dic = model.withdrawServerList?[0] {
            dicInfo = dic
            if let isBool = dicInfo["FacePay"] as? Bool {
             fp = isBool
            }
        }
        //"FacePay": dicInfo["FacePay"] ?? false println_debug(dic["FacePay"] as? Bool ?? false
        let appInfo = ["CellId":"","Lang": currentLong,"Lat":currentLat,"merchant_type": agentServiceTypeString(type: UserModel.shared.agentType),"MobileNo": UserModel.shared.mobileNo,"MsId": msid,"ProfileImg": UserModel.shared.proPic,"SimId": uuid,"TransType":"1","TransactionType":"PAYTO","SecureToken": UserLogin.shared.token] as [String : Any]
        let withdrawInfo = ["State": dicInfo["State"] ?? "","MobileNo": dicInfo["MobileNo"] ?? "","Myself":false,"Name":dicInfo["Name"] ?? "","NrcPassport": dicInfo["NrcPassport"] ?? "","ProfileImg": dicInfo["ProfileImg"] ?? "","ProofBack":"","ProofFront":"","Address": dicInfo["Address"] ?? "","TransType":"1","UserId": dicInfo["UserId"] ?? "","ReferanceNo": dicInfo["ReferanceNo"] ?? "","FacePay": fp] as [String : Any]
        let dic = ["AppInfo": appInfo,"WithdrawerInfo":withdrawInfo] as [String : Any]
        return dic
    }
    private func agentServiceTypeString(type: AgentType) -> String {
        switch type {
        case .user:
            return "SUBSBR"
        case .merchant:
            return "MER"
        case .agent:
            return "AGENT"
        case .advancemerchant:
            return "ADVMER"
        case .dummymerchant:
            return "DUMMY"
        case .oneStopMart:
            return "ONESTOP"
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = cellList[indexPath.row]
        cell.selectionStyle = .none
        if indexPath.row == 0 {
            return WithdrawNumberCell(cell: cell as! WithdrawContactCell)
        }else if indexPath.row == 1 {
            return WithdrawConfirmCell(cell: cell as! WithdrawConfirmCell)
        }else if indexPath.row == 2 {
            return DepositeNumberCell(cell: cell as! WithdrawContactCell)
        }else if indexPath.row == 3 {
            return DepositeConfirmCell(cell: cell as! WithdrawConfirmCell)
        }else if indexPath.row == 4 {
            return OTPWithdrawCell(cell: cell as! WithdrawOTPCell)
        }else if indexPath.row == 5 {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "WithdrawalHeaderCell") as! WithdrawalHeaderCell
            cell1.selectionStyle = .none
            return cell1
        }else if indexPath.row == 6 {
            return NumberCell(cell: cell as! InfoCellPayWithdraw)
        }else if indexPath.row == 7 {
            return NameCell(cell: cell as! InfoCellPayWithdraw)
        }else if indexPath.row == 8 {
            return NRCCell(cell: cell as! InfoCellPayWithdraw)
        }else if indexPath.row == 9 {
            return AddressCell(cell: cell as! AddressCellPayWithdraw)
        }else {
            return DepositeNumberCell(cell: cell as! WithdrawContactCell)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0  {
            return 80
        }else if indexPath.row == 1  {
            if isWithdrawNumberSelectedFromContact {
                return 0
            }else {
                return 80
            }
        }else if indexPath.row == 2 {
            if isDepositorMoreThanOne {
                return 80
            }else {
                return 0
            }
        }else if indexPath.row == 3 {
            if isDepositorMoreThanOne {
                if isDepositeNumberSelectedFromContact {
                    return 0
                }else {
                    return 80
                }
            }else {
                return 0
            }
        }else if indexPath.row == 4 {
            if isHideOTPView {
                return 0
            }else {
                return 70
            }
        }else if indexPath.row == 5 {
            return 50
        }else if indexPath.row == 6 || indexPath.row == 7{
            return 70
        }else if indexPath.row == 8 {
//            let dic = model.withdrawServerList?[0]
//            if let nrc = dic?["NrcPassport"] as? String {
//                if nrc == "" {
//                  return 0
//                }else {
//                   return 70
//                }
//            }else {
//              return 0
//            }
            return 0
        }else if indexPath.row == 9 {
//            let dic = model.withdrawServerList?[0]
//            if let nrc = dic?["Address"] as? String {
//                if nrc == "" {
//                    return 0
//                }else {
//                    let lbl = UILabel()
//                    lbl.font = UIFont(name: appFont, size: 15)
//                    if UitilityClass.heightForView(text: nrc, width: self.view.frame.width - 200, x: 0, y: 0, label: lbl) < 70 {
//                        return 70
//                    }else {
//                        return UITableView.automaticDimension
//                    }
//                }
//            }else {
//                return 0
//            }
            return 0
        }else {
            return 70
        }
    }

    
    private func WithdrawNumberCell(cell : WithdrawContactCell) -> WithdrawContactCell {
        cell.tfNumber.placeholder = "Withdrawal Number".localized
        cell.lblStatus.text = "Withdrawal Mobile Number".localized
        cell.lblStatus.textColor = hexStringToUIColor(hex: "#FF2600")
        if model.withDrawNumber == "" && model.withdrawCountryCode == "+95"{
            model.withDrawNumber = "09"
            cell.btnClose.isHidden = true
        }
        
        cell.lblCountrycode.text = "(" + model.withdrawCountryCode + ")"
        cell.imgCounty.image = UIImage(named: model.withdrawflag)
        cell.tfNumber.text = model.withDrawNumber
        cell.tfNumber.delegate = self
        cell.tfNumber.tag = 100
        cell.tfNumber.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        cell.btnClose.addTarget(self, action: #selector(onClickContactNumberClose(_:)), for: .touchUpInside)
        cell.btnContact.addTarget(self, action: #selector(onClickContactNumberContact(_:)), for: .touchUpInside)
        cell.btnContact.tag = 100
        let tap = UITapGestureRecognizer(target: self, action: #selector(WithdrawVC.onClickCountry))
        tap.name = "withdraw"
        cell.imgCounty.addGestureRecognizer(tap)
        cell.imgCounty.isUserInteractionEnabled = true
        cell.btnClose.tag = 100
        return cell
    }
    private func WithdrawConfirmCell(cell : WithdrawConfirmCell) -> WithdrawConfirmCell {
        cell.tfNumber.placeholder = "Withdrawal Confirm Number".localized
        cell.lblStatus.text = "Withdrawal Confirm Number".localized
        cell.lblStatus.textColor = hexStringToUIColor(hex: "#FF2600")
        if model.withdrawConfirmNumber == "" && model.withdrawCountryCode == "+95" {
            model.withdrawConfirmNumber = "09"
            cell.btnClose.isHidden = true
        }
        
        if model.withdrawConfirmNumber == "" {
           cell.btnClose.isHidden = true
        }
            
        cell.lblCountrycode.text = "(" + model.withdrawCountryCode + ")"
        cell.imgCounty.image = UIImage(named: model.withdrawflag)
        cell.tfNumber.text = model.withdrawConfirmNumber
        cell.tfNumber.delegate = self
        cell.tfNumber.tag = 50
        cell.tfNumber.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        cell.btnClose.addTarget(self, action: #selector(onClickContactNumberClose(_:)), for: .touchUpInside)
        cell.btnClose.tag = 50
        return cell
    }
    
    private func DepositeNumberCell(cell : WithdrawContactCell) -> WithdrawContactCell {
        cell.tfNumber.placeholder = "Depositor Number".localized
        cell.lblStatus.text = "Depositor Number".localized
        cell.lblStatus.textColor = hexStringToUIColor(hex: "#1C2C99")
        if model.depositNumber == "" && model.depositeCountryCode == "+95"{
            model.depositNumber = "09"
            cell.btnClose.isHidden = true
        }
        
        cell.lblCountrycode.text = "(" + model.depositeCountryCode + ")"
        cell.imgCounty.image = UIImage(named: model.withdrawflag)
        cell.tfNumber.text = model.depositNumber
        cell.tfNumber.delegate = self
        cell.tfNumber.tag = 200
        cell.tfNumber.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        cell.btnClose.addTarget(self, action: #selector(onClickContactNumberClose(_:)), for: .touchUpInside)
        cell.btnContact.addTarget(self, action: #selector(onClickContactNumberContact(_:)), for: .touchUpInside)
        cell.btnContact.tag = 200
        let tap = UITapGestureRecognizer(target: self, action: #selector(WithdrawVC.onClickCountry(_:)))
        tap.name = "deposite"
        cell.imgCounty.addGestureRecognizer(tap)
        cell.imgCounty.isUserInteractionEnabled = true
        cell.btnClose.tag = 200
        return cell
    }
    
    private func DepositeConfirmCell(cell : WithdrawConfirmCell) -> WithdrawConfirmCell {
        cell.tfNumber.placeholder = "Deposit Confirm Number".localized
        cell.lblStatus.text = "Deposit Confirm Number".localized
        cell.lblStatus.textColor = hexStringToUIColor(hex: "#1C2C99")
        if model.depositConfirmNumber == "" && model.depositeCountryCode == "+95"{
            model.depositConfirmNumber = "09"
            cell.btnClose.isHidden = true
        }
        cell.lblCountrycode.text = "(" + model.depositeCountryCode + ")"
        cell.imgCounty.image = UIImage(named: model.depositeflag)
        cell.tfNumber.text = model.depositConfirmNumber
        cell.tfNumber.delegate = self
        cell.tfNumber.tag = 250
        cell.tfNumber.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        cell.btnClose.addTarget(self, action: #selector(onClickContactNumberClose(_:)), for: .touchUpInside)
        cell.btnClose.tag = 250
        cell.btnClose.isHidden = true
        return cell
    }
    
    private func OTPWithdrawCell(cell : WithdrawOTPCell) -> WithdrawOTPCell {
        cell.lblStatus.text = otpLableStatus
        cell.tfFirst.delegate = self
        cell.tfFirst.myDelegate = self
        cell.tfFirst.tag = 11
        cell.tfFirst.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        
        cell.tfSecond.delegate = self
        cell.tfSecond.myDelegate = self
        cell.tfSecond.tag = 22
        cell.tfSecond.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        
        cell.tfThired.delegate = self
        cell.tfThired.myDelegate = self
        cell.tfThired.tag = 33
        cell.tfThired.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        
        cell.tfFour.delegate = self
        cell.tfFour.myDelegate = self
        cell.tfFour.tag = 44
        cell.tfFour.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        
        cell.tfFive.delegate = self
        cell.tfFive.myDelegate = self
        cell.tfFive.tag = 55
        cell.tfFive.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        
        cell.tfSix.delegate = self
        cell.tfSix.myDelegate = self
        cell.tfSix.tag = 66
        cell.tfSix.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        return cell
    }
    
    
    
    private func NumberCell(cell : InfoCellPayWithdraw) -> InfoCellPayWithdraw {
        cell.lblName.text = "Number".localized
        cell.lblTitle.text =  "(" + model.withdrawCountryCode + ") " + model.withDrawNumber
        return cell
    }
    private func NameCell(cell : InfoCellPayWithdraw) -> InfoCellPayWithdraw {
        let dic = model.withdrawServerList?[0]
        if let name = dic?["Name"] as? String {
            cell.lblTitle.text = name
        }
        cell.lblName.text = "Name".localized
        return cell
    }
    private func NRCCell(cell : InfoCellPayWithdraw) -> InfoCellPayWithdraw {
        let dic = model.withdrawServerList?[0]
        if let nrc = dic?["NrcPassport"] as? String {
            cell.lblTitle.text = nrc
        }
        cell.lblName.text = "NRC number".localized
        return cell
    }
    private func AddressCell(cell : AddressCellPayWithdraw) -> AddressCellPayWithdraw {
        let dic = model.withdrawServerList?[0]
        if let adress = dic?["Address"] as? String {
            cell.lblTitle.text = adress
        }
        cell.lblName.text = "Address".localized
        return cell
    }
    
    
    @objc func onClickContactNumberClose(_ sender: UIButton) {
        if sender.tag == 11 || sender.tag == 100 {
            if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 0, section: 0)) as? WithdrawContactCell {
                if sender.tag == 100 {
                    if model.withdrawCountryCode == "+95" {
                        cell.tfNumber.text = "09"
                    }else {
                        cell.tfNumber.text = ""
                    }

                     model.withDrawNumber = ""
                    cell.btnClose.isHidden = true
                    cell.imgCounty.image = UIImage(named: model.withdrawflag)
                    cell.lblCountrycode.text = "(" + model.withdrawCountryCode + ")"
                    
                }
                if isWithdrawNumberSelectedFromContact {
                        cell.tfNumber.resignFirstResponder()
                }else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                        cell.tfNumber.becomeFirstResponder()
                    })
                }
                
                self.hideWithdrawConfirmField()
                self.hideDepositeConfirmField()
                self.hideAddressAndOTPFields()
                self.deleteRow(delBelowIndax: 1)
                self.removeContactList()
            }
        }else if sender.tag == 12 || sender.tag == 50 {
            if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 1, section: 0)) as? WithdrawConfirmCell {
                
                if sender.tag == 50 {
                    if model.withdrawCountryCode == "+95" {
                        cell.tfNumber.text = "09"
                    }else {
                        cell.tfNumber.text = ""
                    }
                    model.withdrawConfirmNumber = ""
                    cell.btnClose.isHidden = true
                }
                self.hideDepositeAndConfirmField()
                self.hideAddressAndOTPFields()
                self.deleteRow(delBelowIndax: 2)
            }
        }else if sender.tag == 13 || sender.tag == 200 {
            if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 2, section: 0)) as? WithdrawContactCell {
                if sender.tag == 200 {
                    if model.depositeCountryCode == "+95" {
                        cell.tfNumber.text = "09"
                    }else {
                        cell.tfNumber.text = ""
                    }
                    cell.btnClose.isHidden = true
                    cell.imgCounty.image = UIImage(named: model.depositeflag)
                    cell.lblCountrycode.text = "(" + model.depositeCountryCode + ")"
                }
                if isDepositeNumberSelectedFromContact {
                       cell.tfNumber.resignFirstResponder()
                }else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                        cell.tfNumber.becomeFirstResponder()
                    })
                }
                
            
                self.hideDepositeConfirmField()
                self.hideAddressAndOTPFields()
                self.deleteRow(delBelowIndax: 3)
                self.removeContactList()
            }
        }else if sender.tag == 14 || sender.tag == 250 {
            if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 3, section: 0)) as? WithdrawConfirmCell {
                if sender.tag == 250 {
                    if model.depositeCountryCode == "+95" {
                        cell.tfNumber.text = "09"
                    }else {
                        cell.tfNumber.text = ""
                    }
                    model.depositConfirmNumber = ""
                    cell.btnClose.isHidden = true
                }
                self.hideAddressAndOTPFields()
                self.deleteRow(delBelowIndax: 4)
            }
        }
    }
    
    @objc func onClickCountry(_ sender: UITapGestureRecognizer) {
        DispatchQueue.main.async {
            let sb = UIStoryboard(name: "Country", bundle: nil)
            let countryVC = sb.instantiateViewController(withIdentifier: "CountryViewController") as! CountryViewController
            countryVC.delegate = self
            countryVC.regiCheck = "allCountry"
            self.countryListShownFor = sender.name ?? ""
            countryVC.modalPresentationStyle = .fullScreen
            self.navigationController?.present(countryVC, animated: false, completion: nil)
        }
    }
    
    @objc func onClickContactNumberContact(_ sender: UIButton){
        if sender.tag == 100 {
            contactListShownFor = "withdraw"
        }else {
            contactListShownFor = "deposite"
        }
        let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
        nav.modalPresentationStyle = .fullScreen
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    func insertRowIntoTable(ind: Int, sec: Int) {
        self.tfWithdraw.beginUpdates()
        self.tfWithdraw.insertRows(at: [IndexPath.init(row: ind, section: sec)], with: .none)
        self.tfWithdraw.endUpdates()
        UIView.animate(withDuration: 0.5, animations: {
            _ = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false) { timer in
                let indexPath = IndexPath(row: ind, section: sec)
                self.tfWithdraw.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
            }
        })
    }
    
    func insertRowIntoTableWithOutScroll(ind: Int, sec: Int) {
        self.tfWithdraw.beginUpdates()
        self.tfWithdraw.insertRows(at: [IndexPath.init(row: ind, section: sec)], with: .none)
        self.tfWithdraw.endUpdates()
    }
    func deletRowIntoTable(ind: Int, sec: Int) {
        self.tfWithdraw.beginUpdates()
        self.tfWithdraw.deleteRows(at: [IndexPath.init(row: ind, section: sec)], with: .none)
        self.tfWithdraw.endUpdates()
    }
    
    func getAddressData() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined:
                self.showlocaitonAlert()
                break
            case .denied :
                println_debug("denied")
               self.showlocaitonAlert()
                break
            case .restricted :
                println_debug("restricted")
                self.showlocaitonAlert()
                break
            case .authorizedAlways, .authorizedWhenInUse:
                locationUpdates()
                break
            }
        } else {
           self.showlocaitonAlert()
        }
    }
    func showlocaitonAlert() {
        alertViewObj.wrapAlert(title:"", body: "Please Enable Location Service".localized, img:#imageLiteral(resourceName: "location"))
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary11([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            }
            self.navigationController?.popViewController(animated: true)
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
            alertViewObj.showAlert(controller: self)
        
    }
    func openCamera(){
        self.checkCameraPermission()
    
    }
    
    func checkCameraPermission() {
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            self.openCameraAfterPermission()
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    self.openCameraAfterPermission()
                } else {
                    alertViewObj.wrapAlert(title:"", body:"Please Allow Camera Access".localized, img: UIImage(named: "alert-icon"))
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary11([:]), completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
                        }
                        self.navigationController?.popViewController(animated: true)
                    })
                    DispatchQueue.main.async {
                        alertViewObj.showAlert(controller: self)
                    }
                    
                }
            })
        }
    }
    
    
    
    func openCameraAfterPermission() {
        DispatchQueue.main.async {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.controller.allowsEditing = false
                self.controller.delegate = self
                self.controller.title = "Camera".localized
                self.controller.sourceType = UIImagePickerController.SourceType.camera
                self.controller.cameraCaptureMode = .photo
                self.controller.modalPresentationStyle = .fullScreen
                self.present(self.controller,animated: true,completion: {
                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: nil, body: "Please take withdrawal photo for payment complete process".localized, img: UIImage(named: "alert-icon")!)
                        alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        }
                        alertViewObj.showAlert(controller: self)
                    }
                })
            }
        }
    }
    
    
    func imageUploadOnServer(imageFile: UIImage) {
        showProgressView()
        if let img = imageFile as? UIImage {
            let  convertedStr = img.base64(format: .jpeg(0.1))
            DispatchQueue.main.async {
                let paramString : [String:Any] = [
                    "MobileNumber": self.model.withDrawNumber,
                    "Base64String": convertedStr as? String
                ]
                let web      = WebApiClass()
                web.delegate = self
                let ur = URL(string: "https://www.okdollar.co/RestService.svc/GetImageUrlByBase64String")//getUrl(urlStr: urlStr, serverType: .imageUpload)
                println_debug(ur)
                web.genericClass(url: ur!, param: paramString as AnyObject, httpMethod: "POST", mScreen: "UplaodImage")
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let info = convertFromUIImagePickerControllerInfoKeyDictionary5(info)
        if let image = info[convertFromUIImagePickerControllerInfoKey5(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            self.faceDetection(faceImage: image)
            }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[convertFromUIImagePickerControllerInfoKey5(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            self.faceDetection(faceImage: pickedImage)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func faceDetection(faceImage: UIImage) {
        let imageOptions =  NSDictionary(object: NSNumber(value: 1) as NSNumber, forKey: CIDetectorImageOrientation as NSString)
        let personciImage = CIImage.init(cgImage: faceImage.cgImage!)
        let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyHigh, CIDetectorEyeBlink: true, CIDetectorTypeFace: true] as [String : Any]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: accuracy)
        let faces = faceDetector?.features(in: personciImage, options: imageOptions as? [String : AnyObject])
        if let faces = faces {
            if faces.count > 1 {
                DispatchQueue.main.async {
                    alertViewObj.wrapAlert(title: "", body: "More than 1 faces are there Please reselect again".localized, img: UIImage(named: "face_no_match")!)
                    alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        self.openCamera()
                    }
                    alertViewObj.showAlert(controller: self)
                }
            } else {
                if let face = faces.first as? CIFaceFeature {
                    
                    if face.hasMouthPosition && face.hasLeftEyePosition && face.hasRightEyePosition {
                        println_debug(face)
                        self.imageUploadOnServer(imageFile: faceImage)
                    }else {
                        DispatchQueue.main.async {
                            alertViewObj.wrapAlert(title: "", body: "No face was detected. Please try again".localized, img: UIImage(named: "face_no_match")!)
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                self.openCamera()
                            }
                            alertViewObj.showAlert(controller: self)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: "", body: "No face was detected. Please try again".localized, img: UIImage(named: "face_no_match")!)
                        alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                            self.openCamera()
                        }
                        alertViewObj.showAlert(controller: self)
                    }
                }
            }
        }
    }
    
}


extension WithdrawVC {
    func locationUpdates() {
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
        LocationManager.sharedInstance.getLocation { (location: CLLocation?, error) in
            if error != nil {
                println_debug(error.debugDescription)
                return
            }
            guard let _ = location else { return }
            if let location = location {
                let lat = String(location.coordinate.latitude)
                let long = String(location.coordinate.longitude)
                self.currentLat = lat
                self.currentLong = long
                GeoLocationManager.shared.getAddressFrom(lattitude: lat, longitude:  long,language: "en") { (isSuccess, dic) in
                    DispatchQueue.main.async {
                        progressViewObj.removeProgressView()
                    }
                    if isSuccess == false {
                        DispatchQueue.main.async { progressViewObj.removeProgressView() }
                        return
                    }
                    
                    if let dict = dic as? Dictionary<String,String>{
                        let country = dict["Country"]
                        if country == "MM" {
                            let address = CurrentAddressWithdraw.init(dic: dict)
                            for item in self.allLocationsList {
                                let state_name : LocationDetail = item
                                if self.stringBeforeSpace(str: address.division) == self.stringBeforeSpace(str: state_name.stateOrDivitionNameEn) {
                                   self.model.stateCode = item.stateOrDivitionCode
                                    break
                                }
                            }
                        }else {
                            alertViewObj.wrapAlert(title:"", body:"Address not found".localized, img: #imageLiteral(resourceName: "alert-icon.png"))
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                            })
                            DispatchQueue.main.async {
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                }
            }
        }
    }
    func stringBeforeSpace(str : String) -> String{
        let delimiter = " "
        let token = str.components(separatedBy: delimiter)
        return token[0]
    }
}

fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary5(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey5(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}



struct CurrentAddressWithdraw {
    var street = ""
    var city = ""
    var division = ""
    var township = ""
    
    
    init(dic: Dictionary<String,String>) {
        if let strt = dic["street"] {
            self.street   = strt
        }
        
        if let cityData = dic["city"] {
            self.city = cityData
        }
        
        if let dvsn = dic["region"] {
            self.division = dvsn
        }
        
        if let ts = dic["township"] {
            self.township = ts
        }
    }
}

class WithdrawContactCell: UITableViewCell {
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lblStatus: UILabel!{
        didSet{
            lblStatus.font  = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgCounty: UIImageView!
    @IBOutlet weak var lblCountrycode: UILabel!
    @IBOutlet weak var tfNumber: RestrictedCursorMovement!
    @IBOutlet weak var btnContact: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}

class WithdrawConfirmCell: UITableViewCell {
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lblStatus: UILabel!{
        didSet{
            lblStatus.font  = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgCounty: UIImageView!
    @IBOutlet weak var lblCountrycode: UILabel!
    @IBOutlet weak var tfNumber: RestrictedCursorMovement!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}



class WithdrawOTPCell: UITableViewCell {
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var tfFirst: MyTextField!
    @IBOutlet weak var tfSecond: MyTextField!
    @IBOutlet weak var tfThired: MyTextField!
    @IBOutlet weak var tfFour: MyTextField!
    @IBOutlet weak var tfFive: MyTextField!
    @IBOutlet weak var tfSix: MyTextField!
    @IBOutlet weak var lblFirst: UILabel!
    @IBOutlet weak var lblSecond: UILabel!
    @IBOutlet weak var lblThired: UILabel!
    @IBOutlet weak var lblFour: UILabel!
    @IBOutlet weak var lblFice: UILabel!
    @IBOutlet weak var lblSix: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}


class InfoCellPayWithdraw: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblName: UILabel!
}

class AddressCellPayWithdraw: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblName: UILabel!
}

class WithdrawalHeaderCell: UITableViewCell {
    
    @IBOutlet weak var lblWith: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        let txt = "  " + "Withdrawal Information".localized + "   "
        lblWith.text = txt
        lblWith.layer.cornerRadius = 20
        lblWith.layer.masksToBounds = true
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary11(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
