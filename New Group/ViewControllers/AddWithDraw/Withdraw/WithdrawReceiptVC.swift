//
//  WithdrawReceiptVC.swift
//  OK
//
//  Created by Imac on 7/25/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class WithdrawReceiptVC: OKBaseController,WithdrawRecieptMenuDelegate {
 
    @IBOutlet weak var scrollBG: UIScrollView!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var withdrawView: UIView!
    @IBOutlet weak var withdrawViewBG: CardDesignView!
    
    @IBOutlet weak var lblWithdraw: UILabel!
    @IBOutlet weak var lblWithdrawBG: UILabel!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblNameBG: UILabel!
    @IBOutlet weak var lblNumberBG: UILabel!
    
    @IBOutlet weak var lblAmout: UILabel!
    @IBOutlet weak var lblAmountBG: UILabel!
    
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblCategoryInput: UILabel!
    @IBOutlet weak var lblCategoryBG: UILabel!
    @IBOutlet weak var lblCategoryInputBG: UILabel!
    
    @IBOutlet weak var lblDepositor: UILabel!
    @IBOutlet weak var lblDepositorName: UILabel!
    @IBOutlet weak var lblDipositorBG: UILabel!
    @IBOutlet weak var lblDepositorNameBG: UILabel!
    
    @IBOutlet weak var lblDepositorNumber: UILabel!
    @IBOutlet weak var lblDepositorNumberInput: UILabel!
    @IBOutlet weak var lblDepositorNumberBG: UILabel!
    @IBOutlet weak var lblDepositorNumberInputBG: UILabel!
    
    @IBOutlet weak var lblAgetAccNumber: UILabel!
    @IBOutlet weak var lblAgentAccNumberInput: UILabel!
    @IBOutlet weak var lblAgentBG: UILabel!
    @IBOutlet weak var lblAgentNumberInputBG: UILabel!
    
    
    @IBOutlet weak var lblTrasictionID: UILabel!
    @IBOutlet weak var lblTransictionIDInput: UILabel!
    @IBOutlet weak var lblTransIDBG: UILabel!
    @IBOutlet weak var lblTransInputBG: UILabel!
    
    @IBOutlet weak var lblTrasictionType: UILabel!
    @IBOutlet weak var lblTransictionTypeInput: UILabel!
    @IBOutlet weak var lblTransTypeBG: UILabel!
    @IBOutlet weak var lblTransTypeInputBG: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDateBG: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTimeBG: UILabel!
    
    @IBOutlet weak var lblHome: UILabel!
    @IBOutlet weak var lblMore: UILabel!
    @IBOutlet weak var lblBottomNote: UILabel!
    
    var receiptDic : Dictionary<String,String>?
    var withdraw_ID: String?
    var depositCountrycode: String?
    var withdrawCountrycode: String?
    let qrCode = QRCodeGenericClass()
    var formatterDepositNumber = ""
    var formatterWithdrawNumber = ""
    var formatterAgentNumber = ""
    var dateTime = ""
    var amount: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        lblHome.text = "HomePagePT".localized
        lblMore.text = "More".localized
        lblWithdraw.text = "WithdrawalR".localized
        lblWithdrawBG.text = "WithdrawalR".localized
        lblName.text = "\("Name".localized) : \(receiptDic?["WithdrawalName"] ?? "")"
        lblNameBG.text = "\("Name".localized) : \(receiptDic?["WithdrawalName"] ?? "")"
        lblAmout.attributedText = qrCode.concatinateAmountWithMMK(amount: getDigitDisplayColor(amount ?? ""))
        lblAmountBG.attributedText = qrCode.concatinateAmountWithMMK(amount: getDigitDisplayColor(amount ?? ""))
     
        if let number = receiptDic?["WithdrawalNumber"] {
            let (mobileNumber,CountryCode) = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: number)
               let cc = "(" + CountryCode + ")"
            lblNumber.text = "\("Mobile No.".localized) : \(cc + mobileNumber)"
            lblNumberBG.text = "\("Mobile No.".localized) : \(cc + mobileNumber)"
            formatterWithdrawNumber = cc + mobileNumber
        }
    
        lblCategory.text = "Categories".localized
        lblCategoryBG.text = "Categories".localized
        lblCategoryInput.text = "Face ID Pay"
        lblCategoryInputBG.text = "Face ID Pay"
        lblDepositor.text = "Depositor NameF".localized
        lblDipositorBG.text = "Depositor NameF".localized
        lblDepositorName.text = receiptDic?["DepositorName"]
        lblDepositorNameBG.text = receiptDic?["DepositorName"]
        lblDepositorNumber.text = "Depositor Mobile Number".localized
        lblDepositorNumberBG.text = "Depositor Mobile Number".localized
        
        if let number = receiptDic?["DepositorNumber"] {
             let (mobileNumber,CountryCode) = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: number)
            let cc1 = "(" + CountryCode + ")"
            lblDepositorNumberInput.text =  cc1 + mobileNumber
            lblDepositorNumberInputBG.text =  cc1 + mobileNumber
            formatterDepositNumber = cc1 + mobileNumber
        }
        lblAgetAccNumber.text = "Agent Account Number".localized
        lblAgentBG.text = "Agent Account Number".localized
        let (mobileNumber,CountryCode) = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: UserModel.shared.mobileNo)
        lblAgentAccNumberInput.text = "(" + CountryCode + ")" + mobileNumber
        lblAgentNumberInputBG.text = "(" + CountryCode + ")" + mobileNumber
        formatterAgentNumber = "(" + CountryCode + ")" + mobileNumber
        
        
        lblTrasictionID.text = "Transaction ID".localized
        lblTransIDBG.text = "Transaction ID".localized
        lblTransictionIDInput.text = withdraw_ID
        lblTransInputBG.text = withdraw_ID
        
        lblTrasictionType.text = "Transaction Type".localized
        lblTransTypeBG.text = "Transaction Type".localized
        lblTransictionTypeInput.text = "PAYTO"
        lblTransTypeInputBG.text = "PAYTO"
        
        self.lblTime.text = ""
        let (date,time) = self.qrCode.currentDateAndTime()
        self.lblDate.text = date
        self.lblDateBG.text = date
        self.lblTime.text = time
        self.lblTimeBG.text = time
        self.dateTime = "\(date)\n\(time)"
        lblBottomNote.text = "You will receive money within 15 minutes".localized
        setMarqueLabelInNavigation()
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {

    }
    
    func getDigitDisplayColor(_ digit: String) -> String {
        var FormateStr: String
        let mystring = digit.replacingOccurrences(of: ",", with: "")
        let number = NSDecimalNumber(string: mystring)
        
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        FormateStr = formatter.string(from: number)!
        if FormateStr == "NaN"{
            FormateStr = ""
        }
        return FormateStr
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.hidesBackButton = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        let newImage = self.withdrawView.toImage()
        UIImageWriteToSavedPhotosAlbum(newImage, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 18)
        lblMarque.textAlignment = .center
        lblMarque.text = "Receipt".localized
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
    }
    
    @IBAction func onClickHomeAction(_ sender: Any) {
     //   self.navigationController?.popToRootViewController(animated: false)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickMoreAction(_ sender: Any) {
        let receiptVC  = self.storyboard?.instantiateViewController(withIdentifier: "WithdrawRecieptMenu") as! WithdrawRecieptMenu
        receiptVC.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        receiptVC.delegate = self
        receiptVC.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(receiptVC, animated: false, completion: nil)
    }
    
    func optionChoosed(option: String, viewController: UIViewController) {
        viewController.dismiss(animated: false, completion: nil)
        if option == "Invoice" {
            DispatchQueue.main.async {
                let recieptVC = self.storyboard?.instantiateViewController(withIdentifier: "WithdrawPDF") as! WithdrawPDF
                recieptVC.depositDic = ["DepositorName": self.receiptDic?["DepositorName"], "DepositorNumber": self.formatterDepositNumber,"WithdrawarName": self.receiptDic?["WithdrawalName"], "WithdrawarNumber": self.formatterWithdrawNumber,"AgentNumber": self.formatterAgentNumber,"TranID": self.withdraw_ID ,"DateTime": self.dateTime,"Amount":  self.amount ?? ""] as? Dictionary<String, String>
                 self.navigationController?.pushViewController(recieptVC, animated: true)
            }
        }else if option == "Share" {
            shareQRWithDetail()
        }
    }
    
    private func shareQRWithDetail() {
        let newImage = self.withdrawViewBG.toImage()
        let activityViewController = UIActivityViewController(activityItems: [newImage], applicationActivities: nil)
        DispatchQueue.main.async {self.navigationController?.present(activityViewController, animated: true, completion: nil)}
    }

}
