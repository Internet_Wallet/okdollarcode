//
//  FaceRecognitionWithdraw.swift
//  OK
//
//  Created by Imac on 7/23/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import MobileCoreServices


protocol FaceRecognitionWithdrawDelegate {
    func faceRecongnitionStatus(status: Bool)
}

class FaceRecognitionWithdraw: OKBaseController,SuccessViewAddWDDelegate {
    @IBOutlet weak var imgAnimation: UIImageView!
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var faceView: UIView!
    @IBOutlet weak var imgFaceGif: UIImageView!
    @IBOutlet weak var lblFaceNote: UILabel!
    
    @IBOutlet weak var lblPercent: UILabel!
    @IBOutlet weak var progBar: UIProgressView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var viewProgress: UIView!
    
    
    var movieData = Data()
    var controller = UIImagePickerController()
    let gifManager = SwiftyGifManager(memoryLimit:6)
    var delegate: FaceRecognitionWithdrawDelegate?
    var depositorDic : Dictionary<String,Any>?
    
    var token = ""
    var faceTimer = Timer()
    var videoUploadTimer = Timer()
    var videoUploadCount = 0
    var timerCount = 0
    
    var alertView = Bundle.main.loadNibNamed("SuccessViewAddWD", owner: self, options: nil)?[0] as! SuccessViewAddWD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        outerView.isHidden = false
        faceView.layer.cornerRadius = 5
        faceView.layer.masksToBounds = true
        faceView.layer.borderWidth = 1.0
        faceView.layer.borderColor = UIColor.black.cgColor
        lblFaceNote.text = "Turn withdrawal head left and right \n Please make the video in 5 sec".localized
       
        lblNote.isHidden = true
        lblNote.text = "We are verifying your face please wait ...".localized
        self.lblPercent.text =  "% 0"
        viewProgress.isHidden = false
        lblStatus.text = "Video is uploading. Please wait...".localized
        self.progBar.progress = 0.0
        self.controller.videoMaximumDuration = 6
        loadFaceGIF()
        loadGIF()
        timerShowCamera()
        setMarqueLabelInNavigation()
    }
    
    func timerShowCamera() {
        DispatchQueue.main.async {
            self.faceTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
                self.timerCount += 1
                if self.timerCount == 5 {
                    self.timerCount = 0
                    self.getTokenForNRCScan()
                    self.faceTimer.invalidate()
                }
            }
        }
    }
    
    func timerShowVideoUpload() {
        
            self.videoUploadTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
                self.videoUploadCount += 1
                self.viewProgress.isHidden = false
                self.lblNote.isHidden = true
                let progress = self.videoUploadCount * 20
                if progress == 20 {
                    self.progBar.setProgress(0.2, animated: true)
                }else if progress == 40 {
                    self.progBar.setProgress(0.4, animated: true)
                }else if progress == 60 {
                    self.progBar.setProgress(0.6, animated: true)
                }else if progress == 80 {
                    self.progBar.setProgress(0.8, animated: true)
                }else if progress == 100 {
                    self.progBar.setProgress(1.0, animated: true)
                }else {
                    self.progBar.setProgress(1.0, animated: true)
                }
                print("*******************")
                self.lblPercent.text = String(progress) + " %"
                if self.videoUploadCount == 6 {
                    self.videoUploadCount = 0
                    self.viewProgress.isHidden = true
                    self.lblNote.isHidden = false
                    self.videoUploadTimer.invalidate()
                }
        }
    }
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 16)
        lblMarque.text = "Face Recognition".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white//UIColor.init(hex: "#1C2C99")
        self.navigationItem.titleView = lblMarque
    }
    
    func loadFaceGIF() {
        self.imgAnimation.gifImage = nil
        let gif = UIImage(gifName: "face_rotate2")
        self.imgFaceGif.setGifImage(gif, manager: gifManager)
    }
    
    func loadGIF() {
        self.imgAnimation.gifImage = nil
        let gif = UIImage(gifName: "multi_face_scaning_gif3")
        self.imgAnimation.setGifImage(gif, manager: gifManager)
    }
    
    @IBAction func onClickBackAction(_ sender: Any) {
        if let del = self.delegate {
            del.faceRecongnitionStatus(status: false)
            self.navigationController?.popViewController(animated: false)
        }
    }
}


extension FaceRecognitionWithdraw : UINavigationControllerDelegate, UIImagePickerControllerDelegate  {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
            print(videoURL)
            movieData = try! Data(contentsOf: videoURL as URL)
            print(movieData)
            uploadMedia(videoPath : videoURL)
        }
        self.dismiss(animated: false, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: false, completion: {
            if let del = self.delegate {
            del.faceRecongnitionStatus(status: false)
            self.navigationController?.popViewController(animated: false)
            }})
    }
    
}

extension FaceRecognitionWithdraw{
    
    
    func getTokenForNRCScan() {
        if appDelegate.checkNetworkAvail() {
            guard let url = URL(string: "https://iam.ap-southeast-1.myhwclouds.com/v3/auth/tokens") else {
                return
            }
            let dic =  ["auth":["identity":["methods":["password"],"password":["user":["domain":["name":"OkdollarAndroid"],"name":"OkdollarAndroid","password":"okdollar!@#"]]],"scope":["project":["id":"7fdc0f6d4f8e42a38691e4e32d684551"]]]]
            TopupWeb.genericClassWithHeaderInfo(url:url, param: dic as AnyObject, httpMethod: "POST", handle: {response, isSuccess in
                if isSuccess {
                    if let headerRes = response as? HTTPURLResponse {
                        self.token = headerRes.allHeaderFields["X-Subject-Token"] as? String ?? ""
                        DispatchQueue.main.async {
                            self.outerView.isHidden = true
                            self.openCamera()
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "", alertBody: "Network error and try again".localized, alertImage: UIImage(named : "alert-icon")!)
                    }
                    
                }
            })
        }else {
            DispatchQueue.main.async {
                self.showAlert(alertTitle: "", alertBody: "Network error and try again".localized, alertImage: UIImage(named : "alert-icon")!)
            }
            
        }
    }
    
    func openCamera() {
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
        self.openCameraAfterPermission()
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                   self.openCameraAfterPermission()
                } else {
                    self.OpenSettings(message: "Please Allow Camera Access".localized)
                }
            })
        }
    }
    
    func openCameraAfterPermission() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            DispatchQueue.main.async {
                self.controller.sourceType = .camera
                self.controller.cameraDevice = .front
                self.controller.mediaTypes = [kUTTypeMovie as String]
                self.controller.delegate = self
                self.present(self.controller, animated: true, completion: nil)
            }
        }else{
            print("Camera is not available")
        }
    }
    
    
    func OpenSettings(message: String) {
        alertViewObj.wrapAlert(title:"", body: message, img: UIImage(named: "alert-icon"))
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary11111([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            }
            self.navigationController?.popViewController(animated: true)
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    func uploadMedia(videoPath: URL) {
        progressViewObj.showProgressView()
        self.timerShowVideoUpload()
        let url = getUrl(urlStr: Url.URL_FaceVerifyVideoUpload, serverType: .faceIDPay)
        var request = URLRequest(url: url)
        request.httpMethod = "POST";
        let boundary = "Boundary--\(uuid)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        var user_ID = ""
        var profile_Pic = ""
        if let userID = self.depositorDic?["UserId"] as? String {
            user_ID = userID
        }
        if let prifilePic = self.depositorDic?["ProfileImg"] as? String {
            profile_Pic = prifilePic
        }
        if profile_Pic == "" && user_ID == "" {
            return
        }
    
        
        let parameters: [String: Any] = [
            "UserId": user_ID,
            "DepositorURL": profile_Pic,
            "Token": token,
            "OSType": "0"
        ]
        println_debug(parameters)
        request.timeoutInterval = 90
        request.httpBody = createBodyWithParameters(parameters: parameters as? [String : String], KeyName: "file", filePathKey: "\(videoPath)" , imageDataKey: movieData , boundary: boundary) as Data
        DispatchQueue.global(qos: .background).async {
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                 DispatchQueue.main.async {
                    self.removeProgressView()
                }
                
                if error != nil {
                    println_debug("Erroe \(String(describing: error))")
                    alertViewObj.wrapAlert(title: nil, body: "Network error and try again".localized, img: UIImage(named: "alert-icon"))
                    alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
                        if let del = self.delegate {
                            del.faceRecongnitionStatus(status: false)
                            self.navigationController?.popViewController(animated: false)
                        }
                    }
                    alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        self.openCamera()
                    }
                     DispatchQueue.main.async {
                        alertViewObj.showAlert(controller: self)
                    }
                    return
                }
                println_debug("Erroe \(String(describing: response))")
                do {
                    if let data = data {
                        let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                        if let dic = dict as? Dictionary<String,Any> {
                            println_debug(dic)
                            if dic["code"] as? Int == 200 {
                                DispatchQueue.main.async {
                                    self.navigateToPreviouVCAterFaceMatched()
                                }
                            }else  {
                                self.videoUploadTimer.invalidate()
                                self.videoUploadCount = 0
                                alertViewObj.wrapAlert(title: nil, body: "Face not matched".localized, img: UIImage(named: "face_no_match"))
                                alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
                                    if let del = self.delegate {
                                        del.faceRecongnitionStatus(status: false)
                                        self.navigationController?.popViewController(animated: false)
                                    }
                                }
                                alertViewObj.addAction(title: "Retake".localized, style: .cancel) {
                                    self.openCamera()
                                }
                                DispatchQueue.main.async {
                                    alertViewObj.showAlert(controller: self)
                                }
                            }
                        }
                    }
                }catch {
                    self.videoUploadTimer.invalidate()
                    self.videoUploadCount = 0
                    alertViewObj.wrapAlert(title: nil, body: "Network error and try again".localized, img: UIImage(named: "alert-icon"))
                    alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
                        if let del = self.delegate {
                            del.faceRecongnitionStatus(status: false)
                            self.navigationController?.popViewController(animated: false)
                        }
                    }
                    alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        self.openCamera()
                    }
                    DispatchQueue.main.async {
                        alertViewObj.showAlert(controller: self)
                    }
                }
            }
            task.resume()
        }
        
    }
    
    func createBodyWithParameters(parameters: [String: String]?, KeyName :String? , filePathKey: String?, imageDataKey: Data, boundary: String) -> NSData {
        var body = Data()
        if parameters != nil {
            for (key, value) in parameters! {
                body.append(Data("--\(boundary)\r\n".utf8))
                body.append(Data("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
                body.append(Data("\(value)\r\n".utf8))
            }
        }
        
        var filename = ""
        if filePathKey != "" {
            let fileArray = filePathKey?.components(separatedBy: "/")
            filename = (fileArray?.last)!
        }
        let mimetype = "video/mov"
        body.append(Data("--\(boundary)\r\n".utf8))
        body.append(Data("Content-Disposition: form-data; name=\"\(KeyName!)\"; filename=\"\(filename)\"\r\n".utf8))
        body.append(Data("Content-Type: \(mimetype)\r\n\r\n".utf8))
        body.append(imageDataKey as Data)
        body.append(Data("\r\n".utf8))
        body.append(Data("--\(boundary)--\r\n".utf8))
        return body as NSData
    }
    
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
            alertViewObj.showAlert(controller: self)
        
    }
    func navigateToPreviouVCAterFaceMatched() {
        DispatchQueue.main.async {
            self.videoUploadCount = 0
            self.viewProgress.isHidden = true
            self.lblNote.isHidden = false
            self.videoUploadTimer.invalidate()
            if !self.view.contains(self.alertView) {
                self.alertView.frame = self.view.frame
                self.alertView.delegate = self
                self.alertView.setMessage(txt: "Face Verification Successful".localized)
                self.view.bringSubviewToFront(self.alertView)
                self.view.addSubview(self.alertView)
            }
        }
    }

    func hideSuccessAlert(successAlert: UIView) {
        println_debug("Delegate Called in Parent controller")
        if self.view.contains(self.alertView) {
            successAlert.removeFromSuperview()
        }
        if let del = self.delegate {
            del.faceRecongnitionStatus(status: true)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
}

fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary11111(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
