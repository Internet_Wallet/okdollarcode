//
//  WithDrawnodel.swift
//  OK
//
//  Created by Imac on 7/23/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class WithDrawnodel: NSObject {
    var stateCode = ""
    var withDrawNumber = ""
    var withdrawCountryCode = ""
    var withdrawflag = ""
    var withdrawConfirmNumber = ""
    var depositNumber = ""
    var depositConfirmNumber = ""
    var depositeCountryCode = ""
    var depositeflag = ""
    var OTP = ""
    var withdrawServerList: [Dictionary<String,Any>]?
    var depositeServerList: [Dictionary<String,Any>]?
    var monyWithdrawImage = ""
    var saveWithdraw: Dictionary<String,String>?
    var withdrawTransictionID = ""
    var SMSVerificationNo = ""
    var facePay : Bool = false
    var AgentVerifyStatus = 0
    var FinalWithdrawalNumber = ""
    var FinalDepositorNumber = ""
    var finalAmount = ""
}
