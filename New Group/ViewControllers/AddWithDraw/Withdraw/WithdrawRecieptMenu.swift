//
//  WithdrawRecieptMenu.swift
//  OK
//
//  Created by Imac on 7/26/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit


protocol WithdrawRecieptMenuDelegate {
    func optionChoosed(option: String, viewController: UIViewController)
    
}

class WithdrawRecieptMenu: UIViewController {
    var delegate: WithdrawRecieptMenuDelegate?
    
    @IBOutlet weak var lblInvoice: UILabel!
    @IBOutlet weak var lblShare: UILabel!
    
    @IBOutlet weak var bgView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        bgView.layer.cornerRadius = 10
        bgView.layer.masksToBounds = true
        lblInvoice.text = "Invoice".localized
         lblShare.text = "Share".localized
    }

    @IBAction func onClickInvoiceAction(_ sender: Any) {
        if let del = delegate {
            del.optionChoosed(option: "Invoice", viewController: self)
        }
    }
    
    @IBAction func onClickShareAction(_ sender: Any) {
        if let del = delegate {
            del.optionChoosed(option: "Share", viewController: self)
        }
    }
    
    @IBAction func OnClickBG(_ sender: Any) {
           self.dismiss(animated: false, completion: nil)
    }
    
}
