//
//  WithDrawTextFieldExtension.swift
//  OK
//
//  Created by Imac on 7/23/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

extension WithdrawVC: WebServiceResponseDelegate,FaceRecognitionWithdrawDelegate,CountryViewControllerDelegate {
    
    func faceRecongnitionStatus(status: Bool) {
        if status {
            if isOTPHidden {
                isOTPHidden = false
                self.insertOTPCell()
            }
        }else {
            isHideOTPView = false
            isWithdrawNumberSelectedFromContact = false
            let btn = UIButton()
            btn.tag = 100
            self.onClickContactNumberClose(btn)
        }
        
    }
    
    @objc func textFieldDidEditChanged(_ textField : UITextField) {
        if textField.tag == 100 {
           
            if textField.text?.count ?? 0 < 4 {
                self.removeContactList()
            }else if textField.text?.count ?? 0 >= 4 {
                if let _ = tfWithdraw.cellForRow(at: IndexPath(row: 0, section: 0)) as? WithdrawContactCell {
                    let rectOfCellInTableView = self.tfWithdraw.rectForRow(at: IndexPath(row: 0, section: 0) )
                    let rectOfCellInSuperview = self.tfWithdraw.convert(rectOfCellInTableView, to: self.tfWithdraw.superview)
                    showContactList(yAsix: rectOfCellInSuperview.origin.y + rectOfCellInSuperview.height, position: "BOTTOM", contactFor: "Withdraw")
                    contactSuggestionView.loadContacts(txt: textField.text ?? "0", axis: rectOfCellInSuperview.origin.y + rectOfCellInSuperview.height, position: "BOTTOM")
                }
            }
            isWithdrawNumberSelectedFromContact = false
            model.withDrawNumber = textField.text ?? ""
            if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 0, section: 0)) as? WithdrawContactCell {
                if  cell.imgCounty.image == UIImage(named: "myanmar") {
                    let mbLength = validObj.getNumberRangeValidation(textField.text!)
                    if textField.text?.count ?? 0 >= mbLength.max || textField.text?.count ?? 0 >= mbLength.min {
                        if textField.text?.count ?? 0 >= mbLength.max {
                            self.insertContactConfirmCell(isKeyboardShow: true)
                            if !isWithdrawConrimHidden {
                                if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 1, section: 0)) as? WithdrawConfirmCell {
                                    cell.tfNumber.becomeFirstResponder()
                                }
                            }
                        }else {
                            if !isWithdrawConrimHidden {
                                let btn = UIButton()
                                btn.tag = 50
                                self.onClickContactNumberClose(btn)
                            }else {
                                self.insertContactConfirmCell(isKeyboardShow: false)
                            }
                        }
                    }else if textField.text?.count ?? 0 < mbLength.min && !isWithdrawConrimHidden {
                        isWithdrawNumberSelectedFromContact = false
                        let btn = UIButton()
                        btn.tag = 11
                        self.onClickContactNumberClose(btn)
                    }
                }else {
                    if textField.text?.count ?? 0 >= 4 {
                        if textField.text?.count ?? 0 >= 13 {
                            if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 1, section: 0)) as? WithdrawConfirmCell {
                                cell.tfNumber.becomeFirstResponder()
                            }
                        }
                        self.insertContactConfirmCell(isKeyboardShow: false)
                        if model.withDrawNumber.count != model.withdrawConfirmNumber.count && !isWithdrawConrimHidden {
                            let btn = UIButton()
                            btn.tag = 50
                            self.onClickContactNumberClose(btn)
                        }
                    }else if textField.text?.count ?? 0 < 4 && !isWithdrawConrimHidden {
                        isWithdrawNumberSelectedFromContact = false
                        let btn = UIButton()
                        btn.tag = 11
                        self.onClickContactNumberClose(btn)
                    }
                }
            }
        }else if textField.tag == 50 {
            model.withdrawConfirmNumber = textField.text ?? ""
            if validObj.checkMatchingNumber(string: textField.text ?? "", withString: model.withDrawNumber) {
                model.withdrawConfirmNumber = textField.text ?? ""
                
                if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 1, section: 0)) as? WithdrawConfirmCell {
                    if cell.imgCounty.image != UIImage(named: "myanmar") {
                        if cell.tfNumber.text?.count ?? 0 > 0 {
                            cell.btnClose.isHidden = false
                        }else {
                            cell.btnClose.isHidden = true
                        }
                    }else {
                        if cell.tfNumber.text?.count ?? 0 > 2 {
                            cell.btnClose.isHidden = false
                        }else {
                            cell.btnClose.isHidden = true
                        }
                    }
                }
                
                if model.withDrawNumber == model.withdrawConfirmNumber {
                    textField.resignFirstResponder()
                    checkWithdrawValidNumber(number: model.withdrawConfirmNumber)
                }else {
                    if !isDepositeHidden || !isOTPHidden {
                        let btn = UIButton()
                        btn.tag = 12
                        self.onClickContactNumberClose(btn)
                    }
                }
            }else {
                if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 1, section: 0)) as? WithdrawConfirmCell {
                    cell.tfNumber.text?.removeLast()
                    model.withdrawConfirmNumber = String(textField.text?.dropLast() ?? "")
                    if !isWithdrawConrimHidden  {
                        let btn = UIButton()
                        btn.tag = 12
                        self.onClickContactNumberClose(btn)
                    }
                }
            }
        }else if textField.tag == 200 {
            
            if textField.text?.count ?? 0 < 4 {
                self.removeContactList()
            }else if textField.text?.count ?? 0 >= 4 {
                if let _ = tfWithdraw.cellForRow(at: IndexPath(row: 2, section: 0)) as? WithdrawContactCell {
                    let rectOfCellInTableView = self.tfWithdraw.rectForRow(at: IndexPath(row: 2, section: 0) )
                    let rectOfCellInSuperview = self.tfWithdraw.convert(rectOfCellInTableView, to: self.tfWithdraw.superview)
                    showContactList(yAsix: rectOfCellInSuperview.origin.y, position: "TOP", contactFor: "Deposit")
                    contactSuggestionView.loadContacts(txt: textField.text ?? "0", axis: rectOfCellInSuperview.origin.y, position: "TOP")
                }
            }
            
            isDepositeNumberSelectedFromContact = false
            model.depositNumber = textField.text ?? ""
            if model.depositNumber == model.withDrawNumber {
                textField.resignFirstResponder()
                if model.withdrawCountryCode == "+95" {
                    textField.text = "09"
                }else {
                    textField.text = ""
                }
                DispatchQueue.main.async {

                alertViewObj.wrapAlert(title: nil, body: "Depositor number and withdrawal number should not be same".localized, img: UIImage(named: "alert-icon")!)
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                }
                    alertViewObj.showAlert(controller: self)
                }
            }else{
                if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 2, section: 0)) as? WithdrawContactCell {
                    if  cell.imgCounty.image == UIImage(named: "myanmar") {
                        let mbLength = validObj.getNumberRangeValidation(textField.text!)
                        if textField.text?.count ?? 0 >= mbLength.max || textField.text?.count ?? 0 >= mbLength.min {
                            if textField.text?.count ?? 0 >= mbLength.max {
                                insertDepositeConfirmCell(isKeyboardShow: true)
                                if !isDepositeConrimHidden {
                                    if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 3, section: 0)) as? WithdrawConfirmCell {
                                        cell.tfNumber.becomeFirstResponder()
                                    }
                                }
                            }else {
                                if !isDepositeConrimHidden {
                                    let btn = UIButton()
                                    btn.tag = 250
                                    self.onClickContactNumberClose(btn)
                                }else {
                                    insertDepositeConfirmCell(isKeyboardShow: false)
                                }
                            }
                        }else if textField.text?.count ?? 0 < mbLength.min && !isDepositeConrimHidden {
                            let btn = UIButton()
                            btn.tag = 13
                            self.onClickContactNumberClose(btn)
                        }
                    }else {
                        if textField.text?.count ?? 0 >= 4 {
                            if textField.text?.count ?? 0 >= 13 {
                                if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 3, section: 0)) as? WithdrawConfirmCell {
                                    cell.tfNumber.becomeFirstResponder()
                                }
                            }
                            self.insertDepositeConfirmCell(isKeyboardShow: false)
                            if model.depositNumber.count != model.depositConfirmNumber.count && !isDepositeConrimHidden {
                                let btn = UIButton()
                                btn.tag = 250
                                self.onClickContactNumberClose(btn)
                            }
                        }else if textField.text?.count ?? 0 < 4 && !isDepositeConrimHidden {
                            let btn = UIButton()
                            btn.tag = 13
                            self.onClickContactNumberClose(btn)
                        }
                    }
                }
            }
        }else if textField.tag == 250 {
            model.depositConfirmNumber = textField.text ?? ""
            if validObj.checkMatchingNumber(string: textField.text ?? "", withString: model.depositNumber) {
                model.depositConfirmNumber = textField.text ?? ""
                if model.depositNumber == model.depositConfirmNumber {
                    textField.resignFirstResponder()
                    checkDepositeValidNumber(number: model.depositConfirmNumber)
                }else {
                    if !isDepositeConrimHidden || !isOTPHidden {
                        let btn = UIButton()
                        btn.tag = 14
                        self.onClickContactNumberClose(btn)
                    }
                }
            }else {
                if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 3, section: 0)) as? WithdrawConfirmCell {
                    cell.tfNumber.text?.removeLast()
                    model.depositConfirmNumber = String(textField.text?.dropLast() ?? "")
                    if !isDepositeConrimHidden  {
                        let btn = UIButton()
                        btn.tag = 14
                        self.onClickContactNumberClose(btn)
                    }
                }
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.removeContactList()
        if textField.tag == 11 || textField.tag == 22 || textField.tag == 33 || textField.tag == 44 || textField.tag == 55 || textField.tag == 66 {
            if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 4, section: 0)) as? WithdrawOTPCell {
                if cell.tfFirst.text != "" && cell.tfSecond.text != "" && cell.tfThired.text != "" && cell.tfFour.text != "" && cell.tfFive.text != "" && cell.tfSix.text != "" {
                    // Call OTP API
                    var otp = ""
                    let first = cell.tfFirst.text
                    let second = cell.tfSecond.text
                    let third = cell.tfThired.text
                    let four = cell.tfFour.text
                    let five = cell.tfFive.text
                    let six = cell.tfSix.text
                    otp = String(first ?? "") + String(second ?? "")
                    otp = otp + String(third ?? "") + String(four ?? "")
                    otp = otp + String(five ?? "") + String(six ?? "")
                    if otp.count == 6 {
                        self.model.OTP = otp
                        if model.facePay == false {
                             self.OTPCheck(otp: otp)
                        }else if model.AgentVerifyStatus == 201 {
                            self.SMSVerificationAPI()
                        }else if model.AgentVerifyStatus == 205 {
                           self.OTPCheck(otp: otp)
                        }
                    }else {
                        showAlert(alertTitle: "", alertBody: "Please enter OTP".localized, alertImage: UIImage(named: "error")!)
                    }
                    
                }
                cell.lblFirst.backgroundColor = UIColor.lightGray
                cell.lblSecond.backgroundColor = UIColor.lightGray
                cell.lblThired.backgroundColor = UIColor.lightGray
                cell.lblFour.backgroundColor = UIColor.lightGray
                cell.lblFice.backgroundColor = UIColor.lightGray
                cell.lblSix.backgroundColor = UIColor.lightGray
            }
        }
    }
    
    func textFieldDidDelete() {
        if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 4, section: 0)) as? WithdrawOTPCell {
            if cell.tfSecond.text?.count == 0 {
                cell.tfFirst.text = ""
                cell.tfFirst.becomeFirstResponder()
            }else if cell.tfThired.text?.count == 0 {
                cell.tfSecond.text = ""
                cell.tfSecond.becomeFirstResponder()
            }else if cell.tfFour.text?.count == 0 {
                cell.tfThired.text = ""
                cell.tfThired.becomeFirstResponder()
            }else if cell.tfFive.text?.count == 0 {
                cell.tfFour.text = ""
                cell.tfFour.becomeFirstResponder()
            }else if cell.tfSix.text?.count == 0 {
                cell.tfFive.text = ""
                cell.tfFive.becomeFirstResponder()
            }
        }
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 11 || textField.tag == 22 || textField.tag == 33 || textField.tag == 44 || textField.tag == 55 || textField.tag == 66 {

            if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 4, section: 0)) as? WithdrawOTPCell {
               
                if textField.tag == 11 {
                    cell.lblFirst.backgroundColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.5764705882, alpha: 1)
                    cell.lblSecond.backgroundColor = UIColor.lightGray
                    cell.lblThired.backgroundColor = UIColor.lightGray
                    cell.lblFour.backgroundColor = UIColor.lightGray
                    cell.lblFice.backgroundColor = UIColor.lightGray
                    cell.lblSix.backgroundColor = UIColor.lightGray
                    if textField.text?.count == 0 {
                         otpLableStatus = "Enter Withdrawal OTP".localized
                    }else {
                          otpLableStatus = "Withdrawal OTP".localized
                    }
                }else if textField.tag == 22 {
                    cell.lblFirst.backgroundColor = UIColor.lightGray
                    cell.lblSecond.backgroundColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.5764705882, alpha: 1)
                    cell.lblThired.backgroundColor = UIColor.lightGray
                    cell.lblFour.backgroundColor = UIColor.lightGray
                    cell.lblFice.backgroundColor = UIColor.lightGray
                    cell.lblSix.backgroundColor = UIColor.lightGray
                    otpLableStatus = "Withdrawal OTP".localized
                }else if textField.tag == 33 {
                    cell.lblFirst.backgroundColor = UIColor.lightGray
                    cell.lblSecond.backgroundColor = UIColor.lightGray
                    cell.lblThired.backgroundColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.5764705882, alpha: 1)
                    cell.lblFour.backgroundColor = UIColor.lightGray
                    cell.lblFice.backgroundColor = UIColor.lightGray
                    cell.lblSix.backgroundColor = UIColor.lightGray
                    otpLableStatus = "Withdrawal OTP".localized
                }else if textField.tag == 44 {
                    cell.lblFirst.backgroundColor = UIColor.lightGray
                    cell.lblSecond.backgroundColor = UIColor.lightGray
                    cell.lblThired.backgroundColor = UIColor.lightGray
                    cell.lblFour.backgroundColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.5764705882, alpha: 1)
                    cell.lblFice.backgroundColor = UIColor.lightGray
                    cell.lblSix.backgroundColor = UIColor.lightGray
                    otpLableStatus = "Withdrawal OTP".localized
                }else if textField.tag == 55 {
                    cell.lblFirst.backgroundColor = UIColor.lightGray
                    cell.lblSecond.backgroundColor = UIColor.lightGray
                    cell.lblThired.backgroundColor = UIColor.lightGray
                    cell.lblFour.backgroundColor = UIColor.lightGray
                    cell.lblFice.backgroundColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.5764705882, alpha: 1)
                    cell.lblSix.textColor = UIColor.lightGray
                    otpLableStatus = "Withdrawal OTP".localized
                }else if textField.tag == 66 {
                    cell.lblFirst.backgroundColor = UIColor.lightGray
                    cell.lblSecond.backgroundColor = UIColor.lightGray
                    cell.lblThired.backgroundColor = UIColor.lightGray
                    cell.lblFour.backgroundColor = UIColor.lightGray
                    cell.lblFice.backgroundColor = UIColor.lightGray
                    cell.lblSix.backgroundColor = #colorLiteral(red: 0, green: 0.568627451, blue: 0.5764705882, alpha: 1)
                    otpLableStatus = "Withdrawal OTP".localized
                }
                cell.lblStatus.text = otpLableStatus
            }
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if range.location == 0 && string == " " {
            return false
        }
        
        if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
            let endOfDocument = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
            return false
        }
        
        let chars = textField.text! + string
        if textField.tag == 100 {
            let mbLength = validObj.getNumberRangeValidation(chars)
              let isRejected = validObj.getNumberRangeValidation(chars).isRejected
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "1234567890").inverted).joined(separator: "")) { return false }
            if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 0, section: 0)) as? WithdrawContactCell {
              
                if cell.imgCounty.image == UIImage(named: "myanmar") {
                    if text.count > 2 {
                        cell.btnClose.isHidden = false
                    }else {
                        cell.btnClose.isHidden = true
                    }
                    if range.location == 0 && range.length > 1 {
                        textField.text = "09"
                        cell.btnClose.isHidden = true
                        return false
                    }
                    if range.location == 1 {
                        return false
                    }
                    if isRejected {
                        showToast(message: "Invalid Mobile Number".localized, align: .center)
                        textField.text = "09"
                        cell.btnClose.isHidden = true
                        return false
                    }
                    if chars.count > mbLength.max {
                        textField.resignFirstResponder()
                        return false
                    }
                }else {
                    if text.count > 0 {
                        cell.btnClose.isHidden = false
                    }else {
                        cell.btnClose.isHidden = true
                    }
                    if chars.count > 13 {
                        textField.resignFirstResponder()
                        return false
                    }
                }
                
            }
        }else  if textField.tag == 200 {
            let mbLength = validObj.getNumberRangeValidation(chars)
              let isRejected = validObj.getNumberRangeValidation(chars).isRejected
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "1234567890").inverted).joined(separator: "")) { return false }
            if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 2, section: 0)) as? WithdrawContactCell {
             
                
                if cell.imgCounty.image == UIImage(named: "myanmar") {
                    if text.count > 2 {
                        cell.btnClose.isHidden = false
                    }else {
                        cell.btnClose.isHidden = true
                    }
                    if range.location == 0 && range.length > 1 {
                        textField.text = "09"
                        cell.btnClose.isHidden = true
                        return false
                    }
                    if range.location == 1 {
                        return false
                    }
                    if isRejected {
                        showToast(message: "Invalid Mobile Number".localized, align: .center)
                        textField.text = "09"
                        cell.btnClose.isHidden = true
                        return false
                    }
                    if chars.count > mbLength.max {
                        textField.resignFirstResponder()
                        return false
                    }
                }else {
                    if text.count > 0 {
                        cell.btnClose.isHidden = false
                    }else {
                        cell.btnClose.isHidden = true
                    }
                }
            }
        }else if textField.tag == 50 {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "1234567890").inverted).joined(separator: "")) { return false }
            if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 1, section: 0)) as? WithdrawConfirmCell {
                let mbLength = validObj.getNumberRangeValidation(chars)
                if cell.imgCounty.image == UIImage(named: "myanmar") {
                  
                    if range.location == 0 && range.length > 1 {
                        textField.text = "09"
                        cell.btnClose.isHidden = true
                        return false
                    }
                    if range.location == 1 {
                        return false
                    }
                    if chars.count > mbLength.max {
                       // textField.resignFirstResponder()
                        return false
                    }
                }else {

                }
            }else {
                return false
            }
        }else if textField.tag == 250 {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "1234567890").inverted).joined(separator: "")) { return false }
            if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 3, section: 0)) as? WithdrawConfirmCell {
                if cell.imgCounty.image == UIImage(named: "myanmar") {
                    let mbLength = validObj.getNumberRangeValidation(chars)
                    if text.count > 2 {
                        cell.btnClose.isHidden = false
                    }else {
                        cell.btnClose.isHidden = true
                    }
                    if range.location == 0 && range.length > 1 {
                        textField.text = "09"
                        cell.btnClose.isHidden = true
                        return false
                    }
                    if range.location == 1 {
                        return false
                    }
                    if chars.count > mbLength.max {
                        //textField.resignFirstResponder()
                        return false
                    }
                }else {
                    if text.count > 0 {
                        cell.btnClose.isHidden = false
                    }else {
                        cell.btnClose.isHidden = true
                    }
                }
              
            }
        }else  if textField.tag == 11 || textField.tag == 22 || textField.tag == 33 || textField.tag == 44 || textField.tag == 55 || textField.tag == 66 {
            if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 4, section: 0)) as? WithdrawOTPCell {
                if (textField.text!.count < 1  && string.count > 0){
                    let nextTag = textField.tag + 11
                    var nextResponder = cell.superview?.viewWithTag(nextTag)
                    if (nextResponder == nil){
                        nextResponder = cell.superview?.viewWithTag(nextTag)
                    }
                    textField.text = string
                    nextResponder?.becomeFirstResponder()
                    if cell.tfFirst.text != "" && cell.tfSecond.text != "" && cell.tfThired.text != "" && cell.tfFour.text != "" && cell.tfFive.text != "" && cell.tfSix.text != "" {
                        textField.resignFirstResponder()
                    }
                    return false
                }else if (textField.text!.count >= 1  && string.count == 0){
                    let previousTag = textField.tag - 11
                    var previousResponder = cell.superview?.viewWithTag(previousTag)
                    if (previousResponder == nil){
                        previousResponder = cell.superview?.viewWithTag(previousTag)
                    }
                    textField.text = ""
                    previousResponder?.becomeFirstResponder()
                    return false
                }else if (textField.text!.count >= 1  && string.count > 0) {
                    return false
                }
            }
        }
        return true
    }
    
    func insertContactConfirmCell(isKeyboardShow: Bool) {
        if isWithdrawConrimHidden {
            isWithdrawConrimHidden = false
            model.withdrawConfirmNumber = ""
            cellList.append(tfWithdraw.dequeueReusableCell(withIdentifier: "WithdrawConfirmCell") as! WithdrawConfirmCell)
            self.insertRowIntoTableWithOutScroll(ind: 1, sec: 0)
            if isKeyboardShow {
                Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { timer in
                    if let cell = self.tfWithdraw.cellForRow(at: IndexPath(row: 1, section: 0)) as? WithdrawConfirmCell {
                        cell.tfNumber.becomeFirstResponder()
                    }
                }
            }
        }
    }
    
    func insertDepositeCell() {
        if isDepositeHidden {
            isDepositeHidden = false
            model.depositNumber = ""
            cellList.append(tfWithdraw.dequeueReusableCell(withIdentifier: "WithdrawContactCell") as! WithdrawContactCell)
            self.insertRowIntoTable(ind: 2, sec: 0)
            Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { timer in
                if let cell = self.tfWithdraw.cellForRow(at: IndexPath(row: 2, section: 0)) as? WithdrawContactCell {
                    cell.tfNumber.becomeFirstResponder()
                }
            }
        }
    }
    func insertDepositeConfirmCell(isKeyboardShow: Bool) {
        if isDepositeConrimHidden {
            isDepositeConrimHidden = false
            model.depositConfirmNumber = ""
            cellList.append(tfWithdraw.dequeueReusableCell(withIdentifier: "WithdrawConfirmCell") as! WithdrawConfirmCell)
            self.insertRowIntoTableWithOutScroll(ind: 3, sec: 0)
            if isKeyboardShow {
                Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { timer in
                    if let cell = self.tfWithdraw.cellForRow(at: IndexPath(row: 3, section: 0)) as? WithdrawConfirmCell {
                        cell.tfNumber.becomeFirstResponder()
                    }
                }
            }
        }
    }
    
    func insertOTPCell() {
        if isDepositeHidden {
            self.insertDepositeCell()
        }
        if isDepositeConrimHidden {
            self.insertDepositeConfirmCell(isKeyboardShow: false)
        }
        model.OTP = ""
        cellList.append(tfWithdraw.dequeueReusableCell(withIdentifier: "WithdrawOTPCell") as! WithdrawOTPCell)
        self.insertRowIntoTable(ind: 4, sec: 0)
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { timer in
            if let cell = self.tfWithdraw.cellForRow(at: IndexPath(row: 4, section: 0)) as? WithdrawOTPCell {
                cell.tfFirst.text = ""
                cell.tfSecond.text = ""
                cell.tfThired.text = ""
                cell.tfFour.text = ""
                cell.tfFive.text = ""
                cell.tfSix.text = ""
                cell.tfFirst.becomeFirstResponder()
            }
        }
    }
    
    func deleteRow(delBelowIndax: Int) {
        DispatchQueue.main.async {
            if self.cellList.count > delBelowIndax {
                var index = self.cellList.count - 1
                for _ in self.cellList {
                    self.cellList.remove(at: index)
                    self.deletRowIntoTable(ind: index, sec: 0)
                    index -= 1
                    if !(index >= delBelowIndax) {
                        break
                    }
                }
            }
        }
    }
    
    func hideWithdrawConfirmField() {
        model.withdrawConfirmNumber = ""
        isWithdrawConrimHidden = true
        isHideOTPView = false
        otpLableStatus = "Enter Withdrawal OTP".localized
        hideDepositeAndConfirmField()
    }
    
    func hideDepositeAndConfirmField() {
        model.depositNumber = ""
        model.depositConfirmNumber = ""
        model.depositeCountryCode = "+95"
        model.depositeflag = "myanmar"
        isDepositeHidden = true
        isDepositeConrimHidden = true
        isDepositorMoreThanOne = false
        isHideOTPView = false
        otpLableStatus = "Enter Withdrawal OTP".localized
        hideAddressAndOTPFields()
    }
    
    func hideDepositeConfirmField() {
        model.depositConfirmNumber = ""
        isDepositeConrimHidden = true
        isHideOTPView = false
        otpLableStatus = "Enter Withdrawal OTP".localized
        hideAddressAndOTPFields()
    }
    
    func hideAddressAndOTPFields() {
        model.OTP = ""
        isOTPHidden = true
        isWithdrawInfoHidden = true
        isNameHidden = true
        isNumberHidden  = true
        isNRCHidden = true
        isAddressHidden = true
        isHideOTPView = false
        self.tvBottom.constant = -50
        self.btnPay.isHidden = true
    }
    
    func nevigateToFaceRecognisation() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FaceRecognitionWithdraw") as! FaceRecognitionWithdraw
        vc.delegate = self
        vc.depositorDic = model.withdrawServerList?[0]
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func insertNumberNameNRCAddressCell() {
        
        if isWithdrawInfoHidden {
            isWithdrawInfoHidden = false
            cellList.append(tfWithdraw.dequeueReusableCell(withIdentifier: "WithdrawalHeaderCell") as! WithdrawalHeaderCell)
            self.insertRowIntoTable(ind: 5, sec: 0)
        }
        
        if isNumberHidden {
            isNumberHidden = false
            cellList.append(tfWithdraw.dequeueReusableCell(withIdentifier: "InfoCellPayWithdraw") as! InfoCellPayWithdraw)
            self.insertRowIntoTable(ind: 6, sec: 0)
        }
        if isNameHidden{
            isNameHidden = false
            cellList.append(tfWithdraw.dequeueReusableCell(withIdentifier: "InfoCellPayWithdraw") as! InfoCellPayWithdraw)
            self.insertRowIntoTable(ind: 7, sec: 0)
        }
        if isNRCHidden {
            isNRCHidden = false
            cellList.append(tfWithdraw.dequeueReusableCell(withIdentifier: "InfoCellPayWithdraw") as! InfoCellPayWithdraw)
            self.insertRowIntoTable(ind: 8, sec: 0)
        }
        if isAddressHidden {
            isAddressHidden = false
            cellList.append(tfWithdraw.dequeueReusableCell(withIdentifier: "AddressCellPayWithdraw") as! AddressCellPayWithdraw)
            self.insertRowIntoTable(ind: 9, sec: 0)
        }
        //hide OTP Field
        isHideOTPView = true
        self.resetOTPField()
        tfWithdraw.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .none)
        
        btnPay.setTitle("Next".localized, for: .normal)
        
    }
    
    
    func resetOTPField() {
        DispatchQueue.main.async {
            if let cell = self.tfWithdraw.cellForRow(at: IndexPath(row: 4, section: 0)) as? WithdrawOTPCell {
                self.otpLableStatus = "Enter Withdrawal OTP".localized
                cell.lblStatus.text = self.otpLableStatus
                cell.tfFirst.text = ""
                cell.tfSecond.text = ""
                cell.tfThired.text = ""
                cell.tfFour.text = ""
                cell.tfFive.text = ""
                cell.tfSix.text = ""
                cell.lblFirst.backgroundColor = UIColor.lightGray
                cell.lblSecond.backgroundColor = UIColor.lightGray
                cell.lblThired.backgroundColor = UIColor.lightGray
                cell.lblFour.backgroundColor = UIColor.lightGray
                cell.lblFice.backgroundColor = UIColor.lightGray
                cell.lblSix.backgroundColor = UIColor.lightGray
            }
        }
    }
    
    func countryViewController(_ list: CountryViewController, country: Country) {
        if countryListShownFor == "withdraw" {
            self.model.withdrawflag = country.code
            self.model.withdrawCountryCode = country.dialCode
            let btn = UIButton()
            btn.tag = 100
            self.onClickContactNumberClose(btn)
        }else {
            self.model.depositeflag = country.code
            self.model.depositeCountryCode = country.dialCode
            let btn = UIButton()
            btn.tag = 200
            self.onClickContactNumberClose(btn)
        }
        list.dismiss(animated: false, completion: nil)
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: false, completion: nil)
    }
    
    
    func checkWithdrawValidNumber(number : String) {
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            
            let web      = WebApiClass()
            web.delegate = self
            let ur = getUrl(urlStr: Url.URL_FacePayVerifyRefaranceNo, serverType: .faceIDPay)
            let dic = [
                "WithdrawerNo":formateValidNumber(num: number, coutryCode: model.withdrawCountryCode),
                "State": model.stateCode
            ]
            println_debug(ur)
            println_debug(dic)
            web.genericClass(url: ur, param: dic as AnyObject, httpMethod: "POST", mScreen: "checkWithdrawValidNumber")
        }else {
            DispatchQueue.main.async {

            alertViewObj.wrapAlert(title: nil, body: "Please check your internet connection".localized, img: UIImage(named: "alert-icon")!)
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                self.faceRecongnitionStatus(status: false)
            }
                alertViewObj.showAlert(controller: self)
            }

        }
    }
    
    func checkDepositeValidNumber(number : String) {
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let web      = WebApiClass()
            web.delegate = self
            let ur = getUrl(urlStr: Url.URL_FacePayVerifySenderNo, serverType: .faceIDPay)
            let dic = [
                "WithdrawerNo":formateValidNumber(num: model.withDrawNumber, coutryCode: model.withdrawCountryCode),
                "ReferanceNo": formateValidNumber(num: number, coutryCode: model.depositeCountryCode),
                "State": model.stateCode
            ]
            println_debug(ur)
            println_debug(dic)
            web.genericClass(url: ur, param: dic as AnyObject, httpMethod: "POST", mScreen: "checkDepositeValidNumber")
        }else {
            DispatchQueue.main.async {

            alertViewObj.wrapAlert(title: nil, body: "Please check your internet connection".localized, img: UIImage(named: "alert-icon")!)
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                let btn = UIButton()
                btn.tag = 200
                self.isDepositeNumberSelectedFromContact = false
                self.onClickContactNumberClose(btn)
            }
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    func OTPCheck(otp : String) {
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let web      = WebApiClass()
            web.delegate = self
            let ur = getUrl(urlStr: Url.URL_FacePayOtpVerification, serverType: .faceIDPay)
            var user_ID = ""
            let dic = model.withdrawServerList?[0]
            if let userID = dic?["UserId"] as? String {
                user_ID = userID
            }
            let dic1 = [
                "UserId": user_ID,
                "MobileNo": formateValidNumber(num: model.withDrawNumber, coutryCode: model.withdrawCountryCode),
                "Otp": otp
            ]
            println_debug(ur)
            println_debug(dic1)
            web.genericClass(url: ur, param: dic1 as AnyObject, httpMethod: "POST", mScreen: "OTPCheck")
        }else {
            DispatchQueue.main.async {

            alertViewObj.wrapAlert(title: nil, body: "Please check your internet connection".localized, img: UIImage(named: "alert-icon")!)
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    func OTPResend() {
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let web      = WebApiClass()
            web.delegate = self
           let ur = getUrl(urlStr: Url.URL_FacePayResendOtp, serverType: .faceIDPay)
            let dic = [
                "UserId":"5d208fdb0df168668e1d2af4",
                "MobileNo": formateValidNumber(num: model.withDrawNumber, coutryCode: model.withdrawCountryCode),
            ]
            println_debug(ur)
            println_debug(dic)
            web.genericClass(url: ur, param: dic as AnyObject, httpMethod: "POST", mScreen: "OTPResend")
        }else {
            DispatchQueue.main.async {

            alertViewObj.wrapAlert(title: nil, body: "Please check your internet connection".localized, img: UIImage(named: "alert-icon")!)
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        if screen == "checkWithdrawValidNumber" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    println_debug("checkWithdrawValidNumber : -----> \(dict)")
                    if let dic = dict as? Dictionary<String,Any> {
                        if dic["code"] as? Int == 200 {
                            if let data = dic["data"] as? [Dictionary<String, Any>] {
                                println_debug(data)
                                self.model.withdrawServerList = data
                                self.previousCCWithdraw = model.withdrawCountryCode
                                self.previousCFWithdraw = model.withdrawflag
                                
                                if data.count > 1 {
                                    self.isDepositorMoreThanOne = true
                                    DispatchQueue.main.async {
                                        self.insertContactConfirmCell(isKeyboardShow: false)
                                        self.insertDepositeCell()
                                    }
                                }else {
                                    self.isDepositorMoreThanOne = false
                                    var amountString = ""
                                     var imagString = ""
                                    if self.model.SMSVerificationNo == "" {
                                        DispatchQueue.main.async {

                                        alertViewObj.wrapAlert(title:"", body: "We are getting SMS Verification Number, Please try again".localized, img: UIImage(named: "Contact Mobile Number")!)
                                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                            self.getNumberToSendSMS()
                                        })
                                            alertViewObj.showAlert(controller: self)
                                        }
                                        return
                                    }
                                    let number = "(+95)" + self.model.SMSVerificationNo
                                    if let dic = self.model.withdrawServerList?[0] {
                                        if let amount = dic["Amount"] as? String {
                                            println_debug(amount)
                                            let amountLimit = dic["AmountLimit"] as? NSNumber ?? 0
                                            let amountStr = getDigitDisplay(amountLimit.stringValue)
                                            println_debug(amountLimit)
                                            if let dic = self.model.withdrawServerList?[0] {
                                                if let facePay = dic["FacePay"] as? NSNumber {
                                                    if facePay.boolValue {
                                                    self.model.facePay = true
                                                    amountString = "Do you have cash in hand minimum amount".localized + " " + amountStr + " " + "MMK & above. ".localized + " " + "Please inform withdrawal for sending OK Dollar text SMS to".localized + " " + number + " " + "from withdrawal mobile number.".localized
                                                         imagString = "sms-alert"
                                                    }else {
                                                      amountString = "Do you have cash in hand minimum amount".localized + " " + amountStr + " " + "MMK & above. ".localized
                                                        imagString = "minimum_deposit"
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    DispatchQueue.main.async {

                                    alertViewObj.wrapAlert(title: nil, body: amountString, img: UIImage(named: imagString)!)
                                    alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
                                        self.btnPay.setTitle("".localized, for: .normal)
                                        self.faceRecongnitionStatus(status: false)
                                        return
                                    }
                                    alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                        self.btnPay.setTitle("Pay".localized, for: .normal)
                                        if self.isWithdrawNumberSelectedFromContact {
                                            DispatchQueue.main.async {
                                                self.insertContactConfirmCell(isKeyboardShow: false)
                                            }
                                        }
                                        if let dic = self.model.withdrawServerList?[0] {
                                            if let facePay = dic["FacePay"] as? NSNumber {
                                                if facePay.boolValue {
                                                    self.model.facePay = true
                                                    if let num = dic["MobileNo"] as? String {
                                                        self.model.FinalWithdrawalNumber = num
                                                        self.model.FinalDepositorNumber = dic["ReferanceNo"] as? String ?? ""
                                                        self.AgentVerify(withdrawalNum: num, depositNumber: dic["ReferanceNo"] as? String ?? "")
                                                    }
                                                }else {
                                                    self.model.facePay = false
                                                    DispatchQueue.main.async {
                                                        self.nevigateToFaceRecognisation()
                                                    }
                                                }
                                            }
                                        }
                                    }
                                        alertViewObj.showAlert(controller: self)
                                    }
                                }
                            }
                        }else if dic["code"] as? Int == 300 {
                            DispatchQueue.main.async {

                            alertViewObj.wrapAlert(title: nil, body: "Location Not Matched".localized, img: UIImage(named: "location_nomatch")!)
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                              self.faceRecongnitionStatus(status: false)
                            }
                                alertViewObj.showAlert(controller: self)
                            }
                        }else if dic["code"] as? Int == 301 {
                            DispatchQueue.main.async {

                            alertViewObj.wrapAlert(title: nil, body: "There is no record found on Your entered mobile number".localized, img: UIImage(named: "no_deposit")!)
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                self.faceRecongnitionStatus(status: false)
                            }
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                }else {
                    DispatchQueue.main.async {

                    alertViewObj.wrapAlert(title: nil, body: "Technical error in server side, Please try again later.".localized, img: UIImage(named: "alert-icon")!)
                    alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        self.faceRecongnitionStatus(status: false)
                    }
                        alertViewObj.showAlert(controller: self)
                    }
                }
            }catch {
                DispatchQueue.main.async {

                alertViewObj.wrapAlert(title: nil, body: "Technical error in server side, Please try again later.".localized, img: UIImage(named: "alert-icon")!)
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                    self.faceRecongnitionStatus(status: false)
                }
                    alertViewObj.showAlert(controller: self)
                }
            }
        }else if screen == "checkDepositeValidNumber" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,Any> {
                         println_debug("checkDepositeValidNumber : -----> \(dic)")
                        if dic["code"] as? Int == 200 {
                            if let data = dic["data"] as? [Dictionary<String, Any>] {
                                self.model.withdrawServerList = data
                                
                                var amountString = ""
                                var imagString = ""
                                if self.model.SMSVerificationNo == "" {
                                    DispatchQueue.main.async {

                                    alertViewObj.wrapAlert(title:"", body: "We are getting SMS Verification Number, Please try again".localized, img: UIImage(named: "Contact Mobile Number")!)
                                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                        self.getNumberToSendSMS()
                                    })
                                        alertViewObj.showAlert(controller: self)
                                    }
                                    return
                                }
                                let number = "(+95)" + self.model.SMSVerificationNo
                                
                                if let dic = self.model.withdrawServerList?[0] {
                                    if let amount = dic["Amount"] as? String {
                                        println_debug(amount)
                                        let amountLimit = dic["AmountLimit"] as? NSNumber ?? 0
                                        let amountStr = getDigitDisplay(amountLimit.stringValue)
                                        println_debug(amountLimit)
                                        if let dic = self.model.withdrawServerList?[0] {
                                            if let facePay = dic["FacePay"] as? NSNumber {
                                                if facePay.boolValue {
                                                    self.model.facePay = true
                                                    amountString = "Do you have cash in hand minimum amount".localized + " " +  amountStr + " " + " " + "MMK & above. ".localized + " " + "Please inform withdrawal for sending OK Dollar text SMS to".localized + " " + number + " " + "from withdrawal mobile number.".localized
                                                    imagString = "sms-alert"
                                                }else {
                                                    amountString = "Do you have cash in hand minimum amount".localized + " " + amountStr + " " + "MMK & above. ".localized
                                                     imagString = "minimum_deposit"
                                                }
                                            }
                                        }
                                    }
                                }
                                DispatchQueue.main.async {
                                    alertViewObj.wrapAlert(title: nil, body: amountString, img: UIImage(named: imagString)!)
                                    alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
                                        self.btnPay.setTitle("".localized, for: .normal)
                                        let btn = UIButton()
                                        btn.tag = 200
                                        self.isDepositeNumberSelectedFromContact = false
                                        self.onClickContactNumberClose(btn)
                                        //self.faceRecongnitionStatus(status: false)
                                        return
                                    }
                                    alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                        self.btnPay.setTitle("Pay".localized, for: .normal)
                                        
                                        if self.isDepositeNumberSelectedFromContact {
                                            DispatchQueue.main.async {
                                                self.insertDepositeConfirmCell(isKeyboardShow: false)
                                            }
                                        }
                                }
                                
                       
                                    if let dic = self.model.withdrawServerList?[0] {
                                        if let facePay = dic["FacePay"] as? NSNumber {
                                            if facePay.boolValue {
                                                self.model.facePay = true
                                                if let num = dic["MobileNo"] as? String {
                                                    self.model.FinalWithdrawalNumber = num
                                                    self.model.FinalDepositorNumber = dic["ReferanceNo"] as? String ?? ""
                                                    self.AgentVerify(withdrawalNum: num, depositNumber: dic["ReferanceNo"] as? String ?? "")
                                                }
                                            }else {
                                                self.model.facePay = false
                                                DispatchQueue.main.async {
                                                    self.nevigateToFaceRecognisation()
                                                }
                                            }
                                        }
                                    }
                                }
                                DispatchQueue.main.async {
                                    alertViewObj.showAlert(controller: self)
                                }
                            }
                        }else if dic["code"] as? Int == 300 {
                            DispatchQueue.main.async {

                            alertViewObj.wrapAlert(title: nil, body: "Location Not Matched".localized, img: UIImage(named: "location_nomatch")!)
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                let btn = UIButton()
                                btn.tag = 200
                                self.isDepositeNumberSelectedFromContact = false
                                self.onClickContactNumberClose(btn)
                            }
                                alertViewObj.showAlert(controller: self)
                            }
                        }else if dic["code"] as? Int == 301 {
                            DispatchQueue.main.async {

                            alertViewObj.wrapAlert(title: nil, body: "There is no record found on Your entered mobile number".localized, img: UIImage(named: "no_deposit")!)
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                let btn = UIButton()
                                btn.tag = 200
                                self.isDepositeNumberSelectedFromContact = false
                                self.onClickContactNumberClose(btn)
                            }
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                }else {
                    DispatchQueue.main.async {

                    alertViewObj.wrapAlert(title: nil, body: "Technical error in server side, Please try again later.".localized, img: UIImage(named: "alert-icon")!)
                    alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        let btn = UIButton()
                        btn.tag = 200
                        self.isDepositeNumberSelectedFromContact = false
                        self.onClickContactNumberClose(btn)
                    }
                        alertViewObj.showAlert(controller: self)
                    }
                }
            }catch {
                DispatchQueue.main.async {

                alertViewObj.wrapAlert(title: nil, body: "Technical error in server side, Please try again later.".localized, img: UIImage(named: "alert-icon")!)
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                    let btn = UIButton()
                    btn.tag = 200
                    self.isDepositeNumberSelectedFromContact = false
                    self.onClickContactNumberClose(btn)
                }
                    alertViewObj.showAlert(controller: self)
                }

            }
        }else if screen == "OTPCheck" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,Any> {
                        println_debug("OTPCheck : -----> \(dic)")
                        if dic["code"] as? Int == 200 {
                            self.OTPMatched()
                        }else if dic["code"] as? Int == 300  {
                            DispatchQueue.main.async {
                            self.resetOTPField()
                            self.model.OTP = ""
                                DispatchQueue.main.async {

                            alertViewObj.wrapAlert(title:"", body: "Invalid OTP".localized, img: UIImage(named: "otp_invalid")!)
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                //self.OTPResend()
                                DispatchQueue.main.async {
                                    if let cell = self.tfWithdraw.cellForRow(at: IndexPath(row: 4, section: 0)) as? WithdrawOTPCell {
                                        self.otpLableStatus = "Enter Withdrawal OTP".localized
                                        cell.lblStatus.text = self.otpLableStatus
                                        cell.tfFirst.text = ""
                                        cell.tfSecond.text = ""
                                        cell.tfThired.text = ""
                                        cell.tfFour.text = ""
                                        cell.tfFive.text = ""
                                        cell.tfSix.text = ""
                                        cell.lblFirst.backgroundColor = UIColor.lightGray
                                        cell.lblSecond.backgroundColor = UIColor.lightGray
                                        cell.lblThired.backgroundColor = UIColor.lightGray
                                        cell.lblFour.backgroundColor = UIColor.lightGray
                                        cell.lblFice.backgroundColor = UIColor.lightGray
                                        cell.lblSix.backgroundColor = UIColor.lightGray
                                        cell.tfFirst.becomeFirstResponder()
                                    }
                                }
                            })
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                }else {
                    
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "Error".localized, alertBody: "Technical error in server side, Please try again later.".localized, alertImage: UIImage(named: "alert-icon")!)
                    }
                    
                    }
                }
            }catch {}
        }else if screen == "OTPResend" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,Any> {
                         println_debug("OTPResend : -----> \(dic)")
                        if dic["code"] as? Int == 200 {
                            DispatchQueue.main.async {
                                self.tvBottom.constant = 0
                                self.btnPay.isHidden = false
                                self.insertNumberNameNRCAddressCell()
                            }
                        }else if dic["code"] as? Int == 300  {
                            self.resetOTPField()
                            self.model.OTP = ""
                            DispatchQueue.main.async {

                            alertViewObj.wrapAlert(title:"", body: "Invalid OTP".localized, img: UIImage(named: "otp_invalid")!)
                            alertViewObj.addAction(title: "CANCEL".localized, style: .target , action: {
                            })
                            alertViewObj.addAction(title: "RESEND".localized, style: .target , action: {
                                self.OTPResend()
                            })
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "Error".localized, alertBody: "Technical error in server side, Please try again later.".localized, alertImage: UIImage(named: "alert-icon")!)
                    }
                }
            }catch {}
        }else if screen == "UplaodImage" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug("UplaodImage : -----> \(dic)")
                        if dic["Code"] as? NSNumber == 200 {
                            let url = dic["Data"] as! String
                            self.model.monyWithdrawImage = url.replacingOccurrences(of: " ", with: "%20")
                            DispatchQueue.main.async {
                                self.saveWithdraw()
                            }
                        }else {
                            DispatchQueue.main.async {
                                alertViewObj.wrapAlert(title: nil, body: "Image uploading failed. Please try again".localized, img: UIImage(named: "alert-icon")!)
                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                    self.dismiss(animated: true, completion: nil)
                                }
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: nil, body: "Image uploading failed. Please try again".localized, img: UIImage(named: "alert-icon")!)
                        alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertViewObj.showAlert(controller: self)
                    }
                }
            } catch {}
        }else if screen == "SaveWithdraw" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,Any> {
                          println_debug("SaveWithdraw : -----> \(dic)")
                        if dic["code"] as? Int == 200 {
                            if let data = dic["data"] as? Dictionary<String,Any> {
                                if let withdrawID =  data["WithdrawalId"] as? String {
                                    self.model.saveWithdraw = ["WithdrawalName": data["WithdrawerName"] ?? "","WithdrawalNumber": data["WithdrawerMobileNo"] ?? "","DepositorName": data["DepositorName"] ?? "","DepositorNumber": data["DepositorMobileNo"] ?? "","TransictionID": withdrawID, "Amount": data["Amount"] ?? ""] as? Dictionary<String, String>
                                    self.payFinally(withdrawID: withdrawID)
                                }
                            }
                        }else if dic["code"] as? Int == 300  {
                            if let str = dic["message"] as? String {
                                DispatchQueue.main.async {
                                    self.showAlert(alertTitle: "Error".localized, alertBody: str, alertImage: UIImage(named: "no_deposit")!)
                                }
                                
                            }else {
                                DispatchQueue.main.async {
                                    self.showAlert(alertTitle: "", alertBody: "Withdrawing process failed. Please try again".localized, alertImage: UIImage(named: "no_deposit")!)}
                            }
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "Error".localized, alertBody: "Technical error in server side, Please try again later.".localized, alertImage: UIImage(named: "alert-icon")!)}
                }
            }catch {}
        }else if screen == "payFinally" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,Any> {
                        println_debug("payFinally : -----> \(dic)")
                        if dic["code"] as? Int == 200 {
                            if let data = dic["data"] as? Dictionary<String,Any>{
                                println_debug(data)
                                if let id = data["WithdrawLocalTransId"] as? String {
                                    self.model.withdrawTransictionID = id
                                    if let amt = data["Amount"] as? NSNumber {
                                        self.model.finalAmount = amt.stringValue
                                    }
                                    self.FinalPaymentSuccess()
                                }
                            }
                        }else if dic["code"] as? Int == 300  {
                            if let str = dic["message"] as? String {
                                DispatchQueue.main.async {
                                    self.showAlert(alertTitle: "Error".localized, alertBody: str, alertImage: UIImage(named: "no_deposit")!)}
                            }else {
                                DispatchQueue.main.async {
                                    self.showAlert(alertTitle: "", alertBody: "Withdrawing process failed. Please try again".localized, alertImage: UIImage(named: "no_deposit")!)}
                            }
                            
                            self.failureCount = self.failureCount + 1
                            if self.failureCount == 1 {
                                self.failureViewModel.sendDataToFailureApi(request: self.Parms as Any, response: dic , type: FailureHelper.FailureType.FACEPAY.rawValue, finished: {
                                })
                            }
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "", alertBody: "Technical error in server side, Please try again later.".localized, alertImage: UIImage(named: "alert-icon")!)}
                }
            }catch {}
        }else if screen == "NumberForSendSMS" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug("NumberForSendSMS : ------>\(dic)")
                        if dic["code"] as? NSNumber == 200 {
                            if let data = dic["data"] as? Dictionary<String,AnyObject> {
                                self.model.SMSVerificationNo = data["SMSVerificationNo"] as? String ?? ""
                            }
                        }else {
                            DispatchQueue.main.async {
                                self.showAlert(alertTitle: "", alertBody: "Request Failed. Please Try Again".localized, alertImage: UIImage(named: "alert-icon")!)}
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "", alertBody: "Request Failed. Please Try Again".localized, alertImage: UIImage(named: "alert-icon")!)}
                }
            }catch {}
        }else if screen == "AgentVerify" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug("AgentVerify:-----> \(dic)")
                        model.AgentVerifyStatus = 0
                        if dic["code"] as? NSNumber == 201 {
                            model.AgentVerifyStatus = 201
                            DispatchQueue.main.async {
                                self.isOTPHidden = false
                                self.insertOTPCell()
                            }
                        }else if dic["code"] as? NSNumber == 205 {
                            model.AgentVerifyStatus = 205
                            DispatchQueue.main.async {
                                self.isOTPHidden = false
                                self.insertOTPCell()
                            }
                        }else if dic["code"] as? NSNumber == 206 {
                            model.AgentVerifyStatus = 206
                            if let str = dic["message"] as? String {
                                DispatchQueue.main.async {

                                alertViewObj.wrapAlert(title:"", body: str, img: UIImage(named: "alert-icon")!)
                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                    self.faceRecongnitionStatus(status: false)
                                })
                                    alertViewObj.showAlert(controller: self)
                                }
                            }
                        }else if dic["code"] as? NSNumber == 300 {
                            model.AgentVerifyStatus = 300
                            if let str = dic["message"] as? String {
                                DispatchQueue.main.async {
                                alertViewObj.wrapAlert(title:"", body: str, img: UIImage(named: "alert-icon")!)
                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                     self.faceRecongnitionStatus(status: false)
                                })
                                    alertViewObj.showAlert(controller: self)
                                }
                            }
                        }else {
                            if let str = dic["message"] as? String {
                                DispatchQueue.main.async {
                                alertViewObj.wrapAlert(title:"", body: str, img: UIImage(named: "alert-icon")!)
                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                    self.faceRecongnitionStatus(status: false)
                                })
                                
                                    alertViewObj.showAlert(controller: self)
                                }
                            }
                          //  showAlert(alertTitle: "", alertBody: "Request Failed. Please Try Again".localized, alertImage: UIImage(named: "alert-icon")!)
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "", alertBody: "Request Failed. Please Try Again".localized, alertImage: UIImage(named: "alert-icon")!)}
                }
            }catch {}
        }else if screen == "SMSVerification" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug("SMSVerification : ------>\(dic)")
                        if dic["code"] as? NSNumber == 200 {
                            self.OTPCheck(otp: model.OTP)
                        }else if dic["code"] as? NSNumber == 201{
                            DispatchQueue.main.async {

                            alertViewObj.wrapAlert(title:"", body: "SMS not recieved".localized, img: UIImage(named: "otp_invalid")!)
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                 self.resetOTPField()
                                 self.model.OTP = ""
                            })
                            
                                alertViewObj.showAlert(controller: self)
                            }
                        }else if dic["code"] as? NSNumber == 203 || dic["code"] as? NSNumber == 206 {
                            DispatchQueue.main.async {

                            alertViewObj.wrapAlert(title:"", body: "Session Expired".localized, img: UIImage(named: "alert-icon")!)
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                self.resetOTPField()
                                self.model.OTP = ""
                                self.faceRecongnitionStatus(status: false)
                            })
                            
                                alertViewObj.showAlert(controller: self)
                            }
                        }else if dic["code"] as? NSNumber == 204 {
                            DispatchQueue.main.async {

                            alertViewObj.wrapAlert(title:"", body: "OK$ didn't receive SMS message from withdrawal mobile number.".localized, img: UIImage(named: "sms-alert")!)
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                self.resetOTPField()
                                self.model.OTP = ""
                            })
                            
                                alertViewObj.showAlert(controller: self)
                            }
                        }else if dic["code"] as? NSNumber == 300{
                            DispatchQueue.main.async {

                            alertViewObj.wrapAlert(title:"", body: "Request Failed. Please Try Again".localized, img: UIImage(named: "alert-icon")!)
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                 self.resetOTPField()
                                self.model.OTP = ""
                            })
                            
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "", alertBody: "Request Failed. Please Try Again".localized, alertImage: UIImage(named: "alert-icon")!)
                        
                    }
                }
            }catch {}
        }
    }
    
    func formateValidNumber(num: String,coutryCode: String) -> String {
        var finalNumber = ""
        if coutryCode == "+95" {
            finalNumber = "00" +  coutryCode.dropFirst() + num.dropFirst()
        }else {
            finalNumber = "00" + coutryCode.dropFirst() + num
        }
        return finalNumber
    }
    
    func AgentVerify(withdrawalNum: String, depositNumber: String) {
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let ur = getUrl(urlStr: Url.URL_FacePayAgentVerify, serverType: .faceIDPay)
            let web      = WebApiClass()
            web.delegate = self
//            guard let ur = URL(string: "http://52.76.209.187:3002/OKDollar/AgentVerify") else {
//                return
//            }
            let dic =  ["AgentNumber": UserModel.shared.mobileNo,"WithdrawalNumber":withdrawalNum,"FCM": "f1PnUsEqCvc:APA91bHVuVwJQxVHmxpOLN4g8Fjxe","DepositerNumber" : depositNumber]
            println_debug(ur)
            println_debug(dic)
            web.genericClass(url: ur, param: dic as AnyObject, httpMethod: "POST", mScreen: "AgentVerify")
        }else {
            DispatchQueue.main.async {

            alertViewObj.wrapAlert(title: nil, body: "Please check your internet connection".localized, img: UIImage(named: "alert-icon")!)
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                self.faceRecongnitionStatus(status: false)
            }
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func SMSVerificationAPI() {
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let web      = WebApiClass()
            web.delegate = self
            let ur = getUrl(urlStr: Url.URL_FacePaySMSVerification, serverType: .faceIDPay)
//            guard let ur = URL(string: "http://52.76.209.187:3002/OKDollar/SMSVerification") else {
//                return
//            }
            let dic =  ["AgentNumber": UserModel.shared.mobileNo,"WithdrawalNumber": self.model.FinalWithdrawalNumber, "DepositerNumber": self.model.FinalDepositorNumber]
            println_debug(ur)
            println_debug(dic)
            web.genericClass(url: ur, param: dic as AnyObject, httpMethod: "POST", mScreen: "SMSVerification")
        }else {
            DispatchQueue.main.async {

            alertViewObj.wrapAlert(title: nil, body: "Please check your internet connection".localized, img: UIImage(named: "alert-icon")!)
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                self.faceRecongnitionStatus(status: false)
            }
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func OTPMatched() {
        DispatchQueue.main.async {
            if !self.view.contains(self.alertView) {
                self.alertView.frame = self.view.frame
                self.alertView.delegate = self
                self.alertView.setMessage(txt: "OTP Verification Successful".localized)
                self.strForAlert = "OTP"
                self.view.bringSubviewToFront(self.alertView)
                self.view.addSubview(self.alertView)
            }
        }
    }
    
//    func ImageUploadedSuccess() {
//        DispatchQueue.main.async {
//            if !self.view.contains(self.alertView) {
//                self.alertView.frame = self.view.frame
//                self.alertView.delegate = self
//                self.alertView.setMessage(txt: "Image uploaded successfully".localized)
//                self.strForAlert = "IMAGE"
//                self.view.bringSubviewToFront(self.alertView)
//                self.view.addSubview(self.alertView)
//            }
//        }
//    }
    
    func FinalPaymentSuccess() {
        DispatchQueue.main.async {
            if !self.view.contains(self.alertView) {
                self.alertView.frame = self.view.frame
                self.alertView.delegate = self
                self.alertView.setMessage(txt: "Payment Successful".localized)
                self.strForAlert = "PAYMENT"
                self.view.bringSubviewToFront(self.alertView)
                self.view.addSubview(self.alertView)
            }
        }
    }
    
    func hideSuccessAlert(successAlert: UIView) {
        println_debug("Delegate Called in Parent controller")
        
        if self.view.contains(self.alertView) {
            successAlert.removeFromSuperview()
        }
        
        if self.strForAlert == "OTP" {
            DispatchQueue.main.async {
                self.tvBottom.constant = 0
                self.btnPay.isHidden = false
                self.insertNumberNameNRCAddressCell()
            }
        }else if self.strForAlert == "PAYMENT" {
            DispatchQueue.main.async {
                let receipt = self.storyboard?.instantiateViewController(withIdentifier: "WithdrawReceiptVC") as! WithdrawReceiptVC
                receipt.amount = self.model.finalAmount
                receipt.receiptDic = self.model.saveWithdraw
                receipt.withdraw_ID = self.model.withdrawTransictionID
                receipt.depositCountrycode = self.model.depositeCountryCode
                receipt.withdrawCountrycode = self.model.withdrawCountryCode
                self.navigationController?.pushViewController(receipt, animated: true)
            }
        }
    }
    
    
}

extension WithdrawVC : ContactPickerDelegate {
    func contact(_: ContactPickersPicker, didContactFetchFailed error: NSError){}
    func contact(_: ContactPickersPicker, didCancel error: NSError){}
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker){ decodeContact(contact: contact)}
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]){}
    func decodeContact(contact: ContactPicker) {
        var countryCode = ""
        var countryFlag = ""
        var tempContryCode = ""
        var tempPhoneNumber = ""
        let phoneNumberCount = contact.phoneNumbers.count
        var phoneNumber = ""
        
        if phoneNumberCount == 1  {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        }
        else if phoneNumberCount > 1 {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        }else {}
        
        if phoneNumber != "" {
            phoneNumber = phoneNumber.replacingOccurrences(of: ")", with: "")
            phoneNumber = phoneNumber.replacingOccurrences(of: "(", with: "")
            phoneNumber = phoneNumber.trimmingCharacters(in: .whitespaces)
        }
        
        let cDetails = identifyCountry(withPhoneNumber: phoneNumber)
        if cDetails.0 == "" || cDetails.1 == "" {
            countryFlag = "myanmar"
            countryCode = "+95"
            tempContryCode = countryCode
        } else {
            countryFlag = cDetails.1
            countryCode = cDetails.0
            tempContryCode = countryCode
        }
        println_debug(tempContryCode)
        let phone = phoneNumber.replacingOccurrences(of: cDetails.0, with: "0")
        if phone.hasPrefix("0") {
            if countryFlag == "myanmar" {
                tempPhoneNumber = phone
            } else {
                tempPhoneNumber = phoneNumber.replacingOccurrences(of: cDetails.0, with: "")
            }
        } else {
            if countryFlag == "myanmar" {
                tempPhoneNumber =   "0" + phone
            } else {
                tempPhoneNumber =  phone
            }
        }
        
        countryCode.remove(at: countryCode.startIndex)
        if tempPhoneNumber.hasPrefix("0"){
            tempPhoneNumber.remove(at: tempPhoneNumber.startIndex)
        }
        let finalMob =  "00" + countryCode + tempPhoneNumber
        let noToCheck = "0" + tempPhoneNumber
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
        if finalMob.count > 8 {
            if countryCode == "95" {
                let mbLength = (validObj.getNumberRangeValidation(noToCheck))
                if !mbLength.isRejected {
                    if noToCheck.count < mbLength.min || noToCheck.count > mbLength.max {
                        DispatchQueue.main.async {
                            self.showAlert(alertTitle: "", alertBody: "You selected invalid number, Please try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
                            
                        }
                    }else {
                      
                        let countryData = identifyCountryByCode(withPhoneNumber: "+" + countryCode)
                        if contactListShownFor == "withdraw" {
                           self.isWithdrawNumberSelectedFromContact = true
                            let btn = UIButton()
                            btn.tag = 11
                            self.onClickContactNumberClose(btn)
                            
                            previousCCWithdraw = model.withdrawCountryCode
                            previousCFWithdraw = model.withdrawflag
                            
                            model.withDrawNumber = noToCheck
                            model.withdrawConfirmNumber = noToCheck
                            model.withdrawCountryCode = countryData.countryCode
                            model.withdrawflag = countryData.countryFlag
                            
                            if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 0, section: 0)) as? WithdrawContactCell {
                                cell.imgCounty.image = UIImage(named: model.withdrawflag)
                                cell.tfNumber.text = noToCheck
                                cell.lblCountrycode.text = "(" + model.withdrawCountryCode + ")"
                                cell.btnClose.isHidden = false
                            }
                            checkWithdrawValidNumber(number: noToCheck)
                        }else{
                            if noToCheck == model.withDrawNumber {
                                DispatchQueue.main.async {

                                alertViewObj.wrapAlert(title: nil, body: "Depositor number and withdrawal number should not be same".localized, img: UIImage(named: "alert-icon")!)
                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                    let btn = UIButton()
                                    btn.tag = 200
                                    self.onClickContactNumberClose(btn)
                                }
                                    alertViewObj.showAlert(controller: self)
                                }
                            }else {
                            self.isDepositeNumberSelectedFromContact = true
                            let btn = UIButton()
                            btn.tag = 13
                            self.onClickContactNumberClose(btn)
                      
                            previousCCDeposite =  model.depositeCountryCode
                            previousCFDeposite =  model.depositeflag
                            
                            model.depositNumber = noToCheck
                            model.depositConfirmNumber = noToCheck
                            model.depositeCountryCode = countryData.countryCode
                            model.depositeflag = countryData.countryFlag
                            if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 2, section: 0)) as? WithdrawContactCell {
                                cell.imgCounty.image = UIImage(named: model.depositeflag)
                                cell.lblCountrycode.text = "(" + model.depositeCountryCode + ")"
                                cell.tfNumber.text = noToCheck
                                cell.btnClose.isHidden = false
                            }
                            self.isDepositorMoreThanOne = true
                            checkDepositeValidNumber(number:  noToCheck)
                            }
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "", alertBody: "You selected invalid number, Please try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))}
                }
            }else{
                isDepositeNumberSelectedFromContact = true
                let countryData = identifyCountryByCode(withPhoneNumber: "+" + countryCode)
                if contactListShownFor == "withdraw" {
                    model.withDrawNumber = tempPhoneNumber
                    model.withdrawConfirmNumber = tempPhoneNumber
                    model.withdrawCountryCode = countryData.countryCode
                    model.withdrawflag = countryData.countryFlag
                    let btn = UIButton()
                    btn.tag = 100
                    self.onClickContactNumberClose(btn)
                    checkWithdrawValidNumber(number: tempPhoneNumber)
                }else{
                    model.depositNumber = tempPhoneNumber
                    model.depositConfirmNumber = tempPhoneNumber
                    model.depositeCountryCode = countryData.countryCode
                    model.depositeflag = countryData.countryFlag
                    let btn = UIButton()
                    btn.tag = 200
                    self.onClickContactNumberClose(btn)
                    checkDepositeValidNumber(number:  tempPhoneNumber)
                }
            }
        }else{
            DispatchQueue.main.async {
                self.showAlert(alertTitle: "", alertBody: "You selected invalid number, Please try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
            }
        }
    }
    
}

