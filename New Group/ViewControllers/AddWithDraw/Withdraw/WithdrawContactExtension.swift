//
//  WithdrawContactExtension.swift
//  OK
//
//  Created by shubh's MacBookPro on 9/20/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

extension WithdrawVC {
    
    func showContactList(yAsix: CGFloat, position: String, contactFor: String) {
        if !(self.view.contains(self.contactSuggestionView)) {
            DispatchQueue.main.async {
                self.contactSuggestionView.contactTableView.register(UINib.init(nibName: "ContactSuggestionCell", bundle: Bundle.main), forCellReuseIdentifier: "ContactSuggestionCell")
                self.contactSuggestionView.frame = CGRect(x: 5, y: yAsix, width: (self.view.frame.width - 10) , height: 150)
                self.contactSuggestionView.reduceFrom = position
                self.contactSuggestionView.yAxis = yAsix
                self.contactSuggestionView.contactFor = contactFor
                self.contactSuggestionView.delegate = self
                self.view.bringSubviewToFront(self.contactSuggestionView)
                self.view.addSubview(self.contactSuggestionView)
            }
        }
    }
    
    func removeContactViewFromSurperView() {
        removeContactList()
    }
    
    func removeContactList() {
        if self.view.contains(self.contactSuggestionView) {
            self.contactSuggestionView.removeFromSuperview()
        }
    }
    
    func selectedNumber(flag: String, countryCode: String, number: String, isValid: Bool, contactFor: String) {
        removeContactList()
        if contactFor == "Deposit" {
            if isValid {
                self.isDepositeNumberSelectedFromContact = true
                
                model.depositNumber = number
                model.depositConfirmNumber = number
                model.depositeCountryCode = countryCode
                model.depositeflag = flag
                if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 2, section: 0)) as? WithdrawContactCell {
                    cell.imgCounty.image = UIImage(named: model.depositeflag)
                    cell.lblCountrycode.text = "(" + model.depositeCountryCode + ")"
                    cell.tfNumber.text = number
                    cell.btnClose.isHidden = false
                }
                self.isDepositorMoreThanOne = true
                checkDepositeValidNumber(number:  number)
            }else {
                let btn = UIButton()
                btn.tag = 13
                self.onClickContactNumberClose(btn)
            }
        }else if contactFor == "Withdraw" {
            if isValid {
                self.isWithdrawNumberSelectedFromContact = true
                model.withDrawNumber = number
                model.withdrawConfirmNumber = number
                model.withdrawCountryCode = countryCode
                model.withdrawflag = flag
                
                if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 0, section: 0)) as? WithdrawContactCell {
                    cell.imgCounty.image = UIImage(named: model.withdrawflag)
                    cell.tfNumber.text = number
                    cell.lblCountrycode.text = "(" + model.withdrawCountryCode + ")"
                    cell.btnClose.isHidden = false
                }
                checkWithdrawValidNumber(number: number)
            }else {
                let btn = UIButton()
                btn.tag = 11
                self.onClickContactNumberClose(btn)
            }
        }
    }
    
}
