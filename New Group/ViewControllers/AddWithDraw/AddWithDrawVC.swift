//
//  AddWithDrawVC.swift
//  OK
//
//  Created by Imac on 6/26/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import CoreLocation

class AddWithDrawVC: OKBaseController {
    @IBOutlet weak var btnDeposite: UIButton!{
        didSet{
            self.btnDeposite.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnWithdraw: UIButton!{
        didSet{
            self.btnWithdraw.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblDeposit: UILabel!{
        didSet{
            self.lblDeposit.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblVideo: UILabel!{
        didSet{
            lblVideo.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgPopOver: UIImageView!
    var navigateTag = false
    let gifManager = SwiftyGifManager(memoryLimit:6)
    var depositorView = Dictionary<String,Any>()
    var withdrawView = Dictionary<String,Any>()
    var isAllowGoInsideDepositor = false
    var isAllowGoInsideWithdraw = false
    var isKYCStatus = false
    override func viewDidLoad() {
        super.viewDidLoad()
        loadFaceGIF()
        
        self.lblVideo.text = "  " + "Click here to watch demo videos".localized + "  "
        self.lblVideo.layer.masksToBounds = true
        self.lblVideo.layer.cornerRadius = 5.0
        var timecount = 0
        let _ = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
            if timecount  == 3 {
                self.lblVideo.isHidden = true
                self.imgPopOver.isHidden = true
                timecount = 0
                timer.invalidate()
            }
            timecount += 1
        }
        
        
        lblDeposit.text = "Deposit to transfer money".localized
        lblDeposit.layer.masksToBounds = true
        lblDeposit.layer.cornerRadius = btnDeposite.frame.height / 2
        lblDeposit.layer.borderWidth = 5.0
        lblDeposit.layer.borderColor = UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0).cgColor
        // btnDeposite.setTitle("Deposit".localized, for: .normal)
        btnWithdraw.setTitle("Withdraw".localized, for: .normal)
        btnDeposite.layer.masksToBounds = true
        btnDeposite.layer.borderWidth = 5.0
        
        //lblDeposit.layer.borderWidth = 5.0
        btnDeposite.layer.cornerRadius = btnDeposite.frame.height / 2
        btnDeposite.layer.borderColor =  UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0).cgColor
        btnWithdraw.layer.masksToBounds = true
        btnWithdraw.layer.borderWidth = 5.0
        btnWithdraw.layer.cornerRadius = btnDeposite.frame.height / 2
        btnWithdraw.layer.borderColor = UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0).cgColor // ECECEE
        self.setMarqueLabelInNavigation()
        
        self.CheckNumberRegisteredAndMinMaxAmount()
        //moneytransfer
        
        
//        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        
        
    }
    
    @objc func appMovedToBackground() {
        performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
    }
    
    
    override func viewWillAppear(_ animated: Bool){
        self.view.endEditing(true)
        navigateTag = false
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    func loadFaceGIF() {
        self.imgIcon.gifImage = nil
        let gif = UIImage(gifName: "money-transfer")
        self.imgIcon.setGifImage(gif, manager: gifManager)
    }
    @IBAction func clickOnDemoAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FacePayDemoVC") as! FacePayDemoVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 18)
        lblMarque.text = "Face ID Pay".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
    }
    
    @IBAction func onClickBackAction(_ sender: Any) {
//        self.navigationController?.dismiss(animated: false, completion: nil)
        performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
    }
    
    @IBAction func onClickWithdrawAction(_ sender: UIButton) {
        if appDelegate.checkNetworkAvail() {
            self.checkLocationONOFF(handler: { (_ isLocaitonOn: Bool) in
                if isLocaitonOn {
                    DispatchQueue.main.async {
                        if !(self.navigateTag) {
                            if self.isAllowGoInsideWithdraw {
                                if self.isKYCStatus {
                                    self.navigateTag = true
                                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "WithdrawVC")
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }else {
                                    
                                    DispatchQueue.main.async {
                                        
                                        self.showAlert(alertTitle: "", alertBody: "According to the Central Bank of Myanmar Regulation on Mobile Financial Services, those who open account in OK $ have been respectfully requested to provide correct personal information data and Citizenship Scrutiny Card(NRC) Number (or) Name of Citizenship Country. If you are citizen of other country please provide passport details.".localized, alertImage: UIImage(named: "alert-icon")!)
                                    }
                                    
                                   
                                }
                            }else {
                                
                                DispatchQueue.main.async {
                                    self.showAlert(alertTitle: "", alertBody: "Technical error in server side, Please try again later.".localized, alertImage: UIImage(named: "alert-icon")!)
                                }
                                
                                
                            }
                        }
                    }
                }else {
                    self.OpenSettings(message: "Please Enable Location Service".localized)
                }
            })
        }else {
            self.OpenSettings(message: "Please check your internet connection".localized)
        }
    }
    
    @IBAction func onClickDepositeAction(_ sender: UIButton) {
        
        if appDelegate.checkNetworkAvail() {
            self.checkLocationONOFF(handler: { (_ isLocaitonOn: Bool) in
                if isLocaitonOn {
                    DispatchQueue.main.async {
                        if !(self.navigateTag) {
                            
                            if self.isAllowGoInsideDepositor {
                                if self.isKYCStatus {
                                    self.navigateTag = true
                                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "DepositeVC")
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }else {
                                    
                                    
                                    
                                    self.showAlert(alertTitle: "", alertBody: "According to the Central Bank of Myanmar Regulation on Mobile Financial Services, those who open account in OK $ have been respectfully requested to provide correct personal information data and Citizenship Scrutiny Card(NRC) Number (or) Name of Citizenship Country. If you are citizen of other country please provide passport details.".localized, alertImage: UIImage(named: "alert-icon")!)
                                }
                            }else {
                                self.showAlert(alertTitle: "", alertBody: "Technical error in server side, Please try again later.".localized, alertImage: UIImage(named: "alert-icon")!)
                            }
                        }
                    }
                }else {
                    self.OpenSettings(message: "Please Enable Location Service".localized)
                }
            })
        }else {

            self.showAlert(alertTitle: "", alertBody: "Please check your internet connection".localized, alertImage: UIImage(named: "alert-icon")!)
        }
    }
    
    func CheckNumberRegisteredAndMinMaxAmount() {
        if appDelegate.checkNetworkAvail() {
            let ur = getUrl(urlStr: Url.URL_GetCategoryApi, serverType: .faceIDPay)
            let dic = ["OsType":1,"DestinationNo": UserModel.shared.mobileNo,"SourceNo": UserModel.shared.mobileNo,"TransType":"PayTo","AppBuildNumber": buildNumber] as [String : Any]
            println_debug(ur)
            println_debug(dic)
            TopupWeb.genericClass(url: ur, param: dic as AnyObject, httpMethod: "POST") { [weak self](response, success) in
                if success {
                    if let json = response as? Dictionary<String,Any> {
                        if let code = json["code"] as? NSNumber, code == 200 {
                            if let dic = json["data"] as? Dictionary<String,Any> {
                                println_debug("URL_GetCategoryApi: ------>\(json)")
                                if let dipoView = dic["DepositerView"] as? Dictionary<String,Any> {
                                    self?.depositorView = dipoView
                                    if let diposit = self?.depositorView {
                                        println_debug(diposit)
                                        if UserModel.shared.agentType == .user {
                                            if let subscriber = diposit["Subscriber"] as? Bool {
                                                if subscriber == false {
                                                    self?.isAllowGoInsideDepositor = true
                                                }
                                            }
                                        }else if UserModel.shared.agentType == .merchant {
                                            if let merchant = diposit["Merchant"] as? Bool {
                                                if merchant == false {
                                                    self?.isAllowGoInsideDepositor = true
                                                }
                                            }
                                        }else if UserModel.shared.agentType == .advancemerchant {
                                            if let advanceMerchant = diposit["AdvanceMerchant"] as? Bool {
                                                if advanceMerchant == false {
                                                    self?.isAllowGoInsideDepositor = true
                                                }
                                            }
                                        }else if UserModel.shared.agentType == .agent {
                                            if let agent = diposit["Agent"] as? Bool {
                                                if agent == false {
                                                    self?.isAllowGoInsideDepositor = true
                                                }
                                            }
                                        }
                                    }else {
                                        self?.isAllowGoInsideDepositor = false
                                    }
                                }
                                
                                if let withView = dic["WithdrawalView"] as? Dictionary<String,Any> {
                                    self?.withdrawView = withView
                                    if let withdraw = self?.withdrawView {
                                        println_debug(withdraw)
                                        if UserModel.shared.agentType == .user {
                                            if let subscriber = withdraw["Subscriber"] as? Bool {
                                                if subscriber == false {
                                                    self?.isAllowGoInsideWithdraw = true
                                                }
                                            }
                                        }else if UserModel.shared.agentType == .merchant {
                                            if let merchant = withdraw["Merchant"] as? Bool {
                                                if merchant == false {
                                                    self?.isAllowGoInsideWithdraw = true
                                                }
                                            }
                                        }else if UserModel.shared.agentType == .advancemerchant {
                                            if let advanceMerchant = withdraw["AdvanceMerchant"] as? Bool {
                                                if advanceMerchant == false {
                                                    self?.isAllowGoInsideWithdraw = true
                                                }
                                            }
                                        }else if UserModel.shared.agentType == .agent {
                                            if let agent = withdraw["Agent"] as? Bool {
                                                if agent == false {
                                                    self?.isAllowGoInsideWithdraw = true
                                                }
                                            }
                                        }
                                        if let kycStatus = withdraw["KycStatus"] as? Bool {
                                            if kycStatus == false {
                                                self?.isKYCStatus = true
                                            }
                                        }
                                    }else {
                                        self?.isAllowGoInsideWithdraw = false
                                    }
                                    
                                }
                            }
                        }else {
                        }
                    }else {
                        DispatchQueue.main.async {
                            self?.showAlert(alertTitle: "", alertBody: "Technical error in server side, Please try again later.".localized, alertImage: UIImage(named: "alert-icon")!)
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        self?.showAlert(alertTitle: "", alertBody: "Technical error in server side, Please try again later.".localized, alertImage: UIImage(named: "alert-icon")!)
                    }
                }
            }
        }else {
            self.OpenSettings(message: "Please check your internet connection".localized)
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
            alertViewObj.showAlert(controller: self)
        
    }
    
    func checkLocationONOFF(handler : @escaping(_ bool : Bool)-> Void) {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined:
                println_debug("notDetermined")
                handler(false)
                break
            case .denied :
                println_debug("denied")
                handler(false)
                break
            case .restricted :
                println_debug("restricted")
                handler(false)
                break
            case .authorizedAlways, .authorizedWhenInUse:
                handler(true)
                break
            }
        } else {
            self.OpenSettings(message: "Please Enable Location Service".localized)
        }
    }
    
    func OpenSettings(message: String) {
        alertViewObj.wrapAlert(title:"", body: message, img: UIImage(named: "alert-icon"))
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary111([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            }
            self.navigationController?.popViewController(animated: true)
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
}

fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary111(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
