//
//  SAConatactListView.swift
//  OK
//
//  Created by shubh's MacBookPro on 9/4/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import Contacts

protocol SAConatactListViewDelegate {
    func selectedNumber(flag: String, countryCode: String, number: String, isValid: Bool, contactFor: String)
    func removeContactViewFromSurperView()
}

class SAConatactListView: UIView,UITableViewDelegate,UITableViewDataSource {
    var delegate: SAConatactListViewDelegate?
    @IBOutlet weak var contactSuggestion: CardDesignView!
    @IBOutlet weak var contactTableView: UITableView!
    @IBOutlet weak var heightContactSuggestion: NSLayoutConstraint!
    var reduceFrom: String?
    var yAxis: CGFloat?
    var contactFor: String?
    
    var contactSearchedDict = [Dictionary<String,Any>]()

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
    
    }
    
    func hideContactSuggestion() {
        contactSuggestion.isHidden = true
        contactTableView.isHidden  = true
        heightContactSuggestion.constant = 0.0
        self.frame = CGRect.init(x: 5, y: 255, width: (self.viewWidth) , height: 0)
        if let del = delegate  {
            del.removeContactViewFromSurperView()
        }
    }
    
    func showContactSuggestion() {
        contactSuggestion.isHidden = false
        contactTableView.isHidden  = false
        let yy = yAxis ?? 0.0
        if contactSearchedDict.count == 1 {
            if reduceFrom == "TOP" {
                heightContactSuggestion.constant = 100.00
                self.frame = CGRect.init(x: 5, y: yy - 50, width: (self.viewWidth) , height: 50.0)
            }else {
                heightContactSuggestion.constant = 50.00
                self.frame = CGRect.init(x: 5, y: yy, width: (self.viewWidth) , height: 50.0)
            }
        } else if contactSearchedDict.count == 2 {
             if reduceFrom == "TOP" {
                heightContactSuggestion.constant = 150.00
                self.frame = CGRect.init(x: 5, y: yy - 100, width: (self.viewWidth) , height: 100.0)
            }else {
                heightContactSuggestion.constant = 100.00
                self.frame = CGRect.init(x: 5, y: yy , width: (self.viewWidth) , height: 100.0)
            }
        } else {
            if reduceFrom == "TOP" {
                heightContactSuggestion.constant = 0.00
                self.frame = CGRect.init(x: 5, y: yy - 150, width: (self.viewWidth) , height: 150.0)
            }else {
                heightContactSuggestion.constant = 150.00
                self.frame = CGRect.init(x: 5, y: yy, width: (self.viewWidth) , height: 150.0)
            }
           
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactSearchedDict.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.00
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ContactSuggestionCell = (tableView.dequeueReusableCell(withIdentifier: "ContactSuggestionCell", for: indexPath) as? ContactSuggestionCell)!
        let dict = contactSearchedDict[indexPath.row]
        
        let name  = dict.safeValueForKey(ContactSuggessionKeys.contactname) as? String ?? "Unknown"
        let phone = dict.safeValueForKey(ContactSuggessionKeys.phonenumber_onscreen) as? String ?? "Unknown"
        
        cell.lblName.text = name
        if let operatorName = dict.safeValueForKey(ContactSuggessionKeys.operatorname) as? String {
            if phone.hasPrefix("+95") || phone.first == "0" {
                cell.lblNumber.text = phone + " " + "(\(operatorName))"
            } else {
                cell.lblNumber.text = phone
            }
        } else {
            cell.lblNumber.text = phone
        }
        
        var num = ""
        if phone.first == "0" {
            num = "0095" + String(phone.dropFirst())
        }else if phone.hasPrefix("+95") {
            var no = String(phone.dropFirst())
            no = String(no.dropFirst())
            no = String(no.dropFirst())
            if no.first == "0" {
              num  =  "0095" + String(no.dropFirst())
            }
            num =  "0095" + no
        }else {
            num =  phone
        }
     
        let isContains = okDollarALLContacts.contains(num)
        if isContains {
            cell.imgOKDollar.isHidden = false
        }else {
            cell.imgOKDollar.isHidden = true
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = contactSearchedDict[indexPath.row]
        let mobileNumber = dict[ContactSuggessionKeys.phonenumber_backend] as? String ?? ""
        if let number = mobileNumber.addingPercentEncoding(withAllowedCharacters: .decimalDigits) {
            let phones  = number.replacingOccurrences(of: "%20", with: "")
            let phones2 = phones.replacingOccurrences(of: "%C2%A0", with: "")
            if let final = phones2.removingPercentEncoding {
                self.hideContactSuggestion()
                let mobileObject = PTManagerClass.decodeMobileNumber(phoneNumber: final)
                println_debug(mobileObject.country.code)
                println_debug(mobileObject.country.dialCode)
                println_debug(mobileObject.number)
                if mobileObject.country.dialCode == "+95" {
                    let numberValid = PayToValidations().getNumberRangeValidation(mobileObject.number)
                    
                    if numberValid.isRejected {
                        PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                        if let del = delegate {
                            del.selectedNumber(flag: mobileObject.country.code, countryCode: mobileObject.country.dialCode, number: mobileObject.number, isValid: false, contactFor: contactFor ?? "")
                        }
                    }else if mobileObject.number.count < numberValid.min {
                        PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                        if let del = delegate {
                            del.selectedNumber(flag: mobileObject.country.code, countryCode: mobileObject.country.dialCode, number: mobileObject.number, isValid: false, contactFor: contactFor ?? "")
                        }
                    }else if mobileObject.number.count > numberValid.max {
                        PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                        if let del = delegate {
                            del.selectedNumber(flag: mobileObject.country.code, countryCode: mobileObject.country.dialCode, number: mobileObject.number, isValid: false, contactFor: contactFor ?? "")
                        }
                    }else {
                        if let del = delegate {
                            del.selectedNumber(flag: mobileObject.country.code, countryCode: mobileObject.country.dialCode, number: mobileObject.number, isValid: true, contactFor: contactFor ?? "")
                        }
                    }
                }else {
                    if mobileObject.number.count > 13 &&  mobileObject.number.count < 4 {
                        PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                        if let del = delegate {
                            del.selectedNumber(flag: mobileObject.country.code, countryCode: mobileObject.country.dialCode, number: mobileObject.number, isValid: false, contactFor: contactFor ?? "")
                        }
                        return
                    }else {
                        if let del = delegate {
                            del.selectedNumber(flag: mobileObject.country.code, countryCode: mobileObject.country.dialCode, number: mobileObject.number, isValid: true, contactFor: contactFor ?? "")
                        }
                    }
                }
                
            }
        }
    }
    
    
    func loadContacts(txt: String, axis: CGFloat, position: String, maxReached: Bool = false, otherCountry: Bool = false) {
        yAxis = axis
        reduceFrom = position
        DispatchQueue.main.async {
            self.contactSuggesstionCompletion(number: txt) { (contacts) in
                DispatchQueue.main.async {
                    guard contacts.count > 0 else {
                        self.hideContactSuggestion()
                        return
                    }
                    self.contactSearchedDict = contacts
                    if maxReached {
                        self.hideContactSuggestion()
                    } else {
                        self.showContactSuggestion()
                    }
                    self.contactTableView.reloadData()
                }
            }
        }
    }
    
    
    func contactSuggesstionCompletion( number: String,  completionHandler : @escaping ([Dictionary<String, Any>]) -> Void)  {
        var contactNumbers = [Dictionary<String, Any>]()
        
        if okContacts.count > 0 {
            for contact in okContacts {
                for phoneNumber in contact.phoneNumbers {
                    let phoneNumberStruct = phoneNumber.value
                    let phoneNumberString = phoneNumberStruct.stringValue
                    
                    let strMobNo = phoneNumberString
                    let trimmed = String(strMobNo.filter { !" ".contains($0) })
                    
                    var prefixString : String = number
                    
                    if prefixString.hasPrefix("0") {
                        _ = prefixString.remove(at: String.Index.init(encodedOffset: 0))
                    }
                    
                    if trimmed.lowercased().range(of: prefixString.lowercased()) != nil {
                        let dic = [ContactSuggessionKeys.contactname            : contact.givenName,
                                   ContactSuggessionKeys.phonenumber_backend    : trimmed.digitsPhone,
                                   ContactSuggessionKeys.phonenumber_onscreen   : phoneNumberString.digitsPhone,
                                   ContactSuggessionKeys.operatorname           : PayToValidations().getNumberRangeValidation(number).operator]
                        if PayToValidations().getNumberRangeValidation(trimmed).isRejected == false {
                            contactNumbers.append(dic)
                        }
                    }
                }
            }
        }
        completionHandler(contactNumbers)
    }
    
}
