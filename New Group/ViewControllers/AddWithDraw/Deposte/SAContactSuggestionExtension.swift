//
//  SAContactSuggestionExtension.swift
//  OK
//
//  Created by shubh's MacBookPro on 9/19/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit


extension DepositeVC {
    
    func showContactList(yAsix: CGFloat, position: String, contactFor: String) {
        if !(self.view.contains(self.contactSuggestionView)) {
            DispatchQueue.main.async {
                self.contactSuggestionView.contactTableView.register(UINib.init(nibName: "ContactSuggestionCell", bundle: Bundle.main), forCellReuseIdentifier: "ContactSuggestionCell")
                self.contactSuggestionView.frame = CGRect(x: 5, y: yAsix, width: (self.view.frame.width - 10) , height: 150)
                self.contactSuggestionView.reduceFrom = position
                self.contactSuggestionView.yAxis = yAsix
                self.contactSuggestionView.contactFor = contactFor
                self.contactSuggestionView.delegate = self
                self.view.bringSubviewToFront(self.contactSuggestionView)
                self.view.addSubview(self.contactSuggestionView)
            }
        }
    }
    
    func removeContactViewFromSurperView() {
        removeContactList()
    }
    
    func removeContactList() {
        if self.view.contains(self.contactSuggestionView) {
            self.contactSuggestionView.removeFromSuperview()
        }
    }
    func selectedNumber(flag: String, countryCode: String, number: String, isValid: Bool, contactFor: String) {
        removeContactList()
        if contactFor == "Deposit" {
            if isValid {
                model.contactNumber = number
                model.confirmContactNumber = number
                model.depositCountryCode = countryCode
                model.depositFlag = flag
                self.isDepositeNumberSelectedFromContact = true
                if let cell = tbDeposite.cellForRow(at: IndexPath(row: 0, section: 0)) as? depositeContactCell {
                    cell.tfAlternateNumber.text = number
                    cell.btnClose.isHidden = false
                    cell.imgCountryFlag.image = UIImage(named: model.depositFlag)
                    cell.lblCountyCode.text = "(" + model.depositCountryCode + ")"
                }
                self.insertDepositorConfirmNumber(isKeyboardShow: false)
                self.CheckNumberRegisteredAndMinMaxAmount(number: formateValidNumber(num: number, coutryCode: model.depositCountryCode), completion: {(_ isBool: Bool, data: Any) in
                    if isBool {
                        if let dic = data as? NSDictionary {
                            DispatchQueue.main.async {
                                if let min = dic["MinimumAmount"] as? String {
                                    self.minAmount = Int(min) ?? 0
                                }
                                if let max = dic["MaximumFaceAmount"] as? String {
                                    self.maxAmount = Int(max) ?? 0
                                }
                                if let maxWithoutFace = dic["MaximumWithoutFaceAmount"] as? String {
                                    self.withoutFaceMaxAmount = Int(maxWithoutFace) ?? 0
                                }
                                
                                let mynum = self.formateValidNumber(num: number, coutryCode: self.model.depositCountryCode)
                                
                                if UserModel.shared.mobileNo == mynum {
                                    self.btnCheckBox.isSelected = true
                                    self.isdepositorNumberOKDollreResitered = true
                                    self.model.name = UserModel.shared.name
                                    self.model.depositFacePhotoURL = UserModel.shared.proPic
                                    self.AddNameCell()
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                        if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 0)) as? depositeNameCell {
                                            cell.tfUserName.isUserInteractionEnabled = false
                                            cell.btnclose.isHidden = true
                                        }
                                    })
                                    self.AddAmountCell()
                                }else {
                                    if let registerStatus = dic["DestinationNumberRegStatus"] as? NSNumber {
                                        if registerStatus.boolValue {
                                            self.isdepositorNumberOKDollreResitered = true
                                            self.model.name = dic["MercantName"] as? String ?? ""
                                            self.model.depositFacePhotoURL = dic["ProfilePic"] as? String ?? ""
                                            self.AddNameCell()
                                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                                if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 0)) as? depositeNameCell {
                                                    cell.tfUserName.isUserInteractionEnabled = false
                                                    cell.btnclose.isHidden = true
                                                }
                                            })
                                            self.AddAmountCell()
                                        }else {
                                            self.isdepositorNumberOKDollreResitered = false
                                            self.AddNameCell()
                                            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 0)) as? depositeNameCell {
                                                cell.tfUserName.isUserInteractionEnabled = true
                                                cell.btnclose.isHidden = true
                                            }
                                        }
                                    }
                                }
                                
                            }
                        }
                    }else {
                        let btn = UIButton()
                        btn.tag = 100
                        self.onClickContactNumberClose(btn)
                    }
                })
            }else {
                self.btnCheckBox.isSelected = false
                isDepositeNumberSelectedFromContact = false
                let btn = UIButton()
                btn.tag = 100
                self.onClickContactNumberClose(btn)
            }
        }else if contactFor == "Withdraw" {
            if isValid {
                let (mobileNumber,_) = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: UserModel.shared.mobileNo)
                if number == model.contactNumber {
                    alertViewObj.wrapAlert(title: "", body: "Depositor number and withdrawal number should not be same".localized, img: UIImage(named: "alert-icon")!)
                    alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
                        let btn = UIButton()
                        btn.tag = 1000
                        self.onClickContactNumberClose(btn)
                    })
                    DispatchQueue.main.async {
                        alertViewObj.showAlert(controller: self)
                    }
                }else if mobileNumber == number {
                    alertViewObj.wrapAlert(title: "", body: "You can't withdraw money to own number".localized, img: UIImage(named: "alert-icon")!)
                    alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
                        let btn = UIButton()
                        btn.tag = 1000
                        self.onClickContactNumberClose(btn)
                    })
                    DispatchQueue.main.async {
                        alertViewObj.showAlert(controller: self)
                    }
                }else {
                    model.contactNumberWithDrawer = number
                    model.confirmContactNumberWithDrawer = number
                    model.withdrawCountryCode = countryCode
                    model.withdrawFlag = flag
                    isWithdrawNumberSelectedFromContact = true
                    
                    if let cell = tbDeposite.cellForRow(at: IndexPath(row: 0, section: 1)) as? withdrawDepositeContactCell {
                        cell.tfAlternateNumber.text = model.contactNumberWithDrawer
                        cell.btnClose.isHidden = false
                        cell.imgCountryFlag.image = UIImage(named: model.withdrawFlag)
                        cell.lblCountyCode.text = "(" + model.withdrawCountryCode + ")"
                    }
                    
                    self.checkNumberRegisteredWithOKDoller(number: formateValidNumber(num: model.contactNumberWithDrawer, coutryCode: model.withdrawCountryCode), completion: { (_ isBool : Bool, data: Any) in
                        if isBool {
                            self.isWithdrawalNumberOKDollreResitered = true
                            if let dic = data as? NSDictionary {
                                self.model.nameWithdrawer = dic["MercantName"] as? String ?? ""
                                self.AddWithdrawConfirmContactNumber(isKeyboardShow: false)
                                self.model.nameWithdrawer = dic["MercantName"] as? String ?? ""
                                self.model.withdrawalFacePhotoURL = dic["ProfilePic"] as? String ?? ""
                                self.AddWithdrawNameCell()
                                
                                if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 1)) as? withdrawNameCell {
                                    cell.isUserInteractionEnabled = false
                                    cell.btnClose.isHidden = true
                                }
                                
                                if (self.isdepositorNumberOKDollreResitered || self.btnCheckBox.isSelected) && self.isWithdrawalNumberOKDollreResitered {
                                    if let cell1 = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 0)) as? depositeAgentCommCell {
                                        cell1.tfAmount.text = "0"
                                    }
                                }else if self.btnCheckBox.isSelected {
                                    if let cell1 = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 0)) as? depositeAgentCommCell {
                                        let finalCom = (Int(self.model.TotalCommission ) ?? 0) - (Int(self.model.AgentCashInCommission) ?? 0)
                                        cell1.tfAmount.text = String(finalCom)
                                    }
                                }else if self.isWithdrawalNumberOKDollreResitered {
                                    if let cell1 = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 0)) as? depositeAgentCommCell {
                                        let finalCom = (Int(self.model.TotalCommission ) ?? 0) - (Int(self.model.AgentCashInCommission) ?? 0)
                                        cell1.tfAmount.text = String(finalCom)
                                    }
                                }
                                self.bottomcontant.constant = 0
                            }
                        }else {
                            self.isWithdrawalNumberOKDollreResitered = false
                            self.AddWithdrawConfirmContactNumber(isKeyboardShow: false)
                            self.AddWithdrawNameCell()
                        }
                    })
                }
            }else {
                isWithdrawNumberSelectedFromContact = false
                let btn = UIButton()
                btn.tag = 1000
                self.onClickContactNumberClose(btn)
            }
        }
    }
    
}
