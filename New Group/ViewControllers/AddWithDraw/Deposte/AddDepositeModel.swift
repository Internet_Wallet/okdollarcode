//
//  AddDepositeModel.swift
//  OK
//
//  Created by Imac on 7/4/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class AddDepositeModel: NSObject {
    var contactNumber: String = ""
    var confirmContactNumber: String = ""
    var depositCountryCode = ""
    var depositFlag = ""
    var contactNumberWithDrawer: String = ""
    var confirmContactNumberWithDrawer: String = ""
    var withdrawCountryCode = ""
    var withdrawFlag = ""
    var name: String = ""
    var amount: String = ""
    var agentComm: String = ""
    var nrc: String = ""
    var nrcPrefix = ""
    var nrcPostfix = ""
    var nameWithdrawer: String = ""
    var amountWithdrawer: String = ""
    var depositFacePhotoURL: String = ""
    var withdrawalFacePhotoURL = ""
    var nrcWithraw: String = ""
    var nrcPrefixWithdraw = ""
    var nrcPostfixWithdraw = ""
    var divisionCode = "YANGDIVI"
    var townshipCode = ""
    var division = ""
    var township = ""
    var city = ""
    var village = ""
    var street = ""
    var houseNumber = ""
    var floorNumber = ""
    var roomNumber = ""
    var AgentCashInCommission = ""
    var AgentWithDrawCommission = ""
    var OkDollarCommission = ""
    var TotalCommission = ""
    
    func wrapData()  ->Dictionary<String, Any> {
        let dic = ["WithdrawerInfo": withDrawerInfo(),
                   "DepositorInfo": dipositorInfo(),
                   "AppInfo": addInfo()
            ] as [String : Any]
        return dic
    }
    
    
    func withDrawerInfo() ->Dictionary<String, Any> {
        let dic = ["State": divisionCode,"MobileNo":   contactNumber,"Myself":false,"Name": name,"NrcPassport": nrc,"ProfileImg": withdrawalFacePhotoURL,"ProofBack":"","ProofFront":"","Address":""] as [String : Any]
        return dic
    }
    func dipositorInfo() ->Dictionary<String, Any> {
        let dic = ["Amt": amount,"MobileNo": "","Myself":false,"Name":"","NrcPassport":"","ProfileImg":"","ProofBack":"","ProofFront":"","TransType":""] as [String : Any]
        return dic
    }
    func addInfo() ->Dictionary<String, Any> {
        let dic = ["CellId":"","Lang": "","Lat":"","merchant_type":"","MobileNo":"","MsId":"","ProfileImg":"","SimId":"","TransType":0,"TransactionType": "PAYTO","Password":"","SecureToken":""] as [String : Any]
        return dic
    }
    
}
