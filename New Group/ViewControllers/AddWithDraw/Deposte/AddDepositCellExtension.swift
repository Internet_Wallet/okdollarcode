//
//  AddDepositCellExtension.swift
//  OK
//
//  Created by Imac on 7/2/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit



class depositeContactCell: UITableViewCell {
    
    @IBOutlet weak var imgCountryFlag: UIImageView!
    @IBOutlet weak var tfAlternateNumber: RestrictedCursorMovement!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lblCountyCode: UILabel!
    @IBOutlet weak var btnContact: UIButton!
    var contactSuggestionView : SAConatactListView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}




class depositeConfirmContactCell: UITableViewCell {
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var imgCountryFlag: UIImageView!
    @IBOutlet weak var tfConfirmNumber: RestrictedCursorMovement!
    @IBOutlet weak var btnClose: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}


class withdrawDepositeContactCell: UITableViewCell {
    @IBOutlet weak var imgCountryFlag: UIImageView!
    @IBOutlet weak var lblCountyCode: UILabel!
    @IBOutlet weak var tfAlternateNumber: RestrictedCursorMovement!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnContact: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}

class WithdrawConfirmContactCell: UITableViewCell {
    @IBOutlet weak var imgCountryFlag: UIImageView!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var tfConfirmNumber: RestrictedCursorMovement!
    @IBOutlet weak var btnClose: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}

class depositeNameCell: UITableViewCell {
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    
    @IBOutlet weak var btnU: UIButton!
    @IBOutlet weak var btnMg: UIButton!
    @IBOutlet weak var btnMr: UIButton!
    @IBOutlet weak var btnDr: UIButton!
    
    @IBOutlet weak var btnDaw: UIButton!
    @IBOutlet weak var btnMa: UIButton!
    @IBOutlet weak var btnMs: UIButton!
    @IBOutlet weak var btnMrs: UIButton!
    @IBOutlet weak var btnMDr: UIButton!
    
    @IBOutlet weak var btnFU: UIButton!
    @IBOutlet weak var btnFMr: UIButton!
    @IBOutlet weak var btnFDr: UIButton!
    
    @IBOutlet weak var viewMaleFemale: UIView!
    @IBOutlet weak var viewMaleEn: UIView!
    @IBOutlet weak var viewFemaleEn: UIView!
    @IBOutlet weak var viewMaleFemaleMy: UIView!
    
    @IBOutlet weak var tfUserName: UITextField!
    
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblStutus: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var btnclose: UIButton!
    var fnt = UIFont.systemFont(ofSize: 18)
  
    override func awakeFromNib() {
        super.awakeFromNib()
       
        self.btnMale.setTitle("Male".localized, for: .normal)
        self.btnFemale.setTitle("Female".localized, for: .normal)
        
        self.btnU.setTitle("U".localized, for: .normal)
        self.btnMg.setTitle("Mg".localized, for: .normal)
        self.btnMr.setTitle("Mr".localized, for: .normal)
        self.btnDr.setTitle("Dr".localized, for: .normal)
        
        self.btnDaw.setTitle("Daw".localized, for: .normal)
        self.btnMa.setTitle("Ma".localized, for: .normal)
        self.btnMs.setTitle("Ms".localized, for: .normal)
        self.btnMrs.setTitle("Mrs".localized, for: .normal)
        self.btnMDr.setTitle("Dr".localized, for: .normal)
        
        self.btnFU.setTitle("U".localized, for: .normal)
        self.btnFMr.setTitle("Mr".localized, for: .normal)
        self.btnFDr.setTitle("Dr".localized, for: .normal)
        
        self.viewMaleFemale.isHidden = true
        self.viewMaleEn.isHidden = true
        self.viewFemaleEn.isHidden = true
        self.viewMaleFemaleMy.isHidden = true
        
//        var font = UIFont(name: appFont, size: 18.0)
//        if #available(iOS 13.0, *) {
//           font = UIFont.systemFont(ofSize: 18)
//        }
//        tfUserName.font = font
//
//        if ok_default_language == "my" {
//            fnt = UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18)
//             tfUserName.font = fnt
//        }else if ok_default_language == "en"{
//          tfUserName.font = fnt
//        }else {
//            if #available(iOS 13.0, *) {
//                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
//                                  .font : UIFont(name: appFont, size: 18)]
//                tfUserName.attributedPlaceholder = NSAttributedString(string: "Enter Depositor Name", attributes:attributes as [NSAttributedString.Key : Any])
//            } else {
//                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
//                                  .font : UIFont(name: appFont, size: 18)]
//                tfUserName.attributedPlaceholder = NSAttributedString(string:  "Enter Depositor Name", attributes:attributes)
//              //  tfUserName.font = UIFont.systemFont(ofSize: 18)
//            }
       // }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
       
    }
}

class withdrawNameCell: UITableViewCell {
    
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    
    @IBOutlet weak var btnU: UIButton!
    @IBOutlet weak var btnMg: UIButton!
    @IBOutlet weak var btnMr: UIButton!
    @IBOutlet weak var btnDr: UIButton!
   
    @IBOutlet weak var btnDaw: UIButton!
    @IBOutlet weak var btnMa: UIButton!
     @IBOutlet weak var btnMs: UIButton!
    @IBOutlet weak var btnMrs: UIButton!
    @IBOutlet weak var btnMDr: UIButton!
    
    @IBOutlet weak var btnFU: UIButton!
    @IBOutlet weak var btnFMr: UIButton!
    @IBOutlet weak var btnFDr: UIButton!

    @IBOutlet weak var viewMaleFemale: UIView!
    @IBOutlet weak var viewMaleEn: UIView!
    @IBOutlet weak var viewFemaleEn: UIView!
    @IBOutlet weak var viewMaleFemaleMy: UIView!
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblStutus: UILabel!
    @IBOutlet weak var tfUserName: UITextField!{
        didSet{
            self.tfUserName.font = UIFont(name: "Zawgyi-One", size: appFontSize)
        }
    }
    @IBOutlet weak var btnClose: UIButton!
     var fnt = UIFont.systemFont(ofSize: 18)
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.btnMale.setTitle("Male".localized, for: .normal)
        self.btnFemale.setTitle("Female".localized, for: .normal)
        
        self.btnU.setTitle("U".localized, for: .normal)
        self.btnMg.setTitle("Mg".localized, for: .normal)
        self.btnMr.setTitle("Mr".localized, for: .normal)
        self.btnDr.setTitle("Dr".localized, for: .normal)
        
        self.btnDaw.setTitle("Daw".localized, for: .normal)
        self.btnMa.setTitle("Ma".localized, for: .normal)
        self.btnMs.setTitle("Ms".localized, for: .normal)
        self.btnMrs.setTitle("Mrs".localized, for: .normal)
        self.btnMDr.setTitle("Dr".localized, for: .normal)
        
        self.btnFU.setTitle("U".localized, for: .normal)
        self.btnFMr.setTitle("Mr".localized, for: .normal)
        self.btnFDr.setTitle("Dr".localized, for: .normal)
        
        self.viewMaleFemale.isHidden = true
        self.viewMaleEn.isHidden = true
        self.viewFemaleEn.isHidden = true
        self.viewMaleFemaleMy.isHidden = true
       
//        var font = UIFont(name: appFont, size: 18.0)
//        if #available(iOS 13.0, *) {
//            font = UIFont.systemFont(ofSize: 18)
//        }
//        tfUserName.font = font
//
//        if ok_default_language == "my" {
//            fnt = UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18)
//            tfUserName.font = fnt
//        }else if ok_default_language == "en"{
//            tfUserName.font = fnt
//        }else {
//            if #available(iOS 13.0, *) {
//                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
//                                  .font : UIFont(name: appFont, size: 18)]
//                tfUserName.attributedPlaceholder = NSAttributedString(string: "Enter Withdrawal Name", attributes:attributes as [NSAttributedString.Key : Any])
//            } else {
//                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
//                                  .font : UIFont.systemFont(ofSize: 18)]
//                tfUserName.attributedPlaceholder = NSAttributedString(string: "Enter Withdrawal Name", attributes:attributes)
//                tfUserName.font = UIFont.systemFont(ofSize: 18)
//            }
       // }
       
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}

class depositeAmountCell: UITableViewCell {
    @IBOutlet weak var lblStatusTitle: UILabel!{
        didSet{
            self.lblStatusTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var tfAmount: UITextField!{
        didSet{
            self.tfAmount.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnClear: UIButton!
    var fnt = UIFont.systemFont(ofSize: 18)
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        var font = UIFont(name: appFont, size: 18.0)
//        if #available(iOS 13.0, *) {
//            font = UIFont.systemFont(ofSize: 18)
//        }
//        tfAmount.font = font
        
//        if ok_default_language == "my" {
//            fnt = UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18)
//            tfAmount.font = fnt
//        }else if ok_default_language == "en"{
//            tfAmount.font = fnt
//        }else {
//            if #available(iOS 13.0, *) {
//                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
//                                  .font : UIFont(name: appFont, size: 18)]
//                tfAmount.attributedPlaceholder = NSAttributedString(string: "Enter Amount", attributes:attributes as [NSAttributedString.Key : Any])
//            } else {
//                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
//                                  .font : UIFont.systemFont(ofSize: 18)]
//                tfAmount.attributedPlaceholder = NSAttributedString(string: "Enter Amount", attributes:attributes)
//                tfAmount.font = UIFont(name: appFont, size: appFontSize)
//            }
        //}
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}
class depositeAgentCommCell: UITableViewCell {
    @IBOutlet weak var lblStatusTitle: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var tfAmount: UITextField!
    @IBOutlet weak var btnClear: UIButton!
    var fnt = UIFont.systemFont(ofSize: 18)
    override func awakeFromNib() {
        super.awakeFromNib()
        if ok_default_language == "my" {
            fnt = UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18)
        }
        tfAmount.font = fnt
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}




class DepositFacePhotoCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var lblRequired: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}

class depositeNRCCell: UITableViewCell {
    @IBOutlet var viewNRCTF: UIView!
    @IBOutlet var viewNRCButton: UIView!
    @IBOutlet var tfNRC: MyTextField!
    @IBOutlet var btnNRC: UIButton!
    let btnnrc = UIButton()
    @IBOutlet weak var btnNRCClose: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}
class WithdrawNRCCell: UITableViewCell {
    @IBOutlet weak var viewNRCTF: UIView!
    @IBOutlet weak var viewNRCButton: UIView!
    @IBOutlet var tfNRC: MyTextField!
    @IBOutlet var btnNRC: UIButton!
    let btnnrc = UIButton()
    
    @IBOutlet weak var btnNRCClose: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}

class withDrawalFacePhotoCell: UITableViewCell {
    @IBOutlet var imgPhoto: UIImageView!
    @IBOutlet var lblTitlte: UILabel!
    @IBOutlet weak var lblRequired: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}


class DivisionCellWithdraw: UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblStatus: UILabel!{
        didSet{
            lblStatus.font =  UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblDivision: MarqueeLabel!{
        didSet{
            lblDivision.font =  UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgRightArrow: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}

class ShowAddressFacePay: UITableViewCell {
    @IBOutlet weak var lblAddressShow: UILabel!
    @IBOutlet weak var btnAddressShow: UIButton!
    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        lblAddressShow.text = "Do you want to send full address".localized
        bgView.layer.cornerRadius = 5
        bgView.layer.masksToBounds = true
        bgView.layer.borderColor = UIColor.lightGray.cgColor
        bgView.layer.borderWidth = 1.0
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}

class TownshipCellWithdraw: UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblTownship: MarqueeLabel!
    @IBOutlet weak var imgRightArrow: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}
class CityCellWithdraw: UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblcity: MarqueeLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}

class VillageCell: UITableViewCell {
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var tfVilage: UITextField!
    var fnt = UIFont.systemFont(ofSize: 18)
    override func awakeFromNib() {
        super.awakeFromNib()
        
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont.systemFont(ofSize: 18)
        }
        tfVilage.font = font
        
        if ok_default_language == "my" {
            tfVilage.font = UIFont(name: appFont, size: 18)
        }else if ok_default_language == "en"{
            tfVilage.font = UIFont.systemFont(ofSize: 18)
        }else {
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfVilage.attributedPlaceholder = NSAttributedString(string: "Enter Village Tract", attributes:attributes as [NSAttributedString.Key : Any])
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfVilage.attributedPlaceholder = NSAttributedString(string: "Enter Village Tract", attributes:attributes)
                tfVilage.font = UIFont.systemFont(ofSize: 18)
            }
        }
        tfVilage.placeholder = "Enter Village Tract".localized
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}
class StreetVillageCell: UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var txtStreet: UITextField!
    var fnt = UIFont.systemFont(ofSize: 18)
    override func awakeFromNib() {
        super.awakeFromNib()
        
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont.systemFont(ofSize: 18)
        }
        txtStreet.font = font
        
        if ok_default_language == "my" {
            txtStreet.font = UIFont(name: appFont, size: 18)
        }else if ok_default_language == "en"{
            txtStreet.font = UIFont.systemFont(ofSize: 18)
        }else {
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                txtStreet.attributedPlaceholder = NSAttributedString(string: "Enter street", attributes:attributes as [NSAttributedString.Key : Any])
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                txtStreet.attributedPlaceholder = NSAttributedString(string: "Enter street", attributes:attributes)
                txtStreet.font = UIFont.systemFont(ofSize: 18)
            }
        }
    txtStreet.placeholder = "Enter street".localized
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}


class HouseRoomFloorNumberCell: UITableViewCell {
    @IBOutlet weak var lblHouse: UILabel!
    @IBOutlet weak var lblFloor: UILabel!
    @IBOutlet weak var lblRoom: UILabel!
    @IBOutlet weak var tfHouse: UITextField!
    @IBOutlet weak var tfFloor: UITextField!
    @IBOutlet weak var tfRoom: UITextField!
    var fnt = UIFont.systemFont(ofSize: 18)
    
    override func awakeFromNib() {
      
        lblHouse.text = "House No.".localized
        lblFloor.text = "Floor No.".localized
        lblRoom.text = "Room No.".localized
        
        var font = UIFont(name: appFont, size: 18.0)
        if #available(iOS 13.0, *) {
            font = UIFont.systemFont(ofSize: 18)
        }
        tfHouse.font = font
        tfFloor.font = font
        tfRoom.font = font
        
        if ok_default_language == "my" {
            tfHouse.font = UIFont(name: appFont, size: 18)
             tfFloor.font = UIFont(name: appFont, size: 18)
             tfRoom.font = UIFont(name: appFont, size: 18)
        }else if ok_default_language == "en"{
            tfHouse.font = UIFont.systemFont(ofSize: 18)
             tfFloor.font = UIFont.systemFont(ofSize: 18)
             tfRoom.font = UIFont.systemFont(ofSize: 18)
        }else {
            if #available(iOS 13.0, *) {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfHouse.attributedPlaceholder = NSAttributedString(string: "House No.", attributes:attributes as [NSAttributedString.Key : Any])
                
                let attributes1 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfFloor.attributedPlaceholder = NSAttributedString(string: "Floor No.", attributes:attributes1 as [NSAttributedString.Key : Any])
                
                let attributes2 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont(name: appFont, size: 18)]
                tfRoom.attributedPlaceholder = NSAttributedString(string: "Room No.", attributes:attributes2 as [NSAttributedString.Key : Any])
            } else {
                let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfHouse.attributedPlaceholder = NSAttributedString(string: "House No.", attributes:attributes)
                tfHouse.font = UIFont.systemFont(ofSize: 18)
                
                let attributes1 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfFloor.attributedPlaceholder = NSAttributedString(string: "Floor No.", attributes:attributes1)
                tfFloor.font = UIFont.systemFont(ofSize: 18)
                
                let attributes2 = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                                  .font : UIFont.systemFont(ofSize: 18)]
                tfRoom.attributedPlaceholder = NSAttributedString(string: "Room No.", attributes:attributes2)
                tfRoom.font = UIFont.systemFont(ofSize: 18)
            }
        }
        
        
        tfHouse.placeholder = "House No.".localized
        tfFloor.placeholder = "Floor No.".localized
        tfRoom.placeholder = "Room No.".localized
        tfHouse.tag = 206
        tfFloor.tag = 207
        tfRoom.tag = 208
        tfHouse.layer.cornerRadius = 5
        tfHouse.layer.masksToBounds = true
        tfHouse.layer.borderColor = UIColor.lightGray.cgColor
        tfHouse.layer.borderWidth = 1.0
        tfFloor.layer.cornerRadius = 5
        tfFloor.layer.masksToBounds = true
        tfFloor.layer.borderColor = UIColor.lightGray.cgColor
        tfFloor.layer.borderWidth = 1.0
        tfRoom.layer.cornerRadius = 5
        tfRoom.layer.masksToBounds = true
        tfRoom.layer.borderColor = UIColor.lightGray.cgColor
        tfRoom.layer.borderWidth = 1.0
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
}


extension DepositeVC: WebServiceResponseDelegate {

    func webResponse(withJson json: AnyObject, screen: String) {
        self.removeProgressView()
      if screen == "MinMaxAmount" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        println_debug(dic)
                        if dic["code"] as? NSNumber == 200 {
                                if let data = dic["data"] as? Dictionary<String,AnyObject> {
                                    self.minAmount = Int(truncating: data["MinimumAmount"] as? NSNumber ?? 0)
                                    self.maxAmount = Int(truncating: data["MaximumFaceAmount"] as? NSNumber ?? 0)
                                    self.withoutFaceMaxAmount = Int(truncating: data["MaximumWithoutFaceAmount"] as? NSNumber ?? 0)
                            }
                        }else {
                            DispatchQueue.main.async {
                                self.showAlert(alertTitle: "", alertBody: "Request Failed. Please Try Again".localized, alertImage: UIImage(named: "alert-icon")!)}
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "", alertBody: "Request Failed. Please Try Again".localized, alertImage: UIImage(named: "alert-icon")!)}
                }
            }catch {}
        }
    }
}
