//
//  DepositeRecieptVC.swift
//  OK
//
//  Created by Imac on 7/26/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class DepositeRecieptVC: OKBaseController,DepositReceiptMenuDelegate {
    
    @IBOutlet weak var depositorView: CardDesignView!
    @IBOutlet weak var depositorViewBG: CardDesignView!
    
    @IBOutlet weak var scrollBG: UIScrollView!
    @IBOutlet weak var scroll: UIScrollView!
    
    @IBOutlet weak var lblDepositor: UILabel!
    @IBOutlet weak var lblAmout: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblCategoryInput: UILabel!
    
    @IBOutlet weak var lblDepositorBG: UILabel!
    @IBOutlet weak var lblAmoutBG: UILabel!
    @IBOutlet weak var lblCategoryBG: UILabel!
    @IBOutlet weak var lblCategoryInputBG: UILabel!
    
    @IBOutlet weak var lblWtihdraw: UILabel!
    @IBOutlet weak var lblWtihdrawName: UILabel!
    @IBOutlet weak var lblWithDrawNumber: UILabel!
    @IBOutlet weak var lblWithdrawNumberInput: UILabel!
    
    @IBOutlet weak var lblWtihdrawBG: UILabel!
    @IBOutlet weak var lblWtihdrawNameBG: UILabel!
    @IBOutlet weak var lblWithDrawNumberBG: UILabel!
    @IBOutlet weak var lblWithdrawNumberInputBG: UILabel!
    
    @IBOutlet weak var lblDepositName: UILabel!
    @IBOutlet weak var lblDepositNameInput: UILabel!
    @IBOutlet weak var lblDepositNumber: UILabel!
    @IBOutlet weak var lblDepositNumberInput: UILabel!
    
    @IBOutlet weak var lblDepositNameBG: UILabel!
    @IBOutlet weak var lblDepositNameInputBG: UILabel!
    @IBOutlet weak var lblDepositNumberBG: UILabel!
    @IBOutlet weak var lblDepositNumberInputBG: UILabel!
    
    @IBOutlet weak var lblAgetAccNumber: UILabel!
    @IBOutlet weak var lblAgentAccNumberInput: UILabel!
    
    @IBOutlet weak var lblAgetAccNumberBG: UILabel!
    @IBOutlet weak var lblAgentAccNumberInputBG: UILabel!

    @IBOutlet weak var lblBalance: UILabel!
    @IBOutlet weak var lblBalanceInput: UILabel!
    @IBOutlet weak var lblComFee: UILabel!
    @IBOutlet weak var lblComFeeInput: UILabel!
    @IBOutlet weak var lblWallet: UILabel!
    @IBOutlet weak var lblWalletInout: UILabel!
    
    @IBOutlet weak var lblBalanceBG: UILabel!
    @IBOutlet weak var lblBalanceInputBG: UILabel!
    @IBOutlet weak var lblComFeeBG: UILabel!
    @IBOutlet weak var lblComFeeInputBG: UILabel!
 
    
    @IBOutlet weak var lblTrasictionID: UILabel!
    @IBOutlet weak var lblTransictionIDInput: UILabel!
    @IBOutlet weak var lblTrasictionType: UILabel!
    @IBOutlet weak var lblTransictionTypeInput: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblTrasictionIDBG: UILabel!
    @IBOutlet weak var lblTransictionIDInputBG: UILabel!
    @IBOutlet weak var lblTrasictionTypeBG: UILabel!
    @IBOutlet weak var lblTransictionTypeInputBG: UILabel!
    @IBOutlet weak var lblDateBG: UILabel!
    @IBOutlet weak var lblTimeBG: UILabel!
    
    @IBOutlet weak var lblHome: UILabel!
    @IBOutlet weak var lblMore: UILabel!
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var lblToWithdraw: UILabel!
    
    @IBOutlet weak var lblToBG: UILabel!
    @IBOutlet weak var lblToWithdrawBG: UILabel!
    
    var depositDic : Dictionary<String,String>?
    var depositor_ID: String?
    let qrCode = QRCodeGenericClass()
    var formatterDepositNumber = ""
    var formatterWithdrawNumber = ""
    var formatterAgentNumber = ""
    var dateTime = ""
     var finalAmount = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        lblHome.text = "HomePagePT".localized
        lblMore.text = "More".localized
        lblDepositor.text = "Face ID Pay".localized
        lblDepositorBG.text = "Face ID Pay".localized
        
        lblToWithdraw.text = "WithdrawalR".localized
        lblAmout.attributedText = qrCode.concatinateAmountWithMMK(amount: getDigitDisplayColor(depositDic?["Withdraw_Amount"] ?? ""))
        
        lblToWithdrawBG.text = "WithdrawalR".localized
        lblTo.text = "To".localized
        lblToBG.text = "To".localized
        lblAmoutBG.attributedText = qrCode.concatinateAmountWithMMK(amount: getDigitDisplayColor(depositDic?["Withdraw_Amount"] ?? ""))
        
        lblCategory.text = "Categories".localized
        lblCategoryInput.text = "Face ID Pay"
        
        lblCategoryBG.text = "Categories".localized
        lblCategoryInputBG.text = "Face ID Pay"
        
        lblWtihdraw.text = "Name".localized
        lblWtihdrawName.text = depositDic?["WithdrawName"]
        
        lblWtihdrawBG.text = "Name".localized
        lblWtihdrawNameBG.text = depositDic?["WithdrawName"]
        
        lblWithDrawNumber.text = "Mobile Number".localized
        lblWithDrawNumberBG.text = "Mobile Number".localized
        
        let cc1 =  depositDic?["withdrawCountrycode"]
        let num1 = depositDic?["WithdrawNumber"]
        lblWithdrawNumberInput.text = "(" +  cc1! + ")" + num1!
        formatterWithdrawNumber = "(" +  cc1! + ")" + num1!
        
        lblWithdrawNumberInputBG.text = "(" +  cc1! + ")" + num1!
        
        lblDepositName.text = "Depositor NameF".localized
        lblDepositNameInput.text = depositDic?["DepositorName"] ?? ""
        lblDepositNumber.text = "Depositor Number".localized
        
        lblDepositNameBG.text = "Depositor NameF".localized
        lblDepositNameInputBG.text = depositDic?["DepositorName"] ?? ""
        lblDepositNumberBG.text = "Depositor Number".localized
        
        let cc =  depositDic?["depositorCountrycode"]
        let num = depositDic?["DepositorNumber"]
        formatterDepositNumber = "(" +  cc! + ")" + num!
        lblDepositNumberInput.text = "(" +  cc! + ")" + num!
        lblDepositNumberInputBG.text = "(" +  cc! + ")" + num!
        
        lblAgetAccNumber.text = "Agent Account Number".localized
        lblAgetAccNumberBG.text = "Agent Account Number".localized
        
        let (mobileNumber,CountryCode) = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: UserModel.shared.mobileNo)
        lblAgentAccNumberInput.text = "(" + CountryCode + ")" + mobileNumber
        lblAgentAccNumberInputBG.text = "(" + CountryCode + ")" + mobileNumber
        formatterAgentNumber = "(" + CountryCode + ")" + mobileNumber
        
        lblBalance.text = "Amount Paid".localized
        lblBalanceBG.text = "Amount Paid".localized
       
        if let payAmount = depositDic?["Amount"] {
            finalAmount = Int(payAmount) ?? 0
        }
        
        lblBalanceInput.attributedText = qrCode.concatinateAmountWithMMK(amount: getDigitDisplayColor(String(finalAmount)))
        lblBalanceInputBG.attributedText = qrCode.concatinateAmountWithMMK(amount: getDigitDisplayColor(String(finalAmount)))
        
        lblComFee.text = "Commission Fees".localized
        lblComFeeInput.attributedText = qrCode.concatinateAmountWithMMK(amount: getDigitDisplayColor(depositDic?["CommissionFee"] ?? ""))
        
        lblComFeeBG.text = "Commission Fees".localized
        lblComFeeInputBG.attributedText = qrCode.concatinateAmountWithMMK(amount: getDigitDisplayColor(depositDic?["CommissionFee"] ?? ""))
        
        lblWallet.text = "Balance".localized
        lblWalletInout.attributedText = qrCode.concatinateAmountWithMMK(amount: depositDic?["WalletAmount"] ?? "")
        
        lblTrasictionID.text = "Transaction ID".localized
        lblTransictionIDInput.text = depositDic?["TranId"]
        lblTrasictionType.text = "Transaction Type".localized
        lblTransictionTypeInput.text = "PAYTO"
        
        lblTrasictionIDBG.text = "Transaction ID".localized
        lblTransictionIDInputBG.text = depositDic?["TranId"]
        lblTrasictionTypeBG.text = "Transaction Type".localized
        lblTransictionTypeInputBG.text = "PAYTO"
        
        
        let (date,time) = self.qrCode.currentDateAndTime()
        self.lblDate.text = date
        self.lblTime.text = time
        self.lblDateBG.text = date
        self.lblTimeBG.text = time
        self.dateTime = "\(date)\n\(time)"
        setMarqueLabelInNavigation()
        
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {

    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        let newImage = depositorView.toImage()
        UIImageWriteToSavedPhotosAlbum(newImage, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.hidesBackButton = true
    }
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 18)
        lblMarque.textAlignment = .center
        lblMarque.text = "Receipt".localized
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
    }
    
    @IBAction func onClickHomeAction(_ sender: Any) {
        // self.navigationController?.popToRootViewController(animated: true)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickMoreAction(_ sender: Any) {
        let receiptVC  = self.storyboard?.instantiateViewController(withIdentifier: "DepositReceiptMenu") as! DepositReceiptMenu
        receiptVC.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        receiptVC.delegate = self
        receiptVC.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(receiptVC, animated: false, completion: nil)
    }
    
    func optionChoosed(option: String, viewController: UIViewController) {
        viewController.dismiss(animated: false, completion: nil)
        if option == "Invoice" {
            DispatchQueue.main.async {
                let recieptVC = self.storyboard?.instantiateViewController(withIdentifier: "DepositPDF") as! DepositPDF
                recieptVC.depositDic = ["DepositorName": self.depositDic?["DepositorName"], "DepositorNumber": self.formatterDepositNumber,"WithdrawarName": self.depositDic?["WithdrawName"], "WithdrawarNumber": self.formatterWithdrawNumber,"AgentNumber": self.formatterAgentNumber,"TranID": self.depositDic?["TranId"] ,"DateTime": self.dateTime,"Amount": self.depositDic?["Amount"], "Commission_Fees" : self.depositDic?["CommissionFee"] ?? "","NetReceiveAmount" : self.depositDic?["NetReceiveAmount"] ?? ""] as? Dictionary<String, String>
                self.navigationController?.pushViewController(recieptVC, animated: true)
            }
        }else if option == "Share" {
            shareQRWithDetail()
        }else if option == "More" {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
  
    private func shareQRWithDetail() {
        let newImage = self.depositorViewBG.toImage()
        let activityViewController = UIActivityViewController(activityItems: [newImage], applicationActivities: nil)
        DispatchQueue.main.async {self.navigationController?.present(activityViewController, animated: true, completion: nil)}
    }
    
    func getDigitDisplayColor(_ digit: String) -> String {
        var FormateStr: String
        let mystring = digit.replacingOccurrences(of: ",", with: "")
        let number = NSDecimalNumber(string: mystring)
        
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        FormateStr = formatter.string(from: number)!
        if FormateStr == "NaN"{
            FormateStr = ""
        }
        return FormateStr
    }

}
