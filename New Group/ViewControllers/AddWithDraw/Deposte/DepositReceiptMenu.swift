//
//  DepositReceiptMenu.swift
//  OK
//
//  Created by Imac on 7/26/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol DepositReceiptMenuDelegate {
    func optionChoosed(option: String,viewController: UIViewController)
}



class DepositReceiptMenu: UIViewController {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblMore: UILabel!
    @IBOutlet weak var lblInvoice: UILabel!
    @IBOutlet weak var lblShare: UILabel!
    
    var delegate: DepositReceiptMenuDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        bgView.layer.cornerRadius = 10
        bgView.layer.masksToBounds = true
        lblMore.text = "More Payment".localized
        lblInvoice.text = "Invoice".localized
        lblShare.text = "Share".localized
    }
    @IBAction func onClickMorAction(_ sender: Any) {
        if let del = delegate {
            del.optionChoosed(option: "More", viewController: self)
        }
    }
    @IBAction func onClickInvoiceAction(_ sender: Any) {
        if let del = delegate {
            del.optionChoosed(option: "Invoice", viewController: self)
        }
    }
    
    @IBAction func onClickShareAction(_ sender: Any) {
        if let del = delegate {
            del.optionChoosed(option: "Share", viewController: self)
        }
    }
    @IBAction func OnClickBG(_ sender: Any) {
       self.dismiss(animated: false, completion: nil)
    }
    
}
