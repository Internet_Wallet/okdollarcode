//
//  DepositeVC.swift
//  OK
//
//  Created by Imac on 6/26/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import CoreLocation
import Rabbit_Swift

class DepositeVC: OKBaseController,BioMetricLoginDelegate,TakeFacePhotoDepositeDelegate,StreetListViewDelegate,VillageStreetListViewDelegate,SAConatactListViewDelegate {
    
    @IBOutlet weak var btnPay: UIButton!{
        didSet{
            self.btnPay.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var bottomcontant: NSLayoutConstraint!
    @IBOutlet weak var tbDeposite: UITableView!
    let imagePicker = UIImagePickerController()
    var tvVillageList = Bundle.main.loadNibNamed("VillageStreetListView", owner: self, options: nil)?[0] as! VillageStreetListView
    var tvStreetList = Bundle.main.loadNibNamed("StreetListView", owner: self, options: nil)?[0] as! StreetListView
    var contactSuggestionView = Bundle.main.loadNibNamed("SAConatactListView", owner: self, options: nil)?[0] as! SAConatactListView
    var cellList = [UITableViewCell]()
    var cellListForWithdrawer = [UITableViewCell]()
    var cellListAddress = [UITableViewCell]()
    var sectionCount = 2
    var urlIamge = UIImage()
    var depositorImage = UIImage()
    var NAMECHARSET = ""
    var isYourNameKeyboardHidden = true
    var isYourNameWithdrawKeyboardHidden = true
    let validObj = PayToValidations()
    var locationDetails: LocationDetail?
    var categoryDetail : CategoryDetail?
    var locationDetailAddress: LocationDetail?
    var countryListShownFor = ""
    var contactListShownFor = ""
    var isWithdrawNumberSelectedFromContact = false
    var isDepositeNumberSelectedFromContact = false
    var model = AddDepositeModel()
    var isWithDrawContactNubmerHidden = true
    var isWithDrawConfirmContactNubmerHidden = true
    var isContactConfirmHidden = true
    var isNameFieldHidden = true
    var isNameFieldWithdrawHidden = true
    var isNRCTextFieldHidden = true
    var isDepositFacePhotoHidden = true
    var isAmountFieldHidden = true
    var isAgentComFieldHidden = true
    var isWithdrawalFacePhotoHidden = true
    var isWtthdrawalNCRHidden = true
    var isDivisionHidden = true
    var isTownshipHidden = true
    var isDefaultCity = false
    var isCityHidden = true
    var isVillageHidden = true
    var isStreeetHidden = true
    var isHouseNumberHidden = true
    var isVillageAvaible = true
    var imageTakenFor = ""
    var nrcFrom = ""
    var minAmount = 0
    var maxAmount = 0
    var withoutFaceMaxAmount = 0
    var walletBalance = 0
    var OTHERCHARSET = ""
    var depositorPlaceholder = ""
    var withdrawalPlaceholder = ""
    var depositorAmoutStatus = ""
    var isdepositorNumberOKDollreResitered = false
    var isWithdrawalNumberOKDollreResitered = false
    var btnFacePayWithPhoto = UIButton()
    var btnFacePayWithoutPhoto = UIButton()
    var btnCheckBox = UIButton()
    var firstTimeAmount = 0
    var withdrawalImage = UIImageView()
    var isBtnFullAddressHidden = true
    var isBtnFullAddress = false
    var firstTimeWithdrawName = 0
    var depositMaleFemaleTag = false
    var wtihdrawMaleFemaleTag = false
    var withdrawnameShowFirstTime = true
    var withdrawNameTitle = ""
    
    @IBOutlet var viewComm: UIView!
    @IBOutlet weak var bgBtnViewComm: UIButton!
    @IBOutlet weak var lblComFeeHeader: UILabel!
    @IBOutlet weak var lblAgentCashInCom: UILabel!
    @IBOutlet weak var lblAgentCashInComAmount: UILabel!
    @IBOutlet weak var lblOKDollarCom: UILabel!
    @IBOutlet weak var lblOKDollarAmount: UILabel!
    @IBOutlet weak var lblAgentWithdrawCom: UILabel!
    @IBOutlet weak var lblAgentWithdrawComAmount: UILabel!
    @IBOutlet weak var lblTototalAmount: UILabel!
    @IBOutlet weak var lblTotalCpom: UILabel!
    
    
    @IBOutlet weak var viewForUNtoUN: UIView!
    
    @IBOutlet weak var viewForRegitoRegiNo: UIView!
    @IBOutlet weak var lblComHeaderRR: UILabel!
    @IBOutlet weak var lblNoteRR: UILabel!
    
    @IBOutlet weak var viewForMyNoToUN: UIView!
    @IBOutlet weak var lblHeaderMyNo: UILabel!
    @IBOutlet weak var lblOKDollarMyNo: UILabel!
    @IBOutlet weak var lblOKdollarInputMYNo: UILabel!
    @IBOutlet weak var lblAgentWtihdrawMyNo: UILabel!
    @IBOutlet weak var lblAgentWithdrawInputMyNo: UILabel!
    @IBOutlet weak var lblTotalAmtMyNo: UILabel!
    @IBOutlet weak var lblTotalAmtInputMyno: UILabel!
  
    var showComField = false
    
    var btnDepositCom = UIButton()
    var btnWithdrawCom = UIButton()
    
    let objectSound = BurmeseSoundObject()
    var amountCheckingTimer: Timer?
    var minAmountCheck: Timer?
    var minAmountCheckEndEditing : Timer?
    
    var previousYaxisScrolled = 0
    var nextYaxisScrolled = 0
    
    var countPressedOnNextButton = 0
    
    var timer: Timer?
    override func viewDidLoad() {
        super.viewDidLoad()
        walletBalance = Int(walletAmount())
        btnCheckBox.isSelected = false
        btnCheckBox.setBackgroundImage(UIImage(named: "check_box_Blue"), for: .normal)
        btnFacePayWithPhoto.isSelected = true
        btnFacePayWithoutPhoto.isSelected = false
        btnWithdrawCom.isSelected = true
        btnDepositCom.isSelected = false
        
        cellList.append(tbDeposite.dequeueReusableCell(withIdentifier: "depositeContactCell") as! depositeContactCell)
        tbDeposite.delegate = self
        tbDeposite.dataSource = self
       
        let setLanguage = UserDefaults.standard.value(forKey: "currentLanguage") as! String
        ok_default_language = setLanguage
        if setLanguage == "en" {
            NAMECHARSET = NAME_CHAR_SET_En
        OTHERCHARSET = STREETNAME_CHAR_SET_En
        }else if setLanguage == "my" {
            NAMECHARSET = NAME_CHAR_SET_My
            OTHERCHARSET = STREETNAME_CHAR_SET_My
        }else {
            NAMECHARSET = NAME_CHAR_SET_Uni
            OTHERCHARSET = STREETNAME_CHAR_SET_Uni
        }

        
        btnPay.setTitle("Next".localized, for: .normal)
        bottomcontant.constant = -50
       
        depositorPlaceholder = "Enter Depositor Name".localized
        depositorAmoutStatus = "Deposit Amount".localized
        withdrawalPlaceholder = "Enter Withdrawal Name".localized
        
        lblComFeeHeader.text = "Commission Fees".localized
        lblAgentCashInCom.text = "Agent Deposit Commission".localized
        lblOKDollarCom.text =  "Agent Withdraw Commission".localized
        lblAgentWithdrawCom.text = "OK Dollar Commission".localized
        lblTotalCpom.text = "Total Commission".localized
        // if mySelf is selected then no have Agent Deposit Commission
        lblHeaderMyNo.text = "Commission Fees".localized
        lblAgentWtihdrawMyNo.text = "Agent Withdraw Commission".localized
        lblOKDollarMyNo.text = "OK Dollar Commission".localized
        lblTotalAmtMyNo.text = "Total Commission".localized
        
        lblComHeaderRR.text = "Commission Fees".localized
        lblNoteRR.text = "Commission is not applicable for OK Dollar registered number to OK Dollar registered number".localized
        
        setDepositeInfo()
        self.setMarqueLabelInNavigation()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 0, section: 0)) as? depositeContactCell{
                cell.tfAlternateNumber.becomeFirstResponder()
            }
        })
    }
    

    override func viewWillDisappear(_ animated: Bool) {
         self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.view.endEditing(true)
        withdrawnameShowFirstTime = true
        depositMaleFemaleTag = false
        wtihdrawMaleFemaleTag = false
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        countPressedOnNextButton = 0
    }
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 18)
        lblMarque.text = "Deposit".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white//init(hex: "#1C2C99")
        self.navigationItem.titleView = lblMarque
    }
    func setDepositeInfo(){
        model.depositCountryCode = "+95"
        model.depositFlag = "myanmar"
        model.withdrawFlag = "myanmar"
        model.withdrawCountryCode = "+95"
    }
    
    @IBAction func onClickAgentViewAction(_ sender: Any) {
        if self.view.contains(self.viewComm) {
            if let cell1 = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 0)) as? depositeAgentCommCell {
                cell1.btnClear.isSelected = false
            }
            self.viewComm .removeFromSuperview()
        }
    }
    
    
    @IBAction func onClickPayAction(_ sender: Any) {
        
        if countPressedOnNextButton != 0 {
            return
        }
        countPressedOnNextButton = 1
        
        if (self.isdepositorNumberOKDollreResitered || self.btnCheckBox.isSelected)  && self.isWithdrawalNumberOKDollreResitered {
            alertViewObj.wrapAlert(title: "", body: "You entered Depositor & Withdrawal numbers, both are registered in OK$ Account. You can't use Face Pay ID. We will redirect to PayTo".localized, img: UIImage(named: "dashboard_pay_send")!)
            alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
                self.countPressedOnNextButton = 0
                let btn = UIButton()
                btn.tag = 1000
                self.onClickContactNumberClose(btn)
            })
            alertViewObj.addAction(title: "PayTo".localized, style: .target , action: {
                let payto = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PayToNavigation")
                guard let navigation = payto as? UINavigationController else { return }
                for controller in navigation.viewControllers {
                    if let paysend = controller as? PayToViewController {
                        self.countPressedOnNextButton = 0
                        let num = self.formateValidNumber(num: self.model.contactNumberWithDrawer, coutryCode: self.model.withdrawCountryCode)
                        paysend.otherRequirement = (number: num, name: "UnKnown", amount: self.model.amount , remark: "", cashInQR: false, referenceNumber: "")
                        paysend.presentedFrom = "FACE_PAY_ID"
                        payto.modalPresentationStyle = .fullScreen
                        self.present(payto, animated: true, completion: nil)
                    }
                }
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }else {
            var emptyFields = ""
            
            if model.amount == "" {
                emptyFields = emptyFields + "\n" + "Deposit Amount".localized
            }
//            if model.nrc != "" {
//                let token = model.nrc.components(separatedBy: ")")
//                if token[1].count < 6 {
//                     emptyFields = emptyFields + "\n" + "Depositor NRC number must be 6 characters".localized
//                }
//            }
            
            if !self.btnFacePayWithoutPhoto.isSelected {
                if model.withdrawalFacePhotoURL  == "" {
                    emptyFields = emptyFields + "\n" + "Withdrawal Face Photo".localized
                }
            }
            
//            if model.nrcWithraw != "" {
//                let token = model.nrcWithraw.components(separatedBy: ")")
//                if token[1].count < 6 {
//                    emptyFields = emptyFields + "\n" + "Withdrawal NRC number must be 6 characters".localized
//                }
//            }
            
            if emptyFields != "" {
                self.countPressedOnNextButton = 0
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "Please Fill All Mandatory Fields".localized, alertBody: emptyFields , alertImage: UIImage(named: "alert-icon")!)
                    
                }
                return
            }
            
            if let cell = tbDeposite.cellForRow(at: IndexPath(row: 3, section: 0)) as? depositeAmountCell {
                let amout = Int(cell.tfAmount.text?.replacingOccurrences(of: ",", with: "") ?? "0")
                if amout ?? 0 < minAmount {
                    self.countPressedOnNextButton = 0
                    self.miminumAlertShow()
                    return
                }
            }
            
            if self.btnDepositCom.isSelected {
                var amtCom = 0
                if self.isWithdrawalNumberOKDollreResitered {
                    amtCom = (Int(self.model.TotalCommission) ?? 0) - (Int(self.model.AgentCashInCommission) ?? 0)
                }else {
                    amtCom = (Int(self.model.TotalCommission) ?? 0)
                }
                let finalAmout = (Int(self.model.amount) ?? 0) + amtCom
                if finalAmout > self.walletBalance {
                    alertViewObj.wrapAlert(title: "", body: "Insufficient balance to transfer".localized, img: UIImage(named: "alert-icon")!)
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    })
                    DispatchQueue.main.async {
                        alertViewObj.showAlert(controller: self)
                    }
                }else {
                    OKPayment.main.authenticate(screenName: "deposit", delegate: self)
                }
            }else {
                OKPayment.main.authenticate(screenName: "deposit", delegate: self)
            }
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.resetCount(_:)), userInfo: nil, repeats: false)
        }
   
    }
    
    @objc func resetCount(_ timer: Timer) {
        countPressedOnNextButton = 0
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful{
            self.saveDeposit()
            countPressedOnNextButton = 0
        }else {
            countPressedOnNextButton = 0
        }
    }
    
    func saveDeposit() {
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PayConfirmationDeposite") as! PayConfirmationDeposite
            if self.btnCheckBox.isSelected { //|| self.isdepositorNumberOKDollreResitered
                vc.mySelfDepostor = "1"
            }else {
                vc.mySelfDepostor = "0"
            }
            
            if self.isWithdrawalNumberOKDollreResitered {
                vc.mySelfWithdrawal = "1"
            }else {
                vc.mySelfWithdrawal = "0"
            }
            
            vc.depositorNumber = self.model.contactNumber
            vc.depositorCountrycode = self.model.depositCountryCode
            vc.depositorName = self.model.name
            vc.amount = self.model.amount
            vc.depositorFaceURL = self.model.depositFacePhotoURL
            vc.depositorNRC = self.model.nrc
            
            vc.withdrawNumber = self.model.contactNumberWithDrawer
            vc.withdrawCountrycode = self.model.withdrawCountryCode
            vc.withdrawName = self.model.nameWithdrawer
            vc.withdrawFaceURL = self.model.withdrawalFacePhotoURL
            vc.withdrawNRC = self.model.nrcWithraw
            
            vc.state = self.model.division
            vc.stateCode = self.model.divisionCode
            if self.isDefaultCity {
                vc.township = self.model.township
            }else {
                vc.township = ""
            }
            vc.city = self.model.city
            vc.village = self.model.village
            vc.street = self.model.street
            vc.house = self.model.houseNumber
            vc.floor = self.model.floorNumber
            vc.room = self.model.roomNumber
            if self.isWithdrawalNumberOKDollreResitered {
               let amt = (Int(self.model.TotalCommission) ?? 0) - (Int(self.model.AgentCashInCommission) ?? 0)
                vc.AgentTotalCom = String(amt)
            }else {
                let amt = (Int(self.model.TotalCommission) ?? 0)
                vc.AgentTotalCom = String(amt)
            }
            if self.btnFacePayWithoutPhoto.isSelected {
               vc.isFacePayChecked = "true"
            }else {
               vc.isFacePayChecked = "false"
            }
            if self.btnDepositCom.isSelected {
               vc.CommissionFrom = true
            }else {
               vc.CommissionFrom = false
            }
            
           
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func onClickBackAction(_ sender: Any) {
        if btnCheckBox.isSelected || !isNameFieldHidden {
            alertViewObj.wrapAlert(title: "", body: "Clicking back will clear all entered details, Are you sure to exit?".localized, img: UIImage(named: "alert-icon")!)
            alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
            })
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                self.navigationController?.popViewController(animated: true)
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    @objc func tapLabel(_ : UITapGestureRecognizer) {
        
        if btnCheckBox.isUserInteractionEnabled {
            self.onClickCheckBoxAction(btnCheckBox)
        }
    }
    @objc func tapLabelWithPhoto(_ : UITapGestureRecognizer) {
        let btn = UIButton()
        self.onClickFacePay(btn)
    }
    @objc func tapLabelWithOutPhoto(_ : UITapGestureRecognizer) {
        let btn = UIButton()
        self.onClickFacePay1(btn)
    }
    
    @objc func tapLabelCommissionDeposit(_ : UITapGestureRecognizer) {
       let btn = UIButton()
        btn.tag = 100
        onClickDepositCommission(btn)
    }
    @objc func tapLabelCommissionWithdraw(_ : UITapGestureRecognizer) {
        let btn = UIButton()
        btn.tag = 200
        onClickDepositCommission(btn)
    }
    
    
    @objc func onClickDepositCommission(_ sender: UIButton) {
        if sender.tag == 100 {
            if btnDepositCom.isSelected {
                return
            }
            btnDepositCom.isSelected = true
            btnWithdrawCom.isSelected = false
            sender.isSelected = true
        }else {
            if btnWithdrawCom.isSelected {
                return
            }
            btnDepositCom.isSelected = false
            btnWithdrawCom.isSelected = true
            sender.isSelected = true
            
        }
        hideDepositorConfirmNumberField()
        deleteRowFromDepositorSection(delBelowIndax: 1)
        removeWithdrawCell()
        model.contactNumber = ""
        bottomcontant.constant = -50
        firstTimeAmount = 0
        btnFacePayWithPhoto.isSelected = true
        btnCheckBox.isUserInteractionEnabled = false
        if let cell = tbDeposite.cellForRow(at: IndexPath(row: 0, section: 0)) as? depositeContactCell {
            if model.depositCountryCode == "+95" {
                cell.tfAlternateNumber.text = "09"
            }else {
                cell.tfAlternateNumber.text = ""
            }
            
            cell.btnClose.isHidden = true
            cell.imgCountryFlag.image = UIImage(named: model.depositFlag)
            cell.lblCountyCode.text = "(" + model.depositCountryCode + ")"
            cell.tfAlternateNumber.becomeFirstResponder()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.btnCheckBox.isUserInteractionEnabled = true
        })
        self.btnCheckBox.isSelected = false
    }
    
    @objc func onClickCheckBoxAction(_ sender: UIButton) {
        hideDepositorConfirmNumberField()
        deleteRowFromDepositorSection(delBelowIndax: 1)
        removeWithdrawCell()
        model.contactNumber = ""
        sender.isSelected = !sender.isSelected
        bottomcontant.constant = -50
        firstTimeAmount = 0
        btnFacePayWithPhoto.isSelected = true
        btnCheckBox.isUserInteractionEnabled = false
        if let cell = tbDeposite.cellForRow(at: IndexPath(row: 0, section: 0)) as? depositeContactCell {
            if sender.tag == 100 {
                if model.depositCountryCode == "+95" {
                    cell.tfAlternateNumber.text = "09"
                }else {
                    cell.tfAlternateNumber.text = ""
                }
                
                cell.btnClose.isHidden = true
                cell.imgCountryFlag.image = UIImage(named: model.depositFlag)
                cell.lblCountyCode.text = "(" + model.depositCountryCode + ")"
            }
        }
        
        if sender.isSelected {
            let (mobileNumber,CountryCode) = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: UserModel.shared.mobileNo)
            let countryData = identifyCountry(withPhoneNumber: CountryCode)
            
            self.CheckNumberRegisteredAndMinMaxAmount(number: UserModel.shared.mobileNo, completion: {(_ isBool: Bool, data: Any) in
                if isBool {
                    if let dic = data as? NSDictionary {
                        DispatchQueue.main.async {
                            self.model.contactNumber = mobileNumber
                            self.model.depositCountryCode = CountryCode
                            self.model.confirmContactNumber = self.model.contactNumber
                            self.model.depositFlag = countryData.countryFlag
                            self.model.name = UserModel.shared.name
                            self.model.nrc = UserModel.shared.nrc
                            self.isDepositeNumberSelectedFromContact = true
                            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 0, section: 0)) as? depositeContactCell {
                                cell.imgCountryFlag.image = UIImage(named: self.model.depositFlag)
                                cell.tfAlternateNumber.text = self.model.contactNumber
                                cell.lblCountyCode.text = "(" + self.model.depositCountryCode + ")"
                                cell.btnClose.isHidden = false
                                self.isDepositeNumberSelectedFromContact = true
                            }
                            self.insertDepositorConfirmNumber(isKeyboardShow: false)
                            self.AddNameCell()
                            self.AddAmountCell()
                            self.model.depositFacePhotoURL = UserModel.shared.proPic
                            self.model.nrc = UserModel.shared.nrc
                            
                            
                            if let min = dic["MinimumAmount"] as? String {
                                self.minAmount = Int(min) ?? 0
                            }
                            if let max = dic["MaximumFaceAmount"] as? String {
                                self.maxAmount = Int(max) ?? 0
                            }
                            if let maxWithoutFace = dic["MaximumWithoutFaceAmount"] as? String {
                                self.withoutFaceMaxAmount = Int(maxWithoutFace) ?? 0
                            }
                            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 3, section: 0)) as? depositeAmountCell {
                                cell.tfAmount.becomeFirstResponder()
                            }
                        }
                    }
                }
            })
        }else {
            if let cell = tbDeposite.cellForRow(at: IndexPath(row: 0, section: 0)) as? depositeContactCell {
                if model.depositCountryCode == "+95" {
                    cell.tfAlternateNumber.text = "09"
                }else {
                    cell.tfAlternateNumber.text = ""
                }
                if btnCheckBox.isSelected {
                    cell.isUserInteractionEnabled = false
                }else {
                    cell.isUserInteractionEnabled = true
                }
                cell.btnClose.isHidden = true
                isDepositeNumberSelectedFromContact = false
                cell.imgCountryFlag.image = UIImage(named: model.depositFlag)
                cell.lblCountyCode.text = "(" + model.depositCountryCode + ")"
                model.depositFacePhotoURL = ""
                model.nrc = ""
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.btnCheckBox.isUserInteractionEnabled = true
        })
 
    }
    
}


extension DepositeVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                    return 70
            }else if indexPath.row == 1 {
                if isDepositeNumberSelectedFromContact {
                    return 0
                }else {
                    return 70
                }
            }else if indexPath.row == 4 {
                if showComField {
                    return 70
                }else {
                    return 0
                }
            }else if indexPath.row == 5 {
                return 0
//                if btnCheckBox.isSelected || isdepositorNumberOKDollreResitered {
//                    return 0
//                }else {
//                    return 70
//                }
            }else if indexPath.row == 6 {
                return 0
//                if btnCheckBox.isSelected || isdepositorNumberOKDollreResitered {
//                    return 0
//                }else {
//                    return 70
//                }
            }else {
                return 70
            }
        }else if indexPath.section == 1 {
            if indexPath.row == 1 {
                if isWithdrawNumberSelectedFromContact {
                      return 0
                }else {
                     return 70
                }
            }else if indexPath.row == 3 {
                if isWithdrawalNumberOKDollreResitered || btnFacePayWithoutPhoto.isSelected {
                    return 0
                }else {
                    return 70
                }
            }else if indexPath.row == 4 {
                return 0
            }else {
                return 70
            }
        }else if indexPath.section == 2 {
            if indexPath.row == 0 {
                return 70
            }else if indexPath.row == 1 {
                return 60
            }else if indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 5{
                if isBtnFullAddress {
                    return 70
                }else {
                    return 0
                }
            }else if indexPath.row == 4 {
                if isBtnFullAddress {
                    if !isVillageAvaible {
                        return 0
                    }else {
                        return 70
                    }
                }else {
                    return 0
                }
            }else {
                if isBtnFullAddress {
                    return 80
                }else {
                    return 0
                }
            }
        }else {
            return 0
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 265
        }else if section == 1 {
            if cellListForWithdrawer.count > 0 {
                return 150
            }else {
                return 0
            }
        }else if section == 2 {
            return 0
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return cellList.count
        }else if section == 1{
            return cellListForWithdrawer.count
        }else {
            return cellListAddress.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = cellList[indexPath.row]
            if indexPath.row == 0 {
                return contactMobileNumberCell(cell: cell as! depositeContactCell)
            }else if indexPath.row == 1 {
                return contactMobileConfirmNumberCell(cell: cell as! depositeConfirmContactCell)
            }else if indexPath.row == 2 {
                return nameCell(cell: cell as! depositeNameCell)
            }else if indexPath.row == 3 {
                return amountCell(cell: cell as! depositeAmountCell)
            }else if indexPath.row == 4 {
                return agentComCell(cell: cell as! depositeAgentCommCell)
            }else if indexPath.row == 5 {
                return depositFacePhotoCell(cell: cell as! DepositFacePhotoCell)
            }else if indexPath.row == 6 {
                return nrcCell(cell: cell as! depositeNRCCell)
            }
            return cell
        }else if indexPath.section == 1{
            let cell = cellListForWithdrawer[indexPath.row]
            if indexPath.row == 0 {
                return withdrawerContactMobileNumberCell(cell: cell as! withdrawDepositeContactCell)
            }else if indexPath.row == 1 {
                return withdrawweContactMobileConfirmNumberCell(cell: cell as! WithdrawConfirmContactCell)
            }else if indexPath.row == 2 {
                  return withdrawerNameCell(cell: cell as! withdrawNameCell)
            }else if indexPath.row == 3 {
                return withdrawFacePhotoCell(cell: cell as! withDrawalFacePhotoCell)
            }else if indexPath.row == 4 {
                return nrcCellWithdraw(cell: cell as! WithdrawNRCCell)
            }
            return cell
        }else {
            let cell = cellListAddress[indexPath.row]
            if indexPath.row == 0 {
                return DivisionCell(cell: cell as! DivisionCellWithdraw)
            }else if indexPath.row == 1 {
                return ShowAddressFacePayCell(cell: cell as! ShowAddressFacePay)
            }else if indexPath.row == 2 {
                return TownShipCell(cell: cell as! TownshipCellWithdraw)
            }else if indexPath.row == 3 {
                return CityCell(cell: cell as! CityCellWithdraw)
            }else if indexPath.row == 4 {
                return VillageCell(cell: cell as! VillageCell)
            }else if indexPath.row == 5 {
                return StreetCell(cell: cell as! StreetVillageCell)
            }else {
                return HouseNumber(cell: cell as! HouseRoomFloorNumberCell)
            }
        }
        
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return self.createRequiredFieldLabel(headerTitle: "Depositor Information".localized, txtAlignment: .center, gcRect: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 40), section: section)
        }else if section == 1 {
            if cellListForWithdrawer.count == 0 {
                return UIView()
            }else {
                return self.createRequiredFieldLabelCheckBox(headerTitle: "Withdrawal Information".localized, txtAlignment: .center, gcRect: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 70), section: section)
            }
        }else if section == 2 {
              return createRequiredFieldLabelNormal(headerTitle: "", reqTitle: "")
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
          return UIView()
    }
    
    
    
    private func contactMobileNumberCell(cell : depositeContactCell) -> depositeContactCell {
        cell.tfAlternateNumber.placeholder = "Depositor Number".localized
        if model.contactNumber == "" && model.depositCountryCode == "+95"{
          model.contactNumber = "09"
         cell.btnClose.isHidden = true
        }
        
        cell.lblCountyCode.text = "(" + model.depositCountryCode + ")"
        cell.imgCountryFlag.image = UIImage(named: model.depositFlag)
        cell.tfAlternateNumber.text = model.contactNumber
        cell.tfAlternateNumber.delegate = self
        cell.tfAlternateNumber.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        cell.tfAlternateNumber.tag = 100
        cell.btnClose.addTarget(self, action: #selector(onClickContactNumberClose(_:)), for: .touchUpInside)
        cell.btnClose.tag = 100
        cell.btnContact.addTarget(self, action: #selector(onClickContactNumberContact(_:)), for: .touchUpInside)
        cell.btnContact.tag = 100
    
        let tap = UITapGestureRecognizer(target: self, action: #selector(DepositeVC.onClickCountry))
        tap.name = "deposit"
        cell.imgCountryFlag.addGestureRecognizer(tap)
        cell.imgCountryFlag.isUserInteractionEnabled = true
     
        return cell
    }
    
    private func withdrawerContactMobileNumberCell(cell : withdrawDepositeContactCell) -> withdrawDepositeContactCell {
        cell.tfAlternateNumber.placeholder = "Withdrawal Number".localized
        cell.imgCountryFlag.image = UIImage(named: model.withdrawFlag)
        if model.contactNumberWithDrawer == ""{
            model.contactNumberWithDrawer = "09"
            cell.btnClose.isHidden = true
        }
        cell.tfAlternateNumber.text = model.contactNumberWithDrawer
        cell.tfAlternateNumber.delegate = self
        cell.tfAlternateNumber.tag = 1000
        cell.tfAlternateNumber.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        cell.btnClose.addTarget(self, action: #selector(onClickContactNumberClose(_:)), for: .touchUpInside)
        cell.btnClose.tag = 1000
        cell.btnContact.addTarget(self, action: #selector(onClickContactNumberContact(_:)), for: .touchUpInside)
        cell.btnContact.tag = 1000
        let tap = UITapGestureRecognizer(target: self, action: #selector(DepositeVC.onClickCountry))
        tap.name = "withdraw"
        cell.imgCountryFlag.addGestureRecognizer(tap)
        cell.imgCountryFlag.isUserInteractionEnabled = true
        return cell
    }
    
    private func contactMobileConfirmNumberCell(cell : depositeConfirmContactCell) -> depositeConfirmContactCell {
        cell.tfConfirmNumber.placeholder = "Deposit Confirm Number".localized
        if model.confirmContactNumber == "" && model.depositCountryCode == "+95"{
            model.confirmContactNumber = "09"
            cell.btnClose.isHidden = true
        }
        cell.lblCountry.text = "(" + model.depositCountryCode + ")"
        cell.imgCountryFlag.image = UIImage(named: model.depositFlag)
        cell.tfConfirmNumber.text = model.confirmContactNumber
        cell.tfConfirmNumber.delegate = self
        cell.tfConfirmNumber.tag = 200
        cell.tfConfirmNumber.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        cell.btnClose.addTarget(self, action: #selector(onClickContactNumberClose(_:)), for: .touchUpInside)
        cell.btnClose.tag = 200
        return cell
    }
    private func withdrawweContactMobileConfirmNumberCell(cell : WithdrawConfirmContactCell) -> WithdrawConfirmContactCell {
        cell.tfConfirmNumber.placeholder = "Withdrawal Confirm Number".localized
        if model.confirmContactNumberWithDrawer == "" {
            cell.btnClose.isHidden = true
        }
        if  model.confirmContactNumberWithDrawer == "" && model.withdrawCountryCode == "+95" {
            model.confirmContactNumberWithDrawer = "09"
            cell.btnClose.isHidden = true
        }
        cell.lblCountry.text = "(" + model.withdrawCountryCode + ")"
        cell.imgCountryFlag.image = UIImage(named: model.withdrawFlag)
        cell.tfConfirmNumber.text = model.confirmContactNumberWithDrawer
        cell.tfConfirmNumber.delegate = self
        cell.tfConfirmNumber.tag = 2000
        cell.tfConfirmNumber.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        cell.btnClose.addTarget(self, action: #selector(onClickContactNumberClose(_:)), for: .touchUpInside)
        cell.btnClose.tag = 2000
        return cell
    }
    private func nameCell(cell : depositeNameCell) -> depositeNameCell {
        cell.btnMale.addTarget(self, action: #selector(onClickUserNameMale), for: .touchUpInside)
        cell.btnFemale.addTarget(self, action: #selector(onClickUserNameFemale), for: .touchUpInside)
        
        cell.btnU.addTarget(self, action: #selector(onClickUserNameMaleEn), for: .touchUpInside)
        cell.btnMg.addTarget(self, action: #selector(onClickUserNameMaleEn), for: .touchUpInside)
        cell.btnMr.addTarget(self, action: #selector(onClickUserNameMaleEn), for: .touchUpInside)
        cell.btnDr.addTarget(self, action: #selector(onClickUserNameMaleEn), for: .touchUpInside)
        
        cell.btnDaw.addTarget(self, action: #selector(onClickUserNameFemaleEn), for: .touchUpInside)
        cell.btnMa.addTarget(self, action: #selector(onClickUserNameFemaleEn), for: .touchUpInside)
        cell.btnMs.addTarget(self, action: #selector(onClickUserNameFemaleEn), for: .touchUpInside)
        cell.btnMrs.addTarget(self, action: #selector(onClickUserNameFemaleEn), for: .touchUpInside)
        cell.btnMDr.addTarget(self, action: #selector(onClickUserNameFemaleEn), for: .touchUpInside)
        
        cell.btnFU.addTarget(self, action: #selector(onClickUserNameMaleFemaleMy), for: .touchUpInside)
        cell.btnFDr.addTarget(self, action: #selector(onClickUserNameMaleFemaleMy), for: .touchUpInside)
        cell.btnFMr.addTarget(self, action: #selector(onClickUserNameMaleFemaleMy), for: .touchUpInside)
        
        cell.btnclose.addTarget(self, action: #selector(onClickUserNameClose(_:)), for: .touchUpInside)
        cell.btnclose.tag = 300
        cell.tfUserName.tag = 300
        cell.tfUserName.font = UIFont(name: "Zawgyi-One", size: appFontSize)
      
        if model.name != "" {
            if model.name.contains(find: ",") {
               let nameArr = model.name.components(separatedBy: ",")
                if btnCheckBox.isSelected || isdepositorNumberOKDollreResitered {
                 cell.lbltitle.text = nameArr[0] + ","
                }else {
                cell.lbltitle.text = nameArr[0] + " "
                }
                cell.tfUserName.text = nameArr[1]
            }else {
                cell.lbltitle.text = ""
                cell.tfUserName.text = model.name
            }
            cell.btnclose.isHidden = false
        }else {
            cell.lbltitle.text = ""
            cell.tfUserName.text = model.name
            cell.btnclose.isHidden = true
            //cell.isUserInteractionEnabled = true
        }
        cell.tfUserName.placeholder = depositorPlaceholder
        cell.tfUserName.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        cell.tfUserName.delegate = self
        cell.viewMaleFemale.isHidden = true
        cell.viewMaleEn.isHidden = true
        cell.viewFemaleEn.isHidden = true
        cell.viewMaleFemaleMy.isHidden = true
        
        if btnCheckBox.isSelected {
            if UserModel.shared.gender == "1" {
            cell.imgUser.image = UIImage(named: "r_user")
            } else {
            cell.imgUser.image = UIImage(named: "r_female")
            }
            cell.isUserInteractionEnabled = false
            cell.btnclose.isHidden = true
        }else {
            cell.isUserInteractionEnabled = true
        }
        return cell
    }
    
    private func withdrawerNameCell(cell : withdrawNameCell) -> withdrawNameCell {
        cell.btnMale.addTarget(self, action: #selector(onClickUserNameMaleWithdraw), for: .touchUpInside)
        cell.btnFemale.addTarget(self, action: #selector(onClickUserNameFemaleWithdraw), for: .touchUpInside)
        
        cell.btnU.addTarget(self, action: #selector(onClickUserNameMaleEnWithdraw), for: .touchUpInside)
        cell.btnMg.addTarget(self, action: #selector(onClickUserNameMaleEnWithdraw), for: .touchUpInside)
        cell.btnMr.addTarget(self, action: #selector(onClickUserNameMaleEnWithdraw), for: .touchUpInside)
        cell.btnDr.addTarget(self, action: #selector(onClickUserNameMaleEnWithdraw), for: .touchUpInside)
        
        cell.btnDaw.addTarget(self, action: #selector(onClickUserNameFemaleEnWithdraw), for: .touchUpInside)
        cell.btnMa.addTarget(self, action: #selector(onClickUserNameFemaleEnWithdraw), for: .touchUpInside)
        cell.btnMs.addTarget(self, action: #selector(onClickUserNameFemaleEnWithdraw), for: .touchUpInside)
        cell.btnMrs.addTarget(self, action: #selector(onClickUserNameFemaleEnWithdraw), for: .touchUpInside)
        cell.btnMDr.addTarget(self, action: #selector(onClickUserNameFemaleEnWithdraw), for: .touchUpInside)
        
        cell.btnFU.addTarget(self, action: #selector(onClickUserNameMaleFemaleMyWithdraw), for: .touchUpInside)
        cell.btnFDr.addTarget(self, action: #selector(onClickUserNameMaleFemaleMyWithdraw), for: .touchUpInside)
        cell.btnFMr.addTarget(self, action: #selector(onClickUserNameMaleFemaleMyWithdraw), for: .touchUpInside)
        
        cell.btnClose.addTarget(self, action: #selector(onClickUserNameClose(_:)), for: .touchUpInside)
        cell.btnClose.tag = 3000
        cell.tfUserName.tag = 3000
        cell.tfUserName.font = UIFont(name: "Zawgyi-One", size: appFontSize)
        if model.nameWithdrawer != "" {
            if model.nameWithdrawer.contains(find: ",") {
                let nameArr = model.nameWithdrawer.components(separatedBy: ",")
                if isWithdrawalNumberOKDollreResitered {
                    cell.lbltitle.text = nameArr[0] + ","
                }else {
                    cell.lbltitle.text = nameArr[0] + " "
                }
                cell.tfUserName.text = nameArr[1]
            }else {
                cell.lbltitle.text = ""
                cell.tfUserName.text = model.name
            }
            cell.btnClose.isHidden = false
        }else {
            cell.lbltitle.text = withdrawNameTitle
            cell.tfUserName.text = model.nameWithdrawer
            cell.btnClose.isHidden = true
            cell.isUserInteractionEnabled = true
            cell.tfUserName.isUserInteractionEnabled = true
        }
        cell.tfUserName.placeholder = withdrawalPlaceholder
        cell.tfUserName.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        cell.tfUserName.delegate = self
        if withdrawnameShowFirstTime {
            cell.viewMaleFemale.isHidden = true
            cell.viewMaleEn.isHidden = true
            cell.viewFemaleEn.isHidden = true
            cell.viewMaleFemaleMy.isHidden = true
        }
        return cell
    }
        
    private func amountCell(cell : depositeAmountCell) -> depositeAmountCell {
        cell.tfAmount.placeholder = "Enter Amount".localized
        cell.lblStatusTitle.text = depositorAmoutStatus
        let amountStr = getDigitDisplayColor(model.amount)
        cell.tfAmount.text = amountStr
        let amountVal = amountStr.components(separatedBy: ",").joined(separator: "")
        if amountVal.count < 7 {
            cell.tfAmount.textColor = UIColor.black
        } else if amountVal.count == 7 {
            cell.tfAmount.textColor = UIColor.green
        } else if amountVal.count > 7 {
            cell.tfAmount.textColor = UIColor.red
        }
        cell.tfAmount.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        if self.model.amount == "" {
            cell.btnClear.isHidden = true
        }else {
            cell.btnClear.isHidden = false
        }
 
        cell.btnClear.addTargetClosure(closure: { (_ ) in
            cell.btnClear.isHidden = true
            cell.tfAmount.text = ""
            self.model.amount = ""
            self.depositorAmoutStatus = "Deposit Amount".localized
            cell.lblStatusTitle.text = self.depositorAmoutStatus
            self.resetAgentCommission()
        })
        cell.tfAmount.tag = 400
        cell.tfAmount.delegate = self
        return cell
    }
    private func agentComCell(cell : depositeAgentCommCell) -> depositeAgentCommCell {
        cell.lblStatusTitle.text = "Commission Fees".localized
        cell.tfAmount.placeholder = "Commission Fees".localized
        
        var amountStr = ""
        if (self.isdepositorNumberOKDollreResitered || self.btnCheckBox.isSelected) && self.isWithdrawalNumberOKDollreResitered {
            amountStr = "0"
        }else if self.btnCheckBox.isSelected{
            let amt = (Int(self.model.TotalCommission) ?? 0) - (Int(self.model.AgentCashInCommission) ?? 0)
            amountStr = String(amt)
        }else if self.isWithdrawalNumberOKDollreResitered {
            let amt = (Int(self.model.TotalCommission) ?? 0) - (Int(self.model.AgentCashInCommission) ?? 0)
            amountStr = String(amt)
        }else {
             amountStr = getDigitDisplayColor(self.model.TotalCommission)
        }
       
        cell.tfAmount.text = amountStr
        let amountVal = amountStr.components(separatedBy: ",").joined(separator: "")
        if amountVal.count < 7 {
            cell.tfAmount.textColor = UIColor.black
        } else if amountVal.count == 7 {
            cell.tfAmount.textColor = UIColor.green
        } else if amountVal.count > 7 {
            cell.tfAmount.textColor = UIColor.red
        }
        
        cell.btnClear.addTargetClosure(closure: { (_ ) in
            self.view.endEditing(true)
            cell.btnClear.isSelected = !cell.btnClear.isSelected
            if cell.btnClear.isSelected {
                if !self.view.contains(self.viewComm) {
                    self.viewComm.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                    // Unregistered to Unregistered
                    if self.model.AgentCashInCommission == "0" {
                        self.lblAgentCashInComAmount.text = ": " +  self.model.AgentCashInCommission
                    }else {
                        self.lblAgentCashInComAmount.attributedText = self.concatinateAmountWithMMK(amount: self.model.AgentCashInCommission)
                    }
                    
                    if self.model.AgentWithDrawCommission == "0" {
                        self.lblAgentWithdrawComAmount.text = ": " + self.model.AgentWithDrawCommission
                    }else {
                        self.lblAgentWithdrawComAmount.attributedText = self.concatinateAmountWithMMK(amount: self.model.AgentWithDrawCommission)
                    }
                    
                    if self.model.OkDollarCommission == "0" {
                        self.lblOKDollarAmount.text = ": " + self.model.OkDollarCommission
                    }else {
                        self.lblOKDollarAmount.attributedText = self.concatinateAmountWithMMK(amount: self.model.OkDollarCommission)
                    }
                    
                    if self.model.TotalCommission == "0" {
                        self.lblTototalAmount.text = ": " + self.model.TotalCommission
                    }else {
                        self.lblTototalAmount.attributedText = self.concatinateAmountWithMMK(amount: self.model.TotalCommission)
                    }

                    if (self.isdepositorNumberOKDollreResitered || self.btnCheckBox.isSelected) && self.isWithdrawalNumberOKDollreResitered {
                        self.viewForRegitoRegiNo.isHidden = false
                        self.viewForUNtoUN.isHidden = true
                        self.viewForMyNoToUN.isHidden = true
                    }else if self.btnCheckBox.isSelected {
                        self.viewForRegitoRegiNo.isHidden = true
                        self.viewForUNtoUN.isHidden = true
                        self.viewForMyNoToUN.isHidden = false
                       
                        self.lblAgentWtihdrawMyNo.text = "Agent Withdraw Commission".localized
                        self.lblAgentWithdrawInputMyNo.attributedText = self.concatinateAmountWithMMK(amount:self.model.AgentWithDrawCommission)
                        self.lblOKdollarInputMYNo.attributedText = self.concatinateAmountWithMMK(amount: self.model.OkDollarCommission)
                        let amt = (Int(self.model.TotalCommission) ?? 0) - (Int(self.model.AgentCashInCommission) ?? 0)
                        self.lblTotalAmtInputMyno.attributedText = self.concatinateAmountWithMMK(amount: String(amt))
                    }else if self.isWithdrawalNumberOKDollreResitered {
                        self.viewForRegitoRegiNo.isHidden = true
                        self.viewForUNtoUN.isHidden = true
                        self.viewForMyNoToUN.isHidden = false
                        
                        self.lblAgentWtihdrawMyNo.text = "Agent Deposit Commission".localized
                        self.lblAgentWithdrawInputMyNo.attributedText = self.concatinateAmountWithMMK(amount:self.model.AgentWithDrawCommission)
                        self.lblOKdollarInputMYNo.attributedText = self.concatinateAmountWithMMK(amount: self.model.OkDollarCommission)
                        let amt = (Int(self.model.TotalCommission) ?? 0) - (Int(self.model.AgentCashInCommission) ?? 0)
                        self.lblTotalAmtInputMyno.attributedText = self.concatinateAmountWithMMK(amount: String(amt))
                    }else {
                        self.viewForRegitoRegiNo.isHidden = true
                        self.viewForUNtoUN.isHidden = false
                        self.viewForMyNoToUN.isHidden = true
                    }
                    
                    self.view.bringSubviewToFront(self.viewComm)
                    self.view.addSubview(self.viewComm)
                }
            }
        })
        return cell
    }
    
    
    func nrcCell(cell : depositeNRCCell) -> depositeNRCCell {
       cell.btnNRCClose.addTargetClosure(closure: { _ in
        cell.viewNRCButton.isHidden = false
        cell.viewNRCTF.isHidden = true
        cell.tfNRC.leftView = nil
        cell.btnNRC.setTitle("Enter NRC Details".localized, for: .normal)
        cell.btnnrc.setTitle("", for: .normal)
        cell.tfNRC.text = ""
        self.model.nrc = ""
        cell.tfNRC.resignFirstResponder()
        })
        cell.tfNRC.tag = 500
        cell.tfNRC.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        cell.tfNRC.delegate = self
        cell.tfNRC.myDelegate = self
        cell.btnNRC.tag = 500
        if model.nrc != "" {
            if model.nrc.contains(find: ")") {
                let nrc = model.nrc.components(separatedBy: ")")
                if let pre = nrc[0] as? String {
                    model.nrcPrefix = pre + ")"
                    cell.viewNRCButton.isHidden = true
                    cell.viewNRCTF.isHidden = false
                    let myString = model.nrcPrefix
                    let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
                    cell.btnnrc.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
                    cell.btnnrc.frame = CGRect(x: 0, y: -2, width:stringSize.width, height: 52)
                    cell.btnnrc.setTitleColor(.black, for: .normal)
                    cell.btnnrc.titleLabel?.textAlignment = .left
                    cell.btnnrc.contentHorizontalAlignment = .left
                    cell.btnnrc.addTarget(self, action: #selector(self.BtnNrcTypeAction(button:)), for: .touchUpInside)
                    cell.btnnrc.setTitle(model.nrcPrefix, for: UIControl.State.normal)
                    cell.btnnrc.tag = 500
                    let btnView = UIView()
                    btnView.frame = CGRect(x: 0, y: 0, width: cell.btnnrc.frame.width , height: 52)
                    btnView.addSubview(cell.btnnrc)
                    cell.tfNRC.leftView = btnView
                    cell.tfNRC.leftViewMode = UITextField.ViewMode.always
                }else {
                    cell.btnNRC.setTitle("Enter NRC Details".localized, for: .normal)
                }
                if let post = nrc[1] as? String {
                    model.nrcPostfix = post
                    cell.tfNRC.text = model.nrcPostfix
                }else {
                    cell.tfNRC.text = ""
                }
            }
        }else {
            if model.nrcPrefix == "" {
                cell.btnNRC.setTitle("Enter NRC Details".localized, for: .normal)
            }else {
                cell.btnNRC.setTitle(model.nrcPrefix, for: .normal)
            }
            if model.nrcPostfix == "" {
                cell.tfNRC.text = ""
            }else {
                cell.tfNRC.text = model.nrcPostfix
            }
        }
        
        cell.btnNRC.addTargetClosure(closure: { _ in
            if cell.btnNRC.titleLabel?.text == "Enter NRC Details".localized {
                self.showStateDivision()
                self.nrcFrom = "Depositor"
            }else {
                self.showTownshipVC()
                self.nrcFrom = "Depositor"
            }
        })
        
        if btnCheckBox.isSelected {
            cell.tfNRC.isUserInteractionEnabled = false
        }else {
            cell.tfNRC.isUserInteractionEnabled = true
        }
        self.view.endEditing(true)
        return cell
    }
    func nrcCellWithdraw(cell : WithdrawNRCCell) -> WithdrawNRCCell {
        cell.btnNRCClose.addTargetClosure(closure: { _ in
            cell.viewNRCButton.isHidden = false
            cell.viewNRCTF.isHidden = true
            cell.tfNRC.leftView = nil
            cell.btnNRC.setTitle("Enter NRC Details".localized, for: .normal)
            cell.btnnrc.setTitle("", for: .normal)
            cell.tfNRC.text = ""
            self.model.nrcWithraw = ""
            cell.tfNRC.resignFirstResponder()
        })
        cell.tfNRC.tag = 5000
        cell.tfNRC.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        cell.tfNRC.delegate = self
        cell.tfNRC.myDelegate = self
        cell.btnNRC.tag = 5000
        if model.nrcWithraw != "" {
            if model.nrcWithraw.contains(find: ")") {
                let nrc = model.nrcWithraw.components(separatedBy: ")")
                if let pre = nrc[0] as? String {
                    model.nrcPrefixWithdraw = pre + ")"
                    cell.viewNRCButton.isHidden = true
                    cell.viewNRCTF.isHidden = false
                    let myString = model.nrcPrefixWithdraw
                    let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
                    cell.btnnrc.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
                    cell.btnnrc.frame = CGRect(x: 0, y: -2, width:stringSize.width, height: 52)
                    cell.btnnrc.setTitleColor(.black, for: .normal)
                    cell.btnnrc.titleLabel?.textAlignment = .left
                    cell.btnnrc.contentHorizontalAlignment = .left
                    cell.btnnrc.addTarget(self, action: #selector(self.BtnNrcTypeAction(button:)), for: .touchUpInside)
                    cell.btnnrc.setTitle(model.nrcPrefixWithdraw, for: UIControl.State.normal)
                    cell.btnnrc.tag = 5000
                    let btnView = UIView()
                    btnView.frame = CGRect(x: 0, y: 0, width: cell.btnnrc.frame.width, height: 52)
                    btnView.addSubview(cell.btnnrc)
                    cell.tfNRC.leftView = btnView
                    cell.tfNRC.leftViewMode = UITextField.ViewMode.always
                }else {
                    cell.btnNRC.setTitle("Enter NRC Details".localized, for: .normal)
                }
                if let post = nrc[1] as? String {
                    model.nrcPostfixWithdraw = post
                    cell.tfNRC.text = model.nrcPostfixWithdraw
                }else {
                    cell.tfNRC.text = ""
                }
            }
        }else {
            if model.nrcPrefixWithdraw == "" {
                cell.btnNRC.setTitle("Enter NRC Details".localized, for: .normal)
            }else {
                cell.btnNRC.setTitle(model.nrcPrefixWithdraw, for: .normal)
            }
            if model.nrcPostfixWithdraw == "" {
                cell.tfNRC.text = ""
            }else {
                cell.tfNRC.text = model.nrcPostfixWithdraw
            }
        }
        
        cell.btnNRC.addTargetClosure(closure: {_ in
            if cell.btnNRC.titleLabel?.text == "Enter NRC Details".localized {
                self.showStateDivision()
                self.nrcFrom = "WithDraw"
            }else {
                self.showTownshipVC()
                self.nrcFrom = "WithDraw"
            }
        })
        self.view.endEditing(true)
        return cell
    }
    
    
    
    func depositFacePhotoCell(cell : DepositFacePhotoCell) -> DepositFacePhotoCell {
        cell.lblTitle.text = "Attach Depositor Face Photo".localized
        let tap = UITapGestureRecognizer(target: self, action: #selector(openGallaryForFace(_ :)))
        cell.lblTitle.isUserInteractionEnabled = true
        cell.lblRequired.isHidden = true
        tap.name = "deposit"
        cell.lblTitle.addGestureRecognizer(tap)
        return cell
    }
    
    func withdrawFacePhotoCell(cell : withDrawalFacePhotoCell) -> withDrawalFacePhotoCell {
        cell.lblTitlte.text = "Attach Withdrawal Face Photo".localized
        let tap = UITapGestureRecognizer(target: self, action: #selector(openGallaryForFace(_ :)))
        cell.imgPhoto.image = withdrawalImage.image
        tap.name = "withdraw"
        cell.lblTitlte.isUserInteractionEnabled = true
        cell.lblTitlte.addGestureRecognizer(tap)
        return cell
    }
    
    func DivisionCell(cell: DivisionCellWithdraw) -> DivisionCellWithdraw {
        cell.lblStatus.text = "State / Division".localized
        if model.division == "" {
            cell.lblDivision.text = "Select State / Division".localized
        }else {
            cell.lblDivision.text = model.division
        }
        cell.lblDivision.isUserInteractionEnabled = true
        cell.imgRightArrow.isUserInteractionEnabled = true
        let tapAction = UITapGestureRecognizer(target: self, action:#selector(onClickAction(_:)))
        tapAction.name = "Division"
        let tapAction1 = UITapGestureRecognizer(target: self, action:#selector(onClickAction(_:)))
          tapAction1.name = "Division"
        cell.lblDivision.addGestureRecognizer(tapAction)
        cell.imgRightArrow.addGestureRecognizer(tapAction1)
        return cell
    }
    
    func ShowAddressFacePayCell(cell : ShowAddressFacePay) -> ShowAddressFacePay {
        cell.btnAddressShow.isSelected = isBtnFullAddress
        
        let tapAction = UITapGestureRecognizer(target: self, action:#selector(onClickAddressLbl(_:)))
        cell.lblAddressShow.isUserInteractionEnabled = true
        cell.lblAddressShow.addGestureRecognizer(tapAction)
        cell.btnAddressShow.addTarget(self, action: #selector(onClickAddressShowAction(_:)), for: .touchUpInside)
        return cell
    }
    
    func TownShipCell(cell: TownshipCellWithdraw) -> TownshipCellWithdraw {
        cell.lblStatus.text = "Township".localized
        if model.township == "" {
            cell.lblTownship.text = "Select Township".localized
        }else {
            cell.lblTownship.text = model.township
        }
        cell.lblTownship.isUserInteractionEnabled = true
        cell.imgRightArrow.isUserInteractionEnabled = true
        let tapAction = UITapGestureRecognizer(target: self, action:#selector(onClickAction(_:)))
        tapAction.name = "Township"
        let tapAction1 = UITapGestureRecognizer(target: self, action:#selector(onClickAction(_:)))
        tapAction1.name = "Township"
        cell.lblTownship.addGestureRecognizer(tapAction)
        cell.imgRightArrow.addGestureRecognizer(tapAction1)
        return cell
    }
    func CityCell(cell: CityCellWithdraw) -> CityCellWithdraw {
        cell.lblStatus.text = "City".localized
        if model.city == "" {
            cell.lblcity.text = "City/Region".localized
        }else {
            cell.lblcity.text = model.city
        }
        cell.lblcity.isUserInteractionEnabled = false
        return cell
    }
    func VillageCell(cell: VillageCell) -> VillageCell {
        cell.lblStatus.text = "Village Tract".localized
        cell.tfVilage.text = model.village
        cell.tfVilage.delegate = self
        cell.tfVilage.tag = 204
        cell.tfVilage.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        cell.tfVilage.delegate = self
        return cell
    }
    func StreetCell(cell: StreetVillageCell) -> StreetVillageCell {
        cell.lblStatus.text = "Street".localized
        cell.txtStreet.text = model.street
        cell.txtStreet.tag = 205
        cell.txtStreet.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        cell.txtStreet.delegate = self
        return cell
    }
    func HouseNumber(cell: HouseRoomFloorNumberCell) -> HouseRoomFloorNumberCell {
        cell.tfHouse.text = model.houseNumber
        cell.tfFloor.text = model.floorNumber
        cell.tfRoom.text = model.roomNumber
        cell.tfHouse.delegate = self
         cell.tfFloor.delegate = self
         cell.tfRoom.delegate = self
        return cell
    }
    
        
    private func createRequiredFieldLabel(headerTitle:String, txtAlignment: NSTextAlignment, gcRect: CGRect, section: Int) ->UIView? {
        let view1 = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: gcRect.height))
        view1.backgroundColor = UIColor.init(hex: "#EBEBEB")
        let myString = headerTitle
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
        
        let outerView = UIView(frame: CGRect(x: 0, y: 5, width: self.tbDeposite.frame.width, height: 55))
        outerView.layer.cornerRadius = 5
        outerView.layer.masksToBounds = true
        outerView.layer.borderColor = UIColor.lightGray.cgColor
        outerView.layer.borderWidth = 1.0
        outerView.backgroundColor = UIColor.clear
        
        btnCheckBox.frame = CGRect(x: 15, y: 15, width: 25, height: 25)
        btnCheckBox.setBackgroundImage(UIImage(named: "checkboxTick"), for: .selected)
        btnCheckBox.setBackgroundImage(UIImage(named: "check_box_Blue"), for: .normal)
        btnCheckBox.addTarget(self, action: #selector(onClickCheckBoxAction(_:)), for: .touchUpInside)
        
        let lblFacePay1 = UILabel(frame: CGRect(x: 45, y: 10, width: self.view.frame.width - 45, height: 35))
        let (mobileNumber,CountryCode) = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: UserModel.shared.mobileNo)
        lblFacePay1.text = " " + "Use My Account".localized + " " + "(" + CountryCode + ")" + mobileNumber
        lblFacePay1.numberOfLines = 2
        lblFacePay1.font = UIFont(name: appFont, size: 15)
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapLabel(_:)))
        lblFacePay1.addGestureRecognizer(tap)
        lblFacePay1.isUserInteractionEnabled = true
        
        let outerView2 = UIView(frame: CGRect(x: 0, y: 75, width: self.tbDeposite.frame.width, height: 140))
        outerView2.layer.cornerRadius = 5
        outerView2.layer.masksToBounds = true
        outerView2.layer.borderColor = UIColor.lightGray.cgColor
        outerView2.layer.borderWidth = 1.0
        outerView2.backgroundColor = UIColor.clear
        
        let lblCom = UILabel(frame: CGRect(x: 15, y: 5, width: outerView2.frame.width - 20, height: 40))
        lblCom.text = "Deduct Commission From".localized
        lblCom.textAlignment = .left
        lblCom.font = UIFont(name: appFont, size: 16)
        lblCom.backgroundColor = .clear
        lblCom.textColor = UIColor.darkGray
        outerView2.addSubview(lblCom)
        
        btnDepositCom.frame = CGRect(x: 15, y: 50, width: 25, height: 25)
        btnDepositCom.setBackgroundImage(UIImage(named: "select_radio"), for: .selected)
        btnDepositCom.setBackgroundImage(UIImage(named: "r_Unradio"), for: .normal)
        btnDepositCom.tag = 100
        btnDepositCom.addTarget(self, action: #selector(onClickDepositCommission(_:)), for: .touchUpInside)
        outerView2.addSubview(btnDepositCom)
        
        let lblDeposit = UILabel(frame: CGRect(x: 45, y: 43, width: outerView2.frame.width - 45, height: 40))
        lblDeposit.text = "  " + "Depositor".localized
        lblDeposit.textAlignment = .left
        lblDeposit.font = UIFont(name: appFont, size: 16)
        lblDeposit.backgroundColor = .clear
        lblDeposit.textColor = UIColor.black
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(tapLabelCommissionDeposit(_:)))
        lblDeposit.addGestureRecognizer(tap2)
        lblDeposit.isUserInteractionEnabled = true
        outerView2.addSubview(lblDeposit)
        
        btnWithdrawCom.frame = CGRect(x: 15, y: 95, width: 25, height: 25)
        btnWithdrawCom.setBackgroundImage(UIImage(named: "select_radio"), for: .selected)
        btnWithdrawCom.setBackgroundImage(UIImage(named: "r_Unradio"), for: .normal)
        btnWithdrawCom.tag = 200
        btnWithdrawCom.addTarget(self, action: #selector(onClickDepositCommission(_:)), for: .touchUpInside)
        outerView2.addSubview(btnWithdrawCom)
        
        let lblWithdraw = UILabel(frame: CGRect(x: 45, y: 88, width: outerView2.frame.width - 45, height: 40))
        lblWithdraw.text = "  " + "WithdrawalR".localized
        lblWithdraw.textAlignment = .left
        lblWithdraw.font = UIFont(name: appFont, size: 16)
        lblWithdraw.backgroundColor = .clear
        lblWithdraw.textColor = UIColor.black
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(tapLabelCommissionWithdraw(_:)))
        lblWithdraw.addGestureRecognizer(tap3)
        lblWithdraw.isUserInteractionEnabled = true
        outerView2.addSubview(lblWithdraw)
        
        let lblHeader = UILabel(frame: CGRect(x: (self.view.frame.width / 2 ) - (stringSize.width / 2) , y: 220, width: stringSize.width, height: 40))
        lblHeader.text = headerTitle
        lblHeader.numberOfLines = 3
        lblHeader.textAlignment = txtAlignment
        lblHeader.font = UIFont(name: appFont, size: 14)
        lblHeader.backgroundColor = UIColor.init(hex: "#1C2C99")
        lblHeader.textColor = UIColor.white
        lblHeader.layer.masksToBounds = true
        lblHeader.layer.cornerRadius = lblHeader.frame.height / 2

        
        outerView.addSubview(btnCheckBox)
        outerView.addSubview(lblFacePay1)
        view1.addSubview(outerView)
        view1.addSubview(outerView2)
        view1.addSubview(lblHeader)
        return view1
    }
    
    private func createRequiredFieldLabelCheckBox(headerTitle:String, txtAlignment: NSTextAlignment, gcRect: CGRect, section: Int) ->UIView? {
        let view1 = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: gcRect.height))
        view1.backgroundColor = UIColor.init(hex: "#EBEBEB")
        let myString = headerTitle
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
        let lblHeader = UILabel(frame: CGRect(x: (self.view.frame.width / 2 ) - (stringSize.width / 2) , y: 10, width: stringSize.width, height: 40))
        lblHeader.text = headerTitle
        lblHeader.numberOfLines = 3
        lblHeader.textAlignment = txtAlignment
        lblHeader.font = UIFont(name: appFont, size: 14)
        lblHeader.backgroundColor = UIColor.init(hex: "#C71C00")
        
        lblHeader.textColor = UIColor.white//init(hex: "#1C2C99")
        lblHeader.layer.masksToBounds = true
        lblHeader.layer.cornerRadius = lblHeader.frame.height / 2
        
        let outerView = UIView(frame: CGRect(x: 0, y: 60, width: self.tbDeposite.frame.width, height: 85))
        outerView.layer.cornerRadius = 5
        outerView.layer.masksToBounds = true
        outerView.layer.borderColor = UIColor.lightGray.cgColor
        outerView.layer.borderWidth = 1.0
        outerView.backgroundColor = UIColor.clear
        
        btnFacePayWithPhoto.frame = CGRect(x: 15, y: 10, width: 25, height: 25)
        btnFacePayWithoutPhoto.frame = CGRect(x: 15, y: 50, width: 25, height: 25)
        
        let lblFacePay = UILabel(frame: CGRect(x: 55, y: 5, width: self.view.frame.width - 60, height: 35))
        lblFacePay.text = "To Transfer money with photo".localized
        lblFacePay.numberOfLines = 2
        lblFacePay.font = UIFont(name: appFont, size: 15)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapLabelWithPhoto(_:)))
        lblFacePay.addGestureRecognizer(tap)
        lblFacePay.isUserInteractionEnabled = true
        
        btnFacePayWithPhoto.setBackgroundImage(UIImage(named: "select_radio"), for: .selected)
        btnFacePayWithPhoto.setBackgroundImage(UIImage(named: "r_Unradio"), for: .normal)
        btnFacePayWithPhoto.addTarget(self, action: #selector(onClickFacePay(_:)), for: .touchUpInside)
        
        let lblFacePay1 = UILabel(frame: CGRect(x: 55, y: 45, width: self.view.frame.width - 60, height: 35))
        lblFacePay1.text = "To Transfer money without photo".localized
        lblFacePay1.numberOfLines = 2
        lblFacePay1.font = UIFont(name: appFont, size: 15)
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(tapLabelWithOutPhoto(_:)))
        lblFacePay1.addGestureRecognizer(tap1)
        lblFacePay1.isUserInteractionEnabled = true
        
        btnFacePayWithoutPhoto.setBackgroundImage(UIImage(named: "select_radio"), for: .selected)
        btnFacePayWithoutPhoto.setBackgroundImage(UIImage(named: "r_Unradio"), for: .normal)
        btnFacePayWithoutPhoto.addTarget(self, action: #selector(onClickFacePay1(_ :)), for: .touchUpInside)
        
        outerView.addSubview(btnFacePayWithPhoto)
        outerView.addSubview(btnFacePayWithoutPhoto)
        outerView.addSubview(lblFacePay)
        outerView.addSubview(lblFacePay1)
        
        view1.addSubview(outerView)
        view1.addSubview(lblHeader)
        return view1
    }
    
    private func createRequiredFieldLabelNormal(headerTitle:String, reqTitle: String) ->UIView? {
        let view1 = UIView(frame: CGRect(x: 5, y: 0, width:self.view.frame.width - 10, height: 0))
        view1.backgroundColor = hexStringToUIColor(hex: "#EBEBEB")
        let lblHeader = UILabel(frame: CGRect(x: 10, y: 0, width: (sizeOfString(myString: headerTitle)?.width)!, height:10))
        lblHeader.text = headerTitle
        lblHeader.textAlignment = .left
        lblHeader.font = UIFont(name: appFont, size: 14)
        lblHeader.backgroundColor = UIColor.clear
        lblHeader.textColor = UIColor.init(hex: "#C71C00")
        
        let reqLabel = UILabel(frame: CGRect(x: self.view.frame.width - (sizeOfString(myString: reqTitle)?.width)! - 10, y: 0, width: (sizeOfString(myString: reqTitle)?.width)!, height:0))
        reqLabel.text = reqTitle
        reqLabel.textAlignment = .right
        reqLabel.font = UIFont(name: appFont, size: 14)
        reqLabel.backgroundColor = UIColor.clear
        reqLabel.textColor = UIColor.red
        view1.addSubview(lblHeader)
        view1.addSubview(reqLabel)
        return view1
    }
    
    @objc func onClickFacePay(_ sender: UIButton) {
        if btnFacePayWithPhoto.isSelected {
            return
        }
        
        btnFacePayWithPhoto.isSelected = true
        btnFacePayWithoutPhoto.isSelected = false
        
        if !isWithdrawalFacePhotoHidden {
            if let cell = tbDeposite.cellForRow(at: IndexPath(row: 3, section: 1)) as? withDrawalFacePhotoCell {
                cell.imgPhoto.image = nil
            }
        }
        let btn = UIButton()
        btn.tag = 1000
        self.onClickContactNumberClose(btn)  
    }
    
    @objc func onClickFacePay1(_ sender : UIButton) {
        if btnFacePayWithoutPhoto.isSelected {
            return
        }
        btnFacePayWithoutPhoto.isSelected = true
        btnFacePayWithPhoto.isSelected = false
        if !isWithdrawalFacePhotoHidden {
            if let cell = tbDeposite.cellForRow(at: IndexPath(row: 3, section: 1)) as? withDrawalFacePhotoCell {
                cell.imgPhoto.image = nil
            }
        }
        let btn = UIButton()
        btn.tag = 1000
        self.onClickContactNumberClose(btn)
    }
    
    private func sizeOfString(myString : String) ->CGSize? {
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: appFont, size: 16) ?? UIFont.systemFont(ofSize: 16.0) ])
        return stringSize
    }
        
    //deposit
        @objc func onClickUserNameMale(_ sender: UIButton) {
            if !depositMaleFemaleTag {
                depositMaleFemaleTag = true
                let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 0)) as? depositeNameCell
                cell?.viewMaleFemale.isHidden = true
                cell?.imgUser.image = UIImage(named: "r_user")
                cell?.imgUser.isHidden = false
                
                if ok_default_language == "my" || ok_default_language == "uni"{
                    cell?.btnFU.setTitle("U".localized, for: .normal)
                    cell?.btnFMr.setTitle("Mg".localized, for: .normal)
                    cell?.viewMaleFemaleMy.isHidden = false
                }else {
                    cell?.viewMaleEn.isHidden = false
                }
            }
        }
        
        @objc func onClickUserNameFemale(_ sender: UIButton) {
            if !depositMaleFemaleTag {
                depositMaleFemaleTag = true
                let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 0)) as? depositeNameCell
                cell?.viewMaleFemale.isHidden = true
                cell?.imgUser.image = UIImage(named: "r_female")
                cell?.imgUser.isHidden = false
                
                if ok_default_language == "my" || ok_default_language == "uni"{
                    cell?.btnFU.setTitle("Daw".localized, for: .normal)
                    cell?.btnFMr.setTitle("Ma".localized, for: .normal)
                    cell?.viewMaleFemaleMy.isHidden = false
                }else {
                    cell?.viewFemaleEn.isHidden = false
                }
            }
      
        }
        
        
        @objc func onClickUserNameMaleEn(_ sender: UIButton) {
            let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 0)) as? depositeNameCell
            cell?.viewMaleEn.isHidden = true
            cell?.viewMaleFemale.isHidden = true
            isYourNameKeyboardHidden = false
            cell?.btnclose.isHidden = false
            let title = sender.title(for: .normal) ?? ""
            cell?.lbltitle.text = title + " "
            depositorPlaceholder = "Enter Your Name".localized
            cell?.tfUserName.placeholder = depositorPlaceholder
            cell?.tfUserName.becomeFirstResponder()
        }
        
        @objc func onClickUserNameFemaleEn(_ sender: UIButton) {
            let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 0)) as? depositeNameCell
            cell?.viewFemaleEn.isHidden = true
            cell?.viewMaleFemale.isHidden = true
            isYourNameKeyboardHidden = false
            cell?.btnclose.isHidden = false
            let title = sender.title(for: .normal) ?? ""
            cell?.lbltitle.text = title + " "
            depositorPlaceholder = "Enter Your Name".localized
            cell?.tfUserName.placeholder = depositorPlaceholder
            cell?.tfUserName.becomeFirstResponder()
        }
        
        @objc func onClickUserNameMaleFemaleMy(_ sender: UIButton) {
            let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 0)) as? depositeNameCell
            cell?.viewFemaleEn.isHidden = true
            cell?.viewMaleFemale.isHidden = true
            cell?.viewMaleFemaleMy.isHidden = true
            isYourNameKeyboardHidden = false
            cell?.btnclose.isHidden = false
            cell?.btnclose.isHidden = false
            let title = sender.title(for: .normal) ?? ""
            cell?.lbltitle.text = title + " "
            depositorPlaceholder = "Enter Your Name".localized
            cell?.tfUserName.placeholder = depositorPlaceholder
            cell?.tfUserName.becomeFirstResponder()
        }
    
    @objc func onClickUserNameMaleWithdraw(_ sender: UIButton) {
        if !wtihdrawMaleFemaleTag {
            wtihdrawMaleFemaleTag = true
            let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 1)) as? withdrawNameCell
            cell?.viewMaleFemale.isHidden = true
            cell?.imgUser.image = UIImage(named: "r_user")
            cell?.imgUser.isHidden = false
            
            if ok_default_language == "my" || ok_default_language == "uni"{
                cell?.btnFU.setTitle("U".localized, for: .normal)
                cell?.btnFMr.setTitle("Mg".localized, for: .normal)
                cell?.viewMaleFemaleMy.isHidden = false
            }else {
                cell?.viewMaleEn.isHidden = false
            }
        }
        
  
    }
    
    @objc func onClickUserNameFemaleWithdraw(_ sender: UIButton) {
        if !wtihdrawMaleFemaleTag {
            wtihdrawMaleFemaleTag = true
            let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 1)) as? withdrawNameCell
            cell?.viewMaleFemale.isHidden = true
            cell?.imgUser.image = UIImage(named: "r_female")
            cell?.imgUser.isHidden = false
            
            if ok_default_language == "my" || ok_default_language == "uni"{
                cell?.btnFU.setTitle("Daw".localized, for: .normal)
                cell?.btnFMr.setTitle("Ma".localized, for: .normal)
                cell?.viewMaleFemaleMy.isHidden = false
            }else {
                cell?.viewFemaleEn.isHidden = false
            }
        }
  
    }
    
    
    @objc func onClickUserNameMaleEnWithdraw(_ sender: UIButton) {
        let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 1)) as? withdrawNameCell
        withdrawnameShowFirstTime = false
        cell?.viewMaleEn.isHidden = true
        cell?.viewMaleFemale.isHidden = true
        isYourNameWithdrawKeyboardHidden = false
        cell?.btnClose.isHidden = false
        let title = sender.title(for: .normal) ?? ""
        cell?.lbltitle.text = title + " "
        withdrawNameTitle = title + " "
        withdrawalPlaceholder = "Enter Withdrawal Name".localized
        cell?.tfUserName.placeholder = withdrawalPlaceholder
        cell?.tfUserName.becomeFirstResponder()
    }
    
    @objc func onClickUserNameFemaleEnWithdraw(_ sender: UIButton) {
        let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 1)) as? withdrawNameCell
        withdrawnameShowFirstTime = false
        cell?.viewFemaleEn.isHidden = true
        cell?.viewMaleFemale.isHidden = true
        isYourNameWithdrawKeyboardHidden = false
        cell?.btnClose.isHidden = false
        let title = sender.title(for: .normal) ?? ""
        cell?.lbltitle.text = title + " "
        withdrawNameTitle = title + " "
        withdrawalPlaceholder = "Enter Withdrawal Name".localized
        cell?.tfUserName.placeholder = withdrawalPlaceholder
        cell?.tfUserName.becomeFirstResponder()
    }
    
    @objc func onClickUserNameMaleFemaleMyWithdraw(_ sender: UIButton) {
        let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 1)) as? withdrawNameCell
        withdrawnameShowFirstTime = false
        cell?.viewFemaleEn.isHidden = true
        cell?.viewMaleFemale.isHidden = true
        cell?.viewMaleFemaleMy.isHidden = true
        isYourNameWithdrawKeyboardHidden = false
        cell?.btnClose.isHidden = false
        let title = sender.title(for: .normal) ?? ""
        cell?.lbltitle.text = title + " "
        withdrawNameTitle = title + " "
        withdrawalPlaceholder = "Enter Withdrawal Name".localized
        cell?.tfUserName.placeholder = withdrawalPlaceholder
        cell?.tfUserName.becomeFirstResponder()
    }
    
    @objc func openGallaryForFace(_ tap: UITapGestureRecognizer) {
        DispatchQueue.main.async {
            self.imageTakenFor = tap.name ?? ""
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TakeFacePhotoDeposite") as! TakeFacePhotoDeposite
            vc.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            vc.modalPresentationStyle = .overCurrentContext
            vc.screenFor = tap.name ?? ""
            vc.delegate = self
           // vc.modalPresentationStyle = .fullScreen
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    
    func facePhoto(urlString: String, realImage: UIImage, screen: String,viewController: UIViewController) {
        DispatchQueue.main.async {
            viewController.dismiss(animated: true, completion: nil)
            if screen == "deposit" {
                self.model.depositFacePhotoURL = urlString
                if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 5, section: 0)) as? DepositFacePhotoCell {
                    cell.imgPhoto.image = realImage
                }
            }else if screen == "withdraw" {
                self.model.withdrawalFacePhotoURL = urlString
                if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 3, section: 1)) as? withDrawalFacePhotoCell {
                    cell.imgPhoto.image = realImage
                    self.withdrawalImage.image = realImage
                    self.AddNRCellWithdrawal()
                    //self.InsertDivisionCellWithScroll()
                    self.scrollingBottom(ind: 3, sec: 1)
                    self.bottomcontant.constant = 0
                }
            }
        }
    }
    
    func DismissCameraController(viewController: UIViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
        func showStateDivision() {
            self.view.endEditing(true)
            let sb = UIStoryboard(name: "Registration", bundle: nil)
            let townVC = sb.instantiateViewController(withIdentifier: "TownshipVC") as! TownshipVC
            townVC.townShipDelegate = self
            self.navigationController?.pushViewController(townVC, animated: false)
        }
        
        func showTownshipVC() {
            self.view.endEditing(true)
            let sb = UIStoryboard(name: "Registration", bundle: nil)
            let townshipVC2 = sb.instantiateViewController(withIdentifier: "TownshipVC2") as! TownshipVC2
            townshipVC2.selectedDivState = locationDetails
            townshipVC2.townshipVC2Delegate = self
            self.navigationController?.pushViewController(townshipVC2, animated: true)
        }
    
    @objc func onClickAction(_ reconiger: UITapGestureRecognizer) {
        let sb = UIStoryboard.init(name: "Registration", bundle: nil)
        if reconiger.name == "Division" {
            let stateDivisionVC  = sb.instantiateViewController(withIdentifier: "StateDivisionVC") as! StateDivisionVC
            stateDivisionVC.stateDivisonVCDelegate = self
            stateDivisionVC.navigateFrom = "FACEPAY"
            self.navigationController?.pushViewController(stateDivisionVC, animated: true)
        }else if reconiger.name == "Township" {
            if locationDetailAddress != nil {
                let sb = UIStoryboard.init(name: "Registration", bundle: nil)
                let addressTownshiopVC  = sb.instantiateViewController(withIdentifier: "AddressTownshiopVC") as! AddressTownshiopVC
                addressTownshiopVC.addressTownshiopVCDelegate = self
                addressTownshiopVC.selectedDivState = locationDetailAddress
                self.navigationController?.pushViewController(addressTownshiopVC, animated: true)
            }else {
                
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "", alertBody: "Select State / Division".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
                    
                }
            }
        }
    }
    
    @objc func onClickAddressLbl(_ reconiger: UITapGestureRecognizer) {
        checkUncheckShowAddressWithdraw()
    }
    
    @objc func onClickAddressShowAction(_ sender: UIButton) {
        checkUncheckShowAddressWithdraw()
    }
    
    func checkUncheckShowAddressWithdraw() {
        if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 1, section: 2)) as? ShowAddressFacePay {
            cell.btnAddressShow.isSelected = !cell.btnAddressShow.isSelected
            if cell.btnAddressShow.isSelected {
                self.isBtnFullAddress = true
                if self.isTownshipHidden {
                    self.isTownshipHidden = false
                    self.cellListAddress.append(self.tbDeposite.dequeueReusableCell(withIdentifier: "TownshipCellWithdraw") as! TownshipCellWithdraw)
                    self.insertRowIntoTableWithScroll(ind: 2, sec: 2)
                }else {
                    self.tbDeposite.reloadRows(at: [IndexPath(row: 2, section: 2)], with: .none)
                }
                if self.isCityHidden {
                    self.isCityHidden = false
                    self.cellListAddress.append(self.tbDeposite.dequeueReusableCell(withIdentifier: "CityCellWithdraw") as! CityCellWithdraw)
                    self.insertRowIntoTableWithScroll(ind: 3, sec: 2)
                }
                if self.isVillageHidden {
                    self.isVillageHidden = false
                    self.cellListAddress.append(self.tbDeposite.dequeueReusableCell(withIdentifier: "VillageCell") as! VillageCell)
                    self.insertRowIntoTableWithScroll(ind: 4, sec: 2)
                }
                if self.isStreeetHidden {
                    self.isStreeetHidden = false
                    self.cellListAddress.append(self.tbDeposite.dequeueReusableCell(withIdentifier: "StreetVillageCell") as! StreetVillageCell)
                    self.insertRowIntoTableWithScroll(ind: 5, sec: 2)
                }
                
                if self.isHouseNumberHidden {
                    self.isHouseNumberHidden = false
                    self.cellListAddress.append(self.tbDeposite.dequeueReusableCell(withIdentifier: "HouseRoomFloorNumberCell") as! HouseRoomFloorNumberCell)
                    self.insertRowIntoTableWithScroll(ind: 6, sec: 2)
                }
                UIView.animate(withDuration: 0.5, animations: {
                    _ = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false) { timer in
                        let indexPath = IndexPath(row: 6, section: 2)
                        self.tbDeposite.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    }
                })
            }else {
                self.isBtnFullAddress = false
                self.model.townshipCode = ""
                self.model.township = ""
                self.model.city = ""
                self.model.street = ""
                self.model.village = ""
                self.model.houseNumber = ""
                self.model.roomNumber = ""
                self.model.floorNumber = ""
                
                if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 3, section: 2)) as? CityCellWithdraw {
                    cell.lblcity.text = ""
                }
                
                if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 2)) as? VillageCell {
                    cell.tfVilage.text = ""
                }
                if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 5, section: 2)) as? StreetVillageCell {
                    cell.txtStreet.text = ""
                }
                if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 6, section: 2)) as? HouseRoomFloorNumberCell {
                    cell.tfRoom.text = ""
                    cell.tfHouse.text = ""
                    cell.tfFloor.text = ""
                }
                
                let indexPath = IndexPath(row: 1, section: 2)
                self.tbDeposite.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                
                UIView.animate(withDuration: 0.5, animations: {
                    _ = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false) { timer in
                        self.tbDeposite.reloadRows(at: [IndexPath(row: 2, section: 2)], with: .none)
                        self.tbDeposite.reloadRows(at: [IndexPath(row: 3, section: 2)], with: .none)
                        self.tbDeposite.reloadRows(at: [IndexPath(row: 4, section: 2)], with: .none)
                        self.tbDeposite.reloadRows(at: [IndexPath(row: 5, section: 2)], with: .none)
                        self.tbDeposite.reloadRows(at: [IndexPath(row: 6, section: 2)], with: .none)
                    }
                })
            }
        }
    }
    
}


extension DepositeVC : StateDivisionVCDelegate,AddressTownshiopVCDelegate {
    func setDivisionStateName(SelectedDic: LocationDetail) {
        isBtnFullAddress = false
        locationDetailAddress = SelectedDic
        if ok_default_language == "my" {
            model.division = SelectedDic.stateOrDivitionNameMy
        }else if ok_default_language == "uni" {
            model.division = SelectedDic.stateOrDivitionNameUni
        }else {
            model.division = SelectedDic.stateOrDivitionNameEn
        }
        
        if let cell = tbDeposite.cellForRow(at: IndexPath(row: 0, section: 2)) as? DivisionCellWithdraw {
            cell.lblDivision.text = model.division
        }
        
        model.divisionCode = SelectedDic.stateOrDivitionCode
        
        model.townshipCode = ""
        model.township = ""
        model.city = ""
        model.village = ""
        model.street = ""
        
        if isBtnFullAddressHidden {
            isBtnFullAddressHidden = false
            cellListAddress.append(tbDeposite.dequeueReusableCell(withIdentifier: "ShowAddressFacePay") as! ShowAddressFacePay)
            self.insertRowIntoTable(ind: 1, sec: 2)
        }else {
            tbDeposite.reloadRows(at: [IndexPath(row: 1, section: 2)], with: .none)
        }
        self.bottomcontant.constant = 0
    }
    
    func setTownshipCityNameFromDivison(Dic: TownShipDetailForAddress) {
        self.setTownShip(township: Dic)
    }
    
    func setTownshipCityName(cityDic: TownShipDetailForAddress) {
        self.setTownShip(township: cityDic)
    }
    
    private func setTownShip(township: TownShipDetailForAddress) {
        // Check isDefaultCity
        if township.isDefaultCity == "0" {
            isDefaultCity = false
        }else {
           isDefaultCity = true
        }
        
        if ok_default_language == "my" {
            model.township = township.townShipNameMY
        }else if ok_default_language == "uni" {
            model.township = township.townShipNameUni
        }else {
            model.township = township.townShipNameEN
        }
        model.townshipCode = township.townShipCode
        tbDeposite.reloadRows(at: [IndexPath(row: 2, section: 2)], with: .none)
        
        //City
        if ok_default_language == "my" {
            if township.GroupName == "" {
                model.city = township.cityNameMY
            }else {
                if township.isDefaultCity == "1" {
                    model.city = township.DefaultCityNameMY
                }else{
                    model.city = township.cityNameMY
                }
            }
        }else if ok_default_language == "uni" {
            if township.GroupName == "" {
                model.city = township.cityNameUni
            }else {
                if township.isDefaultCity == "1" {
                    model.city = township.DefaultCityNameUni
                }else{
                    model.city = township.cityNameUni
                }
            }
        }else {
            if township.GroupName == "" {
                model.city = township.cityNameEN
            }else {
                if township.isDefaultCity == "1" {
                    model.city = township.DefaultCityNameEN
                }else{
                    model.city = township.cityNameEN
                }
            }
        }
        
        if isCityHidden {
            isCityHidden = false
            cellListAddress.append(tbDeposite.dequeueReusableCell(withIdentifier: "CityCellWithdraw") as! CityCellWithdraw)
            self.insertRowIntoTable(ind: 3, sec: 2)
        }else {
            tbDeposite.reloadRows(at: [IndexPath(row: 3, section: 2)], with: .none)
        }
        
        isVillageAvaible = true
        if township.GroupName.contains(find: "YCDC") || township.GroupName.contains(find: "MCDC") {
            isVillageAvaible = false
        }
        
        if township.cityNameEN == "Amarapura" || township.cityNameEN == "Patheingyi" || township.townShipNameEN == "PatheinGyi" {
            isVillageAvaible = true
        }
        //village
        model.village = ""
        model.street = ""
        model.houseNumber = ""
        model.floorNumber = ""
        model.roomNumber = ""
        if isVillageHidden {
            isVillageHidden = false
            cellListAddress.append(tbDeposite.dequeueReusableCell(withIdentifier: "VillageCell") as! VillageCell)
            self.insertRowIntoTable(ind: 4, sec: 2)
        }else {
            tbDeposite.reloadRows(at: [IndexPath(row: 4, section: 2)], with: .none)
        }
        if isStreeetHidden {
            isStreeetHidden = false
            cellListAddress.append(tbDeposite.dequeueReusableCell(withIdentifier: "StreetVillageCell") as! StreetVillageCell)
            self.insertRowIntoTable(ind: 5, sec: 2)
        }else {
            tbDeposite.reloadRows(at: [IndexPath(row: 5, section: 2)], with: .none)
        }
        
        if isHouseNumberHidden {
            isHouseNumberHidden = false
            cellListAddress.append(tbDeposite.dequeueReusableCell(withIdentifier: "HouseRoomFloorNumberCell") as! HouseRoomFloorNumberCell)
            self.insertRowIntoTable(ind: 6, sec: 2)
            bottomcontant.constant = 0
        }else {
            tbDeposite.reloadRows(at: [IndexPath(row: 6, section: 2)], with: .none)
        }
        
        if let cell = tbDeposite.cellForRow(at: IndexPath(row: 4, section: 2)) as? VillageCell {
            cell.tfVilage.text = ""
        }
        if let cell = tbDeposite.cellForRow(at: IndexPath(row: 5, section: 2)) as? StreetVillageCell {
            cell.txtStreet.text = ""
        }
        if let cell = tbDeposite.cellForRow(at: IndexPath(row: 6, section: 2)) as? HouseRoomFloorNumberCell {
            cell.tfRoom.text = ""
            cell.tfHouse.text = ""
            cell.tfFloor.text = ""
        }
        self.callStreetVillageAPI(townshipcode: township.townShipCode)
    }
    
    func callStreetVillageAPI(townshipcode : String){
        let urlStr   = Url.URL_VillageList + townshipcode
        let url = getUrl(urlStr: urlStr, serverType: .serverApp)
        let params = Dictionary<String,String>()
        TopupWeb.genericClass(url:url, param: params as AnyObject, httpMethod: "GET", handle: {response, isSuccess in
            if isSuccess {
                if let json = response as? Dictionary<String,Any> {
                    let response = json["Data"] as Any
                    let dic = OKBaseController.convertToDictionary(text: response as! String)
                    if let d = dic {
                        if let arrDic = d["Streets"] as? Array<Dictionary<String,String>> {
                            let streetList = StreetList()
                            streetList.isStreetList = true
                            streetList.streetArray = arrDic
                            VillageManager.shared.allStreetList = streetList
                            
                        }else{
                            let streetList = StreetList()
                            streetList.streetArray.removeAll()
                            streetList.isStreetList = false
                            VillageManager.shared.allStreetList = streetList
                        }
                        if let arrDic1 = d["Villages"] as? Array<Dictionary<String,String>> {
                            DispatchQueue.main.async {
                                let villageList = VillageList()
                                villageList.villageArray = arrDic1
                                VillageManager.shared.allVillageList = villageList
                                villageList.isVillageList = true
                                if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 3, section: 2)) as? VillageCell {
                                    cell.tfVilage.isUserInteractionEnabled = true
                                }
                            }
                        }else{
                            DispatchQueue.main.async {
                                let villageList = VillageList()
                                villageList.villageArray.removeAll()
                                VillageManager.shared.allVillageList = villageList
                                villageList.isVillageList = false
                                if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 3, section: 2)) as? VillageCell {
                                    cell.tfVilage.isUserInteractionEnabled = false
                                }
                            }
                        }
                        println_debug(VillageManager.shared.allVillageList)
                    }
                }
            }else {
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "", alertBody: "Network error and try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
                    
                }
            }
        })
    }
    
    func showVillageView(text: String) {
        if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 3, section: 2)) as? TownshipCellWithdraw {
            if cell.lblTownship.text == "Select Township".localized {
                if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 2)) as? VillageCell {
                    cell.tfVilage.text = ""
                    model.village = ""
                    
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "", alertBody: "Please select township".localized, alertImage: UIImage(named: "alert-icon")!)}
                    return
                }
            }
        }
        
        if text.count > 0 {
            if self.view.contains(tvVillageList) {
            }else {
                self.setUpVillageList()
                tvVillageList.tvList.isHidden = false
            }
            tvVillageList.searchString(string: text)
             self.view.bringSubviewToFront(tvVillageList)
        }else {
            if self.view.contains(tvVillageList) {
                self.removeVillageList()
            }
        }
    }
    
    func showStreetView(text: String) {
        if text.count > 0 {
            if self.view.contains(tvStreetList) {
            }else {
                setUpStreetList()
                tvStreetList.tvList.isHidden = false
            }
            tvStreetList.getAllAddressDetails(searchTextStr: text, yAsix: 0)
            self.view.bringSubviewToFront(tvStreetList)
        }else {
            if self.view.contains(tvStreetList) {
                self.removeStreetList()
            }
        }
    }
    
    func setUpVillageList() {
        if !self.view.contains(tvVillageList) {
            tvVillageList.frame = CGRect(x: 5, y: 64, width: self.view.frame.width - 10, height: 50)
            tvVillageList.backgroundColor = UIColor.clear
            tvVillageList.delegate = self
            tvVillageList.tvList.isHidden = true
            tvVillageList.isUserInteractionEnabled = true
            self.view.addSubview(tvVillageList)
            self.view.bringSubviewToFront(tvVillageList)
        }
       
    }
    
    func setUpStreetList() {
        if !self.view.contains(tvStreetList)  {
        tvStreetList.frame = CGRect(x: 5, y: 64, width: self.view.frame.width - 10, height: 50)
        tvStreetList.backgroundColor = UIColor.clear
        tvStreetList.delegate = self
        tvStreetList.tvList.isHidden = true
        tvStreetList.isUserInteractionEnabled = true
        self.view.addSubview(tvStreetList)
        self.view.bringSubviewToFront(tvStreetList)
        }
    }
    
    func villageNameSelected(vill_Name: String) {
        let indexPath = IndexPath(row: 4, section: 2)
        if let cell = self.tbDeposite.cellForRow(at: indexPath) as? VillageCell {
            cell.tfVilage.text = vill_Name
            cell.tfVilage.resignFirstResponder()
            model.village = vill_Name
            self.removeVillageList()
        }
    }
    
    func streetNameSelected(street_Name: addressModel) {
        let indexPath = IndexPath(row: 5, section: 2)
        if let cell = self.tbDeposite.cellForRow(at: indexPath) as? StreetVillageCell {
            cell.txtStreet.text = street_Name.shortName
            cell.txtStreet.resignFirstResponder()
            model.street = street_Name.shortName ?? ""
            self.removeStreetList()
        }
    }
    func removeVillageList() {
        if self.view.contains(tvVillageList) {
            tvVillageList.removeFromSuperview()
        }
    }
    func removeStreetList() {
        if self.view.contains(tvStreetList) {
            tvStreetList.removeFromSuperview()
        }
    }
    
    func imageIsNullOrNot(imageName : UIImage)-> Bool {
        let size = CGSize(width: 0, height: 0)
        if (imageName.size.width == size.width) {
            return false
        }else {
            return true
        }
    }
    
}

extension DepositeVC : ContactPickerDelegate,CountryViewControllerDelegate {
    func countryViewController(_ list: CountryViewController, country: Country) {
        if countryListShownFor == "deposit" {
            self.model.depositFlag = country.code
            self.model.depositCountryCode = country.dialCode
            let btn = UIButton()
            btn.tag = 100
            self.onClickContactNumberClose(btn)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 0, section: 0)) as? depositeContactCell {
                    cell.tfAlternateNumber.becomeFirstResponder()
                }
            })
           
        }else {
            self.model.withdrawFlag = country.code
            self.model.withdrawCountryCode = country.dialCode
            let btn = UIButton()
            btn.tag = 1000
            self.onClickContactNumberClose(btn)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 0, section: 1)) as? withdrawDepositeContactCell {
                cell.tfAlternateNumber.becomeFirstResponder()
            }
            })
        }
        list.dismiss(animated: false, completion: nil)
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
                list.dismiss(animated: false, completion: nil)
    }
    
    func contact(_: ContactPickersPicker, didContactFetchFailed error: NSError){}
    func contact(_: ContactPickersPicker, didCancel error: NSError){}
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker){ decodeContact(contact: contact)}
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]){}
    func decodeContact(contact: ContactPicker) {
        var countryCode = ""
        var countryFlag = ""
        var tempContryCode = ""
        var tempPhoneNumber = ""
        let phoneNumberCount = contact.phoneNumbers.count
        var phoneNumber = ""
        
        if phoneNumberCount == 1  {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        }
        else if phoneNumberCount > 1 {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        }else {}
        
        if phoneNumber != "" {
            phoneNumber = phoneNumber.replacingOccurrences(of: ")", with: "")
            phoneNumber = phoneNumber.replacingOccurrences(of: "(", with: "")
            phoneNumber = phoneNumber.trimmingCharacters(in: .whitespaces)
        }
        
        let cDetails = identifyCountry(withPhoneNumber: phoneNumber)
        if cDetails.0 == "" || cDetails.1 == "" {
            countryFlag = "myanmar"
            countryCode = "+95"
            tempContryCode = countryCode
        } else {
            countryFlag = cDetails.1
            countryCode = cDetails.0
            tempContryCode = countryCode
        }
        println_debug(tempContryCode)
        let phone = phoneNumber.replacingOccurrences(of: cDetails.0, with: "0")
        if phone.hasPrefix("0") {
            if countryFlag == "myanmar" {
                tempPhoneNumber = phone
            } else {
                tempPhoneNumber = phoneNumber.replacingOccurrences(of: cDetails.0, with: "")
            }
        } else {
            if countryFlag == "myanmar" {
                tempPhoneNumber =   "0" + phone
            } else {
                tempPhoneNumber =  phone
            }
        }
        
        countryCode.remove(at: countryCode.startIndex)
        if tempPhoneNumber.hasPrefix("0"){
            tempPhoneNumber.remove(at: tempPhoneNumber.startIndex)
        }
        let finalMob =  "00" + countryCode + tempPhoneNumber
        let noToCheck = "0" + tempPhoneNumber
        if finalMob.count > 8 {

            if countryCode == "95" {
                let mbLength = (validObj.getNumberRangeValidation(noToCheck))
                if !mbLength.isRejected {
                    if noToCheck.count < mbLength.min || noToCheck.count > mbLength.max {
                        DispatchQueue.main.async {
                            self.showAlert(alertTitle: "", alertBody: "You selected invalid number, Please try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))}
                    }else {
                        let countryData = identifyCountryByCode(withPhoneNumber: "+" + countryCode)
                        if contactListShownFor == "deposit" {
                            self.btnCheckBox.isSelected = false
                            isDepositeNumberSelectedFromContact = true
                            let btn = UIButton()
                            btn.tag = 100
                            self.onClickContactNumberClose(btn)
                            model.contactNumber = noToCheck
                            model.confirmContactNumber = noToCheck
                            model.depositCountryCode = countryData.countryCode
                            model.depositFlag = countryData.countryFlag
                            if let cell = tbDeposite.cellForRow(at: IndexPath(row: 0, section: 0)) as? depositeContactCell {
                                cell.tfAlternateNumber.text = noToCheck
                                cell.btnClose.isHidden = false
                                cell.imgCountryFlag.image = UIImage(named: model.depositFlag)
                                cell.lblCountyCode.text = "(" + model.depositCountryCode + ")"
                                
                            }
                            
                            DispatchQueue.main.async {
                               self.insertDepositorConfirmNumber(isKeyboardShow: false)
                            }
                            
                            self.CheckNumberRegisteredAndMinMaxAmount(number: formateValidNumber(num: noToCheck, coutryCode: countryData.countryCode), completion: {(_ isBool: Bool, data: Any) in
                                if isBool {
                                    if let dic = data as? NSDictionary {
                                        DispatchQueue.main.async {
                                            if let min = dic["MinimumAmount"] as? String {
                                                self.minAmount = Int(min) ?? 0
                                            }
                                            if let max = dic["MaximumFaceAmount"] as? String {
                                                self.maxAmount = Int(max) ?? 0
                                            }
                                            if let maxWithoutFace = dic["MaximumWithoutFaceAmount"] as? String {
                                                self.withoutFaceMaxAmount = Int(maxWithoutFace) ?? 0
                                            }
                                            
                                            let mynum = self.formateValidNumber(num: noToCheck, coutryCode: countryData.countryCode)
                                            
                                            if UserModel.shared.mobileNo == mynum {
                                                self.btnCheckBox.isSelected = true
                                                self.isdepositorNumberOKDollreResitered = true
                                                self.model.name = UserModel.shared.name
                                                self.model.depositFacePhotoURL = UserModel.shared.proPic
                                                self.AddNameCell()
                                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                                    if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 0)) as? depositeNameCell {
                                                        cell.tfUserName.isUserInteractionEnabled = false
                                                        cell.btnclose.isHidden = true
                                                    }
                                                })
                                                self.AddAmountCell()
                                            }else {
                                                if let registerStatus = dic["DestinationNumberRegStatus"] as? NSNumber {
                                                    if registerStatus.boolValue {
                                                        self.isdepositorNumberOKDollreResitered = true
                                                        self.model.name = dic["MercantName"] as? String ?? ""
                                                        self.model.depositFacePhotoURL = dic["ProfilePic"] as? String ?? ""
                                                        self.AddNameCell()
                                                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                                            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 0)) as? depositeNameCell {
                                                                cell.tfUserName.isUserInteractionEnabled = false
                                                                cell.btnclose.isHidden = true
                                                            }
                                                        })
                                                        self.AddAmountCell()
                                                    }else {
                                                        self.isdepositorNumberOKDollreResitered = false
                                                        self.AddNameCell()
                                                        if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 0)) as? depositeNameCell {
                                                            cell.tfUserName.isUserInteractionEnabled = true
                                                            cell.btnclose.isHidden = true
                                                        }
                                                    }
                                                }
                                            }
                   
                                        }
                                    }
                                }else {
                                    let btn = UIButton()
                                    btn.tag = 100
                                    self.onClickContactNumberClose(btn)
                                }
                            })
                            
                        }else {
                             let (mobileNumber,_) = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: UserModel.shared.mobileNo)
                            println_debug(tempPhoneNumber)
                            if noToCheck == model.contactNumber {
                                alertViewObj.wrapAlert(title: "", body: "Depositor number and withdrawal number should not be same".localized, img: UIImage(named: "alert-icon")!)
                                alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
                                    let btn = UIButton()
                                    btn.tag = 1000
                                    self.onClickContactNumberClose(btn)
                                })
                                DispatchQueue.main.async {
                                    alertViewObj.showAlert(controller: self)
                                }
                            }else if mobileNumber == noToCheck {
                                alertViewObj.wrapAlert(title: "", body: "You can't withdraw money to own number".localized, img: UIImage(named: "alert-icon")!)
                                alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
                                    let btn = UIButton()
                                    btn.tag = 1000
                                    self.onClickContactNumberClose(btn)
                                })
                                DispatchQueue.main.async {
                                    alertViewObj.showAlert(controller: self)
                                }
                            }else {
                                let btn = UIButton()
                                btn.tag = 1000
                                self.onClickContactNumberClose(btn)
                                isWithdrawNumberSelectedFromContact = true
                                model.contactNumberWithDrawer = noToCheck
                                model.confirmContactNumberWithDrawer = noToCheck
                                model.withdrawCountryCode = countryData.countryCode
                                model.withdrawFlag = countryData.countryFlag
                                
                                if let cell = tbDeposite.cellForRow(at: IndexPath(row: 0, section: 1)) as? withdrawDepositeContactCell {
                                    cell.tfAlternateNumber.text = noToCheck
                                    cell.btnClose.isHidden = false
                                    cell.imgCountryFlag.image = UIImage(named: model.withdrawFlag)
                                    cell.lblCountyCode.text = "(" + model.withdrawCountryCode + ")"
                                }
                                
                                self.checkNumberRegisteredWithOKDoller(number: formateValidNumber(num: noToCheck, coutryCode: countryData.countryCode), completion: { (_ isBool : Bool, data: Any) in
                                    if isBool {
                                        self.isWithdrawalNumberOKDollreResitered = true
                                        if let dic = data as? NSDictionary {
                                            self.model.nameWithdrawer = dic["MercantName"] as? String ?? ""
                                            self.AddWithdrawConfirmContactNumber(isKeyboardShow: false)
                                            self.model.nameWithdrawer = dic["MercantName"] as? String ?? ""
                                            self.model.withdrawalFacePhotoURL = dic["ProfilePic"] as? String ?? ""
                                            self.AddWithdrawNameCell()
                                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 1)) as? withdrawNameCell {
                                                cell.isUserInteractionEnabled = false
                                                cell.btnClose.isHidden = true
                                                }
                                            })
                                            
                                            if (self.isdepositorNumberOKDollreResitered || self.btnCheckBox.isSelected) && self.isWithdrawalNumberOKDollreResitered {
                                                if let cell1 = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 0)) as? depositeAgentCommCell {
                                                    cell1.tfAmount.text = "0"
                                                }
                                            }else if self.btnCheckBox.isSelected {
                                                if let cell1 = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 0)) as? depositeAgentCommCell {
                                                    let finalCom = (Int(self.model.TotalCommission ) ?? 0) - (Int(self.model.AgentCashInCommission) ?? 0)
                                                    cell1.tfAmount.text = String(finalCom)
                                                }
                                            }else if self.isWithdrawalNumberOKDollreResitered {
                                                if let cell1 = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 0)) as? depositeAgentCommCell {
                                                    let finalCom = (Int(self.model.TotalCommission ) ?? 0) - (Int(self.model.AgentCashInCommission) ?? 0)
                                                    cell1.tfAmount.text = String(finalCom)
                                                }
                                            }
                                            self.bottomcontant.constant = 0
                                        }
                                    }else {
                                        self.isWithdrawalNumberOKDollreResitered = false
                                        self.AddWithdrawConfirmContactNumber(isKeyboardShow: false)
                                        self.AddWithdrawNameCell()
                                    }
                                })
                            }
                         
                        }
                        println_debug("Final Number :\(model.contactNumber) ,flag = \(countryData.countryFlag) , CountryCode: \(countryData.countryCode)")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "", alertBody: "You selected invalid number, Please try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))}
                }
            }else{
                let countryData = identifyCountryByCode(withPhoneNumber: "+" + countryCode)
                
                if contactListShownFor == "deposit" {
                    let btn = UIButton()
                    btn.tag = 100
                    self.onClickContactNumberClose(btn)
                    
                    self.btnCheckBox.isSelected = false
                    isDepositeNumberSelectedFromContact = true
                    model.contactNumber = tempPhoneNumber
                    model.confirmContactNumber = tempPhoneNumber
                    model.depositCountryCode = countryData.countryCode
                    model.depositFlag = countryData.countryFlag
                    self.isdepositorNumberOKDollreResitered = false
                    if let cell = tbDeposite.cellForRow(at: IndexPath(row: 0, section: 0)) as? depositeContactCell {
                        cell.tfAlternateNumber.text = tempPhoneNumber
                        cell.btnClose.isHidden = false
                        cell.imgCountryFlag.image = UIImage(named: model.depositFlag)
                        cell.lblCountyCode.text = "(" + model.depositCountryCode + ")"
                        
                    }
                    DispatchQueue.main.async {
                        self.insertDepositorConfirmNumber(isKeyboardShow: false)
                    }
                    self.CheckNumberRegisteredAndMinMaxAmount(number: formateValidNumber(num: tempPhoneNumber, coutryCode: countryData.countryCode), completion: {(_ isBool: Bool, data: Any) in
                        if isBool {
                            if let dic = data as? NSDictionary {
                                DispatchQueue.main.async {
                                    if let min = dic["MinimumAmount"] as? String {
                                        self.minAmount = Int(min) ?? 0
                                    }
                                    if let max = dic["MaximumFaceAmount"] as? String {
                                        self.maxAmount = Int(max) ?? 0
                                    }
                                    if let maxWithoutFace = dic["MaximumWithoutFaceAmount"] as? String {
                                        self.withoutFaceMaxAmount = Int(maxWithoutFace) ?? 0
                                    }
                                    
                                    let mynum = self.formateValidNumber(num: tempPhoneNumber, coutryCode: countryData.countryCode)
                                    
                                    if UserModel.shared.mobileNo == mynum {
                                        self.btnCheckBox.isSelected = true
                                        self.isdepositorNumberOKDollreResitered = true
                                        self.model.name = UserModel.shared.name
                                        self.model.depositFacePhotoURL = UserModel.shared.proPic
                                        self.AddNameCell()
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 0)) as? depositeNameCell {
                                                cell.tfUserName.isUserInteractionEnabled = false
                                                cell.btnclose.isHidden = true
                                            }
                                        })
                                        self.AddAmountCell()
                                    }else {
                                    if let registerStatus = dic["DestinationNumberRegStatus"] as? NSNumber {
                                        if registerStatus.boolValue {
                                            self.model.name = dic["MercantName"] as? String ?? ""
                                            self.model.depositFacePhotoURL = dic["ProfilePic"] as? String ?? ""
                                            self.AddNameCell()
                                              DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 0)) as? depositeNameCell {
                                                cell.tfUserName.isUserInteractionEnabled = false
                                                cell.btnclose.isHidden = true
                                                }
                                              })
                                            self.AddAmountCell()
                                        }else {
                                          
                                            self.AddNameCell()
                                            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 0)) as? depositeNameCell {
                                                cell.tfUserName.isUserInteractionEnabled = true
                                                cell.btnclose.isHidden = true
                                            }
                                        }
                                    }
                                    }
                                }
                            }
                        }else {
                            DispatchQueue.main.async {
                                self.showAlert(alertTitle: "", alertBody: "Request Failed. Please Try Again".localized, alertImage: UIImage(named: "alert-icon")!)}
                        }
                    })
                    
                }else {
                    if tempPhoneNumber == model.contactNumber {
                        DispatchQueue.main.async {
                            self.showAlert(alertTitle: "", alertBody: "Depositor number and withdrawal number should not be same".localized, alertImage: UIImage(named: "alert-icon")!)}
                        return
                    }
                    let btn = UIButton()
                    btn.tag = 1000
                    self.onClickContactNumberClose(btn)
                    
                    isWithdrawNumberSelectedFromContact = true
                    model.contactNumberWithDrawer = tempPhoneNumber
                    model.confirmContactNumberWithDrawer = tempPhoneNumber
                    model.withdrawCountryCode = countryData.countryCode
                    model.withdrawFlag = countryData.countryFlag
                    
                    if let cell = tbDeposite.cellForRow(at: IndexPath(row: 0, section: 1)) as? withdrawDepositeContactCell {
                        cell.tfAlternateNumber.text = tempPhoneNumber
                        cell.btnClose.isHidden = false
                        cell.imgCountryFlag.image = UIImage(named: model.withdrawFlag)
                        cell.lblCountyCode.text = "(" + model.withdrawCountryCode + ")"
                    }
                    
                    self.checkNumberRegisteredWithOKDoller(number: formateValidNumber(num: tempPhoneNumber, coutryCode: countryData.countryCode), completion: { (_ isBool : Bool, data: Any) in
                        if isBool {
                            self.isWithdrawalNumberOKDollreResitered = true
                            if let dic = data as? NSDictionary {
                                self.model.nameWithdrawer = dic["MercantName"] as? String ?? ""
                                self.AddWithdrawConfirmContactNumber(isKeyboardShow: false)
                                self.model.nameWithdrawer = dic["MercantName"] as? String ?? ""
                                self.model.withdrawalFacePhotoURL = dic["ProfilePic"] as? String ?? ""
                                self.AddWithdrawNameCell()
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 1)) as? withdrawNameCell {
                                    cell.isUserInteractionEnabled = false
                                    cell.btnClose.isHidden = true
                                    }
                                })
                                self.bottomcontant.constant = 0
                            }
                        }else {
                            self.isWithdrawalNumberOKDollreResitered = false
                            self.AddWithdrawConfirmContactNumber(isKeyboardShow: false)
                            self.AddWithdrawNameCell()
                        }
                    })
                }
              // println_debug("Final Number :\(model.withDrawNumber) ,flag = \(countryData.countryFlag) , CountryCode: \(countryData.countryCode)")
            }
        }else{
            DispatchQueue.main.async {
                self.showAlert(alertTitle: "", alertBody: "You selected invalid number, Please try again".localized, alertImage: #imageLiteral(resourceName: "alert-icon"))
            }
        }
    }
    
}
