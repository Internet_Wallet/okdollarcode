//
//  DipositorVCTextFieldExtension.swift
//  OK
//
//  Created by Imac on 7/4/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

extension DepositeVC : UITextFieldDelegate,MyTextFieldDelegate,TownShipVcDeletage,TownshipVC2Delegate,NRCTypesVCDelegate {
    
    // MARK: - Select NRC Fucntions
    
    func setDivison(division: LocationDetail) {
        // After Selecting NRC Divison
        locationDetails = division
        if nrcFrom == "Depositor" {
            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 6, section: 0)) as? depositeNRCCell {
                cell.viewNRCButton.isHidden = false
                cell.viewNRCTF.isHidden = true
                var divStr = ""
                if ok_default_language == "my"{
                    divStr = division.nrcCodeNumberMy + "/"
                }else if ok_default_language == "uni"{
                    divStr = division.nrcCodeNumberUni + "/"
                }else {
                    divStr = division.nrcCodeNumber + "/"
                }
                cell.btnNRC.setTitle(divStr, for: .normal)
                model.nrcPrefix = divStr
            }
        }else {
            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 1)) as? WithdrawNRCCell {
                cell.viewNRCButton.isHidden = false
                cell.viewNRCTF.isHidden = true
                var divStr = ""
                if ok_default_language == "my"{
                    divStr = division.nrcCodeNumberMy + "/"
                }else if ok_default_language == "uni"{
                    divStr = division.nrcCodeNumberUni + "/"
                }else {
                    divStr = division.nrcCodeNumber + "/"
                }
                cell.btnNRC.setTitle(divStr, for: .normal)
                model.nrcPrefixWithdraw = divStr
            }
        }
    }
    
    func textFieldDidDelete() {
        if nrcFrom == "Depositor" {
            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 6, section: 0)) as? depositeNRCCell {
                if cell.tfNRC.text?.count == 0 {
                    self.showNRCSuggestionView()
                }
            }
        }else {
            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 1)) as? WithdrawNRCCell {
                if cell.tfNRC.text?.count == 0 {
                    self.showNRCSuggestionView()
                }
            }
        }
    }
    
    func setDivisionAndTownship(strDivison_township: String) {
        self.setNRCDetails(nrcDetails: strDivison_township)

    }
    
    func NRCValueString(nrcSting : String) {
        self.setNRCDetails(nrcDetails: nrcSting)
    }
    
    func setNRCDetails(nrcDetails : String) {
        if nrcFrom == "Depositor" {
            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 6, section: 0)) as? depositeNRCCell {
                cell.viewNRCButton.isHidden = true
                cell.viewNRCTF.isHidden = false
                let myString = nrcDetails
                let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
                cell.btnnrc.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
                cell.btnnrc.frame = CGRect(x: 0, y: -2, width:stringSize.width, height: 52)
                cell.btnnrc.setTitleColor(.black, for: .normal)
                cell.btnnrc.titleLabel?.textAlignment = .left
                cell.btnnrc.contentHorizontalAlignment = .left
                cell.btnnrc.stringTag = "Depositor"
                cell.btnnrc.addTarget(self, action: #selector(self.BtnNrcTypeAction(button:)), for: .touchUpInside)
                cell.btnnrc.setTitle(nrcDetails, for: UIControl.State.normal)
                model.nrcPrefix = nrcDetails
                cell.tfNRC.leftView = nil
                let btnView = UIView()
                btnView.frame = CGRect(x: 0, y: 0, width: cell.btnnrc.frame.width , height: 52)
                btnView.addSubview(cell.btnnrc)
                cell.tfNRC.leftView = btnView
                cell.tfNRC.leftViewMode = UITextField.ViewMode.always
                self.showNRCSuggestion()
            }
        }else {
            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 1)) as? WithdrawNRCCell {
                cell.viewNRCButton.isHidden = true
                cell.viewNRCTF.isHidden = false
                let myString = nrcDetails
                let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
                cell.btnnrc.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
                cell.btnnrc.frame = CGRect(x: 0, y: -2, width:stringSize.width, height: 52)
                cell.btnnrc.setTitleColor(.black, for: .normal)
                cell.btnnrc.titleLabel?.textAlignment = .left
                cell.btnnrc.contentHorizontalAlignment = .left
                cell.btnnrc.stringTag = "WithDraw"
                cell.btnnrc.addTarget(self, action: #selector(self.BtnNrcTypeAction(button:)), for: .touchUpInside)
                cell.btnnrc.setTitle(nrcDetails, for: UIControl.State.normal)
                model.nrcPrefixWithdraw = nrcDetails
                cell.tfNRC.leftView = nil
                let btnView = UIView()
                btnView.frame = CGRect(x: 0, y: 0, width: cell.btnnrc.frame.width , height: 52)
                btnView.addSubview(cell.btnnrc)
                cell.tfNRC.leftView = btnView
                cell.tfNRC.leftViewMode = UITextField.ViewMode.always
                self.showNRCSuggestion()
            }
        }
    }
    
    @objc func BtnNrcTypeAction(button : UIButton) {
         println_debug(button)
        if button.stringTag == "Depositor" {
           nrcFrom = "Depositor"
        }else {
           nrcFrom = "WithDraw"
        }
        self.showNRCSuggestion()
    }
    
    private func showNRCSuggestion() {
        self.view.endEditing(true)
        if nrcFrom == "Depositor" {
            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 6, section: 0)) as? depositeNRCCell {
                let sb = UIStoryboard(name: "Registration" , bundle: nil)
                let nRCTypesVC  = sb.instantiateViewController(withIdentifier: "NRCTypesVC") as! NRCTypesVC
                nRCTypesVC.modalPresentationStyle = .overCurrentContext
                nRCTypesVC.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                nRCTypesVC.setName(nrcName: (cell.btnnrc.titleLabel?.text)!)
                nRCTypesVC.nRCTypesVCDelegate = self
                present(nRCTypesVC, animated: false, completion: nil)
                cell.tfNRC.resignFirstResponder()
            }
        }else {
            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 1)) as? WithdrawNRCCell {
                let sb = UIStoryboard(name: "Registration" , bundle: nil)
                let nRCTypesVC  = sb.instantiateViewController(withIdentifier: "NRCTypesVC") as! NRCTypesVC
                nRCTypesVC.modalPresentationStyle = .overCurrentContext
                nRCTypesVC.view.frame = .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                nRCTypesVC.setName(nrcName: (cell.btnnrc.titleLabel?.text)!)
                nRCTypesVC.nRCTypesVCDelegate = self
                present(nRCTypesVC, animated: false, completion: nil)
                cell.tfNRC.resignFirstResponder()
            }
        }
    }
    
    func setNrcTypeName(nrcType : String) {
        if nrcFrom == "Depositor" {
            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 6, section: 0)) as? depositeNRCCell {
                cell.tfNRC.text = ""
                let stringSize: CGSize = nrcType.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
                 cell.btnnrc.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
                cell.btnnrc.titleLabel?.textAlignment = .left
                cell.btnnrc.contentHorizontalAlignment = .left
                cell.btnnrc.frame = CGRect(x: 0, y: -2, width:stringSize.width, height: 52)
                cell.tfNRC.leftView = nil
                let btnView = UIView()
                btnView.frame = CGRect(x: 0, y: 0, width: cell.btnnrc.frame.width , height: 52)
                btnView.addSubview(cell.btnnrc)
                cell.tfNRC.leftView = btnView
                cell.tfNRC.leftViewMode = UITextField.ViewMode.always
                model.nrcPrefix = nrcType
                cell.btnnrc.setTitle(nrcType, for: .normal)
                if (cell.tfNRC.text?.length)! < 6 {
                    cell.tfNRC.becomeFirstResponder()
                }
            }
        }else {
            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 1)) as? WithdrawNRCCell {
                cell.tfNRC.text = ""
                let stringSize: CGSize = nrcType.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
                cell.btnnrc.titleLabel?.font =  UIFont(name: appFont, size: appButtonSize)
                cell.btnnrc.titleLabel?.textAlignment = .left
                cell.btnnrc.contentHorizontalAlignment = .left
                cell.btnnrc.frame = CGRect(x: 0, y: -2, width:stringSize.width, height: 52)
                cell.tfNRC.leftView = nil
                let btnView = UIView()
                btnView.frame = CGRect(x: 0, y: 0, width: cell.btnnrc.frame.width , height: 52)
                btnView.addSubview(cell.btnnrc)
                cell.tfNRC.leftView = btnView
                cell.tfNRC.leftViewMode = UITextField.ViewMode.always
                model.nrcPrefixWithdraw = nrcType
                cell.btnnrc.setTitle(nrcType, for: .normal)
                if (cell.tfNRC.text?.length)! < 6 {
                    cell.tfNRC.becomeFirstResponder()
                }
            }
        }
    }
    
    func showNRCSuggestionView() {
        self.showNRCSuggestion()
    }
    
    
    // MARK: - TextField Delegates Methods
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        self.removeContactList()
        if textField.tag == 100 {
            model.contactNumber = textField.text ?? ""
        }else if textField.tag == 200 {
            model.confirmContactNumber = textField.text ?? ""
        }else if textField.tag == 300 {
            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 0)) as? depositeNameCell {
                var title = ""
                if textField.text?.count ?? 0 > 0 {
                    if let txt = cell.lbltitle.text {
                        if txt.contains(find: " ") {
                           title =  txt.replacingOccurrences(of: " " , with: "") + ","
                        }else {
                           title = txt + ","
                        }
                    }
                    if let txt = textField.text {
                        title =  title + txt.uppercased()
                    }
                }
                model.name = title
            }

            var count = 0
            if ok_default_language == "my" || ok_default_language == "uni"{
                count = 1
            }else {
                count = 2
            }
            if textField.text?.count ?? 0 > count {
                self.AddAmountCell()
            }else {
                if !isAmountFieldHidden {
                    hideDepositorAmountField()
                    deleteRowFromDepositorSection(delBelowIndax: 3)
                }
            }
        }else if textField.tag == 400 {
            model.amount = (textField.text ?? "").replacingOccurrences(of: ",", with: "")
            let amout = Int(textField.text?.replacingOccurrences(of: ",", with: "") ?? "0")
            if let cell = tbDeposite.cellForRow(at: IndexPath(row: 3, section: 0)) as? depositeAmountCell {
                if textField.text?.count ?? 0 > 0 {
                    //  if firstTimeAmount == 1 && isWithDrawConfirmContactNubmerHidden {
                    self.model.AgentCashInCommission = ""
                    self.model.AgentWithDrawCommission = ""
                    self.model.OkDollarCommission = ""
                    self.model.TotalCommission = ""
                    var number = ""
                    var exdAgentCom = false
                    if btnCheckBox.isSelected {
                        number = UserModel.shared.mobileNo
                        exdAgentCom = true
                    }else {
                        if model.depositCountryCode == "+95" {
                            number = "0095" + String(model.confirmContactNumber.dropFirst())
                        }else {
                            number = "00" + model.depositCountryCode.dropFirst() + model.confirmContactNumber
                        }
                        exdAgentCom = false
                    }
                    if amout ?? 0 < self.minAmount {
                        self.minAmountCheck?.invalidate()
                        self.minAmountCheckEndEditing = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.checkMinAmountEndEdinting(_:)), userInfo: nil, repeats: false)
                        return
                    }
                    
                    let url = getUrl(urlStr: Url.URL_FacePayGetCommisionsTaxi, serverType: .faceIDPay)
                    let dic = ["PhoneNumber": number,"Amount": Int(model.amount) ?? 0,"ExcludeAgentCommission": exdAgentCom] as [String : Any]
                    DispatchQueue.main.async {
                        progressViewObj.showProgressView()
                    }
                    TopupWeb.genericClassForFacePay(url: url, param: dic as AnyObject, httpMethod: "POST") { (response, success) in
                       DispatchQueue.main.async {
                           self.removeProgressView()
                       }
                        if success {
                            if let json = response as? Dictionary<String,Any> {
                                if let code = json["code"] as? NSNumber, code == 200 {
                                    if let dic = json["data"] as? NSDictionary {
                                        println_debug(json["data"])
                                        if let content = dic["Content"] as? NSDictionary {
                                            DispatchQueue.main.async {
                                                let cashInComm = content["AgentCashInCommission"] as? NSNumber ?? 0
                                                let withdrawComm = content["AgentWithDrawCommission"] as? NSNumber ?? 0
                                                let okDollarComm = content["OkDollarCommission"] as? NSNumber ?? 0
                                                let totalComm = content["TotalCommission"] as? NSNumber ?? 0
                                                self.model.AgentCashInCommission = cashInComm.stringValue
                                                self.model.AgentWithDrawCommission = withdrawComm.stringValue
                                                self.model.OkDollarCommission = okDollarComm.stringValue
                                                self.model.TotalCommission = totalComm.stringValue
                                                if !self.isAgentComFieldHidden {
                                                    if let cell1 = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 0)) as? depositeAgentCommCell {
                                                        let amountStr = self.getDigitDisplayColor(self.model.TotalCommission)
                                                        cell1.tfAmount.text = amountStr
                                                        let amountVal = amountStr.components(separatedBy: ",").joined(separator: "")
                                                        if amountVal.count < 7 {
                                                            cell1.tfAmount.textColor = UIColor.black
                                                        } else if amountVal.count == 7 {
                                                            cell1.tfAmount.textColor = UIColor.green
                                                        } else if amountVal.count > 7 {
                                                            cell1.tfAmount.textColor = UIColor.red
                                                        }
                                                       if self.btnDepositCom.isSelected {
                                                            if let _ = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 0)) as? depositeAgentCommCell {
                                                                if amout ?? 0 >= self.minAmount {
                                                                    self.showComField = true
                                                                    self.tbDeposite.reloadRows(at: [ IndexPath(row: 4, section: 0)], with: .none)
                                                                }
                                                            }
                                                       }else {
                                                        self.showComField = false
                                                        self.tbDeposite.reloadRows(at: [ IndexPath(row: 4, section: 0)], with: .none)
                                                        }
                                                    }
                                                }else {
                                                    if self.btnDepositCom.isSelected {
                                                        if amout ?? 0 >= self.minAmount {
                                                            self.showComField = true
                                                        }
                                                    }else {
                                                        self.showComField = false
                                                    }
                                                    self.InsertDepositAgentCom()
                                                    self.InsertDepositFacePhotoCell()
                                                    self.InsertNRCDepositeCell()
                                                    self.AddWithdrawContactNumber()
                                                    self.tbDeposite.reloadSections([1], with: .none)
                                                }
                                                if amout ?? 0 < self.minAmount {
                                                }else {
                                                    self.amountCheckingTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.searchForKeyword(_:)), userInfo: nil, repeats: false)
                                                }
                                            }
                                        }
                                    }
                                }else {
                                    alertViewObj.wrapAlert(title: "", body: "Request Failed. Please Try Again".localized, img: UIImage(named: "alert-icon")!)
                                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                        cell.tfAmount.text = ""
                                        cell.btnClear.isHidden = true
                                        self.model.amount = ""
                                        self.resetAgentCommission()
                                        cell.tfAmount.becomeFirstResponder()
                                    })
                                    DispatchQueue.main.async {
                                        alertViewObj.showAlert(controller: self)
                                    }
                                }
                            }else {
                                alertViewObj.wrapAlert(title: "", body: "Request Failed. Please Try Again".localized, img: UIImage(named: "alert-icon")!)
                                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                    cell.tfAmount.text = ""
                                    cell.btnClear.isHidden = true
                                    self.model.amount = ""
                                        self.resetAgentCommission()
                                    cell.tfAmount.becomeFirstResponder()
                                })
                                DispatchQueue.main.async {
                                    alertViewObj.showAlert(controller: self)
                                }
                            }
                        }else {
                            alertViewObj.wrapAlert(title: "", body: "Request Failed. Please Try Again".localized, img: UIImage(named: "alert-icon")!)
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                cell.tfAmount.text = ""
                                cell.btnClear.isHidden = true
                                self.model.amount = ""
                                self.resetAgentCommission()
                                cell.tfAmount.becomeFirstResponder()
                            })
                            DispatchQueue.main.async {
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                    
                }else {
                    alertViewObj.wrapAlert(title: "", body: "Minimum Deposit Amount".localized + " " + getDigitDisplay(String(minAmount)) + " " + "MMK", img: UIImage(named: "minimum_deposit")!)
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        cell.tfAmount.text = ""
                        self.depositorAmoutStatus = "Deposit Amount".localized
                        cell.lblStatusTitle.text = self.depositorAmoutStatus
                        cell.btnClear.isHidden = true
                        self.model.amount = ""
                        self.resetAgentCommission()
                        //cell.tfAmount.becomeFirstResponder()
                    })
                    DispatchQueue.main.async {
                        alertViewObj.showAlert(controller: self)
                    }
                }
            }
        }else if textField.tag == 500 || textField.tag == 5000 {
            if let txt = textField.text {
                if nrcFrom == "Depositor" {
                    model.nrcPostfix = txt
                    if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 6, section: 0)) as? depositeNRCCell {
                        model.nrc = cell.btnnrc.title(for: .normal)! + cell.tfNRC.text!
                    }
                }else {
                    model.nrcPostfixWithdraw = txt
                    if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 1)) as? WithdrawNRCCell {
                        model.nrcWithraw = cell.btnnrc.title(for: .normal)! + cell.tfNRC.text!
                    }
                }
            }
        }else if textField.tag == 3000 {
            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 1)) as? withdrawNameCell {
                var title = ""
                if textField.text?.count ?? 0 > 0 {
                    if let txt = cell.lbltitle.text {
                        if txt.contains(find: " ") {
                            title =  txt.replacingOccurrences(of: " " , with: "") + ","
                        }else {
                            title = txt + ","
                        }
                    }
                    if let txt = textField.text {
                        title = title + txt
                    }
                }
                model.nameWithdrawer = title
            }
            var count = 0
            if ok_default_language == "my" || ok_default_language == "uni"{
                count = 1
            }else {
                count = 2
            }
            
            if textField.text?.count ?? 0 > count {
                if btnFacePayWithoutPhoto.isSelected {
                    self.InsertWithdrawFacePhotoCell()
                    self.AddNRCellWithdrawal()
                    //self.InsertDivisionCellWithScroll()
                    self.scrollingBottom(ind: 2, sec: 1)
                    self.bottomcontant.constant = 0
                }else {
                    self.InsertWithdrawFacePhotoCellWithScroll()
                }
            }else {
                if !isWithdrawalFacePhotoHidden {
                    hideWithdrawPhotoField()
                    deleteRowFromWithdrawSection(delBelowIndax: 3)
                }
            }
        }else if textField.tag == 204 {
            model.village = textField.text ?? ""
            self.removeVillageList()
        }else if textField.tag == 205 {
             model.street = textField.text ?? ""
            self.removeStreetList()
        }else if textField.tag == 206 {
              model.houseNumber = textField.text ?? ""
        }else if textField.tag == 207 {
              model.floorNumber = textField.text ?? ""
        }else if textField.tag == 208 {
              model.roomNumber = textField.text ?? ""
        }
    }
    
    @objc func textFieldDidEditChanged(_ textField : UITextField) {
        // Contact Number
        if textField.tag == 100 {
            btnCheckBox.isSelected = false
            model.contactNumber = textField.text ?? ""
            isDepositeNumberSelectedFromContact = false
            
            if textField.text?.count ?? 0 < 4 {
               self.removeContactList()
            }else if textField.text?.count ?? 0 >= 4 {
                if let _ = tbDeposite.cellForRow(at: IndexPath(row: 0, section: 0)) as? depositeContactCell{
                    let rectOfCellInTableView = self.tbDeposite.rectForRow(at: IndexPath(row: 0, section: 0) )
                    let rectOfCellInSuperview = self.tbDeposite.convert(rectOfCellInTableView, to: self.tbDeposite.superview)
                    showContactList(yAsix: (rectOfCellInSuperview.origin.y) - 5, position: "TOP", contactFor: "Deposit")
                    contactSuggestionView.loadContacts(txt: textField.text ?? "0", axis: (rectOfCellInSuperview.origin.y) - 5, position: "TOP")
                }
            }
            
            if model.depositCountryCode == "+95" {
                let mbLength = validObj.getNumberRangeValidation(textField.text!)
                if textField.text?.count ?? 0 >= mbLength.max || textField.text?.count ?? 0 >= mbLength.min {
    
                    if textField.text?.count ?? 0 >= mbLength.max {
                        let (mobileNumber,_) = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: UserModel.shared.mobileNo)
                        if mobileNumber == model.contactNumber {

                            self.CheckNumberRegisteredAndMinMaxAmount(number: UserModel.shared.mobileNo, completion: {(_ isBool: Bool, data: Any) in
                                if isBool {
                                    if let dic = data as? NSDictionary {
                                        DispatchQueue.main.async {
                                            self.isDepositeNumberSelectedFromContact = true
                                            self.btnCheckBox.isSelected = true
                                            self.model.name = UserModel.shared.name
                                            self.model.depositFacePhotoURL = UserModel.shared.proPic
                                            self.insertDepositorConfirmNumber(isKeyboardShow: false)
                                            self.AddNameCell()
                                            self.AddAmountCell()
                                            self.model.depositFacePhotoURL = UserModel.shared.proPic
                                            self.model.nrc = UserModel.shared.nrc
                                            if let min = dic["MinimumAmount"] as? String {
                                                self.minAmount = Int(min) ?? 0
                                            }
                                            if let max = dic["MaximumFaceAmount"] as? String {
                                                self.maxAmount = Int(max) ?? 0
                                            }
                                            if let maxWithoutFace = dic["MaximumWithoutFaceAmount"] as? String {
                                                self.withoutFaceMaxAmount = Int(maxWithoutFace) ?? 0
                                            }
                                            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 3, section: 0)) as? depositeAmountCell {
                                                cell.tfAmount.becomeFirstResponder()
                                            }
                                        }
                                    }
                                }
                            })
                            return
                        }
                        
                        if !isContactConfirmHidden {
                           if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 1, section: 0)) as? depositeConfirmContactCell {
                                cell.tfConfirmNumber.becomeFirstResponder()
                            }
                        }
                        self.insertDepositorConfirmNumber(isKeyboardShow: true)
                        if !isNameFieldHidden {
                            let btn = UIButton()
                            btn.tag = 200
                            onClickContactNumberClose(btn)
                        }
                    }else {
                        if !isContactConfirmHidden {
                            let btn = UIButton()
                            btn.tag = 200
                            onClickContactNumberClose(btn)
                        }else {
                           self.insertDepositorConfirmNumber(isKeyboardShow: false)
                        }
                    }
                   
      
                }else if textField.text?.count ?? 0 < mbLength.min && !isContactConfirmHidden {
                    let btn = UIButton()
                    btn.tag = 10
                    onClickContactNumberClose(btn)
                }
            }else {
                if textField.text?.count ?? 0 == 13 {
                    textField.resignFirstResponder()
                    if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 1, section: 0)) as? depositeConfirmContactCell {
                        cell.tfConfirmNumber.becomeFirstResponder()
                    }
                    if (model.contactNumber.count != model.confirmContactNumber.count) && !isNameFieldHidden {
                        let btn = UIButton()
                        btn.tag = 200
                        onClickContactNumberClose(btn)
                    }
                }else if textField.text?.count ?? 0 >= 4 {
                    self.insertDepositorConfirmNumber(isKeyboardShow: false)
                    if model.contactNumber.count != model.confirmContactNumber.count && !isContactConfirmHidden {
                            let btn = UIButton()
                            btn.tag = 200
                            self.onClickContactNumberClose(btn)
                    }
                }else if textField.text?.count ?? 0 < 4 {
                    isDepositeNumberSelectedFromContact = false
                    let btn = UIButton()
                    btn.tag = 10
                    onClickContactNumberClose(btn)
                }
            }
        }else if textField.tag == 200 {
            if validObj.checkMatchingNumber(string: textField.text ?? "", withString: model.contactNumber) {
                model.confirmContactNumber = textField.text ?? ""
                if let cell = tbDeposite.cellForRow(at: IndexPath(row: 1, section: 0)) as? depositeConfirmContactCell {
                    if cell.imgCountryFlag.image == UIImage(named: "myanmar") {
                        if textField.text?.count ?? 0 > 2 {
                            cell.btnClose.isHidden = false
                        }else {
                            cell.btnClose.isHidden = true
                        }
                    }else {
                        if textField.text?.count ?? 0 > 0 {
                            cell.btnClose.isHidden = false
                        }else {
                            cell.btnClose.isHidden = true
                        }
                    }
                }
                
                if model.contactNumber == model.confirmContactNumber {
                    self.view.endEditing(true)
                    var number = ""
                    if model.depositCountryCode == "+95" {
                        number = "0095" + String(model.confirmContactNumber.dropFirst())
                    }else {
                        number = "00" + model.depositCountryCode.dropFirst() + model.confirmContactNumber
                    }
                    self.CheckNumberRegisteredAndMinMaxAmount(number: number, completion: {(_ isBool: Bool, data: Any) in
                        if isBool {
                            if let dic = data as? NSDictionary {
                                DispatchQueue.main.async {
                                    if let min = dic["MinimumAmount"] as? String {
                                        self.minAmount = Int(min) ?? 0
                                    }
                                    if let max = dic["MaximumFaceAmount"] as? String {
                                        self.maxAmount = Int(max) ?? 0
                                    }
                                    if let maxWithoutFace = dic["MaximumWithoutFaceAmount"] as? String {
                                        self.withoutFaceMaxAmount = Int(maxWithoutFace) ?? 0
                                    }
                                    
                                    if UserModel.shared.mobileNo == number {
                                        self.btnCheckBox.isSelected = true
                                        self.isdepositorNumberOKDollreResitered = true
                                        self.model.name = UserModel.shared.name
                                        self.model.depositFacePhotoURL = UserModel.shared.proPic
                                        self.AddNameCell()
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 0)) as? depositeNameCell {
                                                cell.tfUserName.isUserInteractionEnabled = false
                                                cell.btnclose.isHidden = true
                                            }
                                        })
                                        self.AddAmountCell()
                                    }else {
                                        if let registerStatus = dic["DestinationNumberRegStatus"] as? NSNumber {
                                            if registerStatus.boolValue {
                                                self.isdepositorNumberOKDollreResitered = true
                                                self.model.name = dic["MercantName"] as? String ?? ""
                                                self.model.depositFacePhotoURL = dic["ProfilePic"] as? String ?? ""
                                                
                                                self.AddNameCell()
                                                if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 0)) as? depositeNameCell {
                                                    cell.tfUserName.isUserInteractionEnabled = false
                                                    cell.btnclose.isHidden = true
                                                }
                                                self.AddAmountCell()
                                            }else {
                                                self.isdepositorNumberOKDollreResitered = false
                                                self.AddNameCell()
                                            }
                                        }

                                    }
                                    
                                }
                            }
                        }else {
                            DispatchQueue.main.async {
                                let btn = UIButton()
                                btn.tag = 100
                                self.onClickContactNumberClose(btn)
                            }
                        }
                    })
                }else {
                    if !isNameFieldHidden {
                        let btn = UIButton()
                        btn.tag = 20
                        self.onClickContactNumberClose(btn)
                    }
                }
            }else {
                if let cell = tbDeposite.cellForRow(at: IndexPath(row: 1, section: 0)) as? depositeConfirmContactCell {
                    cell.tfConfirmNumber.text?.removeLast()
                    model.confirmContactNumber = String(textField.text?.dropLast() ?? "")
                    if !isNameFieldHidden {
                        let btn = UIButton()
                        btn.tag = 20
                        self.onClickContactNumberClose(btn)
                    }
                }
            }
        }else if textField.tag == 300 {
            
       /*     if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 0)) as? depositeNameCell {
                var title = ""
                if textField.text?.count ?? 0 > 0 {
                    if let txt = cell.lbltitle.text {
                        if txt.contains(find: " ") {
                            title =  txt.replacingOccurrences(of: " " , with: "") + ","
                        }else {
                            title = txt + ","
                        }
                    }
                    if let txt = textField.text {
                        title =  title + txt.uppercased()
                    }
                }
                model.name = title
            }
            var count = 0
            if ok_default_language == "my" || ok_default_language == "uni"{
                count = 1
            }else {
                count = 2
            }
            if textField.text?.count ?? 0 > count {
                self.AddAmountCell()
            }else {
                if !isAmountFieldHidden {
                    hideDepositorAmountField()
                    deleteRowFromDepositorSection(delBelowIndax: 3)
                }
            }
            */
        }else if textField.tag == 400  {
            
            if self.showComField {
                if let cell = tbDeposite.cellForRow(at: IndexPath(row: 4, section: 0)) as? depositeAgentCommCell {
                    self.showComField = false
                    cell.tfAmount.text = ""
                    self.tbDeposite.reloadRows(at: [ IndexPath(row: 4, section: 0)], with: .none)
                }
            }
            
            if let cell = tbDeposite.cellForRow(at: IndexPath(row: 3, section: 0)) as? depositeAmountCell {
                if cell.tfAmount.text?.count ?? 0 > 0  {
                    let amout = Int(textField.text?.replacingOccurrences(of: ",", with: "") ?? "0")
                    if amout == 0 {
                        cell.tfAmount.text = ""
                        return
                    }
                    cell.btnClear.isHidden = false
                    depositorAmoutStatus = "Amount".localized
                    cell.lblStatusTitle.text = depositorAmoutStatus
                    var amountStr = getDigitDisplayColor(textField.text ?? "")
                    if amountStr.count > 14 {
                        amountStr = String((amountStr.dropLast()))
                        textField.text = amountStr
                    } else {
                        let amountVal = amountStr.components(separatedBy: ",").joined(separator: "")
                        if amountVal.count < 7 {
                            textField.textColor = UIColor.black
                        } else if amountVal.count == 7 {
                            textField.textColor = UIColor.green
                        } else if amountVal.count > 7 {
                            textField.textColor = UIColor.red
                        }
                        textField.text = amountStr
                    }
                    
                    
                 
                    if amout ?? 0 > self.walletBalance {
                        showToastForProfile(message: "Insufficient balance to transfer".localized, handler: {_ in })
                        cell.tfAmount.text = ""
                        cell.btnClear.isHidden = true
                        model.amount = ""
                            self.resetAgentCommission()
                        return
                    }else if btnFacePayWithPhoto.isSelected {
                        if amout ?? 0 > self.maxAmount {
                            showToastForProfile(message: "Insufficient balance to transfer".localized, handler: {_ in })
                            cell.tfAmount.text = ""
                            cell.btnClear.isHidden = true
                            model.amount = ""
                            self.resetAgentCommission()
                              return
                        }
                    }else {
                        if amout ?? 0 > self.withoutFaceMaxAmount {
                            showToastForProfile(message: "Insufficient balance to transfer".localized, handler: {_ in })
                            cell.tfAmount.text = ""
                            cell.btnClear.isHidden = true
                            model.amount = ""
                            self.resetAgentCommission()
                            return
                        }
                    }
                    
                    if textField.text?.count ?? 0 > 0 {
                        self.minAmountCheck?.invalidate()
                        self.minAmountCheckEndEditing?.invalidate()
                        self.minAmountCheck = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.checkMinAmount(_:)), userInfo: nil, repeats: false)
                    }
                    
                }else {
                    cell.btnClear.isHidden = true
                    depositorAmoutStatus = "Deposit Amount".localized
                    cell.lblStatusTitle.text = depositorAmoutStatus
                }
            }
        }else if textField.tag == 500 || textField.tag == 5000 {
            if textField.text == "000000" {
                textField.text?.removeLast()
            }else if textField.text!.count > 6 {
                textField.text?.removeLast()
                textField.resignFirstResponder()
            }else if textField.text!.count == 6 {
                textField.resignFirstResponder()
                if nrcFrom == "Depositor" {
                    if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 6, section: 0)) as? depositeNRCCell {
                        model.nrc = cell.btnnrc.title(for: .normal)! + cell.tfNRC.text!
                    }
                }else {
                    if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 1)) as? WithdrawNRCCell {
                        model.nrcWithraw = cell.btnnrc.title(for: .normal)! + cell.tfNRC.text!
                    }
                }
            }else {
                if nrcFrom == "Depositor" {
                    if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 6, section: 0)) as? depositeNRCCell {
                        model.nrc = cell.btnnrc.title(for: .normal)! + cell.tfNRC.text!
                    }
                }else {
                    if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 1)) as? WithdrawNRCCell {
                        model.nrcWithraw = cell.btnnrc.title(for: .normal)! + cell.tfNRC.text!
                    }
                    self.InsertDivisionCell()
                }
            }
        }else if textField.tag == 1000 {
            model.contactNumberWithDrawer = textField.text ?? ""
            isWithdrawNumberSelectedFromContact = false
            self.resetCommissionAmount()
            if textField.text?.count ?? 0 < 4 {
                self.removeContactList()
            }else if textField.text?.count ?? 0 >= 4 {
                if let _ = tbDeposite.cellForRow(at: IndexPath(row: 0, section: 1)) as? withdrawDepositeContactCell {
                    let rectOfCellInTableView = self.tbDeposite.rectForRow(at: IndexPath(row: 0, section: 1) )
                    let rectOfCellInSuperview = self.tbDeposite.convert(rectOfCellInTableView, to: self.tbDeposite.superview)
                    showContactList(yAsix: rectOfCellInSuperview.origin.y, position: "TOP", contactFor: "Withdraw")
                    contactSuggestionView.loadContacts(txt: textField.text ?? "0", axis: rectOfCellInSuperview.origin.y, position: "TOP")
                }
            }
            if model.withdrawCountryCode == "+95" {
                let mbLength = validObj.getNumberRangeValidation(textField.text!)
                if textField.text?.count ?? 0 >= mbLength.max || textField.text?.count ?? 0 >= mbLength.min {
                    let (mobileNumber,_) = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: UserModel.shared.mobileNo)
                    
                    if model.contactNumber == model.contactNumberWithDrawer {
                        DispatchQueue.main.async {
                            self.showAlert(alertTitle: "", alertBody: "Depositor number and withdrawal number should not be same".localized, alertImage: UIImage(named: "alert-icon")!)}
                        let btn = UIButton()
                        btn.tag = 1000
                        onClickContactNumberClose(btn)
                    }else if mobileNumber == model.contactNumberWithDrawer {
                        DispatchQueue.main.async {
                            self.showAlert(alertTitle: "", alertBody: "You can't withdraw money to own number".localized, alertImage: UIImage(named: "alert-icon")!)}
                        let btn = UIButton()
                        btn.tag = 1000
                        onClickContactNumberClose(btn)
                    }else {
                        if textField.text?.count ?? 0 >= mbLength.max {
                            if !isWithDrawConfirmContactNubmerHidden {
                                if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 1, section: 1)) as? WithdrawConfirmContactCell {
                                    cell.tfConfirmNumber.becomeFirstResponder()
                                }
                            }
                            self.AddWithdrawConfirmContactNumber(isKeyboardShow: true)
                        }else {
                            if !isWithDrawConfirmContactNubmerHidden {
                                let btn = UIButton()
                                btn.tag = 2000
                                onClickContactNumberClose(btn)
                            }else{
                                self.AddWithdrawConfirmContactNumber(isKeyboardShow: false)
                            }
                        }
                    }
                    
                    if !isNameFieldWithdrawHidden {
                        let btn = UIButton()
                        btn.tag = 2000
                        onClickContactNumberClose(btn)
                    }
                    
                    
                }else if textField.text?.count ?? 0 < mbLength.min && !isWithDrawConfirmContactNubmerHidden {
                    let btn = UIButton()
                    btn.tag = 30
                    onClickContactNumberClose(btn)
                }
            }else {
                if textField.text?.count ?? 0 == 13 {
                    textField.resignFirstResponder()
                    if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 1, section: 1)) as? WithdrawConfirmContactCell {
                        cell.tfConfirmNumber.becomeFirstResponder()
                    }
                    if model.contactNumberWithDrawer.count != model.confirmContactNumberWithDrawer.count && !isNameFieldWithdrawHidden {
                        let btn = UIButton()
                        btn.tag = 2000
                        self.onClickContactNumberClose(btn)
                    }
                }else if textField.text?.count ?? 0 >= 4 {
                    self.AddWithdrawConfirmContactNumber(isKeyboardShow: false)
                    if model.contactNumberWithDrawer.count != model.confirmContactNumberWithDrawer.count && !isWithDrawConfirmContactNubmerHidden {
                        let btn = UIButton()
                        btn.tag = 2000
                        self.onClickContactNumberClose(btn)
                    }
                }else if textField.text?.count ?? 0 < 4 && !isWithDrawConfirmContactNubmerHidden {
                    let btn = UIButton()
                    btn.tag = 30
                    onClickContactNumberClose(btn)
                }
            }
        }else if textField.tag == 2000 {
            if validObj.checkMatchingNumber(string: textField.text ?? "", withString: model.contactNumberWithDrawer) {
                model.confirmContactNumberWithDrawer = textField.text ?? ""
                if let cell = tbDeposite.cellForRow(at: IndexPath(row: 1, section: 1)) as? WithdrawConfirmContactCell {
                    if cell.imgCountryFlag.image == UIImage(named: "myanmar") {
                        if textField.text?.count ?? 0 > 2 {
                            cell.btnClose.isHidden = false
                        }else {
                            cell.btnClose.isHidden = true
                        }
                    }else {
                        if textField.text?.count ?? 0 > 0 {
                            cell.btnClose.isHidden = false
                        }else {
                            cell.btnClose.isHidden = true
                        }
                    }
                }
                
                if model.contactNumberWithDrawer == model.confirmContactNumberWithDrawer {
                    self.view.endEditing(true)
                    var number = ""
                    if model.withdrawCountryCode  == "+95" {
                        number = "0095" + String(model.confirmContactNumberWithDrawer.dropFirst())
                    }else {
                        number = "00" + model.withdrawCountryCode.dropFirst() + model.confirmContactNumberWithDrawer
                    }
                    
                    self.checkNumberRegisteredWithOKDoller(number: number, completion: { (_ isBool : Bool, data: Any) in
                        if isBool {
                            if let dic = data as? NSDictionary {
                                self.model.nameWithdrawer = dic["MercantName"] as? String ?? ""
                                self.isWithdrawalNumberOKDollreResitered = true
                                self.model.nameWithdrawer = dic["MercantName"] as? String ?? ""
                                self.model.withdrawalFacePhotoURL = dic["ProfilePic"] as? String ?? ""
                                self.AddWithdrawNameCell()
                                // clear commission field if both numbers registered
                                if (self.isdepositorNumberOKDollreResitered || self.btnCheckBox.isSelected) && self.isWithdrawalNumberOKDollreResitered {
                                    if let cell1 = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 0)) as? depositeAgentCommCell {
                                        cell1.tfAmount.text = "0"
                                    }
                                }else if self.btnCheckBox.isSelected {
                                    if let cell1 = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 0)) as? depositeAgentCommCell {
                                        let finalCom = (Int(self.model.TotalCommission ) ?? 0) - (Int(self.model.AgentCashInCommission) ?? 0)
                                        cell1.tfAmount.text = String(finalCom)
                                    }
                                }else if self.isWithdrawalNumberOKDollreResitered {
                                    if let cell1 = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 0)) as? depositeAgentCommCell {
                                        let finalCom = (Int(self.model.TotalCommission ) ?? 0) - (Int(self.model.AgentCashInCommission) ?? 0)
                                        cell1.tfAmount.text = String(finalCom)
                                    }
                                }
                                self.bottomcontant.constant = 0
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                    if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 1)) as? withdrawNameCell {
                                        cell.tfUserName.isUserInteractionEnabled = false
                                        cell.btnClose.isHidden = true
                                        cell.lbltitle.text = ""
                                        cell.tfUserName.text = self.model.nameWithdrawer
                                    }
                                })
                            }
                        }else {
                            self.AddWithdrawNameCell()
                        }
                    })
                }else {
                    self.resetCommissionAmount()
                    if !isNameFieldWithdrawHidden {
                        let btn = UIButton()
                        btn.tag = 41
                        self.onClickContactNumberClose(btn)
                    }
                }
            }else {
                if let cell = tbDeposite.cellForRow(at: IndexPath(row: 1, section: 1)) as? WithdrawConfirmContactCell {
                    cell.tfConfirmNumber.text?.removeLast()
                    model.confirmContactNumberWithDrawer = String(textField.text?.dropLast() ?? "")
                }
                if let cell = tbDeposite.cellForRow(at: IndexPath(row: 1, section: 0)) as? WithdrawConfirmContactCell {
                    cell.tfConfirmNumber.text?.removeLast()
                    model.confirmContactNumberWithDrawer = String(textField.text?.dropLast() ?? "")
                    if !isNameFieldWithdrawHidden {
                        let btn = UIButton()
                        btn.tag = 40
                        self.onClickContactNumberClose(btn)
                    }
                }
            }
        }else if textField.tag == 3000 {
         /*   if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 1)) as? withdrawNameCell {
                var title = ""
                if textField.text?.count ?? 0 > 0 {
                    if let txt = cell.lbltitle.text {
                        if txt.contains(find: " ") {
                            title =  txt.replacingOccurrences(of: " " , with: "") + ","
                        }else {
                            title = txt + ","
                        }
                    }
                    if let txt = textField.text {
                        title = title + txt.uppercased()
                    }
                }
            }
            var count = 0
            if ok_default_language == "my" || ok_default_language == "uni"{
                count = 1
            }else {
                count = 2
            }
            
            if textField.text?.count ?? 0 > count {
                if btnFacePayWithoutPhoto.isSelected {
                    self.InsertWithdrawFacePhotoCell()
                    self.AddNRCellWithdrawal()
                    self.InsertDivisionCell()
                    if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 1)) as? withdrawNameCell {
                        cell.tfUserName.becomeFirstResponder()
                        }
                }else {
                   self.InsertWithdrawFacePhotoCell()
                }
            }else {
                firstTimeWithdrawName = 0
                if !isWithdrawalFacePhotoHidden {
                    hideWithdrawPhotoField()
                    deleteRowFromWithdrawSection(delBelowIndax: 3)
                }
            }*/
        }else if textField.tag == 204 {
            self.setUpVillageList()
            showVillageView(text: textField.text ?? "")
        }else if textField.tag == 205 {
            self.setUpStreetList()
            showStreetView(text: textField.text ?? "")
        }
    }
    
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 100 || textField.tag == 200 || textField.tag == 400{
            textField.keyboardType = .numberPad
        }else if textField.tag == 300 {
            //textField.keyboardType = .alphabet
            let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 0)) as? depositeNameCell
            if !isYourNameKeyboardHidden {
                cell?.viewMaleFemale.isHidden = true
                cell?.imgUser.isHidden = false
            }else{
                cell?.viewMaleFemale.isHidden = false
                cell?.imgUser.isHidden = true
                return false
            }
        }else if textField.tag == 500 {
                self.nrcFrom = "Depositor"
        }else if textField.tag == 5000 {
                self.nrcFrom = "WithDraw"
        }else if textField.tag == 3000 {
          //  textField.keyboardType = .alphabet
            let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 1)) as? withdrawNameCell
            withdrawnameShowFirstTime = false
            if !isYourNameWithdrawKeyboardHidden {
                cell?.viewMaleFemale.isHidden = true
                cell?.imgUser.isHidden = false
            }else{
                cell?.viewMaleFemale.isHidden = false
                cell?.imgUser.isHidden = true
                return false
            }
        }else if textField.tag == 204{
           
        }else if textField.tag == 205{
           
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if range.location == 0 && string == " " {
            return false
        }
        
        if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
            let endOfDocument = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
            return false
        }
        
        let chars = textField.text! + string
        // Contact Number
        if textField.tag == 100 {
            let mbLength = validObj.getNumberRangeValidation(chars)
            let isRejected = validObj.getNumberRangeValidation(chars).isRejected
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "1234567890").inverted).joined(separator: "")) { return false }
            if let cell = tbDeposite.cellForRow(at: IndexPath(row: 0, section: 0)) as? depositeContactCell {
                if cell.imgCountryFlag.image == UIImage(named: "myanmar") {
                    if text.count > 2 {
                        cell.btnClose.isHidden = false
                    }else {
                        cell.btnClose.isHidden = true
                    }
                    
                    if range.location == 0 && range.length > 1 {
                        textField.text = "09"
                        cell.btnClose.isHidden = true
                        return false
                    }
                    if range.location == 1 {
                        return false
                    }
                    if isRejected {
                        showToast(message: "Invalid Mobile Number".localized, align: .top)
                        textField.text = "09"
                        cell.btnClose.isHidden = true
                        return false
                    }
                    if chars.count > mbLength.max {
                        textField.resignFirstResponder()
                        return false
                    }
                }else {
                    if text.count > 0 {
                        cell.btnClose.isHidden = false
                    }else {
                        cell.btnClose.isHidden = true
                    }
                    if range.location == 0 && range.length > 1 {
                        cell.btnClose.isHidden = true
                        return false
                    }
                    
                    if text.count > 13 {
                        return false
                    }
                }
            }
        }else if textField.tag == 200 {
            let mbLength = validObj.getNumberRangeValidation(chars)
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "1234567890").inverted).joined(separator: "")) { return false }
            if let cell = tbDeposite.cellForRow(at: IndexPath(row: 1, section: 0)) as? depositeConfirmContactCell {
                if cell.imgCountryFlag.image == UIImage(named: "myanmar") {
                    if text.count > 3 {
                        cell.btnClose.isHidden = false
                    }else {
                        cell.btnClose.isHidden = true
                    }
                    
                    if range.location == 0 && range.length > 1 {
                        textField.text = "09"
                        cell.btnClose.isHidden = true
                        return false
                    }
                    if range.location == 1 {
                        return false
                    }
                    if chars.count > mbLength.max {
                        textField.resignFirstResponder()
                        return false
                    }
                }
            }
        }else if textField.tag == 300 {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: NAMECHARSET).inverted).joined(separator: "")) { return false }
            if text.count >= 40 && string == " " {
                return false
            }
            
            if string == "" || string == " " {
                if let char = string.cString(using: String.Encoding.utf8) {
                    let isBackSpace = strcmp(char, "\\b")
                    if (isBackSpace == -92) {
                        return true
                    }
                    let _ = TextValidations.trimSuccessiveSpaces(textField: textField, range: range, string: string)
                    return false
                }
            }
            println_debug(text.count)
            return text.count <= 40
        }else if textField.tag == 400 {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "1234567890").inverted).joined(separator: "")) { return false }
//            if textField.text?.count ?? 0 > 0 {
//                    self.minAmountCheck?.invalidate()
//                    self.minAmountCheckEndEditing?.invalidate()
//                    self.minAmountCheck = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.checkMinAmount(_:)), userInfo: nil, repeats: false)
//            }
        }else if textField.tag == 500 || textField.tag == 5000{
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "1234567890").inverted).joined(separator: "")) { return false }
            let length = textField.text!.count + string.count - range.length
            if range.location == 0 && string == " " {
                return false
            }
            
            if length > 6 {
                if textField.tag == 500 {
                    if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 6, section: 0)) as? depositeNRCCell {
                        cell.tfNRC.resignFirstResponder()
                    }
                }else {
                    if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 0)) as? WithdrawNRCCell {
                        cell.tfNRC.resignFirstResponder()
                    }
                }
                return false
            }
        }else if textField.tag == 1000 {
            let mbLength = validObj.getNumberRangeValidation(chars)
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "1234567890").inverted).joined(separator: "")) { return false }
            if let cell = tbDeposite.cellForRow(at: IndexPath(row: 0, section: 1)) as? withdrawDepositeContactCell {
                if cell.imgCountryFlag.image == UIImage(named: "myanmar") {
                    if text.count > 2 {
                        cell.btnClose.isHidden = false
                    }else {
                        cell.btnClose.isHidden = true
                    }
                    
                    if range.location == 0 && range.length > 1 {
                        textField.text = "09"
                        cell.btnClose.isHidden = true
                        return false
                    }
                    if range.location == 1 {
                        return false
                    }
                    if mbLength.isRejected {
                        showToast(message: "Invalid Mobile Number".localized, align: .top)
                        textField.text = "09"
                        cell.btnClose.isHidden = true
                        return false
                    }
                    if chars.count > mbLength.max {
                        textField.resignFirstResponder()
                        return false
                    }
                }else {
                    if text.count > 0 {
                        cell.btnClose.isHidden = false
                    }else {
                        cell.btnClose.isHidden = true
                    }
                    if range.location == 0 && range.length > 1 {
                        cell.btnClose.isHidden = true
                        return false
                    }
                    
                    if text.count > 13 {
                        return false
                    }
                    
                }
            }
        }else if textField.tag == 2000 {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "1234567890").inverted).joined(separator: "")) { return false }
            if let cell = tbDeposite.cellForRow(at: IndexPath(row: 1, section: 1)) as? WithdrawConfirmContactCell {
                if cell.imgCountryFlag.image == UIImage(named: "myanmar") {
                    if text.count > 3 {
                        cell.btnClose.isHidden = false
                    }else {
                        cell.btnClose.isHidden = true
                    }
                    
                    if range.location == 0 && range.length > 1 {
                        textField.text = "09"
                        cell.btnClose.isHidden = true
                        return false
                    }
                    if range.location == 1 {
                        return false
                    }
                }
            }
        }else if textField.tag == 3000 {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: NAMECHARSET).inverted).joined(separator: "")) { return false }
            if text.count >= 40 && string == " " {
                return false
            }
            if string == "" || string == " " {
                if let char = string.cString(using: String.Encoding.utf8) {
                    let isBackSpace = strcmp(char, "\\b")
                    if (isBackSpace == -92) {
                        return true
                    }
                    
                    let _ = TextValidations.trimSuccessiveSpaces(textField: textField, range: range, string: string)
                    return false
                }
            }
            return text.count <= 40
        }else if textField.tag == 204 {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: OTHERCHARSET).inverted).joined(separator: "")) { return false }
            if string == "" || string == " " {
                if let char = string.cString(using: String.Encoding.utf8) {
                    let isBackSpace = strcmp(char, "\\b")
                    if (isBackSpace == -92) {
                        return true
                    }
                    
                    let _ = TextValidations.trimSuccessiveSpaces(textField: textField, range: range, string: string)
                    return false
                }
            }
            return text.count <= 40
        }else if textField.tag == 205 {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: OTHERCHARSET).inverted).joined(separator: "")) { return false }
          
            if string == "" || string == " " {
                if let char = string.cString(using: String.Encoding.utf8) {
                    let isBackSpace = strcmp(char, "\\b")
                    if (isBackSpace == -92) {
                        return true
                    }
                    
                    let _ = TextValidations.trimSuccessiveSpaces(textField: textField, range: range, string: string)
                    return false
                }
            }
            return text.count <= 40
        }else if textField.tag == 206 {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: OTHERCHARSET).inverted).joined(separator: "")) { return false }
            if string == "" || string == " " {
                if let char = string.cString(using: String.Encoding.utf8) {
                    let isBackSpace = strcmp(char, "\\b")
                    if (isBackSpace == -92) {
                        return true
                    }
                    
                    let _ = TextValidations.trimSuccessiveSpaces(textField: textField, range: range, string: string)
                    return false
                }
            }
             return text.count <= 6
        }else if textField.tag == 207 {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: OTHERCHARSET).inverted).joined(separator: "")) { return false }
            if string == "" || string == " " {
                if let char = string.cString(using: String.Encoding.utf8) {
                    let isBackSpace = strcmp(char, "\\b")
                    if (isBackSpace == -92) {
                        return true
                    }
                    
                    let _ = TextValidations.trimSuccessiveSpaces(textField: textField, range: range, string: string)
                    return false
                }
            }
            return text.count <= 6
        }else if textField.tag == 208 {
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: OTHERCHARSET).inverted).joined(separator: "")) { return false }
            if string == "" || string == " " {
                if let char = string.cString(using: String.Encoding.utf8) {
                    let isBackSpace = strcmp(char, "\\b")
                    if (isBackSpace == -92) {
                        return true
                    }
                    
                    let _ = TextValidations.trimSuccessiveSpaces(textField: textField, range: range, string: string)
                    return false
                }
            }
            return text.count <= 6
        }
        return true
    }
    
    
    // MARK: - Custom Methods for hide/show fields
    
    @objc func onClickContactNumberClose(_ sender: UIButton) {
        if sender.tag == 100 || sender.tag == 10 {
            if let cell = tbDeposite.cellForRow(at: IndexPath(row: 0, section: 0)) as? depositeContactCell {
                btnCheckBox.isSelected = false
                if sender.tag == 100 {
                    if model.depositCountryCode == "+95" {
                        cell.tfAlternateNumber.text = "09"
                    }else {
                        cell.tfAlternateNumber.text = ""
                    }
                     cell.btnClose.isHidden = true
                    cell.imgCountryFlag.image = UIImage(named: model.depositFlag)
                    cell.lblCountyCode.text = "(" + model.depositCountryCode + ")"
                }
                hideDepositorConfirmNumberField()
                deleteRowFromDepositorSection(delBelowIndax: 1)
                removeContactList()
            }
        }else if sender.tag == 200 || sender.tag == 20 {
            if let cell = tbDeposite.cellForRow(at: IndexPath(row: 1, section: 0)) as? depositeConfirmContactCell {
                if sender.tag == 200 {
                    if model.depositCountryCode == "+95" {
                        cell.tfConfirmNumber.text = "09"
                    }else {
                        cell.tfConfirmNumber.text = ""
                    }
                    model.confirmContactNumber = ""
                    cell.btnClose.isHidden = true
                }
                hideDepositorNameField()
                deleteRowFromDepositorSection(delBelowIndax: 2)
            }
        }else if sender.tag == 1000 || sender.tag == 30 {
            if let cell = tbDeposite.cellForRow(at: IndexPath(row: 0, section: 1)) as? withdrawDepositeContactCell {
                if sender.tag == 1000 {
                    model.contactNumberWithDrawer = ""
                    if model.withdrawCountryCode == "+95" {
                        cell.tfAlternateNumber.text = "09"
                    }else {
                        cell.tfAlternateNumber.text = ""
                    }
                    cell.btnClose.isHidden = true
                    cell.imgCountryFlag.image = UIImage(named: model.withdrawFlag)
                    cell.lblCountyCode.text = "(" + model.withdrawCountryCode + ")"
                }
                self.resetCommissionAmount()
                hideWithdrawConfirmNumberField()
                deleteRowFromWithdrawSection(delBelowIndax: 1)
                removeContactList()
            }
        }else if sender.tag == 2000 || sender.tag == 40 || sender.tag == 41 {
            if let cell = tbDeposite.cellForRow(at: IndexPath(row: 1, section: 1)) as? WithdrawConfirmContactCell {
                
                if sender.tag == 2000 {
                    if model.withdrawCountryCode == "+95" {
                        cell.tfConfirmNumber.text = "09"
                    }else {
                        cell.tfConfirmNumber.text = ""
                    }
                    model.confirmContactNumberWithDrawer = ""
                    cell.btnClose.isHidden = true
                }else if sender.tag != 41 {
                    cell.btnClose.isHidden = true
                }
                self.resetCommissionAmount()
                hideWithdrawNameField()
                deleteRowFromWithdrawSection(delBelowIndax: 2)
            }
        }
    }
    
    @objc func onClickUserNameClose(_ sender: UIButton) {
        if sender.tag == 300 {
            depositMaleFemaleTag = false
            hideDepositorNameField()
            deleteRowFromDepositorSection(delBelowIndax: 3)
        }else if sender.tag == 3000 {
            wtihdrawMaleFemaleTag = false
            withdrawnameShowFirstTime = true
            hideWithdrawNameField()
            deleteRowFromWithdrawSection(delBelowIndax: 3)
        }
    }
    
   // MARK: - Insert New cell Methods
    
    func insertDepositorConfirmNumber(isKeyboardShow : Bool) {
        if isContactConfirmHidden {
            isContactConfirmHidden = false
            model.confirmContactNumber = ""
            cellList.append(tbDeposite.dequeueReusableCell(withIdentifier: "depositeConfirmContactCell") as! depositeConfirmContactCell)
            self.insertRowIntoTableWithScroll(ind: 1, sec: 0)
            
            if !isDepositeNumberSelectedFromContact {
                if isKeyboardShow {
                    Timer.scheduledTimer(withTimeInterval: 0.4, repeats: false) { timer in
                        if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 1, section: 0)) as? depositeConfirmContactCell {
                            cell.tfConfirmNumber.becomeFirstResponder()
                        }
                    }
                }
            }
        }
    }
    
    func AddNameCell() {
        if self.isNameFieldHidden {
            self.view.endEditing(true)
            self.isNameFieldHidden = false
            self.depositMaleFemaleTag = false
            self.cellList.append(self.tbDeposite.dequeueReusableCell(withIdentifier: "depositeNameCell") as! depositeNameCell)
            self.insertRowIntoTable(ind: 2, sec: 0)
        }
    }
    
    func AddWithdrawNameCell() {
        if self.isNameFieldWithdrawHidden {
            self.view.endEditing(true)
            self.isNameFieldWithdrawHidden = false
            self.withdrawnameShowFirstTime = false
            self.wtihdrawMaleFemaleTag = false
            self.cellListForWithdrawer.append(self.tbDeposite.dequeueReusableCell(withIdentifier: "withdrawNameCell") as! withdrawNameCell)
            self.insertRowIntoTable(ind: 2, sec: 1)
        }
    }
    
    func AddAmountCell() {
        if isAmountFieldHidden {
            isAmountFieldHidden = false
            model.amount = ""
            cellList.append(tbDeposite.dequeueReusableCell(withIdentifier: "depositeAmountCell") as! depositeAmountCell)
            self.insertRowIntoTable(ind: 3, sec: 0)
        }
    }
    
    func InsertDepositAgentCom() {
        if isAgentComFieldHidden {
            isAgentComFieldHidden = false
            model.agentComm = ""
            cellList.append(tbDeposite.dequeueReusableCell(withIdentifier: "depositeAgentCommCell") as! depositeAgentCommCell)
            self.insertRowIntoTableWithScroll(ind: 4,sec: 0)
            
        }
    }
    func InsertDepositFacePhotoCell() {
        if isDepositFacePhotoHidden {
            isDepositFacePhotoHidden = false
            model.depositFacePhotoURL = ""
            cellList.append(tbDeposite.dequeueReusableCell(withIdentifier: "DepositFacePhotoCell") as! DepositFacePhotoCell)
            self.insertRowIntoTableWithScroll(ind: 5,sec: 0)
            
        }
    }
    
    func InsertNRCDepositeCell() {
        if isNRCTextFieldHidden {
            isNRCTextFieldHidden = false
            model.nrc = ""
            cellList.append(tbDeposite.dequeueReusableCell(withIdentifier: "depositeNRCCell") as! depositeNRCCell)
            self.insertRowIntoTableWithScroll(ind: 6, sec: 0)
            
            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 6, section: 0)) as? depositeNRCCell {
                if !btnCheckBox.isSelected {
                    cell.viewNRCButton.isHidden = false
                    cell.viewNRCTF.isHidden = true
                    cell.btnNRC.setTitle("Enter NRC Details".localized, for: .normal)
                }
            }
        }
        
    }
    
    func AddNRCellWithdrawal() {
        if isWtthdrawalNCRHidden {
            isWtthdrawalNCRHidden = false
            model.nrcWithraw = ""
            cellListForWithdrawer.append(tbDeposite.dequeueReusableCell(withIdentifier: "WithdrawNRCCell") as! WithdrawNRCCell)
            self.insertRowIntoTableWithScroll(ind: 4, sec: 1)
            
            if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 1)) as? WithdrawNRCCell {
                cell.viewNRCButton.isHidden = false
                cell.viewNRCTF.isHidden = true
                cell.btnNRC.setTitle("Enter NRC Details".localized, for: .normal)
            }
        }
        

    }
    
    
    func AddWithdrawContactNumber () {
        if isWithDrawContactNubmerHidden {
           // self.tbDeposite.reloadSections([1], with: .none)
            isWithDrawContactNubmerHidden = false
            model.contactNumberWithDrawer = ""
            cellListForWithdrawer.append(tbDeposite.dequeueReusableCell(withIdentifier: "withdrawDepositeContactCell") as! withdrawDepositeContactCell)
            self.insertRowIntoTableWithScroll(ind: 0, sec: 1)
            
            UIView.animate(withDuration: 0.5, animations: {
                _ = Timer.scheduledTimer(withTimeInterval: 0.4, repeats: false) { timer in
                    let indexPath = IndexPath(row: 0, section: 1)
                    self.tbDeposite.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                }
            })
        }
    }
    func AddWithdrawConfirmContactNumber(isKeyboardShow: Bool) {
        if isWithDrawConfirmContactNubmerHidden {
            isWithDrawConfirmContactNubmerHidden = false
            model.confirmContactNumberWithDrawer = ""
            cellListForWithdrawer.append(tbDeposite.dequeueReusableCell(withIdentifier: "WithdrawConfirmContactCell") as! WithdrawConfirmContactCell)
            self.insertRowIntoTable(ind: 1, sec: 1)
        }
        
        
        if !isWithDrawConfirmContactNubmerHidden {
            if !isWithdrawNumberSelectedFromContact  {
                if isKeyboardShow {
                    Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { timer in
                        if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 1, section: 1)) as? WithdrawConfirmContactCell {
                            cell.tfConfirmNumber.becomeFirstResponder()
                        }
                    }
                }
            }
        }

    }
    
    func InsertWithdrawFacePhotoCell() {
        if isWithdrawalFacePhotoHidden {
            isWithdrawalFacePhotoHidden = false
            model.withdrawalFacePhotoURL = ""
            cellListForWithdrawer.append(tbDeposite.dequeueReusableCell(withIdentifier: "withDrawalFacePhotoCell") as! withDrawalFacePhotoCell)
            self.insertRowIntoTableWithScroll(ind: 3, sec: 1)
            
        }
    }
    
    func InsertWithdrawFacePhotoCellWithScroll() {
        if isWithdrawalFacePhotoHidden {
            isWithdrawalFacePhotoHidden = false
            model.withdrawalFacePhotoURL = ""
            cellListForWithdrawer.append(tbDeposite.dequeueReusableCell(withIdentifier: "withDrawalFacePhotoCell") as! withDrawalFacePhotoCell)
            self.insertRowIntoTable(ind: 3, sec: 1)
            
        }
    }
    
    func InsertDivisionCell(){
        if isDivisionHidden {
            isDivisionHidden = false
            cellListAddress.append(tbDeposite.dequeueReusableCell(withIdentifier: "DivisionCellWithdraw") as! DivisionCellWithdraw)
            self.insertRowIntoTableWithScroll(ind: 0, sec: 2)
            firstTimeAmount = 1
        }
    }
    
    func InsertDivisionCellWithScroll(){
        if isDivisionHidden {
            isDivisionHidden = false
            cellListAddress.append(tbDeposite.dequeueReusableCell(withIdentifier: "DivisionCellWithdraw") as! DivisionCellWithdraw)
            self.insertRowIntoTable(ind: 0, sec: 2)
        }
    }
    
    
    func insertRowIntoTable(ind: Int, sec: Int) {
        self.tbDeposite.beginUpdates()
        self.tbDeposite.insertRows(at: [IndexPath.init(row: ind, section: sec)], with: .none)
        self.tbDeposite.endUpdates()
        UIView.animate(withDuration: 0.5, animations: {
            _ = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false) { timer in
                let indexPath = IndexPath(row: ind, section: sec)
                if ind == 1 && sec == 1 {
                    if let _ = self.tbDeposite.cellForRow(at: indexPath) as? WithdrawConfirmContactCell {
                        self.tbDeposite.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                    }
                }else {
                    self.tbDeposite.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
                }
            }
        })
    }
    
    func scrollingBottom(ind:Int, sec:Int) {
        UIView.animate(withDuration: 0.4, animations: {
            _ = Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { timer in
                let indexPath = IndexPath(row: ind, section: sec)
                self.tbDeposite.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
            }
        })
    }
  
    func insertRowIntoTableWithScroll(ind: Int, sec: Int) {
        self.tbDeposite.beginUpdates()
        self.tbDeposite.insertRows(at: [IndexPath.init(row: ind, section: sec)], with: .none)
        self.tbDeposite.endUpdates()
    }
    
// MARK: - Delete cell Methods
    
    func deletRowIntoTable(ind: Int, sec: Int) {
        self.tbDeposite.beginUpdates()
        self.tbDeposite.deleteRows(at: [IndexPath.init(row: ind, section: sec)], with: .none)
        self.tbDeposite.endUpdates()
    }
    
    
    func deleteRowFromDepositorSection(delBelowIndax: Int) {
        DispatchQueue.main.async {
            if self.cellList.count > delBelowIndax {
                var index = self.cellList.count - 1
                for _ in self.cellList {
                    self.cellList.remove(at: index)
                    self.deletRowIntoTable(ind: index, sec: 0)
                    index -= 1
                    if !(index >= delBelowIndax) {
                        break
                    }
                }
            }
        }
    }
    
    func deleteRowFromWithdrawSection(delBelowIndax: Int) {
        DispatchQueue.main.async {
        if self.cellListForWithdrawer.count > delBelowIndax {
            var index = self.cellListForWithdrawer.count - 1
            for _ in self.cellListForWithdrawer {
                self.cellListForWithdrawer.remove(at: index)
                self.deletRowIntoTable(ind: index, sec: 1)
                index -= 1
                if !(index >= delBelowIndax) {
                    break
                }
            }
        }
        }
    }
    
    func resetAgentCommission() {
        if let cell = tbDeposite.cellForRow(at: IndexPath(row: 4, section: 0)) as? depositeAgentCommCell {
            cell.tfAmount.text = ""
            self.model.TotalCommission = ""
            self.model.AgentCashInCommission = ""
            self.model.AgentWithDrawCommission = ""
            self.model.agentComm = ""
            cell.btnClear.isSelected = false
            self.showComField = false
            self.tbDeposite.reloadRows(at: [ IndexPath(row: 4, section: 0)], with: .none)
        }
    }
    
    func hideDepositorConfirmNumberField() {
        firstTimeAmount = 0
        btnFacePayWithPhoto.isSelected = true
        btnFacePayWithoutPhoto.isSelected = false
        bottomcontant.constant = -50
        model.contactNumber = ""
        model.confirmContactNumber = ""
        model.name = ""
        model.amount = ""
        model.depositFacePhotoURL = ""
        model.nrc = ""
        model.nrcPrefix = ""
        model.nrcPostfix = ""
        isContactConfirmHidden = true
        isYourNameKeyboardHidden = true
        isNameFieldHidden = true
        isAmountFieldHidden = true
        isAgentComFieldHidden = true
        isDepositFacePhotoHidden = true
        isNRCTextFieldHidden = true
        self.resetDepositorName()
        self.isdepositorNumberOKDollreResitered = false
        self.removeDepositorFaceImage()
        self.removeWithdrawCell()
        self.removeWithdrawFaceImage()
        withdrawalImage.image = nil
        if !isDivisionHidden {
            removeAddressCell()
        }
    }
    
    func hideDepositorNameField() {
        firstTimeAmount = 0
        depositMaleFemaleTag = false
        btnFacePayWithPhoto.isSelected = true
        btnFacePayWithoutPhoto.isSelected = false
        bottomcontant.constant = -50
        model.name = ""
        model.amount = ""
        model.depositFacePhotoURL = ""
        model.nrc = ""
        model.nrcPrefix = ""
        model.nrcPostfix = ""
        isYourNameKeyboardHidden = true
        isNameFieldHidden = true
        isAmountFieldHidden = true
        isAgentComFieldHidden = true
        isDepositFacePhotoHidden = true
        isNRCTextFieldHidden = true
        
        self.resetDepositorName()
        self.removeDepositorFaceImage()
        self.removeWithdrawCell()
        self.removeWithdrawFaceImage()
        withdrawalImage.image = nil
        if !isDivisionHidden {
            removeAddressCell()
        }
    }
    
    func resetDepositorName() {
        if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 0)) as? depositeNameCell {
            cell.tfUserName.text = ""
            cell.lbltitle.text = ""
            cell.tfUserName.resignFirstResponder()
            cell.viewMaleFemale.isHidden = true
            cell.viewMaleEn.isHidden = true
            cell.viewFemaleEn.isHidden = true
            cell.viewMaleFemaleMy.isHidden = true
            cell.btnclose.isHidden = true
            cell.imgUser.isHidden = false
            cell.imgUser.image = UIImage(named: "r_user")
            depositorPlaceholder = "Enter Depositor Name".localized
            depositorAmoutStatus = "Deposit Amount".localized
            cell.tfUserName.placeholder = depositorPlaceholder
            cell.tfUserName.isUserInteractionEnabled = true
            cell.isUserInteractionEnabled = true
        }
    }
    
    
    
    func hideDepositorAmountField() {
        showComField = false
        firstTimeAmount = 0
        btnFacePayWithPhoto.isSelected = true
        btnFacePayWithoutPhoto.isSelected = false
        model.amount = ""
        model.depositFacePhotoURL = ""
        model.nrc = ""
        model.nrcPrefix = ""
        model.nrcPostfix = ""
        isContactConfirmHidden = true
        isAmountFieldHidden = true
        isAgentComFieldHidden = true
        isDepositFacePhotoHidden = true
        isNRCTextFieldHidden = true
        self.removeDepositorFaceImage()
        self.removeWithdrawCell()
        self.removeWithdrawFaceImage()
        withdrawalImage.image = nil
    }
    
    func removeDepositorFaceImage() {
        if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 5, section: 0)) as? DepositFacePhotoCell {
            cell.imgPhoto.image = nil
        }
        if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 6, section: 0)) as? depositeNRCCell {
                cell.viewNRCButton.isHidden = false
                cell.viewNRCTF.isHidden = true
                cell.tfNRC.text = ""
               // cell.tfNRC.placeholder = "Enter NRC Details".localized
                cell.btnNRC.setTitle("Enter NRC Details".localized, for: .normal)
        }
    }
  
    
    //Hide withdraw method
    
    func removeWithdrawCell() {
        DispatchQueue.main.async {
        self.removeWithdrawFaceImage()
        if !self.isWithDrawContactNubmerHidden {
           self.hideWithdrawNumberField()
            if self.cellListForWithdrawer.count > 0 {
                var index = self.cellListForWithdrawer.count - 1
                for _ in self.cellListForWithdrawer {
                    self.cellListForWithdrawer.remove(at: index)
                    self.deletRowIntoTable(ind: index, sec: 1)
                    index -= 1
                }
            }
            self.tbDeposite.reloadSections([1], with: .none)
            if !self.isDivisionHidden {
                self.removeAddressCell()
            }
            }
        }
    }
    func hideWithdrawNumberField() {
        bottomcontant.constant = -50
        model.contactNumberWithDrawer = ""
        model.withdrawFlag = "myanmar"
        model.withdrawCountryCode = "+95"
        isWithDrawContactNubmerHidden = true
        
        if !isWithDrawConfirmContactNubmerHidden {
            hideWithdrawConfirmNumberField()
        }
        if !isDivisionHidden {
            removeAddressCell()
        }
      firstTimeWithdrawName = 0
    }
    
    func hideWithdrawConfirmNumberField() {
        self.bottomcontant.constant = -50
        self.wtihdrawMaleFemaleTag = false
        self.withdrawnameShowFirstTime = true
        self.withdrawNameTitle = ""
        self.hideWithdrawNameField()
        if !self.isDivisionHidden {
            self.removeAddressCell()
        }
        self.model.confirmContactNumberWithDrawer = ""
        self.model.nameWithdrawer = ""
        self.model.withdrawalFacePhotoURL = ""
        self.model.nrcWithraw = ""
        
        self.isWithDrawConfirmContactNubmerHidden = true
        self.isYourNameWithdrawKeyboardHidden = true
        self.isNameFieldWithdrawHidden = true
        self.isWithdrawalFacePhotoHidden = true
        self.isWtthdrawalNCRHidden = true
        self.isWithdrawalNumberOKDollreResitered = false
        self.firstTimeWithdrawName = 0
    }
    func hideWithdrawNameField() {
        self.bottomcontant.constant = -50
        self.model.nameWithdrawer = ""
        self.model.withdrawalFacePhotoURL = ""
        self.model.nrcWithraw = ""
        self.model.nrcPrefixWithdraw = ""
        self.model.nrcPostfixWithdraw = ""
        self.isYourNameWithdrawKeyboardHidden = true
        self.isNameFieldWithdrawHidden = true
        self.isWithdrawalFacePhotoHidden = true
        self.isWtthdrawalNCRHidden = true
        
        self.withdrawnameShowFirstTime = true
        self.withdrawNameTitle = ""
        if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 2, section: 1)) as? withdrawNameCell {
            cell.tfUserName.text = ""
            cell.lbltitle.text = ""
            cell.tfUserName.resignFirstResponder()
            cell.viewMaleFemale.isHidden = true
            cell.viewMaleEn.isHidden = true
            cell.viewFemaleEn.isHidden = true
            cell.viewMaleFemaleMy.isHidden = true
            cell.btnClose.isHidden = true
            cell.imgUser.isHidden = false
            cell.imgUser.image = UIImage(named: "r_user")
            withdrawalPlaceholder = "Enter Withdrawal Name".localized
            cell.tfUserName.placeholder = withdrawalPlaceholder
            cell.tfUserName.isUserInteractionEnabled = true
            cell.isUserInteractionEnabled = true
        }
        self.firstTimeWithdrawName = 0
        if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 3, section: 1)) as? withDrawalFacePhotoCell {
            cell.imgPhoto.image = nil
        }
        self.removeWithdrawFaceImage()
        self.withdrawalImage.image = nil
        if !self.isDivisionHidden {
            self.removeAddressCell()
        }
    }
    
    
    func hideWithdrawPhotoField() {
        self.bottomcontant.constant = -50
        self.model.withdrawalFacePhotoURL = ""
        self.model.nrcWithraw = ""
        self.model.nrcPrefixWithdraw = ""
        self.model.nrcPostfixWithdraw = ""
        self.isWithdrawalFacePhotoHidden = true
        self.isWtthdrawalNCRHidden = true
        if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 3, section: 1)) as? withDrawalFacePhotoCell {
            cell.imgPhoto.image = nil
        }
        self.removeWithdrawFaceImage()
        self.withdrawalImage.image = nil
        if !self.isDivisionHidden {
            self.removeAddressCell()
        }
    }
    
    
    func removeWithdrawFaceImage() {
        if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 3, section: 1)) as? withDrawalFacePhotoCell {
            cell.imgPhoto.image = nil
        }
        if let cell = self.tbDeposite.cellForRow(at: IndexPath(row: 4, section: 1)) as? WithdrawNRCCell {
            cell.viewNRCButton.isHidden = false
            cell.viewNRCTF.isHidden = true
            cell.tfNRC.text = ""
            cell.tfNRC.leftView = nil
            cell.btnnrc.setTitle("", for: .normal)
            cell.btnNRC.setTitle("Enter NRC Details".localized, for: .normal)
        }
    }
    
    func removeAddressCell() {
        DispatchQueue.main.async {
        self.hideAddressFields()
        if self.cellListAddress.count > 0 {
            var index = self.cellListAddress.count - 1
            for _ in self.cellListAddress {
                self.cellListAddress.remove(at: index)
                self.deletRowIntoTable(ind: index, sec: 2)
                index -= 1
            }
        }
        self.tbDeposite.reloadSections([2], with: .none)
        }
    }
    
    func hideAddressFields() {
        self.bottomcontant.constant = -50
        self.model.division = ""
        self.model.divisionCode = ""
        self.model.township = ""
        self.model.townshipCode = ""
        self.model.city = ""
        self.model.village = ""
        self.model.street = ""
        self.model.houseNumber = ""
        self.model.floorNumber = ""
        self.model.roomNumber = ""
        self.isDivisionHidden = true
        self.isBtnFullAddressHidden = true
        self.isBtnFullAddress = false
        self.isTownshipHidden = true
        self.isCityHidden = true
        self.isVillageHidden = true
        self.isStreeetHidden = true
        self.isHouseNumberHidden = true
    }
    
    
    func checkNumberRegisteredWithOKDoller(number: String, completion: @escaping(_ bool: Bool, _ data: Any?) -> Void) {
        self.view.endEditing(true)
        let urlString = "\(Url.URLgetOKacDetails)" + "\(number)"
        let trimmedString = urlString.components(separatedBy: .whitespaces).joined()
        let url = getUrl(urlStr: trimmedString, serverType: .serverApp)
        progressViewObj.showProgressView()
        
        
        
        
        JSONParser.GetApiResponse(apiUrl: url, type: "POST") {(status : Bool, data : Any?) in
             progressViewObj.removeProgressView()
            DispatchQueue.main.async {
                if status == true {
                    completion(true,data)
                } else {
                    if let _ = data as? String  {
                        completion(false,data)
                    } else {
                        completion(false,data)
                    }
                }
            }
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        
            alertViewObj.showAlert(controller: self)
        
    }
    
    func CheckNumberRegisteredAndMinMaxAmount(number: String, completion: @escaping(_ bool: Bool, _ data: Any?) -> Void) {
        let ur = getUrl(urlStr: Url.URL_GetCategoryApi, serverType: .faceIDPay)
        let dic = ["OsType":1,"DestinationNo":number,"SourceNo": UserModel.shared.mobileNo,"TransType":"PayTo","AppBuildNumber": buildNumber] as [String : Any]
        println_debug(dic)
        TopupWeb.genericClass(url: ur, param: dic as AnyObject, httpMethod: "POST") { [weak self](response, success) in
            if success {
                if let json = response as? Dictionary<String,Any> {
                    print(json)
                    if let code = json["code"] as? NSNumber, code == 200 {
                        completion(true, json["data"])
                    }else {
                        completion(false, json)
                    }
                }else {
                    DispatchQueue.main.async {
                        self?.showAlert(alertTitle: "", alertBody: "Request Failed. Please Try Again".localized, alertImage: UIImage(named: "alert-icon")!)}
                }
            }else {
                let json = Dictionary<String,Any>()
                completion(false, json)
                DispatchQueue.main.async {
                    self?.showAlert(alertTitle: "", alertBody: "Request Failed. Please Try Again".localized, alertImage: UIImage(named: "alert-icon")!)}
            }
        }
    }
    
    func resetCommissionAmount() {
        if self.showComField {
            if let cell = tbDeposite.cellForRow(at: IndexPath(row: 4, section: 0)) as? depositeAgentCommCell {
                let finalCom = (Int(self.model.TotalCommission ) ?? 0)
                cell.tfAmount.text = String(finalCom)
                self.model.TotalCommission = String(finalCom)
            }
        }
    }

    override var textInputMode: UITextInputMode? {
        let language = ok_default_language
        if language.isEmpty {
            return super.textInputMode
        } else {
            for tim in UITextInputMode.activeInputModes {
                if tim.primaryLanguage!.contains(language) {
                    return tim
                }
            }
            return super.textInputMode
        }
    }

    
    @objc func onClickCountry(_ sender: UITapGestureRecognizer) {
        DispatchQueue.main.async {
            let sb = UIStoryboard(name: "Country", bundle: nil)
            let countryVC = sb.instantiateViewController(withIdentifier: "CountryViewController") as! CountryViewController
            countryVC.modalPresentationStyle = .fullScreen
            countryVC.delegate = self
            countryVC.regiCheck = "allCountry"
            self.countryListShownFor = sender.name ?? ""
            self.navigationController?.present(countryVC, animated: false, completion: nil)
        }
    }
    
    @objc func onClickContactNumberContact(_ sender: UIButton){
        if sender.tag == 100 {
            contactListShownFor = "deposit"
        }else {
            contactListShownFor = "withdraw"
        }
        let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    func formateValidNumber(num: String,coutryCode: String) -> String {
        var finalNumber = ""
        if coutryCode == "+95" {
            finalNumber = "00" +  coutryCode.dropFirst() + num.dropFirst()
        }else {
            finalNumber = "00" + coutryCode.dropFirst() + num
        }
        return finalNumber
    }
    
    
    func getDigitDisplayColor(_ digit: String) -> String {
        var FormateStr: String
        let mystring = digit.replacingOccurrences(of: ",", with: "")
        let number = NSDecimalNumber(string: mystring)
        
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        FormateStr = formatter.string(from: number)!
        if FormateStr == "NaN"{
            FormateStr = ""
        }
        return FormateStr
    }
    
    func concatinateAmountWithMMK(amount : String) -> NSAttributedString {
        var myMutableString = NSMutableAttributedString()
        var myMutableString1 = NSMutableAttributedString()
        myMutableString1 = NSMutableAttributedString(string: ": " + amount, attributes: [NSAttributedString.Key.font:UIFont(name: appFont, size: 20.0) ?? UIFont.systemFont(ofSize: 20)])
        myMutableString = NSMutableAttributedString(string: " MMK", attributes: [NSAttributedString.Key.font:UIFont(name: appFont, size: 10.0) ?? UIFont.systemFont(ofSize: 10)])
        let concate = NSMutableAttributedString(attributedString: myMutableString1)
        concate.append(myMutableString)
        return concate
    }
    
    //Add this function
    @objc func searchForKeyword(_ timer: Timer) {
        if let cell = tbDeposite.cellForRow(at: IndexPath(row: 3, section: 0)) as? depositeAmountCell {
            if cell.tfAmount.text?.count ?? 0 > 0 {
                        if var burmese = cell.tfAmount.text {
                            burmese = burmese.replacingOccurrences(of: ",", with: "")
                            if let ch = burmese.last, ch == "." {
                            } else {
                                self.objectSound.getBurmeseSoundText(text: burmese)
                            }
                        }
            }
        }
    }
    
    @objc func checkMinAmount(_ timer: Timer) {
        self.minAmountCheck?.invalidate()
        miminumAlertShow()
    }
    @objc func checkMinAmountEndEdinting(_ timer: Timer) {
        self.minAmountCheckEndEditing?.invalidate()
        miminumAlertShow()
    }
    
    
    func miminumAlertShow() {
        if let cell = tbDeposite.cellForRow(at: IndexPath(row: 3, section: 0)) as? depositeAmountCell {
            let amout = Int(cell.tfAmount.text?.replacingOccurrences(of: ",", with: "") ?? "0")
            if amout ?? 0 < minAmount && cell.tfAmount.text?.count ?? 0 > 0 {
                alertViewObj.wrapAlert(title: "", body: "Minimum Deposit Amount".localized + " " + getDigitDisplay(String(minAmount)) + " " + "MMK", img: UIImage(named: "minimum_deposit")!)
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    cell.tfAmount.text = ""
                    self.depositorAmoutStatus = "Deposit Amount".localized
                    cell.lblStatusTitle.text = self.depositorAmoutStatus
                    cell.btnClear.isHidden = true
                    self.model.amount = ""
                    self.resetAgentCommission()
                    cell.tfAmount.becomeFirstResponder()
                })
                DispatchQueue.main.async {
                    alertViewObj.showAlert(controller: self)
                }
            }
        }else {
            let amout = Int(self.model.amount)
            if amout ?? 0 < minAmount {
                alertViewObj.wrapAlert(title: "", body: "Minimum Deposit Amount".localized + " " + getDigitDisplay(String(minAmount)) + " " + "MMK", img: UIImage(named: "minimum_deposit")!)
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    self.depositorAmoutStatus = "Deposit Amount".localized
                    self.model.amount = ""
                    self.resetAgentCommission()
                })
                DispatchQueue.main.async {
                    alertViewObj.showAlert(controller: self)
                }
            }
        }
    }
    
}

extension DepositeVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.previousYaxisScrolled = Int(scrollView.contentOffset.y)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.nextYaxisScrolled = Int(scrollView.contentOffset.y)
        if (nextYaxisScrolled + 140) > previousYaxisScrolled || (nextYaxisScrolled + 140) < previousYaxisScrolled {
            self.view.endEditing(true)
        }
    }

}
