//
//  PayConfirmationDeposite.swift
//  OK
//
//  Created by Imac on 7/23/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import CoreLocation
import Rabbit_Swift


class PayConfirmationDeposite: OKBaseController,WebServiceResponseDelegate,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tvPay: UITableView!
    var currentLat = ""
    var currentLong = ""
      var allLocationsList = [LocationDetail]()
    
    var mySelfDepostor: String?
    var mySelfWithdrawal: String?
    var depositorNumber: String?
    var depositorCountrycode: String?
    var depositorName: String?
    var amount: String?
    var depositorFaceURL: String?
    var depositorNRC: String?
    
    var withdrawNumber: String?
    var withdrawCountrycode: String?
    var dwithdrawName: String?
    var withdrawName: String?
    var withdrawFaceURL: String?
    var withdrawNRC: String?
    
    var state: String?
    var stateCode: String?
    var township: String?
    var city: String?
    var village: String?
    var street: String?
    var house: String?
    var floor: String?
    var room: String?
    var AgentTotalCom: String?
    var CommissionFrom : Bool?
    var formatedAddress = ""
    var timerCount = 59
    var timer = UILabel()
    var countTimer = Timer()
    var parmsData : Any?
    var failureViewModel = FailureViewModel()
    
    @IBOutlet weak var tbPay: UITableView!
    @IBOutlet weak var btnPAy: UIButton!{
        didSet{
            self.btnPAy.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var bottomConstraints: NSLayoutConstraint!
    
    var isFacePayChecked: String?
    var finalAmout = 0
    var netReceiveAmout = ""
    var failureCount = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        allLocationsList = TownshipManager.allDivisionList
        self.btnPAy.setTitle("Pay".localized, for:  .normal)
        self.btnPAy.isHighlighted = true
        self.btnPAy.isUserInteractionEnabled = false
        self.setMarqueLabelInNavigation()
        getAddressData()
        viewAtFooter()
        self.tvPay.delegate = self
        self.tbPay.dataSource = self
        bottomConstraints.constant = -50
        tbPay.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool){
        self.view.endEditing(true)
        failureCount = 0
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 16)
        lblMarque.text = "Confirmation".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white//init(hex: "#1C2C99")
        self.navigationItem.titleView = lblMarque
    }
    
    
    func viewAtFooter() {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100))
        customView.backgroundColor = UIColor.clear
        timer.frame = CGRect(x: (self.view.frame.width / 2) - 25 ,y: 10,width: 50 ,height: 50)
        timer.backgroundColor = UIColor.clear
        timer.textColor = UIColor.black
        timer.textAlignment = .center
        timer.layer.cornerRadius = 25.0
        timer.layer.masksToBounds = true
        timer.layer.borderColor = UIColor.black.cgColor
        timer.layer.borderWidth = 1.0
        timer.font = UIFont(name: appFont, size: 14)
        self.timer.text = "0:\(60)"
        customView.addSubview(timer)
        tvPay.tableFooterView = customView
        
        let timerString = UILabel(frame: CGRect(x: 0,y: 70 ,width: self.view.frame.width,height: 30))
        timerString.backgroundColor = UIColor.clear
        timerString.textColor = UIColor.black
        timerString.font = UIFont(name: appFont, size: 12)
        timerString.text  = "Seconds left".localized
        timerString.textAlignment = .center
        customView.addSubview(timerString)
        tvPay.tableFooterView = customView
    
        countTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { tim in
            self.timer.text = "0:\(self.timerCount)"
            if self.timerCount <= 9 {
                self.timer.layer.borderColor = UIColor.red.cgColor
                self.timer.textColor = UIColor.red
                timerString.textColor = UIColor.red
            }else {
               self.timer.layer.borderColor = UIColor.black.cgColor
               self.timer.textColor = UIColor.black
               timerString.textColor = UIColor.black
            }
            
            if self.timerCount <= 0 {
                self.countTimer.invalidate()
                alertViewObj.wrapAlert(title:"", body: "Session expired".localized, img: UIImage(named: "alert-icon")!)
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                  self.navigationController?.popViewController(animated: true)
                })
                DispatchQueue.main.async {
                    alertViewObj.showAlert(controller: self)
                }
            }
            self.timerCount -= 1
        }
    }
    
    func getAddressData() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined:
                println_debug("notDetermined")
                locationUpdates()
                break
            case .denied :
                println_debug("denied")
                showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                
                break
            case .restricted :
                println_debug("restricted")
                showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                break
            case .authorizedAlways, .authorizedWhenInUse:
                locationUpdates()
                break
            }
        } else {
            alertViewObj.wrapAlert(title:"", body: "Please Enable Location Service".localized, img:#imageLiteral(resourceName: "location"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func locationUpdates() {
        LocationManager.sharedInstance.getLocation { (location: CLLocation?, error) in
            if error != nil {
                println_debug(error.debugDescription)
                return
            }
            guard let _ = location else { return }
            if let location = location {
                let lat = String(location.coordinate.latitude)
                let long = String(location.coordinate.longitude)
                self.currentLat = lat
                self.currentLong = long
            }
        }
    }
    
    func stringBeforeSpace(str : String) -> String{
        let delimiter = " "
        let token = str.components(separatedBy: delimiter)
        return token[0]
    }


    @IBAction func onClickBackAction(_ sender: Any) {
        countTimer.invalidate()
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickPayAction(_ sender: Any) {
        saveDeposit()
    }
    
    
    func setHeightForAddress() ->Int {
        var height = 0
        if house != "" || floor != "" || room != ""{
           height = 1
        }
      
        if street != "" || village != ""{
            height =  height + 1
        }
       
        if city != "" {
             height =  height + 1
        }
        if township != "" || state != "" {
             height =  height + 1
        }

        return height
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
             return 0
        }else {
            if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 4 {
                return 60
            }else if indexPath.row == 5 {
                if withdrawNRC == "" {
                    return 0
                }else {
                  return 60
                }
            }else if indexPath.row == 6 {
                var add = ""
                if house != "" {
                    add = house!
                }
                if floor != "" {
                    add = add + floor!
                }
                if room != "" {
                    add = add + room!
                }
                if street != "" {
                    add = add + street!
                }
                if village != "" {
                    add = add + village!
                }
                if township != "" {
                    add = add + township!
                }

                if city != "" {
                    add = add + city!
                }

                if state != "" {
                    add = add + state!
                }
                
                if state == "" {
                    return 0
                }
                
               let lbl = UILabel()
                lbl.font = UIFont(name: appFont, size: 17)
        
                if ok_default_language == "my" || ok_default_language == "uni"{
                    if UitilityClass.heightForView(text: add, width: self.view.frame.width - 200, x: 0, y: 0, label: lbl) < 40 {
                        return 60
                    }else {
                        return UITableView.automaticDimension
                    }
                }else {
                    if UitilityClass.heightForView(text: add, width: self.view.frame.width - 200, x: 0, y: 0, label: lbl) < 40 {
                        return 60
                    }else {
                        return UITableView.automaticDimension
                    }
                }
            }else if indexPath.row == 7 {
                    return 150
            }else {
                return 0
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
           return 0
        }else {
        return 60
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 4
        }else {
            return 8
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
      
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "infoCellPay") as! infoCellPay
                cell.selectionStyle = .none
                cell.lblTitle.text = "Mobile Number".localized
                    cell.lblValue.text = "(" + depositorCountrycode! +  ")" + depositorNumber!
                return cell
            }else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "infoCellPay") as! infoCellPay
                    cell.selectionStyle = .none
                cell.lblTitle.text = "Name".localized
              
                cell.lblValue.text = depositorName
                   return cell
            }else if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "infoCellPay") as! infoCellPay
                cell.selectionStyle = .none
                cell.lblTitle.text = "Amount".localized
                cell.lblValue.attributedText = concatinateAmountWithMMK(amount: amount ?? "")
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "infoCellPay") as! infoCellPay
                    cell.selectionStyle = .none
                cell.lblTitle.text = "NRC Number".localized
                cell.lblValue.text = depositorNRC
                   return cell
            }
        }else {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "infoCellPay") as! infoCellPay
                    cell.selectionStyle = .none
                cell.lblTitle.text = "Mobile Number".localized
                    cell.lblValue.text = "(" + withdrawCountrycode! +  ")" + withdrawNumber!
                   return cell
            }else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "infoCellPay") as! infoCellPay
                    cell.selectionStyle = .none
                cell.lblTitle.text = "Name".localized
                cell.lblValue.text = withdrawName
                   return cell
            }else if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "infoCellPay") as! infoCellPay
                cell.selectionStyle = .none
                cell.lblTitle.text = "Net Receive Withdrawal Amount".localized
                
                if self.CommissionFrom ?? false {
                    if let amt = amount {
                        cell.lblValue.attributedText = concatinateAmountWithMMK(amount: getDigitDisplayColor(String((Int(amt) ?? 0))))
                    }
                }else {
                    if let amt = amount {
                        cell.lblValue.attributedText = concatinateAmountWithMMK(amount: getDigitDisplayColor(String((Int(amt) ?? 0) - (Int(self.AgentTotalCom ?? "") ?? 0))))
                    }
                }
                netReceiveAmout = cell.lblValue.text ?? ""
                return cell
            }else if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "infoCellPay") as! infoCellPay
                cell.selectionStyle = .none
                cell.lblTitle.text = "Commission Fees".localized
                cell.lblValue.attributedText = concatinateAmountWithMMK(amount: getDigitDisplayColor(self.AgentTotalCom ?? ""))
                return cell
            }else if indexPath.row == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "infoCellPay") as! infoCellPay
                    cell.selectionStyle = .none
                cell.lblTitle.text = "Net Paid Amount".localized
                if self.CommissionFrom ?? false {
                    if let amt = amount {
                        cell.lblValue.attributedText = concatinateAmountWithMMK(amount: getDigitDisplayColor(String((Int(amt) ?? 0) + (Int(self.AgentTotalCom ?? "") ?? 0))))
                    }
                }else {
                    if let amt = amount {
                        cell.lblValue.attributedText = concatinateAmountWithMMK(amount: getDigitDisplayColor(String((Int(amt) ?? 0))))
                    }
                }
                   return cell
            }else if indexPath.row == 5 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "infoCellPay") as! infoCellPay
                    cell.selectionStyle = .none
                cell.lblTitle.text = "NRC Number ".localized
                cell.lblValue.text = withdrawNRC
                   return cell
            }else if indexPath.row == 6 {
                 let cell2 = tableView.dequeueReusableCell(withIdentifier: "AddressCellPay") as! AddressCellPay
                    cell2.selectionStyle = .none
                cell2.lblTitle.text = "Address".localized
                var add = ""
                if house != "" {
                   add = house! + ","
                }
                if floor != "" {
                    add = add + floor! + ","
                }
                if room != "" {
                    add = add + room! + "," + "\n"
                }
                if street != "" {
                    add = add + street! + "," + "\n"
                }
                if village != "" {
                    add = add + village! + "," + "\n"
                }
                if township != "" {
                    add = add + township! + "," + "\n"
                }
                
                if city != "" {
                    add = add + city! + "," + "\n"
                }
               
                if state != "" {
                    add = add + state!
                }
                cell2.lblValue.text =  add
                formatedAddress = add
                return cell2
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmationDepositCell") as! ConfirmationDepositCell
                cell.selectionStyle = .none
                var agentNumber = ""
                var txt = ""
                
                let (mobileNumber,_) = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: UserModel.shared.mobileNo)
                if ok_default_language == "en" {
                agentNumber = "(" + "not for agent number" + " " + mobileNumber + ")"
                    cell.lblnote.text = "* " + "Only depositor need to be press" + " " + agentNumber
                txt = "Deposit Number" + " (" + depositorNumber! + ") " + "confirm that entered all above information are correct"
                }else if ok_default_language == "en"{
                   agentNumber = "(" + " ေအးဂ်င့္" + " " + depositorNumber! + " ႏွိပ္ရန္ မဟုတ္ပါ)"
                cell.lblnote.text = "* " + "ေငြလႊဲသူမွ ႏွိပ္ရန္သာ" + " " + agentNumber
                txt = "(" + depositorNumber! + ") " + "မွ ေငြသြင္းသည့္ အထက္ေဖာ္ျပပါ အခ်က္အလက္မ်ား မွန္ကန္ေၾကာင္း အတည္ျပဳရန္။"
                }else {
                    agentNumber = "(" + " အေးဂျင့်" + " " + depositorNumber! + " နှိပ်ရန် မဟုတ်ပါ)"
                    cell.lblnote.text = "* " + "ငွေလွှဲသူမှ နှိပ်ရန်သာ" + " " + agentNumber
                    txt = "(" + depositorNumber! + ") " + "မှ ငွေသွင်းသည့် အထက်ဖော်ပြပါ အချက်အလက်များ မှန်ကန်ကြောင်း အတည်ပြုရန်။"
                    
                    

                }
                cell.lblDeclare.text = txt
                cell.btnAccept.addTarget(self, action: #selector(AcceptTerms(_:)), for: .touchUpInside)
                return cell
            }
        }
    }

    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return self.createRequiredFieldLabel(headerTitle: "Depositor Information".localized, txtAlignment: .center, gcRect: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 40), section: section)
        }else if section == 1 {
            return self.createRequiredFieldLabel(headerTitle: "Withdrawal Information".localized, txtAlignment: .center, gcRect: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 40), section: section)
        }
        let view = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 0))
        view.backgroundColor = hexStringToUIColor(hex: "#E0E0E0")
        return view
    }
    private func createRequiredFieldLabel(headerTitle:String, txtAlignment: NSTextAlignment, gcRect: CGRect, section: Int) ->UIView? {
        let view1 = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: gcRect.height))
        view1.backgroundColor = UIColor.init(hex: "#EBEBEB")
        let myString = headerTitle
        let stringSize: CGSize = myString.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18.0) ])
        let lblHeader = UILabel(frame: CGRect(x: (self.view.frame.width / 2 ) - (stringSize.width / 2) , y: 10, width: stringSize.width, height: view1.frame.height))
        lblHeader.text = headerTitle
        lblHeader.numberOfLines = 3
        lblHeader.textAlignment = txtAlignment
        lblHeader.font = UIFont(name: appFont, size: 14)
        if section == 0 {
            lblHeader.backgroundColor = UIColor.init(hex: "#1C2C99")
        }else {
            lblHeader.backgroundColor = UIColor.init(hex: "#C71C00")
        }
        
        lblHeader.textColor = UIColor.white//init(hex: "#1C2C99")
        lblHeader.layer.masksToBounds = true
        lblHeader.layer.cornerRadius = lblHeader.frame.height / 2
        view1.addSubview(lblHeader)
        return view1
    }
    
    @objc func AcceptTerms(_ sender: UIButton) {
        if let cell = tbPay.cellForRow(at: IndexPath(row: 7, section: 1)) as? ConfirmationDepositCell {
            sender.isSelected  = !sender.isSelected
            if sender.isSelected {
                 bottomConstraints.constant = 0
                cell.imgCheck.image = UIImage(named: "checkboxTick")
                self.btnPAy.isHighlighted = false
                self.btnPAy.isUserInteractionEnabled = true
            }else {
                bottomConstraints.constant = -50
                cell.imgCheck.image = UIImage(named: "check_box")
                self.btnPAy.isHighlighted = true
                self.btnPAy.isUserInteractionEnabled = false
            }
        }
    }
    
    func saveDeposit() {
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let web      = WebApiClass()
            web.delegate = self
            let ur = getUrl(urlStr: Url.URL_SaveDeposit, serverType: .faceIDPay)
            let depositNumber = formateValidNumber(num: depositorNumber!, coutryCode: depositorCountrycode!)
            let withdrawNum = formateValidNumber(num: withdrawNumber!, coutryCode: withdrawCountrycode!)
         
            var facePay = false
            if isFacePayChecked == "true" {
                facePay = true
            }else {
                 facePay = false
            }
            
            var boolSelfWithDraw = false
            if mySelfWithdrawal == "1" {
                boolSelfWithDraw = true
            }else {
                boolSelfWithDraw = false
            }
            
            let wtdInfo = WithdrawInfo.init(state: stateCode ?? "", mobileNo: withdrawNum, name: withdrawName ?? "", myself: boolSelfWithDraw, nrcPassport: withdrawNRC ?? "", profileImg: withdrawFaceURL ?? "", address: formatedAddress,facePay: facePay,withDrawerAddress: formatedAddress)
       
            var boolSelfDeposite = false
            if mySelfDepostor == "1" {
                boolSelfDeposite = true
            }else {
                boolSelfDeposite = false
            }
            
            var comAmount = "0"
            
            if self.CommissionFrom ?? false {
               self.finalAmout = (Int(self.amount ?? "") ?? 0) + (Int(self.AgentTotalCom ?? "") ?? 0)
                comAmount = self.AgentTotalCom ?? ""
            }else {
               self.finalAmout = (Int(self.amount ?? "") ?? 0)
            }
            
            let dptInfo = DepositInfo.init(amt: String(finalAmout), commissionAmt : comAmount, mobileNo: depositNumber, name: depositorName ?? "", myself: boolSelfDeposite, nrcPassport: depositorNRC ?? "", profileImg: depositorFaceURL ?? "", transType: 0)

            let appinfo = DepositAppInfo.init(cellID: "", lang: currentLong, lat: currentLat, merchantType: agentServiceTypeString(type: UserModel.shared.agentType), mobileNo: UserModel.shared.mobileNo, msID: msid, profileImg: UserModel.shared.proPic, simID: uuid,transactionType: "PAYTO", secureToken: UserLogin.shared.token, password: ok_password ?? "", transType: 0)
            
            let model = FinalDic.init(withdrawerInfo: wtdInfo, depositorInfo: dptInfo, appInfo: appinfo)
            var data = Data()
            
            do {
              data  =  try JSONEncoder().encode(model)
            } catch {
                println_debug(error)
            }
              do {
                    let dicFromData = try PropertyListSerialization.propertyList(from: data, options: PropertyListSerialization.ReadOptions.mutableContainers, format: nil)
                        if let dictA = dicFromData as? [String: Any]{
                            self.parmsData = dictA
                        }
                    } catch{
                        print(error)
                    }
            println_debug(ur)
            println_debug(data)
            println_debug(String(data: data, encoding: .utf8))
            web.genericClassReg(url: ur, param: Dictionary<String,Any>(), httpMethod: "POST", mScreen: "SaveDeposite", hbData: data, authToken: nil)
        }else {
            alertViewObj.wrapAlert(title: nil, body: "Please check your internet connection".localized, img: UIImage(named: "alert-icon")!)
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    private func agentServiceTypeString(type: AgentType) -> String {
        switch type {
        case .user:
            return "SUBSBR"
        case .merchant:
            return "MER"
        case .agent:
            return "AGENT"
        case .advancemerchant:
            return "ADVMER"
        case .dummymerchant:
            return "DUMMY"
        case .oneStopMart:
            return "ONESTOP"
        }
    }
    func webResponse(withJson json: AnyObject, screen: String) {
        self.removeProgressView()
        do {
            if let data = json as? Data {
                let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dic = dict as? Dictionary<String,Any> {
                     println_debug(dic)
                    if dic["code"] as? Int == 200 {
                        if let data = dic["data"] as? Dictionary<String,Any> {
                           
                            DispatchQueue.main.async {
                                self.countTimer.invalidate()
                                let amt = data["EstelAmount"] as? String ?? ""
                                let arrAmount = amt.components(separatedBy: ".")
                                
                                var withdrawAmout = 0
                                var TotalAmount = 0
                                if self.CommissionFrom ?? false {
                                    withdrawAmout = (Int(arrAmount[0] ) ?? 0) - (Int(self.AgentTotalCom ?? "") ?? 0)
                                    TotalAmount = (Int(arrAmount[0] ) ?? 0)
                                }else {
                                    withdrawAmout = (Int(arrAmount[0] ) ?? 0) - (Int(self.AgentTotalCom ?? "") ?? 0)
                                    TotalAmount = (Int(self.amount ?? "") ?? 0)
                                }
                             
                                let amtWallet = data["EstelPostWallet"] as? String ?? ""
                                let arrWallet = amtWallet.components(separatedBy: ".")
                                let recieptVC = self.storyboard?.instantiateViewController(withIdentifier: "DepositeRecieptVC") as! DepositeRecieptVC
                                recieptVC.depositDic = ["DepositorName": self.depositorName ?? "", "DepositorNumber": self.depositorNumber ?? "","depositorCountrycode": self.depositorCountrycode, "Amount": String(TotalAmount),"Withdraw_Amount": String(withdrawAmout) ,"WalletAmount": self.getDigitDisplayColor(arrWallet[0]), "WithdrawName": self.withdrawName ?? "","WithdrawNumber": self.withdrawNumber ?? "","withdrawCountrycode": self.withdrawCountrycode,"TranId": data["EstelOkTransid"] as? String,"CommissionFee": self.AgentTotalCom ?? "","NetReceiveAmount" : self.netReceiveAmout] as? Dictionary<String, String>
                                self.navigationController?.pushViewController(recieptVC, animated: true)
                            }
                        }
                    }else if dic["code"] as? Int == 300  {
                        DispatchQueue.main.async {
                            self.showAlert(alertTitle: "", alertBody: "You can't deposit on this number due to last deposited amount did't withdraw from withdrawal".localized, alertImage: UIImage(named: "no_deposit")!)}
                    }else if dic["code"] as? Int == 400  {
                        DispatchQueue.main.async {
                            self.showAlert(alertTitle: "", alertBody: "Amount has not been deposited to receiver account".localized, alertImage: UIImage(named: "no_deposit")!)}
                    }
                    if dic["code"] as? Int != 200 {
                        self.failureCount = self.failureCount + 1
                        if self.failureCount == 2 {
                            self.failureViewModel.sendDataToFailureApi(request: self.parmsData as Any, response: dic , type: FailureHelper.FailureType.PAYTO.rawValue, finished: {
                                
                            })
                        }
                    }
                }
            }else {
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "", alertBody: "Request Failed. Please Try Again".localized, alertImage: UIImage(named: "error")!)}
            }
        }catch {}
    }
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    func formateValidNumber(num: String,coutryCode: String) -> String {
        var finalNumber = ""
        if coutryCode == "+95" {
            finalNumber = "00" +  coutryCode.dropFirst() + num.dropFirst()
        }else {
            finalNumber = "00" + coutryCode.dropFirst() + num
        }
        return finalNumber
    }
    func getDigitDisplayColor(_ digit: String) -> String {
        var FormateStr: String
        let mystring = digit.replacingOccurrences(of: ",", with: "")
        let number = NSDecimalNumber(string: mystring)
        
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        FormateStr = formatter.string(from: number)!
        if FormateStr == "NaN"{
            FormateStr = ""
        }
        return FormateStr
    }
    
    func concatinateAmountWithMMK(amount : String) -> NSAttributedString {
        var myMutableString = NSMutableAttributedString()
        var myMutableString1 = NSMutableAttributedString()
        myMutableString1 = NSMutableAttributedString(string: amount, attributes: [NSAttributedString.Key.font:UIFont(name: appFont, size: 20.0) ?? UIFont.systemFont(ofSize: 20)])
        myMutableString = NSMutableAttributedString(string: " MMK", attributes: [NSAttributedString.Key.font:UIFont(name: appFont, size: 10.0) ?? UIFont.systemFont(ofSize: 10)])
        let concate = NSMutableAttributedString(attributedString: myMutableString1)
        concate.append(myMutableString)
        return concate
    }

    
    
}




class infoCellPay: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!{
        didSet{
            self.lblTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    
    @IBOutlet weak var lblValue: UILabel!
    
}

class AddressCellPay: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!{
        didSet{
            self.lblTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblValue: UILabel!
}


class DepositeTimerCell: UITableViewCell {
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var lblTimeString: UILabel!
}
class AgentComHeaderCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAgentCom: UILabel!
    @IBOutlet weak var lblAgentComAmount: UILabel!
    @IBOutlet weak var btnShowFullFees: UIButton!
}
class AgentComCell: UITableViewCell {
    @IBOutlet weak var lblAgentCom: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
}

class ConfirmationDepositCell: UITableViewCell {
    @IBOutlet weak var lblnote: UILabel!
    @IBOutlet weak var lblDeclare: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var viewDeclare: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewDeclare.layer.cornerRadius = 5
        viewDeclare.layer.masksToBounds = true
        viewDeclare.layer.borderColor = UIColor.lightGray.cgColor
         viewDeclare.layer.borderWidth = 1.0
        imgCheck.image = UIImage(named: "check_box")
    }
    
}



struct FinalDic: Codable {
    let withdrawerInfo: WithdrawInfo?
    let depositorInfo: DepositInfo?
    let appInfo: DepositAppInfo?
    
    enum CodingKeys: String, CodingKey {
        case withdrawerInfo = "WithdrawerInfo"
        case depositorInfo = "DepositorInfo"
        case appInfo = "AppInfo"
    }
}

struct DepositAppInfo: Codable {
    let cellID, lang, lat, merchantType: String
    let mobileNo, msID, profileImg, simID: String
    let transactionType, secureToken, password: String
    let transType : Int
    
    enum CodingKeys: String, CodingKey {
        case cellID = "CellId"
        case lang = "Lang"
        case lat = "Lat"
        case merchantType = "merchant_type"
        case mobileNo = "MobileNo"
        case msID = "MsId"
        case profileImg = "ProfileImg"
        case simID = "SimId"
        case transType = "TransType"
        case transactionType = "TransactionType"
        case secureToken = "SecureToken"
        case password = "Password"
    }
}

struct DepositInfo: Codable {
    let amt,commissionAmt ,mobileNo, name: String
    let myself: Bool
    let nrcPassport, profileImg: String
    let transType: Int
    
    enum CodingKeys: String, CodingKey {
        case amt = "Amt"
        case commissionAmt = "CommissionAmt"
        case mobileNo = "MobileNo"
        case myself = "Myself"
        case name = "Name"
        case nrcPassport = "NrcPassport"
        case profileImg = "ProfileImg"
        case transType = "TransType"
    }
}

struct WithdrawInfo: Codable {
    let state, mobileNo, name: String
    let myself: Bool
    let nrcPassport, profileImg: String
    let address: String
    let facePay: Bool
    let withDrawerAddress: String
    enum CodingKeys: String, CodingKey {
        case state = "State"
        case mobileNo = "MobileNo"
        case myself = "Myself"
        case name = "Name"
        case nrcPassport = "NrcPassport"
        case profileImg = "ProfileImg"
        case address = "Address"
        case facePay = "FacePay"
        case withDrawerAddress = "withDrawerAddress"
    }
}
