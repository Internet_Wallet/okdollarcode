//
//  DepositPDF.swift
//  OK
//
//  Created by shubh's MacBookPro on 9/10/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class DepositPDF: OKBaseController {
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var lblFaceTitle: UILabel!
    
    @IBOutlet weak var lblDepositorName: UILabel!
    @IBOutlet weak var lblDepositorNameInput: UILabel!
    @IBOutlet weak var lblDepoMobNumber: UILabel!
    @IBOutlet weak var lblDepoMobMunInput: UILabel!
    @IBOutlet weak var lblWithName: UILabel!
    @IBOutlet weak var lblWithNameInput: UILabel!
    @IBOutlet weak var lblWithMobNum: UILabel!
    @IBOutlet weak var lblWithMobNumInput: UILabel!
    @IBOutlet weak var lblAgentAccNum: UILabel!
    @IBOutlet weak var lblAgentAccNumInput: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblCategoryInput: UILabel!
    @IBOutlet weak var lblComFee: UILabel!
    @IBOutlet weak var lblComFeeInput: UILabel!
    @IBOutlet weak var lblNetreceivAmount: UILabel!
    @IBOutlet weak var lblNetReceivAmountInput: UILabel!
    
    @IBOutlet weak var lblTransID: UILabel!
    @IBOutlet weak var lblTansIDInput: UILabel!
    @IBOutlet weak var lblTransType: UILabel!
    @IBOutlet weak var lblTransTypeInput: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblDatetimeInpput: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblAmountInout: UILabel!
    
    @IBOutlet weak var amoutView: UIView!
    var depositDic : Dictionary<String,String>?
    @IBOutlet weak var btnShare: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblFaceTitle.text = "Face ID Pay Receipt".localized
        
        lblDepositorName.text = "Depositor NameF".localized
        if let name = depositDic?["DepositorName"] {
          lblDepositorNameInput.text = ": " + name
        }
        
        lblDepoMobNumber.text = "Depositor Mobile Number".localized
        if let nubmer = depositDic?["DepositorNumber"] {
           lblDepoMobMunInput.text =  ": " + nubmer
        }
      
        lblWithName.text = "Withdrawal Name".localized
        if let name = depositDic?["WithdrawarName"] {
            lblWithNameInput.text = ": " + name
        }
      
        lblWithMobNum.text = "Withdrawal Mobile Number".localized
        if let number = depositDic?["WithdrawarNumber"] {
              lblWithMobNumInput.text = ": " +  number
        }
        
        lblAgentAccNum.text = "Agent Account Number".localized
        if let number = depositDic?["AgentNumber"] {
           lblAgentAccNumInput.text = ": " + number
        }
       
        lblCategory.text = "Categories".localized
        lblCategoryInput.text = ": " + "Face ID Pay"
        
        lblComFee.text = "Commission Fees".localized
        if let fee = depositDic?["Commission_Fees"] {
            lblComFeeInput.attributedText = concatinateAmountWithMMK(amount: ": " + getDigitDisplayColor(fee))
        }
        
        lblNetreceivAmount.text = "Net Receive Withdrawal Amount".localized
        if let fee = depositDic?["NetReceiveAmount"] {
             lblNetReceivAmountInput.attributedText = concatinateAmountWithMMK(amount: ": " + getDigitDisplayColor(fee))
        }
      
        lblTransID.text = "Transaction ID".localized
        if let id = depositDic?["TranID"] {
             lblTansIDInput.text = ": " + id
        }
     
        lblTransType.text = "Transaction Type".localized
        lblTransTypeInput.text = ": " + "PAYTO"
        
        lblDateTime.text = "Date & Time".localized
        if let time = depositDic?["DateTime"] {
           lblDatetimeInpput.text = ": " + time
        }
      
        lblAmount.text = "Amount Paid".localized
        if let amount = depositDic?["Amount"] {
            lblAmountInout.attributedText = concatinateAmountWithMMK(amount: getDigitDisplayColor(amount))
        }
        amoutView.layer.borderWidth = 1.0
        amoutView.layer.borderColor = UIColor.black.cgColor
        btnShare.setTitle("Share".localized, for: .normal)
        setMarqueLabelInNavigation()
    }
    
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 16)
        lblMarque.text = "Invoice".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white//init(hex: "#1C2C99")
        self.navigationItem.titleView = lblMarque
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickPdf(_ sender: Any) {
        if self.contentView.exportAsPdfFromView(pdfName: "Face_ID_Receipt") == "Success"{
            var pdfURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
            pdfURL = pdfURL.appendingPathComponent("Face_ID_Receipt.pdf") as URL
            let activityViewController = UIActivityViewController(activityItems: [pdfURL], applicationActivities: nil)
            DispatchQueue.main.async {self.navigationController?.present(activityViewController, animated: true, completion: nil)}
        }
    }

    func concatinateAmountWithMMK(amount : String) -> NSAttributedString {
        var myMutableString = NSMutableAttributedString()
        var myMutableString1 = NSMutableAttributedString()
        myMutableString1 = NSMutableAttributedString(string: amount, attributes: [NSAttributedString.Key.font:UIFont(name: appFont, size: 20.0) ?? UIFont.systemFont(ofSize: 20)])
        myMutableString = NSMutableAttributedString(string: " MMK", attributes: [NSAttributedString.Key.font:UIFont(name: appFont, size: 10.0) ?? UIFont.systemFont(ofSize: 10)])
        let concate = NSMutableAttributedString(attributedString: myMutableString1)
        concate.append(myMutableString)
        return concate
    }
    func getDigitDisplayColor(_ digit: String) -> String {
        var FormateStr: String
        let mystring = digit.replacingOccurrences(of: ",", with: "")
        let number = NSDecimalNumber(string: mystring)
        
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        FormateStr = formatter.string(from: number)!
        if FormateStr == "NaN"{
            FormateStr = ""
        }
        return FormateStr
    }
}


extension UIView {
    
    // Export pdf from Save pdf in drectory and return pdf file path
    func exportAsPdfFromView(pdfName: String) -> String {
        let pdfPageFrame = self.bounds
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageFrame, nil)
        UIGraphicsBeginPDFPageWithInfo(pdfPageFrame, nil)
        guard let pdfContext = UIGraphicsGetCurrentContext() else { return "" }
        self.layer.render(in: pdfContext)
        UIGraphicsEndPDFContext()
        return self.saveViewPdf(data: pdfData,name: pdfName)
    }
    
    // Save pdf file in document directory
    func saveViewPdf(data: NSMutableData,name: String) -> String {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docDirectoryPath = paths[0]
        let pdfPath = docDirectoryPath.appendingPathComponent("\(name).pdf")
        if data.write(to: pdfPath, atomically: true) {
            return "Success"
        } else {
            return ""
        }
    }
}
