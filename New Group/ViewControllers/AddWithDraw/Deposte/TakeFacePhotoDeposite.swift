//
//  TakeFacePhotoDeposite.swift
//  OK
//
//  Created by Imac on 7/30/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import CoreMedia
import Vision
import AVFoundation
import Photos

protocol TakeFacePhotoDepositeDelegate {
    func facePhoto(urlString: String, realImage: UIImage, screen: String,viewController: UIViewController)
    func DismissCameraController(viewController: UIViewController)
}


class TakeFacePhotoDeposite: OKBaseController, UIImagePickerControllerDelegate, UINavigationControllerDelegate,SuccessViewAddWDDelegate {
    
    @IBOutlet weak var lblTitleView: UILabel!
    @IBOutlet var viewWarning: UIView!
    @IBOutlet weak var btnOKView: UIButton!
    
    @IBOutlet weak var optionView: UIView!
    var delegate: TakeFacePhotoDepositeDelegate?
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnGallery: UIButton!
    @IBOutlet weak var btnFacebook: UIButton!
    var urlIamge = UIImage()
    var screenFor: String?
    var token = ""
    var urlImageString = ""
    var alertView = Bundle.main.loadNibNamed("SuccessViewAddWD", owner: self, options: nil)?[0] as! SuccessViewAddWD
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewWarning.isHidden = true
        btnCamera.setTitle("Camera".localized, for: .normal)
        btnGallery.setTitle("Gallery".localized, for: .normal)
        btnFacebook.setTitle("Facebook", for: .normal)
        btnOKView.setTitle("OK".localized, for: .normal)
        btnCamera.layer.cornerRadius = 25.0
        btnCamera.layer.masksToBounds = true
        btnGallery.layer.cornerRadius = 25.0
        btnGallery.layer.masksToBounds = true
        btnFacebook.layer.cornerRadius = 25.0
        btnFacebook.layer.masksToBounds = true
        setMarqueLabelInNavigation()
    }
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 16)
        lblMarque.text = "Take Photo".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white//init(hex: "#1C2C99")
        self.navigationItem.titleView = lblMarque
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    @IBAction func onClickWarningView(_ sender: Any) {
        viewWarning.isHidden = true
        if self.viewWarning.tag == 100 {
            presentGelleryView()
        }
    }
    
    @IBAction func onClickGalleryAction(_ sender: Any) {
        optionView.isHidden = true
        self.viewWarning.frame = CGRect(x: 0, y: self.view.frame.height - 245, width: self.view.frame.width, height: 245)
        self.viewWarning.isHidden = false
        self.viewWarning.tag = 100
        self.lblTitleView.text = "Don’t attach with your full body image. Please attach only your face photo because we could not scan your face.".localized
        self.view.addSubview(self.viewWarning)
    }
    
    func presentGelleryView() {
        
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.authorized) {
            self.OpenGallaryAfterPermission()
        }else if (status == PHAuthorizationStatus.denied) {
            self.OpenSettings(message: "Please Allow Photo Access".localized)
        }else if (status == PHAuthorizationStatus.notDetermined) {
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                if (newStatus == PHAuthorizationStatus.authorized) {
                    self.OpenGallaryAfterPermission()
                }else {
                   self.OpenSettings(message: "Please Allow Photo Access".localized)
                }
            })
        }else if (status == PHAuthorizationStatus.restricted) {
            self.OpenSettings(message: "Please Allow Photo Access".localized)
        }
    }
    
    
    func OpenGallaryAfterPermission() {
        DispatchQueue.main.async {
            let imagePicker = UIImagePickerController()
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            imagePicker.title = "Gallery".localized
            imagePicker.sourceType = .photoLibrary
            imagePicker.modalPresentationStyle = .fullScreen
            self.viewWarning.isHidden = true
            self.optionView.isHidden = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func OpenSettings(message: String) {
        alertViewObj.wrapAlert(title:"", body: message, img: UIImage(named: "alert-icon"))
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary1111([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            }
            self.navigationController?.popViewController(animated: true)
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    
    @IBAction func onClickCameraAction(_ sender: Any) {
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            self.OpenCamera()
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    self.OpenCamera()
                } else {
                    self.OpenSettings(message: "Please Allow Camera Access".localized)
                }
            })
        }
  
    }
    
    func OpenCamera(){
        DispatchQueue.main.async {
            let imagePicker = UIImagePickerController()
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            imagePicker.title = "Camera".localized
            imagePicker.sourceType = .camera
            imagePicker.modalPresentationStyle = .fullScreen
            self.viewWarning.frame = CGRect(x: 0, y: self.view.frame.height - 245, width: self.view.frame.width, height: 245)
            self.viewWarning.isHidden = false
            self.viewWarning.tag = 200
            self.lblTitleView.text = "Please take clear face photo for withdraw money process".localized
            self.optionView.isHidden = true
            imagePicker.view.addSubview(self.viewWarning)
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    
    @IBAction func onClickFacebookAction(_ sender: Any) {
        if !(UitilityClass.openApplication(applicationName: "fb")){
            showAlert(alertTitle: "", alertBody: "Please Install FaceBook Application".localized, alertImage: #imageLiteral(resourceName: "tbNotif_facebook"))
        }
    }
    
    @IBAction func onTouchView(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let info = convertFromUIImagePickerControllerInfoKeyDictionaryFacePhotoDeposide(info)
        
        if let image = info[convertFromUIImagePickerControllerInfoKey1FacePhotoDeposite(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            urlIamge = resizeForFacePay(image)
            self.getTokenForNRCScan(img: urlIamge)
        }
        dismiss(animated: false, completion: nil)
    }

    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[convertFromUIImagePickerControllerInfoKey1FacePhotoDeposite(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            urlIamge = resizeForFacePay(pickedImage)
            self.getTokenForNRCScan(img: urlIamge)
        }
        dismiss(animated: false, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        viewWarning.isHidden = true
       // optionView.isHidden = false
        dismiss(animated: false, completion: nil)
        if let del = delegate {
            del.DismissCameraController(viewController: self)
        }
    }
    
    
    func getTokenForNRCScan(img: UIImage) {
        if appDelegate.checkNetworkAvail() {
            guard let url = URL(string: "https://iam.ap-southeast-1.myhwclouds.com/v3/auth/tokens") else {
                return
            }
            let dic =  ["auth":["identity":["methods":["password"],"password":["user":["domain":["name":"OkdollarAndroid"],"name":"OkdollarAndroid","password":"okdollar!@#"]]],"scope":["project":["id":"7fdc0f6d4f8e42a38691e4e32d684551"]]]]
            TopupWeb.genericClassWithHeaderInfo(url:url, param: dic as AnyObject, httpMethod: "POST", handle: {response, isSuccess in
                if isSuccess {
                    if let headerRes = response as? HTTPURLResponse {
                        self.token = headerRes.allHeaderFields["X-Subject-Token"] as? String ?? ""
                        println_debug(self.token)
                        DispatchQueue.main.async {
                             let imageData = img.pngData() as NSData?
                            self.imageUploadDetect(imgName : "FaceImage.png", fileData : imageData! as Data, urlIamge: img)
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "", alertBody: "Network error and try again".localized, alertImage: UIImage(named : "alert-icon")!)}
                }
            })
        }else {
            DispatchQueue.main.async {
                self.showAlert(alertTitle: "", alertBody: "Network error and try again".localized, alertImage: UIImage(named : "alert-icon")!)}
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func resizeForFacePay(_ image: UIImage) -> UIImage {
        var actualHeight = Float(image.size.height)
        var actualWidth = Float(image.size.width)
        let maxHeight: Float = 450.0
        let maxWidth: Float = 300.0
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        var compressionQuality: Float = 0.0
        if device.type == .iPhoneX || device.type == .iPhoneXR || device.type == .iPhoneXS || device.type == .iPhoneXSMax {
            compressionQuality = 0.4
        }else {
            compressionQuality = 0.7
        }
        compressionQuality = 1
        //50 percent compression
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img?.jpegData(compressionQuality: CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        let imageSize: Int = imageData!.count
        print("size of image in KB: %f ", Double(imageSize) / 1024.0)
      //  UIImageWriteToSavedPhotosAlbum(UIImage(data: imageData!) ?? UIImage(), self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        return UIImage(data: imageData!) ?? UIImage()
    }
    
//    //MARK: - Add image to Library
//    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
//        if let error = error {
//            //showAlert(alertTitle: "Save error", alertBody: "", alertImage: UIImage(named: "r_male")!)
//        } else {
//           // showAlert(alertTitle: "Saved successs", alertBody: "", alertImage: UIImage(named: "r_male")!)
//        }
//    }
    
    func imageUploadDetect(imgName : String, fileData : Data, urlIamge: UIImage) {
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            let url = getUrl(urlStr: Url.URL_FacePayFaceDetect, serverType: .faceIDPay)
            let request = NSMutableURLRequest(url:url)
            request.httpMethod = "POST";
            let boundary = "Boundary-\(uuid)"
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            let parameters: [String: Any] = [
                "Type": "ImageUpload",
                "TestType": "live",
                "Token": token,
            ]
            println_debug(url)
            println_debug("Face Detect Params: \(parameters)")
            request.httpBody = createBodyWithParameters(parameters: parameters as? [String : String], filePathKey: imgName , imageDataKey: fileData as Data, boundary: boundary) as Data
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                (data, response, error) in
                progressViewObj.removeProgressView()
                if error != nil {
                    return
                }
                println_debug(response)
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 201 || httpStatus.statusCode == 200 {
                    do {
                        if let dd = data {
                            let dict = try JSONSerialization.jsonObject(with: dd, options: .allowFragments)
                            println_debug(dict)
                            if let dic = dict as? Dictionary<String,Any> {
                                if dic["code"] as? NSNumber == 200 {
                                    if let arr = dic["data"] as? Array<Any> {
                                        if arr.count == 1 {
                                        println_debug("One Face")
                                        self.imageUpload(imgName : imgName, fileData :fileData, urlIamge: urlIamge)
                                        }else if arr.count > 1 {
                                         println_debug("More than One Face")
                                        DispatchQueue.main.async {
                                                alertViewObj.wrapAlert(title: nil, body: "More than 1 faces are there Please reselect again".localized, img: UIImage(named: "face_no_match")!)
                                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                                    self.dismiss(animated: false, completion: nil)
                                                }
                                                alertViewObj.showAlert(controller: self)
                                            }
                                        }else {
                                            DispatchQueue.main.async {
                                                alertViewObj.wrapAlert(title: nil, body: "No Face recognized".localized, img: UIImage(named: "face_no_match")!)
                                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                                    self.dismiss(animated: false, completion: nil)
                                                }
                                                alertViewObj.showAlert(controller: self)
                                            }

                                        }
                                    }
                                }else {
                                    DispatchQueue.main.async {
                                        alertViewObj.wrapAlert(title: nil, body: "No Face recognized".localized, img: UIImage(named: "face_no_match")!)
                                        alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                            self.dismiss(animated: false, completion: nil)
                                        }
                                        alertViewObj.showAlert(controller: self)
                                    }
                                }
                                println_debug(dic)
                            }
                        }
                    } catch { }
                    println_debug("statusCode should be 200, but is \(httpStatus.statusCode)")
                }
            }
            task.resume()
        }
    }

    
    func imageUpload(imgName : String, fileData : Data, urlIamge: UIImage) {
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            let url = getUrl(urlStr: Url.URL_FacePayFileUpload, serverType: .faceIDPay)
            println_debug(url)
            let request = NSMutableURLRequest(url:url)
            request.httpMethod = "POST";
            let boundary = "Boundary-\(uuid)"
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            let parameters: [String: Any] = [
                "Type": "ImageUpload",
                "TestType": "live",
            ]
            request.httpBody = createBodyWithParameters(parameters: parameters as? [String : String], filePathKey: imgName , imageDataKey: fileData as Data, boundary: boundary) as Data
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                (data, response, error) in
                progressViewObj.removeProgressView()
                if error != nil {
                    return
                }
                println_debug(response)
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 201 || httpStatus.statusCode == 200 {
                    do {
                        if let dd = data {
                            let dict = try JSONSerialization.jsonObject(with: dd, options: .allowFragments)
                            if let dic = dict as? Dictionary<String,Any> {
                                if dic["code"] as? NSNumber == 200 {
                                    if let url = dic["data"] as? String {
                                        let url = url.replacingOccurrences(of: " ", with: "%20")
                                        self.urlImageString = url
                                        self.urlIamge = urlIamge
                                        self.showSuccessAlert()
                                        
//                                        if let del = self.delegate {
//                                            del.facePhoto(urlString: url, realImage: urlIamge, screen: self.screenFor ?? "", viewController: self)
//                                        }
//
                                    }
                                }else {
                                    DispatchQueue.main.async {
                                        alertViewObj.wrapAlert(title: nil, body: "Image uploading failed. Please try again".localized, img: #imageLiteral(resourceName: "info_one"))
                                        alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                            self.dismiss(animated: false, completion: nil)
                                        }
                                        alertViewObj.showAlert(controller: self)
                                    }
                                }
                                println_debug(dic)
                            }
                        }
                        } catch { }
                    println_debug("statusCode should be 200, but is \(httpStatus.statusCode)")
  
                }
            }
            task.resume()
        }
    }
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> NSData {
        var body = Data()
        if parameters != nil {
            for (key, value) in parameters! {
                body.append(Data("--\(boundary)\r\n".utf8))
                body.append(Data("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
                body.append(Data("\(value)\r\n".utf8))
            }
        }

        var filename = ""
        if filePathKey != "" {
            let fileArray = filePathKey?.components(separatedBy: "/")
            filename = (fileArray?.last)!
        }
        let file = "file"
        let mimetype = "image/png"
        body.append(Data("--\(boundary)\r\n".utf8))
        body.append(Data("Content-Disposition: form-data; name=\"\(file)\"; filename=\"\(filename)\"\r\n".utf8))
        body.append(Data("Content-Type: \(mimetype)\r\n\r\n".utf8))
        body.append(imageDataKey as Data)
        body.append(Data("\r\n".utf8))
        body.append(Data("--\(boundary)--\r\n".utf8))
        return body as NSData
    }
    
    func showSuccessAlert() {
        DispatchQueue.main.async {
            if !self.view.contains(self.alertView) {
                self.alertView.frame = self.view.frame
                self.alertView.delegate = self
                if self.screenFor == "deposit" {
                   self.alertView.setMessage(txt: "Depositor face photo uploaded successfully".localized)
                }else {
                    self.alertView.setMessage(txt: "Withdrawal face photo uploaded successfully".localized)
                }
                self.view.bringSubviewToFront(self.alertView)
                self.view.addSubview(self.alertView)
            }
        }
    }
    
    func hideSuccessAlert(successAlert: UIView) {
        println_debug("Delegate Called in Parent controller")
            if let del = self.delegate {
                println_debug(self.urlImageString)
                println_debug(self.screenFor ?? "")
                del.facePhoto(urlString: self.urlImageString, realImage: self.urlIamge, screen: self.screenFor ?? "", viewController: self)
                self.urlImageString = ""
                self.urlIamge.clear()
            }
        
            if self.view.contains(self.alertView) {
                successAlert.removeFromSuperview()
            }
        
    }
    
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionaryFacePhotoDeposide(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey1FacePhotoDeposite(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}



fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary1111(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
