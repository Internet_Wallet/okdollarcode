//
//  SuccessViewAddWD.swift
//  OK
//
//  Created by shubh's MacBookPro on 9/27/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol SuccessViewAddWDDelegate {
    func hideSuccessAlert(successAlert: UIView)
}


class SuccessViewAddWD: UIView {
    var delegate: SuccessViewAddWDDelegate?
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var imgTitle: UIImageView!
    @IBOutlet weak var lblAlertMessage: UILabel!
    let gifManager = SwiftyGifManager(memoryLimit: 6)
    var timer: Timer?
    var timecount = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        viewContainer.layer.cornerRadius = 20
        viewContainer.layer.masksToBounds = true
      
    }

    func loadFaceGIF() {
      
    }
    
    func setMessage(txt: String) {
        if txt == "Face Verification Successful".localized {
            self.imgTitle.image = UIImage(named: "face_matched")
            println_debug("Face Verification Successful")
        }else {
            self.imgTitle.gifImage = nil
            let gif = UIImage(gifName: "otp_verify")
            self.imgTitle.setGifImage(gif, manager: gifManager)
        }
        lblAlertMessage.text = txt
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.hideAlert(_:)), userInfo: nil, repeats: true)
    }
    
    
    @objc func hideAlert(_ timer: Timer) {
        if self.timecount >= 2 {
           self.timecount = 0
            if let del = delegate {
                println_debug("Delegate Called")
                del.hideSuccessAlert(successAlert: self)
            }
             timer.invalidate()
        }
        println_debug("second time : \(timecount)")
        self.timecount += 1
        
    }
    
    
}
