//
//  ContactSuggestionCell.swift
//  OK
//
//  Created by Imac on 9/12/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class ContactSuggestionCell: UITableViewCell {

    @IBOutlet weak var imgOKDollar: UIImageView!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
