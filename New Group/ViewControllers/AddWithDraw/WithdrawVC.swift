//
//  WithdrawVC.swift
//  OK
//
//  Created by Imac on 6/26/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class WithdrawVC: OKBaseController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
  
    
    @IBOutlet weak var tfWithdraw: UITableView!
    var cellList = [UITableViewCell] ()
    var model = WithDrawnodel()
    let validObj = PayToValidations()
    var isContactConfirmHidden = true
    var isOTPHidden = true
    override func viewDidLoad() {
        super.viewDidLoad()
        tfWithdraw.tableFooterView = UIView(frame: .zero)
        cellList.append(tfWithdraw.dequeueReusableCell(withIdentifier: "WithdrawContactCell") as! WithdrawContactCell)
        tfWithdraw.delegate = self
        tfWithdraw.dataSource = self
        tfWithdraw.reloadData()
    }
    @IBAction func onClickBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = cellList[indexPath.row]
        if indexPath.row == 0 {
            return WithdrawNumberCell(cell: cell as! WithdrawContactCell)
        }else if indexPath.row == 1 {
            return DepositeNumberCell(cell: cell as! WithdrawContactCell)
        }else {
            return DepositeNumberCell(cell: cell as! WithdrawContactCell)
        }
    }
    
    
    private func WithdrawNumberCell(cell : WithdrawContactCell) -> WithdrawContactCell {
        cell.tfNumber.placeholder = "Withdrawer Number".localized
        cell.lblStatus.text = "Withdrawer Number".localized
        cell.imgCounty.image = UIImage(named: "myanmar")
        if model.withDrawNumber == ""{
            model.withDrawNumber = "09"
            cell.btnClose.isHidden = true
        }
        cell.tfNumber.text = model.withDrawNumber
        cell.tfNumber.delegate = self
        cell.tfNumber.tag = 100
        cell.tfNumber.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        cell.btnClose.addTarget(self, action: #selector(onClickContactNumberClose(_:)), for: .touchUpInside)
        cell.btnClose.tag = 100
        return cell
    }
    
    private func DepositeNumberCell(cell : WithdrawContactCell) -> WithdrawContactCell {
        cell.tfNumber.placeholder = "Diposite Number".localized
        cell.imgCounty.image = UIImage(named: "myanmar")
        if model.depositNumber == ""{
            model.depositNumber = "09"
            cell.btnClose.isHidden = true
        }
        cell.tfNumber.text = model.depositNumber
        cell.tfNumber.delegate = self
        cell.tfNumber.tag = 200
        cell.tfNumber.addTarget(self, action: #selector(textFieldDidEditChanged(_:)), for: .editingChanged)
        cell.btnClose.addTarget(self, action: #selector(onClickContactNumberClose(_:)), for: .touchUpInside)
        cell.btnClose.tag = 200
        return cell
    }
    
    
    
    
    @objc func textFieldDidEditChanged(_ textField : UITextField) {
        if textField.tag == 100 {
            model.withDrawNumber = textField.text ?? ""
            let mbLength = validObj.getNumberRangeValidation(textField.text!)
            if textField.text?.count ?? 0 >= mbLength.max || textField.text?.count ?? 0 >= mbLength.min {
                if isContactConfirmHidden {
                    isContactConfirmHidden = false
                    textField.resignFirstResponder()
                    model.depositNumber = ""
                    cellList.append(tfWithdraw.dequeueReusableCell(withIdentifier: "WithdrawContactCell") as! WithdrawContactCell)
                    self.insertRowIntoTable(ind: 1, sec: 0)
                }
            }else if textField.text?.count ?? 0 < mbLength.min && !isContactConfirmHidden {
                model.depositNumber = ""
                isContactConfirmHidden = true
                //self.removeAllCell()
            }
        }else if textField.tag == 200 {
            model.depositNumber = textField.text ?? ""
            let mbLength = validObj.getNumberRangeValidation(textField.text!)
            if textField.text?.count ?? 0 >= mbLength.max || textField.text?.count ?? 0 >= mbLength.min {
                //Nevigate to face recognition screen
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "FaceRecognitionWithdraw") as! FaceRecognitionWithdraw
                self.navigationController?.pushViewController(vc, animated: true)
//                if isOTPHidden {
//                    isOTPHidden = false
//                    textField.resignFirstResponder()
//                    model.depositNumber = ""
//                    cellList.append(tfWithdraw.dequeueReusableCell(withIdentifier: "WithdrawContactCell") as! WithdrawContactCell)
//                    self.insertRowIntoTable(ind: 1, sec: 0)
//                }
            }else if textField.text?.count ?? 0 < mbLength.min && !isContactConfirmHidden {
                model.depositNumber = ""
                isOTPHidden = true
                //self.removeAllCell()
            }
            
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if range.location == 0 && string == " " {
            return false
        }
        let chars = textField.text! + string
        // Contact Number
        if textField.tag == 100 || textField.tag == 200 {
            let mbLength = validObj.getNumberRangeValidation(chars)
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "1234567890").inverted).joined(separator: "")) { return false }
            if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 0, section: 0)) as? WithdrawContactCell {
                if text.count > 3 {
                    cell.btnClose.isHidden = false
                }else {
                    cell.btnClose.isHidden = true
                }
                
                if range.location == 0 && range.length > 1 {
                    textField.text = "09"
                    cell.btnClose.isHidden = true
                    return false
                }
                if range.location == 1 {
                    return false
                }
                if mbLength.isRejected {
                    textField.text = "09"
                    cell.btnClose.isHidden = true
                    return false
                }
                if chars.count > mbLength.max {
                    textField.resignFirstResponder()
                    return false
                }
            }
        }
        return true
    }
    @objc func onClickContactNumberClose(_ sender: UIButton){
        if let cell = tfWithdraw.cellForRow(at: IndexPath(row: 0, section: 0)) as? WithdrawContactCell {
            cell.btnClose.isHidden = true
            cell.tfNumber.text = "09"
            model.withDrawNumber = ""
            isContactConfirmHidden = true
           // self.removeAllCell()
        }
    }
    
    func insertRowIntoTable(ind: Int, sec: Int) {
        self.tfWithdraw.beginUpdates()
        self.tfWithdraw.insertRows(at: [IndexPath.init(row: ind, section: sec)], with: .none)
        self.tfWithdraw.endUpdates()
    }
    
    func deletRowIntoTable(ind: Int, sec: Int) {
        self.tfWithdraw.beginUpdates()
        self.tfWithdraw.deleteRows(at: [IndexPath.init(row: ind, section: sec)], with: .none)
        self.tfWithdraw.endUpdates()
    }
    
    
    
}


class WithdrawContactCell: UITableViewCell {
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var imgCounty: UIImageView!
    @IBOutlet weak var lblCountrycode: UILabel!
    @IBOutlet weak var tfNumber: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}

class WithdrawOTPCell: UITableViewCell {
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var tfFirst: UITextField!
    @IBOutlet weak var tfSecond: UITextField!
    @IBOutlet weak var tfThired: UITextField!
    @IBOutlet weak var tfFour: UITextField!
    @IBOutlet weak var tfFive: UITextField!
    @IBOutlet weak var tfSix: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}


class InfoCellPayWithdraw: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblName: UILabel!
}

class AddressCellPayWithdraw: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblName: UILabel!
}




