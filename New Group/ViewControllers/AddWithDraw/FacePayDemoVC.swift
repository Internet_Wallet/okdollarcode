//
//  FacePayDemoVC.swift
//  OK
//
//  Created by shubh's MacBookPro on 8/31/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class FacePayDemoVC: OKBaseController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
 
    

    @IBOutlet weak var myCollection: UICollectionView!
    var datalist = [Dictionary<String,Any>]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setMarqueLabelInNavigation()
        DispatchQueue.main.async {
            self.CallAPI()
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 18)
        lblMarque.text = "Face ID Pay Demo Videos".localized
        lblMarque.textAlignment = .center
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if datalist.count % 2 == 0 {
            let width = (myCollection.frame.width-20)/2
            let height : CGFloat = 150.0
            return CGSize(width: width, height: height)
        }else {
            if datalist.count - 1 == indexPath.row {
                let width = myCollection.frame.width-10
                let height : CGFloat = 150.0
                return CGSize(width: width, height: height)
            }else{
                let width = (myCollection.frame.width / 2) - 5
                let height : CGFloat = 150.0
                return CGSize(width: width, height: height)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datalist.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FacePayVideoCell", for: indexPath) as! FacePayVideoCell
        let dic = datalist[indexPath.row]
        let dd = dic["Demo"] as? Dictionary<String,Any>
        cell.btnOpenURL.addTargetClosure(closure: {(_) in
            alertViewObj.wrapAlert(title:"", body: "Do you want to play this video in YouTube?".localized, img: UIImage(named: "r_demo_cam")!)
            alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
            })
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                if let youtubeurl = dd?["Url"] as? String {
                    guard let url = URL(string: youtubeurl) else { return }
                    UIApplication.shared.open(url)
                }
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        })
        if ok_default_language == "my" {
            cell.lblVideo.text = dd?["Label_my"] as? String ?? ""
        }else {
            cell.lblVideo.text = dd?["Label_Eng"] as? String ?? ""
        }
        
        return cell
    }
    
    func CallAPI() {
        let ur = getUrl(urlStr: Url.URL_FacePayDemoApi, serverType: .faceIDPay)
        let dic = [:] as [String : Any]
        println_debug(ur)
        TopupWeb.genericClass(url: ur, param: dic as AnyObject, httpMethod: "POST") { [weak self](response, success) in
            if success {
                if let json = response as? Dictionary<String,Any> {
                    if let code = json["code"] as? NSNumber, code == 200 {
                        println_debug(json)
                        println_debug(json["data"])
                        if let arrr = json["data"] as? [Dictionary<String,Any>] {
                            DispatchQueue.main.async {
                                println_debug(arrr[0])
                                self?.datalist = arrr
                                
                                if self?.datalist.count == 0 {
                                    DispatchQueue.main.async {
                                        self?.showAlert(alertTitle: "", alertBody: "No record found.".localized, alertImage: UIImage(named: "r_demo_cam")!)
                                    }
                                }else {
                                self?.myCollection.delegate = self
                                self?.myCollection.dataSource = self
                                self?.myCollection.reloadData()
                                self?.myCollection.reloadData()
                                }
                            }
                        }
                    }else {
                        
                        DispatchQueue.main.async {
                            self?.showAlert(alertTitle: "", alertBody: "Request Failed. Please Try Again".localized, alertImage: UIImage(named: "alert-icon")!)

                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        self?.showAlert(alertTitle: "", alertBody: "Request Failed. Please Try Again".localized, alertImage: UIImage(named: "alert-icon")!)
                    }
                    
                }
            }else {
                
                DispatchQueue.main.async {
                    self?.showAlert(alertTitle: "", alertBody: "Request Failed. Please Try Again".localized, alertImage: UIImage(named: "alert-icon")!)
                }
                
            }
        }
    }
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
            alertViewObj.showAlert(controller: self)
        
    }
    
}

class FacePayVideoCell: UICollectionViewCell {
    @IBOutlet weak var btnOpenURL: UIButton!
    
    @IBOutlet weak var lblVideo: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    
}
