//
//  MainBalanceViewController.swift
//  OK
//
//  Created by Ashish on 1/10/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class MainBalanceViewController: OKBaseController {
    
    @IBOutlet weak var circularView: UIView!
    @IBOutlet weak var circularCashbackView: UIView!
    @IBOutlet weak var okDollarAccount: UILabel!
    @IBOutlet weak var numberFormat: UILabel!
    @IBOutlet weak var mainBalance: UILabel!
    @IBOutlet weak var dateAndTime: UILabel!
    @IBOutlet weak var dateAndTimeCB: UILabel!

    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var mainBalanceCB: UILabel!
        {
        didSet
        {
            mainBalanceCB.font = UIFont(name: appFont, size: appFontSize)
            mainBalanceCB.text = "Main Balance".localized
            
        }
    }
    @IBOutlet weak var amountCB: UILabel!
    @IBOutlet weak var cashBackLabel: UILabel!
        {
        didSet
        {
            cashBackLabel.font = UIFont(name: appFont, size: appFontSize)
            cashBackLabel.text = "Cashback".localized
            
        }
    }
    @IBOutlet weak var cashBackAmount: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewLayouting(onView: self.circularView, cashBackView: self.circularCashbackView)
        self.updateView()
        self.dismissView()
        okDollarAccount.text = okDollarAccount.text?.localized
        //mainBalance.text = mainBalance.text?.localized
        //cashBackLabel.text = cashBackLabel.text?.localized

        let formatter = DateFormatter()
        formatter.dateFormat = "EEE, dd-MMM-yyyy hh:mm:ss a"
        formatter.locale = Locale.current
        formatter.calendar = Calendar(identifier: .gregorian)
        
        self.dateAndTime.text = formatter.string(from: NSDate() as Date)
        self.dateAndTimeCB.text = formatter.string(from: NSDate() as Date)

    }
    
    func updateView() {
        let agent = UserModel.shared.agentType
        let amount = UserLogin.shared.kickBack
        let amountArray = amount.components(separatedBy: "|")
        var kickBackAmount = "0.0"
        if amountArray.count > 1 {
          kickBackAmount = amountArray[1]
        }

//        if kickBackAmount != "0.0" {
//            self.circularView.isHidden = true
//            self.circularCashbackView.isHidden = false
//            self.amountCB.attributedText = self.walletBalance()
//            self.cashBackAmount.attributedText = attributedString(amount: self.amountInFormat(kickBackAmount))
//        }
//        else {
//            self.circularView.isHidden = false
//            self.circularCashbackView.isHidden = true
//            self.amount.attributedText = self.walletBalance()
//        }
        
        if agent == .user {
            self.circularView.isHidden = false
            self.circularCashbackView.isHidden = true
        self.amount.attributedText = self.walletBalance()
            
        } else {
        self.circularView.isHidden = true
        self.circularCashbackView.isHidden = false
        self.amountCB.attributedText = self.walletBalance()
        self.cashBackAmount.attributedText = attributedString(amount: self.amountInFormat(kickBackAmount))
        
       }
        var agentCode = UserModel.shared.mobileNo
        agentCode.remove(at: String.Index.init(encodedOffset: 0))
        agentCode.remove(at: String.Index.init(encodedOffset: 0))
        agentCode.insert("+", at: String.Index.init(encodedOffset: 0))

        let mobDetails = identifyCountry(withPhoneNumber: agentCode)

        let formatNumber = agentCode.replacingOccurrences(of: mobDetails.0, with: "")
        
        self.numberFormat.text = "\(mobDetails.0)" + " " + formatNumber

    }
    
   private func dismissView() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(dismissSelf))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc private func dismissSelf() {
       self.dismiss(animated: false, completion: nil)
    }

    
    fileprivate func viewLayouting(onView v : UIView, cashBackView: UIView) {
        v.layer.masksToBounds = true
        v.layer.cornerRadius = v.frame.width / 2
        // border
        v.layer.borderColor = UIColor.init(hex: "F3C632").cgColor
        v.layer.borderWidth = 5
        // drop shadow
        v.layer.shadowColor = UIColor.lightGray.cgColor
        v.layer.shadowOpacity = 0.8
        v.layer.shadowRadius = 2.0
        v.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
        
        cashBackView.layer.masksToBounds = true
        cashBackView.layer.cornerRadius = v.frame.width / 2
        // border
        cashBackView.layer.borderColor = UIColor.init(hex: "F3C632").cgColor
        cashBackView.layer.borderWidth = 5
        // drop shadow
        cashBackView.layer.shadowColor = UIColor.lightGray.cgColor
        cashBackView.layer.shadowOpacity = 0.8
        v.layer.shadowRadius = 2.0
        cashBackView.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)

    }

    
    
}
