//
//  IntercityBusViewController.swift
//  OKSwiftMigration
//
//  Created by Uma Rajendran on 10/12/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit


class IntercityBusViewController: OKBaseController {

    enum viewFrom : Int {
        case bus = 0
        case flight
        case hotel
        case none
    }
    var _viewFrom = viewFrom.bus

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadNavigationBarSettings()
        loadBackButton()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadHeaderName()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        deleteCookie()

    }

    override func viewWillDisappear(_ animated: Bool) {
        let storage = HTTPCookieStorage.shared
        for cookie in storage.cookies! {
            storage.deleteCookie(cookie)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func loadNavigationBarSettings() {
        let kHeaderColor = UIColor.init(red: 242.0/255.0, green: 130.0/255.0, blue: 50.0/255.0, alpha: 1.0)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationController?.navigationBar.barTintColor        = kHeaderColor
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font: UIFont(name: appFont, size: 21) ?? UIFont.systemFont(ofSize: 21)]
        self.view.backgroundColor = UIColor.lightGray

    }

    override func loadBackButton() {
        let backBtn = UIBarButtonItem(image: UIImage(named: "backButton"), style: .plain, target: self, action: #selector(backBtnAction))
        self.navigationItem.leftBarButtonItem  = backBtn
    }

    @objc func backBtnAction() {
        if let presentingView = self.presentingViewController {
            presentingView.dismiss(animated: true, completion: nil)
        }
    }
    
    func loadHeaderName() {
        switch _viewFrom {
        case .bus:
            println_debug("bus called")
             self.title = setLocStr("Bus Ticket Booking")
 
        case .flight:
            println_debug("flight called")
             self.title = setLocStr("Flight")
 
        case .hotel:
            println_debug("hotel called")
             self.title = setLocStr("Hotel")
 
        default:
            println_debug("default called")
        }
    }
    
    func deleteCookie() {
        let storage = HTTPCookieStorage.shared
        for cookie in storage.cookies! {
            storage.deleteCookie(cookie)
        }
        
        if appDelegate.checkNetworkAvail() {
            prepareJsonRequest()
        }else {
            println_debug("here we need to show the alert for network lost")
        }
        
    }
    
    
    
    // MARK: - Show and Remove Indicator
    func showIndicator()  {
        progressViewObj.showProgressView()
    }
    
    func removeIndicator() {
        progressViewObj.removeProgressView()
    }
    
    // MARK: - Process Request and Response
    func prepareJsonRequest() {
        
        let inputStr = "IsXMLHttpPostBack=true&strAmpersAndReplacement=&Type=Domestic&Direction=RoundTrip&CountryCode=AU&FromLocation1=&FromLocationValue1=&FromLocationCountryCode1=&ToLocation1=&ToLocationValue1=&ToLocationCountryCode1=&flagDifferenetLocationForHotel=false&HotelCityNamePackage=05&PackageHotelCountryCode=&LocationCode=&HotelCommonCodePackage=&chkIsDateFlexible=0&TripDate1=29/12/2013&selTripTime1=00:00:00&TripDate2=31/12/2013&selTripTime2=00:00:00&FromLocation2=&FromLocationValue2=&FromLocationCountryCode2=&ToLocation2=&ToLocationValue2=&ToLocationCountryCode2=&flagDifferentDatesForHotel=false&NoSpecificCheckinDate=&CheckInDateTimePackageHotel=2013-11-13&NoSpecificCheckOutDate=&CheckOutDateTimePackageHotel=2013-11-15&ddlRoomPackage=1&TotalRooms="
        let apiUrlStr = "https://www.goautoticket.com/booking/AirBooking/AirSearch.aspx?\(inputStr)"
        //        let apiUrlStr = "http://preprod-www.goautoticket.com/booking/AirBooking/AirSearch.aspx?\(inputStr)"
        let apiURL = URL.init(string: apiUrlStr)
        guard appDelegate.checkNetworkAvail() else {
            return
        }
        
        self.showIndicator()
        let request = URLRequest.init(url: apiURL!, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
        _ = URLSession.shared.configuration
        
        URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            self.removeIndicator()
            
            guard data != nil || error == nil else {
                //show the error alert
                return }
            
            let httpResponse = response as! HTTPURLResponse
            guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
                // show the error alert
                return
            }
            var cookieReceived = ""
            if let cookieValue = httpResponse.allHeaderFields["Set-Cookie"] as? String {
                let cookieArr = cookieValue.split(separator: ";").map(String.init)
                println_debug("cookies array contains \(cookieArr)")
                if cookieArr.count > 0 {
                    println_debug("first value of cookie array\(cookieArr[0])")
                    cookieReceived = cookieArr[0]
                }
            }
            
            if let responseHeaders = httpResponse.allHeaderFields as? [String:String] {
                let cookies: [HTTPCookie] = HTTPCookie.cookies(withResponseHeaderFields: responseHeaders, for: request.url!)
                println_debug("just print the cookies array\(cookies)")
                HTTPCookieStorage.shared.setCookies(cookies, for: httpResponse.url!, mainDocumentURL: nil)
            }
            for cookie in HTTPCookieStorage.shared.cookies! {
                println_debug("EXTRACTED COOKIE: \(cookie)") //find your cookie here instead of httpUrlResponse.allHeaderFields
            }
            
            self.callTicketAuthenticationAPI(cookie: cookieReceived)
            
            }.resume()
        
    }
    
    func callTicketAuthenticationAPI(cookie: String)  {
        
//        let apiUrlStr       = "http://preprod-www.goautoticket.com/booking/authenticateuser.axd"
        let apiUrlStr       = "https://www.goautoticket.com/booking/authenticateuser.axd"

        let firstName       = UserModel.shared.name
        let lastName        = ""
        let countryCode     = UserModel.shared.countryCode
        
        var phoneNumber     = UserModel.shared.mobileNo.substring(from: 4)
        if !phoneNumber.hasPrefix("0") {
            phoneNumber = "0\(phoneNumber)"
        }
        
        let city            = UserModel.shared.township
        let state           = UserModel.shared.state
        let documentType    = "NationalRegistrationCard"
        let documentNumber  = "K123654"
        let packageName     = "com.jas.digitalkyats"
        let packageSignKey  = "A500974739D76DE4AB26F9005BFEA9D19410D1CDCADCE6264FE8235CE65A42F1871C131DE08CFDBB52FDD7631932E7229F61322F12D666C842FA0488FD51E6DE"
        let method          = "mobileapp"
        var language: String {
            if let curLanguage = UserDefaults.standard.value(forKey: "currentLanguage") as? String {
                switch curLanguage {
                case "en":
                    return "en-GB"
                case "my":
                    return "my-MM"
                default:
                    return "en-GB"
                }
            }
            return "en-GB"
        }
        let email           = UserModel.shared.email
        
        let requestStr = "firstname=\(firstName)&lastname=\(lastName)&countrycode=\(countryCode)&phonenumber=\(phoneNumber)&city=\(city)&state=\(state)&documenttype=\(documentType)&documentnumber=\(documentNumber)&packagename=\(packageName)&packagesignkey=\(packageSignKey)&Method=\(method)&language=\(language)&email=\(email)"
        println_debug("just print the request string :::  \(requestStr)")
        
        let session = URLSession.shared
        let apiUrl  = URL.init(string: apiUrlStr)
        let request = self.composeURLRequest(toDownloadData: apiUrl!, input: requestStr, cookie: cookie)
        self.showIndicator()

        let dataTask = session.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in

            OperationQueue.main.addOperation({ () -> Void in
                    let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                    println_debug("response dic for call api with cookie :::: \(String(describing: jsonDic))")
                    guard let httpResponse = response as? HTTPURLResponse else {
                        DispatchQueue.main.async {
                            self.removeIndicator()
                            self.showErrorAlert(errMessage: setLocStr("Try Again"))
                        }
                        return
                    }
                    guard httpResponse.statusCode == 200 else {
                        DispatchQueue.main.async {
                            self.removeIndicator()
                            self.showErrorAlert(errMessage: setLocStr("Try Again"))
                        }
                        return
                    }
                    var webViewUrlStr = ""
                    switch self._viewFrom {
                    case .bus:
                        webViewUrlStr = "https://www.goautoticket.com/"
//                        webViewUrlStr = "http://preprod-www.goautoticket.com/"

                    case .flight:
                        webViewUrlStr = "https://www.goautoticket.com/?busname=flights"
//                        webViewUrlStr = "http://preprod-www.goautoticket.com/busname=flights"

                    case .hotel:
                        webViewUrlStr = "https://www.goautoticket.com/?busname=hotels"
//                        webViewUrlStr = "http://preprod-www.goautoticket.com/busname=hotels"
                        
                    default:
                        println_debug("default called")
                    }

                    let webUrl = URL.init(string: webViewUrlStr)
                    let webRequest = self.composeURLRequest(toDownloadData: webUrl!, input: requestStr, cookie: cookie)
                    self.webView.loadRequest(webRequest)
                    DispatchQueue.main.async {
                        self.removeIndicator()
                    }
                })
        }
        dataTask.resume()
        
    }
    
    
    func composeURLRequest(toDownloadData url: URL, input inputReq: String, cookie: String) -> URLRequest {
        var request = URLRequest.init(url: url, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    request.addValue("A500974739D76DE4AB26F9005BFEA9D19410D1CDCADCE6264FE8235CE65A42F1871C131DE08CFDBB52FDD7631932E7229F61322F12D666C842FA0488FD51E6DE", forHTTPHeaderField: "MobileAppSignKey")
        request.addValue("true", forHTTPHeaderField: "IsFromMobileApp")
        request.addValue(cookie, forHTTPHeaderField: "Cookie")
        request.httpMethod = "POST"
        let postData: Data? = inputReq.data(using: String.Encoding.ascii, allowLossyConversion: true)
        request.httpBody = postData
        return request
        
    }
    
    
}
