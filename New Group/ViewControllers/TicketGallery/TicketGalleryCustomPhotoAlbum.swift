//
//  CustomPhotoAlbum.swift
//  Ticketgallery
//
//  Created by Prabu on 12/27/17.
//  Copyright © 2017 prabu. All rights reserved.
//

import UIKit
import Foundation
import Photos


class TicketGalleryCustomPhotoAlbum: NSObject {
    static let albumName = "OK$"
    static let sharedInstance = TicketGalleryCustomPhotoAlbum()
    var firsttimeimage = UIImage()
    
    var assetCollection: PHAssetCollection!
    
    override init() {
        
        super.init()
        
        if let assetCollection = fetchAssetCollectionForAlbum() {
            self.assetCollection = assetCollection
            return
        }
        
        if PHPhotoLibrary.authorizationStatus() != PHAuthorizationStatus.authorized {
            PHPhotoLibrary.requestAuthorization({ (status: PHAuthorizationStatus) -> Void in
                ()
            })
        }
        
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized {
            self.createAlbum()
            
        } else {
            PHPhotoLibrary.requestAuthorization(requestAuthorizationHandler)
        }
    }
    
    func requestAuthorizationHandler(status: PHAuthorizationStatus) {
        
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized {
            // ideally this ensures the creation of the photo album even if authorization wasn't prompted till after init was done
            println_debug("trying again to create the album")
            self.createAlbum()
            
        } else {
            println_debug("should really prompt the user to let them know it's failed")
        }
        
    }
    
    func createAlbum() {
        
        PHPhotoLibrary.shared().performChanges({
            PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: TicketGalleryCustomPhotoAlbum.albumName)   // create an asset collection with the album name
        }) { success, error in
            if success {
                self.assetCollection = self.fetchAssetCollectionForAlbum()
                println_debug("Album Created")
                self.save(image:self.firsttimeimage)
            } else {
                //println_debug("error \(error)")
            }
        
        }
        
    }
    
    func fetchAssetCollectionForAlbum() -> PHAssetCollection? {
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", TicketGalleryCustomPhotoAlbum.albumName)
        let collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
        
        if let _: AnyObject = collection.firstObject {
            return collection.firstObject
        }
        return nil
    }
    
    func save(image: UIImage) {
        
        //First time need to store
        firsttimeimage = image
        
        if assetCollection == nil {
            return
        }
        
        PHPhotoLibrary.shared().performChanges({
            let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
            let assetPlaceHolder = assetChangeRequest.placeholderForCreatedAsset
            let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
            let enumeration: NSArray = [assetPlaceHolder!]
            albumChangeRequest!.addAssets(enumeration)
            
        }, completionHandler: nil)
    }
}
