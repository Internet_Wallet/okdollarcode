//
//  Gallery.swift
//  Ticketgallery
//
//  Created by Prabu on 12/14/17.
//  Copyright © 2017 prabu. All rights reserved.
//

import UIKit


class TicketGallery: NSObject, NSCoding {
    
    
    var tearingstatus: Bool
    var signaturestatus: Bool
    var phonegallerystatus: Bool
    var timerstatus: Bool
    var identifystatus: Bool
    var timervalue: String
    var imageUrl: String
    
    
    init(tearing: Bool, signature: Bool, phonegallery:Bool , timer:Bool , identify:Bool , timerdate:String, image: String) {
        
        self.tearingstatus = tearing
        self.signaturestatus = signature
        self.phonegallerystatus = phonegallery
        self.timerstatus = timer
        self.identifystatus = identify
        self.timervalue = timerdate
        self.imageUrl = image
        
    }
    
    required  convenience init(coder aDecoder: NSCoder) {
        let tearingstatus = aDecoder.decodeBool(forKey:"tearingstatus")
        let signaturestatus = aDecoder.decodeBool(forKey:"signaturestatus")
        let phonegallerystatus = aDecoder.decodeBool(forKey: "phonegallerystatus")
        let timerstatus = aDecoder.decodeBool(forKey: "timerstatus")
        let identifystatus = aDecoder.decodeBool(forKey: "identifystatus")
        let timervalue = aDecoder.decodeObject(forKey: "timervalue") as! String
        let imageUrl = aDecoder.decodeObject(forKey: "imageUrl") as! String

        self.init(tearing: tearingstatus, signature: signaturestatus, phonegallery : phonegallerystatus ,timer : timerstatus , identify : identifystatus , timerdate:timervalue , image: imageUrl)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(tearingstatus, forKey: "tearingstatus")
        aCoder.encode(signaturestatus, forKey: "signaturestatus")
        aCoder.encode(phonegallerystatus, forKey: "phonegallerystatus")
        aCoder.encode(timerstatus, forKey: "timerstatus")
        aCoder.encode(identifystatus, forKey: "identifystatus")
        aCoder.encode(timervalue, forKey: "timervalue")
        aCoder.encode(imageUrl, forKey: "imageUrl")
    }
}


