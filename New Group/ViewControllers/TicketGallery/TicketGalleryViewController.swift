//
//  ViewController.swift
//  Ticketgallery
//
//  Created by Prabu on 11/9/17.
//  Copyright © 2017 prabu. All rights reserved.
//

import UIKit
import CoreGraphics
import QuartzCore
import AVFoundation



var player: AVAudioPlayer?
var imagesDisplayArr = [String]()
var imagesNameArr = [String]()
var selectedImagePath = String()
var selectedImageName = String()
var userdefaultObject : TicketGallery!
var checkedBool : Bool!


//Timer
var timer = Timer()
var timeCount:TimeInterval = 0

//Custom Photo Album
var customAlbum = TicketGalleryCustomPhotoAlbum()

extension UIImage {
    
    var topHalf: UIImage? {
        guard let cgImage = cgImage, let image = cgImage.cropping(to: CGRect(origin: .zero, size: CGSize(width: size.width, height: size.height))) else { return nil }
        return UIImage(cgImage: image, scale: 1, orientation: imageOrientation)
    }
    var bottomHalf: UIImage? {
        guard let cgImage = cgImage, let image = cgImage.cropping(to: CGRect(origin: CGPoint(x: 0,  y: CGFloat(Int(size.height)-Int(size.height/2))), size: CGSize(width: size.width, height: CGFloat(Int(size.height) - Int(size.height/2))))) else { return nil }
        return UIImage(cgImage: image)
    }
    var leftHalf: UIImage? {
        guard let cgImage = cgImage, let image = cgImage.cropping(to: CGRect(origin: .zero, size: CGSize(width: size.width/2, height: size.height))) else { return nil }
        return UIImage(cgImage: image)
    }
    var rightHalf: UIImage? {
        guard let cgImage = cgImage, let image = cgImage.cropping(to: CGRect(origin: CGPoint(x: CGFloat(Int(size.width)-Int((size.width/2))), y: 0), size: CGSize(width: CGFloat(Int(size.width)-Int((size.width/2))), height: size.height)))
            else { return nil }
        return UIImage(cgImage: image)
    }
    
}


extension UIImage {
    public func imageRotatedByDegrees(degrees: CGFloat) -> UIImage {
        //Calculate the size of the rotated view's containing box for our drawing space
        let rotatedViewBox: UIView = UIView(frame: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        let t: CGAffineTransform = CGAffineTransform(rotationAngle: degrees * CGFloat.pi / 180)
        rotatedViewBox.transform = t
        let rotatedSize: CGSize = rotatedViewBox.frame.size
        //Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize)
        let bitmap: CGContext = UIGraphicsGetCurrentContext()!
        //Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap.translateBy(x: rotatedSize.width / 2, y: rotatedSize.height / 2)
        //Rotate the image context
        bitmap.rotate(by: (degrees * CGFloat.pi / 180))
        //Now, draw the rotated/scaled image into the context
        bitmap.scaleBy(x: 1.0, y: -1.0)
        bitmap.draw(self.cgImage!, in: CGRect(x: -self.size.width / 2, y: -self.size.height / 2, width: self.size.width, height: self.size.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    
    public func fixedOrientation() -> UIImage {
        
        if imageOrientation == UIImage.Orientation.up {
            return self
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch imageOrientation {
            
        case UIImage.Orientation.down, UIImage.Orientation.downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat.pi)
            break
        case UIImage.Orientation.left, UIImage.Orientation.leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat.pi/2)
            break
        case UIImage.Orientation.right, UIImage.Orientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: -CGFloat.pi/2)
            break
        case UIImage.Orientation.up, UIImage.Orientation.upMirrored:
            break
            
        }
        
        switch imageOrientation {
            
        case UIImage.Orientation.upMirrored, UIImage.Orientation.downMirrored:
            transform.translatedBy(x: size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImage.Orientation.leftMirrored, UIImage.Orientation.rightMirrored:
            transform.translatedBy(x: size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImage.Orientation.up, UIImage.Orientation.down, UIImage.Orientation.left, UIImage.Orientation.right:
            break
            
        }
        
        let ctx: CGContext = CGContext(data: nil,
                                       width: Int(size.width),
                                       height: Int(size.height),
                                       bitsPerComponent: self.cgImage!.bitsPerComponent,
                                       bytesPerRow: 0,
                                       space: self.cgImage!.colorSpace!,
                                       bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case UIImage.Orientation.left, UIImage.Orientation.leftMirrored, UIImage.Orientation.right, UIImage.Orientation.rightMirrored:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        default:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        
        let cgImage: CGImage = ctx.makeImage()!
        
        return UIImage(cgImage: cgImage)
    }
}


class TicketGalleryViewController : UIViewController , UICollectionViewDataSource , UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout, TicketGalleryYPSignatureDelegate  {
    
    enum UIUserInterfaceIdiom : Int
    {
        case Unspecified
        case Phone
        case Pad
    }
    
    struct ScreenSize
    {
        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    struct DeviceType
    {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 480.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPHONE_X         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    }
    
    
    //HeaderView
    @IBOutlet var headerLbl: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    
    //Transactionpopview
    @IBOutlet var transactionpopupview: UIView!
    @IBOutlet var noticketfoundLbl: UILabel!
    @IBOutlet var sampleimg: UIImageView!
    var imageArr = [[UIImage]]()
    
    //Appdelegate Language Change
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    //MARK:VIEWDIDLOAD
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        // loadcontent()
        
        
        //sigView.delegate = self
        sealImg.isHidden = true
        gobackView.isHidden = true
        timerView.isHidden = true
        transactionpopupview.isHidden = true
        signatureView.delegate = self
        checkedBool = false
        loadAllImages()
        
        // LongPress Gesture
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPress(longPress:)))
        longGesture.minimumPressDuration = 1
        self.collectionView.addGestureRecognizer(longGesture)
        collectionView.reloadData()
        
        
        
        //Checkticket Double Tap
        ticketBtnOut.addTarget(self, action:#selector(multipleTap(_:event:)), for: UIControl.Event.touchDownRepeat)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setLang()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: CHANGE LANGUAGE
    
    func setLang()
    {
    
        //Navigation Bar Title , color ,Font changes
        self.navigationItem.title = appDelegate.getlocaLizationLanguage(key: "payment_gallery")
        navigationController?.navigationBar.titleTextAttributes  =  [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        
        forTicketCollectorLbl.font = UIFont(name:appFont, size: 20.0)
       forTicketCollectorLbl.text = appDelegate.getlocaLizationLanguage(key: "For Ticket collector")

        forCustomerLbl.font = UIFont(name:appFont, size: 15.0)
        forCustomerLbl.text = appDelegate.getlocaLizationLanguage(key: "For Customer")

        tickettearLbl.font = UIFont(name:appFont, size: 20.0)
        tickettearLbl.text = appDelegate.getlocaLizationLanguage(key: "check_ticket")

        saveCollectorSignatureLbl.font = UIFont(name:appFont, size: 20.0)
        saveCollectorSignatureLbl.text = appDelegate.getlocaLizationLanguage(key: "save_ticket_controller_signature")

        reclaimLblOut.font = UIFont(name:appFont, size: 20.0)
        reclaimLblOut.text = appDelegate.getlocaLizationLanguage(key: "reclaim")

        noticketfoundLbl.font = UIFont(name:appFont, size: 20.0)
        noticketfoundLbl.text = appDelegate.getlocaLizationLanguage(key: "No_Tickets_Found")

        
    }
    
    
    
    //MARK: RETRIEVE AND STORE IMAGE
    
    func loadAllImages() {
        
        ///////////////////
        let filemgr1 = FileManager.default
        
        let dirPaths1 = filemgr1.urls(for: .documentDirectory, in: .userDomainMask)
        
        let docsURL1 = dirPaths1[0]
        
        let newDir1 = docsURL1.appendingPathComponent("LocalCache").path
        
        let fm = FileManager.default
        // let path = Bundle.main.resourcePath!
        
        do {
            
            let items = try fm.contentsOfDirectory(atPath: newDir1)
            // println_debug("All items-------\(items)")
            
            for item in items {
                println_debug("Found \(item)")
                
                /////Retrieve stored object
                let defaultval = UserDefaults.standard
                if let decoded = defaultval.object(forKey:item) as? Data {
                    if let storeObj = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? TicketGallery {
                        println_debug("store obj-------\(storeObj.timervalue)")
                        let timer = timercellcheck(startTime: storeObj.timervalue)
                        println_debug("store obj timer-------\(timer)")
                        
                        if timer <= 86400 {
                            let getImagePath = newDir1.appending("/\(item)")
                            
                            imagesNameArr.append(item)
                            imagesDisplayArr.append(getImagePath)
                            
                        } else {
                            
                            let checkValidation = FileManager.default
                            
                            let getImagePath = newDir1.appending("/\(item)")
                            
                            if (checkValidation.fileExists(atPath: getImagePath))
                            {
                                //remove file as its already existed
                                try!  checkValidation.removeItem(atPath: getImagePath)
                            }
                        }
                    }
                }
            }
            
            println_debug("All items imagename Array-------\(imagesNameArr)")
            println_debug("All items Array-------\(imagesDisplayArr)")
            println_debug("All items Array count-------\(imagesDisplayArr.count)")
            
            if(imagesDisplayArr.count == 0) {
                
                noticketfoundLbl.isHidden = false
                
            } else {
                
                noticketfoundLbl.isHidden = true
                
            }
            
        } catch {
            // failed to read directory – bad permissions, perhaps?
        }
        
    }
    
    
    func retrieveUserdefaults () {
        
        /////Retrieve stored object
        let defaultval = UserDefaults.standard
        let decoded  = defaultval.object(forKey:selectedImageName) as! Data
        userdefaultObject = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? TicketGallery
        
        println_debug("All values------>\(userdefaultObject.tearingstatus)--->\(userdefaultObject.signaturestatus)---\(userdefaultObject.phonegallerystatus)---\(userdefaultObject.timerstatus)---\(userdefaultObject.identifystatus)---\(userdefaultObject.timervalue)----\(userdefaultObject.imageUrl)")
        
        
        
    }
    
    
    func saveUserdefaults() {
        
        let strval = TicketGallery(tearing:userdefaultObject.tearingstatus , signature:userdefaultObject.signaturestatus , phonegallery:userdefaultObject.phonegallerystatus , timer:userdefaultObject.timerstatus ,identify:userdefaultObject.identifystatus ,timerdate:userdefaultObject.timervalue , image:userdefaultObject.imageUrl)
        
        let kUserDefault = UserDefaults.standard
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: strval)
        kUserDefault.set(encodedData, forKey: selectedImageName)
        kUserDefault.synchronize()
        
        //  retrieveUserdefaults(imageName: imageName)
        
    }
    
    //MARK:TIMERCOUNT METHOD
    
    func timercellcheck (startTime:String) -> TimeInterval {
        
        let dateFormatter = DateFormatter()
        var dateAsString = String()
        
        var startDate = Date()
        var endDate = Date()
        
        //Document URL Date
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateAsString = startTime
        //dateAsString = "2017-12-21 18:32:00"
        startDate = (dateFormatter.date(from: dateAsString))!
        
        //Current Date
        let timeStamp = dateFormatter.string(from: Date())
        endDate = (dateFormatter.date(from: timeStamp))!
        
        
        //  println_debug("Time elapsed current \(timeStamp)")
        timeCount = endDate.timeIntervalSince(startDate as Date)
        // println_debug("Time elapsed \(timeCount)")
        
        return timeCount
        
    }
    
    
    @objc func timerAction() {
        
        
        if timeCount != 0 {
            
            timeCount -= 1
            timerLbl.text = "\(stringFromTimeInterval(interval: timeCount))"
            
        } else {
            
            timer.invalidate()
            
        }
        
        
    }
    
    
    func stringFromTimeInterval(interval: TimeInterval) -> NSString {
        
        let ti = NSInteger(interval)
        //let ms = Int((ti % 1) * 1000)
        let seconds = ti % 60
        let minutes = (ti / 60) % 60
        let hours = (ti / 3600)
        
        return NSString(format:"%0.2d:%0.2d:%0.2d",hours,minutes,seconds)
        
    }
    
    
    
    func replaceImage () {
        
        do {
            
            println_debug("Replaceimage path\(selectedImagePath)")
            
            //write file as its not available
            let imageData =  transactionImgView.image!.jpegData(compressionQuality: 1.0)
            try imageData?.write(to: URL.init(fileURLWithPath: selectedImagePath), options: .atomicWrite)
            
        } catch let error as NSError {
            
            println_debug("Error: \(error.localizedDescription)")
            
        }
        
    }
    
    
    
    
    //MARK:SIGNATURE AND MERGE AND TEAR IMAGE
    
    func slice(image: UIImage, into howMany: Int) -> [UIImage] {
        
        let width: CGFloat
        let height: CGFloat
        
        switch image.imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            width = image.size.height
            height = image.size.width
        default:
            width = image.size.width
            height = image.size.height
        }
        
        let tileWidth = Int(width / CGFloat(howMany))
        let tileHeight = Int(height / CGFloat(howMany))
        
        let scale = Int(image.scale)
        var images = [UIImage]()
        
        let cgImage = image.cgImage!
        
        var adjustedHeight = tileHeight
        
        var y = 0
        for row in 0 ..< howMany {
            if row == (howMany - 1) {
                adjustedHeight = Int(height) - y
            }
            var adjustedWidth = tileWidth
            var x = 0
            for column in 0 ..< howMany {
                if column == (howMany - 1) {
                    adjustedWidth = Int(width) - x
                }
                let origin = CGPoint(x: x * scale, y: y * scale)
                let size = CGSize(width: adjustedWidth * scale, height: adjustedHeight * scale)
                let tileCgImage = cgImage.cropping(to: CGRect(origin: origin, size: size))!
                images.append(UIImage(cgImage: tileCgImage, scale: image.scale, orientation: image.imageOrientation))
                x += tileWidth
            }
            y += tileHeight
        }
        return images
    }
    
    func imageRotatedByDegrees(oldImage: UIImage, deg degrees: CGFloat) -> UIImage {
        
        //Calculate the size of the rotated view's containing box for our drawing space
        let rotatedViewBox: UIView = UIView(frame: CGRect(x: 0, y: 0, width: oldImage.size.width, height: oldImage.size.height))
        let t: CGAffineTransform = CGAffineTransform(rotationAngle: degrees * CGFloat.pi / 180)
        rotatedViewBox.transform = t
        let rotatedSize: CGSize = rotatedViewBox.frame.size
        //Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize)
        let bitmap: CGContext = UIGraphicsGetCurrentContext()!
        //Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap.translateBy(x: rotatedSize.width / 2, y: rotatedSize.height / 2)
        //Rotate the image context
        bitmap.rotate(by: (degrees * CGFloat.pi / 180))
        //Now, draw the rotated/scaled image into the context
        bitmap.scaleBy(x: 1.0, y: -1.0)
        bitmap.draw(oldImage.cgImage!, in: CGRect(x: -oldImage.size.width/2 , y: -oldImage.size.height/2 , width: oldImage.size.width, height: oldImage.size.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
        
    }
    
    
    func splitImage(row : Int , column : Int){
        
        let oImg = transactionImgView.image
        
        let height =  (transactionImgView.image?.size.height)! /  CGFloat (row) //height of each image tile
        let width =  (transactionImgView.image?.size.width)!  / CGFloat (column)  //width of each image tile
        
        let scale = (transactionImgView.image?.scale)! //scale conversion factor is needed as UIImage make use of "points" whereas CGImage use pixels.
        
        //imageArr = [UIImage]() // will contain small pieces of image
        
        for y in 0..<row{
            
            var yArr = [UIImage]()
            
            for x in 0..<column{
                
                UIGraphicsBeginImageContextWithOptions(
                    CGSize(width:width, height:height),
                    false, 0)
                let i =  oImg?.cgImage?.cropping(to:  CGRect.init(x: CGFloat(x) * width * scale, y:  CGFloat(y) * height * scale  , width: width * scale  , height: height * scale) )
                
                let newImg = UIImage.init(cgImage: i!)
                
                if x == 0 {
                    //let rotateImg = imageRotatedByDegrees(oldImage:newImg, deg: -10)
                    let rotateImg = newImg.fixedOrientation().imageRotatedByDegrees(degrees: -10.0)
                    yArr.append(rotateImg)
                    
                }
                
                if x == 1 {
                    
                    // let rotateImg = imageRotatedByDegrees(oldImage:newImg, deg: 10)
                    let rotateImg = newImg.fixedOrientation().imageRotatedByDegrees(degrees: 10.0)
                    
                    yArr.append(rotateImg)
                }
                
                UIGraphicsEndImageContext();
                
            }
            
            // imageArr = yArr
            imageArr.append(yArr)
            //imageArr[y] = yArr
            
        }
        
    }
    
    func createNewImage(imgArr : [[UIImage]]){
        
        let row = imageArr.count
        let column = 2
        let height =  (transactionImgView.frame.size.height) /  CGFloat (row )
        let width =  (transactionImgView.frame.size.width) / CGFloat (column )
        
        UIGraphicsBeginImageContext(CGSize.init(width: transactionImgView.frame.size.width , height: transactionImgView.frame.size.height))
        
        
        for y in 0..<row{
            
            for x in 0..<column{
                
                let newImage = imgArr[y][x]
                
                newImage.draw(in: CGRect.init(x: CGFloat(x) * width, y:  CGFloat(y) * height  , width: width - 1  , height: height - 1 ))
            }
        }
        
        
        let originalImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        transactionImgView.image =  originalImg
        
    }
    
    func didStart() {
        println_debug("Started Drawing")
    }
    
    // didFinish() is called rigth after the last touch of a gesture is registered in the view.
    // Can be used to enabe scrolling in a scroll view if it has previous been disabled.
    func didFinish() {
        println_debug("Finished Drawing")
    }
    
    
    //MARK:LONG PRESS
    
    @objc func longPress(longPress: UILongPressGestureRecognizer) {
        
        let location = longPress.location(in: self.collectionView!)
        
        // self.showErrorAlert(errMessage: appDelegate.getlocaLizationLanguage(key: "Do you want to delete the Ticket"))
      
       // let alertC = UIAlertController(title: "Alert", message: appDelegate.getlocaLizationLanguage(key: "Do you want to delete the Ticket"), preferredStyle: UIAlertControllerStyle.alert)
        
        let alertC = UIAlertController(title: "Alert", message: "Do you want to delete the Ticket".localized, preferredStyle: UIAlertController.Style.alert)
        
        let ok = UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default) {(alert) in
            
            guard let cv = self.collectionView else { return }
            guard let indexPath = cv.indexPathForItem(at: location) else { return }
            
            //guard cv.dataSource?.collectionView?(cv, canMoveItemAtIndexPath: indexPath) == true else { return }
            //  guard let cell = cv.cellForItem(at: indexPath) else { return }
            
            println_debug(indexPath.section)
            println_debug(indexPath.row)
            
            //Delete image path from Array
            let localimagepath = imagesDisplayArr[indexPath.row]
            imagesDisplayArr .remove(at: indexPath.row)
            self.collectionView.reloadData()
            
            //Delete Document path
            let checkValidation = FileManager.default
            
            if (checkValidation.fileExists(atPath: localimagepath))
            {
                //remove file as its already existed
                try!  checkValidation.removeItem(atPath: localimagepath)
            }
            
        }
        
        let cancel = UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.default) {(alert) in
            
            
        }
        
        alertC.addAction(ok)
        alertC.addAction(cancel)
        
        self.present(alertC, animated: true, completion: nil)
        
        
    }
    
    
    
    
    //MARK:TIMER VIEW
    
    @IBOutlet var timerView: UIView!
    @IBOutlet var transactionLabel: UILabel!
    
    @IBOutlet var timerLbl: UILabel!
    @IBOutlet var sealImg: UIImageView!
    @IBOutlet var transactionImgView: UIImageView!
    
    @IBOutlet var signatureView: TicketGalleryYPDrawSignatureView!
    
    
    @IBAction func transactionBackBtn(_ sender: Any) {
        
        transactionpopupview.isHidden = true
        signatureView.clear()
        transactionImgView.image = nil
        timerLbl.text = ""
        imagesDisplayArr.removeAll()
        imagesNameArr.removeAll()
        println_debug("image Name arr all delete count ------------\(imagesNameArr.count)")
        println_debug("image arr all delete count ------------\(imagesDisplayArr.count)")
        timer.invalidate()
        
        checkedBool = false
        loadAllImages()
        collectionView .reloadData()
    }
    
    
    @objc func multipleTap(_ sender: UIButton, event: UIEvent) {
        
        if userdefaultObject.tearingstatus == false && userdefaultObject.signaturestatus == true{
            
            let touch: UITouch = event.allTouches!.first!
            
            if (touch.tapCount == 2) {
                
                // do action.
                println_debug("multiple tap")
                
                imageArr.removeAll()
                
                mergecheckedImage()
                splitImage(row: 1, column: 2)
                createNewImage(imgArr: imageArr)
                
                guard let url = Bundle.main.url(forResource: "Tear", withExtension: "mp3") else { return }
                
                do {
                    
                    try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.playback)), mode: .default)
                    try AVAudioSession.sharedInstance().setActive(true)
                    
                    /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
                    
                    player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
                    
                    /* iOS 10 and earlier require the following line:
                     player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
                    
                    guard let player = player else { return }
                    
                    player.play()
                    
                    //Replace image into document path
                    replaceImage()
                    
                    //Cahnge colorCahnge
                    tickettearLbl.backgroundColor = UIColor.init(red: 65.0/255.0, green: 129.0/255.0, blue: 10.0/255.0, alpha: 1)
                    
                } catch let error {
                    println_debug(error.localizedDescription)
                }
                
                userdefaultObject.tearingstatus = true
                saveUserdefaults()
                retrieveUserdefaults()
                
                if userdefaultObject.tearingstatus == false {
                    
                    tickettearLbl.backgroundColor = UIColor.init(red: 65.0/255.0, green: 129.0/255.0, blue: 10.0/255.0, alpha: 1)
                    
                } else {
                    
                    tickettearLbl.backgroundColor = UIColor.red
                    
                }
                
            }
            
        } else {
            
            if userdefaultObject.tearingstatus == true {
                
                 self.showErrorAlert(errMessage: appDelegate.getlocaLizationLanguage(key: "Image Already Teared"))
            
            }
            
            if userdefaultObject.signaturestatus == false {
                
                 self.showErrorAlert(errMessage: appDelegate.getlocaLizationLanguage(key: "Please save your signature"))
        
            }
        }
        
    }
    
    
    //MARK:TOP HEADER VIEW
    @IBAction func backBtn(_ sender: Any) {
        
        if (timerView.isHidden == false && gobackView.isHidden == true) {
            // do stuff
            signatureView.clear()
            transactionImgView.image = nil
            timerLbl.text = ""
            imagesDisplayArr.removeAll()
            imagesNameArr.removeAll()
            println_debug("image Name arr all delete count ------------\(imagesNameArr.count)")
            println_debug("image arr all delete count ------------\(imagesDisplayArr.count)")
            timer.invalidate()
           
            transactionpopupview.isHidden = true
            timerView.isHidden = true
            
            checkedBool = false
            loadAllImages()
            collectionView .reloadData()
            
        } else if (timerView.isHidden == true && gobackView.isHidden == false){
            
            gobackView.isHidden = true
            timerView.isHidden = true
            transactionpopupview.isHidden = true
            noticketfoundLbl.isHidden = true
            
        } else {
            
            imagesNameArr.removeAll()
            imagesDisplayArr.removeAll()
            self.navigationController?.popViewController(animated: true)
            
        }

    }
    
    
    @IBAction func deleteAllScreenShotsButtonAction(_ sender: Any) {
        
        
    }
    
    
    
    //MARK:TIMERBOTTOM VIEW
    
    @IBOutlet var saveCollectorSignatureLbl: UILabel!
    
    @IBOutlet var forTicketCollectorLbl: UILabel!
    @IBOutlet var ticketBtnOut: UIButton!
    @IBOutlet var tickettearLbl: UILabel!
    @IBOutlet var forCustomerLbl: UILabel!
    @IBOutlet var reclaimBtnOut: UIButton!
    @IBOutlet var reclaimLblOut: UILabel!
    
    @IBAction func savesignatureBtn(_ sender: Any) {
        
        if userdefaultObject.signaturestatus == false {
            
            if self.signatureView.getSignature(scale: 0) != nil {
                
                checkedBool = true
                
                let bottomImage = transactionImgView.image
                let topImage = signatureView.getSignature()
                
                let size = CGSize(width: transactionImgView.frame.size.width , height: transactionImgView.frame.size.height)
                UIGraphicsBeginImageContext(size)
                
                let areaSize = CGRect(x: 0, y: 0, width: size.width, height: size.height)
                bottomImage!.draw(in: areaSize)
                
                topImage!.draw(in: CGRect(x:0, y:0, width: signatureView.frame.size.width,  height: signatureView.frame.size.height))
                
                
                let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
                UIGraphicsEndImageContext()
                
                // Here is your final combined images into a single image view.
                transactionImgView.image = newImage
                
                //Replace image into document path
                replaceImage()
                
                // Saving signatureImage from the line above to the Photo Roll.
                // The first time you do this, the app asks for access to your pictures.
                // UIImageWriteToSavedPhotosAlbum(signatureImage, nil, nil, nil)
                
                // Since the Signature is now saved to the Photo Roll, the View can be cleared anyway.
                self.signatureView.isHidden = true
                self.signatureView.clear()
                
                userdefaultObject.signaturestatus = true
                saveUserdefaults()
                retrieveUserdefaults()
                
                
            } else {
                
                self.showErrorAlert(errMessage: appDelegate.getlocaLizationLanguage(key: "Please Sign the Ticket"))
                
            }
            
        } else {

            self.showErrorAlert(errMessage: appDelegate.getlocaLizationLanguage(key: "Already Signature Saved"))
            
        }
    }
    
    
    
    func mergecheckedImage () {
        
        sealImg.isHidden = false
        
        let bottomImage = transactionImgView.image
        let topImage = sealImg.image
        
        let size = CGSize(width: transactionImgView.frame.size.width , height: transactionImgView.frame.size.height)
        UIGraphicsBeginImageContext(size)
        
        let areaSize = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        bottomImage!.draw(in: areaSize)
        
        // topImage!.drawInRect(areaSize, blendMode: kCGBlendModeNormal, alpha: 0.8)
        // topImage!.drawInRect(CGRectMake(0, 0, topImage!.size.width, topImage!.size.height))
        
        topImage!.draw(in: CGRect(x:sealImg.frame.origin.x, y:sealImg.frame.origin.y, width: sealImg.frame.size.width,  height: sealImg.frame.size.height))
        
        
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        // I've added an UIImageView, You can change as per your requirement.
        // let mergeImageView = UIImageView(frame: CGRect(x:0, y: 200, width: 355, height: 260))
        
        // Here is your final combined images into a single image view.
        transactionImgView.image = newImage
        sealImg.isHidden = true
        
    }
    
    
    @IBAction func reclaimBtn(_ sender: Any) {
        
        if userdefaultObject.phonegallerystatus == false && userdefaultObject.tearingstatus == true{
            
            // This  line save photo into Album Give Saved or Error Alerts
             UIImageWriteToSavedPhotosAlbum(transactionImgView.image!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
            
           // TicketGalleryCustomPhotoAlbum.sharedInstance.save(image: transactionImgView.image!)
            // customAlbum.save(image: transactionImgView.image!)
            
            //Change label color
            reclaimLblOut.backgroundColor = UIColor.init(red: 255.0/255.0, green: 20.0/255.0, blue: 147.0/255.0, alpha: 1)
            
            userdefaultObject.phonegallerystatus = true
            saveUserdefaults()
            retrieveUserdefaults()
            
            if userdefaultObject.phonegallerystatus == false {
                
                reclaimLblOut.backgroundColor = UIColor.init(red: 255.0/255.0, green: 20.0/255.0, blue: 147.0/255.0, alpha: 1)
                
            } else {
                
                reclaimLblOut.backgroundColor = UIColor.red
                
            }

            
        } else {
            
            if userdefaultObject.phonegallerystatus == true {
                
                self.showErrorAlert(errMessage: appDelegate.getlocaLizationLanguage(key: "Already Reclaimed"))

                
            }
            
            if userdefaultObject.tearingstatus == false {
                
                self.showErrorAlert(errMessage: appDelegate.getlocaLizationLanguage(key: "After check Ticket only you able to Reclaim"))

            }
        }
        
    }
    
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        
        if let error = error {
            // we got back an error!
            
            self.showErrorAlert(errMessage: error.localizedDescription)

            
        } else {
            
             self.showErrorAlert(errMessage: appDelegate.getlocaLizationLanguage(key: "Image stored in phone Gallery"))
            
        }
        
    }
    
    func splitImage(image2D: UIImage) -> [UIImage] {
        
        let imgWidth = image2D.size.width / 2
        let imgHeight = image2D.size.height / 2
        var imgImages:[UIImage] = []
        
        let leftHigh = CGRect(x: 0, y: 0, width: imgWidth, height: imgHeight)
        let rightHigh = CGRect(x: imgWidth, y: 0, width: imgHeight, height: imgHeight)
        let leftLow = CGRect(x: 0, y: imgHeight, width: imgWidth, height: imgHeight)
        let rightLow = CGRect(x: imgWidth, y: imgHeight, width: imgWidth, height: imgHeight)
        
        let leftQH = image2D.cgImage?.cropping(to:leftHigh)
        let rightHQ = image2D.cgImage?.cropping(to:rightHigh)
        let leftQL = image2D.cgImage?.cropping(to:leftLow)
        let rightQL = image2D.cgImage?.cropping(to:rightLow)
        
        imgImages.append(UIImage(cgImage: leftQH!))
        imgImages.append(UIImage(cgImage: rightHQ!))
        imgImages.append(UIImage(cgImage: leftQL!))
        imgImages.append(UIImage(cgImage: rightQL!))
        
        return imgImages
        
    }
    
    
    //MARK:GOBACK VIEW
    
    @IBOutlet var gobackView: UIView!
    @IBOutlet var goBackImgView: UIImageView!
    @IBOutlet var backBtnOut: UIButton!
    
    @IBAction func goBackBtn(_ sender: Any) {
        
        gobackView.isHidden = true
        timerView.isHidden = true
        transactionpopupview.isHidden = true
        noticketfoundLbl.isHidden = true
        
    }
    
    
    //MARK:COLLECTIONVIEW
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        var size = CGSize()
        
        if(DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS) {
            
            size = CGSize(width: 150.0, height: 200.0)
            
        } else if (DeviceType.IS_IPHONE_6) {
            
            size =  CGSize(width: 150.0, height: 200.0)
            
        }else if (DeviceType.IS_IPHONE_6P) {
            
            size = CGSize(width: 180.0, height: 230.0)
            
        }else if (DeviceType.IS_IPHONE_X) {
            
            size = CGSize(width: 180.0, height: 230.0)
            
        }
        
        return size
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesDisplayArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! TicketGalleryCollectionViewCell
        
       
        /////Retrieve stored object
        let defaultval = UserDefaults.standard
        let decoded  = defaultval.object(forKey:imagesNameArr[indexPath.row]) as! Data
        let storeObj = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! TicketGallery
        println_debug("store cell obj-------\(storeObj.timervalue)")
        let timer = timercellcheck(startTime: storeObj.timervalue)
        println_debug("store obj cell timer-------\(timer)")
        
        if timer > 14400 {
            
            // Use the outlet in our custom class to get a reference to the UILabel in the cell
            cell.statusLbl.isHidden = false
            cell.statusLbl.text = "Timeup"
            
        } else {
            
            cell.statusLbl.isHidden = true
        }
        
    
        let image  = UIImage(contentsOfFile: imagesDisplayArr[indexPath.row])
        cell.imageView.image = image
        cell.contentView.backgroundColor = UIColor.black
        
        cell.backgroundColor = UIColor.black
        cell.imageView.backgroundColor = UIColor.black
        cell.selectedBackgroundView?.backgroundColor = .black
        
        let view = UIView(frame: cell.bounds)
        // Set background color that you want
        view.backgroundColor = UIColor.blue
        cell.selectedBackgroundView = view
        cell.layoutIfNeeded()

        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        
        // handle tap events
        println_debug("You selected cell #\(indexPath.section)!")
        
       
        //////
        let image  = UIImage(contentsOfFile: imagesDisplayArr[indexPath.row])
        transactionImgView.image = image
        goBackImgView.image = image
        
        self.signatureView.isHidden = false
        
        selectedImagePath = imagesDisplayArr[indexPath.row]
        println_debug("selected imagepath\(selectedImagePath)")
        
        selectedImageName = (selectedImagePath as NSString).lastPathComponent
        println_debug("selected imagepath last component\(selectedImageName)")
        
        /////Retrieve stored object
        let defaultval = UserDefaults.standard
        let decoded  = defaultval.object(forKey:imagesNameArr[indexPath.row]) as! Data
        userdefaultObject = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? TicketGallery
        println_debug("store cell obj-------\(userdefaultObject.timervalue)")
        let timercount = timercellcheck(startTime: userdefaultObject.timervalue)
        println_debug("store obj cell timer-------\(timer)")
        
        if userdefaultObject.tearingstatus == false {
            
            tickettearLbl.backgroundColor = UIColor.init(red: 65.0/255.0, green: 129.0/255.0, blue: 10.0/255.0, alpha: 1)
            
        } else {
            
            tickettearLbl.backgroundColor = UIColor.red

        }
        
        if userdefaultObject.phonegallerystatus == false {
            
            reclaimLblOut.backgroundColor = UIColor.init(red: 255.0/255.0, green: 20.0/255.0, blue: 147.0/255.0, alpha: 1)
            
        } else {
            
            reclaimLblOut.backgroundColor = UIColor.red

        }

        
        
        if timercount <= 14400.0 {
            
            if userdefaultObject.signaturestatus == false {
                
                signatureView.isUserInteractionEnabled = true

            } else {
                signatureView.isUserInteractionEnabled = false

            }
            
            gobackView.isHidden = true
            timerView.isHidden = false
            transactionpopupview.isHidden = false
            noticketfoundLbl.isHidden = true
            
            timeCount = 0
            timeCount = 14400 - timercount
            
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
            
        } else {
            
            timeCount = 0
            transactionpopupview.isHidden = false
            gobackView.isHidden = false
            timerView.isHidden = true

        }
        
        // retrieveUserdefaults()
        
    }
    
     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.black
    }
}





// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}
