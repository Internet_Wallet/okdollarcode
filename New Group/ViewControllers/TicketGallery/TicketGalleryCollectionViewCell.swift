//
//  MyCollectionViewCell.swift
//  Ticketgallery
//
//  Created by Prabu on 11/9/17.
//  Copyright © 2017 prabu. All rights reserved.
//

import UIKit

class TicketGalleryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var statusLbl: UILabel!
    @IBOutlet var imageView: UIImageView!
    
}
