//
//  BankWalletViewController.swift
//  Demo
//
//  Created by isure on 8/6/19.
//  Copyright © 2019 isure. All rights reserved.
//

import UIKit

class CLCBankWalletViewController: OKBaseController {

    @IBOutlet weak var generateCodeButton: UIButton!
    @IBOutlet weak var withDrawLabel: UILabel!
    @IBOutlet weak var cardlessBankLabel: UILabel!
    @IBOutlet weak var withdrawalImagewView: UIImageView!
    @IBOutlet weak var cardlessImageView: UIImageView!
    @IBOutlet weak var walletView: UIView!
    @IBOutlet weak var okWalletBalanceLabel: UILabel!
    @IBOutlet weak var okWalletLabel: UILabel!
    @IBOutlet weak var amopuntPickerView: UIPickerView!

    var dimensionObj = [DimensionList]()
    var isPlayingPickerView = false
    var bankViewModelObj = BankDetailsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }

    @IBAction func onClickGenerateCode(_ sender: Any) {
        getReferenceCode()
    }
    
}

extension CLCBankWalletViewController : UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dimensionObj.count
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return amopuntPickerView.frame.size.height/3
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dimensionObj[row].dimenssionValue ?? ""
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
    
    let rowSize = pickerView.rowSize(forComponent: component)
    let width = rowSize.width
    let height = rowSize.height
    let frame = CGRect(x: 0, y: 0, width: width, height: height)
    let label = UILabel(frame: frame)
    label.textAlignment = .center
    label.text =  wrapAmountWithCommaDecimal(key: "\(dimensionObj[row].dimenssionValue ?? "")")  
    label.font = UIFont(name:appFont, size: 17.0)
        if row == 1 {
            
            if !isPlayingPickerView {
                
                label.textColor = .red
            }else{
                label.textColor = .black
            }
        }else{
            label.textColor = .black
        }
        
    return label
    }                           
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        isPlayingPickerView = true
        guard let label = pickerView.view(forRow: row, forComponent: component) as? UILabel else {
            return
        }
        label.textColor = .red
    }
    
    
}

extension CLCBankWalletViewController {
    
    fileprivate func setUI(){
        generateCodeButton.setTitle("Generate Code", for: .normal)
        cardlessBankLabel.text = "Cardless Bank Charges Fee 200 MMK."
        self.okWalletLabel.text = "OK$ Wallet Balance"
        withDrawLabel.text = "You can withdraw money CB Bank ATM, CB Bank Branch or Agent"
        self.okWalletBalanceLabel.attributedText = self.walletBalance()
        self.configureBackButton(withTitle: "Cardless Cash Withdrawal")
        walletView.layer.cornerRadius = 5.0
        amopuntPickerView.layer.cornerRadius = 5.0
        amopuntPickerView.delegate = self
        amopuntPickerView.dataSource = self
        withdrawalImagewView.roundedCorner()
        cardlessImageView.roundedCorner()
        amopuntPickerView.selectRow(1, inComponent: 0, animated: true)
        amopuntPickerView.reloadAllComponents()
        
    }
    
    
    fileprivate func getReferenceCode(){
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            bankViewModelObj.response.removeAll()
            bankViewModelObj.getBankReferenceCode(completion: { (success,message) in
                self.removeProgressView()
                if success {
                    DispatchQueue.main.async{
                        if let obj = self.storyboard?.instantiateViewController(withIdentifier: "CLCBankAccountViewController") as? CLCBankAccountViewController{
                            obj.bankReferenceId = self.bankViewModelObj.referenceID
                            self.navigationController?.pushViewController(obj, animated: true)
                        }
                    }
                }else{
                    alertViewObj.wrapAlert(title: nil, body:message, img: #imageLiteral(resourceName: "bill_splitter"))
                    alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                    }
                    alertViewObj.showAlert(controller: self)
                }
            })
        }else{
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            alertViewObj.showAlert(controller: UIViewController.init())
            
        }
        
    }
}





