//
//  BankAccountViewController.swift
//  Demo
//
//  Created by isure on 8/6/19.
//  Copyright © 2019 isure. All rights reserved.
//

import UIKit

class CLCBankAccountViewController: OKBaseController {

    @IBOutlet weak var doneButtonOutlet: UIButton!
    @IBOutlet weak var sendCodeLabel: UILabel!
    @IBOutlet weak var cardlessWithDraw: UILabel!
    @IBOutlet weak var referenceCodeLabel: UILabel!
    @IBOutlet weak var screenShotView: UIView!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var amountView: UIView!
    @IBOutlet weak var amountLabel: UILabel!
    var bankReferenceId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }

    @IBAction func onClickDone(_ sender: Any) {
       let viewControllers = self.navigationController?.viewControllers
       if let vc = viewControllers?[2] as? CardlessCashHomeVC {
           self.navigationController?.popToViewController(vc, animated: true)
       }
    }
}

extension CLCBankAccountViewController {
    
     func shareQRWithDetail() {
        let newImage =  self.screenShotView.toImage()
        let activityViewController = UIActivityViewController(activityItems: [newImage], applicationActivities: nil)
        DispatchQueue.main.async {self.navigationController?.present(activityViewController, animated: true, completion: nil)}
    }
    
    fileprivate func setUI(){
        sendCodeLabel.text =  "Send Code to someone"
        cardlessWithDraw.text = "Select Cardless Withdrawal at CB Bank ATM:"
        referenceCodeLabel.text = "Your Cash Withdrawal Reference Code is"
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.hidesBackButton = true
        self.title = "Cardless Cash Withdrawal"
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        shareView.addGestureRecognizer(tap)
        amountLabel.text = bankReferenceId
        doneButtonOutlet.setTitle("Done", for: .normal)
        //self.configureBackButton(withTitle: "Cardless Cash Withdrawal")
        amountView.layer.cornerRadius = 5.0
        shareView.layer.cornerRadius = 5.0
        shareView.layer.borderWidth = 1.0
        shareView.layer.borderColor = UIColor.blue.cgColor
        
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
       
        //If yes open share sheet
        alertViewObj.wrapAlert(title: "Alert".localized, body:"Do you wish to share the Reference Code.", img: #imageLiteral(resourceName: "tax_Logo"))
        alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            self.shareQRWithDetail()
        }
        
        alertViewObj.addAction(title: "No".localized, style: .cancel) {
           
        }
        alertViewObj.showAlert(controller: UIViewController.init())
    }
    
}

