//
//  BankListModel.swift
//  OK
//
//  Created by isure on 8/8/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation


open class BankListModel{
    var dimensionObj = [DimensionList]()
    var bankName: String?
    
    public init(data: Dictionary<String,Any>){
        if data.count>0{
            self.bankName = data["BankName"] as? String
            
            if let data = data["DimenssionList"] as? NSArray {
                    if data.count > 0 {
                        self.dimensionObj = data.compactMap{DimensionList(data: $0 as! NSDictionary)}
                    }
            }
        }
    }
}


open class DimensionList {
    var dimenssionValue: String?
    
    public init(data: NSDictionary){
        if data.count>0{
        self.dimenssionValue =  data.value(forKey: "DimenssionValue") as? String
        }
    }
}
