//
//  Extension.swift
//  Demo
//
//  Created by isure on 8/6/19.
//  Copyright © 2019 isure. All rights reserved.
//

import Foundation
import UIKit



extension UICollectionViewCell{
    
    //this will set rounded border with shadow
    func roundedBorderWithShadow(borderColor: UIColor,shadowColor: UIColor,backgroundColor: UIColor){
        self.layer.cornerRadius = 10
        self.layer.borderWidth = 1.0
        self.layer.borderColor = borderColor.cgColor
        self.layer.backgroundColor = backgroundColor.cgColor
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = CGSize(width: 2.0, height: 4.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
        
    }
    
}


extension UIImageView{
    
    func roundedCorner(){
        self.layer.borderWidth = 1.0
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
    }
    
}
