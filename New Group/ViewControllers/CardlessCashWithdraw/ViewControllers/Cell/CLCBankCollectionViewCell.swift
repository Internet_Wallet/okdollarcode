//
//  BankCollectionViewCell.swift
//  Demo
//
//  Created by isure on 8/6/19.
//  Copyright © 2019 isure. All rights reserved.
//

import UIKit

class CLCBankCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bankNameLabel: UILabel!
    @IBOutlet weak var bankImageView: UIImageView!
}
