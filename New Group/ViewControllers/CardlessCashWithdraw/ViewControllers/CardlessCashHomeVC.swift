//
//  ViewController.swift
//  Demo
//
//  Created by isure on 8/6/19.
//  Copyright © 2019 isure. All rights reserved.
//

import UIKit

class CardlessCashHomeVC: OKBaseController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var selectBankLabel: UILabel!
    
    var bankViewModelObj = BankDetailsViewModel()
    var index = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getBank()
        self.configureBackButton(withTitle: "Cardless Cash Withdrawal")
    }
    
}

extension CardlessCashHomeVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if bankViewModelObj.response.count > 0{
            return bankViewModelObj.response.count
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
     
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CLCBankCollectionViewCell", for: indexPath) as? CLCBankCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.roundedBorderWithShadow(borderColor: .lightGray,shadowColor: .gray,backgroundColor: .white)
        cell.bankNameLabel.text = bankViewModelObj.response[indexPath.row].bankName ?? ""
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width/2) - 10 , height: (collectionView.frame.size.width/2) - 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        index = indexPath.row
        OKPayment.main.authenticate(screenName: "CardlessCash", delegate: self)
    }
    
}

extension CardlessCashHomeVC: BioMetricLoginDelegate {
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if(isSuccessful) {
            if(screen == "CardlessCash") {
                if let obj = self.storyboard?.instantiateViewController(withIdentifier: "CLCBankWalletViewController") as? CLCBankWalletViewController{
                    obj.dimensionObj = bankViewModelObj.response[index].dimensionObj
                    self.navigationController?.pushViewController(obj, animated: true)
                }
            }
        }
        else {
            println_debug("User not authorized")
        }
    }
}


extension CardlessCashHomeVC{
    
    fileprivate func getBank(){
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            bankViewModelObj.response.removeAll()
            bankViewModelObj.getBankListWithAmount(completion: { (success,message) in
                self.removeProgressView()
                if success {
                    DispatchQueue.main.async{
                        self.collectionView.delegate  = self
                        self.collectionView.dataSource = self
                        self.collectionView.reloadData()
                    }
                }else{
                    alertViewObj.wrapAlert(title: nil, body:message, img: #imageLiteral(resourceName: "bill_splitter"))
                    alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                    }
                    alertViewObj.showAlert(controller: self)
                }
            })
        }else{
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            alertViewObj.showAlert(controller: UIViewController.init())
            
        }
        
    }
}
        
        


