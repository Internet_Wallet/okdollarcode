//
//  CardlessParameters.swift
//  OK
//
//  Created by isure on 8/8/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation
import UIKit


class CardlessParameters: NSObject {
    
    
    static func getParamForBankDetails(AppId: String,Limit: String,MobileNumber: String,Msid: String,Offset: String,Ostype: String,Otp: String,Simid: String) -> Dictionary<String,Any> {
       
        var bankDetailParam  = Dictionary<String,Any>()
        bankDetailParam["AppId"] = AppId
        bankDetailParam["Limit"] = Limit
        bankDetailParam["MobileNumber"] = MobileNumber
        bankDetailParam["Msid"] = Msid
        bankDetailParam["Offset"] = Offset
        bankDetailParam["Ostype"] = Ostype
        bankDetailParam["Otp"] = Otp
        bankDetailParam["Simid"] = Simid
        
        return bankDetailParam
    }
    
    static func getSinglePayParamas() -> [String: Any]? {
        var arrayParam = [String: Any]()
        var TransactionsCellTower = [String: Any]()
        TransactionsCellTower["Lac"] = ""
        TransactionsCellTower["Mcc"] = mccStatus.mcc
        TransactionsCellTower["Mnc"] = mccStatus.mnc
        TransactionsCellTower["SignalStrength"] = ""
        TransactionsCellTower["NetworkType"] = UitilityClass.getNetworkType()
        TransactionsCellTower["NetworkSpeed"] = UitilityClass.getSignalStrength()
        
        if UitilityClass.isConnectedToWifi(){
            let wifiInfo = UitilityClass.getNetworkInfos()[0].components(separatedBy: ",")
            TransactionsCellTower["ConnectedwifiName"] = wifiInfo[0]
            TransactionsCellTower["Ssid"] = wifiInfo[0]
            TransactionsCellTower["Mac"] = wifiInfo[1]
        }else{
            TransactionsCellTower["ConnectedwifiName"] = ""
            TransactionsCellTower["Ssid"] = ""
            TransactionsCellTower["Mac"] = ""
        }
        
        // l External Reference
        var lExternalReference = [String: Any]()
        lExternalReference["Direction"] = true
        lExternalReference["EndStation"] = ""
        lExternalReference["StartStation"] = ""
        lExternalReference["VendorID"] = ""
        
        // L geo Location
        var lGeoLocation = [String: Any]()
        lGeoLocation["CellID"] = ""
        
        let lat  = (GeoLocationManager.shared.currentLatitude == nil) ? "0.0" : GeoLocationManager.shared.currentLatitude
        let long = (GeoLocationManager.shared.currentLongitude == nil) ? "0.0" : GeoLocationManager.shared.currentLongitude
        
        lGeoLocation["Latitude"] = lat ?? "0.0"
        lGeoLocation["Longitude"] = long ?? "0.0"
        
        // L Proximity
        var lProximity = [String: Any]()
        lProximity["BlueToothUsers"]  = [Any]()
        lProximity["SelectionMode"]   = false
        lProximity["WifiUsers"]       = [Any]()
        lProximity["mBltDevice"]      = ""
        lProximity["mWifiDevice"]    = ""
        
        //comments
        //let gender = UserModel.shared.gender
        //let age    = UserModel.shared.ageNow
        //let area    = ""
        let senderBusiness = UserModel.shared.businessName
        //let senderName = UserModel.shared.name
        let aName = UserModel.shared.name.uppercased()
        
        let techComments = String.init(format: "[%@,%@,%@,%@,%@,%@,##%@##%@##%@##]-OKNAME-%@", GeoLocationManager.shared.currentLatitude, GeoLocationManager.shared.currentLongitude, "",UserModel.shared.gender,UserModel.shared.ageNow,"", "" as CVarArg,"" as CVarArg, senderBusiness as CVarArg ,aName as CVarArg)
        
        func replaceEscapeCharacter(_ value: String) -> String {
            if value == "" { return ""}
            var str = value.replacingOccurrences(of: "$", with: "")
            str = value.replacingOccurrences(of: "&", with: "AND")
            str = value.replacingOccurrences(of: "/", with: "")
            str = value.replacingOccurrences(of: "<", with: "")
            str = value.replacingOccurrences(of: ">", with: "")
            return str
        }
        
        var final_comment = ""
        
        final_comment = String.init(format: "%@%@", replaceEscapeCharacter(
            ""), techComments)
        
        
        // lTransactions//
        var lTransactionsDict = [String: Any]()
        lTransactionsDict["Amount"]                = "1"
        lTransactionsDict["DestinationNumberAccountType"] = ""
        lTransactionsDict["Balance"]                = ""
        lTransactionsDict["BonusPoint"]             = 0
        lTransactionsDict["CashBackFlag"]           = 0 // need to check the flag value
        lTransactionsDict["Comments"]               = final_comment
        lTransactionsDict["Destination"]            = "00959975278302"
        lTransactionsDict["DiscountPayTo"]          = "0" // discount need to check from the server
        lTransactionsDict["IsMectelTopUp"]          = false
        lTransactionsDict["KickBack"]               =  ""
        lTransactionsDict["KickBackMsisdn"]         = ""
        lTransactionsDict["LocalTransactionType"]   = "PAYTO"
        lTransactionsDict["MerchantName"]           = ""
        lTransactionsDict["MobileNumber"]           = UserModel.shared.mobileNo
        lTransactionsDict["Mode"]                   = true
        lTransactionsDict["Password"]               = ok_password ?? ""
        lTransactionsDict["PromoCodeId"]            = ""
        lTransactionsDict["ResultCode"]             = 0
        lTransactionsDict["ResultDescription"]      = ""
        lTransactionsDict["SecureToken"]            = UserLogin.shared.token
        lTransactionsDict["TransactionID"]          =  ""
        lTransactionsDict["TransactionTime"]        = Date.init(timeIntervalSinceNow: 0).stringValue()
        lTransactionsDict["TransactionType"]        = "PAYTO"
        lTransactionsDict["accounType"]             = UserModel.shared.agentServiceTypeString()
        
        arrayParam["TransactionsCellTower"] = TransactionsCellTower
        arrayParam["lExternalReference"]    = lExternalReference
        arrayParam["lGeoLocation"]          = lGeoLocation
        arrayParam["lProximity"]            = lProximity
        arrayParam["lTransactions"]         = lTransactionsDict
        return arrayParam
        
    }
    
    
}
