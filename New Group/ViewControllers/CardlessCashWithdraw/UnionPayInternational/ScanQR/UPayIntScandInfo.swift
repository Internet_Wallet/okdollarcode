//
//  UPayIntScandInfo.swift
//  OK
//
//  Created by Sam' MacBook on 10/08/20.
//  Copyright © 2020 Sam. All rights reserved.
//

import UIKit

class UPayIntScandInfo: OKBaseController {
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var rightViewBG: UIView!
    @IBOutlet weak var tbOption: UITableView!
    @IBOutlet weak var tbScanInfo: UITableView!
    let listOption = ["History".localized,"help".localized,"Refund","Offers / Promotion".localized, "Merchant List".localized]
    var listTitle = ["Merchant".localized,"Merchant ID".localized,"Bill Currency".localized, "Bill Amount".localized,"Exchange Rate".localized,"Converted Amount".localized,"Remark".localized]
    
    var listImage = ["merchantUPI","midUPI","midUPI","merchantAmount","exchangeUPI","amount","merchantRemarkNew"]
    
    var listValue = ["","","", "","","",""]
    var currencyCode = ""
    
    var infoDic: Dictionary<String,String>?
    var qrInfo: UPIQRCodeInfoModel?
    var currancy : Float = 0
    var netAmout : Float = 0
    var discout : Float = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if let hasCode = qrInfo{
            listValue[0] = infoDic?["merchant_name"] ?? ""
            listValue[1] = infoDic?["merchant_id"] ?? ""
            let valueExist = UitilityClass.returnUPICurrencyDetail(code: hasCode.data.trxInfo.trxCurrency)
            listValue[2] = valueExist?["Currency"] as? String ?? infoDic?["bill_currency"] ?? "" + "\(valueExist?["AlphabeticCode"] as? String ?? infoDic?["bill_currency"] ?? "")"
            currencyCode = valueExist?["AlphabeticCode"] as? String ?? infoDic?["bill_currency"] ?? ""
            listValue[3] = infoDic?["bill_Amount"] ?? ""
            listValue[4] = infoDic?["exchange_rate"] ?? ""
            listValue[5] = infoDic?["converted_amount"] ?? ""
            listValue[6] = infoDic?["remark"] ?? ""
        }
        
        rightViewBG.isHidden = true
        btnNext.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        self.configureRightButton(imgName: "menu_white_payto")
        
        let amount = Float(listValue[3]) ?? 0
        discout = Float(listValue[4]) ?? 0
        currancy = Float(listValue[6]) ?? 0
       
//        netAmout = amount - self.calculatePercentage(value: amount, percentageVal: discout)
//        listValue[5] =  "\(netAmout)"
//        listValue[7] = "\(netAmout * currancy)"
        tbScanInfo.tableFooterView = UIView()
        tbScanInfo.reloadData()
        self.configureBackButton(withTitle: "")
        
        if listValue[3] == "0" {
            btnNext.isUserInteractionEnabled = false
            btnNext.backgroundColor = .lightGray
            btnNext.titleLabel?.textColor = .white
            if let cell = tbScanInfo.cellForRow(at: IndexPath(row: 3, section: 0)) as? UPIScanAmountCell {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    cell.tfAmount.becomeFirstResponder()
                    cell.lblRequired.isHidden = false
                    
                }
            }
        }
    }
    
    func configureRightButton(imgName: String) {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: imgName), for: UIControl.State.normal)
        button.tintColor = UIColor.white
        button.sizeToFit()
        button.addTarget(self, action: #selector(onClickRightAction), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
       self.navigationItem.title = title
       let imageView = UIImageView(image: UIImage(named: "upi3"))
       self.navigationItem.titleView = imageView
    }
    
    @objc func onClickRightAction() {
        rightViewBG.isHidden = !rightViewBG.isHidden
    }
    
    
    @IBAction func onClickOptionHideAction(_: UIButton) {
        if !rightViewBG.isHidden {
            rightViewBG.isHidden  = true
        }
    }
    
    @IBAction func onClickNextAction(_: UIButton) {
        DispatchQueue.main.async {
            let cardlessCashStory = UIStoryboard(name: "CardLessCashStoryBoard", bundle: nil)
            guard let vc = cardlessCashStory.instantiateViewController(withIdentifier: "UPayIntCheckDetails") as? UPayIntCheckDetails else { return }
            vc.finalDic = self.infoDic
            vc.qrInfo = self.qrInfo
            vc.values = self.listValue
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    func calculatePercentage(value: Float,percentageVal:Float)-> Float {
        let val = value * percentageVal
        return val / 100.0
    }
    
    
}

extension UPayIntScandInfo: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 100 {
            return 7
        }else {
            return listOption.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 100 {
            
            switch indexPath.row {
            //this will show merchant data
            case 0,1,2,4,5:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "UPIScanInfoCell") as? UPIScanInfoCell else{
                    return UITableViewCell()
                }
                
                if indexPath.row == 4{
                    let currentRate = infoDic?["exchange_rate"] ?? ""
                    let x = Double(currentRate) ?? 0.0
                    cell.wrapData(customtitle: listTitle[indexPath.row],customValue: "1 \(currencyCode) = \(Double(round(100*x)/100)) MMK",customImage: listImage[indexPath.row])
                }else{
                    cell.wrapData(customtitle: listTitle[indexPath.row],customValue: listValue[indexPath.row],customImage: listImage[indexPath.row])
                }
                
                cell.selectionStyle = .none
                return cell
                
            case 3:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "UPIScanAmountCell") as? UPIScanAmountCell else{
                    return UITableViewCell()
                }
                cell.tfAmount.keyboardType = .numberPad
                cell.tfAmount.addTarget(self, action: #selector(textFieldEditingChanged), for: .editingChanged)
                cell.tfAmount.delegate = self
                cell.selectionStyle = .none
                if listValue[indexPath.row] == "0"{
                    cell.wrapData(customTitle: listTitle[indexPath.row],customValue: "",customePalceHolder: "Enter Amount",lblCurrency: currencyCode,tag: indexPath.row,imageName: listImage[indexPath.row],userTouch: true,isLabelrequired:false)
                }else{
                    cell.wrapData(customTitle: listTitle[indexPath.row],customValue: listValue[indexPath.row],customePalceHolder: "Enter Amount",lblCurrency: currencyCode,tag: indexPath.row,imageName: listImage[indexPath.row],userTouch: false,isLabelrequired:true)
                }
                
                return cell
                
            case 6:
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "UPIScanRemarkCell") as? UPIScanRemarkCell else{
                    return UITableViewCell()
                }
                cell.tfRemark.delegate = self
                cell.selectionStyle = .none
                cell.wrapData(customTitle: listTitle[indexPath.row], customValue: listValue[indexPath.row], customePalceHolder: "Enter Remark", tag: indexPath.row, imageName: listImage[indexPath.row])
                
                return cell
                
            default:
                break
            }
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UPIOptionCell", for: indexPath) as! UPIOptionCell
            cell.lblOption.text = listOption[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 100 {
            if indexPath.row == 6 {
                return UITableView.automaticDimension
            }else{
                return 70.0
            }
        }else {
            return 40
        }
    }
    
}

extension UPayIntScandInfo : UITextFieldDelegate {
    
    @objc func textFieldEditingChanged() {
        if let cell = tbScanInfo.cellForRow(at: IndexPath(row: 3, section: 0)) as? UPIScanAmountCell {
            if cell.tfAmount.text?.count == 0 {
                cell.lalTitle.text = "Enter Bill Amount *".localized
                cell.lalTitle.textColor = .red
                cell.lblRequired.isHidden = false
            }else {
                cell.lalTitle.text = "Bill Amount".localized
                cell.lalTitle.textColor = #colorLiteral(red: 0.0867683813, green: 0.1547777355, blue: 0.649089396, alpha: 1)
                cell.lblRequired.isHidden = true
                
            }
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.tag == 3 {
            
            if let cell = tbScanInfo.cellForRow(at: IndexPath(row: 5, section: 0)) as? UPIScanInfoCell {
                let currentRate = infoDic?["exchange_rate"] ?? ""
             
                if textField.text != ""{
                    let x = Double(currentRate) ?? 0.0
                    let value = Double(round(100*x)/100)
                    let new = Double(textField.text ?? "0.0")
                    if let hasValue = new{
                        cell.lalValue.text = "\(hasValue * value) MMK"
                    }
                }
            }
            
            if textField.text == "" {
                btnNext.isUserInteractionEnabled = false
                btnNext.backgroundColor = .darkGray
                btnNext.titleLabel?.textColor = .white
            }else {
                btnNext.isUserInteractionEnabled = true
                btnNext.backgroundColor = #colorLiteral(red: 0.9961667657, green: 0.7674615979, blue: 0, alpha: 1)
                btnNext.titleLabel?.textColor = #colorLiteral(red: 0.0867683813, green: 0.1547777355, blue: 0.649089396, alpha: 1)
            }
          
        }else if textField.tag == 6 {
            println_debug("Remark Feild")
            listValue[6] = textField.text ?? ""
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 3 {
            textField.keyboardType = .numberPad
        }
    }
}


class UPIScanInfoCell: UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lalTitle: UILabel!
    @IBOutlet weak var lalValue: UILabel!
    @IBOutlet weak var btnInfo: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lalTitle.font = UIFont(name: appFont, size: 12)
        lalValue.font = UIFont(name: appFont, size: 16)
        btnInfo.isHidden = true
    }
    
    func wrapData(customtitle: String,customValue: String,customImage: String){
        lalTitle.text = customtitle
        lalValue.text = customValue
        imgIcon.image = UIImage(named: customImage)
    }
    
}


class UPIScanAmountCell: UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lalTitle: UILabel!
    @IBOutlet weak var tfAmount: UITextField!
    @IBOutlet weak var lblCurrancy: UILabel!
    @IBOutlet weak var lblRequired: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lalTitle.font = UIFont(name: appFont, size: 12)
        tfAmount.font = UIFont(name: appFont, size: 16)
        lblCurrancy.font = UIFont(name: appFont, size: 17)
    }
    
    func wrapData(customTitle: String,customValue: String,customePalceHolder: String,lblCurrency: String,tag: Int,imageName: String,userTouch: Bool,isLabelrequired: Bool){
        lalTitle.text = customTitle
        lblCurrancy.text = lblCurrency
        imgIcon.image = UIImage(named: imageName)
        tfAmount.tag = tag
        tfAmount.placeholder = customePalceHolder
        tfAmount.text = customValue
        tfAmount.isUserInteractionEnabled = userTouch
        lblRequired.isHidden = isLabelrequired
    }
    
}

class UPIScanRemarkCell: UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lalTitle: UILabel!
    @IBOutlet weak var tfRemark: UITextField!
    @IBOutlet weak var lblOptional: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lalTitle.font = UIFont(name: appFont, size: 12)
        tfRemark.font = UIFont(name: appFont, size: 16)
        lblOptional.font = UIFont(name: appFont, size: 15)
    }
    
    
    func wrapData(customTitle: String,customValue: String,customePalceHolder: String,tag: Int,imageName: String){
        lalTitle.text = customTitle
        tfRemark.text = customValue
        tfRemark.placeholder = customePalceHolder
        imgIcon.image = UIImage(named: imageName)
       
    }
    
}



class UPIScanDiscountCell: UITableViewCell {
    @IBOutlet var topConstant: NSLayoutConstraint!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lalTitle: UILabel!
    @IBOutlet weak var tfDiscont: UITextField!
    @IBOutlet weak var lblDiscount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lalTitle.font = UIFont(name: appFont, size: 12)
        tfDiscont.font = UIFont(name: appFont, size: 16)
        lblDiscount.font = UIFont(name: appFont, size: 10)
        lblDiscount.layer.cornerRadius = 10.0
        lblDiscount.layer.masksToBounds = true
    }
    
}
