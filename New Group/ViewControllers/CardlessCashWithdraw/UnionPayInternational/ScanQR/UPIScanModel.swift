//
//  UPIScanModel.swift
//  OK
//
//  Created by Sam' MacBook on 13/08/20.
//  Copyright © 2020 Sam. All rights reserved.
//

import UIKit

// MARK: - UPI Enrollment Params
struct UPIEnrollmentParams: Codable {
    let mobileNo, deviceID, amount, fcm ,firstName: String
    let lastName: String
    
    enum CodingKeys: String, CodingKey {
        case mobileNo = "MobileNo"
        case deviceID = "DeviceId"
        case amount = "Amount"
        case firstName = "FirstName"
        case lastName = "LastName"
        case fcm = "FCM"
    }
}


func wrapEnrollParams() -> Data? {
    let model = UPIEnrollmentParams.init(mobileNo: UserModel.shared.mobileNo, deviceID: uuid, amount: UserLogin.shared.walletBal, fcm: UserDefaults.standard.string(forKey: "FCMToken") ?? "",  firstName: UserModel.shared.name, lastName: "" )
    do {
        let data  =  try JSONEncoder().encode(model)
        println_debug(String(data: data, encoding: .utf8))
        return data
    } catch {
        println_debug(error)
    }
    return Data.init()
}

// MARK: - UPI Enrollment Response
struct UPIEnrollmentModel: Codable {
    let message: String
    let data: UPIEnrollment
    let code: String

    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case data = "Data"
        case code = "Code"
    }
}

// MARK: - DataClass
struct UPIEnrollment: Codable {
    let id: Int
    let maskedPan, pan, cardFaceID, token: String
    let cvn2, prdNo, cardType, cardMed: String
    let expiryDate, tokenExpiry: String
    let cardState: Bool
    let encryptMNo: String
    let createDateTime, updateDateTime, name, okMobileNo: String
}


// MARK: - UPI QR Code Params
struct UPIQRInfoParams: Codable {
    let mobileNo, token, deviceID, mpQrcPayload: String
    let amount, password: String
    
    enum CodingKeys: String, CodingKey {
        case mobileNo = "MobileNo"
        case token = "Token"
        case deviceID = "DeviceId"
        case mpQrcPayload
        case amount = "Amount"
        case password = "Password"
    }
    
    
    
}



func wrapGetQRInFoParams(token: String, qrStr: String) -> Data? {
    let model = UPIQRInfoParams.init(mobileNo: UserModel.shared.mobileNo, token: token, deviceID: uuid, mpQrcPayload: qrStr, amount:  "0", password: ok_password ?? "")
    do {
        let data  =  try JSONEncoder().encode(model)
        println_debug(String(data: data, encoding: .utf8))
        return data
    } catch {
        println_debug(error)
    }
    return Data.init()
}

// MARK: - UPIGenerateQR
struct UPIGenerateQR: Codable {
    let message: String
    let data: GenerateQRData
    let code: String

    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case data = "Data"
        case code = "Code"
    }
}

// MARK: - DataClass
struct GenerateQRData: Codable {
    let msgType, cpqrcNo, emvCpqrcPayload, barcodeCpqrcPayload: String
    let responseCode, responseMsg: String
}


// MARK: - UPI QR Code Response
struct UPIQRCodeInfoModel: Codable {
    let message: String
    let data: UPIQRCodeScan
    let code: String
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case data = "Data"
        case code = "Code"
    }
}

struct UPIQRCodeScan: Codable {
    let msgInfo: MsgInfo
    let trxInfo: TrxInfo
}

struct MsgInfo: Codable {
    let versionNo, msgID, timeStamp, msgType: String
    let insID: String
}

struct TrxInfo: Codable {
    let deviceID, userID, token, trxAmt: String
    let trxCurrency, trxCurrencyName, mpqrcPayload: String
    let merchantInfo: MerchantInfo
    let riskInfo: RiskInfo
    let additionalData: AdditionalData
}

struct AdditionalData: Codable {
    let billNo, referenceLabel, terminalLabel: String
}

struct MerchantInfo: Codable {
    let acquirerIIN, fwdIIN, mid, merchantName: String
    let mcc: String
}

struct RiskInfo: Codable {
    let captureMethod, deviceType: String
}



// MARK: - UPIExchangeParams
struct UPIExchangeParams: Codable {
    let origAmt, exchangeCurrency, origCurrency: String
}


func wrapExchangeParams(origAmt: String, exchange: String, original: String) -> Data? {
    let model = UPIExchangeParams.init(origAmt: origAmt, exchangeCurrency: exchange, origCurrency: original)
    do {
        let data  =  try JSONEncoder().encode(model)
        println_debug(String(data: data, encoding: .utf8))
        return data
    } catch {
        println_debug(error)
    }
    return Data.init()
}

struct UPIExchangeRateModel: Codable {
    let message: String
    let data: UPIExchangeRate
    let code: String

    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case data = "Data"
        case code = "Code"
    }
}

struct UPIExchangeRate: Codable {
    let exchangeAmt, exchangeRate, responseCode, responseMsg: String
}


// MARK: - UPIPaymentParams
struct UPIPaymentParams: Codable {
    let mobileNo, token, deviceID, mpQrcPayload: String
    let amount, password: String

    enum CodingKeys: String, CodingKey {
        case mobileNo = "MobileNo"
        case token = "Token"
        case deviceID = "DeviceId"
        case mpQrcPayload
        case amount = "Amount"
        case password = "Password"
    }
}


func wrapPaymentParams(mobileNo: String, token: String, deviceID: String, qrStr: String, amount: String) -> Data? {
    let model = UPIPaymentParams.init(mobileNo: mobileNo, token: token, deviceID: deviceID, mpQrcPayload: qrStr, amount: amount, password: ok_password ?? "")
    do {
        let data  =  try JSONEncoder().encode(model)
        println_debug(String(data: data, encoding: .utf8))
        return data
    } catch {
        println_debug(error)
    }
    return Data.init()
}


// MARK: - UPIPaymentModel
struct UPIPaymentModel: Codable {
    let message: String
    let data: UPIPayment
    let code: String

    enum CodingKeys: String, CodingKey {
        case message = "Message"
        case data = "Data"
        case code = "Code"
    }
}

// MARK: - DataClass
struct UPIPayment: Codable {
}
