//
//  UPayIntReceipt.swift
//  OK
//
//  Created by Sam' MacBook  on 28/07/20.
//  Copyright © 2020 Sam. All rights reserved.
//

import UIKit

class UPayIntReceipt: OKBaseController, NearByPaymentFeedbackDelegate, NearByPaymentNoteDelegate {
   
    
    
    @IBOutlet var tvReciept: UITableView!
    @IBOutlet var lblReceipt: UILabel!
    @IBOutlet var btnShare: UIButton!
    @IBOutlet var btnHome: UIButton!
    @IBOutlet var bgView:UIView!
    var pdfview = UIView()
    var listTitleValue : [Dictionary<String,String>]?
    var yAxis : CGFloat = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        println_debug(listTitleValue)
        self.navigationItem.title = "Receipt"
        pdfview.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30)
        self.pdfview.backgroundColor = #colorLiteral(red: 0.9285323024, green: 0.9334669709, blue: 0.9376094937, alpha: 1)
        self.bgView.backgroundColor = .white
        
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "star1"), for: UIControl.State.normal)
        button.tintColor = .clear
        button.sizeToFit()
        button.addTarget(self, action: #selector(onClickRightAction), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
        self.createPDF()
        
        DispatchQueue.main.async {
            self.saveToGallery()
        }
    }
    @objc func onClickRightAction() {
         println_debug("enterNoteButtonAction")
             self.navigationController?.navigationBar.isUserInteractionEnabled = false
             guard  let vc = UIStoryboard.init(name: "NearMePaymentGeneric", bundle: nil).instantiateViewController(withIdentifier: "NearByPaymentNoteVC") as? NearByPaymentNoteVC else {return}
             vc.strStatusScreen = "Receipt"
             vc.delegate = self
             addChild(vc)
             vc.view.frame = self.view.bounds
             vc.didMove(toParent: self)
             let transition = CATransition()
             transition.duration = 0.5
             transition.type = CATransitionType.reveal
             vc.view.layer.add(transition, forKey: nil)
             view.addSubview(vc.view)
    }
    
    @IBAction func onClickShareActin(_: UIButton) {
        alertViewObj.wrapAlert(title:"", body: "Do you want to share Receipt?".localized, img: UIImage(named: "save_gray"))
        
        alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
        })
        
        alertViewObj.addAction(title: "Share".localized, style: .target , action: {
            self.shareQRWithDetail()
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
     
    }
    
    @IBAction func onClickHomeAction(_ sender: UIButton) {
             self.navigationController?.navigationBar.isUserInteractionEnabled = false
             guard  let vc = UIStoryboard.init(name: "NearMePaymentGeneric", bundle: nil).instantiateViewController(withIdentifier: "NearByPaymentFeedbackVC") as? NearByPaymentFeedbackVC else {return}
             vc.delegate = self
             addChild(vc)
             vc.view.frame = self.view.bounds
             vc.didMove(toParent: self)
             let transition = CATransition()
             transition.duration = 0.5
             transition.type = CATransitionType.reveal
             vc.view.layer.add(transition, forKey: nil)
             view.addSubview(vc.view)
    }
    
    func returnFeedback(feedback: String) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func shareQRWithDetail() {

        let image = captureScreen(view: pdfview)
        let imageToShare = [image]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        DispatchQueue.main.async {
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    func getDate() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "EEE, dd-MMM-yyyy"
        return formatter.string(from: date)
    }
    
    func getTime() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "HH:mm:ss"
        return formatter.string(from: date)
    }
    
}



extension UPayIntReceipt: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }else if section == 2 {
            return 1
        }else {
            return (listTitleValue?.count ?? 0) - 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UPIRecieptHeader", for: indexPath) as! UPIRecieptHeader
                cell.lblPaymentSuccess.text = "Payment Successful".localized
                cell.lblAmount.text = "905,000,00"
                cell.lblDate.text = self.getDate()
                cell.lblCountry.text = "Myanmar"
                cell.lblTime.text = self.getTime()
                cell.selectionStyle = .none
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UPIPayementTitle", for: indexPath) as! UPIPayementTitle
                cell.lblUPI.text = "Union Pay International Payment"
                cell.selectionStyle = .none
                return cell
            }
        }else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UPIRemarkFooter", for: indexPath) as! UPIRemarkFooter
            let dic = self.listTitleValue?[(listTitleValue?.count ?? 0) - 1]
            cell.btnOKBalance.setTitle("OK$ Balance: \(dic?["value"] ?? "") MMK", for: .normal)
            cell.selectionStyle = .none
            return cell
        }else {
            if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UPIMerchant", for: indexPath) as! UPIMerchant
                let dic = self.listTitleValue?[indexPath.row]
                cell.lblTitle.text = dic?["title"]
                cell.lblValue.text = dic?["value"]
                cell.selectionStyle = .none
                return cell
            }else if indexPath.row == (listTitleValue?.count ?? 0) - 2 {
                // Remark
                let cell = tableView.dequeueReusableCell(withIdentifier: "UPIRemark", for: indexPath) as! UPIRemark
                let dic = self.listTitleValue?[indexPath.row]
                cell.lblTitle.text = dic?["title"]
                cell.lblValue.text = dic?["value"]
                cell.selectionStyle = .none
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UPITitleValue", for: indexPath) as! UPITitleValue
                let dic = self.listTitleValue?[indexPath.row]
                cell.lblTitle.text = dic?["title"]
                cell.lblValue.text = dic?["value"]
                cell.selectionStyle = .none
                return cell
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                return 120
            }else {
                return 60
            }
        }else if indexPath.section == 2 {
            return 50
        }else {
            return UITableView.automaticDimension
        }
    }
    
}

class UPIRecieptHeader: UITableViewCell {
    @IBOutlet var lblPaymentSuccess: UILabel!
    @IBOutlet var lblAmount: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblCountry: UILabel!
    @IBOutlet var lblSep: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblPaymentSuccess.font = UIFont(name: appFont, size: 18)
        lblAmount.font = UIFont(name: appFont, size: 16)
        lblDate.font = UIFont(name: appFont, size: 12)
        lblTime.font = UIFont(name: appFont, size: 12)
        lblCountry.font = UIFont(name: appFont, size: 14)
    }
    
}

class UPIPayementTitle: UITableViewCell {
    @IBOutlet var lblUPI: UILabel!
    @IBOutlet var lblSep: UILabel!
    @IBOutlet var imgIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblUPI.font = UIFont(name: appFont, size: 16)
        imgIcon.image = UIImage(named: "upi")
    }
}

class UPITitleValue: UITableViewCell {
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblValue: UILabel!
    @IBOutlet var lblSep: UILabel!
    @IBOutlet var lblColon: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblTitle.font = UIFont(name: appFont, size: 15)
        lblValue.font = UIFont(name: appFont, size: 15)
        lblColon.font = UIFont(name: appFont, size: 15)
    }
}

class UPIMerchant: UITableViewCell {
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblValue: UILabel!
    @IBOutlet var lblSep: UILabel!
    @IBOutlet var lblColon: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblTitle.font = UIFont(name: appFont, size: 15)
        lblValue.font = UIFont(name: appFont, size: 15)
        lblColon.font = UIFont(name: appFont, size: 15)
    }
}

class UPIRemark: UITableViewCell {
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblValue: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblTitle.font = UIFont(name: appFont, size: 15)
        lblValue.font = UIFont(name: appFont, size: 15)
    }
}

class UPIRemarkFooter: UITableViewCell {
    @IBOutlet var btnOKBalance: UIButton!
    @IBOutlet var lblSep: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        btnOKBalance.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
    }
}
