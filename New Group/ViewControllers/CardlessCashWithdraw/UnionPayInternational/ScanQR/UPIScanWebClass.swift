//
//  UPIScanWebClass.swift
//  OK
//
//  Created by Sam' MacBook on 13/08/20.
//  Copyright © 2020 Sam. All rights reserved.
//

import UIKit

extension UPayIntScan: WebServiceResponseDelegate {
    
    func generateQRCode(token: String) {
        if appDelegate.checkNetworkAvail() {
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
            let web = WebApiClass()
            web.delegate = self
            let urlString = String.init(format: Url.upayIntGenerateQR)
            let urlStr = getUrl(urlStr: urlString, serverType: .UPayInt)
            println_debug(urlStr)
            let dic = ["MobileNo": UserModel.shared.mobileNo,"Token": token, "Password": ok_password ?? ""]
            println_debug("BankTransfer : \(dic)")
            web.genericClass(url: urlStr, param: dic as AnyObject, httpMethod: "POST", mScreen: "generateQR")
        }else {
            println_debug("No Internet")
        }
    }
    
     
    
    func enrollmentUPI() {
        if appDelegate.checkNetworkAvail() {
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
            let web = WebApiClass()
            web.delegate = self
            let urlString = String.init(format: Url.upayIntEnrollment)
            let urlStr = getUrl(urlStr: urlString, serverType: .UPayInt)
            println_debug(urlStr)
            let data = wrapEnrollParams()
            let dic = Dictionary<String,Any>()

            
            
            web.genericClass(url: urlStr, param: dic as AnyObject, httpMethod: "POST", mScreen: "enrollment",hbData: data)
        }else {
            println_debug("No Internet")
        }
    }
    
    func getQRInfo(qrString: String) {
        if appDelegate.checkNetworkAvail() {
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
            let web = WebApiClass()
            web.delegate = self
            let urlString = String.init(format: Url.upayIntQRInfo)
            let urlStr = getUrl(urlStr: urlString, serverType: .UPayInt)
            println_debug(urlStr)
            let data = wrapGetQRInFoParams(token: modelEnrollment?.data.token ?? "", qrStr: qrString)
            let dic = Dictionary<String,Any>()
            web.genericClass(url: urlStr, param: dic as AnyObject, httpMethod: "POST", mScreen: "getQRInfo",hbData: data)
        }else {
            println_debug("No Internet")
        }
    }
    
    
    
    func getExchageRate() {
        if appDelegate.checkNetworkAvail() {
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
            let web = WebApiClass()
            web.delegate = self
            let urlString = String.init(format: Url.upayIntExchangeRate)
            let urlStr = getUrl(urlStr: urlString, serverType: .UPayInt)
            println_debug(urlStr)
            //exchange rate will be 104 constant
            let data = wrapExchangeParams(origAmt: modelQRInfo?.data.trxInfo.trxAmt ?? "", exchange: "104", original: modelQRInfo?.data.trxInfo.trxCurrency ?? "")
            let dic = Dictionary<String,Any>()
            web.genericClass(url: urlStr, param: dic as AnyObject, httpMethod: "POST", mScreen: "exchangeRate",hbData: data)
        }else {
            println_debug("No Internet")
        }
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        DispatchQueue.main.async {
            progressViewObj.removeProgressView()
        }
        if screen == "generateQR" {
            guard let upayData = json as? Data else {
                println_debug("Data error")
                return
            }
            do {
                if let model = try? JSONDecoder().decode(UPIGenerateQR.self, from: upayData) {
                        
                    if model.code == "200" {
                        modelGenerateQR = model
                        DispatchQueue.main.async {
                            let cardlessCashStory = UIStoryboard(name: "CardLessCashStoryBoard", bundle: nil)
                            guard let generateQR = cardlessCashStory.instantiateViewController(withIdentifier: "UPayIntQRCode") as? UPayIntQRCode else { return }
                            generateQR.model = model
                            self.navigationController?.pushViewController(generateQR, animated: false)
                        }
                    }else {
                        println_debug("failed")
                    }
                }
            }
        }else if screen == "enrollment" {
            guard let enrollData = json as? Data else {
                return
            }
            do {
                if let model = try? JSONDecoder().decode(UPIEnrollmentModel.self, from: enrollData) {
                    modelEnrollment = model
                    if modelEnrollment?.code == "200" || modelEnrollment?.code == "201"{
                        println_debug("enrollment API Success")
                    }else {
                        //need to change the alert image
                        DispatchQueue.main.async {
                            alertViewObj.wrapAlert(title: "", body: self.modelEnrollment?.message ?? ""  , img: #imageLiteral(resourceName: "NearMeNew"))
                                alertViewObj.addAction(title: "YES".localized, style: .target , action: {
                                    self.navigationController?.popViewController(animated: true)
                                })
//                                alertViewObj.addAction(title: "NO".localized, style: .target , action: {
//                                    
//                                })
                                alertViewObj.showAlert(controller: self)
                        }
                        println_debug("enrollment API Failed")
                    }
                }else {
                    println_debug("enrollment api mode failed")
                }
                
            }
        }else if screen == "getQRInfo" {
            guard  let infoData = json as? Data else {
                return
            }
            do {
                if let model = try? JSONDecoder().decode(UPIQRCodeInfoModel.self, from: infoData) {
                    modelQRInfo = model
                    if modelQRInfo?.code == "200" {
                        self.getExchageRate()
                    }else {
                        println_debug("getQRInfo")
                    }
                }
            }
        }else if screen == "exchangeRate" {
            guard  let infoData = json as? Data else {
                return
            }
            do {
                if let model = try? JSONDecoder().decode(UPIExchangeRateModel.self, from: infoData) {
                    modelExchange = model
                    if modelExchange?.code == "200" {
                        println_debug("getQRInfo")
                        DispatchQueue.main.async {
                            let cardlessCashStory = UIStoryboard(name: "CardLessCashStoryBoard", bundle: nil)
                            guard let vc = cardlessCashStory.instantiateViewController(withIdentifier: "UPayIntScandInfo") as? UPayIntScandInfo else { return }
                            vc.infoDic = ["merchant_name": self.modelQRInfo?.data.trxInfo.merchantInfo.merchantName ?? "", "merchant_id" : self.modelQRInfo?.data.trxInfo.merchantInfo.mid ?? "","bill_currency": self.modelQRInfo?.data.trxInfo.trxCurrencyName ?? "","bill_Amount": self.modelQRInfo?.data.trxInfo.trxAmt ?? ""
                                          ,"exchange_rate": self.modelExchange?.data.exchangeRate ?? "","converted_amount": self.modelExchange?.data.exchangeAmt ?? "","remark": ""]
                            vc.qrInfo = self.modelQRInfo
                            self.navigationController?.pushViewController(vc, animated: false)
                            self.rightViewBG.isHidden = true
                        }
                           
                    }else {
                        println_debug("getQRInfo")
                    }
                }
            }
        }
        
    }
    
}
