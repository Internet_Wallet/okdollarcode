//
//  UPayIntScan.swift
//  OK
//
//  Created by Sam' MacBook  on 28/07/20.
//  Copyright © 2020 Sam. All rights reserved.
//

import UIKit
import AVFoundation

class UPayIntScan:  OKBaseController, AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet weak var lblScanDescption: UILabel!
    @IBOutlet weak var btnFlash: UIButton!
    @IBOutlet weak var btnGallery: UIButton!
    @IBOutlet weak var imgScanLayer: UIImageView!
    @IBOutlet weak var containerViews: UIView!
    
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnShowQR: UIButton!
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    @IBOutlet weak var rightViewBG: UIView!
    @IBOutlet weak var tbOption: UITableView!
    let listOption = ["History".localized,"help".localized,"Refund","Offers / Promotion".localized, "Merchant List".localized]
    
    let codeFrame:UIView = {
        let codeFrame = UIView()
        codeFrame.layer.borderColor =  UIColor.init(hex: "F3C632").cgColor
        codeFrame.layer.borderWidth = 2.5
        codeFrame.frame = CGRect.zero
        codeFrame.translatesAutoresizingMaskIntoConstraints = false
        return codeFrame
        
    }()
    
    var modelEnrollment : UPIEnrollmentModel?
    var modelQRInfo: UPIQRCodeInfoModel?
    var modelExchange: UPIExchangeRateModel?
    var modelGenerateQR : UPIGenerateQR?
    override func viewDidLoad() {
        super.viewDidLoad()
        rightViewBG.isHidden = true
        self.configureBackButton(withTitle: "QR Code")
        lblScanDescption.text = "Scan Union Pay \n QR Code"
        btnFlash.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        btnFlash.layer.borderWidth = 1.0
        btnFlash.layer.masksToBounds = true
        btnFlash.layer.cornerRadius = btnFlash.frame.width / 2
        
        btnGallery.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        btnGallery.layer.borderWidth = 1.0
        btnGallery.layer.masksToBounds = true
        btnGallery.layer.cornerRadius = btnGallery.frame.width / 2
        
        imgScanLayer.image = UIImage(named: "scanCover")
        self.configureRightButton(withTitle: "", imgName: "menu_white_payto")
        self.enrollmentUPI()
    }
    
    
    func configureRightButton(withTitle title:String,imgName: String) {
          let button = UIButton(type: .system)
          button.setImage(UIImage(named: imgName), for: UIControl.State.normal)
          button.tintColor = UIColor.white
          button.sizeToFit()
          button.addTarget(self, action: #selector(onClickRightAction), for: .touchUpInside)
          self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
          self.navigationItem.title = title
          let imageView = UIImageView(image: UIImage(named: "upi3"))
          self.navigationItem.titleView = imageView
      }
    
    @objc func onClickRightAction() {
        rightViewBG.isHidden = !rightViewBG.isHidden
        if !rightViewBG.isHidden {
            self.view.bringSubviewToFront(rightViewBG)
        }
    }
    
    @IBAction func onClickOptionHideAction(_: UIButton) {
        if !rightViewBG.isHidden {
            rightViewBG.isHidden  = true
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if AVCaptureDevice.authorizationStatus(for: .video) == AVAuthorizationStatus.authorized {
            self.loadCamera()
            //  self.flashImage.isHighlighted = false
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted) in
                switch granted {
                case true:
                    self.loadCamera()
                case false:
                    self.requestAlertForPermissions(withBody: "OK$ need camera access to scan".localized , handler: { (cancelled) in
                        
                    })
                }
            })
        }
        
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    private func loadCamera() {
        //  DispatchQueue.global(qos: .background).async {
        let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        if let captureDevice = captureDevice {
            do {
                self.captureSession = AVCaptureSession()
                // CaptureSession needs an input to capture Data from
                let input = try AVCaptureDeviceInput(device: captureDevice)
                self.captureSession?.addInput(input)
                // CaptureSession needs and output to transfer Data to
                let captureMetadataOutput = AVCaptureMetadataOutput()
                self.captureSession?.addOutput(captureMetadataOutput)
                //We tell our Output the expected Meta-data type
                captureMetadataOutput.setMetadataObjectsDelegate(self , queue: DispatchQueue.main)
                captureMetadataOutput.metadataObjectTypes = [.code128, .qr,.ean13, .ean8, .code39, .upce, .aztec, .pdf417] //AVMetadataObject.ObjectType
                self.captureSession?.startRunning()
                //The videoPreviewLayer displays video in conjunction with the captureSession
                if let session = self.captureSession {
                    self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
                }
                self.videoPreviewLayer?.videoGravity = .resizeAspectFill
                DispatchQueue.main.async {
                    self.videoPreviewLayer?.frame = self.containerViews.frame
                    if let viewVideo = self.videoPreviewLayer {
                        self.view.layer.addSublayer(viewVideo)
                    }
                    self.view.bringSubviewToFront(self.containerViews)
                    //self.view.bringSubviewToFront(self.buttonContainerView)
                }
            }
            catch {
                println_debug("Error")
                DispatchQueue.main.async {
                    self.removeActivityIndicator(view: self.view)
                }
            }
        }
        DispatchQueue.main.async {
            self.removeActivityIndicator(view: self.view)
        }
        //  }
    }
    
    
    //MARK:- Delegate QR
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if metadataObjects.count == 0 {
            println_debug("no objects returned")
            return
        }
        
        let metaDataObject = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        guard let StringCodeValue = metaDataObject.stringValue else {
            return
        }
        view.addSubview(codeFrame)
        guard let metaDataCoordinates = videoPreviewLayer?.transformedMetadataObject(for: metaDataObject) else {
            return
        }
        codeFrame.frame = metaDataCoordinates.bounds
        let feedbackGenerator = UIImpactFeedbackGenerator(style: .heavy)
        feedbackGenerator.impactOccurred()
        
        if self.captureSession?.isRunning ?? false {
            self.captureSession?.stopRunning()
            }
        println_debug("Qr Scanned String : \(StringCodeValue)")
        scannedString(str: StringCodeValue)
    }
    
    @IBAction func onClickShowScanQRAction(_ sender: Any) {
        self.generateQRCode(token: modelEnrollment?.data.token ?? "")
    }
    
    @IBAction func onClickBackAction(_ sender: Any) {
        DispatchQueue.main.async {
        self.navigationController?.popViewController(animated: false)
        }
    }
}


extension UPayIntScan: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOption.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UPIOptionCell", for: indexPath) as! UPIOptionCell
        cell.lblOption.text = listOption[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            DispatchQueue.main.async {
                let cardlessCashStory = UIStoryboard(name: "CardLessCashStoryBoard", bundle: nil)
                guard let vc = cardlessCashStory.instantiateViewController(withIdentifier: "UPIHistory") as? UPIHistory else { return }
                self.navigationController?.pushViewController(vc, animated: false)
            }
            rightViewBG.isHidden = true
        }
    }
}


extension UPayIntScan {
    
    fileprivate func scannedString(str: String) {
        
        if(str.count > 0) {
            self.getQRInfo(qrString: str)
        }else {
            codeFrame.removeFromSuperview()
            captureSession?.startRunning()
            return
        }
        codeFrame.removeFromSuperview()
        captureSession?.stopRunning()
    }
    
 
}

class UPIOptionCell: UITableViewCell {

@IBOutlet weak var lblOption: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblOption.font = UIFont.init(name: appFont, size: 15)
    }
    
}
