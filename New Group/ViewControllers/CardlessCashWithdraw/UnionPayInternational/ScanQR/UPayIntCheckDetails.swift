//
//  UPayIntCheckDetails.swift
//  OK
//
//  Created by Sam' MacBook  on 27/07/20.
//  Copyright © 2020 Sam. All rights reserved.
//

import UIKit

class UPayIntCheckDetails: OKBaseController {
    
    @IBOutlet weak var tvDetails: UITableView!
    @IBOutlet weak var btnPAy: UIButton!
    var titleList = ["upi", "Merchant", "Merchant ID","Bill Currancy","Bill Amount", "Union Pay Discount", "Net Amount", "Exchange Rate", "Converted Amount", "Remarks :"]
    var finalDic: Dictionary<String,String>?
    var valueList = ["Union Pay International Payment","Sam Pharma","+959979275277","SGD","100","100","100","1 SGD = 1050 MMK","1000000", "Remark"]
    
    var qrInfo: UPIQRCodeInfoModel?
    var paymentModel : UPIPaymentModel?
    var values : [String]?
    override func viewDidLoad() {
        super.viewDidLoad()
        tvDetails.tableFooterView = UIView()
        println_debug(values)
        valueList[1] = values?[0] ?? ""
        valueList[2] = values?[1] ?? ""
        valueList[3] = values?[2] ?? ""
        valueList[4] = values?[3] ?? ""
        valueList[5] = values?[4] ?? ""
        valueList[6] = values?[5] ?? ""
        valueList[7] = values?[6] ?? ""
        valueList[8] = values?[7] ?? ""
        valueList[9] = values?[8] ?? ""
        if valueList[9] == "" {
        titleList.removeLast()
        }
        
     
        tvDetails.tableFooterView = UIView()
        tvDetails.reloadData()
        self.configureBackButton(withTitle: "Check Details")
    }
    
    @IBAction func onClickPayAction(_: UIButton) {
                if UserLogin.shared.loginSessionExpired {
                    OKPayment.main.authenticate(screenName: "UPIPayment", delegate: self)
                }else {
                    self.payment()
                }
    }
}

extension UPayIntCheckDetails: WebServiceResponseDelegate,BioMetricLoginDelegate {
    func payment() {
        if appDelegate.checkNetworkAvail() {
            let web = WebApiClass()
            web.delegate = self
            let urlString = String.init(format: Url.upayIntPayment)
            let urlStr = getUrl(urlStr: urlString, serverType: .UPayInt)
            println_debug(urlStr)
            let data = wrapPaymentParams(mobileNo: UserModel.shared.mobileNo, token: qrInfo?.data.trxInfo.token ?? "", deviceID: uuid, qrStr: qrInfo?.data.trxInfo.mpqrcPayload ?? "", amount: valueList[8])
            let dic = Dictionary<String,Any>()
            web.genericClass(url: urlStr, param: dic as AnyObject, httpMethod: "POST", mScreen: "payment",hbData: data)
        }else {
            println_debug("No Internet")
        }
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
     DispatchQueue.main.async {
        progressViewObj.removeProgressView()
    }
        if screen == "payment" {
            guard  let paymentData = json as? Data else {
            
                return
            }
            do {
                if let model = try? JSONDecoder().decode(UPIPaymentModel.self, from: paymentData) {
                    println_debug(model.message)
                    println_debug(model.code)
                    println_debug(model.data)
                    if model.code == "200" {
                        println_debug("Payment success")
                        self.paymentModel = model
                        DispatchQueue.main.async {
                            let cardlessCashStory = UIStoryboard(name: "CardLessCashStoryBoard", bundle: nil)
                            guard let vc = cardlessCashStory.instantiateViewController(withIdentifier: "UPayIntReceipt") as? UPayIntReceipt else { return }
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }else {
                    DispatchQueue.main.async {
                                let cardlessCashStory = UIStoryboard(name: "CardLessCashStoryBoard", bundle: nil)
                                guard let vc = cardlessCashStory.instantiateViewController(withIdentifier: "UPayIntReceipt") as? UPayIntReceipt else { return }
                                let bill = self.values?[3] ?? ""
                                let amount = self.values?[7] ?? ""
                                var dicount = "0"
                                if self.values?[4] ?? "" != "" {
                                    dicount = self.values?[4] ?? ""
                                
                                vc.listTitleValue = [
                                    ["title" : "Paid By OK$" ,"value" : "\(UserModel.shared.name) \(UserModel.shared.phoneNumber)"],
                                    ["title" : "Merchant" ,"value" : self.values?[0] ?? ""],
                                    ["title" : "Merchant ID" ,"value" : self.values?[1] ?? ""],
                                    ["title" : "UPI Txn ID" ,"value" :  "242342408"], //self.values?[2] ??
                                    ["title" : "OK$ Ten ID" ,"value" : "6549393"], // self.values?[3] ??
                                    ["title" : "Bill Currancy" ,"value" : self.values?[2] ?? ""],
                                    ["title" : "Bill Amount" ,"value" : "\(bill) \(self.values?[2] ?? "")"],
                                    ["title" : "Union Pay Discount" ,"value" : "\(dicount) \(self.values?[2] ?? "")"], //self.values?[6] ?? ""
                                    ["title" : "Net Amount","value" : "\(self.values?[5] ?? "") \(self.values?[2] ?? "")"],
                                    ["title" : "Exchange Rate" ,"value" : "1 \(self.values?[2] ?? "") = \(self.values?[6] ?? "")"],
                                    ["title" : "Convrted Amount" ,"value" : "\(amount) MMK"],
                                    ["title" : "Remark" ,"value" : self.values?[8] ?? ""],
                                    ["title" : "OkBalance" ,"value" : "\(self.walletAmount())"]
                                ]
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
          if isSuccessful {
              if screen == "UPIPayment" {
                    self.payment()
              }
        }
      }
    
}


extension UPayIntCheckDetails: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpayIntHeaderCell", for: indexPath) as! UpayIntHeaderCell
            cell.selectionStyle = .none
            cell.imgIcon.image = UIImage(named: titleList[indexPath.row])
            cell.lblTitle.text = valueList[indexPath.row]
            return cell
        }else if indexPath.row == 9 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpayIntRemarkCell", for: indexPath) as! UpayIntRemarkCell
            cell.selectionStyle = .none
            cell.lblTitle.text = titleList[indexPath.row]
            cell.lblValue.text = valueList[indexPath.row]
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpayIntValueCell", for: indexPath) as! UpayIntValueCell
            cell.selectionStyle = .none
            cell.lblTitle.text = titleList[indexPath.row]
            if indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 6 {
                if indexPath.row == 5 && (valueList[indexPath.row] == "") {
                    cell.lblValue.text = "0 \(valueList[3])"
                }else {
                    cell.lblValue.text = "\(valueList[indexPath.row]) \(valueList[3])"
                }
            }else if indexPath.row == 7 {
                cell.lblValue.text =  " 1 \(valueList[3]) = \(valueList[7]) MMK"
            }else if indexPath.row == 8 {
                cell.lblValue.text = valueList[indexPath.row] + " MMK"
            }else {
                cell.lblValue.text = valueList[indexPath.row]
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 80
        }else if indexPath.row == 9 {
            return UITableView.automaticDimension
        }else {
            return 70
        }
    }
    
}


class UpayIntHeaderCell : UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}


class UpayIntValueCell : UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

class UpayIntRemarkCell : UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}


