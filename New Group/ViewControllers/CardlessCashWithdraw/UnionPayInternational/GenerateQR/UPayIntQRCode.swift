//
//  UPayIntQRCode.swift
//  OK
//
//  Created by Sam' MacBook  on 29/07/20.
//  Copyright © 2020 Sam. All rights reserved.
//

import UIKit

class UPayIntQRCode: OKBaseController {
    @IBOutlet weak var rightViewBG: UIView!
    @IBOutlet weak var tbOption: UITableView!
    @IBOutlet weak var tbQR: UITableView!
    
    var model : UPIGenerateQR?
    
    let listOption = ["History".localized,"help".localized,"Refund","Offers / Promotion".localized, "Merchant List".localized]
    var firstView : ViewSelection = .showView
    var timerQR: Timer?
    var totalTime = 120
    override func viewDidLoad() {
        super.viewDidLoad()
        rightViewBG.isHidden = true
        
        self.configureBackButton(withTitle: "QR Code")
        
        self.showViewOnScreen(view: .showView)
        self.configureRightButton(withTitle: "Generate QR", imgName: "menu_white_payto")
        
        tbQR.reloadData()
        if let strQr = model?.data.emvCpqrcPayload {
            if let img = self.getQRImageHaldler(stringQR: strQr, withSize: 10) {
                if let cell = tbQR.cellForRow(at: IndexPath(row: 0, section: 0)) as? UPIGenerateQRCell {
                    cell.imgBarCode.isHidden = true
                    cell.imgQR.image = img
                    cell.lblTimer.text = "02:00"
                    self.startOtpTimer()
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    private func configureRightButton(withTitle title:String,imgName: String) {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: imgName), for: UIControl.State.normal)
        button.tintColor = UIColor.white
        button.sizeToFit()
        button.addTarget(self, action: #selector(onClickRightAction), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.title = title
    }
    
    @objc func onClickRightAction() {
        rightViewBG.isHidden = !rightViewBG.isHidden
    }
    
    @IBAction func onClickOptionHideAction(_: UIButton) {
        if !rightViewBG.isHidden {
            rightViewBG.isHidden  = true
        }
    }
    
    @IBAction func onClickScanAction(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    @IBAction func onClickBackAction(_ sender: UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: AddWithdrawMoneyVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    @objc func onClickChangeAction() {
        if let cell = tbQR.cellForRow(at: IndexPath(row: 0, section: 0)) as? UPIGenerateQRCell {
            if firstView == .showView {
                firstView = .showChina
                self.showViewOnScreen(view: .showChina)
                if let strBar = model?.data.barcodeCpqrcPayload {
                    cell.imgQR.isHidden = true
                    cell.imgBarCode.isHidden = false
                    cell.imgBarCode.image =  self.generateBarcode(from: strBar)
                }
            }else {
                firstView = .showView
                self.showViewOnScreen(view: .showView)
                if let strQr = model?.data.emvCpqrcPayload {
                    if let img = self.getQRImageHaldler(stringQR: strQr, withSize: 10) {
                        cell.imgQR.isHidden = false
                        cell.imgBarCode.isHidden = true
                        cell.imgQR.image = img
                    }
                }
            }
        }
    }
    
    @objc func onClickInfoAction() {
        
    }
    
    private func showViewOnScreen(view : ViewSelection) {
        if let cell = tbQR.cellForRow(at: IndexPath(row: 0, section: 0)) as? UPIGenerateQRCell {
            switch view {
            case .showView:
                cell.imgDesription.image = UIImage(named: "globeUPI")
                cell.lblShowDescption.text = "My Global QR Code"
                cell.imgQRIcon.image = UIImage(named: "barcode")
                cell.lblTitleBarType.text = "My China Bar Code"
                cell.imgMybarFlag.isHidden = false
                cell.imgGlobe.isHidden = true
                break
            case .showChina:
                cell.imgDesription.image = UIImage(named: "chinaF")
                cell.lblShowDescption.text = "My China Bar Code"
                cell.imgQRIcon.image = UIImage(named: "qrcodeUPI")
                cell.lblTitleBarType.text = "My Global QR Code"
                cell.imgMybarFlag.isHidden = true
                cell.imgGlobe.isHidden = false
                break
            }
        }
    }
    
    func startOtpTimer() {
        if let cell = tbQR.cellForRow(at: IndexPath(row: 0, section: 0)) as? UPIGenerateQRCell {
            cell.lblTimer.isHidden = false
            self.totalTime = 119
            self.timerQR = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        }
    }
    
    @objc func updateTimer() {
        if let cell = tbQR.cellForRow(at: IndexPath(row: 0, section: 0)) as? UPIGenerateQRCell {
            cell.lblTimer.text = self.timeFormatted(self.totalTime)
            if totalTime != 0 {
                totalTime -= 1
            }else {
                if let timer = self.timerQR {
                    timer.invalidate()
                    println_debug("Time over")
                    //self.dismiss(animated: true, completion: nil)
                }
            }
            
        }
    }
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%01d:%02d", minutes, seconds)
    }
    
}


extension UPayIntQRCode: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 5 {
            return listOption.count
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UPIOptionCell", for: indexPath) as! UPIOptionCell
            cell.lblOption.text = listOption[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UPIGenerateQRCell", for: indexPath) as! UPIGenerateQRCell
            cell.btnChangeAction.addTarget(self, action: #selector(onClickChangeAction), for: .touchUpInside)
            cell.btnInfo.addTarget(self, action: #selector(onClickInfoAction), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 5 {
            return  40
        }else {
            return  600
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 5 {
            if indexPath.row == 0 {
                DispatchQueue.main.async {
                    let cardlessCashStory = UIStoryboard(name: "CardLessCashStoryBoard", bundle: nil)
                    guard let vc = cardlessCashStory.instantiateViewController(withIdentifier: "UPIHistory") as? UPIHistory else { return }
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                rightViewBG.isHidden = true
            }
        }
    }
}

extension UPayIntQRCode {
    
    fileprivate func getQRImageHaldler(stringQR: String, withSize rate: CGFloat) -> UIImage? {
        if let filter = CIFilter.init(name: "CIQRCodeGenerator") {
            filter.setDefaults()
            if let data : Data = stringQR.data(using: String.Encoding.utf8) {
                filter.setValue(data, forKey: "inputMessage")
                if let resultImage : CIImage = filter.outputImage {
                    let transform = CGAffineTransform(scaleX: 12, y: 12)
                    let translatedImage = resultImage.transformed(by: transform)
                    //                      guard let logo = UIImage(named: "okEmbedInQR"), let logoInCGImage = logo.cgImage else {
                    //                          return nil
                    //                      }
                    //                      guard let qrWithLogoCI = translatedImage.combined(with:  CIImage(cgImage: logoInCGImage)) else {
                    //                          return nil
                    //                      }
                    let context = CIContext.init(options: nil)
                    guard let qrWithLogoCG = context.createCGImage(translatedImage, from: translatedImage.extent) else {
                        return nil
                    }
                    var image   = UIImage.init(cgImage: qrWithLogoCG, scale: 1.0, orientation: UIImage.Orientation.up)
                    let width  =  image.size.width * rate
                    let height =  image.size.height * rate
                    UIGraphicsBeginImageContext(.init(width: width, height: height))
                    let cgContext = UIGraphicsGetCurrentContext()
                    cgContext?.interpolationQuality = .none
                    image.draw(in: .init(origin: .init(x: 0, y: 0), size: .init(width: width, height: height)))
                    guard let imageFromCurrentImageContext = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
                    image = imageFromCurrentImageContext
                    UIGraphicsEndImageContext()
                    return image
                }
            }
        }
        return nil
    }
    
    func generateBarcode(from string: String) -> UIImage? {
        
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CICode128BarcodeGenerator") {
            filter.setDefaults()
            //Margin
            filter.setValue(7.00, forKey: "inputQuietSpace")
            filter.setValue(data, forKey: "inputMessage")
            //Scaling
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                let context:CIContext = CIContext.init(options: nil)
                let cgImage:CGImage = context.createCGImage(output, from: output.extent)!
                let rawImage:UIImage = UIImage.init(cgImage: cgImage)
                
                let cgimage: CGImage = (rawImage.cgImage)!
                let cropZone = CGRect(x: 0, y: 0, width: Int(rawImage.size.width), height: Int(rawImage.size.height))
                let cWidth: size_t  = size_t(cropZone.size.width)
                let cHeight: size_t  = size_t(cropZone.size.height)
                let bitsPerComponent: size_t = cgimage.bitsPerComponent
                let bytesPerRow = (cgimage.bytesPerRow) / (cgimage.width  * cWidth)
                
                let context2: CGContext = CGContext(data: nil, width: cWidth, height: cHeight, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: CGColorSpaceCreateDeviceRGB(), bitmapInfo: cgimage.bitmapInfo.rawValue)!
                
                context2.draw(cgimage, in: cropZone)
                
                let result: CGImage  = context2.makeImage()!
                let finalImage = UIImage(cgImage: result)
                
                return finalImage
                
            }
        }
        
        return nil
    }
}



class UPIGenerateQRCell: UITableViewCell {
    @IBOutlet weak var lblShowDescption: UILabel!
    @IBOutlet var imgDesription: UIImageView!
    @IBOutlet var viewShowLayer: UIView!
    @IBOutlet var imgQR: UIImageView!
    @IBOutlet var imgBarCode: UIImageView!
    @IBOutlet var imgQRIcon: UIImageView!
    @IBOutlet var imgGlobe: UIImageView!
    @IBOutlet var viewMyBarCode: UIView!
    @IBOutlet var imgMybarFlag: UIImageView!
    @IBOutlet var lblTitleBarType: UILabel!
    @IBOutlet var btnInfo: UIButton!
    @IBOutlet var btnChangeAction: UIButton!
    @IBOutlet var lblTimeTitle: UILabel!
    @IBOutlet var lblTimer: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewShowLayer.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        viewShowLayer.layer.borderWidth = 1.0
        viewShowLayer.layer.masksToBounds = true
        
        viewMyBarCode.layer.cornerRadius = 5.0
        viewMyBarCode.layer.borderColor = #colorLiteral(red: 0.08835596591, green: 0.1538327336, blue: 0.6491383314, alpha: 1)
        viewMyBarCode.layer.borderWidth = 1.0
        viewMyBarCode.layer.masksToBounds = true
        
        
        lblTimer.layer.cornerRadius = 5.0
        lblTimer.layer.borderColor = #colorLiteral(red: 0.08835596591, green: 0.1538327336, blue: 0.6491383314, alpha: 1)
        lblTimer.layer.borderWidth = 2.0
        lblTimer.layer.masksToBounds = true
        imgMybarFlag.image = UIImage(named: "chinaF")
        imgGlobe.isHidden = true
    }
}


enum ViewSelection {
    case showView
    case showChina
    
    var bool: Bool {
        switch self {
        case .showView:
            return true
        case .showChina:
            return true
        }
    }
}





