//
//  UPIFilter.swift
//  OK
//
//  Created by Sam' MacBook on 29/08/20.
//  Copyright © 2020 Sam. All rights reserved.
//

import UIKit


protocol UPIFilterDelegate {
    func applySortingAndFiltering(isSorting: Bool, sortingValue: [String] , isFiltering: Bool, filterinValues: [Bool])
}

class UPIFilter: OKBaseController, UPICalendarDelegate {
    var delegate: UPIFilterDelegate?
    @IBOutlet var lblSortBy: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var bgDate: UIView!
    @IBOutlet var btnDate: UIButton!
    @IBOutlet var btnDateSort: UIButton!
    @IBOutlet var lblFilter: UILabel!
    @IBOutlet var lblMark: UILabel!
    
    @IBOutlet var bgStar: UIView!
    @IBOutlet var bgNote: UIView!
    @IBOutlet var bgRefund: UIView!
    @IBOutlet var lblNote: UILabel!
    @IBOutlet var lblRefund: UILabel!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var stackButton: UIStackView!
    var filterValues = [false,false,false]
    var sortingValues = ["",""]
    var isSortingOn = false
    var isFiltingOn = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bgDate.layer.cornerRadius = 5.0
        bgDate.layer.masksToBounds = true
        bgDate.layer.borderWidth = 1.5
        bgDate.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        bgStar.layer.cornerRadius = 5.0
        bgStar.layer.masksToBounds = true
        bgStar.layer.borderWidth = 1.5
        bgDate.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        bgNote.layer.cornerRadius = 5.0
        bgNote.layer.masksToBounds = true
        bgNote.layer.borderWidth = 1.5
        bgNote.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        bgRefund.layer.cornerRadius = 5.0
        bgRefund.layer.masksToBounds = true
        bgRefund.layer.borderWidth = 1.5
        bgRefund.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        nagivationTitle(withTitle: "Sort & Filter")
        
        btnBack.isHidden = false
        stackButton.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.07430977374, green: 0.2444769144, blue: 0.7130269408, alpha: 1)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.9961667657, green: 0.7674615979, blue: 0, alpha: 1)
    }
    
    func nagivationTitle(withTitle title:String) {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "tabBarBack"), for: UIControl.State.normal)
        button.tintColor = UIColor.white
        button.sizeToFit()
        button.addTarget(self, action: #selector(backButtonCustomAction), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.title = title
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.07430977374, green: 0.2444769144, blue: 0.7130269408, alpha: 1)
        
    }
    
    
    
    @IBAction func onClickDateSortAction(_ sender: UIButton) {
        let sb = UIStoryboard(name: "CardLessCashStoryBoard" , bundle: nil)
        let calendarView  = sb.instantiateViewController(withIdentifier: "UPICalendar") as! UPICalendar
        if let window = UIApplication.shared.keyWindow {
            window.rootViewController?.addChild(calendarView)
            calendarView.view.frame = .init(x: 0, y: 0, width: window.frame.width , height: window.frame.height)
            calendarView.delegate = self
            window.addSubview(calendarView.view)
            window.makeKeyAndVisible()
        }
    }
    
    func selectedDateForSorting(fromDate: String, toDate: String) {
        println_debug("fromDate: \(fromDate)       toDate: \(toDate)")
        btnDate.setTitle("\(fromDate)  T0  \(toDate)", for: .normal)
        bgDate.layer.borderColor = #colorLiteral(red: 0.07623932511, green: 0.4076983333, blue: 1, alpha: 1)
        btnDate.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        sortingValues[0] = fromDate
        sortingValues[1] = toDate
        btnBack.isHidden = true
        stackButton.isHidden = false
        isSortingOn = true
        
        self.resetFilter()
    }
    
    @IBAction func onClickStarAction(_ sender: Any) {
        filterValues[0] = !filterValues[0]
        
        if filterValues[0] {
            bgStar.layer.borderColor = #colorLiteral(red: 0.07623932511, green: 0.4076983333, blue: 1, alpha: 1)
        }else {
            bgStar.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        self.showResultResetButton()
        self.resetSorting()
    }
    
    @IBAction func onClickNoteAction(_ sender: Any) {
        filterValues[1] = !filterValues[1]
        
        if filterValues[1] {
            bgNote.layer.borderColor = #colorLiteral(red: 0.07623932511, green: 0.4076983333, blue: 1, alpha: 1)
        }else {
            bgNote.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        self.showResultResetButton()
        self.resetSorting()
    }
    
    @IBAction func onClickRefundAction(_ sender: Any) {
        filterValues[2] = !filterValues[2]
        
        if filterValues[2] {
            bgRefund.layer.borderColor = #colorLiteral(red: 0.07623932511, green: 0.4076983333, blue: 1, alpha: 1)
        }else {
            bgRefund.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        self.showResultResetButton()
        self.resetSorting()
    }
    
    @IBAction func onClickBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickShowResultAction(_: UIButton) {
        
        if filterValues[0] || filterValues[1] || filterValues[2] {
            isFiltingOn = true
        }else{
            isFiltingOn = false
        }
        
        if let del = delegate {
            del.applySortingAndFiltering(isSorting: isSortingOn, sortingValue: sortingValues, isFiltering: isFiltingOn, filterinValues: filterValues)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickShowResetAction(_: UIButton) {
        btnBack.isHidden = false
        stackButton.isHidden = true
        self.resetSorting()
        self.resetFilter()
    }
    
    func showResultResetButton() {
        if filterValues[0] || filterValues[1] || filterValues[2] {
            btnBack.isHidden = true
            stackButton.isHidden = false
        }else{
            btnBack.isHidden = false
            stackButton.isHidden = true
        }
    }
    
    func resetSorting() {
        isSortingOn = false
        bgDate.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        sortingValues[0] = ""
        sortingValues[1] = ""
        btnDate.setTitleColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), for: .normal)
        btnDate.setTitle("Last Bill (Default)", for: .normal)
    }
    
    func resetFilter() {
        isFiltingOn = false
        bgStar.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        bgNote.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        bgRefund.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        filterValues[0] = false
        filterValues[1] = false
        filterValues[2] = false
    }
    
}
