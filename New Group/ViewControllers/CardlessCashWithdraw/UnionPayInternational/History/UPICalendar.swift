//
//  UPICalendar.swift
//  OK
//
//  Created by Sam' MacBook on 29/08/20.
//  Copyright © 2020 Sam. All rights reserved.
//

import UIKit

protocol UPICalendarDelegate {
    func selectedDateForSorting(fromDate: String,toDate: String)
}

class UPICalendar: OKBaseController {
    var delegate: UPICalendarDelegate?
    @IBOutlet var lblFromDate: UILabel!
    @IBOutlet var lblToDate: UILabel!
    @IBOutlet var dpFrom: UIDatePicker!
    @IBOutlet var dpTo: UIDatePicker!
    @IBOutlet var bgView:UIView!
    
    var dateFormator = DateFormatter()
    var fromDate = Date()
    var toDate = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dpFrom.addTarget(self, action: #selector(fromDateChanged(_:)), for: .valueChanged)
        dpTo.addTarget(self, action: #selector(toDateChanged(_:)), for: .valueChanged)
        bgView.layer.cornerRadius = 10.0
        bgView.layer.masksToBounds = true
        bgView.layer.borderWidth = 1.5
        bgView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        let minDate = Calendar.current.date(byAdding: .year, value: -1, to: Date())
        dpFrom.minimumDate = minDate
        dpTo.minimumDate = minDate
        
        dpFrom.maximumDate = Date()
        dpTo.maximumDate = Date()
        
        dpFrom.date = Date()
        dpTo.date = Date()
        if #available(iOS 13.4, *) {
            dpFrom.preferredDatePickerStyle = .wheels
            dpTo.preferredDatePickerStyle = .wheels
                   } else {
                       // Fallback on earlier versions
                   }
        dateFormator.dateFormat = "dd-MMM-yyyy"
        lblFromDate.text = "From Date: \(dateFormator.string(from: Date()))"
        lblToDate.text = "To Date: \(dateFormator.string(from: Date()))"
    }
    
    @objc func fromDateChanged(_ sender: UIDatePicker) {
        let components = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
        if let day = components.day, let month = components.month, let year = components.year {
            print("\(day) \(month) \(year)")
            self.fromDate = sender.date
            dpFrom.maximumDate = self.toDate
            dpTo.minimumDate = sender.date
            lblFromDate.text = "From Date: \(dateFormator.string(from: self.fromDate))"
        }
    }
    
    @objc func toDateChanged(_ sender: UIDatePicker) {
        let components = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
        if let day = components.day, let month = components.month, let year = components.year {
            print("\(day) \(month) \(year)")
            self.toDate = sender.date
            lblToDate.text = "To Date: \(dateFormator.string(from: self.toDate))"
        }
    }
    
    @IBAction func onClickSortAction(_ sender: UIButton) {
        if self.toDate >= self.fromDate {
            if let del = delegate {
                del.selectedDateForSorting(fromDate: "\(dateFormator.string(from: self.fromDate))", toDate: "\(dateFormator.string(from: self.toDate))")
            }
            self.view.removeFromSuperview()
        }else {
            alertViewObj.wrapAlert(title:"", body: "Please select valid date range" , img: #imageLiteral(resourceName: "alert-icon.png"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    @IBAction func onClickCancelAction(_ sender: UIButton) {
        self.view.removeFromSuperview()
    }
    
    
}

