//
//  UPIHistory.swift
//  OK
//
//  Created by Sam' MacBook on 28/08/20.
//  Copyright © 2020 Sam. All rights reserved.
//

import UIKit

class UPIHistory: OKBaseController {
    @IBOutlet weak var tbHistory: UITableView!
    @IBOutlet weak var lblTotalRecords: UILabel!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var btnClearSearch: UIButton!
    
    var data = [["daten:":"27-08-2020","dateAmount": "7966","business": "Pizza company","businessAmount": "685"],["daten:":"28-08-2020","dateAmount": "796","business":"KFC","businessAmount": "500"]]
    
    var isSearchOn = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewSearch.isHidden = true
        self.lblTotalRecords.isHidden = false
        self.configureBackButton(withTitle: "History")
        
        viewSearch.layer.cornerRadius = 5
        viewSearch.layer.masksToBounds = true
        viewSearch.layer.borderColor = #colorLiteral(red: 0.07080212981, green: 0.4077293575, blue: 1, alpha: 1)
        viewSearch.layer.borderWidth = 1.0
        tbHistory.tableFooterView = UIView()
        
        btnFilter.layer.cornerRadius = 5
        btnFilter.layer.masksToBounds = true
        btnFilter.layer.borderColor = #colorLiteral(red: 0.07080212981, green: 0.4077293575, blue: 1, alpha: 1)
        btnFilter.layer.borderWidth = 1.0
        
        btnSearch.layer.cornerRadius = 5
        btnSearch.layer.masksToBounds = true
        btnSearch.layer.borderColor = #colorLiteral(red: 0.07080212981, green: 0.4077293575, blue: 1, alpha: 1)
        btnSearch.layer.borderWidth = 1.0
    }

    @IBAction func onClickSearchAction(_ sender: UIButton) {
        isSearchOn = !isSearchOn
        if isSearchOn {
            self.viewSearch.isHidden = false
            self.lblTotalRecords.isHidden = true
            self.btnSearch.setTitle("Cancel", for: .normal)
            self.btnSearch.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.btnSearch.setImage(UIImage(named: ""), for: .normal)
        }else {
            self.viewSearch.isHidden = true
            self.lblTotalRecords.isHidden = false
            self.btnSearch.setTitle("", for: .normal)
            self.btnSearch.setImage(UIImage(named: "search_blue"), for: .normal)
            self.btnSearch.layer.borderColor = #colorLiteral(red: 0.07080212981, green: 0.4077293575, blue: 1, alpha: 1)
        }
    }
    
    @IBAction func onClickTextFieldClearAction(_: UIButton) {
        tfSearch.text = ""
    }
    @IBAction func onClickFilterAction(_ sender: UIButton) {
        DispatchQueue.main.async {
            let cardlessCashStory = UIStoryboard(name: "CardLessCashStoryBoard", bundle: nil)
            guard let vc = cardlessCashStory.instantiateViewController(withIdentifier: "UPIFilter") as? UPIFilter else { return }
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension UPIHistory: UPIFilterDelegate {
    func applySortingAndFiltering(isSorting: Bool, sortingValue: [String], isFiltering: Bool, filterinValues: [Bool]) {
        println_debug("Sorting On : \(isSorting) sortingValue:\(sortingValue) Filtering On: \(isFiltering) filteinng Values: \(filterinValues)")
    }
}

extension UPIHistory: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UPIHistoryCell", for: indexPath) as! UPIHistoryCell
        let dic = data[indexPath.row]
        cell.lblDate.text = "28-08-2020"//dic["daten"]
        cell.lblDateAmount.text = dic["dateAmount"]
        cell.lblBusiness.text = dic["business"]
        cell.lblBusinessAmount.text = dic["businessAmount"]
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return createRequiredFieldLabel(headerTitle: "28-8-2020")
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    private func createRequiredFieldLabel(headerTitle:String) ->UIView? {
          let view1 = UIView(frame: CGRect(x: 0, y: 0, width:self.view.frame.width, height: 30))
          view1.backgroundColor = .clear
          let lblHeader = UILabel(frame: CGRect(x: 15, y: 5, width: self.view.frame.width - 20, height: 30))
          lblHeader.text = headerTitle
          lblHeader.numberOfLines = 1
          lblHeader.textAlignment = .left
          lblHeader.font = UIFont(name: appFont, size: 14)
          lblHeader.backgroundColor = UIColor.clear
          lblHeader.textColor = UIColor.darkGray
          view1.addSubview(lblHeader)
          return view1
      }

}



class UPIHistoryCell: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDateAmount: UILabel!
    @IBOutlet weak var lblBusiness: UILabel!
    @IBOutlet weak var lblBusinessAmount: UILabel!
    @IBOutlet weak var btnStar: UIButton!
    @IBOutlet weak var btnRefund: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.layer.cornerRadius = 10
        bgView.layer.masksToBounds = true
        bgView.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        bgView.layer.borderWidth = 1.0
    }
    
}
