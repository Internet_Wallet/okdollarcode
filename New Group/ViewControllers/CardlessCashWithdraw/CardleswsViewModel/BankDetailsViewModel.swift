//
//  BankDetailsViewModel.swift
//  OK
//
//  Created by isure on 8/8/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation

class BankDetailsViewModel: NSObject{
    
    var networkObj = NetworkClass()
    var response = [BankListModel]()
    var referenceID = ""
    
    
    func getBankListWithAmount(completion :@escaping (_ success: Bool, _ message: String) -> Void) {
        response.removeAll()
        networkObj.getBankDetails(completion: { (response, success, message) in
            if success{
                self.response = response
                completion(true,"")
            }else{
                completion(false,message)
                
            }
        })
    }
    
    func getBankReferenceCode(completion :@escaping (_ success: Bool, _ message: String) -> Void) {
        response.removeAll()
        networkObj.getBankReferenceCode(completion: { (response, success, message) in
            if success{
                if let data = response{
                    self.referenceID = data
                    completion(true,"")
                }
            }else{
                completion(false,message)
            }
        })
    }
    
}
