//
//  NetworkClass.swift
//  OK
//
//  Created by isure on 8/8/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation

class NetworkClass: NSObject{
    
    var bankListModelObj = [BankListModel]()
    var transaction = Dictionary<String,Any>()
    
    
    
    func getBankDetails(completion :@escaping (_ result: [BankListModel], _ success: Bool, _ message: String) -> Void){
        
            //web.delegate = self
            guard let url = URL.init(string: "http://69.160.4.151:8001/RestService.svc/GetBankListAndDenominations") else { return }
            
            TopupWeb.genericClass(url: url, param: CardlessParameters.getParamForBankDetails(AppId: UserModel.shared.appID , Limit: "0", MobileNumber: UserModel.shared.mobileNo, Msid: UserModel.shared.msid, Offset: "0", Ostype: "1", Otp: uuid, Simid: uuid) as AnyObject, httpMethod: "POST") {[weak self] (response, success) in
            
            if success {
                if let json = response as? Dictionary<String,Any> {
                    if let code = json["Code"] as? NSNumber, code == 200 {
                        self?.bankListModelObj.removeAll()
                        if let data = json["Data"] as? String {
                            if let value = OKBaseController.convertToArrDictionary(text: data) as? Array<Dictionary<String,Any>> {
                                if value.count > 0 {
                                    for data in value{
                                       self?.bankListModelObj.append(BankListModel(data: data))
                                    }
                               }
                                completion(self?.bankListModelObj ?? [],true, "")
                            }
                        }
                    } else {
                        if let msg = json["Msg"] as? String {
                            completion([],false,msg)
                            //self?.showErrorAlert(errMessage: msg)
                        }
                    }
                }
            }else{
               completion([],false, "error")
        }
        
    }
    

}
    
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            transaction[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    
    func getBankReferenceCode(completion :@escaping (_ result: String?, _ success: Bool, _ message: String) -> Void){
       
        let param =  CardlessParameters.getSinglePayParamas()
        guard let url = URL.init(string: "https://www.okdollar.co/RestService.svc/GenericPayment") else { return }
        
        TopupWeb.genericClass(url: url, param: param as AnyObject, httpMethod: "POST") { [weak self](response, success) in
            if success {
                if let json = response as? Dictionary<String,Any> {
                    if let code = json["Code"] as? NSNumber, code == 200 {
                        if let data = json["Data"] as? String {
                            self?.transaction.removeAll()
                            let xmlString = SWXMLHash.parse(data)
                           self?.enumerate(indexer: xmlString)
                            completion(self?.transaction["transid"] as? String,true,"Success")
                        }
                    } else {
                        if let msg = json["Msg"] as? String {
                            completion("",false,msg)
                        }
                    }
                }
            }else{
                completion("",false, "error")
            }
        }
    }
    
}


