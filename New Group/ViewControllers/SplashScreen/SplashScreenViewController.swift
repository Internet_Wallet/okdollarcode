//
//  SplashScreenViewController.swift
//  OK
//
//  Created by Ashish Kumar Singh on 10/13/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreTelephony
import ContactsUI
import AddressBookUI
import CoreLocation

// login scenario check
let keyLoginStatus    = "checkForLogin"
let keyUserLoginModel = "userLoginModelKey"
let keyUserModel      = "userModelKey"
let keySectQuestionModel      = "sectQuestionModelKey"
let keyMsnIdModel     = "OK$AddFiveNumbersKey"
let keyQRCodeModel    =  "qrCodeModel"


class SplashScreenViewController: OKBaseController {
    
    @IBOutlet weak var greetingLabel: UILabel!
    @IBOutlet weak var greetingImage: UIImageView!
    @IBOutlet var gifImage: UIImageView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var copyRgtLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    
    var contactsArray = [String]()
    
    var deletedArray = [String]()
    
    var defaults = UserDefaults.standard
    
    let lastRetrieval = String()
    
    var imgArray = [UIImage]()
    private var initialRecords = [UserLoginDetails]()
    var currentHelpSuppostCase : HelpSupportNavigation = .dashboard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.detectSIMChange()
        self.gettingCountryCode()
        self.copyRgtLabel.text = "Copyright @ 2021 Internet Wallet Myanmar Ltd. All rights reserved".localized
        self.versionLabel.text = "Version  \(buildVersion), Build (\(buildNumber)) \( UserDefaults.standard.string(forKey: "BuildInstalledDate") ?? "")"
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(showHelpandSupportVC),
            name: NSNotification.Name(rawValue: "HelpAssistiveButtonTap"),
            object: nil)
        
        if CLLocationManager.locationServicesEnabled() {
            GeoLocationManager.shared.startUpdateLocation()
        } 
        
        self.timeintervalfun()
    }
    
    func timeintervalfun()
    {
        let lastDate = NSDate()
        
        if (defaults.object(forKey: "lastRetrieval") != nil) {
            
            if  lastRetrieval == defaults.value(forKey: "lastRetrieval") as! String {
                
                if abs(lastDate.timeIntervalSinceNow) > 36000 { // seconds in 24 hours  86400
                    // Time to change the label
                    
                    // Check to see if next incremented index is out of bounds
                    self.test()
                    
                    //    defaults.set(lastRetrieval, forKey: "lastRetrieval")
                    //    defaults.synchronize()
                }
                // Do nothing, not enough time has elapsed to change labels
                
            } else {
                
                // Nothing can do here
                
            }
        }
        else
        {
            
            self.test()
            
        }
    }
    
    @objc func showHelpandSupportVC() {
        
        self.createDocumentDirectory()
        self.saveImageToDocumentDirectory()
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            var currentName = ""
            if let topNav = topController as? UINavigationController
            {
                if let visibleVC = topNav.visibleViewController
                {
                    if let curCase = visibleVC.helpSupportNavigationEnum {
                        currentHelpSuppostCase = curCase
                        currentName = self.getCaseString()
                    } else {
                        currentName = "Dashboard"
                    }
                } else {
                    currentName = "Dashboard"
                }
            }
            println_debug(currentName)
            
            if currentName == "Dashboard"
                
            {
                DispatchQueue.main.async {
                    let navigation = UINavigationController()
                    let viewcontroller = UIStoryboard(name: "HelpSupport", bundle: Bundle.main).instantiateViewController(withIdentifier: "HelpContactViewController") as? HelpContactViewController
                    //viewcontroller?.navFrom = "DashBoardVC"
                    navigation.modalPresentationStyle = .fullScreen
                    navigation.viewControllers = [viewcontroller!]
                    topController.present(navigation, animated: true, completion: nil)
                }
                
            } else {
                self.getAllCallBackDetails(completion: { (modelArray) in
                    let filteredDict = modelArray.filter {
                        return $0.issueType == currentName
                    }
                    DispatchQueue.main.async {
                        let navigation = UINavigationController()
                        let viewController = UIStoryboard(name: "HelpSupport", bundle: Bundle.main).instantiateViewController(withIdentifier: "CallBackViewController") as? CallBackViewController
                        if filteredDict.count != 0 {
                            viewController?.subCategory = filteredDict[0]
                        }
                        viewController?.navFrom = "DashBoardVC"
                        navigation.viewControllers = [viewController!]
                        navigation.modalPresentationStyle = .fullScreen
                        topController.present(navigation, animated: true, completion: nil)
                    }
                })
            }
        }
        
    }
    
    
    func genericClassCallBack(url: URL, param: AnyObject, httpMethod: String, handle :@escaping (_ result: Any, _ success: Bool) -> Void ) {
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.capitalized
        
        if httpMethod == "POST" {
            
            if let stringParams = param as? String {
                let postLength = NSString(format:"%lu", stringParams.count) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = stringParams.data(using: String.Encoding.utf8, allowLossyConversion:true)
            } else {
                let theJSONData = try? JSONSerialization.data(
                    withJSONObject: param ,
                    options: JSONSerialization.WritingOptions(rawValue: 0))
                let jsonString          = NSString(data: theJSONData!,encoding: String.Encoding.ascii.rawValue)
                let postLength = NSString(format:"%lu", jsonString!.length) as String
                request.setValue(postLength, forHTTPHeaderField:"Content-Length")
                request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
            }
        }
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if((error) != nil) {
                handle(error!.localizedDescription, false)
            }else {
                handle(data as Any, true)
            }
        }
        dataTask.resume()
    }
    
    func getAllCallBackDetails(completion: @escaping (_ modelArray: [callBackModel]) -> Void) {
        
        if appDel.checkNetworkAvail() {
            var callBackModelArray = [callBackModel]()
            progressViewObj.showProgressView()
            let url = getUrl(urlStr: Url.getAllCallBack, serverType: .helpandSupportUrl)
            //println_debug(url)
            
            let params = Dictionary<String,String>()
            self.genericClassCallBack(url: url, param: params as AnyObject, httpMethod: "GET", handle: { (response, success) in
                if success {
                    do {
                        if let data = response as? Data {
                            let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                            if let dic = dict as? Dictionary<String,AnyObject> {
                                if let callBackArray = dic["Content"] as? [Dictionary<String,Any>] {
                                    for callBack in callBackArray {
                                        callBackModelArray.append(callBackModel.init(dict: callBack))
                                    }
                                    DispatchQueue.main.async {
                                        progressViewObj.removeProgressView()
                                    }
                                }
                            }
                        }
                        DispatchQueue.main.async {
                            progressViewObj.removeProgressView()
                        }
                    } catch {
                        DispatchQueue.main.async {
                            progressViewObj.removeProgressView()
                        }
                        //println_debug(error)
                    }
                } else {
                    DispatchQueue.main.async {
                        progressViewObj.removeProgressView()
                    }
                }
                completion(callBackModelArray)
            })
            
        } else {
            self.showErrorAlert(errMessage: "Please check your internet connection".localized)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //super.viewWillAppear(animated)
        UserDefaults.standard.set(true, forKey: "isLogOut")
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        
        let originalFrame = self.logoImage.frame
        
        self.logoImage.frame.size = originalFrame.size
        
        let time : DispatchTime = DispatchTime.now() + 2.0
        DispatchQueue.main.asyncAfter(deadline: time) {
            if userDef.bool(forKey: keyLoginStatus) {
                self.navigateToDashboard()
            } else {
                appDel.floatingButtonControl?.window.isHidden = true
                appDel.floatingButtonControl?.window.closeButton?.isHidden = true
                if let object = userDef.value(forKey: "PreloginSuccess") as? [Any] {
                    self.setupLocalization()
                    if object.last as? Bool ?? false == true {
                        self.navigateToPasswordScreen()
                    } else {
                        self.navigateToRegistration()
                    }
                } else {
                    let story = UIStoryboard.init(name: "Login", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "loginxyz")
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
    
    private func setupLocalization() {
        if let object = userDef.value(forKey: "PreloginSuccess") as? [Any], object.count > 1 {
            if let language = object[1] as? String {
                UserDefaults.standard.set(language, forKey: "currentLanguage")
                appDel.setSeletedlocaLizationLanguage(language: language)
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    
    private func navigateToPasswordScreen() {
        DispatchQueue.main.async(execute: {
            let story = UIStoryboard.init(name: "Login", bundle: nil)
            guard let vc = story.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else { return }
            if let object = userDef.value(forKey: "PreloginSuccess") as? [Any] {
                vc.agentCode = object.first as? String ?? ""
            }
            vc.screenInitialFrom = true
            self.navigationController?.pushViewController(vc, animated: true)
        })
    }
    
    
    private func navigateToRegistration() -> Void {
        DispatchQueue.main.async(execute: {
            
            let story = UIStoryboard.init(name: "Registration", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "regXYZ")
            if let vcNav = vc as? UINavigationController {
                vcNav.navigationBar.barTintColor = kYellowColor
                vcNav.navigationBar.tintColor = .white
                if let vcReg = vcNav.topViewController as? Regi_HelpVC {
                    vcReg.initialScreenFrom = true
                }
            }
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        })
    }
    
    func navigateToDashboard() {
        
        self.userModelUpdates()
        self.performSegue(withIdentifier: "presentModallyMainWindow", sender: nil)
        userDef.set(true, forKey: keyLoginStatus)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "presentModallyMainWindow" {
            if let chatVC = segue.destination as? SplashScreenViewController {
                chatVC.modalPresentationStyle = .fullScreen
            }
        }
    }
    func userModelUpdates() {
        UserLogin.wrapLoginFromDB()
        UserModel.wrapFromDatabase()
        closeDayModel.wrapFromDB()
        let sectModelDict = unArchiveObjectWith(withKey: keySectQuestionModel)
        if sectModelDict != nil {
            UserModel.wrapSecurityQuestionData(dict: sectModelDict!)
        }
        //Request Money-Mar15
        let qrCodeModelDict = unArchiveObjectWith(withKey: keyQRCodeModel)
        if qrCodeModelDict != nil {
            QrCodeModel.wrapUserData(dict: qrCodeModelDict!)
        }
    }
    
    func detectSIMChange() {
        let networkInfo = CTTelephonyNetworkInfo.init()
        networkInfo.subscriberCellularProviderDidUpdateNotifier = { carrier in
            // Do whatever you wanna do when a callback comes
            println_debug("Worked ------------------------------------------------------------------------------->")
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "HelpAssistiveButtonTap"), object: nil)
    }
    
    func getCaseString() -> String
    {
        switch currentHelpSuppostCase {
        case .Bonus_Point_Discount_Money:
            return "Bonus Point Discount Money"
        case .Bus:
            return "Bus"
        case .DTH:
            return "DTH"
        case .Electricity:
            return "Electricity"
        case .Ferry:
            return "Ferry"
        case .Flight:
            return "Flight"
        case .Gift_Cards:
            return "Gift Cards"
        case .Hotel:
            return "Hotel"
        case .Lucky_Draw:
            return "Lucky Draw"
        case .Merchant_Payment:
            return "Merchant Payment"
        case .Pay_Send:
            return "Pay/Send"
        case .Postpaid_Mobile:
            return "Postpaid Mobile"
        case .Resale:
            return "Resale"
        case .Recharge:
            return "Recharge"
        case .Request_Money:
            return "Request Money"
        case .Bill_Splitter:
            return "Bill Splitter"
        case .Taxi:
            return "Taxi"
        case .Landline_Bill:
            return "Landline Bill"
        case .Toll:
            return "Toll"
        case .Train:
            return "Train"
        case .Add_Withdraw:
            return "Add Withdraw"
        case .Reset_Password_Account_Blocked:
            return "ResetPassword/Account Blocked"
        case .dashboard:
            return "Dashboard"
        case .Others:
            return "Others"
        case .InternationalTopUp:
            return "InternationalTopUp"
        default:
            return "Dashboard"
        }
    }
    
    fileprivate func createDocumentDirectory() {
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("customDirectory")
        if !fileManager.fileExists(atPath: paths){
            try! fileManager.createDirectory(atPath: paths, withIntermediateDirectories: true, attributes: nil)
        }else{
            println_debug("Already dictionary created.")
        }
    }
    
    fileprivate func saveImageToDocumentDirectory() {
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("screenShot.jpg")
        let imageLoc = self.view.snapshot
        println_debug(paths)
        if fileManager.fileExists(atPath: paths) {
            try! fileManager.removeItem(atPath: paths)
        }
        if let image = imageLoc {
            let imageData = image.jpegData(compressionQuality: 0.5)
            fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
        }
    }
    
    @objc func test() {
        
        // Check contacts already Exist or not
        
        if (defaults.object(forKey: "contactssavearray") != nil) {
            deletedArray = UserDefaults.standard .value(forKey: "contactssavearray") as! [String]
        }
        else
        {
            deletedArray.removeAll()
        }
        
        if deletedArray.count>0
        {
            for i in 0..<self.deletedArray.count {
                
                self.deleteConatct()
                
            }
            
            defaults.removeObject(forKey: "contactssavearray")
            
            defaults.synchronize()
            
            deletedArray.removeAll()
            
            contactsArray.removeAll()
            
            if deletedArray.count == 0
            {
                self.httpRequest()
            }
            else
            {
                // Nothing Can do here
            }
            
        }
            
        else
        {
            
            self.httpRequest()
        }
        
        
    }
    
    
    private func httpRequest() {
        
        DispatchQueue.global(qos: .background).async {
            
            //create the url with NSURL
            let url = URL(string: "https://www.okdollar.co/RestService.svc/GetMessageReadingMobileNumbers")! //change the url
            
            //create the session object
            let session = URLSession.shared
            
            //now create the URLRequest object using the url object
            let request = URLRequest(url: url)
            
            //create dataTask using the session object to send data to the server
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                    //create json object from data
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                        
                        
                        if let some = json["Data"] as? String{
                            let value = some.components(separatedBy: "\"")
                            let fullNameArr = value[1].components(separatedBy: ",")
                            
                            
                            
                            for obj in fullNameArr{
                                if obj.hasPrefix("+95") || obj.hasPrefix("09") || obj.hasPrefix("\"+95"){
                                    
                                    self.contactsArray.append(obj)
                                }
                            }
                            
                        }
                        print(self.contactsArray)
                        
                        self.defaults.set(self.contactsArray, forKey: "contactssavearray")
                        self.defaults.synchronize()
                        
                        self.defaults.set(self.lastRetrieval, forKey: "lastRetrieval")
                        self.defaults.synchronize()
                        
                        
                        let store = CNContactStore()
                        let pred = CNContact.predicateForContacts(matchingName: "OK$")
                        
                        let contacts = try store.unifiedContacts(matching: pred, keysToFetch: [CNContactFormatter.descriptorForRequiredKeys(for: .fullName)])
                        
                        //                        guard contacts.count > 0 else{
                        //                            print("No contacts found")
                        //                            return
                        //                        }
                        
                        if (contacts.count>0)
                        {
                            //
                        }
                        else
                            
                        {
                            let store = CNContactStore()
                            let contact = CNMutableContact()
                            contact.familyName = ""
                            contact.givenName = "OK$"
                            let profileImage = UIImage(named:"okLogo")!
                            contact.imageData = profileImage.pngData()
                            
                            for i in 0..<self.contactsArray.count {
                                
                                //                                if i  == 0{
                                //
                                ////                                    contact.phoneNumbers = [CNLabeledValue(
                                ////                                        label:CNLabelPhoneNumberiPhone,
                                ////                                        value:CNPhoneNumber(stringValue:self.contactsArray[i]))]
                                //
                                //                                }
                                //                                else{
                                ////                                    contact.phoneNumbers = [CNLabeledValue(
                                ////                                        label:CNLabelPhoneNumberMain,
                                ////                                        value:CNPhoneNumber(stringValue:self.contactsArray[i]))]
                                //
                                //
                                //                                }
                                
                                let homePhone = CNLabeledValue(label: CNLabelPhoneNumberiPhone,
                                                               value: CNPhoneNumber(stringValue: self.contactsArray[i]))
                                let workPhone = CNLabeledValue(label: CNLabelPhoneNumberMobile,
                                                               value: CNPhoneNumber(stringValue: self.contactsArray[1]))
                                
                                contact.phoneNumbers = [homePhone, workPhone]
                                
                                // Save
                                let saveRequest = CNSaveRequest()
                                saveRequest.add(contact, toContainerWithIdentifier: nil)
                                try? store.execute(saveRequest)
                                
                            }
                        }
                    }
                } catch let error {
                    print(error.localizedDescription)
                }
            })
            task.resume()
        }
    }
    
    func deleteConatct() {
        
        let store = CNContactStore()
        let predicate = CNContact.predicateForContacts(matchingName: "OK$")
        //  let toFetch = [CNContactGivenNameKey]
        
        do{
            // let contacts = try store.unifiedContacts(matching: predicate,keysToFetch: toFetch as [CNKeyDescriptor])
            let contacts = try store.unifiedContacts(matching: predicate, keysToFetch: [CNContactFormatter.descriptorForRequiredKeys(for: .fullName)])
            guard contacts.count > 0 else{
                print("No contacts found")
                return
            }
            
            guard let contact = contacts.first
                
                else{
                    
                    return
            }
            
            let req = CNSaveRequest()
            let mutableContact = contact.mutableCopy() as! CNMutableContact
            req.delete(mutableContact)
            
            do{
                try store.execute(req)
                print("Success, You deleted the user")
            } catch let e{
                print("Error = \(e)")
            }
        } catch let err{
            print(err)
        }
    }
}


