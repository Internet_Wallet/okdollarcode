//
//  ChatMessageVC.swift
//  OK
//
//  Created by E J ANTONY on 13/12/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit

class ChatMessageVC: UIViewController {

    //MARK: - Outlet
    @IBOutlet weak var viewNavBar: UIView!
    @IBOutlet weak var chatTypingView: UIView!
    
    //MARK: - Properties
    var contact: OKContacts?
    
    //MARK: - Views Method
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(cont: contact)
        chatTypingView.layer.cornerRadius = 23.0
    }
    
    //MARK: - Methods
    func setupNavigationBar(cont: OKContacts? = nil) {
        guard let nibViews = Bundle.main.loadNibNamed("ChatHelperViews", owner: self, options: nil), let navBar = nibViews[2] as? ChatNavigationBar else { return }
        navBar.frame = CGRect(x: 0, y: 0, width: screenWidth, height: 65)
        navBar.initChatMsgNavBar()
        navBar.displayReceiverDetail(cont: cont)
        viewNavBar.addSubview(navBar)
    }
}
