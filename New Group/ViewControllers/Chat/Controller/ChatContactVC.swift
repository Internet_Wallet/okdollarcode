//
//  ChatContactVC.swift
//  OK
//
//  Created by E J ANTONY on 12/12/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit
import CoreData
protocol ChatContactProtocol: class {
    func contactDetail(contact: OKContacts)
}
class ChatContactVC: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var lblContactsCount: UILabel!
    
    //MARK: - Properties
    var chatContacts: [OKContacts] = []
    var delegateContact: ChatContactProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblContactsCount.text = ""
        loadContacts()
    }
    
    //MARK: - Methods
    func loadContacts() {
        let fetchReq = NSFetchRequest<NSManagedObject>(entityName: "OKContacts")
        let managedContext = appDelegate.persistentContainer.viewContext
        do {
            guard let fetchedObjects    = try managedContext.fetch(fetchReq) as? [OKContacts] else { return }
            chatContacts = fetchedObjects
        } catch {
            println_debug("Fetching operation failed \(error.localizedDescription)")
        }
        let contactCount = chatContacts.count
        if contactCount > 1 {
            lblContactsCount.text = "\(contactCount) Contacts"
        } else {
            lblContactsCount.text = "\(contactCount) Contact"
        }
    }
    
    //MARK: - Button Actions
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchAction(_ sender: UIButton) {
        
    }
}

//MARK: - UITableViewDataSource
extension ChatContactVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatContacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let contactCell = tableView.dequeueReusableCell(withIdentifier: "ChatContactCell", for: indexPath) as? ChatContactCell else { return ChatContactCell() }
        let chatContact = chatContacts[indexPath.row]
        contactCell.setCell(contactData: chatContact)
        return contactCell
    }
}

//MARK: - UITableViewDelegate
extension ChatContactVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chatContact = chatContacts[indexPath.row]
        delegateContact?.contactDetail(contact: chatContact)
    }
}

//MARK: - ChatContactCell
class ChatContactCell: UITableViewCell {
    
    //MARK: - Outlet
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var imgContact: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgContact.cornerEdge(radius: self.imgContact.frame.width / 2)
        self.layoutIfNeeded()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setCell(contactData: OKContacts) {
        self.lblName.text = contactData.name
        self.lblDesc.text = "Hi, Good"
        imgContact.image = nil
        if let imgData = contactData.imgData {
            if let userImage = UIImage(data: imgData, scale: 1.0) {
                imgContact.image = userImage
            }
        }
    }
}
