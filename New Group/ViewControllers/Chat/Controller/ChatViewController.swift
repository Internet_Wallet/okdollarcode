//
//  ChatViewController.swift
//  OK
//
//  Created by E J ANTONY on 06/12/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ChatViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var tableChat: UITableView!
    
    //MARK: - Properties
    var messages = [String]()
    
    //MARK: - Views Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        chatRegistration()
        setupNavigationBar()
        registerCells()
        configureTableView()
    }
    
    //MARK: - Methods
    func chatRegistration() {
        if !UserDefaults.standard.bool(forKey: "IsChatUserRegistered") {
            registerUser()
        }
        Database.database().reference().child("friend").removeValue { (error, ref) in
            if error != nil {
                print(error!)
            } else {
                print(ref)
            }
        }
        
        Database.database().reference().child("message").removeValue { (error, ref) in
            if error != nil {
                print(error!)
            } else {
                print(ref)
            }
        }
        
//        let messageDB = Database.database().reference().child("friend").child(UserModel.shared.mobileNo)
        
//        messageDB.setValue(["AKey": "AValue"]) { (error, ref) in
//            if error != nil {
//                print(error)
//            } else {
//                print(ref)
//            }
//        }
    }
    
    func registerUser() {
        let messageDB = Database.database().reference().child("user").child(UserModel.shared.mobileNo)
        var dic = [String: Any]()
        
        var msgDic = [String: Any]()
        msgDic["deleveryStatus"] = 0
        msgDic["idReceiver"] = "0"
        msgDic["idSender"] = "0"
        msgDic["msgType"] = ""
        msgDic["readStatus"] = false
        msgDic["text"] = ""
        msgDic["timestamp"] = 0
        
        var status = [String: Any]()
        status["isOnline"] = false
        status["timestamp"] = 0
        status["userStatus"] = ""
        
        dic["avata"] = UserModel.shared.proPic
        dic["buildVersion"] = buildNumber
        dic["customerStatus"] = 1
        dic["deviceId"] = UserModel.shared.deviceId
        dic["deviceModel"] = UIDevice.current.model
        dic["email"] = UserModel.shared.email
        dic["location"] = "0.0-0.0"
        dic["mobileNo"] = UserModel.shared.mobileNo
        dic["name"] = UserModel.shared.name
        dic["message"] = msgDic
        dic["status"] = status
        
        messageDB.setValue(dic) {
            (error, reference) in
            if error != nil {
                println_debug(error!)
            } else {
                UserDefaults.standard.set(true, forKey: "IsChatUserRegistered")
            }
        }
    }
    
    func setupNavigationBar() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        setLeftBarButtonItems()
        setRightBarButtonItems()
    }
    
    func setLeftBarButtonItems() {
        let backButton = UIBarButtonItem(image: UIImage(named: "chatBack")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(backAction))
        let okdBarButton = UIBarButtonItem(image: UIImage(named: "chatOkDollar")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: nil)
        self.navigationItem.leftBarButtonItems  = [backButton, okdBarButton]
    }
    
    func setRightBarButtonItems() {
        let moreBarButton = UIBarButtonItem(image: UIImage(named: "chatMore")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(showMoreOption))
        
        let ccButton = UIBarButtonItem(image: UIImage(named: "chatCustomerCare")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(customerCareChat))
        
        let searchBarButton = UIBarButtonItem(image: UIImage(named: "chatSearch")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(searchAction))
        
//        let moreButton = UIButton(type: .system)
//        moreButton.setImage(UIImage(named: "chatMore"), for: .normal)
//        let moreBarButton = UIBarButtonItem(customView: moreButton)
//        moreBarButton.customView?.translatesAutoresizingMaskIntoConstraints = false
//        moreBarButton.customView?.heightAnchor.constraint(equalToConstant: 35).isActive = true
//        moreBarButton.customView?.widthAnchor.constraint(equalToConstant: 35).isActive = true
        
        self.navigationItem.rightBarButtonItems = [moreBarButton, ccButton, searchBarButton]
    }
    
    func registerCells() {
        tableChat.register(UINib(nibName: "ChatHelperViews", bundle: nil), forCellReuseIdentifier: "ChatListMessageCell")
    }

    func configureTableView() {
        tableChat.rowHeight = UITableView.automaticDimension
        tableChat.estimatedRowHeight = 120.0
    }
    
    func setBase(tableView: UITableView) {
        if messages.count == 0 {
            guard let nibViews = Bundle.main.loadNibNamed("ChatHelperViews", owner: self, options: nil), let startChatView = nibViews[0] as? ViewOnNoChats else { return }
            startChatView.chatDelegate = self
            startChatView.frame = tableView.backgroundView?.frame ?? CGRect.zero
            tableView.backgroundView = startChatView
        } else {
            tableView.backgroundView = nil
        }
    }
    
    func openOKDollarContacts() {
        let chatStoryboard = UIStoryboard(name: "Chat", bundle: nil)
        guard let contactVC = chatStoryboard.instantiateViewController(withIdentifier: "ChatContactVC") as? ChatContactVC else { return }
        contactVC.delegateContact = self
        self.navigationController?.pushViewController(contactVC, animated: true)
    }
    
    func openChatMessage(contact: OKContacts? = nil) {
        let chatStoryboard = UIStoryboard(name: "Chat", bundle: nil)
        guard let chatMessageVC = chatStoryboard.instantiateViewController(withIdentifier: "ChatMessageVC") as? ChatMessageVC else { return }
        if let cont = contact {
            chatMessageVC.contact = cont
        }
        self.navigationController?.pushViewController(chatMessageVC, animated: true)
    }
    
    //MARK: - Target Methods
    @objc func backAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func showMoreOption() {
        println_debug("Show more option")
    }
    
    @objc func searchAction() {
        println_debug("Search is tapped")
    }
    
    @objc func customerCareChat() {
        println_debug("To Customer Care Room")
    }
    
    //Button Action Methods
    @IBAction func openContactList(_ sender: UIButton) {
        openOKDollarContacts()
    }
}

//MARK: - UITableViewDataSource
extension ChatViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        setBase(tableView: tableView)
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ChatListMessageCell", for: indexPath) as? ChatListMessageCell else { return ChatListMessageCell() }
        return cell
    }
}

//MARK: - ChatDelegates
extension ChatViewController: ChatDelegates {
    func startChat() {
        openOKDollarContacts()
    }
}

//MARK: - ChatContactProtocol
extension ChatViewController: ChatContactProtocol {
    func contactDetail(contact: OKContacts) {
        self.openChatMessage(contact: contact)
    }
}
