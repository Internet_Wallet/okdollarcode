//
//  ViewOnNoChats.swift
//  OK
//
//  Created by E J ANTONY on 06/12/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit
protocol ChatDelegates: class {
    func startChat()
}

class ViewOnNoChats: UIView {

    //MARK: - Outlets
    @IBOutlet weak var btnStartChat: UIButton!
    
    //MARK: - Properties
    var chatDelegate: ChatDelegates?
    
    //Button action method
    @IBAction func startChat(_ sender: UIButton) {
        chatDelegate?.startChat()
    }
}
