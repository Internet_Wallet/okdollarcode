//
//  ChatNavigationBar.swift
//  OK
//
//  Created by E J ANTONY on 08/12/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit

class ChatNavigationBar: UIView {

    //MARK: - Outlet
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var imgSearch: UIImageView!
    @IBOutlet weak var imgContactUser: UIImageView!
    @IBOutlet weak var imgMore: UIImageView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnContactUser: UIButton!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    
    //MARK: - Properties
    
    //MARK: - Methods
    func initChatMsgNavBar() {
        self.buttonsImages(isChatMessageVC: true)
        self.imgUser.layer.cornerRadius = 22
        self.imgUser.layer.masksToBounds = true
    }
    
    func initChatListNavBar() {
        self.buttonsImages(isChatMessageVC: false)
    }
    
    func buttonsImages(isChatMessageVC: Bool) {
        self.imgSearch.isHidden = isChatMessageVC
        self.imgContactUser.isHidden = isChatMessageVC
        self.imgMore.isHidden = isChatMessageVC
        self.btnSearch.isHidden = isChatMessageVC
        self.btnContactUser.isHidden = isChatMessageVC
        self.btnMore.isHidden = isChatMessageVC
        
        self.lblUserName.isHidden = !isChatMessageVC
    }
    
    func displayReceiverDetail(cont: OKContacts? = nil) {
        if let contact = cont {
            lblUserName.text = contact.name ?? ""
            imgUser.image = nil
            if let imgData = contact.imgData {
                if let userImage = UIImage(data: imgData, scale: 1.0) {
                    imgUser.image = userImage
                }
            }
            print(contact.phone ?? "No Phone")
        }
    }
    
    //MARK: - Button Action Methods
    @IBAction func goToPreviousScreen(_ sender: UIButton) {
        
    }
    
    @IBAction func searchAction(_ sender: UIButton) {
        
    }
    
    @IBAction func showContactList(_ sender: UIButton) {
        
    }
    
    @IBAction func moreAction(_ sender: UIButton) {
        
    }
}
