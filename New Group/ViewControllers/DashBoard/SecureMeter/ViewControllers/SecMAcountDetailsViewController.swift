//
//  SecMAcountDetailsViewController.swift
//  OK
//
//  Created by iMac on 7/4/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class SecMAcountDetailsViewController: SecMetBaseViewController {
    @IBOutlet weak var mainView: SecMAccountDetailsView!
    var userAccountModel: SecMetDatum?
    var tokenModel: CBBankTokenModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateNavigation(title: "User Details")
        mainView.updateView(model: userAccountModel, tokenModel: tokenModel)
    }
    
}
