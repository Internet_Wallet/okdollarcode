//
//  SecMAccountDetailsView.swift
//  OK
//
//  Created by iMac on 7/4/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class SecMAccountDetailsView: UIView {
    var userAccountModel: SecMetDatum?
    var customerDetailModel: SecMetDataClass?
    var secMeterTokenModel: CBBankTokenModel?
    @IBOutlet weak var accountIdLbl: UILabel! {
        didSet {
            self.accountIdLbl.text = accountIdLbl.text?.localized
        }
    }
    
    @IBOutlet weak var detailStackView: UIStackView! {
        didSet {
            self.detailStackView.isHidden = true
        }
    }
    @IBOutlet weak var accountIdTxt: UITextField! {
        didSet {
            self.accountIdTxt.placeholder = "Please enter 10 digit account id".localized
        }
    }
    @IBOutlet weak var selboardLbl: UILabel!
    @IBOutlet weak var selboardImageView: UIImageView!
    
    @IBOutlet weak var consumerNameLbl: UILabel! {
        didSet {
            consumerNameLbl.text = consumerNameLbl.text?.localized
        }
    }
    @IBOutlet weak var consumerNameVal: UILabel!
    
    @IBOutlet weak var divisionLbl: UILabel! {
        didSet {
            divisionLbl.text = divisionLbl.text?.localized
        }
    }
    @IBOutlet weak var divisionVal: UILabel!
    
    @IBOutlet weak var addressLbl: UILabel! {
        didSet {
            addressLbl.text = addressLbl.text?.localized
        }
    }
    @IBOutlet weak var addressVal: UILabel!
    
    @IBOutlet weak var billAmtLbl: UILabel! {
        didSet {
            billAmtLbl.text = billAmtLbl.text?.localized
        }
    }
    @IBOutlet weak var billAmtVal: UILabel!
    
    @IBOutlet weak var billMonthLbl: UILabel! {
        didSet {
            billMonthLbl.text = billMonthLbl.text?.localized
        }
    }
    @IBOutlet weak var billMonthVal: UILabel!
    
    @IBOutlet weak var billNoLbl: UILabel! {
        didSet {
            billNoLbl.text = billNoLbl.text?.localized
        }
    }
    @IBOutlet weak var billNoVal: UILabel!
    
    @IBOutlet weak var detailViewHeight: NSLayoutConstraint! {
        didSet {
            detailViewHeight.constant = 45.0
        }
    }
    
    @IBOutlet weak var amountTxt: FloatLabelTextField! {
        didSet {
            amountTxt.isHidden = true
        }
    }
    @IBOutlet weak var detailStackViewHeight: NSLayoutConstraint! {
        didSet {
            detailStackViewHeight.constant = 0.0
        }
    }
    @IBOutlet weak var amountHeight: NSLayoutConstraint! {
        didSet {
            amountHeight.constant = 0.0
        }
    }
    
    
    @IBOutlet weak var payBtn: UIButton! {
        didSet {
            payBtn.isHidden = true
        }
    }
    
    func updateView(model: SecMetDatum?, tokenModel: CBBankTokenModel?) {
        userAccountModel = model
        secMeterTokenModel = tokenModel
        self.selboardLbl.text = model?.companyName
        if let imageData = self.getImage(imageUrl: model?.companyLogo ?? "") {
            selboardImageView.image = UIImage(data: imageData)
        }
    }
    
    private func getImage(imageUrl: String) -> Data? {
        if let url = URL(string: imageUrl) {
            do {
                let imageData = try Data.init(contentsOf: url)
                return imageData
                
            } catch {
                
            }
        }
        return nil
    }
    
    //MARK:- Update UI
    func updateUI() {
        guard let customerInfo = customerDetailModel?.customerContactInfo else { return }
        self.detailViewHeight.constant = 410.0
        self.detailStackViewHeight.constant = 300.0
        self.amountHeight.constant = 45.0
        self.detailStackView.isHidden = false
        self.amountTxt.isHidden = false
        self.consumerNameVal.text = "\(customerInfo.title ?? "") \(customerInfo.firstName ?? "") \(customerInfo.lastName ?? "")"
        self.addressVal.text = customerInfo.connectionNo ?? ""
        self.divisionVal.text = customerInfo.meterNo ?? ""
        self.billNoVal.text = customerInfo.postcode ?? ""
        self.billMonthVal.text = customerInfo.phone1
        var address = ""
        if let addr1 = customerInfo.addr1, addr1.count > 0 {
            address = "\(address)\(addr1)"
        }
        if let addr2 = customerInfo.addr2, addr2.count > 0 {
            address = "\(address),\(addr2)"
        }
        if let addr3 = customerInfo.addr3, addr3.count > 0 {
            address = "\(address),\(addr3)"
        }
        if let addr4 = customerInfo.addr4, addr4.count > 0 {
            address = "\(address),\(addr4)"
        }
        self.billAmtVal.text = address
    }
    
    
    //MARK:- Button Actions
    @IBAction func getUserAccountDetails(_ sender: UIButton) {
        if appDelegate.checkNetworkAvail() {
            let accountId = accountIdTxt.text ?? ""
            if accountId.count != 20 {
                PaytoConstants.alert.showErrorAlert(title: "" , body: "Invalid Account Id")
                return
            }
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            
            let urlStr = URL(string: "http://52.76.209.187:3001/okdollar/v1/GetUserDetails/\(accountId)")
            let params = Dictionary<String, Any>()
            web.genericClass(url: urlStr!, param: params as AnyObject, httpMethod: "GET", mScreen: "GetUserDetail", authToken: secMeterTokenModel?.data?.token, authTokenKey: "x-access-token")
        } else {
            println_debug("No Internet")
        }
    }
    
    private func agentServiceTypeString(type: AgentType) -> String {
        switch type {
        case .user:
            return "SUBSBR"
        case .merchant:
            return "MER"
        case .agent:
            return "AGENT"
        case .advancemerchant:
            return "ADVMER"
        case .dummymerchant:
            return "DUMMY"
        case .oneStopMart:
            return "ONESTOP"
        }
    }
    
    private func getParamsforTopup() -> Dictionary<String, Any> {
        var appInfoDict = Dictionary<String, Any>()
        appInfoDict["mobile_no"] =  UserModel.shared.mobileNo
        appInfoDict["sim_id"] =  UserModel.shared.simID
        appInfoDict["ms_id"] =  UserModel.shared.msid
        appInfoDict["cell_id"] =  ""
        appInfoDict["trans_type"] =  "\(1)"
        appInfoDict["amount"] =  self.amountTxt.text ?? ""
        appInfoDict["lat"] =  UserModel.shared.lat
        appInfoDict["long"] =  UserModel.shared.long
        appInfoDict["profile_img"] = UserModel.shared.proPic
        appInfoDict["merchant_type"] = self.agentServiceTypeString(type: UserModel.shared.agentType)
        appInfoDict["password"] = ok_password ?? ""
        appInfoDict["transaction_type"] = "PAYTO"
        
        var topUpInfoDict = Dictionary<String, Any>()
        topUpInfoDict["UserID"] = userAccountModel?.userID ?? ""
        topUpInfoDict["Password"] =  userAccountModel?.password ?? ""
        topUpInfoDict["TransactionNo"] =  "\(customerDetailModel?.transactionNo ?? 0)"
        topUpInfoDict["ComServerId"] =  userAccountModel?.comServerID ?? ""
        topUpInfoDict["PaymentCardId"] =  userAccountModel?.paymentCardID ?? ""
        topUpInfoDict["TerminalId"] =  userAccountModel?.terminalID ?? ""
        topUpInfoDict["AgentNo"] =  userAccountModel?.agentNo ?? ""
    //{
//        "AppInfo":{
//            "mobile_no":"00959783798314",
//            "sim_id":"36578975464",
//            "ms_id":"36578975464",
//            "profile_img":"http://imageurl.png",
//            "lat":"1.5555",
//            "lang":"2.584121",
//            "trans_type":1,
//            "transaction_type":"PAYTO",
//            "merchant_type":"SUBSBR",
//            "cell_id":"134957334",
//            "amount":"200",
//            "password":"123456"
//        },
//        "TopUpData":{
//            "UserID": "QkVTQ09NV0VCVVNFUg==",
//            "Password": "Y2NuNnczYmVzY29t",
//            "TransactionNo": "155522",
//            "ComServerId": "199",
//            "PaymentCardId": "13132106011234567891",
//            "TerminalId": "1",
//            "AgentNo": "2"
//        }
//        }
        
        let params = ["AppInfo" : appInfoDict, "TopUpData" : topUpInfoDict]
        return params
    }
    
    @IBAction func payBtnAction(_ sender: UIButton) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            
            let urlStr = URL(string: "http://52.76.209.187:3001/okdollar/v1/AppNGetTopUpCode")
            let params = self.getParamsforTopup()
            web.genericClass(url: urlStr!, param: params as AnyObject, httpMethod: "POST", mScreen: "topUpCall", authToken: secMeterTokenModel?.data?.token, authTokenKey: "x-access-token")
        } else {
            println_debug("No Internet")
        }
    }

}


//MARK:- API Response
extension SecMAccountDetailsView: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        if screen == "GetUserDetail" {
            guard let castedData = json as? Data else {
                PTLoader.shared.hide()
                println_debug("Data error")
                return
            }
            do {
                PTLoader.shared.hide()
                let decoder = JSONDecoder()
                let validation = try decoder.decode(SecMetCustModel.self, from: castedData)
                customerDetailModel = validation.data
                if validation.code == 200 {
                    DispatchQueue.main.async {
                        self.updateUI()
                    }
                }
            } catch {
                
            }
        } else if screen == "topUpCall" {
            guard let castedData = json as? Data else {
                PTLoader.shared.hide()
                println_debug("Data error")
                return
            }
            do {
                PTLoader.shared.hide()
                let decoder = JSONDecoder()
                let validation = try decoder.decode(SecMetTopUpModel.self, from: castedData)
                if validation.code == 200 {
                    PaytoConstants.alert.showErrorAlert(title: "", body: "Success")
                } else {
                    PaytoConstants.alert.showErrorAlert(title: "", body: validation.message ?? "")
                }
            } catch {
                
            }
        }
    }
    
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        PTLoader.shared.hide()
        if screenName == "CBBankValidation" {
            println_debug("Api Failure in PhNoValidation response")
        }
    }
}


//MARK:- Textfield Delegate
extension SecMAccountDetailsView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text as! NSString).replacingCharacters(in: range, with: string)
        if textField == self.amountTxt {
            if text.count > 3 {
                return false
            } else if text.count > 1 {
                self.payBtn.isHidden = false
            }
        } else {
            if text.count > 20 {
                return false
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return false
    }
}
