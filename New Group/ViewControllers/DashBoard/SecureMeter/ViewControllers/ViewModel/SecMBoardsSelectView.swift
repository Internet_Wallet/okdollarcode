//
//  SecMBoardsSelectView.swift
//  OK
//
//  Created by iMac on 7/4/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class SecMBoardsSelectView: UIView {
    weak var secMSelectBoardDelegate: SecMSelectBoardsViewProtocol?
    var secMeterTokenModel: CBBankTokenModel?
    var boardsArray: [SecMetDatum]?
    @IBOutlet weak var selectBoardLbl: UILabel! {
        didSet {
            self.selectBoardLbl.text = "Select Board".localized
        }
    }
    @IBOutlet weak var selectBoardTbl: UITableView! {
        didSet {
            self.selectBoardTbl.delegate = self
            self.selectBoardTbl.dataSource = self
            self.selectBoardTbl.tableFooterView = UIView()
        }
    }

    func updateView() {
        self.generateToken()
    }
    
    private func generateToken() {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            
            let urlStr = URL(string: "http://52.76.209.187:3001/okdollar/v1/auth/token")
            
            let TICKS_AT_EPOCH: Int64 = 621355968000000000
            let TICKS_PER_MILLISECOND: Int64 = 10000
            var timeInterval = DateComponents()
            timeInterval.hour = 6
            timeInterval.minute = 30
            let futureDate = Calendar.current.date(byAdding: timeInterval, to: Date())!
            let currentTime = futureDate.millisecondsSince1970
            let getUTCTicks =  (currentTime * TICKS_PER_MILLISECOND) + TICKS_AT_EPOCH
            let aKey = Url.aKey_SecureMeter + "\(getUTCTicks)"
            do {
                let hashKey = try Encryption.encryptData(plainText: aKey, hexKey: Url.sKey_SecureMeter)
                let params = ["password": hashKey]
                web.genericClass(url: urlStr!, param: params as AnyObject, httpMethod: "POST", mScreen: "SecureMeterTokenGenerate")
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            println_debug("No Internet")
        }
    }
    
    private func getBoards() {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStr = URL(string: "http://52.76.209.187:3001/okdollar/v1/GetSecureClients")
            let params = Dictionary<String, Any>()
            web.genericClass(url: urlStr!, param: params as AnyObject, httpMethod: "GET", mScreen: "GetBoardsList", authToken: secMeterTokenModel?.data?.token, authTokenKey: "x-access-token")
        } else {
            println_debug("No Internet")
        }
    }
    
    private func updateTableView(model: SecMetBoardModel) {
        DispatchQueue.main.async {
            self.selectBoardTbl.reloadData()
        }
    }

}




extension SecMBoardsSelectView: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.boardsArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "secMetBoardCell", for: indexPath) as? SecMeterBoardCell
        cell?.wrapData(model: self.boardsArray?[indexPath.row])
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.secMSelectBoardDelegate?.naviagetToUser(model: boardsArray?[indexPath.row], secMeterTokenModel: secMeterTokenModel)
    }
}


extension SecMBoardsSelectView: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        if screen == "SecureMeterTokenGenerate" {
            guard let castedData = json as? Data else {
                PTLoader.shared.hide()
                println_debug("Data error")
                return
            }
            do {
                PTLoader.shared.hide()
                let decoder = JSONDecoder()
                let validation = try decoder.decode(CBBankTokenModel.self, from: castedData)
                secMeterTokenModel = validation
                if validation.code == 200 {
                    self.getBoards()
                }
            } catch {
                
            }
        } else if screen == "GetBoardsList" {
            guard let castedData = json as? Data else {
                PTLoader.shared.hide()
                println_debug("Data error")
                return
            }
            do {
                PTLoader.shared.hide()
                let decoder = JSONDecoder()
                let validation = try decoder.decode(SecMetBoardModel.self, from: castedData)
                if validation.code == 200 {
                    self.boardsArray = validation.data
                    self.updateTableView(model: validation)
                }
            } catch {
                
            }
        }
    }
    
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        PTLoader.shared.hide()
        if screenName == "CBBankValidation" {
            println_debug("Api Failure in PhNoValidation response")
        }
    }
}


class SecMeterBoardCell: UITableViewCell {
    @IBOutlet weak var boardNameLabel: UILabel!
    @IBOutlet weak var boardImageView: UIImageView!
    
    
    func wrapData(model: SecMetDatum?) {
        boardNameLabel.text = model?.companyName ?? ""
        if let imageData = self.getImage(imageUrl: model?.companyLogo ?? "") {
            boardImageView.image = UIImage(data: imageData)
        }
    }
    
    private func getImage(imageUrl: String) -> Data? {
        if let url = URL(string: imageUrl) {
            do {
                 let imageData = try Data.init(contentsOf: url)
                    return imageData
                
            } catch {
                
            }
        }
        return nil
    }
}
