//
//  SecMSelectBoardsViewController.swift
//  OK
//
//  Created by iMac on 7/4/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol SecMSelectBoardsViewProtocol: class {
    func naviagetToUser(model: SecMetDatum?, secMeterTokenModel: CBBankTokenModel?)
}

class SecMSelectBoardsViewController: SecMetBaseViewController, SecMSelectBoardsViewProtocol {
    @IBOutlet weak var mainView: SecMBoardsSelectView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateNavigation(title: "Secure Meter")
        mainView.secMSelectBoardDelegate = self
        mainView.updateView()
    }
    
    func naviagetToUser(model: SecMetDatum?, secMeterTokenModel: CBBankTokenModel?) {
        let story = UIStoryboard(name: "secureMeter", bundle: nil)
        guard let userVC = story.instantiateViewController(withIdentifier: "SecMAcountDetailsViewController") as? SecMAcountDetailsViewController else { return }
        userVC.userAccountModel = model
        userVC.tokenModel = secMeterTokenModel
        self.navigationController?.pushViewController(userVC, animated: true)
    }
    
}
