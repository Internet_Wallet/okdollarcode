//
//  SecureMeterModel.swift
//  OK
//
//  Created by iMac on 7/4/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation

//Boards
struct SecMetBoardModel: Codable {
    let code: Int?
    let message: String?
    let data: [SecMetDatum]?
}

// MARK: - Datum
struct SecMetDatum: Codable {
    let terminalID, projectID, agentNo: String?
    let companyLogo: String?
    let id, companyName, userID, password: String?
    let transactionNo, comServerID, paymentCardID: String?
    
    enum CodingKeys: String, CodingKey {
        case terminalID = "TerminalId"
        case projectID = "ProjectId"
        case agentNo = "AgentNo"
        case companyLogo = "CompanyLogo"
        case id = "_id"
        case companyName = "CompanyName"
        case userID = "UserID"
        case password = "Password"
        case transactionNo = "TransactionNo"
        case comServerID = "ComServerId"
        case paymentCardID = "PaymentCardId"
    }
}


//Mark: - Customer Information
struct SecMetCustModel: Codable {
    let code: Int?
    let message: String?
    let data: SecMetDataClass?
}

// MARK: - SecMetDataClass
struct SecMetDataClass: Codable {
    let returnCode, transactionNo: Int?
    let customerContactInfo: SecMetCustomerContactInfo?
    
    enum CodingKeys: String, CodingKey {
        case returnCode = "ReturnCode"
        case transactionNo = "TransactionNo"
        case customerContactInfo = "CustomerContactInfo"
    }
}

// MARK: - SecMetCustomerContactInfo
struct SecMetCustomerContactInfo: Codable {
    let additionalInfo, addr1, addr2, addr3: String?
    let addr4, connectionNo, email: String?
    let firstName, houseNo, lastName, mpan: String?
    let meterNo, phone1, phone2, postcode: String?
    let searchKey, title: String?
    
    enum CodingKeys: String, CodingKey {
        case additionalInfo = "AdditionalInfo"
        case addr1 = "Addr1"
        case addr2 = "Addr2"
        case addr3 = "Addr3"
        case addr4 = "Addr4"
        case connectionNo = "ConnectionNo"
        case email = "Email"
        case firstName = "FirstName"
        case houseNo = "HouseNo"
        case lastName = "LastName"
        case mpan = "MPAN"
        case meterNo = "MeterNo"
        case phone1 = "Phone1"
        case phone2 = "Phone2"
        case postcode = "Postcode"
        case searchKey = "SearchKey"
        case title = "Title"
    }
}


//Topup Model
struct SecMetTopUpModel: Codable {
    let code: Int?
    let message: String?
    let data: SecMetTopUpDataClass?
}

// MARK: - SecMetTopUpDataClass
struct SecMetTopUpDataClass: Codable {
    let id, transactionNo, creditAmount, customerMessage: String?
    let customerName, debtDeducted, encryptedToken, meterCreditAmount: String?
    let meterNo, outstandingDebt, systemMessage, transactionAcknowledgeNo: String?
    let okTransactionID, paymentCardID, userID, created: String?
    let updated: String?
    let v: Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case transactionNo = "TransactionNo"
        case creditAmount = "CreditAmount"
        case customerMessage = "CustomerMessage"
        case customerName = "CustomerName"
        case debtDeducted = "DebtDeducted"
        case encryptedToken = "EncryptedToken"
        case meterCreditAmount = "MeterCreditAmount"
        case meterNo = "MeterNo"
        case outstandingDebt = "OutstandingDebt"
        case systemMessage = "SystemMessage"
        case transactionAcknowledgeNo = "TransactionAcknowledgeNo"
        case okTransactionID = "OkTransactionId"
        case paymentCardID = "PaymentCardId"
        case userID = "UserID"
        case created, updated
        case v = "__v"
    }
}

