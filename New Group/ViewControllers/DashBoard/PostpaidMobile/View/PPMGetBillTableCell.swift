//
//  LLBGetBillTableCell.swift
//  OK
//
//  Created by Rahul Tyagi on 10/17/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol PPMGetBillTableCellDelegate{
    func passData(cell: PPMGetBillTableCell)
}

protocol HideAd {
    func hide(hide: Bool)
}

class PPMGetBillTableCell: UITableViewCell{
    var placeholderArray : [String] = []
    var imageArray : [UIImage] = []
    var delegate : PPMGetBillTableCellDelegate?
    var adDelegate: HideAd?
    var parentClassVC:PPMGetBillVC?
    var customIndexPath = IndexPath()
    var holdBillNumber = String()
    let validObj = PayToValidations()
    @IBOutlet weak var imageViewOutlet: UIImageView!
    @IBOutlet weak var btnCross: UIButton!
    @IBOutlet weak var textFieldOutlet: RestrictedCursorMovementWithFloat!{
        didSet
        {
            textFieldOutlet.font =  UIFont(name: appFont, size: appFontSize)
            textFieldOutlet.text = textFieldOutlet.text?.localized
        }
    }
    @IBOutlet weak var btn: UIButton!{
        didSet
        {
            btn.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
            btn.titleLabel?.text = btn.titleLabel?.text?.localized
        }
    }
    override func awakeFromNib(){
        super.awakeFromNib()
        textFieldOutlet.delegate = self
         self.btnCross.isHidden = true
        placeholderArray = ["Mobile Number".localized, "Confirm Mobile Number".localized, "Bill Month".localized, "Amount".localized, "Bill Status".localized, "Service Fee".localized]
        imageArray = [#imageLiteral(resourceName: "contact_bcd"),#imageLiteral(resourceName: "contact_bcd"), #imageLiteral(resourceName: "bill_monthImg"),#imageLiteral(resourceName: "amount"),#imageLiteral(resourceName: "status"),#imageLiteral(resourceName: "add_money")]
        textFieldOutlet.delegate = self
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    @IBAction func onClickCrossButton(_ sender: Any) {
        textFieldOutlet.text = "09"
        btnCross.isHidden = true
    }
    
    func setCellData(index : IndexPath, text: String){
        customIndexPath = index
        self.textFieldOutlet.placeholder = (placeholderArray[index.row]).localized
        self.imageViewOutlet.image = imageArray[index.row]
        self.textFieldOutlet.text = text.localized
        self.textFieldOutlet.title = (placeholderArray[index.row]).localized
        self.textFieldOutlet.tag = index.row
        configureCell(withIndex: index)
        if textFieldOutlet.tag == 1{
            self.textFieldOutlet.becomeFirstResponder()
        }
        self.btn.tag = index.row
        
        //        self.textFieldOutlet.becomeFirstResponder()
    }
    func setCellData(index : IndexPath, text: String, billDataModelObject: PPMHistoryVCDataModel){
     
        if isObjectNotNil(object: billDataModelObject){
            println_debug(billDataModelObject)
            println_debug(index.row)
            customIndexPath = index
            self.textFieldOutlet.placeholder = (placeholderArray[index.row]).localized
            self.imageViewOutlet.image = imageArray[index.row]
            self.textFieldOutlet.tag = index.row
            self.textFieldOutlet.text = text.localized
            self.textFieldOutlet.title = (placeholderArray[index.row]).localized
            self.btn.tag = index.row
            configureCell(withIndex: index)
            switch(index.row) {
            case 0 :
                if text != "09" {
                    if !(text.hasPrefix("09")) {
                        self.textFieldOutlet.text = "0".appending(text).localized
                    }
                }
                if text == "" {
                    self.textFieldOutlet.text = "09".appending(text).localized
                }
                //            self.holdBillNumber = ("0".appending(billDataModelObject.PhoneNumber)).localized
                break
            case 1 :
                if text != "09" {
                    if !(text.hasPrefix("09")) {
                        self.textFieldOutlet.text = "0".appending(text).localized
                    }
                }
                if text == "" {
                    self.textFieldOutlet.text = "09".appending(text).localized
                }
                //            self.holdBillNumber = ("0".appending(billDataModelObject.PhoneNumber)).localized
                break
            default:  break
            }
        }
        else{
            customIndexPath = index
            self.textFieldOutlet.placeholder = (placeholderArray[index.row]).localized
            self.imageViewOutlet.image = imageArray[index.row]
            self.textFieldOutlet.tag = index.row
            self.textFieldOutlet.text = text.localized
            self.textFieldOutlet.title = (placeholderArray[index.row]).localized
            self.btn.tag = index.row
            configureCell(withIndex: index)
        }
    }
    func configureCell(withIndex index : IndexPath){
        switch index.row{
        case 0:  // self.textFieldOutlet.isEnabled = false
            self.textFieldOutlet.keyboardType = .numberPad
            self.btn.isHidden = false
        case 1:  //self.textFieldOutlet.isEnabled = false
            
            self.textFieldOutlet.keyboardType = .numberPad
            break
        case 2:  self.textFieldOutlet.isEnabled = false;
        self.btn.isHidden = true
            break
        case 3:  self.textFieldOutlet.isEnabled = false;
        self.btn.isHidden = true
            break
        case 4:  self.textFieldOutlet.isEnabled = false;
        self.btn.isHidden = true
            break
        case 5:  self.textFieldOutlet.isEnabled = false;
        self.btn.isHidden = true
            break
        default: self.textFieldOutlet.isEnabled = false;
        self.btn.isHidden = true
            break
        }
        if index.row == 0{ }
        else{}
    }
    func isObjectNotNil(object:PPMHistoryVCDataModel!) -> Bool{
        if let _:PPMHistoryVCDataModel = object{
            return true
        }
        return false
    }
    
    
    
    @IBAction func textFieldDidChange(_ sender: UITextField){
        delegate?.passData(cell: self)
        let chars = textFieldOutlet.text!
        //println_debug("ssssssssss",sender.tag)
        if sender.tag == 0 {
            if chars.count > 2 {
                self.btnCross.isHidden = false
            }else {
                self.btnCross.isHidden = true
            }
        }
        else if sender.tag == 1 {
            if chars.count > 2 {
                self.btnCross.isHidden = false
            }else {
                self.btnCross.isHidden = true
            }
        }
    }
}
extension PPMGetBillTableCell: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       
        adDelegate?.hide(hide: false)
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        adDelegate?.hide(hide: false)
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
     adDelegate?.hide(hide: true)
        if textField.text?.length == 0{
            textField.text = "09"
            textField.becomeFirstResponder()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text: String? = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        if (text?.length)! > 9 {
            return false
        }
        
        let chars = textField.text! + string;
        if (chars.hasPrefix("099")) || (chars.hasPrefix("094")) || (chars.hasPrefix("091")) || (chars.hasPrefix("093")) || (chars.hasPrefix("097")) {
            alertViewObj.wrapAlert(title: "", body: PPMStrings.ErrorMessages.incorrectMobileNumber, img: #imageLiteral(resourceName: "dashboard_my_number"))
            alertViewObj.addAction(title: "OK".localized, style: .target, action: {})
            alertViewObj.showAlert(controller: parentClassVC)
            return false
        }
        else {
            if validObj.getNumberRangeValidation(chars).isRejected == true {
                alertViewObj.wrapAlert(title: "", body: PPMStrings.ErrorMessages.incorrectMobileNumber, img: #imageLiteral(resourceName: "dashboard_my_number"))
                alertViewObj.addAction(title: "OK".localized, style: .target, action: {})
                alertViewObj.showAlert(controller: parentClassVC)
                return false
            }
        }
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) && (text?.length)! < 2 {
            
            textField.text = "09"
            return false
            
        }
        
        
        if textField.tag == 0{
            //            if (isBackSpace == -92) && (text?.length)! < 9 {
            //                self.parentClassVC?.classActionButton?.isHidden = true
            //                self.parentClassVC?.checkAnotherNumber()
            //            }
            
            if (text?.length)! > 8 {
                self.parentClassVC?.LastBillAmountStr = ""
                self.parentClassVC?.rowCountArray.append("")
                self.parentClassVC?.textFieldDataArray[0] = text!
                self.parentClassVC?.textFieldDataArray[1] = "09"
                self.textFieldOutlet.resignFirstResponder()
                self.parentClassVC?.tableviewOutlet.reloadData()
                return false
            }
            
            if (isBackSpace == -92) && (text?.length)! == 8 {
                self.parentClassVC?.rowCountArray.removeAll()
                self.parentClassVC?.rowCountArray.append("")
                self.parentClassVC?.textFieldDataArray.removeAll()
                self.parentClassVC?.textFieldDataArray = ["", "", "", "", "", ""]
                self.parentClassVC?.textFieldDataArray[0] = text!
//                self.parentClassVC?.classActionButton?.isHidden = true
//                self.parentClassVC?.classActionButton?.setTitle("Get Bill", for: .normal)
                self.parentClassVC?.getBillBtn.isHidden = true
                self.parentClassVC?.heightGetBillBtnConstraint.constant = 0
                self.parentClassVC?.getBillBtn.setTitle("Get Bill", for: .normal)
                self.parentClassVC?.tableviewOutlet.tableFooterView = UIView()
                self.parentClassVC?.tableviewOutlet.reloadData()
                self.parentClassVC?.LastBillAmountStr = ""
                return false
            }
            
        }
        
        if textField.tag == 1 {
            
            let enteredMbText = self.parentClassVC?.textFieldDataArray[0]   as! String
            let count:Int = (textField.text?.count)! + 1
            // First n chars
            let firstChars = String(enteredMbText.prefix(count)) // first2Chars = "My"
            //enter cha is equal to mb no
            if string != "" && (textField.text! + string == firstChars) {
                //replace row value with new value
                self.parentClassVC?.textFieldDataArray[1] = text!
                
                if (text?.length)! > 8 {
//                    self.parentClassVC?.classActionButton?.isHidden = false
                    self.parentClassVC?.getBillBtn.isHidden = false
                    self.parentClassVC?.heightGetBillBtnConstraint.constant = 50
                }
                else {
//                    self.parentClassVC?.classActionButton?.isHidden = true
                    self.parentClassVC?.getBillBtn.isHidden = true
                    self.parentClassVC?.heightGetBillBtnConstraint.constant = 0
                }
                return true
            }else {
                //not matching with entered mb no
                if (isBackSpace == -92) {
                    if (text?.length)! > 8 {
//                        self.parentClassVC?.classActionButton?.isHidden = false
                        self.parentClassVC?.getBillBtn.isHidden = false
                        self.parentClassVC?.heightGetBillBtnConstraint.constant = 50
                    }
                    else {
                        self.parentClassVC?.textFieldDataArray[1] = text!
//                        self.parentClassVC?.classActionButton?.isHidden = true
                        self.parentClassVC?.getBillBtn.isHidden = true
                        self.parentClassVC?.heightGetBillBtnConstraint.constant = 0
                    }
                    return true
                }
                else {
                    return false
                }
            }
        }
        return true
    }
}
