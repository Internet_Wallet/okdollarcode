//
//  LLBConfirmationTableCell.swift
//  OK
//
//  Created by Rahul Tyagi on 10/17/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit

class PPMConfirmationTableCell: UITableViewCell {
    @IBOutlet weak var labelOutlet: UILabel!{
        didSet
        {
            labelOutlet.font = UIFont(name: appFont, size: appFontSize)
            labelOutlet.text = labelOutlet.text?.localized
        }
    }
    @IBOutlet weak var valueOutlet: UILabel!{
        didSet
        {
            valueOutlet.font = UIFont(name: appFont, size: appFontSize)
            valueOutlet.text = valueOutlet.text?.localized
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
