//
//  RTJsonParser.swift
//  OK
//
//  Created by Rahul Tyagi on 9/28/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit

protocol PPMJsonParserDelegate {
    func ResponseFromServer(withSuccess Success: Bool, json: AnyObject?, screen: String, Error : String?)
}
class PPMJsonParser : NSObject {
    var delegate : PPMJsonParserDelegate?
    func ParseMyJson(url: URL, param: AnyObject?, httpMethod: String, contentType : String, mScreen: String, authStr : String?){
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue(contentType, forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.capitalized
        if let authstr = authStr{
            request.addValue(authstr, forHTTPHeaderField: "Authorization")
        }
        if httpMethod == "POST"{
            if let inputstr = param as? String{
                request.addValue(String(describing: inputstr.count), forHTTPHeaderField: "Content-Length")
                let data = inputstr.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion: true)
                request.httpBody = data
            }else if let inputDic = param as? NSDictionary{
                do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: inputDic, options: JSONSerialization.WritingOptions.prettyPrinted)
                }catch {
                    println_debug("some error comes when prepare json request")
                }
            }
        }
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if((error) != nil) {
                println_debug(error!.localizedDescription)
            }else{
                println_debug("\nResponse in RTJSONParser : \(String(describing: response!))")
                
                if let httpStatus = response as? HTTPURLResponse, (httpStatus.statusCode == 200 || httpStatus.statusCode == 201 || httpStatus.statusCode == 302 || httpStatus.statusCode == 404 || httpStatus.statusCode == 400 || httpStatus.statusCode == 500) {
                    
                    do {
                        //Success with json serialization
                        let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                        guard (self.delegate?.ResponseFromServer(withSuccess: true, json: json as AnyObject, screen: mScreen, Error: nil) != nil) else { return }
                    } catch{
                        //Success without json serialization
                        
                        if let data = data, let dataString = String(data: data, encoding: .utf8) {
                            print("Response data string:\n \(dataString)")
                            guard (self.delegate?.ResponseFromServer(withSuccess: true, json: dataString as AnyObject, screen: mScreen, Error: nil) != nil) else { return }
                        }
                        
                        
                    }
                } else {
                    var ErrorString = ""
                    //Failure
                    if let httpStatus = response as? HTTPURLResponse {
                        ErrorString = "Response Code is \(httpStatus.statusCode)".localized
                    }
                    else{
                        ErrorString = "Response Code is not 200".localized
                    }
                    guard (self.delegate?.ResponseFromServer(withSuccess: false, json: nil, screen: mScreen, Error: ErrorString) != nil) else { return }
                }
                
                
                //                if let httpStatus = response as? HTTPURLResponse, (httpStatus.statusCode == 200 || httpStatus.statusCode == 201) {
                //                    do {
                //                        let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                //                        guard (self.delegate?.ResponseFromServer(withSuccess: true, json: json as AnyObject, screen: mScreen, Error: nil) != nil) else { return }
                //                    } catch{
                //                        guard (self.delegate?.ResponseFromServer(withSuccess: true, json: nil, screen: mScreen, Error: nil) != nil) else { return }
                //                    }
                //                }
                //                else{
                //                    var ErrorString = ""
                //                    if let httpStatus = response as? HTTPURLResponse {
                //                        ErrorString = "Response Code is \(httpStatus.statusCode)".localized
                //                    }
                //                    else
                //                    {
                //                        ErrorString = "Response Code is not 200".localized
                //                    }
                //                    guard (self.delegate?.ResponseFromServer(withSuccess: false, json: nil, screen: mScreen, Error: ErrorString) != nil) else { return }
                //                }
            }
        }
        dataTask.resume()
    }
    func convertStringToJson(ReceivedString : String) -> Any{
        let CustomStringData = ReceivedString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion:true)
        do {
            let jsonData = try? JSONSerialization.jsonObject(with: CustomStringData!, options: .allowFragments) as Any
            return jsonData!
        }
    }
}
