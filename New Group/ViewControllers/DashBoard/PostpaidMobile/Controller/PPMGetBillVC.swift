//
//  LLBGetBillVC.swift
//  OK
//
//  Created by Rahul Tyagi on 10/17/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit


protocol PPMGetBillVCDelegate: class {
    func CallReloadAPI()
}
class PPMGetBillVC: OKBaseController,UIGestureRecognizerDelegate {
    enum ControllerStateType{
        case presented, pushed, notDefined
    }
    enum StatusOptions{
        case Paid, Unpaid, PayForView, PaidWithOkDollar
    }
    var favoriteTouple: (areaCode: String,billNo: String)?
    let jsonParserObj = PPMJsonParser()
    var controllerState : ControllerStateType = .notDefined
    var PayForViewParamDict = ["BillID".localized : "","BillAmount".localized : ""]
    var PPMVCDelegate: PPMGetBillVCDelegate?
    var billStatusFromServer : StatusOptions?
    var rowCountArray : [String] = [""]
    var classActionButton : UIButton?
    var KeyboardSize : CGFloat = 0.0
    var activeField : UITextField? = nil
    var textFieldDataArray = ["", "", "", "", "", ""]
    var FooterViewContainer : UIView? = nil
    var FooterText :String? = nil
    var DictToShow = Dictionary<String, Any>()
    var GetBillResponseDict = Dictionary<String, Any>()
    var billDataModelObj : PPMHistoryVCDataModel?
    var LastBillAmountStr : String = ""
    var SelectCityCustomController : SpringView? = nil
    var DarkView :  UIView? = nil
    var City1View : UIView? = nil
    var City2View : UIView? = nil
    var adImageConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableviewOutlet: UITableView!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var adImage: UIImageView!
    
    @IBOutlet weak var bottomGetBillBtnConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightGetBillBtnConstraint: NSLayoutConstraint!
    
    @IBOutlet var AFFbtn: UIButton!{
        didSet{
            AFFbtn.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
            AFFbtn.setTitle(AFFbtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var AFCbtn: UIButton!{
        didSet{
            AFCbtn.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
            AFCbtn.setTitle(AFCbtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var paybtn: UIButton!{
        didSet{
            paybtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            paybtn.setTitle(paybtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var getBillBtn: UIButton!{
        didSet{
            getBillBtn.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
            getBillBtn.setTitle(getBillBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialView()
        self.title = "Postpaid Bill".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20)!, NSAttributedString.Key.foregroundColor: UIColor.white]
        self.automaticallyAdjustsScrollViewInsets = false
        jsonParserObj.delegate = self
        //        adImageConstraint = adImage.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        //        adImageConstraint.isActive = true
        bottomGetBillBtnConstraint.constant = 0
        self.view.layoutIfNeeded()
        subscribeToShowKeyboardNotifications()
    }
    override func viewWillAppear(_ animated: Bool){
        //        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white ,NSAttributedStringKey.font: UIFont(name: appFont, size: 20)!]
        bottomGetBillBtnConstraint.constant = 0
        self.title = "Postpaid Bill".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20)!, NSAttributedString.Key.foregroundColor: UIColor.white]
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        appDel.floatingButtonControl?.window.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden(notification:)) , name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillChangeHeight(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        self.loadAdvertisement()
        if userDef.bool(forKey: "makeAnotherPayment"){
            userDef.set(false, forKey: "makeAnotherPayment")
            userDef.synchronize()
            let customView = UIView()
            customView.backgroundColor = .lightGray
            self.tableviewOutlet.tableFooterView = customView
            self.checkAnotherNumber()
        }
    }
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            //            self.adImageConstraint.constant = 0 - keyboardHeight
            self.bottomGetBillBtnConstraint.constant = keyboardHeight
            if self.getBillBtn.isHidden {
                self.heightGetBillBtnConstraint.constant = 0
            } else {
                self.heightGetBillBtnConstraint.constant = 50
            }
        })
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        //        self.adImageConstraint.constant = 0
        self.bottomGetBillBtnConstraint.constant = 0
        if self.getBillBtn.isHidden {
            self.heightGetBillBtnConstraint.constant = 0
        } else {
            self.heightGetBillBtnConstraint.constant = 50
        }
    }
    
    func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    func loadAdvertisement() {
        let randomIndex = Int(arc4random_uniform(UInt32(advImagesUrl.AdImageData.count)))
        if let urlString = advImagesUrl.AdImageData[safe: randomIndex] {
            DispatchQueue.main.async {
                self.adImage.contentMode = .scaleToFill
                self.adImage.image = UIImage(data: urlString)
                //                self.adImage.setImage(url: URL.init(string: urlString), contentMode: UIView.ContentMode.scaleAspectFit, placeholderImage: nil)
            }
            
        }
    }
    override func viewWillDisappear(_ animated: Bool){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification,object : nil)
        self.view.endEditing(true)
    }
}
extension PPMGetBillVC : FavoriteSelectionDelegate {
    func selectedFavoriteObject(obj: FavModel) {
        self.parentView.isHidden = true
        
        if obj.favDeleteNum.length <= 9{
            self.LastBillAmountStr = ""
            self.rowCountArray.removeAll()
            self.textFieldDataArray.removeAll()
            self.rowCountArray.append("")
            self.rowCountArray.append("")
            self.tableviewOutlet.tableFooterView = UIView()
            self.textFieldDataArray.append("")
            self.textFieldDataArray.append((""))
            self.textFieldDataArray[0] = obj.favDeleteNum.replacingOccurrences(of: "-", with: "")
            self.textFieldDataArray[1] = obj.favDeleteNum.replacingOccurrences(of: "-", with: "")
            self.getBillBtn.isHidden = false
            self.heightGetBillBtnConstraint.constant = 50
            self.getBillBtn.setTitle("Get Bill".localized, for: .normal)
            tableviewOutlet.reloadData()
        }
        
        println_debug(obj)
    }
}
extension PPMGetBillVC {
    func setupInitialView(){
        //        classActionButton = UIButton.init(frame: CGRect(x: 0, y: self.view.frame.height - KeyboardSize - 50 , width: self.view.frame.width, height: 54))
        //        classActionButton?.setTitle("Get Bill".localized, for: .normal)
        //        classActionButton?.titleLabel?.font = UIFont.init(name: "Zawgyi-One", size: 17)
        //        classActionButton?.backgroundColor = UIColor.init(red: 246/255.0, green: 198/255.0, blue: 0, alpha: 1)
        //        classActionButton?.addTarget(self, action: #selector(self.classActionButtonClicked(sender:)), for: .touchUpInside)
        //        self.view.addSubview(classActionButton!)
        //        self.view.bringSubview(toFront: classActionButton!)
        if isObjectNotNil(object: self.billDataModelObj){
            self.initialAddtext()
        }
        else if favoriteTouple != nil{
            self.rowCountArray.append("")
            //        classActionButton?.isHidden = false
            getBillBtn.isHidden = false
            self.textFieldDataArray[0] = favoriteTouple!.areaCode
            self.textFieldDataArray[1] = favoriteTouple!.billNo
            // self.GetBillBtnAction()
        }
        else {
            //            classActionButton?.isHidden = true
            getBillBtn.isHidden = true
        }
        FooterViewContainer = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100))
        let customView = UIView()
        customView.backgroundColor = .lightGray
        self.tableviewOutlet.tableFooterView = customView
        self.view.addSubview(parentView)
    }
    func initialAddtext(){
        self.rowCountArray.append("")
        //        classActionButton?.isHidden = false
        getBillBtn.isHidden = false
        self.textFieldDataArray[0] = (self.billDataModelObj?.PhoneNumber)!
        self.textFieldDataArray[1] = (self.billDataModelObj?.PhoneNumber)!
        self.GetBillBtnAction()
    }
    
    //    @objc func keyboardWasShown(notification: NSNotification){
    //        let info : NSDictionary = notification.userInfo! as NSDictionary
    //        let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
    //        self.KeyboardSize = (keyboardSize?.height)!
    //        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height + 45, 0.0)
    //        self.tableviewOutlet.contentInset = contentInsets
    //        self.tableviewOutlet.scrollIndicatorInsets = contentInsets
    //        var aRect : CGRect = self.view.frame
    //        aRect.size.height -= keyboardSize!.height
    //        if activeField != nil{
    //            if (!aRect.contains(activeField!.frame.origin)){
    //                self.tableviewOutlet.scrollRectToVisible(activeField!.frame, animated: true)
    //            }
    //        }
    //    }
    //    @objc func keyboardWillChangeHeight(notification: NSNotification){
    //        let info : NSDictionary = notification.userInfo! as NSDictionary
    //        let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
    //        self.KeyboardSize = (keyboardSize?.height)!
    //        println_debug("Keyboard changed \(KeyboardSize,keyboardSize?.height))")
    ////        classActionButton?.frame.origin.y = self.view.frame.height - KeyboardSize - 54
    //        bottomGetBillBtnConstraint.constant = KeyboardSize + 54
    //        self.view.layoutIfNeeded()
    //    }
    //    @objc func keyboardWillBeHidden(notification: NSNotification){
    //        let info : NSDictionary = notification.userInfo! as NSDictionary
    //        let _ = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
    //        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
    //        self.tableviewOutlet.contentInset = contentInsets
    //        self.tableviewOutlet.scrollIndicatorInsets = contentInsets
    ////        classActionButton?.frame.origin.y = self.view.frame.height - 54
    //        bottomGetBillBtnConstraint.constant = 0
    //
    //    }
    //    @objc func classActionButtonClicked(sender : UIButton){
    //        self.view.endEditing(true)
    //        let ButtonTittle = sender.titleLabel?.text!
    //        switch ButtonTittle!{
    //        case "Get Bill".localized : GetBillBtnAction()
    //        case "Submit".localized : submitBtnAction()
    //        case "CHECK ANOTHER NUMBER".localized : checkAnotherNumber()
    //        default: println_debug("Exception Occurred".localized)
    //        }
    //    }
    
    @IBAction func getBillAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let ButtonTittle = sender.titleLabel?.text!
        switch ButtonTittle!{
        case "Get Bill".localized : GetBillBtnAction()
        case "Submit".localized : submitBtnAction()
        case "CHECK ANOTHER NUMBER".localized : checkAnotherNumber()
        default: println_debug("Exception Occurred".localized)
        }
    }
    
    
    @IBAction func showParent(_ sender: UIButton){
         
         self.view.endEditing(true)
         self.parentView.isHidden = false
         
         self.parentView.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
         
         UIView.animate(withDuration: 0.7, animations: {() -> Void in
             
             self.parentView.transform = CGAffineTransform(scaleX: 1,y: 1)
             
         })
         
     }
    
    
    @IBAction func showParentView(_ sender: UIButton){
        self.view.endEditing(true)
        self.parentView.isHidden = true
        let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
        self.present(nav, animated: true, completion: nil)
        
    }
    @IBAction func FavAccess(_ sender: UIButton) {
//        let vc = self.openFavoriteFromNavigation(self, withFavorite: .topup)
//        self.present(vc, animated: true, completion: nil)
        
        let vc = UIStoryboard.init(name: "Favorite", bundle: Bundle.main).instantiateViewController(withIdentifier: "favoriteNav")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func contactAccess(_ sender: UIButton){
        self.parentView.isHidden = true
        let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
        self.present(nav, animated: true, completion: nil)
    }
    @objc func GetBillBtnAction(){
        requestTokenFromServer(TokenForAPIName: "GetBillToken")
    }
    @objc func submitBtnAction(){
        //        if NSString(string: UserLogin.shared.walletBal).floatValue < NSString(string: (self.billDataModelObj?.LastBillAmount)!).floatValue  {
        if NSString(string: UserLogin.shared.walletBal).floatValue < NSString(string: LastBillAmountStr).floatValue  {
            alertViewObj.wrapAlert(title: "", body: "Your account have insufficient balance. Please recharge OK$ Wallet money at nearest OK$ Service Counter.".localized, img: #imageLiteral(resourceName: "dashboard_postpaid_mobile"))
            alertViewObj.addAction(title: "Add Money".localized, style: AlertStyle.target, action: {
                DispatchQueue.main.async {
                    if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                        addWithdrawView.modalPresentationStyle = .fullScreen
                        self.navigationController?.present(addWithdrawView, animated: true, completion: nil)
                    }
                }
            })
            alertViewObj.showAlert(controller: self)
        }
            //        }
        else{
            let confirmationVCObj = PPMStrings.FileNames.PPMStoryBoardObj.instantiateViewController(withIdentifier: "PPMConfirmationVC") as! PPMConfirmationVC
            confirmationVCObj.valueArray = [textFieldDataArray[0],textFieldDataArray[1],textFieldDataArray[2],    textFieldDataArray[3],textFieldDataArray[5]]
            confirmationVCObj.DictToSend = self.GetBillResponseDict
            self.navigationController?.pushViewController(confirmationVCObj, animated: true)
        }
    }
    @objc func checkAnotherNumber(){
        self.LastBillAmountStr = ""
        rowCountArray.removeAll()
        rowCountArray.append("")
        textFieldDataArray.removeAll()
        textFieldDataArray = ["", "", "", "", "", ""]
        //        classActionButton?.isHidden = true
        //        classActionButton?.setTitle("Get Bill".localized, for: .normal)
        getBillBtn.isHidden = true
        self.getBillBtn.setTitle("Get Bill".localized, for: .normal)
        self.tableviewOutlet.tableFooterView = UIView()
        self.tableviewOutlet.reloadData()
    }
    
    @IBAction func backBtnAction(_ sender: Any){
        
        if controllerState == .presented{
            self.dismiss(animated: true, completion: {self.PPMVCDelegate?.CallReloadAPI()})
        }
        else if controllerState == .pushed {
            self.navigationController?.popViewController(animated: true)
        }
        else{
            self.dismiss(animated: true, completion: {self.PPMVCDelegate?.CallReloadAPI()})
        }
    }
    func ChangeCityAction(){
        DarkView = UIView.init()
        DarkView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        DarkView?.backgroundColor = .black
        DarkView?.alpha = 0.85
        self.view.addSubview(DarkView!)
        SelectCityCustomController = SpringView.init()
        if(self.view.frame.size.height) < 600{
            SelectCityCustomController?.frame =  CGRect(x: 0, y: self.view.frame.size.height * 0.40, width: self.view.frame.size.width, height: self.view.frame.size.height * 0.60)
        }
        else{
            SelectCityCustomController?.frame = CGRect(x: 0, y: self.view.frame.size.height * 0.45, width: self.view.frame.size.width, height: self.view.frame.size.height * 0.70)
        }
        SelectCityCustomController?.backgroundColor = .white
        self.view.addSubview(SelectCityCustomController!)
        SelectCityCustomController?.animation = "slideUp"
        SelectCityCustomController?.curve = "easeInOut"
        SelectCityCustomController?.force = 2.0
        SelectCityCustomController?.duration = 1.0
        SelectCityCustomController?.animate()
        let offset:CGFloat =  SelectCityCustomController!.frame.width/0.6
        let bounds: CGRect = SelectCityCustomController!.bounds
        let rectBounds: CGRect = CGRect(x: bounds.origin.x, y: bounds.origin.y + bounds.size.height/2, width: bounds.size.width, height: bounds.size.height / 2)
        let rectPath: UIBezierPath = UIBezierPath(rect: rectBounds)
        let ovalBounds: CGRect = CGRect(x: bounds.origin.x - offset / 2, y: bounds.origin.y, width: bounds.size.width + offset, height: bounds.size.height)
        let ovalPath: UIBezierPath = UIBezierPath.init(ovalIn: ovalBounds)
        rectPath.append(ovalPath)
        let maskLayer: CAShapeLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = rectPath.cgPath
        SelectCityCustomController?.layer.mask = maskLayer
        let SelectCityLabel = UILabel.init(frame: CGRect(x: self.view.frame.size.width/2 - 100, y: 25, width: 200, height: 50))
        SelectCityLabel.text = "Select City".localized
        SelectCityLabel.textAlignment = .center
        SelectCityLabel.textColor = .black
        SelectCityLabel.font = UIFont.systemFont(ofSize: 25, weight: UIFont.Weight.semibold)
        SelectCityCustomController?.addSubview(SelectCityLabel)
        City1View = UIView.init(frame: CGRect(x: 0, y: (SelectCityLabel.frame.origin.y) + 70, width: self.view.frame.width , height: 50))
        SelectCityCustomController?.addSubview(City1View!)
        let city1 = UILabel.init(frame: CGRect(x: 25, y: 10, width: 350, height: 30))
        city1.text = "Yangon                                          (01)".localized
        city1.textColor = .black
        city1.font = UIFont.systemFont(ofSize: 20)
        City1View?.addSubview(city1)
        let BlackLine = UIView.init(frame: CGRect(x: 25, y: (city1.frame.origin.y) + 40, width: self.view.frame.width - 25, height: 0.5))
        BlackLine.backgroundColor = .gray
        City1View?.addSubview(BlackLine)
        City2View = UIView.init(frame: CGRect(x: 0, y: (City1View?.frame.origin.y)! + 80, width: self.view.frame.width , height: 50))
        SelectCityCustomController?.addSubview(City2View!)
        let city2 = UILabel.init(frame: CGRect(x: 25, y: 10, width: 350, height: 30))
        city2.text = "Mandalay                                         (02)".localized
        city2.textColor = .black
        city2.font = UIFont.systemFont(ofSize: 20)
        City2View?.addSubview(city2)
        let BlackLine2 = UIView.init(frame: CGRect(x: 25, y: (city2.frame.origin.y) + 40, width: self.view.frame.width - 25, height: 0.5))
        BlackLine2.backgroundColor = .gray
        City2View?.addSubview(BlackLine2)
        let CancelBtn = UIButton.init(frame: CGRect(x: 25, y: (City2View?.frame.origin.y)! + 120 , width: self.view.frame.size.width - 50, height: 50))
        CancelBtn.setTitle("CANCEL".localized, for: .normal)
        CancelBtn.setTitleColor(.white , for: .normal)
        CancelBtn.addTarget(self, action: #selector(self.HideSelectCityCustomController), for: .touchUpInside)
        CancelBtn.backgroundColor = PPMStrings.yellowAppColor
        SelectCityCustomController?.addSubview(CancelBtn)
        CancelBtn.layer.cornerRadius = 17
        let  SelectCityCustomControllerTap = UITapGestureRecognizer(target: self, action: #selector(self.HideSelectCityCustomController))
        SelectCityCustomControllerTap.delegate = self as UIGestureRecognizerDelegate
        DarkView?.addGestureRecognizer(SelectCityCustomControllerTap)
        let city1Gesture = UITapGestureRecognizer(target: self, action: #selector(self.city1Tapped(sender:)))
        city1Gesture.delegate = self as UIGestureRecognizerDelegate
        City1View?.addGestureRecognizer(city1Gesture)
        let city2Gesture = UITapGestureRecognizer(target: self, action: #selector(self.city2Tapped(sender:)))
        city2Gesture.delegate = self as UIGestureRecognizerDelegate
        City2View?.addGestureRecognizer(city2Gesture)
        let PanGesture = UIPanGestureRecognizer(target: self, action: #selector(self.wasDragged(gestureRecognizer:)))
        SelectCityCustomController?.addGestureRecognizer(PanGesture)
    }
    @objc func city1Tapped(sender : UITapGestureRecognizer) {
        println_debug("Call was in City1 Tapped")
        rowCountArray.removeAll()
        textFieldDataArray.removeAll()
        //        classActionButton?.isHidden = true
        getBillBtn.isHidden = true
        rowCountArray.append("")
        rowCountArray.append("")
        self.tableviewOutlet.tableFooterView = UIView()
        //        textFieldDataArray[0] = "Yangon"
        //        textFieldDataArray[1] = "01"
        textFieldDataArray.append("Yangon".localized)
        textFieldDataArray.append("01".localized)
        //      if rowCountArray.count == 1{self.rowCountArray.append("")}
        tableviewOutlet.reloadData()
        HideSelectCityCustomController()
    }
    @objc func city2Tapped(sender : UITapGestureRecognizer) {
        println_debug("Call was in City2 Tapped")
        rowCountArray.removeAll()
        textFieldDataArray.removeAll()
        //        classActionButton?.isHidden = true
        getBillBtn.isHidden = true
        rowCountArray.append("")
        rowCountArray.append("")
        self.tableviewOutlet.tableFooterView = UIView()
        //        textFieldDataArray[0] = "Mandalay"
        //        textFieldDataArray[1] = "02"
        //        if rowCountArray.count == 1{self.rowCountArray.append("")}
        textFieldDataArray.append("Mandalay".localized)
        textFieldDataArray.append("02".localized)
        tableviewOutlet.reloadData()
        HideSelectCityCustomController()
    }
    @objc func HideSelectCityCustomController(){
        UIView.animate(withDuration: 0.5, animations: {
            self.SelectCityCustomController?.frame.origin.y = self.view.frame.size.height
            self.DarkView?.alpha = 0.30
        }) { (Bool) in
            self.DarkView?.removeFromSuperview()
            self.SelectCityCustomController?.removeFromSuperview()
        }
    }
    @objc func wasDragged(gestureRecognizer: UIPanGestureRecognizer){
        if gestureRecognizer.state == UIGestureRecognizer.State.began || gestureRecognizer.state == UIGestureRecognizer.State.changed {
            let translation = gestureRecognizer.translation(in: self.view)
            println_debug(translation)
            gestureRecognizer.view!.center = CGPoint.init(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
            gestureRecognizer.setTranslation(CGPoint.init(x: 0, y: 0), in: self.view)
            if ((SelectCityCustomController?.frame.origin.y)! > self.view.frame.size.height * 0.85){
                UIView.animate(withDuration: 0.2, animations: {
                    self.SelectCityCustomController?.frame.origin.y = self.view.frame.size.height
                    self.DarkView?.alpha = 0.30
                }) { (Bool) in
                    self.DarkView?.removeFromSuperview()
                    self.SelectCityCustomController?.removeFromSuperview()
                }
            }
            if(self.view.frame.size.height) < 600{
                if (SelectCityCustomController?.frame.origin.y)! < self.view.frame.size.height * 0.40{
                    SelectCityCustomController?.frame =  CGRect(x: 0, y: self.view.frame.size.height * 0.40, width: self.view.frame.size.width, height: self.view.frame.size.height * 0.60)
                }
            }
            else {
                if (SelectCityCustomController?.frame.origin.y)! < self.view.frame.size.height * 0.45
                {
                    SelectCityCustomController?.frame = CGRect(x: 0, y: self.view.frame.size.height * 0.45, width: self.view.frame.size.width, height: self.view.frame.size.height * 0.55)
                }
            }
            if(translation.y) > 0 {
                DarkView?.alpha = (DarkView?.alpha)! - 0.005
            }
            else{   if (DarkView?.alpha)! > 0.85 { }
            else { DarkView?.alpha = (DarkView?.alpha)! + 0.009}
            }
        }
        if (gestureRecognizer.state == UIGestureRecognizer.State.ended){
            if ((SelectCityCustomController?.frame.origin.y)! < self.view.frame.size.height * 0.75){
                if(self.view.frame.size.height) < 600{
                    SelectCityCustomController?.frame =  CGRect(x: 0, y: self.view.frame.size.height * 0.40, width: self.view.frame.size.width, height: self.view.frame.size.height * 0.60)
                }
                else{
                    SelectCityCustomController?.frame = CGRect(x: 0, y: self.view.frame.size.height * 0.45, width: self.view.frame.size.width, height: self.view.frame.size.height * 0.55)
                }
                DarkView?.alpha = 0.85
            }
            else{
                UIView.animate(withDuration: 0.2, animations: {
                    self.SelectCityCustomController?.frame.origin.y = self.view.frame.size.height
                    self.DarkView?.alpha = 0.30
                }) { (Bool) in
                    self.DarkView?.removeFromSuperview()
                    self.SelectCityCustomController?.removeFromSuperview()
                }
            }
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        self.parentView.isHidden = true
    }
    func isObjectNotNil(object:PPMHistoryVCDataModel!) -> Bool{
        if let _:PPMHistoryVCDataModel = object{
            return true
        }
        return false
    }
}
extension PPMGetBillVC: UITableViewDelegate, UITableViewDataSource, PPMGetBillTableCellDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return rowCountArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
      
        let customCell : PPMGetBillTableCell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! PPMGetBillTableCell
        customCell.delegate = self
        customCell.adDelegate = self
        customCell.parentClassVC = self
        if isObjectNotNil(object: self.billDataModelObj) {
            customCell.setCellData(index: indexPath, text: textFieldDataArray[indexPath.row], billDataModelObject: self.billDataModelObj!)
        }
        else{
            customCell.setCellData(index: indexPath, text: textFieldDataArray[indexPath.row])
        }
        println_debug("Text data \(textFieldDataArray[indexPath.row])")
        customCell.btnCross.tag = indexPath.row
        customCell.btnCross.addTarget(self, action: #selector(PPMGetBillVC.deleteNumber(sender:)), for: .touchUpInside)
        return customCell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if LastBillAmountStr != "" {
            if indexPath.row == 1 {
                return 0
            }
            else {
                return 70
            }
        }
        else {
            return 70
        }
    }
    @objc func deleteNumber (sender: UIButton) {
        if sender.tag == 0{
            DispatchQueue.main.async {
                //                self.btnCross.isHidden = true
                self.rowCountArray.removeAll()
                self.rowCountArray.append("")
                self.textFieldDataArray.removeAll()
                self.textFieldDataArray = ["", "", "", "", "", ""]
                self.textFieldDataArray[0] = "09"
                //        classActionButton?.isHidden = true
                self.getBillBtn.isHidden = true
                
                //                self.classActionButton?.setTitle("Get Bill", for: .normal)
                self.getBillBtn.setTitle("Get Bill".localized, for: .normal)
                self.tableviewOutlet.tableFooterView = UIView()
                self.tableviewOutlet.reloadData()
                self.LastBillAmountStr = ""
            }
        }
        else if sender.tag == 1{
            self.textFieldDataArray[1] = "09"
            //        classActionButton?.isHidden = true
            self.getBillBtn.isHidden = true
            //            self.classActionButton?.setTitle("Get Bill", for: .normal)
            self.getBillBtn.setTitle("Get Bill".localized, for: .normal)
            self.tableviewOutlet.tableFooterView = UIView()
            self.tableviewOutlet.reloadData()
        }
        //        }
    }
    func passData(cell: PPMGetBillTableCell){ }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        println_debug("Call was in did Select row")
    }
}
extension PPMGetBillVC : ContactPickerDelegate {
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]) {
        for contact in contacts {
            decodeContact(contact: contact, isMulti: true)
        }
    }
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker){
        println_debug(contact)
        decodeContact(contact: contact, isMulti: false)
    }
    func decodeContact(contact: ContactPicker, isMulti: Bool){
        subscribeToShowKeyboardNotifications()
        println_debug(contact.firstName)
        println_debug(contact.phoneNumbers[0].phoneNumber)
        //        let cell = self.tableviewOutlet.cellForRow(at: IndexPath.init(row: 0, section: 0)) as? LLBGetBillTableCell
        //        cell?.textFieldOutlet.text = contact.phoneNumbers[0].phoneNumber
        var postPaidNum = contact.phoneNumbers[0].phoneNumber
        
        if postPaidNum.hasPrefix("+9595") || postPaidNum.hasPrefix("095") {
            
            if postPaidNum.hasPrefix("+9595") {
            postPaidNum = postPaidNum.replacingOccurrences(of: "+95", with: "0")
            }
            
            if postPaidNum.length <= 9{
                self.LastBillAmountStr = ""
                self.rowCountArray.removeAll()
                self.textFieldDataArray.removeAll()
                self.rowCountArray.append("")
                self.rowCountArray.append("")
                self.tableviewOutlet.tableFooterView = UIView()
                self.textFieldDataArray.append("")
                self.textFieldDataArray.append((""))
                self.textFieldDataArray[0] = postPaidNum //contact.phoneNumbers[0].phoneNumber.localized
                self.textFieldDataArray[1] = postPaidNum //contact.phoneNumbers[0].phoneNumber.localized
                //            self.classActionButton?.isHidden = false
                self.getBillBtn.isHidden = false
                self.heightGetBillBtnConstraint.constant = 50
                
                //            self.classActionButton?.setTitle("Get Bill".localized, for: .normal)
                self.getBillBtn.setTitle("Get Bill".localized, for: .normal)
                //        self.GetBillBtnAction()
                tableviewOutlet.reloadData()
                let phoneNumberCount = contact.phoneNumbers.count
                if phoneNumberCount == 1 {
                    _ = "\(contact.phoneNumbers[0].phoneNumber)"
                    println_debug(contact.firstName)
                    println_debug(contact.phoneNumbers[0].phoneNumber)
                }
                else if phoneNumberCount > 1 {
                    _ = "\(contact.phoneNumbers[0].phoneNumber) and \(contact.phoneNumbers.count-1) more"
                }
                else {
                    _ = ContactGlobalConstants.Strings.phoneNumberNotAvaialable
                }
            }

        }
        
        
        else{
            DispatchQueue.main.async{
                alertViewObj.wrapAlert(title: "", body: PPMStrings.ErrorMessages.incorrectMobileNumber, img:#imageLiteral(resourceName: "dashboard_my_number"))
                alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                    self.rowCountArray.removeAll()
                    self.rowCountArray.append("")
                    self.textFieldDataArray.removeAll()
                    self.textFieldDataArray = ["", "    ", "", "", "", ""]
                    self.textFieldDataArray[0] = "09"
                    //                    self.classActionButton?.isHidden = true
                    self.getBillBtn.isHidden = true
                    //                    self.classActionButton?.setTitle("Get Bill", for: .normal)
                    self.getBillBtn.setTitle("Get Bill".localized, for: .normal)
                    self.tableviewOutlet.tableFooterView = UIView()
                    self.tableviewOutlet.reloadData()
                })
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    func createMultiContactView(contact: ContactPicker) {}
}
extension PPMGetBillVC {
    func addNewRowInSection(rows:NSMutableArray) -> NSMutableArray{
        if (rows.count == 0) || (rows.count == 1){
            rows.insert("09".localized, at:rows.count)
        }
        else{
            rows.insert("Enter or Select Amount".localized, at: rows.count)
        }
        return rows
    }
}
extension PPMGetBillVC: PPMJsonParserDelegate{
    func requestTokenFromServer(TokenForAPIName : String){
        progressViewObj.showProgressView()
        println_debug(" \nRequesting Token From Server")
        let hashValue = PPMStrings.aKey.hmac_SHA1(key: PPMStrings.sKey)
        let inputValue = "password=\(hashValue)&grant_type=password"
        jsonParserObj.ParseMyJson(url: URL.init(string: PPMStrings.kTokenAPIURL)! , param: inputValue as AnyObject, httpMethod: PPMStrings.kMethod_Post, contentType: PPMStrings.kContentType_urlencoded, mScreen: TokenForAPIName, authStr: nil)
    }
    func ResponseFromServer(withSuccess Success: Bool, json: AnyObject?, screen: String, Error: String?){
        println_debug("Response Received : \(Success) ,\(screen), \(String(describing: Error)), \(String(describing: json)) \n\n\n")
        
        
        
          
             
         
        
        progressViewObj.removeProgressView()
        if !Success{
            DispatchQueue.main.async{
                alertViewObj.wrapAlert(title: nil, body: PPMStrings.ErrorMessages.invalidNumber, img:#imageLiteral(resourceName: "dashboard_postpaid_mobile"))
                alertViewObj.addAction(title: "OK".localized, style: .target) {
                    self.rowCountArray.removeAll()
                    self.rowCountArray.append("")
                    self.textFieldDataArray.removeAll()
                    self.textFieldDataArray = ["", "    ", "", "", "", ""]
                    self.textFieldDataArray[0] = "09"
                    //                    self.classActionButton?.isHidden = true
                    self.getBillBtn.isHidden = true
                    //                    self.classActionButton?.setTitle("Get Bill", for: .normal)
                    self.getBillBtn.setTitle("Get Bill".localized, for: .normal)
                    self.tableviewOutlet.tableFooterView = UIView()
                    self.tableviewOutlet.reloadData()
                }
                alertViewObj.showAlert(controller: self)
            }
        }
        else{
            if let jsonArray = json as? Dictionary<String, Any> {
                switch screen{
                case "GetBillToken".localized : handleTokenResponse(TokenSuccess: Success, TokenForAPI: "GetBillToken", Tokenjson: json,  TokenError: Error)
                    break
                case "PayForViewToken".localized : handleTokenResponse(TokenSuccess: Success, TokenForAPI: "PayForViewToken", Tokenjson: json, TokenError: Error)
                    break
                case "PPMGetBillAPI".localized :  println_debug("ResponseFromServer:- ")
                println_debug(jsonArray)
                handleGetBillAPIResponse(jsonResponse : jsonArray)
                    break
                case "PayForViewAPI".localized : handlePayForViewAPIResponse(jsonResponse : jsonArray)
                    break
                default:
                    break
                }
            }
            else{
                
                DispatchQueue.main.async{
                    alertViewObj.wrapAlert(title: "", body: json as! String, img: #imageLiteral(resourceName: "landlineImg"))
                    alertViewObj.addAction(title: "Done".localized, style: .target, action:{
                        self.checkAnotherNumber()
                    })
                    alertViewObj.showAlert(controller: self)
                }
                
                //                DispatchQueue.main.async {
                
                //                    self.requestTokenFromServer(TokenForAPIName: "GetBillToken")
                
                
                //                    alertViewObj.wrapAlert(title: PPMStrings.ErrorMessages.GenericAlertTitle, body: PPMStrings.ErrorMessages.TryAgainErrorMsg, img: #imageLiteral(resourceName: "alert-icon"))
                //                    alertViewObj.addAction(title: "OK".localized, style: .target){}
                //                    alertViewObj.showAlert(controller: self)
                
                //                }
            }
        }
    }
    func handleTokenResponse(TokenSuccess: Bool, TokenForAPI : String,  Tokenjson: AnyObject?, TokenError: String?){
        println_debug("After Token")
        if TokenSuccess == true{
            let responseDic = Tokenjson as! NSDictionary
            let tokentype = responseDic.value(forKey: "token_type")
            let accesstoken = responseDic.value(forKey: "access_token")
            let authorizationString =  "\(tokentype!) \(accesstoken!)"
            var txtValue : String = self.textFieldDataArray[1]
            println_debug(txtValue)
            if txtValue.hasPrefix("0"){
                txtValue = String(txtValue.suffix(txtValue.length-1))
            }
            println_debug(txtValue)
            switch TokenForAPI{
            case "GetBillToken".localized :
                println_debug("token Received, Requesting Get Bill API")
                let urlString = String.init(format: PPMStrings.kGetBillAPIURL, PPMStrings.okAccountNum, "test", txtValue)
                let getBillUrl = URL.init(string: urlString)
                DispatchQueue.main.async{
                    //                  self.showProgressView()
                    println_debug(getBillUrl!)
                    println_debug(PPMStrings.kMethod_Get)
                    println_debug(PPMStrings.kContentType_Json)
                    println_debug(authorizationString)
                    self.jsonParserObj.ParseMyJson(url: getBillUrl!, param: nil, httpMethod: PPMStrings.kMethod_Get, contentType: PPMStrings.kContentType_Json, mScreen: "PPMGetBillAPI", authStr: authorizationString)
                }
            case "PayForViewToken".localized :
                println_debug("token Received, Requesting Pay For View API")
                let pipeStr = "OkAccountNumber=\(PPMStrings.okAccountNum)|Password=\(UserModel.shared.pass)|Amount=\(PayForViewParamDict["BillAmount"]!)|BillDetailId=\(PayForViewParamDict["BillID"]!)";
                println_debug(pipeStr)
                let hashValueStrForPipeStr = pipeStr.hmac_SHA1(key: PPMStrings.sKey)
                println_debug(hashValueStrForPipeStr)
                
                let jsonText = "{\"OkAccountNumber\":\"\(PPMStrings.okAccountNum)\", \"Password\":\"\(ok_password ?? "")\", \"Amount\":\"\(PayForViewParamDict["BillAmount"]!)\", \"BillDetailId\":\"\(PayForViewParamDict["BillID"]!)\",\"HashValue\":\"\(hashValueStrForPipeStr)\"}"
                println_debug(jsonText)
                let PayForViewURL = URL.init(string: PPMStrings.kPayForViewAPIURL)
                DispatchQueue.main.async{
                    //                  self.showProgressView()
                    self.jsonParserObj.ParseMyJson(url: PayForViewURL!, param: jsonText as AnyObject, httpMethod: PPMStrings.kMethod_Post, contentType: PPMStrings.kContentType_Json, mScreen: "PayForViewAPI", authStr: authorizationString)
                }
            default : break
            }
        }
        else{
            println_debug("token Not Received")
            DispatchQueue.main.async {
                //                self.removeProgressView()
                
                self.requestTokenFromServer(TokenForAPIName: "GetBillToken")
                
//                alertViewObj.wrapAlert(title: PPMStrings.ErrorMessages.GenericAlertTitle, body: PPMStrings.ErrorMessages.TryAgainErrorMsg, img: #imageLiteral(resourceName: "alert-icon"))
//                alertViewObj.addAction(title: "OK".localized, style: .target){}
//                alertViewObj.showAlert(controller: self)
                
                
              
            }
        }
    }
    
    func handleGetBillAPIResponse(jsonResponse : Dictionary<String, Any>?){
        
     
        guard let jsonResponse = jsonResponse else {
            println_debug("jsonResponse is nil")
            return
        }
        let BillPaymentStatus = jsonResponse["BillPaymentStatus"] as? String ?? ""
        let BillDetailDict = (jsonResponse["BillDetail"] as! Dictionary<String, Any>)
        println_debug(BillPaymentStatus)
        self.LastBillAmountStr = String(describing: jsonResponse["TotalBillAmount"]!)
        switch BillPaymentStatus {
        case "Paid","PaidWithOkDollar","PaidWithOk"  :
            DispatchQueue.main.async {
                alertViewObj.wrapAlert(title: "", body: PPMStrings.ErrorMessages.PostPaidErrorMessage, img: #imageLiteral(resourceName: "dashboard_my_number.png"))
                alertViewObj.addAction(title: "OK".localized, style: .target, action:{
                    self.rowCountArray.append("")
                    self.rowCountArray.append("")
                    self.rowCountArray.append("")
                    self.textFieldDataArray.append("")
                    self.textFieldDataArray.append("")
                    self.textFieldDataArray.append("")
                    let BillingMonth = String(describing: BillDetailDict["BillingMonth"]!)
                    let BillingYear = String(describing: BillDetailDict["BillingYear"]!)
                    let dateToPass = BillingMonth + "-" + BillingYear
                    let helperObj = RTHelperClass()
                    let convertedValue = helperObj.formattedDateFromString(dateString: dateToPass, withFormat: "MM-YY", toBeConvertedToFormat: "MMMM YYYY")
                    self.textFieldDataArray[2] = convertedValue!.localized
                    self.textFieldDataArray[3] =  (self.getDigitDisplay(String(describing: jsonResponse["TotalBillAmount"]!)).appending(" MMK")).localized
                    self.textFieldDataArray[4] = (jsonResponse["BillPaymentStatus"] as? String ?? "").localized
                    //                    self.textFieldDataArray.append(convertedValue!)
                    //                    self.textFieldDataArray.append(self.getDigitDisplay(String(describing: jsonResponse["TotalBillAmount"]!)).appending(" MMK"))
                    //                    self.textFieldDataArray.append(jsonResponse["BillPaymentStatus"] as! String ?? "")
                    //                DispatchQueue.main.async {
                    //                    self.classActionButton?.setTitle("CHECK ANOTHER NUMBER".localized, for: .normal)
                    self.getBillBtn.setTitle("CHECK ANOTHER NUMBER".localized, for: .normal)
                    
                    self.tableviewOutlet.reloadData()
                    //                }
                })
                alertViewObj.showAlert(controller: self)
            }
        case "Unpaid".localized : HandleUnpaidStatusFromGetBillAPI(jsonResponse: jsonResponse)
        self.GetBillResponseDict = jsonResponse as [String : AnyObject]
            break
        case "RequiredToPayForView".localized : println_debug("PayForView")
        self.GetBillResponseDict = jsonResponse as [String : AnyObject]
        DispatchQueue.main.async{
            let PayForViewAmount = String(describing: jsonResponse["ViewOnlyAmount"]! )
            let alertString = String.init(format: PPMStrings.ErrorMessages.PayForViewErrorMessage, PayForViewAmount)
            alertViewObj.wrapAlert(title: "", body: alertString, img: #imageLiteral(resourceName: "dashboard_postpaid_mobile"))
            alertViewObj.addAction(title: "OK".localized, style: .target, action:{
                let BillDetailID = String(describing: BillDetailDict["BilDetailId"]!)
                let ViewOnlyAmount = String(describing: jsonResponse["ViewOnlyAmount"]! )
                self.PayForViewParamDict["BillID"] =  BillDetailID
                self.PayForViewParamDict["BillAmount"] = ViewOnlyAmount
                self.requestTokenFromServer(TokenForAPIName: "PayForViewToken")
                //                self.submitBtnAction()
            })
            alertViewObj.addAction(title: "CANCEL".localized, style: .target, action:{
                self.dismiss(animated: true, completion: nil)})
            alertViewObj.showAlert(controller: self)
            }
        //            case "PaidWithOkDollar" : println_debug("Show Check another no Alert")
        default : break
        }
    }
    
    func handlePayForViewAPIResponse(jsonResponse : Dictionary<String, Any>){
        HandleUnpaidStatusFromGetBillAPI(jsonResponse: GetBillResponseDict)
    }
    
    func HandleUnpaidStatusFromGetBillAPI(jsonResponse : Dictionary<String, Any>){
        let helperObj = RTHelperClass()
        let BillDetailDict = (jsonResponse["BillDetail"] as! Dictionary<String, Any>)
        println_debug("Reload Table view for submitting")
        self.rowCountArray.append("")
        self.rowCountArray.append("")
        self.rowCountArray.append("")
        self.rowCountArray.append("")
        self.textFieldDataArray.append("")
        self.textFieldDataArray.append("")
        self.textFieldDataArray.append("")
        self.textFieldDataArray.append("")
        let BillingMonth = String(describing: BillDetailDict["BillingMonth"]!)
        let BillingYear = String(describing: BillDetailDict["BillingYear"]!)
        let dateToPass = BillingMonth + "-" + BillingYear
        let convertedValue = helperObj.formattedDateFromString(dateString: dateToPass, withFormat: "MM-yy", toBeConvertedToFormat: "MMMM YYYY")
        DispatchQueue.main.async {
            self.textFieldDataArray[2] = convertedValue!.localized
            self.textFieldDataArray[3] =  (self.getDigitDisplay(String(describing: jsonResponse["TotalBillAmount"]!)).appending(" MMK")).localized
//            self.textFieldDataArray[4] = (jsonResponse["BillPaymentStatus"] as? String ?? "").localized
            let status = jsonResponse["BillPaymentStatus"] as? String ?? ""
            if status == "Unpaid"{
                self.textFieldDataArray[4] = "Paid With OK$"
            }
            else {
                self.textFieldDataArray[4] = status//jsonResponse["BillPaymentStatus"] as? String ?? ""
            }
            
            self.textFieldDataArray[5] = (String(describing: jsonResponse["ServiceFee"]!).appending(" MMK")).localized
            let dueDateStr = String(describing: BillDetailDict["DueDate"]! )
            let FormattedDate = helperObj.formattedDateFromString(dateString: dueDateStr, withFormat: "yyyy-MM-dd'T'HH:mm:ss", toBeConvertedToFormat: "d MMM, yyyy")
            let dateFormatter = DateFormatter()
            dateFormatter.calendar = Calendar(identifier: .gregorian)
            dateFormatter.dateFormat = "d MMM, yyyy"
            dateFormatter.setLocale()
            let dateToCompare = dateFormatter.date(from: FormattedDate!)
            let currentDateFormatter = DateFormatter()
            currentDateFormatter.calendar = Calendar(identifier: .gregorian)
            currentDateFormatter.dateFormat = "d MMM, yyyy"
            currentDateFormatter.setLocale()
            let currentDateStr = currentDateFormatter.string(from:Date())
            let currentDate = dateFormatter.date(from: currentDateStr)
            println_debug(currentDate!)
            println_debug(dateToCompare!)
            //            self.classActionButton?.setTitle("Submit".localized, for: .normal)
            self.getBillBtn.setTitle("Submit".localized, for: .normal)
            self.tableviewOutlet.reloadData()
            self.FooterText = ("Your Due date is \(FormattedDate!)").localized
            let footerViewLabel = UILabel.init(frame: CGRect(x: 15, y: 5, width: self.view.frame.width - 30, height: 90))
            footerViewLabel.textAlignment = .center
            footerViewLabel.textColor = .red
            footerViewLabel.numberOfLines = 4
            footerViewLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
            footerViewLabel.text = self.FooterText
            self.tableviewOutlet.tableFooterView = nil
            self.FooterViewContainer?.addSubview(footerViewLabel)
            self.tableviewOutlet.beginUpdates()
            self.tableviewOutlet.tableFooterView  = self.FooterViewContainer
            self.tableviewOutlet.endUpdates()
            if currentDate! >= dateToCompare!{
                alertViewObj.wrapAlert(title: "", body: PPMStrings.ErrorMessages.DueDateErrorMessage, img: #imageLiteral(resourceName: "dashboard_postpaid_mobile"))
                alertViewObj.addAction(title: "OK".localized, style: .target, action: {})
                alertViewObj.showAlert(controller: self)
            }
        }
        DictToShow = ["Mobile Number".localized : textFieldDataArray[0].localized,"Confirm Mobile Number".localized : textFieldDataArray[1].localized,"Bill Month".localized : textFieldDataArray[2].localized,"Total Amount".localized : textFieldDataArray[3].localized,"Service Fee".localized : textFieldDataArray[5].localized]
    }
    
}

//Tushar to hide image view

extension PPMGetBillVC: HideAd{
    func hide(hide: Bool){
        if hide{
            adImage.isHidden = true
        }else{
            adImage.isHidden = false
            
        }
        
    }
}
