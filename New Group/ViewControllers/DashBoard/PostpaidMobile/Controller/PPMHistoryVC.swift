//
//  LLBHistoryVC.swift
//  OK
//
//  Created by Rahul Tyagi on 10/17/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class PPMHistoryVC: OKBaseController {
    var AddBtn : UIButton?
    var numberToDelete : String = "" //To Delete object from Model
    var NoResultsLabel : UILabel? = nil
    var HistoryDataModelObj = [PPMHistoryVCDataModel]()
    var searchResults = [PPMHistoryVCDataModel]()
    var customerID: String?
    var number: String?
    @IBOutlet var searchBarOutlet: UISearchBar!
    @IBOutlet weak var searchBarButtonOutlet: UIBarButtonItem!
    @IBOutlet weak var tableviewOutlet: UITableView!
    @IBOutlet weak var navigationItemOutlet: UINavigationItem!
    @IBOutlet var lblRecord: UILabel!{
        didSet
        {
            lblRecord.font = UIFont(name: appFont, size: appFontSize)
            lblRecord.text = lblRecord.text?.localized
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableviewOutlet.estimatedRowHeight = 44.0
        tableviewOutlet.rowHeight = UITableView.automaticDimension
        self.setTitleAttribute()
        self.lblRecord.isHidden = true
        self.helpSupportNavigationEnum = .Postpaid_Mobile
        searchBarOutlet.delegate = self
        self.automaticallyAdjustsScrollViewInsets = false
        setupInitialView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.setTitleAttribute()
        searchResults.removeAll()
        tableviewOutlet.reloadData()
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        appDel.floatingButtonControl?.window.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
    }
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
    }
    func PPMdeleteAPIRequest(customerID : String, PhoneNumber : String){
        progressViewObj.showProgressView()
        println_debug(" \nRequesting Token From Server")
        let LLBHistoryStatusObj = PPMCheckHistoryStatus()
        LLBHistoryStatusObj.getTokenFromServer { (isSuccess, authString, APIError) in
            if isSuccess == true {
                println_debug("token Received, Requesting Delete API")
                let urlString = String.init(format: PPMStrings.kDeleteAPIURL, customerID,PhoneNumber)
                let DeleteURL = URL.init(string: urlString)
                let jsonParserObj = PPMJsonParser()
                jsonParserObj.delegate = self
                jsonParserObj.ParseMyJson(url: DeleteURL!, param: nil, httpMethod: PPMStrings.kMethod_Put, contentType: PPMStrings.kContentType_Json, mScreen: "PPMDeleteAPI", authStr: authString as? String)
            }
            else{
                println_debug("token Not Received")
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    
                    self.PPMdeleteAPIRequest(customerID: self.customerID!, PhoneNumber: self.number!)
                    
                    //                    alertViewObj.wrapAlert(title: nil, body: PPMStrings.ErrorMessages.TryAgainErrorMsg, img: #imageLiteral(resourceName: "alert-icon"))
                    //                    alertViewObj.addAction(title: "OK".localized, style: .target){}
                    //                    alertViewObj.showAlert(controller: self)
                    
                }
            }
        }
    }
    
    func setTitleAttribute() {
        self.title = "Postpaid Bill".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20)!, NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    func responseDataApiCall(){
        DispatchQueue.main.async {
            self.setTitleAttribute()
        }
        
        let PPMHistoryStatusObj = PPMCheckHistoryStatus()
        PPMHistoryStatusObj.requestAPI {(responseStatus, ErrorMessage, historyDataModelRef) in
            DispatchQueue.main.async{
                progressViewObj.removeProgressView()
            }
            switch responseStatus {
            case .showGetBill:  break
            case .showHistory:  DispatchQueue.main.async{self.HistoryDataModelObj = historyDataModelRef!
                self.tableviewOutlet?.reloadData()}
                break
            case .showAlert:
                DispatchQueue.main.async{
                    alertViewObj.wrapAlert(title: nil, body: ErrorMessage!, img:  #imageLiteral(resourceName: "alert-icon"))
                    alertViewObj.addAction(title: "OK", style: .target , action: { })
                    alertViewObj.showAlert(controller: self)
                }
            }
        }
    }
}
extension PPMHistoryVC:PPMGetBillVCDelegate{
    func CallReloadAPI() {
        self.responseDataApiCall()
    }
}
extension PPMHistoryVC{
    func setupInitialView(){
        AddBtn = UIButton.init(frame: CGRect(x: self.view.frame.width - 75, y: self.view.frame.height - 120 , width: 55    , height: 55))
        AddBtn?.setImage(#imageLiteral(resourceName: "AddBtnImg"), for: .normal)
        AddBtn?.addTarget(self, action: #selector(self.AddbtnAction), for: .touchUpInside)
        AddBtn?.backgroundColor = .white
        AddBtn?.layer.cornerRadius =  (AddBtn!.bounds.size.width) * 0.5
        AddBtn?.clipsToBounds = true
        self.view.addSubview(AddBtn!)
        AddBtn?.layer.masksToBounds = false
        AddBtn?.layer.shadowColor = UIColor.black.cgColor
        AddBtn?.layer.shadowOffset = CGSize(width: 0.2, height: 3.0)
        AddBtn?.layer.shadowOpacity = 0.25;
        self.view.bringSubviewToFront(AddBtn!)
        //AddBtn?.isHidden = true
        NoResultsLabel = UILabel.init(frame: CGRect(x: 0, y: self.view.frame.height/2 - 150, width: self.view.frame.width, height: 30))
        NoResultsLabel?.text =  "No Results Found".localized    //appDel.getlocaLizationLanguage("No Results Found!")
        NoResultsLabel?.textColor = .darkGray
        NoResultsLabel?.textAlignment = .center
        NoResultsLabel?.numberOfLines = 2
        self.view.addSubview(NoResultsLabel!)
        NoResultsLabel?.textAlignment = .center
        NoResultsLabel?.font = UIFont.init(name: appFont, size: 22)
        NoResultsLabel?.isHidden = true
        customizeSearchBar()
    }
    func filterArrayObject(objectArray: [PPMHistoryVCDataModel]){
        var demoArry = [PPMHistoryVCDataModel]()
        println_debug(objectArray.count)
        println_debug(HistoryDataModelObj.count as Any)
        println_debug(HistoryDataModelObj as Any)
        var i = 0
        while i < (objectArray.count){
            println_debug(i)
            //println_debug(HistoryDataModelObj![i].PhoneNumber)
            if objectArray[i].PhoneNumber.hasPrefix("1"){ //HistoryDataModelObj?.remove(at: i)
            }
            else{
                println_debug(objectArray[i].PhoneNumber)
                demoArry.append(objectArray[i])
            }
            i = i + 1
        }
        HistoryDataModelObj.removeAll()
        HistoryDataModelObj = demoArry
        //println_debug(HistoryDataModelObj)
    }
    @objc func AddbtnAction(){
        let GetBillObj = PPMStrings.FileNames.PPMStoryBoardObj.instantiateViewController(withIdentifier: "PPMGetBillVCSID") as? PPMGetBillVC
        GetBillObj?.PPMVCDelegate = self
        GetBillObj?.controllerState = .presented
        let navController = PPMNavController.init(rootViewController: GetBillObj!)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: nil)
    }
    func didTapPayButton(cell: PPMHistoryTableCell){
        println_debug(cell.indexPath.row)
        self.clearSearchBar()
        let GetBillVCObj = PPMStrings.FileNames.PPMStoryBoardObj.instantiateViewController(withIdentifier: "PPMGetBillVCSID") as! PPMGetBillVC
        GetBillVCObj.controllerState = .pushed
        if searchResults.count != 0{
            GetBillVCObj.billDataModelObj = searchResults[cell.indexPath.row]
        }
        else {
            GetBillVCObj.billDataModelObj = HistoryDataModelObj[cell.indexPath.row]
        }
        println_debug(GetBillVCObj.billDataModelObj)
        self.navigationController?.pushViewController(GetBillVCObj, animated: true)
    }
    func didTapDeleteButton(cell: PPMHistoryTableCell){
        println_debug("Delete Button Tapped : \n \(HistoryDataModelObj[cell.indexPath.row])")
        alertViewObj.wrapAlert(title: nil, body: "Do you want to Delete this record?".localized, img: #imageLiteral(resourceName: "landlineImg"))
        alertViewObj.addAction(title: "NO".localized, style: .cancel) {}
        alertViewObj.addAction(title: "YES".localized, style: .target) {
            self.customerID = (self.HistoryDataModelObj[cell.indexPath.row]).customerID
            self.number = (self.HistoryDataModelObj[cell.indexPath.row]).PhoneNumber
            self.numberToDelete = self.number!
            self.PPMdeleteAPIRequest(customerID: self.customerID!, PhoneNumber: self.number!)
        }
        alertViewObj.showAlert(controller: self)
    }
    @IBAction func backBtnAction(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }
}
extension PPMHistoryVC: UISearchControllerDelegate, UISearchBarDelegate{
    func customizeSearchBar(){
        
        searchBarOutlet.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
        searchBarOutlet.showsCancelButton = true
        searchBarOutlet.delegate = self
        searchBarOutlet.tintColor = UIColor.white
        searchBarOutlet.placeholder = "Search".localized
        if let searchTextField = searchBarOutlet.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 16) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                searchTextField.font = myFont
                searchTextField.font = myFont
                searchTextField.keyboardType = .numberPad
            }
        }
        let view = self.searchBarOutlet.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = UIColor.gray
            }
        }
        
        
        //        searchBarOutlet.isHidden = true
        
        //        self.searchBarOutlet.returnKeyType = .done
        //        self.searchBarOutlet.setValue(UIColor.lightText, forKeyPath: "_searchField._placeholderLabel.textColor")
        //        let searchBarTextField : UITextField = self.searchBarOutlet.value(forKey: "_searchField") as! UITextField
        //        searchBarTextField.textColor = UIColor.black
        //        let leftImageView : UIImageView = searchBarTextField.leftView as! UIImageView
        //        leftImageView.tintColor = UIColor.lightText
        //        leftImageView.image? = (leftImageView.image?.withRenderingMode(.alwaysTemplate))!
        //        let clearButton : UIButton = searchBarTextField.value(forKey: "clearButton") as! UIButton
        //        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        //        clearButton.tintColor = UIColor.lightText
        
        
        
        
        
        //        searchBarOutlet.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
        //        searchBarOutlet.showsCancelButton = true
        //        searchBarOutlet.delegate = self
        //        searchBarOutlet.tintColor = UIColor.white
        //        searchBarOutlet.placeholder = "Search".localized
        //        if let searchTextField = searchBarOutlet.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: "Zawgyi-One", size: 18) {
        //            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
        //                searchTextField.font = myFont
        //                searchTextField.font = myFont
        //                searchTextField.keyboardType = .asciiCapable
        //            }
        //        }
        //        let view = self.searchBarOutlet.subviews[0] as UIView
        //        let subViewsArray = view.subviews
        //        for subView: UIView in subViewsArray {
        //            if subView.isKind(of: UITextField.self) {
        //                subView.tintColor = ConstantsColor.navigationHeaderTransaction
        //            }
        //        }
        //        let searchBarButton = UIBarButtonItem(customView:searchBarOutlet)
        //        let backButton = UIButton(type: .custom)
        //        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        //        backButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        //        backButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        //        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        //        self.navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: backButton), searchBarButton]
        //        searchBarOutlet.becomeFirstResponder()
        //
        //        let TapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.HandleTapGesture(_:)))
        //        TapGesture.cancelsTouchesInView = false
        //        self.view.addGestureRecognizer(TapGesture)
        
    }
    @IBAction func searchBtnAction(_ sender: Any){
        println_debug("Search Btn was clicked")
        self.navigationItemOutlet.titleView = searchBarOutlet
        self.navigationItemOutlet.title = ""
        self.navigationItemOutlet.rightBarButtonItem?.isEnabled = false
        self.navigationItemOutlet.rightBarButtonItem?.tintColor = UIColor.clear
        searchBarOutlet.isHidden = false
        searchBarOutlet.becomeFirstResponder()
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            uiButton.titleLabel?.font = UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: 14)
            if appDel.currentLanguage == "my" {
                uiButton.setTitle("မလုပ်ပါ", for:.normal)
            } else if appDel.currentLanguage == "uni" {
                uiButton.setTitle("မလုပ္ပါ", for:.normal)
            } else {
                uiButton.setTitle("Cancel".localized, for:.normal)
            }
        }
        
        println_debug("call in search bar text did began editing")
        //        self.searchBarOutlet.setValue(UIColor.black, forKeyPath: "_searchField._placeholderLabel.textColor")
        //        let searchBarTextField : UITextField = self.searchBarOutlet.value(forKey: "_searchField") as! UITextField
        //        let leftImageView : UIImageView = searchBarTextField.leftView as! UIImageView
        //        leftImageView.tintColor = UIColor.white
        //        leftImageView.image? = (leftImageView.image?.withRenderingMode(.alwaysTemplate))!
        //        let clearButton : UIButton = searchBarTextField.value(forKey: "_clearButton") as! UIButton
        //        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        //        clearButton.tintColor = UIColor.white
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        self.FilterContentForSearchText(searchText: searchText)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        self.view.endEditing(true)
    }
    func clearSearchBar(){
        self.view.endEditing(true)
        self.searchBarOutlet.text = ""
        self.navigationItemOutlet.titleView = nil
        self.navigationItemOutlet.title = "Postpaid Bill".localized //appDelegate.getlocaLizationLanguage("LandLine Bill")
        self.navigationItemOutlet.rightBarButtonItem?.isEnabled = true
        self.navigationItemOutlet.rightBarButtonItem?.tintColor = UIColor.white;
        searchBarOutlet.isHidden = true
        
        //        self.FilterContentForSearchText(searchText: searchBar.text!)
        //        self.searchBarOutlet.setValue(UIColor.lightText, forKeyPath: "_searchField._placeholderLabel.textColor")
        //        let searchBarTextField : UITextField = self.searchBarOutlet.value(forKey: "_searchField") as! UITextField
        //        searchBarTextField.textColor = UIColor.black
        //        let leftImageView : UIImageView = searchBarTextField.leftView as! UIImageView
        //        leftImageView.tintColor = UIColor.lightText
        //        leftImageView.image? = (leftImageView.image?.withRenderingMode(.alwaysTemplate))!
        //        let clearButton : UIButton = searchBarTextField.value(forKey: "_clearButton") as! UIButton
        //        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        //        clearButton.tintColor = UIColor.lightText
        //        searchResults.removeAll()
        //        tableviewOutlet.reloadData()
        
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        self.clearSearchBar()
        searchResults.removeAll()
        self.AddBtn?.isHidden = false
        self.tableviewOutlet.reloadData()
    }
    @objc func HandleTapGesture(_:UIGestureRecognizer){
        self.view.endEditing(true)
        if searchBarOutlet.text == ""{
            searchBarOutlet.isHidden = true
            self.navigationItemOutlet.title = "Postpaid Bill".localized //appDelegate.getlocaLizationLanguage("LandLine Bill")
            self.navigationItemOutlet.rightBarButtonItem?.isEnabled = true
            self.navigationItemOutlet.rightBarButtonItem?.tintColor = UIColor.white;
        }
    }
    func FilterContentForSearchText(searchText : String){
        println_debug(searchText)
        searchResults.removeAll()
        if searchText.isEmpty && searchText == ""{
            tableviewOutlet.isHidden = false
            NoResultsLabel?.isHidden = true
            tableviewOutlet.reloadData()
            AddBtn?.isHidden = false
        }
        else {
            AddBtn?.isHidden = true
            tableviewOutlet.isHidden = false
            var demoArry = [PPMHistoryVCDataModel]()
            var i = 0
            while i < (HistoryDataModelObj.count) {
                let modelObj = HistoryDataModelObj[i]
                let landlineNumber = "0" + modelObj.PhoneNumber
                if landlineNumber.containsIgnoringCase(find: searchText) {
                    demoArry.append(modelObj)
                }
                i = i + 1
            }
            if demoArry.count > 0 {
                searchResults = demoArry
                self.tableviewOutlet.isHidden = false
                tableviewOutlet.reloadData()
            } else {
                self.tableviewOutlet.isHidden = true
                NoResultsLabel?.isHidden = false
            }
        }
    }
}
extension PPMHistoryVC: PPMJsonParserDelegate{
    func ResponseFromServer(withSuccess: Bool, json: AnyObject?, screen: String, Error: String?) {
        progressViewObj.removeProgressView()
        if screen == "PPMDeleteAPI" && withSuccess == true {
            var i = 0
            while i < (HistoryDataModelObj.count) {
                if (HistoryDataModelObj[i]).PhoneNumber == numberToDelete {
                    HistoryDataModelObj.remove(at: i)
                }
                i = i + 1
            }
            DispatchQueue.main.async {
                self.responseDataApiCall()
                self.tableviewOutlet.reloadData()
            }
        }
    }
}
extension  PPMHistoryVC: UITableViewDelegate, UITableViewDataSource, PPMCellDelegate{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell : PPMHistoryTableCell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! PPMHistoryTableCell
        if searchResults.count != 0{
            cell.wrapData(obj: searchResults[indexPath.row], index: indexPath)
        }
        else{
            cell.wrapData(obj: HistoryDataModelObj[indexPath.row], index: indexPath)
        }
        //        cell.wrapData(obj: HistoryDataModelObj![indexPath.row], index: indexPath)
        cell.delegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if searchResults.count != 0 || (searchResults.isEmpty == false){
            println_debug((searchResults.count))
            return (searchResults.count)
        }
        else{
            println_debug((HistoryDataModelObj.count))
            return (HistoryDataModelObj.count)
        }
    }
}
