//
//  LLBReceiptVC.swift
//  OK
//
//  Created by Rahul Tyagi on 10/17/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit


import Contacts
import ContactsUI
fileprivate enum TabBarItem : Int {
    case fav
    case home
    case addContact
    case more
}
class PPMReceiptVC: OKBaseController{
    @IBOutlet weak var  viewReceipt : UIView!
    
    let currentFormatter = DateFormatter()
    var DictToSend = Dictionary<String, AnyObject>()
    var strBillNo : String!
    @IBOutlet weak var lblLocation: UILabel!{
        didSet
        {
            lblLocation.font = UIFont(name: appFont, size: appFontSize)
            lblLocation.text = lblLocation.text?.localized
        }
    }
    @IBOutlet weak var lblBillId: UILabel!{
        didSet
        {
            lblBillId.font = UIFont(name: appFont, size: appFontSize)
            lblBillId.text = lblBillId.text?.localized
        }
    }
    @IBOutlet weak var lblAmount: UILabel!{
        didSet
        {
            lblAmount.font = UIFont(name: appFont, size: appFontSize)
            lblAmount.text = lblAmount.text?.localized
        }
    }
    @IBOutlet weak var lblTellBill: UILabel!{
        didSet
        {
            lblTellBill.font = UIFont(name: appFont, size: appFontSize)
            lblTellBill.text = lblTellBill.text?.localized
        }
    }
    @IBOutlet weak var lbTransactionId: UILabel!{
        didSet
        {
            lbTransactionId.font = UIFont(name: appFont, size: appFontSize)
            lbTransactionId.text = lbTransactionId.text?.localized
        }
    }
    @IBOutlet weak var lbTransactionTY: UILabel!{
        didSet
        {
            lbTransactionTY.font = UIFont(name: appFont, size: appFontSize)
            lbTransactionTY.text = lbTransactionTY.text?.localized
        }
    }
    @IBOutlet weak var lblTransactionId: UILabel!{
        didSet
        {
            lblTransactionId.font = UIFont(name: appFont, size: appFontSize)
            lblTransactionId.text = lblTransactionId.text?.localized
        }
    }
    @IBOutlet weak var lblTransactionType: UILabel!{
        didSet
        {
            lblTransactionType.font = UIFont(name: appFont, size: appFontSize)
            lblTransactionType.text = lblTransactionType.text?.localized
        }
    }
    @IBOutlet weak var lblDate: UILabel!{
        didSet
        {
            lblDate.font = UIFont(name: appFont, size: appFontSize)
            lblDate.text = lblDate.text?.localized
        }
    }
    @IBOutlet weak var lblTime: UILabel!{
        didSet
        {
            lblTime.font = UIFont(name: appFont, size: appFontSize)
            lblTime.text = lblTime.text?.localized
        }
    }
    @IBOutlet var btnRepayment: UIButton!{
        didSet
        {
            btnRepayment.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnRepayment.setTitle(btnRepayment.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var btnShare: UIButton!{
        didSet
        {
            btnShare.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnShare.setTitle(btnShare.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var rTabBar: UITabBar!
    @IBOutlet var home: UITabBarItem!{
        didSet
        {
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: appFont, size: appFontSize) ?? 12], for: .normal)
            home.title = home.title?.localized
        }
    }
    @IBOutlet var more: UITabBarItem!{
        didSet
        {
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: appFont, size: appFontSize) ?? 12], for: .normal)
            more.title = more.title?.localized
        }
    }
    @IBOutlet var fav: UITabBarItem!{
        didSet
        {
            if appFont == "Myanmar3"{
                UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: appFont, size: 12.0) ?? 12], for: .normal)
            }else{
                UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: appFont, size: 15.0) ?? 12], for: .normal)
            
            }
            
            
            
            
            
            fav.title = fav.title?.localized
        }
    }
    @IBOutlet var addContact: UITabBarItem!{
        didSet
        {
            
            if appFont == "Myanmar3"{
                
                UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: appFont, size: 12.0) ?? 12], for: .normal)
            }else{
                UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: appFont, size: 15.0) ?? 12], for: .normal)
            
            }
            
           
            addContact.title = addContact.title?.localized
        }
    }
    @IBOutlet var transactionView: CardDesignView!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var childView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Postpaid Bill".localized
        
        let color = UIColor.init(hex: "162D9F")
        let attrFont = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.0) ?? UIFont.systemFont(ofSize: 11.0), NSAttributedString.Key.foregroundColor: color]
        
        self.home.setTitleTextAttributes(attrFont, for: .normal)
        self.more.setTitleTextAttributes(attrFont, for: .normal)
        self.addContact.setTitleTextAttributes(attrFont, for: .normal)
        self.fav.setTitleTextAttributes(attrFont, for: .normal)
        
        self.fav.title = "Add Favorite".localized
        self.home.title = "Home".localized
        self.addContact.title = "Share".localized
        self.more.title = "More".localized
        
        println_debug(DictToSend)
        self.loadResponseValue()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissView))
        tap.numberOfTapsRequired = 1
        self.transactionView.addGestureRecognizer(tap)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11)], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11)], for: .normal)
        UITabBar.appearance().unselectedItemTintColor = .lightGray
    }
    
    override func viewDidAppear(_ animated: Bool)  {
        super.viewDidAppear(true)
//        let image = OK_.captureScreen(view: viewReceipt)
//         UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)

    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            alertViewObj.wrapAlert(title: "Error".localized, body: error.localizedDescription, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        } else {
            println_debug("Your altered image has been saved to your photos.")
        }
    }
    
    func shareQRWithDetail() {
        let image = OK_.captureScreen(view: viewReceipt)
          let imageToShare = [image]
          let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
          activityViewController.popoverPresentationController?.sourceView = self.view
          DispatchQueue.main.async {
              self.present(activityViewController, animated: true, completion: nil)
          }
      }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20)!]
        self.navigationController?.isNavigationBarHidden = true
        //        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        //        appDel.floatingButtonControl?.window.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        self.parentView.isHidden = true
    }
    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnRepayment(_ sender: UIButton){
        //        self.navigationController?.popToViewController((self.navigationController?.viewControllers[1])!, animated: true)
        userDef.set(true, forKey: "makeAnotherPayment")
        userDef.synchronize()
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func btnShareClick(_ sender: UITabBarItem){
        //self.shareQRWithDetail()
        
        //        let pdfUrl = self.createPdfFromView(aView: transactionView, saveToDocumentsWithFileName: "Transaction Receipt".localized)
        //        let activityViewController = UIActivityViewController(activityItems: [pdfUrl!], applicationActivities: nil)
        //        self.present(activityViewController, animated: true, completion: nil)
        //
        self.transactionView.isHidden = false
        self.parentView.isHidden = false
        //        let activityItem = self.captureScreen()
        //        "".share(activityItem: (activityItem)!)
    }
    @IBAction func shareClick(_ sender: UITabBarItem) {
        let pdfUrl = self.createPdfFromView(aView: self.viewReceipt, saveToDocumentsWithFileName: "Transaction Receipt".localized)
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl!], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    @IBAction func btnHomeClick(_ sender: UITabBarItem){
        NotificationCenter.default.post(name: .goDashboardScreen, object: nil)
        performSegue(withIdentifier: "unwindSegueToVC2", sender: self)
    }
    @IBAction func btnInvoiceClick(_ sender: UIButton){
        guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: self.viewReceipt, pdfFile: "OK$ PostPaid Mobile Receipt") else { return }
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    @IBAction func btnAddFavoriteClick(_ sender: UITabBarItem){
        println_debug("Add to Favorite")
        if let amountData = DictToSend["Amount"] as? Int {
            let vc = self.addFavoriteController(withName: "Unknown",favNum: "0\(strBillNo!)",type: "POSTPAID", amount: self.getDigitDisplay(String(amountData)))
            if let vcs = vc {
                self.present(vcs, animated: true, completion: nil)
            }
        }
    }
    @IBAction func btnAddContactClick(_ sender: UITabBarItem){
        showNewContactViewController()
    }
}
extension PPMReceiptVC: UITabBarDelegate{
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch(item) {
        case fav        : self.btnAddFavoriteClick(item)
        case home       : self.btnHomeClick(item)
        case addContact : self.shareClick(item)    // self.btnAddContactClick(item)
        case more       : self.btnShareClick(item)
        default: break
        }
    }
}
extension PPMReceiptVC{
    func loadResponseValue(){
        self.lblBillId.text = "0\(strBillNo!)"
        if let amount = DictToSend["Amount"] as? String {
            self.lblAmount.text =  self.getDigitDisplay(amount)
        } else if let amount = DictToSend["Amount"] as? NSNumber {
            self.lblAmount.text =  self.getDigitDisplay(String(describing: amount))
        } else if let amount = DictToSend["Amount"] as? Int {
            self.lblAmount.text =  self.getDigitDisplay(String(amount))
        } else { }
        if let transID = DictToSend["OkTransactionId"] as? String {
            self.lblTransactionId.text = transID
        }
        if let dateFromServer = DictToSend["PaymentDate"] as? String {
//            if let date = self.getDate(serverDate: dateFromServer){
//                println_debug(date)
//                self.lblDate.text = date
//            }
            
            self.lblDate.text = self.getDate()
            self.lblTime.text = convertDateFormatter(date: dateFromServer)

//            if let time = self.getTime(serverDate: dateFromServer){
//                self.lblTime.text = time
//                println_debug(time)
//                
//            }
        }
    }
    
    func convertDateFormatter(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
        let dateVal = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "EE, dd-MM-yyyy, hh:mm a"///this is what you want to convert format
        if let newDate = dateVal{
            let timeStamp = dateFormatter.string(from: newDate)
            if timeStamp.count>0{
                let actualValue = timeStamp.components(separatedBy: ",")
                if actualValue.count>0{
                    if actualValue.indices.contains(2){
                        return actualValue[2]
                    }
                }else{
                    return ""
                }
            }
        }else{
            let payDate = date.components(separatedBy: "T")
            if payDate.count>0{
                if payDate.indices.contains(1){
                    let newPayDate = payDate[1].components(separatedBy: ".")
                    if newPayDate[0].count>0{
                        return newPayDate[0]
                    }else{
                        return ""
                    }
                }
            }
        }
        return ""
    }
    
    func getDate() -> String {
         let date = Date()
         let formatter = DateFormatter()
         formatter.calendar = Calendar(identifier: .gregorian)
         formatter.dateFormat = "EEE, dd-MMM-yyyy"
         return formatter.string(from: date)
     }
     
    /*
    func getDate(serverDate : String) -> String? {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.calendar = Calendar(identifier: .gregorian)
        dateFormatterPrint.dateFormat = "MMM dd,yyyy"
        dateFormatterPrint.setLocale()
        let date: Date? = currentFormatter.date(from: serverDate)
        if let dateStr = date {
            return dateFormatterPrint.string(from: dateStr)
        }
        return ""
    }
    func getTime(serverDate : String) -> String? {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.calendar = Calendar(identifier: .gregorian)
        dateFormatterPrint.dateFormat = "HH:mm:ss"
        dateFormatterPrint.setLocale()
        let time: Date? = currentFormatter.date(from: serverDate)
        if let timeStr = time {
            return dateFormatterPrint.string(from: timeStr)
        }
        return ""
    }
 */
    
    func captureScreen() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, UIScreen.main.scale)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    @objc func dismissView() {
        self.transactionView.isHidden = true
    }
    func createPdfFromView(aView: UIView, saveToDocumentsWithFileName pdfFileName: String) -> URL? {
        let render = UIPrintPageRenderer()
        let page = CGRect(x: 0, y: 0, width: 595.2, height: 841) // A4, 72 dpi
        let printable = page.insetBy(dx: 0, dy: 0)
        //        render.addPrintFormatter(aView.viewPrintFormatter(), startingAtPageAt: 0)
        render.setValue(NSValue(cgRect: page), forKey: "paperRect")
        render.setValue(NSValue(cgRect: printable), forKey: "printableRect")
        let priorBounds = aView.bounds
        let fittedSize = aView.sizeThatFits(CGSize(width:priorBounds.size.width, height:aView.bounds.size.height))
        aView.bounds = CGRect(x:0, y:0, width:fittedSize.width, height:fittedSize.height)
        let pdfPageBounds = CGRect(x:0, y:0, width:aView.frame.width, height:self.view.frame.height)
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds,nil)
        let pageOriginY: CGFloat = 0
        //        while pageOriginY < fittedSize.height {
        UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
        UIGraphicsGetCurrentContext()!.saveGState()
        UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
        aView.layer.render(in: UIGraphicsGetCurrentContext()!)
        UIGraphicsGetCurrentContext()!.restoreGState()
        //            pageOriginY += pdfPageBounds.size.height
        //        }
        UIGraphicsEndPDFContext()
        aView.bounds = priorBounds
        guard let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else  { return nil}
        let properPDFFileName = pdfFileName.replacingOccurrences(of: ".pdf", with: "") + ".pdf"
        let pdfUrl = documentDirectories + "/" + properPDFFileName
        //Delete file if alreay exists
        do {
            try FileManager.default.removeItem(atPath: pdfUrl)
        } catch let error as NSError {
            println_debug("Error: \(error.domain)")
        }
        //Write file
        if pdfData.write(toFile: pdfUrl, atomically: true) {
            println_debug("Successfully wrote to \(pdfUrl)")
            let pathUrl = URL(fileURLWithPath: documentDirectories)
            let fileUrl = pathUrl.appendingPathComponent(properPDFFileName)
            return fileUrl
        }
        return nil
    }
}
extension PPMReceiptVC : CNContactViewControllerDelegate {
    func showNewContactViewController(){
        let contactViewController: CNContactViewController = CNContactViewController(forNewContact: nil)
        contactViewController.contactStore = CNContactStore()
        contactViewController.delegate = self
        let navigationController: UINavigationController = UINavigationController(rootViewController: contactViewController)
        present(navigationController, animated: false){
            println_debug("Present")
        }
    }
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?){
        self.dismiss(animated: true, completion: nil)
    }
    func contactViewController(_ viewController: CNContactViewController, shouldPerformDefaultActionFor property: CNContactProperty) -> Bool{
        return true
    }
}

//extension PPMReceiptVC :  UIImagePickerControllerDelegate  {
//    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
//        if let error = error {
//            alertViewObj.wrapAlert(title: "Error".localized, body: error.localizedDescription, img:   #imageLiteral(resourceName: "IT") )
//            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {})
//            alertViewObj.showAlert(controller: self)
//        } else {
//            alertViewObj.wrapAlert(title: "Saved!".localized, body: "Image saved successfully".localized, img:   #imageLiteral(resourceName: "IT") )
//            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {})
//            alertViewObj.showAlert(controller: self)
//        }
//    }
//}
extension UIApplication {
    class var PPMReceiptVC: OKBaseController? {
        return getTopViewController() as? OKBaseController
    }
    private class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return getTopViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}
