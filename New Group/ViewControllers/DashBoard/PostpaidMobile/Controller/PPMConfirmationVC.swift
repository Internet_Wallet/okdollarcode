//
//  LLBConfirmationVC.swift
//  OK
//
//  Created by Rahul Tyagi on 10/17/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit

class PPMConfirmationVC: OKBaseController  {
    let jsonParserObj = PPMJsonParser()
    var labelArray = ["City".localized,"Postpaid Number".localized,"Bill Month".localized,"Total Bill Amount".localized, "Total Payable Amount".localized]
    var valueArray : [String] = []
    var DictToSend = Dictionary<String, Any>()
    @IBOutlet var city: UILabel!{
        didSet
        {
            city.font = UIFont(name: appFont, size: appFontSize)
            city.text = city.text?.localized
        }
    }
    @IBOutlet var postNum: UILabel!{
        didSet
        {
            postNum.font = UIFont(name: appFont, size: appFontSize)
            postNum.text = postNum.text?.localized
        }
    }
    @IBOutlet var billamt: UILabel!{
        didSet
        {
            billamt.font = UIFont(name: appFont, size: appFontSize)
            billamt.text = billamt.text?.localized
        }
    }
    @IBOutlet var totPayAmt: UILabel!{
        didSet
        {
            totPayAmt.font = UIFont(name: appFont, size: appFontSize)
            totPayAmt.text = totPayAmt.text?.localized
        }
    }
    @IBOutlet var totbillAmt: UILabel!{
        didSet
        {
            totbillAmt.font = UIFont(name: appFont, size: appFontSize)
            totbillAmt.text = totbillAmt.text?.localized
        }
    }
    @IBOutlet weak var lblCity: UILabel!{
        didSet
        {
            lblCity.font = UIFont(name: appFont, size: appFontSize)
            lblCity.text = lblCity.text?.localized
        }
    }
    @IBOutlet weak var lblLandlineNumber: UILabel!{
        didSet
        {
            lblLandlineNumber.font = UIFont(name: appFont, size: appFontSize)
            lblLandlineNumber.text = lblLandlineNumber.text?.localized
        }
    }
    @IBOutlet weak var lblBillMonth: UILabel!{
        didSet
        {
            lblBillMonth.font = UIFont(name: appFont, size: appFontSize)
            lblBillMonth.text = lblBillMonth.text?.localized
        }
    }
    @IBOutlet weak var lblTotalBillAmount: UILabel!{
        didSet
        {
            lblTotalBillAmount.font = UIFont(name: appFont, size: appFontSize)
            lblTotalBillAmount.text = lblTotalBillAmount.text?.localized
        }
    }
    @IBOutlet weak var lblTotalPayAmount: UILabel!{
        didSet
        {
            lblTotalPayAmount.font = UIFont(name: appFont, size: appFontSize)
            lblTotalPayAmount.text = lblTotalPayAmount.text?.localized
        }
    }
    @IBOutlet var paybtn: UIButton!{
        didSet{
            paybtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            paybtn.setTitle(paybtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Postpaid Bill".localized
        println_debug(DictToSend)
        self.initConfigure()
        jsonParserObj.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20)!]
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        appDel.floatingButtonControl?.window.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
    }
}
extension  PPMConfirmationVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let customCell : PPMConfirmationTableCell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! PPMConfirmationTableCell
        customCell.labelOutlet.text = labelArray[indexPath.row].localized
        customCell.valueOutlet.text = valueArray[indexPath.row].localized
        return customCell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  labelArray.count
    }
}
extension  PPMConfirmationVC: PPMJsonParserDelegate{
    func ResponseFromServer(withSuccess Success: Bool, json: AnyObject?, screen: String, Error: String?) {
        if Success == false{
            DispatchQueue.main.async{
                progressViewObj.showProgressView()
            }
        }
        if Success == true && screen == "TokenForPayAPI"{
            let responseDic = json as! NSDictionary
            let tokentype = responseDic.value(forKey: "token_type")
            let accesstoken = responseDic.value(forKey: "access_token")
            let authorizationString =  "\(tokentype!) \(accesstoken!)"
            let BillDetailDict = (DictToSend["BillDetail"] as! Dictionary<String, Any>)
            let pipeLineStr = "OkAccountNumber=\(UserModel.shared.mobileNo)|Password=\(ok_password!)|BillDetailId=\(BillDetailDict["BilDetailId"]!)"
            println_debug("pipeline str :: \(pipeLineStr)")
            let hashValue: String = pipeLineStr.hmac_SHA1(key: PPMStrings.sKey)
            println_debug("actual hash value :: \(hashValue)")
            let keys : [String] = ["OkAccountNumber","Password","BillDetailId","HashValue"]
            let values : [String] = [UserModel.shared.mobileNo,ok_password!,BillDetailDict["BilDetailId"] as! String,hashValue]
            let jsonObj = JSONStringWriter()
            let jsonText = jsonObj.writeJsonIntoString(keys, values)
            let PayURL = URL.init(string: PPMStrings.kPayAPIURL)
            DispatchQueue.main.async{
                progressViewObj.showProgressView()
                self.jsonParserObj.ParseMyJson(url: PayURL!, param: jsonText as AnyObject, httpMethod: PPMStrings.kMethod_Post, contentType: PPMStrings.kContentType_Json, mScreen: "PayAPI", authStr: authorizationString)
            }
        }
        else if Success == true && screen == "PayAPI"{
            if json != nil{
                let responseDic = json as! NSDictionary
                println_debug("screen PayAPI:- \(screen)")
                println_debug(responseDic["Data"]!)
                DispatchQueue.main.async  {
                    progressViewObj.removeProgressView()
                    let payVCObj = PPMStrings.FileNames.PPMStoryBoardObj.instantiateViewController(withIdentifier: "PPMReceiptVC") as! PPMReceiptVC
                    if let responseDict =  OKBaseController.convertToDictionary(text: responseDic["Data"]! as? String ?? ""){
                        payVCObj.DictToSend = responseDict as [String : AnyObject]
                        let dict = self.DictToSend.safeValueForKey("BillDetail") as! Dictionary<String, AnyObject>
                        payVCObj.strBillNo = (dict.safeValueForKey("Number") as! String)
                        self.navigationController?.pushViewController(payVCObj, animated: true)
                    }
                }
            }
            else{
                DispatchQueue.main.async{
                    progressViewObj.removeProgressView()
                    alertViewObj.wrapAlert(title: "", body: "Error".localized, img:  #imageLiteral(resourceName: "error"))
                    alertViewObj.addAction(title: "OK".localized, style: .cancel){}
                    alertViewObj.showAlert(controller: self)
                }
            }
        }
    }
}
extension  PPMConfirmationVC{
    func initConfigure(){
        guard  let dict = DictToSend["BillDetail"] as? Dictionary<String, AnyObject> else{
            return
        }
        self.lblCity.text = dict["SourceDescription"] as? String
        self.lblLandlineNumber.text = ("0".appending(dict["Number"] as! String)).localized
        let helperObj = RTHelperClass()
        let BillingMonth = String(describing: dict["BillingMonth"]!)
        let BillingYear = String(describing: dict["BillingYear"]!)
        let dateToPass = BillingMonth + "-" + BillingYear
        let convertedValue = helperObj.formattedDateFromString(dateString: dateToPass, withFormat: "MM-yy", toBeConvertedToFormat: "MMMM YYYY")
        self.lblBillMonth.text = convertedValue
        self.lblTotalBillAmount.text = ("\( self.getDigitDisplay(String(describing: DictToSend["TotalBillAmount"] ?? "" as AnyObject)).appending(" MMK"))").localized
        if let total = dict["TotalBillAmount"]?.int64Value {
            if let paid = dict["TotalBillAmount"]?.int64Value {
                let refund = (total - paid)
                self.lblTotalPayAmount.text = (((refund as NSNumber).stringValue).appending(" MMK")).localized
                println_debug("Inside")
            }
        }
        self.lblTotalPayAmount.text = ("\( self.getDigitDisplay(String(describing: dict["TotalAmountMmk"] ?? "" as AnyObject)).appending(" MMK"))").localized
        if (dict["BillStatusDescription"] as? String) == "payforview"{
            
        }
        else{
            
        }
    }
    func getBillMonth(serverValue: Int) -> String{
        let monthNumber = serverValue
        let fmt = DateFormatter()
        fmt.calendar = Calendar(identifier: .gregorian)
        fmt.dateFormat = "MM"
        fmt.setLocale()
        let month = fmt.monthSymbols[monthNumber - 1]
        return month
    }
    @IBAction func btnBackClick(_ sender: UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func PayBtnAction(_ sender: Any){
        let hashValue = PPMStrings.aKey.hmac_SHA1(key: PPMStrings.sKey)
        let inputValue = "password=\(hashValue)&grant_type=password"
        jsonParserObj.ParseMyJson(url: URL.init(string: PPMStrings.kTokenAPIURL)! , param: inputValue as AnyObject, httpMethod: PPMStrings.kMethod_Post, contentType: PPMStrings.kContentType_urlencoded, mScreen: "TokenForPayAPI", authStr: nil)
    }
}
