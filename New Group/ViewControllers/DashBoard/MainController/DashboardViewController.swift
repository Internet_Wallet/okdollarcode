//
//  DashboardViewController.swift
//  OK
//
//  Created by Ashish Kr Singh on 25/08/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

public protocol ActionDashboardDelegate{
    func getAction(vName: String, index: Int)
}

protocol DashBoardDelegate{
    func didTapView(view: DashBoardViewType, withIndex index: Int)
}

enum DashBoardViewType{
    
    case loginOkWallet
    case topupRecharge
    case loyalityPromotion
    case bookOnOKDollar
    case billPaymentView
    case advertisementView
    
}

class DashboardViewController: OKBaseController, WebServiceResponseDelegate, ActionDashboardDelegate{
    
   
        
    @IBOutlet var tableview: UITableView!
    @IBOutlet var leftBarButton: UIBarButtonItem!
    @IBOutlet var rightBarButton: UIBarButtonItem!
    
    
    var sideMenu : ITRAirSideMenu?
    var leftMenu : SideMenuViewController?
    
    
    var dashboardView          = UIView()
    var loginOKWalletView      = LoginOKWalletView.updateViewResources     (frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 250))    as! LoginOKWalletView
    
    var topUpRechargeView      = TopUpRechargeView.updateViewResources     (frame: CGRect(x: 0, y: 250, width: UIScreen.main.bounds.width, height: 150))  as! TopUpRechargeView
    
    var addvertizeView         = AddvertizeView.updateViewResources        (frame: CGRect(x: 0, y: 400, width: UIScreen.main.bounds.width, height: 150))  as! AddvertizeView
    
    var loyalityPromotionsView = LoyalityPromotionsView.updateViewResources(frame: CGRect(x: 0, y: 550, width: UIScreen.main.bounds.width, height: 150))  as! LoyalityPromotionsView
    
    var bookOnOkDollarView     = BookOnOkDollarView.updateViewResources    (frame: CGRect(x: 0, y: 700, width: UIScreen.main.bounds.width, height: 350))  as! BookOnOkDollarView
    
    var billPaymentView        = BillPaymentView.updateViewResources       (frame: CGRect(x: 0, y: 1050, width: UIScreen.main.bounds.width, height: 250)) as! BillPaymentView

    override func viewDidLoad(){
        super.viewDidLoad()
        
        print(UserLogin.shared.aLevel)

        let logo = UIImage(named: "ok")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .center
        imageView.frame = CGRect.init(x: self.view.frame.origin.x/2 - 17, y: 22, width: 100, height: 100)
        
        self.navigationItem.titleView = imageView

        
        loadDashboardView()
        self.tableview.tableHeaderView = dashboardView
        
        
        self.apiForAdvertisement()
        
        billPaymentView.dbDelegate = self
        loginOKWalletView.dbDelegate = self
        loyalityPromotionsView.dbDelegate = self
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        for views in self.view.subviews {
            views.layoutIfNeeded()
        }
    }
    
    func loadDashboardView(){
        
        dashboardView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 1300)
        dashboardView.addSubview(loginOKWalletView)
        dashboardView.addSubview(topUpRechargeView)
        dashboardView.addSubview(addvertizeView)
        dashboardView.addSubview(loyalityPromotionsView)
        dashboardView.addSubview(bookOnOkDollarView)
        billPaymentView.delegate = self
        dashboardView.addSubview(billPaymentView)
        
        bookOnOkDollarView.dbDelegate = self
    }
    
    func apiForAdvertisement(){
        
        let url = URL.init(string: "http://120.50.43.152:8002/AdService.svc/GetAdvertisements")
        let web = WebApiClass()
        web.delegate = self
        let pram = Dictionary<String, Any>()
        web.genericClass(url: url!, param: pram as AnyObject, httpMethod: "get", mScreen: "menuAdver")
        
    }
    
    @IBAction func openSideMenu(_ sender: UIBarButtonItem){
            sideMenu?.presentLeftMenuViewController()
    }
    
    
    func webResponse(withJson json: AnyObject, screen: String){
        if screen == "menuAdver" {
            if let data = json as? Data {
                do {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dataDict = dict as? Dictionary<String,AnyObject> {
                        if let dic = dataDict["Data"] as? String {
                            _ =   dic.replacingOccurrences(of: "\\", with: "")
                            if let arr = OKBaseController.convertToArrDictionary(text: dic) {
                                DispatchQueue.main.async {
                                    for object in arr {
                                        if let obj = object as? Dictionary<String,Any> {
                                            let str = obj["ImagePath"] as? String
                                            if let encoded = str?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed), let url = URL(string: encoded)
                                            {
                                                let imageV = UIImageView.init(frame: CGRect.zero)
                                                imageV.downloadedFrom(link: url.absoluteString, contentMode: .scaleAspectFill) { (image) in
                                                    if image != nil {
                                                        AdvertisementImages.wrapModel(img: image!)
                                                        self.loadImagesInAdView()
                                                    }
                                                }
                                            }
                                            
                                        }
                                    }

                                }
                            }
                        }
                    }
                    
                    
                } catch {
                    
                }
                
            }
            
        }
    }
    
    func loadImagesInAdView() {
        addvertizeView.updateScrollImages()
        addvertizeView.autoScroll()
    }
    
    @IBAction func notificationAction(_ sender: UIBarButtonItem){
        self.performSegue(withIdentifier: "notification", sender: self)
    }
    
    func getAction(vName: String, index: Int) {
        if vName == "BillPayment" {
            
        }
    }
    
}



extension DashboardViewController: PaymentValidationDelegate{
    
    func paymentAuthorisationSuccess() {
        print("Authorised")
        
        
    }
}


extension DashboardViewController: DashBoardDelegate {
    func didTapView(view: DashBoardViewType, withIndex index: Int) {
        
        switch view {
        case .loginOkWallet:
            self.loginOkWallet(index: index)
            break
        case .topupRecharge:
            self.topupRecharge(index: index)
            break
        case .billPaymentView:
            self.billPaymentView(index: index)
            break
        case .bookOnOKDollar:
            self.bookOnOKDollar(index: index)
            break
        case .loyalityPromotion:
            self.loyalityPromotion(index: index)
            break
        case .advertisementView:
            self.advertisementView()
            break
        }
        
    }
    
    func loginOkWallet(index: Int) {
        let story = UIStoryboard.init(name: "BookOnOKDollar", bundle: nil)
        
        if index == 0 {

            
            let payment = PaymentAuthorisation()
            payment.authenticate()
            payment.delegate = self

//            let paytoVC = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PayToNavigation")
//            self.present(paytoVC, animated: true, completion: nil)

            let paytoVC = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PayToNavigation")
            self.present(paytoVC, animated: true, completion: nil)

        }

        if index == 1{
            
            let addWithdrawView = UIStoryboard(name: "AddWithdrawMain", bundle: nil).instantiateViewController(withIdentifier: "AddWithdrawView_ID") as? AddWithdrawViewController
            let aObjNav = UINavigationController(rootViewController: addWithdrawView!)
            aObjNav.isNavigationBarHidden = true
            self.present(aObjNav, animated: true, completion: nil)
        }
        else if index == 3 {
            _ =   profileObj.callUpdateProfileApi(preLoginModel: preLoginModel)
            
            let vc = story.instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
            vc?.type = WebType.bankDeposit
            self.present(vc!, animated: true, completion: nil)
        }
        else if index == 5 {
            let story: UIStoryboard = UIStoryboard(name: "NearbyOkServices", bundle: nil)
            let nearByOkServicesView = story.instantiateViewController(withIdentifier: "NearbyOkServicesMainView_ID") as? NearbyMainViewController
            let aObjNav = UINavigationController(rootViewController: nearByOkServicesView!)
            aObjNav.isNavigationBarHidden = true
            self.present(aObjNav, animated: true, completion: nil)
            
        }
    }
    
    func topupRecharge(index: Int) {
        
    }
    
    func billPaymentView(index: Int) {
        
        switch index{
            
        case 0:     _ =   profileObj.callUpdateProfileApi(preLoginModel: preLoginModel)
                    let story = UIStoryboard.init(name: "BookOnOKDollar", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
                    vc?.type = WebType.dth
                    self.present(vc!, animated: true, completion: nil)
            
        case 1 : break
            
        case 2 :
            
            /*
                let story = UIStoryboard.init(name: "BillSplitterView", bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: "BillSplitterView") as? BillSplitterView
                self.present(vc!, animated: true, completion: nil)
            */
            
            let story = UIStoryboard.init(name: "BillSplitterView", bundle: nil)

            let vc = story.instantiateViewController(withIdentifier: "BillSplitterView") as? BillSplitterView
            let navController = BillSplitterNavController.init(rootViewController: vc!)
                
            self.present(navController, animated: true, completion: nil)
                
            
        case 3 : loadLandLineBillModule()
            
        case 4 : loadPostpaidMobileModule()
            
        case 5 :    _ =   profileObj.callUpdateProfileApi(preLoginModel: preLoginModel)
                    let story = UIStoryboard.init(name: "BookOnOKDollar", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
                    vc?.type = WebType.merchantPayment
                    self.present(vc!, animated: true, completion: nil)
            
        default: break
            
        }
        
    }
    
    func loadLandLineBillModule(){
        
       progressViewObj.showProgressView()
        
        
        if !appDelegate.checkNetworkAvail(){
            
            alertViewObj.wrapAlert(title: nil, body: LLBStrings.ErrorMessages.NetWorkUnavailable, img: #imageLiteral(resourceName: "landlineImg"))
            alertViewObj.addAction(title: "OK", style: .target , action:{
                
            })
            alertViewObj.showAlert(controller: self)
            
        }
        else{
            
            let LLBHistoryStatusObj = LLBCheckHistoryStatus()
            LLBHistoryStatusObj.requestAPI {(responseStatus, ErrorMessage, historyDataModelRef) in
                
                DispatchQueue.main.async{
                    
                       progressViewObj.removeProgressView()
                    
                }
                
                let llbStoryBoardObj = LLBStrings.FileNames.llbStoryBoardObj
                
                switch responseStatus{
                    
                case .showGetBill:
                    
                    let GetBillObj = llbStoryBoardObj.instantiateViewController(withIdentifier: "LLBGetBillVCSID") as? LLBGetBillVC
                    let navController = LLBNavController.init(rootViewController: GetBillObj!)
                    DispatchQueue.main.async{
                        self.present(navController, animated: true, completion: nil)
                    }
                    
                case .showHistory:
                    
                    let HistoryObj = llbStoryBoardObj.instantiateViewController(withIdentifier: "LLBHistoryVCSID") as? LLBHistoryVC
                    HistoryObj?.HistoryDataModelObj = historyDataModelRef
                    let navController = LLBNavController.init(rootViewController: HistoryObj!)
                    
                    DispatchQueue.main.async{
                        self.present(navController, animated: true, completion: nil)
                    }
                    
                case .showAlert:
                    
                    alertViewObj.wrapAlert(title: nil, body: ErrorMessage!, img: #imageLiteral(resourceName: "landlineImg"))
                    alertViewObj.addAction(title: "OK", style: .target , action:{
                        
                    })
                    
                    DispatchQueue.main.async{
                        
                        alertViewObj.showAlert(controller: self)
                        
                    }
                }
                
            }
        }
    }

    
    
    
    
    
    func loadPostpaidMobileModule(){
        
        progressViewObj.showProgressView()
        
        
        if !appDelegate.checkNetworkAvail(){
            
            alertViewObj.wrapAlert(title: nil, body: PPMStrings.ErrorMessages.NetWorkUnavailable, img: #imageLiteral(resourceName: "landlineImg"))
            alertViewObj.addAction(title: "OK", style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
            
        }
        else{
            
            let PPMHistoryStatusObj = PPMCheckHistoryStatus()
            PPMHistoryStatusObj.requestAPI {(responseStatus, ErrorMessage, historyDataModelRef) in
                
                DispatchQueue.main.async{
                    progressViewObj.removeProgressView()
                }
                
                let llbStoryBoardObj = PPMStrings.FileNames.PPMStoryBoardObj
                
                switch responseStatus{
                    
                case .showGetBill:
                    let GetBillObj = llbStoryBoardObj.instantiateViewController(withIdentifier: "PPMGetBillVCSID") as? PPMGetBillVC
                    let navController = LLBNavController.init(rootViewController: GetBillObj!)
                    DispatchQueue.main.async{
                        self.present(navController, animated: true, completion: nil)
                    }
                    
                case .showHistory:
                    
                    let HistoryObj = llbStoryBoardObj.instantiateViewController(withIdentifier: "PPMHistoryVCSID") as? PPMHistoryVC
                    HistoryObj?.HistoryDataModelObj = historyDataModelRef
                    let navController = PPMNavController.init(rootViewController: HistoryObj!)
                    DispatchQueue.main.async{
                        self.present(navController, animated: true, completion: nil)
                    }
                    
                case .showAlert:
                    
                    alertViewObj.wrapAlert(title: nil, body: ErrorMessage!, img: #imageLiteral(resourceName: "landlineImg"))
                    alertViewObj.addAction(title: "OK", style: .target , action: {
                        
                    })
                    
                    DispatchQueue.main.async{
                        
                        alertViewObj.showAlert(controller: self)
                        
                    }
                }
                
            }
        }
    }
    
    
    
    
    func bookOnOKDollar(index: Int) {
        
        if index == 7 {
            let storyboard : UIStoryboard = UIStoryboard(name: "Common", bundle: nil)
            let interCityBusView = storyboard.instantiateViewController(withIdentifier: "IntercityBus_ID") as! IntercityBusViewController
            interCityBusView._viewFrom = .bus
            self.present(interCityBusView, animated: true, completion: nil)
        }
        
        let story = UIStoryboard.init(name: "BookOnOKDollar", bundle: nil)
        
        if index == 8 {
            _ =   profileObj.callUpdateProfileApi(preLoginModel: preLoginModel)

            let vc = story.instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
            vc?.type = WebType.giftCard
            self.present(vc!, animated: true, completion: nil)
        }
        
    }
    
    func loyalityPromotion(index: Int) {
        switch index {
        case 0:  break
        
        case 1:
            let story: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
            let promotionView = story.instantiateViewController(withIdentifier: "PromotionMainView_ID") as? PromotionsMainViewController
            let aObjNavi = UINavigationController(rootViewController: promotionView!)
            aObjNavi.isNavigationBarHidden = true
            self.present(aObjNavi, animated: true, completion: nil)

//            self.present(promotionView!, animated: true, completion: nil)
 
        default:  break
            
        }
    }
    
    func advertisementView(){
        
    }
}

extension DashboardViewController {
    
    func moveToDashboardAction(index: Int) {
        switch index {
        case 9:
            let viewController = UIStoryboard(name: "Setting", bundle: Bundle.main).instantiateViewController(withIdentifier: "myid")
            self.present(viewController, animated: true, completion: nil)
            break
            
        case 10:
            let textToShare: String = "Share OK$ application to your family"
            let link: String = "https://itunes.apple.com/us/app/ok-$/id1067828611?ls=1&mt=8"
            
            let url:NSURL = NSURL.init(string: link)!
            var activityItems = [Any]()
            activityItems.append(textToShare)
            activityItems.append(url)
            
            let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
            activityViewController.setValue("Hey this is awesome app.You also download and enjoy the features", forKey: "subject")
            activityViewController.excludedActivityTypes = [.assignToContact, .print]
            self.present(activityViewController, animated: true, completion: nil)

        case 9:
            let viewController = UIStoryboard(name: "Setting", bundle: Bundle.main).instantiateViewController(withIdentifier: "myid")
            self.present(viewController, animated: true, completion: nil)
            break
            
        
            
        default:
            break
        }
    }

}

