//
//  CashINOUT_PaymentRequestNavigation.swift
//  OK
//
//  Created by Ashish on 1/16/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreLocation

extension TBHomeViewController {
    
    //MARK: Cash In & OUT ____ Payment Request
    //-----------------------------------------------------------------------------------------------------------------------------------
    func navigateToCashIN() {
        println_debug("navigateToCashIN")
        
//        DispatchQueue.main.async(){
//            
//            alertViewObj.wrapAlert(title: "", body: "Cash In Coming Soon!".localized, img: #imageLiteral(resourceName: "dashboard_cashin"))
//            alertViewObj.addAction(title: "OK".localized, style: .cancel , action: {
//    
//            })
//            
//            alertViewObj.showAlert(controller: self)
//            progressViewObj.removeProgressView()
//        }
        
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                self.showErrorAlert(errMessage: "Please Enable Location Service".localized)
            case .authorizedAlways, .authorizedWhenInUse:
                let story = UIStoryboard.init(name: cashInOutViewControllers.cashinoutStoryBoard, bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: cashInOutViewControllers.viewControllers.cioHomeVC) as? CIOHomeViewController
                vc?.screenName = "Cash In"
                let navController = UINavigationController(rootViewController: vc!)
                navController.modalPresentationStyle = .fullScreen
                self.present(navController, animated: true, completion: nil)
            }
        } else {
            println_debug("Please Enable Location Service".localized)
        }
        
        
        
        
    }
    
    func navigateToCashOUT() {
//        println_debug("navigateToCashOUT")
//        DispatchQueue.main.async(){
//
//            alertViewObj.wrapAlert(title: "", body: "Cash Out Coming Soon!".localized, img: #imageLiteral(resourceName: "dashboard_cashout"))
//            alertViewObj.addAction(title: "OK".localized, style: .cancel , action: {
//
//            })
//
//            alertViewObj.showAlert(controller: self)
//            progressViewObj.removeProgressView()
//        }
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                self.showErrorAlert(errMessage: "Please Enable Location Service".localized)
            case .authorizedAlways, .authorizedWhenInUse:
                let story = UIStoryboard.init(name: cashInOutViewControllers.cashinoutStoryBoard, bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: cashInOutViewControllers.viewControllers.cioHomeVC) as? CIOHomeViewController
                vc?.screenName = "Cash Out"
                let navController = UINavigationController(rootViewController: vc!)
                navController.modalPresentationStyle = .fullScreen
                self.present(navController, animated: true, completion: nil)
            }
        } else {
            println_debug("Please Enable Location Service".localized)
        }
        
        
        
    }
    
    func  navigateToRequestMoney() {
        println_debug("navigateToRequestMoney")
        
        let story = UIStoryboard.init(name: "requestmoney", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "request_money_navigation_controller")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func navigateToBillSplitter() {
        println_debug("navigateToBillSplitter")
        
        let story = UIStoryboard.init(name: "BillSplitterView", bundle: nil)
        
        let vc = story.instantiateViewController(withIdentifier: "BillSplitterView") as? BillSplitterView
        let navController = BillSplitterNavController.init(rootViewController: vc!)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: nil)

        
//        let vc = story.instantiateViewController(withIdentifier: "bill_splitter_navigation_controller")
//        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    func navigateToOKChat() {
        println_debug("navigateToOKChat")
        #if DEBUG
        DispatchQueue.main.async() {
            alertViewObj.wrapAlert(title: "", body: "OK$ Chat Coming Soon!".localized, img: #imageLiteral(resourceName: "dashboard_chat"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel , action: {
            })
            alertViewObj.showAlert(controller: self)
            progressViewObj.removeProgressView()
        }
//            let story = UIStoryboard.init(name: "Chat", bundle: nil)
//            let chatNavVC = story.instantiateViewController(withIdentifier: "ChatNavC")
//            self.present(chatNavVC, animated: true, completion: nil)
        #else
            DispatchQueue.main.async() {
                alertViewObj.wrapAlert(title: "", body: "OK$ Chat Coming Soon!".localized, img: #imageLiteral(resourceName: "dashboard_chat"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel , action: {
                })
                alertViewObj.showAlert(controller: self)
                progressViewObj.removeProgressView()
            }
        #endif
    }
}
