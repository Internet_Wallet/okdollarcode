//
//  Recharge_BillPaymentNavigation.swift
//  OK
//
//  Created by Ashish on 1/16/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

extension TBHomeViewController : BioMetricLoginDelegate {
    
    //MARK: - Navigation
    func navigateToReSale() {
        println_debug("navigateToReSale")
        let viewController = UIStoryboard(name: "Resale", bundle: Bundle.main).instantiateViewController(withIdentifier: "ResaleNavigate")
        
        //let navVC = UINavigationController(rootViewController: buyBonusVC)
        viewController.modalPresentationStyle = .fullScreen
        self.present(viewController, animated: true, completion: nil)
    }
    
    func navigateToMobileNumber() {
        println_debug("My Number Recharge Navigation")
        if (UserLogin.shared.walletBal as NSString).floatValue < 100.00 {
            alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
            alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                TBHomeNavigations.main.navigate(.addWithdraw)
            })
            alertViewObj.showAlert(controller: self)
        } else {
            let vc = UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "myNumberNavigation"))
            DispatchQueue.main.async {
                self.getTopMostViewController()?.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    func navigateToOtherNumber() {
        println_debug("navigateToOtherNumber")
        let vc = UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "TopupNavigation"))
        //        self.navigationController?.viewControllers[0].present(vc, animated: true, completion: nil)
        DispatchQueue.main.async {
            self.getTopMostViewController()?.present(vc, animated: true, completion: nil)
        }
        //        self.present(vc, animated: true, completion: nil)
    }
    
    func navigateToOverseasRecharge() {
        println_debug("navigateToOverseasRecharge")
        /*
        InternationalTopupManager.selectedCountry =  ("+66", "thailand")
        let vc = UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "InternationalTopup"))

        self.present(vc, animated: true, completion: nil)
      //  */
        
        //*
//        if (UserLogin.shared.walletBal as NSString).floatValue < 1000.00 {
//            alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
//            alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
//                TBHomeNavigations.main.navigate(.addWithdraw)
//            })
//            alertViewObj.showAlert(controller: self)
//        } else {
            if appDel.checkNetworkAvail() {
                let vc = UIStoryboard(name: "InternationalTopUp", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "InternationalMainVC")) as? InternationalMainVC
                vc?.modalPresentationStyle = .fullScreen
                vc?.navigation = self.navigationController
                self.navigationController?.present(vc!, animated: true, completion: nil)
            }
        //}
    }
    
    func navigateToLandlineBill() {
        println_debug("navigateToLandlineBill")
        
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "navigateToLandlineBill", delegate: self)
        } else {
            self.loadLandLineBillModule()
        }
        
    }
    
    func navigateToPostpaidMobile() {
        println_debug("navigateToPostpaidMobile")
    
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "navigateToPostpaidMobile", delegate: self)
        } else {
            self.loadPostpaidMobileModule()
        }
    }
    
    func navigateToThaiMobileRecharge() {
        println_debug("navigateToThaiMobileRecharge")
        DispatchQueue.main.async(){
            InternationalTopupManager.selectedCountry =  ("+66", "thailand")
            let vc = UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "InternationalTopup"))
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func navigateToChinaMobileRecharge() {
        println_debug("navigateToChinaMobileRecharge")
        DispatchQueue.main.async(){
            InternationalTopupManager.selectedCountry = ("+86","china")
            let vc = UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "InternationalTopup"))
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func navigateToBuyTopupBonusPoint()  {
        if (UserLogin.shared.walletBal as NSString).floatValue < 1000.00 {
            alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
            alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                TBHomeNavigations.main.navigate(.addWithdraw)
            })
            alertViewObj.showAlert(controller: self)
        } else {
            if appDel.checkNetworkAvail() {
                let vc = UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "LoyaltyTopupNavigation"))
                self.present(vc, animated: true, completion: nil)
            } else {
                self.showErrorAlert(errMessage: PaytoConstants.messages.noInternet.localized)
            }
        }
    }
    
    
    func navigateToBuyBonusPoint() {
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "LoyaltyBuyBonus", delegate: self)
            println_debug("navigateToLoyalty")
        } else {
            self.navToBuyBonusVC()
        }
    }
    
    
    
    func navToBuyBonusVC() {
        if let buyBonusVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: "BuySaleBonusPointHomeViewController_ID") as? BuySaleBonusPointHomeViewController {
            //            buyBonusVC.screenFrom = .dashboard
            let navVC = UINavigationController(rootViewController: buyBonusVC)
            navVC.modalPresentationStyle = .fullScreen
            self.present(navVC, animated: true, completion: nil)
        }
    }
    
    func navigateToResaleBonusPoint() {
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "LoyaltyResaleBonus", delegate: self)
            println_debug("navigateToLoyalty")
        } else {
            self.navToResaleBonusVC()
        }
    }
    
    func navToResaleBonusVC() {
        if let resaleVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: "BuySaleBonusPointHomeViewController_ID") as? BuySaleBonusPointHomeViewController {
            //            resaleVC.screenFrom = .dashboard
            let rVC = UINavigationController(rootViewController: resaleVC)
            rVC.modalPresentationStyle = .fullScreen
            self.present(rVC, animated: true, completion: nil)
        }
    }
    
    func navigateToBankDepositVerification() -> Void {
        //Security check
        println_debug("navigateToBankDepositVerification")
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "BankDepositVerficationSession", delegate: self)
        } else {
            self.navigateToBDV()
        }
        
    }
    
    func navigateToBDV() {
        _ =   profileObj.callUpdateProfileApi(aCode: UserModel.shared.mobileNo, handler: { (success) in
            if success {
                DispatchQueue.main.async {
                    let story = UIStoryboard.init(name: "BookOnOKDollar", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
                    vc?.type = WebType.bankDeposit
                    vc?.modalPresentationStyle = .fullScreen
                    self.present(vc!, animated: true, completion: nil)
                }
            }
        })
    }
    
    func navigateToElectricitybill() {
        println_debug("navigateToElectricitybill")
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "ElectricitySession", delegate: self)
        } else {
            self.navigateToEB()
        }
    }
    
    func navigateToTaxPayment() {
        println_debug("navigateToTaxPayment")
        self.navigateToTaxPayModule()
        //        DispatchQueue.main.async(){
        //            alertViewObj.wrapAlert(title: "", body: "Coming Soon".localized, img: #imageLiteral(resourceName: "dashboard_taxpayment"))
        //            alertViewObj.addAction(title: "OK".localized, style: .cancel , action: {})
        //            alertViewObj.showAlert(controller: self)
        //            progressViewObj.removeProgressView()
        //        }
    }
    
    func navigateToLottery() {
        println_debug("navigateTolottery")
        // change the controller for lottery
        
        let viewController = UIStoryboard(name: "Lottery", bundle: Bundle.main).instantiateViewController(withIdentifier: LotteryConstant.kLotteryListViewController) as? LotteryListViewController
        navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        navigationController?.pushViewController(viewController!, animated: true)
        
        
    }
    
    func navigateToDth() {
        println_debug("navigateToDth")
        
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "DTH_Screen", delegate: self)
        } else {
            self.callingDTHViewControllers()
        }
        
    }
    
    func navigateToDonation() { // donation
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "WebType.donation", delegate: self)
        } else {
            self.callingDonation()
        }
    }
    
    func callingDonation() {
        println_debug("callingDonation")
        
        var urlS = ""
        let urlString = getUrl(urlStr: Url.donation, serverType: .donationUrl)
        urlS = String.init(format: "%@", urlString.absoluteString)
        let dict = self.getParamsGiftCard()
        
        let queryItems = dict.flatMap { key, value -> [URLQueryItem] in
            if let strings = value as? [String] {
                return strings.map{ URLQueryItem(name: key, value: $0) }
            } else {
                return [URLQueryItem(name: key, value: value as? String)]
            }
        }
        
        var urlComponents = URLComponents(string: urlS)!
        urlComponents.queryItems = queryItems
        let url = urlComponents.url!
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
        
        //        let url2 = NSURL(string: "http://www.apple.com/")
        //        let request = NSMutableURLRequest(URL: url2! as URL)
        //        request.HTTPBody = "company=Locassa&quality=AWESOME!".dataUsingEncoding(NSUTF8StringEncoding)
        
        
        //        let urlString = getUrl(urlStr: Url.donation, serverType: .donationUrl)
        //        let urlS = String.init(format: "%@", urlString.absoluteString)
        //        let request = self.giftCardLoader(urlString: urlS)
        //        //self.webView.loadRequest(request)
        //
        //        if let url = URL(string: urlS) {
        //            UIApplication.shared.open(url)
        //        }
        
        /*
         let story = UIStoryboard.init(name: "BookOnOKDollar", bundle: nil)
         let vc = story.instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
         vc?.type = WebType.donation
         self.present(vc!, animated: true, completion: nil)
         */
    }
    
    
    //MARK: Gift Card Loader
    func giftCardLoader(urlString: String) -> URLRequest {
        let url = URL.init(string: urlString)
        var request = URLRequest.init(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60)
        request.addValue("application/json;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "POST"
        
        let param = self.getParamsGiftCard()
        let postData: Data? = try? JSONSerialization.data(withJSONObject: param, options: [])
        if postData != nil {
            request.httpBody = postData
        }
        
        return request
    }
    
    //MARK: Parameters according to enum
    func getParamsGiftCard() -> Dictionary<String,Any> {
        
        var language = "en-GB"
        var isNativeApp = true
        var languageType = ""
        
        if appDel.currentLanguage == "my"{
            language = "my-MM"
            languageType = ""
            
        } else if appDel.currentLanguage == "uni"{
            isNativeApp = false
            language = "my-MM"
            languageType = "1"
        }else {
            language = "en-GB"
            languageType = ""
        }
        
        let childDicts = [
            "Pwd": ok_password ?? "",
            "Appid": LoginParams.setUniqueIDLogin(),
            "MobileNumber": UserModel.shared.mobileNo,
            "Simid": uuid,
            "Msid": UserModel.shared.msid,
            "Lat": UserModel.shared.lat,
            "Long": UserModel.shared.long,
            "DeviceID": uuid,
            "Language": language,
            "LanguageType": languageType,
            "Balance": UserLogin.shared.walletBal,
            "AppVersion": buildVersion,
            "IsNativeApp": isNativeApp
            ] as [String: Any]
        
        let final = [
            "UtilityJsonRequest": childDicts
        ]
        return final
    }
    
    func navigateToEarnPointTransfer() {
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "EarnPointTransfer", delegate: self)
        } else {
            self.navigateToEPT()
        }
    }
    
    func navigateToEPT() {
        //        let loyaltyViewNav = UIStoryboard(name: "LoyaltyModule", bundle: nil).instantiateViewController(withIdentifier: "loyaltyNavigation")
        //        self.present(loyaltyViewNav, animated: true, completion: nil)
        if let earnPointTrfVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: loyaltyMainViews.viewControllers.earnPointTransferVC) as? EarnPointTransferViewController {
            earnPointTrfVC.screenFrom = .dashboard
            let rVC = UINavigationController(rootViewController: earnPointTrfVC)
            rVC.modalPresentationStyle = .fullScreen
            self.present(rVC, animated: true, completion: nil)
        }
    }
    
    func navigateToRedeem() {
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "Redeem", delegate: self)
        } else {
            self.navigateToRedeemModule()
        }
    }
    
    func navigateToRedeemModule() {
        if let discountMoneyVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: loyaltyMainViews.viewControllers.discountMoneyVC) as? DiscountMoneyHomeViewController {
            discountMoneyVC.screenFrom = .dashboard
            let rVC = UINavigationController(rootViewController: discountMoneyVC)
            rVC.modalPresentationStyle = .fullScreen
            self.present(rVC, animated: true, completion: nil)
        }
    }
    
//     func navigateToEB() {
//          DispatchQueue.main.async {
//              progressViewObj.removeProgressView()
//              let CustomStoreyboard = UIStoryboard(name: "ElectricityModule", bundle: nil)
//              if let EBObj = CustomStoreyboard.instantiateInitialViewController() as? UINavigationController {
//              EBObj.isNavigationBarHidden = true
//              EBObj.modalPresentationStyle = .fullScreen
//              self.present(EBObj, animated: true) {() -> Void in }
//              }
//          }
//      }
    
    func navigateToEB() {
        progressViewObj.showProgressView()
        let agentNum = UserModel.shared.mobileNo
        let msid = UserModel.shared.msid
        println_debug("\(uuid)")
        let str: String = uuid
        var inputDict = [String : Any]()
        
        inputDict = [
            "MobileNumber" : agentNum ,
            "Simid" : str,
            "Msid" : msid ,
            "Ostype" : "1",
            "Otp" : ""
        ]
        println_debug("\(inputDict)")
        let url = URL(string: String(format: Url.Get_Electricity_appType))
        println_debug("url :: \(String(describing: url))")
        
        var request = URLRequest.init(url: url!, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
        request.httpMethod = "POST"
        request.addValue(kContentType_Json, forHTTPHeaderField: "Content-Type")
        request.addValue(kContentType_Json, forHTTPHeaderField: "Accept")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: inputDict, options: JSONSerialization.WritingOptions.prettyPrinted)
        } catch {
            println_debug(error)
        }
        
        JSONParser.GetResaleApiResponse(apiUrlReq: request) { (isSuccess, response) in
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    println_debug("remove progress view called inside get token from server call")
                }
                return
            }
            if let responseDic = response as? NSDictionary {
                println_debug("response dict :: \(String(describing: responseDic))")
                if let code = responseDic["Code"] as? NSNumber, code == 200 {
                    println_debug("Success. Checking Electric Bill History Now")
                    if responseDic["Data"] as? String == "0" {
                        self.getEBHistoryByMobileNoFromServer()
                    } else {
                        let CustomStoreyboard = UIStoryboard(name: "ElectricityModule", bundle: nil)
                        if let EBObj = CustomStoreyboard.instantiateViewController(withIdentifier: "EBWeb") as? ElectricityWebViewController {
                            let CustomNavigationController = UINavigationController(rootViewController: EBObj)
                            CustomNavigationController.isNavigationBarHidden = true
                            EBObj.modalPresentationStyle = .fullScreen
                            self.present(CustomNavigationController, animated: true) {() -> Void in }
                        }
                        progressViewObj.removeProgressView()
                    }
                }
            }
        }
    }
    
    func navigateToTaxPayModule() {
        //        getTokenAndHitPaymentsEnquiryAPI()
        let viewController = UIStoryboard(name: "TaxPayment", bundle: Bundle.main).instantiateViewController(withIdentifier: "TaxPaymentRoot")
        self.present(viewController, animated: true, completion: nil)
    }
    
    
    func navigateToScanAndPay() {
        println_debug("navigateToScanAndPay")
      }
    
    func getTokenAndHitPaymentsEnquiryAPI() {
        if appDelegate.checkNetworkAvail() {
            self.view.endEditing(true)
            getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
                guard isSuccess, let _ = tokenString else {
                    self.showErrorAlert(errMessage: "Try Again".localized)
                    return
                }
                DispatchQueue.main.async(execute: {
                    self.callingHttp()
                })
            })
        }
        else {
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            alertViewObj.showAlert(controller: UIViewController.init())
        }
    }
    
    func getTokenFromServer(completionHandler: @escaping (Bool,Any?) -> Void) {
        let urlString = String.init(format: Url.TaxPaymentTokenAPIURL)
        let getURL = getUrl(urlStr: urlString, serverType: .taxation) as? URL
        
        guard let tokenUrl = getURL else {
            showErrorAlert(errMessage: "Something went wrong : Dev need to check")
            return
        }
        let hashValue = Url.aKey_Tax_Payment.hmac_SHA1(key: Url.sKey_Tax_Payment)
        let inputVal = "password=\(hashValue)&grant_type=password"
        
        progressViewObj.showProgressView()
        println_debug("get token from server called........")
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: kMethod_Post, contentType: kContentType_urlencoded, inputVal: inputVal, authStr: nil)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    println_debug("remove progress view called inside get token from server call")
                }
                completionHandler(false,nil)
                return
            }
            println_debug("response dict for get token from server :: \(String(describing: response))")
            let responseDic = response as! NSDictionary
            guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                completionHandler(false,nil)
                return
            }
            let authorizationString =  "\(tokentype) \(accesstoken)"
            self.tokenFullString = authorizationString
            completionHandler(true,authorizationString)
        })
    }
    
    func callingHttp() {
        if appDelegate.checkNetworkAvail() {
            let urlString   = String.init(format: Url.taxPaymentEnquiry, "\(UserModel.shared.mobileNo)", "1")
            let getURL = getUrl(urlStr: urlString, serverType: .taxation)
            println_debug(getURL)
            let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: getURL, methodType: kMethod_Get, contentType: kContentType_Json, inputVal: nil, authStr: self.tokenFullString)
            DispatchQueue.main.async {
                println_debug("progress view started")
            }
            
            JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
                guard isSuccess else {
                    DispatchQueue.main.async {
                        println_debug("remove progress view called inside cashin main api call with error")
                        progressViewObj.removeProgressView()
                    }
                    self.showErrorAlert(errMessage: "Try Again".localized)
                    return
                }
                DispatchQueue.main.async(execute: {
                    if let responseDic = response as? Dictionary<String,Any> {
                        let jsonData = JSON(responseDic)
                        println_debug(jsonData)
                        if jsonData["StatusCode"].stringValue == "302" && jsonData["Status"].boolValue == true {
                            
                        }
                        else {
                            let currentStr = UserDefaults.standard.string(forKey: "currentLanguage")
                            var displayMsg = ""
                            if currentStr == "my" {
                                displayMsg = jsonData["Result"]["MessageZawgyi"].stringValue
                            } else {
                                displayMsg = jsonData["Result"]["MessageEnglish"].stringValue
                            }
                            //                                self.showMsgPopupAlert(msg: displayMsg, image: #imageLiteral(resourceName: "dashboard_taxpayment"))
                            alertViewObj.wrapAlert(title: "".localized, body:displayMsg, img: #imageLiteral(resourceName: "tax_Logo"))
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                //                                    self.parent?.parent?.dismiss(animated: true, completion: nil)
                                self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                            }
                            alertViewObj.showAlert(controller: UIViewController.init())
                        }
                    }
                })
                progressViewObj.removeProgressView()
            })
        }
        else {
            alertViewObj.wrapAlert(title: "No Internet Connection".localized, body:"Please connect to the Internet to use OK$".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            alertViewObj.showAlert(controller: UIViewController.init())
        }
    }
    
    func navigateToSolarElectricity() {
        if (UserLogin.shared.walletBal as NSString).floatValue < 500.00 {
            alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
            alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                    addWithdrawView.modalPresentationStyle = .fullScreen
                    self.navigationController?.present(addWithdrawView, animated: true, completion: nil)
                }
            })
            alertViewObj.showAlert()
            return
        }
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "SolarElectricity", delegate: self)
        } else {
            let viewController = UIStoryboard(name: "ElectricityBillSolar", bundle: Bundle.main).instantiateViewController(withIdentifier: "SolarElectricityRoot")
            viewController.modalPresentationStyle = .fullScreen

            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    //MARK: - Methods
    func getEBHistoryByMobileNoFromServer() {
        let agentNum = UserModel.shared.mobileNo
        let msid = UserModel.shared.msid
        println_debug("\(uuid)")
        let str: String = uuid
        let inputDict = [
            "MobileNumber" : agentNum,
            "Simid" : str,
            "Msid" : msid,
            "Ostype" : "1",
            "Otp" : ""
        ]
        println_debug("Here is the dict \(inputDict)")
        let userDict = [
            "LoginDeviceInfo" : inputDict,
            "SearchDate" : "01-05-2017",
            "SearchType" : "0"
            ] as [String : Any]
        
        println_debug(userDict)
        let url = URL(string: String(format: Url.Get_Ebill_Histrory))
        
        var request = URLRequest.init(url: url!, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
        request.httpMethod = "POST"
        request.addValue(kContentType_Json, forHTTPHeaderField: "Content-Type")
        request.addValue(kContentType_Json, forHTTPHeaderField: "Accept")
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: userDict, options: JSONSerialization.WritingOptions.prettyPrinted)
        } catch {
            println_debug(error)
        }
        
        JSONParser.GetResaleApiResponse(apiUrlReq: request) { (isSuccess, response) in
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                }
                return
            }
            println_debug("response dict for get token from server :: \(String(describing: response))")
            if let responseDic = response as? NSDictionary {
                if let code = responseDic["Code"] as? NSNumber, code == 200 {
                    println_debug("Success.print Checking Electric Bill History Now")
                    if responseDic["Data"] as? String == "[]" {
                        DispatchQueue.main.async(execute: {() -> Void in
                            progressViewObj.removeProgressView()
                            let CustomStoreyboard = UIStoryboard(name: "ElectricityModule", bundle: nil)
                            if let EBObj = CustomStoreyboard.instantiateViewController(withIdentifier: "GetBillVCSID") as? GetElectricityBillVC
                            {
                            let CustomNavigationController = UINavigationController(rootViewController: EBObj)
                            CustomNavigationController.isNavigationBarHidden = true
                            CustomNavigationController.modalPresentationStyle = .fullScreen
                            self.present(CustomNavigationController, animated: true) {() -> Void in }
                            }
                        })
                    }else {
                        DispatchQueue.main.async {
                            progressViewObj.removeProgressView()
                            let CustomStoreyboard = UIStoryboard(name: "ElectricityModule", bundle: nil)
                            if let EBObj = CustomStoreyboard.instantiateViewController(withIdentifier: "PayBillVCSID") as? PayBillVC
                            {
                                let CustomNavigationController = UINavigationController(rootViewController: EBObj)
                                CustomNavigationController.isNavigationBarHidden = true
                                CustomNavigationController.modalPresentationStyle = .fullScreen
                                self.present(CustomNavigationController, animated: true) {() -> Void in }
                            }
                        }
                    }
                }

            }
            
        }
    }
    
    func callingDTHViewControllers() {
        _ =   profileObj.callUpdateProfileApi(aCode: UserModel.shared.mobileNo, handler: { (success) in
            if success {
                DispatchQueue.main.async {
                    let story = UIStoryboard.init(name: "BookOnOKDollar", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
                    vc?.type = WebType.dth
                    vc?.modalPresentationStyle = .fullScreen
                    self.present(vc!, animated: true, completion: nil)
                }
            }
        })
    }
    
    func loadLandLineBillModule() {
        progressViewObj.showProgressView()
        if !appDelegate.checkNetworkAvail() {
            alertViewObj.wrapAlert(title: nil, body: LLBStrings.ErrorMessages.NetWorkUnavailable, img: #imageLiteral(resourceName: "landlineImg"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action:{
            })
            alertViewObj.showAlert(controller: self)
        } else {
            let LLBHistoryStatusObj = LLBCheckHistoryStatus()
            LLBHistoryStatusObj.requestAPI {(responseStatus, ErrorMessage, historyDataModelRef) in
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    //                }
                    let llbStoryBoardObj = LLBStrings.FileNames.llbStoryBoardObj
                    
                    switch responseStatus {
                    case .showGetBill:
                        let GetBillObj = llbStoryBoardObj.instantiateViewController(withIdentifier: "LLBGetBillVCSID") as? LLBGetBillVC
                        let navController = LLBNavController.init(rootViewController: GetBillObj!)
                        navController.modalPresentationStyle = .fullScreen
                        GetBillObj?.modalPresentationStyle = .fullScreen
                        GetBillObj?.newlandlinepayment = "NEW"
                        //                    DispatchQueue.main.async{
                        self.present(navController, animated: true, completion: nil)
                        //                    }
                        break
                        
                    case .showHistory:
                        let HistoryObj = llbStoryBoardObj.instantiateViewController(withIdentifier: "LLBHistoryVCSID") as? LLBHistoryVC
                        HistoryObj?.HistoryDataModelObj = historyDataModelRef!
                        let navController = LLBNavController.init(rootViewController: HistoryObj!)
                        navController.modalPresentationStyle = .fullScreen
                        HistoryObj?.modalPresentationStyle = .fullScreen
                        //                    DispatchQueue.main.async{
                        self.present(navController, animated: true, completion: nil)
                        //                    }
                        break
                        
                    case .showAlert:
                        //                    alertViewObj.wrapAlert(title: nil, body: ErrorMessage!, img: #imageLiteral(resourceName: "landlineImg"))
                        //                    alertViewObj.addAction(title: "OK".localized, style: .target , action:{
                        //                    })
                        //                    DispatchQueue.main.async {
                        //                        alertViewObj.showAlert(controller: self)
                        self.loadLandLineBillModule()
                        //                    }
                        break
                    }
                }
            }
        }
    }
    
    func loadPostpaidMobileModule() {
        progressViewObj.showProgressView()
        if !appDelegate.checkNetworkAvail() {
            alertViewObj.wrapAlert(title: nil, body: PPMStrings.ErrorMessages.NetWorkUnavailable, img: #imageLiteral(resourceName: "landlineImg"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        } else {
            let PPMHistoryStatusObj = PPMCheckHistoryStatus()
            PPMHistoryStatusObj.requestAPI {(responseStatus, ErrorMessage, historyDataModelRef) in
                DispatchQueue.main.async{
                    progressViewObj.removeProgressView()
                    //                }
                    let llbStoryBoardObj = PPMStrings.FileNames.PPMStoryBoardObj
                    switch responseStatus {
                    case .showGetBill:
                        let GetBillObj = llbStoryBoardObj.instantiateViewController(withIdentifier: "PPMGetBillVCSID") as? PPMGetBillVC
                        let navController = PPMNavController.init(rootViewController: GetBillObj!)
                        navController.modalPresentationStyle = .fullScreen
                        GetBillObj?.modalPresentationStyle = .fullScreen
                        //                    DispatchQueue.main.async{
                        self.present(navController, animated: true, completion: nil)
                        //                    }
                        break
                        
                    case .showHistory:
                        let HistoryObj = llbStoryBoardObj.instantiateViewController(withIdentifier: "PPMHistoryVCSID") as? PPMHistoryVC
                        HistoryObj?.HistoryDataModelObj = historyDataModelRef!
                        let navController = PPMNavController.init(rootViewController: HistoryObj!)
                        navController.modalPresentationStyle = .fullScreen
                        HistoryObj?.modalPresentationStyle = .fullScreen
                        //                    DispatchQueue.main.async{
                        self.present(navController, animated: true, completion: nil)
                        //                    }
                        break
                        
                    case .showAlert:
                        //                    alertViewObj.wrapAlert(title: nil, body: ErrorMessage!, img: #imageLiteral(resourceName: "landlineImg"))
                        //                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        //                    })
                        //                    DispatchQueue.main.async{
                        //                        alertViewObj.showAlert(controller: self)
                        self.loadPostpaidMobileModule()
                        //                    }
                        break
                    }
                }
            }
        }
    }
    
    //MARK: - Biometric Authentication
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if !isSuccessful { return }
        if screen == "DTH_Screen"{
            self.callingDTHViewControllers()
        }
        else if screen == "WebType.donation" {
            self.callingDonation()
        }else if screen == "LoyaltyList" {
            if isSuccessful{
                let loyaltyViewNav = UIStoryboard(name: "LoyaltyModule", bundle: nil).instantiateViewController(withIdentifier: "loyaltyNavigation")
                loyaltyViewNav.modalPresentationStyle = .fullScreen
                self.present(loyaltyViewNav, animated: true, completion: nil)
            }
        }else if screen == "VoucherSession" {
            if isSuccessful {
                self.navigateVoucher()
            }
        }else if screen == "OKShoppingSession" {
            if isSuccessful {
                self.navigateEcomerce()
            }
        }else if screen == "MerchantPaySession" {
            if isSuccessful {
                self.navigateMerchantPay()
            }
        }else if screen == "ElectricitySession" {
            if isSuccessful{
                self.navigateToEB()
            }
        }else if screen == "BankDepositVerficationSession" {
            if isSuccessful{
                self.navigateToBDV()
            }
        }else if screen == "SolarElectricity" {
            if isSuccessful{
                let viewController = UIStoryboard(name: "ElectricityBillSolar", bundle: Bundle.main).instantiateViewController(withIdentifier: "SolarElectricityRoot")
                viewController.modalPresentationStyle = .fullScreen
                self.present(viewController, animated: true, completion: nil)
            }
        }else if screen == "TaxPayment" {
            if isSuccessful{
                self.navigateToTaxPayModule()
            }
        }else if screen == "ScanPay" {
            if isSuccessful{
                self.navigateToScanPay()
            }
        }else if screen == "EarnPointTransfer" {
            if isSuccessful {
                self.navigateToEPT()
            }
        }else if screen == "Redeem" {
            if isSuccessful {
                self.navigateToRedeemModule()
            }
        }else if screen == "LoyaltyResaleBonus" {
            if isSuccessful {
                self.navToResaleBonusVC()
            }
        }else if screen == "LoyaltyBuyBonus" {
            if isSuccessful {
                self.navToBuyBonusVC()
            }
        }else if screen == "SunKing" || screen == "Insurance" {
            if isSuccessful {
                self.navigateAfterLogin(str: screen)
            }
        }else if screen == "navigateToLandlineBill" {
            if isSuccessful {
                self.loadLandLineBillModule()
            }
        }else if screen == "navigateToPostpaidMobile" {
            if isSuccessful {
                self.loadPostpaidMobileModule()
            }
        }else if screen == "Tusar" {
            if isSuccessful {
                TBHomeNavigations.main.navigateToQRWithData(isComingFromQR: false)
            }else{
                self.noInternetAlert()
                progressViewObj.removeProgressView()
            }
        }else {
            if isSuccessful {
                let loyaltyViewNav = UIStoryboard(name: "LoyaltyModule", bundle: nil).instantiateViewController(withIdentifier: "loyaltyNavigation")
                loyaltyViewNav.modalPresentationStyle = .fullScreen
                self.present(loyaltyViewNav, animated: true, completion: nil)
            }
        }
    }
}
