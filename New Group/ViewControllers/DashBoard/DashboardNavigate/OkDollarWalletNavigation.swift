//
//  TBHomeNavigation.swift
//  OK
//
//  Created by Ashish on 1/16/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreLocation

extension TBHomeViewController  {
    
    
    
    //Mohit
    
    func getTopMostViewController() -> UIViewController? {
        var topMostViewController = UIApplication.shared.keyWindow?.rootViewController
        
        while let presentedViewController = topMostViewController?.presentedViewController {
            topMostViewController = presentedViewController
        }
        
        return topMostViewController
    }
    
    //MARK: OK DOLLAR WALLET
    //-----------------------------------------------------------------------------------------------------------------------------------
    
    func showKYCinfoView() -> Void {
        guard let kycView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TBKYCUpdateViewController") as? TBKYCUpdateViewController else { return }
        kycView.modalPresentationStyle = .overCurrentContext
        kycView.delegate = self
        self.present(kycView, animated: true, completion: nil)
    }
    
    //here the storyboard load of payto
    func navigateToPayto() -> Void {
        println_debug("navigateToPayto")
        let scanPromotionObj = ["status" :false, "beforePromotion" : "", "afterPromotion": ""] as [String : Any]
        UserDefaults.standard.set(scanPromotionObj, forKey: "ScanQRPromotion")
        let payto = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PayToNavigation")
        isPayToDashboard = true
        payto.modalPresentationStyle = .fullScreen
        //        self.present(payto, animated: true, completion: nil)
        
        DispatchQueue.main.async {
            self.getTopMostViewController()?.present(payto, animated: true, completion: nil)
        }
        
        
    }
    
    func navigateTo_transferTo_Payto() {
        
        // Subhash Add Code here
        
        //                DispatchQueue.main.async(){
        //
        //                    alertViewObj.wrapAlert(title: "", body: "Coming Soon!".localized, img: #imageLiteral(resourceName: "dashboard_facePay"))
        //                    alertViewObj.addAction(title: "OK".localized, style: .cancel , action: {
        //
        //                    })
        //
        //                    alertViewObj.showAlert(controller: self)
        //                }
        
        // From here Navigating
        let sb = UIStoryboard(name: "AddWithDrawNew", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "AddWithdrawNavi")
        vc.modalPresentationStyle = .fullScreen
        //        self.navigationController?.present(vc, animated: true, completion: nil)
        //        self.present(vc, animated: true, completion: nil)
        
        DispatchQueue.main.async {
            self.getTopMostViewController()?.present(vc, animated: true, completion: nil)
        }
        
        
    }
    
    
    func navigateTo_cashInOut_Payto(type: PaytoEnum) {
        if type == .cashOutPayto {
            if walletAmount() == 0.0 {
                alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                    if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                        addWithdrawView.modalPresentationStyle = .fullScreen
                        self.navigationController?.present(addWithdrawView, animated: true, completion: nil)
                    }
                })
                alertViewObj.showAlert()
                return
            }
        }
        
        
        let scanPromotionObj = ["status" :false, "beforePromotion" : "", "afterPromotion": ""] as [String : Any]
        UserDefaults.standard.set(scanPromotionObj, forKey: "ScanQRPromotion")
        //        guard let cicoPayto = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PayToCashInOutViewController") as? PayToCashInOutViewController else { return }
        //        if type == .cashInPayto {
        //            cicoPayto.transType = PayToType(type: .cashIn)
        //        } else {
        //            cicoPayto.transType = PayToType(type: .cashOut)
        //        }
        //        self.navigationController?.pushViewController(cicoPayto, animated: true)
        
        let addMoneyStoryboard = UIStoryboard.init(name: "AddWithdraw", bundle: nil)
        guard let firstView_addMoneyVC = addMoneyStoryboard.instantiateViewController(withIdentifier: "AddWithdrawMoneyVC") as? AddWithdrawMoneyVC else { return }
        firstView_addMoneyVC.modalPresentationStyle = .fullScreen
        firstView_addMoneyVC.screenFrom.0 = "DashBoard"
        if type == .cashInPayto {
            firstView_addMoneyVC.screenFrom.1 = "Cash In"
        } else {
            firstView_addMoneyVC.screenFrom.1 = "Cash Out"
        }
        self.navigationController?.pushViewController(firstView_addMoneyVC, animated: true)
    }
    
    func navigateToAddWithDraw() -> Void {
        if(appDel.checkNetworkAvail()) {
            /*
             if let addWithdrawView = UIStoryboard(name: "AddWithdrawMain", bundle: nil).instantiateViewController(withIdentifier: "AddWithdrawView_ID") as? AddWithdrawViewController {
             let aObjNav = UINavigationController(rootViewController: addWithdrawView)
             self.present(aObjNav, animated: true, completion: nil)
             }*/
            
            if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                addWithdrawView.modalPresentationStyle = .fullScreen
                self.navigationController?.present(addWithdrawView, animated: true, completion: nil)
            }
            
        } else {
            self.noInternetAlert()
        }
    }
    
    
    
    func navigateToBankCounterDeposit() -> Void {
        println_debug("navigateToBankCounterDeposit")
        let controller:BankCounterDepositeVC = UitilityClass.getStoryBoardAddWithdraw().instantiateViewController(withIdentifier: "BankCounterDepositeVC") as! BankCounterDepositeVC
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    func navigateToNearByOKServices() -> Void {
        println_debug("navigateToNearByOKServices")
        
        if(appDel.checkNetworkAvail()) {
            //Check location access
            
            if CLLocationManager.locationServicesEnabled() {
                switch(CLLocationManager.authorizationStatus()) {
                case .notDetermined:
                    println_debug("notDetermined")
                    loadNearByServicesView()
                    break
                case .denied :
                    println_debug("denied")
                    showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                    break
                case .restricted :
                    println_debug("restricted")
                    showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                    break
                case .authorizedAlways, .authorizedWhenInUse:
                    loadNearByServicesView()
                    break
                }
            } else {
                alertViewObj.wrapAlert(title:"", body: "Please Enable Location Service".localized, img: #imageLiteral(resourceName: "location"))
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                    
                })
                DispatchQueue.main.async {
                    alertViewObj.showAlert(controller: self)
                }
            }
        } else {
            self.noInternetAlert()
        }
    }
    
    func loadNearByServicesView() {
        let story: UIStoryboard = UIStoryboard(name: "NBSMain", bundle: nil)
        let nearByOkServicesView = story.instantiateViewController(withIdentifier: "NearByServicesMainVC") as! NearByServicesMainVC
        let aObjNav = UINavigationController(rootViewController: nearByOkServicesView)
        aObjNav.isNavigationBarHidden = true
        aObjNav.modalPresentationStyle  = .fullScreen
        //nearByOkServicesView.statusScreen = "NearBy"
        self.present(aObjNav, animated: true, completion: nil)
    }
    
}


extension TBHomeViewController: ShowUpdateKYCProtocol {
    func showUpdateprofileVC() {
        if(UserModel.shared.accountType == "1") {
            let story = UIStoryboard.init(name: "MyProfile", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "UpdateProfile") as! UpdateProfile
            vc.navigation = self.navigationController
            let navController = UpdateProfileNavController.init(rootViewController: vc)
            self.present(navController, animated: true, completion: nil)
        } else {
            let story = UIStoryboard.init(name: "MyProfile", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "UpdateProfile") as! UpdateProfile
            vc.navigation = self.navigationController
            let navController = UpdateProfileNavController.init(rootViewController: vc)
            self.present(navController, animated: true, completion: nil)
        }
    }
}
