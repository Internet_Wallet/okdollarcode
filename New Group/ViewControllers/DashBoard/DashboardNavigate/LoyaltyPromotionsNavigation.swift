//
//  LoyaltyPromotionsNavigation.swift
//  OK
//
//  Created by Ashish on 1/16/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreLocation

extension TBHomeViewController {
    
    func navigateToLoyalty() {
        
//
//        alertViewObj.wrapAlert(title: nil, body: "Coming Soon".localized, img: #imageLiteral(resourceName: "dashboard_loyalty"))
//        alertViewObj.addAction(title: "OK".localized, style: .cancel) {
//            
//        }
//        alertViewObj.showAlert()
        
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "LoyaltyList", delegate: self)
            println_debug("navigateToLoyalty")
        } else {
            let loyaltyViewNav = UIStoryboard(name: "LoyaltyModule", bundle: nil).instantiateViewController(withIdentifier: "loyaltyNavigation")
            loyaltyViewNav.modalPresentationStyle = .fullScreen
            self.present(loyaltyViewNav, animated: true, completion: nil)
        }
    }
    
    
    func navigateToVoting() {
        let story: UIStoryboard = UIStoryboard(name: "Voting", bundle: nil)
        guard let votingVC = story.instantiateViewController(withIdentifier: "VotingAllProgramViewController") as? VotingAllProgramViewController else { return }
       // let votingNC = UINavigationController(rootViewController: votingVC)
        //self.present(votingNC, animated: true, completion: nil)
        self.navigationController?.pushViewController(votingVC, animated: true)

    }
    
    func navigateToPromotions() {
        
        //Check location access
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined:
                println_debug("notDetermined")
                loadPromotionsView()
                break
            case .denied :
                println_debug("denied")
                showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                break
            case .restricted :
                println_debug("restricted")
                showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                break
            case .authorizedAlways, .authorizedWhenInUse:
                loadPromotionsView()
                break
            }
        } else {
            alertViewObj.wrapAlert(title:"", body: "Please Enable Location Service".localized, img:#imageLiteral(resourceName: "location"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
        
        /*
        let story: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
        let promotionView = story.instantiateViewController(withIdentifier: "PromotionMainView_ID") as? PromotionsMainViewController
        let aObjNavi = UINavigationController(rootViewController: promotionView!)
        aObjNavi.isNavigationBarHidden = true
        self.present(aObjNavi, animated: true, completion: nil)
         */
        
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:"OK$ would like to use your Current Location. Please turn on the Location Services for your device.".localized, img:#imageLiteral(resourceName: "map"))
        
        alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
          
        })
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            }
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func loadPromotionsView() {
        let story: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
        let promotionView = story.instantiateViewController(withIdentifier: "PromotionMainView_ID") as? PromotionsMainViewController
        let aObjNavi = UINavigationController(rootViewController: promotionView!)
        aObjNavi.isNavigationBarHidden = true
        aObjNavi.modalPresentationStyle = .fullScreen
        self.present(aObjNavi, animated: true, completion: nil)
    }
    
    func navigateToLuckyDrawCoupon() {
//        guard let luckyDrawHomevc = UIStoryboard(name: "LuckyDrawCoupan", bundle: nil).instantiateViewController(withIdentifier: "LuckyDrawHomeViewController") as? LuckyDrawHomeViewController else { return }
        guard let luckyDrawHomevc = UIStoryboard(name: "LuckyDrawCoupan", bundle: nil).instantiateViewController(withIdentifier: "LDCScanBarCodeViewController") as? LDCScanBarCodeViewController else { return }
        let rootNavigation = UINavigationController(rootViewController: luckyDrawHomevc)
         self.helpSupportNavigationEnum = .Lucky_Draw
         rootNavigation.modalPresentationStyle = .fullScreen
        self.present(rootNavigation, animated: true, completion: nil)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
