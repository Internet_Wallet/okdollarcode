//
//  BookOnOKDollar.swift
//  OK
//
//  Created by Ashish on 1/16/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

extension TBHomeViewController {

    //MARK:- Book On OK Dollar
    func navigateToFlights() {
        println_debug("navigateToFlights")
        let storyboard : UIStoryboard = UIStoryboard(name: "Common", bundle: nil)
        let interCityBusView = storyboard.instantiateViewController(withIdentifier: "IntercityBus_ID") as! IntercityBusViewController
        interCityBusView._viewFrom = .flight
        //        self.present(interCityBusView, animated: true, completion: nil)
        let aObjNav = UINavigationController(rootViewController: interCityBusView)
        //        aObjNav.isNavigationBarHidden = false
        aObjNav.modalPresentationStyle = .fullScreen
        self.present(aObjNav, animated: true, completion: nil)
    }
    
    func navigateToHotel() {
        println_debug("navigateToHotel")
        let storyboard : UIStoryboard = UIStoryboard(name: "Common", bundle: nil)
        let interCityBusView = storyboard.instantiateViewController(withIdentifier: "IntercityBus_ID") as! IntercityBusViewController
        interCityBusView._viewFrom = .hotel
        //        self.present(interCityBusView, animated: true, completion: nil)
        let aObjNav = UINavigationController(rootViewController: interCityBusView)
        //        aObjNav.isNavigationBarHidden = false
        aObjNav.modalPresentationStyle = .fullScreen
        self.present(aObjNav, animated: true, completion: nil)
    }
    
    func navigateToIntercityBus() {
        println_debug("navigateToIntercityBus")
        let storyboard : UIStoryboard = UIStoryboard(name: "Common", bundle: nil)
        let interCityBusView = storyboard.instantiateViewController(withIdentifier: "IntercityBus_ID") as! IntercityBusViewController
        interCityBusView._viewFrom = .bus
        //        self.present(interCityBusView, animated: true, completion: nil)
        let aObjNav = UINavigationController(rootViewController: interCityBusView)
        //        aObjNav.isNavigationBarHidden = false
        aObjNav.modalPresentationStyle = .fullScreen
        self.present(aObjNav, animated: true, completion: nil)
    }
    
    func navigateToLiveTaxi() {
        println_debug("navigateToLiveTaxi")
        DispatchQueue.main.async(){
            self.openUrlForTaxi()
        }
    }

    private  func openUrlForTaxi() {
        let urlTaxi = "gocgmoktaxi://?action=login&name=%@&email=%@&cuncode=%@&phnum=%@&gen=%@&dob=%@"
        
        var gender = UserModel.shared.gender
        if gender == "1" {
            gender = "male"
        } else {
            gender = "female"
        }
        
        let mobile = PTManagerClass.decodeMobileNumber(phoneNumber: UserModel.shared.mobileNo)
        
        let params = String.init(format: urlTaxi, UserModel.shared.name, UserModel.shared.email, mobile.country.dialCode,UserModel.shared.mobileNo, gender, UserModel.shared.dob)
        
        if let urlString = params.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let okTaxiUrl = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(okTaxiUrl) {
                    UIApplication.shared.open(okTaxiUrl, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:])
                        , completionHandler: nil)
                } else {
                    self.navigateToTaxiAppStoreLink()
                }
            }
        }
    }
    
    func navigateToTaxiAppStoreLink(){
        
            let urlStr = "https://itunes.apple.com/in/app/ok-taxi-myanmar/id1435362402?mt=8"
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: urlStr)!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                
            } else {
                UIApplication.shared.openURL(URL(string: urlStr)!)
            }
        
        /*
        if UIApplication.shared.canOpenURL(URL(string: "vmart://?OKRegisteredNum=\(UserModel.shared.mobileNo)&OKRegisteredDeviceId=\(UserModel.shared.deviceId)")!){
            
            UIApplication.shared.canOpenURL(URL(string: "vmart://?OKRegisteredNum=\(UserModel.shared.mobileNo)&OKRegisteredDeviceId=\(UserModel.shared.deviceId)")!)
        }else{
            let urlStr = "https://itunes.apple.com/in/app/ok-taxi-myanmar/id1435362402?mt=8"
            //let urlStr = "https://apps.apple.com/sg/app/1-stop-mart/id1501302377"
            if #available(iOS 10.0, *) {
                
                
                UIApplication.shared.open(URL(string: urlStr)!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                
            } else {
                UIApplication.shared.openURL(URL(string: urlStr)!)
            }
        }
        */
        
    }
    
    func navigateToOffers() {
        
        let offerStoryboard   = UIStoryboard(name: "Offers", bundle: Bundle.main)
        guard let offers       = offerStoryboard.instantiateViewController(withIdentifier: "OffersViewController") as? OffersViewController else { return }
        let aObjNav = UINavigationController(rootViewController: offers)
        aObjNav.modalPresentationStyle = .fullScreen
        self.present(aObjNav, animated: true, completion: nil)
    }
    
    func comingSoon() {
        DispatchQueue.main.async(){
            alertViewObj.wrapAlert(title: "", body: "Coming Soon!", img: nil)
            alertViewObj.addAction(title: "OK", style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
            progressViewObj.removeProgressView()
        }
    }
    
    func navigateTo_Train_Bus_Toll_Ferry(type: TransportationEnum) {
        let payto = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PayToNavigation")
        isPayToDashboard = false
        if let navController = payto as? UINavigationController {
            for controller in navController.viewControllers {
                if let transportVC = controller as? PayToViewController {
                    switch type {
                    case .bus:
                        transportVC.headerTitle = "Bus Ticket".localized
                        let payToType = PayToType(type: .payTo)
                        payToType.categoryType = .bus
                        transportVC.transType = payToType
                    case .toll:
                        transportVC.headerTitle = "Toll".localized
                        let payToType = PayToType(type: .payTo)
                        payToType.categoryType = .toll
                        transportVC.transType = payToType
                    case .ferry:
                        transportVC.headerTitle = "Ferry".localized
                        let payToType = PayToType(type: .payTo)
                        payToType.categoryType = .ferry
                        transportVC.transType = payToType
                    }
                }
            }
        }
        payto.modalPresentationStyle = .fullScreen
        self.present(payto, animated: true, completion: nil)
    }
    
    func navigateToVoucher() {
       OKPayment.main.authenticate(screenName: "VoucherSession", delegate: self)
    }
    
    func navigateVoucher() {
        _ =   profileObj.callUpdateProfileApi(aCode: UserModel.shared.mobileNo, handler: { (success) in
            if success {
                DispatchQueue.main.async {
                    let story = UIStoryboard.init(name: "BookOnOKDollar", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
                    vc?.type = WebType.giftCard
                    vc?.modalPresentationStyle = .fullScreen
                    self.present(vc!, animated: true, completion: nil)
                }
            }
        })
    }
    
    func navigateToFoodWallet() {
        println_debug("navigateToFoodWallet")
        DispatchQueue.main.async(){
            alertViewObj.wrapAlert(title: "", body: "Food Wallet Coming Soon!".localized, img: #imageLiteral(resourceName: "dashboard_food_wallet"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel , action: {
            })
            alertViewObj.showAlert(controller: self)
            progressViewObj.removeProgressView()
        }
    }
    
    func navigateToMerchantPayment() {
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "MerchantPaySession", delegate: self)
        }
        else {
            navigateMerchantPay()
        }
    }
    
    func navigateMerchantPay() {
        println_debug("navigateToMerchantPayment")
        _ =   profileObj.callUpdateProfileApi(aCode: UserModel.shared.mobileNo, handler: { (success) in
            if success {
                DispatchQueue.main.async {
                    let story = UIStoryboard.init(name: "BookOnOKDollar", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "WebViewController") as? WebViewController
                    vc?.type = WebType.merchantPayment
                    vc?.modalPresentationStyle = .fullScreen
                    self.present(vc!, animated: true, completion: nil)
                }
            }
        })
    }
    
    func navigateToMobileAndElectronics() {
//        if UserLogin.shared.loginSessionExpired {
//            OKPayment.main.authenticate(screenName: "OKShoppingSession", delegate: self)
//        }
//        else {
            navigateEcomerce()
//        }

//            alertViewObj.wrapAlert(title: "", body: "Mobile & Electronics Coming Soon!".localized, img: #imageLiteral(resourceName: "mobile_shopping_new.gif"))
//            alertViewObj.addAction(title: "OK".localized, style: .cancel , action: {
//            })
//            alertViewObj.showAlert(controller: self)
//            progressViewObj.removeProgressView()
    }
    
    func navigateEcomerce() {
        
        if let url = URL(string:"VMart://?param1=\(UserModel.shared.mobileNo)&?param2=\(uuid)") {
            UIApplication.shared.open(url) { (result) in
                if result {
                    // The URL was delivered successfully!
                } else {
                    println_debug("navigateToMobileAndElectronics")
                    
                    DispatchQueue.main.async {
                        
                        let viewController = UIStoryboard(name: "BookOnOKDollar", bundle: Bundle.main).instantiateViewController(withIdentifier: "Ecommerce")
                        viewController.modalPresentationStyle = .fullScreen
                        self.present(viewController, animated: true, completion: nil)
                    }
                }
            }
        }
        
    }
    
    
    func navigateAfterLogin(str: String) {
        println_debug("navigateToSunKingPayment")
        DispatchQueue.main.async {
            let viewController = UIStoryboard(name: "BookOnOKDollar", bundle: Bundle.main).instantiateViewController(withIdentifier: "sunKingPayNav")
            if let navController = viewController as? UINavigationController {
                for controller in navController.viewControllers {
                    if let transportVC = controller as? SunKingPaymentViewController {
                        transportVC.fromScreen = str
                    }
                }
            }
            viewController.modalPresentationStyle = .fullScreen
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    func navigateToSunKingorInsurancePayment(str: String) {
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: str, delegate: self)
        }
        else {
            self.navigateAfterLogin(str: str)
        }
        
    }
    
    func navigateOneSTopMart() {
        
        println_debug("navigateOneSTopMart")
          guard let url = URL(string: "VMART://") else { return }
                   if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(URL(string:"VMART://?OKRegisteredNum=\(UserModel.shared.mobileNo)&OKRegisteredDeviceId=\(uuid)")!, options: [:]
        , completionHandler: nil)
                   } else{
                       let urlStr = "https://apps.apple.com/sg/app/1-stop-mart/id1501302377"
                       if #available(iOS 10.0, *) {
                           
                           
                           UIApplication.shared.open(URL(string: urlStr)!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                           
                       } else {
                           UIApplication.shared.openURL(URL(string: urlStr)!)
                       }
                   }
//        if let url = URL(string:"VMart://?param1=\(UserModel.shared.mobileNo)&?param2=\(uuid)") {
//            UIApplication.shared.open(url) { (result) in
//                if result {
//                    // The URL was delivered successfully!
//                } else {
//                    println_debug("navigateToMobileAndElectronics")
//
//                    DispatchQueue.main.async {
//                        let viewController = UIStoryboard(name: "BookOnOKDollar", bundle: Bundle.main).instantiateViewController(withIdentifier: "Ecommerce")
//                        viewController.modalPresentationStyle = .fullScreen
//                        self.present(viewController, animated: true, completion: nil)
//                    }
//                }
//            }
//        }
        
    }
    
    func navigateToInstaPay() {
        let story = UIStoryboard.init(name: "BookOnOKDollar", bundle: nil)
        guard let vc = story.instantiateViewController(withIdentifier: "InstaPayViewController") as? InstaPayViewController else { return }
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func navigateToSendMoneyToBank() {
        let story = UIStoryboard(name: "SendMoney", bundle: nil)
        guard let sendMoneyToBankView = story.instantiateViewController(withIdentifier: "SendMoneyToBankView_ID") as? SendMoneyToBankViewController else { return }        
        let navController = BillSplitterNavController.init(rootViewController: sendMoneyToBankView)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: nil)
    }
    
    func navigateToTaxPaymentNew() {
        
     //self.showErrorAlert(errMessage: "Coming Soon".localized)
        
        let viewController = UIStoryboard(name: "NearByModule", bundle: Bundle.main).instantiateViewController(withIdentifier: NearByConstant.kNearByController) as? NearByController
        viewController?.isComingFromTaxPayment = true
        viewController?.isComingFromRecent = false
        navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        navigationController?.pushViewController(viewController!, animated: true)
    }
    
    func navigateToNearMe() {
         println_debug("navigateToNearMe")
         OKPayment.main.authenticate(screenName: "Tusar", delegate: self)
    }
    
    func navigateToScanPay() {
        let vc = UIStoryboard(name: "NewScanToPay", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: NewScanToPayHelper.KNewScanToPayViewController)) as? NewScanToPayViewController
        if let isValidViewObj = vc{
           self.navigationController?.pushViewController(isValidViewObj, animated: true)
        }
    }

    func navigateToNearByNew(isComingFromTaxPayment: Bool) {

        let viewController = UIStoryboard(name: "NearByModule", bundle: Bundle.main).instantiateViewController(withIdentifier: NearByConstant.kNearByController) as? NearByController
               viewController?.isComingFromTaxPayment = false
               viewController?.isComingFromRecent = false
               navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
               navigationController?.pushViewController(viewController!, animated: true)
        
//         let viewController = UIStoryboard(name: "NewNearMePayment", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: "NearByPaymentInitialVC")) as? NearByPaymentInitialVC
//        viewController?.modalPresentationStyle = .fullScreen
//        //viewController?.navigation = self.navigationController
//        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
}

enum TransportationEnum {
    case bus, toll, ferry
}

enum PaytoEnum {
    case cashInPayto, cashOutPayto
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
