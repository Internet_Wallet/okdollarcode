//
//  PointDistributionExtension.swift
//  OK
//
//  Created by Mohit on 3/5/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SwiftMultiSelect

extension PointDistributionViewController  {

    func updateViewsInitialLogic() {
        self.mobileNumberTxt.delegate     = self
        self.confirmMobileNumberTxt.delegate = self
    }
    
    //MARK:- Helper Methods
    func identifyCountry(withPhoneNumber prefix: String, inCountryArray countArray: Array<NSDictionary>) -> (String, String) {
        
        let countArray = revertCountryArray() as! Array<NSDictionary>
        
        var finalCodeString = ""
        var flagCountry = ""
        
        for dic in countArray {
            if let code = dic .object(forKey: "CountryCode") as? String, let flag = dic .object(forKey: "CountryFlagCode") as? String {
                if prefix.hasPrefix(code) {
                    finalCodeString = code
                    flagCountry  = flag
                }
            }
        }
        
        return (finalCodeString,flagCountry)
    }
    
    func revertCountryArray() -> NSArray {
        do {
            if let file = Bundle.main.url(forResource: "mcc_mnc", withExtension: "txt") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? NSDictionary {
                    if let arr = object.object(forKey: "data") as? NSArray {
                        arrCountry = arr
                        return arrCountry
                    }
                } else if let object = json as? [Any] {
                    println_debug(object)
                } else {
                    println_debug("JSON is invalid")
                }
            } else {
                println_debug("no file")
            }
        } catch {
            println_debug(error.localizedDescription)
        }
        return arrCountry
    }
    
    func wrapMobileLeftView() {
        
        leftViewCountry.delegate = self
        
        leftViewCountry.wrapCountryViewData(img: "myanmar", str: "+95")
        leftViewCountryForConfirmation.wrapCountryViewData(img: "myanmar", str: "+95")
        
        self.mobileNumberTxt.leftView     = leftViewCountry
        self.mobileNumberTxt.leftViewMode = UITextField.ViewMode.always
        
        self.confirmMobileNumberTxt.leftView      = leftViewCountryForConfirmation
        self.confirmMobileNumberTxt.leftViewMode  = UITextField.ViewMode.always
        
    }
    
    
    func showTextfield(textfield: UITextField) {
        textfield.isHidden = true
    }
    
    func hideTextfield(textfield: UITextField) {
        textfield.isHidden = false
    }
    
    
    func layouting() {
        for view in self.view.subviews {
            view.layoutIfNeeded()
        }
    }
    
    //MARK:- Country Selection Delegates
    func countryViewController(_ list: CountryViewController, country: Country) {
        
        self.mobileNumberTxt.text = ""
        self.confirmMobileNumberTxt.text = ""
        
        countryObject.code = country.dialCode
        countryObject.flag = country.code
        
        leftViewCountry.wrapCountryViewData(img: country.code, str: country.dialCode)
        leftViewCountryForConfirmation.wrapCountryViewData(img: country.code, str: country.dialCode)
        
        self.mobileNumberTxt.leftView = nil
        
        self.mobileNumberTxt.leftView     = leftViewCountry
        self.mobileNumberTxt.leftViewMode = UITextField.ViewMode.always
        
        self.confirmMobileNumberTxt.leftView = nil
        
        self.confirmMobileNumberTxt.leftView     = leftViewCountryForConfirmation
        self.confirmMobileNumberTxt.leftViewMode = UITextField.ViewMode.always
        
        
        
        self.nameORnumber(selectiontype: .number, codeDetails: (country.dialCode,country.code))
        
        
        self.view.layoutIfNeeded()
        list.dismiss(animated: true, completion: nil)
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: true, completion: nil)
    }
}

extension EarnPointTransferViewController {
    
    func updateViewsInitialLogic() {
        self.mobileNumberTxt.delegate     = self
        self.confirmMobileNumberTxt.delegate = self
    }
    
    
    //MARK:- Helper Methods
    func identifyCountry(withPhoneNumber prefix: String, inCountryArray countArray: Array<NSDictionary>) -> (String, String) {
        let countArray = revertCountryArray() as! Array<NSDictionary>
        
        var finalCodeString = ""
        var flagCountry = ""
        
        for dic in countArray {
            if let code = dic .object(forKey: "CountryCode") as? String, let flag = dic .object(forKey: "CountryFlagCode") as? String {
                if prefix.hasPrefix(code) {
                    finalCodeString = code
                    flagCountry  = flag
                }
            }
        }
        
        return (finalCodeString,flagCountry)
    }
    
    func revertCountryArray() -> NSArray {
        do {
            if let file = Bundle.main.url(forResource: "mcc_mnc", withExtension: "txt") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? NSDictionary {
                    if let arr = object.object(forKey: "data") as? NSArray {
                        arrCountry = arr
                        return arrCountry
                    }
                } else if let object = json as? [Any] {
                    println_debug(object)
                } else {
                    println_debug("JSON is invalid")
                }
            } else {
                println_debug("no file")
            }
        } catch {
            println_debug(error.localizedDescription)
        }
        return arrCountry
    }
    
    
    func wrapMobileLeftView() {
        
        leftViewCountry.delegate = self
        
        leftViewCountry.wrapCountryViewData(img: "myanmar", str: "+95")
        leftViewCountryForConfirmation.wrapCountryViewData(img: "myanmar", str: "+95")
        
        self.mobileNumberTxt.leftView     = leftViewCountry
        self.mobileNumberTxt.leftViewMode = UITextField.ViewMode.always
        
        self.confirmMobileNumberTxt.leftView      = leftViewCountryForConfirmation
        self.confirmMobileNumberTxt.leftViewMode  = UITextField.ViewMode.always

    }
    
    
    func showTextfield(textfield: UITextField) {
        textfield.isHidden = true
    }
    
    func hideTextfield(textfield: UITextField) {
        textfield.isHidden = false
    }
    
    
    func layouting() {
        for view in self.view.subviews {
            view.layoutIfNeeded()
        }
    }
    
    //MARK:- Country Selection Delegates
    func countryViewController(_ list: CountryViewController, country: Country) {
        
        self.mobileNumberTxt.text = ""
        self.confirmMobileNumberTxt.text = ""
        
        countryObject.code = country.dialCode
        countryObject.flag = country.code
        
        leftViewCountry.wrapCountryViewData(img: country.code, str: country.dialCode)
        leftViewCountryForConfirmation.wrapCountryViewData(img: country.code, str: country.dialCode)
        
        self.mobileNumberTxt.leftView = nil
        
        self.mobileNumberTxt.leftView     = leftViewCountry
        self.mobileNumberTxt.leftViewMode = UITextField.ViewMode.always
        
        self.confirmMobileNumberTxt.leftView = nil
        
        self.confirmMobileNumberTxt.leftView     = leftViewCountryForConfirmation
        self.confirmMobileNumberTxt.leftViewMode = UITextField.ViewMode.always
        self.mobClearButton.isHidden = true
        self.confMobClearButton.isHidden = true
        
        
        self.nameORnumber(selectiontype: .number, codeDetails: (country.dialCode,country.code))
        
        
        self.view.layoutIfNeeded()
        list.dismiss(animated: true, completion: nil)
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: true, completion: nil)
    }
}
