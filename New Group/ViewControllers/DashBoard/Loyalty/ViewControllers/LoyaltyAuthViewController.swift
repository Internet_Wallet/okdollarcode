//
//  LoyaltyAuthViewController.swift
//  OK
//
//  Created by Mohit on 3/9/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit



class LoyaltyAuthViewController: OKBaseController, BioMetricLoginDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        OKPayment.main.authenticate(screenName: "LoyaltyList", delegate: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        print(screen)
        print(isSuccessful)
        if isSuccessful {
            if let loyaltyHomeVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: loyaltyMainViews.viewControllers.loyaltyHomeVC) as? LoyaltyHomeViewController {
            loyaltyHomeVC.delegate = self
            showSelectedViewController(viewController: loyaltyHomeVC)
            }
        }
    }
    private func showSelectedViewController(viewController : UIViewController)
    {
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }

}

extension LoyaltyAuthViewController : dismissLoyaltyView
{
    func dismissLoyaltyController() {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
