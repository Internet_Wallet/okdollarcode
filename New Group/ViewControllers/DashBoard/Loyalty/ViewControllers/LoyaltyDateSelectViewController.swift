//
//  RequestMoneyDateSelectViewController.swift
//  OK
//
//  Created by PC on 5/18/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol loyaltyDateSelectForSort : class {
    func okButtonAction(fromDate : Date, toDate : Date)
    func resetButtonAction()
}

class LoyaltyDateSelectViewController: LoyaltyBaseViewController {
    var startSeldate: Date?
    var endSelDate: Date?
    
    @IBOutlet weak var resetButton : UIBarButtonItem!
    @IBOutlet weak var lblFrom : UILabel! {
        didSet {
            self.lblFrom.font = UIFont(name: appFont, size: appFontSize)
            lblFrom.text = lblFrom?.text?.localized
        }
    }
    @IBOutlet weak var lblTo : UILabel! {
        didSet {
            self.lblTo.font = UIFont(name: appFont, size: appFontSize)
            lblTo.text = lblTo?.text?.localized
        }
    }
    
    @IBOutlet weak var cancelButton : UIButton! {
        didSet {
            cancelButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            cancelButton.backgroundColor = UIColor.red
            cancelButton.setTitle(cancelButton.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var okButton : UIButton! {
        didSet {
            okButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            okButton.backgroundColor = UIColor.red
            okButton.setTitle(okButton.titleLabel?.text?.localized, for: .normal)
        }
    }
    

    @IBOutlet weak var fromDatePicker : UIDatePicker!
    @IBOutlet weak var toDatePicker : UIDatePicker! 
    
    @IBOutlet weak var durationLabel : UILabel!{
        didSet{
            self.durationLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    var fromDate = Date()
    var toDate = Date()
    var dateFormatter = DateFormatter()
    weak var delegate : loyaltyDateSelectForSort?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        if #available(iOS 13.4, *) {
            fromDatePicker.preferredDatePickerStyle = .wheels
            toDatePicker.preferredDatePickerStyle = .wheels
                   } else {
                       // Fallback on earlier versions
                   }
        let dateMax = Calendar.current.date(byAdding: .year, value: 25, to: Date())
        fromDatePicker.minimumDate = Date()
        toDatePicker.minimumDate = Date()
        fromDatePicker.maximumDate = dateMax
        toDatePicker.maximumDate = dateMax
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        self.title = "Loyalty".localized
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize)
, NSAttributedString.Key.foregroundColor: UIColor.white ]
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        fromDatePicker.backgroundColor = .white
        toDatePicker.backgroundColor = .white
        if let dateStr = startSeldate {
            fromDatePicker.date = dateStr
            fromDate = dateStr
        }
        if let dateEnd = endSelDate {
            toDatePicker.date = dateEnd
            toDate = dateEnd
        }
        self.calculateDay()
        self.getRightBarButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func getRightBarButton() {
        let barButton = self.navigationItem.rightBarButtonItem
        barButton?.setTitleTextAttributes([
            NSAttributedString.Key.font: UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: 17.0)],
                                          for: .normal)
        barButton?.title = barButton?.title?.localized
    }
    
    @IBAction func okButtonTapped(_ sender : UIButton) {
        if fromDate > toDate {
            self.showCustomErrorAlert(errMessage: "From date should be less than To date".localized)
        } else {
            delegate?.okButtonAction(fromDate: fromDate, toDate: toDate)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender : UIButton) {
         self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func fromDatePickerChange(_ sender: Any) {
        if fromDatePicker.date > toDate {
            fromDatePicker.setDate(fromDate, animated: true)
            self.showCustomErrorAlert(errMessage: "From date should be less than To date".localized)
        } else {
            fromDate = fromDatePicker.date
            self.calculateDay()
        }
    }
    
    @IBAction func toDatePickerChange(_ sender: Any) {
        if toDatePicker.date < fromDate {
            toDatePicker.setDate(toDate, animated: true)
           self.showCustomErrorAlert(errMessage: "To date should be greater than From date".localized)
        } else {
            toDate = toDatePicker.date
            self.calculateDay()
        }
    }
    
    func calculateDay()  {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss SSS"
        var toDateStr = dateFormat.string(from: toDatePicker.date)
        toDateStr = toDateStr.components(separatedBy: " ").first ?? ""
        toDateStr = "\(toDateStr) 23:59:59"
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let toDateConverted = dateFormat.date(from: toDateStr)
       
        let calendar = Calendar.current
        let componentsDay = calendar.dateComponents([.day], from: fromDatePicker.date, to: toDateConverted ?? toDatePicker.date).day ?? 0
        durationLabel.text = "Duration".localized + ": " + "\(componentsDay + 1) days"
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func resetAction(_ sender: Any) {
        delegate?.resetButtonAction()
        self.navigationController?.popViewController(animated: false)
    }

}

extension Date {
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return tomorrow.month != month
    }
    
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
    
}


