//
//  bonusConfigurationViewController.swift
//  OK
//
//  Created by Kethan on 2/27/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct bonusConfigViews {
    static let loyaltyStoryBoard = "LoyaltyModule"
    struct viewControllers {
        static let displayBonusConfigVC = "DisplayBonusConfigViewController_ID"
        static let pointDistributionVC = "PointDistributionViewController_ID"
        static let createShopNameVC = "CreateShopNameViewController_ID"
        static let loyaltyTopUpVC = "LoyaltyTopUpViewController_ID"
        static let shopDetailsVC = "ShopDetailsViewController_ID"
    }
    
}

class BonusConfigurationViewController: LoyaltyBaseViewController {

    var availablePoint = "0"
    var bonusConfigListArray = [Dictionary<String,Any>]()
    var bonusConfigDict = Dictionary<String,Any>()
    
    weak var delegate : pointDistributionProtocol?
    
    var balanceListArray = [Dictionary<String,Any>]()
    var balanceDict = Dictionary<String,Any>()

    @IBOutlet weak var earnPointLbl: UILabel!{
        didSet {
            self.earnPointLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var earnPointLblText: UILabel!{
        didSet {
            self.earnPointLblText.font = UIFont(name: appFont, size: appFontSize)
            self.earnPointLblText.text = self.earnPointLblText.text?.localized
        }
    }
    @IBOutlet weak var availablePointLbl: UILabel!{
        didSet {
            self.availablePointLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var availablePointLblText: UILabel! {
        didSet {
            self.availablePointLblText.font = UIFont(name: appFont, size: appFontSize)
            self.availablePointLblText.text = self.availablePointLblText.text?.localized
        }
    }
    @IBOutlet weak var merchantConfigTableView : UITableView!
    var bonusConfigMainDictionary = [Dictionary<String, String>]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addBckButton(navTitle : "Bonus Point Setup".localized)
        let footerView = UIView()
        
        initializeMerchantDictionary()
        footerView.backgroundColor = UIColor.gray
        merchantConfigTableView.tableFooterView = footerView
        getAvalaibleBalance()
        getLoyaltyDetails()
        profileObj.callUpdateProfileApi(aCode: UserModel.shared.mobileNo, handler: { (success) in
             if success {
             }else {
                 println_debug("callUpdateProfileApi Failed")
             }
         })
    }
    
    
    private func addBckButton(navTitle: String) {
        let backButton = UIButton(type: .custom)
        backButton.setBackgroundImage(#imageLiteral(resourceName: "tabBarBack"), for: .normal)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        backButton.addTarget(self, action: #selector(bckAction), for: .touchUpInside)
        navigationItem.title = navTitle.localized
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white ]
        self.navigationController?.navigationBar.barTintColor = UIColor.red
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    @objc func bckAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func initializeMerchantDictionary() {
        if UserModel.shared.agentType == .advancemerchant {
            bonusConfigMainDictionary = [["Title" : "Bonus Configuration", "Image" : "merchant_confi.png"],["Title" : "Dummy Wallet Configuration", "Image" : "dummy_wallet.png"], ["Title" : "Point Distribution", "Image" : "point_distribution.png"], ["Title" : "Create Shop Name", "Image" : "shop_details.png"], ["Title" : "Shop Details", "Image" : "shop_details.png"], ["Title" : "Loyalty Top Up", "Image" : "loyalty_topup.png"], ["Title" : "Loyalty Wallet Transfer", "Image" : "loyalty_wallet.png"],["Title" : "Loyalty Pullout", "Image" : "loyalty_pullout.png"]]
        } else if UserModel.shared.agentType == .merchant || UserModel.shared.agentType == .agent {
           bonusConfigMainDictionary = [["Title" : "Bonus Configuration", "Image" : "merchant_confi.png"], ["Title" : "Point Distribution", "Image" : "point_distribution.png"], ["Title" : "Create Shop Name", "Image" : "shop_details.png"], ["Title" : "Shop Details", "Image" : "shop_details.png"], ["Title" : "Loyalty Top Up", "Image" : "loyalty_topup.png"]]
        }
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func selectedIndex(selectedIndex : String) {
        switch selectedIndex {
        case "Bonus Configuration":
            if let bonusConfigSubVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: bonusConfigViews.viewControllers.displayBonusConfigVC) as? DisplayBonusConfigViewController {
                bonusConfigSubVC.bonusConfigListArray = self.bonusConfigListArray
                showSelectedViewController(viewController: bonusConfigSubVC)
            }
        case "Point Distribution":
            if let pointDistributionVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: bonusConfigViews.viewControllers.pointDistributionVC) as? PointDistributionViewController {
                pointDistributionVC.delegate = self
                pointDistributionVC.availablePoint = availablePointLbl.text ?? "0"
                showSelectedViewController(viewController: pointDistributionVC)
            }
        case "Create Shop Name":
            if let createShopNameVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: bonusConfigViews.viewControllers.createShopNameVC) as? CreateShopNameViewController {
                showSelectedViewController(viewController: createShopNameVC)
            }
        case "Shop Details":
        if let shopDetailsVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: bonusConfigViews.viewControllers.shopDetailsVC) as? ShopDetailsViewController {
                showSelectedViewController(viewController: shopDetailsVC)
            }
        case "Loyalty Top Up":
            if let loyaltyTopUpVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: bonusConfigViews.viewControllers.loyaltyTopUpVC) as? LoyaltyTopUpViewController {
                loyaltyTopUpVC.delegate = self
                loyaltyTopUpVC.availablePoint = availablePointLbl.text ?? "0"
                showSelectedViewController(viewController: loyaltyTopUpVC)
            }
        case "Dummy Wallet Configuration":
            if let dummyWalCreateVC = UIStoryboard(name: loyaltyMainViews.loyaltyAdvModuleStory, bundle: nil).instantiateViewController(withIdentifier: loyaltyMainViews.viewControllers.selectAdvMerchantVC) as? SelectAdvanceMerchantViewController {
                dummyWalCreateVC.headerTitle = "Dummy Wallet Configuration"
                dummyWalCreateVC.bonusPoints = availablePointLbl.text ?? "0"
                showSelectedViewController(viewController: dummyWalCreateVC)
            }
        case "Loyalty Wallet Transfer":
            if let loyaltyWalTrfVC = UIStoryboard(name: loyaltyMainViews.loyaltyAdvModuleStory, bundle: nil).instantiateViewController(withIdentifier: loyaltyMainViews.viewControllers.selectAdvMerchantVC) as? SelectAdvanceMerchantViewController {
                loyaltyWalTrfVC.headerTitle = "Loyalty Wallet Transfer"
                loyaltyWalTrfVC.bonusPoints = availablePointLbl.text ?? "0"
                loyaltyWalTrfVC.delegate = self
                showSelectedViewController(viewController: loyaltyWalTrfVC)
            }
        case "Loyalty Pullout":
            if let loyaltyPullOutVC = UIStoryboard(name: loyaltyMainViews.loyaltyAdvModuleStory, bundle: nil).instantiateViewController(withIdentifier: loyaltyMainViews.viewControllers.selectAdvMerchantVC) as? SelectAdvanceMerchantViewController {
                loyaltyPullOutVC.headerTitle = "Loyalty Pullout"
                loyaltyPullOutVC.bonusPoints = availablePointLbl.text ?? "0"
                loyaltyPullOutVC.delegate = self
                showSelectedViewController(viewController: loyaltyPullOutVC)
            }
            
        default:
            println_debug("")
        }
    }
    
    private func showSelectedViewController(viewController : UIViewController) {
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    private func getAvalaibleBalance() {
        if appDelegate.checkNetworkAvail() {
            let web      = PayToWebApi()
            web.delegate = self
            var ip = ""
            if let ipAdd = OKBaseController.getIPAddress() {
                ip = ipAdd
            }
            let urlStr   = String.init(format: Url.CheckLoyaltyBalanceApi, UserModel.shared.mobileNo, ok_password ?? "", UserModel.shared.mobileNo, ip , "ios", UserLogin.shared.token)
            let url = getUrl(urlStr: urlStr, serverType: .serverEstel)
                        println_debug(url)
            let pRam = Dictionary<String,String>()
            web.genericClassXML(url: url, param: pRam as AnyObject, httpMethod: "POST", mScreen: "mAvailableBalance")
        }
    }
    
    private func getLoyaltyDetails() {
        if appDelegate.checkNetworkAvail() {
            let web      = PayToWebApi()
            web.delegate = self
            var ip = ""
            if let ipAdd = OKBaseController.getIPAddress() {
                ip = ipAdd
            }
            let urlStr   = String.init(format: Url.bonusConfigApiUrl, UserModel.shared.mobileNo, ip , "ios", UserLogin.shared.token)
            let url = getUrl(urlStr: urlStr, serverType: .serverEstel)
//            println_debug(url)
            let pRam = Dictionary<String,String>()
            web.genericClassXML(url: url, param: pRam as AnyObject, httpMethod: "POST", mScreen: "mBonusConfigList")
        } else {
            self.noInternetAlert()
        }
    }
}

extension BonusConfigurationViewController : pointDistributionProtocol {
    func currentPoint(pointText: String) {
        self.availablePointLbl.text = pointText
        delegate?.currentPoint(pointText: pointText)
    }
}


//PTWebresponseDelegate
extension BonusConfigurationViewController : PTWebResponseDelegate {
    func webSuccessResult(data: Any, screen: String) {
//        println_debug(data)
//        println_debug(screen)
        
        if screen == "mBonusConfigList" {
            if let xmlString = data as? String {
                let xml = SWXMLHash.parse(xmlString)
                
                self.bonusConfigDict.removeAll()
                self.bonusConfigListArray.removeAll()
                self.enumerateAlert(indexer: xml)
                //                println_debug(bonusConfigListArray)
            }
            self.removeProgressView()
        } else if screen == "mAvailableBalance" {
            if let xmlString = data as? String {
                let xml = SWXMLHash.parse(xmlString)
                
                balanceDict.removeAll()
                self.enumerateBalance(indexer: xml)
                self.balanceListArray.removeAll()
                self.balanceListArray.append(balanceDict)
                              println_debug(balanceListArray)
                self.removeProgressView()
                if balanceDict["resultdescription"] as? String == "Transaction Successful" {
                    DispatchQueue.main.async {
                        let formatter = NumberFormatter()
                        formatter.numberStyle = .decimal
                        if UserModel.shared.agentType == .user {
                            let earnBal = self.balanceDict["udv5"] as? String ?? "0"
                            let earnBalArray = earnBal.components(separatedBy: "|")
                            if  earnBalArray.count == 2 {
                                if let numberStr = formatter.string(from: Float(earnBalArray[1])! as NSNumber) {
                                    self.earnPointLbl.text = numberStr
                                    UserDefaults.standard.set(self.earnPointLbl.text!, forKey: "MyEarnPoint")
                                }
                            }
                        } else {
                            let availableBal = self.balanceDict["udv4"] as? String ?? "0"
                            let availableBalArray = availableBal.components(separatedBy: "|")
                            if  availableBalArray.count == 2 {
                                if let numberStr = formatter.string(from: Float(availableBalArray[1])! as NSNumber) {
                                    self.availablePointLbl.text = numberStr
                                }
                            }
                            let earnBal = self.balanceDict["udv5"] as? String ?? "0.0"
                            let earnBalArray = earnBal.components(separatedBy: "|")
                            if  earnBalArray.count == 2 {
                                if let numberStr = formatter.string(from: Float(earnBalArray[1])! as NSNumber) {
                                    self.earnPointLbl.text = numberStr
                                    UserDefaults.standard.set(self.earnPointLbl.text!, forKey: "MyEarnPoint")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            bonusConfigDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func enumerateBalance(indexer: XMLIndexer) {
        for child in indexer.children {
            balanceDict[child.element!.name] = child.element!.text
            enumerateBalance(indexer: child)
        }
    }
    
    func enumerateAlert(indexer: XMLIndexer) {
        for child in indexer.children {
            if child.element?.name == "record" {
                self.enumerate(indexer: child)
                bonusConfigListArray.append(bonusConfigDict)
                bonusConfigDict.removeAll()
            }
            enumerateAlert(indexer: child)
        }
    }
    
    func webFailureResult(screen: String) {
        println_debug("Failure")
    }
}

extension BonusConfigurationViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let title = self.bonusConfigMainDictionary[indexPath.row]["Title"] {
            selectedIndex(selectedIndex: title)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

extension BonusConfigurationViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: LoyaltyMainTableViewCell = tableView.dequeueReusableCell(withIdentifier: loyaltyMainViews.tableViewCells.loyaltyMainTableCell, for: indexPath) as! LoyaltyMainTableViewCell
        cell.loadCell(dict: bonusConfigMainDictionary[indexPath.row])
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bonusConfigMainDictionary.count
    }
    
}



