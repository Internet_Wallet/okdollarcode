//
//  MerchantListTableViewCell.swift
//  OK
//
//  Created by Kethan on 3/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class MerchantListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var merchantNameLbl : UILabel!{
        didSet{
            self.merchantNameLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var pointsLbl : UILabel!{
        didSet{
            self.pointsLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadCell(dictionary : Dictionary<String, Any>) {
        let points = wrapAmountWithCommaDecimal(key: "\(dictionary["points"] as? String ?? "")").replacingOccurrences(of: ".0", with: "")
        let name = "\(dictionary["name"] as? String ?? "") / \(points)"
        merchantNameLbl.text = name
    }
    
    func loadBonusCell(dictionary : merchantDetailsModel) {
        let dict = dictionary.merchant
        merchantNameLbl.text = dict["shopName"] as? String ?? ""
        if let points = dictionary.pointBalance {
            var pointTxt = " Points"
            if points == 1 {
                pointTxt = " Point"
            }
            pointsLbl.text = wrapAmountWithCommaDecimal(key: "\(points)").replacingOccurrences(of: ".0", with: "") + pointTxt
        }
    }

}
