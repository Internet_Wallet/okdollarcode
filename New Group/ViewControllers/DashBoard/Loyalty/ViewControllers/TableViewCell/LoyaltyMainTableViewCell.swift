//
//  LoyaltyMainTableViewCell.swift
//  OK
//
//  Created by Kethan on 2/26/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class LoyaltyMainTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var loyaltyMainTableImageView: UIImageView!
    @IBOutlet weak var loyaltyMainTableTitle: UILabel!{
        didSet{
            self.loyaltyMainTableTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.accessoryType = .disclosureIndicator
        self.tintColor = UIColor.darkGray
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadCell(dict : Dictionary<String, String>) {
        if let imageName = dict.safeValueForKey("Image") {
            loyaltyMainTableImageView.image = UIImage(named: imageName)
        }
        if let title = dict["Title"] {
            loyaltyMainTableTitle.text = title.localized
        }
    }

}
