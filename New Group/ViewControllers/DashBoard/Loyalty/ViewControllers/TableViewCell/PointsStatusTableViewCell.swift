//
//  PointsStatusTableViewCell.swift
//  OK
//
//  Created by Kethan on 3/7/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PointsStatusTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameImageView : UIImageView! {
        didSet {
            self.nameImageView.layer.cornerRadius = self.nameImageView.frame.size.width / 2.0
            self.nameImageView.layer.borderWidth = 0.5
            self.nameImageView.layer.borderColor = UIColor.gray.cgColor
            self.nameImageView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        }
    }
    
    @IBOutlet weak var merchantNameLbl : UILabel!{
        didSet{
            self.merchantNameLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var merchantMobileNoLbl : UILabel!{
        didSet{
            self.merchantMobileNoLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var merchantPointsLbl : UILabel!{
        didSet{
            self.merchantPointsLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var merchantSellingPointsLabel : UILabel!{
        didSet{
            self.merchantSellingPointsLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var nameLabel : UILabel!{
        didSet{
            self.nameLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func loadCell(dictionary : buyBonusPointModel) {
             
        let dict = dictionary.merchant
        if let name = (dict.safeValueForKey("shopName") as? String)?.uppercased() {
            self.nameLabel.text = String(name.first!)
        }
        self.merchantNameLbl.text = dict.safeValueForKey("shopName") as? String ?? ""
        let mobileNum = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: dict.safeValueForKey("merchantAccountNo") as? String ?? "")
        self.merchantMobileNoLbl.text = "(\(mobileNum.counrtycode))\(mobileNum.mobileNo)"
        if let originalPoints = dictionary.originalPrice {
            self.merchantPointsLbl.text = wrapAmountWithCommaDecimal(key: "\(originalPoints)").replacingOccurrences(of: ".0", with: "") + " " + "Points".localized
        }
        if let sellingPrice = dictionary.sellingPrice {
            self.merchantSellingPointsLabel.text = wrapAmountWithCommaDecimal(key: "\(sellingPrice)").replacingOccurrences(of: ".0", with: "") + " " + "MMK".localized
        }
    }

}


class BonusPointTabTableViewCell : UITableViewCell {
    @IBOutlet weak var nameImageView : UIImageView! {
        didSet {
            self.nameImageView.layer.cornerRadius = self.nameImageView.frame.size.width / 2.0
            self.nameImageView.layer.borderWidth = 0.5
            self.nameImageView.layer.borderColor = UIColor.gray.cgColor
            self.nameImageView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        }
    }
    
    @IBOutlet weak var merchantNameLbl : UILabel!{
        didSet{
            self.merchantNameLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var merchantMobileNoLbl : UILabel!{
        didSet{
            self.merchantMobileNoLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var merchantPointsLbl : UILabel!{
        didSet{
            self.merchantPointsLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var nameLabel : UILabel!{
        didSet{
            self.nameLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func loadCell(dictionary : Dictionary<String, Any>) {
        
        if let name = (dictionary.safeValueForKey("name") as? String)?.uppercased() {
            self.nameLabel.text = String(name.first!)
        }
        self.merchantNameLbl.text = dictionary.safeValueForKey("name") as? String ?? ""
        self.merchantMobileNoLbl.text = (dictionary.safeValueForKey("mobile") as? String ?? "").replacingOccurrences(of: "0095", with: "(+95)0")
        let pointsText = dictionary.safeValueForKey("points") as? String ?? "0"
        let pointsTextInt = Int((pointsText as NSString).intValue)
        self.merchantPointsLbl.text = self.getDigitDisplay(digit: pointsTextInt)//String(pointsTextInt)
    }
    
    func getDigitDisplay(digit : Int) -> String {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        return formatter.string(from: digit as NSNumber)!
    }
}
