//
//  BonusConfigSubTableViewCell.swift
//  OK
//
//  Created by Kethan on 2/27/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BonusConfigSubTableViewCell: UITableViewCell {

    @IBOutlet weak var maxRangeLbl: UILabel! {
        didSet{
            self.maxRangeLbl.font = UIFont(name: appFont, size: appFontSize)
            maxRangeLbl.text = maxRangeLbl.text?.localized
        }
    }
    @IBOutlet weak var minRangeLbl: UILabel! {
        didSet{
            self.minRangeLbl.font = UIFont(name: appFont, size: appFontSize)
            minRangeLbl.text = minRangeLbl.text?.localized
        }
    }
    @IBOutlet weak var perAmountLbl: UILabel! {
        didSet{
            self.perAmountLbl.font = UIFont(name: appFont, size: appFontSize)
            perAmountLbl.text = perAmountLbl.text?.localized
        }
    }
    @IBOutlet weak var bonusPointLbl: UILabel! {
        didSet{
            self.bonusPointLbl.font = UIFont(name: appFont, size: appFontSize)
            bonusPointLbl.text = bonusPointLbl.text?.localized
        }
    }
    @IBOutlet weak var startDateLbl: UILabel! {
        didSet{
            self.startDateLbl.font = UIFont(name: appFont, size: appFontSize)
            startDateLbl.text = startDateLbl.text?.localized
        }
    }
    @IBOutlet weak var endDateLbl: UILabel! {
        didSet{
            self.endDateLbl.font = UIFont(name: appFont, size: appFontSize)
            endDateLbl.text = endDateLbl.text?.localized
        }
    }
    @IBOutlet weak var expiryDateLbl: UILabel! {
        didSet{
            self.expiryDateLbl.font = UIFont(name: appFont, size: appFontSize)
            expiryDateLbl.text = expiryDateLbl.text?.localized
        }
    }
    
    @IBOutlet weak var maxRangeLblTxt: UILabel!{
        didSet{
            self.maxRangeLblTxt.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var minRangeLblTxt: UILabel!{
        didSet{
            self.minRangeLblTxt.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var perAmountLblTxt: UILabel!{
        didSet{
            self.perAmountLblTxt.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var bonusPointLblTxt: UILabel!{
        didSet{
            self.bonusPointLblTxt.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var startDateLblTxt: UILabel!{
        didSet{
            self.startDateLblTxt.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var endDateLblTxt: UILabel!{
        didSet{
            self.endDateLblTxt.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var expiryDateLblTxt: UILabel!{
        didSet{
            self.expiryDateLblTxt.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    let dateFormatter = DateFormatter()
    let apiDateFormatter = DateFormatter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func loadCell(bonusConfigSubArray : Dictionary<String, Any>) {
        if let value = bonusConfigSubArray.safeValueForKey("maxrange") as? String {
            maxRangeLblTxt.text = value.replacingOccurrences(of: ".00", with: "").replacingOccurrences(of: ".0", with: "")
        }
        if let value = bonusConfigSubArray.safeValueForKey("minrange") as? String {
            minRangeLblTxt.text = value.replacingOccurrences(of: ".00", with: "").replacingOccurrences(of: ".0", with: "")
        }
        if let value = bonusConfigSubArray.safeValueForKey("peramount") as? String {
            perAmountLblTxt.text = value.replacingOccurrences(of: ".00", with: "").replacingOccurrences(of: ".0", with: "")
        }
        if let value = bonusConfigSubArray.safeValueForKey("point") as? String {
            bonusPointLblTxt.text = value
        }
        if let value = bonusConfigSubArray.safeValueForKey("startdate") as? String {
            startDateLblTxt.text = getDateString(dateStr: value)
        }
        if let value = bonusConfigSubArray.safeValueForKey("enddate") as? String {
            endDateLblTxt.text = getDateString(dateStr: value)
        }
        if let value = bonusConfigSubArray.safeValueForKey("expiry") as? String {
            expiryDateLblTxt.text = value
        }
        
    }
    
    func getDateString(dateStr : String) -> String {
        apiDateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        apiDateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        var date = ""
        if dateStr.contains(find: " ") {
            let str = dateStr.components(separatedBy: " ")
            let str1 = str[0]
            if let apiDate = apiDateFormatter.date(from: str1) {
                date = dateFormatter.string(from: apiDate)
                return date
            }
        }else {
            return date
        }
        return date
    }

}
