//
//  LoyaltyRedeemPointsViewController.swift
//  OK
//
//  Created by Kethan on 8/9/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
struct  loyaltyRecieptModel {
    var agentCode : String?
    var receiverNumber : String?
    var recieverName : String?
    var transactionID : String?
    var transactionType : String?
    var transactionDate : String?
    var amount : String?
    
    init(dict : Dictionary<String , Any>) {
        self.agentCode = UserModel.shared.mobileNo
        self.receiverNumber = dict["merchant"] as? String ?? ""
        self.recieverName = dict["agentname"] as? String ?? ""
        self.transactionID = dict["transid"] as? String ?? ""
        self.transactionType = "BONUS"
        self.transactionDate = dict["responsects"] as? String ?? ""
        self.amount = dict["amount"] as? String ?? "0"
    }
}

struct loyaltyPointTrfModel {
    var receiverNo: String?
    var merchantName: String?
    var balance: String?
    var transactionDate : String?
    var transactionID : String?
    var points: String?
    var name: String?
    var category: String?
    init(dict : Dictionary<String , Any>) {
        self.receiverNo = dict["destination"] as? String ?? ""
        self.balance = dict["walletbalance"] as? String ?? ""
        self.transactionID = dict["transid"] as? String ?? ""
        self.transactionDate = dict["responsects"] as? String ?? ""
        self.points = dict["amount"] as? String ?? "0"
        self.name = dict["agentname"] as? String ?? ""
    }
}

class LoyaltyRedeemPointsViewController: LoyaltyBaseViewController, UINavigationControllerDelegate, UITextFieldDelegate, CountryLeftViewDelegate, CountryViewControllerDelegate, PTWebResponseDelegate, BioMetricLoginDelegate {
    //MARK: - Outlets
    @IBOutlet weak var heightMobNum: NSLayoutConstraint!
    @IBOutlet weak var heightShopName: NSLayoutConstraint!
    @IBOutlet weak var mobileNumberView : UIView! {
        didSet {
            self.mobileNumberView.isHidden = false
        }
    }
    @IBOutlet weak var shopNameView : UIView! {
        didSet {
            self.shopNameView.isHidden = false
        }
    }
    @IBOutlet weak var redeemTopLabel : UILabel! {
        didSet {
            self.redeemTopLabel.font = UIFont(name: appFont, size: appFontSize)
            self.redeemTopLabel.text = self.redeemTopLabel.text?.localized
        }
    }
    @IBOutlet weak var redeemTableView : UITableView!
    @IBOutlet weak var pointsText : UITextField! {
        didSet {
            self.pointsText.font = UIFont(name: appFont, size: appFontSize)
            self.pointsText.placeholder = self.pointsText.placeholder?.localized
        }
    }
    @IBOutlet weak var mobileNumberTxt : UITextField! {
        didSet {
            self.mobileNumberTxt.font = UIFont(name: appFont, size: appFontSize)
            self.mobileNumberTxt.placeholder = self.mobileNumberTxt.placeholder?.localized
            self.mobileNumberTxt.isUserInteractionEnabled = false
        }
    }
    @IBOutlet weak var shopNameTxt : UITextField! {
        didSet {
            self.shopNameTxt.font = UIFont(name: appFont, size: appFontSize)
            self.shopNameTxt.placeholder = self.shopNameTxt.placeholder?.localized
            self.shopNameTxt.isUserInteractionEnabled = false
        }
    }
    @IBOutlet weak var buttonView : UIView! {
        didSet {
            buttonView.isHidden = true
        }
    }
    @IBOutlet weak var merchantNameLbl : UILabel! {
        didSet {
           self.merchantNameLbl.font = UIFont(name: appFont, size: appFontSize)
           self.merchantNameLbl.text = self.merchantNameLbl.text?.localized
        }
    }
    @IBOutlet weak var totalPointsLbl : UILabel! {
        didSet {
            self.totalPointsLbl.font = UIFont(name: appFont, size: appFontSize)
            self.totalPointsLbl.text = self.totalPointsLbl.text?.localized
        }
    }
    @IBOutlet weak var expiryDateLbl : UILabel! {
        didSet {
            self.expiryDateLbl.font = UIFont(name: appFont, size: appFontSize)
            self.expiryDateLbl.text = "Expiry Date".localized
        }
    }
    @IBOutlet weak var redeemNowButton : UIButton! {
        didSet {
            self.redeemNowButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.redeemNowButton.isHidden = true
            self.redeemNowButton.setTitle(self.redeemNowButton.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var pointClearBtn: UIButton! {
        didSet {
            self.pointClearBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.pointClearBtn.isHidden = true
        }
    }
    
    //MARK: - Properties
    var totalPoints = 0
    var points = 0
    var merchantListDict = Dictionary<String,Any>()
    var redeemDictionary = [Dictionary<String, Any>()]
    var merchantDictionary = [Dictionary<String, Any>()]
    var merchantNumber = ""
    var recieptModel : loyaltyRecieptModel?
    var redeemSuccessListArray = [Dictionary<String,Any>]()
    var redeemSuccessDict = Dictionary<String,Any>()
    let validObj  = PayToValidations()
    let mobileNumberAcceptableCharacters = "0123456789"
    var shopName = ""
    var codedPoints = 0
    
    //AccessoryView
    var submitView: UIView {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 50))
        let btn = UIButton.init(frame: view.frame)
        btn.backgroundColor = UIColor.red
        btn.setTitle("Redeem Bonus Point".localized, for: .normal)
        if let myFont = UIFont(name: appFont, size: 18) {
            btn.titleLabel?.font =  myFont
        }
        btn.addTarget(self, action: #selector(redeemNowButtonAction(_:)), for: UIControl.Event.touchUpInside)
        view.addSubview(btn)
        return view
    }
    
    //MARK: - Views Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        redeemTableView.tableFooterView = UIView()
        addCustomBackButton(navTitle: "Bonus Points")
        self.heightMobNum.constant = 60.0
        self.heightShopName.constant = 60.0
        self.mobileNumberView.isHidden = false
        self.shopNameView.isHidden = false
        mobileNumberTxt.delegate = self
        pointsText.delegate = self
        pointsText.inputAccessoryView = self.submitView
        pointsText.inputAccessoryView?.isHidden = true
        pointsText.addTarget(self, action: #selector(AddNewBonusConfigViewController.textFieldDidChange(_:)),
                               for: UIControl.Event.editingChanged)
        setupMerchantList()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.redeemNowButton.isUserInteractionEnabled = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    //MARK: - Methods
    func openAction(type: ButtonType) {
        self.navigationController?.present(countryViewController(delegate: self), animated: true, completion: nil)
    }
    
    func clearActionType() {
        
    }
    
    func updateViewsInitialLogic() {
        self.mobileNumberTxt.delegate     = self
    }
    
    func wrapMobileLeftView() {
        
    }
    
    func showTextfield(textfield: UITextField) {
        textfield.isHidden = true
    }
    
    func hideTextfield(textfield: UITextField) {
        textfield.isHidden = false
    }
    
    func layouting() {
        for view in self.view.subviews {
            view.layoutIfNeeded()
        }
    }
    
    @IBAction func pointsClearAction(_ sender: UIButton) {
        self.pointClearBtn.isHidden = true
        self.pointsText.inputAccessoryView?.isHidden = true
        self.pointsText.text = ""
        self.redeemNowButton.isHidden = true
    }
    //MARK:- Country Selection Delegates
    func countryViewController(_ list: CountryViewController, country: Country) {
        
        self.mobileNumberTxt.text = ""
        self.view.layoutIfNeeded()
        list.dismiss(animated: true, completion: nil)
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: true, completion: nil)
    }
    
    private func getAllMerchantDetails() {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            
            let urlStr   = String.init(format: Url.redeemInitialApiCall, self.merchantDictionary[0]["mobile"] as? String ?? "")
            let url = getUrl(urlStr: urlStr, serverType: .serverApp)
            let params = [String: Any]()
            web.genericClassRegLoyalty(url: url, param: params, httpMethod: "GET", mScreen: "mMerchantList")
        } else {
            
        }
    }
    
    fileprivate func showAccessoryView(status : Bool) {
        if status {
            pointsText.inputAccessoryView?.isHidden = false
        } else {
            pointsText.inputAccessoryView?.isHidden = true
        }
    }
    
    private func setupMerchantList() {
        merchantDictionary.removeAll()
        if self.redeemDictionary.count > 0 {
            if let merchantNames = redeemDictionary[0]["merchant"] as? String, let loyaltyPoints = redeemDictionary[0]["loyaltypoints"] as? String, let merchantId = redeemDictionary[0]["merchantid"] as? String, let expireDate = redeemDictionary[0]["expirydate"] as? String {
                let merchantArrayNames = merchantNames.components(separatedBy: "|")
                let merchantLoyaltyPnts = loyaltyPoints.components(separatedBy: "|")
                let merchantMobNum = merchantId.components(separatedBy: "|")
                let expiryDateArray = expireDate.components(separatedBy: "|")
                for (index, name) in merchantArrayNames.enumerated() {
                    if merchantLoyaltyPnts.count >= index {
                        if merchantMobNum[index] == merchantNumber, name != "",  merchantMobNum[index] != "" {
                            let dict = ["name" : name, "points" : merchantLoyaltyPnts[index], "mobile" : merchantMobNum[index], "expiredate" : expiryDateArray[index]]
                            let pointsStr = merchantLoyaltyPnts[index] as NSString
                            self.points = self.points + Int(pointsStr.intValue)
                            merchantDictionary.append(dict)
                        }
                    }
                }
            }
        }
        
        if self.merchantDictionary.count > 0 {
            DispatchQueue.main.async {
                self.pointsText.text = "" //String(self.points)
                let phoneNum = self.merchantDictionary[0]["mobile"] as? String ?? ""
                var FormateStr = ""
                if phoneNum.hasPrefix("0095") {
                    FormateStr = "0\((phoneNum).substring(from: 4))"
                }
                self.mobileNumberTxt.text = FormateStr
                self.shopNameTxt.text = self.merchantDictionary[0]["name"] as? String ?? ""
                self.redeemTableView.reloadData()
                self.getAllMerchantDetails()
            }
        }
    }
    
    func identifyCountry(withPhoneNumber prefix: String, inCountryArray countArray: Array<NSDictionary>) -> (String, String) {
        
        let countArray = revertCountryArray() as! Array<NSDictionary>
        var finalCodeString = ""
        var flagCountry = ""
        
        for dic in countArray {
            if let code = dic .object(forKey: "CountryCode") as? String, let flag = dic .object(forKey: "CountryFlagCode") as? String {
                if prefix.hasPrefix(code) {
                    finalCodeString = code
                    flagCountry  = flag
                }
            }
        }
        return (finalCodeString,flagCountry)
    }
    
    func revertCountryArray() -> NSArray {
        do {
            if let file = Bundle.main.url(forResource: "mcc_mnc", withExtension: "txt") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? NSDictionary {
                    if let arr = object.object(forKey: "data") as? NSArray {
                        arrCountry = arr
                        return arrCountry
                    }
                } else if let object = json as? [Any] {
                    println_debug(object)
                } else {
                    println_debug("JSON is invalid")
                }
            } else {
                println_debug("no file")
            }
        } catch {
            println_debug(error.localizedDescription)
        }
        return arrCountry
    }
    
    func MobileNumberHandler(textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        let objectValidation = validObj.getNumberRangeValidation(text)
        
        let textCount  = text.count
        
        guard validObj.getNumberRangeValidation(text).isRejected == false else {
            loyaltyAlertView.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: UIImage(named : "AppIcon"))
            loyaltyAlertView.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                textField.text = commonPrefixMobileNumber
            })
            loyaltyAlertView.showAlert(controller: self)
            return false
        }
        
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            if textField.text == "09" {
                return false
            }
        }
        
        if let clearButton = self.view.viewWithTag(141) as? UIButton {
            if textCount > 0 {
                clearButton.isHidden = false
            } else {
                clearButton.isHidden = true
            }
        }
        
        let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: mobileNumberAcceptableCharacters).inverted
        let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
        
        if string != filteredSet {
            return false
        }
        
        // Normal Validations
        let  validCount = objectValidation.max
        if validCount == textCount {
            self.mobileNumberTxt.text = text
            return false
        }
        if textCount > validCount {
            return false
        }
        return true
    }
    
    private func rejectNumberAlgorithm() -> Bool {
        self.mobileNumberTxt.text = commonPrefixMobileNumber
        self.showCustomErrorAlert(errMessage: "Rejected Number".localized)
        return false
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
       
        if textField == pointsText {
            pointsText.text =  self.getDigitDisplay(textField.text!)
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == pointsText {
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let amount = Double(self.removeCommaFromDigit(text)) ?? 0.0//Double((text as NSString).doubleValue)
            let availablePointsInt = Double(points)
            if amount > availablePointsInt {
                //self.showCustomErrorAlert(errMessage: "Enter below available points \(String(points))")
                self.showCustomErrorAlert(errMessage: "Redeem amount must be less than or equal to total loyalty.".localized)
                return false
            }
            if range.location == 0, string == "0" {
                return false
            }
            if text.count > 0 {
                pointsText.inputAccessoryView?.isHidden = false
                self.pointClearBtn.isHidden = false
                self.redeemNowButton.isHidden = false
                self.showAccessoryView(status : true)
                textField.placeholder = "Redeem Point".localized
                redeemNowButton.isHidden = false
            } else {
                pointsText.inputAccessoryView?.isHidden = true
                self.pointClearBtn.isHidden = true
                self.redeemNowButton.isHidden = true
                self.showAccessoryView(status : false)
                textField.placeholder = "Enter redeem Point".localized
                redeemNowButton.isHidden = true
            }
            return true
        } else  if  textField == mobileNumberTxt {
            let changingAccetable = self.MobileNumberHandler(textField: textField, shouldChangeCharactersIn: range, replacementString: string)
            return changingAccetable
        } else {
            return true
        }
    }
    
    fileprivate func showAllDetails(index : Int) {
        if let pointsStr = merchantDictionary[index].safeValueForKey("points") as? String {
            let pointsInt = Int((pointsStr as NSString).intValue)
            self.pointClearBtn.isHidden = false
            self.pointsText.text = self.getDigitDisplay(String(pointsInt))//String(pointsInt)
            self.pointsText.placeholder = "Redeem Point".localized
            self.redeemNowButton.isHidden = false
        }
//        self.heightMobNum.constant = 60.0
//        self.heightShopName.constant = 60.0
//        self.mobileNumberView.isHidden = false
//        self.shopNameView.isHidden = false
    }
    
    fileprivate func showReceiptScreen() {
        DispatchQueue.main.async {
            if let model = self.recieptModel {
                if let recieptVC = self.storyboard?.instantiateViewController(withIdentifier: "LORedeemReceiptViewController_ID") as? LORedeemReceiptViewController {
                    recieptVC.reciptModel = model
                    recieptVC.shopName = self.shopNameTxt.text ?? ""
                    self.navigationController?.pushViewController(recieptVC, animated: true)
                }
            }
        }
    }
    
    private func updateUser() {
        //http://120.50.43.157:1318/UserService/UpdateUser
        //{"CountryCode":"95","MobileNumber":"00959763893150","OpeningTime":"","ClosingTime":"","OkDollarBalance":7238,"Latitude":"16.799999442882836","Longitude":"96.13830749876797","CellId":"d6aa159671147295","CashIn":true,"CashOut":true,"AgentType":"1","Type":2,"IsOpenAlways":true,"AppVersion":"58","Category":"Mobile, Computers & Electronics","SubCategory":"Computer Programming & Support"}
        
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = PayToWebApi()
            web.delegate = self
            var version = ""
            if let versionLoc = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                version = versionLoc
            }
            
            let urlStr   = String.init(format: Url.loyaltyUpdateUserApiUrl)
            let url = getUrl(urlStr: urlStr, serverType: .businessLocationUrl)
            var pRam = Dictionary<String,String>()
            pRam["CountryCode"] = UserModel.shared.countryCode
            pRam["MobileNumber"] = UserModel.shared.formattedNumber
            pRam["OpeningTime"] = UserModel.shared.openTime
            pRam["ClosingTime"] = UserModel.shared.closeTime
            pRam["OkDollarBalance"] = String(self.walletAmount())
            pRam["Latitude"] = UserModel.shared.lat
            pRam["Longitude"] = UserModel.shared.long
            
            pRam["CellId"] = UserModel.shared.cellTowerID
            pRam["CashIn"] = "true"
            pRam["CashOut"] = "true"
            pRam["AgentType"] = String(UserModel.shared.agentType.hashValue)
            pRam["Type"] = UserModel.shared.accountType
            
            pRam["IsOpenAlways"] = "true"
            pRam["AppVersion"] = buildVersion
            pRam["Category"] = UserModel.shared.businessCate
            pRam["SubCategory"] = UserModel.shared.businessSubCate
            
            web.genericClassXML(url: url, param: pRam as AnyObject, httpMethod: "POST", mScreen: "mRedeemNow")
        } else {
            self.noInternetAlert()
        }
        
    }
    
    private func submitRedeemNow() {
        if appDelegate.checkNetworkAvail() {
            let urlString = String.init(format: "%@", Url.submitRedeemNowApiUrl)
            let url = getUrl(urlStr: urlString, serverType: .serverApp)
            let params = self.JSONStringFromAnyObject(value: self.getParamsForReedeemPoint() as AnyObject)
            
            DispatchQueue.main.async {
                TopupWeb.genericClass(url: url, param: params as AnyObject, httpMethod: "POST") { [weak self](response, success) in
                    if success {
                        if let json = response as? Dictionary<String,Any> {
                            if let code = json["Code"] as? NSNumber, code == 200 {
                                if let data12 = json["Data"] as? String, data12 != "" {
                                    let dataDict = OKBaseController.convertToDictionary(text: data12)
                                    if var data = dataDict?["RedeemPointsResponse"] as? String {
                                        //convert string to dictionary
                                        data = data.replacingOccurrences(of: "\"", with: "")
                                        let xmlString = SWXMLHash.parse(data)
                                        self?.redeemSuccessListArray.removeAll()
                                        self?.enumerate(indexer: xmlString)
                                        self?.parserXMLResult()
                                    }
                                }
                            } else {
                                if let msg = json["Msg"] as? String {
                                    self?.showErrorAlert(errMessage: msg)
                                }
                            }
                        }
                    }
                }
            }
        } else {
            self.noInternetAlert()
        }
    }
    
    private func getParamsForReedeemPoint() -> Dictionary<String,Any> {
        //{"RedeempointsSellingDto":{"AvialableBonusPoints":2037.0,"BeneficiaryAmount":"0","BeneficiaryNumber":"","ShopName":"Aasife Briyani","BonusPointName":"Aasife Briyani","BonusPointSourceNumber":"00959761870362","Comments":"Aasife Briyani","Destination":"00959761870362","Password":"B3CF3EJ2Eb3Gd2H","QtyOfRedeemPoints":"5","SecureToken":"EZhri8J3Cl","Source":"00959965966288","TelcoName":"","TransactionType":"LOYALTYREDEEMForPay"}
        var loginParams = Dictionary<String,Any>()
        loginParams["AppId"] = LoginParams.setUniqueIDLogin()
        loginParams["Limit"] = 0
        loginParams["MobileNumber"] = UserModel.shared.mobileNo
        loginParams["Msid"] = getMsid()
        loginParams["Offset"] = 0
        loginParams["Ostype"] = 1
        loginParams["Otp"] = ""
        loginParams["Simid"] = simid
        
        var formatStr = ""
        if let agentCode = mobileNumberTxt.text {
            if (agentCode.hasPrefix("09")) {
                formatStr = "0095\((agentCode).substring(from: 1))"
            }
        }

        
        var bonusParams = Dictionary<String,Any>()
        bonusParams["AvialableBonusPoints"]    = Double(points)
        bonusParams["BeneficiaryAmount"]    = "0"
        bonusParams["BeneficiaryNumber"]    = ""
        bonusParams["ShopName"]               = self.shopNameTxt.text
        bonusParams["BonusPointName"]       = self.shopNameTxt.text
        bonusParams["BonusPointSourceNumber"] = formatStr
        bonusParams["Comments"] = self.shopNameTxt.text
        bonusParams["Destination"]      = formatStr
        bonusParams["Password"]            = ok_password.safelyWrappingString()
        bonusParams["QtyOfRedeemPoints"]            = self.removeCommaFromDigit(pointsText.text ?? "")//pointsText.text
        bonusParams["SecureToken"]           = UserLogin.shared.token
        bonusParams["Source"]           = UserModel.shared.mobileNo
        bonusParams["TelcoName"]      = ""
        bonusParams["TransactionType"] = "LOYALTYREDEEMForPay"
        
        
        var finalParams = Dictionary<String,Any>()
        finalParams["LoginInfo"] = loginParams
        finalParams["RedeempointsSellingDto"] = bonusParams
        
        return finalParams
    }
    
    func parserXMLResult() {
        self.redeemSuccessListArray.removeAll()
        self.redeemSuccessListArray.append(redeemSuccessDict)
        println_debug(redeemSuccessListArray)
        if redeemSuccessDict["resultdescription"] as? String == "Transaction Successful" {
            var transID = ""
            var dTime: Date?
            var dest = ""
            var amount = ""
            var source = ""
            var balance = ""
            if let tID = redeemSuccessDict["transid"] as? String {
                transID = tID
            }
            if let dT = redeemSuccessDict["requestcts"] as? String {
                dTime = dT.dateValue(dateFormatIs: "dd-MMM-yyyy HH:mm:ss")
            }
            if let destNo = redeemSuccessDict["merchant"] as? String {
                dest = destNo
                if destNo.hasPrefix("0095") {
                    dest = "0\((destNo).substring(from: 4))"
                }
            }
            if let amnt = redeemSuccessDict["amount"] as? String {
                amount = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: amnt)
            }
            if let src = redeemSuccessDict["agentcode"] as? String {
                source = src
                if src.hasPrefix("0095") {
                    source = "0\((src).substring(from: 4))"
                }
            }
            
            let balStr = "\(points - codedPoints)"
            balance = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: balStr)
            
            let loyaltyDB = LoyaltyDBData(balancePoint: balance, dateTime: dTime, destinationNo: dest,
                                          merchantNo: source, shopName: shopName,
                                          transferPoint: amount, transID: transID,
                                          type: "LOYALTY REDEEM")
            println_debug(loyaltyDB)
            LoyaltyCoreDataManager.sharedInstance.insertLoyaltyPointsInfo(dataToInsert: loyaltyDB)
            //self.updateUser()
            if self.redeemSuccessListArray.count > 0 {
                self.recieptModel = loyaltyRecieptModel.init(dict: self.redeemSuccessListArray[0])
            }
            self.showReceiptScreen()
        } else if redeemSuccessDict["resultdescription"] as? String == "Invalid Secure Token" {
            DispatchQueue.main.async {
                OKPayment.main.authenticate(screenName: "mRedeemNow", delegate: self)
            }
        } else {
            DispatchQueue.main.async {
                let alertMessage = self.redeemSuccessDict.safeValueForKey("resultdescription") as? String ?? ""
                loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: #imageLiteral(resourceName: "error"))
                loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                    
                })
                loyaltyAlertView.showAlert(controller: self)
            }
        }
        
    }
    
    //PTWebresponseDelegate
    func webSuccessResult(data: Any, screen: String) {
        
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful {
            if screen == "mRedeemNow" {
                submitRedeemNow()
            }
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            redeemSuccessDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func webFailureResult(screen: String) {
        println_debug("Failure")
    }
    
    //MARK: - Button Action Methods
    @IBAction func contactButtonAction(_ sender : UIButton) {
        self.buttonView.isHidden = false
    }
    
    @IBAction func addFromOKAccountAction(_ sender : UIButton) {
        let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
        self.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func redeemNowButtonAction(_ sender : UIButton) {
        sender.isEnabled = false
        
        print("submit many times")
        let amount = Double((pointsText.text! as NSString).doubleValue)
        let availablePointsInt = Double(points)
        if amount > availablePointsInt {
            self.showCustomErrorAlert(errMessage: "Enter below available points \(String(points))")
            return
        }
        self.redeemNowButton.isUserInteractionEnabled = false
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "mRedeemNow", delegate: self)
        } else {
            self.submitRedeemNow()
        }
        
        //https://www.okdollar.net/WebServiceIpay/services/request;requesttype=LOYALTYREDEEM;agentcode=00959763893150;amount=1;merchant=00959790681375;pin=123456;vendorcode=IPAY;clientip=fe80%3A%3Af2ee%3A10ff%3Afe1f%3A56a0%25wlan0;clientos=Android%3Da7y17lte;clienttype=GPRS;securetoken=x02vTMcySS
    }
}

extension LoyaltyRedeemPointsViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return merchantDictionary.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "redeemCell", for: indexPath) as! redeermPointTabelCell
        cell.wrapData(redeemDictionary: merchantDictionary[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.showAllDetails(index : indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
}

class redeermPointTabelCell : UITableViewCell {
    
    @IBOutlet weak var merchantName : UILabel!
    @IBOutlet weak var totalPoints : UILabel!
    @IBOutlet weak var expiryDate : UILabel!
    
    let dateFormatter = DateFormatter()
    
    override func awakeFromNib() {
        
    }
    
    func wrapData(redeemDictionary : Dictionary<String , Any>) {
        merchantName.text = redeemDictionary.safeValueForKey("name") as? String ?? ""
        if let pointsStr = redeemDictionary.safeValueForKey("points") as? String {
            let pointsInt = Int((pointsStr as NSString).intValue)
            totalPoints.text = String(pointsInt)
        }
        
        if let date = redeemDictionary.safeValueForKey("expiredate") as? String {
            dateFormatter.dateFormat = "dd/MM/yyyy"
            if let convertedDate = dateFormatter.date(from: date) {
                dateFormatter.dateFormat = "dd-MMM-yyyy"
                let dateToShow = dateFormatter.string(from: convertedDate)
                if dateToShow != "" {
                    expiryDate.text = dateToShow
                }
            }
        }
    }
}

//MARK: - Contact Picker Protocol
extension LoyaltyRedeemPointsViewController: ContactPickerDelegate {
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]) {
        for contact in contacts {
            decodeContact(contact: contact, isMulti: true)
        }
    }
    
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        decodeContact(contact: contact, isMulti: false)
    }
    
    func wrapCountryViewData(img: String, str: String) {
        //        self.countryImage.image = UIImage.init(named: img)
        //        self.countryLabel.text  = str
        //
        //        let size   = self.countryLabel.text?.size(withAttributes: [.font: self.countryLabel.font]) ?? .zero
        //        let vWidth = 50 + size.width
        //        self.frame = .init(x: 0, y: 0, width: vWidth, height: 40.00)
        //        self.layoutIfNeeded()
    }
    
    func decodeContact(contact: ContactPicker) {
        
        self.view.layoutIfNeeded()
        let phoneNumberCount = contact.phoneNumbers.count
        var phoneNumber = ""
        
        if phoneNumberCount == 1  {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        } else if phoneNumberCount > 1 {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        } else {
            
        }
        
        if phoneNumber != "" {
            phoneNumber = phoneNumber.replacingOccurrences(of: ")", with: "")
            phoneNumber = phoneNumber.replacingOccurrences(of: "(", with: "")
            phoneNumber = phoneNumber.trimmingCharacters(in: .whitespaces)
        }
        
        if phoneNumber.hasPrefix("00") {
            _ = phoneNumber.remove(at: String.Index.init(encodedOffset: 0))
            _ = phoneNumber.remove(at: String.Index.init(encodedOffset: 0))
            
            if phoneNumber.hasPrefix("+") {
            } else {
                phoneNumber = "+" + phoneNumber
            }
        }
        
        let countryArray = revertCountryArray() as! Array<NSDictionary>
        let cDetails = self.identifyCountry(withPhoneNumber: phoneNumber, inCountryArray: countryArray) // 1 -> flag, 0 -> code
       // var countryCode = String.init(format: "(%@)", "+95")
        if cDetails.0 == "" || cDetails.1 == "" {
            
        }
        self.view.layoutIfNeeded()
        var phone = ""
        
        if phoneNumber.hasPrefix("+95") || phoneNumber.hasPrefix("(+95)") {
            phone = phoneNumber.replacingOccurrences(of: cDetails.0, with: "0")
            phone = phone.replacingOccurrences(of: " ", with: "")
            
            if phone.hasPrefix("0") {
                self.mobileNumberTxt.text = phone
            } else {
                self.mobileNumberTxt.text = "0" + phone
            }
        }
        
       // self.nameORnumber(selectiontype: .name, codeDetails: cDetails)
        if contact.displayName().count > 0 {
            self.shopNameTxt.text = contact.displayName()
        }
    }
    
    func decodeContact(contact: ContactPicker, isMulti: Bool){
        buttonView.isHidden = true
        decodeContact(contact: contact)
    }
}

extension LoyaltyRedeemPointsViewController: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        progressViewObj.removeProgressView()
        
        do {
            if let data = json as? Data {
                if let responseDict = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Dictionary<String , Any> {
                    if let message = responseDict.safeValueForKey("Msg") as? String, message != "Successredeemnowac" {
                        DispatchQueue.main.async {
                            
                        }
                    } else {
                        if let statusCode = responseDict.safeValueForKey("StatusCode") as? Int, statusCode == 302 {
                            if let _ = responseDict.safeValueForKey("Results") as? [Dictionary<String, Any>] {
                                // for _ in dictionaryArray {
                                //  self.merchantDetailsModelArray.append(merchantDetailsModel.init(dictionary: dictionary))
                                // }
                            }
                        }
                    }
                }
            }
        } catch {
        }
    }
    
    func enumerateMerchant(indexer: XMLIndexer) {
        for child in indexer.children {
            merchantListDict[child.element!.name] = child.element!.text
            enumerateMerchant(indexer: child)
        }
    }
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        PTLoader.shared.hide()
    }
}
