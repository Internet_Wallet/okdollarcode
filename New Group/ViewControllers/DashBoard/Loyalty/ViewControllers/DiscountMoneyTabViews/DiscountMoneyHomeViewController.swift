//
//  DiscountMoneyHomeViewController.swift
//  OK
//
//  Created by Kethan on 3/7/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class DiscountMoneyHomeViewController: LoyaltyBaseViewController {
    enum ScreenFrom {
        case dashboard, other
    }
    //MARK: - Properties
    fileprivate var discountMoneyMenu: PaytoScroller?
    fileprivate var controllerArray: [UIViewController] = []
    lazy var searchBar:UISearchBar = UISearchBar()
    var searchActive: Bool = false
    var userInfo: Dictionary<String, Any> = [:]
    var bonusPointTabVC = BonusPointTabViewController()
    var discountMoneyTabVC = DiscountMoneyTabViewController()
    var screenFrom = ScreenFrom.other
    
    //MARK: - Views Method
    override func viewDidLoad() {
        super.viewDidLoad()
        addBckButton(navTitle: "Redeem Bonus Point")
        addCustomMenuButton()
        MyNumberTopup.theme = UIColor.white
        searchBar.delegate = self
        searchBar.tintColor = UIColor.blue
        bonusPointTabVC =  UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: "BonusPointTabViewController_ID") as! BonusPointTabViewController
        bonusPointTabVC.delegate = self
        bonusPointTabVC.title = "Bonus Points".localized
        
        discountMoneyTabVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: "DiscountMoneyTabViewController_ID") as! DiscountMoneyTabViewController
        discountMoneyTabVC.title = "Discount Money".localized
        controllerArray = [bonusPointTabVC, discountMoneyTabVC]
        
        let parameters = [PaytoScrollerOptionMenuItemSeparatorWidth: 0,
                          PaytoScrollerOptionMenuItemSeparatorPercentageHeight: 0.1,
                          PaytoScrollerOptionScrollMenuBackgroundColor: UIColor.red,
                          PaytoScrollerOptionSelectedMenuItemLabelColor: UIColor.white,
                          PaytoScrollerOptionViewBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectionIndicatorColor: UIColor.yellow,
                          PaytoScrollerOptionUnselectedMenuItemLabelColor:UIColor.white,
                          PaytoScrollerOptionMenuHeight: 50,
                          PaytoScrollerOptionMenuItemFont: "Zawgyi-One",
                          PaytoScrollerOptionMenuItemWidth: (screenWidth / 2.0)] as [String: Any]
        
        if let navi = self.navigationController {
            let yAxis = navi.navigationBar.frame.height + UIApplication.shared.statusBarFrame.height
            discountMoneyMenu = PaytoScroller(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: yAxis, width: self.view.frame.width, height: self.view.frame.height - yAxis), options: parameters)
//        discountMoneyMenu = PaytoScroller(viewControllers: controllerArray, frame: self.view.bounds, options: parameters)
            discountMoneyMenu?.currentPageIndex = 0
            discountMoneyMenu?.delegate = self
            discountMoneyMenu?.scrollingEnable(true)
            if let menu = discountMoneyMenu {
                self.view.addSubview(menu.view)
            }
        }
        
        if let navi = self.navigationController {
            let yAxis = navi.navigationBar.frame.height + UIApplication.shared.statusBarFrame.height
            discountMoneyMenu = PaytoScroller(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: yAxis, width: self.view.frame.width, height: self.view.frame.height - yAxis), options: parameters)
            discountMoneyMenu?.currentPageIndex = 0
            discountMoneyMenu?.delegate = self
            discountMoneyMenu?.scrollingEnable(true)
            if let menu = discountMoneyMenu {
                self.view.addSubview(menu.view)
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(dismissScreen), name: NSNotification.Name(rawValue: "DismissLoyaltyView"), object: nil )
    }
    
    deinit {
        discountMoneyMenu?.removeFromParent()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "DismissLoyaltyView"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.barTintColor = UIColor.red
        discountMoneyMenu?.selectedMenuItemLabelColor = UIColor.white
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func addBckButton(navTitle: String) {
        let backButton = UIButton(type: .custom)
        backButton.setBackgroundImage(#imageLiteral(resourceName: "tabBarBack"), for: .normal)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        backButton.addTarget(self, action: #selector(bckAction), for: .touchUpInside)
        navigationItem.title = navTitle.localized
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white ]
        self.navigationController?.navigationBar.barTintColor = UIColor.red
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    private func addCustomMenuButton() {
        let searchButton = UIButton(type: .custom)
        searchButton.setBackgroundImage(UIImage.init(named: "search_white.png"), for: .normal)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: searchButton)
        searchButton.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
    }
    
    @objc func bckAction() {
        switch screenFrom {
        case .dashboard:
            self.dismiss(animated: true, completion: nil)
        case .other:
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func searchAction() {
        searchBar.text = ""
        if self.navigationItem.titleView == nil {
            searchBar.placeholder = "Search Name and Number".localized
          if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
               
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
            }
          }
            searchBar.frame = (self.navigationController?.navigationBar.frame)!
            self.navigationItem.titleView = searchBar
            searchBar.becomeFirstResponder()
        } else {
            searchActive = false
            self.searchTextChangeNotification(searchText: "", status: false)
            self.navigationItem.titleView = nil
        }
    }
    
    private func searchTextChangeNotification(searchText: String, status: Bool) {
        userInfo.removeAll()
        userInfo["searchText"] = searchText
        userInfo["searchStatus"] = searchActive
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SearchTextChange"), object: nil, userInfo: userInfo)
    }
}

//MARK: - showRedeemVCProtocol
extension DiscountMoneyHomeViewController: showRedeemVCProtocol {
    func showRedeemVC(dictionary: [Dictionary<String, Any>], selectedMerchant: String) {
        if let redeemVC = self.storyboard?.instantiateViewController(withIdentifier: "LoyaltyRedeemPointsViewController_ID") as? LoyaltyRedeemPointsViewController {
            redeemVC.redeemDictionary = dictionary
            redeemVC.merchantNumber = selectedMerchant
            self.navigationController?.pushViewController(redeemVC, animated: true)
        }
    }
}

//MARK: - PaytoScrollerDelegate
extension DiscountMoneyHomeViewController: PaytoScrollerDelegate {
    func willMove(toPage controller: UIViewController!, index: Int) {
        searchBar.text = ""
        searchActive = false
        self.searchTextChangeNotification(searchText: "", status: false)
        self.navigationItem.titleView = nil
    }
    
    func didMove(toPage controller: UIViewController!, index: Int) {
        searchBar.text = ""
        searchActive = false
        self.searchTextChangeNotification(searchText: "", status: false)
        self.navigationItem.titleView = nil
    }
}

//MARK: - UISearchBarDelegate
extension DiscountMoneyHomeViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        searchActive = false
//        self.searchTextChangeNotification(searchText: "", status: false)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        self.searchTextChangeNotification(searchText: "", status: false)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        searchActive = false
//        self.searchTextChangeNotification(searchText: "", status: false)
    }
    
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.hasPrefix(" ") {
            searchBar.text?.removeFirst()
            return
        }
        if searchText.contains(find: "  ") {
            searchBar.text?.removeLast()
            return
        }
        if searchText.containsEmoji {
            searchBar.text?.removeLast()
            return
        }
        if searchText.count > 50 {
            searchBar.text?.removeLast()
            return 
        }
        self.searchTextChangeNotification(searchText: searchText, status: true)
    }
}
