//
//  BonusPointTabViewController.swift
//  OK
//
//  Created by Kethan on 3/7/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol showRedeemVCProtocol: class {
    func showRedeemVC(dictionary: [Dictionary<String, Any>], selectedMerchant: String)
}

class BonusPointTabViewController: LoyaltyBaseViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var noRecordView: UIView! {
        didSet {
            self.noRecordView.isHidden = true
        }
    }
    @IBOutlet weak var noRecordsLabel: UILabel! {
        didSet {
            self.noRecordsLabel.font = UIFont(name: appFont, size: appFontSize)
            self.noRecordsLabel.text = self.noRecordsLabel.text?.localized
        }
    }
    @IBOutlet weak var recordView: UIView!
    @IBOutlet weak var topView: UIView! {
        didSet {
            self.topView.isHidden = true
        }
    }
    @IBOutlet weak var bottomView: UIView! {
        didSet {
            self.bottomView.isHidden = true
        }
    }
    @IBOutlet weak var merchantNameLbl: UILabel! {
        didSet {
            self.merchantNameLbl.font = UIFont(name: appFont, size: appFontSize)
            self.merchantNameLbl.text = self.merchantNameLbl.text?.localized
        }
    }
    @IBOutlet weak var pointsLbl: UILabel! {
        didSet {
            self.pointsLbl.font = UIFont(name: appFont, size: appFontSize)
            self.pointsLbl.text = self.pointsLbl.text?.localized
        }
    }
    
    @IBOutlet weak var totalLabel: UILabel! {
        didSet {
            self.totalLabel.font = UIFont(name: appFont, size: appFontSize)
            self.totalLabel.text = self.totalLabel.text?.localized
        }
    }
    @IBOutlet weak var totalAmountLabel: UILabel!{
        didSet{
            self.totalAmountLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var bonusPointTableView: UITableView!
    
    //MARK: - Properties
    weak var delegate: showRedeemVCProtocol?
    var merchantArray = [String]()
    var merchantListDict = Dictionary<String,Any>()
    var merchantListArray = [Dictionary<String,Any>]()
    var merchantDictionary = [Dictionary<String,Any>]()
    var points: Int = 0
    var filtered = [Dictionary<String, String>]()
    var searchActive = false
    
    //MARK: - Views Method
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showProgressView()
        self.redeemPointHistoryByCategory()
        self.bonusPointTableView.tableFooterView = UIView()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSearchText(_:)), name: NSNotification.Name(rawValue: "SearchTextChange"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(popFromReceipt), name: NSNotification.Name(rawValue: "PopFromReceipt"), object: nil)
    }
    
    @objc func popFromReceipt() {
         merchantArray.removeAll()
         merchantListDict.removeAll()
         merchantListArray.removeAll()
         merchantDictionary.removeAll()
         points = 0
         filtered.removeAll()
         searchActive = false
        PTLoader.shared.show()
        DispatchQueue.main.async {
            self.bonusPointTableView.reloadData()
        }
        self.redeemPointHistoryByCategory()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Methods
    private func redeemPointHistoryByCategory() {
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let web      = PayToWebApi()
            web.delegate = self
            
            let urlStr   = String.init(format: Url.redeemPointHistoryByCategoryApiUrl)
            let url = getUrl(urlStr: urlStr, serverType: .serverApp)
            var pRam = Dictionary<String,Any>()
            var loginInfoDict = Dictionary<String,Any>()
                loginInfoDict["AppId"] = 0
                loginInfoDict["Limit"] = 0
                loginInfoDict["MobileNumber"] = UserModel.shared.mobileNo
                loginInfoDict["Msid"] = UserModel.shared.msid
                loginInfoDict["Offset"] = 0
                loginInfoDict["Ostype"] = 1
                loginInfoDict["Otp"] = ""
                loginInfoDict["Simid"] = UserModel.shared.simID
            pRam["LoginInfo"] = loginInfoDict
            pRam["RedeemPointsCategory"] = "SELLING"
            web.genericClass(url: url, param: pRam as AnyObject, httpMethod: "POST", mScreen: "mRedeemHistByCat")
        }
    }
    
    fileprivate func getMerchantMobileNumberByCat(dict: [Dictionary<String, Any>]) {
        for (_, singleDict) in dict.enumerated() {
            if (singleDict.safeValueForKey("TransactionTypeName") as? String ?? "") == "TOPUP" {
                if let arrayOfMerchant = singleDict["BuyRedeemPointsModel"] as? [Dictionary<String, Any>] {
                    for dictIn in arrayOfMerchant {
                        let mobileNumber = dictIn.safeValueForKey("MerchantMobileNumber") as? String ?? ""
                        let status = self.merchantArray.contains(mobileNumber)
                        if !status {
                            self.merchantArray.append(mobileNumber)
                        }
                    }
                }
            }
        }
    }
    
    private func getBonusPointList() {
        if appDel.checkNetworkAvail() {
            self.showProgressView()
            let web      = PayToWebApi()
            web.delegate = self
            var ip = ""
            if let ipAdd = OKBaseController.getIPAddress() {
                ip = ipAdd
            }
            let urlStr   = String.init(format: Url.loyaltyViewApiUrl, UserModel.shared.mobileNo, ok_password ?? "",  ip , "ios",  UserLogin.shared.token )
           println_debug(urlStr)
            let url = getUrl(urlStr: urlStr, serverType: .serverEstel)
            let pRam = Dictionary<String,String>()
            println_debug(pRam)
            web.genericClassXML(url: url, param: pRam as AnyObject, httpMethod: "POST", mScreen: "mMerchantList")
        } else {
            self.removeProgressView()
            self.noInternetAlert()
        }
    }
    
    func setUpViews() {
        DispatchQueue.main.async {
            self.noRecordView.isHidden = true
        }
        if self.merchantListArray.count > 0 {
            if let merchantNames = merchantListArray[0]["merchant"] as? String, let loyaltyPoints = merchantListArray[0]["loyaltypoints"] as? String, let merchantId = merchantListArray[0]["merchantid"] as? String, let expireDate = merchantListArray[0]["expirydate"] as? String {
                let merchantArrayNames = merchantNames.components(separatedBy: "|")
                let merchantLoyaltyPnts = loyaltyPoints.components(separatedBy: "|")
                let merchantMobNum = merchantId.components(separatedBy: "|")
                let expiryDateArray = expireDate.components(separatedBy: "|")
                for (index, name) in merchantArrayNames.enumerated() {
                    if merchantLoyaltyPnts.count >= index {
                        let dict = ["name": name, "points": merchantLoyaltyPnts[index], "mobile": merchantMobNum[index], "expiredate": expiryDateArray[index]]
                        let pointsStr = merchantLoyaltyPnts[index] as NSString
                        self.points = self.points + Int(pointsStr.intValue)
                        
                        let status = merchantDictionary.contains { (dict1) -> Bool in
                            return (dict1["mobile"] as? String ?? "") == dict["mobile"]
                        }
                        if status {
                            let index = merchantDictionary.index { (dict2) -> Bool in
                                return (dict2["mobile"] as? String ?? "") == dict["mobile"]
                            }
                            var dict3 = merchantDictionary[index!]
                            let points1 = Int((dict3["points"] as! NSString).intValue)
                            let points2 = Int((dict["points"]! as NSString).intValue)
                            dict3["points"] = String(points1 + points2)
                            self.merchantDictionary.remove(at: index!)
                            self.merchantDictionary.insert(dict3, at: index!)
                        } else {
                            merchantDictionary.append(dict)
                        }
                    } else {
                        let dict = ["name": name, "points": "0", "mobile": merchantMobNum[index], "expiredate": expiryDateArray[index]]
                        let status = merchantDictionary.contains { (dict1) -> Bool in
                            return (dict1["mobile"] as? String ?? "") == dict["mobile"]
                        }
                        if status {
                            let index = merchantDictionary.index { (dict2) -> Bool in
                                return (dict2["mobile"] as? String ?? "") == dict["mobile"]
                            }
                            var dict3 = merchantDictionary[index!]
                            let points1 = Int((dict3["points"] as! NSString).intValue)
                            let points2 = Int((dict["points"]! as NSString).intValue)
                            dict3["points"] = String(points1 + points2)
                            self.merchantDictionary.remove(at: index!)
                            self.merchantDictionary.insert(dict3, at: index!)
                        } else {
                            merchantDictionary.append(dict)
                        }
                    }
                }
            }
        }
        
        if self.merchantDictionary.count > 0 {
            self.removeProgressView()
            DispatchQueue.main.async {
                self.bottomView.isHidden = false
                self.topView.isHidden = false
                self.totalAmountLabel.text = self.getDigitDisplay(String(self.points))//String(self.points)
                self.bonusPointTableView.reloadData()
            }
        } else {
            DispatchQueue.main.async {
                self.noRecordView.isHidden = false
            }
        }
    }
    
    @objc func showSearchText(_ notification: NSNotification) {
        if let searchText = notification.userInfo?["searchText"] as? String, searchText.count > 0 {
            self.filtered = self.merchantDictionary.filter({ (pair) -> Bool in
                guard let mallName = pair["name"] as? String else { return false }
                return mallName.containsIgnoringCase(find: searchText) || (pair["mobile"] as? String ?? "").contains(find: searchText)
            }) as! [Dictionary<String, String>]
            if let searchStatus = notification.userInfo?["searchStatus"] as? Bool {
                searchActive = searchStatus
                DispatchQueue.main.async {
                    self.bonusPointTableView.reloadData()
                }
            }
            if self.filtered.count > 0 {
                self.noRecordView.isHidden = true
            } else {
                self.noRecordView.isHidden = false
            }
        } else {
            self.noRecordView.isHidden = true
            searchActive = false
            DispatchQueue.main.async {
                self.bonusPointTableView.reloadData()
            }
        }
    }
    
    fileprivate func showAlert() {
        DispatchQueue.main.async {
            loyaltyAlertView.wrapAlert(title: "", body: "You cannot Redeem OK DOLLAR TOPUP point here, please use from Bonus Point Top-Up".localized, img: #imageLiteral(resourceName: "discount_money"))
            loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                self.navigationController?.popViewController(animated: true)
            })
            loyaltyAlertView.showAlert(controller: self)
        }
        
    }
    
    fileprivate func showRedeemVC(index: Int) {
       var mobileNumber = ""
        if searchActive {
            mobileNumber = filtered[index]["mobile"]  ?? ""
            let status = self.merchantArray.contains(mobileNumber)
            if status {
                self.showAlert()
            } else {
                let dictTopass = self.filtered.filter({ (dict) -> Bool in
                    return (dict["mobile"] ?? "") == mobileNumber
                })
                if dictTopass.count > 0 {
                    delegate?.showRedeemVC(dictionary: merchantListArray, selectedMerchant: mobileNumber)
                }
            }
        } else {
            mobileNumber = merchantDictionary[index]["mobile"] as? String ?? ""
            let status = self.merchantArray.contains(mobileNumber)
            if status {
                self.showAlert()
            } else {
                let dictTopass = self.merchantDictionary.filter({ (dict) -> Bool in
                    return (dict["mobile"] as? String ?? "") == mobileNumber
                })
                if dictTopass.count > 0 {
                    delegate?.showRedeemVC(dictionary: merchantListArray, selectedMerchant: mobileNumber)
                }
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SearchTextChange"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "PopFromReceipt"), object: nil)
    }
}

extension BonusPointTabViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchActive {
            return self.filtered.count
        }
        return self.merchantDictionary.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: BonusPointTabTableViewCell = tableView.dequeueReusableCell(withIdentifier: "bonusPointTabTableViewCell", for: indexPath) as! BonusPointTabTableViewCell
        if searchActive {
            cell.loadCell(dictionary: filtered[indexPath.row])
        } else {
            cell.loadCell(dictionary: merchantDictionary[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.showRedeemVC(index: indexPath.row)
    }
}

//PTWebresponseDelegate
extension BonusPointTabViewController: PTWebResponseDelegate {
    func webSuccessResult(data: Any, screen: String) {
         if screen == "mMerchantList" {
            if let xmlString = data as? String {
                let xml = SWXMLHash.parse(xmlString)
                merchantListDict.removeAll()
                self.enumerateMerchant(indexer: xml)
                self.merchantListArray.removeAll()
                self.merchantListArray.append(merchantListDict)
                println_debug(self.merchantListArray)
                if merchantListDict["resultdescription"] as? String == "Transaction Successful" {
                    self.setUpViews()
                } else {
                    self.showErrorAlert(errMessage: merchantListDict["resultdescription"] as? String ?? "Please try again".localized)
                }
            } else {
               self.removeProgressView()
            }
         } else if screen == "mRedeemHistByCat" {
            if let dictData = data as? Dictionary<String, Any> {
                if let dict = dictData["Data"] as? String {
                    if let dictAll = OKBaseController.convertToArrDictionary(text: dict) {
                        println_debug(dict)
                        if let dict = dictAll as? [Dictionary<String, Any>] {
                            self.getMerchantMobileNumberByCat(dict: dict)
                        }
                    }
                }
            }
            self.getBonusPointList()
        }
    }
    
    func enumerateMerchant(indexer: XMLIndexer) {
        for child in indexer.children {
            merchantListDict[child.element!.name] = child.element!.text
            enumerateMerchant(indexer: child)
        }
    }
    
    func webFailureResult(screen: String) {
        println_debug("Failure")
    }
}
