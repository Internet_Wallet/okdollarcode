//
//  DiscountMoneyTabViewController.swift
//  OK
//
//  Created by Kethan on 3/7          /18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class DiscountMoneyTabViewController: LoyaltyBaseViewController {

    @IBOutlet weak var noRecordView : UIView!
    @IBOutlet weak var noRecordsLabel : UILabel! {
        didSet {
            self.noRecordsLabel.font = UIFont(name: appFont, size: appFontSize)
            self.noRecordsLabel.text = self.noRecordsLabel.text?.localized
        }
    }
    @IBOutlet weak var recordView : UIView!
    @IBOutlet weak var reqMoneyTabTableView : UITableView!
    var filtered : [Dictionary<String, String>] = [[:]]
    var searchActive = false
    
    var bonusPointListArray = Array<Dictionary<String, Any>>()
    
    var bonusPointDict = Dictionary<String,Any>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.reqMoneyTabTableView.tableFooterView = UIView()
        //self.getReqMoneyStatusList()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSearchText(_:)), name: NSNotification.Name(rawValue: "SearchTextChange"), object: nil)
        // Do any additional setup after loading the view.
        setUpViews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func showSearchText(_ notification: NSNotification) {
        if let searchText = notification.userInfo?["searchText"] as? String {
            filtered = filtered.filter { pair in
                guard let mallName = pair["Name"] else { return false }
                return mallName.hasPrefix(searchText)
            }
            if let searchStatus = notification.userInfo?["searchStatus"] as? Bool {
                searchActive = searchStatus
                if searchActive {
                    DispatchQueue.main.async {
                        self.reqMoneyTabTableView.reloadData()
                    }
                }
            }
        }
    }
    
    func setUpViews() {
        DispatchQueue.main.async {
            self.noRecordView.isHidden = false
        }
       
        if self.bonusPointListArray.count > 0 {
            DispatchQueue.main.async {
                self.noRecordView.isHidden = true
                self.reqMoneyTabTableView.reloadData()
            }
        } else {
            DispatchQueue.main.async {
                self.noRecordView.isHidden = false
            }
        }
    }
    
    private func getReqMoneyStatusList() {
        if appDelegate.checkNetworkAvail() {
            let web      = PayToWebApi()
            web.delegate = self
            var ip = ""
            if let ipAdd = OKBaseController.getIPAddress() {
                ip = ipAdd
            }
            let urlStr   = String.init(format: Url.discountMoneyApiUrl, UserModel.shared.mobileNo, ip , "ios", UserLogin.shared.token)
            let url = getUrl(urlStr: urlStr, serverType: .serverEstel)
//            println_debug(url)
            let pRam = Dictionary<String,String>()
            web.genericClassXML(url: url, param: pRam as AnyObject, httpMethod: "POST", mScreen: "mReqMoneyStatusList")
        } else {
            self.noInternetAlert()
        }
    }

}

extension DiscountMoneyTabViewController : UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bonusPointListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : BonusPointTabTableViewCell = tableView.dequeueReusableCell(withIdentifier: "bonusPointTabTableViewCell", for: indexPath) as! BonusPointTabTableViewCell
//        if searchActive {
//            cell.loadCell(dictionary: filtered[indexPath.row])
//        } else {
//            cell.loadCell(dictionary: merchantDictionary[indexPath.row])
//        }
        return cell
    }
}

//PTWebresponseDelegate
extension DiscountMoneyTabViewController : PTWebResponseDelegate {
    func webSuccessResult(data: Any, screen: String) {
//        println_debug(data)
//        println_debug(screen)
        
        if screen == "mReqMoneyStatusList" {
            if let xmlString = data as? String {
                let xml = SWXMLHash.parse(xmlString)
                
                self.bonusPointDict.removeAll()
                self.bonusPointListArray.removeAll()
                self.enumerate(indexer: xml)
                self.bonusPointListArray.append(bonusPointDict)
//                println_debug(bonusPointListArray)
                DispatchQueue.main.async {
                    self.removeProgressView()
                    if self.bonusPointDict["resultdescription"] as? String == "Transaction Successful" {
                        self.setUpViews()
                    } else {
                        self.showErrorAlert(errMessage: self.bonusPointDict["resultdescription"] as? String ?? "Please try again".localized)
                    }
                }
            }
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            bonusPointDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
//    func enumerateAlert(indexer: XMLIndexer) {
//        for child in indexer.children {
//            if child.element?.name == "record" {
//                self.enumerate(indexer: child)
//                bonusPointListArray.append(bonusPointDict)
//                bonusPointDict.removeAll()
//            }
//            enumerateAlert(indexer: child)
//        }
//    }
    
    func webFailureResult(screen: String) {
        println_debug("Failure")
    }
}

