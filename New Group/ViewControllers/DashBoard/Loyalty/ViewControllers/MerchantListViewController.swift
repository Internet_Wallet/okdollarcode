//
//  MerchantListViewController.swift
//  OK
//
//  Created by Kethan on 3/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol SelectedMerchantDetail : class {
    func selectedMerchant(dictionary : Dictionary<String, Any>)
}

class MerchantListViewController: LoyaltyBaseViewController, UIPopoverPresentationControllerDelegate {
    
    var merchantDictionary = [Dictionary<String, Any>]()
    var merchantDictionary1 = [Dictionary<String, Any>]()
    var filteredMerchantDictionary = [Dictionary<String, Any>]()
    
    var merchantDetailsDictionary = [merchantDetailsModel]()
    var filteredMerchant = [merchantDetailsModel]()
    var show = false
    var screenType = ""
    lazy var searchBar: UISearchBar = UISearchBar()
    
    @IBOutlet weak var merchantListTableView : UITableView!
    @IBOutlet weak var noRecordView : UIView! {
        didSet {
            self.noRecordView.isHidden = true
        }
    }
  @IBOutlet weak var lblNoRecord : UILabel! {
    didSet {
      self.lblNoRecord.font = UIFont(name: appFont, size: appFontSize)
      self.lblNoRecord.text  = "No Record Found!".localized
    }
  }
    weak var delegate : SelectedMerchantDetail?
    override func viewDidLoad() {
        super.viewDidLoad()
        addCustomBackButton(navTitle: "Select Merchant")
        addCustomRightButton()
        getMerchantList()
        merchantListTableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    private func getMerchantList() {
        if merchantDictionary.count > 0 {
            if let merchantNames = merchantDictionary[0]["merchant"] as? String, let loyaltyPoints = merchantDictionary[0]["loyaltypoints"] as? String, let merchantid = merchantDictionary[0]["merchantid"] as? String {
                let merchantArrayNames = merchantNames.components(separatedBy: "|")
                let merchantLoyaltyPnts = loyaltyPoints.components(separatedBy: "|")
                let merchantId = merchantid.components(separatedBy: "|")
                for (index, name) in merchantArrayNames.enumerated() {
                    let status = self.merchantDictionary1.contains { (dict) -> Bool in
                        return dict.safeValueForKey("name") as? String ?? "" == name
                    }
                    if status {
                        for (index1, dict) in self.merchantDictionary1.enumerated() {
                            if dict.safeValueForKey("name") as? String == name {
                                var dictTemp = dict
                                var points = 0
                                points = Int((merchantLoyaltyPnts[index] as NSString).intValue) + Int(((dict.safeValueForKey("points") as? String ?? "0" )as NSString).intValue)
                                dictTemp["points"] = String(points)
                                self.merchantDictionary1.remove(at: index1)
                             
                                self.merchantDictionary1.insert(dictTemp, at: index1)
                            }
                        }
                    } else {
                        let dict = ["name" : name, "points" : merchantLoyaltyPnts[index], "id" : merchantId[index]]
                        merchantDictionary1.append(dict)
                    }
                }
            }
        }
        if merchantDetailsDictionary.count > 0 {
            self.merchantDetailsDictionary = self.merchantDetailsDictionary.filter{ pair in
                return pair.status == "Available" }
        }
        
//        for singleDict in self.merchantDetailsDictionary {
//           let status = self.merchantDetailsFilteredDictionary.contains { (singleMerchant) -> Bool in
//                let firstMerchant = singleDict.merchant
//                let secondMerchant = singleMerchant.merchant
//                return (firstMerchant["merchantAccountNo"] as? String ?? "" == secondMerchant["merchantAccountNo"] as? String ?? "")
//            }
//            println_debug(status)
//            if status {
//                for (index, merchantDict) in self.merchantDetailsFilteredDictionary.enumerated() {
//                    let firstMerchant = singleDict.merchant
//                    let secondMerchant = merchantDict.merchant
//                    if firstMerchant["merchantAccountNo"] as? String == secondMerchant["merchantAccountNo"] as? String {
//                        var merchantDictCopy = merchantDict
//                        merchantDictCopy.pointBalance = merchantDictCopy.pointBalance! + singleDict.pointBalance!
//                        merchantDictCopy.sellingPrice = merchantDictCopy.sellingPrice! + singleDict.sellingPrice!
//                        self.merchantDetailsFilteredDictionary.remove(at: index)
//                        self.merchantDetailsFilteredDictionary.insert(merchantDictCopy, at: index)
//                    }
//                }
//            } else {
//                self.merchantDetailsFilteredDictionary.append(singleDict)
//            }
//        }
        
        if merchantDictionary1.count > 0 || merchantDetailsDictionary.count > 0 {
            self.merchantListTableView.isHidden = false
            self.noRecordView.isHidden = true
        } else {
            self.merchantListTableView.isHidden = true
            self.noRecordView.isHidden = false
        }
        DispatchQueue.main.async {
            self.merchantListTableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func addCustomRightButton() {
        if screenType == "Bonus Point Resale" {
            let menuButton = UIButton(type: .custom)
            menuButton.frame = CGRect(x: 0, y: 0, width: 30, height: 40)
            menuButton.setImage(UIImage.init(named: "menu_white_payto.png"), for: .normal)
            menuButton.addTarget(self, action: #selector(menuAction(_ : )), for: .touchUpInside)
            let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
            space.width = 40.0

            let searchButton = UIButton(type: .custom)
            searchButton.frame = CGRect(x: 0, y: 0, width: 30, height: 40)
            searchButton.setImage(UIImage.init(named: "search_white.png"), for: .normal)
            searchButton.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
            navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: menuButton), UIBarButtonItem(customView: searchButton)]
        } else {
            let searchButton = UIButton(type: .custom)
            searchButton.frame = CGRect(x: 0, y: 0, width: 30, height: 40)
            searchButton.setImage(UIImage.init(named: "search_white.png"), for: .normal)
            searchButton.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
            navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: searchButton)]
        }
       
    }
    
    @objc func menuAction(_ sender : UIBarButtonItem) {
        let popoverContent = self.storyboard?.instantiateViewController(withIdentifier: "ViewAllStatusPopover_ID") as? ViewAllStatusPopover
        popoverContent?.delegate = self
        popoverContent?.modalPresentationStyle = .popover
        let popover = popoverContent?.popoverPresentationController
        popoverContent?.preferredContentSize = CGSize(width: 200.0, height: 45.0)
        popover?.delegate = self
        popover?.sourceView = self.view
        popover?.permittedArrowDirections = .init(rawValue: 0)
        popover?.sourceRect = CGRect(x: self.view.frame.size.width, y: 0, width: 0, height: 0)
        self.present(popoverContent!, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        show = false
    }
    
    @objc func searchAction() {
        searchBar.text = ""
        if self.navigationItem.titleView == nil {
            searchBar.placeholder = "Search".localized
            if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
                if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                   //(appDel.currentLanguage == "uni") ? UIFont.systemFont(ofSize: 15) : myFont
                    //searchTextField.font = (appDel.currentLanguage == "uni") ? UIFont.systemFont(ofSize: 15) : myFont
                    if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                        searchTextField.font = myFont
                    }else{
                        //
                    }
                    searchTextField.keyboardType = .asciiCapable
                    searchTextField.placeholder = "Search".localized
                    searchTextField.backgroundColor  = .white
                }
            }
            let view = self.searchBar.subviews[0] as UIView
            let subViewsArray = view.subviews
            for subView: UIView in subViewsArray {
                if subView.isKind(of: UITextField.self) {
                    subView.tintColor = UIColor.blue
                }
            }
            searchBar.delegate = self
            searchBar.tintColor = .black
            self.navigationItem.titleView = searchBar
            searchBar.becomeFirstResponder()
        } else {
            self.noRecordView.isHidden = true
            if screenType == "Bonus Point Resale" {
                if self.merchantDetailsDictionary.count == 0 {
                   self.noRecordView.isHidden = false
                }
            } else {
                if merchantDictionary1.count == 0 {
                    self.noRecordView.isHidden = false
                }
            }
            self.merchantListTableView.reloadData()
            self.navigationItem.titleView = nil
        }
    }
    
    private func dismissMerchantViewController() {
        backAction()
    }
    
    private func showSelectedViewController(viewController : UIViewController) {
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    private func showMerchantDetailsVC(index : Int) {
        if !show {
            show = true
            if let merchantListVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: "MerchantDetailViewController_ID") as? MerchantDetailViewController {
                if let text = self.searchBar.text, text.count > 0 {
                    merchantListVC.modelData = self.filteredMerchant[index]
                } else {
                    merchantListVC.modelData = self.merchantDetailsDictionary[index]
                }
                showSelectedViewController(viewController: merchantListVC)
            }
        }
    }

}


extension MerchantListViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if screenType == "Bonus Point Resale" {
            self.navigationItem.titleView = nil
            showMerchantDetailsVC(index: indexPath.row)
        } else {
            self.delegate?.selectedMerchant(dictionary: merchantDictionary1[indexPath.row])
            dismissMerchantViewController()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

extension MerchantListViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if screenType == "Bonus Point Resale" {
            if let text = self.searchBar.text, text.count > 0 {
                return filteredMerchant.count
            }
            return self.merchantDetailsDictionary.count
        } else {
            if let text = self.searchBar.text, text.count > 0 {
                return filteredMerchantDictionary.count
            }
            return merchantDictionary1.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if screenType == "Bonus Point Resale" {
            let cell : MerchantListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "merchantBonusCell", for: indexPath) as! MerchantListTableViewCell
            if let text = self.searchBar.text, text.count > 0 {
                cell.loadBonusCell(dictionary: filteredMerchant[indexPath.row])
            } else {
                cell.loadBonusCell(dictionary: self.merchantDetailsDictionary[indexPath.row])
            }
            return cell
        } else {
            if let text = self.searchBar.text, text.count > 0 {
                let cell : MerchantListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "merchantCell", for: indexPath) as! MerchantListTableViewCell
                cell.loadCell (dictionary: filteredMerchantDictionary[indexPath.row])
                return cell
            } else {
                let cell : MerchantListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "merchantCell", for: indexPath) as! MerchantListTableViewCell
                cell.loadCell (dictionary: merchantDictionary1[indexPath.row])
                return cell
            }
        }
    }
}

extension MerchantListViewController : ViewAllStatusProtocol {
    func showViewAllStatusVC() {
        let viewAllStatusVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: "BonusPointResaleStatusViewController_ID")
        self.navigationController?.pushViewController(viewAllStatusVC, animated: true)
    }
}

extension MerchantListViewController : UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            uiButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize) ?? UIFont.systemFont(ofSize: 14)
            uiButton.setTitle("Cancel".localized, for:.normal)
        }
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.merchantListTableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.merchantListTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.merchantListTableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.hasPrefix(" ") {
            searchBar.text?.removeFirst()
            return
        }
        if searchText.contains(find: "  ") {
            searchBar.text?.removeLast()
            return
        }
        if searchText.containsEmoji {
            searchBar.text?.removeLast()
            return
        }
        if searchText.count > 50 {
            searchBar.text?.removeLast()
            return 
        }
        if screenType == "Bonus Point Resale" {
            if merchantDetailsDictionary.count > 0 {
                filteredMerchant = merchantDetailsDictionary.filter { pair in
                    let dict = pair.merchant
                    guard let mallName = dict.safeValueForKey("shopName") as? String else { return false }
                    return mallName.lowercased().hasPrefix(searchText.lowercased())
                }
            }
        } else {
            if merchantDictionary1.count > 0 {
                filteredMerchantDictionary = merchantDictionary1.filter({ (dict) -> Bool in
                    return (dict.safeValueForKey("name") as? String ?? "").lowercased().hasPrefix(searchText.lowercased())
                })
            }
        }
        if screenType == "Bonus Point Resale" {
            if filteredMerchant.count == 0, searchText.count > 0 {
                self.noRecordView.isHidden = false
            } else {
                self.noRecordView.isHidden = true
            }
        } else {
            if filteredMerchantDictionary.count == 0, searchText.count > 0 {
                self.noRecordView.isHidden = false
            } else {
                self.noRecordView.isHidden = true
            }
        }
        self.merchantListTableView.reloadData()
    }
    
}
