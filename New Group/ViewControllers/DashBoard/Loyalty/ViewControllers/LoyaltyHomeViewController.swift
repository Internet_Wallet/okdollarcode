//
//  LoyaltyHomeViewController.swift
//  OK
//
//  Created by Kethan on 2/26/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct loyaltyMainViews {
    static let loyaltyStoryBoard = "LoyaltyModule"
    static let loyaltyAdvModuleStory = "LoyaltyAdvanceMerchant"

    struct viewControllers {
        static let bonusConfigVC = "BonusConfigurationViewController_ID"
        static let bonusPointResaleVC = "BonusPointResaleViewController_ID"
        static let discountMoneyVC = "DiscountMoneyHomeViewController_ID"
        static let earnPointTransferVC = "EarnPointTransferViewController_ID"
        static let buyBonusPointVC = "BuyBonusPointViewController_ID"
        static let selectAdvMerchantVC = "SelectAdvanceMerchantViewController_ID"
        static let selectAdvMerchantListVC = "AdvanceMerchantListViewController_ID"
    }
    
    struct tableViewCells {
        static let loyaltyMainTableCell = "loyaltyMainTableViewCell"
    }
}

class LoyaltyHomeViewController: LoyaltyBaseViewController, PTWebResponseDelegate {

    @IBOutlet weak var availablePointLbl: UILabel!
    @IBOutlet weak var earnPointLbl: UILabel!
    
    var loyaltyListArray = [Dictionary<String,Any>]()
    var loyatyDict = Dictionary<String,Any>()
    
    var balanceListArray = [Dictionary<String,Any>]()
    var balanceDict = Dictionary<String,Any>()
    
    var loyaltyMainDictionary = [Dictionary<String, String>]()
    
    @IBOutlet weak var loyaltyHomeTableView: UITableView!
    
    @IBOutlet weak var availablePointLblText: UILabel! {
        didSet {
            self.availablePointLblText.font = UIFont(name: appFont, size: appFontSize)
            self.availablePointLblText.text = self.availablePointLblText.text?.localized
        }
    }
    @IBOutlet weak var earnPointLblText: UILabel! {
        didSet {
            self.earnPointLblText.font = UIFont(name: appFont, size: appFontSize)
            self.earnPointLblText.text = self.earnPointLblText.text?.localized
        }
    }
    @IBOutlet weak var availablePointHeightConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addBackButton(navTitle: "Loyalty")
        initializeLoyaltyMainDictionary()
        let footerView = UIView()
        footerView.backgroundColor = UIColor.gray
        loyaltyHomeTableView.tableFooterView = footerView
        getAvalaibleBalance()
        NotificationCenter.default.addObserver(self, selector: #selector(backAction), name: NSNotification.Name(rawValue: "DismissLoyaltyView"), object: nil )
        // Do any additional setup after loading the view.
    }
    
    private func initializeLoyaltyMainDictionary() {
        if UserModel.shared.agentType == .user  {
            earnPointLblText.text = "Total Earn Point".localized
            availablePointHeightConstraint.constant = 0.0
            loyaltyMainDictionary = [["Title" : "Earn Point Transfer", "Image" : "earnpoint.png"], ["Title" : "Bonus Point Resale", "Image" : "bonuspoint_resale.png"], ["Title" : "Buy Bonus Points", "Image" : "buy_bonuspoint.png"], ["Title" : "Discount Money", "Image" : "discount_money.png"]]
        } else if UserModel.shared.agentType == .advancemerchant || UserModel.shared.agentType == .merchant || UserModel.shared.agentType == .agent {
            availablePointHeightConstraint.constant = 70.0
            earnPointLblText.text = "My Earn Point".localized
            loyaltyMainDictionary = [["Title" : "Merchant Configuration", "Image" : "merchant_confi.png"], ["Title" : "Earn Point Transfer", "Image" : "earnpoint.png"], ["Title" : "Bonus Point Resale", "Image" : "bonuspoint_resale.png"], ["Title" : "Buy Bonus Points", "Image" : "buy_bonuspoint.png"], ["Title" : "Discount Money", "Image" : "discount_money.png"]]
        }
    }
    
    private func getAvalaibleBalance() {
        if appDelegate.checkNetworkAvail() {
            let web      = PayToWebApi()
            web.delegate = self
            var ip = ""
            if let ipAdd = OKBaseController.getIPAddress() {
                ip = ipAdd
            }
            let urlStr   = String.init(format: Url.CheckLoyaltyBalanceApi, UserModel.shared.mobileNo, ok_password ?? "", UserModel.shared.mobileNo, ip , "ios", UserLogin.shared.token)
            let url = getUrl(urlStr: urlStr, serverType: .serverEstel)
//            println_debug(url)
            let pRam = Dictionary<String,String>()
            web.genericClassXML(url: url, param: pRam as AnyObject, httpMethod: "POST", mScreen: "mAvailableBalance")
        }
    }
    
    private func getLoyaltyDetails() {
        if appDelegate.checkNetworkAvail() {
            let web      = PayToWebApi()
            web.delegate = self
            var ip = ""
            if let ipAdd = OKBaseController.getIPAddress() {
                ip = ipAdd
            }
            
            let urlStr   = String.init(format: Url.bonusConfigApiUrl, UserModel.shared.mobileNo, ip , "ios", UserLogin.shared.token)
            let url = getUrl(urlStr: urlStr, serverType: .serverEstel)
//            println_debug(url)
            let pRam = Dictionary<String,String>()
            web.genericClassXML(url: url, param: pRam as AnyObject, httpMethod: "POST", mScreen: "mLoyaltyList")
        } else {
            self.noInternetAlert()
        }
    }
    
    private func addBackButton(navTitle: String) {
        let backButton = UIButton(type: .custom)
        backButton.setBackgroundImage(#imageLiteral(resourceName: "tabBarBack"), for: .normal)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        navigationItem.title = navTitle.localized
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white ]
        self.navigationController?.navigationBar.barTintColor = UIColor.red
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    @objc override func backAction() {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "DismissLoyaltyView") , object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func selectedIndex(selectedIndex : String) {
        switch selectedIndex {
        case "Merchant Configuration":
            if let bonusConfigVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: loyaltyMainViews.viewControllers.bonusConfigVC) as? BonusConfigurationViewController {
                bonusConfigVC.delegate = self
                bonusConfigVC.availablePoint = self.availablePointLbl.text ?? "0"
                    showSelectedViewController(viewController: bonusConfigVC)
            }
        case "Earn Point Transfer":
            if let earnPointTranfer = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: loyaltyMainViews.viewControllers.earnPointTransferVC) as? EarnPointTransferViewController {
                earnPointTranfer.earnPoints = earnPointLbl.text ?? "0"
                earnPointTranfer.delegate = self
                    showSelectedViewController(viewController: earnPointTranfer)
            }
        case "Bonus Point Resale":
            if let bonusPointResaleVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: loyaltyMainViews.viewControllers.bonusPointResaleVC) as? BonusPointResaleViewController {
                showSelectedViewController(viewController: bonusPointResaleVC)
            }
        case "Buy Bonus Points":
            if let discountMoneyVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: loyaltyMainViews.viewControllers.buyBonusPointVC) as? BuyBonusPointViewController {
                showSelectedViewController(viewController: discountMoneyVC)
            }
        case "Discount Money":
            if let discountMoneyVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: loyaltyMainViews.viewControllers.discountMoneyVC) as? DiscountMoneyHomeViewController {
                showSelectedViewController(viewController: discountMoneyVC)
            }
        default:
            println_debug("")
        }
    }
    
    private func showSelectedViewController(viewController : UIViewController) {
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    //PTWebresponseDelegate
    func webSuccessResult(data: Any, screen: String) {
//        println_debug(data)
//        println_debug(screen)
        
        if screen == "mLoyaltyList" {
            if let xmlString = data as? String {
                let xml = SWXMLHash.parse(xmlString)
                
                loyatyDict.removeAll()
                self.enumerate(indexer: xml)
                self.loyaltyListArray.removeAll()
                self.loyaltyListArray.append(loyatyDict)
//                println_debug(loyaltyListArray)
                self.removeProgressView()
                if loyatyDict["resultdescription"] as? String == "Transaction Successful" {
                } else {
                    DispatchQueue.main.async {
                        let alertMessage = "Transaction Successful".localized
                        loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: #imageLiteral(resourceName: "discount_money"))
                        loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                            
                        })
                        loyaltyAlertView.showAlert(controller: self)
                    }
                }
            }
        } else if screen == "mAvailableBalance" {
            if let xmlString = data as? String {
                let xml = SWXMLHash.parse(xmlString)
                
                balanceDict.removeAll()
                self.enumerateBalance(indexer: xml)
                self.balanceListArray.removeAll()
                self.balanceListArray.append(balanceDict)
//                println_debug(balanceListArray)
                self.removeProgressView()
                if balanceDict["resultdescription"] as? String == "Transaction Successful" {
                    DispatchQueue.main.async {
                        let formatter = NumberFormatter()
                        formatter.numberStyle = .decimal
                        if UserModel.shared.agentType == .user {
                            let earnBal = self.balanceDict["udv5"] as? String ?? "0"
                            let earnBalArray = earnBal.components(separatedBy: "|")
                            if  earnBalArray.count == 2 {
                                if let numberStr = formatter.string(from: Float(earnBalArray[1])! as NSNumber) {
                                    self.earnPointLbl.text = numberStr
                                    UserDefaults.standard.set(self.earnPointLbl.text!, forKey: "MyEarnPoint")
                                }
                            }
                        } else {
                            let availableBal = self.balanceDict["udv4"] as? String ?? "0"
                            let availableBalArray = availableBal.components(separatedBy: "|")
                            if  availableBalArray.count == 2 {
                                if let numberStr = formatter.string(from: Float(availableBalArray[1])! as NSNumber) {
                                    self.availablePointLbl.text = numberStr
                                }
                            }
                            let earnBal = self.balanceDict["udv5"] as? String ?? "0.0"
                            let earnBalArray = earnBal.components(separatedBy: "|")
                            if  earnBalArray.count == 2 {
                                if let numberStr = formatter.string(from: Float(earnBalArray[1])! as NSNumber) {
                                    self.earnPointLbl.text = numberStr
                                    UserDefaults.standard.set(self.earnPointLbl.text!, forKey: "MyEarnPoint")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            loyatyDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func enumerateBalance(indexer: XMLIndexer) {
        for child in indexer.children {
            balanceDict[child.element!.name] = child.element!.text
            enumerateBalance(indexer: child)
        }
    }
    
    
    func webFailureResult(screen: String) {
        println_debug("Failure")
    }
    
}

extension LoyaltyHomeViewController : pointDistributionProtocol {
    func currentPoint(pointText: String) {
        self.availablePointLbl.font = UIFont(name: appFont, size: appFontSize)
        self.availablePointLbl.text = pointText
        UserDefaults.standard.set(pointText, forKey: "MyEarnPoint")
    }
}


extension LoyaltyHomeViewController : earnPointDistributionProtocol {
    func currentEarnPoint(pointText: String) {
        self.earnPointLbl.font = UIFont(name: appFont, size: appFontSize)
        self.earnPointLbl.text = pointText
        UserDefaults.standard.set(pointText, forKey: "MyEarnPoint")
    }
}

extension LoyaltyHomeViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let title = self.loyaltyMainDictionary[indexPath.row]["Title"] {
            selectedIndex(selectedIndex: title)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

extension LoyaltyHomeViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: LoyaltyMainTableViewCell = tableView.dequeueReusableCell(withIdentifier: loyaltyMainViews.tableViewCells.loyaltyMainTableCell, for: indexPath) as! LoyaltyMainTableViewCell
        cell.accessoryType = .disclosureIndicator
        cell.tintColor = UIColor.darkGray
        cell.loadCell(dict: loyaltyMainDictionary[indexPath.row])
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return loyaltyMainDictionary.count
    }
    
}



