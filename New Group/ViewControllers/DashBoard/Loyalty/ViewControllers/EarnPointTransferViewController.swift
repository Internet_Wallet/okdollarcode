//
//  EarnPointTransferViewController.swift
//  OK
//
//  Created by Kethan on 3/9/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol earnPointDistributionProtocol : class {
    func currentEarnPoint(pointText : String)
}

class EarnPointTransferViewController: LoyaltyBaseViewController, UINavigationControllerDelegate, UITextFieldDelegate, CountryLeftViewDelegate, CountryViewControllerDelegate, PTWebResponseDelegate {
    enum ScreenFrom {
        case dashboard, other
    }
     var screenFrom = ScreenFrom.other
     var reciptModel : loyaltyPointTrfModel?
    
    func clearActionType() {
        // Added in delegate 
    }
    var balanceListArray = [Dictionary<String,Any>]()
    var balanceDict = Dictionary<String,Any>()
    var merchantDetailsModelArray = [merchantDetailsModel]()

    var earnPoints = "0"
    var selectedMerchantId = ""
    var selectedShopName = ""
    @IBOutlet weak var earnPointScrollView: UIScrollView!
    @IBOutlet weak var heightConfMobNum: NSLayoutConstraint!
    @IBOutlet weak var pointsLabel: UILabel!{
        didSet{
            self.pointsLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var pointsLabelValue: UILabel!{
        didSet{
            self.pointsLabelValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var merchaneNameLbl: UILabel! {
        didSet {
            self.merchaneNameLbl.font = UIFont(name: appFont, size: appFontSize)
            self.merchaneNameLbl.text = self.merchaneNameLbl.text?.localized
        }
    }
    @IBOutlet weak var mobileNumberTxt: BonuspointSubClassTextField! {
        didSet {
            self.mobileNumberTxt.font = UIFont(name: appFont, size: appFontSize)
            self.mobileNumberTxt.placeholder = self.mobileNumberTxt.placeholder?.localized
        }
    }
    @IBOutlet weak var confirmMobileNumberTxt: BonuspointSubClassTextField! {
        didSet {
            self.confirmMobileNumberTxt.font = UIFont(name: appFont, size: appFontSize)
            self.confirmMobileNumberTxt.placeholder = self.confirmMobileNumberTxt.placeholder?.localized
        }
    }
    let mobileNumberAcceptableCharacters = "0123456789"
    @IBOutlet weak var pointsText: UITextField! {
        didSet {
            self.pointsText.font = UIFont(name: appFont, size: appFontSize)
            self.pointsText.placeholder = "Enter Points".localized
        }
    }
    
    let validObj  = PayToValidations()
    var contactSuggessionView   : ContactSuggestionVC?
    @IBOutlet var imageFile: UIImage!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var addFrmFavBtn : UIButton! {
        didSet {
            self.addFrmFavBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.addFrmFavBtn.layer.masksToBounds = true
            self.addFrmFavBtn.layer.cornerRadius = 5.0
            self.addFrmFavBtn.setTitle(self.addFrmFavBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var mobClearButton : UIButton! {
        didSet {
            self.mobClearButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.mobClearButton.isHidden = true
        }
    }
    
    @IBOutlet weak var confMobClearButton : UIButton! {
        didSet {
            self.confMobClearButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.confMobClearButton.isHidden = true
        }
    }
    
    @IBOutlet weak var pointsClearButton : UIButton! {
        didSet {
            self.pointsClearButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.pointsClearButton.isHidden = true
        }
    }
    
    @IBOutlet weak var addFrmOKAccountBtn : UIButton! {
        didSet {
            self.addFrmOKAccountBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.addFrmOKAccountBtn.layer.masksToBounds = true
            self.addFrmOKAccountBtn.layer.cornerRadius = 5.0
            self.addFrmOKAccountBtn.setTitle(self.addFrmOKAccountBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var buttonView : UIView! {
        didSet {
            self.buttonView.isHidden = true
        }
    }
    
    @IBOutlet weak var mobileNumberView : UIView! {
        didSet {
            self.mobileNumberView.isHidden = false
        }
    }
    
    @IBOutlet weak var confirmMobileNumberView : UIView! {
        didSet {
            self.confirmMobileNumberView.isHidden = true
        }
    }
    
    @IBOutlet weak var selectMerchantView : UIView! {
        didSet {
            self.selectMerchantView.isHidden = true
        }
    }
    @IBOutlet weak var pointsView : UIView! {
        didSet {
            self.pointsView.isHidden = true
        }
    }
    
    @IBOutlet weak var transferButton : UIButton! {
        didSet {
            self.transferButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.transferButton.setTitle("TransferEPt".localized, for: .normal)
            self.transferButton.isHidden = true
        }
    }
    
    var selectedMerchantPoints = 0.0
    
    struct CountryPayTo {
        var code = ""
        var flag = ""
        
        init(code: String, flag: String) {
            self.code = code
            self.flag = flag
        }
    }
    
    @IBOutlet weak var tblPerson: UITableView!
    weak var delegate : earnPointDistributionProtocol?
    var countryObject = CountryPayTo.init(code: "+95", flag: "myanmar")
    var leftViewCountry                = PaytoViews.updateView()
    var leftViewCountryForConfirmation = PaytoViews.updateView()
    
    var pointListArray = [Dictionary<String,Any>]()
    var pointDict = Dictionary<String,Any>()
    
    var merchantListDict = Dictionary<String,Any>()
    var merchantListArray = [Dictionary<String,Any>]()
    
    //AccessoryView
    var submitView: UIView {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 50))
        let btn = UIButton.init(frame: view.frame)
        btn.backgroundColor = UIColor.red
        btn.setTitle("TransferEPt".localized, for: .normal)
        if let myFont = UIFont(name: appFont, size: 18) {
            btn.titleLabel?.font =  myFont
        }
        btn.addTarget(self, action: #selector(transferButtonAction(_:)), for: UIControl.Event.touchUpInside)
        view.addSubview(btn)
        return view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        self.pointsLabelValue.text = earnPoints
        pointsText.inputAccessoryView = self.submitView
        pointsText.inputAccessoryView?.isHidden = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
        switch screenFrom {
        case .dashboard:
            getAvalaibleBalance()
        case .other:
            break
        }
        NotificationCenter.default.addObserver(self, selector: #selector(repeatEarnPointTrf(_:)), name: Notification.Name("RepeatEarnPoint"), object: nil)
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
        self.hideContactSuggesionView()
    }
    
    fileprivate func showAccessoryView(status : Bool) {
        if status {
            pointsText.inputAccessoryView?.isHidden = false
        } else {
            pointsText.inputAccessoryView?.isHidden = true
        }
    }
    
    private func getAvalaibleBalance() {
        if appDelegate.checkNetworkAvail() {
            let web      = PayToWebApi()
            web.delegate = self
            var ip = ""
            if let ipAdd = OKBaseController.getIPAddress() {
                ip = ipAdd
            }
            let urlStr   = String.init(format: Url.CheckLoyaltyBalanceApi, UserModel.shared.mobileNo, ok_password ?? "", UserModel.shared.mobileNo, ip , "ios", UserLogin.shared.token)
            let url = getUrl(urlStr: urlStr, serverType: .serverEstel)
            //            println_debug(url)
            let pRam = Dictionary<String,String>()
            web.genericClassXML(url: url, param: pRam as AnyObject, httpMethod: "POST", mScreen: "mAvailableBalance")
        }
    }

    private func addBckButton(navTitle: String) {
        let backButton = UIButton(type: .custom)
        backButton.setBackgroundImage(#imageLiteral(resourceName: "tabBarBack"), for: .normal)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        backButton.addTarget(self, action: #selector(bckAction), for: .touchUpInside)
        navigationItem.title = navTitle.localized
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white ]
        self.navigationController?.navigationBar.barTintColor = UIColor.red
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    @objc func repeatEarnPointTrf(_ notification: Notification) {
        guard let userInfoDict = notification.userInfo else { return }
        if var phoneNumber = userInfoDict["number"] as? String {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                
                
                self.mobileNumberTxt.text = ""
                if phoneNumber.hasPrefix("00") {
                    _ = phoneNumber.remove(at: String.Index.init(encodedOffset: 0))
                    _ = phoneNumber.remove(at: String.Index.init(encodedOffset: 0))
                    
                    if phoneNumber.hasPrefix("+") {
                    } else {
                        phoneNumber = "+" + phoneNumber
                    }
                }
                
                let countryArray = self.revertCountryArray() as! Array<NSDictionary>
                
                let cDetails = self.identifyCountry(withPhoneNumber: phoneNumber, inCountryArray: countryArray) // 1 -> flag, 0 -> code
                var countryCode = String.init(format: "(%@)", "+95")
                if cDetails.0 == "" || cDetails.1 == "" {
                    self.countryObject.flag = "myanmar"
                    self.countryObject.code = "+95"
                    self.leftViewCountry.wrapCountryViewData(img: "myanmar", str: countryCode)
                    self.leftViewCountryForConfirmation.wrapCountryViewData(img: "myanmar", str: countryCode)
                } else {
                    self.countryObject.flag = cDetails.1
                    self.countryObject.code = cDetails.0
                    countryCode = String.init(format: "(%@)", cDetails.0)
                    self.leftViewCountry.wrapCountryViewData(img: cDetails.1, str: countryCode)
                    self.leftViewCountryForConfirmation.wrapCountryViewData(img: cDetails.1, str: countryCode)
                }
                
                
                //        self.mobileNumberTxt.leftView     = nil
                //        self.confirmMobileNumberTxt.leftView = nil
                
                self.mobileNumberTxt.leftView     = self.leftViewCountry
                self.mobileNumberTxt.leftViewMode = UITextField.ViewMode.always
                
                self.view.layoutIfNeeded()
                
                var phone = ""
                
                if phoneNumber.hasPrefix("+95") || phoneNumber.hasPrefix("(+95)") {
                    phone = phoneNumber.replacingOccurrences(of: cDetails.0, with: "0")
                    phone = phone.replacingOccurrences(of: " ", with: "")
                    
                    if phone.hasPrefix("0") {
                        self.mobileNumberTxt.text = phone
                    } else {
                        self.mobileNumberTxt.text = "0" + phone
                    }
                    
                }else {
                    phone = phoneNumber.replacingOccurrences(of: cDetails.0, with: "")
                    phone = phone.replacingOccurrences(of: " ", with: "")
                    self.mobileNumberTxt.text = phone
                }
                
                let validations = PayToValidations().getNumberRangeValidation(self.mobileNumberTxt.text ?? "")
                if validations.isRejected || phoneNumber.count < validations.min {
                    loyaltyAlertView.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: UIImage(named : "AppIcon"))
                    loyaltyAlertView.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                        self.mobileNumberTxt.text = (self.countryObject.code == "+95") ? "09" : ""
                        self.mobClearButton.isHidden = true
                        self.updateMobileUI(count: 0, textField: self.mobileNumberTxt)
                    })
                    loyaltyAlertView.showAlert(controller: self)
                    return
                }
                
                self.nameORnumber(selectiontype: .name, codeDetails: cDetails)
                
                self.selectMerchantView.isHidden = false
                self.merchaneNameLbl.text = "Select Merchant".localized
                self.mobClearButton.isHidden = false
                self.mobileNumberTxt.resignFirstResponder()
                self.heightConfMobNum.constant = 0
                self._verifyInBackEnd(number: phone, completion: { (isFound) in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func bckAction() {
        self.view.endEditing(true)
        switch screenFrom {
        case .dashboard:
            self.dismiss(animated: true, completion: nil)
        case .other:
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    private func initialSetUp() {
        getAllMerchantListEstel()
        addBckButton(navTitle : "Earning Point Transfer")
        pointsLabel.text = "Total Earn Point".localized
        self.loadContactSuggessionView()
        self.updateViewsInitialLogic()
        self.mobileNumberTxt.text?.removeAll()
        self.confirmMobileNumberTxt.text?.removeAll()
        //self.mobileNumberTxt.text = commonPrefixMobileNumber
        self.wrapMobileLeftView()
        
        //self.mobileNumberTxt.text = ""
        self.confirmMobileNumberTxt.text = ""
        self.pointsText.text = ""
        self.mobileNumberTxt.placeholder = mobileNumberTxt.placeholder?.localized
        self.mobileNumberTxt.text = commonPrefixMobileNumber
        self.confirmMobileNumberView.isHidden = true
        self.selectMerchantView.isHidden = true
        self.pointsView.isHidden = true
        self.transferButton.isHidden = true
        self.showAccessoryView(status : false)
        self.mobClearButton.isHidden = true
        self.confMobClearButton.isHidden = true
        self.pointsClearButton.isHidden = true
        self.merchaneNameLbl.text = "Select Merchant".localized
        self.view.endEditing(true)
        
        self.confirmMobileNumberTxt.isUserInteractionEnabled = true
        self.confirmMobileNumberTxt.placeholder = "Confirm Mobile Number".localized
        self.confirmMobileNumberTxt.isEnabled = true
        let countryCode = String.init(format: "(%@)", countryObject.code)
        leftViewCountryForConfirmation.wrapCountryViewData(img: countryObject.flag , str: countryCode)
        self.confirmMobileNumberTxt.leftView = leftViewCountryForConfirmation
        self.confirmMobileNumberTxt.leftViewMode  = UITextField.ViewMode.always
        self.mobileNumberTxt.becomeFirstResponder()
    }
    
    private func setUpRepeat(phoneNumber: String, repeatPay: Bool) {
        if !repeatPay  {
            let countryCode = String.init(format: "(%@)", "+95")
            countryObject.flag = "myanmar"
            countryObject.code = "+95"
            leftViewCountry.wrapCountryViewData(img: "myanmar", str: countryCode)
            leftViewCountryForConfirmation.wrapCountryViewData(img: "myanmar", str: countryCode)
            
            self.mobileNumberTxt.text = (self.countryObject.code == "+95") ? "09" : ""
            self.mobClearButton.isHidden = true
            self.updateMobileUI(count: 0, textField: self.mobileNumberTxt)
            self.mobileNumberTxt.becomeFirstResponder()
        } else {
            let countryArray = revertCountryArray() as! Array<NSDictionary>
            let cDetails = self.identifyCountry(withPhoneNumber: phoneNumber, inCountryArray: countryArray) // 1 -> flag, 0 -> code
            var countryCode = String.init(format: "(%@)", "+95")
            if cDetails.0 == "" || cDetails.1 == "" {
                countryObject.flag = "myanmar"
                countryObject.code = "+95"
                leftViewCountry.wrapCountryViewData(img: "myanmar", str: countryCode)
                leftViewCountryForConfirmation.wrapCountryViewData(img: "myanmar", str: countryCode)
            } else {
                countryObject.flag = cDetails.1
                countryObject.code = cDetails.0
                countryCode = String.init(format: "(%@)", cDetails.0)
                leftViewCountry.wrapCountryViewData(img: cDetails.1, str: countryCode)
                leftViewCountryForConfirmation.wrapCountryViewData(img: cDetails.1, str: countryCode)
            }
            self.mobileNumberTxt.leftView     = leftViewCountry
            self.mobileNumberTxt.leftViewMode = UITextField.ViewMode.always
            
            self.view.layoutIfNeeded()
            
            var phone = ""
            
            phone = self.getFormattedNumberInLoyalty(number: phoneNumber)
            self.mobileNumberTxt.text = phone
            
            let validations = PayToValidations().getNumberRangeValidation(self.mobileNumberTxt.text ?? "")
            if validations.isRejected || phoneNumber.count < validations.min {
                loyaltyAlertView.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: UIImage(named : "AppIcon"))
                loyaltyAlertView.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                    self.mobileNumberTxt.text = (self.countryObject.code == "+95") ? "09" : ""
                    self.mobClearButton.isHidden = true
                    self.updateMobileUI(count: 0, textField: self.mobileNumberTxt)
                })
                loyaltyAlertView.showAlert(controller: self)
                return
            }
            
            self.nameORnumber(selectiontype: .name, codeDetails: cDetails)
            self.selectMerchantView.isHidden = false
            merchaneNameLbl.text = "Select Merchant".localized
            self.mobClearButton.isHidden = false
            mobileNumberTxt.resignFirstResponder()
            self.heightConfMobNum.constant = 0
            _verifyInBackEnd(number: phone, completion: { (isFound) in
                self.view.endEditing(true)
                self.view.layoutIfNeeded()
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        buttonView.isHidden = true
    }
    
    
    private func showSelectedViewController(viewController : UIViewController) {
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func hideContactSuggesionView() {
        if let contactview = contactSuggessionView {
            contactview.view.isHidden = true
        }
        self.earnPointScrollView.isScrollEnabled = true
    }
    
    @IBAction func addFromFavoriteAction(_ sender : UIButton) {
        let vc = self.openFavoriteFromNavigation(self, withFavorite: .payto)
        //self.navigationController?.present(vc, animated: true, completion: nil)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func addFromOKAccountAction(_ sender : UIButton) {
        let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
        self.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func hideAndShowContactsButton(_ sender : UIButton) {
        if buttonView.isHidden == true {
            buttonView.isHidden = false
        } else {
            buttonView.isHidden = true
        }
    }
    
    
    func openAction(type: ButtonType) {
        self.navigationController?.present(countryViewController(delegate: self), animated: true, completion: nil)
    }
    
    private func loadContactSuggessionView() {
        contactSuggessionView  =  UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "ContactSugestView_ID") as? ContactSuggestionVC
        contactSuggessionView?.delegate   = self //as? ContactSuggestionDelegate
        contactSuggessionView?.view.frame = CGRect.zero
        addChild(contactSuggessionView!)
        self.view.addSubview(contactSuggessionView!.view)
        contactSuggessionView!.didMove(toParent: self)
        contactSuggessionView?.view.isHidden = true
    }
    
    
    func nameORnumber(selectiontype: selectionType, codeDetails: (String, String)) {
        self.view.endEditing(true)
        switch selectiontype {
        case .name:
            _ = DefaultIconView.updateView(icon: "name")
        case .number:
            leftViewCountryForConfirmation.wrapCountryViewData(img: codeDetails.1, str: codeDetails.0)
            self.heightConfMobNum.constant = 60
            self.confirmMobileNumberView.isHidden = true
            self.selectMerchantView.isHidden = true
            self.pointsView.isHidden = true
            
            self.confirmMobileNumberTxt.text = ""
            self.pointsText.text = ""
        }
    }
    
    fileprivate func getSuggessionviewFrame(arr: Int) -> CGRect {
        _ = self.mobileNumberView.bounds
        var height          : CGFloat = CGFloat(Float(arr) * 70.0)
        let cellHgt         : CGFloat = 70.0
        let normalHeaderHgt : CGFloat = 40.0
        
        let maxHeight = screenHeight - 200 - cellHgt - normalHeaderHgt // - cellHgt
        if arr > 5 {
            height = maxHeight
        }
        
        var yPos = 205.0
        let device = UIDevice()
        if device.type == .iPhoneX || device.type == .iPhoneXR || device.type == .iPhoneXS || device.type == .iPhoneXSMax {
            yPos = 228.0
        }
        return CGRect(x: 50.0, y: yPos, width: Double(screenWidth - 60.0), height: Double(height))
//        return CGRect(x: 50.0, y:  yPos , width: screenWidth - 60.0, height: height)
    }
    
    func showContactSuggessionView(withFrame frame: CGRect, andContacts contactList: [Dictionary<String, Any>]) {
        contactSuggessionView?.view.isHidden    = false
        contactSuggessionView?.view.frame       = frame
        contactSuggessionView?.contactsList     = contactList
        contactSuggessionView?.contactsTable.reloadData()
        contactSuggessionView?.view.layoutIfNeeded()
        self.earnPointScrollView.isScrollEnabled = false
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        contactSuggessionView?.view.isHidden = true
    }
    
    func getActualMobileNum(_ phoneNum: String, withCountryCode countryCode: String) -> String {
        var FormateStr: String
        if phoneNum.hasPrefix("0") {
            _ = (phoneNum).substring(from: 1)
        }
        let notAllowedChars = CharacterSet(charactersIn: "+()")
        let resultString: String = countryCode.components(separatedBy: notAllowedChars).joined(separator: "")
        if (countryCode == "+95") || (countryCode == "(+95)") {
            if phoneNum.hasPrefix("0095") {
                FormateStr = "+95\((phoneNum).substring(from: 4))"
            } else if phoneNum.hasPrefix("0091") {
                FormateStr = "+91\((phoneNum).substring(from: 4))"
            } else if phoneNum.hasPrefix("95") {
                FormateStr = "+95\((phoneNum).substring(from: 2))"
            } else {
                FormateStr = "\(phoneNum)"
            }
        } else {
            FormateStr = "00\(resultString)\(phoneNum)"
        }
        return FormateStr
    }
    
    //Select merchant action
    @IBAction func selectMerchantButtonAction(_ sender : UIButton) {
        if let merchantListVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: "MerchantListViewController_ID") as? MerchantListViewController {
            merchantListVC.screenType = "Earn Point Transfer"
            merchantListVC.merchantDictionary = self.merchantListArray
            merchantListVC.delegate = self
            showSelectedViewController(viewController: merchantListVC)
        }
    }
    
    @IBAction func transferButtonAction(_ sender : UIButton) {
        self.earnPointTransfer()
    }
    
    
    func getAllMerchantList() {
        self.getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
            guard isSuccess, let tokenAuthStr = tokenString as? String else {
                self.showCustomErrorAlert(errMessage: "try again")
                return
            }
            DispatchQueue.main.async(execute: {
                self.getAllMerchantList(authToken: tokenAuthStr)
            })
        })
    }
    
    
    private func getAllMerchantList(authToken: String) {
        
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            
            let urlStr   = String.init(format: Url.getMerchantListUrl, UserModel.shared.mobileNo, UserModel.shared.name, ok_password ?? "")
            let url = getUrl(urlStr: urlStr, serverType: .bonusPointApi)
            let params = [String: Any]()
            web.genericClassRegLoyalty(url: url, param: params, httpMethod: "GET", mScreen: "mMerchantList", authToken: authToken)
        } else {
            
        }
    }
    
    private func getAllMerchantListEstel() {
        if appDelegate.checkNetworkAvail() {
            let web      = PayToWebApi()
            web.delegate = self
            var ip = ""
            if let ipAdd = OKBaseController.getIPAddress() {
                ip = ipAdd
            }
            let urlStr   = String.init(format: Url.loyaltyViewApiUrl, UserModel.shared.mobileNo, ok_password ?? "",  ip , "ios",  UserLogin.shared.token )
            println_debug(urlStr)
            let url = getUrl(urlStr: urlStr, serverType: .serverEstel)
            let pRam = Dictionary<String,String>()
            web.genericClassXML(url: url, param: pRam as AnyObject, httpMethod: "POST", mScreen: "mMerchantList")
        }
    }
    
    func earnPointTransfer() {
        if appDelegate.checkNetworkAvail() {
            let urlString = String.init(format: "%@", Url.earnPointTrfApiUrl)
            let url = getUrl(urlStr: urlString, serverType: .serverApp)
            let params = self.JSONStringFromAnyObject(value: self.getParamsForEarnTrf() as AnyObject)
            println_debug(url)
            println_debug(params)
            DispatchQueue.main.async {
                TopupWeb.genericClass(url: url, param: params as AnyObject, httpMethod: "POST") { [weak self](response, success) in
                    if success {
                        if let json = response as? Dictionary<String,Any> {
                            println_debug(json)
                            if let code = json["Code"] as? NSNumber, code == 200 {
                                if var data = json["Data"] as? String {
                                    data = data.replacingOccurrences(of: "\"", with: "")
                                    let xmlString = SWXMLHash.parse(data)
                                    println_debug(xmlString)
                                    self?.pointDict.removeAll()
                                    self?.enumerate(indexer: xmlString)
                                    self?.parserXMLResult()
                                }
                            } else {
                                if let msg = json["Msg"] as? String {
                                    self?.showErrorAlert(errMessage: msg)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
 

    private func getParamsForEarnTrf() -> Dictionary<String,Any> {
        var loginParams = Dictionary<String,Any>()
        loginParams["AppId"] = LoginParams.setUniqueIDLogin()
        loginParams["Limit"] = 0
        loginParams["MobileNumber"] = UserModel.shared.mobileNo
        loginParams["Msid"] = getMsid()
        loginParams["Offset"] = 0
        loginParams["Ostype"] = 1
        loginParams["Otp"] = ""
        loginParams["Simid"] = simid
        
        var ip = ""
        if let ipAdd = OKBaseController.getIPAddress() {
            ip = ipAdd
        }
         let mobileNo = getAgentCode(numberWithPrefix: self.mobileNumberTxt.text!, havingCountryPrefix: self.countryObject.code)

        var bonusParams = Dictionary<String,Any>()
        bonusParams["Agentcode"]    = UserModel.shared.mobileNo
        bonusParams["Clientip"]      = ip
        bonusParams["Clientos"]      = "ios"
        bonusParams["Clienttype"] = "SELFCARE"
        bonusParams["Destination"]            = mobileNo
        bonusParams["Pin"]            = ok_password.safelyWrappingString()
        bonusParams["Points"]             = self.removeCommaFromDigit(pointsText.text ?? "")
        bonusParams["Requesttype"]      = "LOYALTYPOINTTRF"
        bonusParams["Securetoken"]           = UserLogin.shared.token
        bonusParams["Shopname"]               = selectedShopName
        bonusParams["Vendorcode"]            = "IPAY"
        bonusParams["IsPaymentGateway"]         = "0"
        bonusParams["MerchantNumber"]       = self.selectedMerchantId
        
        var finalParams = Dictionary<String,Any>()
        finalParams["LoginInfo"] = loginParams
        finalParams["EpTransferDto"] = bonusParams
        
        return finalParams
    }
    
    private func updateAvailablePoints(points : String) {
        DispatchQueue.main.async {
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            let text = (self.pointsLabelValue.text ?? "0").replacingOccurrences(of: ",", with: "")
            var pointsFloatVal  = (text as NSString?)?.floatValue
            
            pointsFloatVal = (pointsFloatVal ?? 0.0) - Float(points)!
            if let numberStr = formatter.string(from: pointsFloatVal! as NSNumber) {
                self.pointsLabelValue.text = numberStr 
            }
            self.delegate?.currentEarnPoint(pointText: self.pointsLabelValue.text ?? "")
        }
    }
    
    
    fileprivate func showReceiptScreen() {
        DispatchQueue.main.async {
            if let model = self.reciptModel {
                if let recieptVC = self.storyboard?.instantiateViewController(withIdentifier: "LOPointsTrfReceiptViewController_ID") as? LOPointsTrfReceiptViewController {
                    recieptVC.delegate = self
                    recieptVC.type = "EarnPointTrf"
                    recieptVC.reciptModel = model
                    self.navigationController?.pushViewController(recieptVC, animated: true)
                }
            }
        }
    }
    
    func parserXMLResult() {
        
        self.pointListArray.removeAll()
        self.pointListArray.append(pointDict)
        //                println_debug(pointListArray)
        self.removeProgressView()
        if pointDict["resultdescription"] as? String == "Transaction Successful" {
            var transID = ""
            var dest = ""
            var source = ""
            var dTime: Date?
            var balance = ""
            var amount = ""
            var shopName = ""
            if let bal = pointDict["merearnedpoints"] as? String {
                balance = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: bal)
            }
            if let dT = pointDict["requestcts"] as? String {
                dTime = dT.dateValue(dateFormatIs: "dd-MMM-yyyy HH:mm:ss")
            }
            if let destNo = pointDict["destination"] as? String {
                dest = destNo
                if destNo.hasPrefix("0095") {
                    dest = "0\((destNo).substring(from: 4))"
                }
            }
            if let src = pointDict["source"] as? String {
                source = src
                if src.hasPrefix("0095") {
                    source = "0\((src).substring(from: 4))"
                }
            }
            if let shName = pointDict["shopname"] as? String {
                shopName = shName
            }
            if let amnt = pointDict["amount"] as? String {
                amount = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: amnt)
            }
            if let tID = pointDict["transid"] as? String {
                transID = tID
            }
            let loyaltyDB = LoyaltyDBData(balancePoint: balance, dateTime: dTime, destinationNo: dest,
                                          merchantNo: source, shopName: shopName,
                                          transferPoint: amount, transID: transID,
                                          type: "LOYALTY TRANSFER")
            println_debug(loyaltyDB)
            LoyaltyCoreDataManager.sharedInstance.insertLoyaltyPointsInfo(dataToInsert: loyaltyDB)
            
            self.updateAvailablePoints(points: pointDict["points"] as? String ?? "0.0")
           // let remBal =  self.selectedMerchantPoints - (Double(amount) ?? 0.0)
            DispatchQueue.main.async {
                self.updateAvailablePoints(points: self.pointDict["walletbalance"] as? String ?? "0.0")
                if self.pointListArray.count > 0 {
                    self.reciptModel = loyaltyPointTrfModel.init(dict: self.pointListArray[0])
                }
                self.reciptModel?.category = "LOYALTY TRANSFER"
                self.reciptModel?.merchantName = self.selectedShopName
                var pointsFloatVal  = (self.pointsText.text as NSString?)?.doubleValue ?? 0
                pointsFloatVal = self.selectedMerchantPoints - pointsFloatVal
                self.reciptModel?.balance =  "\(pointsFloatVal)"
                self.showReceiptScreen()
             }
            
        } else {
            DispatchQueue.main.async {
                let alertMessage = self.pointDict.safeValueForKey("resultdescription") as? String ?? ""
                loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: #imageLiteral(resourceName: "dashboard_loyalty"))
                loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                    self.navigationController?.popViewController(animated: true)
                })
                loyaltyAlertView.showAlert(controller: self)
            }
        }
    }

    
    //PTWebresponseDelegate
    func webSuccessResult(data: Any, screen: String) {
//        println_debug(data)
//        println_debug(screen)
         if screen == "mMerchantList" {
            if let xmlString = data as? String {
                let xml = SWXMLHash.parse(xmlString)
                
                merchantListDict.removeAll()
                self.enumerateMerchant(indexer: xml)
                self.merchantListArray.removeAll()
                self.merchantListArray.append(merchantListDict)
//                println_debug(pointListArray)
                self.removeProgressView()
                if merchantListDict["resultdescription"] as? String == "Transaction Successful" {
                    
                }
            }
         } else if screen == "mAvailableBalance" {
            if let xmlString = data as? String {
                let xml = SWXMLHash.parse(xmlString)
                
                balanceDict.removeAll()
                self.enumerateBalance(indexer: xml)
                self.balanceListArray.removeAll()
                self.balanceListArray.append(balanceDict)
                //                println_debug(balanceListArray)
                self.removeProgressView()
                if balanceDict["resultdescription"] as? String == "Transaction Successful" {
                    DispatchQueue.main.async {
                        let formatter = NumberFormatter()
                        formatter.numberStyle = .decimal
                        if UserModel.shared.agentType == .user {
                            let earnBal = self.balanceDict["udv5"] as? String ?? "0"
                            let earnBalArray = earnBal.components(separatedBy: "|")
                            if  earnBalArray.count == 2 {
                                if let numberStr = formatter.string(from: Float(earnBalArray[1])! as NSNumber) {
                                    self.pointsLabelValue.text = numberStr
                                    UserDefaults.standard.set(self.pointsLabelValue.text!, forKey: "MyEarnPoint")
                                }
                            }
                        } else {
                            let availableBal = self.balanceDict["udv4"] as? String ?? "0"
                            let availableBalArray = availableBal.components(separatedBy: "|")
                            if  availableBalArray.count == 2 {
                                if let numberStr = formatter.string(from: Float(availableBalArray[1])! as NSNumber) {
                                    self.pointsLabelValue.text = numberStr
                                }
                            }
                            let earnBal = self.balanceDict["udv5"] as? String ?? "0.0"
                            let earnBalArray = earnBal.components(separatedBy: "|")
                            if  earnBalArray.count == 2 {
                                if let numberStr = formatter.string(from: Float(earnBalArray[1])! as NSNumber) {
                                    self.pointsLabelValue.text = numberStr
                                    UserDefaults.standard.set(self.pointsLabelValue.text!, forKey: "MyEarnPoint")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            pointDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func enumerateBalance(indexer: XMLIndexer) {
        for child in indexer.children {
            balanceDict[child.element!.name] = child.element!.text
            enumerateBalance(indexer: child)
        }
    }
    
    func enumerateMerchant(indexer: XMLIndexer) {
        for child in indexer.children {
            merchantListDict[child.element!.name] = child.element!.text
            enumerateMerchant(indexer: child)
        }
    }
    
    func webFailureResult(screen: String) {
        println_debug("Failure")
    }
    
    fileprivate func _verifyInBackEnd(number :String, completion :@escaping (Bool)->()) {
        if number.hasPrefix(UserModel.shared.formattedNumber) {
            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.ownMobileNumber.localized)
            self.mobileNumberTxt.text = (self.countryObject.code == "+95") ? "09" : ""
            self.confirmMobileNumberView.isHidden = true
            return
        }
        
        let urlString = "\(Url.URLgetOKacDetails)" + "\(getConatctNum(number,withCountryCode: self.countryObject.code))"
        
        let trimmedString = urlString.components(separatedBy: .whitespaces).joined()
        
        let url = getUrl(urlStr: trimmedString, serverType: .serverApp)
        
        progressViewObj.showProgressView()
        
        JSONParser.GetApiResponse(apiUrl: url, type: "POST") { [weak self] (status : Bool, data : Any?) in
            
            DispatchQueue.main.async {
                
                if status == true {
                    
                    completion(true)
                } else {
                    
                    if let _ = data as? String  {
                        
                        completion(false)
                        DispatchQueue.main.async {
                            loyaltyAlertView.wrapAlert(title: nil, body: "Are you sure want to transfer earn points to unregistered number?".localized, img: #imageLiteral(resourceName: "Contact Mobile Number"))
                            loyaltyAlertView.addAction(title: "Cancel".localized, style: .cancel , action: {
                                self?.mobileNumberTxt.text = (self?.countryObject.code == "+95") ? "09" : ""
                                self?.mobileNumberTxt.becomeFirstResponder()
                                self?.confirmMobileNumberTxt.text = ""
                                self?.confirmMobileNumberView.isHidden = true
                                self?.selectMerchantView.isHidden = true
                                self?.mobClearButton.isHidden = true
                                self?.confMobClearButton.isHidden = true
                            })
                            loyaltyAlertView.addAction(title: "OK".localized, style: .target , action: {
                            })
                            loyaltyAlertView.showAlert(controller: self)
                        }
                        
                    } else {
                        
                        completion(false)
                    }
                }
            }
            progressViewObj.removeProgressView()
        }
    }
    
    func confirmMobileNumberHandler(textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let textCount  = text.count
        self.pointsText.text = ""
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            if countryObject.code == "+95" {
                self.pointsText.text = ""
                self.confirmMobileNumberTxt.isHidden = false
            }
            if countryObject.code == "+95", text == "09" {
                self.confMobClearButton.isHidden = true
            } else {
                if text.count == 0 {
                    self.confMobClearButton.isHidden = true
                }
            }
            if countryObject.code == "+95", (textField.text == "09" || text == "09") {
                self.confirmMobileNumberTxt.text = "09"
                self.mobClearButton.isHidden = true
                return false
            }
            
            updateMobileUI(count: textCount, textField: textField)
        }
        if self.mobileNumberTxt.text?.count == textCount, String(describing: self.mobileNumberTxt!.text!.last!) == string {
            self.confirmMobileNumberTxt.resignFirstResponder()
            _verifyInBackEnd(number: text, completion: { (isFound) in
                    self.selectMerchantView.isHidden = false
                    self.view.layoutIfNeeded()
            })
        }
        if validObj.checkMatchingNumber(string: text, withString: self.mobileNumberTxt.text!) {
            self.confirmMobileNumberTxt.text = text
            if textCount > 0 {
                confMobClearButton.isHidden = false
            } else {
                confMobClearButton.isHidden = true
            }
            return false
        } else {  return false  }
    }
    
    private func updateMobileUI(count: Int, textField: UITextField) {
        if textField == self.mobileNumberTxt {
            confirmMobileNumberTxt.text = (countryObject.code == "+95") ? "09" : ""
            confirmMobileNumberView.isHidden = true
            confMobClearButton.isHidden = true
            selectMerchantView.isHidden = true
            pointsView.isHidden = true
            pointsText.text = ""
            pointsClearButton.isHidden = true
            merchaneNameLbl.text = "Select Merchant".localized
        } else if textField == self.confirmMobileNumberTxt {
            selectMerchantView.isHidden = true
            pointsView.isHidden = true
            pointsText.text = ""
            pointsClearButton.isHidden = true
            merchaneNameLbl.text = "Select Merchant".localized
        }
        transferButton.isHidden = true
    }
    
    func MobileNumberHandler(textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let textCount  = text.count
        self.pointsText.text = ""
        let validations = PayToValidations().getNumberRangeValidation(text)
        if validations.isRejected {
            loyaltyAlertView.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: UIImage(named : "AppIcon"))
            loyaltyAlertView.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                self.mobileNumberTxt.text = (self.countryObject.code == "+95") ? "09" : ""
            })
            loyaltyAlertView.showAlert(controller: self)
            return false
        }
        
        if textCount > 0 {
            mobClearButton.isHidden = false
        } else {
            mobClearButton.isHidden = true
        }
        
        let contactList = self.contactSuggesstion(text)
        if contactList.count > 0 {
            // here show the contact suggession screen
            let rect = self.getSuggessionviewFrame(arr: contactList.count)
            self.showContactSuggessionView(withFrame: rect, andContacts: contactList)
            
        } else {
            // here hide the contact suggession screen
            self.hideContactSuggesionView()
        }
        
        
        let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: mobileNumberAcceptableCharacters).inverted
        let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
        
        if string != filteredSet {
            return false
        }
        
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            self.confirmMobileNumberTxt.isUserInteractionEnabled = true
            self.confirmMobileNumberTxt.placeholder = "Confirm Mobile Number".localized
            self.confirmMobileNumberTxt.isEnabled = true
            let countryCode = String.init(format: "(%@)", countryObject.code)
            leftViewCountryForConfirmation.wrapCountryViewData(img: countryObject.flag , str: countryCode)
            self.confirmMobileNumberTxt.leftView = leftViewCountryForConfirmation
            self.confirmMobileNumberTxt.leftViewMode  = UITextField.ViewMode.always
            if countryObject.code == "+95", text == "09" {
                self.mobClearButton.isHidden = true
                self.hideContactSuggesionView()
            } else {
                if text.count == 0 {
                    self.mobClearButton.isHidden = true
                    self.hideContactSuggesionView()
                }
            }
            if countryObject.code == "+95", (textField.text == "09" || text == "09") {
                self.mobileNumberTxt.text = "09"
                self.mobClearButton.isHidden = true
                return false
            }
            self.updateMobileUI(count: textCount, textField: textField)
        }
        
        // Normal Validations
        
        if countryObject.code == "+95" {
            
            if text.count >= validations.min {
                self.hideContactSuggesionView()
                if validations.max == text.count {
                    self.transferButton.isHidden = true
                    self.heightConfMobNum.constant = 60.0
                    self.confirmMobileNumberTxt.text = commonPrefixMobileNumber
                    self.confirmMobileNumberView.isHidden = false
                    self.mobileNumberTxt.text = text
                    self.confirmMobileNumberTxt.becomeFirstResponder()
                    self.hideContactSuggesionView()
                    selectMerchantView.isHidden = true
                    merchaneNameLbl.text = "Select Merchant".localized
                    pointsView.isHidden = true
                    pointsText.text = ""
                    confMobClearButton.isHidden = true
                    pointsClearButton.isHidden = true
                    return false
                }
                if text.count > validations.max {
                    //self.cnfMobileNumber.becomeFirstResponder()
                    self.selectMerchantView.isHidden = true
                    self.pointsView.isHidden = true
                    self.heightConfMobNum.constant = 60.0
                    self.confirmMobileNumberView.isHidden = false
                    self.confirmMobileNumberTxt.text = commonPrefixMobileNumber
                    self.confirmMobileNumberTxt.becomeFirstResponder()
                    self.hideContactSuggesionView()
                    return false
                } else {
                    self.transferButton.isHidden = true
                    selectMerchantView.isHidden = true
                    merchaneNameLbl.text = "Select Merchant".localized
                    pointsView.isHidden = true
                    pointsText.text = ""
                    confMobClearButton.isHidden = true
                    pointsClearButton.isHidden = true
                    self.heightConfMobNum.constant = 60.0
                    self.confirmMobileNumberView.isHidden = false
                    self.confirmMobileNumberTxt.text = commonPrefixMobileNumber
                }
                return true
            } else {
                self.heightConfMobNum.constant = 60.0
                self.confirmMobileNumberView.isHidden = true
            }           
        } else {
            if textCount > 4 {
                self.heightConfMobNum.constant = 60.0
                self.confirmMobileNumberView.isHidden = false
            } else {
                self.heightConfMobNum.constant = 0
                self.confirmMobileNumberView.isHidden = true
            }
            if textCount > 8 {
                self.hideContactSuggesionView()
            }
            if textCount == 13 {
                self.mobileNumberTxt.text = text
                self.confirmMobileNumberTxt.becomeFirstResponder()
                return false
            }
            if textCount > 13 {
                return false
            }
        }
        
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.mobileNumberTxt {
            if let text = textField.text, text.count > 2 {
                self.mobClearButton.isHidden = false
            } else {
                if let text = textField.text, text.count <= 2, countryObject.code == "+95" {
                    textField.text = "09"
                    self.mobClearButton.isHidden = true
                }
            }
        }
        return true
    }
    
    func shouldChangeCharacters(textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
            let endOfDocument = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
            return false
        }
        switch textField {
        case self.mobileNumberTxt:
            let mobileShouldReturn = self.MobileNumberHandler(textField: textField, shouldChangeCharactersIn: range, replacementString: string)
            return mobileShouldReturn
        case self.confirmMobileNumberTxt:
            let confirmShouldReturn = self.confirmMobileNumberHandler(textField: textField, shouldChangeCharactersIn: range, replacementString: string)
            return confirmShouldReturn
        case self.pointsText:
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string).replacingOccurrences(of: ",", with: "")
            let amount = Double((text as NSString).doubleValue)
            if range.location == 0, string == "0" {
                return false
            }
            if amount > self.selectedMerchantPoints {
                //self.showCustomErrorAlert(errMessage: "Enter below or equal to " + "\(String(self.selectedMerchantPoints))")
                self.pointsText.placeholder = "Enter Points".localized
                self.showCustomErrorAlert(errMessage: "Entered point can not be \n greater than available point(s).".localized)

                textField.text = ""
                self.showAccessoryView(status : false)
                self.transferButton.isHidden = true
                self.pointsClearButton.isHidden = true
                return false
            }
            if text.count > 0 && text != "0" {
                textField.placeholder = "Points".localized
                self.showAccessoryView(status : true)
                self.transferButton.isHidden = true
                self.pointsClearButton.isHidden = false
            } else {
                textField.placeholder = "Enter Points".localized
                self.showAccessoryView(status : false)
                self.transferButton.isHidden = true
                self.pointsClearButton.isHidden = true
            }
            self.pointsText.text = wrapAmountWithCommaDecimal(key: text)
            return false
        default:
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.pointsText {
            if let text = textField.text, text.count > 0, text != "0" {
                self.transferButton.isHidden = false
            } else {
                self.transferButton.isHidden = true
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let changingAccetable = self.shouldChangeCharacters(textField: textField, shouldChangeCharactersIn: range, replacementString: string)
        return changingAccetable
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.mobileNumberTxt {
            if textField.text == "" && countryObject.code == "+95" {
                textField.text = commonPrefixMobileNumber
            }
        } else if textField == self.pointsText {
            self.transferButton.isHidden = true
        } else if textField == self.pointsText {
            if let text = textField.text, text.count > 0 {
                self.showAccessoryView(status: true)
                self.transferButton.isHidden = false
            } else {
                self.showAccessoryView(status: false)
                self.transferButton.isHidden = true
            }
        }
    }
    
    
    private func rejectNumberAlgorithm() -> Bool {
        self.mobileNumberTxt.text = commonPrefixMobileNumber
        self.showCustomErrorAlert(errMessage: "Rejected Number".localized)
        return false
    }
    
    @IBAction func mobNumClearAction(_ sender : UIButton) {
        self.hideContactSuggesionView()
        self.transferButton.isHidden = true
        if countryObject.code == "+95" {
            self.mobileNumberTxt.text = commonPrefixMobileNumber
            confirmMobileNumberTxt.text = commonPrefixMobileNumber
        } else {
            self.mobileNumberTxt.text = ""
            confirmMobileNumberTxt.text = ""
        }
        confirmMobileNumberView.isHidden = true
        selectMerchantView.isHidden = true
        self.merchaneNameLbl.font = UIFont(name: appFont, size: appFontSize)
        merchaneNameLbl.text = "Select Merchant".localized
        pointsView.isHidden = true
        pointsText.text = ""
        mobClearButton.isHidden = true
        confMobClearButton.isHidden = true
        pointsClearButton.isHidden = true
        self.heightConfMobNum.constant = 60
        self.mobileNumberTxt.becomeFirstResponder()
    }
    
    @IBAction func confMobNumClearAction(_ sender : UIButton) {
        self.transferButton.isHidden = true
        if countryObject.code == "+95" {
            confirmMobileNumberTxt.text = commonPrefixMobileNumber
        } else {
            confirmMobileNumberTxt.text = ""
        }
        selectMerchantView.isHidden = true
        self.merchaneNameLbl.font = UIFont(name: appFont, size: appFontSize)
        merchaneNameLbl.text = "Select Merchant".localized
        pointsView.isHidden = true
        pointsText.text = ""
        confMobClearButton.isHidden = true
        pointsClearButton.isHidden = true
        self.confirmMobileNumberTxt.becomeFirstResponder()
    }
    @IBAction func pointsClearAction(_ sender : UIButton) {
        self.showAccessoryView(status : false)
        self.transferButton.isHidden = true
        self.pointsText.text = ""
        self.pointsText.placeholder = "Enter Points".localized
        pointsClearButton.isHidden = true
    }
    
    @IBAction func scanQRViewAction(_ sender: UIButton) {
        guard let second = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "secondVC") as? ScanToPayViewController else { return }
        second.delegate = self
        second.screenFromEarnPointTfr = true
        second.navigation = self.navigationController
        self.navigationController?.pushViewController(second, animated: true)
    }
    
}

extension EarnPointTransferViewController: ScrollChangeDelegate {
    func changeScrollPage(point: Int, withScanObject object: PTScanObject) {
        
    }
    
    func dismissScanOffer(object: PTScanObject, controller: ScanToPayViewController) {
        let mobileObject = PTManagerClass.decodeMobileNumber(phoneNumber: object.agentNo)
        let numberValid = PayToValidations().getNumberRangeValidation(mobileObject.number)
        self.confirmMobileNumberView.isHidden = true
        if mobileObject.country.dialCode == "+95" {
            if numberValid.isRejected {
                mobileNumberTxt.text = ""
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                return
            }
            
            if mobileObject.number.count < numberValid.min && mobileObject.number != "" {
                mobileNumberTxt.text = ""
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                return
            }
            
            if mobileObject.number.count > numberValid.max {
                mobileNumberTxt.text = ""
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                return
            }
        } else {
            if  mobileObject.number.count > 13 {
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                return
            }
        }
        
        let countryArray = revertCountryArray() as! Array<NSDictionary>
        let cDetails = self.identifyCountry(withPhoneNumber: object.agentNo, inCountryArray: countryArray) // 1 -> flag, 0 -> code
        var countryCode = String.init(format: "(%@)", "+95")
        if cDetails.0 == "" || cDetails.1 == "" {
            countryObject.flag = "myanmar"
            countryObject.code = "+95"
            leftViewCountry.wrapCountryViewData(img: "myanmar", str: countryCode)
            leftViewCountryForConfirmation.wrapCountryViewData(img: "myanmar", str: countryCode)
        } else {
            countryObject.flag = cDetails.1
            countryObject.code = cDetails.0
            countryCode = String.init(format: "(%@)", cDetails.0)
            leftViewCountry.wrapCountryViewData(img: cDetails.1, str: countryCode)
            leftViewCountryForConfirmation.wrapCountryViewData(img: cDetails.1, str: countryCode)
        }
        
        let number = self.getAgentCode(numberWithPrefix: mobileObject.number, havingCountryPrefix: mobileObject.country.dialCode)
        if number == UserModel.shared.mobileNo {
            self.mobileNumberTxt.text = ""
            PaytoConstants.alert.showToast(msg: PaytoConstants.messages.ownMobileNumber.localized)
            return
        }
        mobileNumberTxt.text = mobileObject.number
        heightConfMobNum.constant = 0
        self.mobClearButton.isHidden = false
        mobileNumberTxt.resignFirstResponder()
        _verifyInBackEnd(number: mobileObject.number, completion: { (isFound) in
            self.selectMerchantView.isHidden = false
            self.view.layoutIfNeeded()
        })
    }
    
    func scanningFailed() {
        
    }
}


extension EarnPointTransferViewController : ContactSuggestionDelegate{
    
    func didSelectFromContactSuggession(number: Dictionary<String, Any>) {
        self.hideContactSuggesionView()
        self.confirmMobileNumberView.isHidden = true
        let number = number[ContactSuggessionKeys.phonenumber_backend] as! String
        let validations = PayToValidations().getNumberRangeValidation(number)
        if validations.isRejected || number.count < validations.min {
            loyaltyAlertView.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: #imageLiteral(resourceName: "Contact Mobile Number"))
            loyaltyAlertView.addAction(title: "OK".localized, style: .target , action: {
                self.mobileNumberTxt.text = (self.countryObject.code == "+95") ? "09" : ""
                self.mobClearButton.isHidden = true
                self.updateMobileUI(count: 0, textField: self.mobileNumberTxt)
            })
            loyaltyAlertView.showAlert(controller: self)
            return
        }
        //println_debug("valid count ::: \(validCount)")
        
        if number.count >= validations.min {
            // allow
            mobileNumberTxt.text = number
            heightConfMobNum.constant = 0
            self.mobClearButton.isHidden = false
            mobileNumberTxt.resignFirstResponder()
            _verifyInBackEnd(number: number, completion: { (isFound) in
                self.selectMerchantView.isHidden = false
                self.view.layoutIfNeeded()
            })
        } else {
            // not allowed
            loyaltyAlertView.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: #imageLiteral(resourceName: "Contact Mobile Number"))
            loyaltyAlertView.addAction(title: "OK".localized, style: .target , action: {
            })
            loyaltyAlertView.showAlert(controller: self)
        }
    }
    
}

extension EarnPointTransferViewController : FavoriteSelectionDelegate{
    
    func selectedFavoriteObject(obj: FavModel) {
        self.confirmMobileNumberView.isHidden = true
        buttonView.isHidden = true
        //println_debug("selectedFavoriteObject")
        self.hideContactSuggesionView()
        let mobileNo = self.getActualMobileNum(obj.phone,withCountryCode: "+95")
        
        let validations = PayToValidations().getNumberRangeValidation(mobileNo)
        if validations.isRejected || mobileNo.count < validations.min {
            loyaltyAlertView.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: UIImage(named : "AppIcon"))
            loyaltyAlertView.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                self.mobileNumberTxt.text = (self.countryObject.code == "+95") ? "09" : ""
                self.mobClearButton.isHidden = true
                self.updateMobileUI(count: 0, textField: self.mobileNumberTxt)
            })
            loyaltyAlertView.showAlert(controller: self)
            return
        }
        //println_debug("valid count ::: \(validCount)")
        
        if mobileNo.count >= validations.min {
            // allow
            mobileNumberTxt.text = mobileNo
            self.mobClearButton.isHidden = false
            mobileNumberTxt.resignFirstResponder()
            heightConfMobNum.constant = 0
            selectMerchantView.isHidden = false
        } else {
            // not allowed
            loyaltyAlertView.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: #imageLiteral(resourceName: "Contact Mobile Number"))
            loyaltyAlertView.addAction(title: "OK".localized, style: .target , action: {
            })
            loyaltyAlertView.showAlert(controller: self)
        }
    }
    
}

//MARK: - Contact Picker Protocol
extension EarnPointTransferViewController : ContactPickerDelegate{
    
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]) {
        for contact in contacts {
            decodeContact(contact: contact, isMulti: true)
        }
    }
    
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        decodeContact(contact: contact, isMulti: false)
    }
    
    
    func wrapCountryViewData(img: String, str: String) {
        //        self.countryImage.image = UIImage.init(named: img)
        //        self.countryLabel.text  = str
        //
        //        let size   = self.countryLabel.text?.size(withAttributes: [.font: self.countryLabel.font]) ?? .zero
        //        let vWidth = 50 + size.width
        //        self.frame = .init(x: 0, y: 0, width: vWidth, height: 40.00)
        //        self.layoutIfNeeded()
    }
    
    
    func decodeContact(contact: ContactPicker) {
        
        self.confirmMobileNumberView.isHidden = true
        self.view.layoutIfNeeded()
        
        let phoneNumberCount = contact.phoneNumbers.count
        var phoneNumber = ""
        
        if phoneNumberCount == 1  {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        } else if phoneNumberCount > 1 {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        } else {
            
        }
        
        if phoneNumber != "" {
            phoneNumber = phoneNumber.replacingOccurrences(of: ")", with: "")
            phoneNumber = phoneNumber.replacingOccurrences(of: "(", with: "")
            phoneNumber = phoneNumber.trimmingCharacters(in: .whitespaces)
        }
        
        if phoneNumber.hasPrefix("00") {
            _ = phoneNumber.remove(at: String.Index.init(encodedOffset: 0))
            _ = phoneNumber.remove(at: String.Index.init(encodedOffset: 0))
            
            if phoneNumber.hasPrefix("+") {
            } else {
                phoneNumber = "+" + phoneNumber
            }
        }
        
        let countryArray = revertCountryArray() as! Array<NSDictionary>
        
        let cDetails = self.identifyCountry(withPhoneNumber: phoneNumber, inCountryArray: countryArray) // 1 -> flag, 0 -> code
        var countryCode = String.init(format: "(%@)", "+95")
        if cDetails.0 == "" || cDetails.1 == "" {
            countryObject.flag = "myanmar"
            countryObject.code = "+95"
            leftViewCountry.wrapCountryViewData(img: "myanmar", str: countryCode)
            leftViewCountryForConfirmation.wrapCountryViewData(img: "myanmar", str: countryCode)
        } else {
            countryObject.flag = cDetails.1
            countryObject.code = cDetails.0
            countryCode = String.init(format: "(%@)", cDetails.0)
            leftViewCountry.wrapCountryViewData(img: cDetails.1, str: countryCode)
            leftViewCountryForConfirmation.wrapCountryViewData(img: cDetails.1, str: countryCode)
        }
        
        
//        self.mobileNumberTxt.leftView     = nil
//        self.confirmMobileNumberTxt.leftView = nil
        
        self.mobileNumberTxt.leftView     = leftViewCountry
        self.mobileNumberTxt.leftViewMode = UITextField.ViewMode.always
        
        self.view.layoutIfNeeded()
        
        var phone = ""
        
        if phoneNumber.hasPrefix("+95") || phoneNumber.hasPrefix("(+95)") {
            phone = phoneNumber.replacingOccurrences(of: cDetails.0, with: "0")
            phone = phone.replacingOccurrences(of: " ", with: "")
            
            if phone.hasPrefix("0") {
                self.mobileNumberTxt.text = phone
            } else {
                self.mobileNumberTxt.text = "0" + phone
            }
            
        }else {
            phone = phoneNumber.replacingOccurrences(of: cDetails.0, with: "")
            phone = phone.replacingOccurrences(of: " ", with: "")
            self.mobileNumberTxt.text = phone
        }
        
        let validations = PayToValidations().getNumberRangeValidation(self.mobileNumberTxt.text ?? "")
        if validations.isRejected || phoneNumber.count < validations.min {
            loyaltyAlertView.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: UIImage(named : "AppIcon"))
            loyaltyAlertView.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                self.mobileNumberTxt.text = (self.countryObject.code == "+95") ? "09" : ""
                self.mobClearButton.isHidden = true
                self.updateMobileUI(count: 0, textField: self.mobileNumberTxt)
            })
            loyaltyAlertView.showAlert(controller: self)
            return
        }
        
        self.nameORnumber(selectiontype: .name, codeDetails: cDetails)
        if contact.displayName().count > 0 {
            self.confirmMobileNumberTxt.text = contact.displayName()
        } else {
            self.confirmMobileNumberTxt.text = "+95"
        }
        self.selectMerchantView.isHidden = false
        merchaneNameLbl.text = "Select Merchant".localized
        self.mobClearButton.isHidden = false
        mobileNumberTxt.resignFirstResponder()
        self.heightConfMobNum.constant = 0
        _verifyInBackEnd(number: phone, completion: { (isFound) in
            self.view.layoutIfNeeded()
        })
    }
    
    func decodeContact(contact: ContactPicker, isMulti: Bool){
        buttonView.isHidden = true
        decodeContact(contact: contact)        
    }
    
}

// MARK: - Table View
extension EarnPointTransferViewController : UITableViewDataSource,UITableViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: .default, reuseIdentifier: "")
        return cell
    }
    
    
}

extension EarnPointTransferViewController : SelectedMerchantDetail {
    func selectedMerchant(dictionary: Dictionary<String, Any>) {
        self.merchaneNameLbl.text = String.init(format: "%@ / %@", dictionary["name"] as? String ?? "", dictionary["points"] as? String ?? "0.0")
        self.selectedMerchantPoints = Double((dictionary["points"] as! NSString).doubleValue)
        self.selectedShopName = dictionary["name"] as? String ?? ""
        self.selectedMerchantId = dictionary["id"] as? String ?? ""
        self.pointsView.isHidden = false
        self.transferButton.isHidden = true
        self.pointsText.text = ""
        self.showAccessoryView(status: false)
        self.pointsText.becomeFirstResponder()
    }
}

extension EarnPointTransferViewController: WebServiceResponseDelegate {
    
//    func webResponse(withJson json: AnyObject, screen: String) {
//        progressViewObj.removeProgressView()
//        do {
//            if let data = json as? Data {
//                if let responseDict = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Dictionary<String , Any> {
//                    if let statusCode = responseDict.safeValueForKey("StatusCode") as? Int, statusCode == 404 {
//                        DispatchQueue.main.async {
//                            let alertMessage = responseDict.safeValueForKey("Message") as? String ?? ""
//                            loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: #imageLiteral(resourceName: "dashboard_loyalty"))
//                            loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
//
//                            })
//                            loyaltyAlertView.showAlert(controller: self)
//                        }
//                    } else {
//
//                    }
////                    println_debug(responseDict)
//                }
//            }
//        } catch {
//
//        }
//    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        progressViewObj.removeProgressView()
        do {
            if let data = json as? Data {
                if let responseDict = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Dictionary<String , Any> {
                    if let statusCode = responseDict.safeValueForKey("StatusCode") as? Int, statusCode == 404 {
                        DispatchQueue.main.async {
                            let alertMessage = responseDict.safeValueForKey("Message") as? String ?? ""
                            loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: #imageLiteral(resourceName: "dashboard_loyalty"))
                            loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                                
                            })
                            loyaltyAlertView.showAlert(controller: self)
                        }
                    } else {
                        if let statusCode = responseDict.safeValueForKey("StatusCode") as? Int, statusCode == 302 {
                            if let dictionaryArray = responseDict.safeValueForKey("Results") as? [Dictionary<String, Any>] {
                                var numberStr = 0
                                merchantDetailsModelArray.removeAll()
                                for dictionary in dictionaryArray {
                                    let model = merchantDetailsModel.init(dictionary: dictionary)
                                    self.merchantDetailsModelArray.append(model)
                                    if (model.status ?? "" == "Available") {
                                        numberStr = numberStr + (model.pointBalance ?? 0)
                                    }
                                }
                                DispatchQueue.main.async {
                                    //self.pointsLabelValue.text = "\(numberStr)"
                                    //UserDefaults.standard.set(self.pointsLabelValue.text!, forKey: "MyEarnPoint")
                                }
                                self.getAllMerchantListEstel()
                            }
                        }
                    }
                }
            }
        } catch {
            
        }
    }
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        PTLoader.shared.hide()
    }
}

extension EarnPointTransferViewController: LOReceiptPopVC {
    func morePaymentAction(phoneNumber: String, repeatPay: Bool) {
        self.initialSetUp()
        self.setUpRepeat(phoneNumber: phoneNumber, repeatPay: repeatPay)
    }
}
