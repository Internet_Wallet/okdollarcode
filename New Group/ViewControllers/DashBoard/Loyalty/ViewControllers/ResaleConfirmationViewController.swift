//
//  ResaleConfirmationViewController.swift
//  OK
//
//  Created by Kethan on 7/21/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit


class ResaleConfirmationViewController: LoyaltyBaseViewController, UIPopoverPresentationControllerDelegate {
    
    var modelData : merchantDetailsModel?
    
    @IBOutlet weak var mobileNumberLbl : UILabel! {
        didSet {
            self.mobileNumberLbl.font = UIFont(name: appFont, size: appFontSize)
            self.mobileNumberLbl.text = self.mobileNumberLbl.text?.localized
        }
    }
    
    @IBOutlet weak var btnConfirm : UIButton! {
        didSet {
            self.btnConfirm.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.btnConfirm .setTitle("Confirm".localized, for: .normal)
        }
    }
    
    @IBOutlet weak var shopNameLbl : UILabel! {
        didSet {
            self.shopNameLbl.font = UIFont(name: appFont, size: appFontSize)
            self.shopNameLbl.text = self.shopNameLbl.text?.localized
        }
    }
    
    @IBOutlet weak var sellingPntsLbl : UILabel! {
        didSet {
            self.sellingPntsLbl.font = UIFont(name: appFont, size: appFontSize)
            self.sellingPntsLbl.text = self.sellingPntsLbl.text?.localized
        }
    }
    
    @IBOutlet weak var sellingAmtLbl : UILabel! {
        didSet {
            self.sellingAmtLbl.font = UIFont(name: appFont, size: appFontSize)
            self.sellingAmtLbl.text = self.sellingAmtLbl.text?.localized
        }
    }
    
    @IBOutlet weak var mobileNumberTxt : UILabel!{
        didSet{
            self.mobileNumberTxt.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var shopNameTxt : UILabel!{
        didSet{
            self.shopNameTxt.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var sellingPntsTxt : UILabel!{
        didSet{
            self.sellingPntsTxt.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var sellingAmtTxt : UILabel!{
        didSet{
            self.sellingAmtTxt.font = UIFont(name: appFont, size: appFontSize)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        addCustomBackButton(navTitle: "Bonus Point Resale")
        addCustomMenuButton()
        updateUI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func addCustomMenuButton() {
        let menuButton = UIButton(type: .custom)
        menuButton.frame = CGRect(x: 0, y: 0, width: 30, height: 40)
        menuButton.setImage(UIImage.init(named: "menu_white_payto.png"), for: .normal)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: menuButton)
        menuButton.addTarget(self, action: #selector(menuAction(_ : )), for: .touchUpInside)
    }
    
    @objc func menuAction(_ sender : UIBarButtonItem) {
        let popoverContent = self.storyboard?.instantiateViewController(withIdentifier: "ViewAllStatusPopover_ID") as? ViewAllStatusPopover
        popoverContent?.delegate = self
        popoverContent?.modalPresentationStyle = .popover
        let popover = popoverContent?.popoverPresentationController
        popoverContent?.preferredContentSize = CGSize(width: 200.0, height: 45.0)
        popover?.delegate = self
        popover?.sourceView = self.view
        popover?.permittedArrowDirections = .init(rawValue: 0)
        popover?.sourceRect = CGRect(x: self.view.frame.size.width, y: 0, width: 0, height: 0)
        self.present(popoverContent!, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    private func updateUI() {
        let dict = modelData?.merchant
        let mobileNum = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: dict?.safeValueForKey("merchantAccountNo") as? String ?? "")
        self.mobileNumberTxt.text = "(\(mobileNum.counrtycode))\(mobileNum.mobileNo)"
        self.shopNameTxt.text = dict?.safeValueForKey("shopName") as? String ?? ""
        if let sellingPoints = modelData?.pointBalance {
            var pointTxt = " Points"
            if sellingPoints == 1 {
                pointTxt = " Point"
            }
            self.sellingPntsTxt.text = wrapAmountWithCommaDecimal(key: "\(sellingPoints)").replacingOccurrences(of: ".0", with: "") + pointTxt
        }
        if let sellingAmt = modelData?.sellingPrice {
            self.sellingAmtTxt.text = wrapAmountWithCommaDecimal(key: "\(sellingAmt)").replacingOccurrences(of: ".0", with: "") + " MMK"
        }
       
    }
    
    @IBAction func submitButtonAction(_ sender : UIButton) {
        self.getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
            guard isSuccess, let tokenAuthStr = tokenString as? String else {
                self.showCustomErrorAlert(errMessage: "try again")
                return
            }
            DispatchQueue.main.async(execute: {
                self.submitResalePointApi(authToken: tokenAuthStr)
            })
        })
    }
    
    private func submitResalePointApi(authToken: String) {
    
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
           
            let urlStr   = String.init(format: Url.resalePointApiUrl)
            let url = getUrl(urlStr: urlStr, serverType: .bonusPointApi)
            
            var balStr = "0.0"
            if let pointBal = modelData?.sellingPrice {
                balStr = String(pointBal)
            }
            
            //Generate pipepline string
            let strPipeLine = "BonusPointId=\(modelData?.bonusPointId ?? "")|Amount=\(balStr)|CustomerId=\(modelData?.customerId ?? "")"
            let hashValue = strPipeLine.hmac_SHA1(key: Url.sLoyalty_airtime)
            let keys : [String] = ["BonusPointId","Amount","CustomerId","HashValue"]
            let values : [String] = [modelData?.bonusPointId ?? "", balStr , modelData?.customerId ?? "" , hashValue]
            let jsonObj = JSONStringWriter()
            let jsonStr = jsonObj.writeJsonIntoString(keys, values)
            
            let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url, methodType: kMethod_Post, contentType: kContentType_Json, inputVal: jsonStr, authStr: authToken)
            
            JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
                
                guard isSuccess else
                {
                    //println_debug(" API Fail")
                    self.removeProgressView()
                    return
                }
                
                if let dictResponse = response as? Dictionary<String, Any> {
                    if let statusCode = dictResponse.safeValueForKey("StatusCode") as? Int, statusCode == 201 {
                        DispatchQueue.main.async {
                             self.removeProgressView()
                            let alertMessage = "Successfully Created".localized
                            loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: #imageLiteral(resourceName: "discount_money"))
                            loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                                self.dismiss(animated: true, completion: nil)
                            })
                            loyaltyAlertView.showAlert(controller: self)
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.removeProgressView()
                            let alertMessage = dictResponse.safeValueForKey("Message") as? String ?? ""
                            loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: #imageLiteral(resourceName: "dashboard_loyalty"))
                            loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                                
                            })
                            loyaltyAlertView.showAlert(controller: self)
                        }
                    }
                }
            })
            
        } else {
            
        }
    }

}



extension ResaleConfirmationViewController : ViewAllStatusProtocol {
    func showViewAllStatusVC() {
        let viewAllStatusVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: "BonusPointResaleStatusViewController_ID")
        self.navigationController?.pushViewController(viewAllStatusVC, animated: true)
    }
}
