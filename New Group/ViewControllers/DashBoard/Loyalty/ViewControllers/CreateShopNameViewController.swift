//
//  CreateShopNameViewController.swift
//  OK
//
//  Created by Kethan on 3/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Rabbit_Swift

class CreateShopNameViewController: LoyaltyBaseViewController {
    var activeButtonTap : activeButtonStatus = .active
    
    var shopNameListArray = [Dictionary<String,Any>]()
    var shopNameDict = Dictionary<String,Any>()

    @IBOutlet weak var shopNameTxt : UITextField! {
        didSet {
            self.shopNameTxt.font = UIFont(name: appFont, size: appFontSize)
            self.shopNameTxt.placeholder = self.shopNameTxt.placeholder?.localized
            self.shopNameTxt.isUserInteractionEnabled = false
        }
    }
    @IBOutlet weak var activeBtn : UIButton!{
        didSet{
            activeBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var inActiveBtn : UIButton!{
        didSet{
            inActiveBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    @IBOutlet weak var activeLabel : UILabel! {
        didSet {
            activeLabel.font = UIFont(name: appFont, size: appButtonSize)
            activeLabel.text = activeLabel.text?.localized
        }
    }
    @IBOutlet weak var activeImageView : UIImageView! {
        didSet {
            activeImageView.image = UIImage(named: "active.png")
        }
    }
    @IBOutlet weak var inActiveLabel : UILabel! {
        didSet {
            inActiveLabel.font = UIFont(name: appFont, size: appFontSize)
            inActiveLabel.text = inActiveLabel.text?.localized
        }
    }
    @IBOutlet weak var inActiveImageView : UIImageView! {
        didSet {
            inActiveImageView.image = UIImage(named: "in_active.png")
        }
    }
    
    @IBOutlet weak var createBtn : UIButton! {
        didSet {
            createBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            createBtn.setTitle("Update".localized, for: .normal)
        }
    }
    var isEdit = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        geoLocManager.startUpdateLocation()
        addCustomBackButton(navTitle: "Create Shop Name")
        // Do any additional setup after loading the view.
        let shopName = userDef.string(forKey: "shop_name")
        if shopName == "" {
            if UserModel.shared.businessName != "" {
            shopNameTxt.text = (ok_default_language == "my") ? Rabbit.uni2zg(UserModel.shared.businessName) : UserModel.shared.businessName
            }
        }else {
            if  shopName != nil {
                shopNameTxt.text = (ok_default_language == "my") ? Rabbit.uni2zg(shopName ?? "") : shopName
            }else {
                shopNameTxt.text = (ok_default_language == "my") ? Rabbit.uni2zg(UserModel.shared.businessName) : UserModel.shared.businessName
            }
        
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func activeButtonAction(_ sender : UIButton) {
        activeButtonTap = .active
        activeImageView.image = UIImage(named : "active.png")
        inActiveImageView.image = UIImage(named : "in_active.png")
    }
    
    @IBAction func inActiveButtonAction(_ sender : UIButton) {
        activeButtonTap = .inActive
        activeImageView.image = UIImage(named : "in_active.png")
        inActiveImageView.image = UIImage(named : "active.png")
    }
    
    @IBAction func createButtonAction(_ sender : UIButton) {
        if shopNameTxt.text != "" {
            if appDel.checkNetworkAvail() {
                let web      = PayToWebApi()
                web.delegate = self
                var ip = ""
                if let ipAdd = OKBaseController.getIPAddress() {
                    ip = ipAdd
                }
                var statusString = "PENDING"
                if activeButtonTap == .inActive {
                    statusString = "PENDING"
                }
                var urlStr  = ""
                if isEdit {
                    urlStr   = String.init(format: Url.editSopNameApiUrl, UserModel.shared.mobileNo, statusString, shopNameTxt.text ?? "", ok_password ?? "", ip , "ios", UserLogin.shared.token)
                }else {
                    urlStr = String.init(format: Url.createSopNameApiUrl, UserModel.shared.mobileNo, statusString, shopNameTxt.text ?? "", ok_password ?? "", ip , "ios", UserLogin.shared.token)
                }
                
                guard let escapedString = urlStr.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed) else { return }
                let url = getUrl(urlStr: escapedString, serverType: .serverEstel)
                println_debug(url)
                let pRam = Dictionary<String,String>()
                web.genericClassXML(url: url, param: pRam as AnyObject, httpMethod: "POST", mScreen: "mCreateShopName")
            } else {
                self.noInternetAlert()
            }
        } else {
            let message = "Enter shop name"
            alertViewObj.wrapAlert(title: nil, body: message.localized, img: #imageLiteral(resourceName: "merchant_confi"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func updateShopNameInAppServer() {
       
        if appDelegate.checkNetworkAvail() {
            
            let urlStr   = String.init(format: Url.updateShopInAppServerUrl, UserModel.shared.mobileNo, geoLocManager.currentLatitude, UserModel.shared.state, UserModel.shared.township, geoLocManager.currentLongitude, "1", UserModel.shared.country, UserModel.shared.street, getMsid(), "", self.shopNameTxt.text ?? "", simid)
                        println_debug(urlStr)
            let url = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug(url)
            let params = Dictionary<String,String>()
            DispatchQueue.main.async {
                TopupWeb.genericClass(url: url, param: params as AnyObject, httpMethod: "GET") { [weak self](response, success) in
                    if success {
                        if let json = response as? Dictionary<String,Any> {
                            if let code = json["Code"] as? NSNumber, let message = json["Msg"] as? String, code == 200, message == "success" {
                                println_debug(json)
                                let message = "Success".localized
                                DispatchQueue.main.async {
                                    alertViewObj.wrapAlert(title: nil, body: message.localized, img: #imageLiteral(resourceName: "info_success"))
                                      alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                      //  UserModel.shared.businessName = self?.shopNameTxt.text ?? ""
                                        userDef.set(self?.shopNameTxt.text ?? "", forKey: "shop_name")
                                        self?.navigationController?.popViewController(animated: true)
                                      })
                                      alertViewObj.showAlert(controller: self)
                                }
                            } else {
                                if let msg = json["Msg"] as? String {
                                    self?.showErrorAlert(errMessage: msg)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}


//PTWebresponseDelegate
extension CreateShopNameViewController : PTWebResponseDelegate {
    func webSuccessResult(data: Any, screen: String) {
        
        if screen == "mCreateShopName" {
            if let xmlString = data as? String {
                let xml = SWXMLHash.parse(xmlString)
                println_debug(xmlString)
                shopNameDict.removeAll()
                self.enumerate(indexer: xml)
                self.shopNameListArray.removeAll()
                self.shopNameListArray.append(shopNameDict)
                if shopNameDict["resultdescription"] as? String == "Transaction Successful" {
                    DispatchQueue.main.async {
                        self.shopNameTxt.isUserInteractionEnabled = false
                        self.removeProgressView()
                        self.isEdit = false
                        self.updateShopNameInAppServer()
                    }
                } else if shopNameDict["resultdescription"] as? String == "Duplicate Shopname Not Allowed" || shopNameDict["resultdescription"] as? String == "Shopname Already Exists" {
                    DispatchQueue.main.async {
                        self.removeProgressView()
                        let message = self.shopNameDict["resultdescription"] as? String ?? ""
                        self.isEdit = true
                        alertViewObj.wrapAlert(title: nil, body: message.localized, img: #imageLiteral(resourceName: "error"))
                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                            self.shopNameTxt.isUserInteractionEnabled = true
                            self.shopNameTxt.becomeFirstResponder()
                        })
                        alertViewObj.showAlert(controller: self)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.removeProgressView()
                        self.shopNameTxt.isUserInteractionEnabled = false
                        let message = self.shopNameDict["resultdescription"] as? String ?? ""
                        loyaltyAlertView.wrapAlert(title: nil, body: message.localized, img: #imageLiteral(resourceName: "error"))
                        loyaltyAlertView.addAction(title: "OK".localized, style: .target , action: {
                            
                        })
                        loyaltyAlertView.showAlert(controller: self)
                    }
                }
            }
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            shopNameDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func webFailureResult(screen: String) {
        println_debug("Failure")
    }
}

