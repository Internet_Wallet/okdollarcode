//
//  ViewAllStatusPopover.swift
//  OK
//
//  Created by Kethan on 3/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol ViewAllStatusProtocol : class {
    func showViewAllStatusVC()
}

class ViewAllStatusPopover: UIViewController {
    
    @IBOutlet weak var viewAllStatusLbl: UILabel! {
        didSet {
            self.viewAllStatusLbl.font = UIFont(name: appFont, size: appFontSize)
            self.viewAllStatusLbl.text = self.viewAllStatusLbl.text?.localized
        }
    }
    

    weak var delegate : ViewAllStatusProtocol?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showStatusViewController(_ sender : UIButton) {
        self.delegate?.showViewAllStatusVC()
        self.dismiss(animated: true, completion: nil)
    }
}
