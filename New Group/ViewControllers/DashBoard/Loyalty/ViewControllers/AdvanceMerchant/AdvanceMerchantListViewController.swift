//
//  AdvanceMerchantListViewController.swift
//  OK
//
//  Created by Kethan on 7/31/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol SelectedAdvMerchantDetail : class {
    func selectedAdvMerchant(dictionary : Dictionary<String, Any>)
}

class AdvanceMerchantListViewController: LoyaltyBaseViewController {
    var merchantDetailsModelArray = [Dictionary<String, Any>]()
    weak var delegate : SelectedAdvMerchantDetail?
    
    @IBOutlet weak var listTableView : UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        addCustomBackButton(navTitle: "Select Merchant")
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func dismissMerchantViewController() {
        backAction()
    }
    
    
}


extension AdvanceMerchantListViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return merchantDetailsModelArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell : AdvanceMerchantListTableCell = tableView.dequeueReusableCell(withIdentifier: "advanceMerchantCell", for: indexPath) as! AdvanceMerchantListTableCell
            cell.wrapData(dictionary: merchantDetailsModelArray[indexPath.row])
            return cell
    }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            self.delegate?.selectedAdvMerchant(dictionary: merchantDetailsModelArray[indexPath.row])
            dismissMerchantViewController()
        }
}


class AdvanceMerchantListTableCell : UITableViewCell {
    @IBOutlet weak var numberLabel : UILabel!{
        didSet {
            self.numberLabel.font = UIFont(name: appFont, size: appFontSize)
            
        }
    }
    
    override func awakeFromNib() {
        
    }
    
    func wrapData(dictionary : Dictionary<String, Any>) {
        self.numberLabel.text = dictionary.safeValueForKey("PhoneNumber") as? String ?? ""
    }
}
