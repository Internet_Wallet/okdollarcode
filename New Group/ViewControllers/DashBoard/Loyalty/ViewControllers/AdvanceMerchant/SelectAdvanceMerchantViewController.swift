//
//  SelectAdvanceMerchantViewController.swift
//  OK
//
//  Created by Kethan on 7/31/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit


class SelectAdvanceMerchantViewController: LoyaltyBaseViewController {
    var headerTitle : String = "Select Merchant"
    //
    var bonusPoints = "0.0"
    var activeButtonTap : activeButtonStatus = .active
    
    var pointListArray = [Dictionary<String,Any>]()
    var pointDict = Dictionary<String,Any>()
    
    var merchantListArray = [Dictionary<String,Any>]()
    //outlets
    
    @IBOutlet weak var availablePointsView : UIView! {
        didSet {
            self.availablePointsView.isHidden = true
        }
    }
    
    @IBOutlet weak var availablePointsViewHeight : NSLayoutConstraint!
    
    @IBOutlet weak var availablePointsLabel : UILabel! {
        didSet {
            availablePointsLabel.font = UIFont(name: appFont, size: appFontSize)
            availablePointsLabel.text = availablePointsLabel.text?.localized
        }
    }
    @IBOutlet weak var availablePointsValueLabel : UILabel!{
        didSet {
            availablePointsValueLabel.font = UIFont(name: appFont, size: appFontSize)
           
        }
    }
    
    @IBOutlet weak var pointsView : UIView! {
        didSet {
            self.pointsView.isHidden = true
        }
    }
    @IBOutlet weak var pointsTxt : UITextField! {
        didSet {
            self.pointsTxt.font = UIFont(name: appFont, size: appFontSize)
            self.pointsTxt.placeholder = pointsTxt.placeholder?.localized
        }
    }
    @IBOutlet weak var clearButton : UIButton! {
        didSet {
            self.clearButton.isHidden = true
        }
    }
    @IBOutlet weak var parentTypeView : UIView! {
        didSet {
            self.parentTypeView.isHidden = true
        }
    }
    
    @IBOutlet weak var parentTypeLabel : UILabel! {
        didSet {
            parentTypeLabel.font = UIFont(name: appFont, size: appFontSize)
            parentTypeLabel.text = parentTypeLabel.text?.localized
        }
    }
    
    @IBOutlet weak var merchaneNameLbl: UILabel! {
        didSet {
            self.merchaneNameLbl.font = UIFont(name: appFont, size: appFontSize)
            self.merchaneNameLbl.text = self.merchaneNameLbl.text?.localized
        }
    }
    
    
    @IBOutlet weak var parentBtn : UIButton!{
        didSet{
            parentBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var selfBtn : UIButton!{
        didSet{
            selfBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var parentLabel : UILabel! {
        didSet {
            parentLabel.font = UIFont(name: appFont, size: appFontSize)
            parentLabel.text = parentLabel.text?.localized
        }
    }
    @IBOutlet weak var parentImageView : UIImageView! {
        didSet {
            parentImageView.image = UIImage(named: "active.png")
        }
    }
    @IBOutlet weak var selfLabel : UILabel! {
        didSet {
            selfLabel.font = UIFont(name: appFont, size: appFontSize)
            selfLabel.text = selfLabel.text?.localized
        }
    }
    @IBOutlet weak var selfImageView : UIImageView! {
        didSet {
            selfImageView.image = UIImage(named: "in_active.png")
        }
    }
    
    
    @IBOutlet weak var createBtn : UIButton! {
        didSet {
            createBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            createBtn.setTitle(self.createBtn.titleLabel?.text?.localized, for: .normal)
            self.createBtn.isHidden = true
        }
    }
    
    weak var delegate : pointDistributionProtocol?
    
    @IBOutlet weak var pointsViewHeight : NSLayoutConstraint!
    @IBOutlet weak var parentViewHeight : NSLayoutConstraint!
    
    //AccessoryView
    var submitView: UIView {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 50))
        let btn = UIButton.init(frame: view.frame)
        btn.backgroundColor = UIColor.red
        if headerTitle == "Dummy Wallet Configuration" {
            btn.setTitle("Confirm".localized, for: .normal)
        }
        else if headerTitle == "Loyalty Wallet Transfer" {
            btn.setTitle("Transfer".localized, for: .normal)
        } else {
           btn.setTitle("Pullout".localized, for: .normal)
        }
        
        if let myFont = UIFont(name: appFont, size: appButtonSize) {
            btn.titleLabel?.font =  myFont
        }
        btn.addTarget(self, action: #selector(sendButtonAction(_:)), for: UIControl.Event.touchUpInside)
        view.addSubview(btn)
        return view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addCustomBackButton(navTitle: headerTitle)
        getAllMerchantList()
        if headerTitle == "Dummy Wallet Configuration" {
            availablePointsViewHeight.constant = 0
            pointsViewHeight.constant = 0
            self.createBtn.setTitle("Confirm".localized, for: .normal)
        } else {
            if headerTitle == "Loyalty Wallet Transfer" {
                self.createBtn.setTitle("Transfer".localized, for: .normal)
            } else {
                self.createBtn.setTitle("Pullout".localized, for: .normal)
            }
            availablePointsView.isHidden = false
            parentViewHeight.constant = 0
        }
        
        self.availablePointsValueLabel.text = bonusPoints
        
        pointsTxt.inputAccessoryView = self.submitView
        pointsTxt.inputAccessoryView?.isHidden = true
        pointsTxt.addTarget(self, action: #selector(ResaleMyNumViewController.textFieldDidChange(_:)),
                           for: UIControl.Event.editingChanged)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func parentButtonAction(_ sender : UIButton) {
        activeButtonTap = .active
        selfImageView.image = UIImage(named : "in_active.png")
        parentImageView.image = UIImage(named : "active.png")
    }
    
    @IBAction func selfButtonAction(_ sender : UIButton) {
        activeButtonTap = .inActive
        selfImageView.image = UIImage(named : "active.png")
        parentImageView.image = UIImage(named : "in_active.png")
    }
    
    func getAllMerchantList() {
        if appDelegate.checkNetworkAvail() {
            let web      = PayToWebApi()
            web.delegate = self
            
            let urlStr   = String.init(format: Url.getAdvanceMerchantListUrl, msid, UserModel.shared.simID, UserModel.shared.mobileNo)
            let url = getUrl(urlStr: urlStr, serverType: .serverApp)
            let pRam = Dictionary<String,String>()
            web.genericClass(url: url, param: pRam as AnyObject, httpMethod: "GET", mScreen: "mAdvMerchantList")
        }
    }
    
    
    @IBAction func selectMerchantListAction(_ sender : UIButton) {
        if merchantListArray.count > 0 {
            if let merchantListVC = UIStoryboard(name: loyaltyMainViews.loyaltyAdvModuleStory, bundle: nil).instantiateViewController(withIdentifier: loyaltyMainViews.viewControllers.selectAdvMerchantListVC) as? AdvanceMerchantListViewController {
                merchantListVC.delegate = self
                merchantListVC.merchantDetailsModelArray = self.merchantListArray
                self.navigationController?.pushViewController(merchantListVC, animated: true)
            }
        } else {
            DispatchQueue.main.async{
                let alertMessage = "No merchant found"
                loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: nil)
                loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                    
                })
                loyaltyAlertView.showAlert(controller: self)
            }
        }
    }
    
    
    fileprivate func showAccessoryView(status : Bool) {
        if status {
            self.pointsTxt.inputAccessoryView?.isHidden = false
        } else {
            self.pointsTxt.inputAccessoryView?.isHidden = true
        }
    }
    
    @IBAction func sendButtonAction(_ sender : UIButton) {
        if headerTitle == "Dummy Wallet Configuration" {
            self.dummayWalletConfirmAction()
        } else if headerTitle == "Loyalty Wallet Transfer" {
            self.transferPointButtonAction()
        } else {
            self.pulloutPointButtonAction()
        }
    }

    private func updateAvailablePoints(points : String) {
        DispatchQueue.main.async {
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            let text = (self.availablePointsValueLabel.text ?? "0").replacingOccurrences(of: ",", with: "")
            var pointsFloatVal  = Int((text as NSString).intValue)
            let pointsIntVal = Int((points as NSString).intValue)
            if self.headerTitle == "Loyalty Wallet Transfer" {
                pointsFloatVal = pointsFloatVal - pointsIntVal
            } else {
               pointsFloatVal = pointsFloatVal + pointsIntVal
            }
            
            if let numberStr = formatter.string(from: pointsFloatVal as NSNumber) {
                self.availablePointsValueLabel.text = numberStr
            }
            self.delegate?.currentPoint(pointText: self.availablePointsValueLabel.text!)
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        let dataWithoutComma = self.removeCommaFromDigit(textField.text!)
        pointsTxt.text = self.getDigitDisplay(dataWithoutComma)
    }
    
}

extension SelectAdvanceMerchantViewController : PTWebResponseDelegate, BioMetricLoginDelegate {
    
    func transferPointButtonAction() {
        if appDelegate.checkNetworkAvail() {
            let web      = PayToWebApi()
            web.delegate = self
            var ip = ""
            if let ipAdd = OKBaseController.getIPAddress() {
                ip = ipAdd
            }
            let mobileNo = merchaneNameLbl.text ?? ""
            let poinsNum = pointsTxt.text?.replacingOccurrences(of: ",", with: "") ?? "0"
            let urlStr   = String.init(format: Url.advancePointTrfApiUrl, UserModel.shared.mobileNo, mobileNo, poinsNum, ok_password ?? "", ip , "ios", UserLogin.shared.token)
            let url = getUrl(urlStr: urlStr, serverType: .serverEstel)
            //            println_debug(url)
            let pRam = Dictionary<String,String>()
            web.genericClassXML(url: url, param: pRam as AnyObject, httpMethod: "POST", mScreen: "mPointDistribution")
        }
    }
    
    func pulloutPointButtonAction() {
        if appDelegate.checkNetworkAvail() {
            let web      = PayToWebApi()
            web.delegate = self
            var ip = ""
            if let ipAdd = OKBaseController.getIPAddress() {
                ip = ipAdd
            }
            let mobileNo = merchaneNameLbl.text ?? ""
            let poinsNum = pointsTxt.text?.replacingOccurrences(of: ",", with: "") ?? "0"
            let urlStr   = String.init(format: Url.pulloutApiUrl, UserModel.shared.mobileNo, mobileNo, poinsNum, ok_password ?? "", ip , "ios", UserLogin.shared.token)
            let url = getUrl(urlStr: urlStr, serverType: .serverEstel)
            //            println_debug(url)
            let pRam = Dictionary<String,String>()
            web.genericClassXML(url: url, param: pRam as AnyObject, httpMethod: "POST", mScreen: "mPointDistribution")
        }
    }
    
    func dummayWalletConfirmAction() {
        if appDelegate.checkNetworkAvail() {
            let web      = PayToWebApi()
            web.delegate = self
            var ip = ""
            if let ipAdd = OKBaseController.getIPAddress() {
                ip = ipAdd
            }
            var statusString = "F"
            if activeButtonTap == .inActive {
                statusString = "T"
            }
            
            let mobileNo = merchaneNameLbl.text ?? ""
            let urlStr   = String.init(format: Url.dummyWalConfigApiUrl, UserModel.shared.mobileNo, mobileNo, statusString, ip , "ios", UserLogin.shared.token)
            let url = getUrl(urlStr: urlStr, serverType: .serverEstel)
            //            println_debug(url)
            let pRam = Dictionary<String,String>()
            web.genericClassXML(url: url, param: pRam as AnyObject, httpMethod: "POST", mScreen: "mDummyWalletConfig")
        }
    }
    
    
    //PTWebresponseDelegate
    
    func webSuccessResult(data: Any, screen: String) {
        if screen == "mAdvMerchantList" {
            if let dictData = data as? Dictionary<String, Any> {
                if let dict = dictData["Data"] as? String {
                    if let dictAll = OKBaseController.convertToDictionary(text: dict) {
                        if let arrayOfMerchant = dictAll["DummyProfile"] as? [Dictionary<String, Any>] {
                            self.merchantListArray = arrayOfMerchant
                        }
                    }
                }
                self.removeProgressView()
            }
        }
        else if screen == "mPointDistribution" {
            if let xmlString = data as? String {
                let xml = SWXMLHash.parse(xmlString)
                
                pointDict.removeAll()
                self.enumerate(indexer: xml)
                self.pointListArray.removeAll()
                self.pointListArray.append(pointDict)
                //                println_debug(pointListArray)
                self.removeProgressView()
                if pointDict["resultdescription"] as? String == "Transaction Successful" {
                    DispatchQueue.main.async {
                        self.updateAvailablePoints(points: self.pointDict["points"] as? String ?? "0")
                        let alertMessage = self.pointDict.safeValueForKey("resultdescription") as? String ?? ""
                        loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: #imageLiteral(resourceName: "discount_money"))
                        loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                            self.navigationController?.popViewController(animated: true)
                        })
                        loyaltyAlertView.showAlert(controller: self)
                    }
                } else if pointDict["resultdescription"] as? String == "Invalid Secure Token" {
                    DispatchQueue.main.async {
                        OKPayment.main.authenticate(screenName: "PointDistribution", delegate: self)
                    }
                } else {
                    DispatchQueue.main.async {
                        let alertMessage = self.pointDict.safeValueForKey("resultdescription") as? String ?? ""
                        loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: #imageLiteral(resourceName: "error"))
                        loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                            self.navigationController?.popViewController(animated: true)
                        })
                        loyaltyAlertView.showAlert(controller: self)
                    }
                }
            }
        } else if screen == "mDummyWalletConfig" {
           
            if let xmlString = data as? String {
                let xml = SWXMLHash.parse(xmlString)
                
                pointDict.removeAll()
                self.enumerate(indexer: xml)
                self.pointListArray.removeAll()
                self.pointListArray.append(pointDict)
                //                println_debug(pointListArray)
                self.removeProgressView()
                if pointDict["resultdescription"] as? String == "Transaction Successful" {
                    DispatchQueue.main.async {
                        //self.updateAvailablePoints(points: self.pointDict["walletbalance"] as? String ?? "0.0")
                        let alertMessage = self.pointDict.safeValueForKey("resultdescription") as? String ?? ""
                        loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: #imageLiteral(resourceName: "discount_money"))
                        loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                            self.navigationController?.popViewController(animated: true)
                        })
                        loyaltyAlertView.showAlert(controller: self)
                    }
                } else {
                    DispatchQueue.main.async {
                        let alertMessage = self.pointDict.safeValueForKey("resultdescription") as? String ?? ""
                        loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: #imageLiteral(resourceName: "error"))
                        loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                            self.navigationController?.popViewController(animated: true)
                        })
                        loyaltyAlertView.showAlert(controller: self)
                    }
                }
            }
        }
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful {
            if headerTitle == "Loyalty Wallet Transfer" {
                self.transferPointButtonAction()
            } else {
                self.pulloutPointButtonAction()
            }
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            pointDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func webFailureResult(screen: String) {
        println_debug("Failure")
    }
    
    @IBAction func clearButtonAction(_ sender : UIButton) {
        self.showAccessoryView(status : false)
        sender.isHidden = true
        createBtn.isHidden = true
        pointsTxt.text = ""
    }

}

extension SelectAdvanceMerchantViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if text.count > 0 {
            self.showAccessoryView(status : true)
            self.clearButton.isHidden = false
            self.createBtn.isHidden = false
        } else {
            self.showAccessoryView(status : false)
            self.clearButton.isHidden = true
            self.createBtn.isHidden = true
        }
        return true
    }
}

extension SelectAdvanceMerchantViewController : SelectedAdvMerchantDetail {
    func selectedAdvMerchant(dictionary: Dictionary<String , Any>) {
        self.merchaneNameLbl.text = dictionary.safeValueForKey("PhoneNumber") as? String ?? ""
        if headerTitle == "Dummy Wallet Configuration" {
            self.parentTypeView.isHidden = false
            createBtn.isHidden = false
        } else {
            self.pointsView.isHidden = false
        }
    }
}

