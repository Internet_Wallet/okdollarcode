//
//  LOPointsTrfReceiptViewController.swift
//  OK
//
//  Created by Kethan on 10/30/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

protocol LOReceiptPopVC: class {
    func morePaymentAction(phoneNumber: String, repeatPay: Bool)
}

class LOPointsTrfReceiptViewController: LoyaltyBaseViewController, UITabBarDelegate, CNContactViewControllerDelegate, MoreControllerActionDelegate, PaymentRateDelegate {
    
    weak var delegate: LOReceiptPopVC?
    
    @IBOutlet weak var mobileNumber: UILabel!{
        didSet{
            self.mobileNumber.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var amount: UILabel!{
        didSet{
            self.amount.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var nameLabel: UILabel!{
        didSet{
            self.nameLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var keysenderAccountName: UILabel!{
        didSet{
            self.keysenderAccountName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var valuesenderAccountName: UILabel!{
        didSet{
            self.valuesenderAccountName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var keysenderAccountNumber: UILabel!{
        didSet{
            self.keysenderAccountNumber.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var valuesenderAccountNumber: UILabel!{
        didSet{
            self.valuesenderAccountNumber.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var keysenderAmount: UILabel!{
        didSet{
            self.keysenderAmount.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var valuesenderAmount: UILabel!{
        didSet{
            self.valuesenderAmount.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var keyTransactionID: UILabel!
        {
        didSet
        {
            self.keyTransactionID.font = UIFont(name: appFont, size: appFontSize)
            keyTransactionID.text = "Transaction ID".localized
        }
    }
    @IBOutlet weak var valueTransactionID: UILabel!{
        didSet{
            self.valueTransactionID.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var valueDate: UILabel!{
        didSet{
            self.valueDate.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var valueTime: UILabel!{
        didSet{
            self.valueTime.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var heightDateConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var rateservice: UILabel!
        {
        didSet
        {
            self.rateservice.font = UIFont(name: appFont, size: appFontSize)
            rateservice.text = "Rate OK$ Service".localized
        }
    }
    
    @IBOutlet weak var cardView: CardDesignView!
    
    @IBOutlet weak var rateBtn: UIButton!
    
    var reciptModel : loyaltyPointTrfModel?
    
    var categoryValue : RecipetPlanCases?
    
    let optionTextDefaultColor = UIColor.init(hex: "162D9F")
    var planType : String?
    
    var InternationalTopup : String?
    
    var currencyInt : String?
    
    var contactAdded : Bool = false
    
    @IBOutlet  var starArray: [UIImageView]!
    var type = ""
    
    @IBOutlet weak var viewTab: UIView!
    var viewOptions: TabOptionsOnReceipt?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            self.changeCancelImplementation()
        }
        guard let model = reciptModel else { return }
        let isInCont = checkForContact(model.receiverNo.safelyWrappingString()).isInContact
        
        guard let bView = Bundle.main.loadNibNamed("TabOptionsOnReceipt", owner: self, options: nil)?.first as? TabOptionsOnReceipt else { return }
        viewOptions = bView
        bView.delegate = self
        viewOptions?.frame = CGRect(x: 0, y: 0, width: self.viewTab.frame.size.width, height: viewTab.frame.size.height)
        if let _ = viewOptions {
            viewTab.addSubview(viewOptions!)
            viewOptions!.lblHome.text = "HomePagePT".localized
            viewOptions!.lblMore.text = "More".localized
            viewOptions!.lblAddContact.text = "TabbarAddContact".localized
            viewOptions!.viewAddFav.isHidden = true
            if isInCont == true {
                addContactUI(isInContact: true)
            }
            viewOptions!.layoutIfNeeded()
        }
        self.uiUpdates()
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .normal)
        UITabBar.appearance().unselectedItemTintColor = .lightGray
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Receipt".localized
        self.navigationController?.navigationBar.titleTextAttributes = PaytoConstants.global.attributesForTitle()

        //        [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"AmericanTypewriter" size:20.0f]} forState:UIControlStateNormal];
        self.rateservice.text = "Rate OK$ Service".localized
        
        //self.navigationController?.navigationBar.backgroundColor = MyNumberTopup.OperatorColorCode.okDefault
        //self.navigationController?.navigationBar.tintColor = MyNumberTopup.OperatorColorCode.okDefault
        navigationController?.navigationBar.barTintColor = kYellowColor
        self.navigationItem.setHidesBackButton(true, animated: true)
        
    }
    
    func addContactUI(isInContact: Bool) {
        if let _ = viewOptions {
            viewOptions?.imgAddContact.image = viewOptions?.imgAddContact.image?.withRenderingMode(.alwaysTemplate)
            if isInContact {
                viewOptions?.imgAddContact.tintColor = kYellowColor
                viewOptions!.lblAddContact.textColor = kYellowColor
            } else {
                viewOptions?.imgAddContact.tintColor = UIColor.gray
                viewOptions!.lblAddContact.textColor = optionTextDefaultColor
            }
            viewOptions!.layoutIfNeeded()
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        //navigationController?.navigationBar.barTintColor = UIColor.red
    }
    
    //MARK:- Initial UI Updates
    private func uiUpdates() {
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.keysenderAccountName.text = self.keysenderAccountName.text.safelyWrappingString().localized
        self.keysenderAccountNumber.text = self.keysenderAccountNumber.text.safelyWrappingString().localized
        self.keysenderAmount.text = self.keysenderAmount.text.safelyWrappingString().localized
        //self.keyTransactionID.text = self.keyTransactionID.text.safelyWrappingString().localized
        self.valueDate.text = self.valueDate.text.safelyWrappingString().localized
        self.valueTime.text = self.valueTime.text.safelyWrappingString().localized
        
        guard let model = reciptModel else { return }
        
        self.valueTransactionID.text = model.transactionID.safelyWrappingString()
          //println_debug("Trans Points : \(model.points)")
        self.amount.text = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: model.points.safelyWrappingString())//model.points.safelyWrappingString()
        
        if let date = model.transactionDate {
            let dateFormat = date.components(separatedBy: " ").first?.dateValue(dateFormatIs: "dd-MMM-yyyy")
            let dateKey = (dateFormat?.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy"))!
            self.valueDate.text = dateKey
            
            let timeKey = date.components(separatedBy: " ").last ?? ""
            self.valueTime.text = timeKey
        }
        
//        let preciseTime  = model.transactionDate.safelyWrappingString().components(separatedBy: " ")
//        self.valueDate.text = preciseTime.first.safelyWrappingString()
//        self.valueTime.text = preciseTime.last.safelyWrappingString()
        self.mobileNumber.text = wrapFormattedNumber(model.receiverNo.safelyWrappingString())
        self.valuesenderAccountName.text = model.merchantName.safelyWrappingString()
        self.valuesenderAccountNumber.text = model.category.safelyWrappingString()
        self.navigationController?.navigationBar.backgroundColor = MyNumberTopup.OperatorColorCode.okDefault
        self.navigationController?.navigationBar.tintColor = MyNumberTopup.OperatorColorCode.okDefault
        //println_debug("Rest Points : \(model.balance)")
        let rs = model.balance.safelyWrappingString()
        if rs.contains(find: ".") {
            let pt = rs.components(separatedBy: ".")
            if pt[0].count > 0 {
                let formattedTransAmount =  pt[0]
                let customAttribute1 = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 21) ?? UIFont.systemFont(ofSize: 21),
                                         NSAttributedString.Key.foregroundColor: UIColor.red ]
                let attributedAmountString = NSMutableAttributedString.init(string: formattedTransAmount, attributes: customAttribute1)
                
                let customAttribute2 = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 16) ?? UIFont.systemFont(ofSize: 16),
                                         NSAttributedString.Key.foregroundColor: UIColor.black ]
                let mmkString = NSMutableAttributedString.init(string: " Points", attributes: customAttribute2)
                
                attributedAmountString.append(mmkString)
                self.valuesenderAmount.attributedText = attributedAmountString
            }
        }else {
            let formattedTransAmount =  model.balance.safelyWrappingString()
            let customAttribute1 = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 21) ?? UIFont.systemFont(ofSize: 21),
                                     NSAttributedString.Key.foregroundColor: UIColor.red ]
            let attributedAmountString = NSMutableAttributedString.init(string: formattedTransAmount, attributes: customAttribute1)
            
            let customAttribute2 = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 16) ?? UIFont.systemFont(ofSize: 16),
                                     NSAttributedString.Key.foregroundColor: UIColor.black ]
            let mmkString = NSMutableAttributedString.init(string: " Points", attributes: customAttribute2)
            
            attributedAmountString.append(mmkString)
            self.valuesenderAmount.attributedText = attributedAmountString
        }
    }
    
    
    //MARK:- ADD FAVORITE
    private func addFav(model: loyaltyPointTrfModel) {
        let isInFav = checkForFavorites(model.receiverNo.safelyWrappingString(), type: "Loyalty").isInFavorite
        if isInFav {
            PaytoConstants.alert.showToast(msg: "Contact already added in Favorites".localized)
        } else {
            let favVC = self.addFavoriteController(withName: self.keysenderAccountName.text.safelyWrappingString(), favNum: model.receiverNo.safelyWrappingString(), type: "PAYTO", amount: model.points.safelyWrappingString())
            if let vc = favVC {
                self.navigationController?.present(vc, animated: false, completion: nil)
            }
        }
    }
    
    //MARK:- ADD CONTACT
    private func addContacts(model: loyaltyPointTrfModel) {
        let isInCont = checkForContact(model.receiverNo.safelyWrappingString()).isInContact
        if isInCont || contactAdded {
            self.showErrorAlert(errMessage: "Contact Already Added".localized)
            return
        }
        let store     = CNContactStore()
        let contact   = CNMutableContact()
        let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue : wrapFormattedNumber(model.receiverNo.safelyWrappingString()) ))
        contact.phoneNumbers = [homePhone]
        if let name = model.merchantName, name.count > 0 {
            contact.givenName = "Unknown"
        } else {
            contact.givenName = model.name.safelyWrappingString()
        }
        let controller = CNContactViewController.init(forNewContact: contact)
        controller.contactStore = store
        controller.delegate     = self
        controller.title        = "Add Contact".localized
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.backgroundColor = UIColor.init(hex: "F3C632")
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    // contact delegate
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        if let newContact = contact {
            contactAdded = true
            addContactUI(isInContact: true)
            okContacts.append(newContact)
        }
        viewController.delegate = nil
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- HOME
    private func goToHome() {
        // self.navigationController?.popToRootViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DismissLoyaltyView"), object: nil, userInfo: nil)
    }
    
    //MARK:- MORE
    private func more() {
        guard  let vc = UIStoryboard.init(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PTMoreViewController") as? PTMoreViewController else {return}
        addChild(vc)
        vc.isFromTopup = false
        vc.isFromEarnPointTrf = true
        vc.isFromEarnPointTrf = true
        vc.view.frame = self.view.bounds
        vc.didMove(toParent: self)
        vc.delegate = self
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        
        vc.view.layer.add(transition, forKey: nil)
        view.addSubview(vc.view)
    }
    
    func showPointTrfScreen(repeatPay: Bool = false) {
        DispatchQueue.main.async {
            guard let model = self.reciptModel else { return }
           let number = model.receiverNo.safelyWrappingString()
            self.delegate?.morePaymentAction(phoneNumber: number, repeatPay: repeatPay)
            self.navigationController?.navigationBar.barTintColor = UIColor.red
            self.navigationController?.navigationBar.tintColor = UIColor.white
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    // more delegate callback
    func actionFor(option: MorePaymentAction) {
        switch option {
        case .morePayment:
            self.showPointTrfScreen()
        case .repeatPayment:
//            var key = Dictionary<String,Any>()
//            guard let model = reciptModel else { return }
//            key["number"] = model.receiverNo.safelyWrappingString()
//            NotificationCenter.default.post(name: Notification.Name("RepeatEarnPoint"), object: nil, userInfo: key)
            self.showPointTrfScreen(repeatPay: true)
        case .invoice:
            self.invoice()
        case .share:
            self.share()
        }
    }
    
    private func getDateFormat(date: String) -> String {
        let dateFormat = date
        let dateF = DateFormatter()
        dateF.dateFormat = "dd-MMM-yyyy HH:mm:ss"
        guard let dateLoc = dateF.date(from: date) else { return dateFormat }
        dateF.dateFormat = "EEE, dd-MMM-yyyy HH:mm:ss"
        return dateF.string(from: dateLoc)
    }
    
    
    private func invoice() {
        
        guard let model = reciptModel else { return }
        
        var pdfDictionary = Dictionary<String,Any>()
        pdfDictionary["senderAccName"]      = UserModel.shared.name
        
        let number = "(\(UserModel.shared.countryCode))\(UserModel.shared.formattedNumber)"
        let mobileNum = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: model.receiverNo.safelyWrappingString())
        let destination = "(\(mobileNum.counrtycode))\(mobileNum.mobileNo)"
        if type == "PointDistribution" {
            pdfDictionary["invoiceTitle"]       = "Points Distribution Receipt".localized
        } else {
            pdfDictionary["invoiceTitle"]       = "Earn Points Transfer Receipt".localized
        }
        pdfDictionary["senderAccNo"]        = number
        pdfDictionary["receiverAccNo"]       = destination
        pdfDictionary["transactionID"]      = model.transactionID
        pdfDictionary["transactionType"]    = model.category
        let dateAndTime = self.getDateFormat(date: model.transactionDate.safelyWrappingString())
        let pdfAmount = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: model.points.safelyWrappingString()) + " Points"
        pdfDictionary["amount"]             = pdfAmount
        //pdfDictionary["amount"]             = model.points.safelyWrappingString()
        //pdfDictionary["cashback"]           = model.kickvalue.safelyWrappingString()
        pdfDictionary["transactionDate"]    = dateAndTime
        pdfDictionary["logoName"]           = "appIcon_Ok"
        pdfDictionary["loyaltyType"]           = type
        
//        var name = ""
//        if UserModel.shared.agentType == .user {
//            name = UserModel.shared.name
//        } else {
//            name = UserModel.shared.businessName
//        }
//        let lat  = geoLocManager.currentLatitude ?? ""
//        let long = geoLocManager.currentLongitude ?? ""
//
//        let firstPart  = "00#" + "\(name)" + "-" + "\(UserModel.shared.mobileNo) " + "@" + pdfAmount + "&"
//        let secondPart =  "\(pdfDictionary["loyaltypoints"] ?? "")" + "β" + "\(lat)" + "γ" + "\(long)" + "α" + "\(pdfDictionary["transactionID"] ?? "")" + "" + "`,,"
//
//        let finalPart = String.init(format:"%@%@", firstPart,secondPart)
        
//        guard let hashedQRKey = AESCrypt.encrypt(finalPart, password: "m2n1shlko@$p##d") else { return }
//
//        let qrObject = PTQRGenerator.init()
        //pdfDictionary["qrImage"] = qrObject.getQRImage(stringQR: hashedQRKey, withSize: 10)
        
        guard let pdfUrl = CodeSnippets.createInvoicePDF(dictValues: pdfDictionary, pdfFile: "OK$ loyalityPoint Transfar Receipt") else {
            println_debug("Error - pdf not generated")
            return
        }
        
        guard let vc = UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: "SharePDFViewController") as? SharePDFViewController else { return }
        vc.url = pdfUrl
        navigationController?.navigationBar.barTintColor = kYellowColor
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func share() {
        self.rateservice.isHidden = true
        for star in starArray {
            star.isHidden = true
        }
        let snapImage = self.cardView.snapshotOfCustomeView
        self.rateservice.isHidden = false
        for star in starArray {
            star.isHidden = false
        }
        let vc = UIActivityViewController(activityItems: [snapImage], applicationActivities: [])
        DispatchQueue.main.async {self.present(vc, animated: true)}
        
    }
    
    private func screenShotMethod(_ view: UIView) -> UIImage? {
        //Create the UIImage
        UIGraphicsBeginImageContext(view.frame.size)
        
        if let context = UIGraphicsGetCurrentContext() {
            view.layer.render(in: context)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            if let imageFinal = image {
                UIImageWriteToSavedPhotosAlbum(imageFinal, nil, nil, nil)
            }
            return image
        }
        
        return nil
    }
    
    
    //MARK:- Rating Functionality
    @IBAction func openRateWindowAction(_ sender: UIButton) {
        guard let model = reciptModel else { return }
        guard let ratingScreen = self.storyboard?.instantiateViewController(withIdentifier: "LORedeemRateViewController_ID") as? LORedeemRateViewController else { return }
        ratingScreen.modalPresentationStyle = .overCurrentContext
        ratingScreen.delegate = self
        navigationController?.navigationBar.barTintColor = UIColor.red
        ratingScreen.destinationNumber = model.receiverNo.safelyWrappingString()
//        ratingScreen.modalPresentationStyle = .fullScreen
        self.navigationController?.present(ratingScreen, animated: true, completion: nil)
    }
    
    func ratingShow(_ rate: Int, commentText: String?) {
        
        DispatchQueue.main.async {
            self.rateBtn.isUserInteractionEnabled = false
            for star in self.starArray {
                if star.tag <= rate {
                    UIView.animate(withDuration: 0.2, animations: {
                        star.image = #imageLiteral(resourceName: "receiptStarFilled")
                    })
                } else {
                    star.image = #imageLiteral(resourceName: "receiptStarUnfilled")
                }
            }
        }
    }
    
    func changeCancelImplementation() {
        let originalSelector = Selector(("editCancel:"))
        let swizzledSelector = #selector(CNContactViewController.cancelHack)
        if let originalMethod = class_getInstanceMethod(object_getClass(CNContactViewController()), originalSelector),
           let swizzledMethod = class_getInstanceMethod(object_getClass(CNContactViewController()), swizzledSelector) {
            method_exchangeImplementations(originalMethod, swizzledMethod)
        }
    }
    
}

//MARK:- TabOptionProtocol
extension LOPointsTrfReceiptViewController: TabOptionProtocol {
    func addFavorite() {
        if let _ = viewOptions {
            if viewOptions!.lblAddFav.textColor != kYellowColor {
                
            }
        }
    }
    
    func addContact() {
        if let _ = viewOptions {
            if viewOptions!.lblAddContact.textColor != kYellowColor {
                guard let modelRec = reciptModel else { return }
                self.addContacts(model: modelRec)
            } else {
                PaytoConstants.alert.showToast(msg: "Contact Already Added".localized)
            }
        }
    }    
    
    func goHome() {
        self.goToHome()
    }
    
    func showMore() {
        self.more()
    }
    
}
