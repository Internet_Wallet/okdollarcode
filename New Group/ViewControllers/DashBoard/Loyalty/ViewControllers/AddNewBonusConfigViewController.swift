//
//  AddNewBonusConfigViewController.swift
//  OK
//
//  Created by Kethan on 2/27/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit



enum activeButtonStatus {
    case active, inActive
}

protocol CreateNewBonusConfig : class {
    func newOrUpdateBonusConfig()
}

class AddNewBonusConfigViewController: LoyaltyBaseViewController {
    var startSelDate: Date?
    var endSelDate: Date?
    @IBOutlet weak var topView : UIView!
    var bonusConfigListArray = [Dictionary<String,Any>]()
    var bonusConfigDict = Dictionary<String,Any>()
        
    var testCalendar = Calendar(identifier: .gregorian)
    var currentDate: Date! = Date() {
        didSet {
            setDate()
        }
    }
    
    weak var delegate : CreateNewBonusConfig?
    var selectedModel : Dictionary<String, Any>?
    
    @IBOutlet weak var createBtn : UIButton! {
        didSet {
            self.createBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.createBtn.setTitle(self.createBtn.titleLabel?.text, for: .normal)
            self.createBtn.isHidden = true
        }
    }
    
    @IBOutlet weak var activeBtn : UIButton!{
        didSet{
            self.activeBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var inActiveBtn : UIButton!{
        didSet{
            self.inActiveBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    @IBOutlet weak var activeLabel : UILabel! {
        didSet {
            activeLabel.font = UIFont(name: appFont, size: appFontSize)
            activeLabel.text = activeLabel.text?.localized
        }
    }
    @IBOutlet weak var activeImageView : UIImageView! {
        didSet {
            activeImageView.image = UIImage(named: "active.png")
        }
    }
    @IBOutlet weak var inActiveLabel : UILabel! {
        didSet {
            inActiveLabel.font = UIFont(name: appFont, size: appFontSize)
            inActiveLabel.text = inActiveLabel.text?.localized
        }
    }
    @IBOutlet weak var inActiveImageView : UIImageView! {
        didSet {
            inActiveImageView.image = UIImage(named: "in_active.png")
        }
    }
    
    @IBOutlet weak var maxRangeTxt : UITextField! {
        didSet {
            self.maxRangeTxt.font = UIFont(name: appFont, size: appFontSize)
            self.maxRangeTxt.placeholder = self.maxRangeTxt.placeholder?.localized
        }
    }
    @IBOutlet weak var minRangeTxt : UITextField! {
        didSet {
            self.minRangeTxt.font = UIFont(name: appFont, size: appFontSize)
            self.minRangeTxt.placeholder = self.minRangeTxt.placeholder?.localized
        }
    }
    @IBOutlet weak var perAmountTxt : UITextField! {
        didSet {
            self.perAmountTxt.font = UIFont(name: appFont, size: appFontSize)
            self.perAmountTxt.placeholder = self.perAmountTxt.placeholder?.localized
        }
    }
    @IBOutlet weak var bonusPointsTxt : UITextField! {
        didSet {
            self.bonusPointsTxt.font = UIFont(name: appFont, size: appFontSize)
            self.bonusPointsTxt.placeholder = self.bonusPointsTxt.placeholder?.localized
        }
    }
    @IBOutlet weak var startDateTxt : UITextField! {
        didSet {
            self.startDateTxt.font = UIFont(name: appFont, size: appFontSize)
            self.startDateTxt.isEnabled = false
            self.startDateTxt.placeholder = self.startDateTxt.placeholder?.localized
        }
    }
    @IBOutlet weak var endDateTxt : UITextField! {
        didSet {
            self.endDateTxt.font = UIFont(name: appFont, size: appFontSize)
            self.endDateTxt.isEnabled = false
            self.endDateTxt.placeholder = self.endDateTxt.placeholder?.localized
        }
    }
    @IBOutlet weak var expiryDaysTxt : UITextField! {
        didSet {
            self.expiryDaysTxt.font = UIFont(name: appFont, size: appFontSize)
            self.expiryDaysTxt.placeholder = self.expiryDaysTxt.placeholder?.localized
        }
    }
    
    let fomatter = DateFormatter()
    
    var startDate = Date()
    var endDate = Date()
    
    var activeButtonTap : activeButtonStatus = .active
    let dateFormatter = DateFormatter()
    let apiDateFormatter = DateFormatter()
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        UserDefaults.standard.removeObject(forKey: "StartDate")
        UserDefaults.standard.removeObject(forKey: "EndDate")
        addCustomBackButton(navTitle: "Bonus Configuration")
        fomatter.calendar = Calendar(identifier: .gregorian)
        fomatter.dateFormat = "dd-MMM-yyyy"
        //startDateTxt.addPlaceHolderView(tag: startDateTxt.tag)
        //endDateTxt.addPlaceHolderView(tag: endDateTxt.tag)
        //expiryDaysTxt.addPlaceHolderView(tag: expiryDaysTxt.tag)
        maxRangeTxt.addTarget(self, action: #selector(AddNewBonusConfigViewController.textFieldDidChange(_:)),
                              for: UIControl.Event.editingChanged)
        minRangeTxt.addTarget(self, action: #selector(AddNewBonusConfigViewController.textFieldDidChange(_:)),
                            for: UIControl.Event.editingChanged)
        bonusPointsTxt.addTarget(self, action: #selector(AddNewBonusConfigViewController.textFieldDidChange(_:)),
                                  for: UIControl.Event.editingChanged)
        perAmountTxt.addTarget(self, action: #selector(AddNewBonusConfigViewController.textFieldDidChange(_:)),
                                  for: UIControl.Event.editingChanged)
        // Do any additional setup after loading the view.
        println_debug(selectedModel)
        if let model = selectedModel {
            updateUI(model : model)
            createBtn.isHidden = false
            createBtn.setTitle("Update".localized, for: .normal)
        } else {
            createBtn.setTitle("Create".localized, for: .normal)
        }
    }
    
    private func updateUI(model : Dictionary<String, Any>) {
        if let value = model.safeValueForKey("maxrange") as? String {
            maxRangeTxt.text = value.replacingOccurrences(of: ".00", with: "")
            if let clearButton = self.view.viewWithTag(141) as? UIButton {
                clearButton.isHidden = false
            }
        }
        if let value = model.safeValueForKey("minrange") as? String {
            minRangeTxt.text = value.replacingOccurrences(of: ".00", with: "")
            if let clearButton = self.view.viewWithTag(142) as? UIButton {
                clearButton.isHidden = false
            }
        }
        
        for index in 121...128 {
            let view = self.view.viewWithTag(index)
            view?.isHidden = false
        }

        if let value = model.safeValueForKey("peramount") as? String {
            perAmountTxt.text = value.replacingOccurrences(of: ".0", with: "")
            if let clearButton = self.view.viewWithTag(143) as? UIButton {
                clearButton.isHidden = false
            }
        }
        if let value = model.safeValueForKey("point") as? String {
            bonusPointsTxt.text = value
            if let clearButton = self.view.viewWithTag(144) as? UIButton {
                clearButton.isHidden = false
            }
        }
        
        self.maxRangeTxt.placeholder = "Maximum amount range".localized
        self.minRangeTxt.placeholder = "Minimum amount range".localized
        self.bonusPointsTxt.placeholder = "Bonus Points".localized
        self.perAmountTxt.placeholder = "Per Amount".localized
        
        if let value = model.safeValueForKey("startdate") as? String {
            startDateTxt.text = getDateString(dateStr: value)
        }
        if let value = model.safeValueForKey("enddate") as? String {
            endDateTxt.text = getDateString(dateStr: value)
        }
        
        if let value = model.safeValueForKey("expiry") as? String {
            expiryDaysTxt.text = value
        }
        if let value = model.safeValueForKey("status") as? String {
            if value == "ACTIVE" {
                activeButtonTap = .active
                activeImageView.image = UIImage(named : "active.png")
                inActiveImageView.image = UIImage(named : "in_active.png")
            } else {
                activeButtonTap = .inActive
                activeImageView.image = UIImage(named : "in_active.png")
                inActiveImageView.image = UIImage(named : "active.png")
            }
        }
    }
 
    func getDateString(dateStr : String) -> String {
        apiDateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        apiDateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        var date = ""
        if dateStr.contains(find: " ") {
            let str = dateStr.components(separatedBy: " ")
            let str1 = str[0]
            if let apiDate = apiDateFormatter.date(from: str1) {
                date = dateFormatter.string(from: apiDate)
                return date
            }
        }else {
            return date
        }
        return date
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hideKeyboardWhenTappedAround()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func startDateButtonAction(_ sender : UIButton) {
        
        displayDateView()
    }
    
    func setDate() {
        let month = testCalendar.dateComponents([.month], from: currentDate).month!
        let weekday = testCalendar.component(.weekday, from: currentDate)
        let monthName = DateFormatter().monthSymbols[(month-1) % 12] //GetHumanDate(month: month)//
        let week = DateFormatter().shortWeekdaySymbols[weekday-1]
        
        let day = testCalendar.component(.day, from: currentDate)
        
        startDateTxt.text = "\(week), " + monthName + " " + String(day)
    }
    
    private func displayDateView() {
        if let dateVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: "LoyaltyDateSelectViewController_ID") as? LoyaltyDateSelectViewController {
            dateVC.delegate = self
            dateVC.startSeldate = startSelDate
            dateVC.endSelDate = endSelDate
            self.navigationController?.pushViewController(dateVC, animated: true)
        }
    }
    
    @IBAction func endDateButtonAction(_ sender : UIButton) {
        displayDateView()
//        for (_,label) in (startDateTxt.subviews.filter{$0 is UILabel}).enumerated(){
//            if let label1 = label as? UILabel {
//                label1.textColor = UIColor.lightGray
//            }
//        }
//        for (_,label) in (endDateTxt.subviews.filter{$0 is UILabel}).enumerated(){
//            if let label1 = label as? UILabel {
//                label1.textColor = UIColor.blue
//            }
//        }
    }
    
    @IBAction func createButtonAction(_ sender : UIButton) {
        if startDateTxt.text == "" || endDateTxt.text == "" {
            self.self.showCustomErrorAlert(errMessage: "Please select the date".localized)
            return
        }
        
        if appDel.checkNetworkAvail() {
            if selectedModel != nil {
                let minRange = NumberFormatter().number(from: self.removeCommaFromDigit(minRangeTxt.text!))?.doubleValue ?? 0.0
                let maxRange = NumberFormatter().number(from: self.removeCommaFromDigit(maxRangeTxt.text!))?.doubleValue ?? 0.0
                let perAmt = NumberFormatter().number(from: self.removeCommaFromDigit(perAmountTxt.text!))?.doubleValue ?? 0.0
                let points = NumberFormatter().number(from: self.removeCommaFromDigit(bonusPointsTxt.text!))?.intValue ?? 0
                if maxRange < minRange {
                    self.showCustomErrorAlert(errMessage: "Maximum Range should be greater than minimum range".localized)
                    self.minRangeTxt.text = ""
                    self.minRangeTxt.becomeFirstResponder()
                    return
                } else if perAmt == 0 {
                    self.showCustomErrorAlert(errMessage: "Please enter the Per amount".localized)
                    self.perAmountTxt.text = ""
                    self.perAmountTxt.becomeFirstResponder()
                    return
                } else if points == 0 {
                    self.showCustomErrorAlert(errMessage: "Please enter the points".localized)
                    self.bonusPointsTxt.text = ""
                    self.bonusPointsTxt.becomeFirstResponder()
                    return
                }
            }
            self.addNewBonuSconfig()
        } else {
            self.noInternetAlert()
        }
    }
    
    @IBAction func activeButtonAction(_ sender : UIButton) {
        activeButtonTap = .active
        activeImageView.image = UIImage(named : "active.png")
        inActiveImageView.image = UIImage(named : "in_active.png")
    }
    
    @IBAction func inActiveButtonAction(_ sender : UIButton) {
        activeButtonTap = .inActive
        activeImageView.image = UIImage(named : "in_active.png")
        inActiveImageView.image = UIImage(named : "active.png")
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        /*
         if maxRangeTxt.text == "" {
         self.showCustomErrorAlert(errMessage: "Please enter the maximum range".localized)
         textField.text?.removeAll()
         }*/
        if textField.tag == 131 {
            maxRangeTxt.text =  self.getDigitDisplay(textField.text!)
        } else if(textField.tag == 132 ) {
            if ((textField.text?.length) != 0) {
                minRangeTxt.text =  self.getDigitDisplay(textField.text!)
                let maxRange : Int = Int(self.removeCommaFromDigit(maxRangeTxt.text!)) ?? 0
                let minRange : Int = Int(self.removeCommaFromDigit(textField.text!)) ?? 0
                if minRange >= maxRange {
                    self.showCustomErrorAlert(errMessage: "Minimum range should be less than Maximum range".localized)
                    textField.text?.removeLast()
                    minRangeTxt.text =  self.getDigitDisplay(textField.text!)
                    if textField.text?.count == 0 {
                        if let clearButton = self.view.viewWithTag(142) as? UIButton {
                            clearButton.isHidden = true
                        }
                    }
                }
            }
        } else if(textField.tag == 133) {
            if ((textField.text?.length) != 0) {
                perAmountTxt.text =  self.getDigitDisplay(textField.text!)
                let maxRange : Int = Int(self.removeCommaFromDigit(maxRangeTxt.text!)) ?? 0
                let minRange : Int = Int(self.removeCommaFromDigit(textField.text!)) ?? 0
                if minRange >= maxRange {
                    self.showCustomErrorAlert(errMessage: "Per Amount should be less than Maximum range".localized)
                    textField.text?.removeLast()
                    perAmountTxt.text =  self.getDigitDisplay(textField.text!)
                    if textField.text?.count == 0 {
                        if let clearButton = self.view.viewWithTag(143) as? UIButton {
                            clearButton.isHidden = true
                        }
                    }
                }
            }
        } else if(textField.tag == 134) {
            bonusPointsTxt.text =  self.getDigitDisplay(textField.text!)
        }
        
    }
    
    private func addNewBonuSconfig() {
        let web      = PayToWebApi()
        web.delegate = self
        var ip = ""
        if let ipAdd = OKBaseController.getIPAddress() {
            ip = ipAdd
        }
        
        let startTime = self.timeFormat(dateStr: startDateTxt.text ?? "")
        
        let endTime = self.timeFormat(dateStr: endDateTxt.text ?? "")
        var status = ""
        if (activeButtonTap == .active) {
            status = "ACTIVE"
        } else {
            status = "INACTIVE"
        }
        var mode = "ADD"
        var refId = ""
        if let model = selectedModel {
            mode = "UPDATE"
            refId = model.safeValueForKey("serialno") as? String ?? ""
        }
        
        let minRange = NumberFormatter().number(from: self.removeCommaFromDigit(minRangeTxt.text!))?.doubleValue ?? 0.0
        let maxRange = NumberFormatter().number(from: self.removeCommaFromDigit(maxRangeTxt.text!))?.doubleValue ?? 0.0
        let perAmt = NumberFormatter().number(from: self.removeCommaFromDigit(perAmountTxt.text!))?.doubleValue ?? 0.0
        let expiryDays = NumberFormatter().number(from: self.removeCommaFromDigit(expiryDaysTxt.text!))?.intValue ?? 0
        let points = NumberFormatter().number(from: self.removeCommaFromDigit(bonusPointsTxt.text!))?.intValue ?? 0

        
        let urlStr   = String.init(format: Url.addNewBonusConfigAPiUrl, startTime, UserModel.shared.mobileNo, String(minRange), String(perAmt), String(points), status, ok_password ?? "", mode, endTime, refId, String(maxRange), String(expiryDays), ip , "ios", UserLogin.shared.token)
        let url = getUrl(urlStr: urlStr, serverType: .serverEstel)
        println_debug(url)
        let pRam = Dictionary<String,String>()
        web.genericClassXML(url: url, param: pRam as AnyObject, httpMethod: "POST", mScreen: "mAddBonusConfigList")
    }
    
    fileprivate func timeFormat(dateStr : String) -> String {
        println_debug(dateStr)
        var timeStr = "00000000000000"
        let apiDateFormatter = DateFormatter()
        let dateFormatter = DateFormatter()
        apiDateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.calendar = Calendar(identifier: .gregorian)

        dateFormatter.dateFormat = "dd-MMM-yyyy"
        apiDateFormatter.dateFormat = "yyyyMMdd"
        if let apiDate = dateFormatter.date(from: dateStr) {
            timeStr = ""
            timeStr = apiDateFormatter.string(from: apiDate)
            timeStr = timeStr + "000000"
        }
        return timeStr
    }
    
    fileprivate func dismissAndCallDisplayConfig() {
        self.delegate?.newOrUpdateBonusConfig()
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func clearButtonTapped(_ sender : UIButton) {
        self.startDateTxt.text = ""
        self.endDateTxt.text = ""
        self.createBtn.isHidden = true
        switch sender.tag {
        case 141 :
            self.maxRangeTxt.text = ""
            self.minRangeTxt.text = ""
            self.bonusPointsTxt.text = ""
            self.perAmountTxt.text = ""
            
            self.maxRangeTxt.placeholder = "Enter maximum amount range".localized
            self.minRangeTxt.placeholder = "Enter minimum amount range".localized
            self.perAmountTxt.placeholder = "Enter per amount".localized
            self.bonusPointsTxt.placeholder = "Enter bonus point".localized
            sender.isHidden = true
            let startTag = 142 - 19
            if let clearButton = self.view.viewWithTag(142) as? UIButton {
                clearButton.isHidden = true
            }
            if let clearButton1 = self.view.viewWithTag(143) as? UIButton {
                clearButton1.isHidden = true
            }
            if let clearButton2 = self.view.viewWithTag(144) as? UIButton {
                clearButton2.isHidden = true
            }
            for viewLocal in topView.subviews {
                if viewLocal.tag >= startTag {
                    viewLocal.isHidden = true
                }
            }
            self.maxRangeTxt.becomeFirstResponder()
        case 142:
            self.minRangeTxt.text = ""
            self.bonusPointsTxt.text = ""
            self.perAmountTxt.text = ""
            sender.isHidden = true
            self.minRangeTxt.placeholder = "Enter minimum amount range".localized
            self.perAmountTxt.placeholder = "Enter per amount".localized
            self.bonusPointsTxt.placeholder = "Enter bonus point".localized
            let startTag = 142 - 19
            
            if let clearButton = self.view.viewWithTag(143) as? UIButton {
                clearButton.isHidden = true
            }
            if let clearButton1 = self.view.viewWithTag(144) as? UIButton {
                clearButton1.isHidden = true
            }
            
            for viewLocal in topView.subviews {
                if viewLocal.tag >= startTag {
                    viewLocal.isHidden = true
                }
            }
        case 143 :
            self.perAmountTxt.text = ""
            self.bonusPointsTxt.text = ""
            self.perAmountTxt.placeholder = "Enter per amount".localized
            self.bonusPointsTxt.placeholder = "Enter bonus point".localized
            sender.isHidden = true
            let startTag = 143 - 19
            if let clearButton1 = self.view.viewWithTag(144) as? UIButton {
                clearButton1.isHidden = true
            }
            if let mintxt = self.view.viewWithTag(134) as? UITextField {
                mintxt.text = ""
            }
            for viewLocal in topView.subviews {
                if viewLocal.tag >= startTag {
                    viewLocal.isHidden = true
                }
            }
        case 144:
            self.bonusPointsTxt.placeholder = "Enter bonus point".localized
            self.bonusPointsTxt.text = ""
            sender.isHidden = true
            let startTag = 144 - 19
            for viewLocal in topView.subviews {
                if viewLocal.tag >= startTag {
                    viewLocal.isHidden = true
                }
            }
        default:
            break
        }
    }
    
    private func showClearButton(tag : Int, text : Int) {
        let viewLocal = self.view.viewWithTag(tag + 10)
        if text > 0 {
            viewLocal?.isHidden = false
        } else {
            viewLocal?.isHidden = true
        }
    }
    
    private func updatePlaceHolderText(textfield: UITextField, count: Int) {
        self.startDateTxt.text = ""
        self.endDateTxt.text = ""
        if count > 0 {
            if textfield.tag == 131 {
                textfield.placeholder = "Maximum amount range".localized
                minRangeTxt.placeholder = "Enter minimum amount range".localized
                self.perAmountTxt.placeholder = "Enter per amount".localized
                self.bonusPointsTxt.placeholder = "Enter bonus point".localized
                minRangeTxt.text = ""
                perAmountTxt.text = ""
                bonusPointsTxt.text = ""
                if let clearButton = self.view.viewWithTag(142) as? UIButton {
                    clearButton.isHidden = true
                }
                if let clearButton = self.view.viewWithTag(143) as? UIButton {
                    clearButton.isHidden = true
                }
                if let clearButton = self.view.viewWithTag(144) as? UIButton {
                    clearButton.isHidden = true
                }
            } else if textfield.tag == 132 {
                textfield.placeholder = "Minimum amount range".localized
                self.perAmountTxt.placeholder = "Enter per amount".localized
                self.bonusPointsTxt.placeholder = "Enter bonus point".localized
                perAmountTxt.text = ""
                bonusPointsTxt.text = ""
                if let clearButton = self.view.viewWithTag(143) as? UIButton {
                    clearButton.isHidden = true
                }
                if let clearButton = self.view.viewWithTag(144) as? UIButton {
                    clearButton.isHidden = true
                }
            } else if textfield.tag == 133 {
                textfield.placeholder = "Per Amount".localized
                self.bonusPointsTxt.placeholder = "Enter bonus point".localized
                bonusPointsTxt.text = ""
                if let clearButton = self.view.viewWithTag(144) as? UIButton {
                    clearButton.isHidden = true
                }
            } else if textfield.tag == 134 {
                textfield.placeholder = "Bonus Points".localized
            }
        } else {
            if textfield.tag == 131 {
                textfield.placeholder = "Enter maximum amount range".localized
                minRangeTxt.placeholder = "Enter minimum amount range".localized
                self.perAmountTxt.placeholder = "Enter per amount".localized
                self.bonusPointsTxt.placeholder = "Enter bonus point".localized
                minRangeTxt.text = ""
                perAmountTxt.text = ""
                bonusPointsTxt.text = ""
                if let clearButton = self.view.viewWithTag(142) as? UIButton {
                    clearButton.isHidden = true
                }
                if let clearButton = self.view.viewWithTag(143) as? UIButton {
                    clearButton.isHidden = true
                }
                if let clearButton = self.view.viewWithTag(144) as? UIButton {
                    clearButton.isHidden = true
                }
            } else if textfield.tag == 132 {
                textfield.placeholder = "Enter minimum amount range".localized
                self.perAmountTxt.placeholder = "Enter per amount".localized
                self.bonusPointsTxt.placeholder = "Enter bonus point".localized
                perAmountTxt.text = ""
                bonusPointsTxt.text = ""
                if let clearButton = self.view.viewWithTag(143) as? UIButton {
                    clearButton.isHidden = true
                }
                if let clearButton = self.view.viewWithTag(144) as? UIButton {
                    clearButton.isHidden = true
                }
            } else if textfield.tag == 133 {
                textfield.placeholder = "Enter per amount".localized
                self.bonusPointsTxt.placeholder = "Enter bonus point".localized
                bonusPointsTxt.text = ""
                if let clearButton = self.view.viewWithTag(144) as? UIButton {
                    clearButton.isHidden = true
                }
            } else if textfield.tag == 134 {
                textfield.placeholder = "Enter bonus point".localized
                self.startDateTxt.text = ""
                self.endDateTxt.text = ""
            }
        }
        self.hideViews(textfield, count: count)
    }
}



//PTWebresponseDelegate
extension AddNewBonusConfigViewController : PTWebResponseDelegate {
    func webSuccessResult(data: Any, screen: String) {
//        println_debug(data)
//        println_debug(screen)
        
        if screen == "mAddBonusConfigList" {
            if let xmlString = data as? String {
                let xml = SWXMLHash.parse(xmlString)
                
                bonusConfigDict.removeAll()
                self.enumerate(indexer: xml)
                self.bonusConfigListArray.removeAll()
                self.bonusConfigListArray.append(bonusConfigDict)
//                println_debug(bonusConfigListArray)
                if bonusConfigDict["resultdescription"] as? String == "Transaction Successful" {
                    DispatchQueue.main.async {
                        self.removeProgressView()
                        let message = "Bonus Point Configured Successfully".localized
                        loyaltyAlertView.wrapAlert(title: nil, body: message, img: #imageLiteral(resourceName: "discount_money"))
                        loyaltyAlertView.addAction(title: "OK".localized, style: .target , action: {
                            self.dismissAndCallDisplayConfig()
                        })
                        loyaltyAlertView.showAlert(controller: self)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.removeProgressView()
                        let message = self.bonusConfigDict["resultdescription"] as? String ?? ""
                        loyaltyAlertView.wrapAlert(title: nil, body: message.localized, img: #imageLiteral(resourceName: "error"))
                        loyaltyAlertView.addAction(title: "OK".localized, style: .target , action: {
                            
                        })
                        loyaltyAlertView.showAlert(controller: self)
                    }
                }
            }
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            bonusConfigDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func webFailureResult(screen: String) {
        println_debug("Failure")
    }
}


extension AddNewBonusConfigViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
       /* if let label = textField.viewWithTag(textField.tag) as? UILabel {
            label.isHidden = false
            label.textColor = UIColor.blue
        } else {
            textField.addPlaceHolderView(tag: textField.tag)
        } */
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        for (_,label) in (textField.subviews.filter{$0 is UILabel}).enumerated(){
            if let label1 = label as? UILabel {
                label1.textColor = UIColor.lightGray
            }
        }
        hideViews(textField, count: textField.text?.count ?? 0)
    }
    
    private func updateView(textField: UITextField, count: Int) {
        if textField == maxRangeTxt {
            minRangeTxt.placeholder = "Enter minimum amount range".localized
            self.perAmountTxt.placeholder = "Enter per amount".localized
            self.bonusPointsTxt.placeholder = "Enter bonus point".localized
            minRangeTxt.text = ""
            perAmountTxt.text = ""
            bonusPointsTxt.text = ""
            if let clearButton = self.view.viewWithTag(142) as? UIButton {
                clearButton.isHidden = true
            }
            if let clearButton = self.view.viewWithTag(143) as? UIButton {
                clearButton.isHidden = true
            }
            if let clearButton = self.view.viewWithTag(144) as? UIButton {
                clearButton.isHidden = true
            }
            let startTag = 123
            for viewLocal in topView.subviews {
                if viewLocal.tag >= startTag {
                    viewLocal.isHidden = true
                }
            }
        }
        else if textField == minRangeTxt {
            self.perAmountTxt.placeholder = "Enter per amount".localized
            self.bonusPointsTxt.placeholder = "Enter bonus point".localized
            perAmountTxt.text = ""
            bonusPointsTxt.text = ""
            if let clearButton = self.view.viewWithTag(143) as? UIButton {
                clearButton.isHidden = true
            }
            if let clearButton = self.view.viewWithTag(144) as? UIButton {
                clearButton.isHidden = true
            }
            let startTag = 123
            for viewLocal in topView.subviews {
                if viewLocal.tag >= startTag {
                    viewLocal.isHidden = true
                }
            }
        }
        else if textField == perAmountTxt {
            self.bonusPointsTxt.placeholder = "Enter bonus point".localized
            bonusPointsTxt.text = ""
            if let clearButton = self.view.viewWithTag(144) as? UIButton {
                clearButton.isHidden = true
            }
            let startTag = 124
            for viewLocal in topView.subviews {
                if viewLocal.tag >= startTag {
                        viewLocal.isHidden = true
                    }
            }
        } else if textField == bonusPointsTxt {
            self.startDateTxt.text = ""
            self.endDateTxt.text = ""
            let viewLocal = self.view.viewWithTag(126)
            viewLocal?.isHidden = true
            let viewLocal1 = self.view.viewWithTag(127)
            viewLocal1?.isHidden = true
        }
        self.hideViews(textField, count: count)
        self.createBtn.isHidden = true
    }
    
   
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if textField != minRangeTxt, range.location == 0, string == "0" {
            return false
        }
        let textCount  = text.count
        self.showClearButton(tag : textField.tag, text: textCount)
        self.updateView(textField: textField, count: textCount)
        if textCount > 0 {
            if textField.tag >= 132 {
                let viewLocal = self.view.viewWithTag(textField.tag - 9)
                viewLocal?.isHidden = false
                if let textFld = viewLocal?.viewWithTag(textField.tag + 1) as? UITextField {
                    textFld.text = ""
                }
            }
            if textField == maxRangeTxt {
                
                if textCount > 13 {
                    return false
                }
            } else if textField == minRangeTxt {
                if textCount > 14 {
                    return false
                }
            } else  if textField == perAmountTxt {
                if text == "0" {
                    return false
                }
                if textCount > 13 {
                    return false
                }
            } else if textField == bonusPointsTxt {
                if text == "0" {
                    return false
                }
                if textCount > 13 {
                    return false
                }
            }
            
        }
        self.updatePlaceHolderText(textfield: textField, count: textCount)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        hideViews(textField, count: textField.text?.count ?? 0)
        return true
    }
    
    func hideViews(_ textField : UITextField, count: Int) {
        if count == 0 {
            if let label = textField.viewWithTag(textField.tag) as? UILabel {
                label.isHidden = true
                label.textColor = UIColor.lightGray
            }
            var tag = textField.tag
            tag -= 9
            if textField.tag >= 132 {
                for index in tag...128 {
                    let view = self.view.viewWithTag(index)
                    view?.isHidden = true
                }
            }
            self.createBtn.isHidden = true
        }
    }
}


extension AddNewBonusConfigViewController : loyaltyDateSelectForSort {
    func okButtonAction(fromDate : Date, toDate : Date) {
        self.startSelDate = fromDate
        self.endSelDate = toDate
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss SSS"
        var toDateStr = dateFormat.string(from: toDate)
        toDateStr = toDateStr.components(separatedBy: " ").first ?? ""
        toDateStr = "\(toDateStr) 23:59:59"
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let toDateConverted = dateFormat.date(from: toDateStr)
        
        let viewLocal = self.view.viewWithTag(126)
        viewLocal?.isHidden = false
        let viewLocal1 = self.view.viewWithTag(127)
        viewLocal1?.isHidden = false
        for (_,label) in (startDateTxt.subviews.filter{$0 is UILabel}).enumerated(){
            if let label1 = label as? UILabel {
                label1.textColor = UIColor.lightGray
            }
        }
        for (_,label) in (endDateTxt.subviews.filter{$0 is UILabel}).enumerated(){
            if let label1 = label as? UILabel {
                label1.textColor = UIColor.lightGray
            }
        }
        self.createBtn.isHidden = false
        self.startDateTxt.text = self.fomatter.string(from: fromDate)
        self.endDateTxt.text = self.fomatter.string(from: toDateConverted ?? toDate)
        let calendar = Calendar.current
        let componentsDay = calendar.dateComponents([.day], from: startDate, to: toDateConverted ?? toDate).day ?? 0
        if componentsDay > 0 {
            expiryDaysTxt.text = String(componentsDay + 1)
        } else {
            expiryDaysTxt.text = "1"
        }
    }
    
    func resetButtonAction() {
        self.startDateTxt.text = ""
        self.endDateTxt.text = ""
    }
}
