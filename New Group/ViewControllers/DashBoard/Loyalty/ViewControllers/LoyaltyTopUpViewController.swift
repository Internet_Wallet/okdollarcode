//
//  LoyaltyTopUpViewController.swift
//  OK
//
//  Created by Kethan on 3/3/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class LoyaltyTopUpViewController: LoyaltyBaseViewController, BioMetricLoginDelegate, PTWebResponseDelegate {
    
    @IBOutlet weak var availablePointLblText: UILabel! {
        didSet {
            self.availablePointLblText.font = UIFont(name: appFont, size: appFontSize)
            self.availablePointLblText.text = self.availablePointLblText.text?.localized
        }
    }
    @IBOutlet weak var availablePointLbl : UILabel!{
        didSet{
            self.availablePointLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var pointTxt : UITextField! {
        didSet {
            self.pointTxt.font = UIFont(name: appFont, size: appFontSize)
            self.pointTxt.placeholder =  "Enter Points".localized
        }
    }
    @IBOutlet weak var payButton : UIButton! {
        didSet {
            self.payButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.payButton.isHidden = true
        }
    }
    @IBOutlet weak var clearButton : UIButton! {
        didSet {
            self.clearButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.clearButton.isHidden = true
        }
    }
    
    var availablePoint = "0"
    weak var delegate : pointDistributionProtocol?
    var loyaltyTopUpListArray = [Dictionary<String,Any>]()
    var loyaltyTopUpDict = Dictionary<String,Any>()
    
    //AccessoryView
    var submitView: UIView {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 50))
        let btn = UIButton.init(frame: view.frame)
        btn.backgroundColor = UIColor.red
        btn.setTitle("Pay".localized, for: .normal)
        if let myFont = UIFont(name: appFont, size: 18) {
            btn.titleLabel?.font =  myFont
        }
        btn.addTarget(self, action: #selector(payButtonAction(_:)), for: UIControl.Event.touchUpInside)
        view.addSubview(btn)
        return view
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        addCustomBackButton(navTitle : "Loyalty Top Up")
        self.availablePointLbl.text = availablePoint.replacingOccurrences(of: ".00", with: "").replacingOccurrences(of: ".0", with: "")
        pointTxt.inputAccessoryView = self.submitView
        pointTxt.inputAccessoryView?.isHidden = true
        // Do any additional setup after loading the view.
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
        pointTxt.addTarget(self, action: #selector(ResaleMyNumViewController.textFieldDidChange(_:)),
                            for: UIControl.Event.editingChanged)
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func showAccessoryView(status : Bool) {
        if status {
            pointTxt.inputAccessoryView?.isHidden = false
        } else {
            pointTxt.inputAccessoryView?.isHidden = true
        }
    }
    
    @IBAction func payButtonAction(_ sender : UIButton) {
        submitLoyaltyTopUp()
    }
    
    private func  updateAvailablePoints(points : String) {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        if let numberStr = formatter.string(from: Float(points)! as NSNumber) {
            self.availablePointLbl.text = numberStr
        }
        self.delegate?.currentPoint(pointText: self.availablePointLbl.text!)
    }
    
    private func submitLoyaltyTopUp() {
        if (pointTxt.text?.count ?? 0) > 0 {
            if appDelegate.checkNetworkAvail() {
                let web      = PayToWebApi()
                web.delegate = self
                var ip = ""
                if let ipAdd = OKBaseController.getIPAddress() {
                    ip = ipAdd
                }
                let poinsNum = pointTxt.text?.replacingOccurrences(of: ",", with: "") ?? "0"
                let urlStr   = String.init(format: Url.loyaltyTopUpApiUrl, UserModel.shared.mobileNo, poinsNum, ok_password ?? "", ip , "ios", UserLogin.shared.token)
                let url = getUrl(urlStr: urlStr, serverType: .serverEstel)
                //            println_debug(url)
                let pRam = Dictionary<String,String>()
                web.genericClassXML(url: url, param: pRam as AnyObject, httpMethod: "POST", mScreen: "mLoyaltyList")
            } else {
                self.noInternetAlert()
            }
        } else {
            loyaltyAlertView.wrapAlert(title: nil, body: "Please enter points".localized, img: #imageLiteral(resourceName: "discount_money"))
            loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                
            })
            loyaltyAlertView.showAlert(controller: self)
        }
    }
    
    //PTWebresponseDelegate
    func webSuccessResult(data: Any, screen: String) {
//        println_debug(data)
//        println_debug(screen)
        
        if let xmlString = data as? String {
            let xml = SWXMLHash.parse(xmlString)
            loyaltyTopUpDict.removeAll()
            self.enumerate(indexer: xml)
            self.loyaltyTopUpListArray.removeAll()
            self.loyaltyTopUpListArray.append(loyaltyTopUpDict)
//            println_debug(loyaltyTopUpListArray)
            if loyaltyTopUpDict["resultdescription"] as? String == "Transaction Successful" {
                DispatchQueue.main.async {
                    self.removeProgressView()
                    self.updateAvailablePoints(points: self.loyaltyTopUpDict["walletbalance"] as? String ?? "0.0")
                    loyaltyAlertView.wrapAlert(title: nil, body: "Transaction Successful".localized, img: #imageLiteral(resourceName: "discount_money"))
                    loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                        self.navigationController?.popViewController(animated: true)
                    })
                    loyaltyAlertView.showAlert(controller: self)
                }
            } else if loyaltyTopUpDict["resultdescription"] as? String == "Invalid Secure Token" {
                DispatchQueue.main.async {
                    self.removeProgressView()
                    loyaltyAlertView.wrapAlert(title: nil, body: "Login to continue".localized, img: #imageLiteral(resourceName: "alert-icon"))
                    loyaltyAlertView.addAction(title: "OK".localized, style: .target , action: {
                        OKPayment.main.authenticate(screenName: "LoyaltyTopUp", delegate: self)
                    })
                    loyaltyAlertView.showAlert(controller: self)
                }
            } else if loyaltyTopUpDict["resultdescription"] as? String == "Wallet Not Found" {
                DispatchQueue.main.async {
                    self.removeProgressView()
                    //loyaltyAlertView.wrapAlert(title: nil, body: "Wallet Not Found".localized, img: #imageLiteral(resourceName: "alert-icon"))
                    loyaltyAlertView.wrapAlert(title: nil, body: "Your business is registered with\n loyalty program but not approve yet.\n Please contact to OK$ support.".localized, img: #imageLiteral(resourceName: "alert-icon"))
                    loyaltyAlertView.addAction(title: "OK".localized, style: .target , action: {
                        
                    })
                    loyaltyAlertView.showAlert(controller: self)
                }
            } else {
                DispatchQueue.main.async {
                    self.removeProgressView()
                }
            }
        }
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful {
            if screen == "LoyaltyTopUp" {
                submitLoyaltyTopUp()
            }
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            loyaltyTopUpDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func webFailureResult(screen: String) {
        println_debug("Failure")
    }
    
    @IBAction func clearButtonAction(_ sender : UIButton) {
        self.showAccessoryView(status : false)
        sender.isHidden = true
        self.payButton.isHidden = true
        pointTxt.text = ""
        pointTxt.placeholder = "Enter Point".localized
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        let dataWithoutComma = self.removeCommaFromDigit(textField.text!)
        pointTxt.text = self.getDigitDisplay(dataWithoutComma)
    }

}

extension LoyaltyTopUpViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if text.count > 12 {
            return false
        }
        if range.location == 0 && string == "0" {
            return false
        }
        if text.count > 0 {
            textField.placeholder = "Points".localized
            self.showAccessoryView(status : true)
            self.clearButton.isHidden = false
            self.payButton.isHidden = false
        } else {
            textField.placeholder = "Enter Points".localized
            self.showAccessoryView(status : false)
            self.clearButton.isHidden = true
            self.payButton.isHidden = true
        }
        return true
    }
}
