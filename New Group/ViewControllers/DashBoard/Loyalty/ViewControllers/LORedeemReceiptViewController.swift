
//  LORedeemReceiptViewController.swift
//  OK
//
//  Created by Kethan on 8/10/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

class LORedeemReceiptViewController: LoyaltyBaseViewController, UITabBarDelegate, CNContactViewControllerDelegate, MoreControllerActionDelegate, PaymentRateDelegate {
    
    @IBOutlet weak var mobileNumber: UILabel!{
        didSet{
            self.mobileNumber.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var amount: UILabel!{
        didSet{
            self.amount.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var keysenderAmount: UILabel!{
        didSet{
            self.keysenderAmount.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var valuesenderAmount: UILabel!{
        didSet{
            self.valuesenderAmount.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var keyMerchantName: UILabel!{
        didSet{
            self.keyMerchantName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var valueMerchantName: UILabel!{
        didSet{
            self.valueMerchantName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var keyTransactionID: UILabel!
    {
        didSet
        {
            keyTransactionID.font = UIFont(name: appFont, size: appFontSize)
            keyTransactionID.text = "Transaction ID".localized
        }
    }
    @IBOutlet weak var valueTransactionID: UILabel!{
        didSet{
            self.valueTransactionID.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var keyTransactionType: UILabel!{
        didSet{
            self.keyTransactionType.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var valueTransactionType: UILabel!{
        didSet{
            self.valueTransactionType.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var valueDate: UILabel!{
        didSet{
            self.valueDate.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var valueTime: UILabel!{
        didSet{
            self.valueTime.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var heightDateConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var rateservice: UILabel!
        {
        didSet
        {
            self.rateservice.font = UIFont(name: appFont, size: appFontSize)
            rateservice.text = "Rate OK$ Service".localized
        }
    }
    @IBOutlet weak var tabBar: UITabBar!
    
    @IBOutlet weak var tabHome : UITabBarItem!{
        didSet{
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: appFont, size: appFontSize) ?? 12], for: .normal)
        }
    }
    @IBOutlet weak var tabMore : UITabBarItem!{
        didSet{
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: appFont, size: appFontSize) ?? 12], for: .normal)
        }
    }
    @IBOutlet weak var tabContact : UITabBarItem!{
        didSet{
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: appFont, size: appFontSize) ?? 12], for: .normal)
        }
    }
    @IBOutlet weak var tabfav: UITabBarItem!{
        didSet{
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: appFont, size: appFontSize) ?? 12], for: .normal)
        }
    }
    
    @IBOutlet weak var rateBtn: UIButton!{
        didSet{
            rateBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    @IBOutlet weak var cardView: CardDesignView!
    
    var reciptModel : loyaltyRecieptModel?
    
    var categoryValue : RecipetPlanCases?
    
    var tabBarAction : RecieptTabSelectionType?
    
    var planType : String?
    
    var InternationalTopup : String?
    
    var currencyInt : String?
    
    var contactAdded : Bool = false
    
    @IBOutlet  var starArray: [UIImageView]!
    var shopName: String = ""
    let optionTextDefaultColor = UIColor.init(hex: "162D9F")
    @IBOutlet weak var viewTab: UIView!
    var viewOptions: TabOptionsOnReceipt?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            self.changeCancelImplementation()
        }
        let isInCont = checkForContact(reciptModel?.receiverNumber.safelyWrappingString() ?? "").isInContact
        let isInFav = checkForFavorites(reciptModel?.receiverNumber.safelyWrappingString() ?? "", type: "PAYTO").isInFavorite
        guard let bView = Bundle.main.loadNibNamed("TabOptionsOnReceipt", owner: self, options: nil)?.first as? TabOptionsOnReceipt else { return }
        viewOptions = bView
        bView.delegate = self
        viewOptions?.frame = CGRect(x: 0, y: 0, width: self.viewTab.frame.size.width, height: viewTab.frame.size.height)
        if let _ = viewOptions {
            viewTab.addSubview(viewOptions!)
            viewOptions!.lblHome.text = "HomePagePT".localized
            viewOptions!.lblMore.text = "More".localized
            viewOptions!.lblAddContact.text = "TabbarAddContact".localized
            viewOptions!.lblAddFav.text = "Add Favorite".localized
            if isInCont == true {
                addContactUI(isInContact: true)
            }
            if isInFav == true {
                addFavUI(isInFav: true)
            }
            viewOptions!.layoutIfNeeded()
        }
        self.uiUpdates()
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: kYellowColor, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .normal)
        UITabBar.appearance().tintColor = .lightGray
        UITabBar.appearance().barTintColor = .white
        UITabBar.appearance().unselectedItemTintColor = .lightGray
    }
    
    func addFavUI(isInFav: Bool) {
        if let _ = viewOptions {
            viewOptions?.imgAddFav.image = viewOptions?.imgAddFav.image?.withRenderingMode(.alwaysTemplate)
            if isInFav {
                viewOptions?.imgAddFav.tintColor = kYellowColor
                viewOptions!.lblAddFav.textColor = kYellowColor
            } else {
                viewOptions?.imgAddFav.tintColor = UIColor.gray
                viewOptions!.lblAddFav.textColor = optionTextDefaultColor
            }
            viewOptions!.layoutIfNeeded()
        }
    }
    
    func addContactUI(isInContact: Bool) {
        if let _ = viewOptions {
            viewOptions?.imgAddContact.image = viewOptions?.imgAddContact.image?.withRenderingMode(.alwaysTemplate)
            if isInContact {
                viewOptions?.imgAddContact.tintColor = kYellowColor
                viewOptions!.lblAddContact.textColor = kYellowColor
            } else {
                viewOptions?.imgAddContact.tintColor = UIColor.gray
                viewOptions!.lblAddContact.textColor = optionTextDefaultColor
            }
            viewOptions!.layoutIfNeeded()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Receipt".localized
        self.navigationController?.navigationBar.titleTextAttributes = PaytoConstants.global.attributesForTitle()

        //        [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"AmericanTypewriter" size:20.0f]} forState:UIControlStateNormal];
        //let color = UIColor.lightGray
        //let attrFont = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.0) ?? UIFont.systemFont(ofSize: 11.0), NSAttributedString.Key.foregroundColor: color]
        
//        self.tabHome.setTitleTextAttributes(attrFont, for: .normal)
//        self.tabMore.setTitleTextAttributes(attrFont, for: .normal)
//        self.tabContact.setTitleTextAttributes(attrFont, for: .normal)
//        self.tabfav.setTitleTextAttributes(attrFont, for: .normal)
//        self.tabfav.title = "Add Favorite".localized
//
//        self.tabHome.title = "Home Page".localized
//        self.tabMore.title = "More".localized
//        self.tabContact.title = "TabbarAddContact".localized
//        self.rateservice.text = "Rate OK$ Service".localized
        
        //self.navigationController?.navigationBar.backgroundColor = MyNumberTopup.OperatorColorCode.okDefault
        //self.navigationController?.navigationBar.tintColor = MyNumberTopup.OperatorColorCode.okDefault
        navigationController?.navigationBar.barTintColor = kYellowColor
        self.navigationItem.setHidesBackButton(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        //navigationController?.navigationBar.barTintColor = UIColor.red
    }
    
    //MARK:- Initial UI Updates
    private func uiUpdates() {
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.keyMerchantName.text = self.keyMerchantName.text.safelyWrappingString().localized
        self.keysenderAmount.text = self.keysenderAmount.text.safelyWrappingString().localized
        //self.keyTransactionID.text = self.keyTransactionID.text.safelyWrappingString().localized
        self.keyTransactionType.text = self.keyTransactionType.text.safelyWrappingString().localized
        self.valueDate.text = self.valueDate.text.safelyWrappingString().localized
        self.valueTime.text = self.valueTime.text.safelyWrappingString().localized
        
        guard let model = reciptModel else { return }
        self.valueMerchantName.text = shopName
        self.valueTransactionID.text = model.transactionID.safelyWrappingString()
        
        self.amount.text = self.getDigitDisplay(model.amount.safelyWrappingString().replacingOccurrences(of: ".00", with: "").replacingOccurrences(of: ".0", with: ""))//model.amount.safelyWrappingString()
        
        if let date = model.transactionDate {
            let dateFormat = date.components(separatedBy: " ").first?.dateValue(dateFormatIs: "dd-MMM-yyyy")
            let dateKey = (dateFormat?.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy"))!
            self.valueDate.text = dateKey
            
            let timeKey = date.components(separatedBy: " ").last ?? ""
            self.valueTime.text = timeKey
        }
        
//        let preciseTime  = model.transactionDate.safelyWrappingString().components(separatedBy: " ")
//        self.valueDate.text = preciseTime.first.safelyWrappingString()
//        self.valueTime.text = preciseTime.last.safelyWrappingString()
        self.mobileNumber.text = wrapFormattedNumber(model.receiverNumber.safelyWrappingString())
        self.valuesenderAmount.text = "BONUS"
        self.valueTransactionType.text = "LOYALTYREDEEM"
        self.navigationController?.navigationBar.backgroundColor = MyNumberTopup.OperatorColorCode.okDefault
        self.navigationController?.navigationBar.tintColor = MyNumberTopup.OperatorColorCode.okDefault
    }
  
    
    //MARK:- TAB BAR DELEGATE
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        tabBarAction = RecieptTabSelectionType(rawValue: item.tag)
        
        guard let action = tabBarAction else { return }
        
        guard let modelRec = reciptModel else { return }
        
        switch action {
        case .addFavorite:
            self.addFav(model: modelRec)
        case .addContact:
            self.addContacts(model: modelRec)
        case .home:
            self.goToHome()
        case .more:
            self.more()
        }
        
    }
    
    //MARK:- ADD FAVORITE
    private func addFav(model: loyaltyRecieptModel) {
        let favVC = self.addFavoriteController(withName: "Unknown", favNum: model.receiverNumber.safelyWrappingString(), type: "PAYTO", amount: model.amount.safelyWrappingString())
        if let vc = favVC {
            vc.delegate = self
            self.navigationController?.present(vc, animated: false, completion: nil)
        }
    }
    
    //MARK:- ADD CONTACT
    private func addContacts(model: loyaltyRecieptModel) {
        let store     = CNContactStore()
        let contact   = CNMutableContact()
        let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue : wrapFormattedNumber(model.receiverNumber.safelyWrappingString()) ))
        contact.phoneNumbers = [homePhone]
        contact.givenName = model.recieverName.safelyWrappingString()
        let controller = CNContactViewController.init(forNewContact: contact)
        controller.contactStore = store
        controller.delegate     = self
        controller.title        = "Add Contact".localized
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.backgroundColor = UIColor.init(hex: "F3C632")
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    // contact delegate
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        if let newContact = contact {
            self.addContactUI(isInContact: true)
            okContacts.append(newContact)
        }
        viewController.delegate = nil
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- HOME
    private func goToHome() {
        // self.navigationController?.popToRootViewController(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DismissLoyaltyView"), object: nil, userInfo: nil)
    }
    
    //MARK:- MORE
    private func more() {
        guard  let vc = UIStoryboard.init(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PTMoreViewController") as? PTMoreViewController else {return}
        addChild(vc)
        vc.isFromEarnPointTrf = true
        vc.isFromTopup = false
        vc.view.frame = self.view.bounds
        vc.didMove(toParent: self)
        vc.delegate = self
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        
        vc.view.layer.add(transition, forKey: nil)
        view.addSubview(vc.view)
    }
    
    func showBonusPointRedeemViewController() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "PopFromReceipt"), object: nil)
        self.navigationController?.popToRootViewController(animated: true)
//        if let redeemVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: loyaltyMainViews.viewControllers.discountMoneyVC) as? DiscountMoneyHomeViewController {
//            self.navigationController?.pushViewController(redeemVC, animated: true)
//        }
    }
    
    // more delegate callback
    func actionFor(option: MorePaymentAction) {
        switch option {
        case .morePayment:
            self.showBonusPointRedeemViewController()
        case .repeatPayment:
            self.showBonusPointRedeemViewController()
        case .invoice:
            self.invoice()
        case .share:
            self.share()
        }
    }
    
    private func getDateFormat(date: String) -> String {
        let dateFormat = date
        let dateF = DateFormatter()
        dateF.dateFormat = "dd-MMM-yyyy HH:mm:ss"
        guard let dateLoc = dateF.date(from: date) else { return dateFormat }
        dateF.dateFormat = "EEE, dd-MMM-yyyy HH:mm:ss"
        return dateF.string(from: dateLoc)
    }
    
    private func invoice() {
        
        guard let model = reciptModel else { return }
        
        var pdfDictionary = Dictionary<String,Any>()
        pdfDictionary["senderAccName"]      = UserModel.shared.name
        
        let number = "(\(UserModel.shared.countryCode))\(UserModel.shared.formattedNumber)"
        let mobileNum = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: model.receiverNumber.safelyWrappingString())
        let destination = "(\(mobileNum.counrtycode))\(mobileNum.mobileNo)"
        
        pdfDictionary["invoiceTitle"]       = "Transaction Receipt".localized
        pdfDictionary["senderAccNo"]        = number
        pdfDictionary["receiverAccNo"]       = destination
        pdfDictionary["transactionID"]      = model.transactionID
        pdfDictionary["transactionType"]    = "LOYALTYREDEEM"
        let pdfAmount = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: model.amount.safelyWrappingString()) + " Points"
        pdfDictionary["amount"]             = pdfAmount
        //pdfDictionary["amount"]             = model.amount.safelyWrappingString()
        //pdfDictionary["cashback"]           = model.kickvalue.safelyWrappingString()
        let dateAndTime = self.getDateFormat(date: model.transactionDate.safelyWrappingString())
        pdfDictionary["transactionDate"]    = dateAndTime
        pdfDictionary["logoName"]           = "appIcon_Ok"
        pdfDictionary["loyaltyType"]           = "RedeemPoints"
        
//        let lat  = geoLocManager.currentLatitude ?? ""
//        let long = geoLocManager.currentLongitude ?? ""
//        var name = ""
//        if UserModel.shared.agentType == .user {
//            name = UserModel.shared.name
//        } else {
//            name = UserModel.shared.businessName
//        }
        
//        let firstPart  = "00#" + "\(name)" + "-" + "\(UserModel.shared.mobileNo) " + "@" + pdfAmount + "&"
//        let secondPart =  "\(pdfDictionary["loyaltypoints"] ?? "")" + "β" + "\(lat)" + "γ" + "\(long)" + "α" + "\(pdfDictionary["transactionID"] ?? "")" + "" + "`,,"
//
//        let finalPart = String.init(format:"%@%@", firstPart,secondPart)
//
//        guard let hashedQRKey = AESCrypt.encrypt(finalPart, password: "m2n1shlko@$p##d") else { return }
        
//        let qrObject = PTQRGenerator.init()
//        pdfDictionary["qrImage"] = qrObject.getQRImage(stringQR: hashedQRKey, withSize: 10)
        
        guard let pdfUrl = CodeSnippets.createInvoicePDF(dictValues: pdfDictionary, pdfFile: "OK$ loyality Redeem Receipt") else {
            println_debug("Error - pdf not generated")
            return
        }
        
        guard let vc = UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: "SharePDFViewController") as? SharePDFViewController else { return }
        vc.url = pdfUrl
        navigationController?.navigationBar.barTintColor = kYellowColor
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func share() {        
        self.rateservice.isHidden = true
        for star in starArray {
            star.isHidden = true
        }
        let snapImage = self.cardView.snapshotOfCustomeView
        self.rateservice.isHidden = false
        for star in starArray {
            star.isHidden = false
        }
        let vc = UIActivityViewController(activityItems: [snapImage], applicationActivities: [])
        DispatchQueue.main.async {self.present(vc, animated: true)}
    }
    
    private func screenShotMethod(_ view: UIView) -> UIImage? {
        //Create the UIImage
        UIGraphicsBeginImageContext(view.frame.size)
        
        if let context = UIGraphicsGetCurrentContext() {
            view.layer.render(in: context)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            if let imageFinal = image {
                UIImageWriteToSavedPhotosAlbum(imageFinal, nil, nil, nil)
            }
            return image
        }
        
        return nil
    }
    
    
    //MARK:- Rating Functionality
    @IBAction func openRateWindowAction(_ sender: UIButton) {
        guard let model = reciptModel else { return }
        guard let ratingScreen = self.storyboard?.instantiateViewController(withIdentifier: "LORedeemRateViewController_ID") as? LORedeemRateViewController else { return }
        ratingScreen.modalPresentationStyle = .overCurrentContext
        ratingScreen.delegate = self
        navigationController?.navigationBar.barTintColor = UIColor.red
        ratingScreen.destinationNumber = model.receiverNumber.safelyWrappingString()
        self.navigationController?.present(ratingScreen, animated: true, completion: nil)
    }
    
    func ratingShow(_ rate: Int, commentText: String?) {
        DispatchQueue.main.async {
            self.rateBtn.isUserInteractionEnabled = false

            for star in self.starArray {
                if star.tag <= rate {
                    UIView.animate(withDuration: 0.2, animations: {
                        star.image = #imageLiteral(resourceName: "receiptStarFilled")
                    })
                } else {
                    star.image = #imageLiteral(resourceName: "receiptStarUnfilled")
                }
            }
        }
    }
    
    func changeCancelImplementation() {
        let originalSelector = Selector(("editCancel:"))
        let swizzledSelector = #selector(CNContactViewController.cancelHack)
        if let originalMethod = class_getInstanceMethod(object_getClass(CNContactViewController()), originalSelector),
           let swizzledMethod = class_getInstanceMethod(object_getClass(CNContactViewController()), swizzledSelector) {
            method_exchangeImplementations(originalMethod, swizzledMethod)
        }
    }
    
}


//MARK:- TabOptionProtocol
extension LORedeemReceiptViewController: TabOptionProtocol {
    func addFavorite() {
        if let _ = viewOptions {
            if viewOptions!.lblAddFav.textColor != kYellowColor {
                guard let modelRec = reciptModel else { return }
                self.addFav(model: modelRec)
            } else {
                PaytoConstants.alert.showToast(msg: "Contact already added in Favorites".localized)
            }
        }
    }
    
    func addContact() {
        if let _ = viewOptions {
            if viewOptions!.lblAddContact.textColor != kYellowColor {
                guard let modelRec = reciptModel else { return }
                self.addContacts(model: modelRec)
            } else {
                self.showErrorAlert(errMessage: "Contact Already Added".localized)
            }
        }
    }
    
    func goHome() {
        self.goToHome()
    }
    
    func showMore() {
        self.more()
    }
    
}

extension LORedeemReceiptViewController: PTFavoriteActionDelegate {
    func favoriteAdded() {
        addFavUI(isInFav: true)
    }
}
