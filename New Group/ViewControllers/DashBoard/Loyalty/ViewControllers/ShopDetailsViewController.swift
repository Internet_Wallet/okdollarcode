//
//  ShopDetailsViewController.swift
//  OK
//
//  Created by Kethan on 3/3/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Rabbit_Swift

class ShopDetailsViewController: LoyaltyBaseViewController {
    
    @IBOutlet weak var shopNameLbl : UILabel! {
        didSet {
            self.shopNameLbl.font = UIFont(name: appFont, size: appFontSize)
            self.shopNameLbl.text = self.shopNameLbl.text?.localized
        }
    }
    @IBOutlet weak var mobNumberLbl : UILabel! {
        didSet {
            self.mobNumberLbl.font = UIFont(name: appFont, size: appFontSize)
            self.mobNumberLbl.text = self.mobNumberLbl.text?.localized
        }
    }
    @IBOutlet weak var shopOpnDaysLbl : UILabel! {
        didSet {
            self.shopOpnDaysLbl.font = UIFont(name: appFont, size: appFontSize)
            self.shopOpnDaysLbl.text = self.shopOpnDaysLbl.text?.localized
        }
    }
    @IBOutlet weak var workingHoursLbl : UILabel! {
        didSet {
            self.workingHoursLbl.font = UIFont(name: appFont, size: appFontSize)
            self.workingHoursLbl.text = self.workingHoursLbl.text?.localized
        }
    }
    @IBOutlet weak var emailLbl : UILabel! {
        didSet {
            self.emailLbl.font = UIFont(name: appFont, size: appFontSize)
            self.emailLbl.text = self.emailLbl.text?.localized
        }
    }
    @IBOutlet weak var facebookLbl : UILabel! {
        didSet {
            self.facebookLbl.font = UIFont(name: appFont, size: appFontSize)
            self.facebookLbl.text = self.facebookLbl.text?.localized
        }
    }
    @IBOutlet weak var webSiteLbl : UILabel! {
        didSet {
            self.webSiteLbl.font = UIFont(name: appFont, size: appFontSize)
            self.webSiteLbl.text = self.webSiteLbl.text?.localized
        }
    }
    
    @IBOutlet weak var shopNameVal : UILabel!{
        didSet{
            self.shopNameVal.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var mobNumberVal : UILabel!{
        didSet{
            self.mobNumberVal.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var shopOpnDaysVal : UILabel!{
        didSet{
            self.shopOpnDaysVal.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var workingHoursVal : UILabel!{
        didSet{
            self.workingHoursVal.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var emailVal : UILabel!{
        didSet{
            self.emailVal.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var facebookVal : UILabel!{
        didSet{
            self.facebookVal.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var webSiteVal : UILabel!{
        didSet{
            self.webSiteVal.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var facebookView : UIView!
    @IBOutlet weak var webSiteView : UIView!
    
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getBusinessDetailsData()
        addCustomBackButton(navTitle : "Shop Details")
        
        
        let shopName = userDef.string(forKey: "shop_name")
          if shopName == "" {
              if UserModel.shared.businessName != "" {
              shopNameVal.text = (ok_default_language == "my") ? Rabbit.uni2zg(UserModel.shared.businessName) : UserModel.shared.businessName
              }
          }else {
              if  shopName != nil {
                  shopNameVal.text = (ok_default_language == "my") ? Rabbit.uni2zg(shopName ?? "") : shopName
              }else {
                  shopNameVal.text = (ok_default_language == "my") ? Rabbit.uni2zg(UserModel.shared.businessName) : UserModel.shared.businessName
              }
          
          }
        
       // self.shopNameVal.text = (ok_default_language == "my") ? Rabbit.uni2zg(UserModel.shared.businessName) : UserModel.shared.businessName
        self.mobNumberVal.text = UserModel.shared.formattedNumber
//        self.shopOpnDaysVal.text = "OpenAllDays".localized
//        if UserModel.shared.openTime == "00:00:00" && UserModel.shared.closeTime == "00:00:00" {
//            self.workingHoursVal.text = "24 Hrs"
//        } else {
//            self.workingHoursVal.text = "\(self.get12FormatTime(timeStr: UserModel.shared.openTime)) - \(self.get12FormatTime(timeStr: UserModel.shared.closeTime))"
//        }
        self.emailVal.text = UserModel.shared.email.components(separatedBy: ",").first ?? ""
        self.facebookVal.text = UserModel.shared.fbEmailId
        self.webSiteVal.text =  UserModel.shared.website
        
        if UserModel.shared.website.length == 0 {
            webSiteView.isHidden = true
        }
        
        if UserModel.shared.fbEmailId.length == 0 {
            facebookView.isHidden = true
            webSiteView.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func getBusinessDetailsData() {
        if appDelegate.checkNetworkAvail() {
            self.showProgressView()
            let paramString = self.loginInfoDictionaryWithOTP() as! [String : Any]
            let web      = WebApiClass()
            web.delegate = self
            let urlStr = Url.getBusinessDetails
            // let ur = URL(string: "http://69.160.4.151:8085/UpdateProfile.svc/GetMerchantBusinessInfoAndDocs") //
            let ur = getUrl(urlStr: urlStr, serverType: .updateProfile)
            println_debug(ur)
            println_debug(paramString)
            web.genericClass(url: ur, param: paramString as AnyObject, httpMethod: "POST", mScreen: "GetUserBusinessInfo")
        }
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func updateUI(dict: [String: Any]) {
        DispatchQueue.main.async {
       
        if let dayTime = dict["WorkingDayAndTime"] as? [Dictionary<String,Any>] {
            var days = ""
            let date = Date().stringValue(dateFormatIs: "EEE").uppercased()
            var openClosedTime = ""
            for day in dayTime {
                if date == day["DayCode"] as? String ?? "" {
                    openClosedTime = (day["OpenTime"] as? String ?? "") + " - " + (day["CloseTime"] as? String ?? "")
                }
                days = "\(days)\(day["DayCode"] as? String ?? ""),"
            }
            var daysToShow = ""
            if days.count > 0 {
               daysToShow = "\(days.dropLast())"
            }
            if daysToShow.count > 0 {
                self.shopOpnDaysVal.text = daysToShow
                self.workingHoursVal.text = (openClosedTime.count > 0) ? openClosedTime : "Closed".localized
            } else {
                self.shopOpnDaysVal.text = "OpenAllDays".localized
                self.workingHoursVal.text = "24 Hrs"
            }
            
        }
    }
    }
    
}


extension ShopDetailsViewController: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        self.removeProgressView()
        if screen == "GetUserBusinessInfo" {
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,Any> {
                        if dic["Code"] as? Int == 200 {
                            if let data = dic["Data"]  {
                                if let dic = OKBaseController.convertToDictionary(text: data as? String ?? "") {
                                    self.updateUI(dict: dic)
                                }
                            }
                        }else {
                            DispatchQueue.main.async {
                                self.showAlert(alertTitle: "Error".localized, alertBody: "Request Failed".localized, alertImage: UIImage(named: "error")!)}
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "Error".localized, alertBody: "Request Failed".localized, alertImage: UIImage(named: "error")!)}
                }
            }catch {}
        }
    }
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        
    }
}
