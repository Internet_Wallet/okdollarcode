//
//  PointSuccessStatusViewController.swift
//  OK
//
//  Created by Mohit on 3/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PointSuccessStatusViewController: LoyaltyBaseViewController {

    @IBOutlet weak var noRecordView : UIView!
    @IBOutlet weak var noRecordsLabel : UILabel! {
        didSet {
            self.noRecordsLabel.font = UIFont(name: appFont, size: appFontSize)
            self.noRecordsLabel.text = self.noRecordsLabel.text?.localized
        }
    }
    @IBOutlet weak var recordView : UIView!
    var merchantDictionary = [buyBonusPointModel]()
    @IBOutlet weak var topView : UIView! {
        didSet {
            self.topView.isHidden = true
        }
    }
    @IBOutlet weak var bottomView : UIView! {
        didSet {
            self.bottomView.isHidden = true
        }
    }
    @IBOutlet weak var merchantNameLbl : UILabel! {
        didSet {
            self.merchantNameLbl.font = UIFont(name: appFont, size: appFontSize)
            self.merchantNameLbl.text = self.merchantNameLbl.text?.localized
        }
    }
    @IBOutlet weak var pointsLbl : UILabel! {
        didSet {
            self.pointsLbl.font = UIFont(name: appFont, size: appFontSize)
            self.pointsLbl.text = self.pointsLbl.text?.localized
        }
    }
    @IBOutlet weak var totalAmountLabel : UILabel!{
        didSet{
            self.totalAmountLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var totalLabel : UILabel! {
        didSet {
            self.totalLabel.font = UIFont(name: appFont, size: appFontSize)
            self.totalLabel.text = self.totalLabel.text?.localized
        }
    }
    @IBOutlet weak var successStatusTableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.successStatusTableView.tableFooterView = UIView()
        self.setUpViews()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpViews() {
        DispatchQueue.main.async {
            self.noRecordView.isHidden = true
        }
        if self.merchantDictionary.count > 0 {
            self.merchantDictionary = self.merchantDictionary.filter { $0.status == "Sold"}
            if self.merchantDictionary.count > 0 {
                DispatchQueue.main.async {
                    self.bottomView.isHidden = false
                    self.topView.isHidden = false
                    self.successStatusTableView.reloadData()
                }
            } else {
                DispatchQueue.main.async {
                    self.noRecordView.isHidden = false
                }
            }
        }
        var amount : Int = 0
        for singelRec in self.merchantDictionary {
            amount = amount + Int(singelRec.originalPrice ?? 0)
        }
        DispatchQueue.main.async {
            self.totalAmountLabel.text = String(amount)
        }
    }
    
}

extension PointSuccessStatusViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.merchantDictionary.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PointsStatusTableViewCell = tableView.dequeueReusableCell(withIdentifier: "successPointStatusCell", for: indexPath) as! PointsStatusTableViewCell
        cell.loadCell(dictionary: merchantDictionary[indexPath.row])
        return cell
    }
}
