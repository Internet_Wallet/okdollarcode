//
//  BonusPointResaleStatusViewController.swift
//  OK
//
//  Created by Mohit on 3/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BonusPointResaleStatusViewController: LoyaltyBaseViewController {

    fileprivate var loyaltyMenu : PaytoScroller?
    fileprivate var controllerArray : [UIViewController] = []
    
    var statusMerchantModelArray = [buyBonusPointModel]()
    var pendingStatusVC = PointPendingStatusViewController()
    var successStatusVC = PointSuccessStatusViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MyNumberTopup.theme = UIColor.darkText
        getBonusPointStatusList()
        addCustomBackButton(navTitle: "Bonus Point Resale")
        //MyNumberTopup.theme = MyNumberTopup.OperatorColorCode.okDefault
        
        pendingStatusVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: "PointPendingStatusViewController_ID") as! PointPendingStatusViewController
        pendingStatusVC.title = "PENDING".localized
        
        successStatusVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: "PointSuccessStatusViewController_ID") as! PointSuccessStatusViewController
        successStatusVC.title = "SUCCESSFUL".localized
        
        controllerArray = [pendingStatusVC, successStatusVC]
        
        let parameters = [PaytoScrollerOptionMenuItemSeparatorWidth: 0,
                          PaytoScrollerOptionMenuItemSeparatorPercentageHeight: 0.1,
                          PaytoScrollerOptionScrollMenuBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectedMenuItemLabelColor: UIColor.darkText,
                          PaytoScrollerOptionViewBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectionIndicatorColor: UIColor.red,
                          PaytoScrollerOptionUnselectedMenuItemLabelColor:UIColor.darkText,
                          PaytoScrollerOptionMenuHeight: 50,
                          PaytoScrollerOptionMenuItemFont: "Zawgyi-One",
                          PaytoScrollerOptionMenuItemWidth: (screenWidth / CGFloat(controllerArray.count)) - 10.0] as [String : Any]
        
        if let navi = self.navigationController {
            let yAxis = navi.navigationBar.frame.height + UIApplication.shared.statusBarFrame.height
            loyaltyMenu = PaytoScroller(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: yAxis, width: self.view.frame.width, height: self.view.frame.height - yAxis), options: parameters)
            loyaltyMenu?.delegate = self
            if let menu = loyaltyMenu {
                self.view.addSubview(menu.view)
            }
            loyaltyMenu?.scrollingEnable(true)
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        loyaltyMenu?.selectedMenuItemLabelColor = UIColor.darkText
    }
    
    func getBonusPointStatusList() {
        self.getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
            guard isSuccess, let tokenAuthStr = tokenString as? String else {
                self.showErrorAlert(errMessage: "try again")
                return
            }
            DispatchQueue.main.async(execute: {
                self.getAllMerchantList(authToken: tokenAuthStr)
            })
        })
    }
    
    private func getAllMerchantList(authToken: String) {
        
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStr   = String.init(format: Url.bonusPointResaleStatusApi, UserModel.shared.mobileNo, UserModel.shared.name, ok_password ?? "")
            let url = getUrl(urlStr: urlStr, serverType: .bonusPointApi)
            let params = [String: Any]()
            web.genericClassRegLoyalty(url: url, param: params, httpMethod: "GET", mScreen: "mMerchantList", authToken: authToken)
        } else {
            self.showErrorAlert(errMessage: "No Internet Connection".localized)
        }
    }
}

extension BonusPointResaleStatusViewController: WebServiceResponseDelegate {
    
    func webResponse(withJson json: AnyObject, screen: String) {
        progressViewObj.removeProgressView()
        do {
            if let data = json as? Data {
                if let responseDict = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Dictionary<String , Any> {
                    
                    if let statusCode = responseDict.safeValueForKey("StatusCode") as? Int, statusCode == 302 {
                        if let records = responseDict.safeValueForKey("Results") as? [Dictionary<String, Any>] {
                            for singleRec in records {
                                self.statusMerchantModelArray.append(buyBonusPointModel.init(dictionary: singleRec))
                            }
                            self.pendingStatusVC.merchantDictionary = self.statusMerchantModelArray
                            self.successStatusVC.merchantDictionary = self.statusMerchantModelArray
                            self.pendingStatusVC.setUpViews()
                        }
                    } else if let statusCode = responseDict.safeValueForKey("StatusCode") as? Int, statusCode == 404 {
                        DispatchQueue.main.async {
                            let alertMessage = responseDict.safeValueForKey("Message") as? String ?? ""
                            loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: #imageLiteral(resourceName: "dashboard_loyalty"))
                            loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                                
                            })
                            loyaltyAlertView.showAlert(controller: self)
                        }
                    } else {
                        if let alertMessage = responseDict.safeValueForKey("Message") as? String {
                            DispatchQueue.main.async {
                                loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: #imageLiteral(resourceName: "dashboard_loyalty"))
                                loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                                    
                                })
                                loyaltyAlertView.showAlert(controller: self)
                            }
                        }
                    }
//                    println_debug(responseDict)
                }
            }
        } catch {
            
        }
    }
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        PTLoader.shared.hide()
    }
}



extension BonusPointResaleStatusViewController: PaytoScrollerDelegate {
    
    func willMove(toPage controller: UIViewController!, index: Int) {
    }
    
    func didMove(toPage controller: UIViewController!, index: Int) {
    }
    
}



