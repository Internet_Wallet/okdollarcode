//
//  BonusPointResaleViewController.swift
//  OK
//
//  Created by Kethan on 3/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct merchantDetailsModel {
    var bonusPointId : String?
    var customerId : String?
    var expiryDate : String?
    var merchant = Dictionary<String, Any>()
    var pointBalance : Int?
    var sellingPrice : Float?
    var status : String?
    
    init(dictionary : Dictionary<String, Any>) {
        self.bonusPointId = dictionary.safeValueForKey("BonusPointId") as? String ?? ""
        self.customerId = dictionary.safeValueForKey("CustomerId") as? String ?? ""
        self.expiryDate = dictionary.safeValueForKey("ExpiryDate") as? String ?? ""
        
        if let bonusPriceStr = dictionary.safeValueForKey("PointBalance") as? NSNumber {
            self.pointBalance = bonusPriceStr.intValue
        } else {
            self.pointBalance = 0
        }
        if let sellingPriceStr = dictionary.safeValueForKey("SellingPrice") as? NSNumber {
            self.sellingPrice = sellingPriceStr.floatValue
        } else {
            self.sellingPrice = 0.0
        }
        self.status = dictionary.safeValueForKey("Status") as? String ?? ""
        if let merchantArray = dictionary.safeValueForKey("Merchant") as? Dictionary<String , Any> {
            self.merchant["merchantAccountNo"] = merchantArray.safeValueForKey("MerchantAccountNo") as? String ?? ""
            self.merchant["merchantId"] = merchantArray.safeValueForKey("MerchantId") as? String ?? ""
            self.merchant["shopName"] = merchantArray.safeValueForKey("ShopName") as? String ?? ""
        }
    }
}

class BonusPointResaleViewController: LoyaltyBaseViewController, UIPopoverPresentationControllerDelegate {
    enum ScreenFrom {
        case dashboard, other
    }
    var bonusPoints = 0
    
    var merchantDetailsModelArray = [merchantDetailsModel]()
    var balanceListArray = [Dictionary<String,Any>]()
    var balanceDict = Dictionary<String,Any>()
    var navigation: UINavigationController?

    @IBOutlet weak var merchaneNameLbl: UILabel! {
        didSet {
            self.merchaneNameLbl.font = UIFont(name: appFont, size: appFontSize)
            self.merchaneNameLbl.text = self.merchaneNameLbl.text?.localized
        }
    }
    @IBOutlet weak var earnPointLbl: UILabel!{
        didSet {
            self.earnPointLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var earnPointLblText: UILabel!{
        didSet {
            self.earnPointLblText.font = UIFont(name: appFont, size: appFontSize)
            self.earnPointLblText.text = self.earnPointLblText.text?.localized
        }
    }
    var screenFrom = ScreenFrom.other

    override func viewDidLoad() {
        super.viewDidLoad()
        addBckButton(navTitle: "Bonus Point Resale")
        addCustomMenuButton()
        
        self.getAvalaibleBalance()
        self.getAllMerchantList()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func addBckButton(navTitle: String) {
        let backButton = UIButton(type: .custom)
        backButton.setBackgroundImage(#imageLiteral(resourceName: "tabBarBack"), for: .normal)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        backButton.addTarget(self, action: #selector(bckAction), for: .touchUpInside)
        navigationItem.title = navTitle.localized
        navigation?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white ]
        navigation?.navigationBar.barTintColor = UIColor.red
        navigation?.navigationBar.tintColor = UIColor.white
    }
    
    @objc func bckAction() {
        switch screenFrom {
        case .dashboard:
            self.dismiss(animated: true, completion: nil)
        case .other:
            navigation?.popViewController(animated: true)
        }
    }
    
    private func addCustomMenuButton() {
        let menuButton = UIButton(type: .custom)
        menuButton.frame = CGRect(x: 0, y: 0, width: 30, height: 40)
        menuButton.setImage(UIImage.init(named: "menu_white_payto.png"), for: .normal)
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: menuButton)]
        menuButton.addTarget(self, action: #selector(menuAction(_ : )), for: .touchUpInside)
    }
    
    @objc func menuAction(_ sender : UIBarButtonItem) {
        let popoverContent = self.storyboard?.instantiateViewController(withIdentifier: "ViewAllStatusPopover_ID") as? ViewAllStatusPopover
        popoverContent?.delegate = self
        popoverContent?.modalPresentationStyle = .popover
        let popover = popoverContent?.popoverPresentationController
        popoverContent?.preferredContentSize = CGSize(width: 200.0, height: 45.0)
        popover?.delegate = self
        popover?.sourceView = self.view
        popover?.permittedArrowDirections = .init(rawValue: 0)
        popover?.sourceRect = CGRect(x: self.view.frame.size.width, y: 0, width: 0, height: 0)
        self.present(popoverContent!, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    private func getAvalaibleBalance() {
        if appDelegate.checkNetworkAvail() {
            let web      = PayToWebApi()
            web.delegate = self
            var ip = ""
            if let ipAdd = OKBaseController.getIPAddress() {
                ip = ipAdd
            }
            let urlStr   = String.init(format: Url.CheckLoyaltyBalanceApi, UserModel.shared.mobileNo, ok_password ?? "", UserModel.shared.mobileNo, ip , "ios", UserLogin.shared.token)
            let url = getUrl(urlStr: urlStr, serverType: .serverEstel)
            //            println_debug(url)
            let pRam = Dictionary<String,String>()
            web.genericClassXML(url: url, param: pRam as AnyObject, httpMethod: "POST", mScreen: "mAvailableBalance")
        }
    }
    
    
    func getAllMerchantList() {
        self.getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
            guard isSuccess, let tokenAuthStr = tokenString as? String else {
                self.showCustomErrorAlert(errMessage: "try again")
                return
            }
            DispatchQueue.main.async(execute: {
                self.getAllMerchantList(authToken: tokenAuthStr)
            })
        })
    }
    
  private func getAllMerchantList(authToken: String) {
    
    if appDelegate.checkNetworkAvail() {
      PTLoader.shared.show()
      let web      = WebApiClass()
      web.delegate = self
      
      let urlStr   = String.init(format: Url.getMerchantListUrl, UserModel.shared.mobileNo, UserModel.shared.name, ok_password ?? "")
      guard let encodedString = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
      
      let url = getUrl(urlStr: encodedString, serverType: .bonusPointApi)
      let params = [String: Any]()
      web.genericClassRegLoyalty(url: url, param: params, httpMethod: "GET", mScreen: "mMerchantList", authToken: authToken)
    } else {
      
    }
    
  }
    
    private func showSelectedViewController(viewController : UIViewController) {
        navigation?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func selectMerchantListAction(_ sender : UIButton) {
        if merchantDetailsModelArray.count > 0 {
            if let merchantListVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: "MerchantListViewController_ID") as? MerchantListViewController {
                merchantListVC.screenType = "Bonus Point Resale"
                merchantListVC.merchantDetailsDictionary = self.merchantDetailsModelArray
                showSelectedViewController(viewController: merchantListVC)
            }
        } else {
            DispatchQueue.main.async{
                let alertMessage = "No merchant found"
                loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: nil)
                loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                    
                })
                loyaltyAlertView.showAlert(controller: self)
            }
        }
    }
}

extension BonusPointResaleViewController : ViewAllStatusProtocol {
    func showViewAllStatusVC() {
        let viewAllStatusVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: "BonusPointResaleStatusViewController_ID")
        navigation?.pushViewController(viewAllStatusVC, animated: true)
    }
}


extension BonusPointResaleViewController: PTWebResponseDelegate {
    //PTWebresponseDelegate
    func webSuccessResult(data: Any, screen: String) {
        //        println_debug(data)
        //        println_debug(screen)
        
      if screen == "mAvailableBalance" {
            if let xmlString = data as? String {
                let xml = SWXMLHash.parse(xmlString)
                
                balanceDict.removeAll()
                self.enumerateBalance(indexer: xml)
                self.balanceListArray.removeAll()
                self.balanceListArray.append(balanceDict)
                //                println_debug(balanceListArray)
                self.removeProgressView()
                if balanceDict["resultdescription"] as? String == "Transaction Successful" {
                    DispatchQueue.main.async {
                        let formatter = NumberFormatter()
                        formatter.numberStyle = .decimal
                        if UserModel.shared.agentType == .user {
                            let earnBal = self.balanceDict["udv5"] as? String ?? "0"
                            let earnBalArray = earnBal.components(separatedBy: "|")
                            if  earnBalArray.count == 2 {
                                if let _ = formatter.string(from: Float(earnBalArray[1])! as NSNumber) {
                                    //self.earnPointLbl.text = numberStr
                                    
                                }
                            }
                        } else {
                            let availableBal = self.balanceDict["udv4"] as? String ?? "0"
                            let availableBalArray = availableBal.components(separatedBy: "|")
                            if  availableBalArray.count == 2 {
                                if let _ = formatter.string(from: Float(availableBalArray[1])! as NSNumber) {
//                                    self.availablePointLbl.text = numberStr
                                }
                            }
                            let earnBal = self.balanceDict["udv5"] as? String ?? "0.0"
                            let earnBalArray = earnBal.components(separatedBy: "|")
                            if  earnBalArray.count == 2 {
                                if let _ = formatter.string(from: Float(earnBalArray[1])! as NSNumber) {
                                    //self.earnPointLbl.text = numberStr
//                                    UserDefaults.standard.set(self.earnPointLbl.text!, forKey: "MyEarnPoint")
                                }
                            }
                        }
                    }
                }
            }
        }
    }
        
    func enumerateBalance(indexer: XMLIndexer) {
        for child in indexer.children {
            balanceDict[child.element!.name] = child.element!.text
            enumerateBalance(indexer: child)
        }
    }
    
    
    func webFailureResult(screen: String) {
        println_debug("Failure")
    }
}

extension BonusPointResaleViewController: WebServiceResponseDelegate {
    
    func webResponse(withJson json: AnyObject, screen: String) {
        progressViewObj.removeProgressView()
        do {
            if let data = json as? Data {
                if let responseDict = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Dictionary<String , Any> {
                    if let statusCode = responseDict.safeValueForKey("StatusCode") as? Int, statusCode == 404 {
                        DispatchQueue.main.async {
                            let alertMessage = responseDict.safeValueForKey("Message") as? String ?? ""
                            loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: #imageLiteral(resourceName: "dashboard_loyalty"))
                            loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                                
                            })
                            loyaltyAlertView.showAlert(controller: self)
                        }
                    } else {
                        if let statusCode = responseDict.safeValueForKey("StatusCode") as? Int, statusCode == 302 {
                            if let dictionaryArray = responseDict.safeValueForKey("Results") as? [Dictionary<String, Any>] {
                                var numberStr = 0
                                merchantDetailsModelArray.removeAll()
                                for dictionary in dictionaryArray {
                                    let model = merchantDetailsModel.init(dictionary: dictionary)
                                    self.merchantDetailsModelArray.append(model)
                                    if (model.status ?? "" == "Available") {
                                        numberStr = numberStr + (model.pointBalance ?? 0)
                                    }
                                }
                                DispatchQueue.main.async {
                                    self.earnPointLbl.text = self.amountInFormat("\(numberStr)").replacingOccurrences(of: ".00", with: "")
                                    UserDefaults.standard.set((self.earnPointLbl.text ?? "").replacingOccurrences(of: ",", with: ""), forKey: "MyEarnPoint")
                                }
                            }
                        }
                    }
                }
            }
        } catch {
            
        }
    }
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        PTLoader.shared.hide()
    }
}
