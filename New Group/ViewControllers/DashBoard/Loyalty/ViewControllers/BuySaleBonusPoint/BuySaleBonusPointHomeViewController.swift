//
//  BuySaleBonusPointHomeViewController.swift
//  OK
//
//  Created by iMac on 4/25/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class BuySaleBonusPointHomeViewController: LoyaltyBaseViewController, UIPopoverPresentationControllerDelegate {
    
    fileprivate var buySaleMoneyMenu: PaytoScroller?
    fileprivate var controllerArray: [UIViewController] = []
    lazy var searchBar:UISearchBar = UISearchBar()
    var searchActive: Bool = false
    var userInfo: Dictionary<String, Any> = [:]
    var saleBonusPointTabVC = BonusPointResaleViewController()
    var buyBonusPointTabVC = BuyBonusPointViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
        println_debug("BuySaleBonusPointHomeViewController")
        self.addBckButton(navTitle: "")
        
        
        MyNumberTopup.theme = UIColor.darkText
        addCustomMoreButton()
        saleBonusPointTabVC =  UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: loyaltyMainViews.viewControllers.bonusPointResaleVC) as! BonusPointResaleViewController
        saleBonusPointTabVC.navigation = self.navigationController
        saleBonusPointTabVC.title = "SALE".localized
        
        buyBonusPointTabVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: loyaltyMainViews.viewControllers.buyBonusPointVC) as! BuyBonusPointViewController
        buyBonusPointTabVC.title = "BUY".localized
        buyBonusPointTabVC.searchBar = searchBar
        buyBonusPointTabVC.navigation = self.navigationController
        controllerArray = [saleBonusPointTabVC, buyBonusPointTabVC]
        
        let parameters = [PaytoScrollerOptionMenuItemSeparatorWidth: 0,
                          PaytoScrollerOptionMenuItemSeparatorPercentageHeight: 0.1,
                          PaytoScrollerOptionScrollMenuBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectedMenuItemLabelColor: UIColor.darkText,
                          PaytoScrollerOptionViewBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectionIndicatorColor: UIColor.red,
                          PaytoScrollerOptionUnselectedMenuItemLabelColor:UIColor.darkText,
                          PaytoScrollerOptionMenuHeight: 50,
                          PaytoScrollerOptionMenuItemFont: "Zawgyi-One",
                          PaytoScrollerOptionMenuItemWidth: (screenWidth / CGFloat(controllerArray.count)) - 10.0] as [String : Any]
        
        if let navi = self.navigationController {
            let yAxis = navi.navigationBar.frame.height + UIApplication.shared.statusBarFrame.height
            buySaleMoneyMenu = PaytoScroller(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: yAxis, width: self.view.frame.width, height: self.view.frame.height - yAxis), options: parameters)
            buySaleMoneyMenu?.delegate = self
            if let menu = buySaleMoneyMenu {
                self.view.addSubview(menu.view)
            }
            
            buySaleMoneyMenu?.scrollingEnable(true)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(searchActionHide), name: Notification.Name(rawValue: "BuyBonusPointSearchHide"), object: nil)
        // Do any additional setup after loading the view.
    }
    private func addBckButton(navTitle: String) {
        let backButton = UIButton(type: .custom)
        backButton.setBackgroundImage(#imageLiteral(resourceName: "tabBarBack"), for: .normal)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        backButton.addTarget(self, action: #selector(bckAction), for: .touchUpInside)
        
        
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 60, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
       // let ypos = (titleViewSize.height - 30 ) / 2
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
                  label.type = .continuous
                  label.speed = .duration(4)
                  label.text =  "Buy & Sale Bonus Point".localized
                  label.textColor = UIColor.white
                  if let navFont = UIFont(name: appFont, size: 21) {
                      label.font = navFont
                  }
                  label.center = navTitleView.center
                  label.textAlignment = .center
                  
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
        
        
        
        
        
       // self.title = navTitle.localized
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 12) ?? UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.barTintColor = UIColor.red
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    private func addCustomMenuButton() {
        navigationItem.rightBarButtonItem = nil
        let searchButton = UIButton(type: .custom)
        searchButton.setBackgroundImage(UIImage.init(named: "search_white.png"), for: .normal)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: searchButton)
        searchButton.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
    }
    
    @objc func bckAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor.red
        buySaleMoneyMenu?.selectedMenuItemLabelColor = UIColor.darkText
    }
    
    @objc func searchActionHide() {
        DispatchQueue.main.async {
            self.searchActive = false
            self.searchBar.text = ""
            self.navigationItem.titleView = nil
            self.navigationItem.rightBarButtonItem = nil
        }
    }
    
    @objc func searchAction() {
        DispatchQueue.main.async {
            if self.buyBonusPointTabVC.buyingModelArray.count > 1 {
                self.searchBar.text = ""
                self.searchBar.delegate = self
                self.searchBar.tintColor = UIColor.lightGray
                
                if self.navigationItem.titleView == nil {
                    self.searchBar.placeholder = "Search Name and Number".localized
                    if let searchTextField = self.searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
                        if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                            if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                                searchTextField.font = myFont
                            }else{
                                //
                            }
                            searchTextField.placeholder = "Search".localized
                            searchTextField.backgroundColor  = .white
                        }
                    }
                    self.searchBar.tintColor = .black
                    self.searchBar.frame = (self.navigationController?.navigationBar.frame)!
                    self.navigationItem.titleView = self.searchBar
                    self.searchBar.becomeFirstResponder()
                } else {
                    self.searchActive = false
                    self.searchTextChangeNotification(searchText: "", status: false)
                    self.navigationItem.titleView = nil
                }
            } else {
                self.searchActive = false
                self.searchTextChangeNotification(searchText: "", status: false)
                self.navigationItem.titleView = nil
                self.navigationItem.rightBarButtonItem = nil
            }
        }
    }
    
    @objc func menuAction(_ sender : UIBarButtonItem) {
        let popoverContent = self.storyboard?.instantiateViewController(withIdentifier: "ViewAllStatusPopover_ID") as? ViewAllStatusPopover
        popoverContent?.delegate = self
        popoverContent?.modalPresentationStyle = .popover
        let popover = popoverContent?.popoverPresentationController
        popoverContent?.preferredContentSize = CGSize(width: 200.0, height: 45.0)
        popover?.delegate = self
        popover?.sourceView = self.view
        popover?.permittedArrowDirections = .init(rawValue: 0)
        popover?.sourceRect = CGRect(x: self.view.frame.size.width, y: 0, width: 0, height: 0)
        self.present(popoverContent!, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    private func addCustomMoreButton() {
        navigationItem.rightBarButtonItem = nil
        let searchButton = UIButton(type: .custom)
        searchButton.frame = CGRect(x: 0, y: 0, width: 30, height: 40)
        searchButton.setImage(UIImage.init(named: "menu_white_payto.png"), for: .normal)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: searchButton)
        searchButton.addTarget(self, action: #selector(menuAction(_ : )), for: .touchUpInside)
    }
    
    private func searchTextChangeNotification(searchText: String, status: Bool) {
        userInfo.removeAll()
        userInfo["searchText"] = searchText
        userInfo["searchStatus"] = searchActive
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SearchTextChangeInBuyBonus"), object: nil, userInfo: userInfo)
    }
    
    deinit {
        buySaleMoneyMenu?.removeFromParent()
    }
    
}
    
extension BuySaleBonusPointHomeViewController: PaytoScrollerDelegate {
    func willMove(toPage controller: UIViewController!, index: Int) {
        if index == 0 {
            self.addCustomMoreButton()
        } else {
            self.addCustomMenuButton()
        }
    }
    
    func didMove(toPage controller: UIViewController!, index: Int) {
    }
}


extension BuySaleBonusPointHomeViewController : ViewAllStatusProtocol {
    func showViewAllStatusVC() {
        let viewAllStatusVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: "BonusPointResaleStatusViewController_ID")
        self.navigationController?.pushViewController(viewAllStatusVC, animated: true)
    }
}


//MARK: - UISearchBarDelegate
extension BuySaleBonusPointHomeViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        searchActive = false
//        self.searchTextChangeNotification(searchText: "", status: false)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        self.searchTextChangeNotification(searchText: "", status: false)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        searchActive = false
//        self.searchTextChangeNotification(searchText: "", status: false)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.hasPrefix(" ") {
            searchBar.text?.removeFirst()
            return
        }
        if searchText.contains(find: "  ") {
            searchBar.text?.removeLast()
            return
        }
        if searchText.containsEmoji {
            searchBar.text?.removeLast()
            return
        }
        if searchText.count > 50 {
            searchBar.text?.removeLast()
            return 
        }
        self.searchTextChangeNotification(searchText: searchText, status: true)
    }
}

