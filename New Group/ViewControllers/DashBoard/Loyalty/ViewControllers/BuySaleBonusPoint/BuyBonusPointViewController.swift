//
//  BuyBonusPointViewController.swift
//  OK
//
//  Created by Kethan on 7/5/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct buyBonusPointModel {
    var bonusPointId : String?
    var buyingMarginPercentage: Float?
    var buyingPrice : Float?
    var createdDate : String?
    var customerId : String?
    var expiryDate : String?
    var isDeleted : Int?
    var merchant = Dictionary<String, Any>()
    var merchantId : String?
    var originalPrice : Float?
    var productId : String?
    var sellingPrice : Float?
    var status : String?
    
    init(dictionary : Dictionary<String, Any>) {
        self.bonusPointId = dictionary.safeValueForKey("BonusPointId") as? String ?? ""
        self.customerId = dictionary.safeValueForKey("CustomerId") as? String ?? ""
        self.expiryDate = dictionary.safeValueForKey("ExpiryDate") as? String ?? ""
        if let sellingPriceStr = dictionary.safeValueForKey("SellingPrice") as? NSNumber {
            self.sellingPrice = sellingPriceStr.floatValue
        } else {
            self.sellingPrice = 0.0
        }
        if let buyingMarginPriceStr = dictionary.safeValueForKey("BuyingMarginPercentage") as? NSNumber {
            self.buyingMarginPercentage = buyingMarginPriceStr.floatValue
        } else {
            self.buyingMarginPercentage = 0.0
        }
        if let buyingPriceStr = dictionary.safeValueForKey("BuyingPrice") as? NSNumber {
            self.buyingPrice = buyingPriceStr.floatValue
        } else {
            self.buyingPrice = 0.0
        }
        self.isDeleted = dictionary.safeValueForKey("IsDeleted") as? Int ?? 0
        self.status = dictionary.safeValueForKey("Status") as? String ?? ""
        if let originalPriceStr = dictionary.safeValueForKey("OriginalPrice") as? NSNumber {
            self.originalPrice = originalPriceStr.floatValue
        } else {
            self.originalPrice = 0.0
        }
        self.merchantId = dictionary.safeValueForKey("MerchantId") as? String ?? ""
        self.productId = dictionary.safeValueForKey("ProductId") as? String ?? ""
        if let merchantArray = dictionary.safeValueForKey("Merchant") as? Dictionary<String , Any> {
            self.merchant["merchantAccountNo"] = merchantArray.safeValueForKey("MerchantAccountNo") as? String ?? ""
            self.merchant["merchantId"] = merchantArray.safeValueForKey("MerchantId") as? String ?? ""
            self.merchant["shopName"] = merchantArray.safeValueForKey("ShopName") as? String ?? ""
        }
    }
}

class BuyBonusPointViewController: LoyaltyBaseViewController {
  
    var buyingModelArray = [buyBonusPointModel]()
    @IBOutlet weak var highLightImage1: UIImageView!
    @IBOutlet weak var highLightImage2: UIImageView!
    @IBOutlet weak var highLightImage3: UIImageView!
    
    var filterModelArray = [buyBonusPointModel]()
    var searchModelArray = [buyBonusPointModel]()
    var isFilter = false
    @IBOutlet weak var buyBonusPointTableView : UITableView!
    var successIndex : Int?
    var navigation: UINavigationController?
    
    @IBOutlet weak var sortStackView : UIStackView! {
        didSet {
            self.sortStackView.isHidden = true
        }
    }
    @IBOutlet weak var sortByMainLabel : MarqueeLabel!
        {
        didSet {
            self.sortByMainLabel.font = UIFont(name: appFont, size: appFontSize)
            self.sortByMainLabel.text = "Sort By".localized//self.sortByMainLabel.text?.localized
        }
    }
    
    @IBOutlet weak var sortImageView : UIImageView!
    
    @IBOutlet weak var filterByMainLabel : MarqueeLabel!
        {
        didSet {
            self.filterByMainLabel.font = UIFont(name: appFont, size: appFontSize)
            self.filterByMainLabel.text = "Filter By".localized//self.filterByMainLabel.text?.localized
        }
    }
    
    @IBOutlet weak var dateByMainLabel : UILabel!
        {
        didSet {
            self.dateByMainLabel.font = UIFont(name: appFont, size: appFontSize)
            self.dateByMainLabel.text = "Expiry Date".localized//self.dateByMainLabel.text?.localized
        }
    }
    
    @IBOutlet weak var sortViewHeightConstraint : NSLayoutConstraint!
    
    //SortView
    @IBOutlet weak var sortMainView : UIView! {
        didSet {
            self.sortMainView.isHidden = true
        }
    }
    @IBOutlet weak var sortByLabel : UILabel!
        {
        didSet {
            self.sortByLabel.font = UIFont(name: appFont, size: appFontSize)
            self.sortByLabel.text = self.sortByLabel.text?.localized
        }
    }
    @IBOutlet weak var defSortByLabel : UILabel!
        {
        didSet {
            self.defSortByLabel.font = UIFont(name: appFont, size: appFontSize)
            self.defSortByLabel.text = self.defSortByLabel.text?.localized
        }
    }
    @IBOutlet weak var nameAZSortByLabel : UILabel! {
        didSet {
            self.nameAZSortByLabel.font = UIFont(name: appFont, size: appFontSize)
            self.nameAZSortByLabel.text = self.nameAZSortByLabel.text?.localized
        }
    }
    @IBOutlet weak var nameZASortByLabel : UILabel! {
        didSet {
            self.nameZASortByLabel.font = UIFont(name: appFont, size: appFontSize)
            self.nameZASortByLabel.text = self.nameZASortByLabel.text?.localized
        }
    }
    @IBOutlet weak var defSortByImageView : UIImageView!
    @IBOutlet weak var nameAZSortByImageView : UIImageView!
    @IBOutlet weak var nameZASortByImageView : UIImageView!
    
    var filterByArray = [Dictionary<String, Any>]()
    
    //FilterView
    @IBOutlet weak var filterMainView : UIView! {
        didSet {
            self.filterMainView.isHidden = true
        }
    }
    @IBOutlet weak var filterByLabel : UILabel! {
        didSet {
            self.filterByLabel.font = UIFont(name: appFont, size: appFontSize)
            self.filterByLabel.text = self.filterByLabel.text?.localized
        }
    }
    @IBOutlet weak var defFilterByLabel : UILabel! {
        didSet {
            self.defFilterByLabel.font = UIFont(name: appFont, size: appFontSize)
            self.defFilterByLabel.text = self.defFilterByLabel.text?.localized
        }
    }
    @IBOutlet weak var okDolTopUpFilterByLabel : UILabel! {
        didSet {
            self.okDolTopUpFilterByLabel.font = UIFont(name: appFont, size: appFontSize)
            self.okDolTopUpFilterByLabel.text = self.okDolTopUpFilterByLabel.text?.localized
        }
    }
    @IBOutlet weak var defFilterByImageView : UIImageView!
    @IBOutlet weak var okDolTopUpFilterByImageview : UIImageView!
    
    
    @IBOutlet weak var noRecordsView : UIView! {
        didSet {
            self.noRecordsView.isHidden = false
        }
    }
    @IBOutlet weak var noRecordsLabel : UILabel! {
        didSet {
            self.noRecordsLabel.font = UIFont(name: appFont, size: appFontSize)
            self.noRecordsLabel.text = self.noRecordsLabel.text?.localized
        }
    }
    
    //searchbar
    var searchBar: UISearchBar?
    var searchActive : Bool = false
    enum ScreenFrom {
        case dashboard, other
    }
    var screenFrom = ScreenFrom.other
    
    var refreshControl = UIRefreshControl()
    @IBOutlet weak var filterByTableView: UITableView! {
        didSet {
            filterByTableView.tableFooterView = UIView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let image = UIImage(named: "sort_filter")?.withRenderingMode(.alwaysTemplate)
        sortImageView.image = image
        sortImageView.tintColor = .black
//        addCustomMenuButton()
        self.sortViewHeightConstraint.constant = 0.0
        addBckButton(navTitle: "")
        buyBonusPointList()
        buyBonusPointTableView.tableFooterView = UIView()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSearchText(_:)), name: NSNotification.Name(rawValue: "SearchTextChangeInBuyBonus"), object: nil)
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        buyBonusPointTableView.addSubview(refreshControl) // not required when using UITableViewController
        // Do any additional setup after loading the view.
        var dict = Dictionary<String, Any>()
        dict["name"] = "Default"
        dict["selected"] = true
        filterByArray.append(dict)
        dict["name"] = "OK DOLLAR TOPUP"
        dict["selected"] = false
        filterByArray.append(dict)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        addBckButton(navTitle: "")
    }
    
    @objc func refresh(sender:AnyObject) {
        self.searchBar?.text = ""
        self.navigation?.navigationItem.titleView = nil
        searchActive = false
        isFilter = false
       self.buyBonusPointList()
    }
    
    @objc func showSearchText(_ notification: NSNotification) {
        if let searchStatus = notification.userInfo?["searchStatus"] as? Bool {
            if searchStatus == false {
                searchActive = false
                DispatchQueue.main.async {
                    self.noRecordsView.isHidden = true
                    self.buyBonusPointTableView.reloadData()
                }
                return
            }
        }
        if let searchText = notification.userInfo?["searchText"] as? String {
            if searchText.count > 0 {
                searchActive = true
                if !self.isFilter {
                    self.searchModelArray = self.buyingModelArray.filter({ (singleModel) -> Bool in
                        let merchant = singleModel.merchant
                        return (merchant["shopName"] as? String ?? "").containsIgnoringCase(find: searchText)
                    })
                } else {
                    self.searchModelArray = self.filterModelArray.filter({ (singleModel) -> Bool in
                        let merchant = singleModel.merchant
                        return (merchant["shopName"] as? String ?? "").containsIgnoringCase(find: searchText)
                    })
                }
                DispatchQueue.main.async {
                    if self.searchModelArray.count > 0 {
                        self.noRecordsView.isHidden = true
                    } else {
                        self.noRecordsView.isHidden = false
                    }
                }
            } else {
                isFilter = false
                filterByArray[0]["selected"] = true
                for indexL in filterByArray.indices {
                    if indexL != 0 {
                        filterByArray[indexL]["selected"] = false
                    }
                }
                searchActive = false
            }
            DispatchQueue.main.async {
                self.buyBonusPointTableView.reloadData()
            }
        }
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func addBckButton(navTitle: String) {
        let backButton = UIButton(type: .custom)
        backButton.setBackgroundImage(#imageLiteral(resourceName: "tabBarBack"), for: .normal)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        backButton.addTarget(self, action: #selector(bckAction), for: .touchUpInside)
        
        
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 60, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
       // let ypos = (titleViewSize.height - 30 ) / 2
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
                  label.type = .continuous
                  label.speed = .duration(4)
                  label.text =  "Buy Bonus Points".localized
                  label.textColor = UIColor.white
                  if let navFont = UIFont(name: appFont, size: 21) {
                      label.font = navFont
                  }
                  label.center = navTitleView.center
                  label.textAlignment = .center
                  
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
        
        
        
        
        
        navigation?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: appFontSize) ?? UIFont.systemFont(ofSize: appFontSize), NSAttributedString.Key.foregroundColor: UIColor.white]
        navigation?.navigationItem.title = navTitle.localized
        navigation?.navigationBar.barTintColor = UIColor.red
        navigation?.navigationBar.tintColor = UIColor.white
    }
    
    @objc func bckAction() {
        switch screenFrom {
        case .dashboard:
            self.dismiss(animated: true, completion: nil)
        case .other:
            navigation?.popViewController(animated: true)
        }
    }
    
    func buyBonusPointList() {
        PTLoader.shared.show()
        self.getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
            guard isSuccess, let tokenAuthStr = tokenString as? String else {
                self.showCustomErrorAlert(errMessage: "try again")
                return
            }
            self.buyBonusPointList(authToken: tokenAuthStr)
        })
    }
    
    private func buyBonusPointList(authToken: String) {
        
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            
            let urlStr   = String.init(format: Url.buyBonusPointUrl, UserModel.shared.mobileNo)
            let url = getUrl(urlStr: urlStr, serverType: .bonusPointApi)
            let params = [String: Any]()
            web.genericClassRegLoyalty(url: url, param: params, httpMethod: "GET", mScreen: "mbuyBonusPoint", authToken: authToken)
        } else {
            
        }
    }
    
    private func buyBonusAction(authToken: String, model : buyBonusPointModel?) {
        
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            
            let urlStr   = String.init(format: Url.buyBonusOrderNewUrl)
            let url = getUrl(urlStr: urlStr, serverType: .bonusPointApi)
            let dict = model?.merchant
            //Generate pipepline string
            let strPipeLine = "OkAccountNumber=\(UserModel.shared.mobileNo)|ProductId=\(model?.productId ?? "")|Name=\(dict?.safeValueForKey("shopName") as? String ?? "")|Password=\(ok_password ?? "")"
            let hashValue = strPipeLine.hmac_SHA1(key: Url.sLoyalty_airtime)
            let keys : [String] = ["OkAccountNumber","ProductId","Name","Password","HashValue"]
            let values : [String] = [UserModel.shared.mobileNo, model?.productId ?? "" , dict?.safeValueForKey("shopName") as? String ?? "", ok_password ?? "" , hashValue]
            let jsonObj = JSONStringWriter()
            let jsonStr = jsonObj.writeJsonIntoString(keys, values)
            
            let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url, methodType: kMethod_Post, contentType: kContentType_Json, inputVal: jsonStr, authStr: authToken)
            
            JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
                
                guard isSuccess else
                {
                    //println_debug(" API Fail")
                    self.removeProgressView()
                    return
                }
                
                if let dictResponse = response as? Dictionary<String, Any> {
                    if let statusCode = dictResponse.safeValueForKey("StatusCode") as? Int, statusCode == 201 {
//                        println_debug(dictResponse)
                        DispatchQueue.main.async {
                            self.removeProgressView()
                            let alertMessage = "Purchase was successful".localized
                            loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: #imageLiteral(resourceName: "discount_money"))
                            loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                                self.buyBonusPointList()
                                //self.backAction()
                            })
                            loyaltyAlertView.showAlert(controller: self)
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.removeProgressView()
                            let alertMessage = dictResponse.safeValueForKey("Message") as? String ?? ""
                            loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: #imageLiteral(resourceName: "dashboard_loyalty"))
                            loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                                
                            })
                            loyaltyAlertView.showAlert(controller: self)
                        }
                    }
                }
            })
            
        } else {
            
        }
    }
    
    
    
    fileprivate func reloadTableView() {
        if let index = successIndex {
            self.buyingModelArray.remove(at: index)
            DispatchQueue.main.async {
                if self.buyingModelArray.count == 0 {
                    self.noRecordsView.isHidden = false
                } else {
                    self.buyBonusPointTableView.reloadData()
                }
            }
        }
    }
    
    //Search bar
//    private func addCustomMenuButton() {
//        let searchButton = UIButton(type: .custom)
//        searchButton.setBackgroundImage(UIImage.init(named: "search_white.png"), for: .normal)
//        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: searchButton)
//        searchButton.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
//    }
    
    
//    @objc func searchAction() {
//        if self.navigationItem.titleView == nil {
//            searchBar.placeholder = "Search Name and Number".localized
//          if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: "Zawgyi-One", size: 18) {
//            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
//              searchTextField.font = myFont
//            }
//          }
//            searchBar.frame = (navigation?.navigationBar.frame)!
//            self.navigationItem.titleView = searchBar
//        } else {
//            self.navigationItem.titleView = nil
//        }
//    }

    
    //Sortview Main action
    @IBAction func dateMainButtonAction(_ sender : UIButton) {
        if let dateVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: "LoyaltyDateSelectViewController_ID") as? LoyaltyDateSelectViewController {
            dateVC.delegate = self
            navigation?.pushViewController(dateVC, animated: false)
        }
    }
    
    @IBAction func sortByMainButtonAction(_ sender : UIButton) {
        self.sortMainView.isHidden = false
    }
    
    @IBAction func filterByMainButtonAction(_ sender : UIButton) {
        self.filterByTableView.reloadData()
         self.filterMainView.isHidden = false
    }
    
    // Sory By Sub action
    @IBAction func sortByDefButtonAction(_ sender : UIButton) {
        highLightImage1.image = UIImage(named: "")
        highLightImage2.image = UIImage(named: "category_bill")
        highLightImage3.image = UIImage(named: "")
       
        isFilter = false
        DispatchQueue.main.async {
            self.defSortByImageView.image = #imageLiteral(resourceName: "success")
            self.nameAZSortByImageView.image = nil
            self.nameZASortByImageView.image = nil
            self.buyBonusPointTableView.reloadData()
        }
         self.sortMainView.isHidden = true
    }
    
    @IBAction func sortByAtoZButtonAction(_ sender : UIButton) {
        highLightImage1.image = UIImage(named: "")
        highLightImage2.image = UIImage(named: "category_bill")
        highLightImage3.image = UIImage(named: "")
        var localArray = self.buyingModelArray
        if filterModelArray.count > 0 {
            localArray = filterModelArray
        } else if searchModelArray.count > 0 {
            localArray = searchModelArray
        }
        filterModelArray = localArray.sorted(by: { (firstModel, secondModel) -> Bool in
            let firstMerchant = firstModel.merchant
            let secondMerchant = secondModel.merchant
            return (firstMerchant["shopName"] as? String ?? "") < (secondMerchant["shopName"] as? String ?? "")
        })
        isFilter = true
        DispatchQueue.main.async {
            self.defSortByImageView.image = nil
            self.nameAZSortByImageView.image = #imageLiteral(resourceName: "success")
            self.nameZASortByImageView.image = nil
            self.buyBonusPointTableView.reloadData()
        }
         self.sortMainView.isHidden = true
    }
    
    @IBAction func sortByZtoAButtonAction(_ sender : UIButton) {
        highLightImage1.image = UIImage(named: "")
        highLightImage2.image = UIImage(named: "category_bill")
        highLightImage3.image = UIImage(named: "")
        var localArray = self.buyingModelArray
        if filterModelArray.count > 0 {
            localArray = filterModelArray
        } else if searchModelArray.count > 0 {
            localArray = searchModelArray
        }
        filterModelArray = localArray.sorted(by: { (firstModel, secondModel) -> Bool in
            let firstMerchant = firstModel.merchant
            let secondMerchant = secondModel.merchant
            return (firstMerchant["shopName"] as? String ?? "") > (secondMerchant["shopName"] as? String ?? "")
        })
        isFilter = true
        DispatchQueue.main.async {
            self.defSortByImageView.image = nil
            self.nameAZSortByImageView.image = nil
            self.nameZASortByImageView.image = #imageLiteral(resourceName: "success")
            self.buyBonusPointTableView.reloadData()
        }
         self.sortMainView.isHidden = true
    }
    
    //Filter by sub action
    @IBAction func filterByDefButtonAction(_ sender : UIButton) {
        highLightImage1.image = UIImage(named: "")
        highLightImage3.image = UIImage(named: "category_bill")
        highLightImage2.image = UIImage(named: "")
        searchActive = false
        isFilter = false
        DispatchQueue.main.async {
            self.defFilterByImageView.image = #imageLiteral(resourceName: "success")
            self.okDolTopUpFilterByImageview.image = nil
            self.buyBonusPointTableView.reloadData()
        }
        self.filterMainView.isHidden = true
    }
    
    @IBAction func filterByokTopUpButtonAction(_ sender : UIButton) {
        highLightImage1.image = UIImage(named: "")
        highLightImage3.image = UIImage(named: "category_bill")
        highLightImage2.image = UIImage(named: "")
        var localArray = self.buyingModelArray
        if searchModelArray.count > 0 {
            localArray = searchModelArray
        }
        filterModelArray = localArray.filter({ (singleModel) -> Bool in
            let merchant = singleModel.merchant
            return (merchant["shopName"] as? String ?? "") == "OK DOLLAR TOPUP"
        })
        searchActive = false
        isFilter = true
        DispatchQueue.main.async {
            self.defFilterByImageView.image = nil
            self.okDolTopUpFilterByImageview.image = #imageLiteral(resourceName: "success")
            self.buyBonusPointTableView.reloadData()
        }
        self.filterMainView.isHidden = true
    }
    
    func filterByAction(index: Int) {
        highLightImage1.image = UIImage(named: "")
        highLightImage3.image = UIImage(named: "category_bill")
        highLightImage2.image = UIImage(named: "")
        if index == 0 {
            isFilter = false
            filterByArray[0]["selected"] = true
            for indexL in filterByArray.indices {
                if indexL != index {
                    filterByArray[indexL]["selected"] = false
                }
            }
            DispatchQueue.main.async {
                self.buyBonusPointTableView.reloadData()
                self.filterMainView.isHidden = true
            }
        } else {
            let nameDict = filterByArray[index]
            for indexL in filterByArray.indices {
                if index == indexL {
                    filterByArray[index]["selected"] = true
                } else {
                    filterByArray[indexL]["selected"] = false
                }
            }
            var localArray = self.buyingModelArray
            if searchModelArray.count > 0 {
                localArray = searchModelArray
            }
            filterModelArray = localArray.filter({ (singleModel) -> Bool in
                let merchant = singleModel.merchant
                return (merchant["shopName"] as? String ?? "") == (nameDict["name"] as? String ?? "")
            })
            searchActive = false
            isFilter = true
            DispatchQueue.main.async {
                self.buyBonusPointTableView.reloadData()
                self.filterMainView.isHidden = true
            }
            
        }
    }
    
    //Close button sub action
    @IBAction func sortByCloseButtonAction(_ sender : UIButton) {
        self.sortMainView.isHidden = true
    }
    
    @IBAction func filterByCloseButtonAction(_ sender : UIButton) {
        self.filterMainView.isHidden = true
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "SearchTextChange"), object: nil)
    }
    
}



extension BuyBonusPointViewController : loyaltyDateSelectForSort {
    func okButtonAction(fromDate : Date, toDate : Date) {
        highLightImage3.image = UIImage(named: "")
        highLightImage1.image = UIImage(named: "category_bill")
        highLightImage2.image = UIImage(named: "")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        var date = Date()
        self.isFilter = true
        let calendar = Calendar(identifier: .gregorian)
        guard let dateFromComponents = calendar.date(bySettingHour: 0, minute: 0, second: 0, of: fromDate) else { return }
        guard let dateToComponents = calendar.date(bySettingHour: 23, minute: 59, second: 59, of: toDate) else { return }
        dateFormatter.setLocale()
        self.filterModelArray = self.buyingModelArray.filter({ (singleModel) -> Bool in
            if let expiryDate = singleModel.expiryDate, expiryDate.count > 0 {
                date = dateFormatter.date(from: expiryDate)!
            }
            let range = dateFromComponents...dateToComponents
            return range.contains(date)
        })
        DispatchQueue.main.async {
            self.buyBonusPointTableView.reloadData()
        }
    }
    
    func resetButtonAction() {
        highLightImage1.image = UIImage(named: "")
        highLightImage3.image = UIImage(named: "")
        highLightImage2.image = UIImage(named: "")
        self.isFilter = false
        DispatchQueue.main.async {
            self.buyBonusPointTableView.reloadData()
        }
    }
}

//extension BuyBonusPointViewController : UISearchBarDelegate {
//
//    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        searchActive = true;
//    }
//
//    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        searchActive = false
//
//    }
//
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        searchActive = false
//
//    }
//
//    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//        searchActive = false
//
//    }
//
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        if searchText.count > 0 {
//            searchActive = true
//            if !self.isFilter {
//                self.searchModelArray = self.buyingModelArray.filter({ (singleModel) -> Bool in
//                    let merchant = singleModel.merchant
//                    return (merchant["shopName"] as? String ?? "").containsIgnoringCase(find: searchText)
//                })
//            } else {
//                self.searchModelArray = self.filterModelArray.filter({ (singleModel) -> Bool in
//                    let merchant = singleModel.merchant
//                    return (merchant["shopName"] as? String ?? "").containsIgnoringCase(find: searchText)
//                })
//            }
//        } else {
//            searchActive = false
//        }
//        DispatchQueue.main.async {
//            self.buyBonusPointTableView.reloadData()
//        }
//    }
//}



extension BuyBonusPointViewController : BuyPointsButtonProtocol {
    func buyButtonAction(model: buyBonusPointModel?, index: Int?) {
        self.successIndex = index
        self.getTokenFromServer(completionHandler: { (isSuccess, tokenString) in
            guard isSuccess, let tokenAuthStr = tokenString as? String else {
                self.showCustomErrorAlert(errMessage: "try again")
                return
            }
            DispatchQueue.main.async(execute: {
                self.buyBonusAction(authToken: tokenAuthStr, model : model)
            })
        })
    }
}

extension BuyBonusPointViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == filterByTableView {
            self.filterByAction(index: indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == filterByTableView { return 55 }
        return 90
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == filterByTableView { return self.filterByArray.count }
        if !searchActive {
            if isFilter {
                return self.filterModelArray.count
            }
            return self.buyingModelArray.count
        } else {
            return self.searchModelArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == filterByTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "buyBonusFilterTableCell", for: indexPath) as! FilterByBuyBonusCell
            cell.wrapData(dictionary: filterByArray[indexPath.row])
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "buyBonusTableCell", for: indexPath) as! BuyBonusPointTableViewCell
            cell.delegate = self
            if !searchActive {
                if isFilter {
                    cell.wrapData(dictionary: self.filterModelArray[indexPath.row], index: indexPath.row)
                } else {
                    cell.wrapData(dictionary: self.buyingModelArray[indexPath.row], index: indexPath.row)
                }
            } else {
                cell.wrapData(dictionary: self.searchModelArray[indexPath.row], index: indexPath.row)
            }
            return cell
        }
    }
}


extension BuyBonusPointViewController : WebServiceResponseDelegate {
    
    func webResponse(withJson json: AnyObject, screen: String) {
        DispatchQueue.main.async {
            self.refreshControl.endRefreshing()
        }
        if screen == "mbuyBonusPoint" {
            do {
                if let data = json as? Data {
                    if let responseDict = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Dictionary<String , Any> {
                        self.buyingModelArray.removeAll()
                        if let statusCode = responseDict.safeValueForKey("StatusCode") as? Int, statusCode == 302 {
                            if let records = responseDict.safeValueForKey("Results") as? [Dictionary<String, Any>] {
                                for singleRec in records {
                                    let model = buyBonusPointModel.init(dictionary: singleRec)
                                    self.buyingModelArray.append(model)
                                    let status = self.filterByArray.contains { (filterShop) -> Bool in
                                        return filterShop["name"] as? String == model.merchant["shopName"] as? String ?? ""
                                    }
                                    if !status {
                                        var dict = Dictionary<String, Any>()
                                        dict["name"] = model.merchant["shopName"] as? String ?? ""
                                        dict["selected"] = false
                                        filterByArray.append(dict)
                                    }
                                }
                            }
                        } else {
                            if let statusCode = responseDict.safeValueForKey("StatusCode") as? Int, statusCode == 404 {
                                DispatchQueue.main.async {
                                    let alertMessage = responseDict.safeValueForKey("Message") as? String ?? ""
                                    loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: #imageLiteral(resourceName: "dashboard_loyalty"))
                                    loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                                        
                                    })
                                    loyaltyAlertView.showAlert(controller: self)
                                }
                            }
                        }
                        DispatchQueue.main.async {
                            if self.buyingModelArray.count > 1 {
                                self.sortStackView.isHidden = false
                                self.sortViewHeightConstraint.constant = 45.0
                            }
                            if self.buyingModelArray.count > 0 {
                                self.noRecordsView.isHidden = true
                            } else {
                                self.noRecordsView.isHidden = false
                                self.sortStackView.isHidden = true
                                self.sortViewHeightConstraint.constant = 0.0
                            }
                            self.buyBonusPointTableView.reloadData()
                        }
                        
                        if self.buyingModelArray.count < 2 {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "BuyBonusPointSearchHide"), object: nil)
                        }
                        //                        println_debug(responseDict)
                    }
                }
            } catch {
                
            }
        } else if screen == "mbuyBonusPointAction" {
            do {
                if let data = json as? Data {
                    if let responseDict = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Dictionary<String , Any> {
                        if let statusCode = responseDict.safeValueForKey("StatusCode") as? Int, statusCode == 302 {
                            self.reloadTableView()
                        } else {
                            if let statusCode = responseDict.safeValueForKey("StatusCode") as? Int, statusCode == 404 {
                                DispatchQueue.main.async {
                                    let alertMessage = responseDict.safeValueForKey("Message") as? String ?? ""
                                    loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: #imageLiteral(resourceName: "dashboard_loyalty"))
                                    loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                                        
                                    })
                                    loyaltyAlertView.showAlert(controller: self)
                                }
                            }
                        }
                    }
                }
            } catch {
                
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            PTLoader.shared.hide()
        }
    }
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        PTLoader.shared.hide()
    }
}

protocol BuyPointsButtonProtocol : class {
    func buyButtonAction(model : buyBonusPointModel?, index : Int?)
}

class BuyBonusPointTableViewCell : UITableViewCell {
    
    @IBOutlet weak var nameImageView : UIImageView! {
        didSet {
            self.nameImageView.layer.cornerRadius = self.nameImageView.frame.size.width / 2.0
            self.nameImageView.layer.borderWidth = 0.5
            self.nameImageView.layer.borderColor = UIColor.gray.cgColor
            self.nameImageView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        }
    }
    
    @IBOutlet weak var nameLabel : UILabel!{
        didSet {
            self.nameLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var shopNameLabel : UILabel!{
        didSet {
            self.shopNameLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var originalPriceLbl : UILabel!{
        didSet {
            self.originalPriceLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var buyingPriceLbl : UILabel!{
        didSet {
            self.buyingPriceLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var expiryDateLbl : UILabel!{
        didSet {
            self.expiryDateLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var buybutton : UIButton! {
        didSet {
            self.buybutton.layer.cornerRadius = 1.0
            self.buybutton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.buybutton.setTitle(self.buybutton.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    var model : buyBonusPointModel? = nil
    var indexLocal : Int?
    weak var delegate : BuyPointsButtonProtocol?
    override func awakeFromNib() {
        
    }
    
    
    func wrapData(dictionary : buyBonusPointModel, index : Int) {
        model = dictionary
        indexLocal = index
        let dict = dictionary.merchant
        if let name = (dict.safeValueForKey("shopName") as? String)?.uppercased() {
            self.nameLabel.text = String(name.first!)
        }
        self.shopNameLabel.text = dict.safeValueForKey("shopName") as? String ?? ""
        
        if let originalPoints = dictionary.originalPrice {
            self.originalPriceLbl.text = wrapAmountWithCommaDecimal(key: "\(originalPoints)").replacingOccurrences(of: ".0", with: "") + " Points".localized
        }
        if let buyingPrice = dictionary.buyingPrice {
            self.buyingPriceLbl.text = wrapAmountWithCommaDecimal(key: "\(buyingPrice)").replacingOccurrences(of: ".0", with: "") + " MMK".localized
        }
        if let expDate = dictionary.expiryDate {
            if let date = expDate.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss") {
                self.expiryDateLbl.text = "Expiry Date".localized + ": " + date.stringValue(dateFormatIs: "dd-MMM-yyyy")
            }
        }
    }
    
    @IBAction func buyButtonAction(_ sender : UIButton) {
        self.delegate?.buyButtonAction(model: model, index : indexLocal)
    }
}


class FilterByBuyBonusCell: UITableViewCell {
    @IBOutlet weak var agentLabel: UILabel!{
        didSet {
            self.agentLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var selectedImageView: UIImageView!
    
    func wrapData(dictionary: Dictionary<String, Any>) {
        agentLabel.text = dictionary["name"] as? String ?? ""
        if dictionary["selected"] as? Bool == true {
            selectedImageView.image = UIImage(named: "success")
        } else {
            selectedImageView.image = UIImage(named: "")
        }
    }
}
