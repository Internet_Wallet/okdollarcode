//
//  LoyaltyBaseViewController.swift
//  OK
//
//  Created by Kethan on 7/4/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class LoyaltyBaseViewController: OKBaseController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        appDel.floatingButtonControl?.window.button?.isHidden = true
//        appDel.floatingButtonControl?.closeButton.isHidden = true
    }
    
    
    func addCustomBackButton(navTitle: String) {
        let backButton = UIButton(type: .custom)
        backButton.setBackgroundImage(#imageLiteral(resourceName: "tabBarBack"), for: .normal)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        navigationItem.title = navTitle.localized
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white ]
        self.navigationController?.navigationBar.barTintColor = UIColor.red
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    
    @objc override func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func getTokenFromServer(completionHandler: @escaping (Bool,Any?) -> Void) {
        
        let urlStr   = String.init(format: Url.loyaltyTokenApiUrl)
        let tokenUrl = getUrl(urlStr: urlStr, serverType: .bonusPointApi)
        
        println_debug(tokenUrl)
        let hashValue = Url.aLoyalty_airtime.hmac_SHA1(key: Url.sLoyalty_airtime)
        let inputVal = "password=\(hashValue)&grant_type=password"
        
        progressViewObj.showProgressView()
        //println_debug("get token from server called........")
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: kMethod_Post, contentType: kContentType_urlencoded, inputVal: inputVal, authStr: nil)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    //println_debug("remove progress view called inside get token from server call")
                }
                
                completionHandler(false,nil)
                return
            }
            
            //println_debug("response dict for get token from server :: \(String(describing: response))")
            let responseDic = response as! NSDictionary
            guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                
                completionHandler(false,nil)
                return
            }
            
            let authorizationString =  "\(tokentype) \(accesstoken)"
            completionHandler(true,authorizationString)
        })
    }

    func showCustomErrorAlert(errMessage: String) {
        DispatchQueue.main.async {
            loyaltyAlertView.wrapAlert(title: nil, body: errMessage.localized, img: #imageLiteral(resourceName: "alert-icon"))
            loyaltyAlertView.addAction(title: "OK".localized, style: .target , action: {
            })
            loyaltyAlertView.showAlert(controller: self)
        }
    }
    
    func getFormattedNumberInLoyalty(number: String) -> String {
        var forNum = ""
        var number1 = number
        if number.hasPrefix("0095") {
            number1 = String(number1.dropFirst(4))
            forNum = "0\(number1)"
        } else {
            number1 = String(number1.dropFirst(4))
            forNum = "\(number1)"
        }
        return forNum
    }

}

import CoreData
class LoyaltyCoreDataManager {
    
    static let sharedInstance = LoyaltyCoreDataManager()
    let managedContext  =  appDelegate.persistentContainer.viewContext
    
    // MARK: - Inserting Transaction Records
    func insertLoyaltyPointsInfo(dataToInsert: LoyaltyDBData) {
        let insertDataTo = ReportLoyalty(context: managedContext)
        insertDataTo.balancePoint = dataToInsert.balancePoint
        insertDataTo.dateTime = dataToInsert.dateTime
        insertDataTo.destinationNo = dataToInsert.destinationNo
        insertDataTo.merchantNo = dataToInsert.merchantNo
        insertDataTo.shopName = dataToInsert.shopName
        insertDataTo.transferPoint = dataToInsert.transferPoint
        insertDataTo.transID = dataToInsert.transID
        insertDataTo.type = dataToInsert.type
        self.saveContext()
    }
    
    // MARK: - Saving Context
    func saveContext() {
        let managedContext  =  appDelegate.persistentContainer.viewContext
        do {
            try managedContext.save()
        } catch let error as NSError {
            println_debug("could not save \(error.localizedDescription)")
        }
    }
    
    // MARK: - Fetching Loyalty Records
    func fetchLoyaltyRecords() -> [ReportLoyalty] {
        let fetchReq = NSFetchRequest<NSManagedObject>(entityName: "ReportLoyalty")
        let sortDescr = NSSortDescriptor(key: #keyPath(ReportLoyalty.dateTime), ascending: false)
        fetchReq.sortDescriptors = [sortDescr]
        do {
            guard let fetchedObjects    = try managedContext.fetch(fetchReq) as? [ReportLoyalty] else { return [] }
            return fetchedObjects
        } catch {
            println_debug("Fetching operation failed \(error.localizedDescription)")
        }
        return []
    }
}

struct LoyaltyDBData {
    var balancePoint: String
    var dateTime: Date?
    var destinationNo: String
    var merchantNo: String
    var shopName: String
    var transferPoint: String
    var transID: String
    var type: String
    
    init(balancePoint: String, dateTime: Date?, destinationNo: String, merchantNo: String,
         shopName: String, transferPoint: String, transID: String, type: String) {
        self.balancePoint = balancePoint
        self.dateTime = dateTime
        self.destinationNo = destinationNo
        self.merchantNo = merchantNo
        self.shopName = shopName
        self.transferPoint = transferPoint
        self.transID = transID
        self.type = type
    }
}
