//
//  BonusConfigSubViewController.swift
//  OK
//
//  Created by Kethan on 2/27/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct bonusConfigSubAPIModel {
    var maximumRange : Int?
    var minimumRange : Int?
    var perAmount : Int?
    var bonusPoint : Int?
    var startDate : String?
    var endDate : String?
    var expiryDate : String?
    
    init(dict : Dictionary<String, Any>) {
        if let maxRange = dict["MaximumRange"] as? Int {
            maximumRange = maxRange
        }
        if let minRange = dict["MinimumRange"] as? Int {
            minimumRange = minRange
        }
        if let amount = dict["PerAmount"] as? Int {
            perAmount = amount
        }
        if let bonusPt = dict["BonusPoint"] as? Int {
            bonusPoint = bonusPt
        }
        if let stDate = dict["StartDate"] as? String {
            startDate = stDate
        }
        if let edDate = dict["EndDate"] as? String {
            endDate = edDate
        }
        if let exDate = dict["ExpiryDate"] as? String {
            expiryDate = exDate 
        }
    }
}

class DisplayBonusConfigViewController: LoyaltyBaseViewController {
    
    var bonusConfigListArray = Array<Dictionary<String, Any>>()

    var bonusConfigDict = Dictionary<String,Any>()

    @IBOutlet weak var showAddBonusConfigButton : UIButton! {
        didSet {
            self.showAddBonusConfigButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.showAddBonusConfigButton.layer.masksToBounds = true
            self.showAddBonusConfigButton.layer.cornerRadius = self.showAddBonusConfigButton.frame.size.width / 2.0
        }
    }
    @IBOutlet weak var configTableView : UITableView!
    @IBOutlet weak var noRecordsView : UIView!
    @IBOutlet weak var noRecordsLbl : UILabel! {
        didSet {
            self.noRecordsLbl.font = UIFont(name: appFont, size: appFontSize)
            self.noRecordsLbl.text = self.noRecordsLbl.text?.localized
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.noRecordsView.isHidden = false
        addCustomBackButton(navTitle: "Bonus Configuration")
        configTableView.tableFooterView = UIView()
        if UserModel.shared.agentType == .agent {
            self.noRecordsView.isHidden = false
        }else {
            self.getBonusConfigDetails()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func showAddConfigBonusVC(selectedModel : Dictionary<String, Any>?) {
        if let addNewBonsConfigVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: "AddNewBonusConfigViewController_ID") as? AddNewBonusConfigViewController {
            addNewBonsConfigVC.delegate = self
            addNewBonsConfigVC.selectedModel  = selectedModel
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(addNewBonsConfigVC, animated: true)
            }
        }
    }
    
    @IBAction func showAddBonusConfigViewController(_ sender : UIButton) {
        showAddConfigBonusVC(selectedModel : nil)
    }
    
    
    private func getBonusConfigDetails() {
        
        if appDelegate.checkNetworkAvail() {
            DispatchQueue.main.async {
                let web      = PayToWebApi()
                web.delegate = self
                var ip = ""
                if let ipAdd = OKBaseController.getIPAddress() {
                    ip = ipAdd
                }
                let urlStr   = String.init(format: Url.bonusConfigApiUrl, UserModel.shared.mobileNo, ip , "ios", UserLogin.shared.token)
                let url = getUrl(urlStr: urlStr, serverType: .serverEstel)
                let pRam = Dictionary<String,String>()
                web.genericClassXML(url: url, param: pRam as AnyObject, httpMethod: "POST", mScreen: "mBonusConfigList")
            }
        }else {
            self.noInternetAlert()
        }
    }
    
}


//PTWebresponseDelegate
extension DisplayBonusConfigViewController : PTWebResponseDelegate {
    func webSuccessResult(data: Any, screen: String) {
        self.removeProgressView()
        if screen == "mBonusConfigList" {
            if let xmlString = data as? String {
                let xml = SWXMLHash.parse(xmlString)
                
                self.bonusConfigDict.removeAll()
                self.bonusConfigListArray.removeAll()
                self.enumerateAlert(indexer: xml)
                
                DispatchQueue.main.async {
                    if self.bonusConfigListArray.count > 0 {
                        self.noRecordsView.isHidden = true
                    }else {
                        self.noRecordsView.isHidden = false
                    }
                    self.configTableView.reloadData()
                }
            }
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            bonusConfigDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func enumerateAlert(indexer: XMLIndexer) {
        for child in indexer.children {
            if child.element?.name == "record" {
                self.enumerate(indexer: child)
                bonusConfigListArray.append(bonusConfigDict)
                bonusConfigDict.removeAll()
            }
            enumerateAlert(indexer: child)
        }
    }
    
    func webFailureResult(screen: String) {
        println_debug("Failure")
    }
}

extension DisplayBonusConfigViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 245
    }
}

extension DisplayBonusConfigViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: BonusConfigSubTableViewCell = tableView.dequeueReusableCell(withIdentifier: "bonusConfigSubTableViewCell", for: indexPath) as! BonusConfigSubTableViewCell
        cell.loadCell(bonusConfigSubArray: bonusConfigListArray[indexPath.row])
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bonusConfigListArray.count        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showAddConfigBonusVC(selectedModel : bonusConfigListArray[indexPath.row])
    }
}

extension DisplayBonusConfigViewController : CreateNewBonusConfig {
    func newOrUpdateBonusConfig() {
        self.getBonusConfigDetails()
    }
}

