//
//  PointDistributionViewController.swift
//  OK
//
//  Created by Kethan on 3/3/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol pointDistributionProtocol : class {
    func currentPoint(pointText : String)
}

class PointDistributionViewController: LoyaltyBaseViewController, UINavigationControllerDelegate, UITextFieldDelegate, CountryLeftViewDelegate, CountryViewControllerDelegate, PTWebResponseDelegate, BioMetricLoginDelegate {
    
    var reciptModel : loyaltyPointTrfModel?

    func clearActionType() {
        // added in delegate
    }
    var availablePoint = "0.0"
    @IBOutlet weak var heightConfMobNum: NSLayoutConstraint!
    let mobileNumberAcceptableCharacters = "0123456789"
    @IBOutlet weak var pointsLabel: UILabel!{
        didSet{
            pointsLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
     @IBOutlet weak var pointsLabelValue: UILabel!
    @IBOutlet weak var mobileNumberTxt: BonuspointSubClassTextField! {
        didSet {
            self.mobileNumberTxt.font = UIFont(name: appFont, size: appFontSize)
            mobileNumberTxt.placeholder = "Enter Mobile No".localized
        }
    }
    @IBOutlet weak var confirmMobileNumberTxt: BonuspointSubClassTextField! {
        didSet {
            self.confirmMobileNumberTxt.font = UIFont(name: appFont, size: appFontSize)
            self.confirmMobileNumberTxt.placeholder = "Enter Confirm Mobile Number".localized
        }
    }
    @IBOutlet weak var pointsText: UITextField! {
        didSet {
            self.pointsText.font = UIFont(name: appFont, size: appFontSize)
            self.pointsText.placeholder = "Enter Points".localized
        }
    }
    @IBOutlet weak var expiryDaysText: UITextField! {
        didSet {
            self.expiryDaysText.font = UIFont(name: appFont, size: appFontSize)
            self.expiryDaysText.placeholder = "Enter Expiry Days".localized
        }
    }
        
    let validObj  = PayToValidations()
    var contactSuggessionView   : ContactSuggestionVC?
    @IBOutlet var imageFile: UIImage!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var addFrmFavBtn : UIButton!
        {
        didSet {
            self.addFrmFavBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.addFrmFavBtn.layer.masksToBounds = true
            self.addFrmFavBtn.layer.cornerRadius = 5.0
            self.addFrmFavBtn.setTitle(self.addFrmFavBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    
    @IBOutlet weak var addFrmOKAccountBtn : UIButton!
        {
        didSet {
            self.addFrmOKAccountBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.addFrmOKAccountBtn.layer.masksToBounds = true
            self.addFrmOKAccountBtn.layer.cornerRadius = 5.0
            self.addFrmOKAccountBtn.setTitle(self.addFrmOKAccountBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var buttonView : UIView!
        {
        didSet {
            self.buttonView.isHidden = true
        }
    }
    
    @IBOutlet weak var mobileNumberView : UIView!
        {
        didSet {
            self.mobileNumberView.isHidden = false
        }
    }
    
    @IBOutlet weak var confirmMobileNumberView : UIView! {
        didSet {
            self.confirmMobileNumberView.isHidden = true
        }
    }
    
    @IBOutlet weak var expiryDaysView : UIView!
        {
        didSet {
            self.expiryDaysView.isHidden = true
        }
    }
    @IBOutlet weak var pointsView : UIView!
        {
        didSet {
            self.pointsView.isHidden = true
        }
    }
    
    @IBOutlet weak var transferButton : UIButton!
        {
        didSet {
            self.transferButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.transferButton.isHidden = true
            self.transferButton.setTitle(self.transferButton.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    
    struct CountryPayTo {
        var code = ""
        var flag = ""
        
        init(code: String, flag: String) {
            self.code = code
            self.flag = flag
        }
    }
    
    weak var delegate : pointDistributionProtocol?
    @IBOutlet weak var tblPerson: UITableView!
    
    var countryObject = CountryPayTo.init(code: "+95", flag: "myanmar")
    var leftViewCountry                = PaytoViews.updateView()
    var leftViewCountryForConfirmation = PaytoViews.updateView()
        
    var pointListArray = [Dictionary<String,Any>]()
    var pointDict = Dictionary<String,Any>()
    var strEnterPointValue = ""

    
    //AccessoryView
    var submitView: UIView {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 50))
        let btn = UIButton.init(frame: view.frame)
        btn.backgroundColor = UIColor.red
        btn.setTitle("Transfer".localized, for: .normal)
        if let myFont = UIFont(name: appFont, size: appButtonSize) {
            btn.titleLabel?.font =  myFont
        }
        btn.addTarget(self, action: #selector(transferButtonAction(_:)), for: UIControl.Event.touchUpInside)
        view.addSubview(btn)
        return view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //initialSetUp()
        println_debug("PointDistributionViewController")
        self.pointsLabelValue.text = availablePoint
        // Do any additional setup after loading the view.
        //getPointDistribution()
        
        expiryDaysText.inputAccessoryView = self.submitView
        expiryDaysText.inputAccessoryView?.isHidden = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
        initialSetUp()
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
        self.hideContactSuggesionView()
    }
    
    fileprivate func showAccessoryView(status : Bool) {
        if status {
            expiryDaysText.inputAccessoryView?.isHidden = false
        } else {
            expiryDaysText.inputAccessoryView?.isHidden = true
        }
    }
    
    private func initialSetUp() {
        addCustomBackButton(navTitle : "Point Distribution")
        pointsLabel.text = "My Available Point".localized
        self.pointsText.delegate = self
        self.expiryDaysText.delegate = self
        self.loadContactSuggessionView()
        self.updateViewsInitialLogic()
        self.mobileNumberTxt.text?.removeAll()
        self.confirmMobileNumberTxt.text?.removeAll()
        self.wrapMobileLeftView()
        //self.mobileNumberTxt.text = ""
        self.confirmMobileNumberTxt.text = ""
        self.pointsText.text = ""
        self.expiryDaysText.text = ""
        self.mobileNumberTxt.placeholder = mobileNumberTxt.placeholder?.localized
        self.mobileNumberTxt.text = commonPrefixMobileNumber
        //self.pointsText.placeholder = self.pointsText.placeholder?.localized
        //self.expiryDaysText.placeholder = self.expiryDaysText.placeholder?.localized
        self.confirmMobileNumberView.isHidden = true
        self.expiryDaysView.isHidden = true
        self.pointsView.isHidden = true
        self.transferButton.isHidden = true
        self.showAccessoryView(status : false)
        if let clearButton = self.view.viewWithTag(141) as? UIButton {
            clearButton.isHidden = true
        }
        if let clearButton = self.view.viewWithTag(142) as? UIButton {
            clearButton.isHidden = true
        }
        if let clearButton = self.view.viewWithTag(143) as? UIButton {
            clearButton.isHidden = true
        }
        if let clearButton = self.view.viewWithTag(144) as? UIButton {
            clearButton.isHidden = true
        }
        self.view.endEditing(true)
        //heightConfMobNum.constant = 0
        
    }
    
    private func setUpRepeat(phoneNumber: String, repeatPay: Bool) {
        if !repeatPay {
            let countryCode = String.init(format: "(%@)", "+95")
            countryObject.flag = "myanmar"
            countryObject.code = "+95"
            leftViewCountry.wrapCountryViewData(img: "myanmar", str: countryCode)
            leftViewCountryForConfirmation.wrapCountryViewData(img: "myanmar", str: countryCode)
            self.mobileNumberTxt.text = (self.countryObject.code == "+95") ? "09" : ""
            if let clearButton = self.view.viewWithTag(141) as? UIButton {
                clearButton.isHidden = true
            }
            self.pointsView.isHidden = true
            self.heightConfMobNum.constant = 0
            self.mobileNumberTxt.becomeFirstResponder()
        } else {
        let countryArray = revertCountryArray() as! Array<NSDictionary>
        let cDetails = self.identifyCountry(withPhoneNumber: phoneNumber, inCountryArray: countryArray) // 1 -> flag, 0 -> code
        var countryCode = String.init(format: "(%@)", "+95")
        if cDetails.0 == "" || cDetails.1 == "" {
            countryObject.flag = "myanmar"
            countryObject.code = "+95"
            leftViewCountry.wrapCountryViewData(img: "myanmar", str: countryCode)
            leftViewCountryForConfirmation.wrapCountryViewData(img: "myanmar", str: countryCode)
        } else {
            countryObject.flag = cDetails.1
            countryObject.code = cDetails.0
            countryCode = String.init(format: "(%@)", cDetails.0)
            leftViewCountry.wrapCountryViewData(img: cDetails.1, str: countryCode)
            leftViewCountryForConfirmation.wrapCountryViewData(img: cDetails.1, str: countryCode)
        }
        self.mobileNumberTxt.leftView     = leftViewCountry
        self.mobileNumberTxt.leftViewMode = UITextField.ViewMode.always
        
        self.view.layoutIfNeeded()
        
        var phone = ""
        
        phone = self.getFormattedNumberInLoyalty(number: phoneNumber)
        phone = phone.replacingOccurrences(of: " ", with: "")
        self.mobileNumberTxt.text = phone
        
        
        let validations = PayToValidations().getNumberRangeValidation(self.mobileNumberTxt.text ?? "")
        if validations.isRejected || phoneNumber.count < validations.min {
            loyaltyAlertView.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: UIImage(named : "AppIcon"))
            loyaltyAlertView.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                self.mobileNumberTxt.text = (self.countryObject.code == "+95") ? "09" : ""
                if let clearButton = self.view.viewWithTag(141) as? UIButton {
                    clearButton.isHidden = true
                }
            })
            loyaltyAlertView.showAlert(controller: self)
            return
        }
        
        self.nameORnumber(selectiontype: .name, codeDetails: cDetails)
        self.pointsView.isHidden = false
        if let clearButton = self.view.viewWithTag(141) as? UIButton {
            clearButton.isHidden = false
        }
        mobileNumberTxt.resignFirstResponder()
        self.heightConfMobNum.constant = 0
        _verifyInBackEnd(number: phone, completion: { (isFound) in
            self.view.layoutIfNeeded()
        })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        buttonView.isHidden = true
    }
    
    private func configureMobileNumber(mobileNo : String)   {
        mobileNumberTxt.text?.removeAll()
        var mobileNumber = mobileNo
        if mobileNumber != "" {
            let countryArray = revertCountryArray() as! Array<NSDictionary>
            let cDetails = self.identifyCountry(withPhoneNumber: mobileNumber, inCountryArray: countryArray) // 1 -> flag, 0 -> code
            
            if cDetails.0 == "" || cDetails.1 == "" {
                countryObject.flag = "myanmar"
                countryObject.code = "+95"
                leftViewCountry.wrapCountryViewData(img: "myanmar", str: "+95")
                leftViewCountryForConfirmation.wrapCountryViewData(img: "myanmar", str: "+95")
            } else {
                countryObject.flag = cDetails.1
                countryObject.code = cDetails.0
                leftViewCountry.wrapCountryViewData(img: cDetails.1, str: cDetails.0)
                leftViewCountryForConfirmation.wrapCountryViewData(img: cDetails.1, str: cDetails.0)
            }
            
            mobileNumber =  mobileNumber.replacingOccurrences(of: cDetails.0, with: "0")
            self.mobileNumberTxt.text = mobileNumber
        }
        else
        {
            countryObject.flag = "myanmar"
            countryObject.code = "+95"
            leftViewCountry.wrapCountryViewData(img: "myanmar", str: "+95")
            leftViewCountryForConfirmation.wrapCountryViewData(img: "myanmar", str: "+95")
        }
        pointsView.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func hideContactSuggesionView() {
        if let contactview = contactSuggessionView {
            contactview.view.isHidden = true
        }
    }
    
    @IBAction func addFromFavoriteAction(_ sender : UIButton) {
        let vc = self.openFavoriteFromNavigation(self, withFavorite: .payto)
        //self.navigationController?.present(vc, animated: true, completion: nil)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func addFromOKAccountAction(_ sender : UIButton) {
        let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
        self.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func hideAndShowContactsButton(_ sender : UIButton) {
        if buttonView.isHidden == true {
            buttonView.isHidden = false
        } else {
            buttonView.isHidden = true
        }
    }
    
    private func  updateAvailablePoints(points : String) {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        if let numberStr = formatter.string(from: Float(points)! as NSNumber) {
            self.pointsLabelValue.text = numberStr
        }
        self.delegate?.currentPoint(pointText: self.pointsLabelValue.text!)
    }

    func openAction(type: ButtonType) {
        self.navigationController?.present(countryViewController(delegate: self), animated: true, completion: nil)
    }
    
    private func loadContactSuggessionView() {
        contactSuggessionView  =  UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "ContactSugestView_ID") as? ContactSuggestionVC
        contactSuggessionView?.delegate   = self //as? ContactSuggestionDelegate
        contactSuggessionView?.view.frame = CGRect.zero
        addChild(contactSuggessionView!)
        self.view.addSubview(contactSuggessionView!.view)
        contactSuggessionView!.didMove(toParent: self)
        contactSuggessionView?.view.isHidden = true
    }
    
    
    func nameORnumber(selectiontype: selectionType, codeDetails: (String, String)) {
        self.view.endEditing(true)
        switch selectiontype {
        case .name:
            _ = DefaultIconView.updateView(icon: "name")
        case .number:
            leftViewCountryForConfirmation.wrapCountryViewData(img: codeDetails.1, str: codeDetails.0)
            self.heightConfMobNum.constant = 60
            self.confirmMobileNumberView.isHidden = true
            self.expiryDaysView.isHidden = true
            self.pointsView.isHidden = true
            
            self.confirmMobileNumberTxt.text = ""
            self.pointsText.text = ""
            self.expiryDaysText.text = ""
        }
    }
    
    private func rejectNumberAlgorithm() -> Bool {
        self.mobileNumberTxt.text = commonPrefixMobileNumber
        self.showCustomErrorAlert(errMessage: "Rejected Number".localized)
        return false
    }
    
    fileprivate func getSuggessionviewFrame(arr: Int) -> CGRect {
        var height          : CGFloat = CGFloat(Float(arr) * 70.0)
        let cellHgt         : CGFloat = 70.0
        let normalHeaderHgt : CGFloat = 40.0
        
        let maxHeight = screenHeight - 200 - cellHgt - normalHeaderHgt // - cellHgt
        if arr > 5 {
            height = maxHeight
        }
        var yPos = 205.0
        if device.type == .iPhoneX || device.type == .iPhoneXR || device.type == .iPhoneXS || device.type == .iPhoneXSMax {
            yPos = 220.0
        }
        return CGRect(x: 50.0, y: yPos, width: Double(screenWidth - 60.0), height: Double(height))
    }
    
    func showContactSuggessionView(withFrame frame: CGRect, andContacts contactList: [Dictionary<String, Any>]) {
        contactSuggessionView?.view.isHidden    = false
        contactSuggessionView?.view.frame       = frame
        contactSuggessionView?.contactsList     = contactList
        contactSuggessionView?.contactsTable.reloadData()
        contactSuggessionView?.view.layoutIfNeeded()
    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        contactSuggessionView?.view.isHidden = true
    }
    
    
    func confirmMobileNumberHandler(textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let textCount  = text.count
        self.transferButton.isHidden = true
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            if countryObject.code == "+95" {
                self.pointsText.text = ""
                if let clearButton = self.view.viewWithTag(143) as? UIButton {
                    clearButton.isHidden = true
                }
                if let clearButton = self.view.viewWithTag(144) as? UIButton {
                    clearButton.isHidden = true
                }
                self.confirmMobileNumberTxt.isHidden = false
                self.pointsView.isHidden = true
                self.expiryDaysView.isHidden = true
                self.expiryDaysText.text = ""
                if countryObject.code == "+95", (textField.text == "09" || text == "09") {
                    self.confirmMobileNumberTxt.text = "09"
                    if let clearButton = self.view.viewWithTag(142) as? UIButton {
                        clearButton.isHidden = true
                    }
                    return false
                }
                
            }
        }
        if self.mobileNumberTxt.text?.count == textCount, String(describing: self.mobileNumberTxt!.text!.last!) == string {
            self.confirmMobileNumberTxt.resignFirstResponder()
            _verifyInBackEnd(number: text, completion: { (isFound) in
                
                self.pointsView.isHidden = false
                self.pointsText.becomeFirstResponder()
                self.view.layoutIfNeeded()
            })
        }
        if validObj.checkMatchingNumber(string: text, withString: self.mobileNumberTxt.text!) {
            self.confirmMobileNumberTxt.text = text
            if let clearButton = self.view.viewWithTag(142) as? UIButton {
                if textCount > 0 {
                    clearButton.isHidden = false
                } else {
                    clearButton.isHidden = true
                }
            }
            return false
        } else {  return false  }
    }
    
    func MobileNumberHandler(textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let textCount  = text.count
        self.transferButton.isHidden = true
       let validation = PayToValidations().getNumberRangeValidation(text)
        // Normal Validations
        guard validation.isRejected == false else {
            loyaltyAlertView.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: UIImage(named : "AppIcon"))
            loyaltyAlertView.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                if let clearButton = self.view.viewWithTag(141) as? UIButton {
                    clearButton.isHidden = true
                }
                textField.text = commonPrefixMobileNumber
            })
            loyaltyAlertView.showAlert(controller: self)
            return false
        }
        
        if let clearButton = self.view.viewWithTag(141) as? UIButton {
            if textCount > 0 {
                clearButton.isHidden = false
            } else {
                clearButton.isHidden = true
            }
        }
        
        let contactList = self.contactSuggesstion(text)
        if contactList.count > 0 {
            // here show the contact suggession screen
            let rect = self.getSuggessionviewFrame(arr: contactList.count)
            self.showContactSuggessionView(withFrame: rect, andContacts: contactList)
            
        } else {
            // here hide the contact suggession screen
            self.hideContactSuggesionView()
        }
         
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            self.confirmMobileNumberTxt.isUserInteractionEnabled = true
            self.confirmMobileNumberTxt.placeholder = "Confirm Mobile Number".localized
            self.confirmMobileNumberTxt.isEnabled = true
//            self.confirmMobileNumberTxt.leftView = nil
            let countryCode = String.init(format: "(%@)", countryObject.code)
            leftViewCountryForConfirmation.wrapCountryViewData(img: countryObject.flag , str: countryCode)
            self.confirmMobileNumberTxt.leftView = leftViewCountryForConfirmation
            self.confirmMobileNumberTxt.leftViewMode  = UITextField.ViewMode.always
            if countryObject.code == "+95", textField.text == "09" {
                self.hideContactSuggesionView()
            } else {
                if text.count == 0 {
                    self.hideContactSuggesionView()
                }
            }
            
            if countryObject.code == "+95", (textField.text == "09" || text == "09") {
                self.mobileNumberTxt.text = "09"
                if let clearButton = self.view.viewWithTag(141) as? UIButton {
                    clearButton.isHidden = true
                }
                return false
            }
        }
        self.confirmMobileNumberTxt.text = (countryObject.code == "+95") ? "09" : ""
        self.pointsText.text = ""
        self.pointsView.isHidden = true
        self.expiryDaysView.isHidden = true
        self.expiryDaysText.text = ""
        if let clearButton = self.view.viewWithTag(143) as? UIButton {
            clearButton.isHidden = true
        }
        if let clearButton = self.view.viewWithTag(144) as? UIButton {
            clearButton.isHidden = true
        }
        if let clearButton = self.view.viewWithTag(142) as? UIButton {
            clearButton.isHidden = true
        }
        
        if countryObject.code == "+95" {
            if text.count >= validation.min {
                self.hideContactSuggesionView()
                if validation.max == text.count {
                    self.heightConfMobNum.constant = 60.0
                    self.mobileNumberTxt.text = text
                    self.confirmMobileNumberTxt.text = commonPrefixMobileNumber
                    self.confirmMobileNumberView.isHidden = false
                    self.confirmMobileNumberTxt.becomeFirstResponder()
                    self.hideContactSuggesionView()
                    return false
                }
                if text.count > validation.max {
                    //self.cnfMobileNumber.becomeFirstResponder()
                    self.pointsView.isHidden = true
                    self.expiryDaysView.isHidden = true
                    self.heightConfMobNum.constant = 60.0
                    self.confirmMobileNumberView.isHidden = false
                    self.confirmMobileNumberTxt.text = commonPrefixMobileNumber
                    self.confirmMobileNumberTxt.becomeFirstResponder()
                    self.hideContactSuggesionView()
                    return false
                } else {
                    self.heightConfMobNum.constant = 60.0
                    self.confirmMobileNumberView.isHidden = false
                    self.confirmMobileNumberTxt.text = commonPrefixMobileNumber
                }
                return true
            } else {
                self.heightConfMobNum.constant = 60.0
                self.confirmMobileNumberView.isHidden = true
            }
        } else {
            if textCount > 3 {
                self.heightConfMobNum.constant = 60.0
                self.confirmMobileNumberView.isHidden = false
                if textCount == 13 {
                    self.mobileNumberTxt.text = text
                    self.confirmMobileNumberTxt.becomeFirstResponder()
                    return false
                }
                if textCount > 13 {
                    return false
                }
            } else {
                self.heightConfMobNum.constant = 0
                self.confirmMobileNumberView.isHidden = true
            }
            if textCount > 8 {
                self.hideContactSuggesionView()
            }
        }
        
        return true
    }
    
    func shouldChangeCharacters(textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: mobileNumberAcceptableCharacters).inverted
        let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
        
        if string != filteredSet {
            return false
        }
        switch textField {
        case self.mobileNumberTxt:
            let mobileShouldReturn = self.MobileNumberHandler(textField: textField, shouldChangeCharactersIn: range, replacementString: string)
            return mobileShouldReturn
        case self.confirmMobileNumberTxt:
            let confirmShouldReturn = self.confirmMobileNumberHandler(textField: textField, shouldChangeCharactersIn: range, replacementString: string)
            return confirmShouldReturn
        case self.pointsText:
            expiryDaysText.text = ""
            self.transferButton.isHidden = true
            if let clearButton = self.view.viewWithTag(144) as? UIButton {
                clearButton.isHidden = true
            }
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string).replacingOccurrences(of: ",", with: "")
            if range.location == 0, string == "0" {
                return false
            }
            
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                if text.count == 0 {
                    self.expiryDaysView.isHidden = true
                    if let clearButton = self.view.viewWithTag(143) as? UIButton {
                        clearButton.isHidden = true
                    }
                }
                return true
            }
            
            let amountText = text.replacingOccurrences(of: ",", with: "")
            let amount = Double((amountText as NSString).doubleValue)
            let availablePointStr = availablePoint.replacingOccurrences(of: ",", with: "")
            let availablePointsInt = Double((availablePointStr as NSString).doubleValue)
            if availablePointsInt == 0 {
                return false
            }
            if amount > availablePointsInt {
                //self.showCustomErrorAlert(errMessage: "Enter below or equal to ".localized + "\(availablePoint)")
                self.pointsText.placeholder = "Enter Points".localized
              self.showCustomErrorAlert(errMessage: "Entered point can not be \n greater than available point(s).".localized)
                textField.text = ""
                if let clearButton = self.view.viewWithTag(143) as? UIButton {
                    clearButton.isHidden = true
                }
                return false
            }
            if let clearButton = self.view.viewWithTag(143) as? UIButton {
                if text.count > 0 && text != "0" {
                    clearButton.isHidden = false
                } else {
                    clearButton.isHidden = true
                }
            }
            if text.count > 0 && text != "0" {
                self.expiryDaysView.isHidden = false
                //self.pointsText.text = ""
                self.pointsText.placeholder = "Points".localized
            } else {
                self.pointsText.placeholder = "Enter Points".localized
                self.expiryDaysText.text = ""
                self.expiryDaysView.isHidden = true
                if let clearButton = self.view.viewWithTag(144) as? UIButton {
                    clearButton.isHidden = true
                }
            }
            self.pointsText.text = wrapAmountWithCommaDecimal(key: text)
            return false
        case self.expiryDaysText:
            
            let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string).replacingOccurrences(of: ",", with: "")
            if text.count > 6 {
                return false
            }
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                if text.count == 0 {
                    self.transferButton.isHidden = true
                }
                return true
            }
            if let clearButton = self.view.viewWithTag(144) as? UIButton {
                if text.count > 0 && text != "0"{
                    clearButton.isHidden = false
                } else {
                    clearButton.isHidden = true
                }
            }
            if text.count > 0 && text != "0"{
                 self.showAccessoryView(status : true)
                self.transferButton.isHidden = true
                self.expiryDaysText.placeholder = "Expiry Days".localized
            } else {
                self.expiryDaysText.placeholder = "Enter Expiry Days".localized
                self.showAccessoryView(status : false)
               self.transferButton.isHidden = true
            }
            self.expiryDaysText.text = wrapAmountWithCommaDecimal(key: text)
            return false
        default:
            return true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let changingAccetable = self.shouldChangeCharacters(textField: textField, shouldChangeCharactersIn: range, replacementString: string)
        return changingAccetable
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.mobileNumberTxt {
            if textField.text == "" && countryObject.code == "+95" {
                textField.text = commonPrefixMobileNumber
            }
        } else if textField == self.pointsText {
            if let text = textField.text, text.count > 0 {
                self.showAccessoryView(status : true)
                self.transferButton.isHidden = false
            } else {
                self.showAccessoryView(status : false)
                self.transferButton.isHidden = true
            }
        } else if textField == self.expiryDaysText {
            if let text = textField.text, text.count == 0 {
                self.expiryDaysText.inputAccessoryView?.isHidden = true
                self.transferButton.isHidden = true
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.expiryDaysText {
            if let text = textField.text, text.count > 0, text != "0" {
                self.transferButton.isHidden = false
            } else {
                self.transferButton.isHidden = true
            }
        }
    }
    
    func getActualMobileNum(_ phoneNum: String, withCountryCode countryCode: String) -> String {
        var FormateStr: String
        if phoneNum.hasPrefix("0") {
            _ = (phoneNum).substring(from: 1)
        }
        let notAllowedChars = CharacterSet(charactersIn: "+()")
        let resultString: String = countryCode.components(separatedBy: notAllowedChars).joined(separator: "")
        if (countryCode == "+95") || (countryCode == "(+95)") {
            if phoneNum.hasPrefix("0095") {
                FormateStr = "+95\((phoneNum).substring(from: 4))"
            } else if phoneNum.hasPrefix("0091") {
                FormateStr = "+91\((phoneNum).substring(from: 4))"
            } else if phoneNum.hasPrefix("95") {
                FormateStr = "+95\((phoneNum).substring(from: 2))"
            } else {
                FormateStr = "\(phoneNum)"
            }
        } else {
            FormateStr = "00\(resultString)\(phoneNum)"
        }
        return FormateStr
    }


fileprivate func _verifyInBackEnd(number :String, completion :@escaping (Bool)->()) {
    
    if number.hasPrefix(UserModel.shared.formattedNumber) {
        PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.ownMobileNumber.localized)
        self.mobileNumberTxt.text = (self.countryObject.code == "+95") ? "09" : ""
        self.confirmMobileNumberView.isHidden = true
        return
    }
    
    //var startIndex = 0
    
    //if(number.starts(with: "0")) { startIndex = 1 }
    
    let urlString = "\(Url.URLgetOKacDetails)" + "\(getConatctNum(number,withCountryCode: self.countryObject.code))"
    
    let trimmedString = urlString.components(separatedBy: .whitespaces).joined()
    
    let url = getUrl(urlStr: trimmedString, serverType: .serverApp)
    
    progressViewObj.showProgressView()
    
    JSONParser.GetApiResponse(apiUrl: url, type: "POST") { [weak self] (status : Bool, data : Any?) in
        
        DispatchQueue.main.async {
            
            if status == true {
                
                completion(true)
            } else {
                
                if let _ = data as? String  {
                    
                    completion(false)
                    DispatchQueue.main.async {
                        loyaltyAlertView.wrapAlert(title: nil, body: "Are you sure want to distribute points to Unregistered OK$ number?".localized, img: #imageLiteral(resourceName: "Contact Mobile Number"))
                        loyaltyAlertView.addAction(title: "Cancel".localized, style: .cancel , action: {
                            self?.mobileNumberTxt.text = (self?.countryObject.code == "+95") ? "09" : ""
                            self?.mobileNumberTxt.becomeFirstResponder()
                            self?.confirmMobileNumberTxt.text = ""
                            self?.confirmMobileNumberView.isHidden = true
                            self?.pointsView.isHidden = true
                            if let clearButton = self?.view.viewWithTag(141) as? UIButton {
                                clearButton.isHidden = true
                            }
                        })
                        loyaltyAlertView.addAction(title: "OK".localized, style: .target , action: {
                        })
                        loyaltyAlertView.showAlert(controller: self)
                    }
                } else {
                    
                    completion(false)
                }
            }
        }
        progressViewObj.removeProgressView()
    }
}
    
    
    @IBAction func transferButtonAction(_ sender : UIButton) {
        let availablePointsStr = (self.pointsLabelValue.text)?.replacingOccurrences(of: ",", with: "") ?? "0"
        let availablePoints = Double((availablePointsStr as NSString).doubleValue)
        let enteredPoints = Double((self.pointsText.text! as NSString).doubleValue)
        if enteredPoints > availablePoints {
           // self.showCustomErrorAlert(errMessage: "Enter below or equal to ".localized + "\(self.pointsText.text!)")
          self.showCustomErrorAlert(errMessage: "Entered point can not be \n greater than available point(s).".localized)

        } else {
            submitPointDistribution()
        }
    }
    
    private func submitPointDistribution() {
        self.view.endEditing(true)
        if appDelegate.checkNetworkAvail() {
            let urlString = String.init(format: "%@", Url.pointDistribution)
            let url = getUrl(urlStr: urlString, serverType: .serverApp)
            let params = self.JSONStringFromAnyObject(value: self.getParamsForPointDist() as AnyObject)
            println_debug(url)
            println_debug(params)
            DispatchQueue.main.async {
                TopupWeb.genericClass(url: url, param: params as AnyObject, httpMethod: "POST") { [weak self](response, success) in
                    if success {
                        if let json = response as? Dictionary<String,Any> {
                            if let code = json["Code"] as? NSNumber, code == 200 {
                               
                                if var data = json["Data"] as? String {
                                    data = data.replacingOccurrences(of: "\"", with: "")
                                    let xmlString = SWXMLHash.parse(data)
                                    self?.pointDict.removeAll()
                                    self?.enumerate(indexer: xmlString)
                                    println_debug(xmlString)
                                    
                                    self?.parserXMLResult()
                                }
                            } else {
                                if let msg = json["Msg"] as? String {
                                    self?.showErrorAlert(errMessage: msg)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    private func getParamsForPointDist() -> Dictionary<String,Any> {

        var loginParams = Dictionary<String,Any>()
        loginParams["AppId"] = LoginParams.setUniqueIDLogin()
        loginParams["Limit"] = 0
        loginParams["MobileNumber"] = UserModel.shared.mobileNo
        loginParams["Msid"] = getMsid()
        loginParams["Offset"] = 0
        loginParams["Ostype"] = 1
        loginParams["Otp"] = ""
        loginParams["Simid"] = simid
        
        var ip = ""
        if let ipAdd = OKBaseController.getIPAddress() {
            ip = ipAdd
        }
        let mobileNo = getAgentCode(numberWithPrefix: self.mobileNumberTxt.text!, havingCountryPrefix: self.countryObject.code)
        
        var bonusParams = Dictionary<String,Any>()
        bonusParams["Agentcode"]    = UserModel.shared.mobileNo
        bonusParams["ClientIp"]      = ip
        bonusParams["ClientOs"]      = "ios"
        bonusParams["ClientType"] = "SELFCARE"
        bonusParams["Destination"]            = mobileNo
        bonusParams["Pin"]            = ok_password.safelyWrappingString()
        bonusParams["Amount"]             = self.removeCommaFromDigit(pointsText.text ?? "")//pointsText.text ?? ""
        bonusParams["RequestType"]      = ""
        bonusParams["SecureToken"]           = UserLogin.shared.token
        bonusParams["ShopName"]               = UserModel.shared.businessName
        bonusParams["Image"]            = ""
        bonusParams["PointExpiry"]         = self.removeCommaFromDigit(self.expiryDaysText.text ?? "0")
        
        var finalParams = Dictionary<String,Any>()
        finalParams["LoginInfo"] = loginParams
        finalParams["LoyalityPointDistibutionDto"] = bonusParams
        
        return finalParams
    }
    
    
    func showEarnTrfReceipt() {
        
    }
    
    fileprivate func showReceiptScreen() {
        DispatchQueue.main.async {
            if let model = self.reciptModel {
                if let recieptVC = self.storyboard?.instantiateViewController(withIdentifier: "LOPointsTrfReceiptViewController_ID") as? LOPointsTrfReceiptViewController {
                    recieptVC.type = "PointDistribution"
                    recieptVC.reciptModel = model
                    recieptVC.delegate = self
                    self.navigationController?.pushViewController(recieptVC, animated: true)
                }
            }
        }
    }
    
    func parserXMLResult() {
        self.pointListArray.removeAll()
        self.pointListArray.append(pointDict)
        //                println_debug(pointListArray)
        self.removeProgressView()
        if pointDict["resultdescription"] as? String == "Transaction Successful" {
            var transID = ""
            var dTime: Date?
            var dest = ""
            var amount = ""
            var balance = ""
            var source = ""
            if let tID = pointDict["transid"] as? String {
                transID = tID
            }
            if let dT = pointDict["requestcts"] as? String {
                dTime = dT.dateValue(dateFormatIs: "dd-MMM-yyyy HH:mm:ss")
            }
            if let destNo = pointDict["destination"] as? String {
                dest = destNo
                if destNo.hasPrefix("0095") {
                    dest = "0\((destNo).substring(from: 4))"
                }
            }
            if let amnt = pointDict["amount"] as? String {
                amount = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: amnt)
            }
            if let bal = pointDict["walletbalance"] as? String {
                balance = CodeSnippets.commaFormatWithNoPrecision(forTheAmount: bal)
            }
            if let src = pointDict["source"] as? String {
                source = src
                if src.hasPrefix("0095") {
                    source = "0\((src).substring(from: 4))"
                }
            }
            let loyaltyDB = LoyaltyDBData(balancePoint: balance, dateTime: dTime, destinationNo: dest,
                                          merchantNo: source, shopName: UserModel.shared.businessName,
                                          transferPoint: amount, transID: transID,
                                          type: "LOYALTY DISTRIBUTION")
            println_debug(loyaltyDB)
            LoyaltyCoreDataManager.sharedInstance.insertLoyaltyPointsInfo(dataToInsert: loyaltyDB)
            DispatchQueue.main.async {
                self.updateAvailablePoints(points: self.pointDict["walletbalance"] as? String ?? "0.0")
                if self.pointListArray.count > 0 {
                    self.reciptModel = loyaltyPointTrfModel.init(dict: self.pointListArray[0])
                }
                self.reciptModel?.category = "LOYALTY DISTRIBUTION"
                self.reciptModel?.merchantName = UserModel.shared.businessName
                self.showReceiptScreen()
                
//                let alertMessage = "Transaction Successful".localized
//                loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: #imageLiteral(resourceName: "discount_money"))
//                loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
//                    self.navigationController?.popViewController(animated: true)
//                })
//                loyaltyAlertView.showAlert(controller: self)
            }
        } else if pointDict["resultdescription"] as? String == "Invalid Secure Token" {
            DispatchQueue.main.async {
                OKPayment.main.authenticate(screenName: "PointDistribution", delegate: self)
            }
        } else {
            DispatchQueue.main.async {
                let alertMessage = self.pointDict.safeValueForKey("resultdescription") as? String ?? ""
                loyaltyAlertView.wrapAlert(title: "", body: alertMessage.localized, img: #imageLiteral(resourceName: "error"))
                loyaltyAlertView.addAction(title: "OK".localized, style: .cancel , action: {
                    
                })
                loyaltyAlertView.showAlert(controller: self)
            }
        }
    }
    
    func webSuccessResult(data: Any, screen: String) {
        
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful {
            submitPointDistribution()
        }
    }
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            pointDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func webFailureResult(screen: String) {
        println_debug("Failure")
    }
    
    @IBAction func clearButtonAction(_ sender : UIButton) {
        switch sender.tag {
        case 141:
            self.hideContactSuggesionView()
            if countryObject.code == "+95" {
                self.mobileNumberTxt.text = commonPrefixMobileNumber
            } else {
                self.mobileNumberTxt.text = ""
            }
            if let clearButton = self.view.viewWithTag(142) as? UIButton {
                clearButton.isHidden = true
            }
            if let clearButton1 = self.view.viewWithTag(143) as? UIButton {
                clearButton1.isHidden = true
            }
            if let clearButton2 = self.view.viewWithTag(144) as? UIButton {
                clearButton2.isHidden = true
            }
            sender.isHidden = true
            confirmMobileNumberView.isHidden = true
            confirmMobileNumberTxt.text = ""
            pointsView.isHidden = true
            pointsText.text = ""
            expiryDaysView.isHidden = true
            expiryDaysText.text = ""
            self.transferButton.isHidden = true
            self.heightConfMobNum.constant = 60
            
            self.confirmMobileNumberTxt.isUserInteractionEnabled = true
            self.confirmMobileNumberTxt.placeholder = "Confirm Mobile Number".localized
            self.confirmMobileNumberTxt.isEnabled = true
            let countryCode = String.init(format: "(%@)", countryObject.code)
            leftViewCountryForConfirmation.wrapCountryViewData(img: countryObject.flag , str: countryCode)
            self.confirmMobileNumberTxt.leftView = leftViewCountryForConfirmation
            self.confirmMobileNumberTxt.leftViewMode  = UITextField.ViewMode.always
            self.mobileNumberTxt.becomeFirstResponder()
        case 142:
            sender.isHidden = true
            if countryObject.code == "+95" {
                self.confirmMobileNumberTxt.text = commonPrefixMobileNumber
            } else {
                self.confirmMobileNumberTxt.text = ""
            }
            if let clearButton1 = self.view.viewWithTag(143) as? UIButton {
                clearButton1.isHidden = true
            }
            if let clearButton2 = self.view.viewWithTag(144) as? UIButton {
                clearButton2.isHidden = true
            }
            pointsView.isHidden = true
            pointsText.text = ""
            expiryDaysView.isHidden = true
            expiryDaysText.text = ""
            self.transferButton.isHidden = true
            self.confirmMobileNumberTxt.becomeFirstResponder()
        case 143:
            if let clearButton2 = self.view.viewWithTag(144) as? UIButton {
                clearButton2.isHidden = true
            }
            sender.isHidden = true
            pointsText.text = ""
            expiryDaysView.isHidden = true
            expiryDaysText.text = ""
            self.transferButton.isHidden = true
            self.pointsText.placeholder = "Enter Points".localized
            self.pointsText.becomeFirstResponder()
        case 144:
             self.showAccessoryView(status : false)
            sender.isHidden = true
            expiryDaysText.text = ""
            self.transferButton.isHidden = true
             self.expiryDaysText.placeholder = "Enter Expiry Days".localized
            self.expiryDaysText.becomeFirstResponder()
        default:
            break
            
        }
    }
    
    @IBAction func scanQRViewAction(_ sender: UIButton) {
        guard let second = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "secondVC") as? ScanToPayViewController else { return }
        second.delegate = self
        second.screenFromEarnPointTfr = true
        second.navigation = self.navigationController
        self.navigationController?.pushViewController(second, animated: true)
    }
    
}


extension PointDistributionViewController : ContactSuggestionDelegate{
    
    func didSelectFromContactSuggession(number: Dictionary<String, Any>) {
        self.hideContactSuggesionView()
        self.confirmMobileNumberView.isHidden = true
        let number = number[ContactSuggessionKeys.phonenumber_backend] as! String
        guard validObj.checkRejectedNumber(prefix: number) == false else {
            loyaltyAlertView.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: #imageLiteral(resourceName: "Contact Mobile Number"))
            loyaltyAlertView.addAction(title: "OK".localized, style: .target , action: {
            })
            loyaltyAlertView.showAlert(controller: self)
            return
        }
        let validCount = validObj.getNumberRangeValidation(number)
        //println_debug("valid count ::: \(validCount)")
        
        if number.count >= validCount.min {
            // allow
            mobileNumberTxt.text = number
            heightConfMobNum.constant = 0
            if let clearButton = self.view.viewWithTag(141) as? UIButton {
                clearButton.isHidden = false
            }
            _verifyInBackEnd(number: number, completion: { (isFound) in
                self.pointsView.isHidden = false
                self.view.layoutIfNeeded()
            })
            self.pointsText.becomeFirstResponder()
        } else {
            // not allowed
            loyaltyAlertView.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: #imageLiteral(resourceName: "Contact Mobile Number"))
            loyaltyAlertView.addAction(title: "OK".localized, style: .target , action: {
            })
            loyaltyAlertView.showAlert(controller: self)
        }
    }
    
}

extension PointDistributionViewController : FavoriteSelectionDelegate{
    
    func selectedFavoriteObject(obj: FavModel) {
        self.confirmMobileNumberView.isHidden = true
        buttonView.isHidden = true
        //println_debug("selectedFavoriteObject")
        self.hideContactSuggesionView()
        let mobileNo = self.getActualMobileNum(obj.phone,withCountryCode: "+95")
        
        guard validObj.checkRejectedNumber(prefix: mobileNo) == false else {
            loyaltyAlertView.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: #imageLiteral(resourceName: "Contact Mobile Number"))
            loyaltyAlertView.addAction(title: "OK".localized, style: .target , action: {
            })
            loyaltyAlertView.showAlert(controller: self)
            return
        }
        let validCount = validObj.getNumberRangeValidation(mobileNo)
        //println_debug("valid count ::: \(validCount)")
        
        if mobileNo.count >= validCount.min {
            // allow
            mobileNumberTxt.text = mobileNo
            heightConfMobNum.constant = 0
            if let clearButton = self.view.viewWithTag(141) as? UIButton {
                clearButton.isHidden = false
            }
            _verifyInBackEnd(number: mobileNo, completion: { (isFound) in
                self.pointsView.isHidden = false
                self.view.layoutIfNeeded()
            })
            self.pointsText.becomeFirstResponder()
        } else {
            // not allowed
            loyaltyAlertView.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: #imageLiteral(resourceName: "Contact Mobile Number"))
            loyaltyAlertView.addAction(title: "OK".localized, style: .target , action: {
            })
            loyaltyAlertView.showAlert(controller: self)
        }
    }
    
}


//MARK: - Contact Picker Protocol
extension PointDistributionViewController : ContactPickerDelegate{
    
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]) {
        for contact in contacts {
            decodeContact(contact: contact, isMulti: true)
        }
    }
    
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        decodeContact(contact: contact, isMulti: false)
    }
    
    
    func wrapCountryViewData(img: String, str: String) {
        //        self.countryImage.image = UIImage.init(named: img)
        //        self.countryLabel.text  = str
        //
        //        let size   = self.countryLabel.text?.size(withAttributes: [.font: self.countryLabel.font]) ?? .zero
        //        let vWidth = 50 + size.width
        //        self.frame = .init(x: 0, y: 0, width: vWidth, height: 40.00)
        //        self.layoutIfNeeded()
    }
    
    
    func decodeContact(contact: ContactPicker) {
        
        self.confirmMobileNumberView.isHidden = true
        self.view.layoutIfNeeded()
        
        let phoneNumberCount = contact.phoneNumbers.count
        var phoneNumber = ""
        
        if phoneNumberCount == 1  {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        }
        else if phoneNumberCount > 1 {
            phoneNumber = "\(contact.phoneNumbers[0].phoneNumber)"
        }
        else {
            
        }
        
        if phoneNumber != "" {
            phoneNumber = phoneNumber.replacingOccurrences(of: ")", with: "")
            phoneNumber = phoneNumber.replacingOccurrences(of: "(", with: "")
            phoneNumber = phoneNumber.trimmingCharacters(in: .whitespaces)
        }
        
        if phoneNumber.hasPrefix("00") {
            _ = phoneNumber.remove(at: String.Index.init(encodedOffset: 0))
            _ = phoneNumber.remove(at: String.Index.init(encodedOffset: 0))
            
            if phoneNumber.hasPrefix("+") {
            } else {
                phoneNumber = "+" + phoneNumber
            }
        }
        
        
        
        let countryArray = revertCountryArray() as! Array<NSDictionary>
        
        let cDetails = self.identifyCountry(withPhoneNumber: phoneNumber, inCountryArray: countryArray) // 1 -> flag, 0 -> code
        var countryCode = String.init(format: "(%@)", "+95")
        if cDetails.0 == "" || cDetails.1 == "" {
            countryObject.flag = "myanmar"
            countryObject.code = "+95"
            leftViewCountry.wrapCountryViewData(img: "myanmar", str: countryCode)
            leftViewCountryForConfirmation.wrapCountryViewData(img: "myanmar", str: countryCode)
        } else {
            countryObject.flag = cDetails.1
            countryObject.code = cDetails.0
            countryCode = String.init(format: "(%@)", cDetails.0)
            leftViewCountry.wrapCountryViewData(img: cDetails.1, str: countryCode)
            leftViewCountryForConfirmation.wrapCountryViewData(img: cDetails.1, str: countryCode)
        }
        
        self.mobileNumberTxt.leftView     = leftViewCountry
        self.mobileNumberTxt.leftViewMode = UITextField.ViewMode.always
        
        self.view.layoutIfNeeded()
        
        var phone = ""
        
        if phoneNumber.hasPrefix("+95") || phoneNumber.hasPrefix("(+95)") {
            phone = phoneNumber.replacingOccurrences(of: cDetails.0, with: "0")
            phone = phone.replacingOccurrences(of: " ", with: "")
            
            if phone.hasPrefix("0") {
                self.mobileNumberTxt.text = phone
            } else {
                self.mobileNumberTxt.text = "0" + phone
            }
            
        } else {
            phone = phoneNumber.replacingOccurrences(of: cDetails.0, with: "")
            phone = phone.replacingOccurrences(of: " ", with: "")
            self.mobileNumberTxt.text = phone
        }
        
        let validations = PayToValidations().getNumberRangeValidation(self.mobileNumberTxt.text ?? "")
        if validations.isRejected || phoneNumber.count < validations.min {
            loyaltyAlertView.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: UIImage(named : "AppIcon"))
            loyaltyAlertView.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                self.mobileNumberTxt.text = (self.countryObject.code == "+95") ? "09" : ""
            })
            loyaltyAlertView.showAlert(controller: self)
            return
        }
        
        self.nameORnumber(selectiontype: .name, codeDetails: cDetails)
        if contact.displayName().count > 0 {
            self.confirmMobileNumberTxt.text = contact.displayName()
        } else {
            self.confirmMobileNumberTxt.text = "+95"
        }
        _verifyInBackEnd(number: phone, completion: { (isFound) in
            self.pointsView.isHidden = false
            self.view.layoutIfNeeded()
        })
        if let clearButton = self.view.viewWithTag(141) as? UIButton {
            clearButton.isHidden = false
        }
        self.pointsText.becomeFirstResponder()
        self.heightConfMobNum.constant = 0
    }
    
    func decodeContact(contact: ContactPicker, isMulti: Bool){
        buttonView.isHidden = true
        decodeContact(contact: contact)
    }
    
}

// MARK: - Table View
extension PointDistributionViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: .default, reuseIdentifier: "")
        return cell
    }
    
}


extension PointDistributionViewController: ScrollChangeDelegate {
    func changeScrollPage(point: Int, withScanObject object: PTScanObject) {
        
    }
    
    func dismissScanOffer(object: PTScanObject, controller: ScanToPayViewController) {
        let mobileObject = PTManagerClass.decodeMobileNumber(phoneNumber: object.agentNo)
        let numberValid = PayToValidations().getNumberRangeValidation(mobileObject.number)
        self.confirmMobileNumberView.isHidden = true
        if mobileObject.country.dialCode == "+95" {
            if numberValid.isRejected {
                mobileNumberTxt.text = ""
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                return
            }
            
            if mobileObject.number.count < numberValid.min && mobileObject.number != "" {
                mobileNumberTxt.text = ""
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                return
            }
            
            if mobileObject.number.count > numberValid.max {
                mobileNumberTxt.text = ""
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                return
            }
        } else {
            if mobileObject.number.count < 4 || mobileObject.number.count > 13 {
                PaytoConstants.alert.showToast(msg: PaytoConstants.messages.invalidNumber.localized)
                return
            }
        }
        
        let number = self.getAgentCode(numberWithPrefix: mobileObject.number, havingCountryPrefix: mobileObject.country.dialCode)
        if number == UserModel.shared.mobileNo {
            self.mobileNumberTxt.text = ""
            PaytoConstants.alert.showToast(msg: PaytoConstants.messages.ownMobileNumber.localized)
            return
        }
        mobileNumberTxt.text = mobileObject.number
        heightConfMobNum.constant = 0
        if let clearButton = self.view.viewWithTag(141) as? UIButton {
            clearButton.isHidden = false
        }
        mobileNumberTxt.resignFirstResponder()
        _verifyInBackEnd(number: mobileObject.number, completion: { (isFound) in
            self.pointsView.isHidden = false
            self.view.layoutIfNeeded()
        })
    }
    
    func scanningFailed() {
        
    }
}


extension PointDistributionViewController: LOReceiptPopVC {
    func morePaymentAction(phoneNumber: String, repeatPay: Bool) {
        self.initialSetUp()
        self.setUpRepeat(phoneNumber: phoneNumber, repeatPay: repeatPay)
    }
}
