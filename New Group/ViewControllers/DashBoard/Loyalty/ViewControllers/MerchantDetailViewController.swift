//
//  MerchantDetailViewController.swift
//  OK
//
//  Created by Kethan on 3/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class MerchantDetailViewController: LoyaltyBaseViewController, UIPopoverPresentationControllerDelegate {
    
    var modelData : merchantDetailsModel?

    @IBOutlet weak var totalPointLbl : UILabel!{
        didSet{
            self.totalPointLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var mobileNumberTxt : UITextField! {
        didSet {
            self.mobileNumberTxt.font = UIFont(name: appFont, size: appFontSize)
            self.mobileNumberTxt.isEnabled = false
            self.mobileNumberTxt.placeholder = self.mobileNumberTxt.placeholder?.localized
        }
    }
    @IBOutlet weak var sellingPointTxt : UITextField! {
        didSet {
            self.sellingPointTxt.font = UIFont(name: appFont, size: appFontSize)
            self.sellingPointTxt.isEnabled = false
            self.sellingPointTxt.placeholder = self.sellingPointTxt.placeholder?.localized
        }
    }
    @IBOutlet weak var sellingAmountTxt : UITextField! {
        didSet {
            self.sellingAmountTxt.font = UIFont(name: appFont, size: appFontSize)
            self.sellingAmountTxt.isEnabled = false
            self.sellingAmountTxt.placeholder = self.sellingAmountTxt.placeholder?.localized
        }
    }
    
    @IBOutlet weak var selectMerchantTxt : UITextField! {
        didSet {
            self.selectMerchantTxt.font = UIFont(name: appFont, size: appFontSize)
            self.selectMerchantTxt.placeholder = self.selectMerchantTxt.placeholder?.localized
        }
    }
    
    @IBOutlet weak var submitBtn : UIButton! {
        didSet {
            self.submitBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.submitBtn.setTitle(self.submitBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addCustomBackButton(navTitle: "Bonus Point Resale")
        addCustomMenuButton()
        updateUI()
    }
    
    private func updateUI() {
        self.totalPointLbl.text = UserDefaults.standard.value(forKey: "MyEarnPoint") as? String ?? "0"

        if modelData != nil {
            let dict = modelData?.merchant
            let mobileNum = replaceFirstTwoCharWithPlusAndGetMobilNo_CountryCode(strToTrim: dict?.safeValueForKey("merchantAccountNo") as? String ?? "")
            self.mobileNumberTxt.text = "(\(mobileNum.counrtycode))\(mobileNum.mobileNo)"
            self.selectMerchantTxt.text = dict?.safeValueForKey("shopName") as? String ?? ""
            self.selectMerchantTxt.placeholder = "Shop Name".localized
            var pointTxt = " Points"
            if let points = modelData?.pointBalance {
                if points == 1 {
                    pointTxt = " Point"
                }
                sellingPointTxt.text =  wrapAmountWithCommaDecimal(key: "\(points)").replacingOccurrences(of: ".0", with: "") + pointTxt
            }
            if let sellingPrice = modelData?.sellingPrice {
                sellingAmountTxt.text = wrapAmountWithCommaDecimal(key: "\(sellingPrice)").replacingOccurrences(of: ".0", with: "") + " MMK"
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func addCustomMenuButton() {
        let menuButton = UIButton(type: .custom)
        menuButton.frame = CGRect(x: 0, y: 0, width: 30, height: 40)
        menuButton.setImage(UIImage.init(named: "menu_white_payto.png"), for: .normal)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: menuButton)
        menuButton.addTarget(self, action: #selector(menuAction(_ : )), for: .touchUpInside)
    }
    
    @objc func menuAction(_ sender : UIBarButtonItem) {
        let popoverContent = self.storyboard?.instantiateViewController(withIdentifier: "ViewAllStatusPopover_ID") as? ViewAllStatusPopover
        popoverContent?.delegate = self
        popoverContent?.modalPresentationStyle = .popover
        let popover = popoverContent?.popoverPresentationController
        popoverContent?.preferredContentSize = CGSize(width: 200.0, height: 45.0)
        popover?.delegate = self
        popover?.sourceView = self.view
        popover?.permittedArrowDirections = .init(rawValue: 0)
        popover?.sourceRect = CGRect(x: self.view.frame.size.width, y: 0, width: 0, height: 0)
        self.present(popoverContent!, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    @IBAction func merchantPopAction(_ sender : UIButton) {
         self.navigationController?.popViewController(animated: true)
    }
    
    private func showSelectedViewController(viewController : UIViewController) {
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func submitButtonAction(_ sender : UIButton) {
        if let confirmationVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: "ResaleConfirmationViewController_ID") as? ResaleConfirmationViewController {
                confirmationVC.modelData = modelData
            showSelectedViewController(viewController: confirmationVC)
        }
    }
   
}



extension MerchantDetailViewController : ViewAllStatusProtocol {
    func showViewAllStatusVC() {
        let viewAllStatusVC = UIStoryboard(name: loyaltyMainViews.loyaltyStoryBoard, bundle: nil).instantiateViewController(withIdentifier: "BonusPointResaleStatusViewController_ID")
        self.navigationController?.pushViewController(viewAllStatusVC, animated: true)
    }
}
