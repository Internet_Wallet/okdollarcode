//
//  JTAppleCalendarViewController.swift
//  OK
//
//  Created by Mohit on 2/28/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import JTAppleCalendar

protocol selectedDate : class {
    func selectedStartDate(date : Date)
    func selectedEnddate(date : Date)
}

class JTAppleCalendarViewController: UIViewController {
    
        let formatterCurrent = DateFormatter()
        @IBOutlet weak var startDateLbl: UILabel!
        @IBOutlet weak var endDateLbl: UILabel!
        @IBOutlet weak var startDateValueLbl: UILabel!
        @IBOutlet weak var endDateValueLbl: UILabel!
        @IBOutlet weak var startDateUnderView: UIView!
        @IBOutlet weak var endDateUnderView: UIView!
    
        @IBOutlet weak var calendarView: JTAppleCalendarView!
        @IBOutlet weak var monthLabel: UILabel!
        @IBOutlet weak var weekViewStack: UIStackView!
    
        var currentSelection : dateSelected = .startDate
    
        var selectedButton = ""
        weak var delegate : selectedDate?
        var numberOfRows = 6
        let formatter = DateFormatter()
        var testCalendar = Calendar.current
        var generateInDates: InDateCellGeneration = .forAllMonths
        var generateOutDates: OutDateCellGeneration = .tillEndOfGrid
        var prePostVisibility: ((CellState, CellView?)->())?
        var hasStrictBoundaries = true
        let firstDayOfWeek: DaysOfWeek = .monday
        let disabledColor = UIColor.lightGray
        let enabledColor = UIColor.blue
        let dateCellSize: CGFloat? = nil
        var monthSize: MonthSize? = nil
        var prepostHiddenValue = false
        
        let red = UIColor.red
        let white = UIColor.white
        let black = UIColor.black
        let gray = UIColor.gray
        let lightGray = UIColor.lightGray
        let shade = UIColor.init(hex: "4E4E4E")
    
        override func viewDidLoad() {
            super.viewDidLoad()
             self.preferredContentSize = CGSize(width: 300, height: 400)
            calendarView.register(UINib(nibName: "PinkSectionHeaderView", bundle: Bundle.main),
                                  forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,
                                  withReuseIdentifier: "PinkSectionHeaderView")
            
            view.backgroundColor = UIColor.clear
            view.isOpaque = false
            formatterCurrent.dateFormat = "dd MMM yyyy"
            self.calendarView.visibleDates {[unowned self] (visibleDates: DateSegmentInfo) in
                self.setupViewsOfCalendar(from: visibleDates)
            }
            if selectedButton == "Start" {
                 currentSelection = .startDate
            } else {
                currentSelection = .endDate
            }
            currentButtonSelect()
        }
        
        var rangeSelectedDates: [Date] = []
        func didStartRangeSelecting(gesture: UILongPressGestureRecognizer) {
            let point = gesture.location(in: gesture.view!)
            rangeSelectedDates = calendarView.selectedDates
            if let cellState = calendarView.cellStatus(at: point) {
                let date = cellState.date
                if !calendarView.selectedDates.contains(date) {
                    let dateRange = calendarView.generateDateRange(from: calendarView.selectedDates.first ?? date, to: date)
                    for aDate in dateRange {
                        if !rangeSelectedDates.contains(aDate) {
                            rangeSelectedDates.append(aDate)
                        }
                    }
                    calendarView.selectDates(from: rangeSelectedDates.first!, to: date, keepSelectionIfMultiSelectionAllowed: true)
                } else {
                    let indexOfNewlySelectedDate = rangeSelectedDates.index(of: date)! + 1
                    let lastIndex = rangeSelectedDates.endIndex
                    let followingDay = testCalendar.date(byAdding: .day, value: 1, to: date)!
                    calendarView.selectDates(from: followingDay, to: rangeSelectedDates.last!, keepSelectionIfMultiSelectionAllowed: false)
                    rangeSelectedDates.removeSubrange(indexOfNewlySelectedDate..<lastIndex)
                }
            }
            
            if gesture.state == .ended {
                rangeSelectedDates.removeAll()
            }
        }
        
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            
        }
        
        @IBAction func printSelectedDates() {
            print("\nSelected dates --->")
            for date in calendarView.selectedDates {
                print(formatter.string(from: date))
            }
        }
        
        @IBAction func resize(_ sender: UIButton) {
            calendarView.frame = CGRect(
                x: calendarView.frame.origin.x,
                y: calendarView.frame.origin.y,
                width: calendarView.frame.width,
                height: calendarView.frame.height - 50
            )
            calendarView.reloadData()
        }
    
        private func resetTextFields()
        {
            startDateValueLbl.text = formatterCurrent.string(from: Date())
            endDateValueLbl.text = formatterCurrent.string(from: Date())
        }
    
        private func currentButtonSelect()
        {
            if let startDate = UserDefaults.standard.object(forKey: "StartDate") as? Date
            {
                startDateValueLbl.text = formatterCurrent.string(from: startDate)
            }
            else
            {
                startDateValueLbl.text = formatterCurrent.string(from: Date())
            }
            if let endDate = UserDefaults.standard.object(forKey: "EndDate") as? Date
            {
                endDateValueLbl.text = formatterCurrent.string(from: endDate)
            }
            else
            {
                endDateValueLbl.text = formatterCurrent.string(from: Date())
            }
            if currentSelection == .startDate
            {
                startDateLbl.textColor = UIColor.darkGray
                endDateLbl.textColor = UIColor.lightGray
                startDateUnderView.layer.backgroundColor = UIColor.blue.cgColor
                endDateUnderView.layer.backgroundColor = UIColor.white.cgColor
            }
            else
            {
                startDateLbl.textColor = UIColor.lightGray
                endDateLbl.textColor = UIColor.darkGray
                startDateUnderView.layer.backgroundColor = UIColor.white.cgColor
                endDateUnderView.layer.backgroundColor = UIColor.blue.cgColor
            }
        }
        
        @IBAction func next(_ sender: UIButton) {
            self.calendarView.scrollToSegment(.next)
        }
        
        @IBAction func previous(_ sender: UIButton) {
            self.calendarView.scrollToSegment(.previous)
        }
    
        @IBAction func startDateButtonAction(_ sender: UIButton) {
            currentSelection = .startDate
            currentButtonSelect()
        }
    
        @IBAction func endDateButtonAction(_ sender: UIButton) {
            currentSelection = .endDate
            currentButtonSelect()
        }
        
        func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
            guard let startDate = visibleDates.monthDates.first?.date else {
                return
            }
            let month = testCalendar.dateComponents([.month], from: startDate).month!
            let monthName = DateFormatter().monthSymbols[(month-1) % 12]
            // 0 indexed array
            let year = testCalendar.component(.year, from: startDate)
            monthLabel.text = monthName + " " + String(year)
        }
        
        func handleCellConfiguration(cell: JTAppleCell?, cellState: CellState) {
            handleCellSelection(view: cell, cellState: cellState)
            handleCellTextColor(view: cell, cellState: cellState)
            prePostVisibility?(cellState, cell as? CellView)
        }
        
        // Function to handle the text color of the calendar
        func handleCellTextColor(view: JTAppleCell?, cellState: CellState) {
            guard let myCustomCell = view as? CellView  else {
                return
            }
            
            if cellState.isSelected {
                myCustomCell.dayLabel.textColor = white
            } else {
                if cellState.dateBelongsTo == .thisMonth {
                    myCustomCell.dayLabel.textColor = black
                } else {
                    myCustomCell.dayLabel.textColor = gray
                }
            }
        }
        
        // Function to handle the calendar selection
        func handleCellSelection(view: JTAppleCell?, cellState: CellState) {
            guard let myCustomCell = view as? CellView else {return }
            if cellState.isSelected {
                myCustomCell.selectedView.layer.cornerRadius =  13
                myCustomCell.selectedView.isHidden = false
            } else {
                myCustomCell.selectedView.isHidden = true
            }
        }
    
        @IBAction func closeButtonAction(_ sender : UIButton)
        {
            dismissAppleCalendar()
        }
    
        private func dismissAppleCalendar()
        {
            self.dismiss(animated: true, completion: nil)
        }
    
        @IBAction func cancelButtonAction(_ sender : UIButton)
        {
            dismissAppleCalendar()
        }
    
        @IBAction func resetButtonAction(_ sender : UIButton)
        {
            resetTextFields()
            UserDefaults.standard.set(Date(), forKey: "StartDate")
            UserDefaults.standard.set(Date(), forKey: "EndDate")
        }
    
        @IBAction func okButtonAction(_ sender : UIButton)
        {
            let startDate = UserDefaults.standard.object(forKey: "StartDate") as? Date ?? Date()
            if let endDate = UserDefaults.standard.object(forKey: "EndDate") as? Date
            {
                if endDate >= startDate
                {
                    self.delegate?.selectedStartDate(date: startDate)
                    self.delegate?.selectedStartDate(date: endDate)
                    dismissAppleCalendar()
                }
                else
                {
                    self.showErrorAlert(errMessage: "End date should be greater than or equal to start date")
                }
            }
            else
            {
                self.showErrorAlert(errMessage: "Please select end date")
            }
        }
    }
    
    // MARK : JTAppleCalendarDelegate
    extension JTAppleCalendarViewController: JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource {
        
        func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
            
            formatter.dateFormat = "yyyy MM dd"
            formatter.timeZone = testCalendar.timeZone
            formatter.locale = testCalendar.locale
            let today = Date()
            
            let startDate = formatter.date(from: formatter.string(from: today))!
            let endDate = formatter.date(from: "2020 02 01")!
            
            let parameters = ConfigurationParameters(startDate: startDate,
                                                     endDate: endDate,
                                                     numberOfRows: numberOfRows,
                                                     calendar: testCalendar,
                                                     generateInDates: generateInDates,
                                                     generateOutDates: generateOutDates,
                                                     firstDayOfWeek: firstDayOfWeek,
                                                     hasStrictBoundaries: hasStrictBoundaries)
            return parameters
        }
        
        func configureVisibleCell(myCustomCell: CellView, cellState: CellState, date: Date) {
            myCustomCell.dayLabel.text = cellState.text
            if testCalendar.isDateInToday(date) {
                myCustomCell.backgroundColor = red.withAlphaComponent(0.5)
            } else {
                myCustomCell.backgroundColor = lightGray.withAlphaComponent(0.5)
            }
            
            handleCellConfiguration(cell: myCustomCell, cellState: cellState)
            
            myCustomCell.layer.cornerRadius = 5.0
            
            if cellState.text == "1" {
                let formatter = DateFormatter()
                formatter.dateFormat = "MMM"
                let month = formatter.string(from: date)
                myCustomCell.monthLabel.text = "\(month)"
            } else {
                myCustomCell.monthLabel.text = ""
            }
        }
        
        func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
            // This function should have the same code as the cellForItemAt function
            let myCustomCell = cell as! CellView
            configureVisibleCell(myCustomCell: myCustomCell, cellState: cellState, date: date)
        }
        
        func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
            let myCustomCell = calendar.dequeueReusableCell(withReuseIdentifier: "CellView", for: indexPath) as! CellView
            configureVisibleCell(myCustomCell: myCustomCell, cellState: cellState, date: date)
            return myCustomCell
        }
        
        func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
            handleCellConfiguration(cell: cell, cellState: cellState)
        }
        
        func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
            handleCellConfiguration(cell: cell, cellState: cellState)
            
                if currentSelection == .startDate {
                    if date >= Date().yesterday
                    {
                        UserDefaults.standard.set(date, forKey: "StartDate")
                        self.startDateValueLbl.text = formatterCurrent.string(from: date)
                        currentSelection = .endDate
                        currentButtonSelect()
                    }
                    else
                    {
                        self.showErrorAlert(errMessage: "Start date should be greater than or equal to today's date")
                    }
                    
                }
                else
                {
                    let startDate = UserDefaults.standard.object(forKey: "StartDate") as! Date
                    if date >= startDate
                    {
                        UserDefaults.standard.set(date, forKey: "EndDate")
                        self.endDateValueLbl.text = formatterCurrent.string(from: date)
                        currentSelection = .endDate
                        currentButtonSelect()
                    }
                    else
                    {
                        self.showErrorAlert(errMessage: "End date should be greater than or equal to start date")
                    }
                }
        }
        
        func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
            self.setupViewsOfCalendar(from: visibleDates)
        }
        
        func calendar(_ calendar: JTAppleCalendarView, headerViewForDateRange range: (start: Date, end: Date), at indexPath: IndexPath) -> JTAppleCollectionReusableView {
            let date = range.start
            let month = testCalendar.component(.month, from: date)
            
            let header: JTAppleCollectionReusableView
            if month % 2 > 0 {
                header = calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: "WhiteSectionHeaderView", for: indexPath)
                (header as! WhiteSectionHeaderView).title.text = formatter.string(from: date)
            } else {
                header = calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: "PinkSectionHeaderView", for: indexPath)
                (header as! PinkSectionHeaderView).title.text = formatter.string(from: date)
            }
            return header
        }
        
        func sizeOfDecorationView(indexPath: IndexPath) -> CGRect {
            let stride = calendarView.frame.width * CGFloat(indexPath.section)
            return CGRect(x: stride + 5, y: 5, width: calendarView.frame.width - 10, height: calendarView.frame.height - 10)
        }
        
        func calendarSizeForMonths(_ calendar: JTAppleCalendarView?) -> MonthSize? {
            return monthSize
        }
        
       /* func calendar(_ calendar: JTAppleCalendarView, shouldSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) -> Bool {
            if selectedButton == "Start" {
                if date <= Date().yesterday
                {
                    return false
                }
                return true
            }
            else
            {
                let startDate = UserDefaults.standard.object(forKey: "StartDate") as! Date
                if date < startDate
                {
                    return false
                }
                return true
            }
        } */
    }
    
    