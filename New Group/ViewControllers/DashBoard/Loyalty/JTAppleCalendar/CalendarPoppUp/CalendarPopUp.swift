//
//  CalendarPopUp.swift
//  CalendarPopUp
//
//  Created by Atakishiyev Orazdurdy on 11/16/16.
//  Copyright © 2016 Veriloft. All rights reserved.
//

import UIKit
import JTAppleCalendar

protocol CalendarPopUpDelegate: class {
    func dateChaged(startDate : Date, endDate : Date, duration : Int)
}

enum dateSelected
{
    case startDateBtn, endDateBtn
}

class CalendarPopUp: UIView {
    
    @IBOutlet weak var calendarHeaderLabel: UILabel!
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    @IBOutlet weak var startDateWeekLabel: UILabel!
    @IBOutlet weak var startDateDateLabel: UILabel!
    @IBOutlet weak var startDateYearLabel: UILabel!
    @IBOutlet weak var endDateWeekLabel: UILabel!
    @IBOutlet weak var endDateDateLabel: UILabel!
    @IBOutlet weak var endDateYearLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!

    weak var calendarDelegate: CalendarPopUpDelegate?
    let baseController = OKBaseController()
    
    var currentSelection : dateSelected = .startDateBtn
    
    var pendingApproveView : String?
    
    var durationDays = 0
    var firstDateSelect = true
    var endDate: Date!
    var startDate: Date = Date().getStart()
    var testCalendar = Calendar(identifier: .gregorian)
    var selectedDate: Date! = Date() {
        didSet {
            setDate()
        }
    }
    var selected:Date = Date() {
        didSet {
            calendarView.scrollToDate(selected)
            calendarView.selectDates([selected])
        }
    }

    @IBAction func closePopupButtonPressed(_ sender: AnyObject) {
        if let superView = self.superview as? PopupContainer {
            (superView ).close()
        }
    }
    
    @IBAction func resetButtonPressed(_ sender: AnyObject) {
        UserDefaults.standard.removeObject(forKey: "StartDate")
        UserDefaults.standard.removeObject(forKey: "EndDate")
        currentSelection = .startDateBtn
        selectedDate = Date()
        currentButtonSelect()
        dateSet(reset: "ReSet")
    }
    
    @IBAction func GetDateOk(_ sender: Any) {
        
        if pendingApproveView != nil {
            let startDate = UserDefaults.standard.object(forKey: "StartDate") as? Date ?? Date()
            if let endDate = UserDefaults.standard.object(forKey: "EndDate") as? Date
            {
                if endDate <= Date()
                {
                    if endDate >= startDate
                    {
                        calendarDelegate?.dateChaged(startDate: startDate, endDate: endDate, duration: durationDays)
                        if let superView = self.superview as? PopupContainer {
                            (superView ).close()
                        }
                    }
                    else
                    {
                        baseController.showErrorAlert(errMessage: "End date should be greater than or equal to start date".localized)
                    }
                } else {
                    baseController.showErrorAlert(errMessage: "End date should be less than the current date".localized)
                }
            } else {
                baseController.showErrorAlert(errMessage: "Please select end date".localized)
            }
        } else {
            let startDate = UserDefaults.standard.object(forKey: "StartDate") as? Date ?? Date()
            if let endDate = UserDefaults.standard.object(forKey: "EndDate") as? Date
            {
                if endDate >= startDate
                {
                    calendarDelegate?.dateChaged(startDate: startDate, endDate: endDate, duration: durationDays)
                    if let superView = self.superview as? PopupContainer {
                        (superView ).close()
                    }
                }
                else
                {
                    baseController.showErrorAlert(errMessage: "End date should be greater than or equal to start date".localized)
                }
            }
            else
            {
                baseController.showErrorAlert(errMessage: "Please select end date".localized)
            }
        }
        
    }
    
    override func awakeFromNib() {
        //Calendar
        // You can also use dates created from this function
        
        endDate = Calendar.current.date(byAdding: .month, value: 2, to: startDate)!
        setCalendar()
        setDate()
        self.calendarView.visibleDates {[unowned self] (visibleDates: DateSegmentInfo) in
            self.setupViewsOfCalendar(from: visibleDates)
        }
        currentButtonSelect()
    }
    
    @IBAction func startDateButtonAction(_ sender : UIButton)
    {
        currentSelection = .startDateBtn
        currentButtonSelect()
    }
    
    @IBAction func endDateButtonAction(_ sender : UIButton)
    {
        currentSelection = .endDateBtn
        currentButtonSelect()
    }
    
    private func dateSet(reset : String)
    {
        let month = testCalendar.dateComponents([.month], from: selectedDate).month!
        let weekday = testCalendar.component(.weekday, from: selectedDate)
        let year = testCalendar.component(.year, from: selectedDate)
        let monthName = DateFormatter().monthSymbols[(month-1) % 12] //GetHumanDate(month: month)
        let week = DateFormatter().shortWeekdaySymbols[weekday-1] //GetTurkmenWeek(weekDay: weekday)
        
        let day = testCalendar.component(.day, from: selectedDate)
        if pendingApproveView != nil {
            
            if currentSelection == .startDateBtn {
                if selectedDate < Date().yesterday {
                    startDateWeekLabel.text = week
                    startDateDateLabel.text = monthName + " " + String(day)
                    startDateYearLabel.text = String(year)
                    if reset == "" {
                        if endDateWeekLabel.text == "" { endDateWeekLabel.text = week }
                        if endDateDateLabel.text == "" { endDateDateLabel.text = monthName + " " + String(day) }
                        if endDateYearLabel.text == "" { endDateYearLabel.text = String(year) }
                    }
                    else
                    {
                        endDateWeekLabel.text = week
                        endDateDateLabel.text = monthName + " " + String(day)
                        endDateYearLabel.text = String(year)
                    }
                }
            }
            else
            {
                if let startDt = UserDefaults.standard.object(forKey: "StartDate") as? Date {
                    if selectedDate >= startDt {
                        endDateWeekLabel.text = week
                        endDateDateLabel.text = monthName + " " + String(day)
                        endDateYearLabel.text = String(year)
                    } else {
                        if startDateWeekLabel.text == "" { startDateWeekLabel.text = week }
                        if startDateDateLabel.text == "" { startDateDateLabel.text = monthName + " " + String(day) }
                        if startDateYearLabel.text == "" { startDateYearLabel.text = String(year) }
                    }
                } else {
                    if selectedDate >= Date().yesterday {
                        endDateWeekLabel.text = week
                        endDateDateLabel.text = monthName + " " + String(day)
                        endDateYearLabel.text = String(year)
                    }
                }
            }
            
        } else {
        if currentSelection == .startDateBtn {
            if selectedDate >= Date().yesterday {
                startDateWeekLabel.text = week
                startDateDateLabel.text = monthName + " " + String(day)
                startDateYearLabel.text = String(year)
                if reset == "" {
                    if endDateWeekLabel.text == "" { endDateWeekLabel.text = week }
                    if endDateDateLabel.text == "" { endDateDateLabel.text = monthName + " " + String(day) }
                    if endDateYearLabel.text == "" { endDateYearLabel.text = String(year) }
                }
                else
                {
                    endDateWeekLabel.text = week
                    endDateDateLabel.text = monthName + " " + String(day)
                    endDateYearLabel.text = String(year)
                }
            }
        }
        else
        {
            if let startDt = UserDefaults.standard.object(forKey: "StartDate") as? Date {
                if selectedDate >= startDt {
                    endDateWeekLabel.text = week
                    endDateDateLabel.text = monthName + " " + String(day)
                    endDateYearLabel.text = String(year)
                } else {
                    if startDateWeekLabel.text == "" { startDateWeekLabel.text = week }
                    if startDateDateLabel.text == "" { startDateDateLabel.text = monthName + " " + String(day) }
                    if startDateYearLabel.text == "" { startDateYearLabel.text = String(year) }
                }
            } else {
                if selectedDate >= Date().yesterday {
                    endDateWeekLabel.text = week
                    endDateDateLabel.text = monthName + " " + String(day)
                    endDateYearLabel.text = String(year)
                }
            }
        }
        }
        
    }
    

    func setDate() {
        if firstDateSelect
        {
            if let startDt = UserDefaults.standard.object(forKey: "StartDate") as? Date {
                let monthStart = testCalendar.dateComponents([.month], from: startDt).month!
                let weekdayStart = testCalendar.component(.weekday, from: startDt)
                let yearStart = testCalendar.component(.year, from: startDt)
                let monthNameStart = DateFormatter().monthSymbols[(monthStart-1) % 12] //GetHumanDate(month: month)
                let weekStart = DateFormatter().shortWeekdaySymbols[weekdayStart-1] //GetTurkmenWeek(weekDay: weekday)
                let dayStart = testCalendar.component(.day, from: startDt)
                startDateWeekLabel.text = weekStart
                startDateDateLabel.text = monthNameStart + " " + String(dayStart)
                startDateYearLabel.text = String(yearStart)
            } else {
                dateSet(reset: "")
            }
            if let endDt = UserDefaults.standard.object(forKey: "EndDate") as? Date {
                let monthEnd = testCalendar.dateComponents([.month], from: endDt).month!
                let weekdayEnd = testCalendar.component(.weekday, from: endDt)
                let yearEnd = testCalendar.component(.year, from: endDt)
                let monthNameEnd = DateFormatter().monthSymbols[(monthEnd-1) % 12] //GetHumanDate(month: month)
                let weekEnd = DateFormatter().shortWeekdaySymbols[weekdayEnd-1] //GetTurkmenWeek(weekDay: weekday)
                let dayEnd = testCalendar.component(.day, from: endDt)
                endDateWeekLabel.text = weekEnd
                endDateDateLabel.text = monthNameEnd + " " + String(dayEnd)
                endDateYearLabel.text = String(yearEnd)
            } else {
                dateSet(reset: "")
            }
        } else {
            dateSet(reset: "")
        }
    }

    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        guard let startDate = visibleDates.monthDates.first?.date else {
            return
        }
        let month = testCalendar.dateComponents([.month], from: startDate)
        let monthName = DateFormatter().monthSymbols[Int(month.month!)-1] //GetHumanDate(month: month)
        
        let year = testCalendar.component(.year, from: startDate)
        calendarHeaderLabel.text = monthName + ", " + String(year)
    }
    
    func setCalendar() {
        calendarView.calendarDataSource = self
        calendarView.calendarDelegate = self
        let nibName = UINib(nibName: "CellView", bundle:nil)
        calendarView.register(nibName, forCellWithReuseIdentifier: "CellView")
        calendarView.minimumLineSpacing = 0
        calendarView.minimumInteritemSpacing = 0
    }
    
     func currentButtonSelect()
    {
        if currentSelection == .startDateBtn
        {
            startDateWeekLabel.textColor = UIColor.white
            startDateDateLabel.textColor = UIColor.white
            startDateYearLabel.textColor = UIColor.white
            endDateWeekLabel.textColor = UIColor.lightGray
            endDateDateLabel.textColor = UIColor.lightGray
            endDateYearLabel.textColor = UIColor.lightGray
        }
        else
        {
            startDateWeekLabel.textColor = UIColor.lightGray
            startDateDateLabel.textColor = UIColor.lightGray
            startDateYearLabel.textColor = UIColor.lightGray
            endDateWeekLabel.textColor = UIColor.white
            endDateDateLabel.textColor = UIColor.white
            endDateYearLabel.textColor = UIColor.white
        }
    }

}

// MARK : JTAppleCalendarDelegate
extension CalendarPopUp: JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource {

    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy MM dd"
        
        if pendingApproveView != nil {
            startDate = formatter.date(from: "1990 02 01")!
            endDate = formatter.date(from: formatter.string(from: Date()))!
        } else {
            startDate = formatter.date(from: formatter.string(from: Date()))!
            endDate = formatter.date(from: "2020 02 01")!
        }
        
        let parameters = ConfigurationParameters(startDate: startDate,
                                                 endDate: endDate,
                                                 numberOfRows: 6,
                                                 calendar: testCalendar,
                                                 generateInDates: .forAllMonths,
                                                 generateOutDates: .tillEndOfGrid,
                                                 firstDayOfWeek: DaysOfWeek.monday)

        return parameters
    }
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        (cell as? CellView)?.handleCellSelection(cellState: cellState, date: date, selectedDate: selectedDate)
    }
    
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let myCustomCell = calendar.dequeueReusableCell(withReuseIdentifier: "CellView", for: indexPath) as! CellView
        myCustomCell.handleCellSelection(cellState: cellState, date: date, selectedDate: selectedDate)
        return myCustomCell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        selectedDate = date
        (cell as? CellView)?.cellSelectionChanged(cellState, date: date)
        
        if pendingApproveView != nil {
            if firstDateSelect
            {
                firstDateSelect = false
                currentButtonSelect()
            }
            else if currentSelection == .startDateBtn {
                if date < Date().yesterday
                {
                    UserDefaults.standard.set(date, forKey: "StartDate")
                    currentSelection = .endDateBtn
                    currentButtonSelect()
                }
                else
                {
                    baseController.showErrorAlert(errMessage: "Start date should be less than or equal to today's date".localized)
                    calendar.deselect(dates: [date])
                }
            }
            else
            {
                let startDate = UserDefaults.standard.object(forKey: "StartDate") as? Date ?? Date()
                if date >= startDate && date < Date().yesterday
                {
                    UserDefaults.standard.set(date, forKey: "EndDate")
                    currentButtonSelect()
                    durationDays = Calendar.current.dateComponents([.day], from: startDate, to: date).day!
                    durationLabel.text = "Duration : \(String(describing: durationDays)) days"
                }
                else if date > Date()
                {
                    baseController.showErrorAlert(errMessage: "End date should be less than or equal to today's date".localized)
                    calendar.deselect(dates: [date])
                }
                else
                {
                    baseController.showErrorAlert(errMessage: "End date should be greater than or equal to start date".localized)
                    calendar.deselect(dates: [date])
                }
            }
            
        } else {
            
            if firstDateSelect
            {
                firstDateSelect = false
                currentButtonSelect()
            }
            else if currentSelection == .startDateBtn {
                if date >= Date().yesterday
                {
                    UserDefaults.standard.set(date, forKey: "StartDate")
                    currentSelection = .endDateBtn
                    currentButtonSelect()
                }
                else
                {
                    baseController.showErrorAlert(errMessage: "Start date should be greater than or equal to today's date".localized)
                    calendar.deselect(dates: [date])
                }
            }
            else
            {
                let startDate = UserDefaults.standard.object(forKey: "StartDate") as? Date ?? Date()
                if date >= startDate
                {
                    UserDefaults.standard.set(date, forKey: "EndDate")
                    currentButtonSelect()
                    durationDays = Calendar.current.dateComponents([.day], from: startDate, to: date).day!
                    durationLabel.text = "Duration : \(String(describing: durationDays)) days"
                }
                else
                {
                    baseController.showErrorAlert(errMessage: "End date should be greater than or equal to start date".localized)
                    calendar.deselect(dates: [date])
                }
            }
        }
    }

    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        (cell as? CellView)?.cellSelectionChanged(cellState, date: date)
    }

    func calendar(_ calendar: JTAppleCalendarView, willResetCell cell: JTAppleCell) {
        (cell as? CellView)?.selectedView.isHidden = true
    }

    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        self.setupViewsOfCalendar(from: visibleDates)
    }
}

extension Date {
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return tomorrow.month != month
    }
    
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
    
}

