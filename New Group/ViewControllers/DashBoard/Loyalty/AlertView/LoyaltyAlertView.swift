//
//  LoyaltyAlertView.swift
//  OK
//
//  Created by Kethan on 8/11/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class LoyaltyAlertView: NSObject {
    
    var alert = LoyaltyAlertViewController()
    
    func wrapAlert(title: String?,body: String,img: UIImage?) {
        
        alert = LoyaltyAlertViewController.init(title: title, description: body, image: img, style: .alert)
    }
    
    func addTextField(title: String?,body: String,img: UIImage?) {
        alert = LoyaltyAlertViewController.init(title: title, description: body, image: img, style: .alert)
    }
    
    func AlerTextReturnString() -> String{
        return alert.textFields[0].text!
    }
    
    func addAction(title: String, style: AlertStyle, action: @escaping () -> Void) {
        let sty : PMAlertActionStyle
        if style == .cancel {
            sty = PMAlertActionStyle.cancel
        } else {
            sty = PMAlertActionStyle.default
        }
        alert.addAction(PMLoyaltyAlertAction.init(title: title, style: sty, action: {
            action()
        }))
    }
    
    func showAlert(controller: UIViewController? = nil) {
        if let keyWindow = UIApplication.shared.keyWindow {
            keyWindow.endEditing(true)
            alert.view.frame = keyWindow.bounds
            alert.view.tag = 5556
            keyWindow.addSubview(alert.view)
            keyWindow.makeKeyAndVisible()
        }
    }
}

