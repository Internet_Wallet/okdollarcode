//
//  SearchHomeViewController.swift
//  OK
//
//  Created by Ashish on 1/13/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class SearchHomeViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tableViewSearch: UITableView!
    @IBOutlet weak var viewEmptyRecords: UIView!
    @IBOutlet weak var constraintBottomEmptyRecords: NSLayoutConstraint!
    @IBOutlet weak var labelNoRecords: UILabel!
    
    // MARK: - Properties
    private lazy var searchBar = UISearchBar()
    private var searchCellID = "DashboardSearchCell"
    private struct MySectionRowValues {
        var sectionHeader: String
        var rowArray: [DashboardSearchKeys]
    }
    private var frequentArray = [MySectionRowValues]()
    // MARK: We should not change the existing enum case. If want to change then do reinstall after changing because of db
    private var keyList: [DashboardSearchKeys] = [.landlineNumber, .postpaidNumber, .prepaidNumber, .resale, .topupRequest,
                                                  .dataPlan, .specialOffers, .otherTopup, .viberOut, .oK$Account, .myNumberTopUp,
                                                  .otherNumberTopUp, .paytoFuel, .paytoMerchant, .paymentRequest, .paytoSupermarket, .paytoRestaurant,
                                                  .sendMoneyToBank, .intercityBusTicket, .flightTicket, .allTransactionReport,
                                                  .receivedMoneyReport,.topupRechargeReport, .billPaymentReport, .offers, .luckyDrawCoupon, .requestForOKMoney,
                                                  .requestForTopup, .rechargeOtherNumber, .foodWallet, .loyalty, .rechargeMyNumber,
                                                  .paySend, .reSale, .landlineBill, .postpaidMobile, .oKChat, .liveTaxi, .merchantPayment, .addWithdraw, .bankCounterDeposit, .nearbyOKServices, .cashIn, .cashOut, .intercityBus,
                                                  .billSplitter, .electricity, .skynet, .hotel, .flights, .requestMoney,
                                                  .solar, .buyBonusPoint, .transferTo, .instaPay, .requestCashIn, .requestCashOut,
                                                  .insurance, .redeemBonusPoint, .saleBonusPoint, .bonusPointSetup, .earningPointTransfer, .voting, .sunKingPayment, .InternationalTopUp, .airtel, .mobileAndElectronics , .onestopmart , .buyOk$Point, .voucher , .lottery] //.airtel, .reliance, , .skype, .myanTalk, .chinaMobileRecharge, .overseasRecharge, .overseasTopup .thaiMobileRecharge, .tataSky, .sunDirect, .videocon, .dishTv, .dTHBill, .paytoBus, .ferryTicket, .ferry, .tollTicket, .toll, .bus, .dTHBill, .donation
    private var searchResultList = [DashboardSearchKeys]()
    private enum ScreenMode {
        case search, normal
    }
    private var screenMode = ScreenMode.normal
    
    // MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        labelNoRecords.text = "No records found!".localized
        searchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.tintColor = UIColor.blue
        searchBar.placeholder = "Search".localized
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                //(appDel.currentLanguage == "uni") ? UIFont.systemFont(ofSize: 15) : myFont
                //searchTextField.font = (appDel.currentLanguage == "uni") ? UIFont.systemFont(ofSize: 15) : myFont
              
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.keyboardType = .asciiCapable
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
            }
        }
        let view = self.searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = ConstantsColor.navigationHeaderTransaction
            }
        }
        if UserModel.shared.agentType == .advancemerchant {
            keyList = [.intercityBusTicket, .flightTicket, .allTransactionReport, .receivedMoneyReport, .requestForOKMoney,.requestForTopup,  .loyalty, .paySend,  .liveTaxi, .intercityBus, .billSplitter,  .hotel, .flights, .requestMoney,.onestopmart, .redeemBonusPoint, .saleBonusPoint, .bonusPointSetup]
        }else if UserModel.shared.agentType == .merchant {
           keyList = [.landlineNumber, .postpaidNumber, .prepaidNumber, .resale, .topupRequest,
            .dataPlan, .specialOffers, .otherTopup, .viberOut, .oK$Account, .myNumberTopUp,
            .otherNumberTopUp, .paytoFuel, .paytoMerchant, .paymentRequest, .paytoSupermarket, .paytoRestaurant,
            .sendMoneyToBank, .saiSaiPay ,.intercityBusTicket, .flightTicket, .allTransactionReport,
            .receivedMoneyReport,.topupRechargeReport, .billPaymentReport, .requestForOKMoney,
            .requestForTopup, .rechargeOtherNumber, .foodWallet, .loyalty, .rechargeMyNumber,
            .paySend, .reSale, .landlineBill, .postpaidMobile, .oKChat, .liveTaxi, .addWithdraw, .bankCounterDeposit, .nearbyOKServices, .cashIn, .cashOut, .intercityBus
            , .electricity, .skynet, .hotel, .flights, .requestMoney,
            .solar, .transferTo , .requestCashIn, .requestCashOut,
            .insurance, .onestopmart, .redeemBonusPoint, .bonusPointSetup, .saleBonusPoint, .sunKingPayment, .airtel , .buyOk$Point, .nearMe, .taxPaymentNew, .voucher]
        }else {
            keyList = [.landlineNumber, .postpaidNumber, .prepaidNumber, .resale, .topupRequest,
             .dataPlan, .specialOffers, .otherTopup, .viberOut, .oK$Account, .myNumberTopUp,
             .otherNumberTopUp, .paytoFuel, .paytoMerchant, .paymentRequest, .paytoSupermarket, .paytoRestaurant,
             .sendMoneyToBank, .saiSaiPay, .intercityBusTicket, .flightTicket, .allTransactionReport,
             .receivedMoneyReport,.topupRechargeReport, .billPaymentReport, .requestForOKMoney,
             .requestForTopup, .rechargeOtherNumber, .foodWallet, .loyalty, .rechargeMyNumber,
             .paySend, .reSale, .landlineBill, .postpaidMobile, .oKChat, .liveTaxi, .addWithdraw, .bankCounterDeposit, .nearbyOKServices, .cashIn, .cashOut, .intercityBus
             , .electricity, .skynet, .hotel, .flights, .requestMoney,
             .solar, .transferTo , .requestCashIn, .requestCashOut,
             .insurance, .onestopmart , .redeemBonusPoint, .saleBonusPoint, .sunKingPayment, .airtel , .buyOk$Point, .nearMe, .taxPaymentNew, .voucher]
        }
        
        if isBuySaleBonusPointShow {
          //  keyList.append(.saleBonusPoint)
            keyList.append(.buyBonusPoint)
        }
        
        if isEarningPointShow {
            keyList.append(.earningPointTransfer)
        }
        
        if isLuckyDrawShow {
            keyList.append(.luckyDrawCoupon)
        }
        if isVotingshow {
            keyList.append(.voting)
        }
        if isInstantPayShow {
           if UserModel.shared.agentType != .advancemerchant{
            keyList.append(.instaPay)
            }
          
        }
        if isMobileElectronicsShow {
            keyList.append(.mobileAndElectronics)
        }
        if isOverseasShow {
            if UserModel.shared.agentType != .advancemerchant{
                keyList.append(.InternationalTopUp)
             }
        }
        if isMarchantPaymentShow {
            keyList.append(.merchantPayment)
        }
        if isBillSplitterShow {
            keyList.append(.billSplitter)
        }
        if isLotteryShow {
            keyList.append(.lottery)
        }
        
        
        if UserModel.shared.agentType != .advancemerchant {
            if isOfferShow {
                keyList.append(.offers)
            }
        }
        
        let searchBarButton = UIBarButtonItem(customView:searchBar)
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        self.navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: backButton), searchBarButton]
        searchBar.becomeFirstResponder()
        self.fetchRecentSearch()
        tableViewSearch.tableFooterView = UIView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.keepCurrentNavigationColor()
    }
    
    @objc private func backAction() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInset = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
            self.tableViewSearch.contentInset = contentInset
            self.tableViewSearch.scrollIndicatorInsets = contentInset
            constraintBottomEmptyRecords.constant = keyboardSize.height
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.tableViewSearch.contentInset = UIEdgeInsets.zero
        self.tableViewSearch.scrollIndicatorInsets = UIEdgeInsets.zero
        constraintBottomEmptyRecords.constant = 0
        self.view.layoutIfNeeded()
    }
    
    private func refreshTable() {
        self.fetchRecentSearch()
        tableViewSearch.reloadData()
    }
    
    private func fetchRecentSearch() {
        guard var recentSearchArray = DashboardSearchCoreDataManager.sharedInstance.fetchRecentDashboardSearch() else { return }
        recentSearchArray = recentSearchArray.sorted {
            guard let date1 = $0.updateDate, let date2 = $1.updateDate else { return false }
            return date1.compare(date2) == .orderedDescending
        }
        recentSearchArray = recentSearchArray.sorted { ($0.visitingCount > $1.visitingCount) }
        if recentSearchArray.count > 8 {
            recentSearchArray = Array(recentSearchArray[0..<7])
        }
        frequentArray = []
        var rowArray = [DashboardSearchKeys]()
        for items in recentSearchArray {
            guard let searchKeyName = items.searchKeyName, let enumCase = DashboardSearchKeys(rawValue: searchKeyName) else { return }
            rowArray.append(enumCase)
        }
        //This happens until search is happened and user taps
        if rowArray.count == 0 {
            for item in Array(keyList[0...7]) {
                DashboardSearchCoreDataManager.sharedInstance.insertUpdateSearchKeyCountInDB(caseString: item.rawValue)
                rowArray.append(item)
            }
        }
        frequentArray = [MySectionRowValues(sectionHeader: "Frequent Searches".localized, rowArray: rowArray)]
    }
    
    private func searchFromKey(ketText: String) {
        searchResultList = []
        searchResultList = keyList.filter {
            return $0.keyArray.filter {
                guard let _ = $0.localized.localizedStandardRange(of: ketText) else { return false }
                return true
                }.count > 0
        }
        tableViewSearch.reloadData()
    }
    
    private func keepCurrentNavigationColor() {
        self.navigationController?.navigationBar.barTintColor = kYellowColor
    }
    
    private func navigateToSendMoneyToBank() {
        let story = UIStoryboard(name: "SendMoney", bundle: nil)
        guard let sendMoneyToBankView = story.instantiateViewController(withIdentifier: "SendMoneyToBankView_ID") as? SendMoneyToBankViewController else { return }
        self.navigationController?.pushViewController(sendMoneyToBankView, animated: true)
    }
    
    private func navigateToSaiSaiPay() {
        var homeVC: BankDetailsViewController?
        homeVC = UIStoryboard(name: "UnionPay", bundle: nil).instantiateViewController(withIdentifier: "BankDetailsViewController") as? BankDetailsViewController
        if let homeVC = homeVC {
            navigationController?.pushViewController(homeVC, animated: true)
        }
    }
    
    private func navigateToScanPay() {
        println_debug("navigateToScanPay")
    }
    
    
    private func navigateToTransactionReport() {
        guard let transactionDetailViewController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.transactionDetailViewController.nameAndID) as? TransactionDetailViewController else { return }
        self.navigationController?.pushViewController(transactionDetailViewController, animated: true)
    }
    
    private func navigateToTopupRecharge() {
        guard let reportTopUpViewController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportTopUpViewController.nameAndID) as? ReportTopUpViewController else { return }
        self.navigationController?.pushViewController(reportTopUpViewController, animated: true)
    }
    
    private func navigateToBillPaymentReport() {
        guard let reportBillPaymentsController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportBillPaymentsViewController.nameAndID) as? ReportBillPaymentsViewController else { return }
        self.navigationController?.pushViewController(reportBillPaymentsController, animated: true)
    }
    
    private func navigateToReceivedMoneyReport() {
        guard let receivedMoneyViewController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.receivedMoneyViewController.nameAndID) as? ReceivedMoneyViewController else { return }
        receivedMoneyViewController.isToPopToGoBack = true
        self.navigationController?.pushViewController(receivedMoneyViewController, animated: true)
    }
    
    private func identifyKeyAndNavigateASection(keyCase: DashboardSearchKeys) {
        switch keyCase {
        case .landlineNumber, .landlineBill:
            TBHomeNavigations.main.navigate(.landlineBill)
        case .postpaidNumber, .postpaidMobile:
            TBHomeNavigations.main.navigate(.postpaidMobile)
        case .mobileAndElectronics:
            TBHomeNavigations.main.navigate(.mobileElectronics)
        case .onestopmart:
            TBHomeNavigations.main.navigate(.onestopmart)
        case .prepaidNumber, .otherTopup, .otherNumberTopUp, .rechargeOtherNumber:
            TBHomeNavigations.main.navigate(.otherNumber)
        case .buyOk$Point:
            TBHomeNavigations.main.navigate(.buyTopupPoint)
//        case .overseasTopup, .overseasRecharge:
//            TBHomeNavigations.main.navigate(.OverseasTopUp)
        case .InternationalTopUp:
        TBHomeNavigations.main.navigate(.OverseasTopUp)
        case .resale, .reSale:
            TBHomeNavigations.main.navigate(.resale)
        case .topupRequest, .requestMoney, .paymentRequest, .requestForOKMoney, .requestForTopup:
            TBHomeNavigations.main.navigate(.ReqMoney)
        case .specialOffers, .myNumberTopUp, .rechargeMyNumber, .dataPlan:
            TBHomeNavigations.main.navigate(.mobileNumber)
        case .skynet, .airtel, .reliance, .tataSky, .sunDirect, .videocon, .dishTv:
            TBHomeNavigations.main.navigate(.tataSky)
        case .voucher, .viberOut: //.skype, .myanTalk:
            TBHomeNavigations.main.navigate(.voucher)
        case .addWithdraw, .oK$Account:
            TBHomeNavigations.main.navigate(.addWithdraw)
//        case .bus: //.paytoBus
//            TBHomeNavigations.main.navigate(.bus)
        case .paySend, .paytoFuel, .paytoMerchant, .paytoSupermarket, .paytoRestaurant:
            TBHomeNavigations.main.navigate(.payto)
        case .sendMoneyToBank:
            if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "NavigateToSendMoneyBank", delegate: self)
            } else {
                navigateToSendMoneyToBank()
            }
        case .saiSaiPay:
                navigateToSaiSaiPay()
      //  case .scanPay:
         //       navigateToScanPay()
        default:
            self.identifyKeyAndNavigateBSection(keyCase: keyCase)
        }
    }
    
    private func identifyKeyAndNavigateBSection(keyCase: DashboardSearchKeys) {
        switch keyCase {
        case .intercityBusTicket, .intercityBus:
            TBHomeNavigations.main.navigate(.intercityBus)
//        case .ferry, .ferryTicket:
//            TBHomeNavigations.main.navigate(.ferry)
//        case .toll, .tollTicket:
//            TBHomeNavigations.main.navigate(.toll)
        case .flightTicket, .flights:
            TBHomeNavigations.main.navigate(.flight)
        case .allTransactionReport:
            if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "TransactionReport", delegate: self)
            } else {
                self.navigateToTransactionReport()
            }
        case .topupRechargeReport:
            if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "TopupReport", delegate: self)
            } else {
                self.navigateToTopupRecharge()
            }
        case .billPaymentReport:
            if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "BillPaymentReport", delegate: self)
            } else {
                self.navigateToBillPaymentReport()
            }
        case .receivedMoneyReport:
            if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "ReceivedMoneyReport", delegate: self)
            } else {
                self.navigateToReceivedMoneyReport()
            }
//        case .promotions:
//            TBHomeNavigations.main.navigate(.promotions)
        case .offers:
            TBHomeNavigations.main.navigate(.offers)
        case .luckyDrawCoupon:
            TBHomeNavigations.main.navigate(.luckyDrawCoupon)
        case .foodWallet:
            TBHomeNavigations.main.navigate(.foodwallet)
        default:
            self.identifyKeyAndNavigateCSection(keyCase: keyCase)
        }
    }
    
    private func identifyKeyAndNavigateCSection(keyCase: DashboardSearchKeys) {
        switch keyCase {
        case .loyalty:
            TBHomeNavigations.main.navigate(.loyalty)
            //        case .chinaMobileRecharge:
            //            TBHomeNavigations.main.navigate(.ChinaMobRecharge)
            //        case .thaiMobileRecharge:
        //            TBHomeNavigations.main.navigate(.ThaiMobRecharge)
        case .oKChat:
            TBHomeNavigations.main.navigate(.okChat)
//        case .liveTaxi:
//            TBHomeNavigations.main.navigate(.LiveTaxi)
        case .merchantPayment:
            TBHomeNavigations.main.navigate(.merchantPayment)
        case .bankCounterDeposit:
            TBHomeNavigations.main.navigate(.bankCounterDeposit)
        case .nearbyOKServices:
            TBHomeNavigations.main.navigate(.nearbyOKServices)
        case .cashIn:
            TBHomeNavigations.main.navigate(.cashInPayto)
        case .cashOut:
            TBHomeNavigations.main.navigate(.cashOutPayto)
        case .billSplitter:
            TBHomeNavigations.main.navigate(.BillSpliter)
        case .electricity:
            TBHomeNavigations.main.navigate(.electricity)
        case .nearMe:
            TBHomeNavigations.main.navigate(.nearMe)
        case .scanPay:
            TBHomeNavigations.main.navigate(.scanPay)
        case .taxPaymentNew:
            TBHomeNavigations.main.navigate(.taxPaymentNew)
        case .hotel:
            TBHomeNavigations.main.navigate(.hotel)
        case .solar:
            TBHomeNavigations.main.navigate(.solarElectricity)
        case .lottery:
            TBHomeNavigations.main.navigate(.lottery)
        default:
            self.identifyKeyAndNavigateDSection(keyCase: keyCase)
        }
    }
    
    private func identifyKeyAndNavigateDSection(keyCase: DashboardSearchKeys) {
        switch keyCase {
        case .buyBonusPoint:
            TBHomeNavigations.main.navigate(.BuyBonusPoint)
        case .transferTo:
            TBHomeNavigations.main.navigate(.transferTo)
        case .instaPay:
            TBHomeNavigations.main.navigate(.instaPay)
        case .requestCashIn:
            TBHomeNavigations.main.navigate(.cashIN)
        case .requestCashOut:
            TBHomeNavigations.main.navigate(.cashOUT)
        case .insurance:
            TBHomeNavigations.main.navigate(.insurance)
//        case .donation:
//            TBHomeNavigations.main.navigate(.donation)
        case .redeemBonusPoint:
            TBHomeNavigations.main.navigate(.redeemPoint)
        case .saleBonusPoint:
            TBHomeNavigations.main.navigate(.bonusPointResale)
        case .bonusPointSetup:
            TBHomeNavigations.main.navigate(.loyalty)
        case .earningPointTransfer:
            TBHomeNavigations.main.navigate(.earnPointTransfer)
        case .voting:
            TBHomeNavigations.main.navigate(.voting)
        case .sunKingPayment:
            TBHomeNavigations.main.navigate(.sunKingPayment)
        default:
            break
        }
    }
    
    private func handleNormalListSelection(item: DashboardSearchKeys) {
        identifyKeyAndNavigateASection(keyCase: item)
    }
    
    private func handleSearchListSelection(item: DashboardSearchKeys) {
        identifyKeyAndNavigateASection(keyCase: item)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}

// MARK: -UISearchBarDelegate
extension SearchHomeViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            uiButton.setTitle("Cancel".localized, for:.normal)
            switch appDelegate.currentLanguage {
            case "en":
                if let navFont = UIFont(name: appFont, size: 18) {
                    uiButton.titleLabel?.font = navFont
                }
            default:
                if let navFont = UIFont(name: appFont, size: 14) {
                    uiButton.titleLabel?.font = navFont
                }
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            screenMode = .normal
            tableViewSearch.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            if updatedText.count > 50 {
                return false
            }
            if updatedText != "" && text != "\n" {
                screenMode = .search
                searchFromKey(ketText: updatedText)
            }
        }
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        screenMode = .normal
         self.searchBar.showsCancelButton = false
        tableViewSearch.reloadData()
        //searchString = nil
         searchBar.endEditing(true)
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}

extension SearchHomeViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        switch screenMode {
        case .normal:
            viewEmptyRecords.isHidden = true
            return frequentArray.count
        case .search:
            if searchResultList.count == 0 {
                viewEmptyRecords.isHidden = false
            } else {
                viewEmptyRecords.isHidden = true
            }
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch screenMode {
        case .normal:
            let countValue = frequentArray[section].rowArray.count
            if countValue == 0 {
                let noDataLabel = UILabel(frame: tableView.bounds)
                noDataLabel.text = "No records found!".localized
                noDataLabel.textAlignment = .center
                tableView.backgroundView = noDataLabel
            } else {
                tableView.backgroundView = nil
            }
            return countValue > 8 ? 8 : countValue
        case .search:
            return searchResultList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let searchCell = tableView.dequeueReusableCell(withIdentifier: searchCellID, for: indexPath)
        searchCell.textLabel?.textAlignment = .center
        if let myFont = UIFont(name: appFont, size: 16) {
            searchCell.textLabel?.font = myFont
        }
        switch screenMode {
        case .normal:
            searchCell.textLabel?.text = frequentArray[indexPath.section].rowArray[indexPath.row].rawValue.localized
        case .search:
            searchCell.textLabel?.text = searchResultList[indexPath.row].rawValue.localized
        }
        return searchCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch screenMode {
        case .normal:
            DashboardSearchCoreDataManager.sharedInstance.insertUpdateSearchKeyCountInDB(caseString: frequentArray[indexPath.section].rowArray[indexPath.row].rawValue)
            handleNormalListSelection(item: frequentArray[indexPath.section].rowArray[indexPath.row])
        case .search:
            DashboardSearchCoreDataManager.sharedInstance.insertUpdateSearchKeyCountInDB(caseString: searchResultList[indexPath.row].rawValue)
            handleSearchListSelection(item: searchResultList[indexPath.row])
        }
        searchBar.endEditing(true)
        refreshTable()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch screenMode {
        case .normal:
            if frequentArray[section].rowArray.count != 0 {
                return frequentArray[section].sectionHeader
            }
            return nil
        case .search:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch screenMode {
        case .normal:
            if frequentArray[section].rowArray.count != 0 {
                return 44.0
            }
            return 0
        case .search:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        switch screenMode {
        case .normal:
            let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
            if let myFont = UIFont(name: appFont, size: 16) {
                header.textLabel?.font = myFont
            }
            header.textLabel?.textColor = ConstantsColor.navigationHeaderTransaction
            header.superview?.backgroundColor = .white
            header.textLabel?.textAlignment = .center
        default:
            break
        }
    }
}

extension SearchHomeViewController: BioMetricLoginDelegate {
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful {
            if screen == "TransactionReport" {
                navigateToTransactionReport()
            }else if screen == "TopupReport" {
                navigateToTopupRecharge()
            }else if screen == "BillPaymentReport" {
                navigateToBillPaymentReport()
            }else if screen == "ReceivedMoneyReport" {
                navigateToReceivedMoneyReport()
            }else if screen == "NavigateToSendMoneyBank" {
                navigateToSendMoneyToBank()
            }
        }
    }
}
