//
//  DashboardSearchHelper.swift
//  OK
//
//  Created by ANTONY on 10/05/2018.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import Foundation

enum DashboardSearchKeys: String {
    
    case landlineNumber = "Landline Number", postpaidNumber = "Postpaid Number", prepaidNumber = "Prepaid Number", resale = "Resale", buyOk$Point = "Buy OK$ Point"
    case topupRequest = "Top up Request", dataPlan = "Data Plan", specialOffers = "Special Offers", otherTopup = "Other Topup"
    case viberOut = "Viber Out", oK$Account = "OK$ Account", myNumberTopUp = "My Number TopUp", otherNumberTopUp = "Other Number TopUp"
    case paytoFuel = "Pay to Fuel", paytoMerchant = "Pay to Merchant", paymentRequest = "Payment Request", paytoSupermarket = "Pay to Supermarket"
    case paytoRestaurant = "Pay to Restaurant", sendMoneyToBank = "Send Money To Bank", saiSaiPay = "Sai Sai Pay" ,intercityBusTicket = "Intercity Bus Ticket"
    case flightTicket = "Flight Ticket", allTransactionReport = "All Transaction Report", receivedMoneyReport = "Received Money Report"
    case topupRechargeReport = "Topup & Recharge Report", billPaymentReport = "Bill Payment Report", offers = "Offers", luckyDrawCoupon = "Lucky Draw Coupon"
    case requestForOKMoney = "Request For OK$ Money", requestForTopup = "Request For Top up", voucher = "Voucher", rechargeOtherNumber = "Recharge Other Number", foodWallet = "Food Wallet" , lottery = "lottery"
    case loyalty = "Loyalty", rechargeMyNumber = "Recharge My Number", paySend = "Pay / Send", reSale = "Re-Sale" , InternationalTopUp = "International Top-Up"
    case landlineBill = "Landline Bill", postpaidMobile = "Postpaid Mobile"
    case oKChat = "OK$ Chat", liveTaxi = "Live Taxi", merchantPayment = "Merchant Payment", addWithdraw = "Add / Withdraw" , scanPay = "Scan/Pay"
    case bankCounterDeposit = "Bank Counter Deposit", nearbyOKServices = "Nearby OK$ Services", cashIn = "Cash In", cashOut = "Cash Out", intercityBus = "Intercity Bus"
    case billSplitter = "Bill Splitter", electricity = "Electricity" //bus = "Bus"
    case nearMe = "Near Me", taxPaymentNew = "Tax Payment"
    case skynet = "Skynet", hotel = "Hotel", flights = "Flights", requestMoney = "Request Money", solar = "Solar Home"
    case buyBonusPoint = "Buy Bonus Point", transferTo = "Face ID Pay", instaPay = "Instant Pay", requestCashIn = "Request Cash In_without_newline", requestCashOut = "Request Cash Out_without_newline"
    case insurance = "Insurance", redeemBonusPoint = "Redeem Bonus Point", saleBonusPoint = "Sale Bonus Point", mobileAndElectronics = "Mobile And Electronics" , onestopmart = "1 Stop Mart Online"//donation = "Donation"
    case bonusPointSetup = "Bonus Point Setup", earningPointTransfer = "Earning Point Transfer", voting = "Voting", sunKingPayment = "Sun King Payment"
    case airtel = "DTH", reliance = "Reliance", tataSky = "Tata Sky", sunDirect = "Sun Direct", videocon = "Videocon", dishTv = "Dish Tv" //dTHBill = "DTH"
    //    case skype = "Skype", myanTalk = "Myan Talk", overseasTopup = "Overseas Topup", chinaMobileRecharge = "China Mobile Recharge", overseasRecharge = "Overseas Recharge", thaiMobileRecharge = "Thai Mobile Recharge"
    //ferryTicket = "Ferry Ticket", ferry = "Ferry", tollTicket = "Toll Ticket", paytoBus = "Pay to Bus", toll = "Toll"
    
    var keyArray: [String] {
        switch self {
        case .landlineNumber:
            return ["Top Up", "Mobile", "Phone", "Recharge", "Data", "Special Offers", "Ooredoo", "MPT", "Telenor", "Mectel", "Pay Bill", "Bill", DashboardSearchKeys.landlineNumber.rawValue]
        case .postpaidNumber:
            return ["Top Up", "Mobile", "Phone", "Recharge", "Data", "Special Offers", "Ooredoo", "MPT", "Telenor", "Mectel", "Pay Bill", "Bill", DashboardSearchKeys.postpaidNumber.rawValue]
        case .prepaidNumber:
            return ["Top Up", "Mobile", "Phone", "Recharge", "Data", "Special Offers", "Ooredoo", "MPT", "Telenor", "Mectel", "Pay Bill", "Bill", DashboardSearchKeys.prepaidNumber.rawValue]
        case .resale:
            return ["Top Up", "Mobile", "Phone", "Recharge", "Data", "Special Offers", "Ooredoo", "MPT", "Telenor", "Mectel", "Pay Bill", "Bill", DashboardSearchKeys.resale.rawValue]
        case.buyOk$Point:
        return [DashboardSearchKeys.buyOk$Point.rawValue]
            
        case .topupRequest:
            return ["Top Up", "Mobile", "Phone", "Recharge", "Data", "Special Offers", "Ooredoo", "MPT", "Telenor", "Mectel", "Pay Bill", "Bill", DashboardSearchKeys.topupRequest.rawValue]
        case .dataPlan:
            return ["Top Up", "Mobile", "Phone", "Recharge", "Data", "Special Offers", "Ooredoo", "MPT", "Telenor", "Mectel", "Pay Bill", "Bill", DashboardSearchKeys.dataPlan.rawValue]
        case .specialOffers:
            return ["Top Up", "Mobile", "Phone", "Recharge", "Data", "Special Offers", "Ooredoo", "MPT", "Telenor", "Mectel", "Pay Bill", "Bill", DashboardSearchKeys.specialOffers.rawValue]
        case .otherTopup:
            return ["Top Up", "Mobile", "Phone", "Recharge", "Data", "Special Offers", "Ooredoo", "MPT", "Telenor", "Mectel", "Pay Bill", "Bill", DashboardSearchKeys.otherTopup.rawValue]
            
        case .viberOut:
            return ["Top Up", "Mobile", "Phone", "Recharge", "Data", "Special Offers", "Ooredoo", "MPT", "Telenor", "Mectel", "Pay Bill", "Bill", DashboardSearchKeys.viberOut.rawValue]
        case .oK$Account:
            return ["Top Up", "Mobile", "Phone", "Recharge", "Data", "Special Offers", "Ooredoo", "MPT", "Telenor", "Mectel", "Pay Bill", "Bill", DashboardSearchKeys.oK$Account.rawValue]
        case .myNumberTopUp:
            return ["Top Up", "Mobile", "Phone", "Recharge", "Data", "Special Offers", "Ooredoo", "MPT", "Telenor", "Mectel", "Pay Bill", "Bill", DashboardSearchKeys.myNumberTopUp.rawValue]
        case .otherNumberTopUp:
            return ["Top Up", "Mobile", "Phone", "Recharge", "Data", "Special Offers", "Ooredoo", "MPT", "Telenor", "Mectel", "Pay Bill", "Bill", DashboardSearchKeys.otherNumberTopUp.rawValue]
        
        case .paytoFuel:
            return ["Pay", "Hotel", "Send Money", "Shop", "Restaurant", DashboardSearchKeys.paytoFuel.rawValue]
        case .paytoMerchant:
            return ["Pay", "Hotel", "Send Money", "Shop", "Restaurant", DashboardSearchKeys.paytoMerchant.rawValue]
        case .paymentRequest:
            return ["Pay", "Hotel", "Send Money", "Shop", "Restaurant", DashboardSearchKeys.paymentRequest.rawValue]
        case .paytoSupermarket:
            return ["Pay", "Hotel", "Send Money", "Shop", "Restaurant", DashboardSearchKeys.paytoSupermarket.rawValue]
        case .paytoRestaurant:
            return ["Pay", "Hotel", "Send Money", "Shop", "Restaurant", DashboardSearchKeys.paytoRestaurant.rawValue]
        case .sendMoneyToBank:
            return ["Pay", "Hotel", "Send Money", "Shop", "Restaurant", DashboardSearchKeys.sendMoneyToBank.rawValue]
        case .intercityBusTicket:
            return ["Ticket", "Travel", "Bus", "Ferry", "Toll", "Book Flight", DashboardSearchKeys.intercityBusTicket.rawValue]
        case .flightTicket:
            return ["Ticket", "Travel", "Bus", "Ferry", "Toll", "Book Flight", DashboardSearchKeys.flightTicket.rawValue]
        case .allTransactionReport:
            return ["Report", "History", DashboardSearchKeys.allTransactionReport.rawValue]
        case .receivedMoneyReport:
            return ["Report", "History", DashboardSearchKeys.receivedMoneyReport.rawValue]
        case .topupRechargeReport:
            return ["Report", "History", DashboardSearchKeys.topupRechargeReport.rawValue]
        case .billPaymentReport:
            return ["Report", "History", DashboardSearchKeys.billPaymentReport.rawValue]
//        case .promotions:
//            return ["Deal", "Offer", "Promotion", DashboardSearchKeys.promotions.rawValue]
        case .offers:
            return ["Deal", "Offer", "Promotion", DashboardSearchKeys.offers.rawValue]
        case .luckyDrawCoupon:
            return ["Deal", "Offer", "Promotion", DashboardSearchKeys.luckyDrawCoupon.rawValue]
        case .requestForOKMoney:
            return ["Request", "Loan", "Credit", DashboardSearchKeys.requestForOKMoney.rawValue]
        case .requestForTopup:
            return ["Request", "Loan", "Credit", DashboardSearchKeys.requestForTopup.rawValue]
        case .voucher:
            return [DashboardSearchKeys.voucher.rawValue]
        case .rechargeOtherNumber:
            return [DashboardSearchKeys.rechargeOtherNumber.rawValue]
        case .foodWallet:
            return [DashboardSearchKeys.foodWallet.rawValue]
        case .loyalty:
            return [DashboardSearchKeys.loyalty.rawValue]
        case .rechargeMyNumber:
            return getRechargeMyNumberKeys()
        case .paySend:
            return [DashboardSearchKeys.paySend.rawValue]
        case .reSale:
            return [DashboardSearchKeys.reSale.rawValue]
        case .landlineBill:
            return [DashboardSearchKeys.landlineBill.rawValue]
        case .postpaidMobile:
            return [DashboardSearchKeys.postpaidMobile.rawValue]
        case .oKChat:
            return [DashboardSearchKeys.oKChat.rawValue]
        case .liveTaxi:
            return [DashboardSearchKeys.liveTaxi.rawValue]
        case .merchantPayment:
            return [DashboardSearchKeys.merchantPayment.rawValue]
        case .addWithdraw:
            return [DashboardSearchKeys.addWithdraw.rawValue]
        case .bankCounterDeposit:
            return [DashboardSearchKeys.bankCounterDeposit.rawValue]
        case .nearbyOKServices:
            return [DashboardSearchKeys.nearbyOKServices.rawValue]
        case .cashIn:
            return [DashboardSearchKeys.cashIn.rawValue]
        case .cashOut:
            return [DashboardSearchKeys.cashOut.rawValue]
        case .intercityBus:
            return [DashboardSearchKeys.intercityBus.rawValue]
//        case .bus:
//            return [DashboardSearchKeys.bus.rawValue]
        case .billSplitter:
            return [DashboardSearchKeys.billSplitter.rawValue]
        case .nearMe:
            return [DashboardSearchKeys.nearMe.rawValue]
        case .taxPaymentNew:
            return [DashboardSearchKeys.taxPaymentNew.rawValue]
        case .scanPay:
            return [DashboardSearchKeys.scanPay.rawValue]
        case .electricity:
            return [DashboardSearchKeys.electricity.rawValue]
        case .skynet:
            return [DashboardSearchKeys.skynet.rawValue]
        case .hotel:
            return [DashboardSearchKeys.hotel.rawValue]
        case .flights:
            return [DashboardSearchKeys.flights.rawValue]
        case .requestMoney:
            return [DashboardSearchKeys.requestMoney.rawValue]
        case .solar:
            return [DashboardSearchKeys.solar.rawValue]
        case .buyBonusPoint:
            return [DashboardSearchKeys.buyBonusPoint.rawValue]
        case .transferTo:
            return [DashboardSearchKeys.transferTo.rawValue]
        case .instaPay:
            return [DashboardSearchKeys.instaPay.rawValue]
        case .lottery:
            return [DashboardSearchKeys.lottery.rawValue]
        case .requestCashIn:
            return [DashboardSearchKeys.requestCashIn.rawValue]
        case .requestCashOut:
            return [DashboardSearchKeys.requestCashOut.rawValue]
        case .insurance:
            return [DashboardSearchKeys.insurance.rawValue]
        case .mobileAndElectronics:
            return [DashboardSearchKeys.mobileAndElectronics.rawValue]
        case .onestopmart:
            return [DashboardSearchKeys.onestopmart.rawValue]
//        case .donation:
//            return [DashboardSearchKeys.donation.rawValue]
        case .redeemBonusPoint:
            return [DashboardSearchKeys.redeemBonusPoint.rawValue]
        case .saleBonusPoint:
            return [DashboardSearchKeys.saleBonusPoint.rawValue]
        case .bonusPointSetup:
            return [DashboardSearchKeys.bonusPointSetup.rawValue]
        case .earningPointTransfer:
            return [DashboardSearchKeys.earningPointTransfer.rawValue]
        case .voting:
            return [DashboardSearchKeys.voting.rawValue]
        case .saiSaiPay:
                return [DashboardSearchKeys.saiSaiPay.rawValue]
        case .sunKingPayment:
            return [DashboardSearchKeys.sunKingPayment.rawValue]
                    case .airtel:
                        return [DashboardSearchKeys.airtel.rawValue]
                    case .reliance:
                        return [DashboardSearchKeys.reliance.rawValue]
                    case .tataSky:
                        return [DashboardSearchKeys.tataSky.rawValue]
                    case .sunDirect:
                        return [DashboardSearchKeys.sunDirect.rawValue]
                    case .videocon:
                        return [DashboardSearchKeys.videocon.rawValue]
                    case .dishTv:
                        return [DashboardSearchKeys.dishTv.rawValue]
            //        case .skype:
            //            return ["Top Up", "Mobile", "Phone", "Recharge", "Data", "Special Offers", "Ooredoo", "MPT", "Telenor", "Mectel", "Pay Bill", "Bill", DashboardSearchKeys.skype.rawValue]
            //        case .myanTalk:
            //            return ["Top Up", "Mobile", "Phone", "Recharge", "Data", "Special Offers", "Ooredoo", "MPT", "Telenor", "Mectel", "Pay Bill", "Bill", DashboardSearchKeys.myanTalk.rawValue]
         case .InternationalTopUp:
            return [DashboardSearchKeys.InternationalTopUp.rawValue]
//                    case .chinaMobileRecharge:
//                        return [DashboardSearchKeys.chinaMobileRecharge.rawValue]
            //        case .overseasTopup:
            //            return ["Top Up", "Mobile", "Phone", "Recharge", "Data", "Special Offers", "Ooredoo", "MPT", "Telenor", "Mectel", "Pay Bill", "Bill", DashboardSearchKeys.overseasTopup.rawValue]
            //        case .thaiMobileRecharge:
            //            return [DashboardSearchKeys.thaiMobileRecharge.rawValue]
            //        case .dTHBill:
            //            return ["Top Up", "Mobile", "Phone", "Recharge", "Data", "Special Offers", "Ooredoo", "MPT", "Telenor", "Mectel", "Pay Bill", "Bill", DashboardSearchKeys.dTHBill.rawValue]
//        case .ferryTicket:
//            return ["Ticket", "Travel", "Bus", "Ferry", "Toll", "Book Flight", DashboardSearchKeys.ferryTicket.rawValue]
//        case .tollTicket:
//            return ["Ticket", "Travel", "Bus", "Ferry", "Toll", "Book Flight", DashboardSearchKeys.tollTicket.rawValue]
//        case .ferry:
//            return [DashboardSearchKeys.ferry.rawValue]
//        case .paytoBus:
//            return ["Pay", "Hotel", "Send Money", "Shop", "Restaurant", DashboardSearchKeys.paytoBus.rawValue]
//        case .toll:
//            return [DashboardSearchKeys.toll.rawValue]
//        case .dTHBill:
//            return [DashboardSearchKeys.dTHBill.rawValue]
        }
    }
    
    func getRechargeMyNumberKeys() -> [String] {
        var rechargeMyNumberKeys = ["Recharge My Number"]
        if UserModel.shared.formattedNumber.count > 0 {
            rechargeMyNumberKeys.append(UserModel.shared.formattedNumber)
        }
        return rechargeMyNumberKeys
    }
}
