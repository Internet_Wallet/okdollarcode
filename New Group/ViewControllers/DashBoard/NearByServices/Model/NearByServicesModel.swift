//
//  NearByServicesModel.swift
//  OK
//
//  Created by SHUBH on 6/14/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class NearByServicesModel: NSObject {

    var firstName:String? = ""
    var lastName:String? = ""
    var countryCode:String? = ""
    var phoneNumber:String? = ""
    var email:String? = ""
    var address:String? = ""
    var type: Int? = 0
    var agentType:String? = ""
    var isActive:Bool? = false
    var okDollarBalance: Int? = 0
    var cashIn: Bool? = false
    var cashOut: Bool? = false
    var isOpen: Bool? = false
    var isOpenAlways: Bool? = false
    var isOffersEnable: Bool? = false
    var openingTime:String? = ""
    var closingTime:String? = ""
    var townShipName: String? = ""
    var cityName: String? = ""
    var businessName: String? = ""
    var country: String? = ""
    var divisionName: String? = ""
    var addressLine1: String? = ""
    var addressLine2: String? = ""
    var cellId: String? = ""
    var state: String? = ""
    var rating: Int? = 0
    var totalRateCount: Int? = 0
    var category: String? = ""
    var subCategory: String? = ""
    var createdDate: String? = ""
    var createdBy: String? = ""
    var id: String? = ""
    var transactionId: String?  = ""
    var distanceInKm: Double?  = 0.0
    var distanceInMiles: Double?  = 0.0
    var nearBy_GeoLocation : NearByGeoLocation?
    
     init(model: PTMapNearbyMerchant) {
        self.firstName = model.agentName.safelyWrappingString()
        self.lastName  = ""
        let country = identifyCountry(withPhoneNumber: model.phoneNumber.safelyWrappingString())
        self.countryCode = country.countryCode
        self.phoneNumber = model.phoneNumber.safelyWrappingString()
        self.email = ""
        if let adress = model.address {
            self.address = String.init(format: "%@ %@",adress.addressLine2.safelyWrappingString(),adress.addressLine1.safelyWrappingString())
        }
        self.isActive = (model.isActive.safelyWrappingString() == "1") ? true : false
        self.openingTime = model.shopOpenTime.safelyWrappingString()
        self.closingTime = model.shopCloseTime.safelyWrappingString()
        self.townShipName = model.shopTownShip.safelyWrappingString()
        self.cityName = model.shopCity.safelyWrappingString()
        self.businessName = model.businessCategory?.name.safelyWrappingString()
        self.country = model.address?.country.safelyWrappingString()
        self.divisionName = model.shopDivision.safelyWrappingString()
        self.addressLine1 = model.address?.addressLine1.safelyWrappingString()
        self.addressLine2 = model.address?.addressLine2.safelyWrappingString()
        self.state = model.address?.state?.name.safelyWrappingString()
        self.category = model.businessCategory?.name.safelyWrappingString()
        self.subCategory = model.businessCategory?.businessType?.name.safelyWrappingString()
        self.createdDate = ""
        self.createdBy = ""
        self.id = model.shopID
        self.transactionId = ""
        self.distanceInKm = 0.0
        self.distanceInMiles = 0.0

        self.nearBy_GeoLocation = NearByGeoLocation.init(lat: model.geoLocationShop?.latitude, long: model.geoLocationShop?.longitude)        
    }

    init(dictionary: Dictionary<String, AnyObject>) {
        super.init()
        
        if let FirstName = dictionary["FirstName"] as? String{
            self.firstName = FirstName
        }
        if let LastName = dictionary["LastName"] as? String {
            self.lastName = LastName
        }
        if let CountryCode = dictionary["CountryCode"] as? String {
            self.countryCode = CountryCode
        }
        if let PhoneNumber = dictionary["PhoneNumber"] as? String {
            self.phoneNumber = PhoneNumber
        }
        if let Email = dictionary["Email"] as? String {
            self.email = Email
        }
        if let Address = dictionary["Address"] as? String {
            self.address = Address
        }
        if let Type = dictionary["Type"] as? Int {
            self.type = Type
        }
        if let AgentType = dictionary["AgentType"] as? String {
            self.agentType = AgentType
        }
        if let IsActive = dictionary["IsActive"] as? Bool {
            self.isActive = IsActive
        }
        if let OkDollarBalance = dictionary["OkDollarBalance"] as? Int {
            self.okDollarBalance = OkDollarBalance
        }
        if let title = dictionary["CashIn"] as? Bool {
            self.cashIn = title
        }
        if let title = dictionary["CashOut"] as? Bool {
            self.cashOut = title
        }
        if let title = dictionary["IsOpen"] as? Bool {
            self.isOpen = title
        }
        if let title = dictionary["IsOpenAlways"] as? Bool {
            self.isOpenAlways = title
        }
        if let title = dictionary["OpeningTime"] as? String {
            self.openingTime = title
        }
        if let title = dictionary["ClosingTime"] as? String {
            self.closingTime = title
        }
        if let title = dictionary["TownShipName"]  as? String {
            self.townShipName = title
        }
        if let title = dictionary["CityName"] as? String {
            self.cityName = title
        }
        if let title = dictionary["BusinessName"] as? String {
            self.businessName = title
        }
        if let title = dictionary["Country"] as? String {
            self.country = title
        }
        if let title = dictionary["DivisionName"] as? String   {
            self.divisionName = title
        }
        if let title = dictionary["Line1"] as? String   {
            self.addressLine1 = title
        }
        if let title = dictionary["Line2"] as? String   {
            self.addressLine2 = title
        }
        if let title = dictionary["CellId"] as? String   {
            self.cellId = title
        }
        if let title = dictionary["State"] as? String   {
            self.state = title
        }
        if let title = dictionary["Rating"] as? Int   {
            self.rating = title
        }
        if let title = dictionary["TotalRateCount"] as? Int   {
            self.totalRateCount = title
        }
        if let title = dictionary["Category"] as? String   {
            self.category = title
        }
        if let title = dictionary["SubCategory"] as? String   {
            self.subCategory = title
        }
        if let title = dictionary["CreatedDate"] as? String   {
            self.createdDate = title
        }
        if let title = dictionary["CreatedBy"] as? String   {
            self.createdBy = title
        }
        if let title = dictionary["Id"] as? String   {
            self.id = title
        }
        if let title = dictionary["TransactionId"] as? String   {
            self.transactionId = title
        }
        if let title = dictionary["DistanceInKm"] as? Double   {
            self.distanceInKm = title
        }
        if let title = dictionary["DistanceInMiles"] as? Double   {
            self.distanceInMiles = title
        }
        if let title = dictionary["CurrentLocation"] as? Dictionary<String,Any> {
            self.nearBy_GeoLocation = NearByGeoLocation.init(dictionary: title)
        }
    }
    
}

class NearByGeoLocation: NSObject {
    var lattitude     : Double?
    var longitude     : Double?
    
    init(lat: String?, long: String?) {
        if let latt = lat {
            self.lattitude = (latt as NSString).doubleValue
        }
        
        if let longg = long {
            self.longitude = (longg as NSString).doubleValue
        }
    }
    
    init(dictionary: Dictionary<String, Any>) {
        super.init()
        if let title = dictionary["Latitude"] as? Double {
            self.lattitude = title
        }
        if let title = dictionary["Longitude"] as? Double {
            self.longitude = title
        }
}
}


