//
//  NBSCustomAnnotation.swift
//  OK
//
//  Created by gauri OK$ on 6/7/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import MapKit

class NBSCustomAnnotation: NSObject, MKAnnotation {
    
    let title: String?
    let coordinate: CLLocationCoordinate2D
    
    init(title: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.coordinate = coordinate
    }
}
