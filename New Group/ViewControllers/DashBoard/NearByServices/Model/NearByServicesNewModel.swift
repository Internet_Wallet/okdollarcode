//
//  NearByServicesNewModel.swift
//  OK
//
//  Created by prabu on 04/06/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation

import UIKit

class NearByServicesNewModel: NSObject {
    var distanceInKm: Double?  = 0.0
    var distanceInMiles: Double?  = 0.0
    var GroupId: String?  = ""
    var UserContactType: String?  = ""

    var UserContactData : UserContact?

    init(dictionary: Dictionary<String, AnyObject>) {
        super.init()
        
        if let title = dictionary["DistanceInKm"] as? Double   {
            self.distanceInKm = title
        }
        if let title = dictionary["DistanceInMiles"] as? Double   {
            self.distanceInMiles = title
        }
        if let title = dictionary["GroupId"] as? String   {
            self.GroupId = title
        }
        if let title = dictionary["UserContactType"] as? String   {
            self.UserContactType = title
        }
        
        if let title = dictionary["UserContact"] as? Dictionary<String,Any> {
            self.UserContactData = UserContact.init(dictionary: title as Dictionary<String, AnyObject>)
        }
        
    }
    
}

class UserContact: NSObject {
    var Address:String? = ""
    var BankName:String? = ""
    var Branch:String? = ""
    var BusinessName:String? = ""
    var CashInLimit: Double?  = 0.0
    var CashOutLimit: Double?  = 0.0
    var ContactNumber:String? = ""
    var CreatedBy:String? = ""
    var Division:String? = ""
    var FirstName:String? = ""
    var Id:String? = ""
    var ModifiedDate:String? = ""
    var OfficeCloseDay:String? = ""
    var OfficeHourFrom:String? = ""
    var OfficeHourTo:String? = ""
    var PhoneBill:String? = ""
    var PhoneNumber:String? = ""
    var PostOfficeName:String? = ""
    var PostalCode:String? = ""
    var Remark:String? = ""
    var SaleAndLock:String? = ""
    var Township:String? = ""
    var StreetName:String? = ""
    var TransactionId:String? = ""
    var type: Int? = 0
    var CashType: Int? = 0
    var LocationNewData : LocationNew?
    var ImageUrl:String? = ""
    var AddressLine1:String? = ""
    var AddressLine2:String? = ""
    var OpeningTime:String? = ""
    var ClosingTime:String? = ""
    
    init(dictionary: Dictionary<String, AnyObject>) {
        super.init()
        
        if let title = dictionary["Address"] as? String{
            self.Address = title
        }
        if let title = dictionary["BankName"] as? String {
            self.BankName = title
        }
        if let title = dictionary["Branch"] as? String {
            self.Branch = title
        }
        if let title = dictionary["BusinessName"] as? String {
            self.BusinessName = title
        }
        if let title = dictionary["CashInLimit"] as? Double {
            self.CashInLimit = title
        }
        if let title = dictionary["CashOutLimit"] as? Double {
            self.CashOutLimit = title
        }
        
        if let title = dictionary["ContactNumber"] as? String {
            self.ContactNumber = title
        }
        if let title = dictionary["CreatedBy"] as? String {
            self.CreatedBy = title
        }
        if let title = dictionary["Division"] as? String {
            self.Division = title
        }
        if let title = dictionary["FirstName"] as? String {
            self.FirstName = title
        }
        if let title = dictionary["Id"] as? String {
            self.Id = title
        }
        if let title = dictionary["ModifiedDate"] as? String {
            self.ModifiedDate = title
        }
        if let title = dictionary["OfficeCloseDay"] as? String {
            self.OfficeCloseDay = title
        }
        if let title = dictionary["OfficeHourFrom"] as? String {
            self.OfficeHourFrom = title
        }
        if let title = dictionary["OfficeHourTo"] as? String {
            self.OfficeHourTo = title
        }
        if let title = dictionary["PhoneBill"]  as? String {
            self.PhoneBill = title
        }
        if let title = dictionary["PhoneNumber"] as? String {
            self.PhoneNumber = title
        }
        if let title = dictionary["PostOfficeName"] as? String {
            self.PostOfficeName = title
        }
        if let title = dictionary["PostalCode"] as? String {
            self.PostalCode = title
        }
        if let title = dictionary["Remark"] as? String   {
            self.Remark = title
        }
        if let title = dictionary["SaleAndLock"] as? String   {
            self.SaleAndLock = title
        }
        if let title = dictionary["Township"] as? String   {
            self.Township = title
        }
        if let title = dictionary["StreetName"] as? String   {
            self.StreetName = title
        }
        if let title = dictionary["TransactionId"] as? String   {
            self.TransactionId = title
        }
        if let title = dictionary["ImageUrl"] as? String   {
            self.ImageUrl = title
        }
        if let title = dictionary["Type"] as? Int   {
            self.type = title
        }
        if let title = dictionary["CashType"] as? Int   {
            self.CashType = title
        }
        if let title = dictionary["Location"] as? Dictionary<String,Any> {
            self.LocationNewData = LocationNew.init(dictionary: title)
        }
        if let title = dictionary["Line1"] as? String   {
            self.AddressLine1 = title
        }
        if let title = dictionary["Line2"] as? String   {
            self.AddressLine2 = title
        }
        if let title = dictionary["OpeningTime"] as? String   {
            self.OpeningTime = title
        }
        if let title = dictionary["ClosingTime"] as? String   {
            self.ClosingTime = title
        }
       
    }
    

}

class LocationNew: NSObject {
    var Accuracy: Int? = 0
    var Angle: Int? = 0
    var CreatedTime: String?  = ""
    var Latitude: Double?  = 0.0
    var Longitude: Double?  = 0.0
    var Name: String?  = ""
    var Source: String?  = ""
    var Time: String?  = ""
    var type: String?  = ""
    var coordinates: String?  = ""

    //var nearBy_GeoLocation : NearByGeoLocationNew?
    
    init(dictionary: Dictionary<String, Any>) {
        super.init()
        if let title = dictionary["Accuracy"] as? Int {
            self.Accuracy = title
        }
        if let title = dictionary["Angle"] as? Int {
            self.Angle = title
        }
        if let title = dictionary["CreatedTime"] as? String {
            self.CreatedTime = title
        }
        if let title = dictionary["Latitude"] as? Double {
            self.Latitude = title
        }
        if let title = dictionary["Longitude"] as? Double {
            self.Longitude = title
        }
        if let title = dictionary["Name"] as? String {
            self.Name = title
        }
        if let title = dictionary["Source"] as? String {
            self.Source = title
        }
        if let title = dictionary["Time"] as? String {
            self.Time = title
        }
        if let title = dictionary["type"] as? String {
            self.type = title
        }
        
        if let title = dictionary["coordinates"] as? String {
            self.coordinates = title
        }
    }
}

class NearByGeoLocationNew: NSObject {
    
    var coordinates: String?  = ""

    init(dictionary: Dictionary<String, Any>) {
        super.init()
        if let title = dictionary["coordinates"] as? String {
            self.coordinates = title
        }
        
    }
}
