//
//  PageViewController.h
//

#import <UIKit/UIKit.h>
@class NearByPageViewController;

@protocol NearByPageViewControllerDelegates <NSObject>

@optional
-(void)didScrollToIndex:(NSInteger)index;

@end

@interface NearByPageViewController : UIPageViewController

@property(weak,nonatomic) id<NearByPageViewControllerDelegates> mDelegate;

@property(assign,nonatomic,getter=isCircular) BOOL circular;
@property(nonatomic,strong) NSArray<UIViewController*> *arrViewController;
@property(assign,nonatomic,readonly) NSInteger currentIndex;



-(void)getToIndex:(NSInteger)index animated:(BOOL)animated;

@end
