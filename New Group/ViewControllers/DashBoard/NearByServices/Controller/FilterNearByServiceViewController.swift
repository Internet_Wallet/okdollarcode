//
//  FilterNearByServiceViewController.swift
//  OK
//
//  Created by iMac on 2/27/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData


class FilterNearByServiceViewController: MapBaseViewController {
    
   // let sortList = ["Default".localized,"Amount High to Low".localized,"Amount Low to High".localized,"Name A to Z".localized,"Name Z to A".localized]
    
    var sortList : [String] = []
    var nearByList : [String] = []
    var statusNearBySort = "SortBy"
    var favouriteResult: [NSManagedObject] = []
    
    @IBOutlet weak var filterSortTableView: UITableView!
    
    @IBOutlet weak var headerTitle: UILabel! {
        didSet {
            self.headerTitle.font = UIFont(name: appFont, size: appFontSize)
            self.headerTitle.text = self.headerTitle.text?.localized
           
        }
    }
    
    //prabu
    let defaults = UserDefaults.standard
    weak var mainlocationdelegate: MainLocationViewDelegate?
    weak var mainlocationdelegateReg: MainLocationViewDelegateRegistration?
    weak var nearbylocationdelegate: MainNearByLocationViewDelegate?
    weak var maincurrentlocationdelegate: MaincurrentLocationViewDelegate?
    var findviewStr : String!
    @IBOutlet var sortviewhieghtConstrain: NSLayoutConstraint!
    @IBOutlet var nearviewhieghtConstrain: NSLayoutConstraint!
    @IBOutlet var categoryviewhieghtConstrain: NSLayoutConstraint!

    var checkMap:Bool?
    var selectedsubcategory = SubCategoryDetail ()
    var sortedindex = Int()
    var nearbyindex = Int()

    weak var applybuttondelegate : MainFilterApplyViewDelegate?

    @IBOutlet weak var sortByView: UIView!
    @IBOutlet weak var nearByView: UIView!

    //PopupView
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var sortByPopUpView: UIView!
    @IBOutlet weak var filterByPopUpView: UIView!

    //NearBy
    @IBOutlet weak var nearByTitle: UILabel!{
        didSet {
            self.nearByTitle.font = UIFont(name: appFont, size: appFontSize)
            self.nearByTitle.text = "Near By".localized
            
        }
    }
    
    //Sortby
    @IBOutlet weak var sortByTitle: UILabel!{
        didSet {
            self.sortByTitle.font = UIFont(name: appFont, size: appFontSize)
            self.sortByTitle.text = "Sort By".localized
            
        }
    }
    
    @IBOutlet weak var sortBySubTitle: UILabel!{
        didSet {
            self.sortBySubTitle.font = UIFont(name: appFont, size: appFontSize)
            self.sortBySubTitle.text = "Sort By".localized
            
        }
    }
    
    @IBOutlet weak var viewFavButton: UIButton!
    @IBOutlet var nearByDataTitle: UILabel!{
        didSet {
            self.nearByDataTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var nearByBtnOut: UIButton!

    @IBOutlet var sortByDataTitle: UILabel!{
        didSet {
            self.sortByDataTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var sortByBtnOut: UIButton!{
        didSet {
            self.sortByBtnOut.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var sortByTableView: UITableView!
    @IBOutlet weak var applyBtnOut: UIButton!{
        didSet {
            self.applyBtnOut.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
            self.applyBtnOut.setTitle("Apply".localized, for: .normal)
        }
    }

    //FilterBy
    @IBOutlet weak var filterByTitle: UILabel!{
        didSet {
            self.filterByTitle.font = UIFont(name: appFont, size: appFontSize)
            self.filterByTitle.text = "Filter By".localized
        }
    }
    @IBOutlet weak var filterBySubTitle: UILabel!{
        didSet {
            self.filterBySubTitle.font = UIFont(name: appFont, size: appFontSize)
            self.filterBySubTitle.text = "Filter By".localized
        }
    }
    @IBOutlet weak var filterByDataTitle: UILabel!{
        didSet {
            self.filterByDataTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var filterByBtnOut: UIButton!
    @IBOutlet weak var filterByTableView: UITableView!

    //Category
    @IBOutlet weak var categoryTitle: UILabel!{
        didSet {
            self.categoryTitle.font = UIFont(name: appFont, size: appFontSize)
            self.categoryTitle.text = "Category".localized
        }
    }
    @IBOutlet weak var categoryDataTitle: UILabel!{
        didSet {
            self.categoryDataTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var categoryBtnOut: UIButton!

    //Current Location
    @IBOutlet weak var currentLocationTitle: UILabel!{
        didSet {
            self.currentLocationTitle.font = UIFont(name: appFont, size: appFontSize)
            self.currentLocationTitle.text = "Current Location".localized
        }
    }
    @IBOutlet weak var currentLocationDataTitle: UILabel!{
        didSet {
            self.currentLocationDataTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var currentLocatoionBtnOut: UIButton!

    //Sort View
    weak var delegate: NearBySortViewDelegate?
    var previousSelectedSortOption: Int?
    var previousSelectedSortOptionnew: String?
    var previousSelectedNearByOption: Int?
    var previousSelectedNearbyOptionnew: String?
    
    var dataList: [Any]?
    var filterDataList: [Any]?
    weak var categoryDelegate: CategoriesSelectionDelegate?

    //Filter View
    weak var delegateFilter: NearByFilterViewDelegate?
    
    var previousSelectedFilterData: Any?
    var filterShowList: [Any]?
    var viewFrom: FromView?
    var currentNearByView: NearByView?
    
    //button
    @IBOutlet weak var closeBtn: UIButton! {
        didSet {
            //let image = UIImage(named: "close_overflow")?.withRenderingMode(.alwaysTemplate)
            //self.closeBtn.setImage(image, for: .normal)
            self.closeBtn.setImage(UIImage(named: "close_overflow"), for: .normal)

            self.closeBtn.tintColor = .white
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewFavButton.isHidden = true
        categoryviewhieghtConstrain.constant = 0.0
        
        if self.findviewStr == "NearByServices" || self.findviewStr == "Promotions" {
           //Hide for PayTo
            nearByView.isHidden = true
            sortByView.isHidden = false
            
            if self.findviewStr == "NearByServices" {
            
        sortList = ["Default".localized,"Name A to Z".localized,"Name Z to A".localized]
                
            } else {
                
        sortList = ["Default".localized,"Amount High to Low".localized,"Amount Low to High".localized,"Name A to Z".localized,"Name Z to A".localized]
                
            }
            
        if (defaults.value(forKey: "NBSsortBy") != nil) {
            self.sortByDataTitle.text = defaults.value(forKey: "NBSsortBy") as? String
        } else {
            self.sortByDataTitle.text = "Default".localized
        }
        
        if (defaults.value(forKey: "NBSfilterBy") != nil) {
            self.filterByDataTitle.text = defaults.value(forKey: "NBSfilterBy") as? String
        } else {
            self.filterByDataTitle.text = "Default".localized
        }
        
        if (defaults.value(forKey: "NBScategoryBy") != nil) {
            self.categoryDataTitle.text = defaults.value(forKey: "NBScategoryBy") as? String
        } else {
            self.categoryDataTitle.text = "Default".localized
        }
            
            nearviewhieghtConstrain.constant = 0.0

            if self.checkMap == true {
                sortviewhieghtConstrain.constant = 0.0
                self.sortByDataTitle.text = ""
            } else {
                sortviewhieghtConstrain.constant = 80.0
            }
            
            if self.findviewStr == "Promotions" {
                
                //Location Manage
                self.didSelectCurrentLocation()
                
                filterByTableView.tableFooterView = UIView()
                newremoveDuplicatePromotions()
                // Do any additional setup after loading the view.
                
            } else {
                
                //Location Manage
                self.didSelectCurrentLocation()
                
                filterByTableView.tableFooterView = UIView()
                removeDuplicatePromotions()
                // Do any additional setup after loading the view.
                
            }
            
        } else {
            //Show for PayTo
            nearByView.isHidden = false
            sortByView.isHidden = false
            sortList = ["Default".localized,"Name A to Z".localized,"Name Z to A".localized]
            nearByList = ["All".localized, "Agents".localized, "Branch Office".localized, "Merchants".localized, "Myanmar Post".localized, "Bank".localized,]
            
            
            if (defaults.value(forKey: "PTMNearBy") != nil) {
                // self.sortByDataTitle.text = defaults.value(forKey: "PTMsortBy") as? String
                
                self.nearByDataTitle.text = defaults.value(forKey: "PTMNearBy") as? String
            } else {
                self.nearByDataTitle.text = "All".localized
            }
            
            if (defaults.value(forKey: "PTMsortBy") != nil) {
               // self.sortByDataTitle.text = defaults.value(forKey: "PTMsortBy") as? String
                
                self.sortByDataTitle.text = self.previousSelectedSortOptionnew
            } else {
                self.sortByDataTitle.text = "Default".localized
            }
            
            if (defaults.value(forKey: "PTMfilterBy") != nil) {
                self.filterByDataTitle.text = defaults.value(forKey: "PTMfilterBy") as? String
            } else {
                self.filterByDataTitle.text = "Default".localized
            }
            
            if (defaults.value(forKey: "PTMcategoryBy") != nil) {
                self.categoryDataTitle.text = defaults.value(forKey: "PTMcategoryBy") as? String
            } else {
                self.categoryDataTitle.text = "Default".localized
            }
            nearviewhieghtConstrain.constant = 80.0

            if self.checkMap == true {
                sortviewhieghtConstrain.constant = 0.0
                self.sortByDataTitle.text = ""
            } else {
                sortviewhieghtConstrain.constant = 80.0
            }
            
            //Location Manage
            didSelectCurrentLocation()
            
            filterByTableView.tableFooterView = UIView()
            PTMnearByMerchantName()
            // Do any additional setup after loading the view.
            
        }
    }
    
    @IBAction func onClickViewFav(_ sender: Any) {
       
        if fetch() == 0{
           
            UitilityClass.displayAlert(title: "No Records", message: "Favourite List Not Available",view: self)
          //   self.showAlert(alertTitle: "No Records", alertBody: "Favourite List Not Available" , alertImage: #imageLiteral(resourceName: "payment_gateway_st"))
        }else{
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteListController") as? FavouriteListController
            if let data = obj{
                 let navController = UINavigationController(rootViewController: data)
                data.favouriteResult = favouriteResult
                self.presentDetail(navController)
            }
        }
        
    }
    
    func fetch() -> Int{
        favouriteResult.removeAll()
        guard let appdelegate = UIApplication.shared.delegate as? AppDelegate else {
            return 0
        }
        let managedContext = appdelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FavoritePromotion")
        do{
            let result = try managedContext.fetch(fetchRequest)
            for data in result as! [NSManagedObject]{
                favouriteResult.append(data)
            }
            
        }catch{
            print("Error")
        }
        
        return favouriteResult.count
        
    }
    
    
    
    func newremoveDuplicatePromotions() {
        var newFilteredData: [PromotionsModel] = []
        var checkSet = Set<String>()
        if let filterlist = filterDataList as? [PromotionsModel] {
            for promotion in filterlist {
                if let count = promotion.shop_Name?.count, count != 0 {
                    if !checkSet.contains((promotion.shop_Name ?? "")) {
                        newFilteredData.append(promotion)
                        checkSet.insert((promotion.shop_Name ?? ""))
                    }
                } else {
                    if !checkSet.contains((promotion.shop_AgentName ?? "")) {
                        newFilteredData.append(promotion)
                        checkSet.insert((promotion.shop_AgentName ?? ""))
                    }
                }
            }
            filterShowList?.removeAll()
            filterShowList = newFilteredData
        }
    }
    
    func removeDuplicatePromotions() {
        var newFilteredData: [NearByServicesModel] = []
        var checkSet = Set<String>()
        if let filterlist = filterDataList as? [NearByServicesModel] {
            for promotion in filterlist {
                if let count = promotion.businessName?.count, count != 0 {
                    if !checkSet.contains((promotion.businessName ?? "")) {
                        newFilteredData.append(promotion)
                        checkSet.insert((promotion.businessName ?? ""))
                    }
                } else {
                    if !checkSet.contains((promotion.firstName ?? "")) {
                        newFilteredData.append(promotion)
                        checkSet.insert((promotion.firstName ?? ""))
                    }
                }
            }
            filterShowList?.removeAll()
            filterShowList = newFilteredData
        }
    }
    
   
    
    func PTMnearByMerchantName() {
        var newFilteredData: [PTMapNearbyMerchant] = []
        var checkSet = Set<String>()
        if let filterlist = filterDataList as? [PTMapNearbyMerchant] {
            for object in filterlist {
                if let count = object.agentName?.count, count != 0 {
                    if !checkSet.contains((object.agentName ?? "")) {
                        newFilteredData.append(object)
                        checkSet.insert((object.agentName ?? ""))
                    }
                } else {
                    if !checkSet.contains((object.agentName ?? "")) {
                        newFilteredData.append(object)
                        checkSet.insert((object.agentName ?? ""))
                    }
                }
            }
            filterShowList?.removeAll()
            filterShowList = newFilteredData
        }
    }
    
    
    //All Button
    @IBAction func nearByBtn(_ sender: Any) {
        statusNearBySort = "NearBy"
        sortBySubTitle.text = "Near By".localized
        self.popupView.isHidden = false
        self.sortByPopUpView.isHidden = false
        self.filterByPopUpView.isHidden = true
        sortByTableView.dataSource = self
        sortByTableView.delegate = self
        sortByTableView.reloadData()
    }
    
    @IBAction func sortByBtn(_ sender: Any) {
        statusNearBySort = "SortBy"
        sortBySubTitle.text = "Sort By".localized
        self.popupView.isHidden = false
        self.sortByPopUpView.isHidden = false
        self.filterByPopUpView.isHidden = true
        sortByTableView.dataSource = self
        sortByTableView.delegate = self
        sortByTableView.reloadData()
        
    }
    
    @IBAction func filterByBtn(_ sender: Any) {
        self.popupView.isHidden = false
        self.sortByPopUpView.isHidden = true
        self.filterByPopUpView.isHidden = false
        filterByTableView.dataSource = self
        filterByTableView.delegate = self
        filterByTableView.reloadData()
    }
    
    @IBAction func categoryByBtn(_ sender: Any) {
        
        self.popupView.isHidden = true
        self.sortByPopUpView.isHidden = true
        self.filterByPopUpView.isHidden = true
        
        self.showCategoryView()

    }
    
    @IBAction func sortByCloseBtn(_ sender: Any) {
        self.popupView.isHidden = true
        self.sortByPopUpView.isHidden = true
        self.filterByPopUpView.isHidden = true
    }
    
    @IBAction func filterByCloseByBtn(_ sender: Any) {
        self.popupView.isHidden = true
        self.sortByPopUpView.isHidden = true
        self.filterByPopUpView.isHidden = true
    }
    
    @IBAction func applyAction(_ sender: UIButton) {
        print("Filterpage===\(self.selectedsubcategory)---\(self.sortedindex)")

        if self.selectedsubcategory.mainCategoryName != ""  {

            self.applybuttondelegate?.didSelectApplyFilter(category: self.selectedsubcategory, index: self.sortedindex, str: self.nearbyindex)

        } else {
            self.applybuttondelegate?.didSelectApplyFilter(category: nil, index: self.sortedindex , str: self.nearbyindex)
        }
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func currentLocationByBtn(_ sender: Any) {
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined:
                println_debug("notDetermined")
                //mainlocationdelegate?.didSelectLocationByCurrentLocation()
                //nearbylocationdelegate?.didSelectNearByLocation()

                self.dismiss(animated: true, completion: nil)
                
                break
            case .denied :
                println_debug("denied")
                showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                break
            case .restricted :
                println_debug("restricted")
                showAlert(alertTitle: "", alertBody: "Please Enable Location Service".localized, alertImage: #imageLiteral(resourceName: "location"))
                break
            case .authorizedAlways, .authorizedWhenInUse:
            mainlocationdelegate?.didSelectLocationByCurrentLocation()
                nearbylocationdelegate?.didSelectNearByLocation()
            
            if self.findviewStr == "NearByServices" {
            maincurrentlocationdelegate?.didSelectLocationByCurrentLocation()
                
            }
                self.dismiss(animated: true, completion: nil)
                break
            }
        } else {
            alertViewObj.wrapAlert(title:"", body: "Please Enable Location Service".localized, img: #imageLiteral(resourceName: "location"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
        
    }
    
    func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:"OK$ would like to use your Current Location. Please turn on the Location Services for your device.".localized, img:#imageLiteral(resourceName: "map"))
        
        alertViewObj.addAction(title: "Cancel".localized, style: .target , action: {
            
        })
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            }
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    //Current Location
    func didSelectCurrentLocation() {
        updateCurrentLocationToUI()
        
        guard let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude else {
            // throw the error to the user
            return
        }

    }
    
    func updateCurrentLocationToUI() {
        
        self.currentLocationDataTitle.text = userDef.value(forKey: "lastLoginKey") as? String ?? ""

        //        selectedLocationDetail = nil
        //        selectedTownshipDetail = nil
        //        locationNameLbl.text = "Current Location".localized
        //        locationContentLbl.text = "Loading location.....".localized
        //        guard appDelegate.checkNetworkAvail() else {
        //            return
        //        }
        /*
        self.currentLocationDataTitle.text = "Location".localized
        if let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude {
            geoLocManager.getLocationNameByLatLong(lattitude: lat, longitude: long) { (isSuccess, currentAddress) in
                guard isSuccess else {
                    println_debug("something went wrong when get current address from google api")
                    return
                }
                DispatchQueue.main.async {
                    if let curAddress = currentAddress as? String {
                        let addrAddress = curAddress.components(separatedBy: ",")
                        if addrAddress.count > 1 {
                            self.currentLocationDataTitle.text = addrAddress[addrAddress.count - 2] //Rabbit.uni2zg(curAddress)//curAddress
                        }
                    }else {
                        self.currentLocationDataTitle.text = "Loading".localized
                    }
                }
            }
            //
        }*/
    }
    
    
    private func sortviewAction(index: Int) {
        switch index {
        case 0:
            println_debug("default case falling")
//            if let list = dataList as? [NearByServicesNewModel] {
//               // self.delegate?.didSelectSortOption(sortedList: list, selectedSortOption:index)
//            }
            
       /* case 1:
            println_debug("Amount High to Low")
            if let list = dataList as? [NearByServicesModel] {
                dataList = list.sorted(by: {($0.okDollarBalance!) > ($1.okDollarBalance!)})
                //dataList = list.sorted(by: {($0.okDollarBalance.safelyWrappingString() as NSString).floatValue > ($1.okDollarBalance.safelyWrappingString() as NSString).floatValue})
                self.delegate?.didSelectSortOption(sortedList: dataList!, selectedSortOption:index)
            }
            
        case 2:
            println_debug("Amount Low to High")
            if let list = dataList as? [NearByServicesModel] {
                dataList = list.sorted(by: {($0.okDollarBalance!) < ($1.okDollarBalance!)})
                //dataList = list.sorted(by: {($0.okDollarBalance.safelyWrappingString() as NSString).floatValue < ($1.okDollarBalance.safelyWrappingString() as NSString).floatValue})
                self.delegate?.didSelectSortOption(sortedList: dataList!, selectedSortOption:index)
            } */
            
        case 1:
            println_debug("Name A to Z")
            if let list = dataList as? [NearByServicesNewModel] {
                dataList = list.sorted(by: {$0.UserContactData!.BusinessName! < $1.UserContactData!.BusinessName!})
               // dataList = list.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
                //self.delegate?.didSelectSortOption(sortedList: dataList!, selectedSortOption:index)
            }
            
        case 2:
            println_debug("Name Z to A")
            if let list = dataList as? [NearByServicesNewModel] {
                dataList = list.sorted(by: {$1.UserContactData!.BusinessName! < $0.UserContactData!.BusinessName!})
               // dataList = list.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedDescending }

                //self.delegate?.didSelectSortOption(sortedList: dataList!,selectedSortOption: index)
                
            }
            
        default:
            break
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    private func showCategoryView() {
        let storyBoardName = UIStoryboard(name: "Promotion", bundle: nil)
        guard let categoriesView = storyBoardName.instantiateViewController(withIdentifier: "CategoriesListView_ID") as? CategoriesListViewController else { return }
        categoriesView.delegate = self
        categoriesView.modalPresentationStyle = .overCurrentContext
        self.present(categoriesView, animated: true, completion: nil)
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func resignTheFilterView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func cellItem(_ tableView: UITableView, _ indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == sortByTableView {

        let cell = tableView.dequeueReusableCell(withIdentifier: "filterSubCell", for: indexPath) as? filterByNeartableViewSubCell
        let tab = ""
            
            if statusNearBySort == "SortBy" {
                if self.previousSelectedSortOptionnew != nil {
                    
                    if (previousSelectedSortOptionnew == sortList[indexPath.row]) {
                        self.sortedindex = indexPath.row
                        cell?.wrapData(titleStr: "\(tab)\(sortList[indexPath.row])", imageName: "success")
                        
                    } else {
                        
                        cell?.wrapData(titleStr: "\(tab)\(sortList[indexPath.row])", imageName: "in_active")
                    }
                } else {
                    if indexPath.row == 0 {
                        self.sortedindex = 0
                        cell?.wrapData(titleStr: "\(tab)\(sortList[indexPath.row])", imageName: "success")
                    } else {
                        cell?.wrapData(titleStr: "\(tab)\(sortList[indexPath.row])", imageName: "in_active")
                    }
                }
            } else {
                if self.previousSelectedNearbyOptionnew != nil {
                    
                    if (previousSelectedNearbyOptionnew == nearByList[indexPath.row]) {
                        self.nearbyindex = indexPath.row
                        cell?.wrapData(titleStr: "\(tab)\(nearByList[indexPath.row])", imageName: "success")
                        
                    } else {
                        
                        cell?.wrapData(titleStr: "\(tab)\(nearByList[indexPath.row])", imageName: "in_active")
                    }
                } else {
                    if indexPath.row == 0 {
                        self.nearbyindex = 0
                        cell?.wrapData(titleStr: "\(tab)\(nearByList[indexPath.row])", imageName: "success")
                    } else {
                        cell?.wrapData(titleStr: "\(tab)\(nearByList[indexPath.row])", imageName: "in_active")
                    }
                }
            }
            
        return cell!
            
        } else {
            
            if self.findviewStr == "NearByServices" {
                
                if indexPath.row == 0 {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "filterSubCell", for: indexPath) as? filterByNeartableViewSubCell
                    var imageName = "in_active"
                    if let _ = previousSelectedFilterData as? NearByServicesModel {
                        
                    } else {
                        imageName =  "success"
                    }
                    
                    cell?.wrapData(titleStr: "Default".localized, imageName: imageName)
                    return cell!
                    
                } else {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "filterSubCell", for: indexPath) as? filterByNeartableViewSubCell
                    var imageName = ""
                    var name = ""
                    if let filterList = filterShowList![indexPath.row - 1] as? NearByServicesModel {
                        let promotionData = filterList //as! PromotionsModel
                        let tab = ""
                        if((promotionData.businessName?.count)! > 0) {
                            cell?.cellSubTitle.text = "\(tab)\((promotionData.businessName ?? ""))"
                            name = "\(tab)\((promotionData.businessName ?? ""))"
                            if let prevFilterData = previousSelectedFilterData as? NearByServicesModel, let cellId = prevFilterData.cellId, cellId == promotionData.cellId {
                                imageName =  "success"
                            }else {
                                imageName =  "in_active"
                            }
                        } else {
                            cell?.cellSubTitle.text = "\(tab)\((promotionData.firstName ?? ""))"
                            name = "\(tab)\((promotionData.firstName ?? ""))"
                            if let prevFilterData = previousSelectedFilterData as? NearByServicesModel, let cellId = prevFilterData.cellId, cellId == promotionData.cellId {
                                imageName =  "success"
                            }else {
                                imageName =  "in_active"
                            }
                        }
                    }
                    cell?.wrapData(titleStr: name, imageName: imageName)
                    return cell!
                }
            } else if self.findviewStr == "Promotions" {
                
                if indexPath.row == 0 {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "filterSubCell", for: indexPath) as? filterByNeartableViewSubCell
                    var imageName = "in_active"
                    if let _ = previousSelectedFilterData as? PromotionsModel {
                        
                    } else {
                        imageName =  "success"
                    }
                    
                    cell?.wrapData(titleStr: "Default".localized, imageName: imageName)
                    return cell!
                    
                } else {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "filterSubCell", for: indexPath) as? filterByNeartableViewSubCell
                    var imageName = ""
                    var name = ""
                    if let filterList = filterShowList![indexPath.row - 1] as? PromotionsModel {
                        let promotionData = filterList //as! PromotionsModel
                        let tab = ""
                        if((promotionData.shop_Name?.count)! > 0) {
                            cell?.cellSubTitle.text = "\(tab)\((promotionData.shop_Name ?? ""))"
                            name = "\(tab)\((promotionData.shop_Name ?? ""))"
                            if let prevFilterData = previousSelectedFilterData as? PromotionsModel, let cellId = prevFilterData.shop_Id, cellId == promotionData.shop_Id {
                                imageName =  "success"
                            }else {
                                imageName =  "in_active"
                            }
                        } else {
                            cell?.cellSubTitle.text = "\(tab)\((promotionData.shop_AgentName ?? ""))"
                            name = "\(tab)\((promotionData.shop_AgentName ?? ""))"
                            if let prevFilterData = previousSelectedFilterData as? PromotionsModel, let cellId = prevFilterData.shop_AgentId, cellId == promotionData.shop_AgentId {
                                imageName =  "success"
                            }else {
                                imageName =  "in_active"
                            }
                        }
                    }
                    cell?.wrapData(titleStr: name, imageName: imageName)
                    return cell!
                }
                
            } else {
            
            
                if indexPath.row == 0 {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "filterSubCell", for: indexPath) as? filterByNeartableViewSubCell
                    var imageName = "in_active"
                    if let _ = previousSelectedFilterData as? PTMapNearbyMerchant {
                        
                    } else {
                        imageName =  "success"
                    }
                    
                    cell?.wrapData(titleStr: "Default".localized, imageName: imageName)
                    return cell!
                    
                } else {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "filterSubCell", for: indexPath) as? filterByNeartableViewSubCell
                    var imageName = ""
                    var name = ""
                    if let filterList = filterShowList![indexPath.row - 1] as? PTMapNearbyMerchant {
                        let promotionData = filterList //as! PromotionsModel
                        let tab = ""
                        if((promotionData.agentName?.count)! > 0) {
                            cell?.cellSubTitle.text = "\(tab)\((promotionData.agentName ?? ""))"
                            name = "\(tab)\((promotionData.agentName ?? ""))"
                            if let prevFilterData = previousSelectedFilterData as? PTMapNearbyMerchant, let cellId = prevFilterData.agentName, cellId == promotionData.agentName {
                                imageName =  "success"
                            }else {
                                imageName =  "in_active"
                            }
                        } else {
                            cell?.cellSubTitle.text = "\(tab)\((promotionData.businessCategory?.burmeseName ?? ""))"
                            name = "\(tab)\((promotionData.businessCategory?.burmeseName ?? ""))"
                            if let prevFilterData = previousSelectedFilterData as? PTMapNearbyMerchant, let cellId = prevFilterData.businessCategory?.burmeseName, cellId == promotionData.businessCategory?.burmeseName {
                                imageName =  "success"
                            }else {
                                imageName =  "in_active"
                            }
                        }
                    }
                    cell?.wrapData(titleStr: name, imageName: imageName)
                    return cell!
                }
        }
        
       
    }
}
    
    private func cellSelect(_ tableView: UITableView, _ indexPath: IndexPath) {
       
        self.popupView.isHidden = true
        self.sortByPopUpView.isHidden = true
        self.filterByPopUpView.isHidden = true
        
        if tableView == sortByTableView {
        
            if statusNearBySort == "SortBy" {
            self.sortByDataTitle.text = sortList[indexPath.row]
                self.sortedindex = indexPath.row
                defaults.set(sortList[indexPath.row], forKey: "PTMsortBy")
            } else {
                self.nearByDataTitle.text = nearByList[indexPath.row]
                self.nearbyindex = indexPath.row
                defaults.set(nearByList[indexPath.row], forKey: "PTMNearBy")
            }
            
            if findviewStr == "NearByServices" {
                
                defaults.set(sortList[indexPath.row], forKey: "NBSsortBy")
                self.sortedindex = indexPath.row
                previousSelectedSortOptionnew = sortList[indexPath.row]
                //self.sortviewAction(index: indexPath.row)

            }
            

        } else {
            
            if self.findviewStr == "NearByServices" {

                if indexPath.row == 0 {
                    self.delegateFilter?.didSelectFilterOption(filteredList: filterDataList!,selectedFilter: nil)
                    self.filterByDataTitle.text = "Default".localized
                    defaults.set("Default", forKey: "NBSfilterBy")
                    resignTheFilterView()
                } else {
                    if let filtershowlistData = filterShowList![indexPath.row - 1] as? NearByServicesModel {
                        var newFilteredData: [NearByServicesModel] = []
                        let selectedPromotion = filtershowlistData //as! PromotionsModel
                        if let filterlist = filterDataList as? [NearByServicesModel] {
                            for promotion in filterlist {
                                if let count = promotion.businessName?.count, count > 0 {
                                    if promotion.businessName == selectedPromotion.businessName {
                                       
                                        defaults.set(selectedPromotion.businessName!, forKey: "NBSfilterBy")
                                        self.filterByDataTitle.text = selectedPromotion.businessName!
                                        newFilteredData.append(promotion)
                                    }
                                } else {
                                    if promotion.firstName == selectedPromotion.firstName {
                                       
                                         defaults.set(selectedPromotion.firstName!, forKey: "NBSfilterBy")
                                        self.filterByDataTitle.text = selectedPromotion.firstName!
                                        newFilteredData.append(promotion)
                                    }
                                }
                            }
                        }
                        self.delegateFilter?.didSelectFilterOption(filteredList: newFilteredData ,selectedFilter: selectedPromotion)
                    }
                    resignTheFilterView()
                }
            
            } else if self.findviewStr == "Promotions" {
            
                if indexPath.row == 0 {
                    self.delegateFilter?.didSelectFilterOption(filteredList: filterDataList!,selectedFilter: nil)
                    self.filterByDataTitle.text = "Default".localized
                    defaults.set("Default", forKey: "NBSfilterBy")
                    resignTheFilterView()
                } else {
                    if let filtershowlistData = filterShowList![indexPath.row - 1] as? PromotionsModel {
                        var newFilteredData: [PromotionsModel] = []
                        let selectedPromotion = filtershowlistData //as! PromotionsModel
                        if let filterlist = filterDataList as? [PromotionsModel] {
                            for promotion in filterlist {
                                if let count = promotion.shop_Name?.count, count > 0 {
                                    if promotion.shop_Name == selectedPromotion.shop_Name {
                                        
                                        defaults.set(selectedPromotion.shop_Name!, forKey: "NBSfilterBy")
                                        self.filterByDataTitle.text = selectedPromotion.shop_Name!
                                        newFilteredData.append(promotion)
                                    }
                                } else {
                                    if promotion.shop_AgentName == selectedPromotion.shop_AgentName {
                                        
                                        defaults.set(selectedPromotion.shop_AgentName!, forKey: "NBSfilterBy")
                                        self.filterByDataTitle.text = selectedPromotion.shop_AgentName!
                                        newFilteredData.append(promotion)
                                    }
                                }
                            }
                        }
                        self.delegateFilter?.didSelectFilterOption(filteredList: newFilteredData ,selectedFilter: selectedPromotion)
                    }
                    resignTheFilterView()
                }
            
            } else {
                
                if indexPath.row == 0 {
                    self.filterByDataTitle.text = "Default".localized
                    defaults.set("Default", forKey: "PTMfilterBy")
                    resignTheFilterView()
                    
                } else {
                    if let filtershowlistData = filterShowList![indexPath.row - 1] as? PTMapNearbyMerchant {
                        var newFilteredData: [PTMapNearbyMerchant] = []
                        let selectedPromotion = filtershowlistData //as! PromotionsModel
                        if let filterlist = filterDataList as? [PTMapNearbyMerchant] {
                            for promotion in filterlist {
                                if let count = promotion.agentName?.count, count > 0 {
                                    if promotion.agentName == selectedPromotion.agentName {
                                        
                                            defaults.set(selectedPromotion.agentName!, forKey: "PTMfilterBy")
                                            self.filterByDataTitle.text = selectedPromotion.agentName!
                                            newFilteredData.append(promotion)
                                    }
                                } else {
                                    if promotion.businessCategory?.burmeseName == selectedPromotion.businessCategory?.burmeseName {
                                            defaults.set(selectedPromotion.businessCategory?.burmeseName!, forKey: "PTMfilterBy")
                                            self.filterByDataTitle.text = selectedPromotion.businessCategory?.burmeseName!
                                            newFilteredData.append(promotion)
                                    }
                                }
                            }
                        }
                        
                    }
                    resignTheFilterView()
                }
            }
        }
    }
}

extension FilterNearByServiceViewController: CategoriesSelectionDelegate {
    func didSelectLocationByCategory(category: SubCategoryDetail?) {
        
        if category != nil {
            
            self.categoryDataTitle.text = "\(category!.mainCategoryName) , \(category!.subCategoryName)"
            self.selectedsubcategory = category!
        }

//        self.categoryDelegate?.didSelectLocationByCategory(category: category)
//        self.dismiss(animated: false, completion: nil)
    }
}




extension FilterNearByServiceViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == sortByTableView {
            if statusNearBySort == "SortBy" {
            return sortList.count
            } else {
                return nearByList.count
            }
        } else {
            return filterShowList!.count+1
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.cellItem(tableView, indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       self.cellSelect(tableView,indexPath)
    }
    
}


class filterByNeartableViewCell: UITableViewCell {
    @IBOutlet weak var dropDownImageView: UIImageView!
    @IBOutlet weak var cellTitle: UILabel!{
        didSet{
            cellTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var cellImageView: UIImageView!
    
    func wrapData(titleStr: String, imageName: String, dropDownImage: String) {
        self.cellTitle.text = titleStr
       // let image = UIImage(named: dropDownImage)?.withRenderingMode(.alwaysTemplate)
       // self.dropDownImageView.image = image
       // self.dropDownImageView.tintColor = .white
       // let dropImage = UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate)
       // self.cellImageView.image = dropImage
       // self.cellImageView.tintColor = .white
    }
}

fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}

class filterByNeartableViewSubCell: UITableViewCell {
    @IBOutlet weak var cellSubTitle: UILabel!{
        didSet{
            cellSubTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var cellSubImageView: UIImageView!
    func wrapData(titleStr: String, imageName: String) {
        self.cellSubTitle.text = titleStr
        self.cellSubImageView.image = UIImage(named: imageName)
    }
}
