//
//  NBSMapVC.swift
//  OK
//
//  Created by SHUBH on 6/26/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import Rabbit_Swift
import IQKeyboardManagerSwift

protocol MaincurrentLocationViewDelegate : class {
    func didSelectLocationByCurrentLocation()
    
}

class NBSMapVC: MapBaseViewController, MainLocationViewDelegateRegistration,MainNearByLocationViewDelegate,MainwheretoLocationViewDelegate,MaincurrentLocationViewDelegate {

    let appDel = UIApplication.shared.delegate as! AppDelegate
    var checkurl:String = ""

    @IBOutlet weak var mapView: UIView! {
        didSet {
            self.mapView.clipsToBounds = true
            self.mapView.layer.borderWidth = 0.8
            self.mapView.layer.borderColor = UIColor.lightGray.cgColor
            self.mapView.layer.cornerRadius = 20.0
            
            self.mapView.layer.shadowColor = UIColor.lightGray.cgColor
            self.mapView.layer.shadowOpacity = 0.8
            self.mapView.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
            self.mapView.layer.shadowRadius = 5.0
        }
    }
    
    @IBOutlet weak var TopLocationBtn: UIButton!
        {
        didSet
        {
            self.TopLocationBtn.layer.masksToBounds = true
            self.TopLocationBtn.layer.borderWidth = 1.0
            self.TopLocationBtn.layer.borderColor = UIColor.white.cgColor
            self.TopLocationBtn.layer.cornerRadius = 20.0
            
        }
    }
    
    @IBOutlet weak var filterLabel: UILabel! {
        didSet {
            self.filterLabel.text = self.filterLabel.text?.localized
            self.filterLabel.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    @IBOutlet weak var listLabel: UILabel! {
        didSet {
            self.listLabel.text = self.listLabel.text?.localized
            self.listLabel.font = UIFont(name: appFont, size: appTitleButton)
        }
    }

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLabel: UILabel!{
        didSet{
            self.headerLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    //prabu
    let defaults = UserDefaults.standard

    var currentNearbyView: NearByView = .merchantView

    //Record Manage
    var cashInListBackUPByCurLoc        : [NearByServicesNewModel]?
    var cashInListRecentFromServer      : [NearByServicesNewModel]?
    
    var arrMainList                 = [NearByServicesNewModel]()
    var arraySortList = [NearByServicesNewModel]()

    //Collection cell
    @IBOutlet weak var collectionView           : UICollectionView!
    
    private var collectItems     = [String]()
    private var collectImages    = [String]()
    private var highLightImages  = [Bool]()
    
    let margin: CGFloat = 2
    var cellsPerRow: Int = 0
    
    var selectedSortItem        : Int = 0
    var selectedFilterData      : NearByServicesNewModel?
    
    var baseViewHeight: CGFloat {
        return screenArea.size.height - 190
    }
    
    var promotionsListBackUPByCurLoc   : [NearByServicesNewModel]?
    var promotionsListRecentFromServer : [NearByServicesNewModel]?
    var selectedpromotionData  : NearByServicesNewModel?

    
    var selectedLocationType: Location?
    let nearbydistance = kNearByDistance
    
    var selectedLocationDetail          : LocationDetail?
    var selectedTownshipDetail          : TownShipDetail?
    
    var selectedLocationDetailStr         : String?
    var selectedTownshipDetailStr         : String?
    
    let filterView      = NearByFilterViewController()
    
    //Manage TYpe
    var intType : Int = 0
    var screenType : String = "NearBy"
    
    //prabu search bar
    @IBOutlet weak var topSearchView: UIView!
    
    @IBOutlet weak var topSearchBar: UISearchBar!
    
    @IBOutlet weak var searchViewBackBtn: UIButton!
    
    var isHeaderSearchOpened: Bool = false
    
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var headerSearchHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var headerSearchBtn: UIButton!


    //Manage Map
    @IBOutlet weak var clusterMapView: MKMapView!
    @IBOutlet weak var promotionInfoView: UIView!
    @IBOutlet weak var promotionImgView: UIImageView!
    @IBOutlet weak var promotionNameLbl: UILabel!
    @IBOutlet weak var contactNoLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var remarksLbl: MarqueeLabel!
    let clusteringManager = FBClusteringManager()
    
    var locationManager: CLLocationManager!
    
    let regionRadius: CLLocationDistance = 39000
    var listData: [Any]?
    var infoViewTimer: Timer!
    
    //MARK: View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadUI()
        geoLocManager.startUpdateLocation()
        
        //Search Setup
        searchBarSettings()
        hideSearchWithoutAnimation()
        
        //Manage Keyboard
        IQKeyboardManager.sharedManager().enable            = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
    
        //Location Manage
        //didSelectLocationByCurrentLocation()
        println_debug("mapclusterviewcontroller viewdidload called")
        clusterMapView.delegate = self
        promotionInfoView.isHidden = true
        //Show Header title
        headerLabel.text = screenType
        headerLabel.font = UIFont.init(name: appFont, size: 17)
        headerView.backgroundColor = kYellowColor

        print("map count \(self.arrMainList.count)")
        print("map Data \(self.arrMainList)")

        if self.arrMainList.count > 0 {
            self.addAnnotationsInMap(list: self.arrMainList)
        } else {
            //updateLocalizations()
            let initialLocation = CLLocation(latitude: Double(geoLocManager.currentLatitude)!, longitude: Double(geoLocManager.currentLongitude)!)
            centerMapOnLocation(location: initialLocation)
        }
    }
    
    func loadUI() {
        self.promotionNameLbl.font = UIFont.init(name: appFont, size: 14)
        self.promotionNameLbl.textColor = UIColor.black
        self.contactNoLbl.font = UIFont.init(name: appFont, size: 13)
        self.contactNoLbl.textColor = UIColor.darkGray
        self.distanceLbl.font = UIFont.init(name: appFont, size: 13)
        self.distanceLbl.textColor = UIColor.darkGray
        self.remarksLbl.font = UIFont.init(name: appFont, size: 13)
        self.remarksLbl.textColor = kBlueColor
        self.setUpMarqueeLabel(label: self.remarksLbl)
        promotionImgView.layer.shadowColor = kBackGroundGreyColor.cgColor
        promotionImgView.layer.shadowOpacity = 1
        promotionImgView.layer.shadowOffset = CGSize.zero
        promotionImgView.layer.shadowRadius = 2
    }

    func updateLocalizations() {
        headerLabel.font = UIFont.init(name: appFont, size: 17)
        if(screenType == "NearBy")
        {
             headerLabel.text = "Nearby OK$ Services".localized
        }
        else
        {
             headerLabel.text = "OK$ Services".localized
        }

        headerView.backgroundColor = kYellowColor
    }
    
    func setUpMarqueeLabel(label: MarqueeLabel) {
        label.tag = 501
        label.type = .continuous
        label.speed = .duration(20)
        label.fadeLength = 10.0
        label.trailingBuffer = 30.0
        label.isUserInteractionEnabled = true // Don't forget this, otherwise the gesture recognizer will fail (UILabel has this as NO by default)
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(pauseTap))
        tapRecognizer.numberOfTapsRequired = 1
        tapRecognizer.numberOfTouchesRequired = 1
        label.addGestureRecognizer(tapRecognizer)
    }
    
    @objc func pauseTap(_ recognizer: UIGestureRecognizer) {
        let continuousLabel2 = recognizer.view as! MarqueeLabel
        if recognizer.state == .ended {
            continuousLabel2.isPaused ? continuousLabel2.unpauseLabel() : continuousLabel2.pauseLabel()
        }
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion.init(center: location.coordinate,
                                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        clusterMapView.setRegion(coordinateRegion, animated: true)
    }
    
    func removeAllAnnotationsInMap() {
        clusteringManager.removeAll()
        reloadMap()
    }
    
  
    
    func addAnnotationsInMap(list: Array<Any>) {
        removeAllAnnotationsInMap()
        listData = list
        var clusterArray: [FBAnnotation] = []
        var value = 0
        while value < list.count {
            if let model = list[value] as? NearByServicesNewModel {
                
                if let lat = model.UserContactData?.LocationNewData?.Latitude , let long = model.UserContactData?.LocationNewData?.Longitude {
                    print("AnotationLatitudeandlogtitude-----")

                    let annotation = FBAnnotation()
                    annotation.coordinate = CLLocationCoordinate2D(latitude:  lat, longitude: long)
                    
                    if model.UserContactData?.BusinessName == "" {
                        
                        if model.UserContactData?.FirstName != "" {
                        annotation.title = model.UserContactData?.FirstName
                        }

                    } else {
                        annotation.title = model.UserContactData?.BusinessName

                    }
                    let pinImage = UIImage(named: "ok_logo.png")
                    annotation.image = pinImage
                    annotation.userData = model
                    clusterArray.append(annotation)
                    
                    if value == list.count-2 {
                        let initialLocation = CLLocation(latitude: Double(lat), longitude: Double(long))
                        centerMapOnLocation(location: initialLocation)
                    }
                }
            }
            value += 1
        }
        clusteringManager.add(annotations: clusterArray)
        reloadMap()
    }
    
    func showPromotionInfoView(promotion: NearByServicesNewModel) {
        self.promotionInfoView.isHidden = false
        self.promotionInfoView.center.x -= self.view.bounds.width
        if let timer = infoViewTimer {
            timer.invalidate()
        }
        loadInfoViewData(info: promotion)
        UIView.animate(withDuration: 1.5, delay: 0.5,
                       usingSpringWithDamping: 0.3,
                       initialSpringVelocity: 0.5,
                       options: [], animations: {
                        self.promotionInfoView.center.x += self.view.bounds.width
                        
        }, completion:{ _ in
            self.infoViewTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.update), userInfo: nil, repeats: false)
        })
    }
    
    @objc func update() {
        infoViewTimer.invalidate()
        removeTheInfoView()
    }
    
    func removeTheInfoView() {
        UIView.animate(withDuration: 0.1,
                       delay: 0.1,
                       options: UIView.AnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.promotionInfoView.center.x -= self.view.bounds.width
        }, completion: { (finished) -> Void in
            self.promotionInfoView.isHidden = true
            self.promotionInfoView.center.x += self.view.bounds.width
        })
    }
    
    func loadInfoViewData(info: NearByServicesNewModel) {
        self.selectedpromotionData = nil
        self.selectedpromotionData = info

        promotionImgView.image = UIImage.init()
        
        if info.UserContactData?.BusinessName == "" {
            promotionNameLbl.text = info.UserContactData?.FirstName
        } else {
        promotionNameLbl.text = info.UserContactData?.BusinessName
        }
        
        let mobileNumber  = getPlusWithDestinationNum((info.UserContactData!.PhoneNumber)!, withCountryCode: "+95")
        
        //var mobileNumber = "NA"
//        if info.phoneNumber!.count > 0 {
//            mobileNumber = getPhoneNumberByFormat(phoneNumber: info.phoneNumber!)
//        }
        
        contactNoLbl.text = "Contact: \(mobileNumber)"
        
        if let shopDistance = info.distanceInKm {
            distanceLbl.text = String(format: "(%.2f Km)", shopDistance).replacingOccurrences(of: ".00", with: "")
        }
        
        remarksLbl.text = ""
        
        promotionImgView.image = UIImage.init(named: "btc")
    }
    
    func reloadMap() {
        DispatchQueue.main.async {
            let mapBoundsWidth = Double(self.clusterMapView.bounds.size.width)
            let mapRectWidth = self.clusterMapView.visibleMapRect.size.width
            let scale = mapBoundsWidth / mapRectWidth
            let annotationArray = self.clusteringManager.clusteredAnnotations(withinMapRect: self.clusterMapView.visibleMapRect, zoomScale:scale)
            self.clusteringManager.display(annotations: annotationArray, onMapView:self.clusterMapView)
        }
    }
    
    //MARK: Search Manage Delegate
    func searchBarSettings() {
        topSearchBar.tintColor = UIColor.darkGray
        topSearchBar.barTintColor = UIColor.white
        topSearchBar.searchBarStyle = UISearchBar.Style.minimal
        
        let searchTextField: UITextField? = topSearchBar.value(forKey: "searchField") as? UITextField
        if searchTextField!.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
            let attributeDict = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
            searchTextField!.attributedPlaceholder = NSAttributedString(string: "Search".localized, attributes: attributeDict)
            searchTextField!.backgroundColor = UIColor.white
            searchTextField!.borderStyle = UITextField.BorderStyle.none
            searchTextField!.layer.borderColor = UIColor.clear.cgColor
            searchTextField!.layer.borderWidth = 0.0
            searchTextField!.textColor = UIColor.black
            searchTextField!.autocorrectionType = UITextAutocorrectionType.no
            let myFont = UIFont.init(name: appFont, size: 15)
            if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                searchTextField!.font = myFont
            }else{
                //
            }
            searchTextField!.leftViewMode = UITextField.ViewMode.never
            searchTextField!.placeholder = "Search".localized
        }
        
    }
    
    func resignSearchBarKeypad() {
        topSearchBar.resignFirstResponder()
    }
    
    func hideSearchWithoutAnimation() {
        isHeaderSearchOpened = false
        topSearchBar.text = ""
        topSearchBar.resignFirstResponder()
        topSearchView.isHidden = true
    }
    
    @IBAction func searchBtnAction(_ sender: Any) {
        switch currentNearbyView {
        case .okAllView:
            showCashSearch()
        case .merchantView:
            //self.topSearchView.frame = CGRect(x: screenArea.size.width - 30, y: 0, width: 0, height: kHeaderHeight)
            showCashSearch()
        case .okAgentsView:
            //self.topSearchView.frame = CGRect(x: screenArea.size.width - 30, y: 0, width: 0, height: kHeaderHeight)
            showCashSearch()
        case .okOfficesView:
            //self.topSearchView.frame = CGRect(x: screenArea.size.width - 30, y: 0, width: 0, height: kHeaderHeight)
            showCashSearch()
        }
    }
    
    func showCashSearch() {
        isHeaderSearchOpened = true
        self.mapView.isHidden = true
        topSearchBar.text = ""
        topSearchView.layer.masksToBounds = true
        topSearchView.layer.cornerRadius = 20
        topSearchView.alpha = 0.0
        UIView.animate(withDuration: 0.3, animations: {
            var viewFrame: CGRect = self.topSearchView.frame
            viewFrame.origin.x = 0
            viewFrame.size.width = screenArea.size.width
            self.topSearchView.frame = viewFrame
            self.topSearchView.alpha = 1.0
            self.topSearchView.isHidden = false
            self.topSearchBar.becomeFirstResponder()
            
        }) { _ in
            self.topSearchView.layer.cornerRadius = 0
        }
    }
    
    func hideCashSearch() {
        self.mapView.isHidden = false
        isHeaderSearchOpened = false
        topSearchBar.text = ""
        topSearchBar.resignFirstResponder()
        topSearchView.layer.cornerRadius = 20
        topSearchView.alpha = 1.0
        UIView.animate(withDuration: 0.3, animations: {
            var viewFrame: CGRect = self.topSearchView.frame
            viewFrame.origin.x = screenArea.size.width - 30
            viewFrame.origin.y = self.topSearchView.frame.origin.y
            viewFrame.size.width = 0
            viewFrame.size.height = self.topSearchView.frame.size.height
            self.topSearchView.frame = viewFrame
            
        }) { _ in
            self.topSearchView.alpha = 0.0
        }
    }
    
    @IBAction func searchViewBackAction(_ sender: Any) {
        //Hide search view
        hideCashSearch()
        reloadListViewAfterSwitch()
    }
    
    //MARK: - Location Manager
    
    func didSelectLocationByCurrentLocation() {
        
        print("Current Location called----")

        self.checkurl = "CurrentLocation"

        updateCurrentLocationToUI()
        
        guard let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude else {
            // throw the error to the user
            return
        }
        
        self.getOfficeAPI(lat: lat , long: long )
        
    }
    
    func didSelectNearByLocation() {
        
        self.checkurl = "NearBy"

        
        //updateCurrentLocationToUI()
        
        guard let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude else {
            // throw the error to the user
            return
        }
        
        headerLabel.text = "Near By".localized
        
        self.getOfficeAPI(lat: lat , long: long )
    }
    
    func didSelectwheretoLocation(streetName:String)
    {
        
        headerLabel.text = "     " + streetName + "     "
        
        var arrTempList = [NearByServicesNewModel]()
        
        arrTempList = arrMainList
        
        if arrTempList.count == 0  {
            self.showToast(message: "No Results Found".localized, align: .top)
            return
        }
        
        var newFilteredData: [NearByServicesNewModel] = []
        
        for promotion in arrTempList {
            if let count = promotion.UserContactData?.BusinessName?.count, count > 0 {
               // if promotion.UserContactData?.Address == streetName ||  promotion.addressLine2 == streetName{
                
                if promotion.UserContactData?.StreetName == streetName {
                    
                  //  defaults.set(promotion.addressLine1, forKey: "NBSRecentLocationAddress1")
                  //  defaults.set(promotion.addressLine2, forKey: "NBSRecentLocationAddress2")
                    
                    newFilteredData.append(promotion)
                }
            }
        }
        
        self.addAnnotationsInMap(list: newFilteredData)

    }
    
    func didSelectLocationByTownshipRegistration(location: LocationDetail, township: TownShipDetailForAddress) {
       // updateOtherLocationToUI(location: location, township: township)
        self.checkurl = "Township"

        println_debug("currentlocation ::: cashin :: all list called")

        if self.appDel.currentLanguage == "my" {
            headerLabel.text = location.stateOrDivitionNameMy + " , " + township.cityNameMY + "    "
            updateOtherLocationToUINew(location: location.stateOrDivitionNameMy, township: township.townShipNameMY)
        }
        else
        {
            headerLabel.text = location.stateOrDivitionNameEn + " , " + township.cityNameEN + "    "
            updateOtherLocationToUINew(location: location.stateOrDivitionNameEn, township: township.townShipNameEN)
        }
        
        geoLocManager.getLatLongByName(cityname: township.townShipNameEN) { (isSuccess, lat, long) in
            guard isSuccess, let latti = lat, let longi = long  else {
                self.showErrorAlert(errMessage: "Try Again".localized)
                return
            }
            
            DispatchQueue.main.async {
                
                let locationLat  = CLLocationDegrees.init(Double(latti) ?? 0.0)
                let locationLong = CLLocationDegrees.init(Double(longi) ?? 0.0)
                
                let locationData = CLLocationCoordinate2D.init(latitude: locationLat , longitude: locationLong)
                let distance = 100
                let coordinateRegion = MKCoordinateRegion.init(center: locationData, latitudinalMeters: CLLocationDistance(distance), longitudinalMeters: CLLocationDistance(distance))
                
                self.clusterMapView.setRegion(coordinateRegion, animated: true)
               
                self.getOfficeAPI(lat: latti , long: longi)
                
            }
        }
    }
    
    func updateOtherLocationToUI(location: LocationDetail, township: TownShipDetail) {
        selectedLocationDetail = location
        selectedTownshipDetail = township
    }
    
    func updateOtherLocationToUINew(location: String, township: String) {
        
        selectedLocationDetailStr = location
        selectedTownshipDetailStr = township
    
    }
    
    //MARK: - Initial Setup
    func updateCurrentLocationToUI() {
        headerLabel.text = "Location".localized
        
        //headerLabel.text =  "          \(userDef.value(forKey: "lastLoginKey") as! String)          "
        headerLabel.text = "\(userDef.value(forKey: "lastLoginKey") as? String ?? "")     "

        defaults.set(userDef.value(forKey: "lastLoginKey") as? String ?? nil, forKey: "NBSCurrentLocation")
        
         //defaults.set("Location".localized, forKey: "NBSCurrentLocation")
        /*
        if let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude {
            geoLocManager.getLocationNameByLatLong(lattitude: lat, longitude: long) { (isSuccess, currentAddress) in
                guard isSuccess else {
                    println_debug("something went wrong when get current address from google api")
                    return
                }
                DispatchQueue.main.async {
                    if let curAddress = currentAddress as? String {
                        let addrAddress = curAddress.components(separatedBy: ",")
                        if addrAddress.count > 1 {
                            self.headerLabel.text = "          \(addrAddress[addrAddress.count - 2])          " //Rabbit.uni2zg(curAddress)//curAddress
                        }
                    }else {
                        
//                        self.headerLabel.text = "Loading".localized
//                        self.defaults.set("Loading".localized, forKey: "NBSCurrentLocation")
                        
                        self.headerLabel.text =  "          \(userDef.value(forKey: "lastLoginKey") as! String)          " ?? ""
                        
                        self.defaults.set(userDef.value(forKey: "lastLoginKey") as? String ?? nil, forKey: "NBSCurrentLocation")
                        

                    }
                }
            }
            //
        }
         */
    }
    
    
    func checkAndLoadCollectionView() {
        
        collectItems     = mapViewDetails.collectionItems
        collectImages    = mapViewDetails.collectionImages
        highLightImages  = mapViewDetails.highLightImages
        
        //self.refreshSortFilter()
        cellsPerRow = collectItems.count
        collectionView.reloadData()
    }
    
    
    @IBAction func tapOnCurrentLocationView(_ sender: UIButton) {
        println_debug("tap call on current location view")
        let storyBoardName = UIStoryboard(name: "Promotion", bundle: nil)
        guard let locationSelectionView = storyBoardName.instantiateViewController(withIdentifier: "SelecteMainLocationView_ID") as? SelectMainLocationViewController else { return }
        locationSelectionView.delegateRegistration = self as MainLocationViewDelegateRegistration
        locationSelectionView.delegatenearby = self
        locationSelectionView.delegatewhereto = self
        self.navigationController?.pushViewController(locationSelectionView, animated: true)
    }
    
    @IBAction func nearByBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Get API Data
    
    func getOfficeAPI(lat: String, long: String)
    {
        println_debug("call ok offices api ")
        
        let url = getUrl(urlStr: "", serverType: .nearByServicesUrl)
        
       // var latitudenew = "16.81668401110357"
       // var lagtitudenew = "96.13187085779862"
        
        let jsonDic:[String : Any]

        if checkurl ==  "NearBy" {
            
            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 300, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":intType, "phoneNumberNotToConsider":""] as [String : Any]
            
        } else if checkurl ==  "Township"{
            
            if intType == 3 {
                
                jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1000, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":selectedTownshipDetailStr,"cityName":"","businessName":"","country":"","divisionName":selectedLocationDetailStr,"line1":""],"paginationParams": ["offset":0,"limit":0],"types":[], "phoneNumberNotToConsider":""] as [String : Any]
                
            } else {
                
                jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1000, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":selectedTownshipDetailStr,"cityName":"","businessName":"","country":"","divisionName":selectedLocationDetailStr,"line1":""],"paginationParams": ["offset":0,"limit":0],"types":intType, "phoneNumberNotToConsider":""] as [String : Any]
                
            }
            
        } else {
            
            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1000, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":intType, "phoneNumberNotToConsider":""] as [String : Any]
        }
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url,methodType: kMethod_Post, contentType: kContentType_Json, inputVal: jsonDic, authStr: nil)
        
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    //println_debug("remove progress view called inside cashin offices api main call with error")
                }
                //self.showErrorAlert(errMessage: "Try Again".localized)
                return
            }
            println_debug("response dict for get all offices list :: \(response ?? "")")
            progressViewObj.removeProgressView()
            
            var cashArray = [NearByServicesNewModel]()
            DispatchQueue.main.async {
                cashArray = ParserAttributeClass.parseNearbyServiceJSONNew(anyValue: response as AnyObject) as [NearByServicesNewModel]
                //println_debug("response count for ok offices :::: \(cashArray.count)")
                progressViewObj.removeProgressView()
                self.cashInListRecentFromServer = cashArray

                if cashArray.count > 0 {
                    
                    self.mapView.isHidden = false
                        self.cashInListBackUPByCurLoc = cashArray  // this is too just save the list in backup
                    self.arrMainList.removeAll()
            
                        //Seperate mechant =2,agent=1,office=7
                        for indexVal in 0..<cashArray.count {
                            let currentModel = cashArray[indexVal]
                            
                            if self.intType == currentModel.UserContactData?.type {//Based on tabs map data
                                self.arrMainList.append(currentModel)
                                
                            } else {
                                
                                if self.intType == 3 {//All list
                                self.arrMainList.append(currentModel)
                                }
                            }
                        }
                    print("map count \(self.arrMainList.count)")
                    self.addAnnotationsInMap(list: self.arrMainList)
                    }
                else
                {
                    self.mapView.isHidden = true
                    self.showErrorAlert(errMessage: "No Record Found".localized)
                }
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func PopupAction(_ sender: Any) {
        
        print("Nearby map action called")
        let promotion = selectedpromotionData
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
        let protionDetailsView = storyboard.instantiateViewController(withIdentifier: "PromotionDetailView_ID") as! PromotionDetailViewController
        protionDetailsView.selectedNearByServicesNew = promotion
        protionDetailsView.strNearByPromotion = "NearBy"
        self.navigationController?.pushViewController(protionDetailsView, animated: true)

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension NBSMapVC: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        reloadMap()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var reuseId = ""
        if annotation is FBAnnotationCluster {
            
           // print("FBAnnotationCluster called----\(annotation)")
          
            reuseId = "Cluster"
            var clusterView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
            if clusterView == nil {
                clusterView = FBAnnotationClusterView(annotation: annotation, reuseIdentifier: reuseId, configuration: FBAnnotationClusterViewConfiguration.default())
            
            } else {
                clusterView?.annotation = annotation
            }
            return clusterView
            
        } else {
           
               // print("NBSCustomAnnotation called----\(annotation)")

            var annotationView = MKMarkerAnnotationView()
            let annotation = annotation as? NBSCustomAnnotation
            let identifier = "NBSCustomAnnotation"
          
            if let dequedView = mapView.dequeueReusableAnnotationView(
                withIdentifier: identifier)
                as? MKMarkerAnnotationView {
                annotationView = dequedView
            } else{
                annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            }
            annotationView.glyphImage = UIImage(named: "ok_logo.png")
            annotationView.image = UIImage(named: "ok_logo.png")
            annotationView.backgroundColor = .clear
            annotationView.markerTintColor = .clear
            annotationView.glyphTintColor = .clear
            annotationView.clusteringIdentifier = identifier
            return annotationView

        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation else { return }
        
        if let cluster = annotation as? FBAnnotationCluster {
            var zoomRect = MKMapRect.null
            for annotation in cluster.annotations {
                
                let annotationPoint = MKMapPoint.init(annotation.coordinate)
                let pointRect = MKMapRect.init(x: annotationPoint.x, y: annotationPoint.y, width: 0, height: 0)
                if zoomRect.isNull {
                    zoomRect = pointRect
                } else {
                    zoomRect = zoomRect.union(pointRect)
                }
            }
            mapView.setVisibleMapRect(zoomRect, animated: true)
        }else {
            if let eachAnnotation = annotation as? FBAnnotation {
                if let userdata = eachAnnotation.userData as? NearByServicesNewModel {
                    //println_debug("user each data details :::: \(userdata.shop_Name)  ,, \(userdata.shop_Address?.addressLine1) ,, \(userdata.shop_InMeter)")
                    hideSearchWithoutAnimation()
                    self.showPromotionInfoView(promotion: userdata)
                }
                
            }
        }
    }
    
    //New Methods
    @IBAction func listViewAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func filterButtonAction(_ sender: UIButton) {
        var arrTempList = [NearByServicesNewModel]()
        
        switch currentNearbyView {
        case .okAllView:
            arrTempList = arrMainList
        case .merchantView:
            arrTempList = arrMainList
        case .okAgentsView:
            arrTempList = arrMainList
        case .okOfficesView:
            arrTempList = arrMainList
        }
        
        if arrTempList.count == 0  {
            self.showToast(message: "No Results Found".localized, align: .top)
            return
        }
        guard let sortViewController = UIStoryboard(name: "NBSMain", bundle: nil).instantiateViewController(withIdentifier: "FilterNearByServiceViewController") as? FilterNearByServiceViewController else { return }
        sortViewController.categoryDelegate = self
        sortViewController.maincurrentlocationdelegate = self
        sortViewController.findviewStr = "NearByServices"
        if (defaults.value(forKey: "NBSsortBy") != nil) {
            sortViewController.previousSelectedSortOptionnew = defaults.value(forKey: "NBSsortBy") as? String
        } else {
            sortViewController.previousSelectedSortOptionnew = nil
        }
        sortViewController.checkMap = true
        sortViewController.previousSelectedSortOption = selectedSortItem
        sortViewController.previousSelectedFilterData = self.selectedFilterData
        sortViewController.dataList = arrTempList // this is used to sort all the promotions
        sortViewController.filterDataList = arrTempList // this is used to sort all the promotions
        sortViewController.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(sortViewController, animated: true, completion: nil)
        
    }
}



extension NBSMapVC: CategoriesSelectionDelegate {
    
    func didSelectLocationByCategory(category: SubCategoryDetail?) {
        
        var mainCategoryName = ""
        var subCategoryName = ""
        
        if let categ = category {
            mainCategoryName = categ.mainCategoryName
            subCategoryName = categ.subCategoryName
            
            defaults.set(categ.mainCategoryName + "," + categ.subCategoryName, forKey: "NBScategoryBy")
        }
       
        
        let filteredArray = arrMainList.filter { ($0.UserContactData?.BusinessName)?.range(of: mainCategoryName, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
        
        if((filteredArray.count) > 0)
        {
            let subCatArray = arrMainList.filter { ($0.UserContactData?.FirstName)?.range(of: subCategoryName, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
            if((subCatArray.count) > 0)
            {
                self.addAnnotationsInMap(list: subCatArray)
            }
            else
            {
                self.showErrorAlert(errMessage: "No Records Found".localized)

                self.addAnnotationsInMap(list: arrMainList)
            }
        }
        else
        {
            self.showErrorAlert(errMessage: "No Records Found".localized)

            self.addAnnotationsInMap(list: arrMainList)
        }
    }
}



extension NBSMapVC: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        didSelectSearchWithKey(key: searchBar.text!)
        
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        didSelectSearchWithKey(key: searchBar.text!)
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            
            if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: SEARCHCHARSET).inverted).joined(separator: "")) { return false }
            
            if updatedText.count > 25 {
                return false
            }
            if updatedText == " " || updatedText == "  "{
                return false
            }
            if (updatedText.contains("  ")){
                return false
            }
            
            let containsEmoji: Bool = updatedText.containsEmoji
            if (containsEmoji){
                return false
            }
            
            if updatedText != "" && text != "\n" {
                didSelectSearchWithKey(key: updatedText)
            }
        }
        return true
    }
    
    
    func didSelectSearchWithKey(key: String) {
        
        var arrTempList = [NearByServicesNewModel]()
        
        switch currentNearbyView {
        case .okAllView:
            arrTempList = arrMainList
        case .merchantView:
            arrTempList = arrMainList
        case .okAgentsView:
            arrTempList = arrMainList
        case .okOfficesView:
            arrTempList = arrMainList
        }
        
        if key.count > 0 {
            
            let filteredArray = arrTempList.filter { ($0.UserContactData?.BusinessName)?.range(of: key, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
            
            if((filteredArray.count) > 0)
            {
                reloadTableData(arrayList: filteredArray)
            }
            else
            {
                let filteredArray = arrTempList.filter { ($0.UserContactData?.FirstName)?.range(of: key, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
                
                reloadTableData(arrayList: filteredArray)
            }
        } else {
            println_debug("Text field is cleared")
            reloadListViewAfterSwitch()
        }
    }
    
    func reloadTableData(arrayList : [NearByServicesNewModel])
    {
        
        if arrayList.count > 0 {
            self.addAnnotationsInMap(list: arrayList)
        } else {
            //updateLocalizations()
            let initialLocation = CLLocation(latitude: Double(geoLocManager.currentLatitude)!, longitude: Double(geoLocManager.currentLongitude)!)
            centerMapOnLocation(location: initialLocation)
        }
        
      
    }
    
    func reloadListViewAfterSwitch()
    {
       
        if self.arrMainList.count > 0 {
            self.addAnnotationsInMap(list: arrMainList)
        } else {
            //updateLocalizations()
            let initialLocation = CLLocation(latitude: Double(geoLocManager.currentLatitude)!, longitude: Double(geoLocManager.currentLongitude)!)
            centerMapOnLocation(location: initialLocation)
        }
        
    }
    
    
    func reloadListEmptyViewAfterSwitch()
    {
      
    }
}


extension NBSMapVC: NearBySortViewDelegate {
    func didSelectSortOption(sortedList: [Any], selectedSortOption: Int)  {
        
        if selectedSortOption == 0 {
            // default selected so load the data get from server
            self.selectedFilterData = nil
            self.addAnnotationsInMap(list: arrMainList)
            
        }else {
            if let list = sortedList as? [NearByServicesNewModel] {
                self.addAnnotationsInMap(list: list)
            }
            self.selectedSortItem = selectedSortOption
        }
        
    }
}
