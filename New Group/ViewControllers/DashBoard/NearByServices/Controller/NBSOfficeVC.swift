//
//  NBSOfficeVC.swift
//  OK
//
//  Created by SHUBH on 6/26/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

private var longPressGestureRecognizer:UILongPressGestureRecognizer!


class NBSOfficeVC: OKBaseController {

    @IBOutlet weak var listTableView: UITableView! 
    var nearByServicesList = [NearByServicesNewModel]()
    weak var refreshdelegate: RefreshTableDelegate?
    var currenturlstring = String()
    var currentlatitude = String()
    var currentlogtitude = String()
    
    var nav : UINavigationController?
    @IBOutlet weak var infoLabel: UILabel!
    let nearByOfficeNotification = Notification.Name(rawValue:"NearByOfficeNotification")
    private let refreshControl = UIRefreshControl()

    // MARK: - View Life Cycle

    override func viewWillAppear(_ animated: Bool) {
        
        if nearByServicesList.count == 0 {
            showHideInfoLabel(isShow: true , content: "No Record Found".localized)
        }
        else
        {
            showHideInfoLabel(isShow: false , content: "")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadInitialize()
        //Manage Image View Display
        let nc = NotificationCenter.default
        nc.addObserver(forName:nearByOfficeNotification, object:nil, queue:nil, using:nearByOfficeNotification)
        
        //Long Press
        longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGestureRecognizer.minimumPressDuration = 0.2
        longPressGestureRecognizer.numberOfTapsRequired = 1
        longPressGestureRecognizer.numberOfTouchesRequired = 1
        // longPressGestureRecognizer.delegate = self
        self.listTableView.addGestureRecognizer(longPressGestureRecognizer)
        
        listTableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refereshAction), for: .valueChanged)
    }
    
    @objc func refereshAction(refreshControl: UIRefreshControl) {
    refreshdelegate!.didSelectrefreshTable(Latitude:currentlatitude ,Logtitude:currentlogtitude,str:currenturlstring)
        
        refreshControl.endRefreshing()
    }
    
    @objc func handleLongPress(longPressGesture: UILongPressGestureRecognizer) {
        let p = longPressGesture.location(in: self.listTableView)
        let indexPath = self.listTableView.indexPathForRow(at: p)
        if indexPath == nil {
            print("Long press on table view, not row.")
        } else if longPressGesture.state == UIGestureRecognizer.State.began {
            print("Long press on row, at \(indexPath!.row)")
            
            let promotion = nearByServicesList[indexPath!.row]
            let storyboard: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
            let protionDetailsView = storyboard.instantiateViewController(withIdentifier: "PromotionDetailView_ID") as! PromotionDetailViewController
            protionDetailsView.selectedNearByServicesNew = promotion
            protionDetailsView.strNearByPromotion = "NearByService"
            self.navigationController?.pushViewController(protionDetailsView, animated: true)
            
            
        }
    }
    

    // MARK: - Notification
    func nearByOfficeNotification(notification:Notification) -> Void {
        //println_debug("nearByOfficeNotification")
        
        //print("Notification called-------\(notification.userInfo?["NearByData"])---\(notification.userInfo?["Latitude"])-###\(notification.userInfo?["Logtitude"])--###\(notification.userInfo?["urlstring"])")
        
        nearByServicesList = (notification.userInfo?["NearByData"] as? [NearByServicesNewModel])!
        
        if nearByServicesList.count > 0 {
            
            currentlatitude = (notification.userInfo?["Latitude"]) as! String
            currentlogtitude = (notification.userInfo?["Logtitude"]) as! String
            currenturlstring = (notification.userInfo?["urlstring"]) as! String
            
        }
        
        if nearByServicesList.count == 0 {
          showHideInfoLabel(isShow: true , content: "You can't find OK$ office in your current location.".localized)
        }
        else
        {
            showHideInfoLabel(isShow: false , content: "")
        }
        listTableView.reloadData()
        
    }

    func loadInitialize() {
        let nib = UINib.init(nibName: "MapListCell", bundle: nil)
        listTableView.register(nib, forCellReuseIdentifier: "maplistcellidentifier")
        listTableView.tableFooterView = UIView(frame: CGRect.zero)
        listTableView.rowHeight = UITableView.automaticDimension
        listTableView.estimatedRowHeight = 250
        listTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func showHideInfoLabel(isShow: Bool, content: String) {
        DispatchQueue.main.async {
            if isShow {
                self.infoLabel.isHidden = false
                self.infoLabel.text = content
            }else {
                self.infoLabel.isHidden = true
            }
        }
    }
    
    @objc func callBtnAction(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to: self.listTableView)
        if let indexPath = self.listTableView.indexPathForRow(at: buttonPosition), nearByServicesList.count > 0 {
            let selectedpromotion = nearByServicesList[indexPath.row]
            let promotion = selectedpromotion
            if let phonenumber = promotion.UserContactData?.PhoneNumber {
                if phonenumber.count > 0 && phonenumber.count < 20 {
                    var mobNumber = getPhoneNumberByFormat(phoneNumber: phonenumber)
                    mobNumber = mobNumber.removingWhitespaces()
                    let phone = "tel://\(mobNumber)"
                    if let url = URL.init(string: phone) {
                        UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    }
                }
            }
        }else {
            alertViewObj.wrapAlert(title: nil, body: "No Phonenumber Exists".localized, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            alertViewObj.showAlert(controller: self)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension NBSOfficeVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nearByServicesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let listCell = tableView.dequeueReusableCell(withIdentifier: "maplistcellidentifier", for: indexPath) as! MapListCell
        let promotion = nearByServicesList[indexPath.row]
        listCell.callBtn.addTarget(self, action: #selector(callBtnAction(_:)), for: UIControl.Event.touchUpInside)
        listCell.photoImgView.image = nil
        if let imageStr = promotion.UserContactData?.ImageUrl, let imageUrl = URL(string: imageStr) {
            listCell.photoImgView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "cicoCamera"))
        }
        //listCell.photoImgView.image = UIImage.init(named: "cicoCamera")
        listCell.photoImgView.layer.borderWidth = 1.0
        listCell.photoImgView.layer.masksToBounds = false
        listCell.photoImgView.layer.borderColor = UIColor(red: 246/255, green: 198/255, blue: 0/225, alpha: 1).cgColor
        listCell.photoImgView.layer.cornerRadius = listCell.photoImgView.frame.size.width / 2
        listCell.photoImgView.clipsToBounds = true
        
        listCell.nameLbl.text = promotion.UserContactData?.BusinessName
        
        //        let str = promotion.phoneNumber!
        //        let agentCode = str.replacingOccurrences(of: "0095", with: "+95")
        //        listCell.phonenumberLbl.text = agentCode
        
        
        let agentCodenew  = getPlusWithDestinationNum((promotion.UserContactData!.PhoneNumber)!, withCountryCode: "+95")
        //Made this check to avoid crash in future
            if agentCodenew[0] == "+" &&  agentCodenew[1] == "9" && agentCodenew[2] == "5"{
                listCell.phonenumberLbl.text = agentCodenew.replacingOccurrences(of: "+95", with: "(+95) 0")
            }else{
                listCell.phonenumberLbl.text = agentCodenew
            }
        
        
       // listCell.phonenumberLbl.text = promotion.UserContactData?.PhoneNumber
        
        
        if(promotion.UserContactData?.BusinessName!.count == 0)
        {
            listCell.nameLbl.text = promotion.UserContactData?.FirstName
        }
        
        if let openTime = promotion.UserContactData!.OfficeHourFrom , let closeTime = promotion.UserContactData!.OfficeHourTo {

            if openTime.count > 0 && closeTime.count > 0 {
                listCell.timeImgView.isHidden = false
                listCell.timeLbl.isHidden = false
                listCell.timeLblHeightConstraint.constant = 20
                //listCell.timeImgView.image = UIImage.init(named: "time")
                if openTime == "00:00:00" && closeTime == "00:00:00" {
                    listCell.timeLbl.text = "Open 24/7"
                } else {
                    listCell.timeLbl.text = "\(listCell.getTime(timeStr: openTime)) - \(listCell.getTime(timeStr: closeTime))"
                }
            }else {
                listCell.timeImgView.isHidden = true
                listCell.timeLbl.isHidden = true
                listCell.timeLblHeightConstraint.constant = 0
            }
        }
        
        listCell.locationImgView.image = UIImage.init(named: "location.png")
        if let addressnew = promotion.UserContactData?.Address {
            if addressnew.count > 0 {
                
                listCell.locationLbl.text = "\(addressnew)"

                
               // listCell.locationLbl.text = "\(addressnew),\( promotion.UserContactData!.Township!),\( promotion.UserContactData!.Division!)"
            }else {
                listCell.locationLbl.text = "\(promotion.UserContactData!.Township!),\(promotion.UserContactData!.Division!)"
            }
        }
        
        //CashIn And CashOut
        
        if let cashinamount = promotion.UserContactData?.CashInLimit , let cashoutamount = promotion.UserContactData?.CashOutLimit {
            
            if cashinamount > 0 {
                let phone =  self.getNumberFormat("\(promotion.UserContactData!.CashInLimit!)").replacingOccurrences(of: ".00", with: "")
                listCell.cashInLbl.text = "Maximum Cash In Amount".localized + " : \(phone)" + " MMK"
                listCell.cashInLblHeightConstraint.constant = 20
            } else {
                listCell.cashInLblHeightConstraint.constant = 0
            }
            
            if cashoutamount > 0 {
                let phonenew =  self.getNumberFormat("\(promotion.UserContactData!.CashOutLimit!)").replacingOccurrences(of: ".00", with: "")
                listCell.cashOutLbl.text = "Maximum Cash Out Amount".localized + " : \(phonenew)" + " MMK"
                listCell.cashOutLblHeightConstraint.constant = 20
            } else{
                listCell.cashOutLblHeightConstraint.constant = 0
            }
        }
        // listCell.remarksLbl.text = ""
        listCell.callBtn.setImage(UIImage.init(named: "call.png"), for: UIControl.State.normal)
        listCell.distanceLblKm.text = "(0 Km)"
        if let shopDistance = promotion.distanceInKm {
            listCell.distanceLblKm.text = String(format: "(%.2f Km)", shopDistance).replacingOccurrences(of: ".00", with: "")
        }
        
        if promotion.UserContactData?.LocationNewData?.Latitude == 0 && promotion.UserContactData?.LocationNewData?.Longitude == 0 {
            listCell.durationLbl.text = "1 Min"
        } else {
            listCell.durationLbl.text = listCell.durationFromDistance(distance: promotion.distanceInKm) as String
        }

        return listCell
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let verticalIndicator = scrollView.subviews.last as? UIImageView
        verticalIndicator?.backgroundColor = UIColor.yellow
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let promotion = nearByServicesList[indexPath.row]
//        let storyboard: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
//        let protionDetailsView = storyboard.instantiateViewController(withIdentifier: "PromotionDetailView_ID") as! PromotionDetailViewController
//        protionDetailsView.selectedNearByServicesNew = promotion
//        protionDetailsView.strNearByPromotion = "NearByService"
//
//        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
//
////        print("cellforcontrollercount\(viewControllers)")
////        print("cellforcontrollercount\(viewControllers.count)")
//
//        if viewControllers.count < 3 {
//            self.navigationController?.pushViewController(protionDetailsView, animated: true)
//        }
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Map", bundle: nil)
        let vc : MapPayToVC = storyboard.instantiateViewController(withIdentifier: "MapPayToVC") as! MapPayToVC
        vc.merchantModel = nearByServicesList
        vc.merchantModelBackup = nearByServicesList
        vc.isComingFromInitialView = true
        vc.selectedIndex = indexPath.row
        
        let navigationController = UINavigationController(rootViewController: vc)

       self.presentDetail(navigationController)
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 250
        tableView.rowHeight = UITableView.automaticDimension
        return tableView.rowHeight
    }
    
    
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
