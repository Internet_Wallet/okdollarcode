//
//  NearByServicesMainVC.swift
//  OK
//
//  Created by SHUBH on 6/26/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Rabbit_Swift
import CoreLocation
import MapKit
import IQKeyboardManagerSwift

protocol MainFilterApplyViewDelegate : class {
    func didSelectApplyFilter(category: SubCategoryDetail? , index:Int, str:Int)
}

protocol RefreshTableDelegate : class {
    func didSelectrefreshTable(Latitude:String ,Logtitude:String,str:String)
}

class NearByServicesMainVC: MapBaseViewController, UINavigationControllerDelegate, NearByPageViewControllerDelegates ,MainLocationViewDelegateRegistration,MainNearByLocationViewDelegate,MainwheretoLocationViewDelegate,MainLocationViewDelegate,RefreshTableDelegate {
    

    @IBOutlet weak var headerHeight: NSLayoutConstraint!
    var needToShowBackButton: Bool = false
    let appDel = UIApplication.shared.delegate as! AppDelegate
    var statusScreen = "Service"
    var isPushed: Bool = false
    var checkurl = String()
    var menusortedindex : Int = 0
    var currentLatitude = String()
    var currentLogtitude = String()
    
//Tushar
    //This variable will be used from coming from payto to map i am using same view
    var isComingFromMap = false

    @IBOutlet weak var backBtn: UIButton!

    //prabu
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var mapView: UIView! {
        didSet {
             self.mapView.clipsToBounds = true
             self.mapView.layer.borderWidth = 0.8
             self.mapView.layer.borderColor = UIColor.lightGray.cgColor
             self.mapView.layer.cornerRadius = 20.0
             self.mapView.layer.shadowColor = UIColor.lightGray.cgColor
             self.mapView.layer.shadowOpacity = 0.8
             self.mapView.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
             self.mapView.layer.shadowRadius = 5.0
        }
    }
    
    @IBOutlet weak var TopLocationBtn: UIButton!
        {
        didSet
        {
            self.TopLocationBtn.layer.masksToBounds = true
            self.TopLocationBtn.layer.borderWidth = 1.0
            self.TopLocationBtn.layer.borderColor = UIColor.white.cgColor
            self.TopLocationBtn.layer.cornerRadius = 20.0
            
        }
    }
    
    @IBOutlet weak var filterLabel: UILabel! {
        didSet {
            self.filterLabel.text = "FILTER".localized
            self.filterLabel.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    @IBOutlet weak var mapLabel: UILabel! {
        didSet {
            self.mapLabel.text = "MAP".localized
            self.mapLabel.font = UIFont(name: appFont, size: appTitleButton)
        }
    }

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
        {
        didSet
        {
            self.headerLabel.font = UIFont(name: appFont, size: appFontSize)
            //headerLabel.text = "Nearby OK$ Services Title".localized
        }
    }
    
    //prabu
    @IBOutlet weak var allLabel: UILabel!
        {
        didSet
        {
            allLabel.text = "All NBS".localized + "     "
            allLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var agentLabel: UILabel!
        {
        didSet
        {
            agentLabel.text = "Agents".localized + "     "
            agentLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var officeLabel: UILabel!
        {
        didSet
        {
            officeLabel.text = "Branch Office".localized + "     "
            officeLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var merchantLabel: UILabel!
        {
        didSet
        {
            merchantLabel.text = "Merchants".localized + "     "
            merchantLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }

    
    @IBOutlet weak var tabsView: UIView!
    @IBOutlet weak var tabAllBtn: UIButton!
        {
        didSet
        {
            //self.tabAllBtn.setTitle("All NBS".localized, for: .normal)
        }
    }
    @IBOutlet weak var tabMerchantBtn: UIButton!
        {
        didSet
        {
           // self.tabMerchantBtn.setTitle("Merchants".localized, for: .normal)
        }
    }
    @IBOutlet weak var tabOkAgentsBtn: UIButton!
        {
        didSet
        {
           // self.tabOkAgentsBtn.setTitle("Agents".localized, for: .normal)
        }
    }
    @IBOutlet weak var tabOkOfficesBtn: UIButton!
        {
        didSet
        {
           // self.tabOkOfficesBtn.setTitle("Branch Office".localized, for: .normal)
        }
    }
   
    @IBOutlet weak var okALLSeparatorLbl: UILabel!
    @IBOutlet weak var merchantsSeparatorLbl: UILabel!
    @IBOutlet weak var okAgentsSeparatorLbl: UILabel!
    @IBOutlet weak var okOfficesSeparatorLbl: UILabel!
    @IBOutlet weak var headerSearchBtn: UIButton!
    @IBOutlet weak var switchMapListBtn: UIButton!
    
    var mapType: UIType?
    var navController : UINavigationController?
    
    var currentNearbyView: NearByView = .merchantView
    var prevNearbyView: NearByView = .merchantView
    
    @IBOutlet weak var topSearchView: UIView!
    
    @IBOutlet weak var topSearchBar: UISearchBar!
    
    @IBOutlet weak var searchViewBackBtn: UIButton!
    
    var isHeaderSearchOpened: Bool = false
    
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var headerSearchHeightConstraint: NSLayoutConstraint!
    
    //Record Manage
    var cashInListBackUPByCurLoc        : [NearByServicesNewModel]?
    var cashInListRecentFromServer      : [NearByServicesNewModel]?
    var arrAllList                      = [NearByServicesNewModel]()
    var arrMerchantList                 = [NearByServicesNewModel]()
    var arrAgentsList                   = [NearByServicesNewModel]()
    var arrOfficeList                   = [NearByServicesNewModel]()
    var arrEmptyList                    = [NearByServicesNewModel]()
    var arrFilteredList                 = [NearByServicesNewModel]()
    
    let margin: CGFloat = 2
    var cellsPerRow: Int = 0
    
    var selectedSortItem        : Int = 0
    var selectedFilterData      : NearByServicesNewModel?
    
    var selectedLocationType: Location?
    let nearbydistance = kNearByDistance
    
    var selectedLocationDetail          : LocationDetail?
    var selectedTownshipDetail          : TownShipDetail?
    
    var selectedLocationDetailStr         : String?
    var selectedTownshipDetailStr         : String?
    
    let sortView        = NearBySortViewController()
    let filterView      = NearByFilterViewController()
    
    //Manage Page Control
    @IBOutlet weak var container: NearByPageViewController!
    var statusScrollTab = "Scroll"
    
    //MARK: View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        topSearchBar.delegate = self
        let origImage = UIImage(named: "tabBarBack")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        searchViewBackBtn.setImage(tintedImage, for: .normal)
        searchViewBackBtn.tintColor = .lightGray
        
      //This notification will work only when coming from map view which is newly created to help the controller to pop to dashboard
       NotificationCenter.default.addObserver(self, selector: #selector(popToDashboard), name: Notification.Name("PopToDashboard"), object: nil)
        if isComingFromMap{
           headerHeight.constant = 0.0
        }else{
            headerHeight.constant = 55.0
        }
        
        //"Nearby OK$ Services Title"
                //prabu
        defaults.set(nil, forKey: "NBSsortBy")
        defaults.set(nil, forKey: "NBSfilterBy")
        defaults.set(nil, forKey: "NBScategoryBy")
        defaults.set(nil, forKey: "NBSRecentLocationAddress1")
        defaults.set(nil, forKey: "NBSRecentLocationAddress2")
        defaults.set(nil, forKey: "NBSCurrentLocation")

       
        // Do any additional setup after loading the view.
        if needToShowBackButton {
            self.backBtn.isHidden = true
        }
        currentNearbyView = .okAllView

        geoLocManager.startUpdateLocation()

        loadUI()
        updateLocalizations()
        
        //Search Setup
        searchBarSettings()
        hideSearchWithoutAnimation()
        
        //Location Manage
        didSelectLocationByCurrentLocation()
        highLightAllView()
        
        // Do any additional setup after loading the view.
        container.getTo(0, animated: true)
        
        //Manage Keyboard
        IQKeyboardManager.sharedManager().enable            = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
    }
    
    @objc func popToDashboard() {
       isComingFromMap = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        

        if let searchTextField = topSearchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
                    if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                       //(appDel.currentLanguage == "uni") ? UIFont.systemFont(ofSize: 15) : myFont
                        //searchTextField.font = (appDel.currentLanguage == "uni") ? UIFont.systemFont(ofSize: 15) : myFont
                        if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                            searchTextField.font = myFont
                        }else{
                            //
                        }
                        searchTextField.keyboardType = .asciiCapable
                        searchTextField.placeholder = "Search".localized
                        searchTextField.backgroundColor  = .white
                    }
                }
                let view = self.topSearchBar.subviews[0] as UIView
                let subViewsArray = view.subviews
                for subView: UIView in subViewsArray {
                    if subView.isKind(of: UITextField.self) {
                        subView.tintColor = ConstantsColor.navigationHeaderTransaction
                    }
                }
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if needToShowBackButton {
            //self.navigationController?.isNavigationBarHidden = false
        }
        
    }
    
    //MARK: - Promotion Delegate
    func didSelectLocationByCurrentLocation() {
        
        print("Current Location called----")
        updateCurrentLocationToUI()
        
        self.checkurl = "CurrentLocation"

        guard let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude else {
            // throw the error to the use
            return
        }
        
        self.getOfficeAPI(lat: lat , long: long)
    }
    
    func didSelectrefreshTable(Latitude:String ,Logtitude:String,str:String) {
         headerLabel.text = "\(userDef.value(forKey: "lastLoginKey") as? String ?? "")     "
       // print("Refresh Table method called------###\(str)&&&&&----\(Latitude)---\(Logtitude)")
        defaults.set(nil, forKey: "NBSsortBy")
        self.checkurl = str
        self.getOfficeAPI(lat: Latitude , long: Logtitude )
    }

    
    func didSelectLocationByTownship(location: LocationDetail, township: TownShipDetail) {
        
    }

    
    func didSelectNearByLocation() {
        
        self.checkurl = "NearBy"
        headerLabel.text = "Near By".localized

        guard let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude else {
            // throw the error to the user
            return
        }
        
        self.getOfficeAPI(lat: lat , long: long )
    }
    
    func didSelectwheretoLocation(streetName:String)
    {
        headerLabel.text = "     " + streetName + "     "
        
        geoLocManager.getLatLongByName(cityname: streetName) { (isSuccess, lat, long) in
            
            guard isSuccess, let latti = lat, let longi = long  else {
                //self.showErrorAlert(errMessage: "Try Again".localized)
                return
            }
            
            self.getOfficeAPI(lat:latti , long:longi)

        }
        
    }

    
    func didSelectLocationByTownshipRegistration(location: LocationDetail, township: TownShipDetailForAddress) {
        
        self.checkurl = "Township"

        if appDel.getSelectedLanguage() == "my" {
            headerLabel.text = location.stateOrDivitionNameMy  + " , " + township.cityNameMY + "    "
        } else if appDel.getSelectedLanguage() == "en" {
            headerLabel.text = location.stateOrDivitionNameEn + " , " + township.cityNameEN + "    "
        }else{
            headerLabel.text = location.stateOrDivitionNameUni + " , " + township.cityNameUni + "    "
        }
       //This is done by tushar i am sending the english code because we are not gettign response from server on other language
        updateOtherLocationToUINew(location: location.stateOrDivitionNameEn, township: township.townShipNameEN)
        self.getOfficeAPI(lat: geoLocManager.currentLatitude , long: geoLocManager.currentLongitude)

    }
    
    func updateOtherLocationToUI(location: LocationDetail, township: TownShipDetail) {
        selectedLocationDetail = location
        selectedTownshipDetail = township
   }
    
    func updateOtherLocationToUINew(location: String, township: String) {
        
        selectedLocationDetailStr = location
        selectedTownshipDetailStr = township
        
    }
    
    //MARK: - Initial Setup

    func updateCurrentLocationToUI() {
        
        //headerLabel.text =  "          \(userDef.value(forKey: "lastLoginKey") as! String)          " ?? ""
        
        
        headerLabel.text = "\(userDef.value(forKey: "lastLoginKey") as? String ?? "")     "
        
        
        defaults.set(userDef.value(forKey: "lastLoginKey") as? String ?? nil, forKey: "NBSCurrentLocation")
    }
    
    @IBAction func filterButtonAction(_ sender: UIButton) {
        var arrTempList = [NearByServicesNewModel]()
        
        
        switch currentNearbyView {
        case .okAllView:
            arrTempList = arrAllList
        case .merchantView:
            arrTempList = arrMerchantList
        case .okAgentsView:
            arrTempList = arrAgentsList
        case .okOfficesView:
            arrTempList = arrOfficeList
        }
            
        
        if arrTempList.count == 0  {
            self.showToast(message: "No Results Found".localized, align: .top)
            return
        }
        guard let sortViewController = UIStoryboard(name: "NBSMain", bundle: nil).instantiateViewController(withIdentifier: "FilterNearByServiceViewController") as? FilterNearByServiceViewController else { return }
        sortViewController.mainlocationdelegateReg = self
        sortViewController.categoryDelegate = self
        sortViewController.applybuttondelegate = self
        sortViewController.mainlocationdelegate = self
        sortViewController.sortedindex =  menusortedindex
        sortViewController.findviewStr = "NearByServices"
        
    
        if (defaults.value(forKey: "NBSsortBy") != nil) {
            sortViewController.previousSelectedSortOptionnew = defaults.value(forKey: "NBSsortBy") as? String
        }else {
            sortViewController.previousSelectedSortOptionnew = nil
        }
        
        sortViewController.checkMap = false
        sortViewController.previousSelectedSortOption = selectedSortItem
        sortViewController.previousSelectedFilterData = self.selectedFilterData
        sortViewController.dataList = arrTempList // this is used to sort all the promotions
        sortViewController.filterDataList = arrTempList // this is used to sort all the promotions
        sortViewController.modalPresentationStyle = .overCurrentContext
        //self.navigationController?.present(sortViewController, animated: true, completion: nil)
        self.present(sortViewController, animated: true, completion: nil)
    }
    
    
    @IBAction func currentLocationAction(_ sender: UIButton) {
        defaults.set(nil, forKey: "NBSsortBy")

        let storyBoardName = UIStoryboard(name: "Promotion", bundle: nil)
        guard let locationSelectionView = storyBoardName.instantiateViewController(withIdentifier: "SelecteMainLocationView_ID") as? SelectMainLocationViewController else { return }
        locationSelectionView.delegateRegistration = self
        locationSelectionView.delegatenearby = self
        locationSelectionView.delegate = self
        locationSelectionView.delegatewhereto = self
        locationSelectionView.isComingFromAgentList = true
        if isComingFromMap {
            if let _ = self.navigationController{
                self.navigationController?.pushViewController(locationSelectionView, animated: true)
            }else{
                let navigationController = UINavigationController(rootViewController: locationSelectionView)
                self.presentDetail(navigationController)
            }
        } else {
           let navigationController = UINavigationController(rootViewController: locationSelectionView)
            self.presentDetail(navigationController)
        }
    }
    
   
    
    func loadUI() {
        //headerHeightConstraint.constant = kHeaderHeight
       // headerSearchHeightConstraint.constant = kHeaderHeight
       
        headerView.backgroundColor = UIColor(red: 245.0/255.0, green: 198.0/255.0, blue: 0/255.0, alpha: 1.0)

        self.view.setShadowToLabel(thisView: merchantsSeparatorLbl)
        self.view.setShadowToLabel(thisView: okAgentsSeparatorLbl)
        self.view.setShadowToLabel(thisView: okOfficesSeparatorLbl)
        
        tabsView.backgroundColor = UIColor.white
        tabMerchantBtn.backgroundColor = UIColor.clear
        tabOkAgentsBtn.backgroundColor = UIColor.clear
        tabOkOfficesBtn.backgroundColor = UIColor.clear
        
    }
    
    func updateLocalizations() {
        headerLabel.font = UIFont.init(name: appFont, size: 18)
        if(statusScreen == "NearBy")
        {
           //headerLabel.text = "Nearby OK$ Services Title".localized
            self.title = "Nearby OK$ Services Title".localized
            self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18), NSAttributedString.Key.foregroundColor: UIColor.white]
        }
        else
        {
            //headerLabel.text = "OK$ Services".localized
        }
        tabMerchantBtn.titleLabel?.font = UIFont.init(name: appFont, size: appButtonSize)
        tabOkAgentsBtn.titleLabel?.font = UIFont.init(name: appFont, size: appButtonSize)
        tabOkOfficesBtn.titleLabel?.font = UIFont.init(name: appFont, size: appButtonSize)
        
//        locationNameLbl.textColor = kBlueColor
//        locationContentLbl.textColor = kBlueColor
//        locationNameLbl.font = UIFont.init(name: appFont, size: 15)
//        locationContentLbl.font = UIFont.init(name: appFont, size: 14)
//        locationContentLbl.lineBreakMode = NSLineBreakMode.byWordWrapping
    }
    
    
    //
    // MARK: - Pageview Controller Delegate
    //
    
    func didScroll(to index: Int) {

        hideSearchWithoutAnimation()

        statusScrollTab = "Scroll"

        switch index {
        case 0:
            self.highLightAllView()
        case 1:
            self.highLightOKAgentsView()
        case 2:
            self.highLightOKOfficesView()
        case 3:
            self.highLightMerchantView()

        default:
            break
        }
    }
    
    //MARK: Custom Action
    @IBAction func mainViewRefereshAction(_ sender: Any) {
        println_debug("mainViewRefereshAction")
        didSelectLocationByCurrentLocation()
    }
    
    @IBAction func mapBtnAction(_ sender: Any) {
        
        var arrTempList = [NearByServicesNewModel]()
      //  let storyboard: UIStoryboard = UIStoryboard(name: "NBSMain", bundle: nil)
      //  let objMapVC = storyboard.instantiateViewController(withIdentifier: "NBSMapVC") as! NBSMapVC
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Map", bundle: nil)
        let vc : MapPayToVC = storyboard.instantiateViewController(withIdentifier: "MapPayToVC") as! MapPayToVC
        
        switch currentNearbyView {
        case .okAllView:
            arrTempList = arrAllList
           // objMapVC.intType = 3
       case .merchantView:
            arrTempList = arrMerchantList
          //  objMapVC.intType = 2
        case .okAgentsView:
            arrTempList = arrAgentsList
          //  objMapVC.intType = 8
        case .okOfficesView:
            arrTempList = arrOfficeList
          //  objMapVC.intType = 7
       }
       //objMapVC.currentNearbyView = currentNearbyView
       //objMapVC.arrMainList = arrTempList
         vc.merchantModel = arrTempList
         vc.merchantModelBackup = arrTempList
        vc.isComingFromInitialView = false
       // objMapVC.screenType = headerLabel.text ?? "NearBy"
     //   self.navigationController?.pushViewController(objMapVC, animated: true)
       let navigationController = UINavigationController(rootViewController: vc)
       self.presentDetail(navigationController)
    }
    
    @IBAction func nearByBackAction(_ sender: Any) {
        //Tushar Lama
        //This will work only while coming from map view
            if isComingFromMap{
                
                if let _ = self.navigationController{
                    self.navigationController?.isNavigationBarHidden = false
                    self.navigationController?.popViewController(animated: true)
                }else{
                    NotificationCenter.default.post(name: Notification.Name("MapNotification"), object: nil, userInfo: nil)
                    self.dismiss(animated: true, completion: nil)
                }
        }else{
            if isPushed {
                self.navigationController?.isNavigationBarHidden = false
                self.navigationController?.popViewController(animated: true)
            } else {
                if let presentingView = self.presentingViewController {
                    presentingView.dismiss(animated: true, completion: nil)
                }
            }
        }
        
        
    }
    
    //MARK: Manage TABS
    func highLightAllView() {
        
        currentNearbyView = .okAllView
        DispatchQueue.main.async {
            
            UIView.animate(withDuration: 0.5) {
                
                if(self.statusScrollTab == "Scroll")
                {
                    self.tabAllBtn.titleLabel?.textColor = UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1)
                    
                    self.allLabel.textColor = UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1)
                }
                else
                {
                    self.tabAllBtn.setTitleColor(UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1), for: .normal)
                    
                    self.allLabel.textColor = UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1)
                    
                    //self.tabAllBtn.setTitleColor(.orange, for: .normal)
                }
                self.tabMerchantBtn.titleLabel?.textColor = UIColor.black
                self.tabOkAgentsBtn.titleLabel?.textColor = UIColor.black
                self.tabOkOfficesBtn.titleLabel?.textColor = UIColor.black
                self.okALLSeparatorLbl.backgroundColor = UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1)
                self.merchantsSeparatorLbl.backgroundColor = UIColor.clear
                self.okAgentsSeparatorLbl.backgroundColor = UIColor.clear
                self.okOfficesSeparatorLbl.backgroundColor = UIColor.clear
                
                self.agentLabel.textColor = UIColor.black
                self.officeLabel.textColor = UIColor.black
                self.merchantLabel.textColor = UIColor.black


            }
            self.container.getTo(0, animated: true)
            
            let nc = NotificationCenter.default
            
            print(self.arrAllList)
            if self.arrAllList.count > 0 {
                self.mapView.isHidden = false
            } else {
                self.mapView.isHidden = true
            }
            
            
            
            
       nc.post(name:Notification.Name(rawValue:"NearByAllDataNotification"),
                    object: nil, userInfo: ["NearByData": self.arrAllList , "Latitude":self.currentLatitude , "Logtitude":self.currentLogtitude , "urlstring":self.checkurl])
            
        }
    }
    
    
    func highLightMerchantView() {
        currentNearbyView = .merchantView
        DispatchQueue.main.async {
            
            UIView.animate(withDuration: 0.5) {
                
                if(self.statusScrollTab == "Scroll")
                {
                    self.tabMerchantBtn.titleLabel?.textColor = UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1)
                    
                    self.merchantLabel.textColor = UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1)
                }
                else
                {
            self.tabMerchantBtn.setTitleColor(UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1), for: .normal)
                    
                    self.merchantLabel.textColor = UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1)
                //self.tabMerchantBtn.setTitleColor(.orange, for: .normal)
                }
                self.tabOkAgentsBtn.titleLabel?.textColor = UIColor.black
                self.tabOkOfficesBtn.titleLabel?.textColor = UIColor.black
                self.tabAllBtn.titleLabel?.textColor = UIColor.black

                self.merchantsSeparatorLbl.backgroundColor = UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1)
                self.okAgentsSeparatorLbl.backgroundColor = UIColor.clear
                self.okOfficesSeparatorLbl.backgroundColor = UIColor.clear
                self.okALLSeparatorLbl.backgroundColor = UIColor.clear
                
                //prabu
                self.agentLabel.textColor = UIColor.black
                self.officeLabel.textColor = UIColor.black
                self.allLabel.textColor = UIColor.black

            }
            self.container.getTo(3, animated: true)

            let nc = NotificationCenter.default
            
            if self.arrMerchantList.count > 0 {
                self.mapView.isHidden = false
            } else {
                self.mapView.isHidden = true
            }
            nc.post(name:Notification.Name(rawValue:"NearByMerchantNotification"),
                    object: nil,
                    userInfo: ["NearByData": self.arrMerchantList , "Latitude":self.currentLatitude , "Logtitude":self.currentLogtitude , "urlstring":self.checkurl])
            
        }
    }
    
    func highLightOKAgentsView() {
      
        currentNearbyView = .okAgentsView
          DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5) {
                if(self.statusScrollTab == "Scroll")
                {
                    self.tabOkAgentsBtn.titleLabel?.textColor = UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1)
                    
                     self.agentLabel.textColor = UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1)
                }
                else
                {
                    self.tabOkAgentsBtn.setTitleColor(UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1), for: .normal)
                    
                     self.agentLabel.textColor = UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1)
                    
                   // self.tabOkAgentsBtn.setTitleColor(.orange, for: .normal)
                }
                self.tabMerchantBtn.titleLabel?.textColor = UIColor.black
                self.tabOkOfficesBtn.titleLabel?.textColor = UIColor.black
                self.tabAllBtn.titleLabel?.textColor = UIColor.black

                self.merchantsSeparatorLbl.backgroundColor = UIColor.clear
                self.okAgentsSeparatorLbl.backgroundColor = UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1)
                self.okOfficesSeparatorLbl.backgroundColor = UIColor.clear
                self.okALLSeparatorLbl.backgroundColor = UIColor.clear
                
                //prabu
                self.merchantLabel.textColor = UIColor.black
                self.officeLabel.textColor = UIColor.black
                self.allLabel.textColor = UIColor.black


            }
        
            self.container.getTo(1, animated: true)

            let nc = NotificationCenter.default

            if self.arrAgentsList.count > 0 {
                self.mapView.isHidden = false
            } else {
                self.mapView.isHidden = true
            }
            nc.post(name:Notification.Name(rawValue:"NearByAgentNotification"),
                    object: nil,
                    userInfo: ["NearByData": self.arrAgentsList , "Latitude":self.currentLatitude , "Logtitude":self.currentLogtitude , "urlstring":self.checkurl])
        }
    }
    
    func highLightOKOfficesView() {
        currentNearbyView = .okOfficesView
        DispatchQueue.main.async {
            
            UIView.animate(withDuration: 0.5) {
                
                self.tabMerchantBtn.titleLabel?.textColor = UIColor.black
                self.tabOkAgentsBtn.titleLabel?.textColor = UIColor.black
                self.tabAllBtn.titleLabel?.textColor = UIColor.black

                if(self.statusScrollTab == "Scroll")
                {
                    self.tabOkOfficesBtn.titleLabel?.textColor = UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1)
                    
                    self.officeLabel.textColor = UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1)
                }
                else
                {
                    self.tabOkOfficesBtn.setTitleColor(UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1), for: .normal)
                    
                    self.officeLabel.textColor = UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1)
                    //self.tabOkOfficesBtn.setTitleColor(.orange, for: .normal)
                }
                self.okALLSeparatorLbl.backgroundColor = UIColor.clear
                self.merchantsSeparatorLbl.backgroundColor = UIColor.clear
                self.okAgentsSeparatorLbl.backgroundColor = UIColor.clear
                self.okOfficesSeparatorLbl.backgroundColor = UIColor.colorWithRedValue(redValue: 246, greenValue: 198, blueValue: 0, alpha: 1)
                
                //prabu
                self.merchantLabel.textColor = UIColor.black
                self.agentLabel.textColor = UIColor.black
                self.allLabel.textColor = UIColor.black
            }
            self.container.getTo(2, animated: true)

            let nc = NotificationCenter.default
            
            if self.arrOfficeList.count > 0 {
                self.mapView.isHidden = false
            } else {
                self.mapView.isHidden = true
            }
            nc.post(name:Notification.Name(rawValue:"NearByOfficeNotification"),
                    object: nil,
                    userInfo: ["NearByData": self.arrOfficeList , "Latitude":self.currentLatitude , "Logtitude":self.currentLogtitude , "urlstring":self.checkurl])

        }
    }
    
    //MARK: Search Manage Delegate
    func searchBarSettings() {
        topSearchBar.tintColor = UIColor.darkGray
        topSearchBar.barTintColor = UIColor.white
        topSearchBar.searchBarStyle = UISearchBar.Style.minimal
        
        let searchTextField: UITextField? = topSearchBar.value(forKey: "searchField") as? UITextField
        if searchTextField!.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
            let attributeDict = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
            searchTextField!.attributedPlaceholder = NSAttributedString(string: "Search".localized, attributes: attributeDict)
            searchTextField!.backgroundColor = UIColor.white
            searchTextField!.borderStyle = UITextField.BorderStyle.none
            searchTextField!.layer.borderColor = UIColor.clear.cgColor
            searchTextField!.layer.borderWidth = 0.0
            searchTextField!.textColor = UIColor.black
            searchTextField!.autocorrectionType = UITextAutocorrectionType.no
            let myFont = UIFont.init(name: appFont, size: 15)
            if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                searchTextField!.font = myFont
            }else{
                //
            }
            searchTextField!.leftViewMode = UITextField.ViewMode.never
            searchTextField!.placeholder = "Search".localized
        }
        
    }
    
    func resignSearchBarKeypad() {
        topSearchBar.resignFirstResponder()
    }
    
    func hideSearchWithoutAnimation() {
        isHeaderSearchOpened = false
        topSearchBar.text = ""
        topSearchBar.resignFirstResponder()
        topSearchView.isHidden = true
    }
    
    @IBAction func searchBtnAction(_ sender: Any) {
      
        switch currentNearbyView {
        case .okAllView:
            showCashSearch()
        case .merchantView:
            //self.topSearchView.frame = CGRect(x: screenArea.size.width - 30, y: 0, width: 0, height: kHeaderHeight)
            showCashSearch()
        case .okAgentsView:
            //self.topSearchView.frame = CGRect(x: screenArea.size.width - 30, y: 0, width: 0, height: kHeaderHeight)
            showCashSearch()
        case .okOfficesView:
            //self.topSearchView.frame = CGRect(x: screenArea.size.width - 30, y: 0, width: 0, height: kHeaderHeight)
            showCashSearch()
        }
    }
    
    func showCashSearch() {
        isHeaderSearchOpened = true
        self.mapView.isHidden = true
        topSearchBar.text = ""
        topSearchView.layer.masksToBounds = true
        topSearchView.layer.cornerRadius = 20
        topSearchView.alpha = 0.0
        UIView.animate(withDuration: 0.3, animations: {
            var viewFrame: CGRect = self.topSearchView.frame
            viewFrame.origin.x = 0
            viewFrame.size.width = screenArea.size.width
            self.topSearchView.frame = viewFrame
            self.topSearchView.alpha = 1.0
            self.topSearchView.isHidden = false
            self.topSearchBar.becomeFirstResponder()
            
        }) { _ in
            self.topSearchView.layer.cornerRadius = 0
        }
    }
    
    func hideCashSearch() {
        self.mapView.isHidden = false
        isHeaderSearchOpened = false
        topSearchBar.text = ""
        topSearchBar.resignFirstResponder()
        topSearchView.layer.cornerRadius = 20
        topSearchView.alpha = 1.0
        UIView.animate(withDuration: 0.3, animations: {
            var viewFrame: CGRect = self.topSearchView.frame
            viewFrame.origin.x = screenArea.size.width - 30
            viewFrame.origin.y = self.topSearchView.frame.origin.y
            viewFrame.size.width = 0
            viewFrame.size.height = self.topSearchView.frame.size.height
            self.topSearchView.frame = viewFrame
            
        }) { _ in
            self.topSearchView.alpha = 0.0
        }
    }
    
    @IBAction func searchViewBackAction(_ sender: Any) {
        //Hide search view
        hideCashSearch()
        reloadListViewAfterSwitch()
    }
    
    //MARK: Page Control Delegate
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("prepare secure storyboard called-----------------")
        if segue.identifier == "EMBEDD_TAB_NEARBY"
        {
          container = segue.destination as? NearByPageViewController
            
            let commonStoryboard = UIStoryboard.init(name: "NBSMain", bundle: nil)
            
             if isComingFromMap{
                let objAllnew = commonStoryboard.instantiateViewController(withIdentifier: "NBSAllDataVC") as! NBSAllDataVC
                objAllnew.nearByServicesList = arrAllList
                objAllnew.refreshdelegate = self
                container.arrViewController=[objAllnew]
                container.mDelegate = self
             } else {
                let objAllnew = commonStoryboard.instantiateViewController(withIdentifier: "NBSAllDataVC") as! NBSAllDataVC
                objAllnew.nearByServicesList = arrAllList
                objAllnew.refreshdelegate = self
                let objMerchant = commonStoryboard.instantiateViewController(withIdentifier: "NBSMerchantVC") as! NBSMerchantVC
                objMerchant.nearByServicesList = arrMerchantList
                objMerchant.refreshdelegate = self
                // objMerchant.currenturlstring = self.checkurl
                let objAgent = commonStoryboard.instantiateViewController(withIdentifier: "NBSAgentsVC") as! NBSAgentsVC
                objAgent.nearByServicesList = arrAgentsList
                objAgent.refreshdelegate = self
                
                let objOffice = commonStoryboard.instantiateViewController(withIdentifier: "NBSOfficeVC") as! NBSOfficeVC
                objOffice.nearByServicesList = arrOfficeList
                objOffice.refreshdelegate = self
                
                container.arrViewController=[objAllnew,objAgent,objOffice,objMerchant]
                container.mDelegate = self
            }
           
        }
    }
    
   
    
    //MARK: - Tab Handling
    @IBAction func didSelectTabs(_ sender: Any) {
     
        hideSearchWithoutAnimation()

        statusScrollTab = "Tab"
        switch (sender as AnyObject).tag {
        case 0:
            self.highLightAllView()
        case 1:
            self.highLightOKAgentsView()
        case 2:
            self.highLightOKOfficesView()
        case 3:
            self.highLightMerchantView()

            
        default:
            break
        }
    }
    
    //MARK:- Get API Data
    
    func getOfficeAPI(lat: String, long: String)
    {
        println_debug("call ok offices api ")
        
        var type : Int
        switch currentNearbyView {
        case .okAllView:
            type = 3
        case .merchantView:
            type = 2
        case .okAgentsView:
            type = 8
        case .okOfficesView:
            type = 7
        }
        
        let url = getUrl(urlStr: "", serverType: .nearByServicesUrl)
        
//        var lat = "16.81668401110357"
//        var long = "96.13187085779862"
        
        self.currentLatitude = lat
        self.currentLogtitude = long
        
        let jsonDic:[String : Any]
        
        if checkurl ==  "NearBy" {
            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 300, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":[7,2,8,9,11], "phoneNumberNotToConsider":""] as [String : Any]
            
        } else if checkurl ==  "Township"{
            
            if type == 3 {
                 jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1000, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":selectedTownshipDetailStr,"cityName":"","businessName":"","country":"","divisionName":selectedLocationDetailStr,"line1":""],"paginationParams": ["offset":0,"limit":0],"types":[7,2,8,9,11], "phoneNumberNotToConsider":""] as [String : Any]
            } else {
                jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1000, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":selectedTownshipDetailStr,"cityName":"","businessName":"","country":"","divisionName":selectedLocationDetailStr,"line1":""],"paginationParams": ["offset":0,"limit":0],"types":[7,2,8,9,11], "phoneNumberNotToConsider":""] as [String : Any]
            }
            
        } else {
            
            jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 1000, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":0],"types":[7,2,8,9,11], "phoneNumberNotToConsider":""] as [String : Any]
        }
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url,methodType: kMethod_Post, contentType: kContentType_Json, inputVal: jsonDic, authStr: nil)
        
        print("Url Request--------\(jsonDic)")
        print("Url Request11--------\(urlRequest)")
        
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                }
        
                return
            }
            
            progressViewObj.removeProgressView()
            
            var cashArray = [NearByServicesNewModel]()
            self.cashInListRecentFromServer?.removeAll()
            
            DispatchQueue.main.async {
                
                //Tester raised a bug that search field should be empty if user search a data
                
                if !self.topSearchView.isHidden{
                    self.topSearchBar.text = ""
                }
                cashArray.removeAll()
                cashArray = ParserAttributeClass.parseNearbyServiceJSONNew(anyValue: response as AnyObject) as [NearByServicesNewModel]
                progressViewObj.removeProgressView()

                self.cashInListRecentFromServer = cashArray

                if cashArray.count > 0 {
                    
                    self.mapView.isHidden = false
                    
                    self.cashInListBackUPByCurLoc = cashArray  // this is too just save the list in backup
                    self.arrAllList.removeAll()
                    self.arrAgentsList.removeAll()
                    self.arrMerchantList.removeAll()
                    self.arrOfficeList.removeAll()
                    
                    //Seperate mechant =2,agent=8,office=7
                    for indexVal in 0..<cashArray.count {
                        let currentModel = cashArray[indexVal]
                        
                        
                        if let type = currentModel.UserContactData?.type {
                            
                            if UserModel.shared.mobileNo != currentModel.UserContactData?.PhoneNumber
                            {
                                switch type {
                                case 8 :
                                    self.arrAgentsList.append(currentModel)
                                case 2:
                                    self.arrMerchantList.append(currentModel)
                                case 7:
                                    self.arrOfficeList.append(currentModel)
                                default:
                                    self.arrAllList.append(currentModel)
                                    println_debug("others data")
                                }//finish type
                            }
                        }
                    }
                    
                    self.arrAllList.append(contentsOf: self.arrMerchantList)
                    self.arrAllList.append(contentsOf: self.arrAgentsList)
                    self.arrAllList.append(contentsOf: self.arrOfficeList)

                    let nc = NotificationCenter.default
                    
                    nc.post(name:Notification.Name(rawValue:"NearByAllDataNotification"),
                            object: nil,
                            userInfo: ["NearByData": self.arrAllList , "Latitude":lat , "Logtitude":long , "urlstring":self.checkurl])
                    nc.post(name:Notification.Name(rawValue:"NearByMerchantNotification"),
                            object: nil,
                            userInfo: ["NearByData": self.arrMerchantList , "Latitude":lat , "Logtitude":long , "urlstring":self.checkurl])
                    nc.post(name:Notification.Name(rawValue:"NearByAgentNotification"),
                            object: nil,
                            userInfo: ["NearByData": self.arrAgentsList  , "Latitude":lat , "Logtitude":long , "urlstring":self.checkurl])
                    nc.post(name:Notification.Name(rawValue:"NearByOfficeNotification"),
                            object: nil,
                            userInfo: ["NearByData": self.arrOfficeList , "Latitude":lat , "Logtitude":long , "urlstring":self.checkurl])
                    
                }
                else
                {
                    self.arrOfficeList.removeAll()
                    self.arrMerchantList.removeAll()
                    self.arrAgentsList.removeAll()
                    self.arrAllList.removeAll()
                    self.mapView.isHidden = true
                    self.showErrorAlert(errMessage: "No Record Found".localized)
                    self.reloadListEmptyViewAfterSwitch()

                }
            }
        })
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



extension NearByServicesMainVC: UISearchBarDelegate {
        
//    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//
//        didSelectSearchWithKey(key: searchBar.text!)
//
//        searchBar.resignFirstResponder()
//    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        didSelectSearchWithKey(key: searchBar.text!)
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            
            if updatedText.last == "\n"{
                print("Called")
                didSelectSearchWithKey(key: searchBar.text!)
                searchBar.resignFirstResponder()
                return true
            }else{
                if appDel.getSelectedLanguage() == "my" {
                            if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: SearchEnglishZwagi).inverted).joined(separator: "")) { return false }
                       } else if appDel.getSelectedLanguage() == "uni" {
                           if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: SearchEnglishUnicode).inverted).joined(separator: "")) { return false }
                       }else{
                           if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: SEARCH_CHAR_SET_En).inverted).joined(separator: "")) { return false }
                       }
                
                if updatedText.count > 25 {
                    return false
                }
                if updatedText == " " || updatedText == "  "{
                    return false
                }
                if (updatedText.contains("  ")){
                    return false
                }
                
                let containsEmoji: Bool = updatedText.containsEmoji
                if (containsEmoji){
                    return false
                }
                
                if updatedText != "" && text != "\n" {
                    didSelectSearchWithKey(key: updatedText)
                }
            }
        }
        return true
    }
    
    
    func didSelectSearchWithKey(key: String) {
        
        var arrTempList = [NearByServicesNewModel]()
        
        switch currentNearbyView {
        case .okAllView:
            arrTempList = arrAllList
        case .merchantView:
            arrTempList = arrMerchantList
        case .okAgentsView:
            arrTempList = arrAgentsList
        case .okOfficesView:
            arrTempList = arrOfficeList
        }
        
        if key.count > 0 {
            
            
//            for i in 0..<arrTempList.count{
//                print(arrTempList[i].UserContactData?.PhoneNumber ?? "***")
//            }
            
            
            
            if key.isNumeric{
                let filteredArray = arrTempList.filter { ($0.UserContactData?.PhoneNumber)?.range(of: key, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
                reloadTableData(arrayList: filteredArray)
                
            }else{
            
                let nameArray  = arrTempList.filter { ($0.UserContactData?.FirstName)?.range(of: key, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
                let filteredArray = arrTempList.filter { ($0.UserContactData?.BusinessName)?.range(of: key, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
                
                
                
                
                if((filteredArray.count) > 0) {
                    if nameArray.count > filteredArray.count{
                        reloadTableData(arrayList: nameArray)
                    }else{
                        reloadTableData(arrayList: filteredArray)
                    }
                    
                }
                else
                {
                    let filteredArray = arrTempList.filter { ($0.UserContactData?.FirstName)?.range(of: key, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
                    
                    reloadTableData(arrayList: filteredArray)
                }
                
                
            }
            
            
            
        } else {
            println_debug("Text field is cleared")
            reloadListViewAfterSwitch()
        }
    }
    
    func reloadTableData(arrayList : [NearByServicesNewModel])
    {
        let nc = NotificationCenter.default
       // print("currentview--\(currentNearbyView)")
        
        switch currentNearbyView {
        case .okAllView:
            nc.post(name:Notification.Name(rawValue:"NearByAllDataNotification"),
                    object: nil,
                    userInfo: ["NearByData": arrayList , "Latitude":currentLatitude , "Logtitude":currentLogtitude , "urlstring":self.checkurl])
        case .merchantView:
            nc.post(name:Notification.Name(rawValue:"NearByMerchantNotification"),
                    object: nil,
                    userInfo: ["NearByData": arrayList , "Latitude":currentLatitude , "Logtitude":currentLogtitude , "urlstring":self.checkurl])
        case .okAgentsView:
            nc.post(name:Notification.Name(rawValue:"NearByAgentNotification"),
                    object: nil,
                    userInfo: ["NearByData": arrayList , "Latitude":currentLatitude , "Logtitude":currentLogtitude , "urlstring":self.checkurl])
        case .okOfficesView:
            nc.post(name:Notification.Name(rawValue:"NearByOfficeNotification"),
                    object: nil,
                    userInfo: ["NearByData": arrayList , "Latitude":currentLatitude , "Logtitude":currentLogtitude , "urlstring":self.checkurl])
            
        }
    }
    
    func reloadListViewAfterSwitch()
    {
        let nc = NotificationCenter.default
        
        

        switch currentNearbyView {
        case .okAllView:
            nc.post(name:Notification.Name(rawValue:"NearByAllDataNotification"),
                    object: nil,
                    userInfo: ["NearByData": self.arrAllList , "Latitude":currentLatitude , "Logtitude":currentLogtitude , "urlstring":self.checkurl])
        case .merchantView:
            nc.post(name:Notification.Name(rawValue:"NearByMerchantNotification"),
                    object: nil,
                    userInfo: ["NearByData": self.arrMerchantList , "Latitude":currentLatitude , "Logtitude":currentLogtitude , "urlstring":self.checkurl])
        case .okAgentsView:
            nc.post(name:Notification.Name(rawValue:"NearByAgentNotification"),
                    object: nil,
                    userInfo: ["NearByData": self.arrAgentsList , "Latitude":currentLatitude , "Logtitude":currentLogtitude , "urlstring":self.checkurl])
        case .okOfficesView:
            nc.post(name:Notification.Name(rawValue:"NearByOfficeNotification"),
                    object: nil,
                    userInfo: ["NearByData": self.arrOfficeList , "Latitude":currentLatitude , "Logtitude":currentLogtitude , "urlstring":self.checkurl])
            
        }
       
    }
    
   
    
    func reloadListEmptyViewAfterSwitch()
    {
        let nc = NotificationCenter.default
        
        switch currentNearbyView {
        case .okAllView:
            nc.post(name:Notification.Name(rawValue:"NearByAllDataNotification"),
                    object: nil,
                    userInfo: ["NearByData": arrEmptyList])
        case .merchantView:
            nc.post(name:Notification.Name(rawValue:"NearByMerchantNotification"),
                    object: nil,
                    userInfo: ["NearByData": arrEmptyList])
        case .okAgentsView:
            nc.post(name:Notification.Name(rawValue:"NearByAgentNotification"),
                    object: nil,
                    userInfo: ["NearByData": self.arrEmptyList])
        case .okOfficesView:
            nc.post(name:Notification.Name(rawValue:"NearByOfficeNotification"),
                    object: nil,
                    userInfo: ["NearByData": self.arrEmptyList])
            
        }
        
    }
}

extension NearByServicesMainVC: CategoriesSelectionDelegate {

    func didSelectLocationByCategory(category: SubCategoryDetail?) {
        
        if category != nil {
            
        var mainCategoryName = ""
        var subCategoryName = ""
        
        if let categ = category {
            mainCategoryName = categ.mainCategoryName
            subCategoryName = categ.subCategoryName
            
            headerLabel.text = "\(categ.mainCategoryName) , \(categ.subCategoryName)     "
            
            defaults.set(categ.mainCategoryName + "," + categ.subCategoryName, forKey: "NBScategoryBy")
        }
        
        var arrTempList = [NearByServicesNewModel]()
        
        switch currentNearbyView {
        case .okAllView:
            arrTempList = arrAllList
        case .merchantView:
            arrTempList = arrMerchantList
        case .okAgentsView:
            arrTempList = arrAgentsList
        case .okOfficesView:
            arrTempList = arrOfficeList
        }
                
        let filteredArray = arrTempList.filter { ($0.UserContactData?.BusinessName)?.range(of: mainCategoryName, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
        
        if((filteredArray.count) > 0)
        {
            let subCatArray = arrTempList.filter { ($0.UserContactData?.FirstName)?.range(of: subCategoryName, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
            if((subCatArray.count) > 0)
            {

                self.mapView.isHidden = false
                reloadTableData(arrayList : subCatArray)
            }
            else
            {
                self.showErrorAlert(errMessage: "No Records Found".localized)

                self.mapView.isHidden = true
                reloadListEmptyViewAfterSwitch()

            }
        }
        else
        {
            self.showErrorAlert(errMessage: "No Records Found".localized)

            self.mapView.isHidden = true
            reloadListEmptyViewAfterSwitch()

        }
    } else {
            
            defaults.set("Default", forKey: "NBScategoryBy")
            
            self.mapView.isHidden = false
            reloadListViewAfterSwitch()

    }
}

}

extension NearByServicesMainVC: MainFilterApplyViewDelegate {

    func didSelectApplyFilter(category: SubCategoryDetail? , index:Int , str:Int){
        
        //print("Nearbyservice===\(category)---\(index)")
        
        self.menusortedindex = index
        
        if category != nil {
            
            var mainCategoryName = ""
            var subCategoryName = ""
            
            if let categ = category {
                mainCategoryName = categ.mainCategoryName
                subCategoryName = categ.subCategoryName
                
                headerLabel.text = "\(categ.mainCategoryName) , \(categ.subCategoryName)     "
                
                defaults.set(categ.mainCategoryName + "," + categ.subCategoryName, forKey: "NBScategoryBy")
            }
            
            
            var arrTempList = [NearByServicesNewModel]()
            
            
            switch currentNearbyView {
            case .okAllView:
                arrTempList = arrAllList
            case .merchantView:
                arrTempList = arrMerchantList
            case .okAgentsView:
                arrTempList = arrAgentsList
            case .okOfficesView:
                arrTempList = arrOfficeList
            }
            
            let filteredArray = arrTempList.filter { ($0.UserContactData?.BusinessName)?.range(of: mainCategoryName, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
            
            if((filteredArray.count) > 0)
            {
                var subCatArray = filteredArray.filter { ($0.UserContactData?.BusinessName)?.range(of: subCategoryName, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
                
                if((subCatArray.count) > 0)
                {
                    
                    if index == 1 {
                        
                         subCatArray = subCatArray.sorted(by: {$0.UserContactData!.BusinessName! < $1.UserContactData!.BusinessName!})
                        
                    } else if index == 2{
                        
                        subCatArray = subCatArray.sorted(by: {$1.UserContactData!.BusinessName! < $0.UserContactData!.BusinessName!})
                        
                    }
                    
                    self.mapView.isHidden = false
                    reloadTableData(arrayList : subCatArray)
                }
                else
                {
                    
                  
                    self.showErrorAlert(errMessage: "No Records Found".localized)
                    
                    self.mapView.isHidden = true
                    reloadListEmptyViewAfterSwitch()
                    
                    
                }
            }
            else
            {
                self.showErrorAlert(errMessage: "No Records Found".localized)
                
            
                self.mapView.isHidden = true
                reloadListEmptyViewAfterSwitch()
                
            }
        } else {
            
            defaults.set("Default", forKey: "NBScategoryBy")
            
            var arrTempList = [NearByServicesNewModel]()
            
            
            switch currentNearbyView {
            case .okAllView:
                arrTempList = arrAllList
            case .merchantView:
                arrTempList = arrMerchantList
            case .okAgentsView:
                arrTempList = arrAgentsList
            case .okOfficesView:
                arrTempList = arrOfficeList
            }

            if menusortedindex > 0  {
                
                if menusortedindex == 1 {
                    
                    var name1 : String = ""
                    var name2 : String = ""
                 
                    arrTempList = arrTempList.sorted {
                        
                        if $0.UserContactData!.BusinessName != "" {
                            name1 = $0.UserContactData!.BusinessName!
                        } else {
                            name1 = $0.UserContactData!.FirstName!
                        }
                        
                        if $1.UserContactData!.BusinessName != "" {
                            name2 = $1.UserContactData!.BusinessName!
                        } else {
                            name2 = $1.UserContactData!.FirstName!
                        }
                        
                        return name1.compare(name2) == .orderedAscending
                    }
                    
                } else if menusortedindex == 2{
                    
                    var name1 : String = ""
                    var name2 : String = ""
                    
                    arrTempList = arrTempList.sorted {
                        
                        if $0.UserContactData!.BusinessName != "" {
                            name1 = $0.UserContactData!.BusinessName!
                        } else {
                            name1 = $0.UserContactData!.FirstName!
                        }
                        
                        if $1.UserContactData!.BusinessName != "" {
                            name2 = $1.UserContactData!.BusinessName!
                        } else {
                            name2 = $1.UserContactData!.FirstName!
                        }
                        return name1.compare(name2) == .orderedDescending
                    }
                    
                  /*  sortTempList = arrTempList.sorted(by: {$0.UserContactData!.BusinessName! > $1.UserContactData!.BusinessName!})
                    let promotion = sortTempList[0]
                    print("sortTempList ALL Data name called-------\(promotion.UserContactData?.BusinessName)")
                    reloadTableData(arrayList : sortTempList) */
                    
                }
                reloadTableData(arrayList : arrTempList)
                
            } else {
            self.mapView.isHidden = false
            reloadListViewAfterSwitch()
            }
            
        }
        
        
    }

}


