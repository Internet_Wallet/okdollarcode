//
//  SortViewController.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/28/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit

protocol NearBySortViewDelegate: class {
    func didSelectSortOption(sortedList: [Any], selectedSortOption: Int)
}

protocol PTMSortandFilterViewDelegate: class {
    func PTMdidSelectSortOption(selectedSortOption: Int)
    func PTMdidSelectFilterOption(filteredList: [Any], selectedFilter: Any?)
}

class NearBySortViewController: UIViewController {

    @IBOutlet weak var headerLabel: UILabel!
    {
        didSet
        {
            headerLabel.font = UIFont(name: appFont, size: appFontSize)
            headerLabel.text = headerLabel.text?.localized
        }
    }
    @IBOutlet weak var sortTable: UITableView!
    @IBOutlet weak var resigningView: UIView!
    
    let sortList = ["Default".localized,"Amount High to Low".localized,"Amount Low to High".localized,"Name A to Z".localized,"Name Z to A".localized]
    weak var delegate: NearBySortViewDelegate?
    var previousSelectedSortOption: Int?
    var dataList: [Any]?
    var currentNearByView: NearByView?

    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadUI()
        loadInitialize()
        updateLocalizations()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sortTable.reloadData()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func loadUI() {
        headerHeightConstraint.constant = kHeaderHeight
        self.view.backgroundColor = kGradientGreyColor
        resigningView.backgroundColor = UIColor.clear
    }
    
    func loadInitialize() {
        sortTable.register(UINib(nibName: "SortCell", bundle: nil), forCellReuseIdentifier: "sortcellidentifier")
        sortTable.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    func updateLocalizations() {
        headerLabel.font = UIFont.init(name: appFont, size: 17)
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if touch.view == self.resigningView {
                resignTheSortView()
            }
        }
        super.touchesBegan(touches, with: event)
    }

    func resignTheSortView() {
        if let presentingView = self.presentingViewController {
            presentingView.dismiss(animated: true, completion: nil)
        }
    }
    
//    @objc func sortPromotions (array:[PromotionsModel], sortKey:String) -> [PromotionsModel] {
//        let sortValue: (PromotionsModel) -> String? = {
//            $0.value(forKey: sortKey) as? String
//        }
//        return array.sort {
//            sortValue($0.0)! < sortValue($0.1)!
//        }
//    }
}

extension NearBySortViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sortCell = tableView.dequeueReusableCell(withIdentifier: "sortcellidentifier", for: indexPath) as! SortCell
        var imageName = ""
        if previousSelectedSortOption == indexPath.row {
            // show the image
            imageName = "success"
        }
        
        sortCell.wrap_Data(title: sortList[indexPath.row], imgName: imageName)
        return sortCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            println_debug("default case falling")
            self.delegate?.didSelectSortOption(sortedList: dataList!, selectedSortOption:indexPath.row)

        case 1:
            println_debug("Amount High to Low")
                    if let list = dataList as? [NearByServicesModel] {
                        dataList = list.sorted(by: {($0.okDollarBalance!) > ($1.okDollarBalance!)})
                        //dataList = list.sorted(by: {($0.okDollarBalance.safelyWrappingString() as NSString).floatValue > ($1.okDollarBalance.safelyWrappingString() as NSString).floatValue})
                        self.delegate?.didSelectSortOption(sortedList: dataList!, selectedSortOption:indexPath.row)
                    }
            
        case 2:
            println_debug("Amount Low to High")
                    if let list = dataList as? [NearByServicesModel] {
                        dataList = list.sorted(by: {($0.okDollarBalance!) < ($1.okDollarBalance!)})
                        //dataList = list.sorted(by: {($0.okDollarBalance.safelyWrappingString() as NSString).floatValue < ($1.okDollarBalance.safelyWrappingString() as NSString).floatValue})
                        self.delegate?.didSelectSortOption(sortedList: dataList!, selectedSortOption:indexPath.row)
                    }
            
        case 3:
            println_debug("Name A to Z")
                if let list = dataList as? [NearByServicesModel] {
                    dataList = list.sorted(by: {$0.businessName! < $1.businessName!})
                    self.delegate?.didSelectSortOption(sortedList: dataList!, selectedSortOption:indexPath.row)
            }
           
        case 4:
            println_debug("Name Z to A")
                if let list = dataList as? [NearByServicesModel] {
                    dataList = list.sorted(by: {$0.businessName! > $1.businessName!})
                    self.delegate?.didSelectSortOption(sortedList: dataList!,selectedSortOption: indexPath.row)
                
            }
            
        default:
            break
        }
        resignTheSortView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

