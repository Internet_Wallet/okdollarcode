//
//  LDCHelperFile.swift
//  OK
//
//  Created by Mohit on 3/9/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class LDCHelperFile {
    static let aKey = "UGWxJtNKA060BpK7pHNVtA=="
    static let sKey = "eJYbQO0d1Uge7iWGC3JyQbNdHDw0EuKcTDdZblZzZ0g4R1VLc0lwN0o1UTkycmRIV2NQTzhxRWFMMFdRVlBDZXpyakE2cWhJeFI5Y05MWGRkUVIzZFNKUkIzODF3WW00M3ZLS0hkS3A1R2Jxcm1OQWQyeEU3SkJnaVM5RHNwV1lwRWliQU1WQUdsOFgxY2tacThYM2xoNnR1bmx6ZEg3UHRDQm1EdjlSZ3c4V0xucEdzald0UmN4SEl6cldsZ2c4bnNueWE3VzU4R1d5amtpRzhlSDR2TjdSTzRkT0VtcEFQOXNjQTBwdU5idktGOWhpUWtZR1ltTlp1V2tnZEkwV2JXamsxR0dIeldyVWN3TFBXNThlTmlsbmU5NTJmMmE3LWQxNTMtNDg5MC1iNDY3LWUwZjNmYzRlNWY3OA=="
    static let okAccountNum = "00959979933050"
    struct LDUrl {
        static let tokenProdUrl = "https://luckydrawapi.okdollar.org/token" //Post
        static let tokenTestUrl = "http://test.luckydraw.api.okdollar.org/token" //Post
        
        static let barCodeProdUrl = "https://luckydrawapi.okdollar.org/checkproduct/barcode/%@"
        static let barCodeTestUrl = "http://test.luckydraw.api.okdollar.org/checkproduct/barcode/%@"
        
        static let livePromBProdUrl = "https://luckydrawapi.okdollar.org/livePromotions/true"
        static let livePromBTestUrl = "http://test.luckydraw.api.okdollar.org/livePromotions/true"
        
        static let livePromWBProdUrl = "https://luckydrawapi.okdollar.org/livePromotionProductsWithoutBarcode" //Get
        static let livePromWBTestUrl = "http://test.luckydraw.api.okdollar.org/livePromotionProductsWithoutBarcode"
        
        static let checkProductProdUrl = "https://luckydrawapi.okdollar.org/checkproduct/product/%@"
        static let checkProductTestUrl = "http://test.luckydraw.api.okdollar.org/checkproduct/product/%@"
        
        static let updateUserProdUrl = "https://luckydrawapi.okdollar.org/UserService/UpdateUser"
        static let updateUserTestUrl = "http://test.luckydraw.api.okdollar.org/UserService/UpdateUser"
        
        static let checkPriceProdUrl = "https://luckydrawapi.okdollar.org/checkprize/%@/luckyNumber/%@/isLive/true"
        static let checkPriceTestUrl = "http://test.luckydraw.api.okdollar.org/checkprize/%@/luckyNumber/%@/isLive/true"
        
        static let verifyProdUrl = "https://luckydrawapi.okdollar.org/verify/%@/luckyNumber/%@/isLive/true"
        static let verifyTestUrl = "http://test.luckydraw.api.okdollar.org/verify/%@/luckyNumber/%@/isLive/true"
        
        static let GetLuckyDrawAndSeriesProdUrl = "http://69.160.4.151:8001/RestService.svc/GetLuckyDrawCategoryAndSeries"
        static let GetLuckyDrawAndSeriesTestUrl = "http://69.160.4.151:8001/RestService.svc/GetLuckyDrawCategoryAndSeries"
        
        static let ValidateLuckyNumberProdUrl = "http://69.160.4.151:8001/RestService.svc/ValidateLuckyNumber"
        static let ValidateLuckyNumberTestUrl = "http://69.160.4.151:8001/RestService.svc/ValidateLuckyNumber"
        
        static let verifyCouponProdUrl = "https://www.okdollar.co/RestService.svc/ValidateLuckyNumber"
        static let verifyCouponTestUrl = "http://69.160.4.151:8001/RestService.svc/ValidateLuckyNumber"
    }
    
    #if DEBUG
    static let kTokenAPIURL = serverUrl == .productionUrl ? LDUrl.tokenProdUrl : LDUrl.tokenTestUrl
    static let KbarCodeAPIURL = serverUrl == .productionUrl ? LDUrl.barCodeProdUrl : LDUrl.barCodeTestUrl
    static let kLLBLivePromotionAPIURL = serverUrl == .productionUrl ? LDUrl.livePromWBProdUrl : LDUrl.livePromWBTestUrl
    static let kLLBLivePromotion = serverUrl == .productionUrl ? LDUrl.livePromBProdUrl : LDUrl.livePromBTestUrl
    static let kCheckProductAPIURL = serverUrl == .productionUrl ? LDUrl.checkProductProdUrl : LDUrl.checkProductTestUrl
    static let kUpdateUserAPIURL = serverUrl == .productionUrl ? LDUrl.updateUserProdUrl : LDUrl.updateUserTestUrl
    static let kCheckPriceAPIURL = serverUrl == .productionUrl ? LDUrl.checkPriceProdUrl : LDUrl.checkPriceTestUrl
    static let kVerifyAPIURL = serverUrl == .productionUrl ? LDUrl.verifyProdUrl: LDUrl.verifyTestUrl
    static let kVerifyCoupon = serverUrl == .productionUrl ? LDUrl.verifyCouponTestUrl : LDUrl.verifyCouponTestUrl
    #else
    static let kTokenAPIURL = LDUrl.tokenProdUrl
    static let KbarCodeAPIURL = LDUrl.barCodeProdUrl
    static let kLLBLivePromotionAPIURL = LDUrl.livePromWBProdUrl
    static let kLLBLivePromotion = LDUrl.livePromBProdUrl
    static let kCheckProductAPIURL = LDUrl.checkProductProdUrl
    static let kUpdateUserAPIURL = LDUrl.updateUserProdUrl
    static let kCheckPriceAPIURL = LDUrl.checkPriceProdUrl
    static let kVerifyAPIURL = LDUrl.verifyProdUrl
    static let kVerifyCoupon = LDUrl.verifyCouponProdUrl
    #endif
    
    static let kContentType_Json = "application/json"
    static let kContentType_urlencoded = "application/x-www-form-urlencoded"
    static let kMethod_Get = "GET"
    static let kMethod_Post = "POST"
    static let kMethod_Put = "PUT"
    static let yellowAppColor = UIColor.init(hex: "FDB813")
    
    struct FileNames{
        static let LDCStoryBoardObj = UIStoryboard.init(name: "LuckyDrawCoupan", bundle: nil)
        
    }
    struct ErrorMessages {
        static let GenericAlertTitle = "Woops! Something Went Wrong!".localized
        static let TryAgainErrorMsg = "Please try again Later.".localized
        static let urlError = "URL request Error. (Error Code - LLB01)".localized
        static let NetWorkUnavailable = "Please connect to a different Network and Try again.".localized
        static let PaidErrorMessage = "You have Already Paid Landline Bill for the current Month.".localized
        static let DueDateErrorMessage = "Your payment due date is already over and even if you make payment now, your phone line may be disconnected by your operator! \n After your payment, Do not forget to inform your Operator".localized
        static let PayForViewErrorMessage = "Sorry you have already generated and Viewed your landline bill many times. In order to view the bill details again you have to pay %@ MMK initially from your landline bill.".localized
    }
}
