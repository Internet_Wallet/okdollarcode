//
//  LuckydrawModel.swift
//  OK
//
//  Created by E J ANTONY on 3/12/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation

protocol LuckyDrawModelDelegate: class {
    func apiResult(message: String?, statusCode: String?, apiResultFor: String)
    func handleCustomError(error: CustomError)
}

class LuckyDrawModel {
    weak var luckyDrawModelDelegate: LuckyDrawModelDelegate?
    var scanResult: ScanResult?
    var validationCouponResult: ValidateCouponSuccess?
    
    enum ScreenName: String {
        case generateToken = "GenerateToken", scanBarCode = "ScanBarCode", verifyCoupon = "VerifyCoupon"
    }
    
    func generateToken(aKey: String, sKey: String) {
        if appDelegate.checkNetworkAvail() {
            let hashValue = aKey.hmac_SHA1(key: sKey)
            let inputValue = "password=\(hashValue)&grant_type=password"
            let inputInData = inputValue.data(using: .ascii, allowLossyConversion: true)
            let urlStr   = String.init(format: LDCHelperFile.kTokenAPIURL)
            guard let tokenUrl = URL(string: urlStr) else { return }
            let web      = WebApiClass()
            web.delegate = self
            let params = [String: String]()
            PTLoader.shared.show()
            web.genericClass(url: tokenUrl, param: params as AnyObject, httpMethod: "POST", mScreen: ScreenName.generateToken.rawValue, hbData: inputInData, isUrlEncoded: true) //LDCHelperFile.kContentType_urlencoded
        } else {
            luckyDrawModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    private func handleTokenResponse(data: AnyObject) {
        guard let castedData = data as? Data else {
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(TokenResponse.self, from: castedData)
            guard let expireTime = response.expiresIn, expireTime > 0 else { return }
            guard let accessToken = response.accessToken, let tokenType = response.tokenType else { return }
            let authorizationString = "\(tokenType) \(accessToken)"
            luckyDrawModelDelegate?.apiResult(message: authorizationString, statusCode: nil, apiResultFor: ScreenName.generateToken.rawValue)
        } catch let error {
            println_debug(error)
            luckyDrawModelDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
        }
    }
    
    func scanBarCode(authToken: String, barCode: String) {
        let pipeLineStr = "Longitude=0.00|Latitude=0.00|PhoneNumber=\(UserModel.shared.mobileNo)|Name=\(UserModel.shared.name)"
        let hashValue = pipeLineStr.hmac_SHA1(key: LDCHelperFile.sKey)
        
        let keys = ["Longitude","Latitude","PhoneNumber","Name","HashValue"]
        let values = ["0.00","0.00",UserModel.shared.mobileNo,UserModel.shared.name,hashValue]
        
        let jsonObj = JSONStringWriter()
        let jsonText = jsonObj.writeJsonIntoString(keys, values)
        let urlStr = String.init(format: LDCHelperFile.KbarCodeAPIURL, barCode)
        guard let barCodeURL = URL.init(string: urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "") else { return }
        let web      = WebApiClass()
        web.delegate = self
        let params = [String: String]()
        PTLoader.shared.show()
        let data = jsonText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion: true)
        web.genericClass(url: barCodeURL, param: params as AnyObject, httpMethod: "POST", mScreen: ScreenName.scanBarCode.rawValue, hbData: data, authToken: authToken)
    }
    
    private func handleScanBarCodeResponse(data: AnyObject) {
        guard let castedData = data as? Data else {
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let scanResponse = try decoder.decode(ScanResponseDTO.self, from: castedData)
            self.scanResult = scanResponse.result
            self.luckyDrawModelDelegate?.apiResult(message: scanResponse.message ?? "",
                                                   statusCode: String(scanResponse.statusCode ?? 0),
                                                   apiResultFor: ScreenName.scanBarCode.rawValue)
        } catch let error {
            println_debug(error)
            luckyDrawModelDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
        }
    }
    
    func validateLuckyNumber(couponReq: ValidateCouponRequest) {
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStr   = String.init(format: LDCHelperFile.kVerifyCoupon)
            guard let verifyCouponURL = URL(string: urlStr) else { return }
            let params = [String: String]()
            println_debug("REQUEST URL: \(urlStr)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(couponReq)
                web.genericClass(url: verifyCouponURL, param: params as AnyObject, httpMethod: "POST", mScreen: ScreenName.verifyCoupon.rawValue, hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            luckyDrawModelDelegate?.handleCustomError(error: CustomError.noInternet)
        }
    }
    
    private func handleLuckyNumberValidation(data: AnyObject) {
        guard let castedData = data as? Data else {
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(ValidateCouponResponse.self, from: castedData)
            if let responseStr = response.data {
                println_debug(responseStr)
                if responseStr != "null" {
                    if let code = response.code, code == 200 {
                        guard let strData = responseStr.data(using: .utf8) else { return }
                        let successResponse = try decoder.decode(ValidateCouponSuccess.self, from: strData)
                        validationCouponResult = successResponse
                        luckyDrawModelDelegate?.apiResult(message: response.msg, statusCode: String(response.code ?? 0), apiResultFor: ScreenName.verifyCoupon.rawValue)
                    } else {
                        luckyDrawModelDelegate?.apiResult(message: response.msg, statusCode: String(response.code ?? 0), apiResultFor: ScreenName.verifyCoupon.rawValue)
                    }
                } else {
                    println_debug("Null from server in lucky number validation in luckydraw model")
                }
            } else {
                PTLoader.shared.hide()
                println_debug("No specified key from server")
            }
        } catch let error {
            println_debug(error)
            luckyDrawModelDelegate?.handleCustomError(error: CustomError.jsonSerializeFailed)
        }
    }
}

extension LuckyDrawModel: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        PTLoader.shared.hide()
        guard let screenName = ScreenName(rawValue: screen) else { return }
        switch screenName {
        case .generateToken:
            handleTokenResponse(data: json)
        case .scanBarCode:
            handleScanBarCodeResponse(data: json)
        case .verifyCoupon:
            handleLuckyNumberValidation(data: json)
        }
    }
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        PTLoader.shared.hide()
        switch withErrorCode {
        case 404:
            guard let screenName = ScreenName(rawValue: screenName) else { return }
            switch screenName {
            case .generateToken:
                println_debug("generate token Api error in luckydraw model")
            case .scanBarCode:
                println_debug("scan bar code Api error in luckydraw model")
            case .verifyCoupon:
                println_debug("scan bar code Api error in luckydraw model")
            }
        default:
            println_debug("Error \(withErrorCode)")
        }
    }
}
