//
//  LuckydrawDTO.swift
//  OK
//
//  Created by E J ANTONY on 3/12/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation

struct ProductModel {
    
    var BarCode : String
    var CategoryId : String
    var CategoryName : String
    var CompanyId : String
    var IsDeleted : Bool
    var IsLuckydrawModule : Bool
    var Packaging : String
    var ProductId : String
    var ProductImage : String
    var ProductImageFileDtos : String
    var ProductName : String
    var endDate: String
    
    init(_BarCode : String, _CategoryId : String, _CategoryName: String, _CompanyId : String, _IsDeleted : Bool, _IsLuckydrawModule : Bool, _Packaging : String, _ProductId : String, _ProductImage : String,_ProductImageFileDtos : String,_ProductName : String,_endDate: String){
        
        BarCode = _BarCode
        CategoryId = _CategoryId
        CategoryName = _CategoryName
        CompanyId = _CompanyId
        IsDeleted = _IsDeleted
        IsLuckydrawModule = _IsLuckydrawModule
        Packaging = _Packaging
        ProductId = _ProductId
        ProductImage = _ProductImage
        ProductImageFileDtos = _ProductImageFileDtos
        ProductName = _ProductName
        endDate = _endDate
    }
}

struct AccountModel {
    var AccountId : String
    var Balance : Int
    var CreatedDate : String
    var IsEnabled : Bool
    var IsVerified : Bool
    var OkDollarAccountNumber : String
    var PromotionProductId : String
    
    init(_AccountId : String, _Balance : Int, _CreatedDate: String, _IsEnabled : Bool, _IsVerified : Bool,_OkDollarAccountNumber : String, _PromotionProductId : String){
        AccountId = _AccountId
        Balance = _Balance
        CreatedDate = _CreatedDate
        IsEnabled = _IsEnabled
        IsVerified = _IsVerified
        OkDollarAccountNumber = _OkDollarAccountNumber
        PromotionProductId = _PromotionProductId
    }
}

struct ProductDetailModel {
    
    var PromotionPackageImage : String
    var Account : AccountModel?
    var BudgetAmount : NSNumber
    var CreatedDate : String
    var Duration : NSNumber
    var IsEnabled : NSNumber
    var PrizeSelectionType : String
    var Product : ProductModel?
    var ProductId : String
    var PromotionPackageId : String
    var PromotionProductId : String
    var PromotionProductName : String
    var ScanLimitType : NSNumber
    var TotalScanTime : NSNumber
    var companyName: String
    
    init(_PromotionPackageImage : String, _Account : AccountModel, _BudgetAmount : NSNumber, _CreatedDate: String, _Duration : NSNumber, _IsEnabled : NSNumber, _PrizeSelectionType : String, _Product : ProductModel, _ProductId : String, _PromotionPackageId : String, _PromotionProductId : String, _PromotionProductName : String, _ScanLimitType : NSNumber, _TotalScanTime : NSNumber, _companyName: String) {
        
        PromotionPackageImage = _PromotionPackageImage
        Account = _Account
        BudgetAmount = _BudgetAmount
        CreatedDate = _CreatedDate
        Duration = _Duration
        IsEnabled = _IsEnabled
        PrizeSelectionType = _PrizeSelectionType
        Product = _Product
        ProductId = _ProductId
        PromotionPackageId = _PromotionPackageId
        PromotionProductId = _PromotionProductId
        PromotionProductName = _PromotionProductName
        ScanLimitType = _ScanLimitType
        TotalScanTime = _TotalScanTime
        companyName = _companyName
    }
}

struct ProductDetailCompanyModel {
    var Product : ProductModel?
    var CompanyLogo : String
    var CompanyName : String
    var CompanyWebsiteUrl : String
    
    init(_Product : ProductModel, _CompanyLogo : String, _CompanyName : String, _CompanyWebsiteUrl : String){
        Product = _Product
        CompanyLogo = _CompanyLogo
        CompanyName = _CompanyName
        CompanyWebsiteUrl = _CompanyWebsiteUrl
    }
}

struct WheelDetailsModel {
    
    
    var WheelDetailId : String
    var PackageId : String
    var WheelDetailType : String
    var DisplayName : String
    var Amount : NSNumber
    var ParentWheelDetailId : String
    var CreatedDate : String
    var DisplayOrder : NSNumber
    var IsForWinning : NSNumber
    
    
    init(_WheelDetailId : String, _PackageId : String, _WheelDetailType : String, _DisplayName : String,_Amount : NSNumber, _ParentWheelDetailId : String, _CreatedDate : String, _DisplayOrder : NSNumber, _IsForWinning : NSNumber){
        
        WheelDetailId = _WheelDetailId
        PackageId = _PackageId
        WheelDetailType = _WheelDetailType
        DisplayName = _DisplayName
        Amount = _Amount
        ParentWheelDetailId = _ParentWheelDetailId
        CreatedDate = _CreatedDate
        DisplayOrder = _DisplayOrder
        IsForWinning = _IsForWinning
    }
}

struct WheelResultModel {
    var WheelSettingId : String
    var Color1 : String
    var Color2 : String
    var Color3 : String
    var Color4 : String
    var WinningColor : String
    var SpinSpeed : String
    var TextOrientation : String
    var TextSize : NSNumber
    var Duration : NSNumber
    var TextColor : String
    var PromotionPackageId : String
    var CreatedDate : String
    var WheelDetails : [WheelDetailsModel]
    
    init(_WheelSettingId : String, _Color1 : String, _Color2 : String, _Color3 : String,_Color4 : String, _WinningColor : String, _SpinSpeed : String, _TextOrientation : String, _TextSize : NSNumber, _Duration : NSNumber, _TextColor : String, _PromotionPackageId : String, _CreatedDate : String,_WheelDetails : [WheelDetailsModel]) {
        
        WheelSettingId = _WheelSettingId
        Color1 = _Color1
        Color2 = _Color2
        Color3 = _Color3
        Color4 = _Color4
        WinningColor = _WinningColor
        SpinSpeed = _SpinSpeed
        TextOrientation = _TextOrientation
        TextSize = _TextSize
        Duration = _Duration
        TextColor = _TextColor
        PromotionPackageId = _PromotionPackageId
        CreatedDate = _CreatedDate
        WheelDetails = _WheelDetails
    }
}

struct PrizeModel {
    var PrizeId : String
    var PrizeAmount : NSNumber
    var TotalPrize : NSNumber
    var ProfileId : String
    var IsDeleted : NSNumber
    var Priority : NSNumber
    var Claim : NSNumber
    var CreatedDate : String
    var IsAmountType : NSNumber
    var PrizeTypeId : String
    var PrizeCategoryId : String
    var PrizeServiceVariantId : String
    var PrizeType : PrizeType
    var PrizeCategory : PrizeCategory
    //    var PrizeServiceVariant : String
    
    init(_PrizeId: String, _PrizeAmount : NSNumber, _TotalPrize : NSNumber, _ProfileId : String,_IsDeleted : NSNumber, _Priority : NSNumber, _Claim : NSNumber, _CreatedDate : String, _IsAmountType : NSNumber, _PrizeTypeId : String, _PrizeCategoryId : String, _PrizeServiceVariantId : String, _PrizeType : PrizeType, _PrizeCategory : PrizeCategory) {
        PrizeId = _PrizeId
        PrizeAmount = _PrizeAmount
        TotalPrize = _TotalPrize
        ProfileId = _ProfileId
        IsDeleted = _IsDeleted
        Priority = _Priority
        Claim = _Claim
        CreatedDate = _CreatedDate
        IsAmountType = _IsAmountType
        PrizeTypeId = _PrizeTypeId
        PrizeCategoryId = _PrizeCategoryId
        PrizeServiceVariantId = _PrizeServiceVariantId
        PrizeType = _PrizeType
        PrizeCategory = _PrizeCategory
        //        PrizeServiceVariant = _PrizeServiceVariant
    }
}

struct PrizeType {
    var PrizeTypeId : String
    var PrizeTypeName : String
    init(_PrizeTypeId: String, _PrizeTypeName : String) {
        PrizeTypeId = _PrizeTypeId
        PrizeTypeName = _PrizeTypeName
    }
}

struct PrizeItemSetting {
    var PrizeItemSettingId : String
    var PrizeCategoryId : String
    var Validity : String
    var ValidityType : String
    var WhereToClaim : String
    var HowToClaim : String
    var Remark : String
    var IsNoticeByEmail : NSNumber
    var IsNoticeBySms : NSNumber
    var CreatedDate : String
    var EmailAddress : String
    var PhoneNumber : String
    
    init(_PrizeItemSettingId: String, _PrizeCategoryId : String,_Validity: String, _ValidityType : String,_WhereToClaim: String, _HowToClaim : String,_Remark: String, _IsNoticeByEmail : NSNumber,_IsNoticeBySms: NSNumber, _CreatedDate : String,_EmailAddress: String, _PhoneNumber : String) {
        PrizeItemSettingId = _PrizeItemSettingId
        PrizeCategoryId = _PrizeCategoryId
        Validity = _Validity
        ValidityType = _ValidityType
        WhereToClaim = _WhereToClaim
        HowToClaim = _HowToClaim
        Remark = _Remark
        IsNoticeByEmail = _IsNoticeByEmail
        IsNoticeBySms = _IsNoticeBySms
        CreatedDate = _CreatedDate
        EmailAddress = _EmailAddress
        PhoneNumber = _PhoneNumber
    }
}

struct PrizeCategory {
    
    var PrizeCategoryId : String
    var Name : String
    var PrizeTypeId : String
    var IsActive : String
    var CreatedDate : String
    var PrizeType : PrizeType
    var PrizeItemSetting : PrizeItemSetting
    //    var PrizeServiceSetting : String
    
    init(_PrizeCategoryId: String, _Name : String,_PrizeTypeId: String, _IsActive : String,_CreatedDate: String, _PrizeType : PrizeType,_PrizeItemSetting: PrizeItemSetting) {
        
        PrizeCategoryId = _PrizeCategoryId
        Name = _Name
        PrizeTypeId = _PrizeTypeId
        IsActive = _IsActive
        CreatedDate = _CreatedDate
        PrizeType = _PrizeType
        PrizeItemSetting = _PrizeItemSetting
        //        PrizeServiceSetting = _PrizeServiceSetting
        
    }
    
}

// MARK: - PromotionProductDTO
struct PromotionProductDTO: Codable {
    var statusCode: Int?
    var status: Bool?
    var message: String?
    var results: [PromotionProductResults]?
    private enum CodingKeys: String, CodingKey {
        case statusCode = "StatusCode"
        case status = "Status"
        case message = "Message"
        case results = "Results"
    }
}

struct PromotionProductResults: Codable {
    var promotionPackageName: String?
    var endDate: String?
    var isActive: Bool?
    var promotionPackageImage: String?
    var company: PromotionCompany?
    var promotionProducts: [PromotionProducts]?
    private enum CodingKeys: String, CodingKey {
        case promotionPackageName = "PromotionPackageName"
        case endDate = "EndDate"
        case isActive = "IsActive"
        case promotionPackageImage = "PromotionPackageImage"
        case company = "Company"
        case promotionProducts = "PromotionProducts"
    }
}

struct PromotionCompany: Codable {
    var companyName: String?
    var companyLogo: String?
    private enum CodingKeys: String, CodingKey {
        case companyName = "CompanyName"
        case companyLogo = "CompanyLogo"
    }
}

struct PromotionProducts: Codable {
    var promotionProductName: String?
    var product: PromotionProductDetail?
    private enum CodingKeys: String, CodingKey {
        case promotionProductName = "PromotionProductName"
        case product = "Product"
    }
}

struct PromotionProductDetail: Codable {
    var productId: String?
    var productName: String?
    var productImage: String?
    var packaging: String?
    var categoryName: String?
    private enum CodingKeys: String, CodingKey {
        case productId = "ProductId"
        case productName = "ProductName"
        case productImage = "ProductImage"
        case packaging = "Packaging"
        case categoryName = "CategoryName"
    }
}

// MARK: - SpinResultDTO
struct SpinResultDTO: Codable {
    var statusCode: Int?
    var status: Bool?
    var message: String?
    var result: SpinResult?
    private enum CodingKeys: String, CodingKey {
        case statusCode = "StatusCode"
        case status = "Status"
        case message = "Message"
        case result = "Result"
    }
}

struct SpinResult: Codable {
    var scanResultId: String?
    var scanTrackingId: String?
    var luckyNumber: String?
    var isWon: Bool?
    var createdDate: String?
    var oKTransactionId: String?
    var sourceNumber: String?
    var destinationNumber: String?
    var amount: Int?
    var profileId: String?
    var traditionId: String?
    var isProfileMatched: Bool?
    var prizeId: String?
    var prize: PrizeDetail?
    private enum CodingKeys: String, CodingKey {
        case scanResultId = "ScanResultId"
        case scanTrackingId = "ScanTrackingId"
        case luckyNumber = "LuckyNumber"
        case isWon = "IsWon"
        case createdDate = "CreatedDate"
        case oKTransactionId = "OkTransactionId"
        case sourceNumber = "SourceNumber"
        case destinationNumber = "DestinationNumber"
        case amount = "Amount"
        case profileId = "ProfileId"
        case traditionId = "TraditionId"
        case isProfileMatched = "IsProfileMatched"
        case prizeId = "PrizeId"
        case prize = "Prize"
    }
}

struct PrizeDetail: Codable {
    var prizeId: String?
    var prizeAmount: Int?
    var totalPrize: Int?
    var profileId: String?
    var isDeleted: Bool?
    var priority: Int?
    var claim: Int?
    var createdDate: String?
    var isAmountType: Bool?
    var prizeTypeId: String?
    var prizeCategoryId: String?
    var prizeServiceVariantId: String?
    var prizeCategory: String?
    var prizeServiceVariant: String?
    var prizeType: PrizeTypeDetail?
    private enum CodingKeys: String, CodingKey {
        case prizeId = "PrizeId"
        case prizeAmount = "PrizeAmount"
        case totalPrize = "TotalPrize"
        case profileId = "ProfileId"
        case isDeleted = "IsDeleted"
        case priority = "Priority"
        case claim = "Claim"
        case createdDate = "CreatedDate"
        case isAmountType = "IsAmountType"
        case prizeTypeId = "PrizeTypeId"
        case prizeCategoryId = "PrizeCategoryId"
        case prizeServiceVariantId = "PrizeServiceVariantId"
        case prizeCategory = "PrizeCategory"
        case prizeServiceVariant = "PrizeServiceVariant"
        case prizeType = "PrizeType"
    }
}

struct PrizeTypeDetail: Codable {
    var prizeTypeId: String?
    var prizeTypeName: String?
    private enum CodingKeys: String, CodingKey {
        case prizeTypeId = "PrizeTypeId"
        case prizeTypeName = "PrizeTypeName"
    }
}

// MARK: - ScanResponseDTO
struct ScanResponseDTO: Codable {
    var statusCode: Int?
    var status: Bool?
    var message: String?
    var result: ScanResult?
    private enum CodingKeys: String, CodingKey {
        case statusCode = "StatusCode"
        case status = "Status"
        case message = "Message"
        case result = "Result"
    }
}

struct ScanResult: Codable {
    var scanTrackingId: String?
    var name: String?
    var mobileNumber: String?
    var latitude: Double?
    var longtitude: Double?
    var scanDateTime: String?
    var townshipId: String?
    var divisionId: String?
    var promotionProductId: String?
    var gcmRegistrationId: String?
    var brandName: String?
    var deviceModelName: String?
    var dateOfBirth: String?
    var gender: String?
    var product: ScannedProductDetail?
    var promotionPackage: ScannedPromotionPackage?
    private enum CodingKeys: String, CodingKey {
        case scanTrackingId = "ScanTrackingId"
        case name = "Name"
        case mobileNumber = "MobileNumber"
        case latitude = "Latitude"
        case longtitude = "Longtitude"
        case scanDateTime = "ScaDateTime"
        case townshipId = "TownshipId"
        case divisionId = "DivisionId"
        case promotionProductId = "PromotionProductId"
        case gcmRegistrationId = "GcmRegistrationId"
        case brandName = "BrandName"
        case deviceModelName = "DeviceModelName"
        case dateOfBirth = "DateOfBirth"
        case gender = "Gender"
        case product = "Product"
        case promotionPackage = "PromotionPackage"
    }
}

struct ScannedProductDetail: Codable {
    var productId: String?
    var companyId: String?
    var productName: String?
    var productImage: String?
    var barCode: String?
    var isDeleted: Bool?
    var categoryId: String?
    var packaging: String?
    var categoryName: String?
    var productImageFileDtos: String?
    var isLuckydrawModule: Bool?
    private enum CodingKeys: String, CodingKey {
        case productId = "ProductId"
        case companyId = "CompanyId"
        case productName = "ProductName"
        case productImage = "ProductImage"
        case barCode = "BarCode"
        case isDeleted = "IsDeleted"
        case categoryId = "CategoryId"
        case packaging = "Packaging"
        case categoryName = "CategoryName"
        case productImageFileDtos = "ProductImageFileDtos"
        case isLuckydrawModule = "IsLuckydrawModule"
    }
}

struct ScannedPromotionPackage: Codable {
    var promotionPackageId: String?
    var promotionPackageName: String?
    var startDate: String?
    var endDate:String?
    var budgetAmount: Double?
    var companyId: String?
    var createdDate: String?
    var messageId: String?
    var isActive: Bool?
    var promotionPackageImage: String?
    var company: ScannedCompanyDetail?
    private enum CodingKeys: String, CodingKey {
        case promotionPackageId = "PromotionPackageId"
        case promotionPackageName = "PromotionPackageName"
        case startDate = "StartDate"
        case endDate = "EndDate"
        case budgetAmount = "BudgetAmount"
        case companyId = "CompanyId"
        case createdDate = "CreatedDate"
        case messageId = "MessageId"
        case isActive = "IsActive"
        case promotionPackageImage = "PromotionPackageImage"
        case company = "Company"
    }
}

struct ScannedCompanyDetail: Codable {
    var companyId: String?
    var contactName: String?
    var emailAddress: String?
    var phoneNumber: String?
    var companyName: String?
    var companyWebsiteUrl: String?
    var companyLogo: String?
    var chargesType: String?
    var chargesAmount: Double?
    var serviceEndDate: String?
    var addressId: String?
    var isDeleted: Bool?
    var address: ScannedProductAddress?
    private enum CodingKeys: String, CodingKey {
        case companyId = "CompanyId"
        case contactName = "ContactName"
        case emailAddress = "EmailAddress"
        case phoneNumber = "PhoneNumber"
        case companyName = "CompanyName"
        case companyWebsiteUrl = "CompanyWebsiteUrl"
        case companyLogo = "CompanyLogo"
        case chargesType = "ChargesType"
        case chargesAmount = "ChargesAmount"
        case serviceEndDate = "ServiceEndDate"
        case addressId = "AddressId"
        case isDeleted = "IsDeleted"
        case address = "Address"
    }
}

struct ScannedProductAddress: Codable {
    var addressId: String?
    var addressLine1: String?
    var addressLine2: String?
    var townshipId: String?
    var stateId: String?
    private enum CodingKeys: String, CodingKey {
        case addressId = "AddressId"
        case addressLine1 = "AddressLine1"
        case addressLine2 = "AddressLine2"
        case townshipId = "TownshipId"
        case stateId = "StateId"
    }
}

//MARK: - CheckProductResponseDTO
struct CheckProductResponseDTO: Codable {
    let statusCode: Int?
    let status: Bool?
    let message: String?
    let result: CheckProductResponse?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "StatusCode"
        case status = "Status"
        case message = "Message"
        case result = "Result"
    }
}

struct CheckProductResponse: Codable {
    let scanTrackingID, name, mobileNumber: String?
    let scanDateTime, promotionProductID: String?
    let dateOfBirth, gender: String?
    let product: CheckedProduct?
    let promotionPackage: CheckedPromotionPackage?
    
    enum CodingKeys: String, CodingKey {
        case scanTrackingID = "ScanTrackingId"
        case name = "Name"
        case mobileNumber = "MobileNumber"
        case scanDateTime = "ScaDateTime"
        case promotionProductID = "PromotionProductId"
        case dateOfBirth = "DateOfBirth"
        case gender = "Gender"
        case product = "Product"
        case promotionPackage = "PromotionPackage"
    }
}

struct CheckedProduct: Codable {
    let productID, companyID, productName, productImage: String?
    let barCode: String?
    let isDeleted: Bool?
    let categoryID, packaging, categoryName: String?
    
    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case companyID = "CompanyId"
        case productName = "ProductName"
        case productImage = "ProductImage"
        case barCode = "BarCode"
        case isDeleted = "IsDeleted"
        case categoryID = "CategoryId"
        case packaging = "Packaging"
        case categoryName = "CategoryName"
    }
}

struct CheckedPromotionPackage: Codable {
    let promotionPackageID, promotionPackageName, startDate, endDate: String?
    let budgetAmount: Double?
    let companyID: String?
    let isActive: Bool?
    let promotionPackageImage: String?
    let company: ScannedCompanyDetail?
    let promotionTermsAndConditions, promotionPackageSlogan, promotionPackageDescription: String
    
    enum CodingKeys: String, CodingKey {
        case promotionPackageID = "PromotionPackageId"
        case promotionPackageName = "PromotionPackageName"
        case startDate = "StartDate"
        case endDate = "EndDate"
        case budgetAmount = "BudgetAmount"
        case companyID = "CompanyId"
        case isActive = "IsActive"
        case promotionPackageImage = "PromotionPackageImage"
        case company = "Company"
        case promotionTermsAndConditions = "PromotionTermsAndConditions"
        case promotionPackageSlogan = "PromotionPackageSlogan"
        case promotionPackageDescription = "PromotionPackageDescription"
    }
}

//MARK: - CheckPrizeResponseDTO
struct CheckPrizeResponseDTO: Codable {
    let statusCode: Int
    let status: Bool
    let message: String?
    let result: CheckPrizeResponse?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "StatusCode"
        case status = "Status"
        case message = "Message"
        case result = "Result"
    }
}

struct CheckPrizeResponse: Codable {
    let wheelSettingID, color1, color2, color3: String
    let color4, winningColor, spinSpeed, textOrientation: String
    let textSize, duration: Int
    let textColor, promotionPackageID, createdDate: String
    let wheelDetails: [WheelDetail]
    
    enum CodingKeys: String, CodingKey {
        case wheelSettingID = "WheelSettingId"
        case color1 = "Color1"
        case color2 = "Color2"
        case color3 = "Color3"
        case color4 = "Color4"
        case winningColor = "WinningColor"
        case spinSpeed = "SpinSpeed"
        case textOrientation = "TextOrientation"
        case textSize = "TextSize"
        case duration = "Duration"
        case textColor = "TextColor"
        case promotionPackageID = "PromotionPackageId"
        case createdDate = "CreatedDate"
        case wheelDetails = "WheelDetails"
    }
}

struct WheelDetail: Codable {
    let wheelDetailID, packageID, wheelDetailType, displayName: String
    let amount: Int
    let parentWheelDetailID, createdDate: String
    let displayOrder: Int
    let isForWinning: Bool
    let wheelDetails: [WheelDetail]?
    
    enum CodingKeys: String, CodingKey {
        case wheelDetailID = "WheelDetailId"
        case packageID = "PackageId"
        case wheelDetailType = "WheelDetailType"
        case displayName = "DisplayName"
        case amount = "Amount"
        case parentWheelDetailID = "ParentWheelDetailId"
        case createdDate = "CreatedDate"
        case displayOrder = "DisplayOrder"
        case isForWinning = "IsForWinning"
        case wheelDetails = "WheelDetails"
    }
}

//MARK: - Token Response
struct TokenResponse: Codable {
    let accessToken: String?
    let tokenType: String?
    let expiresIn: Int?
    let error: String?
    
    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case tokenType = "token_type"
        case expiresIn = "expires_in"
        case error = "error"
    }
}

//MARK: - Validate Coupon Request
struct ValidateCouponRequest: Codable {
    let categroyName: String?
    let loginInfo: CouponLoginInfo?
    let luckyNumber, userBalance: String?
    
    enum CodingKeys: String, CodingKey {
        case categroyName = "CategroyName"
        case loginInfo = "LoginInfo"
        case luckyNumber = "LuckyNumber"
        case userBalance = "UserBalance"
    }
    
    init(categoryName: String? = "TEST", userBalance: String = "0", luckyNumber: String) {
        self.categroyName = categoryName
        self.loginInfo = CouponLoginInfo()
        self.luckyNumber = luckyNumber
        self.userBalance = userBalance
    }
}

struct CouponLoginInfo: Codable {
    let appID = UserModel.shared.appID
    let limit = 0
    let mobileNumber = UserModel.shared.mobileNo
    let msid = getMsid()
    let offset = 0
    let ostype = 1
    let otp: String? = nil
    let simid = uuid
    
    enum CodingKeys: String, CodingKey {
        case appID = "AppId"
        case limit = "Limit"
        case mobileNumber = "MobileNumber"
        case msid = "Msid"
        case offset = "Offset"
        case ostype = "Ostype"
        case otp = "Otp"
        case simid = "Simid"
    }
}

//MARK: - Validate Coupon Response
struct ValidateCouponResponse: Codable {
    let code: Int?
    let data, msg: String?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case msg = "Msg"
    }
}

//MARK: - Validate Coupon Success
struct ValidateCouponSuccess: Codable {
    let eCouponAmount: String?
    
    enum CodingKeys: String, CodingKey {
        case eCouponAmount = "EcouponAmount"
    }
}
