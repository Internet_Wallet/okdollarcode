//
//  SortListViewController.swift
//  OK
//
//  Created by E J ANTONY on 29/06/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class SortListViewController: UIViewController {
    //MARK: - Outlets
    @IBOutlet weak var buttonAscending: UIButton!{
        didSet{
            buttonAscending.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)

        }
    }
    @IBOutlet weak var buttonDescending: UIButton!{
        didSet{
            buttonDescending.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)

        }
    }
    
    //MARK: - Properties
    let lightColor = UIColor(red: 191.0/255.0, green: 229.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    let darkColor = UIColor(red: 87.0/255.0, green: 96.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    var parentView: LDListOptionsViewController?
    
    //MARK: - Views Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setButtonUI()
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(resetSort), name: Notification.Name("ResetFilterSort"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Target Methods
    @objc func resetSort() {
        setButtonUI()
    }
    
    //MRAK: - Methods
    func setButtonUI() {
        buttonAscending.tag = 0
        buttonDescending.tag = 0
        buttonAscending.layer.cornerRadius = (buttonAscending.frame.size.height / 2)
        buttonDescending.layer.cornerRadius = (buttonDescending.frame.size.height / 2)
        buttonAscending.layer.borderColor = lightColor.cgColor
        buttonAscending.layer.borderWidth = 1.0
        buttonDescending.layer.borderColor = lightColor.cgColor
        buttonDescending.layer.borderWidth = 1.0
        buttonAscending.setTitleColor(lightColor, for: .normal)
        buttonAscending.backgroundColor = darkColor
        buttonDescending.setTitleColor(lightColor, for: .normal)
        buttonDescending.backgroundColor = darkColor
    }
    
    func selectAsc(_ isSelect: Bool) {
        if isSelect {
            buttonAscending.setTitleColor(darkColor, for: .normal)
            buttonAscending.backgroundColor = lightColor
        } else {
            buttonAscending.setTitleColor(lightColor, for: .normal)
            buttonAscending.backgroundColor = darkColor
        }
        let choiceDict = ["sort": isSelect ? SortBy.aToZ : SortBy.none]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FilterSort"), object: nil, userInfo: choiceDict)
    }
    
    func selectDesc(_ isSelect: Bool) {
        if isSelect {
            buttonDescending.setTitleColor(darkColor, for: .normal)
            buttonDescending.backgroundColor = lightColor
        } else {
            buttonDescending.setTitleColor(lightColor, for: .normal)
            buttonDescending.backgroundColor = darkColor
        }
        let choiceDict = ["sort": isSelect ? SortBy.zToA : SortBy.none]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FilterSort"), object: nil, userInfo: choiceDict)
    }
    
    //MARK: - Button Action Methods
    @IBAction func sortByAsc(sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            buttonDescending.tag = 0
            selectDesc(false)
            selectAsc(true)
        } else {
            sender.tag = 0
            selectAsc(false)
        }
    }
    
    @IBAction func sortByDesc(sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            buttonAscending.tag = 0
            selectAsc(false)
            selectDesc(true)
        } else {
            sender.tag = 0
            selectDesc(false)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
