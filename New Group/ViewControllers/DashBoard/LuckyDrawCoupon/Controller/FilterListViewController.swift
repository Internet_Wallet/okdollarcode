//
//  FilterListViewController.swift
//  OK
//
//  Created by E J ANTONY on 28/06/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class FilterListViewController: UIViewController {
    //MARK: - Outlets
    @IBOutlet weak var btnProduct: UIButton!{
        didSet{
            btnProduct.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)

        }
    }
    
    //MARK: - Properties
    let lightColor = UIColor(red: 191.0/255.0, green: 229.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    let darkColor = UIColor(red: 87.0/255.0, green: 96.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    var parentView: LDListOptionsViewController?
    var hideFilter = false
    
    //MARK: - Views Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnProduct.setTitle("Product".localized, for: .normal)
        if let navFont = UIFont(name: appFont, size: 18) {
            self.btnProduct.titleLabel?.font = navFont
        }
        self.setButtonUI()
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(resetFilters), name: Notification.Name("ResetFilterSort"), object: nil)
        if hideFilter {
            self.btnProduct.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Target Methods
    @objc func resetFilters() {
        setButtonUI()
    }
    
    //MRAK: - Methods
    func setButtonUI() {
        btnProduct.tag = 0
        btnProduct.layer.cornerRadius = (btnProduct.frame.size.height / 2)
        btnProduct.layer.borderColor = lightColor.cgColor
        btnProduct.layer.borderWidth = 1.0
        btnProduct.setTitleColor(lightColor, for: .normal)
        btnProduct.backgroundColor = darkColor
    }
    
    //MARK: - Button Action Methods
    @IBAction func filterByProduct(sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            btnProduct.setTitleColor(darkColor, for: .normal)
            btnProduct.backgroundColor = lightColor
        } else {
            sender.tag = 0
            btnProduct.setTitleColor(lightColor, for: .normal)
            btnProduct.backgroundColor = darkColor
        }
        let choiceDict = ["filter": sender.tag == 1 ? FilterBy.product : FilterBy.none]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FilterSort"), object: nil, userInfo: choiceDict)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
