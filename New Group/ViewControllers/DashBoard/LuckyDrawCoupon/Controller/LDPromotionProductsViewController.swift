//
//  LDPromotionProductsViewController.swift
//  OK
//
//  Created by E J ANTONY on 14/06/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class LDPromotionProductsViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var imageViewProduct: UIImageView!
    @IBOutlet weak var labelProductTitle: UILabel!{
        didSet{
            labelProductTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var labelBurmeseTitle: UILabel!{
        didSet{
            labelBurmeseTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var labelDate: UILabel!{
        didSet{
            labelDate.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var labelProductCount: UILabel!{
        didSet{
            labelProductCount.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var tableViewProductList: UITableView!
    @IBOutlet weak var constraintImageViewH: NSLayoutConstraint!
    
    var productDetail: PromotionProductResults?
    var promotionProducts = [PromotionProducts]()
    
    //MARK: View controller life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpNavigation()
        getDataToDisplay()
        tableViewProductList.tableFooterView = UIView()
        tableViewProductList.rowHeight = UITableView.automaticDimension
        tableViewProductList.estimatedRowHeight = 160
        
        if Device.IS_IPHONE_5 {
            constraintImageViewH.constant = 150
            self.view.layoutIfNeeded()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Methods
    private func getDataToDisplay() {
        self.imageViewProduct.image = nil
        guard let safeProductDetail = productDetail else { return }
        if let promoPackImage = safeProductDetail.promotionPackageImage {
            let imageNameWithUrlEncoding = promoPackImage.replacingOccurrences(of: " ", with: "%20")
            if let url = URL(string: imageNameWithUrlEncoding) {
                downloadImage(url: url)
            }
        }
        labelProductTitle.text = safeProductDetail.company?.companyName ?? ""
        labelBurmeseTitle.text = safeProductDetail.promotionPackageName ?? ""
        labelProductCount.text = "Products List"
        if let myFont = UIFont(name: appFont, size: 18) {
            labelProductCount.font =  myFont
        }
        if let promoProds = safeProductDetail.promotionProducts {
            labelProductCount.text = "Number of Promotion Product(s)".localized + " - \(promoProds.count)"
        }
        labelDate.text = ""
        if let endDateStr = safeProductDetail.endDate {
            if let endDateVal = endDateStr.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss") {
                labelDate.text = endDateVal.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy")
            }
        }
        promotionProducts = safeProductDetail.promotionProducts ?? []
        tableViewProductList.reloadData()
    }
    
    private func downloadImage(url: URL) {
        getDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() {
                self.imageViewProduct.image = UIImage(data: data)
            }
        }
    }
    
    private func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
}

extension LDPromotionProductsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return promotionProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let promotionProductCell = tableView.dequeueReusableCell(withIdentifier: "PromotionProductTableViewCell", for: indexPath) as! PromotionProductTableViewCell
        let productItem = promotionProducts[indexPath.row]
        promotionProductCell.imageViewProduct.image = nil
        if let productImageStr = productItem.product?.productImage {
            let imageNameWithUrlEncoding = productImageStr.replacingOccurrences(of: " ", with: "%20")
            if let url = URL(string: imageNameWithUrlEncoding) {
                promotionProductCell.imageViewProduct.sd_setImage(with: url, placeholderImage: nil)
            }
        }
        promotionProductCell.labelProductName.text = productItem.product?.productName ?? ""
        promotionProductCell.labelProductCategory.text = productItem.product?.categoryName ?? ""
        promotionProductCell.labelProductPackaging.text = productItem.product?.packaging ?? ""
        return promotionProductCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 160
        return tableView.rowHeight
    }
}

extension LDPromotionProductsViewController: NavigationSetUpProtocol {
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "Lucky draw & Coupon Title".localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}
