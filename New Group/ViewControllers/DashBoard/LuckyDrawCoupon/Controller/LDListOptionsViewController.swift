//
//  LDListOptionsViewController.swift
//  OK
//
//  Created by E J ANTONY on 28/06/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
enum FilterBy {
    case product, none
}

enum SortBy {
    case aToZ, zToA, none
}

protocol ListOptionsProtocol {
    func optionsToPerform(filterBy: FilterBy, sortBy: SortBy)
}

class LDListOptionsViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var viewPage: UIView!
    @IBOutlet weak var constraintTopOptions: NSLayoutConstraint!
    @IBOutlet weak var viewTransparent: UIView!
    @IBOutlet weak var buttonFilter: UIButton!{
        didSet{
            buttonFilter.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)

        }
    }
    @IBOutlet weak var buttonSort: UIButton!{
        didSet{
            buttonSort.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var buttonReset: UIButton!{
        didSet{
            buttonReset.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var buttonSelect: UIButton!{
        didSet{
            buttonSelect.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var constraintLeadingSlider: NSLayoutConstraint!
    
    //MARK: - Properties
    var pageViewController: UIPageViewController?
    var pages: [UIViewController] = []
    var tapGest: UITapGestureRecognizer?
    var filterOptionVC: FilterListViewController?
    var sortOptionVC: SortListViewController?
    var filterBy = FilterBy.none
    var sortBy = SortBy.none
    var optionDelegate: ListOptionsProtocol?
    var hideFilter = false
    
    //MARK: - Views Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadPageViewController()
        constraintTopOptions.constant = UIScreen.main.bounds.size.height - 290
        self.view.layoutIfNeeded()
        self.buttonFilter.setTitle("FILTER".localized, for: .normal)
        self.buttonSort.setTitle("SORT".localized, for: .normal)
        self.buttonFilter.tag = 0
        self.buttonSort.tag = 1
        self.setGestures()
        self.addFilterSortObserver()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Target Methods
    @objc func actionOnTap(sender: UITapGestureRecognizer) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func updateTheFilterSortOptions(_ notification: NSNotification) {
        if let filterOption = notification.userInfo?["filter"] as? FilterBy {
            self.filterBy = filterOption
        }
        if let sortOption = notification.userInfo?["sort"] as? SortBy {
            self.sortBy = sortOption
        }
    }
    
    //MARK: - Methods
    func loadPageViewController() {
        guard let filterOptionVC = UIStoryboard(name: "LuckyDrawCoupan", bundle: nil).instantiateViewController(withIdentifier: "FilterListViewController") as? FilterListViewController else { return }
        guard let sortOptionVC = UIStoryboard(name: "LuckyDrawCoupan", bundle: nil).instantiateViewController(withIdentifier: "SortListViewController") as? SortListViewController else { return }
        pageViewController = UIPageViewController.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        guard let pageVC = pageViewController else { return }
        pageVC.delegate = self
        pageVC.dataSource = self
        pageVC.view.frame = viewPage.bounds
        pageVC.view.backgroundColor = UIColor.clear
        pages = [filterOptionVC, sortOptionVC]
        filterOptionVC.parentView = self
        filterOptionVC.hideFilter = self.hideFilter
        filterOptionVC.view.tag = 0
        sortOptionVC.parentView = self
        sortOptionVC.view.tag = 1
        pageVC.setViewControllers([pages[0]], direction: .forward, animated: true, completion: nil)
        addChild(pageVC)
        viewPage.addSubview(pageVC.view)
        pageVC.didMove(toParent: self)
    }
    
    func addFilterSortObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateTheFilterSortOptions(_:)), name: NSNotification.Name(rawValue: "FilterSort"), object: nil)
    }
    
    func moveSlider(to index: Int) {
        switch index {
        case buttonFilter.tag:
            updateConstraint(with: 0)
        case buttonSort.tag:
            updateConstraint(with: buttonSort.frame.minX - 5)
        default:
            break
        }
    }
    
    func updateConstraint(with value: CGFloat) {
        UIView.animate(withDuration: 0.2,
                       delay: 0,
                       options: .curveEaseIn,
                       animations: { () -> Void in
                        self.constraintLeadingSlider.constant = value
                        self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func setGestures() {
        tapGest = UITapGestureRecognizer(target: self, action: #selector(actionOnTap))
        if let tapGesture = tapGest {
            self.viewTransparent.addGestureRecognizer(tapGesture)
        }
    }
    
    //MARK: - Button Action Methods
    @IBAction func resetSelection(sender: UIButton) {
        filterBy = .none
        sortBy = .none
        let nc = NotificationCenter.default
        nc.post(name: Notification.Name("ResetFilterSort"), object: nil)
    }
    
    @IBAction func applySelection(sender: UIButton) {
        optionDelegate?.optionsToPerform(filterBy: filterBy, sortBy: sortBy)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func showFilterOptions(_ sender: UIButton) {
        guard let pageVc = self.pageViewController else { return }
        pageVc.setViewControllers([pages[0]], direction: .reverse, animated: true, completion: nil)
        moveSlider(to: buttonFilter.tag)
    }
    
    @IBAction func showSortOptions(_ sender: UIButton) {
        guard let pageVc = self.pageViewController else { return }
        pageVc.setViewControllers([pages[1]], direction: .forward, animated: true, completion: nil)
        moveSlider(to: buttonSort.tag)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

//MARK: - UIPageViewControllerDelegate, UIPageViewControllerDataSource
extension LDListOptionsViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.index(of: viewController) else {
            return nil
        }
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0 else {
            return nil
        }
        guard pages.count > previousIndex else {
            return nil
        }
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.index(of: viewController) else {
            return nil
        }
        let nextIndex = viewControllerIndex + 1
        let pagesCount = pages.count
        
        guard pagesCount != nextIndex else {
            return nil
        }
        
        guard pagesCount > nextIndex else {
            return nil
        }
        return pages[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            guard let currentViewTag = pageViewController.viewControllers?.first?.view.tag else { return }
            moveSlider(to: currentViewTag)
        }
    }
}
