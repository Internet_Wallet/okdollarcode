//
//  LDCSpinerViewController.swift
//  OK
//
//  Created by Mohit on 3/3/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class LDCSpinerViewController: OKBaseController {
    
    //MARK: - Outlet
    @IBOutlet weak var spinningWheel: TTFortuneWheel!
    @IBOutlet var countDownLabel: UILabel!{
        didSet
        {
            countDownLabel.font = UIFont(name: appFont, size: appFontSize)
            countDownLabel.text = countDownLabel.text?.localized
        }
    }
    @IBOutlet var spinDollar: UIImageView!
    @IBOutlet var labelDesc: UILabel!{
        didSet{
            labelDesc.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var spinPointer: UIImageView!
    @IBOutlet var wheelframe: UIImageView!
    @IBOutlet var spinBtn: UIButton!{
        didSet
        {
            spinBtn.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
            spinBtn.setTitle(spinBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    //MARK: - Properties
    let jsonParserObj = RTJsonParser()
    var count = 4
    var countTimer: Timer!
    var checkPrizeResponse: CheckPrizeResponse?
    var wheelColors = [UIColor]()
    var scanTrackID: String?
    var luckyNumber: String?
    private var winningIndex = 0
    var tapGest: UITapGestureRecognizer!
    
    //MARK: - Views Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigation()
        self.wheelframe.isHidden = true
        spinDollar.isHidden = true
        spinPointer.isHidden = true
        self.spinBtn.isHidden = true
        self.spinningWheel.isHidden = true
        self.buildingWheelData()
        countTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        jsonParserObj.delegate = self
        spinDollar.layer.cornerRadius = 20.0
        labelDesc.text = "Spin and Win".localized
        addGestureOnWheel()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Target Methods
    @objc func tapActionOnWheel(sender: UITapGestureRecognizer) {
        spinTheWheel()
    }
    
    @objc func update() {
        count = count - 1
        self.countDownLabel.transform = CGAffineTransform.identity
        if count == 0 {
            countTimer.invalidate()
            self.countDownLabel.isHidden = true
            self.wheelframe.isHidden = false
            spinDollar.isHidden = false
            spinPointer.isHidden = false
            self.spinBtn.isHidden = false
            self.spinningWheel.isHidden = false
            return
        } else {
            countDownLabel.text = String(count).localized
            zoomOut()
        }
    }
    
    //MARK: - Methods
    func zoomOut() {
        UIView.animate(withDuration: 0.8, animations: {
            self.countDownLabel.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        }, completion: nil)
    }
    
    func addGestureOnWheel() {
        tapGest = UITapGestureRecognizer(target: self, action: #selector(tapActionOnWheel(sender:)))
        spinningWheel.addGestureRecognizer(tapGest)
    }
    
    func removeGestureOnWheel() {
        spinningWheel.removeGestureRecognizer(tapGest)
    }
    
    func buildingWheelData() {
        var slices: [CarnivalWheelSlice] = []
        let color1 = checkPrizeResponse?.color1 ?? "#ff0000"
        let color2 = checkPrizeResponse?.color2 ?? "#0000ff"
        let color3 = checkPrizeResponse?.color3 ?? "#00ff00"
        let color4 = checkPrizeResponse?.color4 ?? "#ffff00"
        //let winningColor = checkPrizeResponse?.winningColor ?? "#ff0000"
        if let wheelDetails = checkPrizeResponse?.wheelDetails {
            for (index, item) in wheelDetails.enumerated() {
                if item.isForWinning {
                    winningIndex = index
                }
                let slice = CarnivalWheelSlice.init(title: item.displayName)
                switch index % 4 {
                case 0: slice.sliceBackgroundColor  = UIColor(hex: color1)
                case 1: slice.sliceBackgroundColor  = UIColor(hex: color2)
                case 2: slice.sliceBackgroundColor  = UIColor(hex: color3)
                case 3: slice.sliceBackgroundColor  = UIColor(hex: color4)
                default: slice.sliceBackgroundColor = UIColor(hex: color1)
                }
                slices.append(slice)
            }
        }
        self.spinningWheel.slices = slices
        self.spinningWheel.equalSlices = true
        self.spinningWheel.frameStroke.width = 0
    }
    
    func spinTheWheel() {
        removeGestureOnWheel()
        spinBtn.isUserInteractionEnabled = false
        spinningWheel.startAnimating(fininshIndex: winningIndex, offset: 90.0) { (void) in
            self.generateToken()
        }
    }
    
    func testView(responseData: SpinResultDTO?) {
        RegistrationModel.shared.playSound()
        labelDesc.text = "Winning Prize".localized
        spinPointer.image = #imageLiteral(resourceName: "spinPointerRed")
        DispatchQueue.main.async {
            self.addGestureOnWheel()
            self.spinBtn.isUserInteractionEnabled = true
            guard let ldcReceiptViewController = self.storyboard?.instantiateViewController(withIdentifier: "LDCReceiptViewController") as?  LDCReceiptViewController else { return }
            ldcReceiptViewController.responseObject = responseData
            self.navigationController?.pushViewController(ldcReceiptViewController, animated: true)
        }
    }
    
    func generateToken() {
        if appDelegate.checkNetworkAvail(){
            progressViewObj.showProgressView()
            
            let hashValue = LDCHelperFile.aKey.hmac_SHA1(key: LDCHelperFile.sKey)
            let inputValue = "password=\(hashValue)&grant_type=password"
            
            println_debug(URL.init(string: LDCHelperFile.kTokenAPIURL)!)
            jsonParserObj.ParseMyJson(url: URL.init(string: LDCHelperFile.kTokenAPIURL)! , param: inputValue as AnyObject, httpMethod: LDCHelperFile.kMethod_Post, contentType: LDCHelperFile.kContentType_urlencoded, mScreen: "TokenForPayAPI", authStr: nil)
        }
        else {
            addGestureOnWheel()
            spinBtn.isUserInteractionEnabled = true
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
        }
    }
    
    //MARK: - Button Action Methods
    @IBAction func rotateButton(_ sender: Any) {
        spinTheWheel()
    }
}

//MARK: - RTJsonParserDelegate
extension LDCSpinerViewController: RTJsonParserDelegate {
    func ResponseFromServer(withSuccess Success: Bool, json: AnyObject?, screen: String, Error: String?){
        println_debug("screen PayAPI:- \(screen)")
        println_debug("Success :- \(Success)")
        
        if Success == false {
            DispatchQueue.main.async {
                self.addGestureOnWheel()
                self.spinBtn.isUserInteractionEnabled = true
                progressViewObj.removeProgressView()
                alertViewObj.wrapAlert(title: nil, body: "Server is not reachable".localized, img: nil)
                alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                })
                alertViewObj.showAlert(controller: self)
            }
        }
        if Success == true && screen == "TokenForPayAPI" {
            
            let responseDic = json as! NSDictionary
            let tokentype = responseDic.value(forKey: "token_type")
            let accesstoken = responseDic.value(forKey: "access_token")
            let authorizationString =  "\(tokentype!) \(accesstoken!)"
            
            let hashValueStrForPipeStr = LDCHelperFile.sKey.hmac_SHA1(key: LLBStrings.sKey)
            println_debug(hashValueStrForPipeStr)
            
            let strApp = String.init(format: LDCHelperFile.kVerifyAPIURL,self.scanTrackID ?? "",self.luckyNumber ?? "")
            
            let CheckPriceURL = URL.init(string: strApp.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            
            let pipeLineStr = "Longitude=0.00|Latitude=0.00|PhoneNumber=\(UserModel.shared.mobileNo)|Name=\(UserModel.shared.name)"
            println_debug("pipeline str :: \(pipeLineStr)")
            
            let hashValue: String = pipeLineStr.hmac_SHA1(key: LDCHelperFile.sKey)
            println_debug("actual hash value :: \(hashValue)")
            
            let keys : [String] = ["Longitude","Latitude","PhoneNumber","Name","HashValue"]
            let values : [String] = ["0.00","0.00",UserModel.shared.mobileNo,UserModel.shared.name,hashValue]
            
            let jsonObj = JSONStringWriter()
            let jsonText = jsonObj.writeJsonIntoString(keys, values)
            
            DispatchQueue.main.async {
                self.jsonParserObj.ParseMyJson(url: CheckPriceURL!, param: jsonText as AnyObject, httpMethod: LDCHelperFile.kMethod_Get, contentType: LDCHelperFile.kContentType_Json, mScreen: "VerifyAPI", authStr: authorizationString)
            }
        } else if Success == true && screen == "VerifyAPI" {
            
            guard let resultJson = json as? Dictionary<String, Any> else { return }
            let parsedData = self.parseJsonData(json: resultJson)
            self.testView(responseData: parsedData)
            DispatchQueue.main.async {
                progressViewObj.removeProgressView()
            }
        }
    }
    
    func parseJsonData(json: Dictionary<String,Any>) -> SpinResultDTO? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            do {
                let decoder = JSONDecoder()
                return try decoder.decode(SpinResultDTO.self, from: jsonData)
            }
            catch _ {
                println_debug("JSON CONVERSION ERROR")
            }
        } catch _ {
            println_debug("object to string failed")
        }
        addGestureOnWheel()
        spinBtn.isUserInteractionEnabled = true
        return nil
    }
}

//MARK: - NavigationSetUpProtocol
extension LDCSpinerViewController: NavigationSetUpProtocol {
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "Lucky draw & Coupon Title".localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}
