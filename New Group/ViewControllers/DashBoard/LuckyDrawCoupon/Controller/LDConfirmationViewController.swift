//
//  LDConfirmationViewController.swift
//  OK
//
//  Created by Mohit on 4/26/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class LDConfirmationViewController: UIViewController {
    
    @IBOutlet var lblMobNumber: UILabel!{
        didSet
        {
            lblMobNumber.font = UIFont(name: appFont, size: appFontSize)
            lblMobNumber.text = lblMobNumber.text?.localized
        }
    }
    @IBOutlet var lblAmount: UILabel!{
        didSet
        {
            lblAmount.font = UIFont(name: appFont, size: appFontSize)
            lblAmount.text = lblAmount.text?.localized
        }
    }
    @IBOutlet var lblTransactionId: UILabel!{
        didSet
        {
            lblTransactionId.font = UIFont(name: appFont, size: appFontSize)
            lblTransactionId.text = lblTransactionId.text?.localized
        }
    }
    @IBOutlet var lblDateTime: UILabel!{
        didSet
        {
            lblDateTime.font = UIFont(name: appFont, size: appFontSize)
            lblDateTime.text = lblDateTime.text?.localized
        }
    }
    
    var responseObject: SpinResultDTO?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        println_debug(responseObject)
        DispatchQueue.main.async {
            self.loaData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func loaData() {
        self.lblMobNumber.text = "(+95) \(responseObject?.result?.destinationNumber ?? "")"
        self.lblAmount.text = "\(responseObject?.result?.prize?.totalPrize ?? 0)"
        self.lblTransactionId.text = "Transation ID  \(responseObject?.result?.oKTransactionId ?? "")".localized
        if let createdDateStr = responseObject?.result?.prize?.createdDate, let createdDate = createdDateStr.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSS") {
            self.lblDateTime.text = createdDate.stringValue(dateFormatIs: "dd-MM-yyyy")
        }
    }
    
    @IBAction func btnCloseClick(_ sender: UIButton) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromBottom
        navigationController?.view.layer.add(transition, forKey: nil)
        _ = navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnOpenClick(_ sender: UIButton) {
        
    }
}
