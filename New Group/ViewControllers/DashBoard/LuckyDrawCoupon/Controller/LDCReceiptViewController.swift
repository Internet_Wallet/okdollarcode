//
//  LDCReceiptViewController.swift
//  OK
//
//  Created by Mohit on 3/17/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class LDCReceiptViewController: UIViewController {
    
    @IBOutlet weak var constraintImageViewH: NSLayoutConstraint!
    @IBOutlet var lblWon: UILabel!{
        didSet
        {
            lblWon.font = UIFont(name: appFont, size: appFontSize)
            lblWon.text = lblWon.text?.localized
        }
    }
    
    var responseObject: SpinResultDTO?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        println_debug(responseObject)
        setUpNavigation()
        if let prizeAmount = responseObject?.result?.prize?.prizeAmount {
            let prizeStr = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(prizeAmount)")
            self.lblWon.text = "You won".localized + " " + prizeStr + " " + "mmk".localized
        }
        if Device.IS_IPHONE_5 {
            constraintImageViewH.constant = 150
            self.view.layoutIfNeeded()
        }
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .normal)
        UITabBar.appearance().unselectedItemTintColor = .lightGray
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func backToList() {
        var isFromPromotionList = false
        guard let navController = self.navigationController else { return }
        for controller in navController.viewControllers as Array {
            if controller.isKind(of: LDCPromotionListViewController.self) {
                isFromPromotionList = true
                navController.popToViewController(controller, animated: true)
                break
            }
        }
        if !isFromPromotionList {
            navController.popToRootViewController(animated: true)
        }
    }
}

extension LDCReceiptViewController: NavigationSetUpProtocol {
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backToList), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "Lucky draw & Coupon Title".localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}
