
//  PromotionViewController.swift
//  OK
//
//  Created by Mohit on 2/24/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SDWebImage

class LDCPromotionViewController: OKBaseController {
    //MARK: - Outlets
    @IBOutlet weak var promoTbl: UITableView!
    @IBOutlet var lblNumberofProduct: UILabel!{
        didSet{
            self.lblNumberofProduct.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var cornerBtn: SpringButton!
    @IBOutlet var constraintFloatingBottom: NSLayoutConstraint!
    
    //MARK: - Properties
    fileprivate let cellIdentify = "PromotionalViewCell"
    private let refreshControl = UIRefreshControl()
    let jsonParserObj = RTJsonParser()
    private lazy var searchBar = UISearchBar()
    var homeParentVC: LuckyDrawHomeViewController?
    var responseDic = NSDictionary()
    var navController: UINavigationController?
    var promotionProductDetail: PromotionProductDTO?
    var aggregatedProducts: [PromotionProducts] = []
    var productList = [PromotionProducts]()
    
    //MARK: - Views Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpInitial()
        setUpNavigation()
        jsonParserObj.delegate = self
        self.generateToken()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        promoTbl.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshTableData), for: .valueChanged)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Promotion View"
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    //MARK: - Methods
    func setUpInitial() {
        searchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.tintColor = UIColor.gray
        searchBar.placeholder = "Search".localized
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 16) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
               
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.keyboardType = .asciiCapable
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
            }
        }
        let view = self.searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = UIColor.gray
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let searchButton = UIButton(type: .custom)
        searchButton.setImage(#imageLiteral(resourceName: "r_search").withRenderingMode(.alwaysOriginal), for: .normal)
        searchButton.addTarget(self, action: #selector(showSearchOption), for: .touchUpInside)
//        if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
//            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
//        }else{
            searchButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
 //       }
        navTitleView.addSubview(searchButton)
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backOrCancelAction), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "Lucky draw & Coupon Title".localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
    
    //MARK: - Target Methods
    @objc private func showSearchOption() {
        searchBar.placeholder = "Search".localized
        searchBar.text = ""
        self.navigationItem.titleView = searchBar
        searchBar.becomeFirstResponder()
    }
    
    @objc func backOrCancelAction() {
        self.navigationController?.popViewController(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "StartScan"), object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInset = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
            self.promoTbl.contentInset = contentInset
            self.promoTbl.scrollIndicatorInsets = contentInset
            self.constraintFloatingBottom.constant = keyboardSize.height
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.promoTbl.contentInset = UIEdgeInsets.zero
        self.promoTbl.scrollIndicatorInsets = UIEdgeInsets.zero
        self.constraintFloatingBottom.constant = 15
        self.view.layoutIfNeeded()
    }
    
    @objc func refreshTableData() {
        self.generateToken()
    }
    
    //MARK: - Methods
    func generateToken() {
        self.refreshControl.endRefreshing()
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            let hashValue = LDCHelperFile.aKey.hmac_SHA1(key: LDCHelperFile.sKey)
            let inputValue = "password=\(hashValue)&grant_type=password"
            
            jsonParserObj.ParseMyJson(url: URL.init(string: LDCHelperFile.kTokenAPIURL)! , param: inputValue as AnyObject, httpMethod: LDCHelperFile.kMethod_Post, contentType: LDCHelperFile.kContentType_urlencoded, mScreen: "TokenForPayAPI", authStr: nil)
        } else {
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func animateTable() {
        promoTbl.reloadData()
        let cells = promoTbl.visibleCells
        let tableHeight: CGFloat = promoTbl.bounds.size.height
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        var index = 0
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
    
    func customAlert(msg:String, title:String) {
        alertViewObj.wrapAlert(title: "", body: msg.localized, img: #imageLiteral(resourceName: "dashboard_lucky_draw"))
        alertViewObj.addAction(title: "Done".localized, style: .target, action:{})
        alertViewObj.showAlert(controller: self)
    }
    
    func navigateToPromotionProducts(listNumber: Int? = nil) {
        guard let promotionProductsViewController = UIStoryboard(name: "LuckyDrawCoupan", bundle: Bundle.main).instantiateViewController(withIdentifier: "LDPromotionProductsViewController") as? LDPromotionProductsViewController else { return }
        guard let indexOFList = listNumber, let resultList = promotionProductDetail?.results, resultList.count > 0 else { return }
        promotionProductsViewController.productDetail = resultList[indexOFList]
        self.navigationController?.pushViewController(promotionProductsViewController, animated: true)
    }
    
    func getParentIndex(indexRow: Int) -> Int? {
        if let productId = productList[indexRow].product?.productId {
            if let parentList = promotionProductDetail?.results {
                for (index, item) in parentList.enumerated() {
                    if let products = item.promotionProducts {
                        for prod in products {
                            if let prodId = prod.product?.productId {
                                if productId == prodId {
                                    return index
                                }
                            }
                        }
                    }
                }
            }
        }
        return nil
    }
    
    func applySearch(with searchText: String) {
        productList = aggregatedProducts.filter {
            if let prodName = $0.product?.productName {
                if prodName.lowercased().range(of: searchText.lowercased()) != nil {
                    return true
                }
            }
            return false
        }
        promoTbl.reloadData()
    }
    
    //MARK: - Button Action Methods
    @IBAction func handleToggleBT(_ sender: SpringButton) {
        self.view.endEditing(true)
        guard let listOptionsViewController = UIStoryboard(name: "LuckyDrawCoupan", bundle: nil).instantiateViewController(withIdentifier: "LDListOptionsViewController") as? LDListOptionsViewController else { return }
        listOptionsViewController.optionDelegate = self
        listOptionsViewController.modalPresentationStyle = .overCurrentContext
        //        self.homeParentVC?.present(listOptionsViewController, animated: false, completion: nil)
        self.present(listOptionsViewController, animated: false, completion: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}

//MARK: - PaytoScrollerDelegate
extension LDCPromotionViewController: PaytoScrollerDelegate {
    func willMove(toPage controller: UIViewController!, index: Int) {
        if index == 1 {
            
        } else {
            
        }
    }
    
    func didMove(toPage controller: UIViewController!, index: Int) {
        if index == 1 {
            
        } else {
            
        }
    }
}

//MARK: - UITableViewDataSource, UITableViewDelegate
extension LDCPromotionViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        if productList.count == 0 {
            let noDataLabel = UILabel(frame: tableView.bounds)
            if let myFont = UIFont(name: appFont, size: 17) {
                noDataLabel.font = myFont
            }
            if let _ = promotionProductDetail {
                noDataLabel.text = "No Records Found".localized
            } else {
                noDataLabel.text = "Loading Data...".localized
            }
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            self.lblNumberofProduct.isHidden = true
        } else {
            self.lblNumberofProduct.isHidden = false
            self.lblNumberofProduct.text = "Number of Promotion(s)".localized + " - \(self.productList.count)"
            tableView.backgroundView = nil
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentify, for: indexPath) as! PromotionalViewCell
        var companyName = ""
        var packName = ""
        var endDateStr = ""
        let item = productList[indexPath.row]
        let parentIndexVal = getParentIndex(indexRow: indexPath.row)
        if let parentList = promotionProductDetail?.results, let parentIndex = parentIndexVal, parentList.count > parentIndex {
            if let companyNam = parentList[parentIndex].company?.companyName {
                companyName = companyNam
            }
            if let desc = parentList[parentIndex].promotionPackageName {
                packName = desc
            }
            if let eDateStr = parentList[parentIndex].endDate {
                endDateStr = eDateStr
            }
        }
        cell.wrapCellData(productItem: item, compName: companyName, desc: packName, dateStr: endDateStr)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 160
        tableView.rowHeight = UITableView.automaticDimension
        return tableView.rowHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let parentIndexVal = getParentIndex(indexRow: indexPath.row)
        guard let selectedIndexInParentList = parentIndexVal else { return }
        self.navigateToPromotionProducts(listNumber: selectedIndexInParentList)
    }
}

//MARK: - RTJsonParserDelegate
extension LDCPromotionViewController: RTJsonParserDelegate {
    func ResponseFromServer(withSuccess Success: Bool, json: AnyObject?, screen: String, Error: String?){
        println_debug("screen PayAPI:- \(screen)")
        println_debug("Success :- \(Success)")
        if Success == false {
            DispatchQueue.main.async{
                progressViewObj.removeProgressView()
            }
        }
        if Success == true && screen == "TokenForPayAPI" {
            responseDic = json as! NSDictionary
            let tokentype = responseDic.value(forKey: "token_type")
            let accesstoken = responseDic.value(forKey: "access_token")
            let authorizationString =  "\(tokentype!) \(accesstoken!)"
            let hashValueStrForPipeStr = LDCHelperFile.aKey.hmac_SHA1(key: LDCHelperFile.sKey)
            let ScanURL = URL.init(string: LDCHelperFile.kLLBLivePromotion)
            let jsonText = "{\"Longitude\":\"\(0.0)\", \"Latitude\":\(0.0), \"PhoneNumber\":\"\(UserModel.shared.mobileNo)\",\"Name\":\"\(UserModel.shared.name)\",\"HashValue\":\"\(hashValueStrForPipeStr)\"}"
            println_debug(jsonText)
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
                self.jsonParserObj.ParseMyJson(url: (ScanURL)!, param: jsonText as AnyObject, httpMethod: LDCHelperFile.kMethod_Get, contentType: LDCHelperFile.kContentType_Json, mScreen: "ScanAPI", authStr: authorizationString)
            }
        } else if Success == true && screen == "ScanAPI" {
            if let unwrappedJson = json {
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: unwrappedJson, options: .prettyPrinted)
                    do {
                        let decoder = JSONDecoder()
                        promotionProductDetail = try decoder.decode(PromotionProductDTO.self, from: jsonData)
                        if let prodTypes = promotionProductDetail?.results {
                            aggregatedProducts = []
                            for prod in prodTypes {
                                if let prodList = prod.promotionProducts {
                                    aggregatedProducts += prodList
                                }
                            }
                            productList = aggregatedProducts
                        }
                    }
                    catch _ {
                        println_debug("JSON CONVERSION ERROR")
                    }
                } catch _ {
                    println_debug("object to string failed")
                }
            }
            DispatchQueue.main.async {
                progressViewObj.removeProgressView()
                self.lblNumberofProduct.text = "Number of Products".localized + " - \(self.aggregatedProducts.count)"
                self.animateTable()
            }
        }
    }
}

//MARK: - RTJsonParserDelegate
extension LDCPromotionViewController: ListOptionsProtocol {
    func optionsToPerform(filterBy: FilterBy, sortBy: SortBy) {
        switch filterBy {
        case .product:
            self.productList = productList.filter {
                guard let categoryName = $0.product?.categoryName else { return false }
                return categoryName == "Product"
            }
        default:
            break
        }
        switch sortBy {
        case .aToZ:
            self.productList = productList.sorted(by: {
                guard let prodName1 = $0.product?.productName, let prodName2 = $1.product?.productName else { return false }
                return prodName1 < prodName2
            })
        case .zToA:
            self.productList = productList.sorted(by: {
                guard let prodName1 = $0.product?.productName, let prodName2 = $1.product?.productName else { return false }
                return prodName1 > prodName2
            })
        default:
            break
        }
        self.promoTbl.reloadData()
    }
}

// MARK: - UISearchBarDelegate
extension LDCPromotionViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            uiButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize) ?? UIFont.systemFont(ofSize: 14)
            if appDel.currentLanguage == "my" {
                uiButton.setTitle("မလုပ်ပါ", for:.normal)
            } else if appDel.currentLanguage == "uni" {
                uiButton.setTitle("မလုပ္ပါ", for:.normal)
            } else {
                uiButton.setTitle("Cancel".localized, for:.normal)
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            productList = aggregatedProducts
            promoTbl.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            if updatedText.count > 50 {
                return false
            }
            if updatedText != "" && text != "\n" {
                self.applySearch(with: updatedText)
            }
        }
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        setUpNavigation()
        productList = aggregatedProducts
        promoTbl.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}

//MARK: - PromotionalViewCell
class PromotionalViewCell: UITableViewCell {
    
    //MARK: - Outlet
    @IBOutlet var lblProductName: UILabel!{
        didSet{
            self.lblProductName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var lblCompanyName: UILabel!{
        didSet{
            self.lblCompanyName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var lblDesc: UILabel!{
        didSet{
            self.lblDesc.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var lblDate: UILabel!{
        didSet{
            self.lblDate.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var imgProductIcon: UIImageView!
    
    //MARK: - Views Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Methods
    func wrapCellData(productItem: PromotionProducts?, compName: String, desc: String, dateStr: String?) {
        self.imgProductIcon.image = nil
        if let img = productItem?.product?.productImage {
            self.wrapImage(string: img)
        }
        lblProductName.text = productItem?.product?.productName ?? ""
        lblCompanyName.text = compName
        lblDesc.text = desc
        lblDate.text = ""
        if let endDateStr = dateStr, let endDate = endDateStr.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss") {
            lblDate.text = endDate.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy")
        }
    }
    
    func wrapImage(string: String) {
        if let stringUrl = string.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            if let url = URL.init(string: stringUrl) {
                self.imgProductIcon.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ok"), options: .progressiveDownload, completed: { (image, error, type, url) in
                    self.imgProductIcon.image = image
                })
            }
        }
    }
}
