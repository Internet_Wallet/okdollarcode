//
//  LDCECoupon.swift
//  OK
//
//  Created by Mohit on 2/22/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class LDCECoupon: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet var barCodeTxt: SkyFloatingLabelTextField!{
        didSet{
            self.barCodeTxt.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
//    @IBOutlet weak var liftYesBtn: UIButton! {
//        didSet {
//            liftYesBtn.setTitle((liftYesBtn.titleLabel?.text ?? "").localized, for: .normal)
//            liftYesBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
//        }
//    }
    
    
    @IBOutlet weak var btnProceed: UIButton! {
        didSet{
            btnProceed.setTitle("Proceed".localized, for: .normal)
            if let myFont = UIFont(name: appFont, size: 18) {
                btnProceed.titleLabel?.font =  myFont
            }
        }
    }
    
    //MARK: - Properties
    private var proceedButton: UIButton {
        let proceedBtn = UIButton.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 50))
        proceedBtn.backgroundColor = UIColor(red: 192.0/255.0, green: 192.0/255.0, blue: 192.0/255.0, alpha: 1.0)
        proceedBtn.setTitle("Proceed".localized, for: .normal)
        if let myFont = UIFont(name: appFont, size: 18) {
            proceedBtn.titleLabel?.font =  myFont
        }
        proceedBtn.setTitleColor(UIColor(red: 30.0/255.0, green: 43.0/255.0, blue: 130.0/255.0, alpha: 1.0), for: .normal)
        proceedBtn.addTarget(self, action: #selector(self.proceedClick(_sender:)), for: .touchUpInside)
        return proceedBtn
    }
    
    private var model: LuckyDrawModel!
    private var barOrCouponCode: String?
    
    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.barCodeTxt.text = ""
        self.barCodeTxt.selectedTitle = "Bar Or Coupon Code".localized
        self.barCodeTxt.placeholder = "Enter Bar Or Coupon Code".localized
        self.model = LuckyDrawModel()
        self.model.luckyDrawModelDelegate = self
        self.barCodeTxt.inputAccessoryView = self.proceedButton
        updateProceedBtnColor(isEcouponEntered: false)
        let tapGest = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tapGest)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.barCodeTxt.becomeFirstResponder()
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    //MARK: - Methods
    private func updateProceedBtnColor(isEcouponEntered: Bool) {
        guard let proceedBtnView = self.barCodeTxt.inputAccessoryView else { return }
        if isEcouponEntered {
            proceedBtnView.backgroundColor = kYellowColor
            proceedBtnView.isUserInteractionEnabled = true
            btnProceed.backgroundColor = kYellowColor
            btnProceed.isUserInteractionEnabled = true
        } else {
            proceedBtnView.backgroundColor = UIColor(red: 192.0/255.0, green: 192.0/255.0, blue: 192.0/255.0, alpha: 1.0)
            proceedBtnView.isUserInteractionEnabled = false
            btnProceed.backgroundColor = UIColor(red: 192.0/255.0, green: 192.0/255.0, blue: 192.0/255.0, alpha: 1.0)
            btnProceed.isUserInteractionEnabled = false
        }
    }
    
    //MARK: - Target Methods
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    //MARK: - Button Action Methods
    @IBAction func close(_sender: UIButton) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func proceedClick(_sender: UIButton) {
        self.view.endEditing(true)
        self.barOrCouponCode = self.barCodeTxt.text
        if let isNumeric = self.barCodeTxt.text?.isNumeric(), isNumeric {
            self.model.generateToken(aKey: LDCHelperFile.aKey, sKey: LDCHelperFile.sKey)
        } else {
            guard let code = self.barOrCouponCode else { return }
            let request = ValidateCouponRequest(luckyNumber: code)
            self.model.validateLuckyNumber(couponReq: request)
        }
    }
}

//MARK: - Textfield Delegate
extension LDCECoupon: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            if string == " " {
                return false
            }
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            updateProceedBtnColor(isEcouponEntered: updatedText.count >= 6 ? true : false)
            return true
        }
        return false
    }
}
    
//MARK: - LuckyDrawModelDelegate
extension LDCECoupon: LuckyDrawModelDelegate {
    func apiResult(message: String?, statusCode: String?, apiResultFor: String) {
        guard let screenName = LuckyDrawModel.ScreenName(rawValue: apiResultFor) else { return }
        switch screenName {
        case .generateToken:
            guard let tokenStr = message else { return }
            self.model.scanBarCode(authToken: tokenStr, barCode: self.barOrCouponCode ?? "")
        case .scanBarCode:
            if let responseCode = statusCode {
                switch responseCode {
                case "404":
                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: "", body: "Invalid barcode. This barcode is invalid and not in standard format".localized, img: #imageLiteral(resourceName: "dashboard_chat"))
                        alertViewObj.addAction(title: "OK".localized, style: .cancel , action: {
                        })
                        alertViewObj.showAlert(controller: self)
                    }
                case "302":
                    DispatchQueue.main.async {
                        guard let productDescriptionVC = self.storyboard?.instantiateViewController(withIdentifier: "LDCProductDescriptionViewController") as? LDCProductDescriptionViewController else { return }
                        productDescriptionVC.screenType = ProductDescriptionScreenFrom.scanProduct
                        productDescriptionVC.productDetailFromScan = self.model.scanResult
                        productDescriptionVC.scanTrackId = self.model.scanResult?.scanTrackingId ?? ""
                        self.navigationController?.pushViewController(productDescriptionVC, animated: true)
                    }
                default:
                    println_debug("Case not handled")
                }
            }
        case .verifyCoupon:
            if let responseCode = statusCode {
                switch responseCode {
                case "200":
                    DispatchQueue.main.async {
                        guard let resultController = self.storyboard?.instantiateViewController(withIdentifier: "LDCECouponResultController") as? LDCECouponResultController else { return }
                        resultController.result = LDCECouponResultController.CouponResult.success
                        resultController.amount = self.model.validationCouponResult?.eCouponAmount
                        self.navigationController?.pushViewController(resultController, animated: true)
                    }
                default:
                    DispatchQueue.main.async {
                        guard let resultController = self.storyboard?.instantiateViewController(withIdentifier: "LDCECouponResultController") as? LDCECouponResultController else { return }
                        resultController.result = LDCECouponResultController.CouponResult.failure
                        resultController.couponCode = self.barOrCouponCode
                        self.navigationController?.pushViewController(resultController, animated: true)
                    }
                }
            }
        }
    }
    
    func handleCustomError(error: CustomError) {
        print(error.rawValue)
    }
}
