//
//  LDCECouponResultController.swift
//  OK
//
//  Created by E J ANTONY on 3/8/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class LDCECouponResultController: UIViewController {

    //MARK: - Outlets
    //Failure properties
    @IBOutlet weak var imgVAlert: UIImageView!
    @IBOutlet weak var lblAlert: UILabel!{
        didSet{
            self.lblAlert.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblCoupon: UILabel!{
        didSet{
            self.lblCoupon.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblFailureDesc: UILabel!{
        didSet{
            self.lblFailureDesc.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnTry: UIButton!{
        didSet{
            btnTry.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    //Success properties
    @IBOutlet weak var lblWishes: UILabel!{
        didSet{
            self.lblWishes.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblAmount: UILabel!{
        didSet{
            self.lblAmount.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblSuccessDesc: UILabel!{
        didSet{
            self.lblSuccessDesc.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgViewSuccess: UIImageView!
    @IBOutlet weak var btnDone: UIButton!{
        didSet{
            btnDone.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var viewSuccess: UIView!
    @IBOutlet weak var viewFailure: UIView!
    
    //MARK: - Properties
    var result = CouponResult.failure
    var amount: String?
    var couponCode: String?
    
    enum CouponResult {
        case success, failure
    }
    
    //MARK: - Views life cycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        switch result {
        case .success:
            viewSuccess.isHidden = false
            viewFailure.isHidden = true
            btnDone.layer.cornerRadius = 5.0
            if let amountStr = amount {
                let couponAmount = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: amountStr)
                if couponAmount.count > 0 {
                    self.lblAmount.text = couponAmount + " MMK"
                }
            }
        case .failure:
            viewSuccess.isHidden = true
            viewFailure.isHidden = false
            btnTry.layer.cornerRadius = 5.0
            if let codeStr = couponCode {
                self.lblCoupon.text = codeStr
            }
        }
    }
    
    //MARK: -Button action methods
    @IBAction func tryAgain(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneAction(_ sender: UIButton) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    } 
}
