//
//  PromotionProductTableViewCell.swift
//  OK
//
//  Created by E J ANTONY on 14/06/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PromotionProductTableViewCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var imageViewProduct: UIImageView!
    @IBOutlet weak var labelProductName: UILabel!{
        didSet{
            labelProductName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var labelProductCategory: UILabel!{
        didSet{
            labelProductCategory.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var labelProductPackaging: UILabel!{
        didSet{
            labelProductPackaging.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
