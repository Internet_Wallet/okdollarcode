//
//  productDescriptionViewController.swift
//  OK
//
//  Created by Mohit on 2/27/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

enum ProductDescriptionScreenFrom {
    case scanProduct, promotionList, other
}

class LDCProductDescriptionViewController: OKBaseController {
    //MARK: - Outlet
    @IBOutlet weak var constraintImageViewH: NSLayoutConstraint!
    @IBOutlet var prodOneImg: UIImageView!
    @IBOutlet weak var labelCompanyName: UILabel!{
        didSet{
            self.labelCompanyName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var txtLuckyNumber: SkyFloatingLabelTextField!{
        didSet{
            self.txtLuckyNumber.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var lblprodName: UILabel!{
        didSet{
            self.lblprodName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var prodImg: UIImageView!
    @IBOutlet var imgbarCode: UIImageView!
    @IBOutlet var customView: UIView!
    @IBOutlet var constraintClearButtonWidth: NSLayoutConstraint!
    @IBOutlet var btnGetCoupan: UIButton!{
        didSet{
            btnGetCoupan.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    
    //MARK: - Properties
    private var colorTimer: Timer!
    private var colorIndex = 0
    let jsonParserObj = RTJsonParser()
    var scanTrackId = ""
    var Responsedict = Dictionary<String,Any>()
    var DictToSend = Dictionary<String, AnyObject>()
    var productDetail: PromotionProductDetail?
    var productDetailFromScan: ScanResult?
    var screenType = ProductDescriptionScreenFrom.other
    var companyDetail: PromotionCompany?
    var packImage: String?
    var checkPrizeResponse: CheckPrizeResponse?
    
    //MARK: - Views Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigation()
        self.btnGetCoupan.isHidden = true
        self.txtLuckyNumber.delegate = self
        jsonParserObj.delegate = self
        self.loadData()
        if Device.IS_IPHONE_5 {
            constraintImageViewH.constant = 150
            self.view.layoutIfNeeded()
        }
        txtLuckyNumber.selectedTitle = "Lucky Number".localized
        txtLuckyNumber.placeholder = "Enter Lucky Number".localized
        btnGetCoupan.setTitle("GET LUCKY".localized, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        colorTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(changeColor), userInfo: nil, repeats: true)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        colorTimer.invalidate()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Target Methods
    @objc func changeColor() {
        colorIndex = colorIndex % 3
        switch colorIndex {
        case 0:
            self.customView.backgroundColor = UIColor.red
            colorIndex += 1
        case 1:
            self.customView.backgroundColor = UIColor.blue
            colorIndex += 1
        case 2:
            self.customView.backgroundColor = UIColor.green
            colorIndex += 1
        default:
            break
        }
    }
    
    //MARK: - Methods
    func generateToken() {
        if appDelegate.checkNetworkAvail(){
            progressViewObj.showProgressView()
            let hashValue = LDCHelperFile.aKey.hmac_SHA1(key: LDCHelperFile.sKey)
            let inputValue = "password=\(hashValue)&grant_type=password"
            println_debug(URL.init(string: LDCHelperFile.kTokenAPIURL)!)
            jsonParserObj.ParseMyJson(url: URL.init(string: LDCHelperFile.kTokenAPIURL)! , param: inputValue as AnyObject, httpMethod: LDCHelperFile.kMethod_Post, contentType: LDCHelperFile.kContentType_urlencoded, mScreen: "TokenForPayAPI", authStr: nil)
        } else {
            self.btnGetCoupan.isUserInteractionEnabled = true
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func loadData() {
        switch screenType {
        case .promotionList:
            self.lblprodName.text = productDetail?.productName ?? ""
            self.labelCompanyName.text = companyDetail?.companyName ?? ""
            if let prodImageStr = productDetail?.productImage {
                let imgUrl = URL.init(string: prodImageStr)
                self.prodImg.sd_setImage(with: imgUrl, placeholderImage: #imageLiteral(resourceName: "ok"), options: .progressiveDownload, completed: {(image, error, type, url) in
                    self.prodImg.image = image
                })
            }
            if let prodImageStr = packImage {
                let barImgUrl = URL.init(string: prodImageStr)
                self.prodOneImg.sd_setImage(with: barImgUrl, placeholderImage: #imageLiteral(resourceName: "ok"), options: .progressiveDownload, completed: {(image, error, type, url) in
                    self.prodOneImg.image = image
                })
            }
        case .scanProduct:
            if let scannedProduct = productDetailFromScan {
                self.lblprodName.text = scannedProduct.product?.productName
                self.labelCompanyName.text = scannedProduct.promotionPackage?.company?.companyName
                if let prodImageStr = scannedProduct.product?.productImage {
                    let properImgaeUrlStr = prodImageStr.replacingOccurrences(of: " ", with: "%20")
                    if let imgUrl = URL(string: properImgaeUrlStr) {
                        self.prodImg.sd_setImage(with: imgUrl, placeholderImage: #imageLiteral(resourceName: "ok"), options: .progressiveDownload, completed: {(image, error, type, url) in
                            self.prodImg.image = image
                        })
                    }
                }
                if let promImageStr = scannedProduct.promotionPackage?.promotionPackageImage {
                    let properImgaeUrlStr = promImageStr.replacingOccurrences(of: " ", with: "%20")
                    if let imgUrl = URL(string: properImgaeUrlStr) {
                        self.prodOneImg.sd_setImage(with: imgUrl, placeholderImage: #imageLiteral(resourceName: "ok"), options: .progressiveDownload, completed: {(image, error, type, url) in
                            self.prodOneImg.image = image
                        })
                    }
                }
            }
        case .other:
            break
        }
    }
    
    //MARK: - Button Action Methods
    @IBAction func clearTextAction(_ sender: UIButton) {
        txtLuckyNumber.text = ""
        self.btnGetCoupan.isHidden = true
        constraintClearButtonWidth.constant = 0
        self.view.layoutIfNeeded()
    }
    
    @IBAction func submitLuckyNumber(_ sender: UIButton) {
        btnGetCoupan.isUserInteractionEnabled = false
        self.generateToken()
    }
}

//MARK: - RTJsonParserDelegate
extension LDCProductDescriptionViewController: RTJsonParserDelegate {
    func ResponseFromServer(withSuccess Success: Bool, json: AnyObject?, screen: String, Error: String?){
        if Success == false {
            DispatchQueue.main.async{
                progressViewObj.removeProgressView()
            }
            self.btnGetCoupan.isUserInteractionEnabled = true
        }
        if Success == true && screen == "TokenForPayAPI" {
            let responseDic = json as! NSDictionary
            let tokentype = responseDic.value(forKey: "token_type")
            let accesstoken = responseDic.value(forKey: "access_token")
            let authorizationString =  "\(tokentype!) \(accesstoken!)"
            let hashValueStrForPipeStr = LDCHelperFile.aKey.hmac_SHA1(key: LLBStrings.sKey)
            
            println_debug(hashValueStrForPipeStr)
            DispatchQueue.main.async{
                let two = self.txtLuckyNumber.text
                let strApp = String.init(format: LDCHelperFile.kCheckPriceAPIURL,self.scanTrackId,two!)
                let ProductURL = URL.init(string: strApp.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
                let pipeLineStr = "Longitude=0.00|Latitude=0.00|PhoneNumber=\(UserModel.shared.mobileNo)|Name=\(UserModel.shared.name)"
                println_debug("pipeline str :: \(pipeLineStr)")
                let hashValue: String = pipeLineStr.hmac_SHA1(key: LDCHelperFile.sKey)
                println_debug("actual hash value :: \(hashValue)")
                let keys : [String] = ["Longitude","Latitude","PhoneNumber","Name","HashValue"]
                let values : [String] = ["0.00","0.00",UserModel.shared.mobileNo,UserModel.shared.name,hashValue]
                let jsonObj = JSONStringWriter()
                let jsonText = jsonObj.writeJsonIntoString(keys, values)
                self.jsonParserObj.ParseMyJson(url: ProductURL!, param: jsonText as AnyObject, httpMethod: LDCHelperFile.kMethod_Get, contentType: LDCHelperFile.kContentType_Json, mScreen: "ProductAPI", authStr: authorizationString)
            }
        } else if Success == true && screen == "ProductAPI" {
            if let unwrappedJson = json {
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: unwrappedJson, options: .prettyPrinted)
                    do {
                        let decoder = JSONDecoder()
                        let checkPrizeResponseDTO = try decoder.decode(CheckPrizeResponseDTO.self, from: jsonData)
                        if let response = checkPrizeResponseDTO.result {
                            checkPrizeResponse = response
                            var wheelBGColors: [UIColor] = []
                            wheelBGColors.append(UIColor(hex: response.color1))
                            wheelBGColors.append(UIColor(hex: response.color2))
                            wheelBGColors.append(UIColor(hex: response.color3))
                            wheelBGColors.append(UIColor(hex: response.color4))
                            
                            DispatchQueue.main.async {
                                let spinerViewController = self.storyboard?.instantiateViewController(withIdentifier: "LDCSpinerViewController") as! LDCSpinerViewController
                                spinerViewController.scanTrackID = self.scanTrackId
                                spinerViewController.luckyNumber = self.txtLuckyNumber.text
                                spinerViewController.checkPrizeResponse = self.checkPrizeResponse
                                spinerViewController.wheelColors = wheelBGColors
                                self.navigationController?.pushViewController(spinerViewController, animated: true)
                                self.btnGetCoupan.isUserInteractionEnabled = true
                            }
                        } else {
                            DispatchQueue.main.async {
                                self.customAlert(msg: (checkPrizeResponseDTO.message ?? "").localized, title: "")
                            }
                        }
                    }
                    catch _ {
                        println_debug("JSON CONVERSION ERROR")
                    }
                } catch _ {
                    println_debug("object to string failed")
                }
            }
        }
        DispatchQueue.main.async {
            progressViewObj.removeProgressView()
        }
    }
    
    func customAlert(msg:String, title:String) {
        alertViewObj.wrapAlert(title: "", body: msg.localized, img: #imageLiteral(resourceName: "dashboard_lucky_draw"))
        alertViewObj.addAction(title: "Cancel".localized, style: .cancel, action:{})
        alertViewObj.addAction(title: "Try Again".localized, style: .target, action:{
            self.txtLuckyNumber.text = ""
            self.txtLuckyNumber.becomeFirstResponder()
        })
        alertViewObj.showAlert(controller: self)
        self.btnGetCoupan.isUserInteractionEnabled = true
    }
    
    func parseJsonData(json: Dictionary<String,Any>) -> WheelResultModel {
        let respObject: WheelResultModel?
        var WheelDetailsArr : [WheelDetailsModel] = []
        if let WheelObj = json.safeValueForKey("WheelDetails") as? [Dictionary<String,Any>] {
            WheelObj.forEach({ (object) in
                let WheelObject = WheelDetailsModel(_WheelDetailId: object.safeValueForKey("WheelDetailId") as? String ?? "", _PackageId: object.safeValueForKey("PackageId") as? String ?? "", _WheelDetailType: object.safeValueForKey("WheelDetailType") as? String ?? "", _DisplayName: object.safeValueForKey("DisplayName") as? String ?? "", _Amount: object.safeValueForKey("Amount") as? NSNumber ?? 0, _ParentWheelDetailId: object.safeValueForKey("ParentWheelDetailId") as? String ?? "", _CreatedDate: object.safeValueForKey("CreatedDate") as? String ?? "", _DisplayOrder: object.safeValueForKey("DisplayOrder") as? NSNumber ?? 0, _IsForWinning: object.safeValueForKey("IsForWinning") as? NSNumber ?? 0)
                
                WheelDetailsArr.append(WheelObject)
            })
        }
        
        respObject = WheelResultModel(_WheelSettingId: json.safeValueForKey("WheelSettingId") as? String ?? "", _Color1: json.safeValueForKey("Color1") as? String ?? "", _Color2: json.safeValueForKey("Color2") as? String ?? "", _Color3: json.safeValueForKey("Color3") as? String ?? "", _Color4: json.safeValueForKey("Color4") as? String ?? "", _WinningColor: json.safeValueForKey("WinningColor") as? String ?? "", _SpinSpeed: json.safeValueForKey("SpinSpeed") as? String ?? "", _TextOrientation: json.safeValueForKey("TextOrientation") as? String ?? "", _TextSize: json.safeValueForKey("TextSize") as? NSNumber ?? 0, _Duration: json.safeValueForKey("Duration") as? NSNumber ?? 0, _TextColor: json.safeValueForKey("TextColor") as? String ?? "", _PromotionPackageId: json.safeValueForKey("PromotionPackageId") as? String ?? "", _CreatedDate: json.safeValueForKey("CreatedDate") as? String ?? "", _WheelDetails: WheelDetailsArr)
        return respObject!
    }
}

// MARK: - UITextFieldDelegate
extension LDCProductDescriptionViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            let allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"
            let cs = NSCharacterSet(charactersIn: allowedChars).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            if string != filtered {
                return false
            }
            if string == " " {
                return false
            }
            if updatedText.count > 4 {
                self.btnGetCoupan.isHidden = false
            } else {
                self.btnGetCoupan.isHidden = true
            }
            if updatedText.count > 20 {
                return false
            }
            if updatedText.count > 0 {
                txtLuckyNumber.selectedTitle = "Lucky Number".localized
                constraintClearButtonWidth.constant = 50
            } else {
                txtLuckyNumber.selectedTitle = "Enter Lucky Number".localized
                constraintClearButtonWidth.constant = 0
            }
            self.view.layoutIfNeeded()
        }
        return true
    }
}

//MARK: - NavigationSetUpProtocol
extension LDCProductDescriptionViewController: NavigationSetUpProtocol {
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(popScreen), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "Lucky draw & Coupon Title".localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}
