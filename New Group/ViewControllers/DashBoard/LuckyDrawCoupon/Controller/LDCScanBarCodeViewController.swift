//
//  ScanBarCodeViewController.swift
//  OK
//
//  Created by Mohit on 2/24/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import AVFoundation
import IQKeyboardManagerSwift

protocol LDCScanBarCodeViewControllerDelegate: class {
    func changeNavigationController(controller: UIViewController, object: ScanResult?)
    func changeNavigationController(controller: UIViewController)
}

class LDCScanBarCodeViewController: OKBaseController {
    
    //MARK: - Outlets
    @IBOutlet weak var enterBarOrCouponCodeOutlet: UIButton! {
        didSet {
            self.enterBarOrCouponCodeOutlet.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.enterBarOrCouponCodeOutlet.setTitle("Enter Bar Or Coupon Code".localized, for: .normal)
        }
    }
    
    @IBOutlet weak var clickhereWithoutBarOutlet: UIButton! {
        didSet {
            self.clickhereWithoutBarOutlet.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.clickhereWithoutBarOutlet.setTitle("Click here Without Bar Code Product".localized, for: .normal)
        }
    }
    @IBOutlet weak var viewPromotionListOutlet: UIButton! {
        
        didSet {
            self.viewPromotionListOutlet.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.viewPromotionListOutlet.setTitle("View Promotion List".localized, for: .normal)
        }
        
    }
    @IBOutlet weak var scanbarOrCouponCodeoutlet: UILabel!{
        didSet {
            self.scanbarOrCouponCodeoutlet.font = UIFont(name: appFont, size: appFontSize)
            self.scanbarOrCouponCodeoutlet.text = "Scan Bar Or Coupon Code".localized
        }
    }
    @IBOutlet weak var containerViews: UIView!
    @IBOutlet weak var flashImage: UIImageView!
    @IBOutlet var scannerView: CardDesignView!
    @IBOutlet var txtContainerView: CardDesignView!
    @IBOutlet weak var myQRView: UIView! {
        didSet {
            self.myQRView.layer.cornerRadius = 20.0
            self.myQRView.layer.borderWidth = 0.5
            self.myQRView.layer.borderColor = UIColor.white.cgColor
        }
    }
    
    //MARK: - Properties
    var productDetailDataModelObj : ProductDetailModel?
    var scanBarCodeDelegate: LDCScanBarCodeViewControllerDelegate?
    var captureSession: AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    
    //Empty Rectangle with green border to outline detected QR or BarCode
    let codeFrame:UIView = {
        let codeFrame = UIView()
        codeFrame.layer.borderColor =  UIColor.init(hex: "F3C632").cgColor
        codeFrame.layer.borderWidth = 2.5
        codeFrame.frame = CGRect.zero
        codeFrame.translatesAutoresizingMaskIntoConstraints = false
        return codeFrame
    }()
    
    let jsonParserObj = RTJsonParser()
    var Responsedict = Dictionary<String,Any>()
    var DictToSend = Dictionary<String, AnyObject>()
    var scanBarCodeStr = ""
    let scanSize = CGSize(width: 200.0, height: 200.0)
    var contentW: CGFloat = 0.0
    var contentH: CGFloat = 0.0
    var DynView = UIView()
    var scanImg = UIImageView()
    var mediumInterval: TimeInterval = 0.0
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK: - Views Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        let screenSize = UIScreen.main.bounds.size
        contentW = screenSize.width
        contentH = screenSize.height
        jsonParserObj.delegate = self
        addObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        mediumInterval = 4.0
        if AVCaptureDevice.authorizationStatus(for: .video) == AVAuthorizationStatus.authorized {
            self.loadCamera()
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted) in
                switch granted {
                case true:
                    self.loadCamera()
                case false:
                    self.requestAlertForPermissions(withBody: "OK$ need camera access to scan".localized , handler: { (cancelled) in
                    })
                }
            })
            IQKeyboardManager.sharedManager().enable = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        mediumInterval = 0.0
        self.navigationController?.navigationBar.isHidden = false
        self.captureSession?.stopRunning()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Methods
    private func setUpQRMask() {
        let centerRect = CGRect(x: (contentW-scanSize.width)/2.0, y: (contentH-scanSize.height)/2.0, width: scanSize.width, height: scanSize.height)
        let path = UIBezierPath(rect: view.bounds)
        let centerPath = UIBezierPath(rect: centerRect)
        path.append(centerPath)
        path.usesEvenOddFillRule = true
        let fillLayer = CAShapeLayer()
        fillLayer.path = path.cgPath
        fillLayer.fillRule = CAShapeLayerFillRule.evenOdd
        fillLayer.fillColor = UIColor.black.withAlphaComponent(0.3).cgColor
        
        scanImg = UIImageView(frame: CGRect(x: (contentW-scanSize.width - 2)/2.0, y: (contentH-scanSize.height - 4)/2.0, width: scanSize.width + 2 , height: scanSize.height + 5 ))
        DynView = UIView(frame: CGRect(x: (contentW-scanSize.width + 20)/2.0, y: scanImg.frame.minY + 100, width: self.scanSize.width - 20, height: 1))
        DynView.backgroundColor = UIColor.red
        scanImg.image =  #imageLiteral(resourceName: "scan_bg")
        view.addSubview(scanImg)
        view.addSubview(DynView)
        view.layer.addSublayer(fillLayer)
    }
    
    private func moveTopToBottom() {
        DynView.frame = CGRect(x: (self.contentW - self.scanSize.width + 20)/2.0, y: scanImg.frame.minY + 20, width: self.scanSize.width - 20, height: 3)
        UIView.animate(withDuration: mediumInterval, animations: {
            self.DynView.frame = CGRect(x: (self.contentW - self.scanSize.width + 20)/2.0 , y: self.scanImg.frame.maxY - 20, width: self.scanSize.width - 20, height: 3)
        }, completion: { (_) in
            self.moveBottomToTop()
        })
    }
    
    private func moveBottomToTop() {
        DynView.frame =  CGRect(x: (self.contentW - self.scanSize.width + 20)/2.0 , y: scanImg.frame.maxY - 20, width: self.scanSize.width - 20 , height: 3)
        UIView.animate(withDuration: mediumInterval, animations: {
            self.DynView.frame = CGRect(x: (self.contentW - self.scanSize.width + 20)/2.0 , y: self.scanImg.frame.minY + 20, width: self.scanSize.width - 20 , height: 3)
        }, completion: { (_) in
            self.moveTopToBottom()
        } )
    }
    
    private func loadCamera() {
        DispatchQueue.global(qos: .background).async {
            let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
            if let captureDevice = captureDevice {
                do {
                    self.captureSession = AVCaptureSession()
                    // CaptureSession needs an input to capture Data from
                    let input = try AVCaptureDeviceInput(device: captureDevice)
                    self.captureSession?.addInput(input)
                    // CaptureSession needs and output to transfer Data to
                    let captureMetadataOutput = AVCaptureMetadataOutput()
                    self.captureSession?.addOutput(captureMetadataOutput)
                    //We tell our Output the expected Meta-data type
                    captureMetadataOutput.setMetadataObjectsDelegate(self , queue: DispatchQueue.main)
                    captureMetadataOutput.metadataObjectTypes = [.code128, .qr,.ean13, .ean8, .code39, .upce, .aztec, .pdf417] //AVMetadataObject.ObjectType
                    self.captureSession?.startRunning()
                    //The videoPreviewLayer displays video in conjunction with the captureSession
                    if let session = self.captureSession {
                        self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
                    }
                    self.videoPreviewLayer?.videoGravity = .resizeAspectFill
                    DispatchQueue.main.async {
                        self.videoPreviewLayer?.frame = self.scannerView.frame
                        if let viewVideo = self.videoPreviewLayer {
                            self.view.layer.addSublayer(viewVideo)
                            self.setUpQRMask()
                        }
                        self.view.bringSubviewToFront(self.containerViews)
                    }
                }
                catch {
                    println_debug("Error")
                    DispatchQueue.main.async {
                        self.removeActivityIndicator(view: self.view)
                    }
                }
            }
            
            DispatchQueue.main.async {
                self.view.bringSubviewToFront(self.containerViews)
                self.view.bringSubviewToFront(self.scannerView)
                self.view.bringSubviewToFront(self.txtContainerView)
                self.scanBarCodeStr = ""
                self.captureSession?.startRunning()
            }
        }
    }
    
    func checkCameraAccessAndOpen() {
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            self.addCameraLayer()
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    self.addCameraLayer()
                } else {
                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: "", body: "Please Allow Camera Access".localized, img: #imageLiteral(resourceName: "alert-icon"))
                        alertViewObj.addAction(title: "Setting".localized, style: .target , action: {
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                            } else {
                                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
                            }
                        })
                        alertViewObj.addAction(title: "OK".localized, style: .cancel, action: {})
                        alertViewObj.showAlert(controller: self)
                    }
                }
            })
        }
    }
    
    func addCameraLayer() {
        DispatchQueue.global(qos: .background).async {
            //AVCaptureDevice allows us to reference a physical capture device (video in our case)
            let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
            
            if let captureDevice = captureDevice {
                
                do {
                    // CaptureSession needs an input to capture Data from
                    let input = try AVCaptureDeviceInput(device: captureDevice)
                    self.captureSession = AVCaptureSession()
                    self.captureSession?.addInput(input)
                    
                    // CaptureSession needs and output to transfer Data to
                    let captureMetadataOutput = AVCaptureMetadataOutput()
                    self.captureSession?.addOutput(captureMetadataOutput)
                    
                    //We tell our Output the expected Meta-data type
                    captureMetadataOutput.setMetadataObjectsDelegate(self , queue: DispatchQueue.main)
                    captureMetadataOutput.metadataObjectTypes = [.code128, .qr,.ean13, .ean8, .code39, .upce, .aztec, .pdf417] //AVMetadataObject.ObjectType
                    
                    DispatchQueue.global(qos: .background).async {
                        self.captureSession?.startRunning()
                    }
                    
                    //The videoPreviewLayer displays video in conjunction with the captureSession
                    self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession!)
                    self.videoPreviewLayer?.videoGravity = .resizeAspectFill
                    DispatchQueue.main.async {
                        self.videoPreviewLayer?.frame = self.view.layer.bounds
                        if let vpLayer = self.videoPreviewLayer {
                            self.view.layer.addSublayer(vpLayer)
                        }
                    }
                } catch {
                    println_debug(error)
                }
            }
            
            DispatchQueue.main.async {
                self.view.bringSubviewToFront(self.containerViews)
                self.view.bringSubviewToFront(self.scannerView)
                self.view.bringSubviewToFront(self.txtContainerView)
                self.removeActivityIndicator(view: self.view)
                self.scanBarCodeStr = ""
                self.captureSession?.startRunning()
            }
        }
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported".localized, message: "Your device does not support scanning a code from an item. Please use a device with a camera.".localized, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK".localized, style: .default))
        present(ac, animated: true)
    }
    
    func generateToken() {
        if appDelegate.checkNetworkAvail() {
            progressViewObj.showProgressView()
            let hashValue = LDCHelperFile.aKey.hmac_SHA1(key: LDCHelperFile.sKey)
            let inputValue = "password=\(hashValue)&grant_type=password"
            progressViewObj.showProgressView()
            jsonParserObj.ParseMyJson(url: URL.init(string: LDCHelperFile.kTokenAPIURL)! , param: inputValue as AnyObject, httpMethod: LDCHelperFile.kMethod_Post, contentType: LDCHelperFile.kContentType_urlencoded, mScreen: "TokenForScanAPI", authStr: nil)
        } else {
            self.captureSession?.startRunning()
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshScanning), name: NSNotification.Name(rawValue: "StartScan"), object: nil)
    }
    
    func customAlert(msg:String, title:String) {
        alertViewObj.wrapAlert(title: "", body: msg.localized, img: #imageLiteral(resourceName: "dashboard_lucky_draw"))
        alertViewObj.addAction(title: "Done".localized, style: .target, action:{})
        alertViewObj.showAlert(controller: self)
    }
    
    //MARK: - Target Methods
    @objc func keyboardWillShow(notification: NSNotification) {
        self.view.layoutIfNeeded()
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.layoutIfNeeded()
    }
    
    @objc func refreshScanning(notification: NSNotification) {
        self.captureSession?.startRunning()
    }
    
    //MARK: - Button Action Methods
    @IBAction func openFlashlight(_ sender: UIButton) {
        if let device = AVCaptureDevice.default(for: AVMediaType.video) {
            if (device.hasTorch) {
                do {
                    try device.lockForConfiguration()
                    if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                        self.flashImage.isHighlighted = false
                        device.torchMode = AVCaptureDevice.TorchMode.off
                    } else {
                        self.flashImage.isHighlighted = true
                        do {
                            try device.setTorchModeOn(level: 1.0)
                        } catch {
                            println_debug(error)
                        }
                    }
                    device.unlockForConfiguration()
                } catch {
                    println_debug(error)
                }
            }
        }
    }
    
    @IBAction func withoutBarCodeClick(_ sender: UIButton) {
        self.view.endEditing(true)
        guard  let VC = self.storyboard?.instantiateViewController(withIdentifier: "LDCPromotionListViewController") as? LDCPromotionListViewController  else { return }
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func promotionListClick(_ sender: UIButton) {
        self.view.endEditing(true)
        guard let VC = self.storyboard?.instantiateViewController(withIdentifier: "LDCPromotionViewController") as? LDCPromotionViewController else { return }
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func navigateToEcoupon(_ sender: UIButton) {
        guard let eCoupon = UIStoryboard(name: "LuckyDrawCoupan", bundle: Bundle.main).instantiateViewController(withIdentifier: "LDCECoupon") as? LDCECoupon else { return }
        self.navigationController?.pushViewController(eCoupon, animated: true)
    }
    
    @IBAction func dismissAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "StartScan"), object: nil)
        self.captureSession?.stopRunning()
    }
}

//MARK: - ScanViewDelegate
extension LDCScanBarCodeViewController: ScanViewDelegate {
    func startCapture() {
        self.captureSession?.stopRunning()
        self.captureSession?.startRunning()
    }
    
    func stopCapture() {
        self.captureSession?.stopRunning()
    }
}

//MARK: - AVCaptureMetadataOutputObjectsDelegate
extension LDCScanBarCodeViewController: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession?.stopRunning()
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
       // dismiss(animated: true)
    }
    
    func found(code: String){
        println_debug(code)
        scanBarCodeStr = code
      //  self.captureSession?.stopRunning()
        if code.contains(find: "EncryptedString") &&  code.contains(find: "Ivector") && code.contains(find: "MerchantId"){
            callInvalidQR()
        }else{
            generateToken()
        }
        
        
        
    }
    
    func callInvalidQR(){
       DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: nil, body: "Invalid QR Code".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                self.captureSession?.startRunning()
            })
            alertViewObj.showAlert(controller: self)
        }
       
    }

}

//MARK: - RTJsonParserDelegate
extension LDCScanBarCodeViewController: RTJsonParserDelegate {
    func ResponseFromServer(withSuccess Success: Bool, json: AnyObject?, screen: String, Error: String?) {
        println_debug("screen PayAPI:- \(screen)")
        println_debug("Success :- \(Success)")
        if Success == false {
            DispatchQueue.main.async{
                progressViewObj.removeProgressView()
                self.captureSession?.startRunning()
            }
        }
        
        if Success == true && screen == "TokenForScanAPI" {
            let responseDic = json as! NSDictionary
            let tokentype = responseDic.value(forKey: "token_type")
            let accesstoken = responseDic.value(forKey: "access_token")
            let authorizationString =  "\(tokentype!) \(accesstoken!)"
            
            let pipeLineStr = "Longitude=0.00|Latitude=0.00|PhoneNumber=\(UserModel.shared.mobileNo)|Name=\(UserModel.shared.name)"
            println_debug("pipeline str :: \(pipeLineStr)")
            
            let hashValue: String = pipeLineStr.hmac_SHA1(key: LDCHelperFile.sKey)
            println_debug("actual hash value :: \(hashValue)")
            
            let keys : [String] = ["Longitude","Latitude","PhoneNumber","Name","HashValue"]
            let values : [String] = ["0.00","0.00",UserModel.shared.mobileNo,UserModel.shared.name,hashValue]
            
            let jsonObj = JSONStringWriter()
            let jsonText = jsonObj.writeJsonIntoString(keys, values)
            
            DispatchQueue.main.async {
                let urlStr = String.init(format: LDCHelperFile.KbarCodeAPIURL, self.scanBarCodeStr)
                if let barCodeURL = URL.init(string: urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) {
                    progressViewObj.showProgressView()
                    self.jsonParserObj.ParseMyJson(url: (barCodeURL), param: jsonText as AnyObject, httpMethod: LDCHelperFile.kMethod_Post, contentType: LDCHelperFile.kContentType_Json, mScreen: "ScanAPI", authStr: authorizationString)
                } else {
                    self.scanBarCodeStr = ""
                    self.captureSession?.startRunning()
                }
            }
        } else if Success == true && screen == "ScanAPI" {
            if let unwrappedJson = json {
                do {
                    if JSONSerialization.isValidJSONObject(json) {
                          print("Valid Json")
                        let jsonData = try JSONSerialization.data(withJSONObject: unwrappedJson, options: .prettyPrinted)
                        do {
                            let decoder = JSONDecoder()
                            let scanResponse = try decoder.decode(ScanResponseDTO.self, from: jsonData)
                            if let scanResponseResult = scanResponse.result {
                                DispatchQueue.main.async {
                                    
                                    
                                    guard let productDescriptionVC = self.storyboard?.instantiateViewController(withIdentifier: "LDCProductDescriptionViewController") as? LDCProductDescriptionViewController else { return }
                                    productDescriptionVC.screenType = ProductDescriptionScreenFrom.scanProduct
                                    productDescriptionVC.productDetailFromScan = scanResponseResult
                                    productDescriptionVC.scanTrackId = scanResponseResult.scanTrackingId ?? ""
                                    self.navigationController?.pushViewController(productDescriptionVC, animated: true)
                                    
                                    //                                self.scanBarCodeDelegate?.changeNavigationController(controller: self, object: scanResponseResult)
                                }
                            } else {
                                DispatchQueue.main.async {
                                    self.captureSession?.startRunning()
                                    alertViewObj.wrapAlert(title: "", body: scanResponse.message?.localized ?? "" , img: #imageLiteral(resourceName: "dashboard_lucky_draw"))
                                    alertViewObj.addAction(title: "OK".localized, style: .target, action:{})
                                    alertViewObj.showAlert(controller: self)
                                }
                            }
                        } catch _ {
                            scanBarCodeStr = ""
                            self.captureSession?.startRunning()
                            println_debug("JSON CONVERSION ERROR")
                        }
                      } else {
                          print("InValid Json")
                        scanBarCodeStr = ""
                        self.captureSession?.startRunning()
                      }
                   
                } catch _ {
                    scanBarCodeStr = ""
                    self.captureSession?.startRunning()
                    println_debug("object to string failed")
                }
            } else {
                scanBarCodeStr = ""
                self.captureSession?.startRunning()
            }
            DispatchQueue.main.async {
                progressViewObj.removeProgressView()
            }
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
