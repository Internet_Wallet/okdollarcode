//
//  CarnivalWheelSlice.swift
//  TTFortuneWheelSample
//
//  Created by Efraim Budusan on 11/1/17.
//  Copyright © 2017 Tapptitude. All rights reserved.
//

import Foundation
//import TTFortuneWheel

public class CarnivalWheelSlice: FortuneWheelSliceProtocol {
    
    public enum Style {
        case brickRed
        case sandYellow
        case babyBlue
        case deepBlue
        case txtColor
    }
    
    public var title: String
    
    public var degree: CGFloat = 0.0
    
    public var backgroundColor: UIColor? {
        return sliceBackgroundColor
    }
    
    public var fontColor: UIColor {
        return UIColor.black
    }
    
    public var offsetFromExterior:CGFloat {
        return 10.0
    }
    
    public var font: UIFont {
        switch style {
        default:
            if let myFont = UIFont(name: appFont, size: 14) {
                return myFont
            } else {
                return UIFont.systemFont(ofSize: 14.0)
            }
        }
    }
    
    public var stroke: StrokeInfo? {
        return StrokeInfo(color: UIColor.white, width: 1.0)
    }
    
    public var style:Style = .brickRed
    
    public var sliceBackgroundColor = UIColor.red
    
    public init(title:String) {
        self.title = title
    }
    
    public convenience init(title:String, degree:CGFloat) {
        self.init(title:title)
        self.degree = degree
    }
}
