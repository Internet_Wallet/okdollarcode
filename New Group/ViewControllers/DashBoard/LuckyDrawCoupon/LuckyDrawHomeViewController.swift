//
//  LuckyDrawHomeViewController.swift
//  OK
//
//  Created by Ashish on 2/27/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


//MARK: step 1 Add Protocol here.
protocol LuckyDrawHomeViewControllerDelegate: class {
    func searchBarTextChange(searchBar: UISearchBar, searchText: String)
    func searchBarShouldChange(searchBar: UISearchBar, range: NSRange, text: String) -> Bool
    func searchBarCancelButtonClicked(searchBar: UISearchBar)
}

protocol ScanViewDelegate: class {
    func startCapture()
    func stopCapture()
}

class LuckyDrawHomeViewController: OKBaseController {
    //MARK: - Outlets
    @IBOutlet weak var viewContainter: UIView!
    
    //MARK: - Properties
    var isSearchIconHidden = true
    weak var ldHomeDelegate: LuckyDrawHomeViewControllerDelegate?
    var scanViewDelegate: ScanViewDelegate?
    fileprivate var luckyMenu : PaytoScroller?
    fileprivate var controllerArray : [UIViewController] = []
    var searchBar = UISearchBar()
    
    //MARK: - Views Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigation()
        setupSearchBar()
        self.helpSupportNavigationEnum = .Lucky_Draw
        MyNumberTopup.theme = MyNumberTopup.OperatorColorCode.okDefault
        
        guard let barCode = self.storyboard?.instantiateViewController(withIdentifier: "LDCScanBarCodeViewController") as? LDCScanBarCodeViewController else { return }
        barCode.scanBarCodeDelegate = self
        self.scanViewDelegate = barCode as ScanViewDelegate
        barCode.title = "Scan Bar Code".localized
        
        guard let tableVC = self.storyboard?.instantiateViewController(withIdentifier: "LDCPromotionViewController") as? LDCPromotionViewController else { return }
        tableVC.navController = self.navigationController
        tableVC.homeParentVC = self
        //        self.ldHomeDelegate = tableVC
        tableVC.title = "Promotion".localized
        
        controllerArray = [barCode, tableVC]
        let parameters = [PaytoScrollerOptionMenuItemSeparatorWidth: 0,
                          PaytoScrollerOptionMenuItemSeparatorPercentageHeight: 0.1,
                          PaytoScrollerOptionScrollMenuBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectedMenuItemLabelColor: UIColor(red: 238.0/255.0, green: 165.0/255.0, blue: 46.0/255.0, alpha: 1.0),
                          PaytoScrollerOptionViewBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectionIndicatorColor: UIColor.black,
                          PaytoScrollerOptionUnselectedMenuItemLabelColor:UIColor.darkText,
                          PaytoScrollerOptionMenuHeight: 50,
                          PaytoScrollerOptionMenuItemFont: "Zawgyi-One",
                          PaytoScrollerOptionMenuItemWidth: (screenWidth / CGFloat(controllerArray.count)) - 10.0] as [String : Any]
        luckyMenu = PaytoScroller(viewControllers: controllerArray, frame: self.viewContainter.bounds, options: parameters)
        luckyMenu?.delegate = self
        self.viewContainter.addSubview(luckyMenu!.view)
        luckyMenu?.scrollingEnable(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.sharedManager().enable            = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // DispatchQueue.global(qos: .background).async {
        // self.scanViewDelegate?.startCapture()
        // }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Methods
    func setupSearchBar() {
        searchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.tintColor = UIColor.white
        searchBar.placeholder = "Search".localized
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 16) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
              
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                //searchTextField.font = myFont
                searchTextField.keyboardType = .asciiCapable
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
            }
        }
        let view = self.searchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = UIColor.gray
            }
        }
    }
    
    deinit {
        self.scanViewDelegate?.stopCapture()
    }
    
    //MARK: - Target Methods
    @objc func dismissOrBackAction() {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func showSearchOption() {
        searchBar.placeholder = "Search".localized
        searchBar.text = ""
        self.navigationItem.titleView = searchBar
        searchBar.becomeFirstResponder()
    }
}

// MARK: - UISearchBarDelegate
extension LuckyDrawHomeViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let uiButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            uiButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize) ?? UIFont.systemFont(ofSize: 14)
            if appDel.currentLanguage == "my" {
                uiButton.setTitle("မလုပ်ပါ", for:.normal)
            } else if appDel.currentLanguage == "uni" {
                uiButton.setTitle("မလုပ္ပါ", for:.normal)
            } else {
                uiButton.setTitle("Cancel".localized, for:.normal)
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.ldHomeDelegate?.searchBarTextChange(searchBar: searchBar, searchText: searchText)
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let safeDel = self.ldHomeDelegate {
            return safeDel.searchBarShouldChange(searchBar: searchBar, range: range, text: text)
        }
        return false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.ldHomeDelegate?.searchBarCancelButtonClicked(searchBar: searchBar)
        isSearchIconHidden = false
        setUpNavigation()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}

//MARK: - PaytoScrollerDelegate
extension LuckyDrawHomeViewController: PaytoScrollerDelegate {
    func willMove(toPage controller: UIViewController!, index: Int) {
        if index == 1 {
            isSearchIconHidden = false
        } else {
            isSearchIconHidden = true
        }
        setUpNavigation()
    }
    
    func didMove(toPage controller: UIViewController!, index: Int) {
        
    }
}

//MARK: - NavigationSetUpProtocol
extension LuckyDrawHomeViewController: NavigationSetUpProtocol {
    func setUpNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        if !isSearchIconHidden {
            let searchButton = UIButton(type: .custom)
            searchButton.setImage(#imageLiteral(resourceName: "r_search").withRenderingMode(.alwaysOriginal), for: .normal)
            searchButton.addTarget(self, action: #selector(showSearchOption), for: .touchUpInside)
            if  device.type == .iPhone12Pro || device.type == .iPhone12ProMax{
                searchButton.frame = CGRect(x: navTitleView.frame.size.width - 100, y: ypos, width: 30, height: 30)
            }else{
               searchButton.frame = CGRect(x: navTitleView.frame.size.width - 30, y: ypos, width: 30, height: 30)
            }
            navTitleView.addSubview(searchButton)
        }
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(dismissOrBackAction), for: .touchUpInside)
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text =  "Lucky draw & Coupon Title".localized + " "
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 21) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
}

//MARK: - LDCScanBarCodeViewControllerDelegate
extension LuckyDrawHomeViewController: LDCScanBarCodeViewControllerDelegate {
    func changeNavigationController(controller: UIViewController, object: ScanResult?) {
        guard let productDescriptionVC = self.storyboard?.instantiateViewController(withIdentifier: "LDCProductDescriptionViewController") as? LDCProductDescriptionViewController else { return }
        productDescriptionVC.screenType = ProductDescriptionScreenFrom.scanProduct
        productDescriptionVC.productDetailFromScan = object
        productDescriptionVC.scanTrackId = object?.scanTrackingId ?? ""
        self.navigationController?.pushViewController(productDescriptionVC, animated: true)
    }
    
    func changeNavigationController(controller: UIViewController) {
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "LDCPromotionListViewController") as? LDCPromotionListViewController
        self.navigationController?.pushViewController(VC!, animated: true)
    }
}
