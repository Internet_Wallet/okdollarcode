//
//  ImagePreviewVC.swift
//  OK
//
//  Created by PC on 1/31/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SDWebImage

class ImagePreviewVC: OKBaseController,UIScrollViewDelegate {

    //Manage Image Preview
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var imgViewAttachFile: UIImageView!
    var imageRotated = 0
    var strURL = ""
    var imageFile :UIImage!
    
    //MARK: - View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        
        self.title = "Attached File".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        
        if strURL.count > 0 {
            println_debug(strURL)
            strURL = strURL.replacingOccurrences(of: "%20", with: " ")
            strURL = strURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let url = URL.init(string: strURL)
            imgViewAttachFile.sd_setImage(with: url, placeholderImage: UIImage(named: "avatar"))
            imageFile = imgViewAttachFile.image
        } else {
            imgViewAttachFile.image = imageFile
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
        scrollView.delegate = self
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 10.0//maximum zoom scale you want
        scrollView.zoomScale = 1.0
        imageRotated = 0
    }
    
    @IBAction func imageShareAction(_ sender: Any) {

        let vc = UIActivityViewController(activityItems: [imageFile], applicationActivities: [])
        DispatchQueue.main.async {self.present(vc, animated: true)}
    }
    
    @IBAction func imageRotationAction(_ sender: Any) {
        imageFile = imgViewAttachFile.image

        var portraitImage  = UIImage()
        if imageRotated == 0 {
            portraitImage = UIImage(cgImage: imageFile.cgImage! ,
                                               scale: 1.0 ,
                                               orientation: UIImage.Orientation.right)
            imageRotated = 90
        } else if imageRotated == 90 {
            portraitImage = UIImage(cgImage: imageFile.cgImage! ,
                                    scale: 1.0 ,
                                    orientation: UIImage.Orientation.down)
            imageRotated = 180
        } else if imageRotated == 180 {
            portraitImage = UIImage(cgImage: imageFile.cgImage! ,
                                    scale: 1.0 ,
                                    orientation: UIImage.Orientation.left)
            imageRotated = 270
        } else {
            portraitImage = UIImage(cgImage: imageFile.cgImage! ,
                                    scale: 1.0 ,
                                    orientation: UIImage.Orientation.up)
            imageRotated = 0
        }
        
        imgViewAttachFile.image = portraitImage
        //imgViewAttachFile.transform = imgViewAttachFile.transform.rotated(by: .pi/2)
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    //zoom in/out image for attach file
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imgViewAttachFile
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

