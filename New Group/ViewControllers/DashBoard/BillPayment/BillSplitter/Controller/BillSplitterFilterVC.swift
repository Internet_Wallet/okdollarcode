//
//  BillSplitterFilterVC.swift
//  OK
//
//  Created by OK$ on 03/12/2019.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

@objc protocol BillSplitterFilterDelegate: class {
    @objc optional func filterDataList(rowData: String)
}

class BillSplitterFilterVC: OKBaseController {
    
    var filterDelegate: BillSplitterFilterDelegate?

    var arrFilters: [String] = ["Personal", "Automotive"]
    var arrList: [String] = ["Personal", "Automotive"]

    @IBOutlet weak var tblList: UITableView!
    //Show check box
    var strCheckBoxTitle = "Default".localized

    //
       // MARK: - Data
       //
       
       struct Section {
           var name: String!
           var items: [String]!
           var collapsed: Bool!
           
           init(name: String, items: [String], collapsed: Bool = false) {
               self.name = name
               self.items = items
               self.collapsed = collapsed
           }
       }
       
       var sections = [Section]()
    
        // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
             
        self.title = "Filter By".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        
        arrFilters = Array(Set(arrList))
        println_debug(arrFilters)

        sections = [
            Section(name: "Type".localized, items: ["Normal"]),
            Section(name: "Categories".localized, items: arrFilters)
        ]
        
        tblList.reloadData()
    }
    
       @IBAction func backAction(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - extension of Table View

extension BillSplitterFilterVC : UITableViewDataSource,UITableViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      
        if indexPath.section == 0 {
            return tableView.rowHeight
        }
        // Calculate the real section index and row index
        let section = getSectionIndex(indexPath.row)
        let row = getRowIndex(indexPath.row)
        
        // Header has fixed height
        if row == 0 {
            return 50.0
        }
        
        return sections[section].collapsed! ? 0 : 44.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            if section == 0 {
                return 1
            }
                // For section 1, the total count is items count plus the number of headers
                var count = sections.count
                
                for section in sections {
                    count += section.items.count
                }
                
                return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "title") as UITableViewCell?
                cell?.textLabel?.text = "Default".localized
                cell?.textLabel?.font = UIFont(name: appFont, size: 17.0)
                return cell!
            }
            
            // Calculate the real section index and row index
            let section = getSectionIndex(indexPath.row)
            let row = getRowIndex(indexPath.row)
            
            if row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! HeaderCell
                cell.titleLabel.text = sections[section].name.localized
                cell.toggleButton.tag = section
                cell.toggleButton.setImage(sections[section].collapsed! ?   #imageLiteral(resourceName: "downArrow") :   #imageLiteral(resourceName: "upArrow"), for: UIControl.State())
                cell.toggleButton.addTarget(self, action: #selector(self.toggleCollapse), for: .touchUpInside)
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SelectionCell") as! SelectionCell
                cell.titleLabel.text = sections[section].items[row - 1].localized
                
                if (strCheckBoxTitle == sections[section].items[row - 1])
                {
                    cell.imgCheck.isHidden = false
                }
                else
                {
                    cell.imgCheck.isHidden = true
                }
                return cell
            }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.selectedRowAction(indexPath: indexPath)
    }
    
    fileprivate func commmonNotification(sortBy : String ,sortType : String)
    {
        self.filterDelegate?.filterDataList?(rowData: sortType)
        self.navigationController?.popViewController(animated: true)
    }
    
   
    fileprivate func selectedRowAction(indexPath : IndexPath)
    {
            
            if indexPath.section == 0
            {
                println_debug("Default")
                self.commmonNotification(sortBy: "filterBy", sortType: "Default" )
            }
            
            let section = getSectionIndex(indexPath.row)
            let row = getRowIndex(indexPath.row)
            
            if row == 0 {
                println_debug("Header selected Type & Categories")
            }
            else
            {
                strCheckBoxTitle = sections[section].items[row - 1]
                
                if (sections[section].items[row - 1] == "Normal")
                {
                    self.commmonNotification(sortBy: "filterBy", sortType: "Normal" )
                }
                else
                {
                    self.commmonNotification(sortBy: "filterBy", sortType: sections[section].items[row - 1] )
                }
                
            }
        
    }
    
    //
    // MARK: - Event Handlers
    //
    @objc func toggleCollapse(_ sender: UIButton) {
        let section = sender.tag
        let collapsed = sections[section].collapsed
        
        // Toggle collapse
        sections[section].collapsed = !collapsed!
        
        let indices = getHeaderIndices()
        
        let start = indices[section]
        let end = start + sections[section].items.count
        
        tblList.beginUpdates()
        for i in start ..< end + 1 {
            tblList.reloadRows(at: [IndexPath(row: i, section: 1)], with: .automatic)
        }
        tblList.endUpdates()
    }
    
    //
    // MARK: - Helper Functions
    //
    func getSectionIndex(_ row: NSInteger) -> Int {
        let indices = getHeaderIndices()
        
        for i in 0..<indices.count {
            if i == indices.count - 1 || row < indices[i + 1] {
                return i
            }
        }
        
        return -1
    }
    
    func getRowIndex(_ row: NSInteger) -> Int {
        var index = row
        let indices = getHeaderIndices()
        
        for i in 0..<indices.count {
            if i == indices.count - 1 || row < indices[i + 1] {
                index -= indices[i]
                break
            }
        }
        
        return index
    }
    
    func getHeaderIndices() -> [Int] {
        var index = 0
        var indices: [Int] = []
        
        for section in sections {
            indices.append(index)
            index += section.items.count + 1
        }
        
        return indices
    }
    
    func arrFilterData(arrData: [String]) {
        
        self.arrFilters = Array(Set(arrData))
        
        //self.arrFilters = arrData
        DispatchQueue.main.async {
            
            self.sections = [
                Section(name: "Type".localized, items: ["Normal"]),
                Section(name: "Categories".localized, items: self.arrFilters)
            ]
            self.tblList.reloadData()
        }
    }
    
    func didNeedToFilter(_ isHide: Bool) {
        //self.isHide = isHide
    }

    
}
