//
//  BillSplitterSuccessfulVC.swift
//  OK
//
//  Created by PC on 10/28/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BillSplitterSuccessfulVC: OKBaseController,WebServiceResponseDelegate {
    
    @IBOutlet weak var tblList: UITableView!
    
    var arrMainList = [BillSplitterAllRequestModel]()
    var arrFilters: [String] = []

    let myNotification = Notification.Name(rawValue:"SuccessfullNotification")
    
    var arrPendingList = [BillSplitterAllRequestModel]()
    var arrDatePendingList = [BillSplitterAllRequestModel]()

    var refreshControl = UIRefreshControl()
    let noDataLabel = UILabel()
    private var multiButton : ActionButton?
    var strFilterTitle = "Default"
    var strSortTitle = "Default"

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.helpSupportNavigationEnum = .Bill_Splitter
        noDataLabel.isHidden = true

        // Do any additional setup after loading the view.
        
        let nc = NotificationCenter.default
        nc.addObserver(forName:myNotification, object:nil, queue:nil, using:successSearchNotification)
        self.getDataFromTheServer()
        
        tblList.refreshControl = self.refreshControl
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tblList.addSubview(refreshControl)
    }
    
    @objc func refresh(sender:AnyObject)
    {
        
        print("Successful refresh called----")
        self.filterDataList(rowData: "Default")
        self.sortDataList(rowData: "Default")
        
    
        let nc = NotificationCenter.default
               
        nc.post(name:Notification.Name(rawValue:"SuccessImageNotification"),
                           object: nil,
                           userInfo: ["search": "HideSearch"])
        
        self.getDataFromTheServer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        println_debug("BillSplitterSuccessfulVC-viewWillAppear")
     
        let nc = NotificationCenter.default
        if(arrMainList.count > 0)
        {
            nc.post(name:Notification.Name(rawValue:"SuccessImageNotification"),
                    object: nil,
                    userInfo: ["search": "Search"])
        }
        else
        {
            nc.post(name:Notification.Name(rawValue:"SuccessImageNotification"),
                    object: nil,
                    userInfo: ["search": "HideSearch"])
        }
    }
    
    func successSearchNotification(notification:Notification) -> Void {
        //println_debug("successSearchNotification")
        
        //Array date By
        if ((notification.userInfo?["dateBy"] as? String) != nil)
        {
            let fulldate = notification.userInfo?["dateBy"] as? String
            let arrayDate = fulldate?.components(separatedBy: "&&")
            let startdate = "\(arrayDate![0])"
            let enddate = "\(arrayDate![1])"
            
            print("successstartdate----\(startdate)")
            print("successenddate----\(enddate)")
            
            arrPendingList.removeAll()
            arrDatePendingList.removeAll()

            // Set date format
            let dateFmt = DateFormatter()
            dateFmt.calendar = Calendar(identifier: .gregorian)
            dateFmt.dateFormat =  "MM/dd/yyyy"
            
            // Get NSDate for the given string
            let dateFrom = dateFmt.date(from: startdate)
            let dateTo = dateFmt.date(from: enddate)
            
            var i = 0
            
            while i < (arrMainList.count)
            {
                let modelObj = arrMainList[i]
                
                let dateString = modelObj.dateTime
                
                let dateFormatter = DateFormatter()
                dateFormatter.calendar = Calendar(identifier: .gregorian)
                dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
                let date: Date? = dateFormatter.date(from: dateString)
                dateFormatter.dateFormat =  "MM/dd/yyyy"
                
                let FormateStr = dateFormatter.string(from: date!)
                let dateTest = dateFmt.date(from: FormateStr)
                
                if(dateFrom == dateTo)
                {
                    switch dateFrom?.compare(dateTest!) {
                    case .orderedAscending?     :
                        println_debug("Date added from")
                    case .orderedDescending?    :
                        println_debug("Date added from")
                    case .orderedSame?          :
                        println_debug("The two dates are the same")
                        arrPendingList.append(modelObj)
                    case .none:
                        println_debug("none")
                    }
                }
                else
                {
                    //println_debug("From\(String(describing: dateFrom))&&To\(String(describing: dateTo))&&Current\(String(describing: dateTest))")
                    
                    if(dateFrom?.compare(dateTest!) == ComparisonResult.orderedSame)
                    {
                        println_debug("Same Date with From")
                        arrPendingList.append(modelObj)
                    }
                    else if(dateFrom?.compare(dateTest!) == ComparisonResult.orderedAscending && dateTo?.compare(dateTest!) == ComparisonResult.orderedDescending)
                    {
                        //Current date is greater than from
                        println_debug("Ascending Date with From")
                        arrPendingList.append(modelObj)
                    }
                    else if(dateTo?.compare(dateTest!) == ComparisonResult.orderedSame)
                    {
                        println_debug("Same Date with To")
                        arrPendingList.append(modelObj)
                    }
                    
                }
                arrDatePendingList = arrPendingList
                print("arrSuccessList=====\(arrPendingList.count)------\(arrDatePendingList.count)")
                tblList.dataSource = self
                tblList.delegate = self
                tblList.reloadData()
                
                i = i + 1
            }
            
            if (arrPendingList.isEmpty)
            {
                tblList.reloadData()
            } else {
                
                DispatchQueue.main.async {
                               self.tblList.reloadData()
                               let index = IndexPath(row: 0, section: 0)
                               self.tblList.scrollToRow(at: index, at: .top, animated: true)
                               UIView.animate(withDuration: 0.5, animations: {
                                   self.view.layoutIfNeeded()
                               })
                           }
            }
        }
            
            //Reset reload all data
        else if (notification.userInfo?["reset"] as? String == "yes")
        {
            arrPendingList.removeAll()
            arrDatePendingList.removeAll()

            arrPendingList = arrMainList
            arrDatePendingList = arrMainList
            tblList.reloadData()
        }
            //Cancel reload all data
        else if (notification.userInfo?["cancel"] as? String == "yes")
        {
            arrPendingList.removeAll()
            arrDatePendingList.removeAll()

            arrPendingList = arrMainList
            arrDatePendingList = arrMainList
            tblList.reloadData()
        }
        else if ((notification.userInfo?["search"] as? String)?.count ?? 0 > 0)
        {
            guard let text = notification.userInfo?["search"] as? String else { return }
            print ("search: \(text)")
            self.tblList.backgroundView = nil

            if(text.count == 0)
            {
                //arrPendingList = arrMainList
                tblList.reloadData()
            }
            else
            {
                var arrSearchList = [BillSplitterAllRequestModel]()
                
                 if(arrPendingList.count == 0){
                        arrSearchList = arrMainList
                    }else {
                        arrSearchList = arrPendingList
                    }
                
                arrPendingList.removeAll()
                
                var i = 0
                
                while i < (arrSearchList.count)
                {
                    let modelObj = arrSearchList[i]
                    
                    if ((modelObj.name.localizedCaseInsensitiveContains(text)) || (modelObj.requestType.localizedCaseInsensitiveContains(text)) || (modelObj.category.localizedCaseInsensitiveContains(text)))
                    {
                        arrPendingList.append(modelObj)
                        
                        tblList.reloadData()
                    }
                    i = i + 1
                }
                
//                if arrPendingList.count == 0 && text.count>0{
//                    self.tblList.separatorStyle = .none
//                    tblList.reloadData()
//                }
                
                        if (arrPendingList.isEmpty)
                               {
                                   //println_debug("Search Results empty & search text also empty")
                                   self.tblList.separatorStyle = .none
                                   tblList.reloadData()
                                   
                               } else {
                                   
                                   DispatchQueue.main.async {
                                                  self.tblList.reloadData()
                                                  let index = IndexPath(row: 0, section: 0)
                                                  self.tblList.scrollToRow(at: index, at: .top, animated: true)
                                                  UIView.animate(withDuration: 0.5, animations: {
                                                      self.view.layoutIfNeeded()
                                                  })
                                              }
                               }
            }
        } else {
            
            print("Zero")
            arrPendingList.removeAll()
            arrDatePendingList.removeAll()

            arrPendingList = arrMainList
            arrDatePendingList = arrMainList
            tblList.reloadData()
        }
    }

    func getDataFromTheServer() {
        
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            showProgressView()

            let buldApi = Url.billSplitterAllRequest + "MobileNumber=\(UserModel.shared.mobileNo)&Simid=\(UserModel.shared.simID)&MSID=01&OSType=1&OTP=\(uuid)&RequestedMoneyStatus=Accepted&RequestType=Normal&Limit=25&OffSet=0&RequestModuleType=1"
            
            let url = getUrl(urlStr: buldApi, serverType: .serverApp)
            println_debug(url)
            
            let params = Dictionary<String,String>()
            
            web.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", mScreen: "mBillSplitterSuccess")
        }
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        self.removeProgressView()

        if screen == "mBillSplitterSuccess" {
            
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dataDict = dict as? Dictionary<String,AnyObject> {
                        println_debug(dataDict)
                        
                        if let dicData = dataDict["Data"] as? String {
                            let arrData = OKBaseController.convertToArrDictionary(text: dicData)
                            
                            if arrData == nil { return }
                            
                            arrPendingList.removeAll()
                            arrMainList.removeAll()
                            arrDatePendingList.removeAll()

                            for element in arrData! {
                                
                                println_debug(element)
                                let notificationModel =  BillSplitterAllRequestModel()
                                notificationModel.id = "\(element["RequestId"]! ?? "")"
                                if !(element["AgentName"] is NSNull) {
                                    notificationModel.name = "\(element["AgentName"]! ?? "")"
                                    if(notificationModel.name == "Not Exist")
                                    {
                                        notificationModel.name = "Unknown"
                                    }
                                }
                                else
                                {
                                    notificationModel.name = "Unknown"
                                    
                                }
                                
                                notificationModel.remindCount = "\(element["RemindCount"]! ?? "")"
                                
                                if !(element["CategoryName"] is NSNull) {
                                    notificationModel.category = "\(element["CategoryName"]! ?? "")"
                                }
                                else
                                {
                                    notificationModel.category = "Not Registered"
                                }
                                arrFilters.append(notificationModel.category)
                                notificationModel.requestType = "OK$ Money"
                                notificationModel.number = "\(element["Destination"]! ?? "")"
                                notificationModel.localTransactionType = "\(element["LocalTransactionType"]! ?? "")"
                                notificationModel.commanTransactionType = "\(element["CommanTransactionType"]! ?? "")"

                                if let ary = element["AttachmentPath"] as? [String] {
                                    if(ary.count > 0)
                                    {
                                        let x: String = ary[0]
                                        println_debug(x)
                                        notificationModel.attachedFile = "Yes"
                                        notificationModel.attachedFileURL = "\(x)"
                                    }
                                    else{
                                        notificationModel.attachedFile = "No"
                                    }
                                }
                                let intdata = "\(element["Amount"]! ?? "")"
                                notificationModel.amount = Int(intdata)!
                                notificationModel.dateTime = "\(element["Scheduledtime"]! ?? "")"
                                arrMainList.append(notificationModel)
                            }
                            
                            //println_debug("arrMainList - %@",arrMainList)
                            DispatchQueue.main.async {
                            let nc = NotificationCenter.default
                            if(self.arrMainList.count > 0)
                            {
                                if(self.arrMainList.count > 1) {
                                    self.loadFloatButtons()
                                }
                                nc.post(name:Notification.Name(rawValue:"SuccessImageNotification"),
                                        object: nil,
                                        userInfo: ["search": "Search"])
                                self.tblList.separatorStyle = .none
                                self.tblList.backgroundView = nil
                            }
                            else
                            {
                                nc.post(name:Notification.Name(rawValue:"SuccessImageNotification"),
                                        object: nil,
                                        userInfo: ["search": "HideSearch"])
                                self.tblList.separatorStyle = .none

                            }
                                self.arrPendingList = self.arrMainList
                                self.arrDatePendingList = self.arrMainList
                               // print("BillSuccessful Count---\(self.arrMainList.count)")
                                self.tblList.reloadData()
                                self.refreshControl.endRefreshing()
                            }
                        }
                    }
                    
                }
            } catch {
                
            }
        }
        
        //mBillSplitterCancel
        if (screen == "mBillSplitterSuccessCancel")
        {
            
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dataDict = dict as? Dictionary<String,AnyObject> {
                        println_debug(dataDict)
                        if dataDict["Code"]! as! Int == 200
                        {
                            self.getDataFromTheServer()
                            
                            DispatchQueue.main.async {
                                
                                alertViewObj.wrapAlert(title: nil, body:"Bill Splitter Request Cancelled Successfully.".localized, img: #imageLiteral(resourceName: "bill_splitter"))
                                
                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                    
                                }
                                alertViewObj.showAlert(controller: self)
 
                            }
                        }
                        else{
                           DispatchQueue.main.async {

                            alertViewObj.wrapAlert(title: nil, body:"\(dataDict["Msg"]!)".localized, img: #imageLiteral(resourceName: "bill_splitter"))
                            
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                
                            }
                            alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                    
                }
            } catch {
                
            }
        }
        //mBillSplitterReminder
        if (screen == "mBillSplitterSuccessReminder")
        {
            
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dataDict = dict as? Dictionary<String,AnyObject> {
                        println_debug(dataDict)
                        
                        if dataDict["Code"]! as! Int == 200
                        {
                            self.getDataFromTheServer()
                            
                            DispatchQueue.main.async {
                              
                            alertViewObj.wrapAlert(title: nil, body:"Bill Splitter Reminder Successfully.".localized, img: #imageLiteral(resourceName: "bill_splitter"))
                            
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                
                            }
                            alertViewObj.showAlert(controller: self)
 
                            }
                        }
                        else if dataDict["Code"]! as! Int == 311
                        {
                           
                            alertViewObj.wrapAlert(title: nil, body:"You can not request to same destination mobile number with in 5 minutes.".localized, img: #imageLiteral(resourceName: "bill_splitter"))
                                
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                    
                            }
                            alertViewObj.showAlert(controller: self)
                            
                        }
                        else{
                           
                            alertViewObj.wrapAlert(title: nil, body:"\(dataDict["Msg"]!)".localized, img: #imageLiteral(resourceName: "bill_splitter"))
                            
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                
                            }
                            alertViewObj.showAlert(controller: self)

                        }
                    }
                }
            } catch {
                
            }
        }
    }


    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

}

extension BillSplitterSuccessfulVC : BillSplitterFilterDelegate
{
    func filterDataList(rowData: String)   {
        strFilterTitle = rowData

        if (rowData == "Normal")
        {
            arrPendingList.removeAll()
            arrPendingList = arrDatePendingList
            
            //arrPendingList = arrMainList
            tblList.reloadData()
        }
        else if (rowData == "Default")
        {
            arrPendingList.removeAll()
            arrPendingList = arrDatePendingList
            
            //arrPendingList = arrMainList
            tblList.reloadData()
        }
        else{
            let text = rowData
            print ("search: \(String(describing: text))")
            
            arrPendingList.removeAll()
            
            var i = 0
            
            while i < (arrDatePendingList.count)
            {
                let modelObj = arrDatePendingList[i]
                
                if (modelObj.category.localizedCaseInsensitiveContains(text))
                {
                    arrPendingList.append(modelObj)
                    
                    tblList.reloadData()
                }
                i = i + 1
            }
            
            if (arrPendingList.isEmpty) && !text.isEmpty
            {
                tblList.reloadData()
            } else {
            
            self.tblList.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
            DispatchQueue.main.async {
                let indexPath = IndexPath(row: 0, section: 0)
                self.tblList.scrollToRow(at: indexPath, at: .top, animated: true)
            }
            }
        }
    }
}

extension BillSplitterSuccessfulVC : BillSplitterSortDelegate
{
    func sortDataList(rowData: String) {
        
            
        println_debug(rowData)
        
//        arrPendingList.removeAll()
//        arrPendingList = arrDatePendingList

        strSortTitle = rowData
        //["Default","Amount High to Low","Amount Low to High","Name A to Z","Name Z to A"]

        if (rowData == "Default")
        {
            //arrPendingList = arrMainList
            tblList.reloadData()

        }
        else if(rowData == "Amount High to Low")
        {
            let sortedArray: [BillSplitterAllRequestModel] = self.arrPendingList.sorted{ $0.amount > $1.amount }
            if sortedArray.count > 0 {
                arrPendingList = sortedArray
            }
        }
        else if(rowData == "Amount Low to High")
        {            
            let sortedArray: [BillSplitterAllRequestModel] = self.arrPendingList.sorted{ $0.amount < $1.amount }
            if sortedArray.count > 0 {
                arrPendingList = sortedArray
            }
        }
        else if(rowData == "Name A to Z")
        {
            arrPendingList = arrPendingList.sorted(by: { (amount1: BillSplitterAllRequestModel, amount2: BillSplitterAllRequestModel) -> Bool in
                return amount1.name < amount2.name
            })
        }
        else if(rowData == "Name Z to A")
        {
            arrPendingList = arrPendingList.sorted(by: { (amount1: BillSplitterAllRequestModel, amount2: BillSplitterAllRequestModel) -> Bool in
                return amount1.name > amount2.name
            })
        }
            
        if (arrPendingList.count > 0)
        {
            DispatchQueue.main.async {
                self.tblList.reloadData()
                let index = IndexPath(row: 0, section: 0)
                self.tblList.scrollToRow(at: index, at: .top, animated: true)
                UIView.animate(withDuration: 0.5, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
            
        
    }
}


extension BillSplitterSuccessfulVC : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 285.0;//Choose your custom row height
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
         noDataLabel.frame = CGRect(x: 0, y: tableView.frame.origin.y + 15, width: screenWidth, height: 40)

     if self.arrPendingList.count == 0 {
            noDataLabel.isHidden = false
            noDataLabel.text = "No records found!".localized
            noDataLabel.textAlignment = .center
            noDataLabel.font = UIFont(name: appFont, size: 18)
           self.view .addSubview(noDataLabel)
        } else {
                noDataLabel.isHidden = true
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPendingList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "RequestModelCell"
        var cell: RequestModelCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? RequestModelCell
        if cell == nil {
            tableView.register(UINib(nibName: "RequestModelCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? RequestModelCell
        }

        var notificationModel: BillSplitterAllRequestModel!
        notificationModel = arrPendingList[indexPath.row]
        cell.nameLabel.text = notificationModel.name
        cell.lblStatus.text = "Success".localized
        cell.lblStatus.textColor = UIColor.green
        cell.categoriesLabel.text = notificationModel.category
      
        cell.requestTypeLabel.text = "OK$ Money".localized
        cell.numberLabel.text = self.getActualNum(notificationModel.number,withCountryCode: "+95")//remove 0095 with 0
        cell.attachedFileLabel.text = notificationModel.attachedFile
        cell.amountLabel.text = self.getDigitDisplay("\(notificationModel.amount)") + " MMK".localized

        cell.dateTimeLabel.text = self.getDisplayDate(notificationModel.dateTime)//notificationModel.dateTime

        if (notificationModel.attachedFile == "Yes")
        {
            let fulldate = "\(notificationModel.attachedFileURL ?? "")"//notificationModel.attachedFileURL  as? String
            let url = URL(string: fulldate)
            cell.attachFileImage.sd_setImage(with: url, placeholderImage: UIImage(named: "avatar"))
            
        }
        else
        {
            //cell.attachFileImage.image = UIImage(named: "avatar")
            cell.attachFileImage.sd_setImage(with: self.getImageURLFromString(notificationModel.profileImage), placeholderImage: UIImage(named: "avatar"))
        }
        cell.attachFileImage.layer.cornerRadius =  cell.attachFileImage!.frame.size.width/2
        cell.attachFileImage.layer.masksToBounds = true
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var notificationModel: BillSplitterAllRequestModel!
        notificationModel = arrPendingList[indexPath.row]
        let nc = NotificationCenter.default

        if (notificationModel.attachedFile == "Yes")
        {
            nc.post(name:Notification.Name(rawValue:"SuccessImageNotification"),
                    object: nil,
                    userInfo: ["image": "\(notificationModel.attachedFileURL ?? "")"])
        }
       
    }
    
    // MARK: - Custom Methods
         func loadFloatButtons() {
             var buttonItems = [ActionButtonItem]()
             let date = ActionButtonItem(title: "Date".localized, image: #imageLiteral(resourceName: "receiptSortDate"))
             date.action = { item in
                 self.multiButton?.toggleMenu()
                 let dateByFilterVC = UIStoryboard(name: "BillSplitterView", bundle: Bundle.main).instantiateViewController(withIdentifier: "DateByFilterVC") as! DateByFilterVC
                dateByFilterVC.intSearchStatus = 1
                  self.navigationController?.pushViewController(dateByFilterVC, animated: true)
             }
             
             let filter = ActionButtonItem(title: "Filter".localized, image: #imageLiteral(resourceName: "offer_report_menu_icon_filter"))
            filter.action = { item in
                self.multiButton?.toggleMenu()
                
                if self.arrDatePendingList.count > 0 {

                let dateByFilterVC = UIStoryboard(name: "BillSplitterView", bundle: Bundle.main).instantiateViewController(withIdentifier: "BillSplitterFilterVC") as! BillSplitterFilterVC
                dateByFilterVC.arrList = self.arrFilters
                dateByFilterVC.filterDelegate = self
                dateByFilterVC.strCheckBoxTitle = self.strFilterTitle
                self.navigationController?.pushViewController(dateByFilterVC, animated: true)
                    
                }
            }
             
          let sort = ActionButtonItem(title: "Sort".localized, image: #imageLiteral(resourceName: "offer_report_menu_icon_sort"))
            
          sort.action = { item in
              self.multiButton?.toggleMenu()
            
            if self.arrPendingList.count > 0 {
              let dateByFilterVC = UIStoryboard(name: "BillSplitterView", bundle: Bundle.main).instantiateViewController(withIdentifier: "BillSplitterSortVC") as! BillSplitterSortVC
              dateByFilterVC.sortDelegate = self
            dateByFilterVC.strCheckBoxTitle = self.strSortTitle
              self.navigationController?.pushViewController(dateByFilterVC, animated: true)
            }
            
          }
          
             buttonItems = [filter, sort, date]
             
             multiButton = ActionButton(attachedToView: self.view, items: buttonItems)
              multiButton?.notifyBlurDelegate = self as? NotifyBlurDelegate
             multiButton?.action = {
                 button in button.toggleMenu()
             }
             multiButton?.setImage(#imageLiteral(resourceName: "menu_white_payto"), forState: .normal)
             multiButton?.backgroundColor = kYellowColor
         }
}
