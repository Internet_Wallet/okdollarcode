 //
//  BillSpiltterView.swift
//  OK
//
//  Created by PC on 10/25/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import AVFoundation

public enum ImageFormat{
    case png
    case jpeg(CGFloat)
}

 class BillSplitterView: OKBaseController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate,UIScrollViewDelegate,WebServiceResponseDelegate, BioMetricLoginDelegate  {
    
    var focusedCell = [Int]()
    //Manage Image Preview
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var imgViewAttachFile: UIImageView!
    @IBOutlet weak var viewImage: UIView!
    
    //Manage Contact Suggestion
    var contactSuggessionView   : ContactSuggestionVC?
    var keyboardHeight : CGFloat?
    
    var duplicateBool = false
    //
    @IBOutlet weak var heightBtnSend: NSLayoutConstraint!
    @IBOutlet weak var btnSend: UIButton!
    {
        didSet
        {
            self.btnSend.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.btnSend.setTitle(self.btnSend.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var viewImageSelection: UIView!
    
    //Related with Top View
    @IBOutlet weak var viewContactSelection: UIView!
    @IBOutlet weak var viewAllRequest: UIView!
    @IBOutlet weak var viewSelectionOption: UIView!
    @IBOutlet weak var viewTop: UIView!
    var imageFile: UIImage? = nil
        
    let imagePicker = UIImagePickerController()
    
    @IBOutlet weak var contactListButton : UIButton!
        {
        didSet
        {
            self.contactListButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.contactListButton.setTitle(self.contactListButton.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var favListButton : UIButton!
    {
        didSet
        {
            self.favListButton.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.favListButton.setTitle(self.favListButton.titleLabel?.text?.localized, for: .normal)
        }
    }
    //Manage splitter list
    @IBOutlet weak var tblPerson: UITableView!
    
    var convertedImage = [String]()
    
    var arrAttachFiles = [String]()
    var arrAmounts = [Int]()
    var arrContacts = [String]()
    var arrUserData = [[String: String]]()

    var amount = 0
    var isFileAttached = 0
    var intStatusContact = 1
    var intContactSelectionRow :IndexPath = []
    var intSuggestionSelectionRow :IndexPath = []

    var strContactName = ""
    var strContactNumber = ""
    var strEnterAmountValue = ""

    var imgURL = ""
    //Validation for Mobile numbers prefix
    let validObj  = PayToValidations()

    //Display number of persons
    var numberPerson: Int = 1
    var numberOfSection: Int = 1
    
    
    var isComingFromMin = false

    func hidePransparentView()
    {
        //Hidden all optional views
        viewImageSelection.isHidden = true
        viewSelectionOption.isHidden = true
        viewAllRequest.isHidden = true
        viewContactSelection.isHidden = true
    }
    // MARK: - View Life Cycle

    override func viewWillAppear(_ animated: Bool) {
        //heightBtnSend.constant = 0
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = UIColor.red
        statusBarView.backgroundColor = statusBarColor
        self.view.addSubview(statusBarView)
        
        self.hidePransparentView()
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(changeTitle(_:)), name: Notification.Name("ChangeTitle"), object: nil)
        
        self.title = "Bill Splitter".localized
        
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    @objc func changeTitle(_ notification : Notification) {
          self.title = "Bill Splitter".localized
          self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
       }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //"{'Name':'Efjcejefj','ShortDescription':'Csjefej','FullDescription':'It's black color' XXL size, man'}"
        let strData = "{'Name':'Efjcejefj','ShortDescription':'Csjefej','FullDescription':'It's black color' XXL size, man'}"
        self.loadContactSuggessionView()
     
        //Manage Keyboard
        IQKeyboardManager.sharedManager().enable            = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
        //Preview Attached Image
        viewImage.isHidden = true
        
        scrollView.delegate = self
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 10.0//maximum zoom scale you want
        scrollView.zoomScale = 1.0
        
        //Set Delegate
        imagePicker.delegate = self

        // Do any additional setup after loading the view.
        arrAttachFiles = ["0","0","0","0","0","0"]
        arrAmounts = [0,0,0,0,0,0]
        arrContacts = ["0","0","0","0","0","0"]
        arrUserData = [["Mobile": "", "Name": "Unknown"], ["Mobile": "", "Name": "Unknown"],["Mobile": "", "Name": "Unknown"],["Mobile": "", "Name": "Unknown"],["Mobile": "", "Name": "Unknown"],["Mobile": "", "Name": "Unknown"]]
        
        //Hidden all optional views
        self.hidePransparentView()
        
        btnSend.isHidden = true
        heightBtnSend.constant = 0.0
        //Handled Attach file selection
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.tranparentViewHideAction(sender:)))
        viewImageSelection.addGestureRecognizer(gesture)
        for case let button as UIButton in viewAllRequest.subviews
        {
            button.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
            button.setTitle(button.titleLabel?.text?.localized, for: .normal)
        }
        
    }
    
    func genericClass(url: URL, param: AnyObject, httpMethod: String, handle :@escaping (_ result: Any, _ success: Bool) -> Void ) {
        let session             = URLSession.shared
        let request             = NSMutableURLRequest(url:url)
        request.timeoutInterval = 60
        request.cachePolicy     = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.capitalized
        
        if httpMethod == "POST" {
            let theJSONData = try? JSONSerialization.data(
                withJSONObject: param ,
                options: JSONSerialization.WritingOptions(rawValue: 0))
            let jsonString          = NSString(data: theJSONData!,
                                               encoding: String.Encoding.ascii.rawValue)
            let postLength = NSString(format:"%lu", jsonString!.length) as String
            request.setValue(postLength, forHTTPHeaderField:"Content-Length")
            request.httpBody = jsonString!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion:true)
        }
        
        let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if((error) != nil) {
                handle(error!.localizedDescription, false)
            }else {
                handle(data as Any, true)
            }
        }
        dataTask.resume()
    }
    
    //zoom in/out image for attach file
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imgViewAttachFile
    }
    
    @objc func tranparentViewHideAction(sender : UITapGestureRecognizer) {
        // Do what you want
        viewImageSelection.isHidden = true
        viewSelectionOption.isHidden = true
        viewAllRequest.isHidden = true
        viewContactSelection.isHidden = true
        viewImage.isHidden = true

        self.navigationController?.navigationBar.isUserInteractionEnabled = true
    }
    
    @IBAction func photoFromLibrary(_ sender: Any) {
        let imagePickerPhoto = UIImagePickerController()
        imagePickerPhoto.delegate = self
        imagePickerPhoto.allowsEditing = false
        imagePickerPhoto.sourceType = .photoLibrary
        imagePickerPhoto.mediaTypes = ["public.image"]
        imagePickerPhoto.modalPresentationStyle = .fullScreen
        present(imagePickerPhoto, animated: true, completion: nil)
        
    }
    
    @IBAction func shootPhoto(_ sender: Any) {
        
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            println_debug("already authorized")
            self.openCamera()
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    println_debug("access allowed")
                    self.openCamera()
                } else {
                    println_debug("access denied")
                    
                    alertViewObj.wrapAlert(title: nil, body:"Please allow camera access".localized, img: #imageLiteral(resourceName: "bill_splitter"))
                    
                    alertViewObj.addAction(title: "Settings".localized, style: .target, action: {
                        
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
                        }
                    
                    })
                    
                    alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                       
                    }
                    
                    DispatchQueue.main.async {
                    alertViewObj.showAlert(controller: self)
                    }
                }
            })
        }
        
    }
    
    func openCamera(){

        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePickerCamera = UIImagePickerController()
            imagePickerCamera.delegate = self
            imagePickerCamera.allowsEditing = false
            imagePickerCamera.sourceType = UIImagePickerController.SourceType.camera
            imagePickerCamera.cameraCaptureMode = .photo
            imagePickerCamera.modalPresentationStyle = .fullScreen

            present(imagePickerCamera,animated: true,completion: nil)
        }
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        //println_debug("Image selection")
        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            imageFile = image
            viewImageSelection.isHidden = true
            viewSelectionOption.isHidden = true
            isFileAttached = 1
      
            tblPerson.reloadData()
            
            dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        viewImageSelection.isHidden = true
        viewSelectionOption.isHidden = true

        dismiss(animated: true, completion: nil)
    }
    
    //MARK: -Image uploading
    
    private func _base64Attachements() -> [String] {
        
        var attachements = [String]()
        if let imageFile1 = imageFile {
            
            let chosenImage = self.resizeImage(image: imageFile1)
            
            if let string = toBase64EncodedString(image: chosenImage) {
                attachements.append(string)
            }
        }
        return attachements
    }
    
    func getImageURL(  completionHandler: @escaping (_ urlArray: [String]) -> Void) {
        let imageBase64Array = _base64Attachements()
        if imageBase64Array.count > 0 {
            var imageUrlArray = [String]()
            var urlString = ""
            let paramString : [String:Any] = [
                "MobileNumber":UserModel.shared.mobileNo,
                "Base64String":imageBase64Array
            ]
            VillageManager.shared.uploadImageToS3(imageBase64String :paramString, handler: {(imageURL,success) in
                if success {
                    for imageStr in imageURL {
                        urlString = ""
                        urlString = imageStr.replacingOccurrences(of: " ", with: "%20")
                        imageUrlArray.append(urlString)
                    }
                }
                completionHandler(imageUrlArray)
            })
        }
    }
    
    func checkDuplicateMobileNo(mobileNo: String , tag: Int, reachedMax: Bool = false)
    {
        
        duplicateBool = false
        var arrPendingList = [String]()
        
        var i = 0
        
        while i < (arrUserData.count)
        {
            let strData = arrUserData[i]["Mobile"]!
            //println_debug(strData)
            if(strData.count > 0)
            {
            if ((strData.localizedCaseInsensitiveContains(mobileNo)))
            {
                arrPendingList.append(strData)
            }
            }
            i = i + 1
        }
        
        if (arrPendingList.count > 1)
        {
            println_debug("Search Results empty & search text also empty")
            
            alertViewObj.wrapAlert(title: nil, body:"Split bill can't apply for same number!".localized, img: #imageLiteral(resourceName: "bill_splitter"))
            
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                self.arrContacts[tag] = "0"
                let dictionary: [String: String] = ["Mobile": "","Name":"Unknown"]
                self.arrUserData.remove(at: tag)
                self.arrUserData.insert(dictionary, at: tag)
                self.enableSendAction()

                self.tblPerson.reloadSections(IndexSet(integer: tag), with: .none)
                let prevCell = self.tblPerson.cellForRow(at: IndexPath.init(row: 0, section: tag)) as? BillSplitterContactCell
                prevCell?.txtMobileNumber.text = "09"
                prevCell?.txtMobileNumber.isUserInteractionEnabled = true
                prevCell?.txtMobileNumber.becomeFirstResponder()
               // self.tblPerson.isScrollEnabled = false
                self.duplicateBool = true

            }
            alertViewObj.showAlert(controller: self)
        } else {
            self.hideContactSuggesionView()
            self.enableSendAction()
            if reachedMax {
                self.reloadSection(indexTag : tag)
            }
        }
    }
    
    // MARK: - UITextField Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text: String? = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)

        
        if(textField.tag >= 1000 && textField.tag <= 2000)//For Split Amount
        {
            if numberPerson == 1 {
                return false
            }
            let str = (textField.text! + string)
            
            if text?.count == 1 && text == "0" {
                return false
            }
            
            if str.count <= 10 {
                return true
            }
            return false
        }
        else if(textField.tag == 3000)//for Total Amount
        {
            let str = (textField.text! + string)

            if text?.count == 1 && text == "0" {
                return false
            }
            
            if str.count <= 10 {
                
                return true
            }
            return false
        }
        else//Mobile Number Amount
        {
            println_debug("Mobile number validation")
            
            let strTotalLength = (textField.text! + string)

            guard validObj.getNumberRangeValidation(strTotalLength).isRejected == false else {
                alertViewObj.wrapAlert(title: nil, body: "Incorrect Mobile Number. Please try again".localized, img: UIImage(named : "AppIcon"))
                alertViewObj.addAction(title: "OK".localized, style: AlertStyle.cancel, action: {
                    textField.text = commonPrefixMobileNumber
                    self.contactSuggessionView?.view.isHidden = true
                })
                alertViewObj.showAlert(controller: self)
                return false
            }
            
            if validObj.getNumberRangeValidation(strTotalLength).min == validObj.getNumberRangeValidation(strTotalLength).max  {
                if validObj.getNumberRangeValidation(strTotalLength).max >= strTotalLength.count {
                    return true
                }
            } else {
                
                if  validObj.getNumberRangeValidation(strTotalLength).max >= strTotalLength.count  {
                    return true
                }
                
            }
            return false
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        //For Text Field Amount Update
        if(textField.tag == 3000)
        {
            //Display split amount equally on all person
            let dataWithoutComma = self.removeCommaFromDigit(textField.text!)
            
            strEnterAmountValue = self.getDigitDisplay(textField.text!)
            
            //Check input should be integer only
            if let number = Int(dataWithoutComma){
                amount = number/numberPerson
            }
            
            for intAmount in 1 ... numberPerson
            {
                arrAmounts[intAmount] = amount
            }
            
            if(strEnterAmountValue.count == 0)
            {
                isFileAttached = 0
                numberOfSection = 1
                arrAttachFiles = ["0","0","0","0","0","0"]
                arrAmounts = [0,0,0,0,0,0]
                arrContacts = ["0","0","0","0","0","0"]
                arrUserData = [["Mobile": "", "Name": "Unknown"], ["Mobile": "", "Name": "Unknown"],["Mobile": "", "Name": "Unknown"],["Mobile": "", "Name": "Unknown"],["Mobile": "", "Name": "Unknown"],["Mobile": "", "Name": "Unknown"]]
            }
            else
            {
                numberOfSection = numberPerson+1
            }
            tblPerson .reloadData()
                      let cell = tblPerson.cellForRow(at: IndexPath.init(row: 0, section: 0)) as? BillSplitterHeaderCell
                      cell?.txtAmount.placeholderColor = .black
                      cell?.txtAmount.becomeFirstResponder()
        }
        else if(textField.tag >= 1000 && textField.tag <= 2000)
        {
            if ((textField.text?.length) != 0)
            {
                let dataWithoutComma = self.removeCommaFromDigit(textField.text!)
                
               textField.text = self.getDigitDisplay(textField.text!)
                
                let cell = tblPerson.cellForRow(at: IndexPath.init(row: 0, section: textField.tag - 1000)) as? BillSplitterContactCell
                
                if(textField.text!.count == 9)
                {
                    cell?.txtAmount.textColor = UIColor.green
                }
                else if(textField.text!.count == 10)
                {
                    cell?.txtAmount.textColor = UIColor.red
                }
                else
                {
                    cell?.txtAmount.textColor = UIColor.black
                }
                
                self.arrAmounts[textField.tag - 1000] = Int(dataWithoutComma)!
                //New
                
                let resultant = arrAmounts.reduce(0, +)
                
                if (Int(self.removeCommaFromDigit(strEnterAmountValue))! <  resultant)
                {
                    alertViewObj.wrapAlert(title: nil, body:"Enter amount less than".localized + "\(strEnterAmountValue)" + " MMK", img: #imageLiteral(resourceName: "bill_splitter"))
                    //Split amount not greater than total amount
                    alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        
                        self.arrAmounts[textField.tag - 1000] = 0
                        textField.text = nil
                        //self.reloadSection(indexTag : textField.tag - 1000)
                        
                        
                    }
                    alertViewObj.showAlert(controller: self)
                    
                }
                else {
                    
                    //self.reloadSection(indexTag : textField.tag - 1000)
                    
                }
            }
        }
        else
        {
            if(textField.tag < 1000)
            {
                
                var value = ""
                //self.tblPerson.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                let cell = tblPerson.cellForRow(at: IndexPath.init(row: 0, section: textField.tag)) as? BillSplitterContactCell
                
                if((textField.text?.length)! > 2)
                {
                    cell?.btnCancel.isHidden = false
                }
                else {
                    cell?.btnCancel.isHidden = true
                }
                
                value = cell?.txtMobileNumber.text ?? ""
                intContactSelectionRow = IndexPath(item: 0, section: textField.tag)
                print("intContactSelectionRow---\(intContactSelectionRow)")
                
                let contactList = self.contactSuggesstion(textField.text!)
                if contactList.count > 0 {
                    
                    // here show the contact suggession screen
                    let rect = UitilityClass.setFrameForContactSuggestion(arrayCount: contactList.count, indexpath: IndexPath(item: 0, section: textField.tag),onTextField: cell!.txtMobileNumber,tableView: tblPerson)
                    self.showContactSuggessionView(withFrame: rect, andContacts: contactList)
                    
                }else {
                    // here hide the contact suggession screen
                    self.hideContactSuggesionView()
                    
                }
                
                //fixed text field with two prefix 09
                if((textField.text?.length)! <= 2)
                {
                    textField.text = "09"
                    print("calling from here")
                    self.hideContactSuggesionView()
                    let dictionary: [String: String] = ["Mobile": "09","Name":"Unknown"]
                    self.arrUserData.remove(at: textField.tag)
                    self.arrUserData.insert(dictionary, at: textField.tag)
                    self.arrContacts[textField.tag] = "0"
                    
                   let indexPath = IndexPath(item: 0, section: textField.tag)
                   self.tblPerson.reloadRows(at: [indexPath], with: .none)
                    
                    //change on 27Feb bug number OIO-I4786 this will not let the keyboard to dismiss 
                    let prevCell = self.tblPerson.cellForRow(at: indexPath) as? BillSplitterContactCell
                       prevCell?.txtMobileNumber.isUserInteractionEnabled = true
                       prevCell?.txtMobileNumber.becomeFirstResponder()
                }
                
                
                if cell?.txtMobileNumber.text != arrUserData[textField.tag]["Mobile"]{
                    cell?.txtUserName.text = "Unknown".localized
                }else{
                    cell?.txtUserName.text = arrUserData[textField.tag]["Mobile"]
                }
                
                 let strTotalLength = (textField.text!)
                //Check Mobile number validation
                guard validObj.getNumberRangeValidation(strTotalLength).isRejected == false else {
                    textField.text = "09"
                    
                    alertViewObj.wrapAlert(title: nil, body:"Incorrect Mobile Number. Please try again".localized, img: #imageLiteral(resourceName: "Contact Mobile Number"))
                    
                    alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        
                    }
                    alertViewObj.showAlert(controller: self)
                    return
                }
                
                let mobileNumber = self.getDestinationNum(textField.text!,withCountryCode: "+95")
                
                if (mobileNumber == UserModel.shared.mobileNo)
                {
                    let dictionary: [String: String] = ["Mobile": "","Name":"Unknown"]
                    self.arrUserData.remove(at: textField.tag)
                    self.arrUserData.insert(dictionary, at: textField.tag)
                    self.arrContacts[textField.tag] = "0"
                    
                    let indexPath = IndexPath(item: 0, section: textField.tag)
                    self.tblPerson.reloadRows(at: [indexPath], with: .none)
                    if let cell = tblPerson.cellForRow(at: IndexPath.init(row: 0, section: textField.tag)) as? BillSplitterContactCell {
                        cell.txtMobileNumber.isUserInteractionEnabled = true
                    }
                    alertViewObj.wrapAlert(title: nil, body:"Does not split amount to own number!".localized, img: #imageLiteral(resourceName: "bill_splitter"))
                    
                    alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                        
                    }
                    alertViewObj.showAlert(controller: self)
                    
                    return
                }
                let rangeCheck = validObj.getNumberRangeValidation(strTotalLength)
                
                if strTotalLength.count  >= rangeCheck.min  {
                   
                        isComingFromMin = true
                        self.arrContacts[textField.tag] = "1"
                        let indexPosition = IndexPath(row: 0, section: textField.tag)
                        tblPerson.reloadRows(at: [indexPosition], with: .none)
                    
                        let cell = tblPerson.cellForRow(at: IndexPath.init(row: 0, section: textField.tag )) as? BillSplitterContactCell
                        cell?.txtMobileNumber.text = value
                      cell?.txtMobileNumber.becomeFirstResponder()
                         if (numberOfSection - textField.tag > 1){
                        let cellNew = tblPerson.cellForRow(at: IndexPath.init(row: 0, section: textField.tag + 1)) as? BillSplitterContactCell
                        cellNew?.txtMobileNumber.isUserInteractionEnabled = true
                    }
                    
                    let dictionary: [String: String] = ["Mobile": textField.text!,"Name":"Unknown"]
                    self.arrUserData.remove(at: textField.tag)
                    self.arrUserData.insert(dictionary, at: textField.tag)
                    self.arrContacts[textField.tag] = "1"
                    //self.checkDuplicateMobileNo(mobileNo: strTotalLength , tag:textField.tag)
                    
                    if strTotalLength.count == rangeCheck.max {
                        self.checkDuplicateMobileNo(mobileNo: strTotalLength , tag:textField.tag, reachedMax: true)
                    }
                    
                    if strTotalLength.count > rangeCheck.max {
                        self.checkDuplicateMobileNo(mobileNo: strTotalLength , tag:textField.tag, reachedMax: true)
                    }
                    
                    if strTotalLength.count == rangeCheck.min {
                        self.enableSendAction()
                    }
                    
                } else {
                    self.arrContacts[textField.tag] = "0"
                    let dictionary: [String: String] = ["Mobile": "","Name":"Unknown"]
                    self.arrUserData.remove(at: textField.tag)
                    self.arrUserData.insert(dictionary, at: textField.tag)
                    self.enableSendAction()
                }
            }
        }
    }
    
    
    fileprivate func reloadSection(indexTag : Int)
    {
        tblPerson.reloadSections(IndexSet(integer: indexTag), with: .none)
        let prevCell = tblPerson.cellForRow(at: IndexPath.init(row: 0, section: indexTag)) as? BillSplitterContactCell
        prevCell?.txtMobileNumber.isUserInteractionEnabled = true
        let cell = tblPerson.cellForRow(at: IndexPath.init(row: 0, section: indexTag + 1)) as? BillSplitterContactCell
        cell?.txtMobileNumber.isUserInteractionEnabled = true
        cell?.txtMobileNumber.becomeFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
  
        if(textField.tag < 1000)
        {
            tblPerson.isScrollEnabled = false
            let contactList = textField.text!
            if contactList.count > 2
            {
              println_debug("dont edit")
            }
            else
            {
                textField.text = "09"
            }
        }
        if (textField.tag < 1000 ) {
            if focusedCell.contains(textField.tag) == false {
                focusedCell.append(textField.tag)
            }
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
  
        if(textField.tag < 1000)
        {
        textField.text = "09"
        return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if(textField.tag < 1000)
        {
            tblPerson.isScrollEnabled = true
            
        }
               
        
//        switch textField.tag {
//        case 1,2,3,4,5:
//            if let text = textField.text{
//                if text.count == 2{
//
//                    textField.becomeFirstResponder()
//                }
//            }
//        default:
//            break
//        }
        
        
        
        if let contactview = contactSuggessionView {
            contactview.view.isHidden = true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
       // textField.resignFirstResponder()
        return true
    }
    
    
    // MARK: - Custom Methods
    
    @IBAction func cancelButtonAction(_ sender: AnyObject) {
        viewImageSelection.isHidden = true
        viewImage.isHidden = true
        self.navigationController?.navigationBar.isUserInteractionEnabled = true

    }
    
    @IBAction func rightMenuAction(_ sender: Any) {
        self.view.endEditing(true)
        viewImageSelection.isHidden = false
        viewAllRequest.isHidden = false
    }
    
    @IBAction func viewAllRequestAction(_ sender: Any) {
        
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "BillSplitterAllRequests", delegate: self)
        } else{
            self.viewPushAllRequest()
        }
        
    }
    
    @IBAction func backAction(_ sender: Any) {
       
        //self.navigationController?.popViewController(animated: true)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnContactListAction(_ sender: Any) {
        println_debug("btnContactListAction")

        viewContactSelection.isHidden = true
        viewImageSelection.isHidden = true
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isUserInteractionEnabled = true

        let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
        self.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func btnFavoriteListAction(_ sender: Any) {
        println_debug("btnFavoriteListAction")

        let cell = tblPerson.cellForRow(at: IndexPath.init(row: 0, section: 0)) as? BillSplitterHeaderCell
        cell?.txtAmount.placeholderColor = .black
        cell?.txtAmount.resignFirstResponder()
        
        viewContactSelection.isHidden = true
        viewImageSelection.isHidden = true
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isUserInteractionEnabled = true

        let vc = self.openFavoriteFromNavigation(self, withFavorite: .payto)
        self.navigationController?.present(vc, animated: true, completion: nil)

    }
    
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func mYcurrentDateAccess() -> String{
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        //dateFormatter.setLocale()
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        let currentDateString: String = dateFormatter.string(from: date)
        return (currentDateString)
        
//        let date = Date()
//        let formatter = DateFormatter()
//        formatter.calendar = Calendar(identifier: .gregorian)
////        formatter.dateStyle = .full
////        formatter.timeStyle = .full
//        formatter.dateFormat = "MM/dd/yyyy hh:mm a"
//        formatter.setLocale()
//        return formatter.string(from:date)
        
    }
    
    func validateAllContactDetails() -> Bool {   //delegate method
       
        var isAllow: Bool = true
        
        //check text amount
        if ((strEnterAmountValue.length) == 0) {
            isAllow = false
            return isAllow;
        }
        
        //check all mobile number filled
        for i in 1...numberPerson
        {
            let memberDetail = arrContacts[i]
        
            if (memberDetail == "0") {
                isAllow = false
               
                alertViewObj.wrapAlert(title: nil, body:"Please fill all the contact details".localized, img: #imageLiteral(resourceName: "bill_splitter"))
                
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                    
                }
                alertViewObj.showAlert(controller: self)
              
                return isAllow

            }
            
            let memberAmount = arrAmounts[i]
            
            if (memberAmount == 0) {
                isAllow = false
                
                alertViewObj.wrapAlert(title: nil, body:"Please enter valid amount.".localized, img: #imageLiteral(resourceName: "bill_splitter"))
                
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                    
                }
                alertViewObj.showAlert(controller: self)
                
                return isAllow
                
            }
            
            }
        
        return isAllow
    }
    
    //image compression
    func resizeImage(image: UIImage) -> UIImage {
        var actualHeight: Float = Float(image.size.height)
        var actualWidth: Float = Float(image.size.width)
        let maxHeight: Float = 300.0
        let maxWidth: Float = 400.0
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        let compressionQuality: Float = 0.75
        //50 percent compression
        
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        
        let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img!.jpegData(compressionQuality: CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!) ?? UIImage()
        
    }
    
    func submitDataToServer(intData:Int)  {
        
        if appDelegate.checkNetworkAvail() {
            
            showProgressView()
            
            if arrAttachFiles[intData] == "1" {
                self.convertedImage.removeAll()
                //let chosenImage = self.resizeImage(image: self.imageFile!)//self.imageFile!
                //let  convertedStr = chosenImage.base64(format: ImageFormat.jpeg(1.0))
                self.convertedImage.append(imgURL)
            } else {
                convertedImage = []
            }
            
                let paramString : [String:Any] = [
                    
                    "MobileNumber":UserModel.shared.mobileNo,
                    "SimId":UserModel.shared.simID,
                    "Msid":01,
                    "Ostype":1,
                    "Otp":uuid,
                    "Source":UserModel.shared.mobileNo,
                    "Destination":self.getDestinationNum(arrUserData[intData]["Mobile"] as Any as! String,withCountryCode: "+95"),//
                    "Amount":arrAmounts[intData],
                    "Date":mYcurrentDateAccess(),
                    "LocalTransactionType":"RequestMoney",
                    "TransactionType":"PayTo",
                    "IsSchduled":false,
                    "SchduledType":-1,
                    "SchduledTime":mYcurrentDateAccess(),
                    "IsMynumber":true,
                    "AttachedImages": [],
                    "AttachedImagesAwsUrl" : convertedImage,
                    "RequestModuleType":1
                    ]
            
            let web      = WebApiClass()
            web.delegate = self
            let urlStr   = Url.billSplitter
            let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
            //println_debug(ur)
            //println_debug(paramString)
            web.genericClass(url: ur, param: paramString as AnyObject, httpMethod: "POST", mScreen: "mBillSplitter")
            
        }
    }
    
     // MARK: - API Integration
    
    func validateOkDollarNumberAPI(phoneNumber: Any) {   //delegate method
        
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            showProgressView()

            let buldApi = Url.billSplitterMobileValidation + "MobileNumber=\(phoneNumber)"
            
            let url = getUrl(urlStr: buldApi, serverType: .serverApp)
            //println_debug(url)
            
            let params = Dictionary<String,String>()
            
            web.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", mScreen: "mBillSplitterMobileValidation")
        }
        
    }
    
    @IBAction func submit(_ sender: UIButton) {
        
        if validateAllContactDetails()
        {
            self.viewImageSelection.isHidden = true
            self.viewContactSelection.isHidden = true
            
            if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "BillSplitterSubmit", delegate: self)
            } else{
                
                if imageFile != nil
                {
                getImageURL { (imageUrlArray) in
                    self.imgURL = imageUrlArray[0]
                    println_debug(self.imgURL)
                    self.submitDataToServer(intData: self.intStatusContact)
                }
                }
                else
                {
                    self.submitDataToServer(intData: self.intStatusContact)
                }
            }
        }
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful {
            if screen == "BillSplitterSubmit" {
                
                if imageFile != nil
                {
                    getImageURL { (imageUrlArray) in
                        self.imgURL = imageUrlArray[0]
                        println_debug(self.imgURL)
                        self.submitDataToServer(intData: self.intStatusContact)
                    }
                }
                else
                {
                    self.submitDataToServer(intData: self.intStatusContact)
                }
            }
            else if screen == "BillSplitterAllRequests"
            {
                println_debug("BillSplitterAllRequests Authorized")
               self.viewPushAllRequest()
            }
        }
    }
    
    func viewPushAllRequest()
    {
        let story = UIStoryboard.init(name: "BillSplitterView", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "BillSplitterAllRequestView") as? BillSplitterAllRequestView
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func enableSendAction()
    {
        println_debug("enableSendAction")
        
        for intPerson in 1 ... numberPerson
        {
            if(arrContacts[intPerson] == "0")
            {
                btnSend.isHidden = true
                heightBtnSend.constant = 0.0
                break
            }
            else
            {
                heightBtnSend.constant = 44.0
               btnSend.isHidden = false
            }
        }
    }
    
    //MARK:- API Response
    func webResponse(withJson json: AnyObject, screen: String) {
        
        self.removeProgressView()

        if screen == "mBillSplitter" {
            
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        //println_debug(dic)
                        
                        if dic["Code"]! as! Int == 200
                        {
                    
                        DispatchQueue.main.async {
                           //"Bill Splitter request sent to Contact \(self.intStatusContact) Successfully"
                            
                            alertViewObj.wrapAlert(title: nil, body:"Bill Splitter request sent to Contact".localized + "\(self.intStatusContact) " + "Successfully".localized, img: #imageLiteral(resourceName: "check_sucess_bill"))
                            
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                
                                if self.intStatusContact < self.numberPerson
                                {
                                    self.intStatusContact = self.intStatusContact+1
                                    
                                    self.submitDataToServer(intData: self.intStatusContact)
                                }
                                else if(self.intStatusContact == self.numberPerson)
                                {
                                    self.intStatusContact = 1
                                    
                                    DispatchQueue.main.async {
                                        self.arrAttachFiles = ["0","0","0","0","0","0"]
                                        self.arrAmounts = [0,0,0,0,0,0]
                                        self.arrContacts = ["0","0","0","0","0","0"]
                                        self.arrUserData = [["Mobile": "", "Name": "Unknown"], ["Mobile": "", "Name": "Unknown"],["Mobile": "", "Name": "Unknown"],["Mobile": "", "Name": "Unknown"],["Mobile": "", "Name": "Unknown"]]
                                        
                                        self.strEnterAmountValue = ""
                                        self.btnSend.isHidden = true
                                        self.heightBtnSend.constant = 0.0
                                        self.isFileAttached = 0
                                        self.numberPerson = 1
                                        self.numberOfSection = 1
                                        self.tblPerson.reloadData()
                                        let index = IndexPath(row: 0, section: 0)
                                        self.tblPerson.scrollToRow(at: index, at: .top, animated: true)
                                        UIView.animate(withDuration: 0.5, animations: {
                                            self.view.layoutIfNeeded()
                                        })
                                    }
                                }
                                else
                                {
                                    self.intStatusContact = 1
                                }
                            }
                            alertViewObj.showAlert(controller: self)
                            }
                            
                        }
                        else
                        {
                            DispatchQueue.main.async {
                            var strMsg = dic["Msg"] as! String
                              if strMsg.contains(find: "Source & Destination")
                              {
                                strMsg = "Source & Destination\n number should not be same."
                              } else if  strMsg.contains(find: "You cannot request split bill for same destination number and amount within 5 mins") {
                                strMsg = "You can not request to same destination mobile number with in 5 minutes."
                            }
                            alertViewObj.wrapAlert(title: nil, body: strMsg.localized, img: #imageLiteral(resourceName: "bill_splitter"))
                                    
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                        
                            }
                            alertViewObj.showAlert(controller: self)
                        
                            }
                        }
                    }
                    
                }
            } catch {
                
            }
        }
        if screen == "mBillSplitterMobileValidation"
        {
            
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject> {
                        //println_debug(dic)
                       
                        if dic["Code"]! as! Int == 200
                        {
                             self.arrContacts[intContactSelectionRow.section] = "1"
                             
                             let dictionary: [String: String] = ["Mobile": strContactNumber,"Name":strContactName]
                             self.arrUserData.remove(at: intContactSelectionRow.section)
                             self.arrUserData.insert(dictionary, at: intContactSelectionRow.section)
                              DispatchQueue.main.async {
                                let indexPath = IndexPath(item: 0, section: self.intContactSelectionRow.section)
                             self.tblPerson.reloadRows(at: [indexPath], with: .none)
                            self.btnSend.isHidden = false
                            self.heightBtnSend.constant = 44.0
                            }
                        }
                        else {
                             DispatchQueue.main.async {
                            alertViewObj.wrapAlert(title: nil, body:"Okdollar Account Number Not Exist. Please check!".localized, img: #imageLiteral(resourceName: "bill_splitter"))
                                self.arrContacts[self.intContactSelectionRow.section] = "0"
                            
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                let dictionary: [String: String] = ["Mobile": "","Name":"Unknown"]
                                self.arrUserData.remove(at: self.intContactSelectionRow.section)
                                self.arrUserData.insert(dictionary, at: self.intContactSelectionRow.section)
                                
                                let indexPath = IndexPath(item: 0, section: self.intContactSelectionRow.section)
                                self.tblPerson.reloadRows(at: [indexPath], with: .none)
                            }
                            alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                    
                }
            } catch {
                
            }
        }
    }
    
    func loadContactSuggessionView() {
        contactSuggessionView  =  UIStoryboard(name: "SendMoney", bundle: nil).instantiateViewController(withIdentifier: "ContactSugestView_ID") as? ContactSuggestionVC
        contactSuggessionView?.delegate   = self //as? ContactSuggestionDelegate
        contactSuggessionView?.view.frame = CGRect.zero
        addChild(contactSuggessionView!)
        self.view.addSubview(contactSuggessionView!.view)
        contactSuggessionView!.didMove(toParent: self)
        contactSuggessionView?.view.isHidden = true
    }
    
//    fileprivate func getSuggessionviewFrame(arr: Int, indexpath: IndexPath,index: Int,onTextField: UITextField) -> CGRect {
//        
//        var height: CGFloat = CGFloat(Float(arr) * 70.0)
//
//        if height >= 200.0{
//            height = 200.0
//        }
//        
//        return CGRect(x: onTextField.frame.minX, y: tblPerson.convert(tblPerson.rectForRow(at: indexpath), to: tblPerson).minY - height + onTextField.frame.height + 20, width: onTextField.frame.width, height: height)
// }
    
    func hideContactSuggesionView() {
        if let contactview = contactSuggessionView {
            contactview.view.isHidden = true
        }
    }
    
    func showContactSuggessionView(withFrame frame: CGRect, andContacts contactList: [Dictionary<String, Any>]) {
        contactSuggessionView?.view.isHidden    = false
        contactSuggessionView?.view.frame = frame
        contactSuggessionView?.contactsList     = contactList
        contactSuggessionView?.contactsTable.reloadData()
        contactSuggessionView?.view.layoutIfNeeded()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        contactSuggessionView?.view.isHidden = true
    }
    

}

extension UIImage {
    
    public func base64(format: ImageFormat) -> String? {
        
        var imageData: Data?
        switch format {
        case .png: imageData = self.pngData()
        case .jpeg(let compression): imageData = self.jpegData(compressionQuality: compression)
        }
        
        return imageData?.base64EncodedString()
        
    }
}

extension BillSplitterView : ContactSuggestionDelegate{

    func didSelectFromContactSuggession(number: Dictionary<String, Any>)
    {
        let mobileNo = self.getActualNum(number[ContactSuggessionKeys.phonenumber_backend] as! String,withCountryCode: "+95")
       
        self.arrContacts[intContactSelectionRow.section] = "1"
        
        let dictionary: [String: String] = ["Mobile": mobileNo,"Name":number[ContactSuggessionKeys.contactname] as! String]
        self.arrUserData.remove(at: intContactSelectionRow.section)
        self.arrUserData.insert(dictionary, at: intContactSelectionRow.section)
        
        self.tblPerson.reloadSections(IndexSet(integer: self.intContactSelectionRow.section), with: .none)
        let prevCell = self.tblPerson.cellForRow(at: IndexPath.init(row: 0, section: self.intContactSelectionRow.section)) as? BillSplitterContactCell
        prevCell?.txtMobileNumber.isUserInteractionEnabled = true
        let cell = self.tblPerson.cellForRow(at: IndexPath.init(row: 0, section: self.intContactSelectionRow.section + 1)) as? BillSplitterContactCell
        cell?.txtMobileNumber.isUserInteractionEnabled = true
        
        self.checkDuplicateMobileNo(mobileNo: mobileNo , tag:self.intContactSelectionRow.section)

        self.enableSendAction()
        
        self.hideContactSuggesionView()
    }

}

extension BillSplitterView : FavoriteSelectionDelegate{
    
    func selectedFavoriteObject(obj: FavModel) {
        
        if(isMyanmarNumber(obj.phone,withCountryCode: "+95"))
        {
        let mobileNo = self.getActualNum(obj.phone,withCountryCode: "+95")
       
       
        //Cal API for verification
        self.arrContacts[intContactSelectionRow.section] = "1"
        
        let dictionary: [String: String] = ["Mobile": mobileNo,"Name":obj.name]
        self.arrUserData.remove(at: intContactSelectionRow.section)
        self.arrUserData.insert(dictionary, at: intContactSelectionRow.section)

            self.tblPerson.reloadSections(IndexSet(integer: self.intContactSelectionRow.section), with: .none)
            let prevCell = self.tblPerson.cellForRow(at: IndexPath.init(row: 0, section: self.intContactSelectionRow.section)) as? BillSplitterContactCell
            prevCell?.txtMobileNumber.isUserInteractionEnabled = true
            let cell = self.tblPerson.cellForRow(at: IndexPath.init(row: 0, section: self.intContactSelectionRow.section + 1)) as? BillSplitterContactCell
            cell?.txtMobileNumber.isUserInteractionEnabled = true

        self.enableSendAction()
        
        //Hide Contact View
        viewImageSelection.isHidden = true
        viewContactSelection.isHidden = true
            
            self.checkDuplicateMobileNo(mobileNo: mobileNo , tag:self.intContactSelectionRow.section)

        }
        else
        {
            alertViewObj.wrapAlert(title: nil, body:"Please select Myanmar number.".localized, img: #imageLiteral(resourceName: "bill_splitter"))
            
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                
            }
            alertViewObj.showAlert(controller: self)
        }

    }
    
}

 extension BillSplitterView : ContactPickerDelegate{
    
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]) {
      
        for contact in contacts {
            decodeContact(contact: contact, isMulti: true)
        }
    }
    
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        
        decodeContact(contact: contact, isMulti: false)
    }
    
    func decodeContact(contact: ContactPicker, isMulti: Bool) {
        
        let phoneNumberCount = contact.phoneNumbers.count
        
        if phoneNumberCount >= 1 {
            
            //println_debug(contact.firstName)
            //println_debug(contact.phoneNumbers[0].phoneNumber)
            
            _ = "\(contact.phoneNumbers[0].phoneNumber)"
            
            if(isMyanmarNumber(contact.phoneNumbers[0].phoneNumber,withCountryCode: "+95"))
            {
                var strMobNo = contact.phoneNumbers[0].phoneNumber
                //println_debug(strMobNo)
                
                let cDetails = identifyCountry(withPhoneNumber: strMobNo) // 1 -> flag, 0 -> code
                println_debug(cDetails.countryCode)
                
                if strMobNo.hasPrefix(cDetails.countryCode) {
                    strMobNo = strMobNo.replacingOccurrences(of: cDetails.countryCode, with: "")
                }
                println_debug(strMobNo)
                if let charEncode = String(strMobNo.first!).addingPercentEncoding(withAllowedCharacters: .decimalDigits) {
                    if charEncode == "%C2%A0" {
                        _ = strMobNo.remove(at: String.Index.init(encodedOffset: 0))
                    }
                }
                println_debug(strMobNo)
                
                let trim = strMobNo.replacingOccurrences(of: " ", with: "", options: .caseInsensitive)
                println_debug(trim)
                let trimmed = String(strMobNo.filter { !" ".contains($0) })
                //println_debug(trimmed)
                
                strContactNumber = self.getConatctNum(trimmed,withCountryCode: "+95")
                //println_debug(strContactNumber)
                
                strContactName = contact.firstName
                
                let mobileNo = self.getActualNum(strContactNumber,withCountryCode: "+95")
                
                //Cal API for verification
                self.arrContacts[intContactSelectionRow.section] = "1"
                
                let dictionary: [String: String] = ["Mobile": mobileNo,"Name":strContactName]
                self.arrUserData.remove(at: intContactSelectionRow.section)
                self.arrUserData.insert(dictionary, at: intContactSelectionRow.section)
                
                let indexPath = IndexPath(item: 0, section: self.intContactSelectionRow.section)
                self.tblPerson.reloadRows(at: [indexPath], with: .none)
                
                //Commecnted by tushar to make textfield user interactor enable
                if intContactSelectionRow.section != 5{
                    
                    if numberOfSection > intContactSelectionRow.section{
                        let sectionNew = intContactSelectionRow.section + 1
                        
                        if let cell = tblPerson.cellForRow(at: IndexPath.init(row: 0, section: sectionNew)) as? BillSplitterContactCell {
                            cell.txtMobileNumber.isUserInteractionEnabled = true
                        }
                        
                        
                    }
                }
                
                
                self.checkDuplicateMobileNo(mobileNo: mobileNo , tag:self.intContactSelectionRow.section)
                
                self.enableSendAction()
            }
            else
            {
                alertViewObj.wrapAlert(title: nil, body:"Please select Myanmar number.".localized, img: #imageLiteral(resourceName: "bill_splitter"))
                
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                    
                }
                alertViewObj.showAlert(controller: self)
            }
        }
        else {
            _ = ContactGlobalConstants.Strings.phoneNumberNotAvaialable
        }
        
        
        //Hide Contact View
        viewImageSelection.isHidden = true
        viewContactSelection.isHidden = true
    }
    
 }


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}

 
 
