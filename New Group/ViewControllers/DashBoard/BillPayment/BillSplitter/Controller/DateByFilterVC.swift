//
//  DateByFilterVC.swift
//  OK
//
//  Created by PC on 2/1/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

@objc protocol BillSplitterDateDelegate: class {
    @objc optional func selectedDate(rowData: String)
}

class DateByFilterVC: OKBaseController {

    @IBOutlet weak var datePickerToDate: UIDatePicker!
    @IBOutlet weak var datePickerFromDate: UIDatePicker!
    @IBOutlet weak var lblDuration: UILabel!{
        didSet {
            lblDuration.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var lblFromDate: UILabel!
    {
        didSet
        {
          self.lblFromDate.font = UIFont(name: appFont, size: appFontSize)

           lblFromDate.text = "From".localized
        }
    }
    @IBOutlet weak var lblToDate: UILabel!
        {
        didSet
        {
            self.lblToDate.font = UIFont(name: appFont, size: appFontSize)
            lblToDate.text = "To".localized
        }
    }
    @IBOutlet weak var btnCancel: UIButton!
        {
        didSet
        {
            self.btnCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.btnCancel.setTitle(self.btnCancel.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var btnOK: UIButton!
        {
        didSet
        {
            self.btnOK.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.btnOK.setTitle(self.btnOK.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var btnReset: UIBarButtonItem!
        {
        didSet
        {
            btnReset.title = "Reset".localized
          btnReset.setTitleTextAttributes([ NSAttributedString.Key.font: UIFont(name: appFont, size: 18) ?? UIFont.systemFont(ofSize: 18),
                                              NSAttributedString.Key.foregroundColor: UIColor.white ], for: .normal)
        }
    }
    
    var fromDatePicker = ""
    var toDatePicker = ""
    var intSearchStatus = 0

    override func viewWillAppear(_ animated: Bool) {
       
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        
        if(fromDatePicker.count == 0)
        {
            datePickerFromDate.maximumDate = Date() // An other date ;
            
            let date = datePickerFromDate.date
            let formatter = DateFormatter()
            formatter.calendar = Calendar(identifier: .gregorian)
            formatter.dateStyle = .full
            formatter.timeStyle = .full
            formatter.dateFormat = "MM/dd/yyyy"
            fromDatePicker = formatter.string(from:date)
            toDatePicker = formatter.string(from:date)
            
            //lblDuration.text = "Duration: 0 day".localized
            //lblDuration.text = "Duration".localized + ": 0 day"
            lblDuration.text = "Duration".localized + ":" + "0" + "day"

        }
        else
        {
            let formatter = DateFormatter()
            formatter.calendar = Calendar(identifier: .gregorian)
            formatter.dateStyle = .full
            formatter.timeStyle = .full
            formatter.dateFormat = "MM/dd/yyyy"
            
            //Convert String to Date
            datePickerFromDate.date = formatter.date(from:fromDatePicker)!
            datePickerToDate.minimumDate = datePickerFromDate.date

            datePickerToDate.date = formatter.date(from:toDatePicker)!
            
            self.calculateDay()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Bill Splitter".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]

        // Do any additional setup after loading the view.
        datePickerToDate.backgroundColor = .lightGray
        datePickerFromDate.backgroundColor = .lightGray
        datePickerToDate.calendar = Calendar(identifier: .gregorian)
        datePickerFromDate.calendar = Calendar(identifier: .gregorian)
        if #available(iOS 13.4, *) {
            datePickerFromDate.preferredDatePickerStyle = .wheels
            datePickerToDate.preferredDatePickerStyle = .wheels

                   } else {
                       // Fallback on earlier versions
                   }
    }
    
    func calculateDay()
    {
        let form = DateComponentsFormatter()
        form.maximumUnitCount = 2
        form.unitsStyle = .full
        form.allowedUnits = [.day]
        let yearValue = form.string(from: datePickerFromDate.date, to: datePickerToDate.date)
        println_debug(yearValue as Any)
        
        //lblDuration.text = "Duration: ".localized+(yearValue)!
        lblDuration.font = UIFont(name: appFont, size: appFontSize)
        lblDuration.text = "Duration".localized + ":" + (yearValue)! //+ "day".localized

    }
    
    @IBAction func fromDatePickerAction(_ sender: Any) {
        
        let date = datePickerFromDate.date
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateStyle = .full
        formatter.timeStyle = .full
        formatter.dateFormat = "MM/dd/yyyy"
        fromDatePicker = formatter.string(from:date)
        
        datePickerToDate.minimumDate = datePickerFromDate.date

        self.calculateDay()
    }
    
    @IBAction func toDatePickerAction(_ sender: Any) {
        
        let date = datePickerToDate.date
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateStyle = .full
        formatter.timeStyle = .full
        formatter.dateFormat = "MM/dd/yyyy"
        toDatePicker = formatter.string(from:date)
        
        datePickerToDate.maximumDate = Date()

        self.calculateDay()
    }
    
    @IBAction func submitDateAction(_ sender: AnyObject) {
        
        let nc = NotificationCenter.default
        
        print("Submit date check----\(intSearchStatus)")
        
        if intSearchStatus == 0
        {
            nc.post(name:Notification.Name(rawValue:"PendingNotification"),
                    object: nil,
                    userInfo: ["dateBy":"\(fromDatePicker)&&\(toDatePicker)"])
                        
            nc.post(name:Notification.Name(rawValue:"PendingImageNotification"),
                    object: nil,
                    userInfo: ["dateBy":"\(fromDatePicker)&&\(toDatePicker)"])
        }
        else if intSearchStatus == 1
        {
            nc.post(name:Notification.Name(rawValue:"SuccessfullNotification"),
                    object: nil,
                    userInfo: ["dateBy":"\(fromDatePicker)&&\(toDatePicker)"])
            nc.post(name:Notification.Name(rawValue:"SuccessImageNotification"),
                    object: nil,
                    userInfo: ["dateBy":"\(fromDatePicker)&&\(toDatePicker)"])
        }
        else if intSearchStatus == 2
        {
            nc.post(name:Notification.Name(rawValue:"RejectNotification"),
                    object: nil,
                    userInfo: ["dateBy":"\(fromDatePicker)&&\(toDatePicker)"])
            nc.post(name:Notification.Name(rawValue:"RejectImageNotification"),
                    object: nil,
                    userInfo: ["dateBy":"\(fromDatePicker)&&\(toDatePicker)"])
        }
        
        self.navigationController?.popViewController(animated: false)

    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func resetAction(_ sender: Any) {
        
        fromDatePicker = ""
        toDatePicker = ""
        
        let nc = NotificationCenter.default
        
        if intSearchStatus == 0
        {
            nc.post(name:Notification.Name(rawValue:"PendingNotification"),
                    object: nil,
                    userInfo: ["reset":"yes"])
            nc.post(name:Notification.Name(rawValue:"PendingImageNotification"),
                    object: nil,
                    userInfo: ["dateBy":"\(fromDatePicker)&&\(toDatePicker)"])
        }
        else if intSearchStatus == 1
        {
            nc.post(name:Notification.Name(rawValue:"SuccessfullNotification"),
                    object: nil,
                    userInfo: ["reset":"yes"])
            nc.post(name:Notification.Name(rawValue:"SuccessImageNotification"),
                    object: nil,
                    userInfo: ["dateBy":"\(fromDatePicker)&&\(toDatePicker)"])
        }
        else if intSearchStatus == 2
        {
            nc.post(name:Notification.Name(rawValue:"RejectNotification"),
                    object: nil,
                    userInfo: ["reset":"yes"])
            nc.post(name:Notification.Name(rawValue:"RejectImageNotification"),
                    object: nil,
                    userInfo: ["dateBy":"\(fromDatePicker)&&\(toDatePicker)"])
        }
            
        self.navigationController?.popViewController(animated: false)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
