//
//  BillSplitterPendingVC.swift
//  OK
//
//  Created by PC on 10/28/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SDWebImage
import Foundation

class BillSplitterPendingVC: OKBaseController,WebServiceResponseDelegate {

    @IBOutlet weak var tblList: UITableView!

    var indexPath: Int = 0
    var arrFilters: [String] = []

    var arrPendingList = [BillSplitterAllRequestModel]()
    var arrDatePendingList = [BillSplitterAllRequestModel]()
    var arrMainList = [BillSplitterAllRequestModel]()

    //manage for date,sort & filter data
    let myNotification = Notification.Name(rawValue:"PendingNotification")
    
    var refreshControl = UIRefreshControl()

    let noDataLabel = UILabel()

    private var multiButton : ActionButton?
    var strFilterTitle = "Default"
    var strSortTitle = "Default"
    
    var savefromDate : String = ""
    var savetoDate : String = ""

    //Pagination
    var offsetLimit: Int = 0


    // MARK: - View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        
        // Do any additional setup after loading the view.
        println_debug("BillSplitterpendingVC-viewWillAppear")

        let nc = NotificationCenter.default
        if(arrMainList.count > 0)
        {
            nc.post(name:Notification.Name(rawValue:"PendingImageNotification"),
                    object: nil,
                    userInfo: ["search": "Search"])
        }
        else
        {
            nc.post(name:Notification.Name(rawValue:"PendingImageNotification"),
                    object: nil,
                    userInfo: ["search": "HideSearch"])
        }
    }
     
    override func viewDidLoad() {
        super.viewDidLoad()
        self.helpSupportNavigationEnum = .Bill_Splitter
        noDataLabel.isHidden = true

        let nc = NotificationCenter.default
        nc.addObserver(forName:myNotification, object:nil, queue:nil, using:pendingSearchNotification)
        
        self.getDataFromTheServer()
        
        
        tblList.refreshControl = self.refreshControl
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tblList.addSubview(refreshControl)
    }
    
    @objc func refresh(sender:AnyObject)
    {
        print("Pending refresh called----")
        self.filterDataList(rowData: "Default")
        self.sortDataList(rowData: "Default")
        
        let nc = NotificationCenter.default
        
        nc.post(name:Notification.Name(rawValue:"PendingImageNotification"),
                    object: nil,
                    userInfo: ["search": "HideSearch"])
            
        self.getDataFromTheServer()
    }
    
    // MARK: - Notification
    func pendingSearchNotification(notification:Notification) -> Void {
        //println_debug("pendingSearchNotification")
        
        //Array date By
        if ((notification.userInfo?["dateBy"] as? String) != nil)
        {
            let fulldate = notification.userInfo?["dateBy"] as? String
            let arrayDate = fulldate?.components(separatedBy: "&&")
            let startdate = "\(arrayDate![0])"
            let enddate = "\(arrayDate![1])"
            savefromDate = startdate
            savetoDate = enddate
            
            print("pendingstartdate----\(startdate)")
            print("pendingenddate----\(enddate)")

            arrPendingList.removeAll()
            arrDatePendingList.removeAll()
            // Set date format
            let dateFmt = DateFormatter()
            dateFmt.dateFormat =  "MM/dd/yyyy"
            
            // Get NSDate for the given string
            let dateFrom = dateFmt.date(from: startdate)
            let dateTo = dateFmt.date(from: enddate)
            
            var i = 0
            
            while i < (arrMainList.count)
            {
                let modelObj = arrMainList[i]
                
                let dateString = modelObj.dateTime
                
                let dateFormatter = DateFormatter()
                dateFormatter.calendar = Calendar(identifier: .gregorian)
                dateFormatter.dateFormat = "MM/dd/yyyy hh:mm:ss a"
                let date: Date? = dateFormatter.date(from: dateString)
                dateFormatter.dateFormat =  "MM/dd/yyyy"

                let FormateStr = dateFormatter.string(from: date!)
                let dateTest = dateFmt.date(from: FormateStr)
                
                if(dateFrom == dateTo)
                {
                    switch dateFrom?.compare(dateTest!) {
                    case .orderedAscending?     :
                        println_debug("Date added from")
                    case .orderedDescending?    :
                        println_debug("Date added from")
                    case .orderedSame?          :
                        println_debug("The two dates are the same")
                            arrPendingList.append(modelObj)
                    case .none:
                        println_debug("none")
                    }
                }
                else
                {
                    //println_debug("From\(String(describing: dateFrom))&&To\(String(describing: dateTo))&&Current\(String(describing: dateTest))")
                    
                    if(dateFrom?.compare(dateTest!) == ComparisonResult.orderedSame)
                    {
                        println_debug("Same Date with From")
                        arrPendingList.append(modelObj)
                    }
                    else if(dateFrom?.compare(dateTest!) == ComparisonResult.orderedAscending && dateTo?.compare(dateTest!) == ComparisonResult.orderedDescending)
                    {
                        //Current date is greater than from
                        println_debug("Ascending Date with From")
                        arrPendingList.append(modelObj)
                    }
                    else if(dateTo?.compare(dateTest!) == ComparisonResult.orderedSame)
                    {
                        println_debug("Same Date with To")
                        arrPendingList.append(modelObj)
                    }
                   
                }
                
                
                i = i + 1
            }
            
            arrDatePendingList = arrPendingList
            print("arrPendingList=====\(arrPendingList.count)------\(arrDatePendingList.count)")
            tblList.dataSource = self
            tblList.delegate = self
            self.tblList.reloadData()

            
            if (arrPendingList.isEmpty)
            {
                //println_debug("Search Results empty & search text also empty")
                tblList.reloadData()
            } else {
                
                DispatchQueue.main.async {
                               self.tblList.reloadData()
                               let index = IndexPath(row: 0, section: 0)
                               self.tblList.scrollToRow(at: index, at: .top, animated: true)
                               UIView.animate(withDuration: 0.5, animations: {
                                   self.view.layoutIfNeeded()
                               })
                           }
            }
        }
        
        //Reset reload all data
        else if (notification.userInfo?["reset"] as? String == "yes")
        {
           
            arrPendingList.removeAll()
            arrDatePendingList.removeAll()

            arrPendingList = arrMainList
            arrDatePendingList = arrMainList
            tblList.reloadData()
        }
        //Cancel reload all data
        else if (notification.userInfo?["cancel"] as? String == "yes")
        {
            arrPendingList.removeAll()
            arrDatePendingList.removeAll()
            
            arrPendingList = arrMainList
            arrDatePendingList = arrMainList

            tblList.reloadData()
        }
        else if ((notification.userInfo?["search"] as? String)?.count ?? 0 > 0)
        {
           
            guard let text = notification.userInfo?["search"] as? String else { return }
            print ("search: \(text)")
            self.tblList.backgroundView = nil

            if(text.count == 0)
            {
                print("TextZero")
                //arrPendingList = arrMainList
                tblList.reloadData()
            }
            else
            {
                var arrSearchList = [BillSplitterAllRequestModel]()
                
                if(arrPendingList.count == 0){
                    arrSearchList = arrMainList
                }else {
                    arrSearchList = arrPendingList
                }
                print("arrSearchList count----\(arrSearchList.count)")
                

                arrPendingList.removeAll()
                
                var i = 0
                
                while i < (arrSearchList.count)
                {
                    let modelObj = arrSearchList[i]
                    
                    if ((modelObj.name.localizedCaseInsensitiveContains(text)) || (modelObj.requestType.localizedCaseInsensitiveContains(text)) || (modelObj.category.localizedCaseInsensitiveContains(text)))
                    {
                        arrPendingList.append(modelObj)
                        
                        tblList.reloadData()
                    }
                    i = i + 1
                }
                
//                if arrPendingList.count == 0 && text.count>0{
//                    self.tblList.separatorStyle = .none
//                    tblList.reloadData()
//                }
                print("arrPendingList count----\(arrPendingList.count)")
                if (arrPendingList.isEmpty)
                {

                    println_debug("Search Results empty & search text also empty")
                    self.tblList.separatorStyle = .none
                    tblList.reloadData()
                    
                } else {
                    
                    DispatchQueue.main.async {
                                   self.tblList.reloadData()
                                   let index = IndexPath(row: 0, section: 0)
                                   self.tblList.scrollToRow(at: index, at: .top, animated: true)
                                   UIView.animate(withDuration: 0.5, animations: {
                                       self.view.layoutIfNeeded()
                                   })
                               }
                }

                
            }
        } else {
            
            print("EmptyZero")
            arrPendingList.removeAll()
            arrDatePendingList.removeAll()

            arrPendingList = arrMainList
            arrDatePendingList = arrMainList
            tblList.reloadData()
        }
    }
    
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - API Delegate
    func getOffsetDataFromTheServer() {
        
            if appDelegate.checkNetworkAvail() {
                let web      = WebApiClass()
                web.delegate = self
                showProgressView()

                  let buldApi = Url.billSplitterAllRequest + "MobileNumber=\(UserModel.shared.mobileNo)&Simid=\(UserModel.shared.simID)&MSID=01&OSType=1&OTP=\(uuid)&RequestedMoneyStatus=Requested&RequestType=Normal&Limit=5&OffSet=\(offsetLimit)&RequestModuleType=1"
                
                let url = getUrl(urlStr: buldApi, serverType: .serverApp)
                println_debug(url)

                let params = Dictionary<String,String>()
                
                web.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", mScreen: "mBillSplitterPendingOffSet")
            }
        }
    
    func getDataFromTheServer() {
        
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            showProgressView()

              let buldApi = Url.billSplitterAllRequest + "MobileNumber=\(UserModel.shared.mobileNo)&Simid=\(UserModel.shared.simID)&MSID=01&OSType=1&OTP=\(uuid)&RequestedMoneyStatus=Requested&RequestType=Normal&Limit=25&OffSet=0&RequestModuleType=1"
            
            let url = getUrl(urlStr: buldApi, serverType: .serverApp)
            println_debug(url)

            let params = Dictionary<String,String>()
            
            web.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", mScreen: "mBillSplitterPending")
        }
    }
    
    func webResponse(withJson json: AnyObject, screen: String) {
        self.removeProgressView()
        //mBillSplitterPending
        if screen == "mBillSplitterPending" {
            
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dataDict = dict as? Dictionary<String,AnyObject> {
                        println_debug(dataDict)
                        
                        if let dicData = dataDict["Data"] as? String {
                            let arrData = OKBaseController.convertToArrDictionary(text: dicData)
                            
                            if arrData == nil { return }
                            
                            arrPendingList.removeAll()
                            arrMainList.removeAll()
                            arrFilters.removeAll()
                            arrDatePendingList.removeAll()

                            for element in arrData! {
                                
                                println_debug(element)
                                let notificationModel =  BillSplitterAllRequestModel()
                                notificationModel.id = "\(element["RequestId"]! ?? "")"
                                
                                if !(element["AgentName"] is NSNull) {
                                    notificationModel.name = "\(element["AgentName"]! ?? "")"
                                    if(notificationModel.name == "Not Exist")
                                    {
                                        notificationModel.name = "Unknown"
                                    }
                                }
                                else
                                {
                                        notificationModel.name = "Unknown"
                        
                                }
                                
                                notificationModel.remindCount = "\(element["RemindCount"]! ?? "")"

                                if !(element["CategoryName"] is NSNull) {
                                    notificationModel.category = "\(element["CategoryName"]! ?? "")"
                                }
                                else
                                {
                                    notificationModel.category = "Not Registered"
                                }
                                arrFilters.append(notificationModel.category)
                                notificationModel.requestType = "OK$ Money"
                                notificationModel.number = "\(element["Destination"]! ?? "")"
                                notificationModel.localTransactionType = "\(element["LocalTransactionType"]! ?? "")"
                                notificationModel.commanTransactionType = "\(element["CommanTransactionType"]! ?? "")"

                                if let ary = element["AttachmentPath"] as? [String] {
                                    if(ary.count > 0)
                                    {
                                    let x: String = ary[0]
                                    println_debug(x)
                                        notificationModel.attachedFile = "Yes"
                                        notificationModel.attachedFileURL = "\(x)"
                                    }
                                    else{
                                        notificationModel.attachedFile = "No"
                                        }
                                    }
                                
                                let intdata = "\(element["Amount"]! ?? "")"
                                notificationModel.amount = Int(intdata)!
                            
                                notificationModel.dateTime = "\(element["Scheduledtime"]! ?? "")"
                                //Date = "2018-03-29T13:52:34.687";
                                notificationModel.reminderDate = "\(element["Date"]! ?? "")"
                                
                                arrMainList.append(notificationModel)
                            }
                            
                            //println_debug("arrMainList - %@",arrMainList)
                             DispatchQueue.main.async {
                            let nc = NotificationCenter.default
                            if(self.arrMainList.count > 0)
                            {
                                if(self.arrMainList.count > 1) {
                                    self.loadFloatButtons()
                                }
                                nc.post(name:Notification.Name(rawValue:"PendingImageNotification"),
                                        object: nil,
                                        userInfo: ["search": "Search"])
                                self.tblList.separatorStyle = .none
                                self.tblList.backgroundView = nil
                            }
                            else
                            {
                                nc.post(name:Notification.Name(rawValue:"PendingImageNotification"),
                                        object: nil,
                                        userInfo: ["search": "HideSearch"])
                            }
                            
                                self.arrPendingList = self.arrMainList
                                self.arrDatePendingList = self.arrMainList
                               // print("Billpending Count---\(self.arrMainList.count)")
                                self.tblList.reloadData()
                                self.refreshControl.endRefreshing()

                            }
                        }
                        
                    }
                    
                }
            } catch {
                
            }
        }
        //mBillSplitterPendingOffSet
        if screen == "mBillSplitterPendingOffSet" {
            do {
                           if let data = json as? Data {
                               let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                               if let dataDict = dict as? Dictionary<String,AnyObject> {
                                   println_debug(dataDict)
                                   
                                   if let dicData = dataDict["Data"] as? String {
                                       let arrData = OKBaseController.convertToArrDictionary(text: dicData)
                                       
                                       if arrData == nil { return }
                                       
                                       for element in arrData! {
                                           
                                           println_debug(element)
                                           let notificationModel =  BillSplitterAllRequestModel()
                                           notificationModel.id = "\(element["RequestId"]! ?? "")"
                                           
                                           if !(element["AgentName"] is NSNull) {
                                               notificationModel.name = "\(element["AgentName"]! ?? "")"
                                               if(notificationModel.name == "Not Exist")
                                               {
                                                   notificationModel.name = "Unknown"
                                               }
                                           }
                                           else
                                           {
                                                   notificationModel.name = "Unknown"
                                   
                                           }
                                           
                                           notificationModel.remindCount = "\(element["RemindCount"]! ?? "")"

                                           if !(element["CategoryName"] is NSNull) {
                                               notificationModel.category = "\(element["CategoryName"]! ?? "")"
                                           }
                                           else
                                           {
                                               notificationModel.category = "Not Registered"
                                           }
                                           arrFilters.append(notificationModel.category)
                                           notificationModel.requestType = "OK$ Money"
                                           notificationModel.number = "\(element["Destination"]! ?? "")"
                                           notificationModel.localTransactionType = "\(element["LocalTransactionType"]! ?? "")"
                                           notificationModel.commanTransactionType = "\(element["CommanTransactionType"]! ?? "")"

                                           if let ary = element["AttachmentPath"] as? [String] {
                                               if(ary.count > 0)
                                               {
                                               let x: String = ary[0]
                                               println_debug(x)
                                                   notificationModel.attachedFile = "Yes"
                                                   notificationModel.attachedFileURL = "\(x)"
                                               }
                                               else{
                                                   notificationModel.attachedFile = "No"
                                                   }
                                               }
                                           
                                           let intdata = "\(element["Amount"]! ?? "")"
                                           notificationModel.amount = Int(intdata)!
                                       
                                           notificationModel.dateTime = "\(element["Scheduledtime"]! ?? "")"
                                           //Date = "2018-03-29T13:52:34.687";
                                           notificationModel.reminderDate = "\(element["Date"]! ?? "")"
                                           
                                           arrMainList.append(notificationModel)
                                       }
                                       
                                       //println_debug("arrMainList - %@",arrMainList)
                                        DispatchQueue.main.async {
                                       let nc = NotificationCenter.default
                                       if(self.arrMainList.count > 0)
                                       {
                                           if(self.arrMainList.count > 1) {
                                               self.loadFloatButtons()
                                           }
                                           nc.post(name:Notification.Name(rawValue:"PendingImageNotification"),
                                                   object: nil,
                                                   userInfo: ["search": "Search"])
                                           self.tblList.separatorStyle = .none
                                           self.tblList.backgroundView = nil
                                       }
                                       else
                                       {
                                           nc.post(name:Notification.Name(rawValue:"PendingImageNotification"),
                                                   object: nil,
                                                   userInfo: ["search": "HideSearch"])
                                       }
                                       
                                           self.arrPendingList = self.arrMainList
                                           self.arrDatePendingList = self.arrMainList
                                          // print("Billpending Count---\(self.arrMainList.count)")
                                           self.tblList.reloadData()
                                           self.refreshControl.endRefreshing()

                                       }
                                   }
                                   
                               }
                               
                           }
                       } catch {
                           
                       }
        }
        //mBillSplitterCancel
        if (screen == "mBillSplitterPendingCancel")
        {
            
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dataDict = dict as? Dictionary<String,AnyObject> {
                        println_debug(dataDict)
                        if dataDict["Code"]! as! Int == 200
                        {
                            self.getDataFromTheServer()
                            
                            DispatchQueue.main.async {
                               
                            alertViewObj.wrapAlert(title: nil, body:"Bill Splitter Request Cancelled Successfully.".localized, img: #imageLiteral(resourceName: "bill_splitter"))
                            
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                
                            }
                            alertViewObj.showAlert(controller: self)
                          
                                  }
                        }
                        else{
                          DispatchQueue.main.async {
                            alertViewObj.wrapAlert(title: nil, body:"\(dataDict["Msg"]!)".localized, img: #imageLiteral(resourceName: "bill_splitter"))
                            
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                
                            }
                            alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                    
                }
            } catch {
                
            }
        }
        //mBillSplitterReminder
        if (screen == "mBillSplitterPendingReminder")
        {
            
            do {
                if let data = json as? Data {
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dataDict = dict as? Dictionary<String,AnyObject> {
                        println_debug(dataDict)
                        
                        if dataDict["Code"]! as! Int == 200
                        {
                            self.getDataFromTheServer()
                            
                            DispatchQueue.main.async {
                               
                            alertViewObj.wrapAlert(title: nil, body:"Bill Splitter Reminder Sent Successfully.".localized, img: #imageLiteral(resourceName: "bill_splitter"))
                            
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                               
                            }
                            alertViewObj.showAlert(controller: self)
                                
                            }
                    }
                        else if dataDict["Code"]! as! Int == 311
                        {
                             DispatchQueue.main.async {
                            alertViewObj.wrapAlert(title: nil, body:"You can not request to same destination mobile number with in 5 minutes.".localized, img: #imageLiteral(resourceName: "bill_splitter"))
                            
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                
                            }
                            alertViewObj.showAlert(controller: self)
                            }
                        }
                        else{
                             DispatchQueue.main.async {
                            alertViewObj.wrapAlert(title: nil, body:"\(dataDict["Msg"]!)".localized, img: #imageLiteral(resourceName: "bill_splitter"))
                            
                            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                
                            }
                            alertViewObj.showAlert(controller: self)
                            }
                        }
                    }
                }
            } catch {
                
            }
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    deinit {
        NotificationCenter.default.removeObserver(self)
        
    }

}

extension BillSplitterPendingVC : BillSplitterFilterDelegate
{
    func filterDataList(rowData: String)   {
        print("Date Filter------\(savefromDate)---\(savetoDate)")
        print("filterDataList----\(rowData)")

        strFilterTitle = rowData
        
        if (rowData == "Normal".localized)
        {
            print("filterDataList  normal")

            arrPendingList.removeAll()
            arrPendingList = arrDatePendingList
            tblList.reloadData()
        }
        else if (rowData == "Default".localized)
        {
            print("filterDataList  Default")
            
            arrPendingList.removeAll()
            arrPendingList = arrDatePendingList
            tblList.reloadData()
        }
        else{
            let text = rowData
            print ("search: \(String(describing: text))")
            
            arrPendingList.removeAll()
            
            var i = 0
            
            while i < (arrDatePendingList.count)
            {
                let modelObj = arrDatePendingList[i]
                
                if (modelObj.category.localizedCaseInsensitiveContains(text))
                {
                    
                    arrPendingList.append(modelObj)
                    
                    tblList.reloadData()
                }
                i = i + 1
            }
            
            if (arrPendingList.isEmpty) && !text.isEmpty
            {
                tblList.reloadData()
            } else {
            
            self.tblList.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.fade)
                   DispatchQueue.main.async {
                       let indexPath = IndexPath(row: 0, section: 0)
                       self.tblList.scrollToRow(at: indexPath, at: .top, animated: true)
                   }
            }
        }
    }
}

extension BillSplitterPendingVC : BillSplitterSortDelegate
{
    func sortDataList(rowData: String) {
        print("Date sortFilter------\(savefromDate)---\(savetoDate)")
        print("sortDataList----\(rowData)")
        
//        arrPendingList.removeAll()
//        arrPendingList = arrDatePendingList

        println_debug(rowData)
        strSortTitle = rowData
        //["Default","Amount High to Low","Amount Low to High","Name A to Z","Name Z to A"]
        if (rowData == "Default".localized)
        {
            //arrPendingList = arrMainList
            tblList.reloadData()

        }
        else if(rowData == "Amount High to Low".localized)
        {
            let sortedArray: [BillSplitterAllRequestModel] = self.arrPendingList.sorted{ $0.amount > $1.amount }
            if sortedArray.count > 0 {
                arrPendingList = sortedArray
            }
        }
        else if(rowData == "Amount Low to High".localized)
        {
            let sortedArray: [BillSplitterAllRequestModel] = self.arrPendingList.sorted{ $0.amount < $1.amount }
            if sortedArray.count > 0 {
                arrPendingList = sortedArray
            }
        }
        else if(rowData == "Name A to Z".localized)
        {
            arrPendingList = arrPendingList.sorted(by: { (amount1: BillSplitterAllRequestModel, amount2: BillSplitterAllRequestModel) -> Bool in
                return amount1.name < amount2.name
            })
        }
        else if(rowData == "Name Z to A".localized)
        {
            arrPendingList = arrPendingList.sorted(by: { (amount1: BillSplitterAllRequestModel, amount2: BillSplitterAllRequestModel) -> Bool in
                return amount1.name > amount2.name
            })
        }
           
        if (arrPendingList.count > 0)
        {
                       DispatchQueue.main.async {
                           self.tblList.reloadData()
                           let index = IndexPath(row: 0, section: 0)
                           self.tblList.scrollToRow(at: index, at: .top, animated: true)
                           UIView.animate(withDuration: 0.5, animations: {
                               self.view.layoutIfNeeded()
                           })
                       }
            
        }
            
    }
}

    
extension BillSplitterPendingVC : UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 330.0;//Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPendingList.count
    }
    
     func numberOfSections(in tableView: UITableView) -> Int {
            noDataLabel.frame = CGRect(x: 0, y: tableView.frame.origin.y + 15, width: screenWidth, height: 40)

        if self.arrPendingList.count == 0 {
               noDataLabel.isHidden = false
               noDataLabel.font = UIFont(name: appFont, size: appFontSize)
               noDataLabel.text = "No records found!".localized
               noDataLabel.textAlignment = .center
               noDataLabel.font = UIFont(name: appFont, size: 18)
              self.view .addSubview(noDataLabel)
           } else {
                   noDataLabel.isHidden = true
           }
           return 1
       }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
        //print("cellForRowAt--------\(arrPendingList.count)")
        let identifier = "PendingRequestModelCell"
        var cell: PendingRequestModelCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? PendingRequestModelCell
        if cell == nil {
            tableView.register(UINib(nibName: "PendingRequestModelCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? PendingRequestModelCell
        }

        var notificationModel: BillSplitterAllRequestModel!
        notificationModel = arrPendingList[indexPath.row]
        
        cell.nameLabel.text = notificationModel.name
        cell.categoriesLabel.text = notificationModel.category
        cell.requestTypeLabel.text = "OK$ Money".localized
        cell.numberLabel.text = self.getActualNum(notificationModel.number,withCountryCode: "+95")//remove 0095 with 0
        cell.attachedFileLabel.text = notificationModel.attachedFile
        
        cell.amountLabel.text = self.getDigitDisplay("\(notificationModel.amount)") + " MMK"
        
        cell.dateTimeLabel.text = self.getDisplayDate(notificationModel.dateTime)//notificationModel.dateTime
        
        if (notificationModel.attachedFile == "Yes")
        {
            let fulldate = "\(notificationModel.attachedFileURL ?? "")"//notificationModel.attachedFileURL  as? String
            let url = URL(string: fulldate)
            cell.attachFileImage.sd_setImage(with: url, placeholderImage: UIImage(named: "avatar"))
            
        }
        else
        {
            //cell.attachFileImage.image = UIImage(named: "avatar")
            cell.attachFileImage.sd_setImage(with: self.getImageURLFromString(notificationModel.profileImage), placeholderImage: UIImage(named: "avatar"))
        }
        //cell.attachFileImage.layer.cornerRadius =  cell.attachFileImage!.frame.size.width/2
        cell.attachFileImage.layer.masksToBounds = true
        
        cell.cancelButton.tag = indexPath.row
        cell.cancelButton.addTarget(self, action:#selector(self.handleCancelAction(sender:)), for: .touchUpInside)
        
        cell.remindButton.tag = indexPath.row
        cell.remindButton.addTarget(self, action:#selector(self.handleRemindAction(sender:)), for: .touchUpInside)
        
        //Reminder count
        if notificationModel.remindCount != ""  && notificationModel.remindCount != "0"{
            if Int(notificationModel.remindCount)! >= 3{
                cell.remindCountLabel?.text = "3"
            }else{
                cell.remindCountLabel?.text = notificationModel.remindCount
            }
            cell.remindCountLabel?.backgroundColor = UIColor.white
        }else{
            cell.remindCountLabel?.text = ""
            cell.remindCountLabel?.backgroundColor = UIColor.clear
        }
        
        cell.remindCountLabel?.layer.cornerRadius = cell.remindCountLabel!.frame.size.width/2
        cell.remindCountLabel?.layer.masksToBounds = true
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var notificationModel: BillSplitterAllRequestModel!
        notificationModel = arrPendingList[indexPath.row]
        
        let nc = NotificationCenter.default

        if (notificationModel.attachedFile == "Yes")
        {
            nc.post(name:Notification.Name(rawValue:"PendingImageNotification"),
                        object: nil,
                        userInfo: ["image": "\(notificationModel.attachedFileURL ?? "")"])
        }
        
    }
    
    //To select Cancel
    @objc func handleCancelAction(sender:UIButton){
        
        //println_debug("you clicked on button \(sender.tag)")
        
        alertViewObj.wrapAlert(title: nil, body:"Do you want to Cancel Bill Splitter Request?".localized, img: #imageLiteral(resourceName: "bill_splitter"))
        
        alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
            
        }
        alertViewObj.addAction(title: "OK".localized, style: .target, action: {
            
            _ = IndexPath(item: 0, section: sender.tag)
            
            if appDelegate.checkNetworkAvail() {
                let web      = WebApiClass()
                web.delegate = self
                self.showProgressView()
                
                var notificationModel: BillSplitterAllRequestModel!
                notificationModel = self.arrPendingList[sender.tag]
                //(notificationModel.number as Any as! String)
                let rejectAPI = Url.billSplitterCancelBill + "MobileNumber=\(UserModel.shared.mobileNo)&Simid=\(UserModel.shared.simID)&MSID=01&OSType=1&OTP=\(uuid)&RequestId=\(notificationModel.id)&Status=Cancelled"
                let url = getUrl(urlStr: rejectAPI, serverType: .serverApp)
                println_debug(url)
                let params = Dictionary<String,String>()
                
                web.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", mScreen: "mBillSplitterPendingCancel")
                
            }
        })
            
        alertViewObj.showAlert(controller: self)
        
       
    }
    
    //To select Remind
    @objc func handleRemindAction(sender:UIButton){
        
        //println_debug("you clicked on button \(sender.tag)")
        //Check time difference
     
        var notificationModel: BillSplitterAllRequestModel!
        notificationModel = self.arrPendingList[sender.tag]
        
        let currentDate = notificationModel.reminderDate
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss.SSS"
        formatter.timeZone = TimeZone.current
        
        let formatter1 = DateFormatter()
        formatter1.calendar = Calendar(identifier: .gregorian)
        formatter1.dateFormat = "yyyy-MM-dd'T'hh:mm:ss.SSS"
        
        println_debug(date)
        println_debug(currentDate)
        
        let todayStr = formatter.string(from: date)
        let todayFomat = formatter.date(from: todayStr)
        
        let minutes = todayFomat?.minutes(from: formatter1.date(from: currentDate)!)
        
        
        if(minutes! < 6)
        {
            alertViewObj.wrapAlert(title: nil, body:"You can not request to same destination mobile number with in 5 minutes.".localized, img: #imageLiteral(resourceName: "bill_splitter"))
            
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                
            }
            alertViewObj.showAlert(controller: self)

        }
        else
        {
        alertViewObj.wrapAlert(title: nil, body:"Do you want to send Bill Splitter Reminder?".localized, img: #imageLiteral(resourceName: "bill_splitter"))
        
        alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
            
        }
        alertViewObj.addAction(title: "OK".localized, style: .target, action: {
            
            if (Int(notificationModel.remindCount)! < 3)
            {
                if appDelegate.checkNetworkAvail() {
                    let web      = WebApiClass()
                    web.delegate = self
                    self.showProgressView()
                    
                    let date = Date()
                    let dateFormater = DateFormatter()
                    dateFormater.calendar = Calendar(identifier: .gregorian)
                    dateFormater.dateFormat = "MM/dd/yyyy%20hh:mm%20a"
                    dateFormater.amSymbol = "AM"
                    dateFormater.pmSymbol = "PM"
                    let currentDate = dateFormater.string(from: date)
                    
                    let reminderApi = Url.billSplitterReminderBill + "MobileNumber=\(UserModel.shared.mobileNo)&Simid=\(UserModel.shared.simID)&MSID=01&OSType=1&OTP=\(uuid)&Source=\(UserModel.shared.mobileNo)&Destination=\(notificationModel.number as Any as! String)&Amount=\(notificationModel.amount)&Date=\(currentDate)&LocalTransactionType=\(notificationModel.localTransactionType as Any as! String)&TransactionType=\(notificationModel.commanTransactionType as Any as! String)&id=\(notificationModel.id as Any as! String)&RequestModuleType=1"
                    
                    let url = getUrl(urlStr: reminderApi, serverType: .serverApp)
                    let params = Dictionary<String,String>()
                    
                    web.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", mScreen: "mBillSplitterPendingReminder")
                    
                }
            }
            else
            {
                alertViewObj.wrapAlert(title: nil, body:"You can remind request maximum 3 times only".localized, img: #imageLiteral(resourceName: "bill_splitter"))
                
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                    
                }
                alertViewObj.showAlert(controller: self)
                
            }
        })
        
        alertViewObj.showAlert(controller: self)
        }
    }
    
    // MARK: - Custom Methods
       func loadFloatButtons() {
           var buttonItems = [ActionButtonItem]()
           let date = ActionButtonItem(title: "Date".localized, image: #imageLiteral(resourceName: "receiptSortDate"))
           date.action = { item in
               self.multiButton?.toggleMenu()
               let dateByFilterVC = UIStoryboard(name: "BillSplitterView", bundle: Bundle.main).instantiateViewController(withIdentifier: "DateByFilterVC") as! DateByFilterVC
            dateByFilterVC.intSearchStatus = 0
                self.navigationController?.pushViewController(dateByFilterVC, animated: true)
           }
           
           let filter = ActionButtonItem(title: "Filter".localized, image: #imageLiteral(resourceName: "offer_report_menu_icon_filter"))
           filter.action = { item in
               self.multiButton?.toggleMenu()
            
            if self.arrDatePendingList.count > 1 {

            let dateByFilterVC = UIStoryboard(name: "BillSplitterView", bundle: Bundle.main).instantiateViewController(withIdentifier: "BillSplitterFilterVC") as! BillSplitterFilterVC
            dateByFilterVC.arrList = self.arrFilters
            dateByFilterVC.strCheckBoxTitle = self.strFilterTitle
            dateByFilterVC.filterDelegate = self
            self.navigationController?.pushViewController(dateByFilterVC, animated: true)
                
            }
            
           }
           
        let sort = ActionButtonItem(title: "Sort".localized, image: #imageLiteral(resourceName: "offer_report_menu_icon_sort"))
        sort.action = { item in
            self.multiButton?.toggleMenu()
            
            if self.arrPendingList.count > 1 {

            let dateByFilterVC = UIStoryboard(name: "BillSplitterView", bundle: Bundle.main).instantiateViewController(withIdentifier: "BillSplitterSortVC") as! BillSplitterSortVC
            dateByFilterVC.sortDelegate = self
            dateByFilterVC.strCheckBoxTitle = self.strSortTitle
            self.navigationController?.pushViewController(dateByFilterVC, animated: true)
                
            }
        }
        
           buttonItems = [filter, sort, date]
           
           multiButton = ActionButton(attachedToView: self.view, items: buttonItems)
            multiButton?.notifyBlurDelegate = self as? NotifyBlurDelegate
           multiButton?.action = {
               button in button.toggleMenu()
           }
           multiButton?.setImage(#imageLiteral(resourceName: "menu_white_payto"), forState: .normal)
           multiButton?.backgroundColor = kYellowColor
       }
}

