//
//  BillSplitterViewTableExtension.swift
//  OK
//
//  Created by OK$ on 10/03/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation


// MARK: - Table View

extension BillSplitterView : UITableViewDataSource,UITableViewDelegate
{
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    if(section != 0)
    {
      let headerView = UIView()
      headerView.backgroundColor = UIColor.clear
      
      let headerLabel = UILabel(frame: CGRect(x: 10, y: -3, width:
        tableView.bounds.size.width, height: tableView.bounds.size.height))
      headerLabel.textColor = UIColor.darkGray
      headerLabel.text = "Bill Splitter".localized + "-\(section)"
      headerLabel.font = UIFont(name: appFont, size: 14.0)
      headerLabel.sizeToFit()
      headerView.addSubview(headerLabel)
      
      return headerView
    }
    else
    {
      let headerView = UIView()
      return headerView
    }
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
  {
    if(indexPath.section == 0)
    {
      return 149.0
    }
    else
    {
      if isFileAttached == 0  {
        
        if arrContacts[indexPath.section] == "1"
        {
          return 150.0
        }
        return 100.0;//Choose your custom row height
      }
      else{
        
        if arrContacts[indexPath.section] == "1"
        {
          return 200.0
        }
        return 150.0
      }
    }
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return numberOfSection
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if(indexPath.section == 0)
    {
      let identifier = "BillSplitterHeaderCell"
      var cellHeader: BillSplitterHeaderCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BillSplitterHeaderCell
      if cellHeader == nil {
        tableView.register(UINib(nibName: "BillSplitterHeaderCell", bundle: nil), forCellReuseIdentifier: identifier)
        cellHeader = tableView.dequeueReusableCell(withIdentifier: identifier) as? BillSplitterHeaderCell
      }
      cellHeader.btnPlus.addTarget(self, action:#selector(self.plusSignAction(sender:)), for: .touchUpInside)
      cellHeader.btnMinus.addTarget(self, action:#selector(self.minusSignAction(sender:)), for: .touchUpInside)
      cellHeader.btnAttachFile.addTarget(self, action:#selector(self.attachImageAction(sender:)), for: .touchUpInside)
      cellHeader.btnViewFile.addTarget(self, action:#selector(self.viewImageAction(sender:)), for: .touchUpInside)
      
      cellHeader.viewBG.layer.cornerRadius = 10
      cellHeader.viewBG.layer.masksToBounds = true
      
      cellHeader.txtAmount.delegate = self
      cellHeader.txtAmount.tag = 3000
      cellHeader.txtAmount.text = strEnterAmountValue
      cellHeader.txtAmount.addTarget(self, action: #selector(BillSplitterView.textFieldDidChange(_:)),
                                     for: UIControl.Event.editingChanged)
      
      cellHeader.txtAmount.font = UIFont(name: appFont, size: appFontSize)
      cellHeader.txtAmount.placeholder = "Amount".localized
        
        cellHeader.txtAmount.placeHolderColor = .black
      
      if(strEnterAmountValue.length == 0)
      {
        cellHeader.txtAmount.placeholder = "Enter Amount".localized
        cellHeader.txtAmount.placeHolderColor = .black
      }
      
      if(strEnterAmountValue.length > 0)
      {
        cellHeader.txtAmount.becomeFirstResponder()
        cellHeader.txtAmount.placeHolderColor = .black
      }
      
      if(strEnterAmountValue.length == 9)
      {
        cellHeader.txtAmount.textColor = UIColor.green
        cellHeader.txtAmount.placeHolderColor = .black
      }
      else if(strEnterAmountValue.length == 10)
      {
        cellHeader.txtAmount.textColor = UIColor.red
        cellHeader.txtAmount.placeHolderColor = .black
      }
      else
      {
        cellHeader.txtAmount.textColor = UIColor.black
        cellHeader.txtAmount.placeHolderColor = .black
      }
      
      cellHeader.lblPerson.text = "\(numberPerson)"
      
      if(isFileAttached == 1)
      {
        cellHeader.lblAttachFile.isHidden = true
        cellHeader.lblAttachedFile.isHidden = false
        cellHeader.imgViewAttach.isHidden = false
        cellHeader.imgViewAttach.image = imageFile
        cellHeader.imgViewAttach.contentMode = .scaleAspectFit
        cellHeader.btnViewFile.isHidden = false
      }
      else
      {
        cellHeader.lblAttachFile.isHidden = false
        cellHeader.lblAttachedFile.isHidden = true
        cellHeader.imgViewAttach.isHidden = true
        cellHeader.btnViewFile.isHidden = true
      }
      cellHeader.selectionStyle = UITableViewCell.SelectionStyle.none
      
      return cellHeader
    }
    else
    {
      //Attach file with Bill Splitter
      let identifier = "BillSplitterContactCell"
      var cellFile: BillSplitterContactCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? BillSplitterContactCell
      if cellFile == nil {
        tableView.register(UINib(nibName: "BillSplitterContactCell", bundle: nil), forCellReuseIdentifier: identifier)
        cellFile = tableView.dequeueReusableCell(withIdentifier: identifier) as? BillSplitterContactCell
      }
      
      if indexPath.section > 1
      {
        if focusedCell.contains(cellFile.txtMobileNumber.tag) {
          cellFile.txtMobileNumber.isUserInteractionEnabled = true
        } else {
          cellFile.txtMobileNumber.isUserInteractionEnabled = false
        }
      }
      
      cellFile.txtAmount.delegate = self
      cellFile.txtAmount.tag = 1000+indexPath.section
      cellFile.txtAmount.text = self.getDigitDisplay(String(arrAmounts[indexPath.section]))
      cellFile.txtAmount.addTarget(self, action: #selector(BillSplitterView.textFieldDidChange(_:)),
                                   for: UIControl.Event.editingChanged)
      if(cellFile.txtAmount.text?.length == 9)
      {
        cellFile.txtAmount.textColor = UIColor.green
      }
      else if(cellFile.txtAmount.text?.length == 10)
      {
        cellFile.txtAmount.textColor = UIColor.red
      }
      else
      {
        cellFile.txtAmount.textColor = UIColor.black
      }
      
      cellFile.btnContact.addTarget(self, action:#selector(self.handleContactAction(sender:)), for: .touchUpInside)
      cellFile.btnContact.tag = indexPath.section
      
      //cellFile.btnCancel.isHidden = true
      cellFile.btnCancel.addTarget(self, action:#selector(self.handleCancelAction(sender:)), for: .touchUpInside)
      cellFile.btnCancel.tag = indexPath.section
      
      cellFile.btnViewFile.addTarget(self, action:#selector(self.viewImageAction(sender:)), for: .touchUpInside)
      cellFile.btnViewFile.isHidden = true
      
      cellFile.txtMobileNumber.delegate = self
      cellFile.txtMobileNumber.tag = indexPath.section
      cellFile.txtMobileNumber.addTarget(self, action: #selector(BillSplitterView.textFieldDidChange(_:)),
                                         for: UIControl.Event.editingChanged)
      
      cellFile.txtMobileNumber?.text = arrUserData[indexPath.section]["Mobile"]
      
      
      if(cellFile.txtMobileNumber.text?.length == 2 || cellFile.txtMobileNumber.text?.length == 0)
      {
        cellFile.btnCancel.isHidden = true
      }
      else
      {
        cellFile.btnCancel.isHidden = false
      }
      
      
      //Image data manage
      cellFile.lblAttachedFile.isHidden = true
      cellFile.imgViewFile.isHidden = true
      cellFile.lblAttachFile.isHidden = false
      cellFile.switchFile.tag = indexPath.section
      cellFile.switchFile.addTarget(self, action: #selector(self.switchValueDidChange(sender:)), for: .valueChanged)
      cellFile.imgViewFile.image = imageFile
      cellFile.switchFile.isOn = false
      cellFile.selectionStyle = UITableViewCell.SelectionStyle.none
      
      if arrContacts[indexPath.section] == "1" || isComingFromMin
      {
        isComingFromMin = false
        cellFile.cellUserName.constant = 50
        cellFile.txtUserName?.text = arrUserData[indexPath.section]["Name"]?.localized
      }
      else
      {
        cellFile.cellUserName.constant = 0
      }
      
      if(isFileAttached == 1)
      {
        cellFile.cellAttachFile.constant = 50
        
        //manage to show img file
        if arrAttachFiles[indexPath.section] == "1"
        {
          cellFile.btnViewFile.isHidden = false
          cellFile.lblAttachedFile.isHidden = false
          cellFile.imgViewFile.isHidden = false
          cellFile.lblAttachFile.isHidden = true
          cellFile.switchFile.isOn = true
        }
      }
      else
      {
        cellFile.cellAttachFile.constant = 0
      }
      
      
      return cellFile
    }
  }
  
  // MARK: - Custom Cell Methods
  
  //Attach file show and hide on table view cell
  @objc func switchValueDidChange(sender:UISwitch) {
    
    println_debug("switchValueDidChange \(sender.tag)")
    
    if sender.isOn {
      arrAttachFiles[sender.tag] = "1"
      self.tblPerson.reloadSections(IndexSet(integer: sender.tag), with: .none)
      if let prevCell = self.tblPerson.cellForRow(at: IndexPath.init(row: 0, section: sender.tag)) as? BillSplitterContactCell {
        prevCell.txtMobileNumber.isUserInteractionEnabled = true
      }
      if let cell = self.tblPerson.cellForRow(at: IndexPath.init(row: 0, section: sender.tag + 1)) as? BillSplitterContactCell {
        cell.txtMobileNumber.isUserInteractionEnabled = true
      }
      
    } else {
      arrAttachFiles[sender.tag] = "0"
      tblPerson.reloadSections(IndexSet(integer: sender.tag), with: .none)
      if let prevCell = self.tblPerson.cellForRow(at: IndexPath.init(row: 0, section: sender.tag)) as? BillSplitterContactCell {
        prevCell.txtMobileNumber.isUserInteractionEnabled = true
      }
      if let cell = self.tblPerson.cellForRow(at: IndexPath.init(row: 0, section: sender.tag + 1)) as? BillSplitterContactCell {
        cell.txtMobileNumber.isUserInteractionEnabled = true
      }
    }
  }
  
  //Remove contact cell
  @objc func handleCancelAction(sender:UIButton){
    
    println_debug("you clicked on button \(sender.tag)")
    arrUserData.remove(at: sender.tag)
    
    let dictionary: [String: String] = ["Mobile": "","Name":"Unknown"]
    arrUserData.insert(dictionary, at: sender.tag)
    
    arrContacts[sender.tag] = "0"
    
    tblPerson.reloadSections(IndexSet(integer: sender.tag), with: .none)
    let prevCell = self.tblPerson.cellForRow(at: IndexPath.init(row: 0, section: sender.tag)) as? BillSplitterContactCell
    prevCell?.txtMobileNumber.isUserInteractionEnabled = true
    prevCell?.txtMobileNumber.becomeFirstResponder()
    self.enableSendAction()
  }
  
  //To select contact from favorite & contact
  @objc func handleContactAction(sender:UIButton){
    
    print("*******************")
    print(arrUserData[sender.tag]["Mobile"] ?? "")
    print("*******************")
    
    println_debug("you clicked on button \(sender.tag)")
    self.tblPerson.endEditing(true)
    
    intContactSelectionRow = IndexPath(item: 0, section: sender.tag)
    
   
    
    if favoriteManager.count(.payto) > 0 {
      favListButton.isHidden = false
    } else {
      favListButton.isHidden = true
    }
    
    
    makeUserInteractionOfContactButton(tag: sender.tag, indexToCheck: sender.tag - 1, imageSelectionView: viewImageSelection,contactSelection: viewContactSelection)
    
    
    
  }
    
    
    func makeUserInteractionOfContactButton(tag: Int, indexToCheck: Int,imageSelectionView: UIView,contactSelection: UIView){
        var hideView = false
        
        if tag == 1{
          hideView = false
        }else{
            if tag == indexToCheck + 1{
                if arrUserData[indexToCheck]["Mobile"] ?? "" == ""{
                    hideView = true
                    
                }else{
                    hideView = false
                }
            }
        }
        
        imageSelectionView.isHidden = hideView
        contactSelection.isHidden = hideView
        
       self.navigationController?.navigationBar.isUserInteractionEnabled = false
        
    }
  
  @objc func plusSignAction(sender:UIButton) {
    
    if(numberPerson > 5)
    {
      numberPerson = 5
      
      alertViewObj.wrapAlert(title: nil, body:"Maximum 5 peoples only allowed".localized, img: #imageLiteral(resourceName: "bill_splitter"))
      
      alertViewObj.addAction(title: "OK".localized, style: .cancel) {
        
      }
      alertViewObj.showAlert(controller: self)
      
    }
    else if(numberPerson == 5)
    {
      println_debug("max limit reached")
      alertViewObj.wrapAlert(title: nil, body:"Maximum 5 peoples only allowed".localized, img: #imageLiteral(resourceName: "bill_splitter"))
      
      alertViewObj.addAction(title: "OK".localized, style: .cancel) {
        
      }
      alertViewObj.showAlert(controller: self)
    }
    else
    {
      numberPerson += 1
      strEnterAmountValue = ""
      numberOfSection = 1
      btnSend.isHidden = true
      heightBtnSend.constant = 0.0
      isFileAttached = 0
      
      arrAttachFiles = ["0","0","0","0","0","0"]
      arrAmounts = [0,0,0,0,0,0]
      arrContacts = ["0","0","0","0","0","0"]
      arrUserData = [["Mobile": "", "Name": "Unknown"], ["Mobile": "", "Name": "Unknown"],["Mobile": "", "Name": "Unknown"],["Mobile": "", "Name": "Unknown"],["Mobile": "", "Name": "Unknown"],["Mobile": "", "Name": "Unknown"]]
      
    }
    
    tblPerson.reloadData()
  }
  
  @objc func minusSignAction(sender:UIButton) {
    
    strEnterAmountValue = ""
    numberOfSection = 1
    btnSend.isHidden = true
    heightBtnSend.constant = 0.0
    isFileAttached = 0
    
    arrAttachFiles = ["0","0","0","0","0","0"]
    arrAmounts = [0,0,0,0,0,0]
    arrContacts = ["0","0","0","0","0","0"]
    arrUserData = [["Mobile": "", "Name": "Unknown"], ["Mobile": "", "Name": "Unknown"],["Mobile": "", "Name": "Unknown"],["Mobile": "", "Name": "Unknown"],["Mobile": "", "Name": "Unknown"],["Mobile": "", "Name": "Unknown"]]
    
    if(numberPerson <= 1)
    {
      numberPerson = 1
    }
    else{
      numberPerson -= 1
    }
    
    tblPerson.reloadData()
  }
  
  @objc func attachImageAction(sender:UIButton) {
    if(strEnterAmountValue.count > 0)
    {
      self.view.endEditing(true)
      
      viewImageSelection.isHidden = false
      viewSelectionOption.isHidden = false
    }
  }
  
  @objc func viewImageAction(sender:UIButton) {
    
    self.navigationController?.navigationBar.isUserInteractionEnabled = false
    
    viewImageSelection.isHidden = false
    viewSelectionOption.isHidden = true
    viewImage.isHidden = false
    
    imgViewAttachFile.image = imageFile
    
  }
  
 }
