//
//  BillSplitterSortVC.swift
//  OK
//
//  Created by OK$ on 03/12/2019.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

@objc protocol BillSplitterSortDelegate: class {
    @objc optional func sortDataList(rowData: String)
}

class BillSplitterSortVC: OKBaseController {

    var sortDelegate: BillSplitterSortDelegate?

    @IBOutlet weak var btnCancel: UIButton!
        {
        didSet
        {
            self.btnCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.btnCancel.setTitle(self.btnCancel.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var btnOK: UIButton!
        {
        didSet
        {
            self.btnOK.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.btnOK.setTitle(self.btnOK.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var tblList: UITableView!
    var arrSortList: [String] = ["Default","Amount High to Low","Amount Low to High","Name A to Z","Name Z to A"]
    //Show check box
    var strCheckBoxTitle = "Default".localized

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
             
        self.title = "Sort By".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        tblList.reloadData()
    }
    
    @IBAction func backAction(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - extension of Table View

extension BillSplitterSortVC : UITableViewDataSource,UITableViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
            return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return arrSortList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectionSortCell") as? SelectionSortCell
            cell!.titleLabel.text = self.arrSortList[indexPath.row].localized
            cell!.imgCheck.isHidden = true
        if (strCheckBoxTitle == self.arrSortList[indexPath.row].localized)
            {
                cell!.imgCheck.isHidden = false
            }
        
            return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.sortDelegate?.sortDataList?(rowData: arrSortList[indexPath.row].localized)
        self.navigationController?.popViewController(animated: true)
    }
}
