//
//  BillSplitterAllRequest.swift
//  OK
//
//  Created by PC on 10/27/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SDWebImage

class BillSplitterAllRequestView: OKBaseController, PageViewControllerDelegates, UINavigationControllerDelegate  {

    @IBOutlet weak var navigationItemOutlet: UINavigationItem!
    @IBOutlet var searchBar: UISearchBar!
    {
        didSet {
            searchBar.showsCancelButton = true
            searchBar.tintColor = .darkGray
            searchBar.delegate = self
            searchBar.setTextColor(color: Color.black)
            searchBar.placeholder = "Search".localized
             UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = .blue
        }
    }
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var barButtonSearch: UIBarButtonItem!
    @IBOutlet weak var viewTabHolder: UIView!
    
    @IBOutlet weak var container: PageViewController!
  
    @IBOutlet weak var viewImageSelection: UIView!
    
    //Show check box
    var strCheckBoxTitle = ""
    
    var intSearchStatus = 0
    var fromDatePicker = ""
    var toDatePicker = ""

    //Show User Balance
    var isSearchBack:Bool = true
    
    let myPendingImageNotification = Notification.Name(rawValue:"PendingImageNotification")
    let myRejectImageNotification = Notification.Name(rawValue:"RejectImageNotification")
    let mySuccessImageNotification = Notification.Name(rawValue:"SuccessImageNotification")
    

    // MARK: - View Life Cycle

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        
        //Search hidden
        viewSearch.isHidden = true
        self.setSearchButton()
        self.barButtonSearch.tintColor = UIColor.clear
        self.barButtonSearch.isEnabled = false
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "Bill Splitter".localized
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
        
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
               
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.keyboardType = .asciiCapable
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
            }
        }

        if let cancelButton : UIButton = searchBar.value(forKey: "cancelButton") as? UIButton {
            cancelButton.setTitle("Cancel".localized, for:.normal)

            if let navFont = UIFont(name: appFont, size: appButtonSize) {
                cancelButton.titleLabel?.font = navFont
            }
        }
        strCheckBoxTitle = "Default".localized
    
        searchBar.delegate = self
        
        viewImageSelection.isHidden = true
        viewSearch.isHidden = true
        
        // Do any additional setup after loading the view.
        container.getTo(0, animated: true)
        for case let view in viewTabHolder.subviews {
            for case let subData in view.subviews {
                if let button = subData as? UIButton {
                    if(button.tag==0){
                        button.isSelected=true
                        break
                    }
                }
            }
        }
        self.setUpLocalization()
        
        //Handled Attach file selection
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.tranparentViewAction(sender:)))
        viewImageSelection.addGestureRecognizer(gesture)
        
        //Manage Image View Display
        let nc = NotificationCenter.default
        nc.addObserver(forName:myPendingImageNotification, object:nil, queue:nil, using:pendingImageNotification)
        nc.addObserver(forName:myRejectImageNotification, object:nil, queue:nil, using:rejectImageNotification)
        nc.addObserver(forName:mySuccessImageNotification, object:nil, queue:nil, using:successImageNotification)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    fileprivate func setUpLocalization()
    {
        for case let viewLocal in viewTabHolder.subviews
        {
            for case let label as UILabel in viewLocal.subviews
            {
                label.font = UIFont(name: appFont, size: 15.0)
                label.text = label.text?.localized
            }
        }
    }
    
    // MARK: - Notification
    func pendingImageNotification(notification:Notification) -> Void {
        //println_debug("pendingImageNotification")
        DispatchQueue.main.async {
            self.newclearSearchBar()
        if ((notification.userInfo?["search"] as? String) == "HideSearch")
        {
                self.barButtonSearch.tintColor = UIColor.clear
                self.barButtonSearch.isEnabled = false
        }
        else
        {
            self.barButtonSearch.tintColor = UIColor.white
            self.barButtonSearch.isEnabled = true
        }
        }
        
        if ((notification.userInfo?["dateBy"] as? String) != nil)
        {
            let fulldate = notification.userInfo?["dateBy"] as? String
            let arrayDate = fulldate?.components(separatedBy: "&&")
            fromDatePicker = "\(arrayDate![0])"
            toDatePicker = "\(arrayDate![1])"
            
       
        }
        else if ((notification.userInfo?["image"] as? String) != nil)
        {
            guard let image = notification.userInfo?["image"] as? String else {
                showToast(message: "No Files Attached")
                return
            }
            
            let vc = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PTGalleryViewController") as? PTGalleryViewController
            vc?.arrayImages = [image]
            vc?.reqMoney = "BillSplitter"
            if let nav = self.navigationController {
                nav.pushViewController(vc!, animated: true)
            }
        }
    }
    
    func rejectImageNotification(notification:Notification) -> Void {
        //println_debug("rejectImageNotification")

         DispatchQueue.main.async {
            self.newclearSearchBar()

        if ((notification.userInfo?["search"] as? String) == "HideSearch")
        {
             self.barButtonSearch.tintColor = UIColor.clear
             self.barButtonSearch.isEnabled = false
        }
        else
        {
             self.barButtonSearch.tintColor = UIColor.white
             self.barButtonSearch.isEnabled = true
        }
    }
        if ((notification.userInfo?["dateBy"] as? String) != nil)
        {
            let fulldate = notification.userInfo?["dateBy"] as? String
            let arrayDate = fulldate?.components(separatedBy: "&&")
            fromDatePicker = "\(arrayDate![0])"
            toDatePicker = "\(arrayDate![1])"
            
        }
        else if ((notification.userInfo?["image"] as? String) != nil)
        {
            guard let image = notification.userInfo?["image"] as? String else {
                showToast(message: "No Files Attached")
                return
            }
            
            let vc = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PTGalleryViewController") as? PTGalleryViewController
            vc?.arrayImages = [image]
            
            if let nav = self.navigationController {
                nav.pushViewController(vc!, animated: true)
            }
        }
    }
    
    func successImageNotification(notification:Notification) -> Void {
        //println_debug("successImageNotification")

         DispatchQueue.main.async {
            self.newclearSearchBar()

        if ((notification.userInfo?["search"] as? String) == "HideSearch")
        {
             self.barButtonSearch.tintColor = UIColor.clear
             self.barButtonSearch.isEnabled = false
        }
        else
        {
             self.barButtonSearch.tintColor = UIColor.white
             self.barButtonSearch.isEnabled = true
        }
    }
    
        if ((notification.userInfo?["dateBy"] as? String) != nil)
        {
            let fulldate = notification.userInfo?["dateBy"] as? String
            let arrayDate = fulldate?.components(separatedBy: "&&")
            fromDatePicker = "\(arrayDate![0])"
            toDatePicker = "\(arrayDate![1])"
            
            
        }
        else if ((notification.userInfo?["image"] as? String) != nil)
        {
            guard let image = notification.userInfo?["image"] as? String else {
                showToast(message: "No Files Attached")
                return
            }
            
            let vc = UIStoryboard(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PTGalleryViewController") as? PTGalleryViewController
            vc?.arrayImages = [image]
            
            if let nav = self.navigationController {
                nav.pushViewController(vc!, animated: true)
            }
        }
    }
        
    @objc func tranparentViewAction(sender : UITapGestureRecognizer) {
        // Do what you want

        viewImageSelection.isHidden = true
    }

    
    
    // MARK: - Custom Methods
    func setupSearchBar() {
        navigationItemOutlet.titleView = searchBar
        navigationItemOutlet.title = ""
//        navigationItemOutlet.rightBarButtonItem?.isEnabled = false
//        navigationItemOutlet.rightBarButtonItem?.tintColor = UIColor.clear
        
        navigationItemOutlet.rightBarButtonItem = nil
        
        self.viewSearch.isHidden = false
        if let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
               
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.keyboardType = .asciiCapable
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
            }
            
            searchTextField.placeholder = "Search".localized
            searchTextField.placeHolderColor = .darkGray
            searchTextField.backgroundColor = .white
            
        }
        
//        if let cancelButton : UIButton = searchBar.value(forKey: "cancelButton") as? UIButton {
//            cancelButton.setTitle("Cancel".localized, for:.normal)
//            
//            if let navFont = UIFont(name: "Zawgyi-One", size: 18) {
//                cancelButton.titleLabel?.font = navFont
//            }
//        }
        //self.searchBar.text = "Gauri"
        //self.searchBar.placeholder = "Search".localized
        self.searchBar.delegate = self
        self.searchBar.becomeFirstResponder()
        
    }
    
    @IBAction func searchAction(_ sender: Any) {
        //println_debug("search selected")
        
        if  isSearchBack == true {
             self.setupSearchBar()
            isSearchBack = false
            viewImageSelection.isHidden = true
        }
        else{
            self.clearSearchBar()
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        if(isSearchBack)
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.clearSearchBar()
        }
    }
    
    //
    // MARK: - Pageview Controller Delegate
    //
    
    func didScroll(to index: Int) {
        intSearchStatus = index
   
        for case let view in viewTabHolder.subviews {
            for case let subData in view.subviews {
                if let button = subData as? UIButton {
                    
                    if(button.tag==index){
                        button.isSelected=true
                    }else{
                        button.isSelected=false
                    }
                }
            }
        }
    }
    
    @IBAction func btnActionTab(_ sender: Any) {
        
        intSearchStatus = (sender as AnyObject).tag
        container.getTo((sender as AnyObject).tag, animated: true)
        
        for case let view in viewTabHolder.subviews {
            for case let subData in view.subviews {
                if let button = subData as? UIButton {
                    if(button.tag==(sender as AnyObject).tag){
                        button.isSelected=true
                    }else{
                        button.isSelected=false
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EMBEDD_TAB"
        {
          container = segue.destination as? PageViewController
            
            let commonStoryboard = UIStoryboard.init(name: "BillSplitterView", bundle: nil)
            
            let PendingObj = commonStoryboard.instantiateViewController(withIdentifier: "BillSplitterPendingVC") as! BillSplitterPendingVC
            
            let SuccessfulObj = commonStoryboard.instantiateViewController(withIdentifier: "BillSplitterSuccessfulVC") as! BillSplitterSuccessfulVC

            let RejectObj = commonStoryboard.instantiateViewController(withIdentifier: "BillSplitterRejectVC") as! BillSplitterRejectVC

            container.arrViewController=[PendingObj,SuccessfulObj,RejectObj]
            container.mDelegate = self
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}

// MARK: -UISearchBar

extension BillSplitterAllRequestView : UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
       // print("textDidChange--------\(searchText)")
       // print("textDidChange--------\(searchText.count)")
        
        if searchText.count == 0 {
            
            let nc = NotificationCenter.default
            
            if intSearchStatus == 0
            {
                nc.post(name:Notification.Name(rawValue:"PendingNotification"),
                        object: nil,
                        userInfo: ["search":searchText])
            }
            else if intSearchStatus == 1
            {
                nc.post(name:Notification.Name(rawValue:"SuccessfullNotification"),
                        object: nil,
                        userInfo: ["search":searchText])
            }
            else if intSearchStatus == 2
            {
                nc.post(name:Notification.Name(rawValue:"RejectNotification"),
                        object: nil,
                        userInfo: ["search":searchText])
            }
            
        }

    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
   
        if let searchText = searchBar.text, let textRange = Range(range, in: searchText) {
            if (text.contains("\n")){
                searchBar.endEditing(true)
                return false
            }
            let updatedText = searchText.replacingCharacters(in: textRange, with: text)
            if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: SEARCHCHARSETOffer).inverted).joined(separator: "")) { return false }
            
            print("Shouldchangecount---\(updatedText.count)")
            if updatedText == " " || updatedText == "  "{
                return false
            }
            if (updatedText.contains("  ")){
                return false
            }
            
            if updatedText.count > 50 {
                return false
            }
          //  if updatedText != "" && text != "\n" {
                let nc = NotificationCenter.default
                
                if intSearchStatus == 0
                {
                    nc.post(name:Notification.Name(rawValue:"PendingNotification"),
                            object: nil,
                            userInfo: ["search":updatedText])
                }
                else if intSearchStatus == 1
                {
                    nc.post(name:Notification.Name(rawValue:"SuccessfullNotification"),
                            object: nil,
                            userInfo: ["search":updatedText])
                }
                else if intSearchStatus == 2
                {
                    nc.post(name:Notification.Name(rawValue:"RejectNotification"),
                            object: nil,
                            userInfo: ["search":updatedText])
                }
           // }
        }
        return true
    }
    
// called when cancel button is clicked
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {

        searchBar.text = ""
        
        let nc = NotificationCenter.default
        
        if intSearchStatus == 0
        {
            nc.post(name:Notification.Name(rawValue:"PendingNotification"),
                    object: nil,
                    userInfo: ["cancel":"yes"])
        }
        else if intSearchStatus == 1
        {
            nc.post(name:Notification.Name(rawValue:"SuccessfullNotification"),
                    object: nil,
                    userInfo: ["cancel":"yes"])
        }
        else if intSearchStatus == 2
        {
            nc.post(name:Notification.Name(rawValue:"RejectNotification"),
                    object: nil,
                    userInfo: ["cancel":"yes"])
        }
        
        self.clearSearchBar()
}

    func setSearchButton() {
        let buttonIcon      = UIImage(named: "r_search")
        let rightBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: self, action: #selector(searchAction))
        rightBarButton.image = buttonIcon
        self.navigationItem.rightBarButtonItem = rightBarButton
        barButtonSearch = rightBarButton
    }
    
    func newclearSearchBar(){
            self.setSearchButton()
            isSearchBack = true
            viewSearch.isHidden = true
            searchBar.resignFirstResponder()
            self.view.endEditing(true)
            
            navigationItemOutlet.titleView = nil
            navigationItemOutlet.title = "Bill Splitter".localized
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]

            navigationItemOutlet.rightBarButtonItem?.isEnabled = true
            navigationItemOutlet.rightBarButtonItem?.tintColor = UIColor.white;
            
            searchBar.text = ""
        }
    
    func clearSearchBar(){
        self.setSearchButton()
        isSearchBack = true
        viewSearch.isHidden = true
        searchBar.resignFirstResponder()
        self.view.endEditing(true)
        
        navigationItemOutlet.titleView = nil
        navigationItemOutlet.title = "Bill Splitter".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]

        navigationItemOutlet.rightBarButtonItem?.isEnabled = true
        navigationItemOutlet.rightBarButtonItem?.tintColor = UIColor.white;
      
        let nc = NotificationCenter.default
      
        searchBar.text = ""
        
        if intSearchStatus == 0
        {
            nc.post(name:Notification.Name(rawValue:"PendingNotification"),
                    object: nil,
                    userInfo: ["search":""])
        }
        else if intSearchStatus == 1
        {
            nc.post(name:Notification.Name(rawValue:"SuccessfullNotification"),
                    object: nil,
                    userInfo: ["search":""])
        }
        else if intSearchStatus == 2
        {
            nc.post(name:Notification.Name(rawValue:"RejectNotification"),
                    object: nil,
                    userInfo: ["search":""])
        }
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
       
        searchBar.endEditing(true)
        searchBar.resignFirstResponder()
    }
    
   
}
