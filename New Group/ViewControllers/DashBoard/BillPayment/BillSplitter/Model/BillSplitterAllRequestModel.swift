//
//  BillSplitterAllRequestModel.swift
//  OK
//
//  Created by PC on 10/27/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BillSplitterAllRequestModel: NSObject {

    var
    id : String = "",
    name : String = "",
    category : String = "",
    requestType : String = "",
    number : String = "",
    attachedFile : String = "",
    attachedFileURL : Any?    = nil,
    amount : Int = 0,
    dateTime : String = "",
    reminderDate : String = "",
    remindCount : String = "",
    localTransactionType : String = "",
    profileImage : String = "",
    commanTransactionType: String = ""
}
