//
//  RequestModelCell.swift
//  OK
//
//  Created by PC on 10/29/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class RequestModelCell: UITableViewCell {

    @IBOutlet weak var lblStatus: UILabel!{
        didSet{
            lblStatus.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var attachFileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!{
        didSet{
            nameLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var categoriesLabel: UILabel!{
        didSet{
            categoriesLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var requestTypeLabel: UILabel!{
        didSet{
            requestTypeLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var numberLabel: UILabel!{
        didSet{
            numberLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var attachedFileLabel: UILabel!{
        didSet{
            attachedFileLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var amountLabel: UILabel!{
        didSet{
            amountLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var dateTimeLabel: UILabel!{
        didSet{
            dateTimeLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var nameTitleLabel: UILabel!
    {
        didSet
        {
            self.nameTitleLabel.font = UIFont(name: appFont, size: appFontSize)
            self.nameTitleLabel.text = self.nameTitleLabel.text?.localized
        }
    }
    @IBOutlet weak var categoriesTitleLabel: UILabel!
    {
        didSet
        {
            self.categoriesTitleLabel.font = UIFont(name: appFont, size: appFontSize)
            self.categoriesTitleLabel.text = self.categoriesTitleLabel.text?.localized
        }
    }
    @IBOutlet weak var requestTypeTitleLabel: MarqueeLabel!
    {
        didSet
        {
            self.requestTypeTitleLabel.font = UIFont(name: appFont, size: appFontSize)
            self.requestTypeTitleLabel.text = "Request Type".localized//self.requestTypeTitleLabel.text?.localized
        }
    }
    @IBOutlet weak var numberTitleLabel: UILabel!
    {
        didSet
        {
            self.numberTitleLabel.font = UIFont(name: appFont, size: appFontSize)
            self.numberTitleLabel.text = self.numberTitleLabel.text?.localized
        }
    }
    @IBOutlet weak var attachedFileTitleLabel: UILabel!
    {
        didSet
        {
            self.attachedFileTitleLabel.font = UIFont(name: appFont, size: appFontSize)
            self.attachedFileTitleLabel.text = self.attachedFileTitleLabel.text?.localized
        }
    }
    @IBOutlet weak var amountTitleLabel: UILabel!
    {
        didSet
        {
            self.amountTitleLabel.font = UIFont(name: appFont, size: appFontSize)
            self.amountTitleLabel.text = self.amountTitleLabel.text?.localized
        }
    }
    @IBOutlet weak var dateTimeTitleLabel: UILabel!
    {
        didSet
        {
            self.dateTimeTitleLabel.font = UIFont(name: appFont, size: appFontSize)
            self.dateTimeTitleLabel.text = self.dateTimeTitleLabel.text?.localized
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.attachFileImage.cornerEdge(radius: self.attachFileImage.frame.size.height / 2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
