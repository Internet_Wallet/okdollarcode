//
//  NoRecordsView.swift
//  OK
//
//  Created by PC on 10/29/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class NoRecordsView: UIView {

    @IBOutlet weak var lblNoRecord: UILabel!
        {
        didSet
        {
            lblNoRecord.font = UIFont(name: appFont, size: appFontSize)
            lblNoRecord.text = "No records found!".localized
        }
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
