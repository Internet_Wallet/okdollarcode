//
//  BillSplitterContactCell.swift
//  OK
//
//  Created by PC on 11/6/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BillSplitterContactCell: UITableViewCell,UITextFieldDelegate {

    @IBOutlet weak var cellAttachFile: NSLayoutConstraint!
    @IBOutlet weak var cellUserName: NSLayoutConstraint!
    @IBOutlet weak var switchFile: UISwitch!
    @IBOutlet weak var imgViewFile: UIImageView!
    @IBOutlet weak var lblAttachedFile: UILabel!{
        didSet{
            lblAttachedFile.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblAttachFile: UILabel!{
        didSet{
            //lblAttachedFile.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var txtAmount: UITextField!{
        didSet{
            txtAmount.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var txtMobileNumber: UITextField!{
        didSet{
            txtMobileNumber.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var txtUserName: UITextField!{
        didSet{
            txtUserName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnContact: UIButton!{
        didSet{
            btnContact.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnCancel: UIButton!{
        didSet{
            btnCancel.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnViewFile: UIButton!{
        didSet{
            btnViewFile.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    var contactsList: [Dictionary<String, Any>]?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewBG.layer.masksToBounds = true
        viewBG.layer.cornerRadius = 10
        viewBG.layer.borderColor = UIColor.lightGray.cgColor
        viewBG.layer.borderWidth = 1
        txtMobileNumber.placeholder = txtMobileNumber.placeholder?.localized
        txtAmount.placeholder = txtAmount.placeholder?.localized
        txtUserName.placeholder = txtUserName.placeholder?.localized
        lblAttachFile.text = lblAttachFile.text?.localized
        lblAttachedFile.text = lblAttachedFile.text?.localized
     
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

