//
//  PendingRequestModelCell.swift
//  OK
//
//  Created by PC on 3/26/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PendingRequestModelCell: UITableViewCell {

    @IBOutlet weak var attachFileImage: UIImageView!
    @IBOutlet weak var remindButton: UIButton!
    {
        didSet
        {
            self.remindButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
            self.remindButton.setTitle(self.remindButton.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var cancelButton: UIButton!
    {
        didSet
        {
            self.cancelButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
            self.cancelButton.setTitle(self.cancelButton.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var nameLabel: UILabel!{
        didSet{
            self.nameLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var categoriesLabel: UILabel!{
        didSet{
            self.categoriesLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var requestTypeLabel: UILabel!{
        didSet{
            self.requestTypeLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var numberLabel: UILabel!{
        didSet{
            self.numberLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var attachedFileLabel: UILabel!{
        didSet{
            self.attachedFileLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var amountLabel: UILabel!{
        didSet{
            self.amountLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var dateTimeLabel: UILabel!{
        didSet{
            self.dateTimeLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var remindCountLabel: UILabel!{
        didSet{
            self.remindCountLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var nameTitleLabel: UILabel!
    {
        didSet
        {
            self.nameTitleLabel.font = UIFont(name: appFont, size: appFontSize)
            self.nameTitleLabel.text = self.nameTitleLabel.text?.localized
        }
    }
    @IBOutlet weak var categoriesTitleLabel: UILabel!
    {
        didSet
        {
            self.categoriesTitleLabel.font = UIFont(name: appFont, size: appFontSize)
            self.categoriesTitleLabel.text = self.categoriesTitleLabel.text?.localized
        }
    }
    @IBOutlet weak var requestTypeTitleLabel: MarqueeLabel!
    {
        didSet
        {
            self.requestTypeTitleLabel.font = UIFont(name: appFont, size: appFontSize)
            self.requestTypeTitleLabel.text =  "Request Type".localized//self.requestTypeTitleLabel.text?.localized
        }
    }
    @IBOutlet weak var numberTitleLabel: UILabel!
    {
        didSet
        {
            self.numberTitleLabel.font = UIFont(name: appFont, size: appFontSize)
            self.numberTitleLabel.text = self.numberTitleLabel.text?.localized
        }
    }
    @IBOutlet weak var attachedFileTitleLabel: UILabel!
    {
        didSet
        {
            self.attachedFileTitleLabel.font = UIFont(name: appFont, size: appFontSize)
            self.attachedFileTitleLabel.text = self.attachedFileTitleLabel.text?.localized
        }
    }
    @IBOutlet weak var amountTitleLabel: UILabel!
    {
        didSet
        {
            self.amountTitleLabel.font = UIFont(name: appFont, size: appFontSize)
            self.amountTitleLabel.text = self.amountTitleLabel.text?.localized
        }
    }
    @IBOutlet weak var dateTimeTitleLabel: UILabel!
    {
        didSet
        {
            self.dateTimeTitleLabel.font = UIFont(name: appFont, size: appFontSize)
            self.dateTimeTitleLabel.text = self.dateTimeTitleLabel.text?.localized
        }
    }
    @IBOutlet weak var remindCountTitleLabel: UILabel!
    {
        didSet
        {
            self.remindCountTitleLabel.font = UIFont(name: appFont, size: appFontSize)
            self.remindCountTitleLabel.text = self.remindCountTitleLabel.text?.localized
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.attachFileImage.cornerEdge(radius: self.attachFileImage.frame.size.height / 2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
