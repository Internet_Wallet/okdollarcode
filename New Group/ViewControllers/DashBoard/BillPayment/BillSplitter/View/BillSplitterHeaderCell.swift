//
//  BillSplitterHeaderCell.swift
//  OK
//
//  Created by PC on 12/29/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class BillSplitterHeaderCell: UITableViewCell {

    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var lblPerson: UILabel!{
        didSet{
            lblPerson.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnPlus: UIButton!{
        didSet{
            btnPlus.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var txtAmount: SkyFloatingLabelTextField!{
        didSet{
            txtAmount.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgViewAttach: UIImageView!
    @IBOutlet weak var noOfSplitterLbl: UILabel!{
        didSet{
            noOfSplitterLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblAttachedFile: UILabel!{
        didSet{
            lblAttachedFile.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblAttachFile: UILabel!{
        didSet{
            lblAttachFile.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnAttachFile: UIButton!{
        didSet{
            btnAttachFile.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)

        }
    }
    @IBOutlet weak var btnViewFile: UIButton!{
        didSet{
            btnViewFile.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtAmount.placeholder = txtAmount.placeholder?.localized
        noOfSplitterLbl.text = noOfSplitterLbl.text?.localized
        lblAttachFile.text = lblAttachFile.text?.localized
        lblAttachedFile.text = lblAttachedFile.text?.localized
        
        txtAmount.placeholderColor = .black
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
