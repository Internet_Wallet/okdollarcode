//
//  SelectionCell.swift
//  OK
//
//  Created by PC on 2/1/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class SelectionCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!{
        didSet{
            self.titleLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var imgCheck: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
