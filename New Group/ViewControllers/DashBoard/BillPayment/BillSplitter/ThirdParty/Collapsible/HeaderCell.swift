//
//  HeaderCell.swift
//  ios-swift-collapsible-table-section-in-grouped-section
//
//  Created by Yong Su on 5/31/16.
//  Copyright © 2016 Yong Su. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!{
        didSet{
            self.titleLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var toggleButton: UIButton!

}
