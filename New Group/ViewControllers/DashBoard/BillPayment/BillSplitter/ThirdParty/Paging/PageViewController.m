//
//  PageViewController.m
//
//

#import "PageViewController.h"


@interface PageViewController ()<UIPageViewControllerDataSource, UIPageViewControllerDelegate >

@end

@implementation PageViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.delegate = self;
    self.dataSource = self;
    
    [[UIPageControl appearance] setPageIndicatorTintColor: [UIColor yellowColor]];
    [[UIPageControl appearance] setCurrentPageIndicatorTintColor: [UIColor whiteColor]];
    [[UIPageControl appearance] setBackgroundColor: [UIColor redColor]];

    //[self setViewControllers:@[_arrViewController[0]] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIPageviewController Delegates and Datasources -

-(UIViewController*)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(nonnull UIViewController *)viewController{

    if (_circular) {
            NSInteger currentIdx =  [_arrViewController indexOfObject:viewController];
            NSInteger prevIdx = ((currentIdx-1)%_arrViewController.count);
            return _arrViewController[prevIdx];
    }
    else{
    NSInteger index =  [_arrViewController indexOfObject:viewController];
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    index--;
    return _arrViewController[index];
    }
}

-(UIViewController*)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    
    if (_circular) {
            NSInteger currentIdx = [_arrViewController indexOfObject:viewController];
            NSInteger nextIdx = ((currentIdx+1)%_arrViewController.count);
            NSLog(@"Index%ld",(long)nextIdx);
            return _arrViewController[nextIdx];
    }
    else{
    NSInteger index = [_arrViewController indexOfObject:viewController];
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == _arrViewController.count) {
        return nil;
    }
    return _arrViewController[index];
    }
}

//-(NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController{
//    return _arrViewController.count;
//}
//
//-(NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController{
//   
//    return 0;
//}

-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed{

    _currentIndex = [_arrViewController indexOfObject:[self.viewControllers objectAtIndex:0]];
    if (completed) {
       [_mDelegate didScrollToIndex:_currentIndex];
    }
}

#pragma mark -  internal methods -
-(void)getToIndex:(NSInteger)index animated:(BOOL)animated{
    if (index>_currentIndex) {
        
        [self setViewControllers:@[_arrViewController[index]] direction:UIPageViewControllerNavigationDirectionForward animated:animated completion:nil];
    }
    else{
        [self setViewControllers:@[_arrViewController[index]] direction:UIPageViewControllerNavigationDirectionReverse animated:animated completion:nil];
    }
    _currentIndex = index;
}

@end
