//
//  InstaPayCollectionView.swift
//  OK
//
//  Created by E J ANTONY on 4/11/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation
import UIKit

//MARK: - UICollectionViewDataSource
extension InstaPayViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrAgentsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InstaPayCCell", for: indexPath) as? InstaPayCCell else { return InstaPayCCell()}
        let favContact = self.arrAgentsList[indexPath.row]
        cell.loadCell(title: favContact.UserContactData?.FirstName ?? "")
        if arrOffersRecord.contains(favContact.UserContactData?.PhoneNumber ?? "") {
            cell.imageViewOffers.isHidden = false
        } else {
            cell.imageViewOffers.isHidden = true
        }
        return cell
    }
    /*
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        switch kind {
        case UICollectionView.elementKindSectionHeader:
            guard let headerView = collectionView.dequeueReusableSupplementaryView(
                    ofKind: kind,
                    withReuseIdentifier: "InstaPayCollectionHeader",
                    for: indexPath) as? InstaPayCollectionHeader
                else {
                    fatalError("Invalid view type")
            }
            return headerView
        default:
            assert(false, "Invalid element type")
        }
    }*/
//        var testHeader:
//        switch kind {
//        case UICollectionView.elementKindSectionHeader:
//            guard let headerView = collectionView.dequeueReusableSupplementaryView(
//                    ofKind: kind,
//                    withReuseIdentifier: "InstaPayCollectionHeader",
//                    for: indexPath) as? InstaPayCollectionHeader
//                else {
//                    fatalError("Invalid view type")
//            }
//            return headerView
//        default:
//            fatalError("Invalid view type")
//        }

    }
    
//MARK: - UICollectionViewDelegate
extension InstaPayViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //For static testing
        /*
        alertViewObj.wrapAlert(title: nil, body:"Do you want to use promo code for this number?".localized, img: #imageLiteral(resourceName: "NoOffersFound"))
        alertViewObj.addAction(title: "YES".localized, style: .cancel) {
            if let addWithdrawView = UIStoryboard(name: "Offers", bundle: nil).instantiateViewController(withIdentifier: "OffersViewController") as? OffersViewController {
                addWithdrawView.navStatus = "InstaPay"
                let aObjNav = UINavigationController(rootViewController: addWithdrawView)
                self.present(aObjNav, animated: true, completion: nil)
            }
        }
        alertViewObj.addAction(title: "NO".localized, style: .cancel) {
            let storyboard = UIStoryboard(name: "BookOnOKDollar", bundle: nil)
            guard let sendVC = storyboard.instantiateViewController(withIdentifier: "InstaPaySendViewController") as? InstaPaySendViewController else { return }
            sendVC.profileImage = self.imgViewProfile.image
            sendVC.contactModel = self.arrAgentsList[indexPath.row]
            let nc = UINavigationController(rootViewController: sendVC)
            nc.navigationBar.barTintColor = kYellowColor
            self.present(nc, animated: true, completion: nil)
        }
        alertViewObj.showAlert()
        */
        //For Dynamic testing
        //*
        let favContact = self.arrAgentsList[indexPath.row]
        if arrOffersRecord.contains(favContact.UserContactData?.PhoneNumber ?? "") {
            alertViewObj.wrapAlert(title: nil, body:"Do you want to use promo code for this number?".localized, img: #imageLiteral(resourceName: "NoOffersFound"))
            alertViewObj.addAction(title: "YES".localized, style: .cancel) {
                if let addWithdrawView = UIStoryboard(name: "Offers", bundle: nil).instantiateViewController(withIdentifier: "OffersViewController") as? OffersViewController {
                    //let aObjNav = UINavigationController(rootViewController: addWithdrawView)
                    addWithdrawView.modalPresentationStyle = .fullScreen
                    self.present(addWithdrawView, animated: true, completion: nil)
                }
            }
            alertViewObj.addAction(title: "NO".localized, style: .cancel) {
                let storyboard = UIStoryboard(name: "BookOnOKDollar", bundle: nil)
                guard let sendVC = storyboard.instantiateViewController(withIdentifier: "InstaPaySendViewController") as? InstaPaySendViewController else { return }
                sendVC.profileImage = self.imgViewProfile.image
            //    sendVC.contactModel = self.arrAgentsList[indexPath.row]
                sendVC.modalPresentationStyle = .fullScreen
                let nc = UINavigationController(rootViewController: sendVC)
                nc.navigationBar.barTintColor = kYellowColor
                self.present(nc, animated: true, completion: nil)
            }
            alertViewObj.showAlert()
        } else {
            let storyboard = UIStoryboard(name: "BookOnOKDollar", bundle: nil)
            guard let sendVC = storyboard.instantiateViewController(withIdentifier: "InstaPaySendViewController") as? InstaPaySendViewController else { return }
            sendVC.profileImage = self.imgViewProfile.image
         //   sendVC.contactModel = self.arrAgentsList[indexPath.row]
            sendVC.modalPresentationStyle = .fullScreen
            let nc = UINavigationController(rootViewController: sendVC)
            nc.navigationBar.barTintColor = kYellowColor
            self.present(nc, animated: true, completion: nil)
        }
         //*/
    }
}

//MARK: - UICollectionViewDelegateFlowLayout
extension InstaPayViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (UIScreen.main.bounds.size.width - 25) / 4
        let size = CGSize(width: width, height: 80)
        return size
    }
}

//MARK: - Additional InstaPayViewController
//extension InstaPayViewController {
//    func showCollection(isHide: Bool) {
//        self.collectionViewReceiver.isHidden = isHide
//    }
//}

//MARK: UICollectionReusableView
class InstaPayCollectionHeader: UICollectionReusableView {
}

//MARK: - InstaPayCCell
class InstaPayCCell : UICollectionViewCell {
    //MARK: - Properties
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var viewHolder: UIView!
    @IBOutlet weak var bgLabel: UILabel!
    @IBOutlet weak var imageViewOffers: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        viewHolder.layer.cornerRadius = imageView.frame.size.width / 2
    }
    
    //MARK: - Methods
    func loadCell(title: String) {
        self.nameLabel.text = title
        self.bgLabel.backgroundColor = randomColor()
        self.bgLabel.text = String(title.prefix(1))
    }
    
    func randomColor() -> UIColor {
        var randomRed:CGFloat = 0.0
        var randomGreen:CGFloat = 0.0
        var randomBlue:CGFloat = 0.0
        let nonRandomAlpha:CGFloat = 1.0
        // reject colors that are too dark -- colors //
        // are rejected 0.35/3 = %11.6 of the time   //
        while ( randomRed + randomGreen + randomBlue < 0.35) {
            // for each color channel we generate a random number //
            randomRed = CGFloat(drand48())
            randomGreen = CGFloat(drand48())
            randomBlue = CGFloat(drand48())
        }
        return UIColor(red: randomRed,
                       green: randomGreen,
                       blue: randomBlue,
                       alpha: nonRandomAlpha)
    }
}
