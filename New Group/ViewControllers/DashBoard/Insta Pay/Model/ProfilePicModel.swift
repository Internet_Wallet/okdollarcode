//
//  ProfilePicModel.swift
//  OK
//
//  Created by E J ANTONY on 6/22/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation
import UIKit

class ProfilePicModel: NSObject {
    
     var FirstName:String? = ""
    var PhoneNumber:String? = ""
    var ProfileImageUrl:String? = ""
     var BusinessImageUrl:String? = ""
     var HaveOffer:String? = ""
     var type: Int? = 0
    
    init(dictionary: Dictionary<String, AnyObject>) {
        
        if let title = dictionary["MobileNumber"] as? String {
            self.PhoneNumber = title
        }
        if let title = dictionary["ProfilePic"] as? String   {
            self.ProfileImageUrl = title
        }
        if let title = dictionary["BusinessPic"] as? String   {
            self.BusinessImageUrl = title
        }
        if let title = dictionary["HaveOffer"] as? String   {
            self.HaveOffer = title
        }
    }

}
