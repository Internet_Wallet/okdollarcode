//
//  InstaPayModel.swift
//  OK
//
//  Created by E J ANTONY on 4/23/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation

protocol InstaPayModelDelegate: class {
    func apiResult(data: InstaPayProfileImages?, message: String?, statusCode: String?)
    func apiResultGeneral(message: String?, statusCode: String?)
    func handleCustomError(error: CustomError)
    func apiResult(data: PTAgentModel, message: String?, statusCode: String?)
}

extension InstaPayModelDelegate {
    func apiResult(data: InstaPayProfileImages?, message: String?, statusCode: String?) {}
    func apiResultGeneral(message: String?, statusCode: String?) {}
    func apiResult(data: PTAgentModel, message: String?, statusCode: String?) {}
}

class InstaPayModel {
    //MARK: - Properties
    weak var instaPayModelDelegate: InstaPayModelDelegate?
    var agentModel: PTAgentModel?
    var remarks = ""
    var transactionArray = [Dictionary<String,Any>]()
    private enum InstaPayApiType: String {
        case getImages = "Get Images"
    }
    
    //MARK: - Methods
    func getImage(request: GetImagesByPhNumberReq) {
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            let urlStrSuffix   = Url.imagesByPhNumbers
            let instaPayRxImagesUrl = getUrl(urlStr: urlStrSuffix, serverType: .instaPay)
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(instaPayRxImagesUrl)")
            let encoder = JSONEncoder()
            do {
                let jsonData = try encoder.encode(request)
                web.genericClass(url: instaPayRxImagesUrl, param: params , httpMethod: "POST", mScreen: InstaPayApiType.getImages.rawValue, hbData: jsonData)
            } catch {
                println_debug("Exception in do in instapay")
            }
        } else {
            println_debug(CustomError.noInternet)
        }
    }
    
    func handleGetImages(data: AnyObject) {
        guard let castedData = data as? Data else {
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(GetImagesByPhNumberRes.self, from: castedData)
            guard let comments = response.data else { return }
            println_debug("\n\nRESPONSE: \(response)")
            guard let commentData = comments.data(using: .utf8) else { return }
            let instaPayImages = try decoder.decode([InstaPayProfileImages].self, from: commentData)
            if instaPayImages.count > 0 {
                instaPayModelDelegate?.apiResult(data: instaPayImages[0], message: "", statusCode: "")
            }
        } catch (let error) {
            println_debug(error)
        }
    }
    
    func checkAgentCall(url: URL) {
        TopupWeb.genericClass(url: url, param: Dictionary<String,Any>() as AnyObject, httpMethod: "GET") { (response, success) in
            if success {
                DispatchQueue.main.async {
                    guard let dictionary = response as? Dictionary<String,Any> else { return }
                    if let code = dictionary.safeValueForKey("Code") as? Int, code != 200  {
                    PaytoConstants.alert.showErrorAlert(title: nil, body: dictionary.safeValueForKey("Msg") as? String ?? "")
//                        PaytoConstants.alert.showErrorAlert(title: nil, body: "Source number & destination number should not be same".localized as? String ?? "")
                        
                        return
                    }
                    guard let dataString = dictionary.safeValueForKey("Data") as? String else { return }
                    guard dataString.count > 0 else { return }
                    guard let dictionaryJson = OKBaseController.convertToDictionary(text: dataString) else { return }
                    self.agentModel = PTAgentModel(withDictionary: dictionaryJson)
                    if let safeAgentModel = self.agentModel {
                        println_debug(safeAgentModel)
                        self.instaPayModelDelegate?.apiResult(data: safeAgentModel, message: "", statusCode: "")
                    }
                }
            } else {
                PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.pleaseTryAgain.localized)
            }
        }
    }
    
    func cashbackApi(_ url: URL, params: Dictionary<String,Any>) {
        TopupWeb.genericClass(url: url, param: params as AnyObject, httpMethod: "POST") { (response, success) in
            DispatchQueue.main.sync {
                if success {
                    guard let arrayResponse = response as? [Any]  else { return }
                    for (index,subItem) in arrayResponse.enumerated() {
                        guard let dictionary = subItem as? Dictionary<String,Any> else { return }
                        if dictionary.safeValueForKey("Code") as? Int == 200 {
                            guard let xmlString = dictionary.safeValueForKey("Data") as? String else { return }
                            self.parsingCashbackData(xml: xmlString, withIndex: index)
                            
                        }
                    }
                }
            }
        }
    }
    
    private func parsingCashbackData(xml: String, withIndex index: Int) {
        let xmlIndexer = SWXMLHash.parse(xml)
        DispatchQueue.main.async {
            self.enumerate(indexer: xmlIndexer) { (dictionary) in
                var transaction = dictionary
                transaction["businessName"] = self.agentModel?.businessName
                var destName = ""
                if let name = self.agentModel?.merchantCatName, name.count > 0 {
                    destName = self.agentModel?.mercantName ?? ""
                } else {
                    destName = "Unregistered User"
                }
                transaction["destinationName"] = destName
                if destName.lowercased() == "Unregistered User".lowercased() {
                    transaction["businessName"] = ""
                }
                transaction["localRemark"] = self.remarks
                //                transaction["commission"] = ""
                //                transaction["totalAmount"] = ""
                //                transaction["CICOPayTo"] = PayToType(type: .payTo)
                if (transaction["resultdescription"] as? String)?.lowercased() == "Transaction Successful".lowercased() {
                    self.transactionArray.append(transaction)
                } else {
                    let msgs = transaction.safeValueForKey("resultdescription") as? String
                    PaytoConstants.alert.showErrorAlert(title: nil, body: msgs.safelyWrappingString().localized)
                }
            }
            self.instaPayModelDelegate?.apiResultGeneral(message: "Parsing Done", statusCode: "")
        }
    }
    
    private func enumerate(indexer: XMLIndexer, handle :@escaping (_ dict: Dictionary<String,Any>) -> Void) {
        var dictionary : Dictionary<String,Any> = Dictionary<String,Any>()
        func indexing(indexer: XMLIndexer) {
            for child in indexer.children {
                guard let element = child.element else { return }
                dictionary[element.name] = element.text
                indexing(indexer: child)
            }
        }
        indexing(indexer: indexer)
        handle(dictionary)
    }
}

//MARK: - WebServiceResponseDelegate
extension InstaPayModel: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        guard let screen = InstaPayApiType(rawValue: screen) else { return }
        switch screen {
        case .getImages:
            handleGetImages(data: json)
        }
    }
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        switch withErrorCode {
        case 404:
            guard let screen = InstaPayApiType(rawValue: screenName) else { return }
            switch screen {
            case .getImages:
                println_debug("Result with 404 in instapay module")
            }
        default:
            println_debug("Error \(withErrorCode)")
        }
    }
}
