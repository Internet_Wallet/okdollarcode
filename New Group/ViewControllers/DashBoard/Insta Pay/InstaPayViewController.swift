//
//  InstaPayViewController.swift
//  OK
//
//  Created by E J ANTONY on 4/6/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import SDWebImage
import CoreLocation

class InstaPayViewController: UIViewController {
    
    //prabu
    @IBOutlet weak var instantPayFooterLabelBottomConstrain: NSLayoutConstraint!
    @IBOutlet weak var instantPayFooterLabelHeightConstrain: NSLayoutConstraint!
    
    //MARK: - Outlets
    @IBOutlet weak var viewPath: UIView!
    @IBOutlet weak var buzzer: UIImageView!
    @IBOutlet weak var buzzerCenterY: NSLayoutConstraint!
    @IBOutlet weak var constraintHPath: NSLayoutConstraint!
    @IBOutlet weak var lblCancel: UILabel!{
        didSet{
            self.lblCancel.font = UIFont(name: appFont, size: appFontSize)
            lblCancel.text = "Cancel".localized
        }
    }
    @IBOutlet weak var lblPay: UILabel!{
        didSet{
            self.lblPay.font = UIFont(name: appFont, size: appFontSize)
            lblPay.text = "Pay".localized
        }
    }
    @IBOutlet weak var lblReceive: UILabel!{
        didSet{
            self.lblReceive.font = UIFont(name: appFont, size: appFontSize)
            lblReceive.text = "Receive".localized
        }
    }
    @IBOutlet weak var recieveButton: UIButton!
    
    @IBOutlet weak var payButton: UIButton!
    @IBOutlet weak var instantPayFooterLabel: UILabel!{
        didSet{
            self.instantPayFooterLabel.font = UIFont(name: appFont, size: appFontSize)
            instantPayFooterLabel.text = "Instant Pay".localized
        }
    }
    
    
    @IBOutlet weak var viewScreen: UIView!
    @IBOutlet weak var viewCancel: UIView!
    @IBOutlet weak var imgViewProfile: UIImageView!
    @IBOutlet weak var lblProfile: UILabel!{
        didSet {
            lblProfile.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var viewInstruction: UIView!
    @IBOutlet weak var constraintBInstruction: NSLayoutConstraint!
    @IBOutlet weak var lblInstructionHeader: UILabel!{
        didSet {
            lblInstructionHeader.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgViewInstrArrow: UIImageView!
    @IBOutlet weak var lblInstruction: UILabel!{
        didSet {
            lblInstruction.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var constraintHCollectionView: NSLayoutConstraint!
    @IBOutlet weak var collectionViewReceiver: UICollectionView!
    @IBOutlet weak var tableRecepient: UITableView!
    @IBOutlet weak var constraintHViewTable: NSLayoutConstraint!
    @IBOutlet weak var viewHoldingTable: UIView!
    @IBOutlet weak var lblRecipientHeader: UILabel!{
        didSet {
            lblRecipientHeader.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    @IBOutlet weak var switchesView: UIView!
    
    @IBOutlet weak var dotButton: UIButton!
    
    @IBOutlet weak var contactsView: UIView!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var taptoPayLabel: UILabel! {
        didSet {
            self.taptoPayLabel.font = UIFont(name: appFont, size: appFontSize)
            taptoPayLabel.text = "Tap to Pay".localized
        }
    }
    var timer = Timer()
    var  yValue: CGFloat = 0.0
   
    
    let currentRoute = AVAudioSession.sharedInstance().currentRoute
    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var mobilenumSwitch: UISwitch!
    
    @IBOutlet weak var profileSwitch: UISwitch!
    
    @IBOutlet weak var contactsTableView: UITableView!
    
    @IBOutlet weak var contactsHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var tabforPayLabel: UILabel!   {
        didSet {
            self.tabforPayLabel.font = UIFont(name: appFont, size: appFontSize)
            tabforPayLabel.text = "Tap for pay or receive mode".localized
        }
    }
    @IBOutlet weak var mobileNumOutlet: UILabel!{
        didSet{
            self.mobileNumOutlet.font = UIFont(name: appFont, size: appFontSize)
            mobileNumOutlet.text = "Mobile Number Insta".localized
        }
    }
    @IBOutlet weak var profilePicOutlet: UILabel!{
        didSet{
            self.profilePicOutlet.font = UIFont(name: appFont, size: appFontSize)
            profilePicOutlet.text = "Profile Picture Insta".localized
        }
    }
    //MARK: - Properties
    var dummyArray =  [NearByServicesNewModel]()
    var arrAgentsList = [NearByServicesNewModel]()
    var profilePicArray = [ProfilePicModel]()
    
    var phonenumberArray = [String]()
    var dummyphonenumberArray = [String]()
    var dummyprofilepicArray = [ProfilePicModel]()
    
    var picStatus = String()
     var MobStatus = String()
    
    var transitcall : Bool = false
    var recieverStatus : Bool = false
    var comparestr = String()
    
    private let heightOfShape: CGFloat = 320
    private let heightOfBuzzer: CGFloat = 106
    private let heightOfInstructionTitle: CGFloat = 40
    private let heightOfInstruction: CGFloat = 140
    private var pan = UIPanGestureRecognizer()
    private var longPress = UILongPressGestureRecognizer()
    private var tap = UITapGestureRecognizer()
    private var swipe = UISwipeGestureRecognizer()
    private var isDetailInstrShow = false
    private enum Shape {
        case expand, collapse
    }
    private enum SwitchState {
        case receive, pay, neutral, towardsPay, towardsReceive, towardsNeutral
    }
    private var currentShape = Shape.collapse
    private var switchState = SwitchState.neutral
    private lazy var buzzerThreshold: CGFloat = {
        return (self.heightOfShape/2) - (self.buzzer.frame.size.height/2)
    }()
    private var rippleView: RippleView?
    private let stringOnPayMode = "Make sure your recipient is in receive mode. You'll also see nearby businesses if they're connected.".localized
    private let stringOnReceiveMode = "Make sure that your payer is in pay mode.".localized
    private let stringOnNormalMode = "Quickly transfer the qr code with the people near you".localized
    private let stringHeaderNormalMode = "InstaPay Mode".localized
    private let stringHeaderActionMode = "Finding people nearby".localized
    var modelFav = [FavoriteContacts]()
    var modelInstaPay: InstaPayModel!
    var modelNBS: NearByServicesNewModel!
    var tx: QMFrameTransmitter = {
        let txConf: QMTransmitterConfig = QMTransmitterConfig(key:"ultrasonic") //ultrasonic-experimental
        let tx: QMFrameTransmitter = QMFrameTransmitter(config: txConf)
        return tx
    }()
    var rx: QMFrameReceiver?
    
    var recipientList = [Recipients]()
    
    struct Recipients {
        var name: String
        var number: String
        var imageUrlStr: String?
    }
    //Manage offers data
    var arrOffersRecord : Array<String> = []
    
    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        updateLocation()
        getOffersData()
        updateUI()
        addGestures()
        configureModel()
        addPaymentObserver()
      
       switchesView.isHidden = true
       contactsView.isHidden = true
        
         tabforPayLabel.isHidden = true
        
          self.change(shape: .expand)
        
        let upSwipe = UISwipeGestureRecognizer(target: self, action: #selector(moveToNextItem(_:)))
        let downSwipe = UISwipeGestureRecognizer(target: self, action: #selector(moveToNextItem(_:)))
        
        let uplbl = UITapGestureRecognizer(target: self, action: #selector(moveTopay(_:)))
        let downlbl = UITapGestureRecognizer(target: self, action: #selector(moveToNext(_:)))
        
        lblPay.isUserInteractionEnabled = true
        lblReceive.isUserInteractionEnabled = true
        
         lblPay.addGestureRecognizer(uplbl)
         lblReceive.addGestureRecognizer(downlbl)
        
        
        
        upSwipe.direction = .up
        downSwipe.direction = .down
        
        contactsTableView.addGestureRecognizer(upSwipe)
        contactsTableView.addGestureRecognizer(downSwipe)
        
        contactsView.addGestureRecognizer(upSwipe)
        contactsView.addGestureRecognizer(downSwipe)
     //Timer Handle
  //      timer = Timer.scheduledTimer(timeInterval: 120.0, target: self, selector:#selector(self.timerManage) , userInfo: nil, repeats: false)
        
        
        AVAudioSession.sharedInstance().requestRecordPermission({(granted: Bool)-> Void in
            if granted {
               // Granted
            } else {
                println_debug("Permission to record not granted")
                let alert: UIAlertController
                
                alert = UIAlertController(title: "ALERT!".localized, message: "PLEASE GIVE PERMISSION TO ACCESS MICROPHONE.".localized, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Setting".localized, style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        
                        UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
                        
                    }
                }))
                
                //   alert.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
                //  self.contactsView.isHidden = true
            }
        })

    }
    
    @objc func moveToNextItem(_ sender:UISwipeGestureRecognizer) {
        
        switch sender.direction{
        case .up:
        //left swipe action
             print("up Swipe")
               comparestr = "1"
           //  contactsTableView.isScrollEnabled = false
          
//             UIView.animate(withDuration: 0.3, animations: {
//
//            self.contactsView.frame = CGRect(x: 0, y: 65, width: self.view.frame.size.width, height: self.view.frame.size.height-65)
//
//            //    let guide = self.view.safeAreaLayoutGuide
//
//            //    self.contactsView.topAnchor.constraint(equalToSystemSpacingBelow: guide.topAnchor, multiplier: 1.0)
//
//               self.contactsHeightConstraint.constant =  self.contactsView.frame.height
//
//         //    self.contactsTableView.frame = CGRect(x: 0, y: 60, width: self.contactsView.frame.size.width, height: self.contactsView.frame.size.height)
//
//                self.contactsView.layoutIfNeeded()
//             }, completion: { (bool) in })
            
            
             UIView.transition(with: contactsView, duration: 0.8, options: .transitionFlipFromLeft, animations: {
                self.contactsView.frame = CGRect(x: 0, y: 65, width: self.view.frame.size.width, height: self.view.frame.size.height-65)
                
                //    let guide = self.view.safeAreaLayoutGuide
                
                //    self.contactsView.topAnchor.constraint(equalToSystemSpacingBelow: guide.topAnchor, multiplier: 1.0)
                
                self.contactsHeightConstraint.constant =  self.contactsView.frame.height
                
             })
            
        case .down:
        //right swipe action
             print("down Swipe")
            comparestr = "0"
            //  contactsTableView.isScrollEnabled = false
//             UIView.animate(withDuration: 0.3, animations: {
//            //    self.contactsHeightConstraint.constant = 272
//
//              self.contactsView.frame = CGRect(x: 0, y: self.view.frame.size.height/2, width: self.view.frame.size.width, height: self.view.frame.size.height-65)
//
//                  self.contactsHeightConstraint.constant =  screenHeight/2-100
//
//              //  self.contactsHeightConstraint.constant =  self.contactsView.frame.height
//
//
//                self.contactsView.layoutIfNeeded()
//             }, completion: { (bool) in })
            
             UIView.transition(with: contactsView, duration: 0.8, options: .transitionFlipFromRight, animations: {
                self.contactsView.frame = CGRect(x: 0, y: self.view.frame.size.height/2, width: self.view.frame.size.width, height: self.view.frame.size.height-65)
                
                self.contactsHeightConstraint.constant =  screenHeight/2-100
                
             })
            
        default://default
            break
        }
        
    }
    
    @objc func moveToNext(_ sender:UITapGestureRecognizer) {
          self.activateHeadPhonesStatus()
         self.switchState = .towardsReceive
      
        self.panchanges(with: -100)
        
        self.animate(isHideText: false, shape: .expand)
        
        self.ispayReceive(active: true)
     
     
}
    @objc func moveTopay(_ sender:UITapGestureRecognizer) {
        
        self.activateHeadPhonesStatus()
        self.switchState = .towardsPay
        
        self.panchanges(with: 100)
        
        self.animate(isHideText: false, shape: .expand)
      
        
        self.ispayReceive(active: true)
    
    }
    func activateHeadPhonesStatus(){
    //   NotificationCenter.default.addObserver(self, selector: #selector(audioRouteChangeListener(_:)), name: AVAudioSession.routeChangeNotification, object: nil)
        if  self.isHeadphonesConnected() == true{
              print("Headphones Connected")
            //self.showErrorAlert(errMessage: "Remove Headphones".localized)
            alertViewObj.wrapAlert(title: nil, body: "Please Remove Headphones".localized, img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                
                self.removeRippleView() {}
                //  self.viewHoldingTable.isHidden = true
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    //  self.change(shape: .collapse)

                    //    self.contactsTableView.reloadData()
                    self.ispayReceive(active: false)
                    self.buzzerCenterY.constant = 0
                    self.constraintHPath.constant = self.viewPath.frame.size.width
                    self.constraintHPath.constant = self.heightOfShape
                    self.currentShape = .expand
                    self.switchState = .neutral

                    //    self.change(shape: .expand)
                    self.bottomView.isHidden = false
                    self.viewInstruction.isHidden = true
                    self.tabforPayLabel.isHidden = true
                    self.imgViewProfile.isHidden = false
                    self.lblProfile.isHidden = false
                    self.backButton.isHidden = false
                    self.dotButton.isHidden = false
                    self.showInstruction(isVisible: false)

                })
                self.contactsView.isHidden = true
                if self.comparestr == "1"
                {
                    self.comparestr = "0"
                    self.contactsView.frame = CGRect(x: 0, y: self.view.frame.size.height/2, width: self.view.frame.size.width, height: self.view.frame.size.height+65)

                    self.contactsHeightConstraint.constant =  screenHeight/2-100

                    self.contactsView.layoutIfNeeded()
                }
                else
                {
                    self.comparestr = "0"
                    //
                }
 
            })
            alertViewObj.showAlert()
        }
        else{
              print("Headphones NotConnected")
        }
        
    }
    
    func bluetoothAudioConnected() -> Bool{
        let outputs = AVAudioSession.sharedInstance().currentRoute.outputs
        for output in outputs{
            if output.portType == AVAudioSession.Port.bluetoothA2DP || output.portType == AVAudioSession.Port.bluetoothHFP || output.portType == AVAudioSession.Port.bluetoothLE{
                
                self.showErrorAlert(errMessage: "Disconnect Bluetooth connectivity for external device inputs".localized)
                return true
            }
        }
        return false
    }
    
    //MARK: isHeadphonesConnected
    func isHeadphonesConnected() -> Bool{
        let routes = AVAudioSession.sharedInstance().currentRoute
        return routes.outputs.contains(where: { (port) -> Bool in
            port.portType == AVAudioSession.Port.headphones
        })
    }

    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.activateHeadPhonesStatus()
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        appDel.floatingButtonControl?.viewDidLayoutSubviews()
        appDel.floatingButtonControl?.window.isHidden = true
           self.bottomView.isHidden = false
           self.viewInstruction.isHidden = true
        imgViewProfile.isHidden = false
        lblProfile.isHidden = false
        backButton.isHidden = false
        dotButton.isHidden = false
        contactsView.isHidden = true
        self.showInstruction(isVisible: false)
        
        comparestr = "0"
        
        self.contactsView.frame = CGRect(x: 0, y: self.view.frame.size.height/2, width: self.view.frame.size.width, height: self.view.frame.size.height+65)
        
        self.contactsHeightConstraint.constant =  screenHeight/2-100
        
        self.contactsView.layoutIfNeeded()
        
        NotificationCenter.default.addObserver(self, selector: #selector(willResignActive), name: UIApplication.willResignActiveNotification, object: nil)
        
          if userDef.object(forKey: "mobileOn") != nil {
            let boolMobileStatus = userDef .value(forKey: "mobileOn") as! Bool
            
            
            if boolMobileStatus == true {
                mobilenumSwitch.setOn(true, animated: false)
                MobStatus = "s"
            }    else    {
                mobilenumSwitch.setOn(false, animated: false)
                 MobStatus = "n"
            }
        }
        else
          {
             MobStatus = "n"
        }
        
       if userDef.object(forKey: "profileOn") != nil {
        let boolProfileStatus = userDef .value(forKey: "profileOn") as! Bool
        if  boolProfileStatus  == true {
            profileSwitch.setOn(true, animated: false)
            picStatus = "s"
        } else  {
            profileSwitch.setOn(false, animated: false)
            picStatus = "n"
        }
    }
    else
       {
              picStatus = "n"
        }
        
        //iphone 11 , XR 828 Check
        switch device.type {
        case    .iPhone11Pro, .iPhone11ProMax, .iPhoneXS , .iPhoneXSMax ,.iPhoneX , .iPhone6plus , .iPhone6Splus , .iPhone7plus , .iPhone8plus:
            //prabu
            self.instantPayFooterLabelHeightConstrain.constant = 32
            self.instantPayFooterLabel.layoutIfNeeded()
        default:
            //prabu
            self.instantPayFooterLabelHeightConstrain.constant = 35
            self.instantPayFooterLabel.layoutIfNeeded()
            
        }
      
        
        
    }
    @objc func willResignActive(_ notification: Notification) {
        // code to execute
        
        removeRippleView() {}
        //  self.viewHoldingTable.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            //  self.change(shape: .collapse)
            self.ispayReceive(active: false)
            self.buzzerCenterY.constant = 0
            self.constraintHPath.constant = self.viewPath.frame.size.width
            self.constraintHPath.constant = self.heightOfShape
            self.currentShape = .expand
            self.switchState = .neutral
            self.contactsView.isHidden = true
            self.bottomView.isHidden = false
               self.viewInstruction.isHidden = true
            self.imgViewProfile.isHidden = false
            self.lblProfile.isHidden = false
           self.backButton.isHidden = false
            self.dotButton.isHidden = false
            self.showInstruction(isVisible: false)
            
        })
     
      
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        removeRippleView() {}
        //  self.viewHoldingTable.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            //  self.change(shape: .collapse)
            self.ispayReceive(active: false)
            self.buzzerCenterY.constant = 0
            self.constraintHPath.constant = self.viewPath.frame.size.width
            self.constraintHPath.constant = self.heightOfShape
            self.currentShape = .expand
            self.switchState = .neutral
            self.contactsView.isHidden = true
              self.showInstruction(isVisible: false)
            
       })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
       
        
        if touch?.view == self.switchesView {
            switchesView.isHidden = false
        }
        else
        {
             switchesView.isHidden = true
        }
    }
    
    //MARK: - Methods
    func getOffersData() {
        println_debug("getOffersData")

        OffersManager.getAllOffers({ [weak self](objects) in
            if objects == nil { return }
            if objects!.count <= 0 { return }
            DispatchQueue.main.async {
            
                //Save merchant numbers which have offers
                if let arrayObjects = objects {
                    for element in arrayObjects {
                        let detail = element.mobilenumber ?? ""
                        println_debug(detail)
                        
                        let mobileNumbers = element.mobilenumber.safelyWrappingString()
                        
                        let mobileNumberArray = mobileNumbers.components(separatedBy: ",")
                        
                            for number in mobileNumberArray {
                                
                                if self?.arrOffersRecord.contains(number) ?? true {
                                    println_debug("Already exist")
                                } else {
                                self?.arrOffersRecord.append(number)
                                }
                            }//end of mobileNumberArray
                    }
                     DispatchQueue.main.async {
                       // self?.collectionViewReceiver.reloadData()
                        
                        self?.contactsTableView.reloadData()
                    }
                }
                
            }
        })//End of getAllOffers
    }
    
    private func addPaymentObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(paymentDoneClose), name: NSNotification.Name(rawValue: "PaymentSuccess"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(morePay), name: NSNotification.Name(rawValue: "InstaPayMore"), object: nil)
    }
    
    private func configureModel() {
        modelInstaPay = InstaPayModel()
        modelInstaPay.instaPayModelDelegate  = self
        //let dictData =
        //modelNBS = NearByServicesNewModel(dictionary: nil)
    }
    
    private func configureFavList() {
        //self.modelFav = favoriteManager.fetchRecordsForFavoriteContactsEntity()
        switch self.arrAgentsList.count {
        case 0:
            constraintHCollectionView.constant = 30
        case let x where x > 0 && x < 5:
            constraintHCollectionView.constant = 120
      //  case let x where x > 4:
      //      constraintHCollectionView.constant = 205
        default:
            break
        }
        self.view.layoutIfNeeded()
      //  self.collectionViewReceiver.reloadData()
        self.contactsTableView.reloadData()
    }
    
    private func updateLocation() {
        guard let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude else {
            // throw the error to the user
            return
        }
        getNearByAgents(lat: lat, long: long)
    }
    
    //MARK: GetNearByAgents
    private func getNearByAgents(lat: String, long: String) {
        
        let url = getUrl(urlStr: "", serverType: .nearByServicesUrl)
        
        let jsonDic:[String : Any]
       // jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 100, "Latitude" : lat, "Longitude": long, "PhoneNumber" : UserModel.shared.mobileNo, "UserIdNotToConsider": ""] as [String : Any]
        
       // jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 100, "Latitude" : lat, "Longitude": long, "PhoneNumber" : UserModel.shared.mobileNo, "UserIdNotToConsider": ""] as [String : Any]
        
         jsonDic = ["latitude" : lat, "longitude": long, "phoneNumber" : UserModel.shared.mobileNo, "distanceLimit" : 100, "cashType" : 0, "amount" : 0, "searchAdditionalParams":["townShipName":"","cityName":"","businessName":"","country":"","divisionName":"","line1":""],"paginationParams": ["offset":0,"limit":20],"types":[], "phoneNumberNotToConsider":""] as [String : Any]
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url,methodType: kMethod_Post, contentType: kContentType_Json, inputVal: jsonDic, authStr: nil)
        
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    //println_debug("remove progress view called inside cashin offices api main call with error")
                }
                return
            }
            println_debug("response dict for get all offices list :: \(response ?? "")")
            progressViewObj.removeProgressView()
            
            var cashArray = [NearByServicesNewModel]()
            DispatchQueue.main.async {
                cashArray = ParserAttributeClass.parseNearbyServiceJSONNew(anyValue: response as AnyObject) as [NearByServicesNewModel]
                progressViewObj.removeProgressView()
         
                if cashArray.count > 0 {
                   self.arrAgentsList.removeAll()
                    //Seperate mechant =2,agent=1,office=7
                    for indexVal in 0..<cashArray.count {
                       let currentModel = cashArray[indexVal]
                        
                        if currentModel.UserContactData?.PhoneNumber == UserModel.shared.mobileNo{
                            print("same number")
                        }
                        else
                        {
                            self.arrAgentsList.append(currentModel)
                            
                            self.phonenumberArray.append(currentModel.UserContactData!.PhoneNumber!)
                            
                        }
                        
//                        self.arrAgentsList.sort {
//                            return Unicode.CanonicalCombiningClass(rawValue: UInt8($0.distanceInKm!)) < Unicode.CanonicalCombiningClass(rawValue: UInt8($1.distanceInKm!))
//                        }
                        
                       /* if let list = self.arrAgentsList as? [NearByServicesNewModel] {
                            self.arrAgentsList = list.sorted(by: {Unicode.CanonicalCombiningClass(rawValue: UInt8($0.distanceInKm!)) < Unicode.CanonicalCombiningClass(rawValue: UInt8($1.distanceInKm!))})
                            } */
                        
                       //prabu 
                        //self.arrAgentsList.sort(by: {(Unicode.CanonicalCombiningClass(rawValue: UInt8($0.distanceInKm!)) ) < (Unicode.CanonicalCombiningClass(rawValue: UInt8($1.distanceInKm!)))})
                    }
                   self.profilepicServicecall()
                   //self.contactsTableView.reloadData()
                  //  self.configureFavList()
                } else {
                    self.showErrorAlert(errMessage: "No Record Found".localized)
                }
            }
        })
    }
    
  //MARK: ProfilepicServicecall
private  func profilepicServicecall() {
    //
    var  url = URL(string: "")
    if serverUrl == .productionUrl {
         url = URL(string: "http://advertisement.api.okdollar.org/AdService.svc/GetProfilePicOrBusinessPicBymobileNumber")
    }  else {
         url = URL(string: "http://69.160.4.151:8002//AdService.svc/GetProfilePicOrBusinessPicBymobileNumber")
    }
    
        let jsonDic:[String : Any]
    
    if dummyArray.count > 0
    {
          jsonDic = ["LoginValidationRequest":["AppId":buildNumber,"Limit":"0","Offset":"0","Ostype":"1"],"MobilenumberList": dummyphonenumberArray ]as [String : Any]
    }
    else
    {
         jsonDic = ["LoginValidationRequest":["AppId":buildNumber,"Limit":"0","Offset":"0","Ostype":"1"],"MobilenumberList": phonenumberArray ]as [String : Any]
    }
    
        
    let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url!,methodType: kMethod_Post, contentType: kContentType_Json, inputVal: jsonDic, authStr: nil)
        
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
    JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
        guard isSuccess else {
            DispatchQueue.main.async {
                progressViewObj.removeProgressView()
                //println_debug("remove progress view called inside cashin offices api main call with error")
            }
            return
        }
        println_debug("response dict for get all offices list :: \(response ?? "")")
        progressViewObj.removeProgressView()
        
        var cashArray = [ProfilePicModel]()
        DispatchQueue.main.async {
            cashArray = ParserAttributeClass.parseprofilepicServiceJSONNew(anyValue: response as AnyObject) as [ProfilePicModel]
            progressViewObj.removeProgressView()
            
            if cashArray.count > 0 {
                for indexVal in 0..<cashArray.count {
                    let currentModel = cashArray[indexVal]
                    currentModel.type  = self.arrAgentsList[indexVal].UserContactData?.type
                    if self.transitcall == false
                    {
                  
                    if self.dummyArray.count>0{
                        self.dummyprofilepicArray.append(currentModel)
                    }else{
                       self.profilePicArray.append(currentModel)
                    }
                }
                    
                }
                println_debug(self.profilePicArray.count)
//               self.arrAgentsList.reverse()
//               self.profilePicArray.reverse()
                self.contactsTableView.reloadData()
            } else {
                self.showErrorAlert(errMessage: "No Record Found".localized)
            }
        }
    })

}
    private func updateUI() {
        self.viewPath.layer.cornerRadius = self.viewPath.frame.width / 2
        self.buzzer.layer.cornerRadius = self.buzzer.frame.width / 2
        self.lblPay.isHidden = true
        self.lblReceive.isHidden = true
        self.imgViewProfile.layer.cornerRadius = 25
        self.lblProfile.text = UserModel.shared.name
      //  self.viewHoldingTable.isHidden = true
      //  self.lblRecipientHeader.text = "Tap to Pay".localized
//        guard let urlPercent = UserModel.shared.proPic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
//        let profilePicUrl = URL(string: urlPercent) else { return }
//        self.imgViewProfile.downloadedFrom(url: profilePicUrl)
//        self.imgViewProfile.contentMode = .scaleAspectFill
        
        let url = URL(string: UserModel.shared.proPic.replacingOccurrences(of: " ", with: "%20"))
        self.imgViewProfile.sd_setImage(with: url, placeholderImage: UIImage(named: "avatar"))
    }
    
    private func addGestures() {
        addLongPressGestures()
        addPanGesture()
        addTapGesture()
      //  addSwipeGesture()
    }
    
    private func loadRippleView() {
        if let _ = rippleView {
            rippleView?.removeFromSuperview()
            rippleView = nil
        }
        let fillColor: UIColor? = UIColor.white
       rippleView = RippleView(frame: buzzer.frame, rippleColor: UIColor.white, rippleThickness: 1, rippleTimer: 0.45, fillColor: fillColor, animationDuration: 1.5, parentFrame: CGRect(x: self.view.bounds.maxX, y: self.view.bounds.maxX, width: self.view.bounds.size.width, height: self.view.bounds.size.width))
        
        self.viewPath.insertSubview(rippleView!, belowSubview: buzzer)
        self.viewPath.setNeedsLayout()
        self.viewPath.layoutIfNeeded()
    }
    
    private func removeRippleView(completionHandler: () -> Void) {
        if let _ = rippleView {
            rippleView?.removeFromSuperview()
            rippleView = nil
        }
        completionHandler()
    }
    
    private func change(shape: Shape) {
        var isHideText = true
         switchesView.isHidden = true
        switch shape {
        case .collapse:
            isHideText = true
            self.buzzerCenterY.constant = 0
            self.constraintHPath.constant = self.viewPath.frame.size.width
             self.currentShape = .collapse
            self.switchState = .neutral
            self.ispayReceive(active: false)
            self.showInstruction(isVisible: false)
            self.recipientList = []
        //    self.rx?.close()
        case .expand:
            switch switchState {
            case .towardsReceive, .receive:
                 self.activateHeadPhonesStatus()
                buzzerCenterY.constant = -buzzerThreshold
                self.ispayReceive(active: true)
            case .towardsPay, .pay:
                 self.activateHeadPhonesStatus()
                bottomView.isHidden = false
                viewInstruction.isHidden = true
                imgViewProfile.isHidden = true
                lblProfile.isHidden = true
                backButton.isHidden = true
                dotButton.isHidden = true
            //     self.showInstruction(isVisible: true)
                
                if (UserLogin.shared.walletBal as NSString).floatValue < 1 {
                    alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                    alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                        if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                            addWithdrawView.modalPresentationStyle = .fullScreen
                            self.present(addWithdrawView, animated: true, completion: nil)
                        }
                    })
                    alertViewObj.showAlert()
                    return
                }else{
                 //    timer.fire()
                    buzzerCenterY.constant = buzzerThreshold
                    self.ispayReceive(active: true)
                }
          
            default:
                break
            }
            constraintHPath.constant = heightOfShape
            currentShape = .expand
            isHideText = false
         //   self.showInstruction(isVisible: true)
        }
        self.animate(isHideText: isHideText, shape: shape)
    }
    
    private func animate(isHideText: Bool, shape: Shape) {
        UIView.animate(withDuration: 0.3, animations: {
            self.lblPay.isHidden = isHideText
            self.lblReceive.isHidden = isHideText
            self.viewPath.layoutIfNeeded()
        }, completion: { (bool) in
            if shape == .expand {
                switch self.switchState {
                case .towardsReceive, .receive:
          //           self.activateHeadPhonesStatus()
                    self.loadRippleView()
                    self.transmit()
                case .towardsPay, .pay:
              //       self.activateHeadPhonesStatus()
                    self.bottomView.isHidden = false
                       self.viewInstruction.isHidden = true
                     self.imgViewProfile.isHidden = true
                     self.lblProfile.isHidden = true
                     self.backButton.isHidden = true
                    self.dotButton.isHidden = true
                   //   self.showInstruction(isVisible: true)
                    
                    if (UserLogin.shared.walletBal as NSString).floatValue < 1 {
                        alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                        alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                            if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                                addWithdrawView.modalPresentationStyle = .fullScreen
                                self.present(addWithdrawView, animated: true, completion: nil)
                            }
                        })
                        alertViewObj.showAlert()
                        return
                    }else{
                        self.loadRippleView()
                        self.receive()
                    }
                    
                  
                default:
                    break
                }
            }
        })
    }
    
    private func panchanges(with yValue: CGFloat) {
        var yValue = yValue
        switch yValue {
        case let y where y > 0:
            if yValue > 50 {
                if yValue >= buzzerThreshold {
                    yValue = buzzerThreshold
                }
                switchState = .towardsPay
     //            self.activateHeadPhonesStatus()
                   self.bottomView.isHidden = false
                   self.viewInstruction.isHidden = true
                imgViewProfile.isHidden = true
                lblProfile.isHidden = true
                backButton.isHidden = true
                dotButton.isHidden = true
              //   self.showInstruction(isVisible: true)
                if (UserLogin.shared.walletBal as NSString).floatValue < 1 {
                    alertViewObj.wrapAlert(title: nil, body: "Your account have insufficient balance. Please recharge OK$ wallet money at nearest OK$ Service Counter".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                    alertViewObj.addAction(title: "ADD MONEY".localized, style: .target, action: {
                        if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                            addWithdrawView.modalPresentationStyle = .fullScreen
                            self.present(addWithdrawView, animated: true, completion: nil)
                        }
                    })
                    alertViewObj.showAlert()
                    return
                }else{
                    self.viewInstruction.isHidden = true
                    contactsView.isHidden = true
                 //   UIView.animate(withDuration: 0.3, animations: {
                        //    self.contactsHeightConstraint.constant = 272

//                        self.contactsView.frame = CGRect(x: 0, y: self.view.frame.size.height/2, width: self.view.frame.size.width, height: self.view.frame.size.height-65)
//
//                         self.contactsHeightConstraint.constant = screenHeight/2-100
//
//                  //  self.contactsView.translatesAutoresizingMaskIntoConstraints = true
//
//                        self.contactsView.layoutIfNeeded()
             //       }, completion: { (bool) in })
               
                    if profilePicArray.count>0
                    {
                       
                        contactsTableView.isHidden = false
                        self.viewInstruction.isHidden = true
                        contactsView.isHidden = false
                      //  timer.fire()
                            //  timer = Timer.scheduledTimer(timeInterval: 120.0, target: self, selector:#selector(self.timerManage) , userInfo: nil, repeats: false)
                    }
                    else
                    {
                          contactsView.isHidden = true
                        contactsTableView.isHidden = true
                    }
                    switchesView.isHidden = true
                    tabforPayLabel.isHidden = true
                }
             
            } else {
                switchState = .towardsNeutral
                  self.contactsView.isHidden = true
                   self.bottomView.isHidden = false
                   self.viewInstruction.isHidden = true
                 switchesView.isHidden = true
                 tabforPayLabel.isHidden = true
                imgViewProfile.isHidden = false
                lblProfile.isHidden = false
                backButton.isHidden = false
                dotButton.isHidden = false
                 self.showInstruction(isVisible: false)
            }
        case let y where y < 0:
            if yValue < -50 {
                if yValue < -buzzerThreshold {
                    yValue = -buzzerThreshold
                }
                switchState = .towardsReceive
           //      self.activateHeadPhonesStatus()
                contactsView.isHidden = true
                self.bottomView.isHidden = true
                self.viewInstruction.isHidden = false
                 switchesView.isHidden = true
                 tabforPayLabel.isHidden = true
                imgViewProfile.isHidden = true
                lblProfile.isHidden = true
                backButton.isHidden = true
                dotButton.isHidden = true
                 self.showInstruction(isVisible: true)
                
            } else {
                switchState = .towardsNeutral
                
                  contactsView.isHidden = true
                 self.bottomView.isHidden = false
                 self.viewInstruction.isHidden = true
                 switchesView.isHidden = true
                 tabforPayLabel.isHidden = true
                imgViewProfile.isHidden = false
                lblProfile.isHidden = false
                backButton.isHidden = false
                dotButton.isHidden = false
                 self.showInstruction(isVisible: false)
            }
        default:
            break
        }
        buzzerCenterY.constant = yValue
        self.buzzer.layoutIfNeeded()
    }
    
    private func ispayReceive(active: Bool) {
        if active {
            viewCancel.isHidden = false
            viewScreen.isHidden = false
        } else {
            viewCancel.isHidden = true
            viewScreen.isHidden = true
        }
    }
    
    private func showInstruction(isVisible: Bool) {
        var headerText = ""
        var instructionText = ""
        switch switchState {
        case .neutral, .towardsNeutral:
            headerText = stringHeaderNormalMode
            instructionText = stringOnNormalMode
        case .pay, .towardsPay:
            headerText = stringHeaderActionMode
            instructionText = stringOnPayMode
        case .receive, .towardsReceive:
            headerText = stringHeaderActionMode
            instructionText = stringOnReceiveMode
        }
        var bottomValue = -heightOfInstruction
        if isVisible {
            bottomValue = -(heightOfInstruction - heightOfInstructionTitle)
        // self.showCollection(isHide: true)
        } else {
            isDetailInstrShow = false
            imgViewInstrArrow.image = UIImage(named: "up_arrow_small")
        }
        UIView.animate(withDuration: 0.3, animations: {
            self.constraintBInstruction.constant = bottomValue
            self.lblInstructionHeader.font = UIFont(name: appFont, size: appFontSize)
            self.lblInstructionHeader.text = headerText
            self.lblInstruction.font = UIFont(name: appFont, size: appFontSize)
            self.lblInstruction.text = instructionText
            self.view.layoutIfNeeded()
        }, completion: { (bool) in
            if !isVisible {
              //  self.showCollection(isHide: false)
            }
        })
    }
    
    //MARK: Transmit
    private func transmit() {

        transitcall = true
        
        self.dummyArray.removeAll()
        self.dummyphonenumberArray.removeAll()
        self.dummyprofilepicArray.removeAll()
        contactsTableView.reloadData()
        self.contactsView.layoutIfNeeded()
        
        let frame_str = UserModel.shared.mobileNo + "#" + UserModel.shared.name + "#" + picStatus + "#" + MobStatus  // mobilenumber # name
        let data = frame_str.data(using: .utf8)
        self.tx.send(data)
 
    }


    //MARK: Receive
    private func receive() {

        AVAudioSession.sharedInstance().requestRecordPermission({(granted: Bool)-> Void in
            if granted {
              //  self.contactsView.isHidden = false
                if self.rx == nil {
                    let rxConf: QMReceiverConfig = QMReceiverConfig (key:"ultrasonic")   //ultrasonic-experimental
                    self.rx = QMFrameReceiver(config: rxConf)
                }
                self.rx?.setReceiveCallback(self.receiveCallback)
            } else {
                println_debug("Permission to record not granted")
                let alert: UIAlertController
                
                alert = UIAlertController(title: "ALERT!".localized, message: "PLEASE GIVE PERMISSION TO ACCESS MICROPHONE.".localized, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Setting".localized, style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        
                        UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
                
                    }
                }))
                
             //   alert.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
              //  self.contactsView.isHidden = true
            }
        })
      
    }
    
     //MARK: timerManage
 @objc   func timerManage() {
    removeRippleView() {}
    //  self.viewHoldingTable.isHidden = true
    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
        //  self.change(shape: .collapse)
        self.ispayReceive(active: false)
        self.buzzerCenterY.constant = 0
        self.constraintHPath.constant = self.viewPath.frame.size.width
        self.constraintHPath.constant = self.heightOfShape
        self.currentShape = .expand
        self.switchState = .neutral
        self.bottomView.isHidden = false
        self.viewInstruction.isHidden = true
        self.imgViewProfile.isHidden = false
        self.lblProfile.isHidden = false
        self.backButton.isHidden = false
        self.dotButton.isHidden = false
        self.showInstruction(isVisible: false)
        
        //    self.change(shape: .expand)
        self.tabforPayLabel.isHidden = true
        
    })
    
    contactsView.isHidden = true
    //  self.contactsHeightConstraint.constant = 272
    
    self.contactsView.frame = CGRect(x: 0, y: self.view.frame.size.height/2, width: self.view.frame.size.width, height: self.view.frame.size.height)
    
    self.contactsHeightConstraint.constant = screenHeight/2-100
    
    self.contactsView.layoutIfNeeded()
    
    contactsTableView.reloadData()
}

    //MARK: Receive Callback
    func receiveCallback(frame: Data?) {
        let msg = String(data: frame ?? Data(), encoding: String.Encoding.utf8) ?? "data could not be decoded"
        println_debug(msg)
        
            let receiverDetailArray = msg.components(separatedBy: "#")
            if receiverDetailArray.count > 1 {
                if !recipientList.contains(where: {$0.number == receiverDetailArray[0]}) {
                    recipientList.append(Recipients(name: receiverDetailArray[1], number: receiverDetailArray[0], imageUrlStr: nil))
                    self.requestImage(mobileNumber: receiverDetailArray[0])
                    
                    var jsonDic = Dictionary<String, Any>()
                    jsonDic ["DistanceInKm"] = 0.0
                    jsonDic ["DistanceInMiles"] = 0.0
                    
                    var userDic = Dictionary<String, Any>()
                    userDic ["FirstName"] = receiverDetailArray[1]
                    userDic ["PhoneNumber"] = receiverDetailArray[0]
                    jsonDic ["UserContact"] = userDic
                    
                    let modelType = NearByServicesNewModel(dictionary: jsonDic as Dictionary<String, AnyObject>)
                    
                    if transitcall == false
                    {
                        recieverStatus = true
                        self.dummyArray.append(modelType)
                         self.contactsTableView.reloadData()
                        
                        self.dummyphonenumberArray.removeAll()
                        self.dummyphonenumberArray.append(receiverDetailArray[0])
                       
                        self.contactsTableView.reloadData()

                        self.profilepicServicecall()
                   
                    }
                    
                }
            }
    }
    
    private func requestImageAgent(mobileNumber: String) {
        let loginValReq = LoginValidationRequest(appID: UserModel.shared.appID, limit: 0, mobileNumber: mobileNumber, msid: msid, offset: 0, ostype: 1, otp: "", simid: uuid)
        let request = GetImagesByPhNumberReq(loginValidationRequest: loginValReq, mobilenumberList: [mobileNumber])
        modelInstaPay?.getImage(request: request)
        
    }
    
    private func requestImage(mobileNumber: String) {
        let loginValReq = LoginValidationRequest(appID: UserModel.shared.appID, limit: 0, mobileNumber: UserModel.shared.mobileNo, msid: msid, offset: 0, ostype: 1, otp: "", simid: uuid)
        let request = GetImagesByPhNumberReq(loginValidationRequest: loginValReq, mobilenumberList: [mobileNumber])
        modelInstaPay?.getImage(request: request)
       
    }
    
    //MARK: Long Press
    private func addLongPressGestures() {
        longPress.addTarget(self, action: #selector(longPressAction))
        longPress.minimumPressDuration = 0.3
        longPress.delegate = self
        self.buzzer.addGestureRecognizer(longPress)
    }
    
    private func removeLongPressGesture() {
        self.buzzer.removeGestureRecognizer(longPress)
    }
    
    //MARK: Pan
    private func addPanGesture() {
        pan.addTarget(self, action: #selector(panAction))
        pan.maximumNumberOfTouches = 1
        pan.delegate = self
        self.buzzer.addGestureRecognizer(pan)
        
    }
    
    //MARK: Tap
    private func addTapGesture() {
        tap.addTarget(self, action: #selector(tapAction))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        tap.delegate = self
        self.buzzer.addGestureRecognizer(tap)
    }
   
    
    //MARK: - Target Methods
    @objc func longPressAction(gestureRecognizer: UILongPressGestureRecognizer) {
        guard gestureRecognizer.view != nil else {return}
        switch gestureRecognizer.state {
        case .began:
            self.change(shape: .expand)
            switchesView.isHidden = true
             tabforPayLabel.isHidden = true
        case .ended:
            switch switchState {
            case .towardsPay, .towardsReceive, .pay, .receive:
                self.change(shape: .expand)
            case .towardsNeutral, .neutral:
          //      self.change(shape: .collapse)
                 tabforPayLabel.isHidden = true
            }
        default:
            break
        }
    }
 
    @objc func panAction(gestureRecognizer: UIPanGestureRecognizer) {
        guard gestureRecognizer.view != nil else {return}
        let piece = gestureRecognizer.view!
        let translation = gestureRecognizer.translation(in: piece.superview)
        switch gestureRecognizer.state {
        case .began:
            switch currentShape {
            case .collapse:
                self.change(shape: .expand)
                switchesView.isHidden = true
                 tabforPayLabel.isHidden = true
            default:
                break
            }
        case .changed:
            self.panchanges(with: translation.y)
        case .ended:
            switch switchState {
            case .towardsPay, .pay, .receive, .towardsReceive:
                self.change(shape: .expand)
            case .towardsNeutral, .neutral:
           //     self.change(shape: .collapse)
                 tabforPayLabel.isHidden = true
            }
        default:
            break
        }
    }
    
    @objc func tapAction(gestureRecognizer: UIPanGestureRecognizer) {
        guard gestureRecognizer.view != nil else {return}
        switch gestureRecognizer.state {
        case .ended:
            switch currentShape {
            case .collapse:
                self.change(shape: .expand)
                 tabforPayLabel.isHidden = true
            case .expand:
          //      self.change(shape: .collapse)
                 tabforPayLabel.isHidden = true
            }
        default:
            break
        }
    }
    
    @objc func paymentDoneClose() {
        self.cancel(sender: UIButton())
    }
    
    @objc func morePay() {
        print("More Pay")
    }
    
    //MARK: - Button Action Methods
    @IBAction func cancel(sender: UIButton) {
        removeRippleView() {}
      //  self.viewHoldingTable.isHidden = true
   //     DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
          //  self.change(shape: .collapse)
    
            self.ispayReceive(active: false)
            self.buzzerCenterY.constant = 0
            self.constraintHPath.constant = self.viewPath.frame.size.width
            self.constraintHPath.constant = self.heightOfShape
            self.currentShape = .expand
             self.switchState = .neutral
       
        //    self.change(shape: .expand)
            self.tabforPayLabel.isHidden = true
           self.bottomView.isHidden = false
           self.viewInstruction.isHidden = true
        imgViewProfile.isHidden = false
        lblProfile.isHidden = false
        backButton.isHidden = false
        dotButton.isHidden = false
         self.showInstruction(isVisible: false)
      
     //   })
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dotbuttonClick(_ sender: Any) {
        
        switchesView.isHidden = false
    }
    
    
    @IBAction func closeButtonClick(_ sender: Any) {
        self.dummyArray.removeAll()
        self.dummyphonenumberArray.removeAll()
        self.dummyprofilepicArray.removeAll()
        self.contactsTableView.reloadData()
       
            self.removeRippleView() {}
        //  self.viewHoldingTable.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            //  self.change(shape: .collapse)
     
        //    self.contactsTableView.reloadData()
            self.ispayReceive(active: false)
            self.buzzerCenterY.constant = 0
            self.constraintHPath.constant = self.viewPath.frame.size.width
            self.constraintHPath.constant = self.heightOfShape
            self.currentShape = .expand
            self.switchState = .neutral
            
            //    self.change(shape: .expand)
            self.bottomView.isHidden = false
            self.viewInstruction.isHidden = true
            self.tabforPayLabel.isHidden = true
            self.imgViewProfile.isHidden = false
            self.lblProfile.isHidden = false
            self.backButton.isHidden = false
            self.dotButton.isHidden = false
            self.showInstruction(isVisible: false)
            
        })
            self.recipientList.removeAll()
            self.transitcall = false
            self.contactsView.isHidden = true
            if self.comparestr == "1"
        {
            self.comparestr = "0"
            self.contactsView.frame = CGRect(x: 0, y: self.view.frame.size.height/2, width: self.view.frame.size.width, height: self.view.frame.size.height+65)
            
            self.contactsHeightConstraint.constant =  screenHeight/2-100
            
            self.contactsView.layoutIfNeeded()
        }
        else
        {
            self.comparestr = "0"
            //
        }
    }
    
    
    @IBAction func mobilenumswitchClick(_ sender: Any) {
         if mobilenumSwitch.isOn
         {
                 userDef.set(true , forKey: "mobileOn")
                 MobStatus = "s"
        }
        else
         {
               userDef.set(false,  forKey: "mobileOn")
               MobStatus = "n"
        }
        
    }
    
    
    @IBAction func profileSwitchClick(_ sender: Any) {
        if profileSwitch.isOn
        {
              userDef.set(true, forKey: "profileOn")
             picStatus = "s"
        }
        else
        {
              userDef.set(false, forKey: "profileOn")
              picStatus = "n"
        }
        
    }
    
    @IBAction func recevieButtonClick(_ sender: Any) {
    }
    
    @IBAction func payButtonClick(_ sender: Any) {
        
    }
    
    @IBAction func detailInstruction(_ sender: UIButton) {
        var bottomValue: CGFloat = 0
        var image: UIImage!
        if isDetailInstrShow {
            bottomValue = -(heightOfInstruction - heightOfInstructionTitle)
            image = UIImage(named: "up_arrow_small")
            viewScreen.isHidden = true
            isDetailInstrShow = false
        } else {
            bottomValue = 0
            image = UIImage(named: "down_arrow_small")
            viewScreen.isHidden = false
            sender.tag = 1
            isDetailInstrShow = true
        }
        UIView.animate(withDuration: 0.3, animations: {
            self.constraintBInstruction.constant = bottomValue
            self.view.layoutIfNeeded()
            self.imgViewInstrArrow.image = image
        }, completion: { (bool) in })
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        self.cancel(sender: UIButton())
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

//MARK: - UIGestureRecognizerDelegate
extension InstaPayViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return gestureRecognizer == longPress && otherGestureRecognizer == pan
    }
}

//MARK: - InstaPayModelDelegate
extension InstaPayViewController: InstaPayModelDelegate {
    func apiResult(data: InstaPayProfileImages?, message: String?, statusCode: String?) {
        if let number = data?.mobileNumber, let profilePic = data?.profilePic {
            if let firstIndex = recipientList.firstIndex(where: {$0.number == number}) {
                if self.recipientList.count > 0 {
                    self.recipientList[firstIndex].imageUrlStr = profilePic
                    DispatchQueue.main.async {
                        println_debug(firstIndex)
                        println_debug(profilePic)
                    //    self.contactsTableView.reloadRows(at: [IndexPath(row: firstIndex, section: 0)], with: .automatic)
                    }
                }
            }
        }
    }
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}
