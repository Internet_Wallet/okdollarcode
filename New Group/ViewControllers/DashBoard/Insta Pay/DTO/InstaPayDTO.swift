//
//  InstaPayDTO.swift
//  OK
//
//  Created by E J ANTONY on 4/23/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation

//MARK: - GetImagesByPhNumber Request
struct GetImagesByPhNumberReq: Codable {
    let loginValidationRequest: LoginValidationRequest
    let mobilenumberList: [String]
    
    enum CodingKeys: String, CodingKey {
        case loginValidationRequest = "LoginValidationRequest"
        case mobilenumberList = "MobilenumberList"
    }
}

struct LoginValidationRequest: Codable {
    let appID: String
    let limit: Int
    let mobileNumber, msid: String
    let offset, ostype: Int
    let otp, simid: String
    
    enum CodingKeys: String, CodingKey {
        case appID = "AppId"
        case limit = "Limit"
        case mobileNumber = "MobileNumber"
        case msid = "Msid"
        case offset = "Offset"
        case ostype = "Ostype"
        case otp = "Otp"
        case simid = "Simid"
    }
}

//MARK: GetImagesByPhNumber Response
struct GetImagesByPhNumberRes: Codable {
    let code: Int?
    let data, msg: String?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case msg = "Msg"
    }
}

struct InstaPayProfileImages: Codable {
    let mobileNumber: String?
    let profilePic: String?
    let businessPic, isShow: String?
    
    enum CodingKeys: String, CodingKey {
        case mobileNumber = "MobileNumber"
        case profilePic = "ProfilePic"
        case businessPic = "BusinessPic"
        case isShow = "IsShow"
    }
}
