//
//  InstaPayTableView.swift
//  OK
//
//  Created by E J ANTONY on 4/19/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

//MARK: - UITableViewDataSource
extension InstaPayViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if dummyArray.count > 0 {
            return 50
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if dummyArray.count > 0 {
            if section == 0 {
                return "Receiver User"
            }else{
                return "NearBy user"
            }

        } else {
            return ""
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if dummyArray.count > 0 {
            return 2
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if dummyArray.count > 0 {
            if section == 0
            {
                return dummyArray.count
            }
            else
            {
                let count = self.arrAgentsList.count
                var constant: CGFloat = 0
                if count > 0 && count < 4 {
                    constant = CGFloat((count * 49)) + 36
                } else {
                    constant = 183 //(3 * 49) + 36
                }
                //   self.constraintHViewTable.constant = constant
                self.view.layoutIfNeeded()
                return self.arrAgentsList.count
            }
        }
        else
          {
            let count = self.arrAgentsList.count
            var constant: CGFloat = 0
            if count > 0 && count < 4 {
                constant = CGFloat((count * 49)) + 36
            } else {
                constant = 183 //(3 * 49) + 36
            }
            //   self.constraintHViewTable.constant = constant
            self.view.layoutIfNeeded()
            return self.arrAgentsList.count
        }
     

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let instaTableCell = tableView.dequeueReusableCell(withIdentifier: "InstaPayTableCell", for: indexPath) as? InstaPayTableCell else { return InstaPayTableCell() }
      
        
        if dummyArray.count > 0
        {
            if indexPath.section == 0
            {
                
                instaTableCell.wrapData(data: self.dummyArray[indexPath.row])
                
                if self.dummyprofilepicArray.count > 0 {
                    
                   if dummyprofilepicArray.count >=  dummyArray.count {

                    let imgUrl = self.dummyprofilepicArray[indexPath.row].ProfileImageUrl

                    let parentId =  self.dummyprofilepicArray[indexPath.row].type

                    let offer = self.dummyprofilepicArray[indexPath.row].HaveOffer

                    getProfilePicture(imgUrl: imgUrl ?? "", parentId: parentId ?? 0, offer: offer ?? "", instaTableCell: instaTableCell)
                   }
                }
                
            }
            else
            {
                instaTableCell.wrapData(data: self.arrAgentsList[indexPath.row])
                
                if self.profilePicArray.count > 0 {
                    
                    let imgUrl = self.profilePicArray[indexPath.row].ProfileImageUrl
                    
                    let parentId =  self.profilePicArray[indexPath.row].type
                    
                    let offer = self.profilePicArray[indexPath.row].HaveOffer
                    
                    getProfilePicture(imgUrl: imgUrl ?? "", parentId: parentId ?? 0, offer: offer ?? "", instaTableCell: instaTableCell)
                }
                instaTableCell.selectionStyle = .none
                
                return instaTableCell
            }
            
//            else
//            {
//                instaTableCell.wrapData(data: self.arrAgentsList[indexPath.row])
//
//
//                if self.profilePicArray.count > 0 {
//
//                    let imgUrl = self.profilePicArray[indexPath.row].ProfileImageUrl
//
//                    let parentId =  self.profilePicArray[indexPath.row].type
//
//                    let offer = self.profilePicArray[indexPath.row].HaveOffer
//
//                     getProfilePicture(imgUrl: imgUrl ?? "", parentId: parentId ?? 0, offer: offer ?? "", instaTableCell: instaTableCell)
//
////                    if offer == "1"
////                    {
////                        instaTableCell.offersImageView.isHidden = false
////                    }
////                    else
////                    {
////                        instaTableCell.offersImageView.isHidden = true
////                    }
////
////                    if parentId == 2
////                    {
////                        //        instaTableCell.profileImageView.image = UIImage (named: "ok_agent")
////                        instaTableCell.profileImageView.sd_setImage(with: URL(string: imgUrl!), placeholderImage: UIImage(named: "ok_agent"), options: .transformAnimatedImage, progress: nil, completed: nil)
////
////                    }
////                    else if parentId == 7  {
////                        //   instaTableCell.profileImageView.image = UIImage (named: "H_headoffice")
////                        instaTableCell.profileImageView.sd_setImage(with: URL(string: imgUrl!), placeholderImage: UIImage(named: "H_headoffice"), options: .transformAnimatedImage, progress: nil, completed: nil)
////                    }
////                    else if parentId == 8  {
////                        //    instaTableCell.profileImageView.image = UIImage (named: "")
////                        instaTableCell.profileImageView.sd_setImage(with: URL(string: imgUrl!), placeholderImage: UIImage(named: ""), options: .transformAnimatedImage, progress: nil, completed: nil)
////                    }
////                    else if parentId == 9 {
////                        //  instaTableCell.profileImageView.image = UIImage (named: "post")
////                        instaTableCell.profileImageView.sd_setImage(with: URL(string: imgUrl!), placeholderImage: UIImage(named: "post"), options: .transformAnimatedImage, progress: nil, completed: nil)
////                    }
////                    else if parentId == 10 {
////                        //    instaTableCell.profileImageView.image = UIImage (named: "bank_cash_icon")
////                        instaTableCell.profileImageView.sd_setImage(with: URL(string: imgUrl!), placeholderImage: UIImage(named: "bank_cash_icon"), options: .transformAnimatedImage, progress: nil, completed: nil)
////                    }
////                    else if parentId == 11  {
////                        //   instaTableCell.profileImageView.image = UIImage (named: "ok_agent_find")
////                        instaTableCell.profileImageView.sd_setImage(with: URL(string: imgUrl!), placeholderImage: UIImage(named: "ok_agent_find"), options: .transformAnimatedImage, progress: nil, completed: nil)
////                    }
////                    else
////                    {
////                        instaTableCell.profileImageView.sd_setImage(with: URL(string: imgUrl!), placeholderImage: UIImage(named: "cicoUserAvatar"), options: .transformAnimatedImage, progress: nil, completed: nil)
////                    }
//
//                    //    instaTableCell.profileImageView.sd_setImage(with:  URL(string: imgUrl!), completed: nil)
//                }
//            }
        }
        else
        {
            instaTableCell.wrapData(data: self.arrAgentsList[indexPath.row])
            
            if self.profilePicArray.count > 0 {
                
                let imgUrl = self.profilePicArray[indexPath.row].ProfileImageUrl
                
                let parentId =  self.profilePicArray[indexPath.row].type
                
                let offer = self.profilePicArray[indexPath.row].HaveOffer
                
                getProfilePicture(imgUrl: imgUrl ?? "", parentId: parentId ?? 0, offer: offer ?? "", instaTableCell: instaTableCell)
        }
      
   
        
    //    instaTableCell.offersImageView.isHidden = false
        
        instaTableCell.selectionStyle = .none
        
        return instaTableCell
    }
        return instaTableCell
        
}
    
}

//MARK: - UITableViewDelegate
extension InstaPayViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let storyboard = UIStoryboard(name: "BookOnOKDollar", bundle: nil)
        //        guard let sendVC = storyboard.instantiateViewController(withIdentifier: "InstaPaySendViewController") as? InstaPaySendViewController else { return }
        //        sendVC.profileImage = self.imgViewProfile.image
        //        //sendVC.recipient = self.recipientList[indexPath.row]
        //        sendVC.contactModel = self.arrAgentsList[indexPath.row]
        //        let nc = UINavigationController(rootViewController: sendVC)
        //        nc.navigationBar.barTintColor = kYellowColor
        //        self.present(nc, animated: true, completion: nil)
        if recieverStatus {
            if indexPath.section == 0 {
                clickSelectCell(profilePicArray:  self.dummyprofilepicArray, index: indexPath.row, arrAgentsList: self.dummyArray)
            }else{
                clickSelectCell(profilePicArray:  self.profilePicArray, index: indexPath.row, arrAgentsList: self.arrAgentsList)
            }
        } else {
            clickSelectCell(profilePicArray:  self.profilePicArray, index: indexPath.row, arrAgentsList: self.arrAgentsList)
        }
      
        
//        let favContact = self.profilePicArray[indexPath.row]
//        if favContact.HaveOffer == "1" {
//            alertViewObj.wrapAlert(title: nil, body:"Do you want to use promo code for this number?".localized, img: #imageLiteral(resourceName: "NoOffersFound"))
//            alertViewObj.addAction(title: "YES".localized, style: .cancel) {
//                if let addWithdrawView = UIStoryboard(name: "Offers", bundle: nil).instantiateViewController(withIdentifier: "OffersViewController") as? OffersViewController {
//                    let aObjNav = UINavigationController(rootViewController: addWithdrawView)
//                    self.present(aObjNav, animated: true, completion: nil)
//                //    self.navigationController?.pushViewController(aObjNav, animated: true)
//                }
//            }
//            alertViewObj.addAction(title: "NO".localized, style: .cancel) {
//                let storyboard = UIStoryboard(name: "BookOnOKDollar", bundle: nil)
//                guard let sendVC = storyboard.instantiateViewController(withIdentifier: "InstaPaySendViewController") as? InstaPaySendViewController else { return }
//                sendVC.profileImage = self.imgViewProfile.image
//                 sendVC.recevierImage = self.profilePicArray[indexPath.row].ProfileImageUrl
//                sendVC.contactModel = self.arrAgentsList[indexPath.row]
//                let nc = UINavigationController(rootViewController: sendVC)
//                nc.navigationBar.barTintColor = kYellowColor
//                self.present(nc, animated: true, completion: nil)
//            }
//            alertViewObj.showAlert()
//        } else {
//            let storyboard = UIStoryboard(name: "BookOnOKDollar", bundle: nil)
//            guard let sendVC = storyboard.instantiateViewController(withIdentifier: "InstaPaySendViewController") as? InstaPaySendViewController else { return }
//            sendVC.profileImage = self.imgViewProfile.image
//            sendVC.recevierImage = self.profilePicArray[indexPath.row].ProfileImageUrl
//            sendVC.contactModel = self.arrAgentsList[indexPath.row]
//            let nc = UINavigationController(rootViewController: sendVC)
//            nc.navigationBar.barTintColor = kYellowColor
//            self.present(nc, animated: true, completion: nil)
//        }
    }
    
}

extension InstaPayViewController{
    
    func clickSelectCell(profilePicArray:  [ProfilePicModel], index: Int,arrAgentsList: [NearByServicesNewModel]){
        
    //    if profilePicArray.count > index {
        let favContact = profilePicArray[index]
        if favContact.HaveOffer == "0" {
            alertViewObj.wrapAlert(title: nil, body:"Do you want to use promo code for this number?".localized, img: #imageLiteral(resourceName: "NoOffersFound"))
            alertViewObj.addAction(title: "YES".localized, style: .cancel) {
                if let addWithdrawView = UIStoryboard(name: "Offers", bundle: nil).instantiateViewController(withIdentifier: "OffersViewController") as? OffersViewController {
                    addWithdrawView.modalPresentationStyle = .fullScreen
                    let aObjNav = UINavigationController(rootViewController: addWithdrawView)
                    self.present(aObjNav, animated: true, completion: nil)
                    //    self.navigationController?.pushViewController(aObjNav, animated: true)
                }
            }
            alertViewObj.addAction(title: "NO".localized, style: .cancel) {
                let storyboard = UIStoryboard(name: "BookOnOKDollar", bundle: nil)
                guard let sendVC = storyboard.instantiateViewController(withIdentifier: "InstaPaySendViewController") as? InstaPaySendViewController else { return }
                sendVC.profileImage = self.imgViewProfile.image
                sendVC.recevierImage = profilePicArray[index].ProfileImageUrl
                sendVC.contactModel = arrAgentsList[index]
                sendVC.modalPresentationStyle = .fullScreen
                let nc = UINavigationController(rootViewController: sendVC)
                nc.navigationBar.barTintColor = kYellowColor
                
//                        let txConf1: QMTransmitterConfig = QMTransmitterConfig(key:"ultrasonic") //ultrasonic-experimental
//                        let tx1: QMFrameTransmitter = QMFrameTransmitter(config: txConf1)
//                        let frame_str = "Close Recevier"
//                        let data = frame_str.data(using: .utf8)
//                        tx1.send(data)
                
                self.present(nc, animated: true, completion: nil)
            }
            alertViewObj.showAlert()
        } else {
            let storyboard = UIStoryboard(name: "BookOnOKDollar", bundle: nil)
            guard let sendVC = storyboard.instantiateViewController(withIdentifier: "InstaPaySendViewController") as? InstaPaySendViewController else { return }
            sendVC.profileImage = self.imgViewProfile.image
            sendVC.recevierImage = profilePicArray[index].ProfileImageUrl
            sendVC.contactModel = arrAgentsList[index]
            let nc = UINavigationController(rootViewController: sendVC)
            nc.modalPresentationStyle = .fullScreen
            nc.navigationBar.barTintColor = kYellowColor
            
//            let txConf1: QMTransmitterConfig = QMTransmitterConfig(key:"ultrasonic") //ultrasonic-experimental
//            let tx1: QMFrameTransmitter = QMFrameTransmitter(config: txConf1)
//            let frame_str = "Close Recevier"
//            let data = frame_str.data(using: .utf8)
//            tx1.send(data)
            
            self.present(nc, animated: true, completion: nil)
        }
    //    }
    }
    
    func getProfilePicture(imgUrl : String, parentId: Int ,offer : String,instaTableCell: InstaPayTableCell){
        
            if offer == "0"
            {
                instaTableCell.offersImageView.isHidden = false
            }
            else
            {
                instaTableCell.offersImageView.isHidden = true
            }
            
            if parentId == 2
            {
                //        instaTableCell.profileImageView.image = UIImage (named: "ok_agent")
                 let url = URL(string: imgUrl.replacingOccurrences(of: " ", with: "%20"))
                instaTableCell.profileImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ok_agent"), options: .transformAnimatedImage, progress: nil, completed: nil)
           
            }
            else if parentId == 7  {
                //   instaTableCell.profileImageView.image = UIImage (named: "H_headoffice")
                let url = URL(string: imgUrl.replacingOccurrences(of: " ", with: "%20"))
                instaTableCell.profileImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "H_headoffice"), options: .transformAnimatedImage, progress: nil, completed: nil)
            }
            else if parentId == 8  {
                //    instaTableCell.profileImageView.image = UIImage (named: "")
                    let url = URL(string: imgUrl.replacingOccurrences(of: " ", with: "%20"))
                instaTableCell.profileImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "onestopmart"), options: .transformAnimatedImage, progress: nil, completed: nil)
            }
            else if parentId == 9 {
                //  instaTableCell.profileImageView.image = UIImage (named: "post")
                  let url = URL(string: imgUrl.replacingOccurrences(of: " ", with: "%20"))
                instaTableCell.profileImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "post"), options: .transformAnimatedImage, progress: nil, completed: nil)
            }
            else if parentId == 10 {
                //    instaTableCell.profileImageView.image = UIImage (named: "bank_cash_icon")
                  let url = URL(string: imgUrl.replacingOccurrences(of: " ", with: "%20"))
                instaTableCell.profileImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "bank_cash_icon"), options: .transformAnimatedImage, progress: nil, completed: nil)
            }
            else if parentId == 11  {
                //   instaTableCell.profileImageView.image = UIImage (named: "ok_agent_find")
                  let url = URL(string: imgUrl.replacingOccurrences(of: " ", with: "%20"))
                instaTableCell.profileImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "ok_agent_find"), options: .transformAnimatedImage, progress: nil, completed: nil)
            }
            else
            {
                instaTableCell.profileImageView.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage(named: "cicoUserAvatar"), options: .transformAnimatedImage, progress: nil, completed: nil)
            }
   
    }
    
    
}

//MARK: - InstaPayTableCell
class InstaPayTableCell: UITableViewCell {
    //MARK: - Outlets
    @IBOutlet weak var labelInitial: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var lableNumber: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var offersImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.labelInitial.layer.cornerRadius = 20
        
        self.labelInitial.layer.masksToBounds = true
       self.profileImageView.layer.cornerRadius = 20
    }
    
    func wrapData(data: NearByServicesNewModel){
        self.labelInitial.backgroundColor = randomColor()
        
        if data.UserContactData?.FirstName == "<null>" || data.UserContactData?.FirstName == "" ||  data.UserContactData?.PhoneNumber == "<null>" {
            println_debug("no first name")

        }
        else
        {
      
            self.labelName.text = data.UserContactData?.FirstName
            
            let string = data.UserContactData?.FirstName ?? ""
            if string.length > 3 {
            let stripped1 = String((string.dropFirst(3)))
           
            let firstCharIndex = stripped1.index(stripped1.startIndex, offsetBy: 1)
            let firstChar = stripped1.substring(to: firstCharIndex)
            
            self.labelInitial.text = firstChar
            } else {
                self.labelInitial.text = ""
            }
            
            let myString = data.UserContactData?.PhoneNumber ?? ""
            
             if myString.length > 2 {
            let stripped = String((myString.dropFirst(2)))
            let Str = "+" + stripped;
            
            let mobileNumer = Str
            let intLetters = mobileNumer.prefix(3)
            let endLetters = mobileNumer.suffix(4)
            
            let newString = String(intLetters + "******" + endLetters)   //"+91*******21"
            
            self.lableNumber.text = newString
            }
            else
            {
                self.lableNumber.text = ""
            }
             
        }
     
    }
    
    //MARK: - Methods
    func configureCell(data: InstaPayViewController.Recipients) {
        self.labelInitial.backgroundColor = randomColor()
        self.labelInitial.text = String(data.name.prefix(1))
        self.labelName.text = data.name
        self.lableNumber.text = data.number
        if let urlStr = data.imageUrlStr {
            if urlStr.contains(find: " ") {
                if let imagePEUrl = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                    let imageURL = URL(string: imagePEUrl)
                    self.imgView.sd_setImage(with: imageURL, completed: nil)
                } else {
                    let imageURL = URL(string: urlStr)
                    self.imgView.sd_setImage(with: imageURL, completed: nil)
                }
            } else {
                let imageURL = URL(string: urlStr)
                self.imgView.sd_setImage(with: imageURL, completed: nil)
            }
        }
    }
    
    func randomColor() -> UIColor {
        var randomRed:CGFloat = 0.0
        var randomGreen:CGFloat = 0.0
        var randomBlue:CGFloat = 0.0
        let nonRandomAlpha:CGFloat = 1.0
        // reject colors that are too dark -- colors //
        // are rejected 0.35/3 = %11.6 of the time   //
        while ( randomRed + randomGreen + randomBlue < 0.35) {
            // for each color channel we generate a random number //
            randomRed = CGFloat(drand48())
            randomGreen = CGFloat(drand48())
            randomBlue = CGFloat(drand48())
        }
        return UIColor(red: randomRed,
                       green: randomGreen,
                       blue: randomBlue,
                       alpha: nonRandomAlpha)
    }
}
