//
//  InstaPaySendViewController.swift
//  OK
//
//  Created by E J ANTONY on 4/12/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import SDWebImage

class InstaPaySendViewController: UIViewController,WebServiceResponseDelegate{
    //MARK: - Outlet
    @IBOutlet weak var viewPayImageHolder: UIView!
    @IBOutlet weak var viewReceiveImageHolder: UIView!
    @IBOutlet weak var imgViewProfilePay: UIImageView!
    @IBOutlet weak var imgViewProfileReceive: UIImageView!
    @IBOutlet weak var lblNameIndex: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPlaceHolder: UILabel!{
        didSet {
            self.lblPlaceHolder.font = UIFont(name: appFont, size: appFontSize)
            self.lblPlaceHolder.text = "Enter your remarks".localized
        }
    }
  //  @IBOutlet weak var txtFldAmount: UITextField!
    @IBOutlet weak var mmkLabel: UILabel!
    @IBOutlet weak var instaCloseButton: UIButton!
    
    @IBOutlet weak var txtFldAmount: RestrictedCursorMovement!
    // @IBOutlet weak var txtFldAmount: SkyFloatingLabelTextField!
    @IBOutlet weak var txtViewComments: UITextView!
    @IBOutlet weak var constraintBottomNext: NSLayoutConstraint!
    @IBOutlet weak var constraintBottomScroll: NSLayoutConstraint!
    @IBOutlet weak var viewNext: UIView!
      let button = UIButton(type: .custom)
    let label = UILabel()
    
    var comparestr = String()
    var amountstr = String()
    var updatedText = String()
    //MARK: - Properties
    private var rNumber: String = ""
   // var contact: FavoriteContacts?
      var categorylistArr = [String:Any]()
       var mainAccountType: String = ""
    var contactModel: NearByServicesNewModel?
    var profileImage: UIImage?
    var recevierImage: String?
    var recipient: InstaPayViewController.Recipients?
    var modelInstaPay: InstaPayModel!
    
    var timer = Timer()
    var isTimerRunning = false
    
 //    var amountCheckingTimer: Timer?
    
    @IBAction func instaCloseClick(_ sender: Any) {
        
        txtFldAmount.text = ""
        //    if txtFldAmount.text == ""{
        //button.isHidden = true
         instaCloseButton.isHidden = true
        self.viewNext.isHidden = true
        mmkLabel.isHidden = true
    }
    
    @IBInspectable var postfix : String = ""
    @IBInspectable var removePostfixOnEditing : Bool = true

    
    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
         self.viewNext.isHidden = true
        self.txtViewComments.layer.cornerRadius = 8
        self.viewPayImageHolder.layer.cornerRadius = (self.viewPayImageHolder.frame.size.width / 2)
        self.viewReceiveImageHolder.layer.cornerRadius = (self.viewReceiveImageHolder.frame.size.width / 2)
        self.txtFldAmount.keyboardType = .decimalPad
        self.txtFldAmount.becomeFirstResponder()
        self.configureModel()
        if let senderImage = profileImage {
            self.imgViewProfilePay.image = senderImage
        } else {
            guard let profilePicUrl = URL(string: UserModel.shared.proPic) else { return }
            self.imgViewProfilePay.downloadedFrom(url: profilePicUrl)
            self.imgViewProfilePay.contentMode = .scaleAspectFill
        }
        if let receiveImage = recevierImage {
            self.imgViewProfileReceive.sd_setImage(with: URL(string: receiveImage), placeholderImage: UIImage(named: "cicoUserAvatar"), options: .transformAnimatedImage, progress: nil, completed: nil)
        } else {
            guard let profilePicUrl = URL(string: UserModel.shared.proPic) else { return }
            self.imgViewProfilePay.downloadedFrom(url: profilePicUrl)
            self.imgViewProfilePay.contentMode = .scaleAspectFill
        }
        
        
        if let contactObj = contactModel, let name = contactObj.UserContactData?.FirstName {
            self.lblName.text = name
            self.lblNameIndex.text = String(name.prefix(1))
            self.rNumber = contactObj.UserContactData?.PhoneNumber ?? ""
        }
//        else if let receiverObj = recipient {
//            self.lblName.text = receiverObj.name
//            self.lblNameIndex.text = String(receiverObj.name.prefix(1))
//            self.rNumber = receiverObj.number
//            if let urlStr = receiverObj.imageUrlStr {
//                if urlStr.contains(find: " ") {
//                    if let imagePEUrl = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
//                        let imageURL = URL(string: imagePEUrl)
//                        self.imgViewProfileReceive.sd_setImage(with: imageURL, completed: nil)
//                    } else {
//                        let imageURL = URL(string: urlStr)
//                        self.imgViewProfileReceive.sd_setImage(with: imageURL, completed: nil)
//                    }
//                } else {
//                    let imageURL = URL(string: urlStr)
//                    self.imgViewProfileReceive.sd_setImage(with: imageURL, completed: nil)
//                }
//            }
//        }
        
        else if let receiverObj = contactModel {
            self.lblName.text = receiverObj.UserContactData?.FirstName
            self.lblNameIndex.text = String(receiverObj.UserContactData!.FirstName!.prefix(1))
            self.rNumber = receiverObj.UserContactData!.PhoneNumber!
//            if let urlStr = receiverObj.imageUrlStr {
//                if urlStr.contains(find: " ") {
//                    if let imagePEUrl = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
//                        let imageURL = URL(string: imagePEUrl)
//                        self.imgViewProfileReceive.sd_setImage(with: imageURL, completed: nil)
//                    } else {
//                        let imageURL = URL(string: urlStr)
//                        self.imgViewProfileReceive.sd_setImage(with: imageURL, completed: nil)
//                    }
//                } else {
//                    let imageURL = URL(string: urlStr)
//                    self.imgViewProfileReceive.sd_setImage(with: imageURL, completed: nil)
//                }
//            }
        }
        self.viewNext.layer.cornerRadius = (self.viewNext.frame.size.width / 2)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
//        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
//        txtFldAmount.rightView = paddingView
//        txtFldAmount.rightViewMode = .always
//
//        let arrow1 = UIImageView(image: UIImage(named: "downarrow"))
//        arrow1.frame = CGRect(x: 0.0, y: 0.0, width: (arrow1.image?.size.width ?? 0.0) + 30.0, height: arrow1.image?.size.height ?? 0.0)
//        arrow1.contentMode = .center
//
//
//
//
//        txtFldAmount.rightView = arrow1
//        txtFldAmount.rightViewMode = .always
//
        
       
//        button.setImage(UIImage(named: "cancel_small")!.withRenderingMode(.alwaysOriginal), for: .normal)
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
//        button.frame = CGRect(x: CGFloat(txtFldAmount.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
//        button.addTarget(self, action: #selector(self.refresh), for: .touchUpInside)
//        txtFldAmount.rightView = button
//        txtFldAmount.rightViewMode = .always
        
     //   button.isHidden = true
        instaCloseButton.isHidden = true
        mmkLabel.isHidden = true
     
    
    }
    @IBAction func refresh(_ sender: Any) {
        txtFldAmount.text = ""
    //    if txtFldAmount.text == ""{
             button.isHidden = true
              self.viewNext.isHidden = true
              mmkLabel.isHidden = true
      //  }
        
    }
 
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
        self.getCategoryList()

    //    txtFldAmount.text = ""
   //     txtViewComments.text = ""
        
        let aString1 =  NSMutableAttributedString(string: "0", attributes: [NSAttributedString.Key.font: UIFont(name: appFont, size: 32.0) ?? UIFont.systemFont(ofSize: 32),NSAttributedString.Key.foregroundColor:UIColor.gray])
        
        let aString2 =  NSAttributedString(string: " MMK ", attributes: [NSAttributedString.Key.font: UIFont(name: appFont, size: 20.0) ?? UIFont.systemFont(ofSize: 20),NSAttributedString.Key.foregroundColor:UIColor.gray])
        
        aString1.append(aString2)
        txtFldAmount!.attributedPlaceholder = aString1;
        
        if userDef.object(forKey: "FromInstaPaySession") != nil {
            
        }
        else{
             self.addPaymentObserver()
        }
       
        amountstr = txtFldAmount.text ?? ""
        
    }
    
    func getCategoryList() {
        
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self as WebServiceResponseDelegate
        //    showProgressView()
            
            let buldApi = Url.billSplitterMobileValidation + "MobileNumber=\(contactModel?.UserContactData?.PhoneNumber ?? "")"
            
            let url = getUrl(urlStr: buldApi, serverType: .serverApp)
            println_debug(url)
            
            let params = Dictionary<String,String>()
            
            web.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", mScreen: "PromotionDetail")
        }
        
    }
    
    
    func webResponse(withJson json: AnyObject, screen: String) {
        //self.removeProgressView()
        //if screen == "PromotionDetail" {
        do {
            if let data = json as? Data {
                let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                print("promotiondetail category----\(dict)")
                if let dic = dict as? Dictionary<String,AnyObject> {
                    if dic["Code"]! as! Int == 200 {
                        DispatchQueue.main.async {
                            
                            let dataDict = dic["Data"] as? String
                            self.categorylistArr = OKBaseController.convertToDictionary(text: dataDict!)!
                        
                            self.mainAccountType = self.categorylistArr.safeValueForKey("AccountType") as! String
                        
                        }
                    } else {
                        
                        DispatchQueue.main.async {
                        }
                    }
                }
            }
        } catch {
            
        }
    }
    
    
    //MARK: - Methods
    private func addPaymentObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(repeatPay), name: NSNotification.Name(rawValue: "InstaPayRepeat"), object: nil)
        userDef.set(txtFldAmount.text, forKey: "repeatpayment")
    }
    
    private func configureModel() {
        modelInstaPay = InstaPayModel()
        modelInstaPay.instaPayModelDelegate  = self
    }
    
    private func isValidationSuccess() -> Bool {
        if let amount = txtFldAmount.text {
            if amount == "0" || amount.count == 0 || amount == ".00" || amount == ".0" || amount == "." {
             
                alertViewObj.wrapAlert(title: nil, body:"Please enter minimum amount 1 MMK".localized, img: #imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                }
                alertViewObj.showAlert(controller: UIViewController.init())
                return false
            }
            else if  NSString(string: UserLogin.shared.walletBal).floatValue < NSString(string: amount.replacingOccurrences(of: ",", with: "")).floatValue {
                alertViewObj.wrapAlert(title: nil, body:"Insufficent Balance".localized, img: #imageLiteral(resourceName: "alert-icon"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                }
                alertViewObj.showAlert(controller: UIViewController.init())
                return false
            }
        
            return true
        }
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let amount = txtFldAmount.text {
            if amount == "" {
              //  button.isHidden = true
                 instaCloseButton.isHidden = true
                self.viewNext.isHidden = true
                  mmkLabel.isHidden = true
                let aString1 =  NSMutableAttributedString(string: "0", attributes: [NSAttributedString.Key.font: UIFont(name: appFont, size: 32.0) ?? UIFont.systemFont(ofSize: 32),NSAttributedString.Key.foregroundColor:UIColor.gray])
                
                let aString2 =  NSAttributedString(string: " MMK ", attributes: [NSAttributedString.Key.font: UIFont(name: appFont, size: 20.0) ?? UIFont.systemFont(ofSize: 20),NSAttributedString.Key.foregroundColor:UIColor.gray])
                
                aString1.append(aString2)
                txtFldAmount!.attributedPlaceholder = aString1;
            }
            else
            {
            //    txtFldAmount.text?.append(" mmk")
             //   button.isHidden = false
                 instaCloseButton.isHidden = false
                self.viewNext.isHidden = false
                  mmkLabel.isHidden = false
                if amountstr == amount
                {
                    comparestr = "1"
                }
                else
                {
                     comparestr = "0"
                }
                
            }
        }
}
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let amount = txtFldAmount.text {
            if amount == "" {
           //     button.isHidden = true
                 instaCloseButton.isHidden = true
                self.viewNext.isHidden = true
                  mmkLabel.isHidden = true
            }
            else
            {
                
         //       txtFldAmount.text?.append("mmk")
             //   button.isHidden = false
                 instaCloseButton.isHidden = false
                 self.viewNext.isHidden = false
                  mmkLabel.isHidden = false
            }
          
            let positionBeginning = textField.endOfDocument
            let textRange = textField.textRange(from: positionBeginning, to: positionBeginning)
            textField.selectedTextRange = textRange
        }
    }
 
    private func multiCashbackParameters() -> Dictionary<String,Any> {
        var finalObject =  Dictionary<String,AnyObject>()
        var dict        = [Dictionary<String,String>]()
        
        var cashbackobj = Dictionary<String,String>()
        cashbackobj["Agentcode"]    = UserModel.shared.mobileNo
        cashbackobj["Amount"]       = txtFldAmount.text?.replacingOccurrences(of: ",", with: "") ?? "0"
        cashbackobj["Clientip"]     = OKBaseController.getIPAddress()
        cashbackobj["Clientos"]     = "iOS"
        cashbackobj["Clienttype"]   = "GPRS"
        cashbackobj["Destination"]  = rNumber
        cashbackobj["Pin"]          = ok_password ?? ""
        cashbackobj["Requesttype"]  = "FEELIMITKICKBACKINFO"
        cashbackobj["Securetoken"]  = UserLogin.shared.token
        cashbackobj["Transtype"]    = "PAYTO"
        cashbackobj["Vendorcode"]   = "IPAY"
        dict.append(cashbackobj)
        
        var log = Dictionary<String,Any>()
        log["MobileNumber"] = UserModel.shared.mobileNo
        log["Msid"]        = getMsid()
        log["Ostype"]      = 1
        log["Otp"]        = ""
        log["SimId"]      = uuid
        
        finalObject["CashBackRequestList"] = dict as AnyObject
        finalObject["Login"] = log as AnyObject
        return finalObject
    }
    
    //MARK: - Target Methods
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            if txtFldAmount.text == ""
            {
                 self.viewNext.isHidden = true
                  mmkLabel.isHidden = true
            }
            else
            {
                  self.viewNext.isHidden = false
                  mmkLabel.isHidden = false
            }
            self.constraintBottomScroll.constant = keyboardSize.height
            self.constraintBottomNext.constant = keyboardSize.height
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        DispatchQueue.main.async {
            if self.txtFldAmount.text == ""
            {
                self.viewNext.isHidden = true
                self.mmkLabel.isHidden = true
            }
            else
            {
                self.viewNext.isHidden = false
                self.mmkLabel.isHidden = false
            }
            self.constraintBottomScroll.constant  = 0
            self.constraintBottomNext.constant = 100
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func repeatPay() {
//        txtFldAmount.text = ""
//        txtViewComments.text = ""
//        textViewDidChange(txtViewComments)
        
        amountstr = txtFldAmount.text ?? ""
        comparestr = "1"
        if self.isTimerRunning == false {
            self.runProdTimer()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    //MARK: - Button Action Methods
    @IBAction func previousScreen(_ sender: UIButton) {
        txtFldAmount.resignFirstResponder()
        txtViewComments.resignFirstResponder()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.dismiss(animated: true, completion: nil)
    }
     @objc   func timerManage() {
         comparestr = "0"
          userDef.removeObject(forKey: "repeatpayment")
    }
    func runProdTimer() {
        timer = Timer.scheduledTimer(timeInterval: 120, target: self, selector: (#selector(timerManage)), userInfo: nil, repeats: true)
        isTimerRunning = true
      
    }
    
    @IBAction func payToAction(_ sender: UIButton) {
         if userDef.object(forKey: "repeatpayment") != nil {
            var defstr = String()
            defstr = userDef.object(forKey: "repeatpayment") as! String
            
            if  defstr == txtFldAmount.text
            {
                alertViewObj.wrapAlert(title: nil, body: "Same Number with Same Amount can not be paid before 2 minutes".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                    self.txtFldAmount.text = ""
                    self.instaCloseButton.isHidden = true
                    self.viewNext.isHidden = true
                    self.mmkLabel.isHidden = true
                  //  self.comparestr = "0"
                })
                alertViewObj.showAlert()
            }
            else {
                comparestr = "0"
                if isValidationSuccess() {
                    self.view.endEditing(true)
                    
                    if self.rNumber.count > 0 {
                        let baseString = PaytoConstants.url.checkAgentCode.safelyWrappingString()
                        let mobile = URLQueryItem.init(name: "MobileNumber", value: rNumber)
                        let source = URLQueryItem.init(name: "SourceNumber", value: UserModel.shared.mobileNo)
                        let transactionType = URLQueryItem.init(name: "TransType", value: "PAYTO")
                        let appBuildNumber = URLQueryItem.init(name: "AppBuildNumber", value: buildNumber)
                        let osType  = URLQueryItem.init(name: "OsType", value: "1")
                        let itemsArray = [mobile, source, transactionType, appBuildNumber, osType]
                        
                        guard let url = PTManagerClass.createUrlComponents(base: baseString, queryItems: itemsArray) else {
                            PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.pleaseTryAgain.localized)
                            return
                        }
                        modelInstaPay.checkAgentCall(url: url)
                    }
                    //            if amountCheckingTimer != nil {
                    //                amountCheckingTimer?.invalidate()
                    //                amountCheckingTimer = nil
                    //            }
                }
            }
            
        }
        
//      if comparestr == "1"
//      {
//        alertViewObj.wrapAlert(title: nil, body: "Same Number with Same Amount can not be paid before 2 minutes".localized, img: #imageLiteral(resourceName: "enter_mobile"))
//        alertViewObj.addAction(title: "OK".localized, style: .target, action: {
//            self.txtFldAmount.text = ""
//            self.instaCloseButton.isHidden = true
//            self.viewNext.isHidden = true
//            self.mmkLabel.isHidden = true
//            self.comparestr = "0"
//        })
//        alertViewObj.showAlert()
//      }
    else {
        comparestr = "0"
        if isValidationSuccess() {
            self.view.endEditing(true)
          
            if self.rNumber.count > 0 {
                let baseString = PaytoConstants.url.checkAgentCode.safelyWrappingString()
                let mobile = URLQueryItem.init(name: "MobileNumber", value: rNumber)
                let source = URLQueryItem.init(name: "SourceNumber", value: UserModel.shared.mobileNo)
                let transactionType = URLQueryItem.init(name: "TransType", value: "PAYTO")
                let appBuildNumber = URLQueryItem.init(name: "AppBuildNumber", value: buildNumber)
                let osType  = URLQueryItem.init(name: "OsType", value: "1")
                let itemsArray = [mobile, source, transactionType, appBuildNumber, osType]
                
                guard let url = PTManagerClass.createUrlComponents(base: baseString, queryItems: itemsArray) else {
                    PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.pleaseTryAgain.localized)
                    return
                }
                modelInstaPay.checkAgentCall(url: url)
            }
//            if amountCheckingTimer != nil {
//                amountCheckingTimer?.invalidate()
//                amountCheckingTimer = nil
//            }
        }
            }
    }
    // amount update delegate fire for pay send view controller
    @objc func searchForKeyword(_ timer: Timer) {
        
        if let twoMinutesRef = self.txtFldAmount.text {
            var uiNumber = UserModel.shared.mobileNo
//            if let uiNumber = UserModel.shared.mobileNo {
                if uiNumber.hasPrefix("0") {
                    uiNumber = uiNumber.deletingPrefix("0")
                }
            
                uiNumber =  UserModel.shared.mobileNo
                if !PaymentVerificationManager.isValidPaymentTransactions(uiNumber, twoMinutesRef) {
                    
                    
                    DispatchQueue.main.async {
                        alertViewObj.wrapAlert(title: nil, body: "Same Number with Same Amount can not be paid before 2 minutes".localized, img: #imageLiteral(resourceName: "enter_mobile"))
                        alertViewObj.addAction(title: "OK".localized, style: .target, action: {
                               self.txtFldAmount.text = ""
                        })
                        alertViewObj.showAlert()
                        return
                    }
                    
                }
            }
    }

}
//}

//MARK: - UITextViewDelegate
extension InstaPaySendViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let textViewText = textView.text, let textRange = Range(range, in: textViewText) {
            let updatedText = textViewText.replacingCharacters(in: textRange, with: text)
            if updatedText.count > 80 {
                return false
            }
            if textViewText.count == 0 && text == " " {
                return false
            }
            if textViewText.count != 0 && updatedText.count == 1 && text == " " {
                textView.text = ""
                return false
            }
            let suff = String(textViewText.suffix(1))
            if suff == " " && text == " " {
                return false
            }
            if text == "\n" {
                txtViewComments .resignFirstResponder()
                return false
            }
        }
        return true
    }


    
    func textViewDidChange(_ textView: UITextView) {
        if let text = textView.text {
            if text.count > 0 {
                lblPlaceHolder.isHidden = true
            } else {
                lblPlaceHolder.isHidden = false
            }
        }
        textView.text = textView.text.replacingOccurrences(of: "  ", with: " ")
        textView.text = textView.text.replacingOccurrences(of: "  ", with: " ")
        //textView.text = textView.text.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.tintColor = UIColor.init(hex: "#E49FA4")
    }

}

//MARK: - UITextFieldDelegate
extension InstaPaySendViewController: UITextFieldDelegate {
    func isValidAmount(text :String) -> Bool {
        
        if text.contains(".") {
            let amountArray = text.components(separatedBy: ".")
            
            if amountArray.count > 2 {
                return false
            }
            
            if let last = amountArray.last {
                if last.count > 2 {
                    return false
                } else {
                    return true
                }
            }
            return true
        } else {
            let mystring = text.replacingOccurrences(of: ",", with: "")
            let number = NSDecimalNumber(string: mystring)
            
            let formatter = NumberFormatter()
            
            formatter.groupingSeparator = ","
            formatter.groupingSize = 3
            formatter.usesGroupingSeparator = true
            var num = formatter.string(from: number)
            if num == "NaN" {
                num = ""
            }
           
            self.txtFldAmount.text = num
            return false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var flag = true
        if let textFieldText = textField.text, let textRange = Range(range, in: textFieldText) {
             updatedText = textFieldText.replacingCharacters(in: textRange, with: string)
            print(updatedText)
            let numberCharSet = "1234567890."
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: numberCharSet).inverted).joined(separator: "")) { return false }
            flag = self.isValidAmount(text: updatedText)
            
            if updatedText.count>9
            {
                return flag == false
            }
            
        }
        let strMainTxt = textField.text?.replacingOccurrences(of: ",", with: "") ?? ""
        let amountArray = strMainTxt.components(separatedBy: ".")
        let mystring = amountArray[0]
        if(mystring.length > 9) {
            textField.textColor = UIColor.red
        } else if(mystring.length > 8) && (mystring.length <= 9)  {
            textField.textColor = UIColor.green
        } else {
            textField.textColor = UIColor.white
        }
        if let amount = textField.text {
            if amount == "" {
               // button.isHidden = true
                 instaCloseButton.isHidden = true
                self.viewNext.isHidden = true
                  mmkLabel.isHidden = true
            }
            else
            {
             //   button.isHidden = false
                 instaCloseButton.isHidden = false
                self.viewNext.isHidden = false
                  mmkLabel.isHidden = false
            }
        }
        if textField.selectedTextRange?.start != textField.selectedTextRange?.end {
            let endOfDocument = textField.endOfDocument
            textField.selectedTextRange = textField.textRange(from: endOfDocument, to: endOfDocument)
            return flag == false
        }
        
        if textField == txtFldAmount {
            
            
            if let text = textField.text, text.count > 0 {
                //                let formatter = NumberFormatter()
                //                formatter.numberStyle = NumberFormatter.Style.decimal
                //
                //                //remove any existing commas
                let textAfterRemovingComma = text.replacingOccurrences(of: ",", with: "")
                //
                //let num = text.replacingOccurrences(of: ".", with: "")
                //
                //                let formattedNum = formatter.string(from: NSNumber(value: Int(num)!))
                //                textField.text = formattedNum!
                
                if NSString(string: UserLogin.shared.walletBal).floatValue < NSString(string: text.replacingOccurrences(of: ",", with: "")).floatValue {
                  
                self.showToast(message: "Insufficient balance".localized, align: .center)
                    self.txtFldAmount.text = ""
                    self.instaCloseButton.isHidden = true
                    self.viewNext.isHidden = true
                    mmkLabel.isHidden = true
                    
                 
             //       return flag == false
                }
                else if NSString(string: UserLogin.shared.walletBal).doubleValue < NSString(string: text.replacingOccurrences(of: ".", with: "")).doubleValue {

                      self.showToast(message: "Insufficient balance".localized, align: .center)
                        self.txtFldAmount.text = ""
                        self.instaCloseButton.isHidden = true
                        self.viewNext.isHidden = true
                    
                       mmkLabel.isHidden = true
                    
               
                   // return flag == false
                }
                
                if textAfterRemovingComma.count > 9 {
                    textField.text = String((textField.text?.dropLast())!)
                }
            }
    
        }
      
        return flag
    }
}



//MARK: - BioMetricLoginDelegate
extension InstaPaySendViewController: BioMetricLoginDelegate {
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful {
            guard let url = PTManagerClass.createUrlComponents(base: PaytoConstants.url.multiCashback.safelyWrappingString()) else { return }
            let params = self.multiCashbackParameters()
            self.modelInstaPay.remarks = txtViewComments.text
            self.modelInstaPay.cashbackApi(url, params: params)
        }
    }
}

//MARK: - InstaPayModelDelegate
extension InstaPaySendViewController: InstaPayModelDelegate {
    func apiResult(data: PTAgentModel, message: String?, statusCode: String?) {
        OKPayment.main.authenticate(screenName: "caskbackPayto", delegate: self)
    }
    
    func apiResultGeneral(message: String?, statusCode: String?) {
        DispatchQueue.main.async {
            guard let vc = UIStoryboard(name: "PayTo", bundle: Bundle.main).instantiateViewController(withIdentifier: "PayToTransactionConfirmationVC") as? PayToTransactionConfirmationVC else { return }
            vc.displayResponse = self.modelInstaPay.transactionArray
            vc.varType = PayToType(type: .payTo)
            vc.instaPay = true
            vc.agentModel = self.modelInstaPay.agentModel
            vc.mainAccountType = self.mainAccountType
            vc.agentTypeNew = self.mainAccountType
            if self.modelInstaPay.transactionArray.count > 0 {
                self.navigationController?.pushViewController(vc, animated: false)
                self.navigationController?.isNavigationBarHidden = false
                self.modelInstaPay.transactionArray.removeAll()
            }
            
        }
    }
    
    func handleCustomError(error: CustomError) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}


