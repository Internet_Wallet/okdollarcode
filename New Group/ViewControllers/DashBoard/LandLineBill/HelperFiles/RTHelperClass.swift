//
//  RTHelperClass.swift
//  ReusabilityProj
//
//  Created by Rahul Tyagi on 10/11/17.
//  Copyright © 2017 Rahul Tyagi. All rights reserved.
//

import UIKit

class RTHelperClass: NSObject {
    
    func formattedDateFromString(dateString: String, withFormat inputFormat: String, toBeConvertedToFormat outputFormat : String) -> String?{
        let inputFormatter = DateFormatter()
        inputFormatter.calendar = Calendar(identifier: .gregorian)
        inputFormatter.dateFormat = inputFormat
        inputFormatter.setLocale()
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.calendar = Calendar(identifier: .gregorian)
            outputFormatter.dateFormat = outputFormat
            outputFormatter.setLocale()
            return outputFormatter.string(from: date)
        }
        return nil
    }
}
