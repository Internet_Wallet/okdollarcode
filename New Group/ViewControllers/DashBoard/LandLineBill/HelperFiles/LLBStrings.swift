//
//  LLBStrings.swift
//  OK
//
//  Created by Rahul Tyagi on 10/19/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class LLBStrings {
    static let aKey = "H0XrsVyTNi4ilYgxDMOxgg=="
    static let sKey = "lczsvF7l1EhblCTGTA57S73yW7rMEL7BMlJEVnI4STJiRUthUVJ1TjExVDBaQlFVZXhXR2NYT3ZJY0RiSU1VMEpRWVZKcUxINHNBUkVqVjFCdDEzbUNhRDBaU3lOVEZvaGNySm01THltT0x0WDZxaUdNYVpWYk9IelZmdFhYNGtuVnVsMk1QRmhDUlgwcnAzc3RyQjJwU0dPalRRYWJhT1VrRkNNRXEzNlUxVEowUklwbEJvcDNyN0NvWFMyOG9GbENEUGxucW5DeEVoeTk2WFNMbHlhd2piM0g5aXJZQjhxdnJIeEJnQjVYbDJ5SkpDczk5cVgwcHpjbVdlbkg2b3dHMUNTeWtScGozQ0tybXQ3NzByWVVrejhhNzc0YTAxLWZmMWQtNGU4ZS1iNzJmLTIyZGNlMDVkYTUyOQ=="
    static let okAccountNum = UserModel.shared.mobileNo
    
    struct LLBServerUrl {
        static let tokenProdUrl = "https://landlineapi.okdollar.org/token"
        static let tokenTestUrl = "http://test.landline.api.okdollar.org/token" //Post
        
        static let historyProdUrl = "https://landlineapi.okdollar.org/users/%@/bills/skip/0/take/20"
        static let historyTestUrl =  "http://test.landline.api.okdollar.org/users/%@/bills/skip/0/take/20" //Get
        
        static let deleteProdUrl = "https://landlineapi.okdollar.org/users/%@/bills/%@"
        static let deleteTestUrl = "http://test.landline.api.okdollar.org/users/%@/bills/%@"
        
        static let billProdUrl = "https://landlineapi.okdollar.org/billdetail/okAccountNumber/%@/customerName/%@/landLineNumber/%@"
        static let billTestUrl = "http://test.landline.api.okdollar.org/billdetail/okAccountNumber/%@/customerName/%@/landLineNumber/%@"
        
        static let payApiProdUrl = "https://landlineapi.okdollar.org/payment/bill"
        static let payApiTestUrl = "http://test.landline.api.okdollar.org/payment/bill"
        
        static let payViewProdUrl = "https://landlineapi.okdollar.org/payment/view"
        static let payViewTestUrl = "http://test.landline.api.okdollar.org/payment/view"
        
        
        static let cityViewProdUrl = "http://69.160.4.151:8002/AdService.svc/GetPostPaidCity"
        static let cityViewTestUrl = "http://69.160.4.151:8002/AdService.svc/GetPostPaidCity"
        
        
        
    }
    
    #if DEBUG
    static let kTokenAPIURL         = serverUrl == .productionUrl ? LLBServerUrl.tokenProdUrl : LLBServerUrl.tokenTestUrl
    static let kLLBHistoryAPIURL    = serverUrl == .productionUrl ? LLBServerUrl.historyProdUrl : LLBServerUrl.historyTestUrl
    static let kDeleteAPIURL        = serverUrl == .productionUrl ? LLBServerUrl.deleteProdUrl : LLBServerUrl.deleteTestUrl
    static let kGetBillAPIURL       = serverUrl == .productionUrl ? LLBServerUrl.billProdUrl : LLBServerUrl.billTestUrl
    static let kPayAPIURL           = serverUrl == .productionUrl ? LLBServerUrl.payApiProdUrl : LLBServerUrl.payApiTestUrl
    static let kPayForViewAPIURL    = serverUrl == .productionUrl ? LLBServerUrl.payViewProdUrl : LLBServerUrl.payViewTestUrl
    static let kCityViewAPIURL      = serverUrl == .productionUrl ? LLBServerUrl.cityViewProdUrl : LLBServerUrl.cityViewTestUrl
    
    #else
    //Live Server
    static let kTokenAPIURL         = LLBServerUrl.tokenProdUrl
    static let kLLBHistoryAPIURL    = LLBServerUrl.historyProdUrl
    static let kDeleteAPIURL        = LLBServerUrl.deleteProdUrl
    static let kGetBillAPIURL       = LLBServerUrl.billProdUrl
    static let kPayAPIURL           = LLBServerUrl.payApiProdUrl
    static let kPayForViewAPIURL    = LLBServerUrl.payViewProdUrl
    static let kCityViewAPIURL      = LLBServerUrl.cityViewProdUrl
    #endif
    
    //URLs
    static let kContentType_Json = "application/json"
    static let kContentType_urlencoded = "application/x-www-form-urlencoded"
    static let kMethod_Get = "GET"
    static let kMethod_Post = "POST"
    static let kMethod_Put = "PUT"
    static let yellowAppColor = UIColor.init(hex: "FDB813")
    struct FileNames {
        static let llbStoryBoardObj = UIStoryboard.init(name: "LandlineBill", bundle: nil)
        static let PMStoryBoardObj = UIStoryboard.init(name: "PMPostpaidMobile", bundle: nil)
        static let PPMStoryBoardObj = UIStoryboard.init(name: "PostpaidMobile", bundle: nil)
    }
    struct LBBHistoryStrings {}
    struct LBBGetBillVCStrings {}
    struct LLBConfirmationVCStrings {}
    struct LLBReciptVCStrings {}
    struct ErrorMessages {
        static let internetError = "No Internet Connection".localized
        static let GenericAlertTitle = "Woops! Something Went Wrong!".localized
        static let TryAgainErrorMsg = "We are unable to get your bill details for the entered number, kindly check your telecom operator/company for the bill.".localized
        static let invalidNumber = "Please enter valid number".localized
        static let urlError = "URL request Error. (Error Code - LLB01)".localized
        static let NetWorkUnavailable = "Please connect to a different Network and Try again.".localized
        static let PaidErrorMessage = "You have Already Paid Landline Bill for the current Month.".localized
        static let PostPaidErrorMessage = "You have already paid up to billing month.".localized
        static let DueDateErrorMessage = "Your payment due date is already over and even if you make payment now, your phone line may be disconnected by your operator! \n After your payment, Do not forget to inform your Operator".localized
        static let PayForViewErrorMessage = "Sorry you have already generated and Viewed your landline bill many times. In order to view the bill details again you have to pay %@ MMK initially from your landline bill.".localized
        
        static let invalideTitle = "Invalid bill number".localized
        static let incorrectMobileNumber = "Incorrect Mobile Number. Please try again".localized
    }
}

class PPMStrings: LLBStrings {
    
}


open class CityModel{
    var Divisionen : String?
    var Divisionmy: String?
    var Divisionun : String?
    var Divisionth: String?
    var Divisionzh : String?
    var DivisionVal: String?
    
    public init(dataVal: NSDictionary){
        if dataVal.count>0{
            self.Divisionen = dataVal.value(forKey: "Divisionen") as? String
            self.Divisionmy = dataVal.value(forKey: "Divisionmy") as? String
            self.Divisionun = dataVal.value(forKey: "Divisionun") as? String
            self.Divisionth = dataVal.value(forKey: "Divisionth") as? String
            self.Divisionzh = dataVal.value(forKey: "Divisionzh") as? String
            self.DivisionVal = dataVal.value(forKey: "DivisionVal") as? String
            
        }
        
    }
    
}
