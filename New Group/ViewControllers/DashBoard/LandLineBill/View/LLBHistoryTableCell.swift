//
//  LLBHistoryTableCell.swift
//  OK
//
//  Created by Rahul Tyagi on 9/28/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit

protocol LLBCellDelegate{
    func didTapPayButton(cell: LLBHistoryTableCell)
    func didTapDeleteButton(cell: LLBHistoryTableCell)
}
class LLBHistoryTableCell: UITableViewCell{
    var delegate : LLBCellDelegate?
    var indexPath = IndexPath()
    @IBOutlet weak var CityDescriptionOutlet: UILabel!{
        didSet
        {
            CityDescriptionOutlet.text = CityDescriptionOutlet.text?.localized
        }
    }
    @IBOutlet weak var LandLineNumberDescriptionOutlet: UILabel!{
        didSet
        {
            LandLineNumberDescriptionOutlet.text = LandLineNumberDescriptionOutlet.text?.localized
        }
    }
    @IBOutlet weak var CityValueOutlet: UILabel!{
        didSet
        {
            CityValueOutlet.text = CityValueOutlet.text?.localized
        }
    }
    @IBOutlet weak var LandLineNumberValueOutlet: UILabel!{
        didSet
        {
            LandLineNumberValueOutlet.text = LandLineNumberValueOutlet.text?.localized
        }
    }
    @IBOutlet weak var DeleteBtnOutlet: UIButton!{
        didSet
        {
            DeleteBtnOutlet.setTitle(DeleteBtnOutlet.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var PayBtnOutlet: UIButton!{
        didSet
        {
            PayBtnOutlet.setTitle(PayBtnOutlet.titleLabel?.text?.localized, for: .normal)
        }
    }
    override func awakeFromNib(){
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @IBAction func DeleteBtnAction(_ sender: Any){
        delegate?.didTapDeleteButton(cell: self)
    }
    @IBAction func PayBtnAction(_ sender: Any){
        delegate?.didTapPayButton(cell: self)
    }
    func wrapData(obj: HistoryVCDataModel, index: IndexPath){
        self.CityValueOutlet.text = obj.SourceDescription.localized
        self.LandLineNumberValueOutlet.text = "0".appending(obj.PhoneNumber).localized
        self.indexPath = index
    }
}
