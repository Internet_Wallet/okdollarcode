//
//  LLBGetBillTableCell.swift
//  OK
//
//  Created by Rahul Tyagi on 10/17/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol LLBGetBillTableCellDelegate {
    func passData(cell: LLBGetBillTableCell)
}
class LLBGetBillTableCell: UITableViewCell{
    var placeholderArray : [String] = []
    var imageArray : [UIImage] = []
    var customIndexPath = IndexPath()
    var delegate : LLBGetBillTableCellDelegate?
    var LLBparentVC: LLBGetBillVC! = nil
    var holdBillNumber = String()
    var numStr = ""
    @IBOutlet weak var imageViewOutlet: UIImageView!
    @IBOutlet weak var textFieldOutlet: RestrictedCursorMovementWithFloat!{
        didSet{
            textFieldOutlet.font = UIFont(name: appFont, size: 17.0)
            textFieldOutlet.text = textFieldOutlet.text?.localized
        }
    }
    @IBOutlet weak var btn: UIButton!{
        didSet{
            btn.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
            btn.setTitle(btn.titleLabel?.text, for: .normal)
            btn.isSelected = false
        }
    }
    
    @IBOutlet var closeButton: UIButton!
    {
        didSet{
            closeButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
            closeButton.setTitle(closeButton.titleLabel?.text?.localized, for: .normal)
            closeButton.isSelected = false
        }
    }
    
    
    override func awakeFromNib(){
        super.awakeFromNib()
        placeholderArray = ["Select City".localized, "Landline Number".localized, "Bill Month".localized, "Amount".localized, "Bill Status".localized, "Service Fee".localized]
        
        imageArray = [#imageLiteral(resourceName: "cityImg"),#imageLiteral(resourceName: "landlineImg"), #imageLiteral(resourceName: "bill_monthImg"),#imageLiteral(resourceName: "amount"),#imageLiteral(resourceName: "status"),#imageLiteral(resourceName: "add_money")]
        textFieldOutlet.delegate = self
        
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setCellData(index : IndexPath, text: String, newpayment:String, showCity: Bool){
        customIndexPath = index
        if showCity{
          placeholderArray = ["City".localized, "Landline Number".localized, "Bill Month".localized, "Amount".localized, "Bill Status".localized, "Service Fee".localized]
        }else{
            placeholderArray = ["Select City".localized, "Landline Number".localized, "Bill Month".localized, "Amount".localized, "Bill Status".localized, "Service Fee".localized]
        }
//
        if placeholderArray.count > 0 {
            if placeholderArray.indices.contains(index.row){
                self.textFieldOutlet.placeholder = placeholderArray[index.row].localized
                self.imageViewOutlet.image = imageArray[index.row]
                self.textFieldOutlet.text = text
                self.textFieldOutlet.title = placeholderArray[index.row].localized
                self.btn.tag = index.row
              //  self.closeButton.tag = index.row
                
                if newpayment == "NEW"{
                     configureCell(withIndex: index)
                }
                else{
                    normalconfigureCell(withIndex: index)
                }
                
                }
            }
       
       
    }
    
    func setCellData(index : IndexPath, text: String, billDataModelObject: HistoryVCDataModel, emptystr:String, showCity: Bool){
        if showCity{
          placeholderArray = ["City".localized, "Landline Number".localized, "Bill Month".localized, "Amount".localized, "Bill Status".localized, "Service Fee".localized]
        }else{
            placeholderArray = ["Select City".localized, "Landline Number".localized, "Bill Month".localized, "Amount".localized, "Bill Status".localized, "Service Fee".localized]
        }
        if isObjectNotNil(object: billDataModelObject){
            println_debug(billDataModelObject)
            println_debug(index.row)
            customIndexPath = index
            self.textFieldOutlet.placeholder = placeholderArray[index.row].localized
            self.imageViewOutlet.image = imageArray[index.row]
            switch(index.row){
            case 0 : self.textFieldOutlet.text = billDataModelObject.SourceDescription.localized
                break
            case 1 : self.textFieldOutlet.text = "0".appending(billDataModelObject.PhoneNumber).localized
            self.holdBillNumber = "0".appending(billDataModelObject.PhoneNumber).localized
                break
            default: break
            }
            self.textFieldOutlet.title = placeholderArray[index.row].localized
            self.btn.tag = index.row
            self.closeButton.tag = index.row
            if emptystr == "empty"{
                 configureCell(withIndex: index)
            }
            else{
                normalconfigureCell(withIndex: index)
            }
            
            
        }
        else{
            customIndexPath = index
            self.textFieldOutlet.placeholder = placeholderArray[index.row].localized
            self.imageViewOutlet.image = imageArray[index.row]
            self.textFieldOutlet.text = text
            self.textFieldOutlet.title = placeholderArray[index.row].localized
            self.btn.tag = index.row
           //  self.closeButton.tag = index.row
            configureCell(withIndex: index)
        }
    }
    
    
    
    func setCellDataWithoutObject(index : IndexPath, text: String, emptystr:String, showCity: Bool){
        if showCity{
          placeholderArray = ["City".localized, "Landline Number".localized, "Bill Month".localized, "Amount".localized, "Bill Status".localized, "Service Fee".localized]
        }else{
            placeholderArray = ["Select City".localized, "Landline Number".localized, "Bill Month".localized, "Amount".localized, "Bill Status".localized, "Service Fee".localized]
        }
            customIndexPath = index
            self.textFieldOutlet.placeholder = placeholderArray[index.row].localized
            self.imageViewOutlet.image = imageArray[index.row]
            self.textFieldOutlet.text = text
            self.textFieldOutlet.title = placeholderArray[index.row].localized
            self.btn.tag = index.row
            self.closeButton.tag = index.row
           if emptystr == "empty"{
                 configureCell(withIndex: index)
            }
            else{
                normalconfigureCell(withIndex: index)
            }
    }
    
    func normalconfigureCell(withIndex index : IndexPath){
          switch index.row{
          case 0:  self.textFieldOutlet.isEnabled = false
         
          self.btn.isHidden = true
              break
          case 1:  self.textFieldOutlet.isEnabled = false
         
          self.btn.isHidden = true
          self.closeButton.isHidden = true
              break
          case 2: self.textFieldOutlet.isEnabled = false; break
          case 3: self.textFieldOutlet.isEnabled = false; break
          case 4: self.textFieldOutlet.isEnabled = false; break
          case 5: self.textFieldOutlet.isEnabled = false; break
          default: self.textFieldOutlet.isEnabled = false; break
          }
          if index.row == 0{ }
          else{}
      }
    
    func configureCell(withIndex index : IndexPath){
        switch index.row{
        case 0:  self.textFieldOutlet.isEnabled = false
        self.btn.setImage(UIImage(named: "downarrow_blue"), for: .normal)
        self.btn.setImage(UIImage(named: "uparrow_blue"), for: .selected)
        
        self.btn.addTarget(self, action: #selector(callArrow(sender:)), for: .touchUpInside)
        self.btn.frame = CGRect(origin: CGPoint.zero, size: CGSize.init(width: 20.0, height: 20.0))
        self.btn.isHidden = false
            break
        case 1:  self.textFieldOutlet.isEnabled = true
        self.btn.setImage(UIImage(named: "contact_bill"), for: .normal)
        self.btn.addTarget(self, action: #selector(callContactPicker), for: .touchUpInside)
        self.textFieldOutlet.keyboardType = .numberPad
        self.btn.isHidden = false
        self.closeButton.addTarget(self, action: #selector(close), for: .touchUpInside)
        self.closeButton.isHidden = true
            break
        case 2: self.textFieldOutlet.isEnabled = false; break
        case 3: self.textFieldOutlet.isEnabled = false; break
        case 4: self.textFieldOutlet.isEnabled = false; break
        case 5: self.textFieldOutlet.isEnabled = false; break
        default: self.textFieldOutlet.isEnabled = false; break
        }
        if index.row == 0{ }
        else{}
    }
    
    @objc func callContactPicker(){
//        self.LLBparentVC.showParent(self.btn)
        self.LLBparentVC.contactAccess(self.btn)
    }
    
    @objc func close(_ sender: UITextField){
    
             self.textFieldShouldClear(sender)
       
    }
    
    @objc func callArrow(sender: UIButton){
        
        
         
        if LLBparentVC.citytableviewContainerOutlet.isHidden {
            sender.setImage(UIImage(named: "uparrow_blue"), for: .normal)
        }else {
            sender.setImage(UIImage(named: "downarrow_blue"), for: .normal)
        }
        sender.tag = 100
        
        
        
        
        self.LLBparentVC.showCity(sender: sender)
    }
    
    @IBAction func textFieldDidChange(_ sender: UITextField){
        self.holdBillNumber = sender.text!.localized
        delegate?.passData(cell: self)
    }
    
    func isObjectNotNil(object:HistoryVCDataModel!) -> Bool{
        if let _:HistoryVCDataModel = object{
            return true
        }
        return false
    }
    
}
extension LLBGetBillTableCell: UITextFieldDelegate {
    
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if let text = textFieldOutlet.text, text.count < 2 {
                    
            self.closeButton.isHidden = true
                    
        }
        
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        if let cityBoolValue = userDef.value(forKey: "citychoose") as? Bool{
            if cityBoolValue{
                
                
                if (textField == textFieldOutlet || (textField.text?.count)! == 9) {
                    self.LLBparentVC.rowCountArray.removeAll()
                    self.LLBparentVC.textFieldDataArray.removeAll()
                    self.LLBparentVC.rowCountArray.append("")
                    self.LLBparentVC.rowCountArray.append("")
                    
                    
                    if let currentStr = appDel.getSelectedLanguage() as? String{
                        
                        if currentStr == "uni"{
                            self.LLBparentVC.textFieldDataArray = [self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].Divisionun!, "0\(self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].DivisionVal!)", "", "", "", ""]
                            self.LLBparentVC.textFieldDataArray.append(self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].Divisionen!)
                            
                            
                        }
                        else if currentStr == "my"{
                            self.LLBparentVC.textFieldDataArray = [self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].Divisionmy!, "0\(self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].DivisionVal!)", "", "", "", ""]
                            self.LLBparentVC.textFieldDataArray.append(self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].Divisionen!)
                            
                        }
                        else{
                            self.LLBparentVC.textFieldDataArray = [self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].Divisionen!, "0\(self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].DivisionVal!)", "", "", "", ""]
                            self.LLBparentVC.textFieldDataArray.append(self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].Divisionen!)
                            
                        }
                        
                    }
                    
                    self.LLBparentVC.textFieldDataArray.append(self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].DivisionVal!)
                    
                    
                    self.LLBparentVC.getBillBtn.isHidden = true
                    self.LLBparentVC.getBillBtn.setTitle("Get Bill".localized, for: .normal)
                    self.LLBparentVC.tableviewOutlet.reloadData()
                    DispatchQueue.main.async {
                        self.LLBparentVC.tableviewOutlet.tableFooterView = nil
                        self.LLBparentVC.tableviewOutlet.tableFooterView = UIView()
                        
                    }
                    
                    return false
                }
            }
            else{
                self.LLBparentVC.rowCountArray.removeAll()
                self.LLBparentVC.textFieldDataArray.removeAll()
                self.LLBparentVC.rowCountArray.append("")
                self.LLBparentVC.rowCountArray.append("")
                
                
                
                
                if let currentStr = appDel.getSelectedLanguage() as? String{
                    
                    if currentStr == "uni"{
                        self.LLBparentVC.textFieldDataArray = [self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].Divisionun!, "0\(self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].DivisionVal!)", "", "", "", ""]
                        self.LLBparentVC.textFieldDataArray.append(self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].Divisionun!)
                        self.LLBparentVC.textFieldDataArray.append("0\(self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].DivisionVal!)")
                        
                    }
                    else if currentStr == "my"{
                        self.LLBparentVC.textFieldDataArray = [self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].Divisionmy!, "0\(self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].DivisionVal!)", "", "", "", ""]
                        self.LLBparentVC.textFieldDataArray.append(self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].Divisionmy!)
                        self.LLBparentVC.textFieldDataArray.append("0\(self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].DivisionVal!)")
                    }
                    else{
                        self.LLBparentVC.textFieldDataArray = [self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].Divisionen!, "0\(self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].DivisionVal!)", "", "", "", ""]
                        self.LLBparentVC.textFieldDataArray.append(self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].Divisionen!)
                        self.LLBparentVC.textFieldDataArray.append("0\(self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].DivisionVal!)")
                    }
                    
                }
                
                self.LLBparentVC.getBillBtn.isHidden = true
                self.LLBparentVC.getBillBtn.setTitle("Get Bill".localized, for: .normal)
                self.LLBparentVC.tableviewOutlet.reloadData()
                DispatchQueue.main.async {
                    self.LLBparentVC.tableviewOutlet.tableFooterView = nil
                    self.LLBparentVC.tableviewOutlet.tableFooterView = UIView()
                    self.LLBparentVC.adImage.isHidden = false
                    self.LLBparentVC.loadAdvertisement()
                }
                return false
            }
        }
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text: String? = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        self.holdBillNumber = text!.localized + string
        println_debug("typing string :: \(String(describing: text))")
        
        if textField == textFieldOutlet {
           
            
           
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            
            let Txvalue = textField.text ?? self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].DivisionVal!
          
            let textCount  = Txvalue.count
            if textCount > 1 {
                self.closeButton.isHidden = false
            } else {
                self.closeButton.isHidden = true
            }
            
            
            if Txvalue.hasPrefix("01") {
                
                if (isBackSpace == -92) && text?.length == 7 {
                    
                    self.LLBparentVC.rowCountArray.removeAll()
                    self.LLBparentVC.rowCountArray.append("")
                    self.LLBparentVC.rowCountArray.append("")
                    self.LLBparentVC.view.endEditing(true)
                    
                    if let currentStr = appDel.getSelectedLanguage() as? String{
                        
                        if currentStr == "uni"{
                            self.LLBparentVC.textFieldDataArray = [self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].Divisionun!, self.holdBillNumber.localized, "", "", "", ""]
                            
                        }
                        else if currentStr == "my"{
                            self.LLBparentVC.textFieldDataArray = [self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].Divisionmy!, self.holdBillNumber.localized, "", "", "", ""]
                        }
                        else{
                            self.LLBparentVC.textFieldDataArray = [self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].Divisionen!, self.holdBillNumber.localized, "", "", "", ""]
                        }
                        
                    }
                    self.LLBparentVC.tableviewOutlet.reloadData()
                    self.LLBparentVC.getBillBtn?.isHidden = true
                    self.LLBparentVC.getBillBtn.setTitle("Get Bill".localized, for: .normal)
                    self.LLBparentVC.footerViewLabel?.text = ""
                    DispatchQueue.main.async{
                        let cell = self.LLBparentVC.tableviewOutlet.cellForRow(at: IndexPath(row: 1, section: 0)) as? LLBGetBillTableCell
                        //cell?.textFieldOutlet.becomeFirstResponder()
                        self.holdBillNumber = text ?? self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].DivisionVal!
                        cell?.textFieldOutlet.text = self.holdBillNumber.localized
                    }
                }
            }
            else {
                
                if (isBackSpace == -92) && text?.length == 7 {
                    self.LLBparentVC.rowCountArray.removeAll()
                    self.LLBparentVC.rowCountArray.append("")
                    self.LLBparentVC.rowCountArray.append("")
                    self.LLBparentVC.view.endEditing(true)
                    
                    
                    if let currentStr = appDel.getSelectedLanguage() as? String{
                        
                        if currentStr == "uni"{
                            self.LLBparentVC.textFieldDataArray = [self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].Divisionun!, self.holdBillNumber.localized, "", "", "", ""]
                            
                        }
                        else if currentStr == "my"{
                            self.LLBparentVC.textFieldDataArray = [self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].Divisionmy!, self.holdBillNumber.localized, "", "", "", ""]
                        }
                        else{
                            self.LLBparentVC.textFieldDataArray = [self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].Divisionen!, self.holdBillNumber.localized, "", "", "", ""]
                        }
                        
                    }
                    
                    self.LLBparentVC.tableviewOutlet.reloadData()
                    DispatchQueue.main.async{
                        let cell = self.LLBparentVC.tableviewOutlet.cellForRow(at: IndexPath(row: 1, section: 0)) as? LLBGetBillTableCell
                        cell?.textFieldOutlet.becomeFirstResponder()
                        self.holdBillNumber = text ?? self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].DivisionVal!
                        cell?.textFieldOutlet.text = self.holdBillNumber.localized
                    }
                }
                self.LLBparentVC.getBillBtn.isHidden = true
                self.LLBparentVC.getBillBtn.setTitle("Get Bill".localized, for: .normal)
                DispatchQueue.main.async {
                    self.LLBparentVC.tableviewOutlet.tableFooterView = nil
                    self.LLBparentVC.tableviewOutlet.tableFooterView = UIView()
                }
                
            }
            
            
            let value = textField.text ?? self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].DivisionVal!
            
            if value.hasPrefix("01") || value.hasPrefix("02"){
                println_debug("yan:::vi")
                if (text?.length ?? 0 ) >=  4 {
                    self.LLBparentVC.adImage.isHidden = true
                }
                else {
                    self.LLBparentVC.adImage.isHidden = false
                }
                
                if (text?.length ?? 0 ) >=  7{
                    self.LLBparentVC.getBillBtn.isHidden = false
                }
                else{
                    self.LLBparentVC.getBillBtn.isHidden = true
                    
                }
                
                if text?.length == 1 {
                    //textField.text = self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].DivisionVal!
                    return false
                }
                    
                    
                else if text?.length == 12 {
                    return false
                }
                
            }
                
                
            else {
                
                
                if (text?.length ?? 0 ) >=  4 {
                    self.LLBparentVC.adImage.isHidden = true
                }
                else {
                    self.LLBparentVC.adImage.isHidden = false
                }
                
                if (text?.length ?? 0 ) >=  7{
                    self.LLBparentVC.getBillBtn.isHidden = false
                }
                else{
                    self.LLBparentVC.getBillBtn.isHidden = true
                    
                }
                
                if text?.length == 2 {
                    //textField.text = self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].DivisionVal!
                    return false
                }
                    
                    
                else if text?.length == 12 {
                    return false
                }
                
                
                //                println_debug("mandalay:::vi")
                //                if (text?.length ?? 0 ) >=  4 {
                //                    self.LLBparentVC.adImage.isHidden = true
                //                }
                //                else {
                //                    self.LLBparentVC.adImage.isHidden = false
                //                }
                //
                //                if (text?.length ?? 0) >= 7 {
                //                    self.LLBparentVC.getBillBtn.isHidden = false
                //                }
                //                else {
                //                    self.LLBparentVC.getBillBtn.isHidden = true
                //
                //                }
                //                if text?.length == 1 {
                //                    textField.text = self.LLBparentVC.cityModelObj[self.LLBparentVC.selectedIndexPath.row].DivisionVal!
                //                    return false
                //                }
                //
                //                else if text?.length == 12 {
                //                    return false
                //                }
                
            }
        }
        return true
    }
    
}


class LLBGetBillCityTableCell: UITableViewCell{
    
    @IBOutlet var lblCityName: UILabel!{
        didSet{
            lblCityName.font = UIFont(name: appFont, size: appFontSize)
            lblCityName.text = lblCityName.text?.localized
        }
    }
    
    @IBOutlet var lblCityNumber: UILabel!{
        didSet{
            lblCityNumber.font = UIFont(name: appFont, size: appFontSize)
            lblCityNumber.text = lblCityNumber.text?.localized
        }
    }
    
}
