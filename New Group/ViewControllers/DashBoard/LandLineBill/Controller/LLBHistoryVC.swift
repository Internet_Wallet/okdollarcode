//
//  LLBHistoryVC.swift
//  OK
//
//  Created by Rahul Tyagi on 10/17/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class LLBHistoryVC: OKBaseController {
    @IBOutlet var searchBarOutlet: UISearchBar!
    @IBOutlet var searchBarButtonOutlet: UIBarButtonItem!
    @IBOutlet weak var tableviewOutlet: UITableView!
    @IBOutlet weak var navigationItemOutlet: UINavigationItem!
    @IBOutlet var lblrecord: UILabel!{
        didSet
        {
            lblrecord.text = lblrecord.text?.localized
        }
    }
    var AddBtn : UIButton?
    var numberToDelete : String = "" //To Delete object from Model
    var NoResultsLabel : UILabel? = nil
    var HistoryDataModelObj = [HistoryVCDataModel]()
    var searchResults = [HistoryVCDataModel]()
    var customerID: String?
    var number: String?
    
    override func viewDidLoad(){
        super.viewDidLoad()
        //self.title = "Landline Bill".localized
        //self.lblrecord.isHidden = true
        self.setTitleAttribute()
        self.helpSupportNavigationEnum = .Landline_Bill
        searchBarOutlet.delegate = self
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.filterArrayObject(objectArray: HistoryDataModelObj)
    }
    
    func setTitleAttribute() {
        self.title = "Landline Bill".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.navigationController?.isNavigationBarHidden = false
        searchResults.removeAll()
        tableviewOutlet.reloadData()
        setupInitialView()
        timerProgess()
        
    }
    func responseDataApiCall(){
        
        progressViewObj.showProgressView()
        
        DispatchQueue.main.async {
            self.setTitleAttribute()
            self.timerProgess()
        }
        let LLBHistoryStatusObj = LLBCheckHistoryStatus()
        LLBHistoryStatusObj.requestAPI {(responseStatus, ErrorMessage, historyDataModelRef) in
            DispatchQueue.main.async{ progressViewObj.removeProgressView()}
            switch responseStatus{
            case .showGetBill:  break
            case .showHistory:  DispatchQueue.main.async{self.HistoryDataModelObj = historyDataModelRef!
                self.tableviewOutlet?.reloadData()}
                break
            case .showAlert:
                DispatchQueue.main.async{
                    alertViewObj.wrapAlert(title: nil, body: ErrorMessage!, img:  #imageLiteral(resourceName: "alert-icon"))
                    alertViewObj.addAction(title: "OK".localized, style: .target , action:{})
                    alertViewObj.showAlert(controller: self)
                }
            }
        }
        
    }
    
    func timerProgess() {
        progressViewObj.showProgressView()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + (5.0)) {
            
            progressViewObj.removeProgressView()
            
        }
    }
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension LLBHistoryVC:LLBGetBillVCDelegate{
    func CallReloadAPI() {
        self.responseDataApiCall()
    }
}
extension LLBHistoryVC: UITableViewDelegate, UITableViewDataSource, LLBCellDelegate{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell : LLBHistoryTableCell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! LLBHistoryTableCell
        if searchResults.count != 0{
            cell.wrapData(obj: searchResults[indexPath.row], index: indexPath)
        }
        else{
            cell.wrapData(obj: HistoryDataModelObj[indexPath.row], index: indexPath)
        }
        cell.delegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 170
    }
    //MARK:- Cell Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if searchResults.count != 0 {
            // println_debug((searchResults?.count)!)
            return (searchResults.count)
        }
        else{
            println_debug((HistoryDataModelObj.count))
            return (HistoryDataModelObj.count)
        }
    }
}
extension LLBHistoryVC{
    
    func setupInitialView(){
        AddBtn = UIButton.init(frame: CGRect(x: self.view.frame.width - 75, y: self.view.frame.height - 120 , width: 55    , height: 55))
        AddBtn?.setImage(#imageLiteral(resourceName: "AddBtnImg"), for: .normal)
        AddBtn?.addTarget(self, action: #selector(self.AddbtnAction), for: .touchUpInside)
        AddBtn?.backgroundColor = .white
        AddBtn?.layer.cornerRadius =  (AddBtn!.bounds.size.width) * 0.5
        AddBtn?.clipsToBounds = true
        self.view.addSubview(AddBtn!)
        AddBtn?.layer.masksToBounds = false
        AddBtn?.layer.shadowColor = UIColor.black.cgColor
        AddBtn?.layer.shadowOffset = CGSize(width: 0.2, height: 3.0)
        AddBtn?.layer.shadowOpacity = 0.25;
        self.view.bringSubviewToFront(AddBtn!)
        //AddBtn?.isHidden = true
        NoResultsLabel = UILabel.init(frame: CGRect(x: 0, y: self.view.frame.height/2 - 150, width: self.view.frame.width, height: 30))
        NoResultsLabel?.text =  "No Results Found".localized    //appDel.getlocaLizationLanguage("No Results Found!")
        NoResultsLabel?.textColor = .darkGray
        NoResultsLabel?.textAlignment = .center
        NoResultsLabel?.numberOfLines = 2
        self.view.addSubview(NoResultsLabel!)
        
        NoResultsLabel?.textAlignment = .center
        NoResultsLabel?.font = UIFont.init(name: appFont, size: 22)
        NoResultsLabel?.isHidden = true
        customizeSearchBar()
    }
    func filterArrayObject(objectArray: [HistoryVCDataModel]){
        var demoArry = [HistoryVCDataModel]()
        println_debug(objectArray.count)
        println_debug(HistoryDataModelObj.count as Any)
        println_debug(HistoryDataModelObj as Any)
        var i = 0
        while i < (objectArray.count){
            println_debug(i)
            //println_debug(HistoryDataModelObj![i].PhoneNumber)
            if objectArray[i].PhoneNumber.hasPrefix("9"){
                //HistoryDataModelObj?.remove(at: i)
            }
            else{
                println_debug(objectArray[i].PhoneNumber)
                demoArry.append(objectArray[i])
            }
            i = i + 1
        }
        
        HistoryDataModelObj.removeAll()
        HistoryDataModelObj = demoArry
        //println_debug(HistoryDataModelObj)
    }
    
    @objc func AddbtnAction(){
        let GetBillObj = LLBStrings.FileNames.llbStoryBoardObj.instantiateViewController(withIdentifier: "LLBGetBillVCSID") as? LLBGetBillVC
        GetBillObj?.LLBVCDelegate = self
        GetBillObj?.controllerState = .presented
        GetBillObj?.newlandlinepayment = "NEW"
        let navController = LLBNavController.init(rootViewController: GetBillObj!)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: nil)
    }
    
    
    func didTapPayButton(cell: LLBHistoryTableCell){
        println_debug(HistoryDataModelObj[cell.indexPath.row])
        self.clearSearchBar()
        let GetBillVCObj = LLBStrings.FileNames.llbStoryBoardObj.instantiateViewController(withIdentifier: "LLBGetBillVCSID") as! LLBGetBillVC
        GetBillVCObj.controllerState = .pushed
        GetBillVCObj.isComingFromChangingCity = true
        if searchResults.count != 0{
            GetBillVCObj.billDataModelObj = searchResults[cell.indexPath.row]
        }
        else {
            GetBillVCObj.billDataModelObj = HistoryDataModelObj[cell.indexPath.row]
        }
        self.navigationController?.pushViewController(GetBillVCObj, animated: true)
    }
    func didTapDeleteButton(cell: LLBHistoryTableCell){
        DispatchQueue.main.async {
            println_debug("Delete Button Tapped : \n \(self.HistoryDataModelObj[cell.indexPath.row])")
            alertViewObj.wrapAlert(title: nil, body: "Do you want to Delete this record?".localized, img: #imageLiteral(resourceName: "landlineImg"))
            alertViewObj.addAction(title: "NO".localized, style: .cancel) {}
            alertViewObj.addAction(title: "YES".localized, style: .target){
                self.customerID = (self.HistoryDataModelObj[cell.indexPath.row]).customerID
                self.number = (self.HistoryDataModelObj[cell.indexPath.row]).PhoneNumber
                self.numberToDelete = self.number!
                self.LLBdeleteAPIRequest(customerID: self.customerID!, PhoneNumber: self.number!)
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    @IBAction func backBtnAction(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension LLBHistoryVC: RTJsonParserDelegate{
    func LLBdeleteAPIRequest(customerID : String, PhoneNumber : String){
        progressViewObj.showProgressView()
        println_debug(" \nRequesting Token From Server")
        let LLBHistoryStatusObj = LLBCheckHistoryStatus()
        LLBHistoryStatusObj.getTokenFromServer { (isSuccess, authString, APIError) in
            if isSuccess == true{
                println_debug("token Received, Requesting Delete API")
                let urlString = String.init(format: LLBStrings.kDeleteAPIURL, customerID,PhoneNumber)
                let DeleteURL = URL.init(string: urlString)
                let jsonParserObj = RTJsonParser()
                jsonParserObj.delegate = self
                jsonParserObj.ParseMyJson(url: DeleteURL!, param: nil, httpMethod: LLBStrings.kMethod_Put, contentType: LLBStrings.kContentType_Json, mScreen: "LLBDeleteAPI", authStr: authString as? String)
            }
            else{
                println_debug("token Not Received")
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    
                    self.LLBdeleteAPIRequest(customerID: self.customerID!, PhoneNumber: self.number!)
                    
                    //                    alertViewObj.wrapAlert(title: nil, body: "Please try again".localized, img: #imageLiteral(resourceName: "alert-icon"))
                    //                    alertViewObj.addAction(title: "OK".localized, style: .target){
                    //
                    //                    }
                    //                    alertViewObj.showAlert(controller: self)
                    
                }
            }
        }
    }
    func ResponseFromServer(withSuccess: Bool, json: AnyObject?, screen: String, Error: String?){
        //progressViewObj.removeProgressView()
        if screen == "LLBDeleteAPI" && withSuccess == true{
            var i = 0
            while i < (HistoryDataModelObj.count) {
                if (HistoryDataModelObj[i]).PhoneNumber == numberToDelete {
                    HistoryDataModelObj.remove(at: i)
                }
                i = i + 1
            }
            DispatchQueue.main.async {
                self.responseDataApiCall()
                self.tableviewOutlet.reloadData()
                
            }
        }
    }
}

extension LLBHistoryVC: UISearchControllerDelegate,UISearchBarDelegate{
    
    func customizeSearchBar(){
        
        searchBarOutlet.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
        searchBarOutlet.showsCancelButton = true
        searchBarOutlet.delegate = self
        searchBarOutlet.tintColor = UIColor.white
        searchBarOutlet.placeholder = "Search".localized
        if let searchTextField = searchBarOutlet.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 16) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
              
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                searchTextField.keyboardType = .numberPad
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
            }
        }
        
        if let uiButton = searchBarOutlet.value(forKey: "cancelButton") as? UIButton {
            uiButton.setTitle("Cancel".localized, for:.normal)
            if let navFont = UIFont(name: appFont, size: 18) {
                uiButton.titleLabel?.font = navFont
            }
        }
        
        let view = self.searchBarOutlet.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = UIColor.gray
            }
        }
        
        //        searchBarOutlet.isHidden = true
        //        self.searchBarOutlet.returnKeyType = .done
        //        self.searchBarOutlet.setValue(UIColor.lightText, forKeyPath: "_searchField._placeholderLabel.textColor")
        //        let searchBarTextField : UITextField = self.searchBarOutlet.value(forKey: "_searchField") as! UITextField
        //        searchBarTextField.textColor = UIColor.black
        //        let leftImageView : UIImageView = searchBarTextField.leftView as! UIImageView
        //        leftImageView.tintColor = UIColor.lightText
        //        leftImageView.image? = (leftImageView.image?.withRenderingMode(.alwaysTemplate))!
        //        let clearButton : UIButton = searchBarTextField.value(forKey: "clearButton") as! UIButton
        //        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        //        clearButton.tintColor = UIColor.lightText
        
        //        searchBarOutlet.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
        //        searchBarOutlet.showsCancelButton = true
        //        searchBarOutlet.delegate = self
        //        searchBarOutlet.tintColor = UIColor.white
        //        searchBarOutlet.placeholder = "Search".localized
        //        if let searchTextField = searchBarOutlet.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: "Zawgyi-One", size: 18) {
        //            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
        //                searchTextField.font = myFont
        //                searchTextField.font = myFont
        //                searchTextField.keyboardType = .asciiCapable
        //            }
        //        }
        //        let view = self.searchBarOutlet.subviews[0] as UIView
        //        let subViewsArray = view.subviews
        //        for subView: UIView in subViewsArray {
        //            if subView.isKind(of: UITextField.self) {
        //                subView.tintColor = ConstantsColor.navigationHeaderTransaction
        //            }
        //        }
        //        let searchBarButton = UIBarButtonItem(customView:searchBarOutlet)
        //        let backButton = UIButton(type: .custom)
        //        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        //        backButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        //        backButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        //        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        //        self.navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: backButton), searchBarButton]
        //        searchBarOutlet.becomeFirstResponder()
        //
        //        let TapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.HandleTapGesture(_:)))
        //        TapGesture.cancelsTouchesInView = false
        //        self.view.addGestureRecognizer(TapGesture)
        
    }
    @IBAction func searchBtnAction(_ sender: Any){
        println_debug("Search Btn was clicked")
        self.navigationItemOutlet.titleView = searchBarOutlet
        self.navigationItemOutlet.title = "".localized
        self.navigationItemOutlet.rightBarButtonItem?.isEnabled = false
        self.navigationItemOutlet.rightBarButtonItem?.tintColor = UIColor.clear;
        searchBarOutlet.isHidden = false
        searchBarOutlet.becomeFirstResponder()
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        
        println_debug("call in search bar text did began editing")
        
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        self.FilterContentForSearchText(searchText: searchText)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        self.view.endEditing(true)
    }
    func clearSearchBar(){
        self.view.endEditing(true)
        self.searchBarOutlet.text = ""
        self.navigationItemOutlet.titleView = nil
        self.navigationItemOutlet.title = "Landline Bill".localized
        //      appDelegate.getlocaLizationLanguage("LandLine Bill")
        self.navigationItemOutlet.rightBarButtonItem?.isEnabled = true
        self.navigationItemOutlet.rightBarButtonItem?.tintColor = UIColor.white;
        searchBarOutlet.isHidden = true
        
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        self.NoResultsLabel?.isHidden = true
        self.tableviewOutlet.isHidden = false
        self.searchResults.removeAll()
        self.AddBtn?.isHidden = false
        self.clearSearchBar()
        self.tableviewOutlet.reloadData()
    }
    @objc func HandleTapGesture(_:UIGestureRecognizer){
        self.view.endEditing(true)
        if searchBarOutlet.text?.localized == ""{
            searchBarOutlet.isHidden = true
            self.navigationItemOutlet.title = "Landline Bill".localized //appDelegate.getlocaLizationLanguage("LandLine Bill")
            self.navigationItemOutlet.rightBarButtonItem?.isEnabled = true
            self.navigationItemOutlet.rightBarButtonItem?.tintColor = UIColor.white;
        }
    }
    func FilterContentForSearchText(searchText : String){
        println_debug(searchText)
        searchResults.removeAll()
        if searchText.isEmpty && searchText == ""{
            tableviewOutlet.isHidden = false
            NoResultsLabel?.isHidden = true
            tableviewOutlet.reloadData()
            AddBtn?.isHidden = false
        }
        else {
            AddBtn?.isHidden = true
            tableviewOutlet.isHidden = false
            var demoArry = [HistoryVCDataModel]()
            var i = 0
            while i < (HistoryDataModelObj.count) {
                let modelObj = HistoryDataModelObj[i]
                let landlineNumber = "0" + modelObj.PhoneNumber
                if landlineNumber.containsIgnoringCase(find: searchText) {
                    demoArry.append(modelObj)
                }
                i = i + 1
            }
            if demoArry.count > 0 {
                searchResults = demoArry
                self.tableviewOutlet.isHidden = false
                tableviewOutlet.reloadData()
            } else {
                self.tableviewOutlet.isHidden = true
                NoResultsLabel?.isHidden = false
            }
        }
    }
}
