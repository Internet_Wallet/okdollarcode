//
//  LLBGetBillVC.swift
//  OK
//
//  Created by Rahul Tyagi on 10/17/17.
//  Copyright © 2017 Cgm. All rights reserved.
//





import UIKit


protocol LLBGetBillVCDelegate: class {
    func CallReloadAPI()
}
class LLBGetBillVC: OKBaseController, UIGestureRecognizerDelegate {
    enum ControllerStateType{
        case presented, pushed, notDefined
    }
    
    enum StatusOptions{
        case Paid, Unpaid, PayForView, PaidWithOkDollar
    }
    
    var tempBillDetails : Dictionary<String,Any>?
    var favoriteTouple: (areaCode: String,billNo: String)?
    let jsonParserObj = RTJsonParser()
    weak var LLBVCDelegate: LLBGetBillVCDelegate?
    var controllerState : ControllerStateType = .notDefined
    var PayForViewParamDict = ["BillID".localized : "","BillAmount".localized : ""]
    var billStatusFromServer : StatusOptions?
    var rowCountArray : [String] = [""]
    var classActionButton : UIButton?
    var KeyboardSize : CGFloat = 0.0
    var activeField : UITextField? = nil
    var textFieldDataArray = ["", "", "", "", "", ""]
    var FooterViewContainer : UIView? = nil
    var FooterText :String? = nil
    var DictToShow = Dictionary<String, Any>()
    var GetBillResponseDict = Dictionary<String, AnyObject>()
    var billDataModelObj : HistoryVCDataModel?
    var footerViewLabel : UILabel?{
        didSet
        {
            footerViewLabel?.text = footerViewLabel?.text?.localized
        }
    }
    var SelectCityCustomController : SpringView? = nil
    var DarkView :  UIView? = nil
    var City1View : UIView? = nil
    var City2View : UIView? = nil
    var billAmountForCheckCondition = String()
    var adImageConstraint: NSLayoutConstraint!
    var cityModelObj = [CityModel]()
    var isComingFromChangingCity = false
    var newlandlinepayment = ""
    var emptydata = ""
    
    @IBOutlet weak var adImage: UIImageView!
    @IBOutlet weak var tableviewOutlet: UITableView!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet var AFFbtn: UIButton!{
        didSet{
            AFFbtn.setTitle(AFFbtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var AFCbtn: UIButton!{
        didSet{
            AFCbtn.setTitle(AFCbtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var bottomGetBillBtnConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightGetBillBtnConstraint: NSLayoutConstraint!
    
    @IBOutlet var getBillBtn: UIButton!{
        didSet{
            getBillBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            getBillBtn.setTitle(getBillBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var citytableviewContainerOutlet: UIView!
    @IBOutlet weak var citytableviewOutlet: UITableView!
    var selectedIndexPath = IndexPath(row: 0, section: 0)
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        //self.title = "Landline Bill".localized
        //self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "Zawgyi-One", size: 20)!, NSAttributedStringKey.foregroundColor: UIColor.white]
        setupInitialView()
        self.automaticallyAdjustsScrollViewInsets = false
        jsonParserObj.delegate = self
        //        adImageConstraint = adImage.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        //        adImageConstraint.isActive = true
        
        bottomGetBillBtnConstraint.constant = 0
        
        self.view.layoutIfNeeded()
        subscribeToShowKeyboardNotifications()
        
        
        // for default township Yangon
        
        userDef.set(true, forKey: "citychoose")
        userDef.synchronize()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        self.title = "Landline Bill".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
        
        self.navigationController?.isNavigationBarHidden = false
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden(notification:)) , name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillChangeHeight(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        self.loadAdvertisement()
        if userDef.bool(forKey: "makeAnotherPayment"){
            userDef.set(false, forKey: "makeAnotherPayment")
            userDef.synchronize()
            let customView = UIView()
            customView.backgroundColor = .lightGray
            self.tableviewOutlet.tableFooterView = customView
            self.checkAnotherNumber()
        }
    }
    override func viewWillDisappear(_ animated: Bool){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification,object : nil)
        self.view.endEditing(true)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        self.parentView.isHidden = true
    }
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            //            self.adImageConstraint.constant = 0 - keyboardHeight
            
        if device.type == .iPhoneX || device.type == .iPhoneXR || device.type == .iPhoneXS || device.type == .iPhoneXSMax {
                    self.bottomGetBillBtnConstraint.constant = keyboardHeight-30
                }
            else {
                    self.bottomGetBillBtnConstraint.constant = keyboardHeight
                }
          //  self.bottomGetBillBtnConstraint.constant = keyboardHeight
            if self.getBillBtn.isHidden {
                self.heightGetBillBtnConstraint.constant = 0
            } else {
                self.heightGetBillBtnConstraint.constant = 50
            }
            
        })
    }
    @objc func keyboardWillHide(_ notification: Notification) {
        //        self.adImageConstraint.constant = 0
        self.bottomGetBillBtnConstraint.constant = 0
        if self.getBillBtn.isHidden {
            self.heightGetBillBtnConstraint.constant = 0
        } else {
            self.heightGetBillBtnConstraint.constant = 50
        }
    }
    func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    func loadAdvertisement() {
        let randomIndex = Int(arc4random_uniform(UInt32(advImagesUrl.AdImageData.count)))
        if let urlString = advImagesUrl.AdImageData[safe: randomIndex] {
            DispatchQueue.main.async {
                self.adImage.contentMode = .scaleToFill
                self.adImage.image = UIImage(data: urlString)
                //                self.adImage.setImage(url: URL.init(string: urlString), contentMode: UIView.ContentMode.scaleAspectFit, placeholderImage: nil)
            }
        }
    }
}
extension LLBGetBillVC : FavoriteSelectionDelegate{
    func selectedFavoriteObject(obj: FavModel) {
        self.parentView.isHidden = true
        if obj.favDeleteNum.length <= 9{
            self.refreshTable()
            self.getBillBtn.isHidden = false
            self.heightGetBillBtnConstraint.constant = 50
            self.getBillBtn.setTitle("Get Bill".localized, for: .normal)
            println_debug(obj.favDeleteNum.replacingOccurrences(of: "-", with: ""))
            println_debug(obj.favDeleteNum.replacingOccurrences(of: "-", with: ""))
            let cell = self.tableviewOutlet.cellForRow(at: IndexPath.init(row: 1, section: 0)) as? LLBGetBillTableCell
            cell?.textFieldOutlet.text = obj.favDeleteNum.replacingOccurrences(of: "-", with: "")
            
            
            if obj.favDeleteNum.hasPrefix("01"){
                textFieldDataArray.append("Yangon")
            }else if obj.favDeleteNum.hasPrefix("02"){
                textFieldDataArray.append("Mandalay")
            }else if obj.favDeleteNum.hasPrefix("067"){
                textFieldDataArray.append("Naypyitaw")
            }else if obj.favDeleteNum.hasPrefix("042"){
                textFieldDataArray.append("Pathein")
            }else if obj.favDeleteNum.hasPrefix("062"){
                textFieldDataArray.append("Magway")
            }else if obj.favDeleteNum.hasPrefix("057"){
                textFieldDataArray.append("Mawlamyaing")
            }
//            textFieldDataArray.append("Yangon")
            textFieldDataArray.append((cell?.textFieldOutlet.text)!.localized)
            tableviewOutlet.reloadData()
        }
        
        println_debug(obj)
    }
}
extension LLBGetBillVC: UITableViewDelegate, UITableViewDataSource,LLBGetBillTableCellDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == citytableviewOutlet{
            return cityModelObj.count
        }else{
            return rowCountArray.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if tableView == citytableviewOutlet{
            let cell: LLBGetBillCityTableCell = tableView.dequeueReusableCell(withIdentifier: "cityCell", for: indexPath) as! LLBGetBillCityTableCell
            
            if let currentStr = appDel.getSelectedLanguage() as? String{
                
                if currentStr == "uni"{
                    cell.lblCityName.font = UIFont(name: appFont, size: appFontSize)
                    cell.lblCityName.text = cityModelObj[indexPath.row].Divisionun
                }
                else if currentStr == "my"{
                    cell.lblCityName.font = UIFont(name: appFont, size: appFontSize)
                    cell.lblCityName.text = cityModelObj[indexPath.row].Divisionmy
                }
                else{
                    cell.lblCityName.font = UIFont(name: appFont, size: appFontSize)
                    cell.lblCityName.text = cityModelObj[indexPath.row].Divisionen
                }
                print(currentStr)
            }
            if let val = cityModelObj[indexPath.row].DivisionVal, val.contains(find: "0"){
                cell.lblCityNumber.text = cityModelObj[indexPath.row].DivisionVal
            }else{
                cell.lblCityNumber.text = "0\(cityModelObj[indexPath.row].DivisionVal ?? "")"
            }
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        }else{
            let customCell : LLBGetBillTableCell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! LLBGetBillTableCell
            customCell.delegate = self
            customCell.LLBparentVC = self
            
            
            
            if isObjectNotNil(object: self.billDataModelObj){
                
                if  emptydata == "empty"{
                   customCell.setCellDataWithoutObject(index: indexPath, text: textFieldDataArray[indexPath.row], emptystr:emptydata, showCity: isComingFromChangingCity)
                   //  customCell.setCellData(index: indexPath, text:textFieldDataArray[indexPath.row],newpayment:newlandlinepayment,showCity: isComingFromChangingCity)
                 }
                else
                {
                    customCell.setCellData(index: indexPath, text: textFieldDataArray[indexPath.row], billDataModelObject: self.billDataModelObj!,emptystr:emptydata, showCity: isComingFromChangingCity)
                }
              }
             else{
                // crash
//                if  emptydata == "empty" {
//                     customCell.setCellData(index: indexPath, text: textFieldDataArray[indexPath.row], billDataModelObject: self.billDataModelObj!,emptystr:emptydata, showCity: isComingFromChangingCity)
//                }
//                else
//                {
                    customCell.setCellData(index: indexPath, text:textFieldDataArray[indexPath.row],newpayment:newlandlinepayment,showCity: isComingFromChangingCity)
//                }
                                
            }
            println_debug("Text data \(textFieldDataArray[indexPath.row])")
            return customCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == citytableviewOutlet{
            return 50
        }else{
            return 70
        }
    }
    func passData(cell: LLBGetBillTableCell){
        self.textFieldDataArray[cell.customIndexPath.row]  =  cell.textFieldOutlet.text!
        println_debug("Text data \(textFieldDataArray[cell.customIndexPath.row])")
        let str = cell.textFieldOutlet.text!
        
        if let chooseCity = userDef.value(forKey: "citychoose") as? Bool{
            if chooseCity{
                if str.count >= 7 && cell.customIndexPath.row == 1{
                    //            classActionButton?.isHidden = false
                    getBillBtn.isHidden = false
                    heightGetBillBtnConstraint.constant = 50
                }
            }else{
                if str.count >= 7 && cell.customIndexPath.row == 1{
                    //            classActionButton?.isHidden = false
                    getBillBtn.isHidden = false
                    heightGetBillBtnConstraint.constant = 50
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == citytableviewOutlet {
            if let cell = self.tableviewOutlet.cellForRow(at: indexPath) as? LLBGetBillTableCell{
              
                cell.btn.tag = 200
                self.showCity(sender: cell.btn)
            }else {
               // cell.btn.setImage(UIImage(named: "uparrow_blue"), for: .selected)
                if let cell = self.tableviewOutlet.cellForRow(at: IndexPath(row: 0, section: 0)) as? LLBGetBillTableCell{
                      cell.btn.setImage(UIImage(named: "downarrow_blue"), for: .normal)
                }
                self.citytableviewContainerOutlet.isHidden = true
                self.citytableviewOutlet.isHidden = true
            }
            isComingFromChangingCity = true
            self.selectedIndexPath = indexPath
            self.selectCityFromMenu(indexPath: indexPath)
             println_debug("Hide state")
        }else{
            println_debug("Show State List")
            if indexPath.row == 0 {
                if let cell = self.tableviewOutlet.cellForRow(at: indexPath) as? LLBGetBillTableCell {
                    if newlandlinepayment == "NEW" {
                        self.view.endEditing(true)
                        if self.citytableviewContainerOutlet.isHidden {
                            cell.btn.setImage(UIImage(named: "uparrow_blue"), for: .normal)
                        }else {
                            cell.btn.setImage(UIImage(named: "downarrow_blue"), for: .normal)
                        }
                        cell.btn.tag = 100
                        self.showCity(sender: cell.btn)
                    }
                    else if emptydata == "empty"
                    {
                        self.view.endEditing(true)
                        if self.citytableviewContainerOutlet.isHidden {
                            cell.btn.setImage(UIImage(named: "uparrow_blue"), for: .normal)
                           }else {
                             cell.btn.setImage(UIImage(named: "downarrow_blue"), for: .normal)
                           }
                         cell.btn.tag = 100
                         self.showCity(sender: cell.btn)
                    }
                    else{
                        // with all data in textfields
                    }
                }
            }
        }
    }
}
extension LLBGetBillVC : ContactPickerDelegate {
    func contact(_: ContactPickersPicker, didSelectMultipleContacts contacts: [ContactPicker]) {
        for contact in contacts{
            decodeContact(contact: contact, isMulti: true)
        }
    }
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker){
        println_debug(contact)
        decodeContact(contact: contact, isMulti: false)
    }
    func decodeContact(contact: ContactPicker, isMulti: Bool){
        subscribeToShowKeyboardNotifications()
        
        if contact.phoneNumbers[0].phoneNumber.length <= 9 && contact.phoneNumbers[0].phoneNumber != ""{
            self.refreshTable()
            //            self.classActionButton?.isHidden = false
            //            classActionButton?.setTitle("Get Bill".localized, for: .normal)
            self.getBillBtn.isHidden = false
            self.heightGetBillBtnConstraint.constant = 50
            self.getBillBtn.setTitle("Get Bill".localized, for: .normal)
            println_debug(contact.firstName)
            println_debug(contact.phoneNumbers[0].phoneNumber)
            let cell = self.tableviewOutlet.cellForRow(at: IndexPath.init(row: 1, section: 0)) as? LLBGetBillTableCell
            cell?.textFieldOutlet.text = contact.phoneNumbers[0].phoneNumber.localized
            
            if contact.phoneNumbers[0].phoneNumber.hasPrefix("01"){
                textFieldDataArray.append("Yangon")
            }else if contact.phoneNumbers[0].phoneNumber.hasPrefix("02"){
                textFieldDataArray.append("Mandalay")
            }else if contact.phoneNumbers[0].phoneNumber.hasPrefix("067"){
                textFieldDataArray.append("Naypyitaw")
            }else if contact.phoneNumbers[0].phoneNumber.hasPrefix("042"){
                textFieldDataArray.append("Pathein")
            }else if contact.phoneNumbers[0].phoneNumber.hasPrefix("062"){
                textFieldDataArray.append("Magway")
            }else if contact.phoneNumbers[0].phoneNumber.hasPrefix("057"){
                textFieldDataArray.append("Mawlamyaing")
            }
            
            textFieldDataArray.append((cell?.textFieldOutlet.text)!.localized)
            
            
            
            
            
            
            
            
            let phoneNumberCount = contact.phoneNumbers.count
            
            if phoneNumberCount == 1 {
                _ = "\(contact.phoneNumbers[0].phoneNumber)"
                println_debug(contact.firstName)
                println_debug(contact.phoneNumbers[0].phoneNumber)
            }
            else if phoneNumberCount > 1 {
                _ = "\(contact.phoneNumbers[0].phoneNumber) and \(contact.phoneNumbers.count-1) more"
            }
            else {
                _ = ContactGlobalConstants.Strings.phoneNumberNotAvaialable
            }
            
            tableviewOutlet.reloadData()
        }
        else{
            DispatchQueue.main.async{
                alertViewObj.wrapAlert(title: "", body: PPMStrings.ErrorMessages.incorrectMobileNumber, img: #imageLiteral(resourceName: "dashboard_landline_bill"))
                alertViewObj.addAction(title: "OK".localized, style: .target, action: {})
                alertViewObj.showAlert(controller: self)
            }
        }
    }
}
extension LLBGetBillVC {
    
    
    func GetPostPaidCityParametersGeneration() -> Dictionary<String,Any> {
        
        var login = Dictionary<String,Any>()
        login["AppId"] = 20//0
        login["Limit"] = 0
        login["MobileNumber"] = 00959790681381//UserModel.shared.mobileNo
        login["Msid"] = 06//getMsid()
        login["Offset"] = 0
        login["Ostype"] = 1
        login["Otp"] = "D21417F1-7666-4D55-BF5E-068F92AE3D05"
        login["Simid"] = "D21417F1-7666-4D55-BF5E-068F92AE3D05"//uuid
        
        return login
        
    }
    
    
    
    
    
    func setupInitialView(){
        
        
        progressViewObj.showProgressView()
        let getBillUrl = URL.init(string: "http://69.160.4.151:8002/AdService.svc/GetPostPaidCity")
        let param = self.JSONStringFromAnyObject(value: self.GetPostPaidCityParametersGeneration() as AnyObject)
        DispatchQueue.main.async{
            self.jsonParserObj.ParseMyJson(url: getBillUrl!, param: param as AnyObject, httpMethod: LLBStrings.kMethod_Post, contentType: LLBStrings.kContentType_Json, mScreen: "LLBGetPostPaidCityAPI", authStr: nil)
        }
        
        
        
        
        //        classActionButton = UIButton.init(frame: CGRect(x: 0, y: self.view.frame.height - KeyboardSize - 50 , width: self.view.frame.width, height: 54))
        //        classActionButton?.setTitle("Get Bill".localized, for: .normal)
        //        classActionButton?.titleLabel?.font = UIFont.init(name: "Zawgyi-One", size: 17)
        //        classActionButton?.backgroundColor = UIColor.init(red: 246/255.0, green: 198/255.0, blue: 0, alpha: 1)
        //        classActionButton?.addTarget(self, action: #selector(self.classActionButtonClicked(sender:)), for: .touchUpInside)
        //        self.view.addSubview(classActionButton!)
        //        self.view.bringSubview(toFront: classActionButton!)
        
        if isObjectNotNil(object: self.billDataModelObj){
            self.rowCountArray.append("")
            //            classActionButton?.isHidden = false
            getBillBtn.isHidden = false
            self.textFieldDataArray[1] = "0".appending((self.billDataModelObj?.PhoneNumber)!).localized
            billAmountForCheckCondition = (self.billDataModelObj?.LastBillAmount)!
            self.GetBillBtnAction()
        }
        else if favoriteTouple != nil{
            self.rowCountArray.append("")
            //        classActionButton?.isHidden = false
            getBillBtn.isHidden = false
            self.textFieldDataArray[0] = favoriteTouple!.areaCode
            self.textFieldDataArray[1] = favoriteTouple!.billNo
            // self.GetBillBtnAction()
        }
        else{
            //            classActionButton?.isHidden = true
            getBillBtn.isHidden = true
        }
        FooterViewContainer = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100))
        let customView = UIView()
        customView.backgroundColor = .lightGray
        self.tableviewOutlet.tableFooterView = customView
        self.view.addSubview(parentView)
    }
    func isObjectNotNil(object:HistoryVCDataModel!) -> Bool{
        if let _:HistoryVCDataModel = object{
            return true
        }
        return false
    }
}
extension LLBGetBillVC: RTJsonParserDelegate{
    func requestTokenFromServer(TokenForAPIName : String) {
        if appDel.checkNetworkAvail() {
            progressViewObj.showProgressView()
            println_debug(" \nRequesting Token From Server")
            let hashValue = LLBStrings.aKey.hmac_SHA1(key: LLBStrings.sKey)
            let inputValue = "password=\(hashValue)&grant_type=password"
            jsonParserObj.ParseMyJson(url: URL.init(string: LLBStrings.kTokenAPIURL)! , param: inputValue as AnyObject, httpMethod: LLBStrings.kMethod_Post, contentType: LLBStrings.kContentType_urlencoded, mScreen: TokenForAPIName, authStr: nil)
        }
        else{
            alertViewObj.wrapAlert(title: nil, body: LLBStrings.ErrorMessages.internetError, img: #imageLiteral(resourceName: "landlineImg"))
            alertViewObj.addAction(title: "OK".localized, style: .target) {}
            alertViewObj.showAlert(controller: self)
        }
    }
    func ResponseFromServer(withSuccess Success: Bool, json: AnyObject?, screen: String, Error: String?){
        if !Success{
            DispatchQueue.main.async {
                //                alertViewObj.wrapAlert(title: nil, body: LLBStrings.ErrorMessages.TryAgainErrorMsg, img: #imageLiteral(resourceName: "landlineImg"))
                //                alertViewObj.addAction(title: "OK".localized, style: .target) { }
                //                alertViewObj.showAlert(controller: self)
                self.requestTokenFromServer(TokenForAPIName: "GetBillToken")
            }
        }
        else {
            if let jsonArray = json as? Dictionary<String, Any> {
                switch screen {
                case "GetBillToken":
                    handleTokenResponse(TokenSuccess: Success, TokenForAPI: "GetBillToken", Tokenjson: json,  TokenError: Error)
                case "PayForViewToken":
                    handleTokenResponse(TokenSuccess: Success, TokenForAPI: "PayForViewToken", Tokenjson: json, TokenError: Error)
                case "LLBGetBillAPI":
                    handleGetBillAPIResponse(jsonResponse : jsonArray)
                case "PayForViewAPI":
                    handlePayForViewAPIResponse(jsonResponse : jsonArray)
                case "LLBGetPostPaidCityAPI":
                    parseCityDataAPIResponse(jsonResponse : jsonArray)
                default:
                    break
                }
            }
            else {
                DispatchQueue.main.async {
                    if let jsonData = json as? String ,jsonData == "already paid for view." {
                        
                        DispatchQueue.main.async{
                            let BillPaymentStatus = self.tempBillDetails?["BillPaymentStatus"] as? String ?? ""
                            let BillDetailDict = (self.tempBillDetails?["BillDetail"] as! Dictionary<String, Any>)
                            self.rowCountArray.append("")
                            self.rowCountArray.append("")
                            self.rowCountArray.append("")
                            self.textFieldDataArray.append("")
                            self.textFieldDataArray.append("")
                                self.textFieldDataArray.append("")
                                let BillingMonth =  String(describing: BillDetailDict["BillingMonth"]!)
                                let BillingYear  =  String(describing: BillDetailDict["BillingYear"]!)
                                let dateToPass   = BillingYear  + "-" + BillingMonth
                                let helperObj = RTHelperClass()
                                let convertedValue = helperObj.formattedDateFromString(dateString: dateToPass, withFormat: "YY-MM", toBeConvertedToFormat: "MMM YYYY")
                                
                                if let currentStr = appDel.getSelectedLanguage() as? String{
                                    
                                    if currentStr == "uni"{
                                        if let num = self.cityModelObj[self.selectedIndexPath.row].Divisionun{
                                            self.textFieldDataArray[0] = num
                                        }
                                    }
                                    else if currentStr == "my"{
                                        if let num = self.cityModelObj[self.selectedIndexPath.row].Divisionmy{
                                            self.textFieldDataArray[0] = num
                                        }
                                    }
                                    else{
                                        if let num = self.cityModelObj[self.selectedIndexPath.row].Divisionen{
                                            self.textFieldDataArray[0] = num
                                        }
                                        
                                    }
                                    print(currentStr)
                                }
                                
                                self.textFieldDataArray[1] = "0".appending(String(describing: BillDetailDict["Number"]!))
                                self.textFieldDataArray[2] =  "\(DateFormatter().monthSymbols[(Int(BillingMonth) ?? 0) - 1]) 20\(BillingYear)" //convertedValue!
                            self.textFieldDataArray[3] =  self.getDigitDisplay(String(describing: self.tempBillDetails?["TotalBillAmount"] ?? "")).appending(" MMK")
                                self.billAmountForCheckCondition = self.getDigitDisplay(String(describing: self.tempBillDetails?["TotalBillAmount"] ?? ""))
                                let status = BillDetailDict["BillStatusDescription"] as? String ?? ""
                                if status == "PaidWithOk"{
                                    self.textFieldDataArray[4] = "Paid With OK$"
                                }
                                else {
                                    self.textFieldDataArray[4] = status//jsonResponse["BillPaymentStatus"] as? String ?? ""
                                }
                                
                                //                    self.classActionButton?.setTitle("CHECK ANOTHER NUMBER".localized, for: .normal)
                                self.getBillBtn.setTitle("Submit".localized, for: .normal)
                                self.tableviewOutlet.reloadData()
                           
                             
                        }
                        
                        
                        
                    }
                    else {
                        alertViewObj.wrapAlert(title: "", body: json as! String, img: #imageLiteral(resourceName: "landlineImg"))
                        alertViewObj.addAction(title: "Done".localized, style: .target, action:{
                            self.isComingFromChangingCity = false
                            self.rowCountArray = [""]
                            self.textFieldDataArray = ["", "", "", "", "", ""]
                            self.emptydata = "empty"
                            self.tableviewOutlet.reloadData()
                            //                        self.ChangeCityAction()
                            self.getBillBtn.isHidden = true
                            self.citytableviewContainerOutlet.isHidden = false
                            self.citytableviewOutlet.isHidden = false
                        })
                        alertViewObj.showAlert(controller: self)
                    }
                }
            }
        }
        progressViewObj.removeProgressView()
    }
    
    func handleTokenResponse(TokenSuccess: Bool, TokenForAPI : String,  Tokenjson: AnyObject?, TokenError: String?) {
        
        if TokenSuccess == true{
            let responseDic = Tokenjson as! NSDictionary
            let tokentype = responseDic.value(forKey: "token_type")
            let accesstoken = responseDic.value(forKey: "access_token")
            let authorizationString =  "\(tokentype!) \(accesstoken!)"
            var txtValue : String = self.textFieldDataArray[1]
            println_debug(txtValue)
            if txtValue.hasPrefix("0"){txtValue = String(txtValue.suffix(txtValue.length-1))}
            println_debug(txtValue)
            switch TokenForAPI {
            case "GetBillToken" :
                println_debug("token Received, Requesting Get Bill API")
                let urlString = String.init(format: LLBStrings.kGetBillAPIURL, LLBStrings.okAccountNum, "test", txtValue)
                let getBillUrl = URL.init(string: urlString)
                DispatchQueue.main.async{
                    //println_debug(getBillUrl!)
                    println_debug(LLBStrings.kMethod_Get)
                    println_debug(LLBStrings.kContentType_Json)
                    println_debug(authorizationString)
                    self.jsonParserObj.ParseMyJson(url: getBillUrl!, param: nil, httpMethod: LLBStrings.kMethod_Get, contentType: LLBStrings.kContentType_Json, mScreen: "LLBGetBillAPI", authStr: authorizationString)
                }
            case "PayForViewToken" :
                println_debug("token Received, Requesting Pay For View API")
                let pipeStr = "OkAccountNumber=\(LLBStrings.okAccountNum)|Password=\(ok_password!)|Amount=\(PayForViewParamDict["BillAmount"]!)|BillDetailId=\(PayForViewParamDict["BillID"]!)"
                println_debug(pipeStr)
                let hashValueStrForPipeStr = pipeStr.hmac_SHA1(key: LLBStrings.sKey)
                println_debug(hashValueStrForPipeStr)
                let jsonText = "{\"OkAccountNumber\":\"\(LLBStrings.okAccountNum)\", \"Password\":\"\(ok_password!)\", \"Amount\":\"\(PayForViewParamDict["BillAmount"]!)\", \"BillDetailId\":\"\(PayForViewParamDict["BillID"]!)\",\"HashValue\":\"\(hashValueStrForPipeStr)\"}"
                println_debug(jsonText)
                let PayForViewURL = URL.init(string: LLBStrings.kPayForViewAPIURL)
                DispatchQueue.main.async{
                    self.jsonParserObj.ParseMyJson(url: PayForViewURL!, param: jsonText as AnyObject, httpMethod: LLBStrings.kMethod_Post, contentType: LLBStrings.kContentType_Json, mScreen: "PayForViewAPI", authStr: authorizationString)
                }
            default : break
            }
        }
        else{
            println_debug("token Not Received")
            DispatchQueue.main.async {
                self.requestTokenFromServer(TokenForAPIName: "GetBillToken")
                //                alertViewObj.wrapAlert(title: "", body: LLBStrings.ErrorMessages.TryAgainErrorMsg, img: #imageLiteral(resourceName: "landlineImg"))
                //                alertViewObj.addAction(title: "Done".localized, style: .target, action:{})
                //                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    
    func parseCityDataAPIResponse(jsonResponse : Dictionary<String, Any>){
        print(jsonResponse)
        DispatchQueue.main.async{
            progressViewObj.removeProgressView()
            if let responseData = jsonResponse.safeValueForKey("Data"){
                if let value = responseData as? String{
                    let  valueOfArray = self.convertStringToJson(ReceivedString: value)
                    let arrayObj = valueOfArray as! NSArray
                    self.cityModelObj.removeAll()
                    for i in 0 ..< arrayObj.count{
                        self.cityModelObj.append(CityModel(dataVal: arrayObj[i] as! NSDictionary))
                    }
                }
                
            }else{
                self.showToast(message: "API not working")
            }
            self.citytableviewOutlet.reloadData()
        }
    }
    
    
    func convertStringToJson(ReceivedString : String) -> Any {
        let CustomStringData = ReceivedString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion:true)
        do {
            let jsonData = try? JSONSerialization.jsonObject(with: CustomStringData!, options: .allowFragments ) as Any
            return jsonData!
        }
    }
    
    
    func handleGetBillAPIResponse(jsonResponse : Dictionary<String, Any>){
        print(jsonResponse)
        if let _ = jsonResponse.safeValueForKey("Message"){
            DispatchQueue.main.async{
                alertViewObj.wrapAlert(title: "", body: LLBStrings.ErrorMessages.TryAgainErrorMsg, img: #imageLiteral(resourceName: "landlineImg"))
                alertViewObj.addAction(title: "OK".localized, style: .target, action:{
                    
                })
                alertViewObj.showAlert(controller: self)
            }
            return
        }
        self.billDataModelObj = nil
        let BillPaymentStatus = jsonResponse["BillPaymentStatus"] as? String ?? ""
        let BillDetailDict = (jsonResponse["BillDetail"] as! Dictionary<String, Any>)
        self.tempBillDetails = jsonResponse
        println_debug(BillPaymentStatus)
        println_debug(BillDetailDict)
        switch BillPaymentStatus{
        case "Paid","PaidWithOkDollar","PaidWithOk" :
            DispatchQueue.main.async{
                alertViewObj.wrapAlert(title: "", body: LLBStrings.ErrorMessages.PaidErrorMessage, img: #imageLiteral(resourceName: "landlineImg"))
                alertViewObj.addAction(title: "OK".localized, style: .target, action:{
                    self.rowCountArray.append("")
                    self.rowCountArray.append("")
                    self.rowCountArray.append("")
                    self.textFieldDataArray.append("")
                    self.textFieldDataArray.append("")
                    self.textFieldDataArray.append("")
                    let BillingMonth =  String(describing: BillDetailDict["BillingMonth"]!)
                    let BillingYear  =  String(describing: BillDetailDict["BillingYear"]!)
                    let dateToPass   = BillingYear  + "-" + BillingMonth
                    let helperObj = RTHelperClass()
                    let convertedValue = helperObj.formattedDateFromString(dateString: dateToPass, withFormat: "YY-MM", toBeConvertedToFormat: "MMM YYYY")
                    
                    if let currentStr = appDel.getSelectedLanguage() as? String{
                        
                        if currentStr == "uni"{
                            if let num = self.cityModelObj[self.selectedIndexPath.row].Divisionun{
                                self.textFieldDataArray[0] = num
                            }
                        }
                        else if currentStr == "my"{
                            if let num = self.cityModelObj[self.selectedIndexPath.row].Divisionmy{
                                self.textFieldDataArray[0] = num
                            }
                        }
                        else{
                            if let num = self.cityModelObj[self.selectedIndexPath.row].Divisionen{
                                self.textFieldDataArray[0] = num
                            }
                            
                        }
                        print(currentStr)
                    }
                    
                    self.textFieldDataArray[1] = "0".appending(String(describing: BillDetailDict["Number"]!))
                    self.textFieldDataArray[2] =  "\(DateFormatter().monthSymbols[(Int(BillingMonth) ?? 0) - 1]) 20\(BillingYear)" //convertedValue!
                    self.textFieldDataArray[3] =  self.getDigitDisplay(String(describing: jsonResponse["TotalBillAmount"]!)).appending(" MMK")
                    self.billAmountForCheckCondition = self.getDigitDisplay(String(describing: jsonResponse["TotalBillAmount"]!))
                    let status =  jsonResponse["BillPaymentStatus"] as? String ?? ""
                    if status == "PaidWithOk"{
                        self.textFieldDataArray[4] = "Paid With OK$"
                    }
                    else {
                        self.textFieldDataArray[4] = status//jsonResponse["BillPaymentStatus"] as? String ?? ""
                    }
                    
                    //                    self.classActionButton?.setTitle("CHECK ANOTHER NUMBER".localized, for: .normal)
                    self.getBillBtn.setTitle("CHECK ANOTHER NUMBER".localized, for: .normal)
                    self.tableviewOutlet.reloadData()
                })
                alertViewObj.showAlert(controller: self)
            }
        case "Unpaid" :         HandleUnpaidStatusFromGetBillAPI(jsonResponse: jsonResponse)
        self.GetBillResponseDict = jsonResponse as [String : AnyObject]
            break
        case "RequiredToPayForView" :   println_debug("PayForView")
        self.GetBillResponseDict = jsonResponse as [String : AnyObject]
        DispatchQueue.main.async{
            let PayForViewAmount = String(describing: jsonResponse["ViewOnlyAmount"]! )
            let alertString = String.init(format: LLBStrings.ErrorMessages.PayForViewErrorMessage, PayForViewAmount)
            alertViewObj.wrapAlert(title: "", body: alertString, img: #imageLiteral(resourceName: "landlineImg"))
            alertViewObj.addAction(title: "OK".localized, style: .target, action:{
                let BillDetailID = String(describing: BillDetailDict["BilDetailId"]!)
                let ViewOnlyAmount = String(describing: jsonResponse["ViewOnlyAmount"]! )
                self.PayForViewParamDict["BillID"] =  BillDetailID
                self.PayForViewParamDict["BillAmount"] = ViewOnlyAmount
                self.requestTokenFromServer(TokenForAPIName: "PayForViewToken")
            })
            alertViewObj.addAction(title: "CANCEL".localized, style: .target, action:{
                //                self.dismiss(animated: true, completion: nil)
                self.checkAnotherNumber()
            })
            
            alertViewObj.showAlert(controller: self)
            }
        default : break
        }
    }
    
    func handlePayForViewAPIResponse(jsonResponse : Dictionary<String, Any>){
        HandleUnpaidStatusFromGetBillAPI(jsonResponse: GetBillResponseDict)
    }
    
    func HandleUnpaidStatusFromGetBillAPI(jsonResponse : Dictionary<String, Any>){
        
        let helperObj = RTHelperClass()
        let BillDetailDict = (jsonResponse["BillDetail"] as! Dictionary<String, Any>)
        println_debug("Reload Table view for submitting")
        println_debug("BillDetailDict : \(BillDetailDict)")
        self.rowCountArray.append("")
        self.rowCountArray.append("")
        self.rowCountArray.append("")
        self.rowCountArray.append("")
        self.textFieldDataArray.append("")
        self.textFieldDataArray.append("")
        self.textFieldDataArray.append("")
        self.textFieldDataArray.append("")
        let BillingMonth = String(describing: BillDetailDict["BillingMonth"]!)
        let BillingYear = String(describing: BillDetailDict["BillingYear"]!)
        let dateToPass = BillingMonth + "-" + BillingYear
        let convertedValue = helperObj.formattedDateFromString(dateString: dateToPass, withFormat: "MM-yy", toBeConvertedToFormat: "MMMM YYYY")
        DispatchQueue.main.async {
            if (jsonResponse["BillCheckingResult"] as? Dictionary<String, Any>) != nil{
                self.showToastlocal(message: "Payment Successful".localized)
            }
            self.textFieldDataArray[0] = String(describing: BillDetailDict["SourceDescription"]!)
            self.textFieldDataArray[1] = "0".appending(String(describing: BillDetailDict["Number"]!))
            self.textFieldDataArray[2] = convertedValue!
            self.textFieldDataArray[3] = self.getDigitDisplay(String(describing: jsonResponse["TotalBillAmount"]!)).appending(" MMK")
            self.billAmountForCheckCondition = self.getDigitDisplay(String(describing: jsonResponse["TotalBillAmount"]!))
            //            self.textFieldDataArray[4] = jsonResponse["BillPaymentStatus"] as? String ?? ""
            
            
            let status = jsonResponse["BillPaymentStatus"] as? String ?? ""
            if status == "Unpaid"{
                self.textFieldDataArray[4] = "Paid With OK$"
            }
            else {
                self.textFieldDataArray[4] = status//jsonResponse["BillPaymentStatus"] as? String ?? ""
            }
            
            self.textFieldDataArray[5] = String(describing: jsonResponse["ServiceFee"]!).appending(" MMK")
        }//Get Due Date
        let dueDateStr = String(describing: BillDetailDict["DueDate"]!)
        //Format it properly
        let FormattedDate = helperObj.formattedDateFromString(dateString: dueDateStr, withFormat: "yyyy-MM-dd'T'HH:mm:ss", toBeConvertedToFormat: "d MMM, yyyy")
        //Convert string back to date
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "d MMM, yyyy"
        dateFormatter.setLocale()
        let dateToCompare = dateFormatter.date(from: FormattedDate!)
        //Find current date with same format
        let currentDateFormatter = DateFormatter()
        currentDateFormatter.calendar = Calendar(identifier: .gregorian)
        currentDateFormatter.dateFormat = "d MMM, yyyy"
        currentDateFormatter.setLocale()
        let currentDateStr = currentDateFormatter.string(from:Date())
        let currentDate = dateFormatter.date(from: currentDateStr)
        println_debug(currentDate!)
        println_debug(dateToCompare!)
        DispatchQueue.main.async {
            //            self.classActionButton?.setTitle("Submit".localized, for: .normal)
            
            
            self.getBillBtn.setTitle("Submit".localized, for: .normal)
            self.tableviewOutlet.reloadData()
            self.adImage.isHidden = true
            self.FooterText = "Your Due date is".localized + " \(FormattedDate!)"
            self.footerViewLabel = UILabel.init(frame: CGRect(x: 15, y: 5, width: self.view.frame.width - 30, height: 90))
            self.footerViewLabel?.textAlignment = .center
            self.footerViewLabel?.textColor = .red
            self.footerViewLabel?.numberOfLines = 4
            //self.footerViewLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
            self.footerViewLabel?.text = self.FooterText
            self.footerViewLabel?.font = UIFont.init(name: appFont, size: 14)
            self.footerViewLabel?.tag = 420
            self.tableviewOutlet.tableFooterView = nil
            self.FooterViewContainer?.addSubview(self.footerViewLabel!)
            self.tableviewOutlet.beginUpdates()
            self.tableviewOutlet.tableFooterView  = self.FooterViewContainer
            self.tableviewOutlet.endUpdates()
            if currentDate! >= dateToCompare!{
                alertViewObj.wrapAlert(title: "", body: LLBStrings.ErrorMessages.DueDateErrorMessage, img: #imageLiteral(resourceName: "landlineImg"))
                alertViewObj.addAction(title: "OK".localized, style: .target, action: {})
                alertViewObj.showAlert(controller: self)
            }
        }
        DictToShow = ["City" : textFieldDataArray[0],"Landline Number" : textFieldDataArray[1],"Bill Month" : textFieldDataArray[2],"Total Amount" : textFieldDataArray[3],"Servie Fee" : textFieldDataArray[5]]
    }
}
extension LLBGetBillVC{
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        self.KeyboardSize = (keyboardSize?.height)!
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: keyboardSize!.height + 45, right: 0.0)
        self.tableviewOutlet.contentInset = contentInsets
        self.tableviewOutlet.scrollIndicatorInsets = contentInsets
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if activeField != nil{
            if (!aRect.contains(activeField!.frame.origin)){
                self.tableviewOutlet.scrollRectToVisible(activeField!.frame, animated: true)
            }
        }
    }
    @objc func keyboardWillChangeHeight(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        self.KeyboardSize = (keyboardSize?.height)!
        //println_debug("Keyboard changed \(KeyboardSize,keyboardSize?.height))")
        //        classActionButton?.frame.origin.y = self.view.frame.height - KeyboardSize - 54
        if device.type == .iPhoneX || device.type == .iPhoneXR || device.type == .iPhoneXS || device.type == .iPhoneXSMax {
               self.bottomGetBillBtnConstraint.constant = KeyboardSize-30
           }
           else {
                 self.bottomGetBillBtnConstraint.constant = KeyboardSize
            }
        
       // bottomGetBillBtnConstraint.constant = KeyboardSize
        self.view.layoutIfNeeded()
    }
    
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        let contentInsets : UIEdgeInsets = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.tableviewOutlet.contentInset = contentInsets
        self.tableviewOutlet.scrollIndicatorInsets = contentInsets
        //        classActionButton?.frame.origin.y = self.view.frame.height - 54
        bottomGetBillBtnConstraint.constant = 0
    }
    //    @objc func classActionButtonClicked(sender : UIButton){
    //        self.view.endEditing(true)
    //        let ButtonTittle = sender.titleLabel?.text!.localized
    //        switch ButtonTittle!{
    //        case "Get Bill".localized : GetBillBtnAction()
    //        case "Submit".localized : submitBtnAction()
    //        case "CHECK ANOTHER NUMBER".localized : checkAnotherNumber()
    //        default: println_debug("Exception Occurred")
    //        }
    //    }
    
    
    
    @IBAction func getBillAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let ButtonTittle = sender.titleLabel?.text!
        switch ButtonTittle!{
        case "Get Bill".localized : GetBillBtnAction()
        case "Submit".localized : submitBtnAction()
        case "CHECK ANOTHER NUMBER".localized : checkAnotherNumber()
        default: println_debug("Exception Occurred".localized)
        }
    }
    
    
    
    
    func showParent(_ sender: UIButton){
        
        self.view.endEditing(true)
        self.parentView.isHidden = false
        
        self.parentView.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        
        UIView.animate(withDuration: 0.7, animations: {() -> Void in
            
            self.parentView.transform = CGAffineTransform(scaleX: 1,y: 1)
            
        })
        
    }
    
    
    func showToastlocal(message : String) {
        let toastLabel = UILabel()
        toastLabel.frame = CGRect(x: 20 , y: (self.view.frame.height-150), width: self.view.frame.width-40, height: 60)
        toastLabel.text = message
        toastLabel.numberOfLines = 0
        toastLabel.font = UIFont(name: appFont,size: 14.0)
        toastLabel.textAlignment = .center
        toastLabel.numberOfLines = 2
        toastLabel.layer.cornerRadius = 5
        toastLabel.layer.masksToBounds = true
        toastLabel.textColor = .white
        toastLabel.backgroundColor = UIColor.gray
        self.view.bringSubviewToFront(toastLabel)
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 5.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    
    func showParentView(_ sender: UIButton){
        self.view.endEditing(true)
        self.parentView.isHidden = true
        let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
        self.present(nav, animated: true, completion: nil)
    }
    
    func showCity(sender: UIButton){
        
        //sender.isSelected = !sender.isSelected
        
        if sender.tag == 100 {
            if self.citytableviewContainerOutlet.isHidden {
                self.citytableviewContainerOutlet.isHidden = false
                self.citytableviewOutlet.isHidden = false
            }else {
                self.citytableviewContainerOutlet.isHidden = true
                self.citytableviewOutlet.isHidden = true
            }
        }else {
            if self.citytableviewContainerOutlet.isHidden {
                self.citytableviewContainerOutlet.isHidden = false
                self.citytableviewOutlet.isHidden = false
            }else {
                self.citytableviewContainerOutlet.isHidden = true
                self.citytableviewOutlet.isHidden = true
            }
        }
    }
    
   
    
    
    @IBAction func FavAccess(_ sender: UIButton) {
        
        let vc = UIStoryboard.init(name: "Favorite", bundle: Bundle.main).instantiateViewController(withIdentifier: "favoriteNav")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
        
//        let vc = self.openFavoriteFromNavigation(self , withFavorite: .topup)
//        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func contactAccess(_ sender: UIButton){
        self.parentView.isHidden = true
        let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false)
        self.present(nav, animated: true, completion: nil)
    }
    @objc func GetBillBtnAction() {
        requestTokenFromServer(TokenForAPIName: "GetBillToken")
    }
    @objc func submitBtnAction() {
        
         
        if NSString(string: UserLogin.shared.walletBal).floatValue < NSString(string: billAmountForCheckCondition.replacingOccurrences(of: ",", with: "")).floatValue {
            alertViewObj.wrapAlert(title: "", body: "Your account have insufficient balance. Please recharge OK$ Wallet money at nearest OK$ Service Counter.".localized, img: #imageLiteral(resourceName: "landlineImg"))
            alertViewObj.addAction(title: "Add Money".localized, style: AlertStyle.target, action: {
                if let addWithdrawView = UIStoryboard(name: "AddWithdraw", bundle: nil).instantiateViewController(withIdentifier: "addWithdrawRoot") as? UINavigationController {
                    addWithdrawView.modalPresentationStyle = .fullScreen
                    self.navigationController?.present(addWithdrawView, animated: true, completion: nil)
                }
            })
            alertViewObj.showAlert(controller: self)
        } else {

            let confirmationVCObj = LLBStrings.FileNames.llbStoryBoardObj.instantiateViewController(withIdentifier: "LLBConfirmationVC") as! LLBConfirmationVC
            confirmationVCObj.valueArray = [textFieldDataArray[0],textFieldDataArray[1]]
            confirmationVCObj.DictToSend = self.GetBillResponseDict
            self.navigationController?.pushViewController(confirmationVCObj, animated: true)
        }
        
    }
    @objc func checkAnotherNumber() {
        rowCountArray.removeAll()
        rowCountArray.append("")
        textFieldDataArray.removeAll()
        textFieldDataArray = ["", "", "", "", "", ""]
        //        classActionButton?.isHidden = true
        getBillBtn.isHidden = true
        //        classActionButton?.setTitle("Get Bill".localized, for: .normal)
        self.getBillBtn.setTitle("Get Bill".localized, for: .normal)
        self.tableviewOutlet.reloadData()
    }
    @IBAction func backBtnAction(_ sender: Any) {
        if controllerState == .presented{self.dismiss(animated: true, completion: {self.LLBVCDelegate?.CallReloadAPI()})}
        else if controllerState == .pushed {self.navigationController?.popViewController(animated: true)}
        else{self.dismiss(animated: true, completion: {self.LLBVCDelegate?.CallReloadAPI()})}
    }
    
    
    func ChangeCityAction() {
        
        
        
        self.view.endEditing(true)
        
        DarkView = UIView.init()
        DarkView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        DarkView?.backgroundColor = .black
        DarkView?.alpha = 0.85
        self.view.addSubview(DarkView!)
        SelectCityCustomController = SpringView.init()
        if(self.view.frame.size.height) < 600{
            SelectCityCustomController?.frame =  CGRect(x: 0, y: self.view.frame.size.height * 0.40, width: self.view.frame.size.width, height: self.view.frame.size.height * 0.60)
        }
        else{
            SelectCityCustomController?.frame = CGRect(x: 0, y: self.view.frame.size.height * 0.45, width: self.view.frame.size.width, height: self.view.frame.size.height * 0.70)
        }
        SelectCityCustomController?.backgroundColor = .white
        self.view.addSubview(SelectCityCustomController!)
        SelectCityCustomController?.animation = "slideUp"
        SelectCityCustomController?.curve = "easeInOut"
        SelectCityCustomController?.force = 2.0
        SelectCityCustomController?.duration = 1.0
        SelectCityCustomController?.animate()
        
        let offset:CGFloat =  SelectCityCustomController!.frame.width/0.6
        let bounds: CGRect = SelectCityCustomController!.bounds
        let rectBounds: CGRect = CGRect(x: bounds.origin.x, y: bounds.origin.y + bounds.size.height/2, width: bounds.size.width, height: bounds.size.height / 2)
        let rectPath: UIBezierPath = UIBezierPath(rect: rectBounds)
        let ovalBounds: CGRect = CGRect(x: bounds.origin.x - offset / 2, y: bounds.origin.y, width: bounds.size.width + offset, height: bounds.size.height)
        let ovalPath: UIBezierPath = UIBezierPath.init(ovalIn: ovalBounds)
        rectPath.append(ovalPath)
        // Create the shape layer and set its path
        let maskLayer: CAShapeLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = rectPath.cgPath
        // Set the newly created shape layer as the mask for the view's layer
        
        SelectCityCustomController?.layer.mask = maskLayer
        let SelectCityLabel = UILabel.init(frame: CGRect(x: self.view.frame.size.width/2 - 100, y: 20, width: 200, height: 50))
        SelectCityLabel.font = UIFont.init(name: appFont, size: 17)
        SelectCityLabel.text = "Select City".localized
        SelectCityLabel.textAlignment = .center
        SelectCityLabel.textColor = .black
        SelectCityCustomController?.addSubview(SelectCityLabel)
        City1View = UIView.init(frame: CGRect(x: 0, y: (SelectCityLabel.frame.origin.y) + 60, width: self.view.frame.width , height: 50))
        SelectCityCustomController?.addSubview(City1View!)
        
        
        let city1 = UILabel.init(frame: CGRect(x: 15, y: 10, width: 200, height: 30))
        city1.font = UIFont.init(name: appFont, size: 16)
        city1.text = "Yangon".localized
        city1.textColor = .black
        City1View?.addSubview(city1)
        let city1Code = UILabel.init(frame: CGRect(x: screenWidth-50, y: 10, width: 40, height: 30))
        city1Code.text = "(01)"
        city1Code.textColor = .black
        city1Code.font = UIFont.systemFont(ofSize: 18)
        City1View?.addSubview(city1Code)
        let BlackLine = UIView.init(frame: CGRect(x: 15, y: (city1.frame.origin.y) + 35, width: self.view.frame.width - 25, height: 0.5))
        BlackLine.backgroundColor = .gray
        City1View?.addSubview(BlackLine)
        
        City2View = UIView.init(frame: CGRect(x: 0, y: (City1View?.frame.origin.y)! + 60, width: self.view.frame.width , height: 50))
        SelectCityCustomController?.addSubview(City2View!)
        
        
        
        
        let city2 = UILabel.init(frame: CGRect(x: 15, y: 10, width: 200, height: 30))
        city2.font = UIFont.init(name: appFont, size: 16)
        //        city2.text = "Mandalay                                        (02)"
        city2.text = "Mandalay".localized
        city2.textColor = .black
        City2View?.addSubview(city2)
        let city2Code = UILabel.init(frame: CGRect(x: screenWidth-50, y: 10, width: 40, height: 30))
        city2Code.text = "(02)"
        city2Code.textColor = .black
        city2Code.font = UIFont.systemFont(ofSize: 18)
        City2View?.addSubview(city2Code)
        let BlackLine2 = UIView.init(frame: CGRect(x: 15, y: (city2.frame.origin.y) + 35, width: self.view.frame.width - 25, height: 0.5))
        BlackLine2.backgroundColor = .gray
        City2View?.addSubview(BlackLine2)
        
        
        let CancelBtn = UIButton.init(frame: CGRect(x: 25, y: (City2View?.frame.origin.y)! + 100 , width: self.view.frame.size.width - 50, height: 50))
        CancelBtn.titleLabel?.font = UIFont.init(name: appFont, size: 16)
        CancelBtn.setTitle("CANCEL".localized, for: .normal)
        CancelBtn.setTitleColor(.white , for: .normal)
        CancelBtn.addTarget(self, action: #selector(self.HideSelectCityCustomController), for: .touchUpInside)
        CancelBtn.backgroundColor = LLBStrings.yellowAppColor
        SelectCityCustomController?.addSubview(CancelBtn)
        CancelBtn.layer.cornerRadius = 17
        
        
        
        let  SelectCityCustomControllerTap = UITapGestureRecognizer(target: self, action: #selector(self.HideSelectCityCustomController))
        SelectCityCustomControllerTap.delegate = self as UIGestureRecognizerDelegate
        DarkView?.addGestureRecognizer(SelectCityCustomControllerTap)
        
        
        
        let city1Gesture = UITapGestureRecognizer(target: self, action: #selector(self.city1Tapped(sender:)))
        city1Gesture.delegate = self as UIGestureRecognizerDelegate
        City1View?.addGestureRecognizer(city1Gesture)
        
        
        
        let city2Gesture = UITapGestureRecognizer(target: self, action: #selector(self.city2Tapped(sender:)))
        city2Gesture.delegate = self as UIGestureRecognizerDelegate
        City2View?.addGestureRecognizer(city2Gesture)
        
        
        
        let PanGesture = UIPanGestureRecognizer(target: self, action: #selector(self.wasDragged(gestureRecognizer:)))
        SelectCityCustomController?.addGestureRecognizer(PanGesture)
    }
    
    
    
    
    func refreshTable(){
        rowCountArray.removeAll()
        textFieldDataArray.removeAll()
        //        classActionButton?.isHidden = true
        self.getBillBtn.setTitle("Get Bill".localized, for: .normal)
        getBillBtn.isHidden = true
        self.tableviewOutlet.tableFooterView = UIView()
        rowCountArray.append("")
        rowCountArray.append("")
        
    }
    
    @objc func city1Tapped(sender : UITapGestureRecognizer){
        userDef.set(true, forKey: "citychoose")
        userDef.synchronize()
        
        println_debug("Call was in City1 Tapped")
        self.refreshTable()
        textFieldDataArray.append("Yangon")
        textFieldDataArray.append("01")
        tableviewOutlet.reloadData()
        HideSelectCityCustomController()
        DispatchQueue.main.async{
            let cell = self.tableviewOutlet.cellForRow(at: IndexPath(row: 1, section: 0)) as? LLBGetBillTableCell
            cell?.textFieldOutlet.text = "01"
            cell?.textFieldOutlet.becomeFirstResponder()
        }
    }
    
    @objc func city2Tapped(sender : UITapGestureRecognizer){
        
        userDef.set(false, forKey: "citychoose")
        userDef.synchronize()
        
        println_debug("Call was in City2 Tapped")
        
        self.refreshTable()
        textFieldDataArray.append("Mandalay")
        textFieldDataArray.append("02")
        tableviewOutlet.reloadData()
        HideSelectCityCustomController()
        DispatchQueue.main.async{
            let cell = self.tableviewOutlet.cellForRow(at: IndexPath(row: 1, section: 0)) as? LLBGetBillTableCell
            cell?.textFieldOutlet.text = "02"
            cell?.textFieldOutlet.becomeFirstResponder()
        }
    }
    
    
    
    func selectCityFromMenu(indexPath: IndexPath){
        
       userDef.set(false, forKey: "citychoose")
       userDef.synchronize()
        
        var name = ""
        var number = ""
        
        
        
        if let currentStr = appDel.getSelectedLanguage() as? String{
            
            if currentStr == "uni"{
                if let cityname = cityModelObj[indexPath.row].Divisionun{
                    name  = cityname
                }
            }
            else if currentStr == "my"{
                if let cityname = cityModelObj[indexPath.row].Divisionmy{
                    name  = cityname
                }
            }
            else{
                if let cityname = cityModelObj[indexPath.row].Divisionen{
                    name  = cityname
                }
            }
            print(currentStr)
        }
        
        
        
        if let val = cityModelObj[indexPath.row].DivisionVal, val.contains(find: "0"){
            number = cityModelObj[indexPath.row].DivisionVal ?? ""
        }else{
            number = "0\(cityModelObj[indexPath.row].DivisionVal ?? "")"
        }
        
        
        
        self.refreshTable()
        textFieldDataArray.append(name)
        textFieldDataArray.append(number)
        
   
    //    delay(delay: 0.5, closure: {
             self.tableviewOutlet.reloadData()
    //    })
       
       
       
        //        HideSelectCityCustomController()
        DispatchQueue.main.async{
            let cell = self.tableviewOutlet.cellForRow(at: IndexPath(row: 1, section: 0)) as? LLBGetBillTableCell
            cell?.textFieldOutlet.text = number
            cell?.textFieldOutlet.becomeFirstResponder()
            
        }
        
        
    }
    
    @objc func HideSelectCityCustomController(){
        UIView.animate(withDuration: 0.5, animations: {
            self.SelectCityCustomController?.frame.origin.y = self.view.frame.size.height
            self.DarkView?.alpha = 0.30
        }) { (Bool) in
            self.DarkView?.removeFromSuperview()
            self.SelectCityCustomController?.removeFromSuperview()
        }
    }
    
    @objc func wasDragged(gestureRecognizer: UIPanGestureRecognizer){
        //Check if gesture has started or changed
        if gestureRecognizer.state == UIGestureRecognizer.State.began || gestureRecognizer.state == UIGestureRecognizer.State.changed{
            let translation = gestureRecognizer.translation(in: self.view)
            println_debug(translation)
            //Move view as per translation
            gestureRecognizer.view!.center = CGPoint.init(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
            gestureRecognizer.setTranslation(CGPoint.init(x: 0, y: 0), in: self.view)
            //If view has been dragged to 85% of the screen height then remove the view
            if ((SelectCityCustomController?.frame.origin.y)! > self.view.frame.size.height * 0.85)
            {
                UIView.animate(withDuration: 0.2, animations: {
                    self.SelectCityCustomController?.frame.origin.y = self.view.frame.size.height
                    self.DarkView?.alpha = 0.30
                }) { (Bool) in
                    self.DarkView?.removeFromSuperview()
                    self.SelectCityCustomController?.removeFromSuperview()
                }
            }
            //If device is iPhone 5s or iPhone se
            if(self.view.frame.size.height) < 600{
                if (SelectCityCustomController?.frame.origin.y)! < self.view.frame.size.height * 0.40
                {
                    SelectCityCustomController?.frame =  CGRect(x: 0, y: self.view.frame.size.height * 0.40, width: self.view.frame.size.width, height: self.view.frame.size.height * 0.60)
                }
                
            }
            else //if device is not 4inch
            {
                if (SelectCityCustomController?.frame.origin.y)! < self.view.frame.size.height * 0.45
                {
                    SelectCityCustomController?.frame = CGRect(x: 0, y: self.view.frame.size.height * 0.45, width: self.view.frame.size.width, height: self.view.frame.size.height * 0.55)
                }
                
            }
            //Check if Drag is upwards or downwards and set alpha accordingly
            if(translation.y) > 0{
                DarkView?.alpha = (DarkView?.alpha)! - 0.005
            }
            else{   if (DarkView?.alpha)! > 0.85{ }
            else{   DarkView?.alpha = (DarkView?.alpha)! + 0.009 }
            }
        }
        if (gestureRecognizer.state == UIGestureRecognizer.State.ended){
            if ((SelectCityCustomController?.frame.origin.y)! < self.view.frame.size.height * 0.75){
                if(self.view.frame.size.height) < 600{
                    SelectCityCustomController?.frame =  CGRect(x: 0, y: self.view.frame.size.height * 0.40, width: self.view.frame.size.width, height: self.view.frame.size.height * 0.60)
                }
                else{
                    SelectCityCustomController?.frame = CGRect(x: 0, y: self.view.frame.size.height * 0.45, width: self.view.frame.size.width, height: self.view.frame.size.height * 0.55)
                }
                DarkView?.alpha = 0.85
            }
            else{
                UIView.animate(withDuration: 0.2, animations: {
                    self.SelectCityCustomController?.frame.origin.y = self.view.frame.size.height
                    self.DarkView?.alpha = 0.30
                }) { (Bool) in
                    self.DarkView?.removeFromSuperview()
                    self.SelectCityCustomController?.removeFromSuperview()
                }
            }
        }
    }
}


