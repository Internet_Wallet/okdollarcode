//
//  LLBinvoice.swift
//  OK
//
//  Created by iMac on 09/07/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class LLBinvoice: OKBaseController {
    
    let currentFormatter = DateFormatter()
    
    var strBillNo : String!
    var qrStr:String = ""
    var refundAmountX : String!
    var totalPayAmountX : String!
    var billMonthX : String!
    var qrImageObject :  PTQRGenerator?
    var DictToSend = Dictionary<String, AnyObject>()
    
    @IBOutlet weak var headerScrollView: UIScrollView!
    
    @IBOutlet weak var lblbusinessName: UILabel!{
        didSet {
            lblbusinessName.text = lblbusinessName.text?.localized
        }
    }
    
    @IBOutlet weak var lblbusinessNameValue: UILabel!{
        didSet {
            lblbusinessNameValue.text = lblbusinessNameValue.text?.localized
        }
    }
    
    @IBOutlet weak var lblsenderAccountName: UILabel!{
        didSet {
            lblsenderAccountName.text = lblsenderAccountName.text?.localized
        }
    }
    
    
    @IBOutlet weak var lblsenderAccountNameValue: UILabel!{
        didSet {
            lblsenderAccountNameValue.text = lblsenderAccountNameValue.text?.localized
        }
    }
    
    
    @IBOutlet weak var lblsenderAccountNum: UILabel!{
        didSet {
            lblsenderAccountNum.text = lblsenderAccountNum.text?.localized
        }
    }
    
    
    @IBOutlet weak var lblsenderAccountNumValue: UILabel!{
        didSet {
            lblsenderAccountNumValue.text = lblsenderAccountNumValue.text?.localized
        }
    }
    
    
    @IBOutlet weak var lblpayTelNum: UILabel!{
        didSet  {
            lblpayTelNum.text = lblpayTelNum.text?.localized
        }
    }
    
    
    @IBOutlet weak var lblpayTelNumValue: UILabel!{
        didSet  {
            lblpayTelNumValue.text = lblpayTelNumValue.text?.localized
        }
    }
    
    
    @IBOutlet weak var lblTransId: UILabel!{
        didSet {
            lblTransId.text = lblTransId.text?.localized
        }
    }
    
    @IBOutlet weak var lblTransIdValue: UILabel!{
        didSet   {
            lblTransIdValue.text = lblTransIdValue.text?.localized
        }
    }
    
    @IBOutlet weak var lblTransType: UILabel!{
        didSet  {
            lblTransType.text = lblTransType.text?.localized
        }
    }
    
    @IBOutlet weak var lblTransTypeValue: UILabel!{
        didSet {
            lblTransTypeValue.text = lblTransTypeValue.text?.localized
        }
    }
    
    @IBOutlet weak var lblPayBillMonth: UILabel!{
        didSet {
            lblPayBillMonth.text = lblPayBillMonth.text?.localized
        }
    }
    
    @IBOutlet weak var lblPayBillMonthValue: UILabel!{
        didSet {
            lblPayBillMonthValue.text = lblPayBillMonthValue.text?.localized
        }
    }
    
    @IBOutlet weak var lblDateTime: UILabel!{
        didSet  {
            lblDateTime.text = lblDateTime.text?.localized
        }
    }
    
    @IBOutlet weak var lblDateTimeValue: UILabel!{
        didSet {
            lblDateTimeValue.text = lblDateTimeValue.text?.localized
        }
    }
    
    @IBOutlet weak var lblBillAmount: UILabel!{
        didSet  {
            lblBillAmount.text = lblBillAmount.text?.localized
        }
    }
    
    @IBOutlet weak var lblBillAmountValue: UILabel!{
        didSet {
            lblBillAmountValue.text = lblBillAmountValue.text?.localized
        }
    }
    
    @IBOutlet weak var lblRefundAmount: UILabel!{
        didSet  {
            lblRefundAmount.text = lblRefundAmount.text?.localized
        }
    }
    
    @IBOutlet weak var lblRefundAmountValue: UILabel!{
        didSet  {
            lblRefundAmountValue.text = lblRefundAmountValue.text?.localized
        }
    }
    
    
    @IBOutlet weak var lblTotalPaidBill: UILabel!{
        didSet {
            lblTotalPaidBill.text = lblTotalPaidBill.text?.localized
        }
    }
    
    @IBOutlet weak var lblTotalPaidBillValue: UILabel!{
        didSet {
            lblTotalPaidBillValue.text = lblTotalPaidBillValue.text?.localized
        }
    }
    
    @IBOutlet weak var lblTellBillPayment: UILabel!{
        didSet  {
            lblTellBillPayment.text = lblTellBillPayment.text?.localized
        }
    }
    
    @IBOutlet weak var aQRImageView: UIImageView!
    @IBOutlet weak var bussinesNameHeightConstaraint : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //        DictToSend["Amount"] = 495 as? AnyObject
        //        DictToSend["ViewPaymentId"] = "87c86317-51aa-441a-afeb-391f8eaace39" as? AnyObject
        //        DictToSend["PaymentId"] = "879687f7-f34b-45c0-9c35-18f4e5ee90ab" as? AnyObject
        //        DictToSend["PaymentDate"] = "2020-07-06T13:16:47.1895803+06:30" as? AnyObject
        //        DictToSend["CustomerId"] = "3e9baa8b-574b-4f54-8f41-b673aeb9f5d6" as? AnyObject
        //        DictToSend["RefundDate"] = "" as? AnyObject
        //        DictToSend["PaymentStatus"] = "Success" as? AnyObject
        //        DictToSend["RefundOkTransactionId"] = "" as? AnyObject
        //        DictToSend["IsRefund"] = "" as? AnyObject
        //        DictToSend["OkTransactionId"] = "1744645472" as? AnyObject
        //        DictToSend["BillDetailId"] = "93cdfa6c-f7e6-4bb9-9d53-47e6ec472712" as? AnyObject
        //        DictToSend["IsPaymentForView"] = "0" as? AnyObject
        
        
        
        self.loadInvoiceValue(dict: self.DictToSend)
        setUpScrollView()
        //        self.loadStaticInvoiceValue()
        //        self.loadResponseValue()
        self.generateQRCodeAction(qrStr)
    }
    
    
    func setUpScrollView() {
        headerScrollView.isPagingEnabled = true
        headerScrollView.contentSize = CGSize(width: self.view.bounds.width, height: self.view.bounds.height)
        headerScrollView.showsHorizontalScrollIndicator = false
    }
    
    func loadInvoiceValue(dict: Dictionary<String,AnyObject>){
//        self.lblbusinessNameValue.text = ": \(UserModel.shared.businessName)"
        if let acountType = UserModel.shared.businessName as? String, acountType.count > 0 {
            self.lblbusinessNameValue.text = ": \(UserModel.shared.businessName)"
            self.bussinesNameHeightConstaraint.constant = 22
        }
        else {
            self.lblbusinessNameValue.text = ""
            self.bussinesNameHeightConstaraint.constant = 0
        }
        self.lblsenderAccountNameValue.text = ": \(UserModel.shared.name)"
        
        //print(appDel.getNetworkCode())
        
        self.lblsenderAccountNumValue.text =  ": \(UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "(+95)0"))"
        
        self.lblTransTypeValue.text = ": Landline Bill".localized
        
        self.lblpayTelNumValue.text  =  ": \(strBillNo ?? "")"
        
        if let transID = DictToSend["OkTransactionId"] as? String {
            //print(transID)
            self.lblTransIdValue.text = ": \(transID)"
        }
        
        if let amount = DictToSend["Amount"] as? String {
            //print(self.getDigitDisplay(amount))
            self.lblTotalPaidBillValue.text = " \(self.getDigitDisplay(amount)) MMK"
            
        } else if let amount = DictToSend["Amount"] as? NSNumber {
            //print(self.getDigitDisplay(String(describing: amount)))
            self.lblTotalPaidBillValue.text = " \(self.getDigitDisplay(String(describing: amount))) MMK"
        } else if let amount = DictToSend["Amount"] as? Int {
            //print(self.getDigitDisplay(String(amount)))
            self.lblTotalPaidBillValue.text = " \(self.getDigitDisplay(String(amount))) MMK"
        }
        
        
        self.lblRefundAmountValue.text = self.refundAmountX
        self.lblBillAmountValue.text = self.totalPayAmountX
        
        if let dateFromServer = DictToSend["PaymentDate"] as? String {
            self.lblDateTimeValue.text = ": \(convertDateFormatter(date: dateFromServer).0)".replacingOccurrences(of: ",", with: "")
            self.lblPayBillMonthValue.text = ": " + self.billMonthX
            
            //            if let date = self.getDate(serverDate: dateFromServer,format: "MMM dd,yyyy"){
            //                println_debug(date)
            //                self.lblDateTimeValue.text = ": \(convertDateFormatter(date: dateFromServer))"
            //            }
            //            if let monthYear = self.getDate(serverDate: dateFromServer,format: "MMMM YYYY"){
            //
            //                self.lblPayBillMonthValue.text = ": \(monthYear)"
            //            }
            
        }
    }
    
    
    
    //    func loadStaticInvoiceValue(){
    //        self.lblbusinessNameValue.text = "Mani COFFEE"
    //        self.lblsenderAccountNameValue.text = "Mr MANI"
    //        self.lblsenderAccountNumValue.text = "MPT (+95)098972441420"
    //        self.lblpayTelNumValue.text = "01-8202520"
    //        self.lblTransIdValue.text = "1742914714"
    //        self.lblTransTypeValue.text = "Landline Bill"
    //        self.lblPayBillMonthValue.text = "Jul-2020"
    //        self.lblDateTimeValue.text = "Thu 02-072020 14:09:27"
    //        self.lblBillAmountValue.text = "2,403 MMK"
    //        self.lblRefundAmountValue.text = "0 MMK"
    //        self.lblTotalPaidBillValue.text = "2,403 MMK"
    //    }
    
    
    
    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
}


extension LLBinvoice{
    func generateQRCodeAction(_ str: String) {
        qrImageObject = PTQRGenerator()
        guard qrImageObject != nil , let qrImage = qrImageObject else {
            return
        }
        
        guard let image =  qrImage.getQRImage(stringQR: str, withSize: 10) else {
            return
        }
        self.aQRImageView.image = image
    }
    
    
    
    func getQRImageForReceiptandPDF(from dict: [String: Any]) -> UIImage? {
        let phNumber = dict.safeValueForKey("destination") as? String ?? ""
        var transDateStr = ""
        if let trDateStr = dict.safeValueForKey("responsects") as? String {
            if let trDateValue = trDateStr.dateValue(dateFormatIs: "d-MMM-yyyy HH:mm:ss") {
                transDateStr = trDateValue.stringValue(dateFormatIs: "dd/MMM/yyyy HH:mm:ss")
            }
        }
        let senderName = UserModel.shared.name
        let senderNumber = UserModel.shared.mobileNo
        var receiverName: String? = nil
        var receiverBusinessName = dict.safeValueForKey("merchantname")
        if let com = dict.safeValueForKey("comments") as? String {
            let rNameArray = com.components(separatedBy: "##")
            if rNameArray.count >= 2 {
                if rNameArray[2] != "" {
                    receiverName = rNameArray[2]
                } else {
                    receiverBusinessName = rNameArray[1]
                }
            } else if rNameArray.count >= 1 {
                receiverBusinessName = rNameArray[1]
            }
        }
        
        let receiverNumber = phNumber
        let firstPartToEncrypt = "\(transDateStr)----\(phNumber)"
        let transID = dict.safeValueForKey("transid") ?? ""
        let trasnstype = dict.safeValueForKey("responsetype") ?? ""
        let lattitude = geoLocManager.currentLatitude ?? ""
        let longitude = geoLocManager.currentLongitude ?? ""
        let firstPartEncrypted = AESCrypt.encrypt(firstPartToEncrypt, password: "m2n1shlko@$p##d")
        let amount = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: (dict.safeValueForKey("amount") as? String ?? "0")) + " MMK"
        let state = UserModel.shared.state
        let gender = (UserModel.shared.gender == "1") ? "M" : "F"/////
        let age = UserModel.shared.ageNow /////
        let balance = "" /////
        let cashBackAmount = "" /////
        let bonus = "" /////
        
        let str2 = "OK-\(senderName)-\(amount)-\(transDateStr)-\(transID)-\(senderNumber)-\(balance)-des-\(receiverNumber)-\(trasnstype)-\(cashBackAmount)-\(bonus)-\(lattitude),\(longitude)-0-\(gender)-\(age)-\(state)-\(receiverName ?? "")-\(receiverBusinessName ?? "")"
        
        let secondEncrypted = AESCrypt.encrypt(str2, password: firstPartToEncrypt)
        
        var firstPartEncryptedSafeValue = ""
        var secondEncryptedSafeValue = ""
        if let firstEncrypted = firstPartEncrypted {
            firstPartEncryptedSafeValue = firstEncrypted
        }
        if let secondEncrypt = secondEncrypted {
            secondEncryptedSafeValue = secondEncrypt
        }
        let finalEncyptFormation = "\(firstPartEncryptedSafeValue)---\(secondEncryptedSafeValue)"
        
        let qrImageObject = PTQRGenerator()
        let qrImage =  qrImageObject.getQRImage(stringQR: finalEncyptFormation, withSize: 1)
        return qrImage
    }
    
    
    /*
     func loadResponseValue() {
     //print("01-\(strBillNo!.dropFirst())")
     
     self.qrStr  = "Recharge Number-" + "01-\(strBillNo!.dropFirst())"
     
     if let transID = DictToSend["OkTransactionId"] as? String {
     //print(transID)
     self.qrStr += "Transaction id-\(transID)"
     }
     
     if let amount = DictToSend["Amount"] as? String {
     //print(self.getDigitDisplay(amount))
     self.qrStr += "\(self.getDigitDisplay(amount))"
     } else if let amount = DictToSend["Amount"] as? NSNumber {
     //print(self.getDigitDisplay(String(describing: amount)))
     self.qrStr += (self.getDigitDisplay(String(describing: amount)))
     } else if let amount = DictToSend["Amount"] as? Int {
     //print(self.getDigitDisplay(String(amount)))
     self.qrStr += (self.getDigitDisplay(String(amount)))
     }
     
     if let dateFromServer = DictToSend["PaymentDate"] as? String {
     if let date = self.getDate(serverDate: dateFromServer, format: "MMM dd,yyyy"){
     println_debug(date)
     self.qrStr += "date-\(date)"
     }
     //print(convertDateFormatter(date: dateFromServer))
     self.qrStr += " \(convertDateFormatter(date: dateFromServer))"
     }
     }
     */
    
    
    func convertDateFormatter(date: String) -> (String , String){
        
        var date1 = ""
        var date2 = ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
        let dateVal = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "EE, dd-MM-yyyy, hh:mm:ss a"///this is what you want to convert format
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.calendar = Calendar(identifier: .gregorian)
        dateFormatter1.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
        let dateVal1 = dateFormatter1.date(from: date)
        dateFormatter1.dateFormat = "MMM-yyyy"///this is what you want to convert format
        
        if let dateFormat1 = dateVal{
            let timeStampA = dateFormatter.string(from: dateFormat1)
            if timeStampA.count>0{
                date1 = timeStampA
            }
        }
        if let dateFormat2 = dateVal1{
            let timeStampB = dateFormatter1.string(from: dateFormat2)
            if timeStampB.count>0{
                date2 = timeStampB
            }
        }
        return (date1,date2)
    }
    
    
    func getDate(serverDate : String,format: String) -> String? {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.calendar = Calendar(identifier: .gregorian)
        dateFormatterPrint.dateFormat = format
        dateFormatterPrint.setLocale()
        
        let date: Date? = currentFormatter.date(from: serverDate)
        if let dateStr = date {
            return dateFormatterPrint.string(from: dateStr)
        }
        return ""
    }
    
    
    
    
    
    
    
    
    @IBAction func shareClick(_ sender: UIBarButtonItem) {
        let pdfUrl = self.createPdfFromView(aView: self.headerScrollView, saveToDocumentsWithFileName: "Receipt".localized)
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl!], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    
    
    func createPdfFromView(aView: UIScrollView, saveToDocumentsWithFileName pdfFileName: String) -> URL? {
        let render = UIPrintPageRenderer()
        let page = CGRect(x: 0, y: 0, width: 595.2, height: 841) // A4, 72 dpi
        let printable = page.insetBy(dx: 0, dy: 0)
        render.setValue(NSValue(cgRect: page), forKey: "paperRect")
        render.setValue(NSValue(cgRect: printable), forKey: "printableRect")
        let priorBounds = aView.bounds
        let fittedSize = aView.sizeThatFits(CGSize(width:priorBounds.size.width, height:aView.bounds.size.height))
        aView.bounds = CGRect(x:0, y:0, width:fittedSize.width, height:fittedSize.height)
        let pdfPageBounds = CGRect(x:0, y:0, width:aView.frame.width, height:self.view.frame.height)
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds,nil)
        let pageOriginY: CGFloat = 0
        UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
        UIGraphicsGetCurrentContext()!.saveGState()
        UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
        aView.layer.render(in: UIGraphicsGetCurrentContext()!)
        UIGraphicsGetCurrentContext()!.restoreGState()
        UIGraphicsEndPDFContext()
        aView.bounds = priorBounds
        guard let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else  { return nil}
        let properPDFFileName = pdfFileName.replacingOccurrences(of: ".pdf", with: "") + ".pdf"
        let pdfUrl = documentDirectories + "/" + properPDFFileName
        //Delete file if alreay exists
        do {
            try FileManager.default.removeItem(atPath: pdfUrl)
        } catch let error as NSError {
            println_debug("Error: \(error.domain)")
        }
        //Write file
        if pdfData.write(toFile: pdfUrl, atomically: true) {
            println_debug("Successfully wrote to \(pdfUrl)")
            let pathUrl = URL(fileURLWithPath: documentDirectories)
            let fileUrl = pathUrl.appendingPathComponent(properPDFFileName)
            return fileUrl
        }
        return nil
    }
    
}
