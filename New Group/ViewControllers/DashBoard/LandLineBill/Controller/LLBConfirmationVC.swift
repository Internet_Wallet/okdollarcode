//
//  LLBConfirmationVC.swift
//  OK
//
//  Created by Rahul Tyagi on 10/17/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit

class LLBConfirmationVC: OKBaseController{
    
    let jsonParserObj = RTJsonParser()
    var labelArray = ["City".localized,"Landline Number".localized,"Bill Month".localized,"Total Bill Amount".localized, "Total Payable Amount".localized]
    var valueArray : [String] = []
    var DictToSend = Dictionary<String, AnyObject>()
    var rfndAmount : String = "0"
    var ttlPayAmount : String = "0"
    var billingMonth : String = ""
    
    @IBOutlet var city: UILabel!{
        didSet
        {
            city.text = city.text?.localized
        }
    }
    @IBOutlet var landNum: UILabel!{
        didSet
        {
            landNum.text = landNum.text?.localized
        }
    }
    @IBOutlet var billmonth: UILabel!{
        didSet
        {
            billmonth.text = billmonth.text?.localized
        }
    }
    @IBOutlet var totbillAmt: UILabel!{
        didSet
        {
            totbillAmt.text = totbillAmt.text?.localized
        }
    }
    @IBOutlet var payAmt: UILabel!{
        didSet
        {
            payAmt.text = payAmt.text?.localized
        }
    }
    @IBOutlet var BtnPay: UIButton!{
        didSet
        {
            BtnPay.setTitle(BtnPay.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var lblCity: UILabel!{
        didSet
        {
            lblCity.text = lblCity.text?.localized
        }
    }
    @IBOutlet weak var lblTeleNumber: UILabel!{
        didSet
        {
            lblTeleNumber.text = lblTeleNumber.text?.localized
        }
    }
    @IBOutlet weak var lblBillMonth: UILabel!{
        didSet
        {
            lblBillMonth.text = lblBillMonth.text?.localized
        }
    }
    @IBOutlet weak var lblTotalBillAmt: UILabel!{
        didSet
        {
            lblTotalBillAmt.text = lblTotalBillAmt.text?.localized
        }
    }
    @IBOutlet weak var lblRefundAmt: UILabel!{
        didSet
        {
            lblRefundAmt.text = lblRefundAmt.text?.localized
        }
    }
    @IBOutlet weak var lblRefundAmtValue: UILabel!{
        didSet
        {
            lblRefundAmtValue.text = lblRefundAmtValue.text?.localized
        }
    }
    @IBOutlet weak var lblPayAmt: UILabel!{
        didSet
        {
            lblPayAmt.text = lblPayAmt.text?.localized
        }
    }
    @IBOutlet weak var lblPayAmtValue: UILabel!{
        didSet
        {
            lblPayAmtValue.text = lblPayAmtValue.text?.localized
        }
    }
    
    @IBOutlet weak var refundLbl : NSLayoutConstraint!
    @IBOutlet weak var refundAmountLbl : NSLayoutConstraint!
    @IBOutlet weak var colonRefund : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        jsonParserObj.delegate = self
        self.title = "Landline Bill".localized
        println_debug(DictToSend)
        self.initConfigure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
        
    }
    
}
extension  LLBConfirmationVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let customCell : LLBConfirmationTableCell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! LLBConfirmationTableCell
        customCell.labelOutlet.text = labelArray[indexPath.row].localized
        customCell.valueOutlet.text = valueArray[indexPath.row].localized //as? String
        return customCell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  labelArray.count
    }
}
extension  LLBConfirmationVC: RTJsonParserDelegate{
    func ResponseFromServer(withSuccess Success: Bool, json: AnyObject?, screen: String, Error: String?){
        println_debug("screen PayAPI:- \(screen)")
        println_debug("Success :- \(Success)")
        if Success == false{
            DispatchQueue.main.async{
                progressViewObj.removeProgressView()
            }
        }
        if Success == true && screen == "TokenForPayAPI"{
            let responseDic = json as! NSDictionary
            let tokentype = responseDic.value(forKey: "token_type")
            let accesstoken = responseDic.value(forKey: "access_token")
            let authorizationString =  "\(tokentype!) \(accesstoken!)"
            let BillDetailDict = (DictToSend["BillDetail"] as! Dictionary<String, Any>)
            let pipeLineStr = "OkAccountNumber=\(LLBStrings.okAccountNum)|Password=\(ok_password!)|BillDetailId=\(BillDetailDict["BilDetailId"]!)"
            println_debug("pipeline str :: \(pipeLineStr)")
            let hashValue: String = pipeLineStr.hmac_SHA1(key: LLBStrings.sKey)
            println_debug("actual hash value :: \(hashValue)")
            let keys : [String] = ["OkAccountNumber","Password","BillDetailId","HashValue"]
            let values : [String] = [LLBStrings.okAccountNum,ok_password!,BillDetailDict["BilDetailId"] as! String,hashValue]
            let jsonObj = JSONStringWriter()
            let jsonText = jsonObj.writeJsonIntoString(keys, values)
            let PayURL = URL.init(string: LLBStrings.kPayAPIURL)
            DispatchQueue.main.async{
                progressViewObj.showProgressView()
                println_debug(PayURL!)
                println_debug(jsonText as AnyObject)
                println_debug(LLBStrings.kContentType_Json)
                println_debug(authorizationString)
                self.jsonParserObj.ParseMyJson(url: PayURL!, param: jsonText as AnyObject, httpMethod: LLBStrings.kMethod_Post, contentType: LLBStrings.kContentType_Json, mScreen: "PayAPI", authStr: authorizationString)
            }
        }
        else if Success == true && screen == "PayAPI" {
            if json != nil{
                if let responseDic = json as? NSDictionary{
                    println_debug("screen PayAPI:- \(screen)")
                    println_debug(responseDic["Data"]!)
                    DispatchQueue.main.async {
                        progressViewObj.removeProgressView()
                        let payVCObj = LLBStrings.FileNames.llbStoryBoardObj.instantiateViewController(withIdentifier: "LLBReceiptVC") as! LLBReceiptVC
                        if let responseDict =  OKBaseController.convertToDictionary(text: responseDic["Data"]! as? String ?? "" ) {
                            payVCObj.DictToSend = responseDict as [String : AnyObject]
                            let dict = self.DictToSend.safeValueForKey("BillDetail") as! Dictionary<String, AnyObject>
                            payVCObj.strBillNo = (dict.safeValueForKey("Number") as! String).localized
                            payVCObj.refundAmount = self.rfndAmount
                            payVCObj.totalPayAmount = self.ttlPayAmount
                            payVCObj.billMonth = self.billingMonth
                            
                            self.navigationController?.pushViewController(payVCObj, animated: true)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        progressViewObj.removeProgressView()
                        alertViewObj.wrapAlert(title: "", body: json as! String, img: #imageLiteral(resourceName: "landlineImg"))
                        alertViewObj.addAction(title: "Done".localized, style: .target, action:{})
                        alertViewObj.showAlert(controller: self)
                    }
                }
            }
            else{
                DispatchQueue.main.async{
                    progressViewObj.removeProgressView()
                    alertViewObj.wrapAlert(title: "", body: "Error".localized, img:  #imageLiteral(resourceName: "error"))
                    alertViewObj.addAction(title: "OK".localized, style: .cancel){}
                    alertViewObj.showAlert(controller: self)
                }
            }
        }
    }
}
extension  LLBConfirmationVC{
    
    func initConfigure(){
        
        guard  let dict = DictToSend["BillDetail"] as? Dictionary<String, AnyObject> else { return }
        self.lblCity.text = ((dict["SourceDescription"] as? String))
        self.lblTeleNumber.text = ("0".appending(dict["Number"] as! String))
        let helperObj = RTHelperClass()
        let BillingMonth = String(describing: dict["BillingMonth"]!)
        let BillingYear = String(describing: dict["BillingYear"]!)
        let dateToPass = BillingMonth + "-" + BillingYear
        let convertedValue = helperObj.formattedDateFromString(dateString: dateToPass, withFormat: "MM-yy", toBeConvertedToFormat: "MMMM YYYY")
        self.lblBillMonth.text = convertedValue
        self.lblTotalBillAmt.text = ("\( self.getDigitDisplay(String(describing: DictToSend["TotalBillAmount"] ?? "" as AnyObject)).appending(" MMK"))")
        self.billingMonth = "\(DateFormatter().monthSymbols[(Int(BillingMonth) ?? 0) - 1]) 20\(BillingYear)"
        if let refundAmount = DictToSend["ViewOnlyAmount"] as? Int64 , refundAmount > 0{
            self.colonRefund.constant = 25
            self.refundLbl.constant = 25
            self.refundAmountLbl.constant = 25
            self.lblRefundAmtValue.text = ("\(self.getDigitDisplay(String(describing: DictToSend["ViewOnlyAmount"] ?? "" as AnyObject)).appending(" MMK"))")
            self.rfndAmount = self.lblRefundAmtValue.text ?? "0 MMK"
            if let total = dict["TotalAmountMmk"]?.int64Value {
                if let paid = DictToSend["ServiceFee"]?.int64Value {
                    let totalChargeBillAmount = ((total + paid) - refundAmount )
                    self.lblPayAmtValue.text = ("\( self.getDigitDisplay("\(totalChargeBillAmount)").appending(" MMK"))")
                    self.ttlPayAmount = ("\( self.getDigitDisplay("\((total + paid))").appending(" MMK"))")
                    println_debug("Inside")
                }
            }
        }
        else {
            self.colonRefund.constant = 0
            self.refundLbl.constant = 0
            self.refundAmountLbl.constant = 0
            self.lblRefundAmtValue.text = ""
            if let total = dict["TotalAmountMmk"]?.int64Value {
                if let paid = DictToSend["ServiceFee"]?.int64Value {
                    let totalChargeBillAmount = (total + paid)
                    self.lblPayAmtValue.text = ("\( self.getDigitDisplay("\(totalChargeBillAmount)").appending(" MMK"))")
                    self.ttlPayAmount = self.lblPayAmtValue.text ?? "0 MMK"
                    println_debug("Inside")
                }
            }
        }
        
    }
    func getBillMonth(serverValue: Int) -> String{
        let monthNumber = serverValue
        let fmt = DateFormatter()
        fmt.calendar = Calendar(identifier: .gregorian)
        fmt.dateFormat = "MM"
        fmt.setLocale()
        let month = fmt.monthSymbols[monthNumber - 1]
        return month
    }
    @IBAction func PayBtnAction(_ sender: Any){
        //
        let hashValue = LLBStrings.aKey.hmac_SHA1(key: LLBStrings.sKey)
        let inputValue = "password=\(hashValue)&grant_type=password"
        progressViewObj.showProgressView()
        jsonParserObj.ParseMyJson(url: URL.init(string: LLBStrings.kTokenAPIURL)! , param: inputValue as AnyObject, httpMethod: LLBStrings.kMethod_Post, contentType: LLBStrings.kContentType_urlencoded, mScreen: "TokenForPayAPI", authStr: nil)
        
        
        
        
        
        
    }
    @IBAction func btnBackClick(_ sender: UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }
}
