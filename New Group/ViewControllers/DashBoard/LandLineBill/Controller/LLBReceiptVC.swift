//
//  LLBReceiptVC.swift
//  OK
//
//  Created by Rahul Tyagi on 10/17/17.
//  Copyright © 2017 Cgm. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI
fileprivate enum TabBarItem : Int {
    case fav
    case home
    case addContact
    case more
}

class LLBReceiptVC: OKBaseController {

    var DictToSend = Dictionary<String, AnyObject>()
    var strBillNo : String!
    var refundAmount : String!
    var totalPayAmount : String!
    var billMonth : String!
    let currentFormatter = DateFormatter()
    var isShowAlert = true
    @IBOutlet weak var lblLocation: UILabel!{
        didSet
        {
            lblLocation.text = lblLocation.text?.localized
        }
    }
    @IBOutlet weak var lblBillId: UILabel!{
        didSet
        {
            lblBillId.text = lblBillId.text?.localized
        }
    }
    @IBOutlet weak var lblAmount: UILabel!{
        didSet
        {
            lblAmount.text = lblAmount.text?.localized
        }
    }
    @IBOutlet weak var lbTransactionId: UILabel!{
        didSet
        {
            lbTransactionId.text = lbTransactionId.text?.localized
        }
    }
    @IBOutlet weak var lbTransactionTY: UILabel!{
        didSet
        {
            lbTransactionTY.text = lbTransactionTY.text?.localized
        }
    }
    
    @IBOutlet weak var lblTellBill: UILabel!{
        didSet
        {
            lblTellBill.text = lblTellBill.text?.localized
        }
    }
    
    @IBOutlet weak var lblTransactionId: UILabel!{
        didSet
        {
            lblTransactionId.text = lblTransactionId.text?.localized
        }
    }
    @IBOutlet weak var lblTransactionType: UILabel!{
        didSet
        {
            lblTransactionType.text = lblTransactionType.text?.localized
        }
    }
    @IBOutlet weak var lblDate: UILabel!{
        didSet
        {
            lblDate.text = lblDate.text?.localized
        }
    }
    @IBOutlet weak var lblTime: UILabel!{
        didSet
        {
            lblTime.text = lblTime.text?.localized
        }
    }
    @IBOutlet var btnRepayment: UIButton!{
        didSet
        {
            btnRepayment.setTitle(btnRepayment.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var btnMorePayment: UIButton!{
        didSet
        {
            btnMorePayment.setTitle(btnMorePayment.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var btnInvoice: UIButton!{
        didSet
        {
            btnInvoice.setTitle(btnInvoice.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var btnShare: UIButton!{
        didSet
        {
            btnShare.setTitle(btnShare.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet var rTabBar: UITabBar!
    @IBOutlet var fav: UITabBarItem!
    @IBOutlet var home: UITabBarItem!
    @IBOutlet var addContact: UITabBarItem!
    @IBOutlet var more: UITabBarItem!
    
    @IBOutlet weak var childView: UIView!
    @IBOutlet var transactionView: UIView!
    @IBOutlet weak var parentView: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        self.parentView.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Landline Bill".localized
        
        let color = UIColor.init(hex: "162D9F")
        let attrFont = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.0) ?? UIFont.systemFont(ofSize: 11.0), NSAttributedString.Key.foregroundColor: color]
        
        self.home.setTitleTextAttributes(attrFont, for: .normal)
        self.more.setTitleTextAttributes(attrFont, for: .normal)
        self.addContact.setTitleTextAttributes(attrFont, for: .normal)
        self.fav.setTitleTextAttributes(attrFont, for: .normal)
        
        self.fav.title = "Add Favorite".localized
        self.home.title = "Home".localized
        self.addContact.title = "Share".localized
        self.more.title = "More".localized
        
        
        self.childView.isHidden = true
        currentFormatter.calendar = Calendar(identifier: .gregorian)
        currentFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        currentFormatter.setLocale()
        println_debug(DictToSend)
        self.loadResponseValue()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissView))
        tap.numberOfTapsRequired = 1
        self.childView.addGestureRecognizer(tap)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .normal)
        UITabBar.appearance().unselectedItemTintColor = .lightGray
    }
    
    @objc func dismissView() {
        self.childView.isHidden = true
    }
    
    func loadResponseValue() {
        self.lblBillId.text = "01-\(strBillNo!.dropFirst())"
        if let amount = DictToSend["Amount"] as? String {
            self.lblAmount.text =  self.getDigitDisplay(amount)
        } else if let amount = DictToSend["Amount"] as? NSNumber {
            self.lblAmount.text =  self.getDigitDisplay(String(describing: amount))
        } else if let amount = DictToSend["Amount"] as? Int {
            self.lblAmount.text =  self.getDigitDisplay(String(amount))
        }
        if let transID = DictToSend["OkTransactionId"] as? String {
            self.lblTransactionId.text = transID
        }
        
        if let dateFromServer = DictToSend["PaymentDate"] as? String {
            if let date = self.getDate(serverDate: dateFromServer){
                println_debug(date)
                self.lblDate.text = date
            }
//         //   if let time = self.getTime(serverDate: dateFromServer){
//                let value = convertDateFormatter(date: dateFromServer)
//                if value.count>0{
//                    let actualValue = value.components(separatedBy: ",")
//                    print(actualValue)
//                    if actualValue[2].count>0{
                        self.lblTime.text = convertDateFormatter(date: dateFromServer)
                  //  }
               // }
         //   }
        }
    }
    
    
    func convertDateFormatter(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
        let dateVal = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "EE, dd-MM-yyyy, hh:mm a"///this is what you want to convert format
        if let newDate = dateVal{
            let timeStamp = dateFormatter.string(from: newDate)
            if timeStamp.count>0{
                let actualValue = timeStamp.components(separatedBy: ",")
                if actualValue.count>0{
                    if actualValue.indices.contains(2){
                        return actualValue[2]
                    }
                }else{
                    return ""
                }
            }
        }else{
            let payDate = date.components(separatedBy: "T")
            if payDate.count>0{
                if payDate.indices.contains(1){
                    let newPayDate = payDate[1].components(separatedBy: ".")
                    if newPayDate[0].count>0{
                        return newPayDate[0]
                    }else{
                        return ""
                    }
                }
            }
        }
        return ""
    }
    
    func getDate(serverDate : String) -> String? {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.calendar = Calendar(identifier: .gregorian)
        dateFormatterPrint.dateFormat = "MMM dd,yyyy"
        dateFormatterPrint.setLocale()
        
        let date: Date? = currentFormatter.date(from: serverDate)
        if let dateStr = date {
            return dateFormatterPrint.string(from: dateStr)
        }
        return ""
    }
    
    func getTime(serverDate : String) -> String? {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.calendar = Calendar(identifier: .gregorian)
        dateFormatterPrint.dateFormat = "HH:mm:ss"
        dateFormatterPrint.setLocale()
        let time: Date? = currentFormatter.date(from: serverDate)
        if let timeStr = time {
            return dateFormatterPrint.string(from: timeStr)
        }
        return ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackClick(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareClick(_ sender: UITabBarItem) {
        let pdfUrl = self.createPdfFromView(aView: transactionView, saveToDocumentsWithFileName: "Transaction Receipt".localized)
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl!], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    
    @IBAction func newBackButton(_ sender: Any) {
        userDef.set(true, forKey: "makeAnotherPayment")
        userDef.synchronize()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    @IBAction func btnRepayment(_ sender: UIButton){
        userDef.set(true, forKey: "makeAnotherPayment")
        userDef.synchronize()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnInvoiceClick(_ sender: UIButton){
        guard let pdfUrl = CodeSnippets.generatePDF(viewForPDF: self.transactionView, pdfFile: "OK$ LandLine Bill Receipt") else { return }
        let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
        performSegue(withIdentifier: "finalreceipt", sender: self)
         
    }
     
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "finalreceipt" {
            if let controller = segue.destination as? LLBinvoice {
                controller.strBillNo = "01-\(strBillNo!.dropFirst())"
                controller.DictToSend = self.DictToSend
                controller.refundAmountX = self.refundAmount
                controller.totalPayAmountX = self.totalPayAmount
                controller.billMonthX = self.billMonth
                
            }
        }
    }
    
    
}

//MARK:- PTFavoriteActionDelegate
extension LLBReceiptVC: PTFavoriteActionDelegate {
    func favoriteAdded() {
         self.isShowAlert = false
    }
}
extension LLBReceiptVC: UITabBarDelegate{
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch(item) {
        case fav        : self.btnAddFavoriteClick(item)
        case home       : self.btnHomeClick(item)
        case addContact : self.shareClick(item)    // self.btnAddContactClick(item)
        case more       : self.btnShareClick(item)
        default: break
        }
    }
}
extension LLBReceiptVC {
    func captureScreen() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, UIScreen.main.scale)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    @IBAction func btnShareClick(_ sender: UITabBarItem){
        self.childView.isHidden = false
        self.parentView.isHidden = false
    }
   
    @IBAction func btnHomeClick(_ sender: UITabBarItem){
        NotificationCenter.default.post(name: .goDashboardScreen, object: nil)
        performSegue(withIdentifier: "unwindSegueToVC1", sender: self)
    }
   
    @IBAction func btnAddFavoriteClick(_ sender: UITabBarItem){
        println_debug("Add to Favorite")
        if isShowAlert {
            if let amountData = DictToSend["Amount"] as? Int {
                  let vc = self.addFavoriteController(withName: "Unknown",favNum: "01-\(8201366)",type: "LANDLINE", amount: self.getDigitDisplay(String(200)))
                  if let vcs = vc {
                    vcs.delegate = self
                      self.present(vcs, animated: true, completion: nil)
                  }
              }
        }else {
          // show alert
            self.showToastlocal(message: "Contact already added in Favorites".localized)
            
        }
  
    }
    
    
    func showToastlocal(message : String) {
        let toastLabel = UILabel()
        toastLabel.frame = CGRect(x: 20 , y: (self.view.frame.height-150), width: self.view.frame.width-40, height: 60)
        toastLabel.text = message
        toastLabel.numberOfLines = 0
        toastLabel.font = UIFont(name: appFont,size: 14.0)
        toastLabel.textAlignment = .center
        toastLabel.numberOfLines = 2
        toastLabel.layer.cornerRadius = 5
        toastLabel.layer.masksToBounds = true
        toastLabel.textColor = .white
        toastLabel.backgroundColor = UIColor.gray
        self.view.bringSubviewToFront(toastLabel)
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 5.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    @IBAction func btnAddContactClick(_ sender: UITabBarItem){
        showNewContactViewController()
    }
    
    func createPdfFromView(aView: UIView, saveToDocumentsWithFileName pdfFileName: String) -> URL? {
        let render = UIPrintPageRenderer()
        let page = CGRect(x: 0, y: 0, width: 595.2, height: 841) // A4, 72 dpi
        let printable = page.insetBy(dx: 0, dy: 0)
        render.setValue(NSValue(cgRect: page), forKey: "paperRect")
        render.setValue(NSValue(cgRect: printable), forKey: "printableRect")
        let priorBounds = aView.bounds
        let fittedSize = aView.sizeThatFits(CGSize(width:priorBounds.size.width, height:aView.bounds.size.height))
        aView.bounds = CGRect(x:0, y:0, width:fittedSize.width, height:fittedSize.height)
        let pdfPageBounds = CGRect(x:0, y:0, width:aView.frame.width, height:self.view.frame.height)
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds,nil)
        let pageOriginY: CGFloat = 0
        UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
        UIGraphicsGetCurrentContext()!.saveGState()
        UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
        aView.layer.render(in: UIGraphicsGetCurrentContext()!)
        UIGraphicsGetCurrentContext()!.restoreGState()
        UIGraphicsEndPDFContext()
        aView.bounds = priorBounds
        guard let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else  { return nil}
        let properPDFFileName = pdfFileName.replacingOccurrences(of: ".pdf", with: "") + ".pdf"
        let pdfUrl = documentDirectories + "/" + properPDFFileName
        //Delete file if alreay exists
        do {
            try FileManager.default.removeItem(atPath: pdfUrl)
        } catch let error as NSError {
            println_debug("Error: \(error.domain)")
        }
        //Write file
        if pdfData.write(toFile: pdfUrl, atomically: true) {
            println_debug("Successfully wrote to \(pdfUrl)")
            let pathUrl = URL(fileURLWithPath: documentDirectories)
            let fileUrl = pathUrl.appendingPathComponent(properPDFFileName)
            return fileUrl
        }
        return nil
    }
    
}
extension LLBReceiptVC : CNContactViewControllerDelegate {
    func showNewContactViewController(){
        let contactViewController: CNContactViewController = CNContactViewController(forNewContact: nil)
        contactViewController.contactStore = CNContactStore()
        contactViewController.delegate = self
        let navigationController: UINavigationController = UINavigationController(rootViewController: contactViewController)
        present(navigationController, animated: false){
            println_debug("Present")
        }
    }
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?){
        self.dismiss(animated: true, completion: nil)
    }
    func contactViewController(_ viewController: CNContactViewController, shouldPerformDefaultActionFor property: CNContactProperty) -> Bool{
        return true
    }
}
extension LLBReceiptVC :  UIImagePickerControllerDelegate  {
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            alertViewObj.wrapAlert(title: "Error".localized, body: error.localizedDescription, img:  #imageLiteral(resourceName: "IT") )
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {})
            alertViewObj.showAlert(controller: self)
        } else {
            alertViewObj.wrapAlert(title: "Saved!".localized, body: "Image saved successfully".localized, img: #imageLiteral(resourceName: "IT") )
            alertViewObj.addAction(title: "OK".localized, style: AlertStyle.target, action: {})
            alertViewObj.showAlert(controller: self)
        }
    }
}
extension UIApplication {
    class var LLBReceiptVC: OKBaseController? {
        return getTopViewController() as? OKBaseController
    }
    private class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return getTopViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}
extension Equatable {
    func share(activityItem: UIImage){
        let activity = UIActivityViewController(activityItems:  [activityItem], applicationActivities: nil)
        UIApplication.topViewController?.present(activity, animated: true, completion: nil)
    }
}
