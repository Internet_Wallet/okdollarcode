//
//  LLBNavController.swift
//  OK
//
//  Created by Rahul Tyagi on 10/25/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class LLBNavController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.topViewController?.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.topViewController?.navigationController?.navigationBar.barTintColor = LLBStrings.yellowAppColor
        self.topViewController?.navigationItem.title = "Landline Bill".localized
        self.topViewController?.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
