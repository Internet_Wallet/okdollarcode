//
//  LLBCheckHistoryStatus.swift
//  OK
//
//  Created by Rahul Tyagi on 10/20/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class LLBCheckHistoryStatus: NSObject{
    enum historyStatusOptions {
        case showGetBill, showHistory, showAlert
    }
    var historyStatus : historyStatusOptions?
    var HistoryModelObj = Array<HistoryVCDataModel>()
    func requestAPI(requestAPICompletionHandler: @escaping (_ StatusResponse : historyStatusOptions, _ ErrorString : String?, _ historyDataModelObj : [HistoryVCDataModel]?) -> Void){
        getTokenFromServer { (isSuccess, authString, TokenAPIErrorMsg) in
            if isSuccess == true{
                let urlString = String.init(format: LLBStrings.kLLBHistoryAPIURL, LLBStrings.okAccountNum)
                self.getBillHistory(apiUrl: URL.init(string: urlString)!, authenticationStr: authString as! String, HistoryAPICompletionHandler: { (historyStatusFromGetBillHistoryAPI, ErrorMessageFromGetBillHistoryAPI, historyDataModelObjFromAPI) in
                    requestAPICompletionHandler(historyStatusFromGetBillHistoryAPI, ErrorMessageFromGetBillHistoryAPI, historyDataModelObjFromAPI)
                })
            }
            else{
                requestAPICompletionHandler(.showAlert, TokenAPIErrorMsg, nil)
            }
        }
    }
    func getTokenFromServer(TokenCompletionHandler: @escaping (_ gotToken : Bool, _ TokenString: Any?, _ TokenAPIError : String?) -> Void){
        guard let tokenUrl = URL.init(string: LLBStrings.kTokenAPIURL) else{
            TokenCompletionHandler(false,nil, LLBStrings.ErrorMessages.urlError)
            return
        }
        let hashValue = LLBStrings.aKey.hmac_SHA1(key: LLBStrings.sKey)
        let inputValue = "password=\(hashValue)&grant_type=password"
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: tokenUrl,methodType: LLBStrings.kMethod_Post, contentType: LLBStrings.kContentType_urlencoded, inputVal: inputValue, authStr: nil)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            guard isSuccess else{
                println_debug("Token API Fail")
                TokenCompletionHandler(false,nil, LLBStrings.ErrorMessages.TryAgainErrorMsg)
                return
            }
            println_debug("\n\n\n Token Response dict : \(response!)")
            let responseDic = response as! NSDictionary
            guard let tokentype = responseDic.value(forKey: "token_type"), let accesstoken = responseDic.value(forKey: "access_token") else {
                TokenCompletionHandler(false,nil,LLBStrings.ErrorMessages.TryAgainErrorMsg)
                return
            }
            let authorizationString =  "\(tokentype) \(accesstoken)"
            TokenCompletionHandler(true,authorizationString,nil)
        })
    }
    func getBillHistory(apiUrl: URL, authenticationStr: String, HistoryAPICompletionHandler: @escaping (_ StatusResponse : historyStatusOptions, _ ErrorString : String?, _ dataModel : [HistoryVCDataModel]?) -> Void){
        // let hashValue = LLBStrings.aKey.hmac_SHA1(key: LLBStrings.sKey)
       // let inputValue = "password=\(hashValue)&grant_type=password"
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: apiUrl, methodType: LLBStrings.kMethod_Get, contentType: LLBStrings.kContentType_Json, inputVal: "", authStr: authenticationStr)
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            guard isSuccess else {
                HistoryAPICompletionHandler(.showGetBill, nil,nil)
                return
            }
            if let res = response as? NSArray {
                for item in res {
                    if let dic = item as? Dictionary<String,Any> {
                        if !dic["PhoneNumber"].safelyWrappingString().hasPrefix("9"){
                            let modelObj = HistoryVCDataModel.init(_customerID: "\(dic["CustomerId"]!)" ,
                                _LastBillAmount: "\(dic["LastBillAmount"]!)" ,
                                _LastBillingMonth: "\(dic["LastBillingMonth"]!)",
                                _LastBillingYear: "\(dic["LastBillingYear"]!)",
                                _LastPaymentDate: "\(dic["LastPaymentDate"]!)",
                                _PhoneNumber: "\(dic["PhoneNumber"]!)",
                                _SourceDescription: "\(dic["SourceDescription"]!)")
                            self.HistoryModelObj.append(modelObj)
                        }
                    }
                }
                if self.HistoryModelObj.count > 0{HistoryAPICompletionHandler(.showHistory, nil,self.HistoryModelObj)}else{HistoryAPICompletionHandler(.showGetBill, nil,self.HistoryModelObj)}
            }
        })
    }
}
