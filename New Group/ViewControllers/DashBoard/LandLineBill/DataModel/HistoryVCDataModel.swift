//
//  HistoryVCDataModel.swift
//  OK
//
//  Created by Rahul Tyagi on 10/21/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import Foundation

struct HistoryVCDataModel {
    var customerID : String
    var LastBillAmount : String
    var LastBillingMonth : String
    var LastBillingYear : String
    var LastPaymentDate : String
    var PhoneNumber : String
    var SourceDescription : String
    init(_customerID : String, _LastBillAmount : String, _LastBillingMonth: String, _LastBillingYear : String, _LastPaymentDate : String, _PhoneNumber : String, _SourceDescription : String){
        customerID = _customerID
        LastBillAmount = _LastBillAmount
        LastBillingMonth = _LastBillingMonth
        LastBillingYear = _LastBillingYear
        LastPaymentDate = _LastPaymentDate
        PhoneNumber = _PhoneNumber
        SourceDescription = _SourceDescription
    }
}
