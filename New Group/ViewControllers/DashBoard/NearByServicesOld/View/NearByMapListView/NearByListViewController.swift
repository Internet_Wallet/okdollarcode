//
//  ListViewController.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/24/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit

class NearByListViewController: MapBaseViewController {

    // maplistcell have to use in this tableview
    
    @IBOutlet weak var listTableView: UITableView! {
        didSet {
            //self.listTableView.isScrollEnabled = false
        }
    }
    var nearByServicesList = [NearByServicesModel]()

    var nav : UINavigationController?
    @IBOutlet weak var infoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadUI()
        loadInitialize()
        updateLocalizations()
    }
    
    func loadUI() {
        println_debug("list view frame :::: \(self.view.frame.origin.x) ,, \(self.view.frame.origin.y) ,, \(self.view.frame.size.width) ,, \(self.view.frame.size.height)")
    }

    func loadInitialize() {
        let nib = UINib.init(nibName: "MapListCell", bundle: nil)
        listTableView.register(nib, forCellReuseIdentifier: "maplistcellidentifier")
        listTableView.tableFooterView = UIView(frame: CGRect.zero)
        listTableView.rowHeight = UITableViewAutomaticDimension
        listTableView.estimatedRowHeight = 250
        listTableView.reloadData()
    }
    
    func updateLocalizations() {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showHideInfoLabel(isShow: Bool, content: String) {
        DispatchQueue.main.async {
            if isShow {
                self.infoLabel.isHidden = false
                self.infoLabel.text = content
            }else {
                self.infoLabel.isHidden = true
            }
        }
    }
    
    func getTime(timeStr: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let time = dateFormatter.date(from: timeStr)
        dateFormatter.dateFormat = "hh:mm a"
        let hour = dateFormatter.string(from: time!)
        return hour
    }

    @objc func callBtnAction(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to: self.listTableView)
        if let indexPath = self.listTableView.indexPathForRow(at: buttonPosition), nearByServicesList.count > 0 {
            let selectedpromotion = nearByServicesList[indexPath.row]
            let promotion = selectedpromotion
            if let phonenumber = promotion.phoneNumber {
                if phonenumber.count > 0 {
                    var mobNumber = getPhoneNumberByFormat(phoneNumber: phonenumber)
                    mobNumber = mobNumber.removingWhitespaces()
                    let phone = "tel://\(mobNumber)"
                    if let url = URL.init(string: phone) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }
            }
        }else {
            alertViewObj.wrapAlert(title: nil, body: "No Phonenumber Exists".localized, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                
            })
            alertViewObj.showAlert(controller: self)
        }
    }
}

extension NearByListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nearByServicesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let listCell = tableView.dequeueReusableCell(withIdentifier: "maplistcellidentifier", for: indexPath) as! MapListCell
        let promotion = nearByServicesList[indexPath.row]
        listCell.callBtn.addTarget(self, action: #selector(callBtnAction(_:)), for: UIControlEvents.touchUpInside)
        listCell.photoImgView.image = UIImage.init(named: "btc")
        listCell.nameLbl.text = promotion.businessName
        if(promotion.businessName?.count == 0)
        {
            listCell.nameLbl.text = promotion.firstName
        }
//        listCell.rateImgView.image = UIImage.init(named: "star_five")
        
        for img in listCell.rateImgViewList {
            img.isHighlighted = false
        }
        if let openTime = promotion.openingTime , let closeTime = promotion.closingTime {
            
            if(promotion.isOpenAlways == true)
            {
                listCell.timeImgView.isHidden = false
                listCell.timeLbl.isHidden = false
                listCell.timeLblHeightConstraint.constant = 20
                listCell.timeImgView.image = UIImage.init(named: "time")
                listCell.timeLbl.text = "Open 24/7"
            }
            else
            {
            if openTime.count > 0 && closeTime.count > 0 {
                listCell.timeImgView.isHidden = false
                listCell.timeLbl.isHidden = false
                listCell.timeLblHeightConstraint.constant = 20
                listCell.timeImgView.image = UIImage.init(named: "time")
                listCell.timeLbl.text = "\(self.getTime(timeStr: openTime)) - \(self.getTime(timeStr: closeTime))"
            }else {
                listCell.timeImgView.isHidden = true
                listCell.timeLbl.isHidden = true
                listCell.timeLblHeightConstraint.constant = 0
            }
            }
        }
        listCell.locationImgView.image = UIImage.init(named: "location.png")
        if let townshipName = promotion.address {
            if townshipName.count > 0 {
                listCell.locationLbl.text = townshipName
            }else {
                listCell.locationLbl.text = "NA"
            }
        }
        listCell.remarksLbl.text = ""
        listCell.callBtn.setImage(UIImage.init(named: "call.png"), for: UIControlState.normal)
        listCell.distanceLbl.text = ""
        
        return listCell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let verticalIndicator = scrollView.subviews.last as? UIImageView
        verticalIndicator?.backgroundColor = UIColor.yellow
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let promotion = nearByServicesList[indexPath.row]
        let storyboard: UIStoryboard = UIStoryboard(name: "Promotion", bundle: nil)
        let protionDetailsView = storyboard.instantiateViewController(withIdentifier: "PromotionDetailView_ID") as! PromotionDetailViewController
        protionDetailsView.selectedNearByServices = promotion
        protionDetailsView.strNearByPromotion = "NearBy"
        nav?.pushViewController(protionDetailsView, animated: true)

//        if let navControl = self.navigationController {
//            navControl.pushViewController(protionDetailsView, animated: true)
//        } else if let navi = nav {
//            navi.pushViewController(protionDetailsView, animated: true)
//        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         tableView.estimatedRowHeight = 250
         tableView.rowHeight = UITableViewAutomaticDimension
         return tableView.rowHeight
    }
  
}
