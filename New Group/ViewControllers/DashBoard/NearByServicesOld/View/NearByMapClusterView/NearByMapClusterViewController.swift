//
//  MapClusterViewController.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/24/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit
import MapKit

class NearByMapClusterViewController: MapBaseViewController {

    @IBOutlet weak var clusterMapView: MKMapView!
    @IBOutlet weak var promotionInfoView: UIView!
    @IBOutlet weak var promotionImgView: UIImageView!
    @IBOutlet weak var promotionNameLbl: UILabel!
    @IBOutlet weak var contactNoLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var remarksLbl: MarqueeLabel!
    let clusteringManager = FBClusteringManager()

    var locationManager: CLLocationManager!
    
    let regionRadius: CLLocationDistance = 1000
    var listData: [Any]?
    var infoViewTimer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadUI()
        println_debug("mapclusterviewcontroller viewdidload called")
        clusterMapView.delegate = self
        promotionInfoView.isHidden = true
        let initialLocation = CLLocation(latitude: Double(geoLocManager.currentLatitude)!, longitude: Double(geoLocManager.currentLongitude)!)
        centerMapOnLocation(location: initialLocation)
    }

    func loadUI() {
            self.promotionNameLbl.font = UIFont.init(name: appFont, size: 14)
            self.promotionNameLbl.textColor = UIColor.black
            self.contactNoLbl.font = UIFont.init(name: appFont, size: 13)
            self.contactNoLbl.textColor = UIColor.darkGray
            self.distanceLbl.font = UIFont.init(name: appFont, size: 13)
            self.distanceLbl.textColor = UIColor.darkGray
            self.remarksLbl.font = UIFont.init(name: appFont, size: 13)
            self.remarksLbl.textColor = kBlueColor
            self.setUpMarqueeLabel(label: self.remarksLbl)
        promotionImgView.layer.shadowColor = kBackGroundGreyColor.cgColor
        promotionImgView.layer.shadowOpacity = 1
        promotionImgView.layer.shadowOffset = CGSize.zero
        promotionImgView.layer.shadowRadius = 2
    }
    
    func setUpMarqueeLabel(label: MarqueeLabel) {
        label.tag = 501
        label.type = .continuous
        label.speed = .duration(20)
        label.fadeLength = 10.0
        label.trailingBuffer = 30.0
        label.isUserInteractionEnabled = true // Don't forget this, otherwise the gesture recognizer will fail (UILabel has this as NO by default)
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(pauseTap))
        tapRecognizer.numberOfTapsRequired = 1
        tapRecognizer.numberOfTouchesRequired = 1
        label.addGestureRecognizer(tapRecognizer)
    }
    
    @objc func pauseTap(_ recognizer: UIGestureRecognizer) {
        let continuousLabel2 = recognizer.view as! MarqueeLabel
        if recognizer.state == .ended {
            continuousLabel2.isPaused ? continuousLabel2.unpauseLabel() : continuousLabel2.pauseLabel()
        }
    }
    
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius, regionRadius)
        clusterMapView.setRegion(coordinateRegion, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func removeAllAnnotationsInMap() {
        clusteringManager.removeAll()
        reloadMap()
    }
    
    func addAnnotationsInMap(list: Array<Any>) {
        removeAllAnnotationsInMap()
        listData = list
        var clusterArray: [FBAnnotation] = []
        var value = 0
        while value < list.count {
            if let model = list[value] as? NearByServicesModel {
                if let lat = model.nearBy_GeoLocation?.lattitude , let long = model.nearBy_GeoLocation?.longitude {
                        let annotation = FBAnnotation()
                        annotation.coordinate = CLLocationCoordinate2D(latitude:  lat, longitude: long)
                        annotation.userData = model
                        clusterArray.append(annotation)
                }
            }
            value += 1
        }
        clusteringManager.add(annotations: clusterArray)
        reloadMap()
    }
    
    func showPromotionInfoView(promotion: NearByServicesModel) {
        self.promotionInfoView.isHidden = false
        self.promotionInfoView.center.x -= self.view.bounds.width
        if let timer = infoViewTimer {
            timer.invalidate()
        }
        loadInfoViewData(info: promotion)
        UIView.animate(withDuration: 1.5, delay: 0.5,
                                   usingSpringWithDamping: 0.3,
                                   initialSpringVelocity: 0.5,
                                   options: [], animations: {
           self.promotionInfoView.center.x += self.view.bounds.width
                           
        }, completion:{ _ in
             self.infoViewTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.update), userInfo: nil, repeats: false)
        })
    }

    @objc func update() {
        infoViewTimer.invalidate()
        removeTheInfoView()
    }
    
    func removeTheInfoView() {
        UIView.animate(withDuration: 0.1,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.promotionInfoView.center.x -= self.view.bounds.width
        }, completion: { (finished) -> Void in
            self.promotionInfoView.isHidden = true
            self.promotionInfoView.center.x += self.view.bounds.width
        })
    }
    
    func loadInfoViewData(info: NearByServicesModel) {
        promotionImgView.image = UIImage.init()
        promotionNameLbl.text = info.businessName
        var mobileNumber = "NA"
        if info.phoneNumber!.count > 0 {
            mobileNumber = getPhoneNumberByFormat(phoneNumber: info.phoneNumber!)
        }
        contactNoLbl.text = "Contact: \(mobileNumber)"
     
        distanceLbl.text = ""
      
        remarksLbl.text = "NA"
        
        promotionImgView.image = UIImage.init(named: "btc")
        //promotionInfoView.backgroundColor = info.isAgent ? kYellowColor : UIColor.white
        
//        let emoji1 = (info.shop_Category?.categoryLogo!)!
//        let emoji2 = (info.shop_Category?.businessType?.businessType_Logo!)!
//        let topImage = emoji1.emojiToImage()
//        let bottomImage = emoji2.emojiToImage()
//        promotionImgView.image = topImage?.combineWith(image: bottomImage!)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func reloadMap() {
        DispatchQueue.main.async {
            let mapBoundsWidth = Double(self.clusterMapView.bounds.size.width)
            let mapRectWidth = self.clusterMapView.visibleMapRect.size.width
            let scale = mapBoundsWidth / mapRectWidth
            let annotationArray = self.clusteringManager.clusteredAnnotations(withinMapRect: self.clusterMapView.visibleMapRect, zoomScale:scale)
            self.clusteringManager.display(annotations: annotationArray, onMapView:self.clusterMapView)
        }
    }

}

extension NearByMapClusterViewController: MKMapViewDelegate {

    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        reloadMap()
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var reuseId = ""
        if annotation is FBAnnotationCluster {
            reuseId = "Cluster"
            var clusterView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
            if clusterView == nil {
                clusterView = FBAnnotationClusterView(annotation: annotation, reuseIdentifier: reuseId, configuration: FBAnnotationClusterViewConfiguration.default())

                
            } else {
                clusterView?.annotation = annotation
            }
            return clusterView
        } else {
//            reuseId = "Pin"
//            var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
//            if pinView == nil {
//                pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
//                pinView?.pinTintColor = UIColor.red
//            } else {
//                pinView?.annotation = annotation
//            }
//            return pinView
            
            let coordinate : CLLocationCoordinate2D = annotation.coordinate
            let annotationIdentifier = "SomeCustomIdentifier"
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
            if annotationView == nil {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
                annotationView?.canShowCallout = false
            
                if let list = listData {
                    for promotion in list {
                        if let promo = promotion as? NearByServicesModel {
                            if promo.nearBy_GeoLocation?.lattitude == coordinate.latitude && promo.nearBy_GeoLocation?.longitude == coordinate.longitude {
                                //if promo.isAgent {
                                    let pinImage = UIImage(named: "ok.png")
                                    annotationView?.image = pinImage
                                /*}else {
                                    let emoji1 = (promo.shop_Category?.categoryLogo!)!
                                    let emoji2 = (promo.shop_Category?.businessType?.businessType_Logo!)!
                                    let topImage = emoji1.emojiToImage()
                                    let bottomImage = emoji2.emojiToImage()
                                    annotationView?.image = topImage?.combineWith(image: bottomImage!)
                                }
                                 */
                               break
                            }
                        }
                    }
                }
            }
            else {
                annotationView?.annotation = annotation
            }
            return annotationView
        }
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation else { return }

        if let cluster = annotation as? FBAnnotationCluster {
            var zoomRect = MKMapRectNull
            for annotation in cluster.annotations {
               
                let annotationPoint = MKMapPointForCoordinate(annotation.coordinate)
                let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0)
                if MKMapRectIsNull(zoomRect) {
                    zoomRect = pointRect
                } else {
                    zoomRect = MKMapRectUnion(zoomRect, pointRect)
                }
            }
            mapView.setVisibleMapRect(zoomRect, animated: true)
        }else {
            if let eachAnnotation = annotation as? FBAnnotation {
                if let userdata = eachAnnotation.userData as? NearByServicesModel {
                    //println_debug("user each data details :::: \(userdata.shop_Name)  ,, \(userdata.shop_Address?.addressLine1) ,, \(userdata.shop_InMeter)")
                    self.showPromotionInfoView(promotion: userdata)
                }
               
            }
        }
    }

//    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
//        views.forEach { $0.alpha = 0 }
//        UIView.animate(withDuration: 0.35, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: [], animations: {
//            views.forEach { $0.alpha = 1 }
//        }, completion: nil)
//    }

}

