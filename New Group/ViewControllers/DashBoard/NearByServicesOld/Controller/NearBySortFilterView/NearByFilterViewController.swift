//
//  FilterViewController.swift
//  Promotions
//
//  Created by Uma Rajendran on 10/28/17.
//  Copyright © 2017 Uma Rajendran. All rights reserved.
//

import UIKit

protocol NearByFilterViewDelegate: class  {
    func didSelectFilterOption(filteredList: [Any], selectedFilter: Any?)
}

class NearByFilterViewController: UIViewController {

    @IBOutlet weak var headerLabel      : UILabel!
    {
        didSet
        {
            headerLabel.text = headerLabel.text?.localized
        }
    }
    @IBOutlet weak var filterTable      : UITableView!
    @IBOutlet weak var resigningView    : UIView!
    weak var delegate                   : NearByFilterViewDelegate?
   
    var isExpandCategories: Bool = true
    var previousSelectedFilterData: Any?
    var filterDataList: [Any]?
    var filterShowList: [Any]?
    var viewFrom: FromView?
    var currentNearByView: NearByView?
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadUI()
        loadInitialize()
        updateLocalizations()
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
            isExpandCategories = true
            removeDuplicatePromotions()
            loadTableView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadUI() {
        headerHeightConstraint.constant = kHeaderHeight
        self.view.backgroundColor = kGradientGreyColor
        resigningView.backgroundColor = UIColor.clear
    }
    
    func loadInitialize() {
      
    }
    
    func updateLocalizations() {
        headerLabel.font = UIFont.init(name: appFont, size: 17)
    }
    
    func loadTableView() {
//        filterTable.register(UITableViewCell.self, forCellReuseIdentifier: "DefaultCell")
        filterTable.register(UINib(nibName: "SortCell", bundle: nil), forCellReuseIdentifier: "sortcellidentifier")

        filterTable.register(UINib(nibName: "DivisionCell", bundle: nil), forCellReuseIdentifier: "divisioncellidentifier")
        filterTable.tableFooterView = UIView(frame: CGRect.zero)
        filterTable.reloadData()
    }
    
    func removeDuplicatePromotions() {
        
        var newFilteredData: [NearByServicesModel] = []
        var checkSet = Set<String>()
        if let filterlist = filterDataList as? [NearByServicesModel] {
            for promotion in filterlist {
                if(promotion.businessName!.count != 0)
                {
                    if !checkSet.contains((promotion.businessName!)) {
                        newFilteredData.append(promotion)
                        checkSet.insert((promotion.businessName!))
                    }
                }
                else
                {
                    if !checkSet.contains((promotion.firstName!)) {
                        newFilteredData.append(promotion)
                        checkSet.insert((promotion.firstName!))
                    }
                }
            }
            filterShowList?.removeAll()
            filterShowList = newFilteredData
            filterTable.reloadData()
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if touch.view == self.resigningView {
              resignTheFilterView()
            }
         
        }
        super.touchesBegan(touches, with: event)
    }
    
    func resignTheFilterView() {
        if let presentingView = self.presentingViewController {
            presentingView.dismiss(animated: true, completion: nil)
        }
    }

}

extension NearByFilterViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount = 1
            if section == 1 && isExpandCategories{
                rowCount = (filterShowList?.count)! + 1
            }
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0) {
            
            let defaultCell = tableView.dequeueReusableCell(withIdentifier: "sortcellidentifier", for: indexPath) as! SortCell
            defaultCell.wrap_Data(title: "Default".localized, imgName: "")
            return defaultCell
            
        }else {
            if indexPath.row == 0 {
                
                let categoriesHeaderCell = tableView.dequeueReusableCell(withIdentifier: "divisioncellidentifier", for: indexPath) as! DivisionCell  // just reusing the division cell design here
                categoriesHeaderCell.expandImgView.isHidden =  false
                let image = (isExpandCategories == true) ? UIImage.init(named: "up_arrow.png") : UIImage.init(named: "down_arrow.png")
                categoriesHeaderCell.wrapData(text: "Business Name".localized, img: image!)
                return categoriesHeaderCell
                
            }else {
                
                let categoriesListCell = tableView.dequeueReusableCell(withIdentifier: "sortcellidentifier", for: indexPath) as! SortCell
                
                    if let filterList = filterShowList![indexPath.row - 1] as? NearByServicesModel {
                        let promotionData = filterList //as! PromotionsModel
                        let tab = "    "
                        if((promotionData.businessName?.count)! > 0)
                        {
                            categoriesListCell.contentLbl.text = "\(tab)\((promotionData.businessName)!)"
                            let name = "\(tab)\((promotionData.businessName)!)"
                            var imageName = ""
                            if let prevFilterData = previousSelectedFilterData as? NearByServicesModel {
                                if prevFilterData.cellId! == promotionData.cellId {
                                    // here show the selected image
                                    imageName =  "selected_icon"
                                }
                            }
                            categoriesListCell.wrap_Data(title: name, imgName: imageName)
                        }
                        else
                        {
                            categoriesListCell.contentLbl.text = "\(tab)\((promotionData.firstName)!)"
                            let name = "\(tab)\((promotionData.firstName)!)"
                            var imageName = ""
                            if let prevFilterData = previousSelectedFilterData as? NearByServicesModel {
                                if prevFilterData.cellId! == promotionData.cellId {
                                    // here show the selected image
                                    imageName =  "selected_icon"
                                }
                            }
                            categoriesListCell.wrap_Data(title: name, imgName: imageName)
                        }
                    }

                return categoriesListCell
                
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            
            self.delegate?.didSelectFilterOption(filteredList: filterDataList!,selectedFilter: nil)
            resignTheFilterView()
        }else if (indexPath.section == 1 && indexPath.row == 0) {
            
            isExpandCategories = !isExpandCategories
            filterTable.reloadSections(NSIndexSet(index: indexPath.section) as IndexSet, with: .fade)
        }else {
            
                if let filtershowlistData = filterShowList![indexPath.row - 1] as? NearByServicesModel {
                    var newFilteredData: [NearByServicesModel] = []
                    let selectedPromotion = filtershowlistData //as! PromotionsModel
                    if let filterlist = filterDataList as? [NearByServicesModel] {
                        for promotion in filterlist {
                            if((promotion.businessName?.count)! > 0)
                            {
                                if promotion.businessName == selectedPromotion.businessName {
                                    newFilteredData.append(promotion)
                                }
                            }
                            else
                            {
                                if promotion.firstName == selectedPromotion.firstName {
                                    newFilteredData.append(promotion)
                                }
                            }
                        }
                    }
                    self.delegate?.didSelectFilterOption(filteredList: newFilteredData ,selectedFilter: selectedPromotion)
                }
           
            resignTheFilterView()
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}


