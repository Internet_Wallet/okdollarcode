//
//  NearByServicesSubVC.swift
//  OK
//
//  Created by SHUBH on 6/15/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class NearByServicesSubVC: MapBaseViewController {

    let appDel = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var currentLocationView      : UIView!
    @IBOutlet weak var locationNameLbl          : UILabel!
        {
        didSet
        {
            locationNameLbl.text = locationNameLbl.text?.localized
        }
    }
    @IBOutlet weak var locationContentLbl       : UILabel!
        {
        didSet
        {
            locationContentLbl.text = locationContentLbl.text?.localized
        }
    }
    @IBOutlet weak var collectionView           : UICollectionView!
    @IBOutlet weak var baseView                 : UIView!
    
    private var collectItems     = [String]()
    private var collectImages    = [String]()
    private var highLightImages  = [Bool]()
    
    var mapClusterView          : NearByMapClusterViewController?
    var listPromotionsView      : NearByListViewController?
    
    var mapUiType: UIType?
    var navController : UINavigationController?
    
    var selectedSortItem        : Int = 0
    var selectedFilterData      : PromotionsModel?
    
    let sortView        = NearBySortViewController()
    let filterView      = NearByFilterViewController()
    
    let margin: CGFloat = 2
    var cellsPerRow: Int = 0
    
    var selectedLocationType: Location?
    let nearbydistance = kNearByDistance
    
    var selectedLocationDetail          : LocationDetail?
    var selectedTownshipDetail          : TownShipDetail?
    
    var promotionsListBackUPByCurLoc   : [NearByServicesModel]?
    var promotionsListRecentFromServer : [NearByServicesModel]?
    
    var viewFrom: FromView?
    var baseViewHeight: CGFloat {
        if viewFrom == .cashInOut {
            return screenArea.size.height - 250
        }
        return screenArea.size.height - 190
    }
    
    //var parentView: NearbyServicesViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func switchMapListView() -> UIType {
        if mapUiType == .listView {
            mapUiType = .mapView
        }else {
            mapUiType = .listView
        }
        DispatchQueue.main.async {
            self.switchBaseView()
        }
        return mapUiType!
    }
    
    
    func switchBaseView() {
        if mapUiType == .listView {
            if let map = mapClusterView {
                map.view.removeFromSuperview()
            }
            //listPromotionsView             = NearByListViewController()
            //            listPromotionsView!.view.frame = baseView.bounds
            //            listPromotionsView!.view.frame = CGRect(x: 0.0, y: 0.0, width: screenArea.size.width, height: screenArea.size.height - 190)// 260)
            listPromotionsView!.view.frame = CGRect(x: 0.0, y: 0.0, width: screenArea.size.width, height: baseViewHeight)// 260)
            
            listPromotionsView?.view.backgroundColor = UIColor.white
            if let nav = self.navController {
                listPromotionsView?.nav = nav
            }
            if let promotionlistbackup = promotionsListBackUPByCurLoc {
                DispatchQueue.main.async {
                    self.listPromotionsView?.showHideInfoLabel(isShow: false, content: "")
                    self.listPromotionsView?.nearByServicesList = promotionlistbackup
                    self.listPromotionsView?.listTableView.reloadData()
                }
                
            }else {
                DispatchQueue.main.async {
                    self.listPromotionsView?.showHideInfoLabel(isShow: true, content: "Loading Data...".localized)
                }
                //didSelectLocationByCurrentLocation()
            }
            DispatchQueue.main.async {
                self.baseView.addSubview(self.listPromotionsView!.view)
            }
        }else {
            if let list = listPromotionsView {
                list.view.removeFromSuperview()
            }
            mapClusterView             = NearByMapClusterViewController()
            //            mapClusterView?.view.frame = baseView.bounds
            mapClusterView?.view.frame = CGRect(x: 0.0, y: 0.0, width: screenArea.size.width, height: baseViewHeight)
            
            baseView.addSubview(mapClusterView!.view)
            if let promotionlistbackup = promotionsListBackUPByCurLoc {
                //updateCurrentLocationToUI()
                mapClusterView?.addAnnotationsInMap(list: promotionlistbackup)
            }else {
                //didSelectLocationByCurrentLocation()
            }
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
