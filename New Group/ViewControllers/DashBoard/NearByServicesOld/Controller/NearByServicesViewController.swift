//
//  NearByServicesViewController.swift
//  OK
//
//  Created by SHUBH on 6/15/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class NearByServicesViewController: MapBaseViewController, MainLocationViewDelegate, CategoriesSelectionDelegate, UIPageViewControllerDelegate, UIPageViewControllerDataSource, NearBySortViewDelegate, NearByFilterViewDelegate {
   

    let appDel = UIApplication.shared.delegate as! AppDelegate

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
        {
        didSet
        {
            headerLabel.text = "Nearby OK $ Services".localized
        }
    }
    @IBOutlet weak var tabsView: UIView!
    @IBOutlet weak var tabMerchantBtn: UIButton!
        {
        didSet
        {
            self.tabMerchantBtn.setTitle("Merchants".localized, for: .normal)
        }
    }
    @IBOutlet weak var tabOkAgentsBtn: UIButton!
        {
        didSet
        {
            self.tabOkAgentsBtn.setTitle("OK $ Agents".localized, for: .normal)
        }
    }
    @IBOutlet weak var tabOkOfficesBtn: UIButton!
        {
        didSet
        {
            self.tabOkOfficesBtn.setTitle("OK $ Office".localized, for: .normal)
        }
    }
    
    @IBOutlet weak var currentLocationView      : UIView!
    @IBOutlet weak var locationNameLbl          : UILabel!
        {
        didSet
        {
            locationNameLbl.text = locationNameLbl.text?.localized
        }
    }
    @IBOutlet weak var locationContentLbl       : UILabel!
        {
        didSet
        {
            locationContentLbl.text = locationContentLbl.text?.localized
        }
    }
    
    @IBOutlet weak var merchantsSeparatorLbl: UILabel!
    @IBOutlet weak var okAgentsSeparatorLbl: UILabel!
    @IBOutlet weak var okOfficesSeparatorLbl: UILabel!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var headerSearchBtn: UIButton!
    @IBOutlet weak var switchMapListBtn: UIButton!
    
    var allSourceViews = [UIViewController]()
    var pageViewController : UIPageViewController?
    
    var mapUiType: UIType?
    var navController : UINavigationController?

    var promotionView: NearByServicesSubVC?
//    var promotionView1: NearByServicesSubVC?
//    var promotionView2: NearByServicesSubVC?
    
    var promotionView1: OK$AgentSubVC?
    var promotionView2: OK$OfficeSubVC?

    var currentNearbyView: NearByView = .merchantView
    var prevNearbyView: NearByView = .merchantView
    
    @IBOutlet weak var topSearchView: UIView!
    
    @IBOutlet weak var topSearchBar: UISearchBar!
    
    @IBOutlet weak var searchViewBackBtn: UIButton!
    
    var isHeaderSearchOpened: Bool = false
    
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var headerSearchHeightConstraint: NSLayoutConstraint!
    
    //Record Manage
    var cashInListBackUPByCurLoc        : [NearByServicesModel]?
    var cashInListRecentFromServer      : [NearByServicesModel]?
    
    var arrMerchantList                 = [NearByServicesModel]()
    var arrAgentsList                   = [NearByServicesModel]()
    var arrOfficeList                   = [NearByServicesModel]()

    //Collection cell
    @IBOutlet weak var collectionView           : UICollectionView!
    
    private var collectItems     = [String]()
    private var collectImages    = [String]()
    private var highLightImages  = [Bool]()
    
    let margin: CGFloat = 2
    var cellsPerRow: Int = 0
    
    var selectedSortItem        : Int = 0
    var selectedFilterData      : NearByServicesModel?
    
    //Manage List & Map
    var mapClusterView          : NearByMapClusterViewController?
    var listPromotionsView      : NearByListViewController?
    
    var baseViewHeight: CGFloat {
        return screenArea.size.height - 190
    }
    
    var promotionsListBackUPByCurLoc   : [NearByServicesModel]?
    var promotionsListRecentFromServer : [NearByServicesModel]?
    
    var selectedLocationType: Location?
    let nearbydistance = kNearByDistance
    
    var selectedLocationDetail          : LocationDetail?
    var selectedTownshipDetail          : TownShipDetail?
    
    let sortView        = NearBySortViewController()
    let filterView      = NearByFilterViewController()
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        geoLocManager.startUpdateLocation()
        loadPageViewController()

        loadUI()
        updateLocalizations()
        loadInitialize()
        mapUiType = .listView
        loadListViewAtFirst()

        highLightPromotionsView()
        searchBarSettings()
        self.checkAndLoadCollectionView()
        setSelectedLocationType(locationtype: .byCurrentLocation)
        didSelectLocationByCurrentLocation()
    
        hideSearchWithoutAnimation()
        hideAndShowHeaderSearchBtn(isShow: true)
        self.switchMapListBtn.setImage(UIImage.init(named: "map_white"), for: .normal)
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        geoLocManager.startUpdateLocation()
        
        if (UserDefaults.standard.bool(forKey: "HelpButtonHideShow")) {
            appDel.floatingButtonControl?.window.closeButton?.isHidden = true
            appDel.floatingButtonControl?.window.isHidden = true
        } else {
            appDel.floatingButtonControl?.window.isHidden = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        geoLocManager.stopUpdateLocation()
    }
    
    func loadListViewAtFirst() {
        mapUiType = .listView
        listPromotionsView             = NearByListViewController()
        listPromotionsView?.nav   = self.navigationController
        listPromotionsView!.view.frame = CGRect(x: 0.0, y: 0.0, width: screenArea.size.width, height: baseViewHeight)//260)
        
        listPromotionsView?.view.backgroundColor = UIColor.white
        
        if let nav = self.navController {
            listPromotionsView?.nav = nav
        }
        DispatchQueue.main.async {
            self.listPromotionsView?.showHideInfoLabel(isShow: true, content: "Loading Data...".localized)
            self.baseView.addSubview(self.listPromotionsView!.view)
        }
    }
    
    func updateCurrentLocationToUI() {
        selectedLocationDetail = nil
        selectedTownshipDetail = nil
        locationNameLbl.text = "Current Location".localized
        locationContentLbl.text = "Loading location.....".localized
        guard appDelegate.checkNetworkAvail() else {
            return
        }
        if let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude {
            geoLocManager.getLocationNameByLatLong(lattitude: lat, longitude: long) { (isSuccess, currentAddress) in
                guard isSuccess else {
                    println_debug("something went wrong when get current address from google api")
                    return
                }
                DispatchQueue.main.async {
                    if let curAddress = currentAddress as? String {
                        self.locationContentLbl.text = curAddress
                    }else {
                        self.locationContentLbl.text = "Loading location.....".localized
                    }
                }
            }
            
        }
    }
    
    func updateOtherLocationToUI(location: LocationDetail, township: TownShipDetail) {
        selectedLocationDetail = location
        selectedTownshipDetail = township
        DispatchQueue.main.async {
            self.locationNameLbl.text = "Other Location".localized
            //            self.locationContentLbl.text = "\(township.townShipNameEN), \(location.stateOrDivitionNameEn)" // later change to localizations
            if self.appDel.currentLanguage == "my" {
                self.locationContentLbl.text = "\(township.townShipNameMY), \(location.stateOrDivitionNameMy)"
            }
            else
            {
                self.locationContentLbl.text = "\(township.townShipNameEN), \(location.stateOrDivitionNameEn)"
            }
        }
    }
    
    func collectionViewSettings() {
        guard let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.minimumInteritemSpacing = margin
        flowLayout.minimumLineSpacing = margin
    }
    
    func setSelectedLocationType(locationtype: Location) {
        selectedLocationType = locationtype
    }
    
    func setTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapOnCurrentLocationView(_:)))
        currentLocationView.addGestureRecognizer(tap)
        currentLocationView.isUserInteractionEnabled = true
    }
    
    @objc func tapOnCurrentLocationView(_ sender: UITapGestureRecognizer) {
        println_debug("tap call on current location view")
        let storyBoardName = UIStoryboard(name: "Promotion", bundle: nil)
        guard let locationSelectionView = storyBoardName.instantiateViewController(withIdentifier: "SelecteMainLocationView_ID") as? SelectMainLocationViewController else { return }
        locationSelectionView.delegate = self
        self.navigationController?.pushViewController(locationSelectionView, animated: true)
    }
    
    //MARK:- Get API Data

    func getOfficeAPI(lat: String, long: String)
    {
        println_debug("call ok offices api ")
        
        let url = getUrl(urlStr: "", serverType: .nearByServicesUrl)
        
        let jsonDic = ["Amount" : 0, "CashType" : 0, "CountryCode" : "+95", "DistanceLimit" : 0, "Latitude" : lat, "Longitude": long, "PhoneNumber" : UserModel.shared.mobileNo, "UserIdNotToConsider": ""] as [String : Any]
        
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url,methodType: kMethod_Post, contentType: kContentType_Json, inputVal: jsonDic, authStr: nil)
        
        DispatchQueue.main.async {
            progressViewObj.showProgressView()
        }
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            
            guard isSuccess else {
                DispatchQueue.main.async {
                    progressViewObj.removeProgressView()
                    //println_debug("remove progress view called inside cashin offices api main call with error")
                }
                //self.showErrorAlert(errMessage: "Try Again".localized)
                self.listPromotionsView?.showHideInfoLabel(isShow: true, content: "No Record Found".localized)
                return
            }
            println_debug("response dict for get all offices list :: \(response ?? "")")
            progressViewObj.removeProgressView()
            self.listPromotionsView?.showHideInfoLabel(isShow: true, content: "No Record Found".localized)

            var cashArray = [NearByServicesModel]()
            DispatchQueue.main.async {
                cashArray = ParserAttributeClass.parseNearbyServiceJSON(anyValue: response as AnyObject) as [NearByServicesModel]
                //println_debug("response count for ok offices :::: \(cashArray.count)")
                progressViewObj.removeProgressView()
                self.cashInListRecentFromServer = cashArray
                if self.mapUiType == .listView {
                    if cashArray.count > 0 {
                        self.listPromotionsView?.showHideInfoLabel(isShow: false, content: "".localized)
                        self.cashInListBackUPByCurLoc = cashArray  // this is too just save the list in backup
                        
                        //Seperate mechant =2,agent=1,office=7
                        for indexVal in 0..<cashArray.count {
                            let currentModel = cashArray[indexVal]
                            if let type = currentModel.type {
                                switch type {
                                case 1:
                                    self.arrAgentsList.append(currentModel)
                                case 2:
                                    self.arrMerchantList.append(currentModel)
                                case 7:
                                    self.arrOfficeList.append(currentModel)
                                default:
                                    println_debug("default")
                                }
                            }
                        }
                        self.listPromotionsView?.nearByServicesList = self.arrMerchantList
                        self.listPromotionsView?.showHideInfoLabel(isShow: false, content: "")
                        self.listPromotionsView?.listTableView.reloadData()
                    }
                }
            }
        })
    }
    
    
    // MARK: - Load UI and Localizations
    
    func loadUI() {
        headerHeightConstraint.constant = kHeaderHeight
        headerSearchHeightConstraint.constant = kHeaderHeight
        headerView.backgroundColor = kYellowColor
        baseView.layer.shadowColor = kBackGroundGreyColor.cgColor
        baseView.layer.shadowOpacity = 1
        baseView.layer.shadowOffset = CGSize.zero
        baseView.layer.shadowRadius = 4
        self.view.setShadowToLabel(thisView: merchantsSeparatorLbl)
        self.view.setShadowToLabel(thisView: okAgentsSeparatorLbl)
        self.view.setShadowToLabel(thisView: okOfficesSeparatorLbl)
        
        tabsView.backgroundColor = UIColor.white
        tabMerchantBtn.backgroundColor = UIColor.clear
        tabOkAgentsBtn.backgroundColor = UIColor.clear
        tabOkOfficesBtn.backgroundColor = UIColor.clear
        
    }
    
    func updateLocalizations() {
        headerLabel.font = UIFont.init(name: appFont, size: 17)
        tabMerchantBtn.titleLabel?.font = UIFont.init(name: appFont, size: 16)
        tabOkAgentsBtn.titleLabel?.font = UIFont.init(name: appFont, size: 16)
        tabOkOfficesBtn.titleLabel?.font = UIFont.init(name: appFont, size: 16)
        
        //
        locationNameLbl.textColor = kBlueColor
        locationContentLbl.textColor = kBlueColor
        locationNameLbl.font = UIFont.init(name: appFont, size: 15)
        locationContentLbl.font = UIFont.init(name: appFont, size: 14)
        locationContentLbl.lineBreakMode = NSLineBreakMode.byWordWrapping
    }
    
    func loadInitialize() -> Void {
        
        setTapGesture()
    }
    
    
    func loadPageViewController() {
        let promotionStory = UIStoryboard(name: "NearByServices", bundle: nil)
        promotionView = promotionStory.instantiateViewController(withIdentifier: "NearByServicesSubVC") as? NearByServicesSubVC
//        promotionView1 = promotionStory.instantiateViewController(withIdentifier: "NearByServicesSubVC") as? NearByServicesSubVC
//        promotionView2 = promotionStory.instantiateViewController(withIdentifier: "NearByServicesSubVC") as? NearByServicesSubVC

        promotionView1 = promotionStory.instantiateViewController(withIdentifier: "OK$AgentSubVC") as? OK$AgentSubVC
        promotionView2 = promotionStory.instantiateViewController(withIdentifier: "OK$OfficeSubVC") as? OK$OfficeSubVC
        

        pageViewController = UIPageViewController.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        addChildViewController(pageViewController!)
        pageViewController!.delegate = self
        pageViewController!.dataSource = self
        pageViewController!.view.frame = baseView.bounds
        pageViewController!.view.backgroundColor = UIColor.clear
        
        allSourceViews = [promotionView!, promotionView1!, promotionView2!]
        
        promotionView?.mapUiType = .listView
        promotionView?.navController = self.navigationController
        
        promotionView1?.mapUiType = .listView
        promotionView1?.navController = self.navigationController
        
        promotionView2?.mapUiType = .listView
        promotionView2?.navController = self.navigationController
        
        pageViewController!.setViewControllers([allSourceViews[0]], direction: .forward, animated: true, completion: nil)
        baseView.addSubview(pageViewController!.view)
        
        addChildViewController(promotionView!)
        addChildViewController(promotionView1!)
        addChildViewController(promotionView2!)
        
        promotionView?.didMove(toParentViewController: pageViewController!)
        promotionView1?.didMove(toParentViewController: pageViewController!)
        promotionView2?.didMove(toParentViewController: pageViewController!)
        
        pageViewController!.didMove(toParentViewController: self)
    }
    
    // MARK: - PageViewController Delegate
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index : Int = self.indexOf(viewController)
        if index == 0 {
            return nil
        }
        index -= 1
        return allSourceViews[index]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index : Int = self.indexOf(viewController)
        index += 1
        if index == allSourceViews.count {
            return nil
        }
        return allSourceViews[index]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        self.setPageIndex()
    }
    
    func indexOf(_ viewController: UIViewController) -> Int {
        return (allSourceViews as NSArray).index(of: viewController)
    }
    
    func viewControllerAtIndex(index: Int ) -> UIViewController? {
        if allSourceViews.count == 0 || index >= allSourceViews.count {
            return nil
        }
        return allSourceViews[index]
    }
    
    func setPageIndex() {
        let currentViewController: UIViewController = pageViewController!.viewControllers![0]
        let currentIndex: Int = self.indexOf(currentViewController)
        prevNearbyView = currentNearbyView
        if currentIndex == 0 {
            highLightPromotionsView()
        }else if currentIndex == 1 {
            highLightOKAgentsView()
        }else if currentIndex == 2 {
            highLightOKOfficesView()
        }
    }
    
    func highLightPromotionsView() {
        currentNearbyView = .merchantView
        DispatchQueue.main.async {
            
            UIView.animate(withDuration: 0.5) {
                
                self.tabMerchantBtn.titleLabel?.textColor = UIColor.orange
                self.tabOkAgentsBtn.titleLabel?.textColor = UIColor.black
                self.tabOkOfficesBtn.titleLabel?.textColor = UIColor.black
                self.merchantsSeparatorLbl.backgroundColor = UIColor.black
                self.okAgentsSeparatorLbl.backgroundColor = UIColor.clear
                self.okOfficesSeparatorLbl.backgroundColor = UIColor.clear
            }
            
              if self.mapUiType == .listView  {
                if(self.arrMerchantList.count == 0)
                {
                    self.listPromotionsView?.showHideInfoLabel(isShow: true, content: "No Record Found".localized)
                }
                else
                {
                    self.listPromotionsView?.nearByServicesList = self.arrMerchantList
                    self.listPromotionsView?.showHideInfoLabel(isShow: false, content: "")
                    self.listPromotionsView?.listTableView.reloadData()
                }
            }
              else
              {
                self.mapClusterView?.addAnnotationsInMap(list: self.arrMerchantList)
            }
        }
        
    }
    
    func highLightOKAgentsView() {
        currentNearbyView = .okAgentsView
        DispatchQueue.main.async {
            
            UIView.animate(withDuration: 0.5) {
                
                self.tabMerchantBtn.titleLabel?.textColor = UIColor.black
                self.tabOkAgentsBtn.titleLabel?.textColor = UIColor.orange
                self.tabOkOfficesBtn.titleLabel?.textColor = UIColor.black
                self.merchantsSeparatorLbl.backgroundColor = UIColor.clear
                self.okAgentsSeparatorLbl.backgroundColor = UIColor.black
                self.okOfficesSeparatorLbl.backgroundColor = UIColor.clear
            }
            
             if self.mapUiType == .listView  {
                if(self.arrAgentsList.count == 0)
                {
                    self.listPromotionsView?.showHideInfoLabel(isShow: true, content: "No Record Found".localized)
                }
                else
                {
                    self.listPromotionsView?.nearByServicesList = self.arrAgentsList
                    self.listPromotionsView?.showHideInfoLabel(isShow: false, content: "")
                    self.listPromotionsView?.listTableView.reloadData()
                }
            }
            else
             {
                self.mapClusterView?.addAnnotationsInMap(list: self.arrAgentsList)
            }
        }
    }
    
    func highLightOKOfficesView() {
        currentNearbyView = .okOfficesView
        DispatchQueue.main.async {
            
            UIView.animate(withDuration: 0.5) {
                
                self.tabMerchantBtn.titleLabel?.textColor = UIColor.black
                self.tabOkAgentsBtn.titleLabel?.textColor = UIColor.black
                self.tabOkOfficesBtn.titleLabel?.textColor = UIColor.orange
                self.merchantsSeparatorLbl.backgroundColor = UIColor.clear
                self.okAgentsSeparatorLbl.backgroundColor = UIColor.clear
                self.okOfficesSeparatorLbl.backgroundColor = UIColor.black
            }
            if self.mapUiType == .listView  {
                if(self.arrOfficeList.count == 0)
                {
                    self.listPromotionsView?.showHideInfoLabel(isShow: true, content: "No Record Found".localized)
                }
                else
                {
                    self.listPromotionsView?.nearByServicesList = self.arrOfficeList
                    self.listPromotionsView?.showHideInfoLabel(isShow: false, content: "")
                    self.listPromotionsView?.listTableView.reloadData()
                }
            }
            else
            {
                self.mapClusterView?.addAnnotationsInMap(list: self.arrOfficeList)
            }
        }
    }
    
    
    @IBAction func searchBtnAction(_ sender: Any) {
        switch currentNearbyView {
        case .merchantView:
            self.topSearchView.frame = CGRect(x: screenArea.size.width - 30, y: 0, width: 0, height: kHeaderHeight)
            showCashSearch()
        case .okAgentsView:
            self.topSearchView.frame = CGRect(x: screenArea.size.width - 30, y: 0, width: 0, height: kHeaderHeight)
            showCashSearch()
        case .okOfficesView:
            self.topSearchView.frame = CGRect(x: screenArea.size.width - 30, y: 0, width: 0, height: kHeaderHeight)
            showCashSearch()
        }
    }
    
    func showCashSearch() {
        isHeaderSearchOpened = true
        topSearchBar.text = ""
        topSearchView.layer.masksToBounds = true
        topSearchView.layer.cornerRadius = 20
        topSearchView.alpha = 0.0
        UIView.animate(withDuration: 0.3, animations: {
            var viewFrame: CGRect = self.topSearchView.frame
            viewFrame.origin.x = 0
            viewFrame.size.width = screenArea.size.width
            self.topSearchView.frame = viewFrame
            self.topSearchView.alpha = 1.0
            self.topSearchView.isHidden = false
            self.topSearchBar.becomeFirstResponder()
            
        }) { _ in
            self.topSearchView.layer.cornerRadius = 0
        }
    }
    
    func hideSearchWithoutAnimation() {
        isHeaderSearchOpened = false
        topSearchBar.text = ""
        topSearchBar.resignFirstResponder()
        topSearchView.isHidden = true
    }
    
    func hideCashSearch() {
        isHeaderSearchOpened = false
        topSearchBar.text = ""
        topSearchBar.resignFirstResponder()
        topSearchView.layer.cornerRadius = 20
        topSearchView.alpha = 1.0
        UIView.animate(withDuration: 0.3, animations: {
            var viewFrame: CGRect = self.topSearchView.frame
            viewFrame.origin.x = screenArea.size.width - 30
            viewFrame.origin.y = self.topSearchView.frame.origin.y + 15
            viewFrame.size.width = 0
            viewFrame.size.height = self.topSearchView.frame.size.height / 2
            self.topSearchView.frame = viewFrame
            
        }) { _ in
            self.topSearchView.alpha = 0.0
        }
    }
    
    
    @IBAction func searchViewBackAction(_ sender: Any) {
        //Hide search view
        hideCashSearch()
        reloadListViewAfterSwitch()
    }
    
    @IBAction func promotionMainBackAction(_ sender: Any) {
        if let presentingView = self.presentingViewController {
            presentingView.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func didSelectTabs(_ sender: Any) {
        prevNearbyView = currentNearbyView
        
        if(mapUiType == .mapView)
        {
            self.hideAndShowHeaderSearchBtn(isShow: false)
            self.switchMapListBtn.setImage(UIImage.init(named: "list"), for: .normal)
        }
        else
        {
            self.hideAndShowHeaderSearchBtn(isShow: true)
            self.switchMapListBtn.setImage(UIImage.init(named: "map_white"), for: .normal)
        }
        let btn: UIButton = sender as! UIButton
        switch btn.tag {
        case 0:
            if let view = viewControllerAtIndex(index: 0) {
                DispatchQueue.main.async {
                    self.highLightPromotionsView()
                }
                pageViewController!.setViewControllers([view], direction: .forward, animated: false, completion: nil)
            }
        case 1:
            if let view = viewControllerAtIndex(index: 1) {
                DispatchQueue.main.async {
                    self.highLightOKAgentsView()
                }
                pageViewController!.setViewControllers([view], direction: .forward, animated: false, completion: nil)
            }
        case 2:
            if let view = viewControllerAtIndex(index: 2) {
                DispatchQueue.main.async {
                    self.highLightOKOfficesView()
                }
                pageViewController!.setViewControllers([view], direction: .forward, animated: false, completion: nil)
            }
        default:
            break
        }
    }
    
    //MARK: - Search Module
    func searchBarSettings() {
        topSearchBar.tintColor = UIColor.darkGray
        topSearchBar.barTintColor = UIColor.white
        topSearchBar.searchBarStyle = UISearchBarStyle.minimal
        
        let searchTextField: UITextField? = topSearchBar.value(forKey: "searchField") as? UITextField
        if searchTextField!.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
            let attributeDict = [NSAttributedStringKey.foregroundColor: UIColor.lightGray]
            searchTextField!.attributedPlaceholder = NSAttributedString(string: "Search...", attributes: attributeDict)
            searchTextField!.backgroundColor = UIColor.white
            searchTextField!.borderStyle = UITextBorderStyle.none
            searchTextField!.layer.borderColor = UIColor.clear.cgColor
            searchTextField!.layer.borderWidth = 0.0
            searchTextField!.textColor = UIColor.black
            searchTextField!.autocorrectionType = UITextAutocorrectionType.no
            searchTextField!.font = UIFont.init(name: appFont, size: 15)
            searchTextField!.leftViewMode = UITextFieldViewMode.never
        }
        
    }
    
    func resignSearchBarKeypad() {
        topSearchBar.resignFirstResponder()
    }
    
    func hideAndShowHeaderSearchBtn(isShow: Bool) {
        headerSearchBtn.isHidden = isShow ? false : true
    }
    
    func reloadListViewAfterSwitch()
    {
        switch currentNearbyView {
            case .merchantView:
                self.listPromotionsView?.nearByServicesList = arrMerchantList
             case .okAgentsView:
                self.listPromotionsView?.nearByServicesList = arrAgentsList
             case .okOfficesView:
                self.listPromotionsView?.nearByServicesList = arrOfficeList
        }
        self.listPromotionsView?.showHideInfoLabel(isShow: false, content: "")
        self.listPromotionsView?.listTableView.reloadData()

    }
    
    
    @IBAction func switchMapListBtnAction(_ sender: Any) {
        switch currentNearbyView {
        case .merchantView:
            let uitype: UIType = (self.switchMapListView())
            let imagename = (uitype == .listView) ? "map_white" : "list"
            
            if(imagename == "list")
            {
                self.hideAndShowHeaderSearchBtn(isShow: false)
            }
            else
            {
                self.hideAndShowHeaderSearchBtn(isShow: true)
            }
            switchMapListBtn.setImage(UIImage.init(named: imagename), for: .normal)
        case .okAgentsView:
            let uitype: UIType = (self.switchMapListView())
            let imagename = (uitype == .listView) ? "map_white" : "list"
            if(imagename == "list")
            {
                self.hideAndShowHeaderSearchBtn(isShow: false)
            }
            else
            {
                self.hideAndShowHeaderSearchBtn(isShow: true)
            }
            switchMapListBtn.setImage(UIImage.init(named: imagename), for: .normal)
        case .okOfficesView:
            let uitype: UIType =  (self.switchMapListView())
            let imagename = (uitype == .listView) ? "map_white" : "list"
            if(imagename == "list")
            {
                self.hideAndShowHeaderSearchBtn(isShow: false)
            }
            else
            {
                self.hideAndShowHeaderSearchBtn(isShow: true)
            }
            switchMapListBtn.setImage(UIImage.init(named: imagename), for: .normal)
            //        default:
            //            break
            
        }
    }
    
    func switchMapListView() -> UIType {
        if mapUiType == .listView {
            mapUiType = .mapView
        }else {
            mapUiType = .listView
        }
        DispatchQueue.main.async {
            self.switchBaseView()
        }
        return mapUiType!
    }
    
    func switchBaseView() {
        if mapUiType == .listView {
            if let map = mapClusterView {
                map.view.removeFromSuperview()
            }
            
            listPromotionsView?.nav = self.navigationController
        
            listPromotionsView!.view.frame = CGRect(x: 0.0, y: 0.0, width: screenArea.size.width, height: baseViewHeight)// 260)
            
            listPromotionsView?.view.backgroundColor = UIColor.white
            
            switch currentNearbyView {
            case .merchantView:
                self.listPromotionsView?.nearByServicesList = arrMerchantList
            case .okAgentsView:
                self.listPromotionsView?.nearByServicesList = arrAgentsList
            case .okOfficesView:
                self.listPromotionsView?.nearByServicesList = arrOfficeList
            }
            self.listPromotionsView?.showHideInfoLabel(isShow: false, content: "")
            self.listPromotionsView?.listTableView.reloadData()
            
            self.baseView.addSubview(self.listPromotionsView!.view)

        }else {
            if let list = listPromotionsView {
                list.view.removeFromSuperview()
            }
            mapClusterView             = NearByMapClusterViewController()
            //            mapClusterView?.view.frame = baseView.bounds
            mapClusterView?.view.frame = CGRect(x: 0.0, y: 0.0, width: screenArea.size.width, height: baseViewHeight)
            
            baseView.addSubview(mapClusterView!.view)
            
                switch currentNearbyView {
                case .merchantView:
                    mapClusterView?.addAnnotationsInMap(list: arrMerchantList)
                case .okAgentsView:
                    mapClusterView?.addAnnotationsInMap(list: arrAgentsList)
                case .okOfficesView:
                    mapClusterView?.addAnnotationsInMap(list: arrOfficeList)
                }
            
        }
    }
    
    func checkAndLoadCollectionView() {

        collectItems     = listViewDetails.collectionItems
        collectImages    = listViewDetails.collectionImages
        highLightImages  = listViewDetails.highLightImages
        
        self.refreshSortFilter()
        cellsPerRow = collectItems.count
        collectionView.reloadData()
    }
    
    func refreshSortFilter() {
        selectedSortItem       = 0
        selectedFilterData     = nil
    }
    
    // MARK: - Custom Delegate Methods
    
    func didSelectLocationByCurrentLocation() {
        updateCurrentLocationToUI()
        setSelectedLocationType(locationtype: .byCurrentLocation) // division , state both are similar oly
        guard let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude else {
            // throw the error to the user
            return
        }
        
        self.getOfficeAPI(lat: lat , long: long )

    }
    
    func didSelectLocationByTownship(location: LocationDetail, township: TownShipDetail) {
        updateOtherLocationToUI(location: location, township: township)
        setSelectedLocationType(locationtype: .byDivision) // division , state both are similar oly
        // need to check how to get this data
//        let mobilenumber  = UserModel.shared.mobileNo
//        let apiUrlStr = getNearByMerchantsWithPromotionsAPI(shopname: "", townshipcode: township.townShipCode, divisioncode: location.stateOrDivitionCode, businesstype: "", businesscategory: "", neardistance: nearbydistance, city: "", mobilenumber: mobilenumber, lat: geoLocManager.currentLatitude,long: geoLocManager.currentLongitude)
//        println_debug("nearbymerchant bytownship api:::: \(apiUrlStr)")
        
        self.getOfficeAPI(lat: geoLocManager.currentLatitude , long: geoLocManager.currentLongitude)

    }
    
    func didSelectLocationBySearch(searchKey: String, isCurLocSearch: Bool) { //SearchPromotionDelegate
        setSelectedLocationType(locationtype: .bySearch)
        if isCurLocSearch {
            updateCurrentLocationToUI()
        }
        let mobilenumber  = UserModel.shared.mobileNo
        var divisionCode = ""
        var townshipCode = ""
        if let loc = selectedLocationDetail {
            if let township = selectedTownshipDetail {
                divisionCode = loc.stateOrDivitionCode
                townshipCode = township.townShipCode
            }
        }
        /*
        let apiUrlStr = getNearByMerchantsWithPromotionsAPI(shopname: searchKey, townshipcode: townshipCode, divisioncode: divisionCode, businesstype: "", businesscategory: "", neardistance: nearbydistance, city: "", mobilenumber: mobilenumber, lat: geoLocManager.currentLatitude,long: geoLocManager.currentLongitude)
        println_debug("nearbymerchant bysearch api:::: \(apiUrlStr)")
        //callNearByMerchantsWithPromotionsAPI(api: apiUrlStr)
        
        self.getOfficeAPI(lat: geoLocManager.currentLatitude , long: geoLocManager.currentLongitude)
         */
        
    }
    
    //MARK: - Category Delegate
    
    func didSelectLocationByCategory(category: SubCategoryDetail?) {

        var mainCategoryName = ""
        var subCategoryName = ""
        
        if let categ = category {
            mainCategoryName = categ.mainCategoryName
            subCategoryName = categ.subCategoryName
        }
    
        if let _ = category {
            self.hightLightNeeded(isHighLight: true, from: "category")
        }else {
            self.hightLightNeeded(isHighLight: false, from: "category")
        }
        
        println_debug(mainCategoryName)
    
        var arrTempList = [NearByServicesModel]()
        
        switch currentNearbyView {
        case .merchantView:
            arrTempList = arrMerchantList
        case .okAgentsView:
            arrTempList = arrAgentsList
        case .okOfficesView:
            arrTempList = arrOfficeList
        }
        
        let filteredArray = arrTempList.filter { ($0.category)?.range(of: mainCategoryName, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
        
        if((filteredArray.count) > 0)
        {
             let subCatArray = arrTempList.filter { ($0.subCategory)?.range(of: subCategoryName, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
            if(subCatArray.count > 0)
            {
                if self.mapUiType == .listView  {

                    DispatchQueue.main.async {
                        self.listPromotionsView?.showHideInfoLabel(isShow: false, content: "")
                        self.listPromotionsView?.nearByServicesList = subCatArray
                        self.listPromotionsView?.listTableView.reloadData()
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        self.mapClusterView?.addAnnotationsInMap(list: subCatArray)
                    }
                }
                
            }
            else
            {
                self.listPromotionsView?.showHideInfoLabel(isShow: true, content: "No Record Found".localized)
            }
        }
        else
        {
            self.listPromotionsView?.showHideInfoLabel(isShow: true, content: "No Record Found".localized)
        }
    }
    
    func hightLightNeeded(isHighLight : Bool, from: String) {

        if isHighLight {
    if from == "category" { // categories
    highLightImages = [true, false, false]
    }else if from == "sort" { // sort
    highLightImages = [false, true, false]
    
    }else { // filter
    highLightImages = [false, false, true]
    
    }
    }else {
    highLightImages = [false, false, false]
    }
    self.collectionView?.reloadData()
    }
}


extension NearByServicesViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        didSelectSearchWithKey(key: searchBar.text!)
        
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        didSelectSearchWithKey(key: searchBar.text!)
    }
    
    
    func didSelectSearchWithKey(key: String) {
        
        var arrTempList = [NearByServicesModel]()

        switch currentNearbyView {
        case .merchantView:
            arrTempList = arrMerchantList
        case .okAgentsView:
            arrTempList = arrAgentsList
        case .okOfficesView:
            arrTempList = arrOfficeList
        }
    
        if key.count > 0 {
            
            let filteredArray = arrTempList.filter { ($0.businessName)?.range(of: key, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
            
            if((filteredArray.count) > 0)
            {
                    DispatchQueue.main.async {
                    
                        self.listPromotionsView?.showHideInfoLabel(isShow: false, content: "")
                        self.listPromotionsView?.nearByServicesList = filteredArray
                        self.listPromotionsView?.listTableView.reloadData()
                    }
            
            }
            else
            {
                let filteredArray = arrTempList.filter { ($0.firstName)?.range(of: key, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
        
                        if filteredArray.count > 0 {
                            DispatchQueue.main.async {
                                
                                self.listPromotionsView?.showHideInfoLabel(isShow: false, content: "")
                                self.listPromotionsView?.nearByServicesList = filteredArray
                                self.listPromotionsView?.listTableView.reloadData()
                            }
                        }else {
                            DispatchQueue.main.async {
                                self.listPromotionsView?.showHideInfoLabel(isShow: true, content: "No Results Found".localized)
                            }
                        }
            }
        } else {
            println_debug("Text field is cleared")
            reloadListViewAfterSwitch()
        }
    }
}

extension NearByServicesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return collectItems.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mapcollectioncellidentifier", for: indexPath) as! NearByServicesCollectionCell
        cell.wrapData(title: collectItems[indexPath.row], imgName: collectImages[indexPath.row], hightLights: highLightImages[indexPath.row])
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if indexPath.row == 0 {
                // this is to show categories list view
                showCategoriesView()
            }else if indexPath.row == 1 {
                // this is to show sort list view
                showSortListView()
            }else if indexPath.row == 2 {
                // this to show filter view
                showFilterListView()
            }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellHeight = self.collectionView.frame.height
        let flowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let marginsAndInsets = flowLayout.sectionInset.left + flowLayout.sectionInset.right + flowLayout.minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        return CGSize(width: itemWidth, height: cellHeight)
    }
   
    //MARK: - Category Sort Filter Options
    
    func showCategoriesView() {
        let storyBoardName = UIStoryboard(name: "Promotion", bundle: nil)
        let categoriesView = storyBoardName.instantiateViewController(withIdentifier: "CategoriesListView_ID") as! CategoriesListViewController
        categoriesView.delegate = self
        categoriesView.modalPresentationStyle = .overCurrentContext
        self.present(categoriesView, animated: true, completion: nil)
    }
    
    func showSortListView() {
        
        var arrTempList = [NearByServicesModel]()
        
        switch currentNearbyView {
        case .merchantView:
            arrTempList = arrMerchantList
        case .okAgentsView:
            arrTempList = arrAgentsList
        case .okOfficesView:
            arrTempList = arrOfficeList
        }
        
        if arrTempList.count == 0  {
            self.showToast(message: "No Results Found".localized, align: .top)
            return
        }
        
        sortView.delegate = self
        //sortView.viewFrom = .promotion
        sortView.previousSelectedSortOption = selectedSortItem
        sortView.dataList = arrTempList // this is used to sort all the promotions
        sortView.modalPresentationStyle = .overCurrentContext
        self.present(sortView, animated: true, completion: nil)
    }
    
    func showFilterListView() {
        
        var arrTempList = [NearByServicesModel]()
        
        switch currentNearbyView {
        case .merchantView:
            arrTempList = arrMerchantList
        case .okAgentsView:
            arrTempList = arrAgentsList
        case .okOfficesView:
            arrTempList = arrOfficeList
        }
        
        if arrTempList.count == 0  {
            self.showToast(message: "No Results Found".localized, align: .top)
            return
        }
        
        filterView.delegate = self
        //filterView.viewFrom = .promotion
        filterView.previousSelectedFilterData = self.selectedFilterData
        filterView.filterDataList = arrTempList
        filterView.modalPresentationStyle = .overCurrentContext
        self.present(filterView, animated: true, completion: nil)
    }
    
    func didSelectSortOption(sortedList: [Any], selectedSortOption: Int)  {
        
        var arrTempList = [NearByServicesModel]()
        
        switch currentNearbyView {
        case .merchantView:
            arrTempList = arrMerchantList
        case .okAgentsView:
            arrTempList = arrAgentsList
        case .okOfficesView:
            arrTempList = arrOfficeList
        }
        
        if selectedSortOption == 0 {
            // default selected so load the data get from server
            self.selectedSortItem = selectedSortOption
            listPromotionsView?.nearByServicesList = arrTempList
            
            self.hightLightNeeded(isHighLight: false, from: "sort")
            
        }else {
            if let list = sortedList as? [NearByServicesModel] {
                listPromotionsView?.nearByServicesList = list
                self.hightLightNeeded(isHighLight: true, from: "sort")
            }
            self.selectedSortItem = selectedSortOption
            
        }
        DispatchQueue.main.async {
            self.listPromotionsView?.showHideInfoLabel(isShow: false, content: "")
            self.listPromotionsView?.listTableView.reloadData()
        }
        
    }
    
    func didSelectFilterOption(filteredList: [Any], selectedFilter: Any?) {
        
        var arrTempList = [NearByServicesModel]()
        
        switch currentNearbyView {
        case .merchantView:
            arrTempList = arrMerchantList
        case .okAgentsView:
            arrTempList = arrAgentsList
        case .okOfficesView:
            arrTempList = arrOfficeList
        }
        
        if self.mapUiType == .listView  {
            if let selectedfilter = selectedFilter as? NearByServicesModel {
                self.selectedFilterData = selectedfilter
                self.listPromotionsView?.nearByServicesList = filteredList as! [NearByServicesModel]
            }else{
                // here is the default filter selection coming
                self.selectedFilterData = nil
                self.listPromotionsView?.nearByServicesList = arrTempList
            }
            DispatchQueue.main.async {
                self.listPromotionsView?.showHideInfoLabel(isShow: false, content: "")
                self.listPromotionsView?.listTableView.reloadData()
            }
        }else {
            if let selectedfilter = selectedFilter as? NearByServicesModel  {
                self.selectedFilterData = selectedfilter
                DispatchQueue.main.async {
                    self.mapClusterView?.addAnnotationsInMap(list: filteredList as! [NearByServicesModel])
                }
            }else{
                // here is the default filter selection coming
                self.selectedFilterData = nil
                DispatchQueue.main.async {
                    self.mapClusterView?.addAnnotationsInMap(list: arrTempList)
                }
            }
            
        }
        
        if let _ = selectedFilterData {
            self.hightLightNeeded(isHighLight: true, from: "filter")
        }else {
            self.hightLightNeeded(isHighLight: false, from: "filter")
        }
    }
    
}


class NearByServicesCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var midSeparatorLbl  : UILabel!
    //    @IBOutlet weak var listBtn          : UIButton!
    @IBOutlet weak var highlightImgV    : UIImageView!
    @IBOutlet weak var itemImgV         : UIImageView!
    @IBOutlet weak var itemNameLbl      : UILabel!
    
    func wrapData(title: String, imgName: String, hightLights: Bool) {
        
        //        self.listBtn.setTitle(title, for: UIControlState.normal)
        //        self.listBtn.setImage(UIImage.init(named: imgName), for: UIControlState.normal)
        
        self.itemNameLbl.text = title
        self.itemImgV.image = UIImage.init(named: imgName)
        
        if hightLights {
            self.highlightImgV.isHidden = false
        }else {
            self.highlightImgV.isHidden = true
        }
        
        //        self.highlightImgV.isHidden =   (hightLights == true) ? false : true
        self.highlightImgV.image    =   UIImage.init(named: "bank_success")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //        self.listBtn.setTitleColor(UIColor.black, for: UIControlState.normal)
        //        self.listBtn.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        //        self.listBtn.titleLabel?.font = UIFont.init(name: appFont, size: 13.0)
        
        self.itemNameLbl.font = UIFont.init(name: appFont, size: 13.0)
        self.itemNameLbl.textColor = UIColor.black
    }
}
