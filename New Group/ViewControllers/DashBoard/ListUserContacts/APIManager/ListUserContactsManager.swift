//
//  ListUserContactsManager.swift
//  OK
//
//  Created by gauri OK$ on 6/6/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit
import CoreData

class ListUserContactsManager: NSObject {

    class func fetchListUserContacts(completionHandler: @escaping (Bool,[NearByServicesNewModel]?) -> Void) {
        let url = getUrl(urlStr: "", serverType: .ListUserContacts)
        let urlRequest = JSONParser.PrepareJsonRequest(apiUrl: url, methodType: kMethod_Get, contentType: kContentType_Json,
                                                       inputVal: nil, authStr: "")
        JSONParser.GetApiResponseWithRequest(apiUrlReq: urlRequest, { (isSuccess, response) in
            guard isSuccess else {
                completionHandler(false, nil)
                return
            }

            DispatchQueue.main.async(execute: {
                if let responseDic = response as? Dictionary<String,Any> {
                    let jsonData = JSON(responseDic)

                    if jsonData["StatusCode"].stringValue == "200" {
                        var userArray = [NearByServicesNewModel]()
                        userArray = ParserAttributeClass.parseNearbyServiceJSONNew(anyValue: response as AnyObject) as [NearByServicesNewModel]
                        println_debug("response count for ok offices :::: \(userArray.count)")
                        
                        completionHandler(true, userArray)
                    }
                }
            })
 
        })
    }
    
    class func insertAllRecordsToBankDatabase(model: [NearByServicesNewModel]) {
        println_debug("Called to insert")
    }
}
