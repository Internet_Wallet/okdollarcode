//
//  QRInfoAdertise.swift
//  OK
//
//  Created by gauri on 11/6/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit
import AVFoundation
class QRInfoAdertise: OKBaseController, PaytoScrollerDelegate, TBScanQRDelegate {
    
    fileprivate var myScanMenu : PaytoScroller?
    fileprivate var controllerArray : [UIViewController] = []
    var scanDelegate : TBScanQRDelegate?
    var fromSideMenu : Bool = false
    var beforePayment = ""
    var afterPayment = ""
    var beforeMessage = ""
    var afterMessage = ""
    var selected = "before"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(true, forKey: "QRFirstItem")
        UserDefaults.standard.synchronize()
        self.setPOPButton()
        //MyNumberTopup.theme = UIColor.init(red: 28.0/255.0, green: 32.0/255.0, blue: 135.0/255.0, alpha: 1)
        self.view.backgroundColor = UIColor.blue
        let beforePay = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: QRBeforePayment.self)) as? QRBeforePayment
        beforePay?.title = "Before Payment".localized
        beforePay?.navigation = self.navigationController
        //        beforePay?.scanDelegate = self
        
        let afterPay = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: QRAfterPayment.self)) as? QRAfterPayment
        afterPay?.navigation = self.navigationController
        afterPay?.title = "After Payment".localized
        afterPay?.itemNavigation = self.navigationItem
        controllerArray = [beforePay!, afterPay!]
        
        let parameters = [PaytoScrollerOptionMenuItemSeparatorWidth: 0, PaytoScrollerOptionMenuItemSeparatorPercentageHeight: 0.1,
                          PaytoScrollerOptionScrollMenuBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectedMenuItemLabelColor: UIColor.init(red: 28.0/255.0, green: 32.0/255.0, blue: 135.0/255.0, alpha: 1),
                          PaytoScrollerOptionViewBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectionIndicatorColor: UIColor.init(red: 28.0/255.0, green: 32.0/255.0, blue: 135.0/255.0, alpha: 1),
                          PaytoScrollerOptionUnselectedMenuItemLabelColor: UIColor.black,
                          PaytoScrollerOptionMenuHeight: 50.0,
                          PaytoScrollerOptionEnableHorizontalBounce: true,
                          PaytoScrollerOptionAddBottomMenuHairline: true,
                          PaytoScrollerOptionBottomMenuHairlineColor: UIColor.gray,
                          PaytoScrollerOptionMenuItemFont: "Zawgyi-One",
                          PaytoScrollerOptionMenuItemWidth: (screenWidth / 2.0) - 10.0] as [String : Any]
        
        
        if device.type == .iPhoneX || device.type == .iPhoneXR || device.type == .iPhoneXS || device.type == .iPhoneXSMax || device.type == .iPhone11 || device.type == .iPhone11Pro || device.type == .iPhone11ProMax || device.type == .iPhone12Pro || device.type == .iPhone12ProMax {
            myScanMenu = PaytoScroller(viewControllers: controllerArray, frame: CGRect.init(x: 0, y: 84, width: screenWidth, height: screenHeight - 84), options: parameters)
        }else {
            myScanMenu = PaytoScroller(viewControllers: controllerArray, frame: CGRect.init(x: 0, y: 64, width: screenWidth, height: screenHeight - 64), options: parameters)
        }
        myScanMenu?.delegate = self
        
        myScanMenu?.scrollingEnable(false)
        myScanMenu?.change(forButtonTap: true)
        self.view.addSubview(myScanMenu!.view)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    
    
    fileprivate func setPOPButton() {
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 18.0) ?? UIFont.systemFont(ofSize: 18.0), NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "Informaiton & Advertisement".localized
        self.navigationController?.navigationBar.tintColor = .white
    }
    
    
    func willMove(toPage controller: UIViewController!, index: Int) {
        self.navigationItem.rightBarButtonItem = nil
    }
    
    @objc func showDetail() {
        guard let sanResultDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "QRScanResultDetailViewController") as? QRScanResultDetailViewController else { return }
        self.navigationController?.pushViewController(sanResultDetailViewController, animated: true)
    }
    
    func didMove(toPage controller: UIViewController!, index: Int) {
        if index == 0 {
            selected = "before"
        } else {
            selected = "after"
        }
        println_debug(selected)
    }
    
    func moveToPayto(withScanObject object: PTScanObject, fromTransfer: Bool?) {
        if let delegate = self.scanDelegate {
            delegate.moveToPayto(withScanObject: object, fromTransfer: false)
        } else {
            self.navigateToPayto(phone: object.agentNo, amount: object.amount, name: object.name, remark: object.remarks)
        }
    }
    
    func moveToPayTo(fromTransfer: Bool?) {
        if let delegate = self.scanDelegate {
            delegate.moveToPayTo(fromTransfer: false)
        }
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        
        println_debug(selected)
        self.dismiss(animated: true, completion: nil)
    }
}

