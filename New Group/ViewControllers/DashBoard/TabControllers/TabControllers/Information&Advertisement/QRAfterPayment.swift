//
//  QRAfterPayment.swift
//  OK
//
//  Created by gauri on 11/6/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit
import CoreData
import AVFoundation
import IQKeyboardManagerSwift


class QRAfterPayment: OKBaseController {
    
    //MARK: - IBOUTLETS
    
    @IBOutlet weak var tbview: UITableView!
  
    @IBOutlet weak var btnSave: UIButton! {
        didSet {
            btnSave.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
          btnSave.setTitle("Save".localized, for: .normal)
        }
    }
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lblHeader: UILabel! {
        didSet {
            lblHeader.font = UIFont(name: appFont, size: appFontSize)
            lblHeader.text = "Information to show after payment".localized
        }
    }
    @IBOutlet weak var saveButtonTopConstant: NSLayoutConstraint!

    //MARK: - CLASS VARIABLES
    
    var itemNavigation : UINavigationItem?
    var navigation : UINavigationController?
    var messageListTemp = [String]()
    var created_Date = [String]()
    var selectedRowTemp = 10
    var CHARSET = ""
    var isSavedAfterDelete = false
    //MARK: - UIVIEWCONTROLLES METHODS
    
    var messageFirst = ""
    var messageSecond = ""
    var messageThird = ""
    var messageFour = ""
    var messageFive = ""
    var isComingFromEdit = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if appDelegate.currentLanguage == "en" {
            CHARSET = SEARCH_CHAR_SET_En
        }else if appDelegate.currentLanguage == "my"{
            CHARSET = SEARCH_CHAR_SET_My
        }else {
            CHARSET = SEARCH_CHAR_SET_Uni
        }
        
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        tbview.tableFooterView = UIView.init(frame: CGRect.zero)
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "AfterPaymentInfo")
        let result = try? managedContext.fetch(fetch)
        
        if (result?.count)! > 0 {
            let after = result?[0] as? AfterPaymentInfo
            if let afterData = after?.afterDataInfo {
                messageListTemp = afterData as! [String]
                let sel = after?.afterSelectedData
                selectedRowTemp = Int(sel ?? "1") ?? 1
                if selectedRowTemp == 10 {
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "", alertBody: "You can advertise offers, promotion & deal to your customers after payments".localized, alertImage: UIImage(named: "infoQR")!)}
                }
            }
            if let date = after?.createdDate {
                        created_Date = date as! [String]
                        if created_Date.count == 0 {
                            let currentDate = Date()
                            let fmt = DateFormatter()
                            fmt.dateFormat = "dd-MMM-yyyy"
                            let someDateTime = fmt.string(from: currentDate)
                            created_Date.append(someDateTime)
                        }
                    }
        }else {
            DispatchQueue.main.async {
                self.showAlert(alertTitle: "", alertBody: "You can advertise offers, promotion & deal to your customers after payments".localized, alertImage: UIImage(named: "infoQR")!)}
        }
        
        if messageFirst != "" {
            if let obj = messageListTemp[safe: 0] {
                print(obj)
            }else{
                messageListTemp.append(messageFirst)
            }
        }
        if messageSecond != "" {
            if let obj = messageListTemp[safe: 1] {
                 print(obj)
            }else{
                messageListTemp.append(messageSecond)
            }
        }
        if messageThird != "" {
            if let obj = messageListTemp[safe: 2] {
                 print(obj)
            }else{
                messageListTemp.append(messageThird)
            }
        }
        if messageFour != "" {
            if let obj = messageListTemp[safe: 3] {
                 print(obj)
            }else{
                messageListTemp.append(messageFour)
            }
        }
        if messageFive != "" {
            if let obj = messageListTemp[safe: 4] {
                 print(obj)
            }else{
                 messageListTemp.append(messageFive)
            }
        }
    
        btnSave.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.8078431373, blue: 0.2745098039, alpha: 1)
        btnSave.setTitleColor(.white , for: .normal)
        btnSave.isUserInteractionEnabled = true
        saveButtonTopConstant.constant = 50
        
        if messageListTemp.count > 4 {
            btnAdd.isHidden = true
        }else {
            btnAdd.isHidden = false
        }
        tbview.reloadData()
    }
    
    //MARK: - IBACTIONS
    
    @IBAction func onClickAdd(_ sender: Any) {
        if messageListTemp.count < 5 {
            if messageListTemp.last != "" {
                btnAdd.isHidden = false
                self.isComingFromEdit = true
                messageListTemp.append("")
                let currentDate = Date()
                               let fmt = DateFormatter()
                               fmt.dateFormat = "dd-MMM-yyyy"
                               let someDateTime = fmt.string(from: currentDate)
                               created_Date.append(someDateTime)
                tbview.beginUpdates()
                tbview.insertRows(at: [IndexPath(row: messageListTemp.count-1, section: 0)], with: .automatic)
                tbview.endUpdates()
                
                if messageListTemp.count == 5 {
                    btnAdd.isHidden = true
                }else{
                    btnAdd.isHidden = false
                }
                saveButtonTopConstant.constant = 50
            }else {
               
                DispatchQueue.main.async {
                    self.showAlert(alertTitle: "", alertBody: "Information is empty. Please enter information before save".localized, alertImage: UIImage(named: "infoQR")!)}
            }
        }else {
            saveButtonTopConstant.constant = 0
            btnAdd.isHidden = true
        }
        println_debug("cell added \(selectedRowTemp)")
    }
    @IBAction func onClickSave(_ sender: Any) {
        var tempMessages = messageListTemp
        let tempDate = created_Date
        messageListTemp.removeAll()
        created_Date.removeAll()
        var index = 0
        for msg in tempMessages {
            if !msg.isEmpty && msg != "Enter Information".localized{
                messageListTemp.append(msg)
            }
            if let cell = tbview.cellForRow(at: NSIndexPath(row: index, section: 0) as IndexPath) as? AfterCell {
                cell.tvEnterInfo.isUserInteractionEnabled = false
            }
            index = index + 1
        }
        
        for date in tempDate {
                  if !date.isEmpty {
                  created_Date.append(date)
                  }
              }
         messageFirst = ""
         messageSecond = ""
         messageThird = ""
         messageFour = ""
         messageFive = ""
        
            let context = appDelegate.persistentContainer.viewContext
            let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "AfterPaymentInfo")
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
            
            do {
                try context.execute(deleteRequest)
                try context.save()
            } catch {
                println_debug("Deleting all records in favorites")
            }
            
            let afterPay = NSEntityDescription.insertNewObject(forEntityName: "AfterPaymentInfo", into: context) as? AfterPaymentInfo
            afterPay?.afterDataInfo = messageListTemp as NSObject
            afterPay?.afterSelectedData = String(selectedRowTemp)
            afterPay?.createdDate = created_Date as NSObject
            do {
                try context.save()
                if isSavedAfterDelete {
                 // No need to show alert after delete templete
                    isSavedAfterDelete = false
                }else {
                    DispatchQueue.main.async {
                        self.showAlert(alertTitle: "", alertBody: "Saved successfully".localized, alertImage: UIImage(named: "check_sucess_bill")!)}
                    self.saveButtonTopConstant.constant = 50
                }
                          } catch let error as NSError {
                            DispatchQueue.main.async {
                                self.showAlert(alertTitle: "", alertBody: "Template did not saved".localized, alertImage: UIImage(named: "infoQR")!)}
                println_debug("Could not save. \(error), \(error.userInfo)")
            }
        messageListTemp = tempMessages
        tempMessages.removeAll()

    }
    
    @IBAction func onClickInformation(_ sender: Any) {
        showAlert(alertTitle: "", alertBody: "You can advertise offers, promotion & deal to your customers after payments".localized, alertImage: UIImage(named: "infoQR")!)
    }
    
    //MARK: - ADD TARGET BUTTON ACTIONS
    @objc func clickOnSelectedIndes(_ sender : UIButton) {
      
        let cell: AfterCell = self.tbview.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! AfterCell
        
        if cell.tvEnterInfo.text.count > 0 && cell.tvEnterInfo.text != "Enter Information".localized {
            if cell.btnSelectCell.currentImage == UIImage(named: "checkboxTick") {
                selectedRowTemp = 10
                cell.btnSelectCell.setImage(UIImage(named: "checkboxBlank"), for: .normal)
            }else {
                for i in 0 ... 4 {
                    if let cell = self.tbview.cellForRow(at: IndexPath(row: i, section: 0)) as? AfterCell {
                        cell.btnSelectCell.setImage(UIImage(named: "checkboxBlank"), for: .normal)
                    }
                }
                selectedRowTemp = cell.tvEnterInfo.tag
                cell.btnSelectCell.setImage(UIImage(named: "checkboxTick"), for: .normal)
            }
            
           // self.tbview.reloadData()
            messageListTemp[cell.tvEnterInfo.tag] = cell.tvEnterInfo.text ?? ""
            let btn = UIButton()
            self.isSavedAfterDelete = true
            self.onClickSave(btn)
        }
    }
    
    //MARK: - COMMAN METHODS
    
    private func showAlert(alertTitle : String ,alertBody : String, alertImage : UIImage) {
        alertViewObj.wrapAlert(title:alertTitle, body:alertBody, img:alertImage)
        alertViewObj.addAction(title: "OKQR".localized, style: .target , action: {
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
}


//MARK: - EXTENSIONS OF UITABLEVIEW DELEGATE & DATA SOURCE

extension QRAfterPayment : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return messageListTemp.count
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoteCellAfter") as! NoteCellAfter
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor.clear
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AfterCell") as! AfterCell
            cell.selectionStyle = .none
            cell.lblHeaderCell.text = "Information - A".localized + String(indexPath.row + 1)
            let date = created_Date[indexPath.row]
            cell.lblDateCell.text = date
            let item = messageListTemp[indexPath.row]
            cell.tvEnterInfo.tag = indexPath.row
            cell.tvEnterInfo.text = item
            cell.tvEnterInfo.delegate = self
            
            if item == "" || item == "Enter Information".localized {
                cell.tvEnterInfo.isUserInteractionEnabled = true
                cell.tvEnterInfo.text = "Enter Information".localized
                cell.tvEnterInfo.textColor = UIColor.lightGray
            }else {
                cell.tvEnterInfo.isUserInteractionEnabled = false
                cell.tvEnterInfo.textColor = UIColor.black
            }
            
            cell.btnSelectCell.tag = indexPath.row
            
            var font = UIFont(name: appFont, size: 18.0)
            if #available(iOS 13.0, *) {
                font = UIFont.systemFont(ofSize: 18)
            }
            cell.tvEnterInfo.font = font
            
            if appDel.currentLanguage == "my" {
                cell.tvEnterInfo.font = UIFont(name: appFont, size: 18)
            }else if appDel.currentLanguage == "en"{
                cell.tvEnterInfo.font = UIFont.systemFont(ofSize: 18)
            }else {
                
                if #available(iOS 13.0, *) {
                    // let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                    // .font : UIFont(name: appFont, size: 18)]
                    // cell.tvEnterInfo.attributedPlaceholder = NSAttributedString(string: "Enter Information", attributes:attributes as [NSAttributedString.Key : Any])
                } else {
                    // let attributes = [NSAttributedString.Key.foregroundColor: UIColor.gray,
                    //     .font : UIFont.systemFont(ofSize: 18)]
                    //cell.tvEnterInfo.attributedPlaceholder = NSAttributedString(string: "Enter Information", attributes:attributes)
                    cell.tvEnterInfo.font = UIFont.systemFont(ofSize: 18)
                }
            }
            
            cell.btnSelectCell.addTarget(self, action: #selector(clickOnSelectedIndes), for: .touchUpInside)
            
            if cell.btnSelectCell.tag == selectedRowTemp {
                cell.btnSelectCell.setImage(UIImage(named: "checkboxTick"), for: .normal)
            }else {
                cell.btnSelectCell.setImage(UIImage(named: "checkboxBlank"), for: .normal)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return 120
        }else{
            return 110
        }
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 1 {
            return false
        }else {
            return true
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        // Delete Button
        let deleteView = UIView(frame: CGRect(x: 0, y: 0, width: 70, height: 110))
        deleteView.backgroundColor = #colorLiteral(red: 0.8705882353, green: 0.2078431373, blue: 0.1803921569, alpha: 1)
        let detete = UIImageView(frame: CGRect(x: 20, y: 40, width: 30, height: 30))
        detete.image =  UIImage(named: "delete_icon")
        deleteView.addSubview(detete)
        
        let imgSize: CGSize = tableView.frame.size
        UIGraphicsBeginImageContextWithOptions(imgSize, false, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()
        deleteView.layer.render(in: context!)
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        let deleteCell = UITableViewRowAction(style: .destructive, title: "") { (action, indexPath) in
            let messageStirng = "Do you want to delete?".localized
            alertViewObj.wrapAlert(title: "", body: messageStirng, img: UIImage(named: "deleteicon"))
            alertViewObj.addAction(title: "NO".localized, style: .target , action: {
            })
            alertViewObj.addAction(title: "YES ".localized, style: .target , action: {
                if self.selectedRowTemp >= indexPath.row {
                    if self.messageListTemp.count == 1 {
                        self.selectedRowTemp = 10
                    }else if self.selectedRowTemp == indexPath.row {
                        self.selectedRowTemp = 10
                    }else{
                        if self.selectedRowTemp != 10 {
                            self.selectedRowTemp -= 1
                        }
                    }
                }
                self.messageListTemp.remove(at: indexPath.row)
                self.created_Date.remove(at: indexPath.row)
                if let _ = self.tbview.cellForRow(at: NSIndexPath(row: indexPath.row, section: 0) as IndexPath) as? AfterCell {
                    self.tbview.deleteRows(at: [indexPath], with: .automatic)
                }
                
                if self.messageListTemp.count < 5 {
                    self.btnAdd.isHidden = false
                }
                let btn = UIButton()
                self.isSavedAfterDelete = true
                self.onClickSave(btn)
                
                if self.messageListTemp.count == 0 {
                    self.saveButtonTopConstant.constant = 50
                }else {
                    for row in self.messageListTemp {
                        if row == "" {
                            self.saveButtonTopConstant.constant = 50
                        }else {
                            self.saveButtonTopConstant.constant = 0
                        }
                    }
                }
            })
         
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
        
        // Edit Button
        let editView = UIView(frame: CGRect(x: 0, y: 0, width: 70, height: 110))
        editView.backgroundColor = #colorLiteral(red: 0.1647058824, green: 0.7019607843, blue: 0.4588235294, alpha: 1) 
        
        let edit = UIImageView(frame: CGRect(x: 20, y: 40, width: 30, height: 30))
        edit.image =  UIImage(named: "edit_icon")
        editView.addSubview(edit)
        
        let imgSize1: CGSize = tableView.frame.size
        UIGraphicsBeginImageContextWithOptions(imgSize1, false, UIScreen.main.scale)
        let context1 = UIGraphicsGetCurrentContext()
        editView.layer.render(in: context1!)
        let newImage1: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        let editCell = UITableViewRowAction(style: .default, title: "") { (action, indexPath) in
            let cell = self.tbview.cellForRow(at: indexPath) as! AfterCell
            self.isComingFromEdit = true
             IQKeyboardManager.sharedManager().enableAutoToolbar = true
            cell.tvEnterInfo.isUserInteractionEnabled = true
             DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                            cell.tvEnterInfo.becomeFirstResponder()
                       })
        }
         println_debug("cell edited \(selectedRowTemp)")
        deleteCell.backgroundColor = UIColor(patternImage: newImage)
        editCell.backgroundColor = UIColor(patternImage: newImage1)
        return [editCell,deleteCell]
    }
    
}

extension QRAfterPayment : UITextViewDelegate {
    
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
           
           if textView.text?.count ?? 0 == 0 || textView.text == "Enter Information".localized {
              self.saveButtonTopConstant.constant = 50
            if let cell = tbview.cellForRow(at: NSIndexPath(row: textView.tag, section: 0) as IndexPath) as? AfterCell {
                           cell.tvEnterInfo.text = ""
                           cell.tvEnterInfo.textColor = UIColor.black
                       }
           }else{
               if isComingFromEdit{
                   textView.becomeFirstResponder()
                  // textView.isUserInteractionEnabled = true
                   IQKeyboardManager.sharedManager().enableAutoToolbar = true
               }else{
                //   textView.isUserInteractionEnabled = false
                   IQKeyboardManager.sharedManager().enableAutoToolbar = false
               }
            self.saveButtonTopConstant.constant = 0
           }
       }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        messageListTemp[textView.tag] = textView.text ?? ""
   
         messageListTemp[textView.tag] = textView.text!
               if isComingFromEdit{
                   isComingFromEdit = false
            
                   }
        if textView.text?.count ?? 0 == 0 {
                  saveButtonTopConstant.constant = 50
            if let cell = tbview.cellForRow(at: NSIndexPath(row: textView.tag, section: 0) as IndexPath) as? AfterCell {
                cell.tvEnterInfo.text = "Enter Information".localized
                cell.tvEnterInfo.textColor = UIColor.lightGray
            }
              }else {
                  saveButtonTopConstant.constant = 0
              }
     
        if let obj = messageListTemp[safe: 0] {
             messageFirst = obj
        }
        if let obj = messageListTemp[safe: 1] {
            messageSecond = obj
        }
        if let obj = messageListTemp[safe: 2] {
            messageThird = obj
        }
        if let obj = messageListTemp[safe: 3] {
            messageFour = obj
        }
        if let obj = messageListTemp[safe: 4] {
            messageFive = obj
        }
        
        // IQKeyboardManager.sharedManager().enableAutoToolbar = false
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let updatedText = (textView.text! as NSString).replacingCharacters(in: range, with: text)
                   if appDel.getSelectedLanguage() == "my" {
                        if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: SEARCH_CHAR_SET).inverted).joined(separator: "")) { return false }
                   } else if appDel.getSelectedLanguage() == "uni" {
                       if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: SEARCH_CHAR_SET).inverted).joined(separator: "")) { return false }
                   }else{
                       if !(updatedText == updatedText.components(separatedBy: NSCharacterSet(charactersIn: SEARCH_CHAR_SET).inverted).joined(separator: "")) { return false }
                   }
                   
                   if updatedText == " " || updatedText == "  "{
                       return false
                   }
                   if (updatedText.contains("  ")){
                       return false
                   }
        
        
        let containsEmoji: Bool = updatedText.containsEmoji
        if (containsEmoji){
            return false
        }
        if updatedText.count > 200 {
                  return false
              }
        
        return true
        
    }
}

//MARK: UITABLEVIEWCELL
class AfterCell : UITableViewCell{
    @IBOutlet weak var lblHeaderCell: UILabel!{
        didSet {
            lblHeaderCell.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var tvEnterInfo: UITextView!{
        didSet {
            tvEnterInfo.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var btnSelectCell: UIButton!
    @IBOutlet weak var lblDateCell: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

class NoteCellAfter: UITableViewCell {
    @IBOutlet weak var lblNote: UILabel! {
        didSet {
            lblNote.font = UIFont(name: appFont, size: appFontSize)
            lblNote.text = "Note".localized
        }
    }
    @IBOutlet weak var lblStatus: UILabel!{
        didSet {
            lblStatus.font = UIFont(name: appFont, size: appFontSize)
            lblStatus.text = "You can add & save upto 5 templates".localized
        }
    }
}



