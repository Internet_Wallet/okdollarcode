//
//  QRFormatModel.swift
//  OK
//
//  Created by Shobhit Singhal on 7/26/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class QRFormatModel: NSObject {
    var childArrayDataModel = [ChildArrayModel]()
    var sectionTitle:String = ""

    init(data: JSON) {
        self.sectionTitle = data["section_name"].stringValue
        if data["Child"].arrayObject != nil{
            let arrayData = data["Child"].arrayObject! as NSArray
            childArrayDataModel =  arrayData.map({(value) -> ChildArrayModel in
                return  ChildArrayModel(data:JSON(value))
            })
        }
    }
    
    var getChildQRFormatData:Array<ChildArrayModel>{
        return childArrayDataModel
    }
    
    func setChildQRFormatData(row: Int) {
        for i in 0..<childArrayDataModel.count {
            childArrayDataModel[i].isSelect = false
        }
        childArrayDataModel[row].isSelect = true
    }
}

class ChildArrayModel {
    var rowsTitle:String = ""
    var selectedField:String = ""
    var isSelect:Bool = false
    
    init(data: JSON) {
        self.rowsTitle = data["row_Title"].stringValue
        self.selectedField = data["selected_field"].stringValue
        self.isSelect = data["is_Select"].boolValue
    }
}

class QRFormatViewModel {
    var qrFormatModel = [QRFormatModel]()

    init(data:JSON) {
        let arrayData = data.arrayObject! as NSArray
        qrFormatModel =  arrayData.map({(value) -> QRFormatModel in
            return  QRFormatModel(data:JSON(value))
        })
    }
    
    var getSectionData:Array<QRFormatModel> {
        return qrFormatModel
    }
    
    func setChildCategory(section: Int, row: Int) {
        qrFormatModel[section].setChildQRFormatData(row:row)
    }
}
