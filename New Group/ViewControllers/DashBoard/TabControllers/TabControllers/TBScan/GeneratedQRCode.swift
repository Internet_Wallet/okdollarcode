//
//  GeneratedQRCode.swift
//  OK
//
//  Created by gauri on 11/3/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import AVFoundation
import Photos


class GeneratedQRCode: OKBaseController, timerDelegate {
    
    @IBOutlet weak var viewShare: UIView!
    @IBOutlet weak var imgUserbc: UIImageView!
    @IBOutlet weak var lblUserNamebc: UILabel!
    @IBOutlet weak var lblMobilNumer: UILabel!
    @IBOutlet weak var lblScanToPaybc: UILabel!{
        didSet{
            lblScanToPaybc.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblQRAmount: UILabel!
    @IBOutlet weak var imgQRbc: UIImageView!
    @IBOutlet weak var lblQRDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var personalMenu: UIStackView!
    @IBOutlet weak var merchantMenu: UIStackView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: MarqueeLabel!
    
    @IBOutlet weak var lblUserNumber: MarqueeLabel!
    @IBOutlet weak var lblScanToPay: UILabel!{
        didSet{
            lblScanToPay.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgQR: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblAmount: UILabel!{
        didSet{
            lblAmount.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var lblRemark: UILabel!{
        didSet{
            lblRemark.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var lblRemarkColon: UILabel!
    @IBOutlet weak var lblDateTime: MarqueeLabel!
    @IBOutlet weak var lblRemarkDescription: UILabel!
    @IBOutlet weak var lbbDateTimeDynamic: UILabel!{
        didSet{
            lbbDateTimeDynamic.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var lblBack: UILabel!{
        didSet{
            lblBack.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblShareQR: UILabel!{
        didSet{
            lblShareQR.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblSaveQR: UILabel!
    @IBOutlet weak var imgShareMerchant: UIImageView!
    @IBOutlet weak var imgSharePersonal: UIImageView!
    @IBOutlet weak var lblBackM: UILabel!{
        didSet{
            lblBackM.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblShareQRM: UILabel!{
        didSet{
            lblShareQRM.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblPrintQRM: UILabel!{
        didSet{
            lblPrintQRM.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblSaveQRM: UILabel!{
        didSet{
            lblSaveQRM.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgVerify: UIImageView!
    @IBOutlet weak var lblMobNumber: UILabel!
    @IBOutlet weak var lblQRTimer: UILabel!
    @IBOutlet weak var lblTimeType: UILabel!
    @IBOutlet weak var lblInfoQRExpire : UILabel!{
        didSet{
            lblInfoQRExpire.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    let qrCode = QRCodeGenericClass()
    var qrCodeInfoDic = Dictionary<String,String>()
    
    var timerQR: TimerModel?
    var totalTime = 120

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if appDel.currentLanguage == "uni"{
//            lblUserName.font = UIFont(name: appFont, size: appFontSize)
//       
//    }
        
        self.timerQR = TimerModel.sharedTimer
        self.timerQR?.timerDelegate = self
        self.lblQRTimer.text = "02:00"
        self.lblInfoQRExpire.text = "QR Code expires in".localized
        
        imgUserbc.layer.cornerRadius = imgUserbc.frame.width / 2
        imgUserbc.layer.cornerRadius = imgUserbc.frame.height / 2
        imgUserbc.layer.masksToBounds = true
        imgUserbc.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        imgUserbc.layer.borderWidth = 1.0

        imgUser.layer.cornerRadius = imgUser.frame.width / 2
        imgUser.layer.cornerRadius = imgUser.frame.height / 2
        imgUser.layer.masksToBounds = true
        imgUser.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        imgUser.layer.borderWidth = 1.0
        
        lblQRTimer.layer.cornerRadius = 5
        lblQRTimer.layer.masksToBounds = true
        lblQRTimer.layer.borderColor = #colorLiteral(red: 0.04988349229, green: 0.1793150902, blue: 0.6487618089, alpha: 1)
        lblQRTimer.layer.borderWidth = 1.0
      
       if kycAppControlInfo.first?.kycApprovalStatus == 1 {
               self.imgVerify.isHidden = false
             }else {
                self.imgVerify.isHidden = true
             }
        
        let (mobileNumber1,CountryCode1) = getMobileNumberWithoutZero(strToTrim: UserModel.shared.mobileNo)
  
        lblScanToPaybc.text = "Scan To PayQR".localized
    
        let (mobileNumber,CountryCode) = getMobileNumberWithoutZero(strToTrim: UserModel.shared.mobileNo)
        lblMobilNumer.text = "(" + CountryCode1 + ")" + " 0" + mobileNumber1
     
        lblScanToPay.text = "Scan To PayQR".localized
        lblAmount.text = "Amount".localized
        lblRemark.text = "Remarks ".localized
        
            
         //   if appDel.currentLanguage == "my" || appDel.currentLanguage == "en"{
                lblDateTime.font = UIFont(name: appFont, size: appTitleButton)
//            }else{
//                lblDateTime.font = UIFont(name: appFont, size: 7)
//            }
            
        
        lblDateTime.text = "Date & Time".localized
        lblBack.text = "Back".localized
        lblShareQR.text = "ShareQR".localized
        lblSaveQR.text = "SaveQR".localized
        imgShareMerchant.image = UIImage(named: "share_gray")
        imgSharePersonal.image = UIImage(named: "share_gray")
        lblBackM.text = "Back".localized
        lblShareQRM.text = "ShareQR".localized
        lblPrintQRM.text = "PrintQR".localized
        lblSaveQRM.text = "SaveQR".localized
        
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.9978122115, green: 0.7681714892, blue: 0.001458939281, alpha: 1)
        
        if UserModel.shared.agentType == .user {
            merchantMenu.isHidden = false
            personalMenu.isHidden = true
            self.lblUserName.text = (UserModel.shared.name.replacingOccurrences(of: ",", with: ". "))
            self.lblUserNamebc.text = (UserModel.shared.name.replacingOccurrences(of: ",", with: ". "))
            let url1 = URL(string: UserModel.shared.proPic.replacingOccurrences(of: " ", with: "%20"))
            self.imgUser.sd_setImage(with: url1, placeholderImage: UIImage(named: "avatar"))
            let url = URL(string: UserModel.shared.proPic.replacingOccurrences(of: " ", with: "%20"))
               self.imgUserbc.sd_setImage(with: url, placeholderImage: UIImage(named: "avatar"))
            lblUserNumber.text =  "(" + CountryCode + ")" + " 0" + mobileNumber
            lblMobNumber.text = ""
        }else {
            merchantMenu.isHidden = false
            personalMenu.isHidden = true
            imgUserbc.isHidden = true
            //Put Text While since and membership come from server
            self.lblUserName.text = UserModel.shared.businessName
            self.lblUserNamebc.text = UserModel.shared.businessName
            let url1 = URL(string: UserModel.shared.logoImages.replacingOccurrences(of: " ", with: "%20"))
            self.imgUser.sd_setImage(with: url1, placeholderImage: UIImage(named: "avatar"))
            let url = URL(string: UserModel.shared.logoImages.replacingOccurrences(of: " ", with: "%20"))
               self.imgUserbc.sd_setImage(with: url, placeholderImage: UIImage(named: "avatar"))
            lblUserNumber.text = UserModel.shared.businessOutlet
            lblMobNumber.text = "(" + CountryCode1 + ")" + " 0" + mobileNumber1
        }
        
        let str = ":  "
        if let amt = qrCodeInfoDic["amount"] {
            if amt == "" {
                lblQRAmount.attributedText = qrCode.concatinateAmountWithMMK(amount: "0")
                lblPrice.text =  ":"
                lblAmount.isHidden = true
                lblQRAmount.isHidden = true
                lblPrice.isHidden = true
            }else {
                lblPrice.attributedText = qrCode.concatinateAmountWithMMK(amount: str + amt)
                lblQRAmount.attributedText = qrCode.concatinateAmountWithMMK(amount: amt)
            }
        }else {
            lblPrice.text =  ":"
            lblQRAmount.text = ""
            lblAmount.isHidden = true
            lblQRAmount.isHidden = true
            lblPrice.isHidden = true
        }
        
        
        if let remark = qrCodeInfoDic["remarks"] {
            if remark == "" {
                lblRemark.isHidden = true
                lblRemarkDescription.isHidden = true
                lblRemarkColon.isHidden = true
                lblRemarkDescription.text = ""
            }else {
                lblRemarkDescription.text = remark
                lblRemarkColon.isHidden = false
            }
        }else{
            lblRemark.isHidden = true
            lblRemarkDescription.isHidden = true
            lblRemarkColon.isHidden = true
            lblRemarkDescription.text =  ""
        }
        
        let amt = self.qrCodeInfoDic["amount"]?.replacingOccurrences(of: ",", with: "") ?? ""
        let Str = self.qrCode.qrStringGenerationForPrint(amount: amt, beforePayment:self.qrCodeInfoDic["beforePayment"] ?? "" , afterPayment: self.qrCodeInfoDic["afterPayment"] ?? "", remarks: self.qrCodeInfoDic["remarks"] ?? "")
       // self.imgQR.image = self.qrCode.generteQRCodeImage(qrStr: Str)
       
        if let img = getQRImageHaldler(stringQR: Str, withSize: 10) {
          self.imgQR.image = img
        }
        self.imgQRbc.image = self.imgQR.image
      self.lblTime.text = ""
        DispatchQueue.main.async {
            let _ = Timer.scheduledTimer(withTimeInterval: 0.2, repeats: true) { timer in
                let (date,time) = self.qrCode.currentDateAndTime()
                self.lblQRDate.text = date
                self.lblTime.text = time
                self.lbbDateTimeDynamic.text = ":  " + date + " " + time
            }
        }
        self.setMarqueLabelInNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.startOtpTimer()
    }
    
    
    
    func getQRImageHaldler(stringQR: String, withSize rate: CGFloat) -> UIImage? {
        if let filter = CIFilter.init(name: "CIQRCodeGenerator") {
            filter.setDefaults()
            if let data : Data = stringQR.data(using: String.Encoding.utf8) {
                filter.setValue(data, forKey: "inputMessage")
                if let resultImage : CIImage = filter.outputImage {
                    let transform = CGAffineTransform(scaleX: 12, y: 12)
                    let translatedImage = resultImage.transformed(by: transform)
                    guard let logo = UIImage(named: "okEmbedInQR"), let logoInCGImage = logo.cgImage else {
                        return nil
                    }
                    guard let qrWithLogoCI = translatedImage.combined(with:  CIImage(cgImage: logoInCGImage)) else {
                        return nil
                    }
                    let context = CIContext.init(options: nil)
                    guard let qrWithLogoCG = context.createCGImage(qrWithLogoCI, from: qrWithLogoCI.extent) else {
                        return nil
                    }
                    let image   = UIImage.init(cgImage: qrWithLogoCG, scale: 1.0, orientation: UIImage.Orientation.up)
                    return image
                }
            }
        }
        return nil
    }
    
    private func startOtpTimer() {
        self.lblQRTimer.isHidden = false
        if (self.timerQR?.internalTimer == nil) {
            self.lblQRTimer.text = "02:00"
            self.totalTime = 119
        }
        else {
            //let remainingTimeInterval = self.timerQR?.internalTimer?.timeInterval
            //self.totalTime = Int(remainingTimeInterval ?? 0)
        }
        self.timerQR?.startTimer(withInterval: 1.0)
//        self.timerQR = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    func timerIntrupCall() {
        self.lblQRTimer.text = self.timeFormatted(self.totalTime)
        if totalTime != 0 {
            if (totalTime < 60) {
              self.lblTimeType.text = "Sec"
                if (totalTime < 10 ) {
                    self.lblQRTimer.tintColor = .red
                    self.lblQRTimer.textColor = .red
                    self.lblTimeType.textColor = .red
                }
            }
            totalTime -= 1
        }
        else {
            if let timer = self.timerQR {
                timer.stopTimer()
                self.showQRExpireAlert()
            }
        }
    }
    
//    @objc func updateTimer() {
//
//    }
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%01d:%02d", minutes, seconds)
    }
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 16)
        lblMarque.textAlignment = .center
        lblMarque.text = "My QR Code".localized
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
    }
    
    //MARK: - Pesonal Menu Buttion Action
    @IBAction func onClickBack(_ sender: Any) {
        timerQR?.stopTimer()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickQRinfo(_ sender: Any) {
        if let timer = self.timerQR {
                timer.stopTimer()
        }
        self.performSegue(withIdentifier: "GeneratedQRCode", sender: self)

    }
    
    @IBAction func onClickShareOR(_ sender: Any) {
        shareQRWithDetail()
    }
    
    @IBAction func onClickSaveQR(_ sender: Any) {
        self.saveToPrint()
    }
    
    //MARK: - Merchant Menu Buttion Action
    
    @IBAction func onClickBack2(_ sender: Any) {
        timerQR?.stopTimer()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickShareOR2(_ sender: Any) {
        shareQRWithDetail()
    }
    
    @IBAction func onClickSaveQR2(_ sender: Any) {
        self.saveToPrint()
    }
    
    @IBAction func onClickPrintQR(_ sender: Any) {
        if let timer = self.timerQR {
            timer.stopTimer()
        }
        performSegue(withIdentifier: "QRFormatDesign", sender: self)
    }
    
    private func saveToPrint() {
        
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.authorized) {
            println_debug(".authorized")
            DispatchQueue.main.async {
                self.saveToGallary()
            }
        }else if (status == PHAuthorizationStatus.denied) {
            println_debug(".denied")
            self.showPhotoAlert()
        }else if (status == PHAuthorizationStatus.notDetermined) {
            println_debug(".notDetermined")
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                if (newStatus == PHAuthorizationStatus.authorized) {
                    println_debug(".authorized 2")
                    DispatchQueue.main.async {
                        self.saveToGallary()
                    }
                }else {
                    self.showPhotoAlert()
                }
            })
        }else if (status == PHAuthorizationStatus.restricted) {
            // Restricted access - normally won't happen.
        }
    }
    
    private func saveToGallary() {
        let newImage = self.viewShare.toImage()
        UIImageWriteToSavedPhotosAlbum(newImage, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    private func showPhotoAlert(){
        alertViewObj.wrapAlert(title:"", body:"Please Allow Camera Access".localized, img: UIImage(named: "alert-icon"))
        alertViewObj.addAction(title: "Setting".localized, style: .target , action: {
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            }
        })
        alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
        }
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    private func showQRExpireAlert(){
        alertViewObj.wrapAlert(title:"", body:"QR Code has been expired".localized, img: UIImage(named: "alert-icon"))
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            self.dismiss(animated: true, completion: nil)
        })
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "QRFormatDesign" {
            let str = qrCode.qrStringGenerationForPrint(amount: qrCodeInfoDic["amount"] ?? "", beforePayment: qrCodeInfoDic["beforePayment"] ?? "" , afterPayment: qrCodeInfoDic["afterPayment"] ?? "" , remarks: qrCodeInfoDic["remarks"] ?? "")
            let controller = segue.destination as! QRFormat
            controller.qrStr = str
        }
    }
    
    
    
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        var strMessage: String?
        if let error = error {
            strMessage = error as? String
        }else {
            if qrCodeInfoDic["amount"] != "" {
                strMessage = "My QR Code with amount Successfully saved in gallery.".localized
            }else {
                strMessage = "My QR Code without amount Successfully saved in gallery.".localized
            }
            
            
        }
        alertViewObj.wrapAlert(title: "", body: strMessage!, img: #imageLiteral(resourceName: "info_success"))
        alertViewObj.addAction(title: "OKQR".localized, style: .target , action: {
        })
        alertViewObj.showAlert(controller: self)
    }
    
    private func shareQRWithDetail() {
        let newImage = self.viewShare.toImage()
        let imageToShare = [newImage]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
    }
    
    private func setValues() {
        
        
    }
    
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
