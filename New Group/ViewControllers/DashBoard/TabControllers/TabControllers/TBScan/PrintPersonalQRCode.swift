//
//  PrintPersonalQRCode.swift
//  OK
//
//  Created by shubh's MacBookPro on 12/4/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class PrintPersonalQRCode: OKBaseController {
    @IBOutlet weak var lblPaymentAcceptHere: UILabel!{
        didSet{
            self.lblPaymentAcceptHere.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var NameLabel : UILabel!{
        didSet{
            self.NameLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var NumberLabel : UILabel!
    @IBOutlet weak var aQRImageView: UIImageView!
    
    @IBOutlet weak var number1Label : UILabel!
    @IBOutlet weak var number2Label : UILabel!
    @IBOutlet weak var number3Label : UILabel!
    
    @IBOutlet weak var bulletLabel1 : UILabel!
    @IBOutlet weak var bulletLabel2 : UILabel!
    @IBOutlet weak var bulletLabel3 : UILabel!
    
    @IBOutlet weak var payToLabel : UILabel!{
        didSet{
            self.payToLabel.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var scanLabel : UILabel!{
        didSet{
            self.scanLabel.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var doneLabel : UILabel!{
        didSet{
            self.doneLabel.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    @IBOutlet weak var servicesAvailableLabel : UILabel!{
        didSet{
            self.servicesAvailableLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var cashInLabel : UILabel!{
        didSet{
            self.cashInLabel.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var cashOutLabel : UILabel!{
        didSet{
            self.cashOutLabel.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var billPaymentsLabel : UILabel!
    @IBOutlet weak var lblScanToPay: UILabel!{
        didSet{
            lblScanToPay.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    @IBOutlet weak var backBottomBtn : UIButton! {
        
        didSet {
            
            self.backBottomBtn.titleLabel?.font = UIFont(name: appFont, size: appTitleButton)
            self.backBottomBtn.setTitle(self.backBottomBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var qrFromatANDSaveQRBtn : UIButton!{
        didSet{
            self.qrFromatANDSaveQRBtn.titleLabel?.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    @IBOutlet weak var printView: UIView!
    @IBOutlet weak var number1CircularView : UIView!
    @IBOutlet weak var number2CircularView : UIView!
    @IBOutlet weak var number3CircularView : UIView!
    
    @IBOutlet weak var imgverify: UIImageView!
    
    var displaySize = ""
    var userSelectedLanguage = ""
    var QRFormatArr = [String]()
    var coverViewForBlackWhite:UIView!
    var qrStr:String = ""
    var qrImageObject :  PTQRGenerator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Print Preview".localized
        if QRFormatArr[3] == "DisplayImage" {
            let url1 = URL(string: UserModel.shared.proPic.replacingOccurrences(of: " ", with: "%20"))
            self.imgUser.sd_setImage(with: url1, placeholderImage: UIImage(named: "avatar"))
        }else {
            self.imgUser.image = nil
        }

//        if appDel.currentLanguage == "uni" {
//            payToLabel.font =  UIFont(name : "Myanmar3", size : 16.0)
//            scanLabel.font =  UIFont(name : "Myanmar3", size : 16.0)
//            doneLabel.font =  UIFont(name : "Myanmar3", size : 16.0)
//            cashInLabel.font =  UIFont(name : "Myanmar3", size : 16.0)
//            cashOutLabel.font =  UIFont(name : "Myanmar3", size : 16.0)
//            billPaymentsLabel.font =  UIFont(name : "Myanmar3", size : 16.0)
//            servicesAvailableLabel.font =  UIFont(name : "Myanmar3", size : 16.0)
//            lblPaymentAcceptHere.font =  UIFont(name : "Myanmar3", size : 16.0)
//            lblScanToPay.font =  UIFont(name : "Myanmar3", size : 16.0)
//        } else {
//            payToLabel.font = UIFont(name : "Zawgyi-One", size : 16.0)
//            scanLabel.font =  UIFont(name : "Zawgyi-One", size : 16.0)
//            doneLabel.font =  UIFont(name : "Zawgyi-One", size : 16.0)
//            cashInLabel.font =  UIFont(name : "Zawgyi-One", size : 16.0)
//            cashOutLabel.font =  UIFont(name : "Zawgyi-One", size : 16.0)
//            billPaymentsLabel.font =  UIFont(name : "Zawgyi-One", size : 16.0)
//            servicesAvailableLabel.font =  UIFont(name : "Zawgyi-One", size : 16.0)
//            lblPaymentAcceptHere.font =  UIFont(name : "Zawgyi-One", size : 16.0)
//            lblScanToPay.font =  UIFont(name : "Zawgyi-One", size : 16.0)
//        }
        payToLabel.text = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Payto")//"Payto".localized
        scanLabel.text = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "ScanQR")//"ScanQR".localized
        doneLabel.text = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Done ")//"Done ".localized
        cashInLabel.text = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Cash-In")//"Cash-In".localized
        cashOutLabel.text = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Cash-Out")//"Cash-Out".localized
        
        if appDelegate.currentLanguage == "my" || appDelegate.currentLanguage == "en"{
            billPaymentsLabel.font = UIFont(name: appFont, size: appTitleButton)
        }else{
            billPaymentsLabel.font = UIFont(name: appFont, size: 7)
        }

        
        billPaymentsLabel.text = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Bill Payments")//"Bill Payments".localized
        self.qrFromatANDSaveQRBtn.setTitle("Print".localized, for: .normal)
        imgUser.layer.cornerRadius = imgUser.frame.width / 2
        imgUser.layer.cornerRadius = imgUser.frame.height / 2
        imgUser.layer.masksToBounds = true
        // imgUser.layer.borderColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        // imgUser.layer.borderWidth = 1.0
        if kycAppControlInfo.first?.kycApprovalStatus == 1 {
            self.imgverify.isHidden = false
        }else {
            self.imgverify.isHidden = true
        }
        
        self.setLayoutViews()
        self.initLoad()
        self.generateQRCodeAction(qrStr)
        if QRFormatArr[1] == "Black & White" {
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if QRFormatArr[1] == "Black & White" {
            coverViewForBlackWhite.isHidden = true
            let image = printView.toImage()
            if let noir = image.noir {
                let blackWhiteImageView  = UIImageView(frame:CGRect(x: 0, y: 0, width: screenWidth, height: printView.bounds.size.height));
                blackWhiteImageView.image = noir
                printView.addSubview(blackWhiteImageView)
            }
        }
        DispatchQueue.main.async {
            self.removeProgressView()
        }
    }
    
    func setLayoutViews() {
         number1Label.layer.cornerRadius = 13
         number2Label.layer.cornerRadius = 13
         number3Label.layer.cornerRadius = 13
     }
    
    func initLoad() {
        
        NameLabel.text = (UserModel.shared.name.replacingOccurrences(of: ",", with: ". "))
        let (mobileNumber,CountryCode) = getMobileNumberWithoutZero(strToTrim: UserModel.shared.mobileNo)
        let mobNummber = "(" + CountryCode + ")" + " 0" + mobileNumber
        NumberLabel.text  = mobNummber
        displaySize = QRFormatArr[0]
        userSelectedLanguage = QRFormatArr[2]
        lblScanToPay.text =  appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Scan To Pay")//"Scan To Pay".localized
        servicesAvailableLabel.text = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Services Available")  //"Services Available".localized
        lblPaymentAcceptHere.text = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "OK$ Payment Accept Here") //"OK$ Payment Accept Here".localized
        payToLabel.text = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Payto") //"Payto".localized
        
        scanLabel.text = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Scan") //"Scan".localized
        
        doneLabel.text = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Done ")//"Done ".localized
        
        cashInLabel.text = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Cash-Out")//"Cash-Out".localized
        
        cashOutLabel.text = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Cash-In")//"Cash-In".localized
        
        billPaymentsLabel.text = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Bill Payments")//"Bill Payments".localized
        
        if QRFormatArr[1] == "Black & White" {
            coverViewForBlackWhite  = UIView(frame:CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
            coverViewForBlackWhite.backgroundColor = UIColor.white
            self.view.addSubview(coverViewForBlackWhite)
        }
    }
    
    func generateQRCodeAction(_ str: String) {
         qrImageObject = PTQRGenerator()
         guard qrImageObject != nil , let qrImage = qrImageObject else {
             return
         }
         
         guard let image =  qrImage.getQRImage(stringQR: str, withSize: 10) else {
             return
         }
         self.aQRImageView.image = image
     }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
       }
  
    @IBAction func OnClickBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func qrFromatANDSaveQRAction(_ sender: UIButton) {
             let image = captureScreen(view: printView)
             var newImage:UIImage!
             if displaySize == "A6" {
                 newImage = self.ResizeImage(image: image, targetSize: CGSize(width: 1240, height: 1748))
             }
             else if displaySize == "A5" {
                 newImage = self.ResizeImage(image: image, targetSize: CGSize(width: 1748, height: 2480))
             }
             else if displaySize == "A4" {
                 newImage = self.ResizeImage(image: image, targetSize: CGSize(width: 2480, height: 3508))
             }
             if QRFormatArr[1] == "Color" {
                 UIImageWriteToSavedPhotosAlbum(newImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
             } else {
                 if let noir = newImage.noir {
                     UIImageWriteToSavedPhotosAlbum(noir, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
                 }
             }
             alertViewObj.wrapAlert(title:"", body:"My QR Code Image already Saved in Gallery. Print it from the Gallery. To share with others click \"Share\" Button.".localized, img: UIImage(named: "save_gray"))
             alertViewObj.addAction(title: "DoneQR".localized, style: .target , action: {
                // self.parent?.dismiss(animated: true, completion: nil)
           // self.dismiss(animated: false, completion: nil)
           // NotificationCenter.default.post(name: Notification.Name(rawValue: "MoveToDashboard"), object: nil)
             })
             alertViewObj.addAction(title: "Share_QR".localized, style: .target , action: {
                 self.shareQRWithDetail()
             })
             DispatchQueue.main.async {
                 alertViewObj.showAlert(controller: self)
             }
     }
     
     func ResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
         let size = image.size
         let widthRatio  = targetSize.width  / image.size.width
         let heightRatio = targetSize.height / image.size.height
         // Figure out what our orientation is, and use that to form the rectangle
         var newSize: CGSize
         if(widthRatio > heightRatio) {
             newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
         } else {
             newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
         }
         // This is the rect that we've calculated out and this is what is actually used below
         let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
         // Actually do the resizing to the rect using the ImageContext stuff
         UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
         image.draw(in: rect)
         let newImage = UIGraphicsGetImageFromCurrentImageContext()
         UIGraphicsEndImageContext()
         return newImage!
     }
     
     @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
         if let error = error {
             alertViewObj.wrapAlert(title: "Error".localized, body: error.localizedDescription, img: nil)
             alertViewObj.addAction(title: "OK".localized, style: .target , action: {
             })
             alertViewObj.showAlert(controller: self)
         } else {
             println_debug("Your altered image has been saved to your photos.")
         }
     }

     func shareQRWithDetail() {
         let image = captureScreen(view: printView)
         if QRFormatArr[1] == "Color" {
             let imageToShare = [image]
             let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
             activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            DispatchQueue.main.async { self.present(activityViewController, animated: true, completion: nil) }
         } else {
             if let noir = image.noir {
                 let imageToShare = [noir]
                 let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
                 activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
                DispatchQueue.main.async { self.present(activityViewController, animated: true, completion: nil) }
             }
         }
     }
     
}
