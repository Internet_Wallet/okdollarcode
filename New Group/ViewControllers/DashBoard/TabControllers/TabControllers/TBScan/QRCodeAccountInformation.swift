//
//  QRCodeAccountInformation.swift
//  OK
//
//  Created by gauri on 11/1/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class QRCodeAccountInformation: OKBaseController {
    
    @IBOutlet weak var btnBack: UIButton! {
        didSet {
            btnBack.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            btnBack.setTitle("Back".localized, for: .normal)
        }
    }
    
    @IBOutlet weak var tableInfo: UITableView!
    var titleArray = [Dictionary<String,String>]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        
        self.setMarqueLabelInNavigation()
        
        let (mobileNumber,CountryCode) = getMobileNumberWithoutZero(strToTrim: UserModel.shared.mobileNo)
        let mobNummber = "(" + CountryCode + ")" + " 0" + mobileNumber
        let operatorName = PayToValidations().getNumberRangeValidation(UserModel.shared.formattedNumber).operator
        println_debug(UserModel.shared.createdDate)
        
        let dateFormatterGet = DateFormatter()
        
        dateFormatterGet.calendar = Calendar(identifier: .gregorian)
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'hh:mm:ss.SSS"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.calendar = Calendar(identifier: .gregorian)
        dateFormatterPrint.dateFormat = "dd/MM/yyyy"

        let lDate = dateFormatterGet.date(from: UserModel.shared.createdDate)
        let joinedDate = dateFormatterPrint.string(from: lDate ?? Date.init())
        
        
        //let joinedDate = dateF6.string(from: parseDate(strDate: UserModel.shared.createdDate))
        var joinedDays = ""
        if self.getYearDifferace(date: UserModel.shared.createdDate ) == "0 days" {
            joinedDays = "Today"
        }else {
            joinedDays = self.getYearDifferace(date: UserModel.shared.createdDate)
        }
        
        if UserModel.shared.agentType == .user {
            self.titleArray = [
                ["title":"","Name": UserModel.shared.proPic.replacingOccurrences(of: " ", with: "%20"),"sub_title": ""],
                ["title":"Account Holder Name","Name": (UserModel.shared.name.replacingOccurrences(of: ",", with: ". ")),"sub_title": ""],
                ["title": "OK$ Account Number","Name": mobNummber,"sub_title": operatorName],
                ["title":"Joined On","Name": joinedDate,"sub_title": joinedDays],
                ["title":"Account level & status","Name": "Level 1","sub_title": "Verified"],
                ["title":"Membership","Name":"Silver","sub_title": ""]]
        }else if UserModel.shared.agentType == .merchant {
            self.titleArray = [
                ["title":"","Name": UserModel.shared.logoImages.replacingOccurrences(of: " ", with: "%20"),"sub_title": ""],
                ["title":"Merchant Name","Name": UserModel.shared.name,"sub_title": ""],
                ["title":"Business Name","Name": UserModel.shared.businessName,"sub_title": ""],
                ["title":"Branch Outlet","Name":UserModel.shared.businessOutlet,"sub_title": ""],
                ["title": "OK$ Account Number","Name": mobNummber,"sub_title": operatorName],
                ["title":"Joined On","Name": joinedDate,"sub_title": joinedDays],
                ["title":"Account level & status","Name": "Level 1","sub_title": "Verified"],
                ["title":"Membership","Name":"Silver","sub_title": ""]]
        }else {
            self.titleArray = [
                ["title":"","Name": UserModel.shared.logoImages.replacingOccurrences(of: " ", with: "%20"),"sub_title": ""],
                ["title":"Agent Name","Name": UserModel.shared.name,"sub_title": ""],
                ["title":"Business Name","Name": UserModel.shared.businessName,"sub_title": ""],
                ["title":"Branch Outlet","Name":UserModel.shared.businessOutlet,"sub_title": ""],
                ["title": "OK$ Account Number","Name": mobNummber,"sub_title": operatorName],
                ["title":"Joined On","Name": joinedDate,"sub_title": joinedDays],
                ["title":"Account level & status","Name": "Level 1","sub_title": "Verified"],
                ["title":"Membership","Name":"Silver","sub_title": ""]]
        }
        
    }
    
    func getDateFormateSringForJoinDate(date: String)-> String {
        return dateF5.string(from: parseDateJaoinDate(strDate: date))
    }
    
    override func getYearDifferace(date : String)-> String {
        let form = DateComponentsFormatter()
        form.maximumUnitCount = 2
        form.unitsStyle = .full
        form.allowedUnits = [.year, .month, .day]
        let date2 = Date()
        let dateFormatterGet = DateFormatter()
        
        dateFormatterGet.calendar = Calendar(identifier: .gregorian)
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'hh:mm:ss.SSS"

        let lDate = dateFormatterGet.date(from: UserModel.shared.createdDate) ?? Date()
        
        let diff = form.string(from: lDate, to: date2)//parseDateJaoinDate7(strDate: date)
        let year = diff?.components(separatedBy: ",")
        return year?[0] ?? ""
    }
    
    private func setMarqueLabelInNavigation() {
        self.navigationItem.titleView = nil
        let lblMarque = MarqueeLabel()
        lblMarque.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        lblMarque.font =  UIFont(name: appFont, size: 16)
        lblMarque.textAlignment = .center
        lblMarque.text = "Information".localized
        lblMarque.textColor = UIColor.white
        self.navigationItem.titleView = lblMarque
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}


extension QRCodeAccountInformation : UITableViewDataSource,UITableViewDelegate  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableInfo.dequeueReusableCell(withIdentifier: "QRInformationUserPicCell") as! QRInformationUserPicCell
            if UserModel.shared.agentType == .user {
                let url = URL(string: UserModel.shared.proPic.replacingOccurrences(of: " ", with: "%20"))
                  cell.imgUser.sd_setImage(with: url, placeholderImage: UIImage(named: "avatar"))
            }else {
                let url = URL(string: UserModel.shared.logoImages.replacingOccurrences(of: " ", with: "%20"))
                  cell.imgUser.sd_setImage(with: url, placeholderImage: UIImage(named: "avatar"))
            }
            cell.selectionStyle = .none
            return cell
        }else {
            let cell = tableInfo.dequeueReusableCell(withIdentifier: "QRInformationCell") as! QRInformationCell
            let dic = self.titleArray[indexPath.row]
            
            if indexPath.row == 1 || indexPath.row == 2{
                cell.lblName.font = UIFont(name: appFont, size: appFontSize)
            }else{
                
            }
            
            cell.lblTitle.text = dic["title"]?.localized ?? ""
            cell.lblName.text = dic["Name"] ?? ""
            cell.lblRight.text = dic["sub_title"] ?? ""
            cell.imgVerify.isHidden = true
            cell.selectionStyle = .none
            if indexPath.row == 6 {
                cell.lblRight.textColor = #colorLiteral(red: 0, green: 0.7294117647, blue: 0, alpha: 1)
                if kycAppControlInfo.first?.kycApprovalStatus == 1 {
                    cell.lblRight.isHidden = false
                    cell.imgVerify.isHidden = false
                }else {
                    cell.lblRight.isHidden = true
                    cell.imgVerify.isHidden = true
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 140
        }else {
            return 75
        }
    }
}


class QRInformationUserPicCell : UITableViewCell {
    @IBOutlet weak var imgUser: UIImageView!{
        didSet {
            imgUser.layer.cornerRadius = imgUser.frame.width / 2
            imgUser.layer.cornerRadius = imgUser.frame.height / 2
            imgUser.layer.masksToBounds = true
            imgUser.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imgUser.layer.borderWidth = 1.0
        }
    }
}

class QRInformationCell : UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!{
        didSet{
            self.lblTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblRight: UILabel!
    @IBOutlet weak var imgVerify: UIImageView!
}
