//
//  QRFormatTableViewCell.swift
//  OK
//
//  Created by Shobhit Singhal on 7/26/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class QRFormatTableViewCell: UITableViewCell {
    @IBOutlet weak var formatTitleLabel : UILabel!
    
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var bottomLineLabel : UILabel!
    @IBOutlet weak var btnAction: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
