//
//  PersonalAccDetailsTableViewCell.swift
//  OK
//
//  Created by Shobhit Singhal on 7/18/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol PersonalAccDetailsTableViewCellDelegate : class {
    func getQRCodeImage(imagQR: UIImage)
}

class PersonalAccDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var viewHearder: UIView!
    @IBOutlet weak var accountTypeLabelValue : UILabel!{
        didSet{
            accountTypeLabelValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var accountNameLabelValue: MarqueeLabel!{
        didSet{
            accountNameLabelValue.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var btnQRCodeInfo: UIButton!
    @IBOutlet weak var lblScanToPay: UILabel!{
        didSet{
         lblScanToPay.font = UIFont(name: appFont, size: appFontSize)
         lblScanToPay.text = "Scan To Pay".localized
        }
    }
    //okdollar
    @IBOutlet weak var imgVerify: UIImageView!
    @IBOutlet weak var aQRImageView: UIImageView!
    var amountValue : String = ""
    var qrImageObject :  PTQRGenerator?
    weak var delegate : PersonalAccDetailsTableViewCellDelegate?
    let validObj = PayToValidations()
    let qrCodeObj = QRCodeGenericClass()

    override func awakeFromNib() {
        super.awakeFromNib()
        viewHearder.layoutIfNeeded()
        lblScanToPay.font = UIFont(name: appFont, size: appFontSize)
        imgUser.layer.cornerRadius = imgUser.frame.width / 2
        imgUser.layer.cornerRadius = imgUser.frame.height / 2
        imgUser.layer.masksToBounds = true
        imgUser.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        imgUser.layer.borderWidth = 1.0
        if kycAppControlInfo.first?.kycApprovalStatus == 1 {
            self.imgVerify.isHidden = false
        }else {
            self.imgVerify.isHidden = true
        }
        self.initLoad()
    }
    
    
    func initLoad() {
        let url = URL(string: UserModel.shared.proPic.replacingOccurrences(of: " ", with: "%20"))
        self.imgUser.sd_setImage(with: url, placeholderImage: UIImage(named: "avatar"))
       self.accountNameLabelValue.text = (UserModel.shared.name.replacingOccurrences(of: ",", with: ". "))
        let (mobileNumber,CountryCode) = getMobileNumberWithoutZero(strToTrim: UserModel.shared.mobileNo)
        accountTypeLabelValue.text = "(" + CountryCode + ")" + " 0" + mobileNumber

       // self.generateQRCodeAction(qrCodeObj.qrStringGenerationForPrint(amount: "0", beforePayment: "", afterPayment: "", remarks: ""))
            self.aQRImageView.image = qrCodeObj.generteQRCodeImage(qrStr: qrCodeObj.qrStringGenerationForPrint(amount: "0", beforePayment: "" , afterPayment: "", remarks: ""))
    }

    override func layoutSubviews() {
        super.layoutSubviews()
    }
   
    
    func generateQRCodeAction(_ str: String) {
        self.aQRImageView.image = qrCodeObj.generteQRCodeImage(qrStr: qrCodeObj.qrStringGenerationForPrint(amount: "0", beforePayment: "" , afterPayment: "", remarks: ""))
        if let del = self.delegate {
            del.getQRCodeImage(imagQR: self.aQRImageView.image!)
        }
    }
    
    func getDate(timeStr: String) -> (date: String, day: String) {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        let time = dateFormatter.date(from: timeStr)//timeStr
        dateFormatter.dateFormat = "dd-MMM-yyyy"//hh:mm:ss”
        let date = dateFormatter.string(from: time!)
        let weekDay = self.dayOfWeek(date: time!)
        return (date, weekDay!)
    }
    
    func dayOfWeek(date: Date) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: date).capitalized
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
    
}
