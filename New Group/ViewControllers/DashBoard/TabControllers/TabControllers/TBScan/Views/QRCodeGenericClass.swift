//
//  QRCodeGenericClass.swift
//  OK
//
//  Created by gauri on 11/9/18.
//  Copyright © 2018 Vinod Kumar. All rights reserved.
//

import UIKit

class QRCodeGenericClass: NSObject {
    var qrImageObject :  PTQRGenerator?
    
   public func qrStringGenerationForPrint(amount: String, beforePayment: String, afterPayment: String, remarks: String) -> String {
        let lat  = geoLocManager.currentLatitude ?? ""
        let long = geoLocManager.currentLongitude ?? ""
        let currentDateTimeStr = getCurrentDateForQRCode()
        var name = ""
        if UserModel.shared.agentType == .user {
            name = UserModel.shared.name
        } else {
            name = UserModel.shared.businessName
        }
        let firstPart  = "00#" + "\(name)" + "-" + "\(UserModel.shared.mobileNo)" + "@" + "\(amount)" + "&"
        let secondPart = "" + "β" + "\(lat)" + "γ" + "\(long)" + "α" + currentDateTimeStr + "`\(afterPayment),\(beforePayment),\(remarks)"
        let finalPart = String.init(format:"%@%@", firstPart,secondPart)
        println_debug("-------> \(finalPart)")
        guard let hashedQRKey = AESCrypt.encrypt(finalPart, password: "m2n1shlko@$p##d") else { return "" }
        return hashedQRKey
    }
    
    public func generteQRCodeImage(qrStr : String) -> UIImage? {
        print("QRCode STR: \(qrStr)")
        qrImageObject = PTQRGenerator()
        guard qrImageObject != nil , let qrImage = qrImageObject else {
            return nil
        }
        
        guard let image =  qrImage.getQRImage(stringQR: qrStr, withSize: 10) else {
            print("QRCode Image not Generated")
            return UIImage(named: "qr-codeside")
            //return nil
        }
        print("QRCode Image Generated")
        return image
    }
    
  private func getCurrentDateForQRCode() -> String {
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd_HHmmss"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    public func currentDateAndTime() -> (String,String){
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd-MMM-yyyy"
        let currentDateString: String = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "HH:mm:ss"
        let someDateTime: String = dateFormatter.string(from: date)
        return (currentDateString,someDateTime)
    }
    
    public func concatinateAmountWithMMK(amount : String) -> NSAttributedString {
        var myMutableString = NSMutableAttributedString()
        var myMutableString1 = NSMutableAttributedString()
        myMutableString1 = NSMutableAttributedString(string: amount, attributes: [NSAttributedString.Key.font:UIFont(name: appFont, size: 15.0) ?? UIFont.systemFont(ofSize: 20)])
        myMutableString = NSMutableAttributedString(string: " MMK", attributes: [NSAttributedString.Key.font:UIFont(name: appFont, size: 10.0) ?? UIFont.systemFont(ofSize: 10)])
        let concate = NSMutableAttributedString(attributedString: myMutableString1)
        concate.append(myMutableString)
        return concate
    }

    
    
    
}
