//
//  AmountRemarkTableViewCell.swift
//  OK
//
//  Created by Shobhit Singhal on 7/18/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol TextChangeForAmountCellDelegate {
    func textChangedAtAmountCell(_ cell: AmountRemarkTableViewCell, text: String?, index: Int)
    func generateButtonTitleChange(title: String)
}


class AmountRemarkTableViewCell: UITableViewCell {
    @IBOutlet weak var textFieldView: UITextField!{
        didSet{
        self.textFieldView.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var mmkRightView: UIView!
    @IBOutlet weak var bottomLineLabel: UIView!
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var lblAmoutMarque: UILabel!
    @IBOutlet var txtAmoutTopConst: NSLayoutConstraint!
    var indexPath: IndexPath! = nil
    var textChangeDelegate: TextChangeForAmountCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        lblAmoutMarque.isHidden = true
        txtAmoutTopConst.constant = -5
        lblAmoutMarque.font = UIFont(name: appFont, size: 12)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configureWithItem(row: Int , item: [String:String], amountCellArr: [String]) {
        textFieldView.placeholder = item["Item"]?.localized
        setColorForText(amountStr: amountCellArr[row])
        imgView.image = UIImage(named: item["Image"]!)
        
        if amountCellArr[0].count > 0 {
            lblAmoutMarque.isHidden = false
            txtAmoutTopConst.constant = 0
        }else {
            lblAmoutMarque.isHidden = true
            txtAmoutTopConst.constant = -5
        }
        
        if row == 1 {
            lblAmoutMarque.text = "Enter Remarks".localized
            mmkRightView.isHidden = true
            textFieldView.keyboardType = .default
            textFieldView.textColor = UIColor.black
        }else {
            lblAmoutMarque.text = "Enter Amount".localized
            mmkRightView.isHidden = false
            textFieldView.keyboardType = .numberPad
        }
        
        if textFieldView.text?.count ?? 0 > 0 {
            crossButton.isHidden = false
        }else{
            crossButton.isHidden = true
        }
        bottomLineLabel.isHidden = false
    }
    
    func setColorForText(amountStr: String) {
        let amountVal = amountStr.components(separatedBy: ",").joined(separator: "")
        if amountVal.count < 7 {
            textFieldView.textColor = UIColor.black
        } else if amountVal.count == 7 {
            textFieldView.textColor = UIColor.green
        } else if amountVal.count > 7 {
            textFieldView.textColor = UIColor.red
        }
        textFieldView.text = amountStr
    }
    
    
    @IBAction func onClickCrossButton(_ sender: Any) {
        crossButton.isHidden = true
        textFieldView.text = ""
        if indexPath.row == 0 {
            lblAmoutMarque.text = ""
            lblAmoutMarque.isHidden = true
            txtAmoutTopConst.constant = -5
            textFieldView.placeholder = "Enter Amount".localized
          //  textChangeDelegate?.generateButtonTitleChange(title: "Generate QR Without Amount".localized)
            textChangeDelegate?.generateButtonTitleChange(title: "Generate QR".localized)
        }else {
            lblAmoutMarque.text = ""
            lblAmoutMarque.isHidden = true
            txtAmoutTopConst.constant = -5
            textFieldView.placeholder = "Enter Remarks".localized
        }
        textChangeDelegate?.textChangedAtAmountCell(self, text: textFieldView.text, index:crossButton.tag)
    }
    
    @IBAction func textFieldEditingChanged(_ sender: UITextField) {
  
        if indexPath.row == 0 {
            if sender.text!.isEmpty {
                lblAmoutMarque.text = ""
                lblAmoutMarque.isHidden = true
                txtAmoutTopConst.constant = -5
                let amountStr = getDigitDisplay(sender.text!)
                textChangeDelegate?.textChangedAtAmountCell(self, text: amountStr, index:indexPath.row)
            }else {
                 lblAmoutMarque.text = "Enter Amount".localized
                 lblAmoutMarque.isHidden = false
                txtAmoutTopConst.constant = 0
                var amountStr = getDigitDisplay(sender.text!)
                if amountStr.count > 14 {
                    amountStr = String((amountStr.dropLast()))
                    textFieldView.text = amountStr
                } else {
                    let amountVal = amountStr.components(separatedBy: ",").joined(separator: "")
                    if amountVal.count < 7 {
                        textFieldView.textColor = UIColor.black
                    } else if amountVal.count == 7 {
                        textFieldView.textColor = UIColor.green
                    } else if amountVal.count > 7 {
                        textFieldView.textColor = UIColor.red
                    }
                    textFieldView.text = amountStr
                }
                textChangeDelegate?.textChangedAtAmountCell(self, text: amountStr, index:indexPath.row)
            }
            if sender.text?.count ?? 0 > 0 {
         //   textChangeDelegate?.generateButtonTitleChange(title: "Generate QR With Amount".localized)
                 textChangeDelegate?.generateButtonTitleChange(title: "Generate QR".localized)
            }else {
           // textChangeDelegate?.generateButtonTitleChange(title: "Generate QR Without Amount".localized)
                 textChangeDelegate?.generateButtonTitleChange(title: "Generate QR".localized)
            }
        }else {
            sender.textColor = UIColor.black
            if sender.text!.isEmpty {
                lblAmoutMarque.text = ""
                lblAmoutMarque.isHidden = true
                txtAmoutTopConst.constant = -7
            }else {
                lblAmoutMarque.text = "Enter Remarks".localized
                lblAmoutMarque.isHidden = false
                txtAmoutTopConst.constant = 0
            }
        }
    }
}

extension AmountRemarkTableViewCell : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let nsString = textField.text as NSString?
        let newString = nsString?.replacingCharacters(in: range, with: string)
        if indexPath.row == 0 {
            if range.location == 0, string == "0" {
                return false
            }
            guard let tfText   = textField.text else { return false }
            let text = (tfText as NSString)
            let txt = text.replacingCharacters(in: range, with: string)
            if txt.count>0{
                crossButton.isHidden = false
            }else{
                crossButton.isHidden = true
            }
            if (newString?.count)! > 12 {
                return false
            }
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: "01234567890, ").inverted).joined(separator: "")) { return false }else {
                return true
            }
        }else {
            if range.location == 0, string == " " {
                return false
            }
            var charAllowed = ""
            //            if appDel.currentLanguage == "my" {
            //            charAllowed = SEARCH_CHAR_SET_My
            //            }else if appDel.currentLanguage == "en"{
            //             charAllowed = SEARCH_CHAR_SET_En
            //            }else {
            //             charAllowed = SEARCH_CHAR_SET_Uni
            //            }
            charAllowed = SEARCH_CHAR_SET
            if !(string == string.components(separatedBy: NSCharacterSet(charactersIn: charAllowed).inverted).joined(separator: "")) { return false }
            if (string.contains("  ")){
                return false
            }
            guard let tfText   = textField.text else { return false }
            let text = (tfText as NSString)
            let txt = text.replacingCharacters(in: range, with: string)
            if txt.count > 0 {
                crossButton.isHidden = false
            }else{
                crossButton.isHidden = true
            }
            print(string)
            print(range.location)
            if textField.text?.count ?? 0 > 79  {
                if range.location == 79 {
                    return true
                }else {
                    return false
                }
            }
           
            textChangeDelegate?.textChangedAtAmountCell(self, text: newString, index:indexPath.row)
            
            return restrictMultipleSpaces(str: string, textField: textField)
        }
    }
  private func getDigitDisplay(_ digit: String) -> String {
        var FormateStr: String
        let mystring = digit.replacingOccurrences(of: ",", with: "")
        let number = NSDecimalNumber(string: mystring)
        
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        FormateStr = formatter.string(from: number)!
        if FormateStr == "NaN"{
            FormateStr = ""
        }
        
        return FormateStr
    }
}


class QRTipsCell: UITableViewCell {
    @IBOutlet weak var lblSubTitle: UILabel!{
        didSet{
            lblSubTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
}
