//
//  TipsViewQR.swift
//  OK
//
//  Created by shubh's MacBookPro on 11/27/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class TipsViewQR: UIView {
    @IBOutlet weak var lblTips: UILabel!
    @IBOutlet weak var lblTipsDescription: UILabel!
    @IBOutlet weak var btnCloseTops: UIButton!

    override func awakeFromNib() {
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
    }
    
    override func layoutIfNeeded() {
        self.layoutIfNeeded()
    }
    
}
