//
//  BusinessAccDetailsTableViewCell.swift
//  OK
//
//  Created by Shobhit Singhal on 7/18/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
protocol BusinessAccDetailsTableViewCellDelegate : class {
      func getQRCodeImage(imagQR: UIImage)
}

class BusinessAccDetailsTableViewCell: UITableViewCell{
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var addressLabel : MarqueeLabel!
    @IBOutlet weak var aQRImageView: UIImageView!
    @IBOutlet weak var btnOnfo: UIButton!
    @IBOutlet weak var businessNameLabel: MarqueeLabel!
    @IBOutlet weak var lblScanToPay: UILabel! {
        didSet {
          lblScanToPay.font = UIFont(name: appFont, size: appFontSize)
          lblScanToPay.text = "Scan To Pay".localized
        }
    }
    @IBOutlet weak var lblMobilNumber: UILabel!
    @IBOutlet weak var imgVerifyUser: UIImageView!
    weak var delegate : BusinessAccDetailsTableViewCellDelegate?
    let qrCodeObj = QRCodeGenericClass()

    override func awakeFromNib() {
        super.awakeFromNib()
        imgUser.layer.cornerRadius = imgUser.frame.width / 2
        imgUser.layer.cornerRadius = imgUser.frame.height / 2
        imgUser.layer.masksToBounds = true
       // imgUser.layer.borderColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        //imgUser.layer.borderWidth = 1.0
        
        if kycAppControlInfo.first?.kycApprovalStatus == 1 {
              self.imgVerifyUser.isHidden = false
            }else {
               self.imgVerifyUser.isHidden = true
            }
        self.initLoad()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func initLoad() {
        // Un hide when user is verified
        let url = URL(string: UserModel.shared.logoImages.replacingOccurrences(of: " ", with: "%20"))
        self.imgUser.sd_setImage(with: url, placeholderImage: UIImage(named: "avatar"))
        
        self.businessNameLabel.text = UserModel.shared.businessName
        self.addressLabel.text = UserModel.shared.businessOutlet
        let (mobileNumber,CountryCode) = getMobileNumberWithoutZero(strToTrim: UserModel.shared.mobileNo)
        self.lblMobilNumber.text = "(" + CountryCode + ")" + " 0" + mobileNumber
//        aQRImageView.image = qrCodeObj.generteQRCodeImage(qrStr: qrCodeObj.qrStringGenerationForPrint(amount: "0", beforePayment: "" , afterPayment: "", remarks: ""))
    }
    
    func generateQRCodeAction(_ str: String) {
        self.aQRImageView.image = qrCodeObj.generteQRCodeImage(qrStr: qrCodeObj.qrStringGenerationForPrint(amount: "0", beforePayment: "" , afterPayment: "", remarks: ""))
        guard self.aQRImageView.image != nil else {
            return
        }
        if let del = self.delegate {
            del.getQRCodeImage(imagQR: self.aQRImageView.image!)
        }
    }

    func getDate(timeStr: String) -> (date: String, day: String) {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        let time = dateFormatter.date(from: timeStr)//timeStr
        dateFormatter.dateFormat = "dd-MMM-yyyy"//hh:mm:ss”
        let date = dateFormatter.string(from: time!)
        let weekDay = self.dayOfWeek(date: time!)
        return (date, weekDay!)
    }
    
    func dayOfWeek(date: Date) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: date).capitalized
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
