//
//  InformationTableViewCell.swift
//  OK
//
//  Created by Shobhit Singhal on 7/18/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol TextChangeForInformationCellDelegate {

}

class InformationTableViewCell: UITableViewCell {
    @IBOutlet weak var bgInfo: UIView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var btnInfomation: UIButton!{
        didSet{
            btnInfomation.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var lblInformationTitle: UILabel!{
        didSet{
            lblInformationTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    weak var parentVC : TBMyQRCode?
    var indexPath: IndexPath! = nil
    var textChangeDelegate: TextChangeForInformationCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configureWithItem(row: Int , item: [String:String], informationCellArr: [String]) {
    }
}


