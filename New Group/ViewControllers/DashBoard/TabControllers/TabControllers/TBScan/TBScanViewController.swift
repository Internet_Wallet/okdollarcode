//
//  TBScanViewController.swift
//  OK
//
//  Created by Ashish on 12/25/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class TBScanViewController: OKBaseController, PaytoScrollerDelegate, TBScanQRDelegate {
    
    fileprivate var myScanMenu : PaytoScroller?
    fileprivate var controllerArray : [UIViewController] = []

    var navigation : UINavigationController?
    
    var scanDelegate : TBScanQRDelegate?
    var fromSideMenu : Bool = false // flag to check particular event is from side menu or tab bar controller

    override func viewDidLoad() {
        super.viewDidLoad()

        if fromSideMenu {
            self.setPOPButton()
        }
        
        MyNumberTopup.theme = UIColor.init(red: 28.0/255.0, green: 32.0/255.0, blue: 135.0/255.0, alpha: 1)
        
        IQKeyboardManager.sharedManager().enable            = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
        initialLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(TBScanViewController.languageChangeObservation), name: NSNotification.Name(rawValue: languageKeyDashboard), object: nil)
    }
    
    @objc func initialLoad() {
        let paytoScan = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TBScanToPayViewController.self)) as? TBScanToPayViewController
        println_debug("Scan QR".localized)
        paytoScan?.title = "Scan QR".localized
        paytoScan?.navigation = self.navigation
        paytoScan?.scanDelegate = self
        
        let myQRCode = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TBMyQRCode.self)) as? TBMyQRCode
        myQRCode?.navigation = self.navigation
        myQRCode?.view.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: 548.00)
        myQRCode?.title = "My QR Code".localized
        myQRCode?.itemNavigation = self.navigationItem
        controllerArray = [paytoScan!, myQRCode!]
        
        let parameters = [PaytoScrollerOptionMenuItemSeparatorWidth: 0,
                          PaytoScrollerOptionSelectionIndicatorHeight : 1.7,
                          PaytoScrollerOptionMenuItemSeparatorPercentageHeight: 0.1,
                          PaytoScrollerOptionScrollMenuBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectedMenuItemLabelColor: UIColor.init(red: 28.0/255.0, green: 32.0/255.0, blue: 135.0/255.0, alpha: 1),
                          PaytoScrollerOptionViewBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectionIndicatorColor: UIColor.black,
                          PaytoScrollerOptionUnselectedMenuItemLabelColor:UIColor.darkText,
                          PaytoScrollerOptionMenuHeight: 45,
                          PaytoScrollerOptionMenuItemFont: "Zawgyi-One",
                          PaytoScrollerOptionMenuItemWidth: (screenWidth / CGFloat(controllerArray.count)) - 10.0] as [String : Any]
        
        myScanMenu = PaytoScroller(viewControllers: controllerArray, frame: self.view.bounds, options: parameters)
        myScanMenu?.delegate = self
        myScanMenu?.scrollingEnable(false)
        self.view.addSubview(myScanMenu!.view)
        myScanMenu?.scrollingEnable(true)
        if let menu = myScanMenu, fromSideMenu {
            menu.move(toPage: 1)
        }
    }
    
    @objc func languageChangeObservation() {
        myScanMenu?.view.removeFromSuperview()
        myScanMenu?.removeFromParent()
        initialLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
         super.viewDidAppear(true)
                        let buttonIcon      = UIImage(named: "log_Report")
                        let leftBarButton   = UIBarButtonItem(title: "", style: .done , target: self, action: #selector(showDetail))
                        leftBarButton.image = buttonIcon
                         self.navigationController?.navigationBar.tintColor = UIColor(red: 47.0/255.0, green: 52.0/255.0, blue: 141.0/255.0, alpha: 1.0)
                        navigationController?.navigationBar.topItem?.rightBarButtonItem = leftBarButton
    }
    
   fileprivate func setPOPButton() {
    self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 16.0) ?? UIFont.systemFont(ofSize: 16.0), NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = "QR Code".localized//"Verify Payment".localized
        self.navigationController?.navigationBar.tintColor = .white
    
    }
    
//    @objc fileprivate func popTheViewControllerPT(_ sender:UIBarButtonItem!) {
//
//        if let navigation = self.navigationController {
//            navigation.popViewController(animated: true)
//        }
//
//    }
    
    func refreshTOScanQR() {
        guard let menu = myScanMenu else { return }
        menu.move(toPage: 0)
    }

    func willMove(toPage controller: UIViewController!, index: Int) {
        //PaymentVerificationDBManager().getAllPaymentVerificationDataFromDB().count > 0,
        if  index == 0 {
            let buttonIcon      = UIImage(named: "log_Report")
            let leftBarButton   = UIBarButtonItem(title: "", style: .done , target: self, action: #selector(showDetail))
            leftBarButton.image = buttonIcon
            self.navigationController?.navigationBar.tintColor = UIColor(red: 47.0/255.0, green: 52.0/255.0, blue: 141.0/255.0, alpha: 1.0)
            navigationController?.navigationBar.topItem?.rightBarButtonItem = leftBarButton
        } else if index == 1 {
            //self.navigationItem.rightBarButtonItem = nil
            navigationController?.navigationBar.topItem?.rightBarButtonItem = nil
        }
    }
    
    @objc func showDetail() {
        guard let sanResultDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "QRScanResultDetailViewController") as? QRScanResultDetailViewController else { return }
      self.navigationController?.pushViewController(sanResultDetailViewController, animated: true)
        //self.present(sanResultDetailViewController, animated: true, completion: nil)
    }
    
    func didMove(toPage controller: UIViewController!, index: Int) {
        if index == 0 {
            self.title = "QR Code".localized
        } else {
            self.title = "QR Code".localized
        }
    }

    func moveToPayto(withScanObject object: PTScanObject, fromTransfer: Bool?) {
        
        if let delegate = self.scanDelegate {
            delegate.moveToPayto(withScanObject: object, fromTransfer: false)
        } else {
            self.navigateToPayto(phone: object.agentNo, amount: object.amount, name: object.name, remark: object.remarks)
        }
        
    }
    
    func moveToPayTo(fromTransfer: Bool?) {
        if let delegate = self.scanDelegate {
            delegate.moveToPayTo(fromTransfer: false)
        }
    }
    
}
