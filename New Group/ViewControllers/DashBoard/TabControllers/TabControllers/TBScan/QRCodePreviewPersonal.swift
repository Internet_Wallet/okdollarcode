//
//  QRCodePreviewPersonal.swift
//  OK
//
//  Created by Shobhit Singhal on 7/25/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class QRCodePreviewPersonal: OKBaseController {
    @IBOutlet weak var userNameLabel : UILabel!{
        didSet{
            userNameLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var accountTypeLabel : UILabel!{
        didSet{
            accountTypeLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var mobileNumberLabel : UILabel!
    @IBOutlet weak var number1Label : UILabel!
    @IBOutlet weak var number2Label : UILabel!
    @IBOutlet weak var number3Label : UILabel!
    @IBOutlet weak var bulletLabel1 : UILabel!
    @IBOutlet weak var bulletLabel2 : UILabel!
    @IBOutlet weak var bulletLabel3 : UILabel!
    @IBOutlet weak var number1CircularView : UIView!
    @IBOutlet weak var number2CircularView : UIView!
    @IBOutlet weak var number3CircularView : UIView!
    @IBOutlet weak var payToLabel : UILabel!{
    didSet{
        payToLabel.font = UIFont(name: appFont, size: appTitleButton)
    }
}

    @IBOutlet weak var scanLabel : UILabel!{
        didSet{
            scanLabel.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var doneLabel : UILabel!{
        didSet{
            doneLabel.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var cashInLabel : UILabel!{
        didSet{
            cashOutLabel.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var cashOutLabel : UILabel!{
        didSet{
            cashOutLabel.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var billPaymentsLabel : MarqueeLabel!{
        didSet{
                billPaymentsLabel.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var servicesAvailableLabel : UILabel!{
        didSet{
            servicesAvailableLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var mainView : UIView!
    @IBOutlet weak var lblMerchantStatus: UILabel!{
        didSet{
          lblMerchantStatus.text = ""
        }
    }
    @IBOutlet weak var lblMerchantType: UILabel! {
        didSet{
         lblMerchantType.text = ""
        }
    }
    
    @IBOutlet weak var imgUser: UIImageView!
    
    @IBOutlet weak var backBottomBtn : UIButton! {
        didSet {
            self.backBottomBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.backBottomBtn.setTitle(self.backBottomBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var imgVerify: UIImageView!
    @IBOutlet weak var qrFromatANDSaveQRBtn : UIButton!{
        didSet {
            self.qrFromatANDSaveQRBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var aQRImageView: UIImageView!

    var isComingFromQRFormat:Bool = false
    var QRFormatArr = [String]()
    var displaySize:String = ""
    var qrStr:String = ""
    var qrImageObject :  PTQRGenerator?
    var userSelectedLanguage:String = ""
    var coverViewForBlackWhite:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        imgVerify.isHidden = true
        imgUser.layer.cornerRadius = imgUser.frame.width / 2
        imgUser.layer.cornerRadius = imgUser.frame.height / 2
        imgUser.layer.masksToBounds = true
        imgUser.layer.borderColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        imgUser.layer.borderWidth = 1.0
        if QRFormatArr[3] == "DisplayImage" {
            let url1 = URL(string: UserModel.shared.proPic.replacingOccurrences(of: " ", with: "%20"))
             self.imgUser.sd_setImage(with: url1, placeholderImage: UIImage(named: "avatar"))
        }else {
            self.imgUser.image = nil
        }
 
        if isComingFromQRFormat {
            self.title = "Print Preview".localized
            qrFromatANDSaveQRBtn.setTitle("Save_".localized, for: .normal)
        }else {
            self.title = "My QR Code".localized
            qrFromatANDSaveQRBtn.setTitle("Next".localized, for: .normal)
        }
        self.setLayoutViews()
        self.initLoad()
        self.generateQRCodeAction(qrStr)
        if QRFormatArr[1] == "Black & White" {
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if isComingFromQRFormat {
            if QRFormatArr[1] == "Black & White" {
                coverViewForBlackWhite.isHidden = true
                let image = captureScreen(view: mainView)
                if let noir = image.noir {
                    let blackWhiteImageView  = UIImageView(frame:CGRect(x: 0, y: 0, width: screenWidth, height: mainView.bounds.size.height));
                    blackWhiteImageView.image = noir
                    mainView.addSubview(blackWhiteImageView)
                }
            }
        }
        DispatchQueue.main.async {
               self.removeProgressView()
           }
    }
    
    func setLayoutViews() {
        number1Label.layer.cornerRadius = 13
        number2Label.layer.cornerRadius = 13
        number3Label.layer.cornerRadius = 13
        number1CircularView.layer.cornerRadius = 25
        number2CircularView.layer.cornerRadius = 25
        number3CircularView.layer.cornerRadius = 25
        bulletLabel1.layer.cornerRadius = 4
        bulletLabel2.layer.cornerRadius = 4
        bulletLabel3.layer.cornerRadius = 4
    }
    
    func initLoad() {
        self.userNameLabel.text = (UserModel.shared.name.replacingOccurrences(of: ",", with: ". "))
        let (mobileNumber,CountryCode) = getMobileNumberWithoutZero(strToTrim: UserModel.shared.mobileNo)
        mobileNumberLabel.text = "(" + CountryCode + ")" + " " + mobileNumber

        if isComingFromQRFormat {
            displaySize = QRFormatArr[0]
            userSelectedLanguage = QRFormatArr[2]
            accountTypeLabel.text = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Personal Account")
            servicesAvailableLabel.text = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Services Available")
            payToLabel.text =  appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Payto")
            scanLabel.text =  appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Scan")
            doneLabel.text =  appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Done")
            cashInLabel.text =  appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Cash-In")
            cashOutLabel.text =  appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Cash-Out")
            billPaymentsLabel.text =  appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Bill Payments")
            if QRFormatArr[1] == "Black & White" {
                coverViewForBlackWhite  = UIView(frame:CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
                coverViewForBlackWhite.backgroundColor = UIColor.white
                self.view.addSubview(coverViewForBlackWhite)
            }
        }
    }
    
    func generateQRCodeAction(_ str: String) {
        qrImageObject = PTQRGenerator()
        guard qrImageObject != nil , let qrImage = qrImageObject else {
            return
        }
        
        guard let image =  qrImage.getQRImage(stringQR: str, withSize: 10) else {
            return
        }
        self.aQRImageView.image = image
    }
    
    @IBAction func dismissControllerAction(_ sender: UIBarButtonItem) {
        if isComingFromQRFormat {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        if isComingFromQRFormat {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
   
    @IBAction func qrFromatANDSaveQRAction(_ sender: UIButton) {
        if isComingFromQRFormat {
            let image = captureScreen(view: mainView)
            var newImage:UIImage!
            if displaySize == "A6" {
                newImage = self.ResizeImage(image: image, targetSize: CGSize(width: 1240, height: 1748))
            }
            else if displaySize == "A5" {
                newImage = self.ResizeImage(image: image, targetSize: CGSize(width: 1748, height: 2480))
            }
            else if displaySize == "A4" {
                newImage = self.ResizeImage(image: image, targetSize: CGSize(width: 2480, height: 3508))
            }
            if QRFormatArr[1] == "Color" {
                UIImageWriteToSavedPhotosAlbum(newImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
            } else {
                if let noir = newImage.noir {
                    UIImageWriteToSavedPhotosAlbum(noir, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
                }
            }
            alertViewObj.wrapAlert(title:"", body:"My QR Code Image already Saved in Gallery. Print it from the Gallery. To share with others click \"Share\" Button.".localized, img: #imageLiteral(resourceName: "galleryQR"))
            alertViewObj.addAction(title: "DoneQR".localized, style: .target , action: {
                self.parent?.dismiss(animated: true, completion: nil)
            })
            alertViewObj.addAction(title: "Share_QR".localized, style: .target , action: {
                self.shareQRWithDetail()
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        } else {
            self.performSegue(withIdentifier: "QRPreviewPersonalToQRFormat", sender: self)
        }
    }
    
    func ResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        }
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            alertViewObj.wrapAlert(title: "Error".localized, body: error.localizedDescription, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        } else {
            println_debug("Your altered image has been saved to your photos.")
        }
    }

    func shareQRWithDetail() {
        let image = captureScreen(view: mainView)
        if QRFormatArr[1] == "Color" {
            let imageToShare = [image]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
        } else {
            if let noir = image.noir {
                let imageToShare = [noir]
                let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
                DispatchQueue.main.async { self.present(activityViewController, animated: true, completion: nil) }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "QRPreviewPersonalToQRFormat" {
            let controller = segue.destination as! QRFormat
            controller.qrStr = self.qrStr
        }
    }
}
