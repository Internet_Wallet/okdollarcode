//
//  TBMyQRCode.swift
//  OK
//
//  Created by Shobhit Singhal on 7/18/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreData
import QuartzCore
import AVFoundation
import Photos

struct Category {
    let name : String
    var items : [[String:String]]
}

class TBMyQRCode: OKBaseController,PersonalAccDetailsTableViewCellDelegate,BusinessAccDetailsTableViewCellDelegate, IndicatorInfoProvider,UIGestureRecognizerDelegate {
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title : "My QR Code".localized)
    }
    
    
    @IBOutlet weak var qrCodeTableView : UITableView!
    @IBOutlet weak var moreOptionsView : UIView!
    @IBOutlet weak var generateQRCodeBtn : UIButton!{
        didSet{
            self.generateQRCodeBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var moreOptionsBtn : UIButton!{
        didSet{
            self.moreOptionsBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var bottomGenerateQRBtnConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomOptionsViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightGenerateQRBtnConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var scanQRView : UIView!
    @IBOutlet weak var scanQRViewBtn : UIButton!{
        didSet{
            self.scanQRViewBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnScanQR : UIButton!{
        didSet{
            self.btnScanQR.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    // View To Share & Save
    
    var scanDelegate: TBScanQRDelegate?
    
    @IBOutlet weak var viewShare: UIView!
    @IBOutlet weak var QRCodeView: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var viewHeader: UIView!
    
    @IBOutlet weak var lblUserName: UILabel!{
        didSet{
            self.lblUserName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblMobilNumer: UILabel!{
        didSet{
            self.lblMobilNumer.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblTelcode: UILabel!{
        didSet{
            self.lblTelcode.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblScanToPay: UILabel! {
        didSet {
            self.lblScanToPay.font = UIFont(name: appFont, size: appFontSize)
            lblScanToPay.text = "Scan To Pay".localized
        }
    }
    @IBOutlet weak var lblQRAmount: UILabel!{
        didSet{
            self.lblQRAmount.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgQR: UIImageView!
    @IBOutlet weak var lblQRDate: UILabel!{
        didSet{
            self.lblQRDate.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblTime: UILabel!{
        didSet{
            self.lblTime.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblMembership: UILabel!{
        didSet{
            self.lblMembership.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblMemberSince: UILabel!{
        didSet{
            self.lblMemberSince.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    var fromTabBar: Bool? = false
    
    var itemNavigation : UINavigationItem?
    var navigation : UINavigationController?
    var sections = [Category]()
    let itemsA = [["Item": "","Image" : ""]]
    let itemsB = [["Item": "Enter Amount","Image" : "amount_new"],["Item": "Enter Remarks","Image" : "remarks_Solar"]]
    let itemsC = [["Item": "Information to show Before Payment","Image" : "n_before_payment"]]
    let itemsD = [["Item": "","Image" : ""]]
    var qrCodeBtnFrame:CGRect!
    var enteredAmountStr:String = ""
    var qrStr: String = ""
    var amountCellArr:[String] = ["", ""]
    var checkAmountCellDisable:Bool = true
    var checkInformationCellDisable:Bool = true
    var informationCellArr:[String] = ["", ""]
    var amountRemarkHeaderView:UIView!
    var informationHeaderView:UIView!
    
    let qrCodeObj = QRCodeGenericClass()
    
    var heightKeyboard = Int()
  
    
    @IBOutlet weak var viewSuggestions: UIView!
    @IBOutlet weak var imgSuggestions: UIImageView!
  
    @IBOutlet weak var btnSkip: UIButton!{
        didSet{
            self.btnSkip.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var btnSuggestionNext: UIButton!{
        didSet{
            self.btnSuggestionNext.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
    }
    @IBOutlet weak var imgS_First: UIImageView!
    @IBOutlet weak var imgS_Second: UIImageView!
    @IBOutlet weak var imgS_thired: UIImageView!
    @IBOutlet weak var lblSugesstionTitle: UILabel!{
        didSet{
            self.lblSugesstionTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblSugesstionSubTitle: UILabel!{
        didSet{
            self.lblSugesstionSubTitle.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var imgTitleImage: UIImageView!
    @IBOutlet weak var lblNewFeatures: UILabel!{
        didSet{
            self.lblNewFeatures.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    var youcanString = ""
    var imageIndex = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        moreOptionsView.isHidden = true
        viewHeader.layoutIfNeeded()
        imgUser.layer.cornerRadius = imgUser.frame.width / 2
        imgUser.layer.cornerRadius = imgUser.frame.height / 2
        imgUser.layer.masksToBounds = true
        
        btnSkip.setTitle("Skip".localized, for: .normal)
        if appDelegate.currentLanguage == "en" {
            youcanString = "Now you can \n"
           btnSkip.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }else {
           btnSkip.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        }
       
        btnSkip.layer.cornerRadius = btnSkip.frame.height / 2
        btnSkip.layer.masksToBounds = true
        
        btnSuggestionNext.layer.cornerRadius = 10.0
        btnSuggestionNext.layer.masksToBounds = true
        
        if UserModel.shared.agentType == .user {
           lblMembership.text = ""
           lblMemberSince.text = ""
        }else {
            // Put value when come from server
            lblMembership.text = ""
            lblMemberSince.text = ""
           imgUser.isHidden = true
        }
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white ]
        let scanImage = UIImage(named: "scan_qrTabNew")?.withRenderingMode(.alwaysTemplate)
        self.scanQRViewBtn.setImage(scanImage, for: .normal)
        self.scanQRViewBtn.imageView?.tintColor = .white
        
        generateQRCodeBtn.isHidden = false // true
        heightGenerateQRBtnConstraint.constant = 50 //0
        self.navigationController?.navigationBar.tintColor = .white

        lblSugesstionTitle.text = "PRINT".localized
        imgTitleImage.image = UIImage(named: "print_QR")
        lblSugesstionSubTitle.text = youcanString + "Print your QR code for stand display".localized
        btnSuggestionNext.setTitle("Next_".localized, for: .normal)
        imgSuggestions.image = UIImage(named: "qrCode_Print")
        imgS_First.image = UIImage(named: "radio_select_qr")
        imgS_Second.image = UIImage(named: "radio")
        imgS_thired.image = UIImage(named: "radio")
        
        
        if UserDefaults.standard.bool(forKey: "is_QR_Sugesstion_Read") {
            self.viewSuggestions.isHidden = true
        }else {
           self.viewSuggestions.isHidden = false
        }
         //self.viewSuggestions.isHidden = false
      
        NotificationCenter.default.addObserver(self, selector: #selector(dismissToDeshboard(_:)), name: Notification.Name("MoveToDashboard"), object: nil)
        
        lblNewFeatures.text = "New \n Feature".localized
        lblNewFeatures.startBlink()
        lblNewFeatures.isHidden = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToBackGround), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(enterForgound), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        generateQRCodeBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        btnScanQR.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        btnScanQR.setTitle("Scan QR".localized, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewShare.isHidden = true
        self.initLoad()
        self.setLayoutViews()
        self.reloadAndScrollToTop()
        self.addSwipe()
    }
    
    @objc func dismissToDeshboard(_ notification: Notification) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func moveToBackGround() {
        lblNewFeatures.stopBlink()
      }
      
      @objc func enterForgound() {
        lblNewFeatures.startBlink()
      }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("MoveToDashboard"), object: nil)
    }
    
    
    func addSwipe() {
        self.imgSuggestions.isUserInteractionEnabled = true
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(_:)))
        swipeRight.direction = .right
        swipeRight.numberOfTouchesRequired = 1
        self.imgSuggestions.addGestureRecognizer(swipeRight)

        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(_:)))
        swipeUp.direction = .left
        swipeUp.numberOfTouchesRequired = 1
        self.imgSuggestions.addGestureRecognizer(swipeUp)
    }
 @objc func handleSwipe(_ sender:UISwipeGestureRecognizer) {
        print(sender.direction)
        if sender.direction == .right {
            imageIndex = imageIndex - 1
            if imageIndex <= 0 {
                imageIndex = 1
                return
            }
        }else {
            imageIndex = imageIndex + 1
            if imageIndex >= 4 {
                imageIndex = 3
                return
            }
        }
        if imageIndex == 1 {
            btnSkip.isHidden = false
            imgSuggestions.image = UIImage(named: "qrCode_Print")
            btnSuggestionNext.setTitle("Next_".localized, for: .normal)
            lblSugesstionTitle.text = "PRINT".localized
            imgTitleImage.image = UIImage(named: "print_QR")
            lblSugesstionSubTitle.text = youcanString + "Print your QR code for stand display".localized
            imgS_First.image = UIImage(named: "radio_select_qr")
            imgS_Second.image = UIImage(named: "radio")
            imgS_thired.image = UIImage(named: "radio")
            lblNewFeatures.startBlink()
            lblNewFeatures.isHidden = false
        }else if imageIndex == 2 {
            btnSkip.isHidden = false
            imgSuggestions.image = UIImage(named: "qrCodeSave")
            btnSuggestionNext.setTitle("Next_".localized, for: .normal)
            lblSugesstionTitle.text = "Save_".localized
            imgTitleImage.image = UIImage(named: "gallery_QR")
            lblSugesstionSubTitle.text = youcanString + "Save your QR code in gallery".localized
            imgS_First.image = UIImage(named: "radio")
            imgS_Second.image = UIImage(named: "radio_select_qr")
            imgS_thired.image = UIImage(named: "radio")
            lblNewFeatures.stopBlink()
            lblNewFeatures.isHidden = true
        }else if imageIndex == 3 {
            btnSkip.isHidden = true //
            imgSuggestions.image = UIImage(named: "qrCode_Share")
            btnSuggestionNext.setTitle("Continue_".localized, for: .normal)
            lblSugesstionTitle.text = "Share_".localized
            imgTitleImage.image = UIImage(named: "reportShareWhite")
            lblSugesstionSubTitle.text = youcanString + "Share your QR code to others".localized
            imgS_First.image = UIImage(named: "radio")
            imgS_Second.image = UIImage(named: "radio")
            imgS_thired.image = UIImage(named: "radio_select_qr")
            lblNewFeatures.stopBlink()
            lblNewFeatures.isHidden = true
        }else {
            UserDefaults.standard.set(true, forKey: "is_QR_Sugesstion_Read")
            self.viewSuggestions.isHidden = true
        }
        
        
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        qrCodeBtnFrame = generateQRCodeBtn.frame
    }
    
    @IBAction func showScanQRVC(_ sender: UIButton) {
        if let fromMainTab = fromTabBar, fromMainTab == true {
            self.navigationController?.popViewController(animated: true)
            return
        }
        let scanQRCode = self.storyboard?.instantiateViewController(withIdentifier: "TBScanToPayViewController") as? TBScanToPayViewController
        scanQRCode?.navigation   = self.navigationController
        scanQRCode?.scanDelegate = self
        scanQRCode?.isFromTabBar = true
        scanQRCode?.isFromSideMenu = true
        self.navigationController?.pushViewController(scanQRCode!, animated: true)
    }
    
    func initLoad() {
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "QRCodeDetails")
        let result = try? managedContext.fetch(fetch)
        
        if (result?.count)! > 0 {
            let qRCodeDetails = result?[0] as? QRCodeDetails
            if let amountCellData = qRCodeDetails?.amountCellData {
                self.amountCellArr = amountCellData
                self.enteredAmountStr = self.amountCellArr[0]
            }
            if let informationCellData = qRCodeDetails?.informationCellData {
                self.informationCellArr = informationCellData
            }
        }
        
        if enteredAmountStr != "" {
             generateQRCodeBtn.setTitle("Generate QR".localized, for: .normal)
          //generateQRCodeBtn.setTitle("Generate QR With Amount".localized, for: .normal)
        }else{
          //generateQRCodeBtn.setTitle("Generate QR Without Amount".localized, for: .normal)
            generateQRCodeBtn.setTitle("Generate QR".localized, for: .normal)
        }
        
        if UserModel.shared.agentType == .user {
            sections = [Category(name:"", items:itemsA), Category(name:"Amount & Remark", items:itemsB), Category(name:"Information and Advertisement", items:itemsC)]
        }else {
            sections = [Category(name:"", items:itemsA), Category(name:"Amount & Remark", items:itemsB), Category(name:"Information and Advertisement", items:itemsC),Category(name:"Information and Advertisement", items:itemsD)]
        }

        qrCodeTableView.register(UINib(nibName: "BusinessAccDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "BusinessCell")
        qrCodeTableView.register(UINib(nibName: "PersonalAccDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "PersonalCell")
        qrCodeTableView.register(UINib(nibName: "AmountRemarkTableViewCell", bundle: nil), forCellReuseIdentifier: "AmountRemarkCell")
        qrCodeTableView.register(UINib(nibName: "InformationTableViewCell", bundle: nil), forCellReuseIdentifier: "InformationCell")
        qrCodeTableView.estimatedRowHeight = 60
        qrCodeTableView.rowHeight = UITableView.automaticDimension
    }

    func reloadAndScrollToTop() {
        qrCodeTableView.reloadData()
        qrCodeTableView.layoutIfNeeded()
        qrCodeTableView.contentOffset = CGPoint(x: 0, y: -qrCodeTableView.contentInset.top)
    }

    func setLayoutViews() {
        moreOptionsView.layer.cornerRadius = 25
        moreOptionsBtn.layer.cornerRadius = 25
        scanQRView.layer.cornerRadius = self.scanQRView.frame.height / 2.0
        scanQRViewBtn.layer.cornerRadius = self.scanQRViewBtn.frame.height / 2.0
    }
    
    @IBAction func moreOptionsAction(_ sender: UIButton) {
        let alertController:UIAlertController=UIAlertController(title: "My QR Code", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let shareAction = UIAlertAction(title: "Share QR", style: UIAlertAction.Style.default){ UIAlertAction in
            self.shareQRWithDetail()
        }
        shareAction.setValue(UIImage(named: "shareQR")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
        shareAction.setValue(UIColor.black, forKey: "titleTextColor")
        
        let saveToPrintAction = UIAlertAction(title: "Save QR", style: UIAlertAction.Style.default){ UIAlertAction in
            let status = PHPhotoLibrary.authorizationStatus()
            if (status == PHAuthorizationStatus.authorized) {
                println_debug(".authorized")
                DispatchQueue.main.async {
                 self.saveToPrint()
                }
            }else if (status == PHAuthorizationStatus.denied) {
                  println_debug(".denied")
                self.showPhotoAlert()
            }else if (status == PHAuthorizationStatus.notDetermined) {
                 println_debug(".notDetermined")
                PHPhotoLibrary.requestAuthorization({ (newStatus) in
                    if (newStatus == PHAuthorizationStatus.authorized) {
                        println_debug(".authorized 2")
                        DispatchQueue.main.async {
                            self.saveToPrint()
                        }
                    }else {
                    self.showPhotoAlert()
                    }
                })
            }else if (status == PHAuthorizationStatus.restricted) {
                // Restricted access - normally won't happen.
            }
        }
        
        saveToPrintAction.setValue(UIImage(named: "printQR")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
        saveToPrintAction.setValue(UIColor.black, forKey: "titleTextColor")
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel){ UIAlertAction in }
        
        if UserModel.shared.agentType == .user {
            alertController.addAction(shareAction)
            alertController.addAction(saveToPrintAction)
            alertController.addAction(cancelAction)
        }else {
            let printQR = UIAlertAction(title: "Print QR", style: UIAlertAction.Style.default){ UIAlertAction in
                self.PrintORCode()
            }
            printQR.setValue(UIImage(named: "printQR")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
            printQR.setValue(UIColor.black, forKey: "titleTextColor")
            alertController.addAction(printQR)
            alertController.addAction(shareAction)
            alertController.addAction(saveToPrintAction)
            alertController.addAction(cancelAction)
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func onClickSkipSuggestonsAction(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "is_QR_Sugesstion_Read")
        self.viewSuggestions.isHidden = true
    }
    
    @IBAction func onClickSuggestionNextAction(_ sender: Any) {
        imageIndex += 1
        if imageIndex == 1 {
            btnSkip.isHidden = false
            imgSuggestions.image = UIImage(named: "qrCode_Print")
            btnSuggestionNext.setTitle("Next_".localized, for: .normal)
            lblSugesstionTitle.text = "PRINT".localized
            imgTitleImage.image = UIImage(named: "print_QR")
            lblSugesstionSubTitle.text = youcanString + "Print your QR code for stand display".localized
            imgS_First.image = UIImage(named: "radio_select_qr")
            imgS_Second.image = UIImage(named: "radio")
            imgS_thired.image = UIImage(named: "radio")
            lblNewFeatures.startBlink()
            lblNewFeatures.isHidden = false
        }else if imageIndex == 2 {
            btnSkip.isHidden = false
            imgSuggestions.image = UIImage(named: "qrCodeSave")
            btnSuggestionNext.setTitle("Next_".localized, for: .normal)
            lblSugesstionTitle.text = "Save_".localized
            imgTitleImage.image = UIImage(named: "gallery_QR")
            lblSugesstionSubTitle.text = youcanString + "Save your QR code in gallery".localized
            imgS_First.image = UIImage(named: "radio")
            imgS_Second.image = UIImage(named: "radio_select_qr")
            imgS_thired.image = UIImage(named: "radio")
            lblNewFeatures.stopBlink()
            lblNewFeatures.isHidden = true
        }else if imageIndex == 3 {
            btnSkip.isHidden = true
            imgSuggestions.image = UIImage(named: "qrCode_Share")
            btnSuggestionNext.setTitle("Continue_".localized, for: .normal)
            lblSugesstionTitle.text = "Share_".localized
            imgTitleImage.image = UIImage(named: "reportShareWhite")
            lblSugesstionSubTitle.text = youcanString + "Share your QR code to others".localized
            imgS_First.image = UIImage(named: "radio")
            imgS_Second.image = UIImage(named: "radio")
            imgS_thired.image = UIImage(named: "radio_select_qr")
             lblNewFeatures.stopBlink()
            lblNewFeatures.isHidden = true
        }else {
            UserDefaults.standard.set(true, forKey: "is_QR_Sugesstion_Read")
            self.viewSuggestions.isHidden = true
        }
    }
    
    private func showPhotoAlert(){
        alertViewObj.wrapAlert(title:"", body:"Please Allow Camera Access".localized, img: UIImage(named: "alert-icon"))
        alertViewObj.addAction(title: "Setting".localized, style: .target , action: {
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string: UIApplication.openSettingsURLString)! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            }
        })
        alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
        }
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func PrintORCode() {
    performSegue(withIdentifier: "myQRCodeToPrintViewPersonal", sender: self)
    }
    
    func shareQRWithDetail() {
        DispatchQueue.main.async {
            
        
            self.viewShare.isHidden = false
        let newImage = self.viewShare.toImage()
        let imageToShare = [newImage]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        self.present(activityViewController, animated: true, completion: {
            self.viewShare.isHidden = true
        })
        }
    }
    
    func saveToPrint() {
         viewShare.isHidden = false
        let newImage = self.viewShare.toImage()
        UIImageWriteToSavedPhotosAlbum(newImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    func getQRCodeImage(imagQR: UIImage) {
        //Set data to QR view to save & Share
        let url = URL(string: UserModel.shared.proPic.replacingOccurrences(of: " ", with: "%20"))
        self.imgUser.sd_setImage(with: url, placeholderImage: UIImage(named: "avatar"))
        self.lblUserName.text = (UserModel.shared.name.replacingOccurrences(of: ",", with: ". "))
        let (mobileNumber,CountryCode) = getMobileNumberWithoutZero(strToTrim: UserModel.shared.mobileNo)
        lblMobilNumer.text = CountryCode + " " + mobileNumber
        lblTelcode.text = getOparatorName(UserModel.shared.mobileNo)
        lblQRAmount.attributedText = qrCodeObj.concatinateAmountWithMMK(amount: "0.0")
        self.imgQR.image = imagQR
        
        let (date,time) = qrCodeObj.currentDateAndTime()
        lblQRDate.text = date
        lblTime.text = time
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        var strMessage = ""
        if let error = error {
            println_debug(error)
            strMessage = "Error while saving QR Code".localized
        } else {
            strMessage = "My QR Code successfully saved in gallery.".localized
        }
        alertViewObj.wrapAlert(title: "".localized, body: strMessage, img: #imageLiteral(resourceName: "info_success"))
        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
             self.viewShare.isHidden = true
        })
        alertViewObj.showAlert(controller: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "myQRCodeToPrintViewBusiness" {
            let vc = segue.destination as! UINavigationController
            let rootViewController = vc.viewControllers.first as! QRCodePreviewBusiness
            let str = qrCodeObj.qrStringGenerationForPrint(amount: enteredAmountStr, beforePayment: informationCellArr[0], afterPayment: informationCellArr[1], remarks: amountCellArr[1])
            rootViewController.qrStr = str
        }else if segue.identifier == "myQRCodeToPrintViewPersonal" {
            let vc = segue.destination as! UINavigationController
            let rootViewController = vc.viewControllers.first as! QRCodePreviewPersonal
            let str = qrCodeObj.qrStringGenerationForPrint(amount: enteredAmountStr, beforePayment: informationCellArr[0], afterPayment: informationCellArr[1], remarks: amountCellArr[1])
            rootViewController.qrStr = str
        }else if (segue.identifier == "GeneratedQRCode") {
            let vc = segue.destination as! UINavigationController
            vc.modalPresentationStyle = .fullScreen
            let rootViewController = vc.viewControllers.first as! GeneratedQRCode
            let infoDic = ["amount": enteredAmountStr,"beforePayment": informationCellArr[0],"afterPayment": informationCellArr[1],"remarks": amountCellArr[1]]
            rootViewController.qrCodeInfoDic = infoDic
        }
    }

    @IBAction func generateQRCodeAction(_ sender: UIButton) {
        self.view.endEditing(true)
      //  heightGenerateQRBtnConstraint.constant = 50 //0
        //self.qrCodeTableView.reloadData()
       // self.qrCodeTableView.reloadSections([0], with: .none)
       // bottomOptionsViewConstraint.constant = 20
        addQrDataInCoreData()
        performSegue(withIdentifier: "GeneratedQRCode", sender: self)
    }

    func addQrDataInCoreData() {
        let context = appDelegate.persistentContainer.viewContext
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "QRCodeDetails")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            println_debug("Deleting all records in favorites")
        }
        
        let qRCodeDetails = NSEntityDescription.insertNewObject(forEntityName: "QRCodeDetails", into: context) as? QRCodeDetails
        qRCodeDetails?.amountCellData = self.amountCellArr
        qRCodeDetails?.informationCellData = self.informationCellArr
        do {
            try context.save()
        } catch let error as NSError {
            println_debug("Could not save. \(error), \(error.userInfo)")
        }
    }
}

extension TBMyQRCode : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sections[section].items.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
            return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else {
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 5))
        headerView.backgroundColor = UIColor.colorWithRedValue(redValue: 242, greenValue: 242, blueValue: 244, alpha: 1)
        return headerView
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let items = self.sections[indexPath.section].items
        switch (indexPath.section) {
        case 0:
            if UserModel.shared.agentType == .user {
                let cell = qrCodeTableView.dequeueReusableCell(withIdentifier: "PersonalCell") as! PersonalAccDetailsTableViewCell
                cell.lblScanToPay.font = UIFont(name: appFont, size: appFontSize)
                cell.delegate = self
                cell.btnQRCodeInfo.addTarget(self, action: #selector(TBMyQRCode.onClickQRCodeInformation(sender:)), for: .touchUpInside)
                  cell.generateQRCodeAction(qrCodeObj.qrStringGenerationForPrint(amount: "", beforePayment: "", afterPayment: "", remarks: ""))
                return cell
            }else {
                let cell = qrCodeTableView.dequeueReusableCell(withIdentifier: "BusinessCell") as! BusinessAccDetailsTableViewCell
                cell.delegate = self
                cell.btnOnfo.addTarget(self, action: #selector(TBMyQRCode.onClickQRCodeInformation(sender:)), for: .touchUpInside)
                cell.generateQRCodeAction(qrCodeObj.qrStringGenerationForPrint(amount: "", beforePayment: "", afterPayment: "", remarks: ""))
                return cell
            }
        case 1:
            let cell = qrCodeTableView.dequeueReusableCell(withIdentifier: "AmountRemarkCell") as! AmountRemarkTableViewCell
            let item = items[indexPath.row]
            cell.configureWithItem(row: indexPath.row, item: item, amountCellArr: amountCellArr)
            cell.textFieldView.tag = indexPath.row
            cell.indexPath = indexPath
            cell.crossButton.tag = indexPath.row
            cell.textChangeDelegate = self
            cell.layoutIfNeeded()  
            return cell
         case 2:
            if UserModel.shared.agentType == .user {
            let cell = qrCodeTableView.dequeueReusableCell(withIdentifier: "QRTipsCell") as! QRTipsCell
                 cell.lblSubTitle.text = "To Share, Print and Save.\n Please press on \"Generate Button\".".localized
                return cell
            }else {
            let cell = qrCodeTableView.dequeueReusableCell(withIdentifier: "InformationCell") as! InformationTableViewCell
            let item = items[indexPath.row]
            var before = ""
            var after = ""
            var beforeMessage = ""
            var afterMessage = ""
            
            //Fetch Value from DataBase
            let managedContext = appDelegate.persistentContainer.viewContext
            let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "BeforePaymentInfo")
            let result = try? managedContext.fetch(fetch)
            let fetch1 = NSFetchRequest<NSFetchRequestResult>(entityName: "AfterPaymentInfo")
            let result1 = try? managedContext.fetch(fetch1)
            
            if (result?.count)! > 0 {
                let be = result?[0] as? BeforePaymentInfo
                let selected = be?.beforeSeletedData
                if let list = be?.beforeDataInfo {
                    let infoList = list as! [String]
                    if infoList.count>0{
                        let index = Int(selected ?? "1") ?? 1
                        if  index != 10 {
                            before = String(index + 1)
                            beforeMessage = infoList[index]
                        }
                    }
                }
            }
            
            if (result1?.count)! > 0 {
                let be = result1?[0] as? AfterPaymentInfo
                let selected = be?.afterSelectedData
                if let list = be?.afterDataInfo {
                    let infoList = list as! [String]
                    
                    let index = Int(selected ?? "1") ?? 1
                    if  index != 10 {
                        after = String(index + 1)
                        afterMessage = infoList[index]
                    }
                }
            }
            
            //arrow_Right
            if before != "" || after != "" {
            var message = ""
            if after == "" {
             message = "Information selected".localized + " " + "B" + before
            }else if before == ""{
            message = "Information selected".localized + " " + "A" + after
            }else {
            message = "Information selected".localized + " " + "B" + before + " & " + "A" + after
            }
           // cell.btnInfomation.setTitle(message, for: .normal)
            cell.lblInformationTitle.text = message
            //cell.btnInfomation.contentHorizontalAlignment = .left
            cell.lblInformationTitle.textAlignment = .left
            cell.bgInfo.backgroundColor = UIColor.white
            //cell.btnInfomation.setTitleColor(.black , for: .normal)
            cell.lblInformationTitle.textColor = UIColor.black
            cell.imgIcon.isHidden = false
            cell.imgArrow.image = UIImage(named: "PD_arrow.")
            informationCellArr[0] = beforeMessage
            informationCellArr[1] = afterMessage
            }else {
            cell.bgInfo.backgroundColor = #colorLiteral(red: 0.1490196078, green: 0.2705882353, blue: 0.662745098, alpha: 1)
            //cell.btnInfomation.setTitleColor(.white , for: .normal)
            cell.lblInformationTitle.textColor = UIColor.white
            //cell.btnInfomation.setTitle("Set Information and Advertisement".localized, for: .normal)
            cell.lblInformationTitle.text = "Set Information and Advertisement".localized
            //cell.btnInfomation.contentHorizontalAlignment = .center
            cell.lblInformationTitle.textAlignment = .center
            cell.imgIcon.isHidden = true
            cell.imgArrow.image = UIImage(named: "arrow_Right")
            informationCellArr[0] = ""
            informationCellArr[1] = ""
            }
            cell.btnInfomation.addTarget(self, action: #selector(TBMyQRCode.onClickQRInformation(sender:)), for: .touchUpInside)
            cell.configureWithItem(row: indexPath.row, item: item, informationCellArr: informationCellArr)
            cell.indexPath = indexPath
            cell.parentVC = self
            cell.textChangeDelegate = self
            if checkInformationCellDisable {
                cell.isUserInteractionEnabled = true
            } else {
                cell.isUserInteractionEnabled = false
            }
            cell.layoutIfNeeded()
            return cell
        }
        case 3:
            let cell = qrCodeTableView.dequeueReusableCell(withIdentifier: "QRTipsCell") as! QRTipsCell
            cell.lblSubTitle.text = "To Share, Print and Save.\n Please press on \"Generate Button\".".localized
            cell.selectionStyle = .none
            return cell
        default:
          let cell = UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
          return cell
      }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return self.qrCodeTableView.estimatedRowHeight
        }else if indexPath.section == 2{
                return 60
        }else if indexPath.section == 3 {
            return 120
        }else {
              return UITableView.automaticDimension
        }
    }
    
    @objc func onClickQRInformation(sender: UIButton) {
        self.performSegue(withIdentifier: "ShowAdertizement", sender: self)
    }
    
    @objc func onClickQRCodeInformation(sender: UIButton) {
        self.performSegue(withIdentifier: "QRCodeToPersonalInfo", sender: self)
    }

    @objc func tapOnInfoViewBtn(sender:UITapGestureRecognizer) {
        self.showMsgPopupAlert(msg: "You can advertise offer, promotion and deal to your customers Before/After Payment.".localized, image: #imageLiteral(resourceName: "infoQR"))
    }
    @objc func tapOnInfoBtn (sender:UITapGestureRecognizer) {
        self.showMsgPopupAlert(msg: "You can advertise offer, promotion and deal to your customers Before/After Payment.".localized, image: #imageLiteral(resourceName: "infoQR"))
    }
}

extension TBMyQRCode: TextChangeForAmountCellDelegate, TextChangeForInformationCellDelegate {
    func generateButtonTitleChange(title: String) {
        generateQRCodeBtn.setTitle(title, for: .normal)
    }
    
    func textChangedAtInformationCell(_ cell: InformationTableViewCell, text: String?, index: Int, indexPath: IndexPath) {
//        informationCellArr[index] = text!
//        qrCodeTableView.layoutIfNeeded()
   }
    
    
    func textChangedAtAmountCell(_ cell: AmountRemarkTableViewCell, text: String?, index: Int) {
        if (text?.count)! == 0 {
             generateQRCodeBtn.isHidden = false //true
            // heightGenerateQRBtnConstraint.constant = 50 // 0
        }else {
          
            DispatchQueue.main.async {
            self.generateQRCodeBtn.isHidden = false
           // self.heightGenerateQRBtnConstraint.constant = 50
                delay(delay: 0.5, closure: {
                 self.qrCodeTableView.scrollRectToVisible(cell.frame, animated: false)
                })
            }
        }
        
        if index == 0 {
            let amountStr = amountWithCommaFormat(textStr: text!)
            enteredAmountStr = amountStr
            amountCellArr[index] = amountStr
        }else {
            amountCellArr[index] = text!
        }
        self.addQrDataInCoreData()
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}


extension TBMyQRCode: TBScanQRDelegate {
    func moveToPayto(withScanObject object: PTScanObject, fromTransfer: Bool?) {
        if let delegate = self.scanDelegate {
            delegate.moveToPayto(withScanObject: object, fromTransfer: false)
        } else {
            self.navigateToPayto(phone: object.agentNo, amount: object.amount, name: object.name, remark: object.remarks)
        }
    }
    
    func moveToPayTo(fromTransfer: Bool?) {
        if let delegate = self.scanDelegate {
            delegate.moveToPayTo(fromTransfer: false)
        } else {
            self.navigatetoPayto()
        }
    }
}
