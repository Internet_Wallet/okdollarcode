//
//  QRCodePreviewBusiness.swift
//  OK
//
//  Created by Shobhit Singhal on 7/25/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class QRCodePreviewBusiness: OKBaseController {
    @IBOutlet weak var backBottomBtn : UIButton! {
        didSet {
            self.backBottomBtn.titleLabel?.font = UIFont(name: appFont, size: appTitleButton)
            self.backBottomBtn.setTitle(self.backBottomBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var qrFromatANDSaveQRBtn : UIButton!{
        didSet {
            self.qrFromatANDSaveQRBtn.titleLabel?.font = UIFont(name: appFont, size: appTitleButton)
        }
    }

    // Print View OUTLETS
    
    @IBOutlet weak var printView: UIView!
    @IBOutlet weak var lblPaymentAcceptHere: UILabel!{
        didSet{
            lblPaymentAcceptHere.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblBusinessName: UILabel!{
        didSet{
            lblBusinessName.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblOutletName: UILabel!
    @IBOutlet weak var lblMerchantSiince: UILabel!
    @IBOutlet weak var lblMerchantRank: UILabel!
    @IBOutlet weak var lblMobilNumber: UILabel!
    @IBOutlet weak var imgQRPrint: UIImageView!
    @IBOutlet weak var lblServiceAvail: UILabel!{
        didSet{
            lblServiceAvail.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var lblPayToPrint: UILabel!{
        didSet{
            lblPayToPrint.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var lblScanPrint: UILabel!{
        didSet{
            lblScanPrint.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var lblbDonePrint: UILabel!{
        didSet{
            lblbDonePrint.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var lblCashInPrint: UILabel!{
        didSet{
            lblCashInPrint.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var lblCashOutPrint: UILabel!{
        didSet{
            lblCashOutPrint.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var lblBillPayPrint: MarqueeLabel!{
        didSet{
                lblBillPayPrint.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var imgVerify: UIImageView!
    @IBOutlet weak var lblScanToPAy: UILabel!{
        didSet{
            lblScanToPAy.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    
    var QRFormatArr = [String]()
    var displaySize:String = ""
    var qrStr:String = ""
    var qrImageObject :  PTQRGenerator?
    var userSelectedLanguage:String = ""
    var coverViewForBlackWhite:UIView!
    let qrCode = QRCodeGenericClass()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        imgLogo.layer.cornerRadius = imgLogo.frame.width / 2
        imgLogo.layer.cornerRadius = imgLogo.frame.height / 2
        imgLogo.layer.masksToBounds = true
        let url1 = URL(string: UserModel.shared.logoImages.replacingOccurrences(of: " ", with: "%20"))
        self.imgLogo.sd_setImage(with: url1, placeholderImage: UIImage(named: "avatar"))
        
        if kycAppControlInfo.first?.kycApprovalStatus == 1 {
            self.imgVerify.isHidden = false
        }else {
            self.imgVerify.isHidden = true
        }
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
            self.title = "Print Preview".localized
            qrFromatANDSaveQRBtn.setTitle("Save_".localized, for: .normal)
        qrFromatANDSaveQRBtn.setTitle("Print".localized, for: .normal)
        self.initLoad()
        self.generateQRCodeAction(qrStr)
        if QRFormatArr[1] == "Black & White" {
            DispatchQueue.main.async {
                progressViewObj.showProgressView()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if QRFormatArr[1] == "Black & White" {
            coverViewForBlackWhite.isHidden = true
            let image = printView.toImage()
            if let noir = image.noir {
                let blackWhiteImageView  = UIImageView(frame:CGRect(x: 0, y: 0, width: screenWidth, height: printView.bounds.size.height));
                blackWhiteImageView.image = noir
                printView.addSubview(blackWhiteImageView)
            }
        }
        DispatchQueue.main.async {
            self.removeProgressView()
        }
    }
    
    func initLoad() {
        
        if UserModel.shared.agentType == .user {
            lblBusinessName.text = (UserModel.shared.name.replacingOccurrences(of: ",", with: ". "))
            lblOutletName.text = ""//UserModel.shared.businessOutlet
        }else {
            lblBusinessName.text = UserModel.shared.businessName
            lblOutletName.text = UserModel.shared.businessOutlet
        }
        let (mobileNumber,CountryCode) = getMobileNumberWithoutZero(strToTrim: UserModel.shared.mobileNo)
        // Set Data TO Print view
 
//        if appDel.currentLanguage == "uni" {
//            lblPayToPrint.font =  UIFont(name : "Myanmar3", size : 12.0)
//            lblScanPrint.font =  UIFont(name : "Myanmar3", size : 12.0)
//            lblbDonePrint.font =  UIFont(name : "Myanmar3", size : 12.0)
//            lblCashInPrint.font =  UIFont(name : "Myanmar3", size : 12.0)
//            lblCashOutPrint.font =  UIFont(name : "Myanmar3", size : 12.0)
//            lblBillPayPrint.font =  UIFont(name : "Myanmar3", size : 12.0)
//            lblServiceAvail.font =  UIFont(name : "Myanmar3", size : 12.0)
//            lblPaymentAcceptHere.font =  UIFont(name : "Myanmar3", size : 12.0)
//            lblScanToPAy.font =  UIFont(name : "Myanmar3", size : 12.0)
//        } else {
//            lblPayToPrint.font = UIFont(name : "Zawgyi-One", size : 16.0)
//            lblScanPrint.font =  UIFont(name : "Zawgyi-One", size : 16.0)
//            lblbDonePrint.font =  UIFont(name : "Zawgyi-One", size : 16.0)
//            lblCashInPrint.font =  UIFont(name : "Zawgyi-One", size : 16.0)
//            lblCashOutPrint.font =  UIFont(name : "Zawgyi-One", size : 16.0)
//            lblBillPayPrint.font =  UIFont(name : "Zawgyi-One", size : 16.0)
//            lblServiceAvail.font =  UIFont(name : "Zawgyi-One", size : 16.0)
//            lblPaymentAcceptHere.font =  UIFont(name : "Zawgyi-One", size : 16.0)
//            lblScanToPAy.font =  UIFont(name : "Zawgyi-One", size : 16.0)
//        }
        
        lblMerchantRank.text = ""
        lblMerchantSiince.text = ""
            //UserDefaults.standard.value(forKey: "OkdollarMembership") as? String
        lblMobilNumber.text  = "(" + CountryCode + ")" + " 0" + mobileNumber
 
            displaySize = QRFormatArr[0]
            userSelectedLanguage = QRFormatArr[2]
        lblPaymentAcceptHere.text = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "OK$ Payment Accept Here")//"OK$ Payment Accept Here".localized //
        lblScanToPAy.text = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Scan To Pay")//"Scan To Pay".localized //
        lblServiceAvail.text  = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Services Available")//"Services Available".localized //
            
        lblPayToPrint.text  = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Payto")//"Payto".localized //
            
        lblScanPrint.text  = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Scan")
            //"Scan".localized //
        lblbDonePrint.text  = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Done ")
            
        lblCashInPrint.text  = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Cash-In")
            
        lblCashOutPrint.text  = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Cash-Out")
            
        lblBillPayPrint.text  = appDel.getStringAccordingToLanguage(language: userSelectedLanguage, key: "Bill Payments")
            
            if QRFormatArr[1] == "Black & White" {
                coverViewForBlackWhite  = UIView(frame:CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
                coverViewForBlackWhite.backgroundColor = UIColor.white
                self.view.addSubview(coverViewForBlackWhite)
            }
    }

    func generateQRCodeAction(_ str: String) {
        qrImageObject = PTQRGenerator()
        guard qrImageObject != nil , let qrImage = qrImageObject else {
            return
        }
        
        guard let image =  qrImage.getQRImage(stringQR: str, withSize: 10) else {
            return
        }
        self.imgQRPrint.image = image
    }
    
    @IBAction func dismissControllerAction(_ sender: UIBarButtonItem) {
            self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backAction(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func qrFromatANDSaveQRAction(_ sender: UIButton) {
       self.saveToPrint()
    }
    
    private func saveToPrint() {
        
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.authorized) {
            println_debug(".authorized")
            DispatchQueue.main.async {
                self.saveToGallary()
            }
        }else if (status == PHAuthorizationStatus.denied) {
            println_debug(".denied")
            self.showPhotoAlert()
        }else if (status == PHAuthorizationStatus.notDetermined) {
            println_debug(".notDetermined")
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                if (newStatus == PHAuthorizationStatus.authorized) {
                    println_debug(".authorized 2")
                    DispatchQueue.main.async {
                        self.saveToGallary()
                    }
                }else {
                    self.showPhotoAlert()
                }
            })
        }else if (status == PHAuthorizationStatus.restricted) {
            // Restricted access - normally won't happen.
        }
    }
    
    private func showPhotoAlert() {
        alertViewObj.wrapAlert(title:"", body:"Please Allow Camera Access".localized, img: UIImage(named: "alert-icon"))
        alertViewObj.addAction(title: "Setting".localized, style: .target , action: {
            UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
        })
        alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
        }
        DispatchQueue.main.async {
            alertViewObj.showAlert(controller: self)
        }
    }
    
    private func saveToGallary() {
            let image = self.printView.toImage()
            var newImage:UIImage!
            if displaySize == "A6" {
                newImage = self.ResizeImage(image: image, targetSize: CGSize(width: 1240, height: 1748))
            }
            else if displaySize == "A5" {
                newImage = self.ResizeImage(image: image, targetSize: CGSize(width: 1748, height: 2480))
            }
            else if displaySize == "A4" {
                newImage = self.ResizeImage(image: image, targetSize: CGSize(width: 2480, height: 3508))
            }
            if QRFormatArr[1] == "Color" {
                UIImageWriteToSavedPhotosAlbum(newImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
            }else {
                if let noir = newImage.noir {
                    UIImageWriteToSavedPhotosAlbum(noir, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
                }
            }
            alertViewObj.wrapAlert(title:"", body: "My QR Code Image already Saved in Gallery. Print it from the Gallery. To share with others click \"Share\" Button.".localized, img: UIImage(named: "save_gray"))
            alertViewObj.addAction(title: "DoneQR".localized, style: .target , action: {
                //self.dismiss(animated: false, completion: nil)
           // NotificationCenter.default.post(name: Notification.Name(rawValue: "MoveToDashboard"), object: nil)
            })
            alertViewObj.addAction(title: "Share_QR".localized, style: .target , action: {
                self.shareQRWithDetail()
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
    }
    
    
    
    func ResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        }
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }

    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            alertViewObj.wrapAlert(title: "Save error", body: error.localizedDescription, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            alertViewObj.showAlert(controller: self)
        } else {
            println_debug("Your altered image has been saved to your photos.")
        }
    }

    func shareQRWithDetail() {
        let image =  printView.toImage()
        if QRFormatArr[1] == "Color" {
            let imageToShare = [image]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
        } else {
            if let noir = image.noir {
                let imageToShare = [noir]
                let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
                DispatchQueue.main.async {self.present(activityViewController, animated: true, completion: nil)}
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "QRPreviewBusinessToQRFormat" {
            let controller = segue.destination as! QRFormat
            controller.qrStr = self.qrStr
        }
    }
}
