//
//  QRPaymentVerificationViewController.swift
//  OK
//
//  Created by Ashish on 6/23/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class QRPaymentVerificationViewController: OKBaseController, UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    
    @IBOutlet weak var paymentTable: UITableView!
    @IBOutlet weak var yesButton: UIButton!
    
    @IBOutlet weak var heightContainerView: NSLayoutConstraint!
    
    var keyArray : [String]?

    override func viewDidLoad() {
        super.viewDidLoad()
        keyArray = ["Amount", "Generate Number", "Generate Name", "Scanner Number", "Scanner Name", "Date & Time", "Transaction ID"]
        self.paymentTable.reloadData()
        self.yesButton.setTitle("YES".localized, for: .normal)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.heightContainerView.constant = 250.00
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func dismissButtonAction(_ sender: UIButton) {
        UIView.animate(withDuration: 0.1, animations: {
            self.view.layoutIfNeeded()
        }) { (success) in
            if success {
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    //MARK:- Delegate & Datasource
    

}

//MARK:- Cell Class Verify Payment

class QRVerifyPaymentCell : UITableViewCell {
    
    @IBOutlet weak var keyLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    func wrapData(withKey key: String, value: String) {
        self.keyLabel.text   = key.localized
        self.valueLabel.text = value
    }

}
