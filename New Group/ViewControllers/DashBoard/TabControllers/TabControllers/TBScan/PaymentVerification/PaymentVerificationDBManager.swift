//
//  PaymentVerificationDBManager.swift
//  OK
//
//  Created by Ashish on 6/23/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreData

class PaymentVerificationDBManager : NSObject {
    
    private func getContext() -> NSManagedObjectContext {
        return appDel.persistentContainer.viewContext
    }
    
    func verifyExistingDataFromDB(transactionID : String) -> Bool {
        // Create Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PaymentVerification")
        let context = getContext()
        // Helpers
        var result = false
        var arrRecord = [PaymentVerification]()

        do {
            // Execute Fetch Request
            let records = try context.fetch(fetchRequest)
            
            if let records = records as? [PaymentVerification] {
                arrRecord = records
                if arrRecord.count == 0 {
                    result = false
                    return result
                } else {
                    for element in arrRecord {
                        if element.transactionId == transactionID {
                            result = true
                            return result
                        } else {
                            result = false
                        }
                    }
                }
            }
            
        } catch {
            println_debug("Unable to fetch managed objects for favorite entity.")
        }
        return result
    }

     func loadScannedPaymentToDatabase(model: VerifyPaymentModel) {
        let context = getContext()
        let entity = NSEntityDescription.entity(forEntityName: "PaymentVerification", in: context)
        if let wrapEntity = entity {
            if let paymentDBObject = NSManagedObject(entity: wrapEntity, insertInto: context) as? PaymentVerification {
                paymentDBObject.age              = model.age.safelyWrappingString()
                paymentDBObject.amount           = model.amount.safelyWrappingString()
                paymentDBObject.bonusPoint       = model.bonusPoint.safelyWrappingString()
                paymentDBObject.cashback         = model.cashback.safelyWrappingString()
                paymentDBObject.gender           = model.gender.safelyWrappingString()
                paymentDBObject.generateTime     = model.generateTime.safelyWrappingString()
                paymentDBObject.receiverName     = model.receiverName.safelyWrappingString()
                paymentDBObject.receiverNumber   = model.receiverNumber.safelyWrappingString()
                paymentDBObject.scanningTime     = model.scanningTime.safelyWrappingString()
                paymentDBObject.senderName       = model.senderName.safelyWrappingString()
                paymentDBObject.senderNumber     = model.senderNumber.safelyWrappingString()
                paymentDBObject.transactionId    = model.transactionId.safelyWrappingString()
                paymentDBObject.transactionType  = model.transactionType.safelyWrappingString()
                paymentDBObject.validated        = (model.validated == nil) ? false : model.validated!
            }
            do {
                try context.save()
            } catch let error as NSError {
                println_debug(error)
            }
        }
    }
    
    func getAllPaymentVerificationDataFromDB() -> [PaymentVerification] {
        // Create Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PaymentVerification")
        let context = getContext()
        // Helpers
        var result = [PaymentVerification]()
        
        do {
            // Execute Fetch Request
            let records = try context.fetch(fetchRequest)
            
            if let records = records as? [PaymentVerification] {
                result = records.reversed()
            }
            
        } catch {
            println_debug("Unable to fetch managed objects for favorite entity.")
        }
        
        return result
    }


}

class VerifyPaymentModel : NSObject {
    
    var receiverName : String?
    var scanningTime : String?
    var generateTime : String?
    var age : String?
    var gender : String?
    var cashback : String?
    var bonusPoint : String?
    var transactionType : String?
    var transactionId : String?
    var amount : String?
    var senderNumber : String?
    var senderName : String?
    var receiverNumber : String?
    var validated : Bool?
    var destination : String?
    
    func wrapModel(processedString: String) {
        
        let dateFormat = DateFormatter()
        dateFormat.timeZone = .current
        dateFormat.dateFormat = "EEE, dd-MMM-yyyy hh:mm:ss a"
        
        let stringComponent = processedString.components(separatedBy: "-")
        if processedString.contains(find: "cashin") || processedString.contains(find: "cashout") {
            let receiverNames = stringComponent[safe: 18] ?? ""
            self.receiverName = receiverNames
            
            self.scanningTime = dateFormat.string(from: Date())
            
            let generateTimes = stringComponent[safe: 4]  ?? ""
            self.generateTime = generateTimes
            
            let age = stringComponent[safe: 16] ?? ""
            self.age = age
            
            let gender = stringComponent[safe: 15] ?? ""
            self.gender = gender
            
            let cashback = stringComponent[safe: 11] ?? ""
            self.cashback = cashback
            
            let bonuspoints = stringComponent[safe: 12] ?? ""
            self.bonusPoint = bonuspoints
            
            let transcType = stringComponent[safe: 10] ?? ""
            self.transactionType = transcType
            
            let transID = stringComponent[safe: 5] ?? ""
            self.transactionId = transID
            
            let dest = stringComponent[safe: 9] ?? ""
            self.destination = dest
            
            let amountStr = stringComponent[safe: 3] ?? ""
            self.amount = amountStr
            
            let sendName = stringComponent[safe: 1] ?? ""
            self.senderName = sendName
            
            let senderNumbers = stringComponent[safe: 6] ?? ""
            self.senderNumber = senderNumbers
            
            self.receiverNumber = UserModel.shared.formattedNumber
        } else {
            
            let receiverNames = stringComponent[safe: 17] ?? ""
            self.receiverName = receiverNames
            
            self.scanningTime = dateFormat.string(from: Date())
            
            let generateTimes = stringComponent[safe: 3]  ?? ""
            self.generateTime = generateTimes
            
            let age = stringComponent[safe: 15] ?? ""
            self.age = age
            
            let gender = stringComponent[safe: 14] ?? ""
            self.gender = gender
            
            let cashback = stringComponent[safe: 10] ?? ""
            self.cashback = cashback
            
            let bonuspoints = stringComponent[safe: 11] ?? ""
            self.bonusPoint = bonuspoints
            
            let transcType = stringComponent[safe: 9] ?? ""
            self.transactionType = transcType
            
            let transID = stringComponent[safe: 4] ?? ""
            self.transactionId = transID
            
            let dest = stringComponent[safe: 8] ?? ""
            self.destination = dest
            
            let amountStr = stringComponent[safe: 2] ?? ""
            self.amount = amountStr
            
            let sendName = stringComponent[safe: 1] ?? ""
            self.senderName = sendName
            
            let senderNumbers = stringComponent[safe: 5] ?? ""
            self.senderNumber = senderNumbers
            
            self.receiverNumber = UserModel.shared.formattedNumber
        }
        
    }
    
}






