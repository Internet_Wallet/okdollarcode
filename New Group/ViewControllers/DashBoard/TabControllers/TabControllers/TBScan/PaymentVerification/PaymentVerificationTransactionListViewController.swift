//
//  PaymentVerificationTransactionListViewController.swift
//  OK
//
//  Created by Ashish on 6/24/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class PaymentVerificationTransactionListViewController: OKBaseController, UITableViewDelegate, UITableViewDataSource {
    
    var modelVerify : [PaymentVerification]?

    @IBOutlet weak var pvTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pvTable.delegate = self
        self.pvTable.dataSource = self
        self.pvTable.reloadData()
        
        self.title = "Verify Payment".localized
        let buttonIcon      = UIImage(named: "tabBarBack")
        let leftBarButton   = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.done, target: self, action: #selector(PaymentVerificationTransactionListViewController.popTheViewControllerPT(_:)))
        leftBarButton.image = buttonIcon
        self.navigationItem.leftBarButtonItem = leftBarButton
        self.navigationController?.navigationBar.tintColor = .white
        
        // Do any additional setup after loading the view.
    }
    
    @objc fileprivate func popTheViewControllerPT(_ sender:UIBarButtonItem!) {
            self.navigationController?.popViewController(animated: true)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // mark table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.modelVerify == nil) ? 0 : self.modelVerify!.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: PaymentVerificationListCell.self), for: indexPath)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: String.init(describing: PaymentVerificationListDataCell.self), for: indexPath) as? PaymentVerificationListDataCell
            let model = modelVerify![indexPath.row - 1]
            cell?.wrapListData(model: model)
            return cell!
        }
    }
    
}

//MARK:- Cell Class for Payment Verification list dsplay

class PaymentVerificationListDataCell : UITableViewCell {
    @IBOutlet weak var keyReceiverName: UILabel!
    @IBOutlet weak var scanningTime: UILabel!
    @IBOutlet weak var generateTime: UILabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var gender: UILabel!
    @IBOutlet weak var cashback: UILabel!
    @IBOutlet weak var bonusPoint: UILabel!
    @IBOutlet weak var transactionTime: UILabel!
    @IBOutlet weak var transactionId: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var senderNumber: UILabel!
    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var receiverNumber: UILabel!

    func wrapListData(model: PaymentVerification) {
        self.keyReceiverName.text = model.receiverName.safelyWrappingString()
        self.scanningTime.text = model.scanningTime.safelyWrappingString()
        self.generateTime.text = model.generateTime.safelyWrappingString()
        self.age.text = model.age.safelyWrappingString()
        self.gender.text = model.gender.safelyWrappingString()
        self.cashback.text = model.cashback.safelyWrappingString()
        self.bonusPoint.text = model.bonusPoint.safelyWrappingString()
        self.transactionTime.text = model.transactionType.safelyWrappingString()
        self.transactionId.text = model.transactionId.safelyWrappingString()
        self.amount.text = model.amount.safelyWrappingString()
        self.senderNumber.text = model.senderNumber.safelyWrappingString()
        self.senderName.text = model.senderName.safelyWrappingString()
        self.receiverNumber.text = model.receiverNumber.safelyWrappingString()
    }
    
}

class PaymentVerificationListCell : UITableViewCell {
    
    @IBOutlet weak var keyReceiverName: UILabel!
    @IBOutlet weak var scanningTime: UILabel!
    @IBOutlet weak var generateTime: UILabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var gender: UILabel!
    @IBOutlet weak var cashback: UILabel!
    @IBOutlet weak var bonusPoint: UILabel!
    @IBOutlet weak var transactionTime: UILabel!
    @IBOutlet weak var transactionId: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var senderNumber: UILabel!
    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var receiverNumber: UILabel!
    
}













