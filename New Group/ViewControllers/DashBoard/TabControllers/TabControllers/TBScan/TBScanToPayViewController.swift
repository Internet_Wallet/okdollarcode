//
//  TBScanToPayViewController.swift
//  OK
//
//  Created by Ashish on 2/5/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import PhotosUI
import AVFoundation
import AudioToolbox

protocol TBScanQRDelegate {
    func moveToPayto(withScanObject object: PTScanObject, fromTransfer: Bool?)
    func moveToPayTo(fromTransfer: Bool?)
}

class TBScanToPayViewController: OKBaseController, AVCaptureMetadataOutputObjectsDelegate, QRDelegate, ImagePickerDelegate, ContactPickerDelegate {
    
    @IBOutlet var mobButton: UIButton!
    @IBOutlet weak var flashImage: UIImageView!
    @IBOutlet weak var containerViews: UIView!
    @IBOutlet weak var lblMyGallery: UILabel!{
        didSet{
            lblMyGallery.text = "Scan OK$ QR From Gallery".localized
        }
    }
    @IBOutlet weak var headerLabel: UILabel! {
        didSet{
            headerLabel.font = UIFont(name: appFont, size: appFontSize)
            headerLabel.text = headerLabel.text?.localized
        }
    }
    @IBOutlet weak var closeBtn: UIButton! {
        didSet {
            let image = UIImage(named: "pt_close")?.withRenderingMode(.alwaysTemplate)
            self.closeBtn.setImage(image, for: .normal)
            self.closeBtn.tintColor = .white
        }
    }
    @IBOutlet weak var mobNumLabel: UILabel! {
        didSet {
            self.mobNumLabel.font = UIFont(name: appFont, size: appFontSize)
            self.mobNumLabel.text = mobNumLabel.text?.localized
        }
    }
    @IBOutlet weak var myQRCodeLbl: UILabel! {
        didSet {
            self.myQRCodeLbl.font = UIFont(name: appFont, size: appFontSize)
            self.myQRCodeLbl.text = myQRCodeLbl.text?.localized
        }
    }
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var countryImageView: UIImageView!
    
    @IBOutlet weak var myQRView: UIView! {
        didSet {
            self.myQRView.layer.cornerRadius = 10.0
            self.myQRView.layer.borderWidth = 0.5
            self.myQRView.layer.borderColor = UIColor.white.cgColor
        }
    }
    @IBOutlet weak var reportBtn: UIView!
    @IBOutlet weak var myQRImageView: UIImageView!
    
    var navigation : UINavigationController?
    
    let systemSoundId : SystemSoundID = 1016
    
    var scanDelegate : TBScanQRDelegate?
    
    @IBOutlet weak var addMoneyView: UIView! {
        didSet {
            self.addMoneyView.isHidden = true
        }
    }
    @IBOutlet weak var addMoneyLbl: UILabel! {
        didSet {
            self.addMoneyLbl.text = self.addMoneyLbl.text?.localized
            self.addMoneyLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var addMoneyImgView: UIImageView! {
        didSet {
            self.addMoneyImgView.image = self.addMoneyImgView.image?.withRenderingMode(.alwaysTemplate)
            self.addMoneyImgView.tintColor = UIColor.init(hex: "0321AA")
        }
    }
    
    @IBOutlet weak var transferCloseBtn: UIButton! {
        didSet {
            let image = UIImage(named: "pt_close")?.withRenderingMode(.alwaysTemplate)
            self.transferCloseBtn.setImage(image, for: .normal)
            self.transferCloseBtn.tintColor = .white
            self.transferCloseBtn.isHidden = true
        }
    }
    var isFromTabBar: Bool? = false
    var isFromSideMenu: Bool? = false
    var isFromTransferTo: Bool? = false
    var isTransfer = false
    
    var isAlreadyScanned = false
    
    //captureSession manages capture activity and coordinates between input device and captures outputs
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    
    //Empty Rectangle with green border to outline detected QR or BarCode
    let codeFrame:UIView = {
        
        let codeFrame = UIView()
        codeFrame.layer.borderColor =  UIColor.init(hex: "F3C632").cgColor
        codeFrame.layer.borderWidth = 2.5
        codeFrame.frame = CGRect.zero
        codeFrame.translatesAutoresizingMaskIntoConstraints = false
        return codeFrame
        
    }()
    
     var newScanViewModel = NewScanToPayViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mobButton.isUserInteractionEnabled = false
        if let transferTo = isFromTransferTo, transferTo == true {
            self.transferCloseBtn.isHidden = false
            let imageLoc = UIImage(named: "moreTab")?.withRenderingMode(.alwaysTemplate)
            self.closeBtn.setImage(imageLoc, for: .normal)
            self.closeBtn.tintColor = .white
            self.isTransfer = true
            self.myQRView.isHidden = true
            self.reportBtn.isHidden = true
        } else if let fromtabbar = isFromTabBar, fromtabbar == false {
            self.headerLabel.isHidden = true
            self.closeBtn.isHidden = true
        }
        self.setCountryFlag()
        
        if let transferTo = isFromTransferTo, transferTo == true {
            self.navigationController?.navigationBar.tintColor = .white
            self.navigationController?.navigationBar.barTintColor = kYellowColor
        }
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideAddMoneyView))
        self.view.addGestureRecognizer(tapGesture)
        NotificationCenter.default.addObserver(self, selector: #selector(ScanToPayViewController.appWillResignActive), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc func appWillResignActive() {
        self.flashImage.isHighlighted = false
    }
    
    @objc private func hideAddMoneyView() {
        self.addMoneyView.isHidden = true
    }
    
    private func loadCamera() {
      //  DispatchQueue.global(qos: .background).async {
            let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
            if let captureDevice = captureDevice {
                do {
                    self.captureSession = AVCaptureSession()
                    // CaptureSession needs an input to capture Data from
                    let input = try AVCaptureDeviceInput(device: captureDevice)
                    self.captureSession?.addInput(input)
                    // CaptureSession needs and output to transfer Data to
                    let captureMetadataOutput = AVCaptureMetadataOutput()
                    self.captureSession?.addOutput(captureMetadataOutput)
                    //We tell our Output the expected Meta-data type
                    captureMetadataOutput.setMetadataObjectsDelegate(self , queue: DispatchQueue.main)
                    captureMetadataOutput.metadataObjectTypes = [.code128, .qr,.ean13, .ean8, .code39, .upce, .aztec, .pdf417] //AVMetadataObject.ObjectType
                    self.captureSession?.startRunning()
                    //The videoPreviewLayer displays video in conjunction with the captureSession
                    if let session = self.captureSession {
                        self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
                    }
                    self.videoPreviewLayer?.videoGravity = .resizeAspectFill
                    DispatchQueue.main.async {
                        self.videoPreviewLayer?.frame = self.containerViews.frame
                        if let viewVideo = self.videoPreviewLayer {
                            self.view.layer.addSublayer(viewVideo)
                        }
                        self.view.bringSubviewToFront(self.containerViews)
                        //self.view.bringSubviewToFront(self.buttonContainerView)
                    }
                }
                catch {
                    println_debug("Error")
                    DispatchQueue.main.async {
                        self.removeActivityIndicator(view: self.view)
                    }
                }
            }
            DispatchQueue.main.async {
                self.removeActivityIndicator(view: self.view)
            }
      //  }
    }
    
    private func setCountryFlag() {
        let imageName = identifyCountryName(countryCode: UserModel.shared.countryCode).lowercased()
        self.countryCodeLabel.text = "(" + UserModel.shared.countryCode + ")"
        countryImageView.image = UIImage(named: imageName)
        self.myQRImageView.image = self.myQRImageView.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        self.myQRImageView.tintColor = .white
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillLayoutSubviews() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            self.mobButton.isUserInteractionEnabled = true
        })
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("View will Appear")
        isAlreadyScanned = false
        super.viewWillAppear(animated)
        self.addMoneyView.isHidden = true
        if let fromtabbar = isFromTabBar, fromtabbar == true {
            self.navigationController?.isNavigationBarHidden = true
        }
        if let transferTo = isFromTransferTo, transferTo == true {
            self.navigationController?.isNavigationBarHidden = true
        }
        if AVCaptureDevice.authorizationStatus(for: .video) == AVAuthorizationStatus.authorized {
            self.loadCamera()
            self.flashImage.isHighlighted = false
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted) in
                switch granted {
                case true:
                    self.loadCamera()
                case false:
                    self.requestAlertForPermissions(withBody: "OK$ need camera access to scan".localized , handler: { (cancelled) in
                        
                    })
                }
            })
        }
        
        appDel.floatingButtonControl?.window.isHidden = true
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
    }
    
    //MARK:- Delegate QR
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if metadataObjects.count == 0 {
            println_debug("no objects returned")
            return
        }
        
        let metaDataObject = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        guard let StringCodeValue = metaDataObject.stringValue else {
            return
        }
        
        view.addSubview(codeFrame)
        
        guard let metaDataCoordinates = videoPreviewLayer?.transformedMetadataObject(for: metaDataObject) else {
            return
        }
        
        codeFrame.frame = metaDataCoordinates.bounds
        let feedbackGenerator = UIImpactFeedbackGenerator(style: .heavy)
        feedbackGenerator.impactOccurred()
        //        if self.captureSession.isRunning {
        //            self.captureSession.stopRunning()
        //        }
        println_debug(StringCodeValue)
        scannedString(str: StringCodeValue)
    }
    
    
    func scannedString(str: String) {
      
        if str.contains(find: "EncryptedString") &&  str.contains(find: "Ivector") && str.contains(find: "MerchantId"){
            let DataStringToJSON : [String : Any] = self.convertStringToJson(ReceivedString: str) as? [String : Any] ?? ["":""]
            if !isAlreadyScanned{
                isAlreadyScanned = true
               getQRScanDetail(encrypted: DataStringToJSON["EncryptedString"] as? String ?? "", iVector: DataStringToJSON["Ivector"] as? String ?? "", merchantID: DataStringToJSON["MerchantId"] as? String ?? "")
            }
            
            
        } else if  str.contains(find: "V5POS") {
            let mitQRStrArray = str.components(separatedBy: "|")
            let scanObj  = PTScanObject.init(amt: (mitQRStrArray.count > 4) ? mitQRStrArray[4] : "", nme: "", agent: mitQRStrArray.first ?? "", remark: "V5POS", cashInQR: false, referenceNumber: (mitQRStrArray.count > 2) ? mitQRStrArray[2] : "")

            if let delegate = self.scanDelegate {
                delegate.moveToPayto(withScanObject: scanObj, fromTransfer: (self.isTransfer == true) ? true : false)
            }
            
            DispatchQueue.main.async {
                if self.captureSession?.isRunning ?? false {
                    self.captureSession?.stopRunning()
                }
            }
            codeFrame.removeFromSuperview()
            progressViewObj.removeProgressView()
        } else {
            //Report scan QR Code
            _ = AESCrypt.decrypt(str.components(separatedBy: "---")[0], password: "m2n1shlko@$p##d")
            
            if let _ = str.components(separatedBy: "---")[safe: 1] {
                if let decryp = AESCrypt.decrypt(str.components(separatedBy: "---")[0], password: "m2n1shlko@$p##d") {
                    if let str1 = AESCrypt.decrypt(str.components(separatedBy: "---")[1], password: decryp)  {
                        if str1.hasPrefix("OK") {
                            let model = VerifyPaymentModel.init()
                            model.wrapModel(processedString: str1)
                            
                            if let dest = model.destination {
                                if dest != UserModel.shared.mobileNo {
                                    //                                self.showErrorAlert(errMessage: "Please scan correct QR code".localized)
                                    DispatchQueue.main.async {
                                        if self.captureSession?.isRunning ?? false {
                                            self.captureSession?.stopRunning()
                                        }
                                        alertViewObj.wrapAlert(title: nil, body: "Please scan correct QR code".localized, img: #imageLiteral(resourceName: "alert-icon"))
                                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                            self.captureSession?.startRunning()
                                        })
                                        alertViewObj.showAlert(controller: self)
                                    }
                                    return
                                }
                            }
                            
                            if PaymentVerificationDBManager().verifyExistingDataFromDB(transactionID: model.transactionId ?? "") {
                                alertViewObj.wrapAlert(title: nil, body: "Already exist!".localized, img: #imageLiteral(resourceName: "pattern"))
                                
                                alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                                    
                                }
                                alertViewObj.showAlert()
                                return
                            } else {
                                PaymentVerificationDBManager().loadScannedPaymentToDatabase(model: model)
                                let story = UIStoryboard(name: "Main", bundle: nil)
                                guard let scanResultViewController = story.instantiateViewController(withIdentifier: "QRScanResultViewController") as? QRScanResultViewController else { return }
                                scanResultViewController.delegate = self
                                scanResultViewController.data = model
                                scanResultViewController.modalPresentationStyle = .overCurrentContext
                                if self.captureSession?.isRunning ?? false {
                                    self.captureSession?.stopRunning()
                                }
                                if let navigation = self.navigationController {
                                    navigation.present(scanResultViewController, animated: true, completion: nil)
                                } else {
                                    self.navigation?.present(scanResultViewController, animated: true, completion: nil)
                                }
                                return
                            }
                            
                        }
                    }
                }
            }
            //SCan QR Code for PayTo
            var sttt: String = ""
            println_debug(AESCrypt.decrypt(str, password: "m2n1shlko@$p##d"))
            if let stringQR = AESCrypt.decrypt(str, password: "m2n1shlko@$p##d") {
                sttt = stringQR
            } else {
                DispatchQueue.main.async {
                    if self.captureSession?.isRunning ?? false {
                        self.captureSession?.stopRunning()
                    }
                    alertViewObj.wrapAlert(title: nil, body: "Invalid QR Code".localized, img: #imageLiteral(resourceName: "alert-icon"))
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        self.captureSession?.startRunning()
                    })
                    alertViewObj.showAlert(controller: self)
                }
            }
           
            /*
            if sttt != "" {
                
                if sttt.contains("α") {
                    if sttt.contains("-1") {
                        sttt = ""
                    }
                    else {
                    }
                    if sttt.contains("`") {
                    }
                    else {
                        // sttt=@"";
                    }
                    println_debug("contains yes")
                }
                else {
                    sttt = ""
                }
                
            }
            */
            
            if(sttt.count>0) {
                
                //            var cellId: String = ""
                var businessName: String = ""
                var businessMobile: String = ""
                
                let myArray: [Any] = sttt.components(separatedBy: "#")
                _ = myArray[0] as! String
                
                let seconArray: [Any] = (myArray[1] as AnyObject).components(separatedBy: "-")
                businessName = seconArray[0] as! String
                
                let thirdArray: [Any] = (seconArray[1] as AnyObject).components(separatedBy: "@")
                businessMobile = thirdArray[0] as! String
                
                let fourthArray: [Any] = (thirdArray[1] as AnyObject).components(separatedBy: "&")
                let amt : String = fourthArray[0] as! String
                var remrk = ""
                
                let fifthArray: [String] = (fourthArray[1] as AnyObject).components(separatedBy: "`")
                if fifthArray.indices.contains(1) {
                    let sixthArray:[String] = (fifthArray[1] as AnyObject).components(separatedBy: ",")
                    if sixthArray.count > 0 {
                        let scanPromotionObj = ["status" :true, "beforePromotion" : fifthArray[1], "afterPromotion": fifthArray[1].components(separatedBy: ",")[0]] as [String : Any]
                        UserDefaults.standard.set(scanPromotionObj, forKey: "ScanQRPromotion")
                        if sixthArray.indices.contains(2) {
                            remrk = sixthArray[2]
                        }
                    }
                }
                var cashInQR = false
                //            if let isfromTrf = isFromTransferTo, isfromTrf == true {
                //                if !sttt.contains(find: "%CashInQR%") {
                //                    DispatchQueue.main.async {
                //                        alertViewObj.wrapAlert(title: nil, body: "QR code is not matching with Agent Number.".localized, img: #imageLiteral(resourceName: "alert-icon"))
                //                        alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                //                            self.captureSession?.startRunning()
                //                        })
                //                        alertViewObj.showAlert(controller: self)
                //                    }
                //                    self.captureSession?.stopRunning()
                //                    return
                //                }
                //            }
                if sttt.contains(find: "%CashInQR%") {
                    let destArray: [Any] = (myArray[1] as AnyObject).components(separatedBy: ">")
                    var destNum : String = destArray[1] as! String
                    destNum = destNum.replacingOccurrences(of: "%CashInQR%", with: "")
                    if UserModel.shared.mobileNo != destNum {
                        DispatchQueue.main.async {
                            alertViewObj.wrapAlert(title: nil, body: "QR code is not matching with Agent Number".localized, img: #imageLiteral(resourceName: "alert-icon"))
                            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                                self.captureSession?.startRunning()
                            })
                            alertViewObj.showAlert(controller: self)
                        }
                        self.captureSession?.stopRunning()
                        return
                    } else {
                        let remrkArr = remrk.components(separatedBy: "!")
                        remrk = remrkArr.first ?? ""
                        cashInQR = true
                    }
                }
                let scanObj  = PTScanObject.init(amt: amt, nme: businessName, agent: businessMobile, remark: remrk, cashInQR: cashInQR)
                
                if let delegate = self.scanDelegate {
                    delegate.moveToPayto(withScanObject: scanObj, fromTransfer: (self.isTransfer == true) ? true : false)
                }
                
                
            } else {
                self.captureSession?.startRunning()
                progressViewObj.removeProgressView()
                return
            }
            
            if self.captureSession?.isRunning ?? false {
                self.captureSession?.stopRunning()
            }
            
            codeFrame.removeFromSuperview()
            progressViewObj.removeProgressView()
        }
    }
    
    func refreshControler() {
        DispatchQueue.main.async {
            self.okActivityIndicator(view: self.view)
            self.captureSession?.startRunning()
            self.removeActivityIndicator(view: self.view)
        }
    }
    
    override func viewDidLayoutSubviews() {
        print("Called Layout")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.okActivityIndicator(view: self.view)
            if self.captureSession?.isRunning ?? false {
                self.captureSession?.stopRunning()
            }
            self.removeActivityIndicator(view: self.view)
        }
        if let fromtabbar = isFromTabBar, fromtabbar == true {
            self.navigationController?.isNavigationBarHidden = false
        }
        if let transferTo = isFromTransferTo, transferTo == true {
            self.navigationController?.isNavigationBarHidden = false
        }
    }
    
    //MARK:- IBActions
    
    @IBAction func openGalleryAction(_ sender: UIButton) {
        let story = UIStoryboard.init(name: "PayTo", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "QRPhotoScanner") as? QRPhotoScanner
        vc?.tbScanQRController = self
        vc?.delegate = self
        vc?.isFromTransferTo = isFromTransferTo ?? false
        if let navigation = self.navigationController {
            navigation.pushViewController(vc!, animated: true)
        } else {
            self.navigation?.pushViewController(vc!, animated: true)
        }
    }
    
    func startCamera() {
        self.refreshControler()
    }
    
    @IBAction func mobNumAction(_ sender: UIButton) {
    print("tushar")
        if self.captureSession?.isRunning ?? false {
            self.captureSession?.stopRunning()
        }
        
        codeFrame.removeFromSuperview()
        progressViewObj.removeProgressView()
        if let delegate = self.scanDelegate {
            delegate.moveToPayTo(fromTransfer: (self.isTransfer == true) ? true : false)
        }
    }
    
    @IBAction func transferCloseAction(_ sender: UIButton) {
        DispatchQueue.main.async {
            if self.captureSession?.isRunning ?? false {
                self.captureSession?.stopRunning()
            }
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func navigateToAddMoneyAction(_ sender : UIButton) {
//        let addMoneyStoryboard = UIStoryboard.init(name: "AddWithdraw", bundle: nil)
//        guard let firstView_addMoneyVC = addMoneyStoryboard.instantiateViewController(withIdentifier: "AddWithdrawMoneyVC") as? AddWithdrawMoneyVC else { return }
//        firstView_addMoneyVC.screenFrom.0 = "DashBoard"
//        firstView_addMoneyVC.screenFrom.1 = "Cash In"
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cancelAction(_ sender: UIButton) {
        DispatchQueue.main.async {
            if self.captureSession?.isRunning ?? false {
                self.captureSession?.stopRunning()
            }
        }
        if let transferTo = isFromTransferTo, transferTo == true {
            DispatchQueue.main.async {
                self.captureSession?.startRunning()
            }
            if self.addMoneyView.isHidden {
                self.addMoneyView.isHidden = false
            } else {
                self.addMoneyView.isHidden = true
            }
        } else if let fromSideMenu = isFromSideMenu, fromSideMenu == true {
            self.navigationController?.popViewController(animated: true)
        } else {
            let transition = CATransition()
            transition.duration = 0.5
            //            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            transition.type = CATransitionType.reveal
            transition.subtype = CATransitionSubtype.fromBottom
            self.navigationController?.view.layer.add(transition, forKey: nil)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    @IBAction func reportAction(_ sender: UIButton) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        guard let sanResultDetailViewController = story.instantiateViewController(withIdentifier: "QRScanResultDetailViewController") as? QRScanResultDetailViewController else { return }
        self.navigation?.pushViewController(sanResultDetailViewController, animated: true)
    }
    
    @IBAction func openFlashlight(_ sender: UIButton) {
        if let device = AVCaptureDevice.default(for: AVMediaType.video) {
            if (device.hasTorch) {
                do {
                    try device.lockForConfiguration()
                    if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                        self.flashImage.isHighlighted = false
                        device.torchMode = AVCaptureDevice.TorchMode.off
                    } else {
                        self.flashImage.isHighlighted = true
                        do {
                            try device.setTorchModeOn(level: 1.0)
                        } catch {
                            println_debug(error)
                        }
                    }
                    device.unlockForConfiguration()
                } catch {
                    println_debug(error)
                }
            }
        }
    }
    
    @IBAction func myQRCodeAction(_ sender: UIButton) {
        if let fromSideMenu = isFromSideMenu, fromSideMenu == true {
            self.navigationController?.popViewController(animated: true)
        } else {
            let story = UIStoryboard(name: "Main", bundle: nil)
            guard let myQRCode = story.instantiateViewController(withIdentifier: String.init(describing:TBMyQRCode.self)) as? TBMyQRCode else { return }
            
            myQRCode.navigation = self.navigationController
            //            myQRCode?.view.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: 548.00)
            myQRCode.title = "My QR Code".localized
            myQRCode.itemNavigation = self.navigationItem
            myQRCode.fromTabBar = true
            self.navigationController?.pushViewController(myQRCode, animated: true)
        }
    }
    
    @IBAction func openContactAction(_ sender: UIButton) {
        let nav = UitilityClass.openContact(multiSelection: false, self, isComingFromPayTo: false, isComingFromOtherNumberTopUp: false )
        if let transferTo = isFromTransferTo, transferTo == true {
            self.navigationController?.present(nav, animated: true, completion: nil)
        } else {
            self.navigation?.present(nav, animated: true, completion: nil)
        }
    }
    
    //MARK:- Contact Delegates    
    func contact(_: ContactPickersPicker, didSelectContact contact: ContactPicker) {
        guard let numbers = contact.phoneNumbers.first else {
            return
        }
        let scanObj  = PTScanObject.init(amt: "", nme: contact.displayName(), agent: numbers.phoneNumber, remark: "")
        
        if let delegate = self.scanDelegate {
            delegate.moveToPayto(withScanObject: scanObj, fromTransfer: (self.isTransfer == true) ? true : false)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
    }
}


extension TBScanToPayViewController{
    //This function will details of the user after scanning the QR
       fileprivate func getQRScanDetail(encrypted: String,iVector: String,merchantID: String) {
           if appDelegate.checkNetworkAvail() {
               progressViewObj.showProgressView()
               newScanViewModel.getDataForQRScan(merchantQr: encrypted,iVector: iVector,merchantID: merchantID,finished:{
                   progressViewObj.removeProgressView()
                   //checking if data is available
                   if let obj = self.newScanViewModel.qrScanObj{
                       DispatchQueue.main.async { [weak self] in
                           if obj.code ?? 0 == 200{
                               DispatchQueue.main.async {
                                   self?.navigate()
                               }
                           }else{
                            alertViewObj.wrapAlert(title: "", body: obj.message ?? "", img: nil)
                                  alertViewObj.addAction(title: "YES".localized, style: .target , action: {
                                    self?.isAlreadyScanned = false
                                      self?.loadCamera()
                                  })
                                  alertViewObj.showAlert(controller: self)
                           }
                       }
                   }else{
                    DispatchQueue.main.async {
                        self.isAlreadyScanned = false
                        self.loadCamera()
                    }
                   }
               })
           } else {
              self.isAlreadyScanned = false
               self.noInternetAlert()
               progressViewObj.removeProgressView()
           }
       }
    
    
    func navigate(){
        let viewController = UIStoryboard(name: "NewScanToPay", bundle: Bundle.main).instantiateViewController(withIdentifier: NewScanToPayHelper.kNewScanQRInfoViewController) as? NewScanQRInfoViewController
        if let isValid = viewController{
            if let objNew = self.newScanViewModel.qrScanObj{
                isValid.qrScanObj = objNew
                navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
                navigationController?.pushViewController(isValid, animated: true)
                DispatchQueue.main.async {
                    if self.captureSession?.isRunning ?? false {
                        self.captureSession?.stopRunning()
                    }
                }
            }
        }
    }
    
    
    func convertStringToJson(ReceivedString : String) -> Any? {
         let CustomStringData = ReceivedString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion:true)
         do {
            if let isValid = CustomStringData{
                let jsonData = try? JSONSerialization.jsonObject(with: isValid, options: .allowFragments ) as Any
                return jsonData ?? nil
            }else{
                return nil
            }
            
         }
     }
    
}
