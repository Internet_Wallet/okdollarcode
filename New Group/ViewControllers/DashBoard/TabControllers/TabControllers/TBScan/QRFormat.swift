//
//  QRFormat.swift
//  OK
//
//  Created by Shobhit Singhal on 7/26/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class QRFormat: OKBaseController {
    @IBOutlet weak var qrFormatTableView : UITableView!
    @IBOutlet weak var qrPreviewBtn : UIButton!{
        didSet{
            self.qrPreviewBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.qrPreviewBtn.setTitle(self.qrPreviewBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    var QRFormatArr = ["A6","Color","en","Not_DisplayImage"]
    var qrImage:UIImage!
    var qrStr:String = ""

    let sectionHeader = ["","Select Display Stand Size","Select Color Format","Select Language"]
    var displaySize = 6
    var designFormate = [["row_Title": "Color", "is_Select": true], ["row_Title": "Black & White","is_Select": false]]
    var selectLanguage = [["row_Title": "English   (အဂၤလိပ္)", "selected_field": "en", "is_Select": true], ["row_Title": "ျမန္မာ   ( ေဇာ္ဂ်ီ)", "selected_field": "my", "is_Select": false],["row_Title": "ျမန္မာ   (ယူနီကုဒ္)", "selected_field": "uni", "is_Select": false],["row_Title": "Chinese   (တရုတ္)", "selected_field": "zh-Hans", "is_Select": false], ["row_Title": "Thai   (ထိုင္း)", "selected_field": "th", "is_Select": false]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)]
        self.title = "QR Format".localized
    }

    @IBAction func dismissControllerAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func qrPreviewAction(_ sender: UIButton) {
        if UserModel.shared.agentType == .user {
            self.performSegue(withIdentifier: "PrintPersonalQR", sender: self)
        } else {
            self.performSegue(withIdentifier: "QRPreviewToPrintViewBusiness", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "PrintPersonalQR" {
            let controller = segue.destination as! PrintPersonalQRCode
            controller.QRFormatArr = self.QRFormatArr
            controller.qrStr = self.qrStr
        }else if segue.identifier == "QRPreviewToPrintViewBusiness" {
            let controller = segue.destination as! QRCodePreviewBusiness
            //controller.isComingFromQRFormat = true
            controller.QRFormatArr = self.QRFormatArr
            controller.qrStr = self.qrStr
        }
    }
}

extension QRFormat : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 || section == 1 || section == 4 {
            return 1
        }else if section == 2 {
            return designFormate.count
        }else if section == 3 {
            return selectLanguage.count
        }else {
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 || section == 4 {
            return 0
        }else {
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 40))
        headerView.backgroundColor = UIColor.colorWithRedValue(redValue: 242, greenValue: 242, blueValue: 244, alpha: 1)
        let headerLabel = UILabel(frame: CGRect(x: 12, y: 5, width: screenWidth-40, height: 30))
        headerLabel.font = UIFont.init(name: appFont, size: 18)
        headerLabel.text = sectionHeader[section].localized
        
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = qrFormatTableView.dequeueReusableCell(withIdentifier: "ProfilePicQRCell") as! ProfilePicQRCell
            if self.QRFormatArr[3] == "Not_DisplayImage" {
                cell.imgProfileImage.image = UIImage(named: "propic")
                cell.imgView.image = UIImage(named: "r_Unradio")
            }else {
                let url1 = URL(string: UserModel.shared.proPic.replacingOccurrences(of: " ", with: "%20"))
                cell.imgProfileImage.sd_setImage(with: url1, placeholderImage: UIImage(named: "propic"))
                cell.imgView.image = UIImage(named: "check_radio")
            }
            cell.btnAction.addTargetClosure(closure: {(_ action) in
                if self.QRFormatArr[3] == "Not_DisplayImage" {
                    self.QRFormatArr[3] = "DisplayImage"
                }else {
                    self.QRFormatArr[3] = "Not_DisplayImage"
                }
                self.qrFormatTableView.reloadSections([indexPath.section], with: .none)
            })
            return cell
        }else if indexPath.section == 1 {
            let cell = qrFormatTableView.dequeueReusableCell(withIdentifier: "SelectDisplayQRCell") as! SelectDisplayQRCell
            cell.btnSix.addTargetClosure(closure: {(_ action) in
                cell.imgASix.image = UIImage(named: "actradio_btn")
                cell.imgAFive.image = UIImage(named: "r_radio_btn")
                cell.imgAFour.image = UIImage(named: "r_radio_btn")
                cell.imgSize.image = UIImage(named: "a6")
                cell.lblASix.textColor = UIColor.init(red: 28.8 / 255.0, green: 32.0 / 255.0, blue: 135.0 / 255.0, alpha: 1)
                cell.lblAFive.textColor = UIColor.black
                cell.lblAFour.textColor = UIColor.black
                self.QRFormatArr[0] = "A6"
                self.displaySize = 6
            })
            cell.btnFive.addTargetClosure(closure: {(_ action) in
                cell.imgASix.image = UIImage(named: "r_radio_btn")
                cell.imgAFive.image = UIImage(named: "actradio_btn")
                cell.imgAFour.image = UIImage(named: "r_radio_btn")
                cell.imgSize.image = UIImage(named: "a5")
                cell.lblASix.textColor = UIColor.black
                cell.lblAFive.textColor = UIColor.init(red: 28.8 / 255.0, green: 32.0 / 255.0, blue: 135.0 / 255.0, alpha: 1)
                cell.lblAFour.textColor = UIColor.black
                self.QRFormatArr[0] = "A5"
                self.displaySize = 5
            })
            
            cell.btnFour.addTargetClosure(closure: {(_ action) in
                cell.imgASix.image = UIImage(named: "r_radio_btn")
                cell.imgAFive.image = UIImage(named: "r_radio_btn")
                cell.imgAFour.image = UIImage(named: "actradio_btn")
                cell.imgSize.image = UIImage(named: "a4")
                cell.lblASix.textColor = UIColor.black
                cell.lblAFive.textColor = UIColor.black
                cell.lblAFour.textColor = UIColor.init(red: 28.8 / 255.0, green: 32.0 / 255.0, blue: 135.0 / 255.0, alpha: 1)
                self.QRFormatArr[0] = "A4"
                self.displaySize = 4
            })
            if displaySize == 6 {
                cell.imgASix.image = UIImage(named: "actradio_btn")
                cell.imgAFive.image = UIImage(named: "r_radio_btn")
                cell.imgAFour.image = UIImage(named: "r_radio_btn")
                cell.imgSize.image = UIImage(named: "a6")
                self.QRFormatArr[0] = "A6"
            }else if displaySize == 5 {
                cell.imgASix.image = UIImage(named: "r_radio_btn")
                cell.imgAFive.image = UIImage(named: "actradio_btn")
                cell.imgAFour.image = UIImage(named: "r_radio_btn")
                cell.imgSize.image = UIImage(named: "a5")
                self.QRFormatArr[0] = "A5"
            }else {
                cell.imgASix.image = UIImage(named: "r_radio_btn")
                cell.imgAFive.image = UIImage(named: "r_radio_btn")
                cell.imgAFour.image = UIImage(named: "actradio_btn")
                cell.imgSize.image = UIImage(named: "a4")
                self.QRFormatArr[0] = "A4"
            }
            return cell
        }else if indexPath.section == 2 {
            let cell = qrFormatTableView.dequeueReusableCell(withIdentifier: "QRFormatCell") as! QRFormatTableViewCell
            let dic = designFormate[indexPath.row]
            if indexPath.row == 0 {
                var myMutableString = NSMutableAttributedString()
                myMutableString = NSMutableAttributedString(string: "Color".localized as String, attributes: [NSAttributedString.Key.font:  UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20)])
                myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: NSRange(location:0,length:1))
                myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.blue, range: NSRange(location:1,length:1))
                myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.orange, range: NSRange(location:2,length:1))
                if myMutableString.hasRange(NSRange(location:3,length:1)) {
                    myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.magenta, range: NSRange(location:3,length:1))
                }
                if myMutableString.hasRange(NSRange(location:4,length:1)) {
                    myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.blue, range: NSRange(location:4,length:1))
                }
                if myMutableString.hasRange(NSRange(location:5,length:1)) {
                    myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: NSRange(location:5,length:1))
                }
                if myMutableString.hasRange(NSRange(location:6,length:1)) {
                    myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.green, range: NSRange(location:6,length:1))
                }
                cell.formatTitleLabel.attributedText = myMutableString
            }else {
                let str = dic["row_Title"] as? String
                
                
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "en"{
                    cell.formatTitleLabel.text = str?.localized
                }else{
                    cell.formatTitleLabel.font = UIFont(name: appFont, size: appTitleButton)
                    cell.formatTitleLabel.text = "အဖြူ နှင့် အမဲ "
                }
                
                
            }
            
            if dic["is_Select"] as? Bool ?? true {
                cell.imgView.image = UIImage(named: "actradio_btn")
            }else {
                cell.imgView.image = UIImage(named: "r_radio_btn")
            }
            
            cell.btnAction.tag = indexPath.row
            cell.btnAction.addTargetClosure(closure: {(_ action) in
                if cell.btnAction.tag == 0 {
                    self.designFormate[0] = ["row_Title": "Color", "is_Select": true]
                    self.designFormate[1] = ["row_Title": "Black & White","is_Select": false]
                    self.QRFormatArr[1] = "Color"
                }else {
                    self.designFormate[0] = ["row_Title": "Color", "is_Select": false]
                    self.designFormate[1] = ["row_Title": "Black & White","is_Select": true]
                    self.QRFormatArr[1] = "Black & White"
                }
                self.qrFormatTableView.reloadSections([indexPath.section], with: .none)
            })
            return cell
        }else if indexPath.section == 3 {
            let cell = qrFormatTableView.dequeueReusableCell(withIdentifier: "QRFormatCell") as! QRFormatTableViewCell
            let dic = selectLanguage[indexPath.row]
            cell.formatTitleLabel.text = dic["row_Title"] as? String
            if dic["is_Select"] as? Bool ?? true {
                cell.imgView.image = UIImage(named: "actradio_btn")
                self.QRFormatArr[2] = dic["selected_field"] as? String ?? "en"
            }else {
                cell.imgView.image = UIImage(named: "r_radio_btn")
            }
            
            cell.btnAction.tag = indexPath.row
            cell.btnAction.addTargetClosure(closure: {(_ action) in
                if cell.btnAction.tag == 0 {
                    self.selectLanguage[0] = ["row_Title": "English   (အဂၤလိပ္)", "selected_field": "en", "is_Select": true]
                    self.selectLanguage[1] = ["row_Title": "ျမန္မာ   ( ေဇာ္ဂ်ီ)", "selected_field": "my", "is_Select": false]
                    self.selectLanguage[2] = ["row_Title": "ျမန္မာ   (ယူနီကုဒ္)", "selected_field": "uni", "is_Select": false]
                    self.selectLanguage[3] = ["row_Title": "Chinese   (တရုတ္)", "selected_field": "zh-Hans", "is_Select": false]
                    self.selectLanguage[4] = ["row_Title": "Thai   (ထိုင္း)", "selected_field": "th", "is_Select": false]
                    self.QRFormatArr[2] = "en"
                }else if cell.btnAction.tag == 1 {
                    self.selectLanguage[0] = ["row_Title": "English   (အဂၤလိပ္)", "selected_field": "en", "is_Select": false]
                    self.selectLanguage[1] = ["row_Title": "ျမန္မာ   ( ေဇာ္ဂ်ီ)", "selected_field": "my", "is_Select": true]
                    self.selectLanguage[2] = ["row_Title": "ျမန္မာ   (ယူနီကုဒ္)", "selected_field": "uni", "is_Select": false]
                    self.selectLanguage[3] = ["row_Title": "Chinese   (တရုတ္)", "selected_field": "zh-Hans", "is_Select": false]
                    self.selectLanguage[4] = ["row_Title": "Thai   (ထိုင္း)", "selected_field": "th", "is_Select": false]
                     self.QRFormatArr[2] = "my"
                }else if cell.btnAction.tag == 2 {
                    self.selectLanguage[0] = ["row_Title": "English   (အဂၤလိပ္)", "selected_field": "en", "is_Select": false]
                    self.selectLanguage[1] = ["row_Title": "ျမန္မာ   ( ေဇာ္ဂ်ီ)", "selected_field": "my", "is_Select": false]
                    self.selectLanguage[2] = ["row_Title": "ျမန္မာ   (ယူနီကုဒ္)", "selected_field": "uni", "is_Select": true]
                    self.selectLanguage[3] = ["row_Title": "Chinese   (တရုတ္)", "selected_field": "zh-Hans", "is_Select": false]
                    self.selectLanguage[4] = ["row_Title": "Thai   (ထိုင္း)", "selected_field": "th", "is_Select": false]
                     self.QRFormatArr[2] = "zh-Hans"
                }else if cell.btnAction.tag == 3 {
                    self.selectLanguage[0] = ["row_Title": "English   (အဂၤလိပ္)", "selected_field": "en", "is_Select": false]
                    self.selectLanguage[1] = ["row_Title": "ျမန္မာ   ( ေဇာ္ဂ်ီ)", "selected_field": "my", "is_Select": false]
                    self.selectLanguage[2] = ["row_Title": "ျမန္မာ   (ယူနီကုဒ္)", "selected_field": "uni", "is_Select": false]
                    self.selectLanguage[3] = ["row_Title": "Chinese   (တရုတ္)", "selected_field": "zh-Hans", "is_Select": true]
                    self.selectLanguage[4] = ["row_Title": "Thai   (ထိုင္း)", "selected_field": "th", "is_Select": false]
                    self.QRFormatArr[2] = "th"
                }else if cell.btnAction.tag == 4 {
                    self.selectLanguage[0] = ["row_Title": "English   (အဂၤလိပ္)", "selected_field": "en", "is_Select": false]
                    self.selectLanguage[1] = ["row_Title": "ျမန္မာ   ( ေဇာ္ဂ်ီ)", "selected_field": "my", "is_Select": false]
                    self.selectLanguage[2] = ["row_Title": "ျမန္မာ   (ယူနီကုဒ္)", "selected_field": "uni", "is_Select": false]
                    self.selectLanguage[3] = ["row_Title": "Chinese   (တရုတ္)", "selected_field": "zh-Hans", "is_Select": false]
                    self.selectLanguage[4] = ["row_Title": "Thai   (ထိုင္း)", "selected_field": "th", "is_Select": true]
                     self.QRFormatArr[2] = "uni"
                }
                self.qrFormatTableView.reloadSections([indexPath.section], with: .none)
            })
            return cell
        }else if indexPath.section == 4 {
            let cell = qrFormatTableView.dequeueReusableCell(withIdentifier: "NoteQRCell") as! NoteQRCell
            return cell
        }else {
            let cell = qrFormatTableView.dequeueReusableCell(withIdentifier: "QRFormatCell") as! QRFormatTableViewCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if UserModel.shared.agentType == .user {
                return 70
            }else {
                return 0
            }
        }else if indexPath.section == 1 {
            return 156
        }else if indexPath.section == 2 || indexPath.section == 3{
            return 52
        }else {
            if UserModel.shared.agentType == .user {
                return 0
            }else {
                return 160
            }
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
}



class ProfilePicQRCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblWithProfileImage: UILabel!
    @IBOutlet weak var imgProfileImage: UIImageView!
    @IBOutlet weak var btnAction: UIButton!
    
    override func awakeFromNib() {
        lblWithProfileImage.text = "With my profile picture"
        imgProfileImage.layer.cornerRadius = 25
        imgProfileImage.layer.masksToBounds = true
        imgProfileImage.layer.borderColor = #colorLiteral(red: 0.1112622418, green: 0.1259785195, blue: 0.5283152526, alpha: 1)
        imgProfileImage.layer.borderWidth = 1.0
    }
}

class SelectDisplayQRCell: UITableViewCell {
    @IBOutlet weak var imgASix: UIImageView!
    @IBOutlet weak var imgAFive: UIImageView!
    @IBOutlet weak var imgAFour: UIImageView!
    @IBOutlet weak var imgSize: UIImageView!
    @IBOutlet weak var lblASix: UILabel!
    @IBOutlet weak var lblAFive: UILabel!
    @IBOutlet weak var lblAFour: UILabel!
    @IBOutlet weak var btnSix: UIButton!
    @IBOutlet weak var btnFive: UIButton!
    @IBOutlet weak var btnFour: UIButton!
    override func awakeFromNib() {
        lblASix.text = "(4.1 x 5.8 in)  (105 x 148 mm)  A6"
        lblAFive.text = "(5.8 x 8.3 in)  (148 x 210 mm)  A5"
        lblAFour.text = "(8.3 x 11.7 in)  (210 x 297 mm)  A4"
        imgSize.image = UIImage(named: "a6")
        lblASix.textColor = UIColor.init(red: 28.8 / 255.0, green: 32.0 / 255.0, blue: 135.0 / 255.0, alpha: 1)
    }
}

class NoteQRCell: UITableViewCell {
    @IBOutlet weak var lblNoteTitle: UILabel!{
        didSet{
            self.lblNoteTitle.font = UIFont(name: appFont, size: appTitleButton)
        }
    }
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblNote: UILabel!{
        didSet{
            self.lblNote.font = UIFont(name: appFont, size: appFontSize)
        }
    }
   
    
    override func awakeFromNib() {
        bgView.layer.cornerRadius = 5
        bgView.layer.masksToBounds = true
        lblNoteTitle.text = "Note".localized
        
        lblNote.text = "Business & Branch Outlet name will appear in the OK$ account registered language.".localized
    }
}


extension NSMutableAttributedString {
    func hasRange(_ range: NSRange) -> Bool {
        return Range(range, in: self.string) != nil
    }
}
