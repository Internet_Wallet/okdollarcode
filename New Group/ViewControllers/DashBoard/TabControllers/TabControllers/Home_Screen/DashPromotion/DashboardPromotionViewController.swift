//
//  DashboardPromotionViewController.swift
//  OK
//
//  Created by Kethan Kumar on 14/02/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import UIKit

class DashboardPromotionViewController: OKBaseController {
    
    @IBOutlet weak var promoImageView: UIImageView!
    @IBOutlet weak var closeBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let imageUrl = kycAppControlInfo.first?.imageUrl else { return }
        self.promoImageView.downloadedFrom(link: imageUrl, contentMode: .scaleAspectFill)
        var count = UserDefaults.standard.integer(forKey: "PromotionShowInDashCount")
        count += 1
        UserDefaults.standard.set(count, forKey: "PromotionShowInDashCount")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.transform = CGAffineTransform(scaleX: 0.00001, y: 0.00001)
        UIView.animate(withDuration: 0.4, animations: { [weak self] in
            self?.view.transform = CGAffineTransform.identity
        })
    }
    
    @IBAction func closeBtnAcvtion(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }

}
