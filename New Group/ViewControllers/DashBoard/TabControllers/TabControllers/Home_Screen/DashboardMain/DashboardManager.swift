//
//  DashboardManager.swift
//  OK
//
//  Created by Ashish on 12/24/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SDWebImage

struct DashboardCellIdentifier {
    
   static  let okDollarIdentifier    = ""
   static  let cashInOut             = ""
   static  let rechargeBillPayment   = ""
   static  let loyaltyPromotion      = ""
   static  let bookOKDollar          = ""
    
}

enum DashboardCellType {
    
    case okDollar, cashInOut, rechargeBillPayment, loyalityPromotion, bookOKDollar
    
}

class DashboardManager: NSObject {
    
    var setting      : UIViewController!
    var scan         : UIViewController!
    var home         : UIViewController!
    //var offers       : UIViewController!
    var agents       : UIViewController!
    var inbox        : UIViewController!
    var more         : UIViewController!
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
   // let settingStoryboard = UIStoryboard(name: "Setting", bundle: Bundle.main)
    let offerStoryboard   = UIStoryboard(name: "Offers", bundle: Bundle.main)
    let agentsStory: UIStoryboard = UIStoryboard(name: "NBSMain", bundle: nil)
    var controllers : [UIViewController]!
    
    func wrapControllers(_ nav: UINavigationController?, item: UINavigationItem) -> [UIViewController] {
        home         = storyboard.instantiateViewController(withIdentifier: "TBHomeViewController")
        agents       = agentsStory.instantiateViewController(withIdentifier: "NearByServicesMainVC") 
        //offers       = offerStoryboard.instantiateViewController(withIdentifier: "OffersViewController")
        scan         = storyboard.instantiateViewController(withIdentifier: "TBScanToPayViewController")
        inbox        = storyboard.instantiateViewController(withIdentifier: "TBNotificationViewController")
        more         = storyboard.instantiateViewController(withIdentifier: "TBMoreViewController")

        //SMSettingViewController

        if let homeVC = home as? TBHomeViewController,let moreVC = more as? TBMoreViewController {
            moreVC.languageDelegate = homeVC
        }
        
        
        controllers  = [home, agents, scan, inbox, more]
        
        return controllers
    }
}

class AdvertisementImages : NSObject {
    var ImageURLArray: [URL] = Array<URL>()
    var AdImageData: [Data]  = Array<Data>()
    var loginImageArray: [URL] = Array<URL>()
}
