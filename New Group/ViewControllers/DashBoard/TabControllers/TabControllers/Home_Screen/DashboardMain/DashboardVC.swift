//
//  DashboardVC.swift
//  OK
//
//  Created by Ashish on 12/24/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreData
import CoreTelephony

protocol SendDataDelegate {
    func sendData(text:String)
}

class DashboardVC: OKBaseController, OffersDashboardSyncDelegate, PhValidationProtocol, OfferSuccessPageDelegate {
  
    //MARK: - Outlets
    @IBOutlet weak var contentView : UIView!
    @IBOutlet weak var manager : DashboardManager!
    @IBOutlet var tabButton : [UITabBarItem]!
    @IBOutlet var tabBar: UITabBar!
    
    var updateKey: Int = 2
    var delegate: SendDataDelegate?
    var searchActive: Bool = false
    var userInfo: Dictionary<String, Any> = [:]
    
    //MARK: - Properties
    private var tabControllers : [UIViewController]!
    private var searchButton :  UIBarButtonItem!
    private var notificationButton : UIBarButtonItem!
    private var paymentLog : UIBarButtonItem!
    private var selectedIndex: Int = 0
    private var isSetting : Bool = true
    private lazy var searchBar = UISearchBar()
    var offerVC : OffersViewController?
    var indexToShowData = 0
    
    var validityDate: String?
    var numberOfTimes: String?
    var newImageURL: String?
    var isVisible: String?
    var showImageView = false
    
    @IBOutlet weak var topSearchView: UIView!
    @IBOutlet weak var topSearchBar: UISearchBar!
    @IBOutlet weak var searchViewBackBtn: UIButton!
    
    var advertisementImageView  = UIImageView()
    var advertisementView = UIView()
    var advertisementButton = UIButton()
    var isHeaderSearchOpened: Bool = false
    var inboxSearchBar = UISearchBar()
    
    @IBOutlet var cardDesignTabView: CardDesignView!
   
    let layerGradient = CAGradientLayer()
    var searchLabelTop: UILabel!
    
     var sideview = UIView()
    
     let fullview = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
    
    var compareStr = String()
    
    private lazy var modelCashInCashOut: CashInCashOutModel = {
        return CashInCashOutModel()
    }()
    let window = UIApplication.shared.keyWindow!
    
    //MARK: - Views Method
    override func viewDidLoad() {
        super.viewDidLoad()
        dashboard = self
       advertiseViewComponents()
       
        searchBar.delegate = self
        
        //updateBonusCount()
        favoriteManager.sync()
        userDef.removeObject(forKey: "PreloginSuccess")
     //   NotificationCenter.default.addObserver(self, selector: #selector(contactChangesValueObserver), name: Notification.Name.CNContactStoreDidChange, object: nil)
       // ContactManager().updateContacts()
        sideview.isHidden = true
        sideview.layer.cornerRadius = 10;
        sideview.layer.masksToBounds = false;
        sideview.layer.borderWidth = 0.2;
        sideview.layer.shadowColor = UIColor.gray.cgColor
        sideview.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        sideview.layer.shadowOpacity = 0.5
        sideview.layer.shadowRadius = 10
        
        compareStr = "0"
       
        phNumValidationsFile = getDataFromJSONFile() ?? []
        
        self.navigationTitleViewSetup()
        tabControllers = manager.wrapControllers(self.navigationController, item: self.navigationItem)
        offerVC = tabControllers[1] as? OffersViewController
        
        
        self.tabBar.selectedItem = tabButton[selectedIndex]
        let selectedVC = tabControllers[selectedIndex]
        addChild(selectedVC)
        selectedVC.view.frame = contentView.bounds
        contentView.addSubview(selectedVC.view)
        selectedVC.didMove(toParent: self)
        self.notificationRightBar()
        paymentLog = UIBarButtonItem(image: UIImage(named: "security_lock"), style: .plain, target: self, action: #selector(DashboardVC.barButtonAction_paymentLog))
        //paymentLog.tintColor = .blue

        let (networkCode, carrierName) = appDel.getNetworkCode()
        print(appDel.getNetworkCode())
        
        userDef.set(false, forKey: "Reverify_SimChange")

        UserDefaults.standard.set(networkCode, forKey: "MobileNetworkCode")
        UserDefaults.standard.set(carrierName, forKey: "MobileCarrierName")
        
        //This is notification for tab bar
        NotificationCenter.default.addObserver(self, selector: #selector(selectHomeOption), name: .selectHome, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUserType), name: .changeUserType, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getDashboardScreen(notification:)), name: .goDashboardScreen, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(backFromSucessPage), name: .scanQRClose, object: nil)
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 17.00) ?? UIFont.systemFont(ofSize: 17.00), NSAttributedString.Key.foregroundColor: UIColor.white]
        NotificationCenter.default.addObserver(self, selector: #selector(shortCutMenuAction), name: Notification.Name("ShortCutMenuAction"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(tabBarLocalization), name: NSNotification.Name(rawValue: languageKeyDashboard), object: nil)
        self.navigationItem.rightBarButtonItem  = nil
        self.updateUser()
        //For AppStore Testing
        if UserModel.shared.mobileNo == "00959782763424" {
            number = "09782763424"
        }
        self.setTabBarColor()
        
    }
    
    override func viewDidAppear(_ animated: Bool){
        super.viewDidAppear(animated)
        //tushar
        //this is multi payto total amount i am tracking to show user insufficient balance
        UserDefaults.standard.removeObject(forKey: "MultiplePayTOAmount")
        UserDefaults.standard.set(false, forKey: "TBAllNotificationcalled")
        UserDefaults.standard.set(false, forKey: "Inboxdatabaseinserted")
        print("Dashboard VC called=====")
        PaytoConstants.global.navigation = nil
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.advertisementView.removeFromSuperview()
        }
    }
    
    @objc func thumbsUpButtonPressed() {
        print("thumbs up button pressed")
        self.window.removeFromSuperview()
        self.advertisementView.removeFromSuperview()
    }
    
    private func setTabBarColor() {
        DispatchQueue.main.async {
            if device.type == .iPhone4 || device.type == .iPhone4S || device.type == .iPhone5 || device.type == .iPhone5S || device.type == .iPhoneSE {
                     UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: appFont, size: 9) ?? UIFont.systemFont(ofSize: 9)], for: .normal)
                 } else {
                     UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 9)], for: .normal)
                 }
                 if UserModel.shared.agentType == .advancemerchant {
                     self.tabBar.barTintColor = kYellowColor
                     self.tabBar.unselectedItemTintColor = UIColor.init(hex: "0321AA")
                     self.setTabBarItemColor(type: "advcanceMerchant", color: UIColor.init(hex: "0321AA"))
                 } else if UserModel.shared.agentType == .agent {
                     self.setGradient(hex1: "#605D5D", hex2: "#000000")
                     self.setTabBarItemColor(type: "agent", color: .white)
                     self.tabBar.unselectedItemTintColor = .white
                 } else if UserModel.shared.agentType == .merchant {
                     self.setGradient(hex1: "#00B0FF", hex2: "#304FFE")
                     self.setTabBarItemColor(type: "merchant", color: .white)
                     self.tabBar.unselectedItemTintColor = .white
                 } else  {
                     self.tabBar.barTintColor = UIColor.white//UIColor.init(red: 227.0, green: 225.0, blue: 221.0, alpha: 1.0)
                    
                     if device.type == .iPhone4 || device.type == .iPhone4S || device.type == .iPhone5 || device.type == .iPhone5S || device.type == .iPhoneSE {
                         UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.blue, NSAttributedString.Key.font: UIFont(name: appFont, size: 9) ?? UIFont.systemFont(ofSize: 9)], for: .selected)
                         UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 9) ?? UIFont.systemFont(ofSize: 9)], for: .normal)
                     } else {
                         UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.blue, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .selected)
                         UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .normal)
                     }
                     self.tabBar.unselectedItemTintColor = .lightGray
                    
                    self.setGradient(hex1: "#ffffff", hex2: "#ffffff")
                 }
            self.tabBarLocalization()
            self.animateTabBarButton()
        }
     
    }
    
    private func setTabBarItemColor(type: String, color: UIColor ) {
        if device.type == .iPhone4 || device.type == .iPhone4S || device.type == .iPhone5 || device.type == .iPhone5S || device.type == .iPhoneSE {
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: color, NSAttributedString.Key.font: UIFont(name: appFont, size: 9) ?? UIFont.systemFont(ofSize: 9)], for: .selected)
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: color, NSAttributedString.Key.font: UIFont(name: appFont, size: 9) ?? UIFont.systemFont(ofSize: 9)], for: .normal)
        } else {
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: color, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .selected)
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: color, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .normal)
        }
        UITabBar.appearance().unselectedItemTintColor = color
    }
    
    private func setGradient(hex1: String, hex2: String) {
        layerGradient.colors = [UIColor.init(hex: hex1).cgColor, UIColor.init(hex: hex2).cgColor]
        layerGradient.startPoint = CGPoint(x: 0, y: 0.25)
        layerGradient.endPoint = CGPoint(x: 0, y: 1)
        layerGradient.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: tabBar.frame.height)
        self.tabBar.layer.insertSublayer(layerGradient, at: 0)
    }
    
    @objc func tabBarLocalization() {
        searchLabelTop.text = "Search".localized
        for tabItem in tabButton {
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: appFont, size: 10) ?? UIFont.systemFont(ofSize: 10)], for: .normal)
            UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: appFont, size: 10) ?? UIFont.systemFont(ofSize: 9)], for: .selected)
            switch tabItem.tag {
            case 0:
                tabItem.title = "Home Tab".localized
            case 1:
                tabItem.title = "Agent".localized
            case 2:
                tabItem.title = "Scan QR Tab".localized
            case 3:
                tabItem.title = "Inbox Tab".localized
            case 4:
                tabItem.title = "More Tab".localized
            default:
                break
            }
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        if (selectedIndex == 2) {
            return true
        }
        return false
    }
    
    @objc func contactChangesValueObserver() {
        //ContactManager().updateContacts()
    }

    @objc func shortCutMenuAction() {
        DispatchQueue.main.async {
            self.decodeNavigationURL(UserDefaults.standard.value(forKey: "UIApplicationLaunchOptionsKey.shortcutItem") as? String ?? "")
        }
    }
    
    func notificationRightBar() {
        notificationButton = UIBarButtonItem(image: UIImage(named: "notification_dashboard"), style: .plain, target: self, action: #selector(DashboardVC.barButtonAction_notification))
        notificationButton.tintColor = .blue
        self.navigationItem.rightBarButtonItem  = notificationButton
    }
    
    //MARK: Search Manage Delegate
    func setupSearchBar() {
        inboxSearchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
      //  inboxSearchBar.showsCancelButton = true
        inboxSearchBar.delegate =  self
        
      //  inboxSearchBar.tintColor = UIColor.darkGray
        inboxSearchBar.barTintColor = UIColor.white
        inboxSearchBar.searchBarStyle = UISearchBar.Style.minimal
        inboxSearchBar.placeholder = "Search".localized
        self.navigationItem.titleView = inboxSearchBar
        if let searchTextField = inboxSearchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
            if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
               
                if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                    searchTextField.font = myFont
                }else{
                    //
                }
                //searchTextField.font = myFont
                searchTextField.keyboardType = .asciiCapable
                searchTextField.placeholder = "Search".localized
                searchTextField.backgroundColor  = .white
            }
        }
        let view = self.inboxSearchBar.subviews[0] as UIView
        let subViewsArray = view.subviews
        for subView: UIView in subViewsArray {
            if subView.isKind(of: UITextField.self) {
                subView.tintColor = UIColor.gray
            }
        }
        
        inboxSearchBar.becomeFirstResponder()
    }
    
    func resignSearchBarKeypad() {
        inboxSearchBar.resignFirstResponder()
    }
    
    func hideSearchWithoutAnimation() {
     //   isHeaderSearchOpened = false
    //    inboxSearchBar.text = ""
        inboxSearchBar.resignFirstResponder()
        inboxSearchBar.isHidden = true
    }
    
     //MARK: inboxRightBar
    func inboxRightBar() {
        
        let searchImage = UIImage(named: "r_search")!
        let clipImage = UIImage(named: "menu_white_payto")!
        
        let searchBtn: UIButton = UIButton(type: UIButton.ButtonType.custom)
        searchBtn.setImage(searchImage, for: UIControl.State.normal)
        searchBtn.addTarget(self, action: #selector(searchInBoxAction), for: .touchUpInside)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let searchBarBtn = UIBarButtonItem(customView: searchBtn)
        
        let clipBtn: UIButton = UIButton(type: UIButton.ButtonType.custom)
        clipBtn.setImage(clipImage, for: UIControl.State.normal)
      clipBtn.addTarget(self, action: #selector(moreInBoxAction), for: .touchUpInside)
        clipBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let clipBarBtn = UIBarButtonItem(customView: clipBtn)
        self.navigationItem.rightBarButtonItems = [clipBarBtn, searchBarBtn]
       // self.navigationController?.navigationItem.rightBarButtonItems = [clipBarBtn, searchBarBtn]
        
        //self.navigationTitleViewSetup()
    }
     @objc func searchInBoxAction() {
         sideview.isHidden = true
         fullview.isHidden = true
        inboxSearchBar.isHidden = false
        //   self.navigationTitleViewSetup()
        compareStr = "0"
        self.setupSearchBar()
    }
    
    
    @objc func moreInBoxAction() {
     //   let sideview = UIView(frame: CGRect(x: screenWidth - 140, y: 0, width: 140, height: 200))
        
        var locaArray = [String]()
        
        inboxSearchBar.isHidden = true
        self.navigationItem.titleView = nil
        //   self.navigationItem.rightBarButtonItem  = nil
        self.navigationItem.leftBarButtonItem   = nil
        self.title = "Inbox".localized
        
        // sideview = UIView(frame: CGRect(x: screenWidth - 250, y: 0, width: 250, height: 200))
        sideview.isHidden = false
        fullview.isHidden = false
        locaArray = ["All", "Pending Approval", "Request Approved", "Cancelled"]
        fullview.backgroundColor = .clear
        self.view.addSubview(fullview)
        self.view.bringSubviewToFront(fullview)
     
   
        if indexToShowData == 2 {
            
          //   sideview = UIView(frame: CGRect(x: screenWidth - 250, y: 0, width: 250, height: 200))
            
//            if(appDel.currentLanguage == "uni") || (appDel.currentLanguage == "my")
//            {
//                sideview = UIView(frame: CGRect(x: screenWidth - 250, y: 0, width: 250, height: 200))
//            }
//
//            else{
//                sideview = UIView(frame: CGRect(x: screenWidth - 140, y: 0, width: 200, height: 200))
//            }
            
             sideview.isHidden = false
             fullview.isHidden = false
            locaArray = ["All", "Pending Approval", "Request Approved", "Cancelled"]
            fullview.backgroundColor = .clear
            self.view.addSubview(fullview)
            self.view.bringSubviewToFront(fullview)
          
        }
       else if indexToShowData == 3 {
            
           // sideview = UIView(frame: CGRect(x: screenWidth - 140, y: 0, width: 200, height: 200))
            sideview.isHidden = false
            fullview.isHidden = false
            locaArray =  ["Pending", "Successful", "Reject", "Scheduled"]
            
            fullview.backgroundColor = .clear
            self.view.addSubview(fullview)
            self.view.bringSubviewToFront(fullview)
        }
        else {
              fullview.isHidden = true
              sideview.isHidden = true
        }
        
        if compareStr == "0"
        {
            sideview.isHidden = false
            compareStr = "1"
        }
        else if compareStr == "1"
        {
            sideview.isHidden = true
            compareStr = "0"
        }
      
     
        sideview.backgroundColor = .white
        let myButton = UIButton(type: UIButton.ButtonType.custom) // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
        myButton.backgroundColor = UIColor.white
        myButton.setTitle(locaArray[0].localized, for: .normal)
        myButton.tag = 0
        myButton.titleLabel?.font = UIFont(name: appFont, size: 15)
        myButton.setTitleColor(UIColor.black, for: .normal)
        myButton.frame = CGRect(x: 10, y: 10, width: 250, height: 40)
        myButton.addTarget(self, action: #selector(calllInBoxAction(_:)), for: .touchUpInside)
        myButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        sideview.addSubview( myButton)
        
        let myButton1 = UIButton(type: UIButton.ButtonType.custom) // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
        myButton1.backgroundColor = UIColor.white
        myButton1.setTitle(locaArray[1].localized, for: .normal)
        myButton1.tag = 1
        myButton1.titleLabel?.font = UIFont(name: appFont, size: 15)
        myButton1.setTitleColor(UIColor.black, for: .normal)
        myButton1.frame = CGRect(x: 10, y: 60, width: 250, height: 40)
        myButton1.addTarget(self, action: #selector(calllInBoxAction(_:)), for: .touchUpInside)
        myButton1.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        sideview.addSubview( myButton1)
        
        let myButton2 = UIButton(type: UIButton.ButtonType.custom) // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
        myButton2.backgroundColor = UIColor.white
        myButton2.setTitle(locaArray[2].localized, for: .normal)
        myButton2.tag = 2
        myButton2.titleLabel?.font = UIFont(name: appFont, size: 15)
        myButton2.setTitleColor(UIColor.black, for: .normal)
        myButton2.frame = CGRect(x: 10, y: 110, width: 250, height: 40)
        myButton2.addTarget(self, action:#selector(calllInBoxAction(_:)), for: .touchUpInside)
        myButton2.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        sideview.addSubview( myButton2)
        
        let myButton3 = UIButton(type: UIButton.ButtonType.custom) // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
        myButton3.backgroundColor = UIColor.white
        myButton3.setTitle(locaArray[3].localized, for: .normal)
        myButton3.tag = 3
        myButton3.titleLabel?.font = UIFont(name: appFont, size: 15)
        myButton3.setTitleColor(UIColor.black, for: .normal)
        myButton3.frame = CGRect(x: 10, y: 160, width: 250, height: 40)
        myButton3.addTarget(self, action: #selector(calllInBoxAction(_:)), for: .touchUpInside)
        myButton3.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        sideview.addSubview( myButton3)
        
        
        self.view.addSubview(sideview)
        self.view.bringSubviewToFront(sideview)
    }

    @objc func calllInBoxAction(_ sender: UIButton) {
        if indexToShowData == 2 {
                callNotificationForSideMenuDropDown(index: sender.tag)
        }
      else
        {
            callNotificationForSideMenu(index: sender.tag)
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view == self.sideview {
            sideview.isHidden = false
            fullview.isHidden = false
              compareStr = "1"
        }
        else
        {
             fullview.isHidden = true
             sideview.isHidden = true
              compareStr = "0"
        }
    }
    
    
    @objc func getDashboardScreen(notification: NSNotification) {
        self.moveToHomeScreen()
    }
    
    private func moveToHomeScreen(fromScan: Bool? = false) {
       
        self.navigationItem.rightBarButtonItem  = nil
        self.navigationItem.leftBarButtonItem = nil
        selectedIndex = 0
        self.navigationTitleViewSetup()
        self.tabBar.selectedItem = tabButton[0]
        let selectedVC = tabControllers[0]
        addChild(selectedVC)
        selectedVC.view.frame = contentView.bounds
        contentView.addSubview(selectedVC.view)
        selectedVC.didMove(toParent: self)
    }
    
    @IBAction func unwindToVC1(segue:UIStoryboardSegue) { }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        UserDefaults.standard.set(false, forKey: "HelpButtonHideShow")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getDataFromApi()
        if selectedIndex == 0 {
            self.navigationTitleViewSetup()
        }
        println_debug(self.navigationController?.isNavigationBarHidden)
        self.navigationController?.isNavigationBarHidden = false
        println_debug(self.navigationController?.isNavigationBarHidden)

        UserDefaults.standard.set(false, forKey: "HelpButtonHideShow")
        self.decodeNavigationURL(UserDefaults.standard.value(forKey: "UIApplicationLaunchOptionsKey.shortcutItem") as? String ?? "")
        
        if(appDel.currentLanguage == "uni")
              {
                  sideview = UIView(frame: CGRect(x: screenWidth - 250, y: 0, width: 250, height: 200))
              }
            else if(appDel.currentLanguage == "my")
                {
                    sideview = UIView(frame: CGRect(x: screenWidth - 250, y: 0, width: 250, height: 200))
                }
              else{
                  sideview = UIView(frame: CGRect(x: screenWidth - 140, y: 0, width: 200, height: 200))
              }
        
        println_debug("SelectedIndex: \(String(describing: self.tabBar.selectedItem))")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tabBar.invalidateIntrinsicContentSize()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Methods
//    func notificationRightBar() {
//        notificationButton = UIBarButtonItem(image: UIImage(named: "notification_dashboard"), style: .plain, target: self, action: #selector(DashboardVC.barButtonAction_notification))
//        notificationButton.tintColor = .blue
//        self.navigationItem.rightBarButtonItem  = notificationButton
//    }
    
    
    func navigationTitleViewSetup() {
        let dashBoardVCTitleVC = UIView(frame: .init(x: 0, y: 0, width: self.view.frame.width * 0.95, height: 42))
        dashBoardVCTitleVC.layer.cornerRadius = 5.0
        dashBoardVCTitleVC.backgroundColor = .white
        let imageV = UIImageView.init(frame: .init(x: 15, y: 2, width: 38, height: 38))
        imageV.contentMode = .scaleAspectFill
         imageV.image = #imageLiteral(resourceName: "ok")
        dashBoardVCTitleVC.addSubview(imageV)
        searchLabelTop = UILabel(frame: CGRect(x: (self.view.frame.width * 0.95) - 130, y: 10 , width: 80, height: 22))
        searchLabelTop.textColor = UIColor.init(hex: "0321AA")
        searchLabelTop.font = UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: 17.0)
        searchLabelTop.text = "Search".localized
        searchLabelTop.textAlignment = .right
        dashBoardVCTitleVC.addSubview(searchLabelTop)
        let searchIcon = UIImageView.init(frame: .init(x: (self.view.frame.width * 0.95) - 40 , y: 10, width: 22, height: 22))
        searchIcon.contentMode = .scaleAspectFit
        searchIcon.image = UIImage(named: "search_dashboard")
        
//        searchButton = UIBarButtonItem(image: UIImage(named: "search_dashboard"), style: .plain, target: self, action: #selector(DashboardVC.barButtonAction_search))
//        searchButton.tintColor = .blue
//        self.navigationItem.leftBarButtonItem  = searchButton
        dashBoardVCTitleVC.addSubview(searchIcon)
        self.navigationItem.titleView = dashBoardVCTitleVC
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(barButtonAction_search))
        tapGesture.numberOfTapsRequired = 1
        dashBoardVCTitleVC.addGestureRecognizer(tapGesture)
    }
    
    func navigationTitleViewSetupMore() {
        let imageV = UIImageView.init(frame: .init(x: 0, y: 0, width: 50, height: 50))
        imageV.contentMode = .scaleAspectFill
        imageV.image = #imageLiteral(resourceName: "ok")
        self.navigationItem.titleView = imageV
    }
    
//    //MARK: - Button Action Methods
//    @IBAction func unwindToVC1(segue:UIStoryboardSegue) { }
//
//    //MARK: - Target Methods
//    @objc func shortCutMenuAction() {
//        DispatchQueue.main.async {
//            self.decodeNavigationURL(UserDefaults.standard.value(forKey: "UIApplicationLaunchOptionsKey.shortcutItem") as? String ?? "")
//        }
//    }
//
//    @objc func getDashboardScreen(notification: NSNotification) {
//        self.tabBar.selectedItem = tabButton[2]
//        let selectedVC = tabControllers[2]
//        addChildViewController(selectedVC)
//        selectedVC.view.frame = contentView.bounds
//        contentView.addSubview(selectedVC.view)
//        selectedVC.didMove(toParentViewController: self)
//    }
    
    @objc func barButtonAction_search(){
        guard let searchHomeViewController = self.storyboard?.instantiateViewController(withIdentifier: ConstantsController.searchHomeViewController.nameAndID) as? SearchHomeViewController else { return }
        searchHomeViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        navigationController?.pushViewController(searchHomeViewController, animated: true)
    }
    
    @objc func barButtonAction_paymentLog() {
        guard let sanResultDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "QRScanResultDetailViewController") as? QRScanResultDetailViewController else { return }
        self.navigationController?.pushViewController(sanResultDetailViewController, animated: true)
    }
    
    @objc func barButtonAction_notification(){
        print("barButtonAction_notification========")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let notificationController = storyboard.instantiateViewController(withIdentifier: "TBNotificationViewController") as? TBNotificationViewController else { return }
        let navigation = UINavigationController(rootViewController: notificationController)
        navigation.modalPresentationStyle = .fullScreen
        UserDefaults.standard.set(false, forKey: "TBAllNotificationcalled")
        self.navigationController?.present(navigation, animated: true, completion: nil)
    }
    
    fileprivate func showPromotionPopup() {
//        let currentDate = Date()
//        guard let promoDateStr = kycAppControlInfo.first?.validityDate else { return }
//        guard let show = kycAppControlInfo.first?.visible, show == "True" else { return }
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-mm-dd"
//        guard let promoDate = dateFormatter.date(from: promoDateStr) else { return }
//        if currentDate > promoDate {
//            UserDefaults.standard.set(0, forKey: "PromotionShowInDashCount")
//            return }
//        let count = UserDefaults.standard.integer(forKey: "PromotionShowInDashCount")
//        if count < 3 {
//            DispatchQueue.main.async {
//                guard let promoVC = UIStoryboard(name: "Promotion", bundle: nil).instantiateViewController(withIdentifier: String.init(describing: DashboardPromotionViewController.self)) as? DashboardPromotionViewController else { return }
//                promoVC.modalPresentationStyle = .overCurrentContext
//                self.navigationController?.present(promoVC, animated: false, completion: nil)
//            }
//        }
    }
    
    
}

//MARK: - TBScanQRDelegate
extension DashboardVC: TBScanQRDelegate {
    
    @objc func backFromSucessPage() { // offers success page
        self.gotoHomeTabVC()
    }
    
    @objc func selectHomeOption() {
        DispatchQueue.main.async {
            println_debug("selectHome")
            self.selectedIndex = 0
            self.gotoHomeTabVC(fromScan: false, fromNearBy: false, fromInBox: false)
            self.tabBar.selectedItem = self.tabButton[self.selectedIndex]
            self.tabBar.unselectedItemTintColor = .white
        }

    }
    
    @objc func updateUserType() { // offers success page
        let type = userDef.string(forKey: "changeType")
        if type != nil && type != "" {
            println_debug("UserUpdatedFromLogin")
            callUserUpdateAPI(type: type ?? "SUBSBR")
        }
      }
    
    
    private func gotoHomeTabVC(fromScan: Bool? = false, fromNearBy: Bool? = false, fromInBox: Bool? = false) {
        guard let items = self.tabBar.items else { return }
        guard let home  = items[safe: 0] else { return }
        self.tabBar(self.tabBar, didSelect: home)
        self.tabBar.selectedItem = home
        let selectedVC = tabControllers[0]
       
        if let frmScan = fromScan, frmScan == true {
            if let homeVC = selectedVC as? TBHomeViewController {
                homeVC.showScanQR = true
            }
        } else if let frmNear = fromNearBy, frmNear == true {
            if let homeVC = selectedVC as? TBHomeViewController {
                homeVC.showNearBy = true
            }
        } else if let fromInBox = fromInBox, fromInBox == true {
            if let homeVC = selectedVC as? TBHomeViewController {
                homeVC.inBox = true
            }
        } 
    }
    
    
    
    func moveToPayTo(fromTransfer: Bool? = false) {
        self.navigatetoPayto()
    }
    
    func moveToPayto(withScanObject object: PTScanObject, fromTransfer: Bool? = false) {
        println_debug("Need To Checl----------------------------------------------------")
        self.navigateToPayto(phone: object.agentNo, amount: object.amount, name: object.name, remark: object.remarks)
    }
}

//MARK: - SMSettingDelegate
extension DashboardVC: SMSettingDelegate {
    func changeNavigationController(controller: UIViewController) {
        println_debug("Delegate call back changeNavigationController")
    }

    func settingCloseClick(sender: UIBarButtonItem?) {
        println_debug("Delegate call back settingCloseClick")
    }
    
    //MARK:- Shortcut Navigation Decoder
    func decodeNavigationURL(_ strURL: String) -> Void {
        if strURL == "com.cgm.PAYTO" {
            TBHomeNavigations.main.navigate(.payto)
        } else if strURL == "com.cgm.MyNumber" {
            TBHomeNavigations.main.navigate(.mobileNumber)
        } else if strURL == "com.cgm.OtherNumber" {
            TBHomeNavigations.main.navigate(.otherNumber)
        } else if strURL == "com.cgm.ScanQR" {
            selectedIndex = 1
            self.tabBar.selectedItem = tabButton[selectedIndex]
            let selectedVC = tabControllers[selectedIndex]
            addChild(selectedVC)
            selectedVC.view.frame = contentView.bounds
            contentView.addSubview(selectedVC.view)
            selectedVC.didMove(toParent: self)
        }else if strURL == "com.cgm.FacePay"{
             TBHomeNavigations.main.navigate(.transferTo)
        }
        UserDefaults.standard.removeObject(forKey: "UIApplicationLaunchOptionsKey.shortcutItem")
        UserDefaults.standard.synchronize()
    }
}

//MARK: - BioMetricLoginDelegate
extension DashboardVC: BioMetricLoginDelegate {
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        if isSuccessful {
            if let settingVC = tabControllers[4] as? SMSettingViewController {
            settingVC.navigation = self.navigationController
            isSetting = false
            DispatchQueue.main.async {
                self.tabBar(self.tabBar, didSelect: self.tabBar.items![4])
                self.tabBar.selectedItem = self.tabBar.items![4]
             }
           }
        }
    }
}


//MARK: - UITabBarDelegate
extension DashboardVC: UITabBarDelegate {
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        let previousIndex = selectedIndex
        selectedIndex = item.tag
        if UserLogin.shared.loginSessionExpired {
            isSetting = true
        }
        
        let previousVC = tabControllers[previousIndex]
        
        if let offersVC = previousVC as? OffersViewController {
            offersVC.delegate = self
            offersVC.customDeinit()
        }
        
        previousVC.willMove(toParent: nil)
        previousVC.view.removeFromSuperview()
        previousVC.removeFromParent()
        
        // prioritize controller setting in tab bar controller items
        if item.tag == 1 {
            if let nearBy = tabControllers[1] as? NearByServicesMainVC {
                nearBy.needToShowBackButton  = true
//                offer.backOption = self
            }
        } else if item.tag == 3 {
            print("Dashboard Tabbar delegate=========\(item.tag)")
            if let notification = tabControllers[3] as? TBNotificationViewController {
                notification.delegate = self 
                notification.navigation  = self.navigationController
            }
        }
        
        let selectedVC : UIViewController!
        // go to home tab if selected index tapped again
        if previousIndex == selectedIndex, item.tag != 0 {
            selectedVC = tabControllers[0]
            addChild(selectedVC)
            selectedVC.view.frame = contentView.bounds
            tabBar.selectedItem = tabButton[0]
            selectedIndex = 0
        } else {
            selectedVC = tabControllers[selectedIndex]
            addChild(selectedVC)
            if item.tag == 2 {
                if device.type == .iPhoneXSMax || device.type == .iPhoneX || device.type == .iPhoneXS || device.type == .iPhoneXR || device.type == .iPhone11 || device.type == .iPhone11Pro || device.type == .iPhone11ProMax || device.type == .iPhone12 {
                    selectedVC.view.frame = CGRect(x: 0.0, y: 0.0, width: contentView.frame.width, height: contentView.frame.height + 84.0)
                } else {
                    selectedVC.view.frame = CGRect(x: 0.0, y: 0.0, width: contentView.frame.width, height: screenHeight - 60.0)
                }
            } else {
                selectedVC.view.frame = contentView.bounds
            }
        }
        
        switch selectedIndex {
        case 0:
            self.title = ""
            self.navigationItem.leftBarButtonItem  = nil
             self.navigationItem.rightBarButtonItems = nil
            navigationTitleViewSetup()
        case 1:
            self.gotoHomeTabVC(fromNearBy: true)
//            self.title = "Agent".localized
//            self.navigationItem.rightBarButtonItem  = nil
//            self.navigationItem.leftBarButtonItem   = nil
//            self.navigationItem.titleView = nil
        case 2:
//            if PaymentVerificationDBManager().getAllPaymentVerificationDataFromDB().count > 0 {
//                self.navigationItem.rightBarButtonItem  = paymentLog
//            } else {
//                self.navigationItem.rightBarButtonItem  = nil
//            }
//            self.navigationItem.titleView = nil
//            self.navigationItem.leftBarButtonItem   = nil
//            self.title = "QR Code".localizedx
            print("!@#!@#!@#@!$!")
            self.gotoHomeTabVC(fromScan: true)
         case 3:
            
         //   self.navigationItem.rightBarButtonItem  = nil
           
//          self.navigationItem.titleView = nil
//          self.navigationItem.leftBarButtonItem   = nil
//          self.title = "Inbox".localized
          self.gotoHomeTabVC(fromInBox: true)
          //self.inboxRightBar()
        case 4:
            self.title = ""
            self.navigationItem.titleView = nil
            navigationTitleViewSetupMore()
            self.navigationItem.rightBarButtonItem  = nil
            self.navigationItem.leftBarButtonItem   = nil
        default :
            break
            
        }
        
        if item.tag == 0  || item.tag == 4 {
            self.contentView.addSubview(selectedVC.view)
            selectedVC.didMove(toParent: self)
            animateTabBarButton()
        }
    }
    
    func animateTabBarButton() {
        tabBar.transform = .identity
//        UIView.animate(withDuration: 1.0, animations: {
//            self.tabBar.tintColor =  UIColor.init(hex: "F3C632")
//        }) { (done) in
//            self.tabBar.tintColor = .blue
//        }
        
        if UserModel.shared.agentType == .advancemerchant {
           self.tabBar.tintColor = UIColor.init(hex: "0321AA")
        } else if UserModel.shared.agentType == .agent || UserModel.shared.agentType == .merchant {
            self.tabBar.tintColor = .white
        } else  {
            self.tabBar.tintColor = .blue
        }
    }
    
    // OffersView controller delegate
    //MARK:- OffersViewController delgate (Navigation to payto)
    func presentController(controller: UIViewController) {
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true, completion: nil)
    }
    
}

extension DashboardVC: InBoxightBarShow {
    func showInBoxRightBar(sttaus: Bool, getIndex: Int) {
        print("showInBoxRightBar Dashboard called----\(getIndex)---\(sttaus)")

        if sttaus {
            indexToShowData = getIndex
            self.inboxRightBar()
            compareStr = "0"
        } else {
            self.navigationItem.rightBarButtonItems = nil
            self.fullview.isHidden = true
            self.sideview.isHidden = true
            inboxSearchBar.isHidden = true
            self.navigationItem.titleView = nil
            //   self.navigationItem.rightBarButtonItem  = nil
            self.navigationItem.leftBarButtonItem   = nil
            self.title = "Inbox".localized
        }
    }
    
//    func showInBoxRightBar(sttaus: Bool) {
//        if sttaus {
//            self.inboxRightBar()
//        } else {
//            self.navigationItem.rightBarButtonItems = nil
//        }
//    }
}

//MARK: - Validation Api and bonus count update
extension DashboardVC {
    
    private func checkVersionAndUpdateDB() {
        ListUserContactsManager.fetchListUserContacts { (isSuccess, userContactList) in
            guard isSuccess else {
                self.checkVersionAndUpdateDB()
                return
            }
            guard let list = userContactList else { return }
            ListUserContactsManager.insertAllRecordsToBankDatabase(model: list)
        }
    }
    
    private func updateSendMoneytoBankDB(version: Int) {
        PaymentVerificationManager.deleteAllRecordsFromDatabase { (status) in
            if status {
                BankApiClient.fetchBanksList { (isSuccess, banklist) in
                    guard isSuccess else {
                        self.updateSendMoneytoBankDB(version: version)
                        return
                    }
                    guard let list = banklist else { return }
                    userDef.set(version, forKey: "SendmoneyToBankVersion")
                    PaymentVerificationManager.insertAllRecordsToBankDatabase(model: list)
                }
            }
        }
    }
    
    private func checkVersionAndUpdateBankApiDB(version: Int) {
        println_debug("checkVersionAndUpdateBankApiDB")
        PaymentVerificationManager.deleteAllRecordsFromDatabase { (status) in
            if status {
                BankApiClient.fetchBanksRoutingList{ (isSuccess, banklist) in
                    guard isSuccess else {
                        self.checkVersionAndUpdateBankApiDB(version: version)
                        return
                    }
                    guard let list = banklist else { return }
                    userDef.set(version, forKey: "BankRoutingapiversion")
                    PaymentVerificationManager.insertAllRecordsToBankRoutingDatabase(model: list)
                }
            }
        }
    }
    
    func getDataFromApi() {
        phNumValidationsApi = []
        if appDelegate.checkNetworkAvail() {
            //PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStr = String.init(format: Url.numberValidation, UserModel.shared.osVersion, UserModel.shared.mobileNo, simid, appVersion, UserModel.shared.msid, "1")
            let getPhNumValidtnUrl = getUrl(urlStr: urlStr, serverType: .serverApp)
            let params = [String: String]()
            println_debug("REQUEST URL: \(getPhNumValidtnUrl)")
            web.genericClass(url: getPhNumValidtnUrl, param: params as AnyObject, httpMethod: "GET", mScreen: "PhNoValidation")
        } else {
            println_debug("No Internet")
        }
    }
    
    func updateBonusCount() {
        func getUpdateBonusCountReq() -> UpdateBonusCountRequest {
            let request = UpdateBonusCountRequest()
            request.userMobile = "9979168471"
            request.userName = "Rajesh"
            request.countryCode = "0095"
            
            let bonusPoint = BonusPointsData()
            bonusPoint.countryCode = "0095"
            bonusPoint.cellNumber = "9979168471"
            bonusPoint.bonusCount = "40"
            bonusPoint.bonusPointName = "OKtaxitest"
            bonusPoint.bonusPointNumber = "9262888032"
            bonusPoint.expiryDate = "2019-07-01T00:00:00"
            
            let bonusPoints = [bonusPoint]
            request.bonusPoints = bonusPoints
            return request
        }
        if appDelegate.checkNetworkAvail() {
            PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            let urlStr = "http://69.160.4.150:1313/UserService/UpdateUserBonusCount"
            guard let updateBonusCountURL = URL(string: urlStr) else { return }
            let params = [String: String]() as AnyObject
            println_debug("REQUEST URL: \(updateBonusCountURL)")
            
            let encoder = JSONEncoder()
            do {
                let req = getUpdateBonusCountReq()
                let jsonData = try encoder.encode(req)
                web.genericClass(url: updateBonusCountURL, param: params, httpMethod: "POST", mScreen: "UpdateBonusCount", hbData: jsonData)
            } catch {
                PTLoader.shared.hide()
            }
        } else {
            println_debug("No Internet")
        }
    }
    
    func getAgentType() -> Int {
        var agentType = 6
        switch UserModel.shared.agentType {
        case .user:
            agentType = 6
        case .agent:
            agentType = 1
        case .advancemerchant:
            agentType = 4
        case .dummymerchant:
            agentType = 5
        case .merchant:
            agentType = 2
        case .oneStopMart:
            agentType = 8
        }
        
        return agentType
    }
    
    func updateUser() {
        let cashIn = UserDefaults.standard.value(forKey: "MerchantCashInStatus") as? Bool ?? false
        let cashOut = UserDefaults.standard.value(forKey: "MerchantCashOutStatus") as? Bool ?? false
        let deliveryType = UserDefaults.standard.value(forKey: "MerchantCashMobStationayStatus") as? Bool ?? false
        let agentType = self.getAgentType()
        let appversion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        let request = UpdateUserRequest(countryCode: UserModel.shared.countryCode.replacingOccurrences(of: "+", with: ""),
                                        mobileNumber: UserModel.shared.mobileNo,
                                        openingTime: UserModel.shared.openTime,
                                        closingTime: UserModel.shared.closeTime,
                                        okDollarBalance: Int(self.walletAmount()),
                                        latitude: GeoLocationManager.shared.currentLatitude,
                                        longitude: GeoLocationManager.shared.currentLongitude, cellID: "0",
                                        cashIn: cashIn, cashOut: cashOut,
                                        agentType: deliveryType ? "1" : "0",
                                        type: agentType, isOpenAlways: true, appVersion: appversion,
                                        category: UserModel.shared.businessCate,
                                        subCategory: UserModel.shared.businessSubCate)
        modelCashInCashOut.updateUser(request: request, showIndicator: false)
    }
}

//MARK: - WebServiceResponseDelegate
extension DashboardVC: WebServiceResponseDelegate {
    func openOKDollarFromAppStore() {
        let urlStr = "itms-apps://itunes.apple.com/app/id1067828611"
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(URL(string: urlStr)!)
        }
    }
    
    
    // check if you want to show and hide Dashboard icons - Avaneesh
    func webResponse(withJson json: AnyObject, screen: String) {
      
        if screen == "PhNoValidation" {
            guard let castedData = json as? Data else {
                PTLoader.shared.hide()
                println_debug("Data error")
                return
            }
            do {
                println_debug("PhNoValidation called")
                let decoder = JSONDecoder()
                let response = try decoder.decode(MobNumValidationResponse.self, from: castedData)
                if let responseStr = response.data {
                    println_debug(responseStr)
                    if responseStr == "null" {
                        println_debug("Null string from server")
                    } else {
                        guard let strData = responseStr.data(using: .utf8) else { return }
                        let decRes = try decoder.decode(MobNumDataResponse.self, from: strData)
                        print(decRes)
                        validityDate = decRes.kycAppControlInfo.first?.validityDate
                        numberOfTimes = decRes.kycAppControlInfo.first?.numberofTimes
                        newImageURL = decRes.kycAppControlInfo.first?.imageUrl
                        isVisible = decRes.kycAppControlInfo.first?.visible
                        telecoTopupSubscription = decRes.telecoTopupSubscriptionValue
                        kycAppControlInfo = decRes.kycAppControlInfo
                        serviceControlAppInfo = decRes.serviceControlAppInfo
                        isShowGiftCard = true
                        isDTHShow = true
                        
                        
                        for item in serviceControlAppInfo {
                            if item.serviceName == "OversesTopup" {
                                if item.myanmar  == 1 {
                                    isOverseasShow = true
                                }else {
                                    isOverseasShow = false
                                }
                            }
                        }

                        
                        if let value = kycAppControlInfo.first?.isMerchantPayment, value == 1 {
                            isMarchantPaymentShow = true
                        }else {
                            isMarchantPaymentShow = false
                        }
                        
                        for item in serviceControlAppInfo {
                            if item.serviceName == "Bill Splitter" {
                                if item.isActive {
                                    if item.myanmar == 1 {
                                        isBillSplitterShow = true
                                    }else {
                                        isBillSplitterShow = false
                                    }
                                }else {
                                    isBillSplitterShow = false
                                }
                            }else if item.serviceName == "instant pay" {
                                if item.isActive {
                                    if item.myanmar == 1 {
                                        isInstantPayShow = true
                                    }else {
                                        isInstantPayShow = false
                                    }
                                }else {
                                    isInstantPayShow = false
                                }
                            }
                        }
       
                        for item in serviceControlAppInfo {
                            if item.serviceName == "mobile & electronics" {
                                if item.isActive {
                                    if item.myanmar  == 1 {
                                        isMobileElectronicsShow = true
                                    }else {
                                        isMobileElectronicsShow = false
                                    }
                                }else {
                                    isMobileElectronicsShow = false
                                }
                            }
                        }
      
                        
                        for item in serviceControlAppInfo {
                            
                            if item.serviceName == "buy & sell bonus points" {
                                if item.isActive {
                                    if item.myanmar == 1 {
                                        isBuySaleBonusPointShow = true
                                    }else {
                                        isBuySaleBonusPointShow = false
                                    }
                                }else {
                                    isBuySaleBonusPointShow = false
                                }
                            }else if item.serviceName == "lucky draw" {
                                if item.isActive {
                                    if item.myanmar == 1 {
                                        isLuckyDrawShow = true
                                    }else {
                                        isLuckyDrawShow = false
                                    }
                                }else {
                                    isLuckyDrawShow = false
                                }
                            }else if item.serviceName == "earning points" {
                                if item.isActive {
                                    if item.myanmar == 1 {
                                        isEarningPointShow = true
                                    }else {
                                        isEarningPointShow = false
                                    }
                                }else {
                                    isEarningPointShow = false
                                }
                            }else if item.serviceName == "voting" {
                                if item.isActive {
                                    if item.myanmar == 1 {
                                        isVotingshow = true
                                    }else {
                                        isVotingshow = false
                                    }
                                }else {
                                    isVotingshow = false
                                }
                            }else if item.serviceName == "offers" {
                                if item.isActive {
                                    if item.myanmar == 1 {
                                        isOfferShow = true
                                    }else {
                                        isOfferShow = false
                                    }
                                }else {
                                    isOfferShow = false
                                }
                            }
                            else if item.serviceName == "lottery" {
                                if item.isActive {
                                    if item.myanmar == 1 {
                                        isLotteryShow = true
                                    }else {
                                        isLotteryShow = false
                                    }
                                }else {
                                    isLotteryShow = false
                                }
                            }
                        }
                        
                        if UserModel.shared.agentType == .advancemerchant {
                            if refRecharge != nil {
                                refRecharge?.advanceMerhantReload()
                            }
                        }else {
//                            if  {
//                                if refRecharge != nil {
//                                    DispatchQueue.main.async {
//                                        refRecharge?.mainCollectionView_first.reloadData()
//                                    }
//                                }
//                                
//                            }
                            if isDTHShow || isMarchantPaymentShow || isBillSplitterShow || isInstantPayShow || isOverseasShow {
                                if refRecharge != nil {
                                    refRecharge?.reloadSection()
                                }
                            }
                        }
                        
                        if UserModel.shared.agentType != .advancemerchant {
                            if isMobileElectronicsShow {
                                if refBoolOKDollar != nil {
                                    refBoolOKDollar?.reloadCollection()
                                }
                            }
                            
                        }
                        
                        
                        if UserModel.shared.agentType != .advancemerchant {
                            if refLoyality != nil {
                                refLoyality?.reload()
                            }
                        }
                            
                        self.showadvertisement()
                        
                        // here we are getting the status of the current user ( what is the user type ) - Avaneesh
                        /******************************************************/
                        let upgrade = kycAppControlInfo.first?.agentType
                        if upgrade != nil {
                            if kycAppControlInfo.first?.agentType == "SUBSBR" && UserModel.shared.agentType == .user {
                                // UserModel.shared.agentType = .user
                            }else if kycAppControlInfo.first?.agentType == "MER" && UserModel.shared.agentType == .merchant {
                                //UserModel.shared.agentType = .merchant
                            }else if kycAppControlInfo.first?.agentType == "AGENT" && UserModel.shared.agentType == .agent{
                                // UserModel.shared.agentType = .agent
                            }else if kycAppControlInfo.first?.agentType == "ADVMER" && UserModel.shared.agentType == .advancemerchant{
                                //UserModel.shared.agentType = .advancemerchant
                            }else if kycAppControlInfo.first?.agentType == "DUMMY" && UserModel.shared.agentType == .dummymerchant{
                                // UserModel.shared.agentType = .dummymerchant
                            }else {
                                // if change the user type then this code will run - avaneesh
                                DispatchQueue.main.async {
                                    self.callUserUpdateAPI(type: kycAppControlInfo.first?.agentType ?? "SUBSBR")
                                }
                                return
                            }
                        }
                        /******************************************************/
                        bankRoutingapiversion = decRes.mobileAppVersion?.first?.bankRoutingapiversion ?? "0"
                        
                        if let bankApiVersion = userDef.value(forKey: "BankRoutingapiversion") as? Int {
                            if bankRoutingapiversion != "0", bankApiVersion != Int(bankRoutingapiversion ?? "0") {
                                self.checkVersionAndUpdateBankApiDB(version: Int(bankRoutingapiversion ?? "0") ?? 0)
                            }
                        } else {
                            self.checkVersionAndUpdateBankApiDB(version: Int(bankRoutingapiversion ?? "0") ?? 0)
                        }

                        self.setTabBarColor()
                        
                        println_debug(UserModel.shared.agentType)
                      
                        if updateKey == 0 || updateKey == 1 {
                            DispatchQueue.main.async {
                                alertViewObj.wrapAlert(title: "", body:"Please update the OK$ app for use".localized, img: #imageLiteral(resourceName: "alert-icon"))
                                if self.updateKey == 0 {
                                    alertViewObj.addActionPermanent(title: "OK", style: .target) {
                                        self.openOKDollarFromAppStore()
                                    }
                                } else {
                                    alertViewObj.addAction(title: "OK", style: .target) {
                                        self.openOKDollarFromAppStore()
                                    }
                                    alertViewObj.addAction(title: "Cancel", style: .cancel) {
                                    }
                                }
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                       
                       
                        if let version = decRes.version, version.count > 0 {
                            if let versionStr = version[0].validationJSON, versionStr != "null"  {
                                guard let jsonData = versionStr.data(using: .utf8) else { return }
                                let finalResponse = try decoder.decode(ValidationListResponse.self, from: jsonData)
                                if let code = finalResponse.code, code == 200 {
                                    if let validations = finalResponse.validations, validations.count > 0 {
                                        phNumValidationsApi = validations
                                    }
                                    if let locationVersion = userDef.value(forKey: "LocationupdateVersion") as? Int {
                                        if locationVersion != (kycAppControlInfo.first?.locationupdateVersion ?? 0) {
                                            //self.checkVersionAndUpdateDB()
                                        } else {
                                            //self.checkVersionAndUpdateDB()
                                        }
                                    } else {
                                        userDef.set(kycAppControlInfo.first?.locationupdateVersion ?? 0, forKey: "LocationupdateVersion")
                                        //self.checkVersionAndUpdateDB()
                                    }
                                    if let sndMoneyToBankVersion = userDef.value(forKey: "SendmoneyToBankVersion") as? Int {
                                        if sndMoneyToBankVersion != (kycAppControlInfo.first?.sendmoneyToBankVersion ?? 0) {
                                            self.updateSendMoneytoBankDB(version: kycAppControlInfo.first?.sendmoneyToBankVersion ?? 0)
                                        }
                                    } else {
                                        self.updateSendMoneytoBankDB(version: kycAppControlInfo.first?.sendmoneyToBankVersion ?? 0)
                                    }
                                }
                            }
                            //self.showPromotionPopup()
                        } else {
                            println_debug("No data from server")
                        }
                    }
                } else {
                    PTLoader.shared.hide()
                    println_debug("No specified key from server")
                }
            } catch _ {
                println_debug("Try error")
                PTLoader.shared.hide()
            }
        } else if screen == "UpdateBonusCount" {
            guard let castedData = json as? Data else {
                PTLoader.shared.hide()
                println_debug("Data error")
                return
            }
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(UpdateBonusCountResponse.self, from: castedData)
                if let sCode = response.statusCode, sCode == 200 {
                    println_debug("Bonus Count Updated")
                } else {
                    println_debug("Bonus Count Not Updated")
                }
            } catch _ {
                println_debug("Bonus Count Parsing Error")
                PTLoader.shared.hide()
            }
        }else if screen == "UpdateUser" {
            do {
                if let data = json as? Data {
                   // let response = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    //let dic = response as? Dictionary<String , Any>
                   // here we update the user profile by using the user mobile numnber - Avaneesh
                    profileObj.callUpdateProfileApi(aCode: UserModel.shared.mobileNo, handler: { (success) in
                                if success {
                                    userDef.set(true, forKey: keyLoginStatus)
                                    self.reloadDashBoard()
                                }else {
                                    self.reloadDashBoard()
                                }
                            })

                }
            }
        }
    }
    
    func reloadDashBoard() {
        DispatchQueue.main.async {
            if (refRecharge != nil) {
                refRecharge?.reloadSection()
            }
            if refLoyality != nil {
                refLoyality?.reload()
            }
            self.setTabBarColor()
        }
    }
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        PTLoader.shared.hide()
        if screenName == "PhNoValidation" {
            println_debug("Api Failure in PhNoValidation response")
        } else if screenName == "UpdateBonusCount" {
            println_debug("Api Failure in UpdateBonusCount response")
        } else if screenName == "UpdateUser" {
            print(withErrorCode)
        }
    }
    
    func callUserUpdateAPI(type : String) {
        if appDelegate.checkNetworkAvail() {
            let web = WebApiClass()
            web.delegate = self
            var urlString   = Url.updateUser
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
              let urlS = getUrl(urlStr: urlString, serverType: .cashInCashOutUrl)
            let param = bindCashInDataForUpdateCashIn(type: type)
            println_debug(urlS)
            println_debug(param)
            web.genericClass(url: urlS, param: param, httpMethod: "POST", mScreen: "UpdateUser")
        }else{
            alertViewObj.wrapAlert(title: "", body: "Network error and try again".localized, img: UIImage(contentsOfFile: "r_user"))
            alertViewObj.addAction(title: "OK".localized, style: .target , action: {
            })
            DispatchQueue.main.async {
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func bindCashInDataForUpdateCashIn(type: String) -> NSDictionary {
        let appVersion = String.init(format: "Version %@, Build (\(buildNumber))", Bundle.main.releaseVersionNumber)
        var c_code = UserModel.shared.countryCode
        if c_code.contains(find: "+") {
            c_code.removeFirst()
        }
        var mainDic = NSDictionary()
        var agent = 0
        var agentType = "0"
        var cashIn = true
        var cashOut = true
        if type == "" {
            agent = 0
            agentType = "0"
            cashIn = true
            cashOut = true
        }else if type == "AGENT" {
            agent = 1
            agentType = "2"
            cashIn = false
            cashOut = false
        }else if type == "BANKAG" {
            agent = 1
            agentType = "1"
            cashIn = true
            cashOut = true
        }else if  type == "MER"{
            agent = 2
            agentType = "2"
            cashIn = true
            cashOut = true
        }else if  type == "ADVMER"{
            agent = 4
            agentType = "2"
            cashIn = true
            cashOut = true
        }else if type == "DUMMY" {
          agent = 5
            agentType = "5"
            cashIn = true
            cashOut = true
        }else if  type == "SUBSBR"{
            agent = 6
            agentType = "6"
            cashIn = false
            cashOut = false
        }else if  type == "ONESTOP"{
            agent = 8
            agentType = "8"
            cashIn = true
            cashOut = true
        } else {
            agent = 8
            agentType = "8"
            cashIn = true
            cashOut = true
        }
        mainDic = ["CountryCode": c_code,"MobileNumber":UserModel.shared.mobileNo,"OpeningTime":"","ClosingTime":"","OkDollarBalance": UserLogin.shared.walletBal,"Latitude":  UserModel.shared.lat,"Longitude":  UserModel.shared.long,"CellId":"","CashIn": cashIn,"CashOut": cashOut, "AgentType": agentType ,"Type": String(agent),"IsOpenAlways":true,"AppVersion": buildVersion,"Category": UserModel.shared.businessCate,"SubCategory": UserModel.shared.businessType]
        return mainDic
    }
    
}

//MARK: - DTO
class UpdateBonusCountRequest: Codable {
    var userMobile: String?
    var userName: String?
    var countryCode: String?
    var bonusPoints: [BonusPointsData]?
    private enum CodingKeys: String, CodingKey {
        case userMobile = "user_Mobile"
        case userName = "user_Name"
        case countryCode = "country_Code"
        case bonusPoints = "bonusPoints"
    }
}

class BonusPointsData: Codable {
    var countryCode: String?
    var cellNumber: String?
    var bonusCount: String?
    var bonusPointName: String?
    var bonusPointNumber: String?
    var expiryDate: String?
    
    private enum CodingKeys: String, CodingKey {
        case countryCode = "countryCode"
        case cellNumber = "userCellNumber"
        case bonusCount = "userBonusCount"
        case bonusPointName = "userBonusPointName"
        case bonusPointNumber = "userBonusPointNumber"
        case expiryDate = "expiryDate"
    }
}

struct UpdateBonusCountResponse: Codable {
    var statusCode: Int?
    var status: Int?
    var comment: String?
    
    private enum CodingKeys: String, CodingKey {
        case statusCode = "StatusCode"
        case status = "Status"
        case comment = "Comment"
    }
}

extension DashboardVC{
    
    fileprivate func callNotificationForSideMenuDropDown(index: Int){
        let passIndex:[String: Int] = ["Title": index+1]
        NotificationCenter.default.post(name: NSNotification.Name("DashboardVC"), object: nil, userInfo: passIndex)
        sideview.isHidden = true
        fullview.isHidden = true
        compareStr = "0"
    }
    
    fileprivate func callNotificationForSideMenu(index: Int){
        let passIndex:[String: Int] = ["Title2": index+1]
        NotificationCenter.default.post(name: NSNotification.Name("DashboardVCsent"), object: nil, userInfo: passIndex)
        sideview.isHidden = true
         fullview.isHidden = true
        compareStr = "0"
    }
    
    private func searchTextChangeNotification(searchText: String, status: Bool) {
        userInfo.removeAll()
        userInfo["searchText"] = searchText
        userInfo["searchStatus"] = searchActive
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DashboardSearchTextChange"), object: nil, userInfo: userInfo)
    }
    
  
}


extension DashboardVC: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        searchActive = false
//        self.searchTextChangeNotification(searchText: "", status: false)
      //   inboxSearchBar.text = ""
        if indexToShowData == 2 {
            inboxSearchBar.text = ""
        }
       else if indexToShowData == 3 {
            inboxSearchBar.text = ""
        }
        inboxSearchBar.isHidden = true
        self.navigationItem.titleView = nil
        //   self.navigationItem.rightBarButtonItem  = nil
        self.navigationItem.leftBarButtonItem   = nil
        self.title = "Inbox".localized
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchActive = false
        self.searchTextChangeNotification(searchText: "", status: false)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        self.searchTextChangeNotification(searchText: "", status: false)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchTextChangeNotification(searchText: searchText, status: true)
    }
}

extension DashboardVC: SendDataDelegate {
    func sendData(text: String) {
        self.delegate?.sendData(text: text)
    }
    
    
}





