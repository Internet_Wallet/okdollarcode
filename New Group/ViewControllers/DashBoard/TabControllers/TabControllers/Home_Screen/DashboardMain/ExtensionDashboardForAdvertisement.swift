//
//  ExtensionDashboardForAdvertisement.swift
//  OK
//
//  Created by Tushar Lama on 11/03/2020.
//  Copyright © 2020 Vinod Kumar. All rights reserved.
//

import Foundation
import UIKit

extension DashboardVC{
    
    func advertiseViewComponents(){
        advertisementView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        advertisementView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        advertisementImageView.frame = CGRect(x: 20, y: 200, width: screenWidth - 40, height: screenHeight - 300)
        advertisementButton.frame = CGRect(x: advertisementImageView.frame.size.width - 20, y: 150, width: 30, height: 30)
        advertisementButton.setImage(UIImage(named: "close_overflow.png"), for: .normal)
        advertisementButton.layer.cornerRadius = 0.5 * advertisementButton.bounds.size.width
        advertisementButton.clipsToBounds = true
        advertisementButton.addTarget(self, action: #selector(thumbsUpButtonPressed), for: .touchUpInside)
        self.advertisementView.addSubview(self.advertisementButton)
        self.advertisementView.addSubview(self.advertisementImageView)
        //  self.view.bringSubviewToFront(self.advertisementButton)
    }
    
    func showadvertisement()  {
        
        //check if coming from appdelegate initial
        if UserDefaults.standard.bool(forKey: "isComingFromAppdelegate"){
            //setting the value to false because everytime dashboard will open
            UserDefaults.standard.set(false, forKey: "isComingFromAppdelegate")
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let firstDate = formatter.date(from: UitilityClass.getStringDateInCustomFormat(dateFormat: "yyyy-MM-dd"))
            let secondDate = formatter.date(from: validityDate ?? "")
            
           // UserDefaults.standard.set(0, forKey: "launchCount")
            
            if let _ = secondDate{
                if secondDate! >  firstDate!{
                    //keep the count continue
                    if let count = numberOfTimes{
                        let applicationCount = UserDefaults.standard.value(forKey: "launchCount") as! Int
                        if Int(count) ?? 0 < applicationCount{
                            //dont show imageview
                            showImageView = false
                        }else{
                            //show image view
                            showImageView = true
                        }
                    }
                }else{
                    //change the count to initial
                    //show image view
                    UserDefaults.standard.set(0, forKey: "launchCount")
                    showImageView = false
                }
                
                //code to show image
                if  showImageView{
                    showImageView = false
                    
                    //check here if you have valid url
                    if let imageURL = newImageURL{
                        //for the first time image url will be nill so implement here
                        if UserDefaults.standard.value(forKey: "imageURL") == nil{
                            UserDefaults.standard.set(imageURL, forKey: "imageURL")
                            //fire from here to show image
                            
                            if let isVisible = isVisible{
                                if isVisible == "True" {
                                    DispatchQueue.main.async {
                                        if let imageURL = self.newImageURL{
                                            self.loadImageUsingCache(withUrl: imageURL.replacingOccurrences(of: " ", with: "%20"))
                                        }
                                    }
                                }
                            }
                        }else{
                            //comparing to check if the url change or no
                            let url = UserDefaults.standard.value(forKey: "imageURL") as? String
                            
                            if imageURL != url{
                                UserDefaults.standard.removeObject(forKey: "imageURL")
                                UserDefaults.standard.removeObject(forKey: "image")
                                UserDefaults.standard.set(imageURL, forKey: "imageURL")
                                //again fire to show image
                                if let isVisible = isVisible{
                                    if isVisible == "True" {
                                        DispatchQueue.main.async {
                                            if let imageURL = self.newImageURL{
                                                self.loadImageUsingCache(withUrl: imageURL.replacingOccurrences(of: " ", with: "%20"))
                                            }
                                        }
                                    }
                                }
                            }else{
                                if UserDefaults.standard.value(forKey: "image") != nil{
                                    DispatchQueue.main.async {
                                        let dataImage = UserDefaults.standard.value(forKey: "image") as! Data
                                        self.advertisementImageView.image = UIImage(data: dataImage)
                                        self.window.addSubview(self.advertisementView)
                                    }
                                }
                            }
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.advertisementView.isHidden = true
                        self.advertisementView.removeFromSuperview()
                    }
                }
            }
        }
        
    }
}

extension DashboardVC {
    func loadImageUsingCache(withUrl urlString : String){
        let url = URL(string: urlString)
        
        // if not, download image from url
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }
            
            DispatchQueue.main.async {
                if let newData = data{
                    if let image = UIImage(data: newData) {
                        UserDefaults.standard.set(newData, forKey: "image")
                        self.advertisementImageView.image = image
                        self.advertisementView.isHidden = false
                        self.window.addSubview(self.advertisementView)
                    }
                }else{
                    self.advertisementView.removeFromSuperview()
                }
            }
            
        }).resume()
        
    }
}
