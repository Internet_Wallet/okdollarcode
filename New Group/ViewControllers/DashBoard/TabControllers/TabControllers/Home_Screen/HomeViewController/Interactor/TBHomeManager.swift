//
//  TBHomeManager.swift
//  OK
//
//  Created by Ashish on 1/11/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class TBHomeManager: NSObject {
    
    private var home : TBHomeViewController!
    
    
    // here are the controller use to append the data in dashboard
    
    var okWallet   : OKDollarWalletViewController!
    var bankService: BankServicesViewController!
    var cashIO     : CashIOPRViewController!
    var advImgs    : ScrollerAdvertisementViewController!
    var rechbill   : RechargeBillPayViewController!
    var loyaltyPromo : LoyalityPromotionHomeViewController!
    var bookOKD     : BookOnOKDollarViewController!

    
    func install(_ home: TBHomeViewController) -> Void {
        self.home     = home
        TBHomeNavigations.main.startSyncing(home)
    }
    
    func setAdvancedDelegate() {
//        cashIO.delegate       = self.home
        if UserModel.shared.agentType == .advancemerchant {
            rechbill.delegate     = self.home
        } else {
            rechbill.delegate     = self.home
            bookOKD.delegate      = self.home
            loyaltyPromo.delegate = self.home
        }
    }
    
    func syncronise() -> Void {
        
        if UserModel.shared.agentType == .advancemerchant {
            let rechargeBillPay = RechargeBillPayViewController()
            rechargeBillPay.tblHomeHeight = self.home.tableHome.frame
            rechbill = rechargeBillPay
            self.home.viewControllers.append(rechargeBillPay)
        } else {
            let ok_Wallet = OKDollarWalletViewController()
            okWallet = ok_Wallet
            self.home.defaultVCInDefaultView = OKDollarWalletViewController()
            self.home.viewControllers.append(ok_Wallet)
            
            let rechargeBillPay = RechargeBillPayViewController()
            rechbill = rechargeBillPay
            self.home.viewControllers.append(rechargeBillPay)
            
            let advImages = ScrollerAdvertisementViewController()
            advImgs  = advImages
            self.home.viewControllers.append(advImages)
            
            //        let bank_Serv = BankServicesViewController()
            //        bankService = bank_Serv
            //        self.home.viewControllers.append(bank_Serv)
            //
            //        let cashIN = CashIOPRViewController()
            //        cashIO = cashIN
            //        self.home.viewControllers.append(cashIN)
            
            let book = BookOnOKDollarViewController()
            bookOKD = book
            self.home.viewControllers.append(book)
            
            let loyalty  = LoyalityPromotionHomeViewController()
            loyaltyPromo = loyalty
            self.home.viewControllers.append(loyalty)
        }
        
        self.home.loadCell()
    }
}
