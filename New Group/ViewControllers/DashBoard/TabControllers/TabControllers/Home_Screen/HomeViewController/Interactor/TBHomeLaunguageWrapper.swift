//
//  TBHomeLaunguageWrapper.swift
//  OK
//
//  Created by Ashish on 1/18/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

enum OKLaunguage {
    case english, burmese, chinese
}

class TBHomeLaunguageWrapper: NSObject {

    var language : OKLaunguage = OKLaunguage.english
    
    func languageKeysUpdation(lang: OKLaunguage) {
        switch lang {
        case .english:
            break
        case .burmese:
            break
        case .chinese:
            break
        }
    }
}
