//
//  TBHomeViewController.swift
//  OK
//
//  Created by Ashish on 12/25/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit


protocol ShowHideTabBar {
    func isPlayingWithTabBar(hide: Bool)
}

class TBHomeViewController: OKBaseController, UIViewControllerTransitioningDelegate, UINavigationControllerDelegate {
    
    //MARK: - Outlet
    @IBOutlet var manager: TBHomeManager!
    @IBOutlet var tableHome: UITableView!
    @IBOutlet weak var viewDefault: UIView!
    @IBOutlet weak var constraintTopDefaultView: NSLayoutConstraint!
    
    //MARK: - Properties
    var viewControllers =  [UIViewController]()
    var cells           =  [UITableViewCell]()
    var tokenFullString = ""
    var scanDelegate: TBScanQRDelegate?
    var showScanQR: Bool = false
    var showNearBy: Bool = false
    var inBox: Bool = false
     var defaultVCInDefaultView: OKDollarWalletViewController!
    var showHideDelegate: ShowHideTabBar?
    var isPlayingWithTab = false
    var count = 0
    var entry = false
    
    
    private var spacingHeight: CGFloat = 20.00
    private lazy var modelCashInCashOut: CashInCashOutModel = {
        return CashInCashOutModel()
    }()
    private var hideConstants: CGFloat = 60
    private var isHideDefaultView: Bool {
        get {
            return constraintTopDefaultView.constant == -hideConstants ? true : false
        }
        set(newValue) {
            if newValue {
                UIView.transition(with: self.viewDefault,
                                          duration:0.325, animations: {
                       self.constraintTopDefaultView.constant = -self.hideConstants
                        self.view.layoutIfNeeded() }, completion: nil)
            } else {
                UIView.transition(with: self.viewDefault,
                                  duration:0.325, animations: {
                                    self.constraintTopDefaultView.constant = 0
                                    self.view.layoutIfNeeded() }, completion: nil)
            }
        }
    }
    
    //MARK: - Views life cycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        if UserModel.shared.agentType == .advancemerchant {
            tableHome.isScrollEnabled = false
        }
        else{
            tableHome.isScrollEnabled = true
        }
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToCICO(_:)), name: Notification.Name("PushToCICO"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToCICOPendingScreen(_:)), name: Notification.Name("PushToCICOPendingScreen"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToCICOCancelAccept(_:)), name: Notification.Name("PushToCICOCancelAccept"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToCICORatingScreen(_:)), name: Notification.Name("PushToCICOShowRatingScreen"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showCICOConfirmatioVC(_:)), name: Notification.Name("ShowCICOConfirmationScreen"), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(showCICOCancelAlert), name: Notification.Name("ShowCICOCancelAlert"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showCICORejectAlert), name: Notification.Name("ShowCICORejectAlert"), object: nil)
        modelCashInCashOut.cashInCashOutDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.scrolltoTop()
        if UserModel.shared.agentType == .advancemerchant {
            self.view.backgroundColor = .white
            self.tableHome.backgroundColor = .white
        }
        else{
            self.view.backgroundColor = kYellowColor
        }
        
       
//        self.tableHome.reloadData()
        if UserModel.shared.mobileNo != "" && ok_password != "" {
            profileObj.callLoginApiDashboard(aCode: UserModel.shared.mobileNo, withPassword: ok_password ?? "")
        }
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        appDel.floatingButtonControl?.window.isHidden = false
    }
    
    
    private func scrolltoTop() {
        tableHome.scrollToTop(animated: false)
        self.navigationController?.isNavigationBarHidden = false
        isHideDefaultView = true
        self.constraintTopDefaultView.constant = -self.hideConstants
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("TBHomeViewcontroller called======")
        super.viewDidAppear(animated)
        if self.showScanQR == true {
            self.showScanQR = false
            transition()
        } else if self.showNearBy == true {
            self.showNearBy = false
            pushToNearBy()
        }
        else if self.inBox{
            self.inBox = false
            presentInbox()
        } 
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        appDel.floatingButtonControl?.window.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //MARK: - Target Methods
    @objc func navigateToCICO(_ notification: Notification) {
        self.navigateToCashIN()
    }
    
    @objc func showCICORejectAlert() {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            alertViewObj.wrapAlert(title: "", body: "Your request is rejected by receiver".localized, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
            }
            alertViewObj.showAlert(controller: topController)
        }
        //showCICOToast(message: "Your request is rejected by receiver".localized)
    }
    
    //    @objc func showCICOCancelAlert() {
    //        if var topController = UIApplication.shared.keyWindow?.rootViewController {
    //            while let presentedViewController = topController.presentedViewController {
    //                topController = presentedViewController
    //            }
    //            alertViewObj.wrapAlert(title: "Transaction Cancelled".localized, body: "Request is cancelled by other user".localized, img: nil)
    //            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
    //            }
    //            alertViewObj.showAlert(controller: topController)
    //        }
    //        //showCICOToast(message: "Request cancelled from requester".localized)
    //    }
    
    @objc func navigateToCICOPendingScreen(_ notification : Notification) {
        if let vc = UIStoryboard(name: "CashInOutStoryboard", bundle: nil).instantiateViewController(withIdentifier: cashInOutViewControllers.viewControllers.cioTransHomeVC) as? CIOTransactionHomeViewController {
            vc.fromCancelAcceptScreen = "CancelAcceptScreen"
            let navigation = UINavigationController(rootViewController: vc)
            navigation.modalPresentationStyle = .fullScreen
            self.present(navigation, animated: true, completion: nil)
        }
    }
    
    @objc func showCICOConfirmatioVC(_ notification: Notification) {
        guard let userInfoDict = notification.userInfo else { return }
        if let reqId = userInfoDict["value"] as? String {
            modelCashInCashOut.getMasterDetailRequest(id: "\"\(reqId)\"")
        }
    }
    
    @objc func navigateToCICOCancelAccept(_ notification: Notification) {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            if let topVC = topController as? UINavigationController, let vc = topVC.visibleViewController, let _ = vc as? CICOCancelAcceptRequestViewController {
                println_debug("Same Screen")
            } else {
                let navigation = UINavigationController()
                guard let userInfoDict = notification.userInfo else { return }
                let storyBoard = UIStoryboard.init(name: "CICOMapAndPay", bundle: nil)
                guard let cancelAcceptVC = storyBoard.instantiateViewController(withIdentifier: "CICOCancelAcceptRequestViewController_ID") as? CICOCancelAcceptRequestViewController else { return }
                cancelAcceptVC.userInfoDict = userInfoDict
                navigation.viewControllers = [cancelAcceptVC]
                navigation.modalPresentationStyle = .fullScreen
                topController.present(navigation, animated: true, completion: nil)
            }
        }
    }
    
    @objc func navigateToCICORatingScreen(_ notification: Notification) {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            let navigation = UINavigationController()
            guard let userInfoDict = notification.userInfo else { return }
            guard let cioRatingtVC = UIStoryboard(name: "CashInOutStoryboard",
                                                  bundle: Bundle.main).instantiateViewController(withIdentifier: "CIORatingViewController_ID") as? CIORatingViewController else { return }
            cioRatingtVC.userInfoDict = userInfoDict
            cioRatingtVC.isPresented = true
            cioRatingtVC.fromUser = true
            navigation.viewControllers = [cioRatingtVC]
            navigation.modalPresentationStyle = .fullScreen
            topController.present(navigation, animated: true, completion: nil)
        }
    }
    
    //MARK: - Methods
    func pushToCICOConfirmationvc() {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            if let vc = UIStoryboard(name: "CashInOutStoryboard", bundle: nil).instantiateViewController(withIdentifier: "CIOConfimationViewController_ID") as? CIOConfimationViewController {
                vc.currentSelectedCashDetails = self.modelCashInCashOut.masterDetail
                vc.isPresented = true
                let navigation = UINavigationController(rootViewController: vc)
                navigation.modalPresentationStyle = .fullScreen
                topController.present(navigation, animated: true, completion: nil)
            }
        }
    }
    
    private func transition() {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromTop
        self.navigationController?.view.layer.add(transition, forKey: nil)
        guard let scanQRCode = self.storyboard?.instantiateViewController(withIdentifier: "TBScanToPayViewController") as? TBScanToPayViewController else { return }
        scanQRCode.navigation   = self.navigationController
        scanQRCode.scanDelegate = self
        scanQRCode.isFromTabBar = true
        scanQRCode.isFromSideMenu = false
        self.navigationController?.pushViewController(scanQRCode, animated: false)
    }
    
    private func pushToNearBy() {
        
        let agentsStory: UIStoryboard = UIStoryboard(name: "NBSMain", bundle: nil)
        guard let nearByVC = agentsStory.instantiateViewController(withIdentifier: "NearByServicesMainVC") as? NearByServicesMainVC else { return }
        nearByVC.isPushed = false
        nearByVC.modalPresentationStyle = .fullScreen
        self.navigationController?.present(nearByVC, animated: true, completion: nil)
//        self.navigationController?.pushViewController(nearByVC, animated: false)
    }
    
    private func presentInbox(){
        print("presentInbox called====")
     //   let obj  = storyboard?.instantiateViewController(identifier: "TBNotificationViewController")
       // let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let notificationController = storyboard?.instantiateViewController(withIdentifier: "TBNotificationViewController") as? TBNotificationViewController else { return }
        let navigation = UINavigationController(rootViewController: notificationController)
        navigation.modalPresentationStyle = .fullScreen
        notificationController.navigation = navigation
        notificationController.delegate = self as? InBoxightBarShow
        self.navigationController?.present(navigation, animated: true, completion: nil)
      //  self.navigationController?.pushViewController(notificationController, animated: false)

    }
    
    func setup() -> Void {
        self.view.backgroundColor = kYellowColor
        manager.install(self)
        manager.syncronise()
        manager.setAdvancedDelegate()
    }
    
    func loadCell() {
        for (index, _) in self.viewControllers.enumerated() {
            let cell = tableHome.dequeueReusableCell(withIdentifier: "cellIdentifier")
            let vc = self.viewControllers[index]
            vc.view.frame = cell!.bounds
            vc.view.layoutIfNeeded()
            cell?.addSubview(vc.view)
            cell?.layoutIfNeeded()
            self.cells.append(cell!)
        }
        if self.viewControllers.count > 0 {
            if let defaultVC = defaultVCInDefaultView {
                defaultVC.screenFor = .popDown
                defaultVC.view.frame = self.viewDefault.bounds
                defaultVC.view.layoutIfNeeded()
                self.viewDefault.addSubview(defaultVC.view)
                self.viewDefault.layoutIfNeeded()
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

var languageKeyDashboard = "LanguageChangeObservationForDashboard"

//MARK:- UITableView Datasource & Delegates
extension TBHomeViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.cells[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = self.viewControllers[indexPath.row].dHeight {
            return height
        }
        return 0.00
    }
}

// MARK: - CashInOutModel Delegate
extension TBHomeViewController: CashInOutModelDelegate {
    func apiResult(message: String?, statusCode: String?, type: CashInCashOutModel.CashInCashOutApiType) {
        switch type {
        case .getMasterDetails:
            println_debug("Master Detail")
            DispatchQueue.main.async {
                if let cashtype = self.modelCashInCashOut.masterDetail?.cashTransferMaster?.cashType, cashtype == 1 {
                    self.pushToCICOConfirmationvc()
                } else {
                    self.showToast(message: "Receiver arrived. Please wait for receiver to complete payment".localized, align: .center)
                }
            }
        default:
            break
        }
        
    }
    
    func handleCustomError(error: CustomError, type: CashInCashOutModel.CashInCashOutApiType) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error, \(error.rawValue)")
        }
    }
}

//MARK: - TBScanQRDelegate
extension TBHomeViewController: TBScanQRDelegate {
    func moveToPayto(withScanObject object: PTScanObject, fromTransfer: Bool? = false) {
       
        if let delegate = self.scanDelegate {
            delegate.moveToPayto(withScanObject: object, fromTransfer: fromTransfer)
        } else {
            self.navigateToPayto(phone: object.agentNo, amount: object.amount, name: object.name, remark: object.remarks, fromMerchant: true, fromTranfer: fromTransfer, cashInQR: object.cashInQR, referenceNumber: object.referenceNumberMIT)
        }
    }
    
    func moveToPayTo(fromTransfer: Bool? = false) {
      
        if let delegate = self.scanDelegate {
            delegate.moveToPayTo(fromTransfer: fromTransfer)
        } else {
            self.navigatetoPayto(fromMerchant: true, fromTranfer: fromTransfer)
        }
    }
}

//MARK: - TBHomeReloadingDelegate
extension TBHomeViewController: TBHomeReloadingDelegate {
    func refreshContentHomeTab(_ indexPath: IndexPath)  {
        for index in 0 ... self.viewControllers.count - 1 {
            let cell   = self.cells[index]
            cell.frame = CGRect.init(x: 0, y: 0, width: cell.bounds.width, height: self.viewControllers[index].dHeight!)
            cell.layoutIfNeeded()
            cell.layoutSubviews()
            
            UIView.setAnimationsEnabled(false)
            self.tableHome.beginUpdates()
            self.tableHome.layoutIfNeeded()
            self.tableHome.layoutSubviews()
            self.tableHome.endUpdates()
            self.tableHome.scrollToNearestSelectedRow(at: .none, animated: false)
            UIView.setAnimationsEnabled(true)
                        
        }
        
    }
}

//MARK: - LanguageChangeActionDashboardUpdate
extension TBHomeViewController: LanguageChangeActionDashboardUpdate {
    func resetDashboardLanguage() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: languageKeyDashboard), object: nil, userInfo: nil)
    }
}

//MARK: - UIScrollViewDelegate
extension TBHomeViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if UserModel.shared.agentType != .advancemerchant {
            if scrollView.contentOffset.y < 215 {
                if !isHideDefaultView {
                    isHideDefaultView = true
                }
            }else{
                if isHideDefaultView {
                    isHideDefaultView = false
                }
            }
        }
    }
    
   
    
}






 
    


