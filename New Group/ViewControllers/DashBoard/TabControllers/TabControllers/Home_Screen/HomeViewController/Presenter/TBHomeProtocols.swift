//
//  TBHomeProtocols.swift
//  OK
//
//  Created by Ashish on 1/11/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class TBHomeProtocols: NSObject {

}

enum HomeNavigationActions {
    case payto, transferTo, instaPay, cashInPayto, cashOutPayto, addWithdraw, bankDepositVerification, bankCounterDeposit, nearbyOKServices, okChat, cashIN, cashOUT, ReqMoney, BillSpliter, resale, mobileNumber, otherNumber, OverseasTopUp, landlineBill, postpaidMobile, ThaiMobRecharge, ChinaMobRecharge, BuyBonusPoint, skynet, airtel, dishTv, sunDirect, reliance, videocon, tataSky, dishHome, electricity, taxPayment, loyalty, promotions, luckyDrawCoupon, flight, hotel, intercityBus, train, bus, toll, ferry, voucher, foodwallet, merchantPayment, mobileElectronics , offers, voting, solarElectricity, donation, earnPointTransfer, redeemPoint, sunKingPayment, buyTopupPoint, bonusPointResale, insurance,onestopmart, sendMoneyToBank,taxPaymentNew , lottery , nearMe, scanPay
}

protocol TBHomeReloadingDelegate {
    func refreshContentHomeTab(_ indexPath: IndexPath)
}
