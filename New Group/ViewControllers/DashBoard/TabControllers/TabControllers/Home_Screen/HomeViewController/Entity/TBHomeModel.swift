//
//  TBHomeModel.swift
//  OK
//
//  Created by Ashish on 1/11/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class TBHomeModel {
    
    static let cellDetails = String.init(describing: CollectionViewCell.self)
    static let okWalletCellDetails = String.init(describing: OkWalletCollectionViewCell.self)
   // C: Subh : new changes
    public struct OKDollarWallet {
        static var dataArray = [ "Pay / Send", "Face_Home_Titlte", "Near me Payment", "Cash In", "Cash Out", "Request Money"]
        static var imgArray = [ #imageLiteral(resourceName: "dashboard_pay_send"), "dashboard_facePay", "NearMeNew", "dashboard_cash_in_payto", "dashboard_cash_out_payto", #imageLiteral(resourceName: "dashboard_requestmoney")] as [Any]
    }

    
//    public struct OKDollarWallet {
//         static var dataArray = [ "Pay / Send", "Face_Home_Titlte", "Instant Pay", "Cash In", "Cash Out", "Request Money"]
//         static var imgArray = [ #imageLiteral(resourceName: "dashboard_pay_send"), "dashboard_facePay", "dashboard_instapay", "dashboard_cash_in_payto", "dashboard_cash_out_payto",  "dashboard_cash_out_payto", #imageLiteral(resourceName: "dashboard_requestmoney")] as [Any]
//     }

    public struct CashPaymentRequest {
        
        enum CashINOUTPR {
            case cash
        }
        
        static var dataArray = ["Request Cash In", "Request Cash Out", "OK$ Chat", "Nearby OK$ Services"]
        static var imgArray  = [#imageLiteral(resourceName: "dashboard_cashin"),#imageLiteral(resourceName: "dashboard_cashout"),#imageLiteral(resourceName: "dashboard_chat"), #imageLiteral(resourceName: "dashboard_nearby")] as [Any]
        
        static var subDataArray = [String]()
        static var subImgArray  = [Any]()
        
        static var cashINData = [String]()
        static var cashINImg  = [Any]()
        

        
        static func switchDataType(_ enumType: CashINOUTPR) {
            switch enumType {
            case .cash:
                CashPaymentRequest.subDataArray = CashPaymentRequest.cashINData
                CashPaymentRequest.subImgArray  = CashPaymentRequest.cashINImg
                break
            }
        }
    }
    
    public struct RechargeBillPay {
        
        enum RechargeBillPayEnum {
            case recharge, dth, electricity, goAuto
        }

        static var dataArrayMain = [ "Other Number", "Mobile Number", "Re-Sale", "Voucher", "Landline Bill", "Postpaid Mobile", "Buy OK$ Point", "Electricity", "DTH"]
        static var imgArrayMain  = ["dashboard_other_number" ,"dashboard_my_number", "dashboard_re_sale" , "voucher", "dashboard_landline_bill", "dashboard_postpaid_mobile","dashboard_buybonuspoint", "eb_solar@3x.gif","r_dth"] as [String]
        
        static var dataArrayMainOverSeas = ["Other Number", "Mobile Number", "Re-Sale" , "Voucher", "Landline Bill", "Postpaid Mobile", "International Top-Up", "Buy OK$ Point", "Electricity"]
          static var imgArrayMainSeas  = ["dashboard_other_number","dashboard_my_number","dashboard_re_sale","voucher", "dashboard_landline_bill","dashboard_postpaid_mobile","dashboard_overseas_recharge","dashboard_buybonuspoint", "eb_solar@3x.gif"] as [String]
        
        static var dataArrayMainSecond = ["DTH", "Bill Splitter Tab", "Merchant Payment","Instant Pay","Tax Payment","Lottery"]
        static var imgArrayMainSecond = ["r_dth","dashboard_bill_splitter", "dashboard_merchant_payment","dashboard_instapay", "tax_Logo","tax_Logo"] as [String]
        
        static var dataArray = [ "Merchant Payment","Instant Pay","Tax Payment","Lottery"]
        static var imgArray  = ["dashboard_merchant_payment","dashboard_instapay","tax_Logo" ,"tax_Logo"] as [String]
        
        static var dataArrayLast = [Any ]()
        static var imgArrayLast  = [Any ]()
        
        static var subDataArray = [String]()
        static var subImgArray  = [Any]()
        
        
        
        static var advanceMerchantArray = ["Pay / Send", "Request Money", "Bill Splitter Tab", "Go-Auto Ticket", "1 Stop Mart Online", "Mobile & Electronics" ]
        static var advanceMerchantImageArray = [#imageLiteral(resourceName: "dashboard_pay_send"), #imageLiteral(resourceName: "dashboard_requestmoney"), #imageLiteral(resourceName: "dashboard_bill_splitter"), "dashboard_go_autoticket.gif",#imageLiteral(resourceName: "onestopmart"), "mobile_shopping_new.gif"] as [Any]
        
        static var advanceMerchantSecondArray = ["Send Money to Bank", "Buy & Sale Bonus Point Tab","Bonus Point Setup Tab", "Lucky draw & Coupon", "Earning Point Transfer Tab", "Redeem Bonus Point Tab", "Voting"]
        static var advanceMerchantSecondImageArray = ["sendMoneyToBank_dashboard", "dashboard_buy_sale", "merchant_confi.png", #imageLiteral(resourceName: "dashboard_lucky_draw") , #imageLiteral(resourceName: "dashboardEPT"), #imageLiteral(resourceName: "dashboardRP"),  #imageLiteral(resourceName: "voting_dashboard") ] as [Any]
        
        static var advanceMerchantGoAutoArray = ["Flights", "Hotel", "Intercity Bus"]
        static var advanceMerchantGoAutoImageArray = [#imageLiteral(resourceName: "dashboard_flight"),#imageLiteral(resourceName: "dashboard_hotel"),#imageLiteral(resourceName: "dashboard_intercity_bus")] as [Any]
        
        static var rechargeDataArray = ["International Top-Up", "Thai Mobile Recharge", "China Mobile Recharge"]
        static var rechargeImgArray = [#imageLiteral(resourceName: "dashboard_overseas_recharge"), #imageLiteral(resourceName: "dashboard_thai_recharge"),#imageLiteral(resourceName: "dashboard_china_recharge")]
        
        static var dthDataArray = ["Skynet", "Airtel", "Dish Tv", "Sun Direct", "Reliance", "Videocon", "Tata Sky", "Dish Home"]
        static var dthImgArray  = [#imageLiteral(resourceName: "dashboard_skynet"),#imageLiteral(resourceName: "dashboard_airtel"),#imageLiteral(resourceName: "dashboard_dishtv"),#imageLiteral(resourceName: "dashboard_sun_direct"),#imageLiteral(resourceName: "dashboard_reliance"),#imageLiteral(resourceName: "dashboard_videocon"),#imageLiteral(resourceName: "dashboard_tatasky"),#imageLiteral(resourceName: "dashboard_dishhome")]
        
        static var electricityDataArray = ["Electricity", "Solar Home", "Sun King Payment"]
        static var electricityImgArray  = ["dashboard_electricity","soloar_home", "dashboard_sunking"] as [Any]
        
        static func switchData(_ enumType: RechargeBillPayEnum) {
            switch enumType {
            case .dth:
//                RechargeBillPay.subDataArray = dthDataArray
//                RechargeBillPay.subImgArray  = dthImgArray
                break
            case .recharge:
//                RechargeBillPay.subDataArray = rechargeDataArray
//                RechargeBillPay.subImgArray  = rechargeImgArray
                break
            case .electricity:
                RechargeBillPay.subDataArray = electricityDataArray
                RechargeBillPay.subImgArray  = electricityImgArray
                break
            case .goAuto:
                RechargeBillPay.subDataArray = advanceMerchantGoAutoArray
                RechargeBillPay.subImgArray  = advanceMerchantGoAutoImageArray
            }
        }
        
    }
    
    public struct LoyaltyPromotions {
    //Hiding 5 modules Start ----->
        
//        static var dataArrayPersonal = ["Buy & Sale Bonus Point Tab",  "Lucky draw & Coupon", "Earning Point Transfer Tab", "Redeem Bonus Point Tab", "Voting", "Offers"]
//        static var imgArrayPersonal = ["dashboard_buy_sale",#imageLiteral(resourceName: "dashboard_lucky_draw"), #imageLiteral(resourceName: "dashboardEPT"), #imageLiteral(resourceName: "dashboardRP"), #imageLiteral(resourceName: "voting_dashboard") , #imageLiteral(resourceName: "NoOffersFound") ] as [Any]
//
//        static var dataArray = ["Buy & Sale Bonus Point Tab","Bonus Point Setup Tab", "Lucky draw & Coupon", "Earning Point Transfer Tab", "Redeem Bonus Point Tab", "Voting", "Offers"]
//        static var imgArray = ["dashboard_buy_sale", "merchant_confi.png", #imageLiteral(resourceName: "dashboard_lucky_draw") , #imageLiteral(resourceName: "dashboardEPT"), #imageLiteral(resourceName: "dashboardRP"),  #imageLiteral(resourceName: "voting_dashboard") , #imageLiteral(resourceName: "NoOffersFound")] as [Any]
        
        
        static var dataArrayPersonal = ["Redeem Bonus Point Tab"]
        static var imgArrayPersonal = [ #imageLiteral(resourceName: "dashboardRP") ] as [Any]
        static var dataArray = ["Bonus Point Setup Tab", "Redeem Bonus Point Tab"]
        static var imgArray = ["merchant_confi.png", #imageLiteral(resourceName: "dashboardRP")] as [Any]
        
        //Hiding 5 modules End ----->
        
        static var subDataArray = [""]
        static var subImgArray = [""]
    }
    
    
    public struct BankServices {
        static var dataArray = ["Add / Withdraw", "Merchant Payment" , "Bank Counter Deposit", "Donation", "Insurance"]
        static var imgArray = [ #imageLiteral(resourceName: "dashboard_add_withdraw"), #imageLiteral(resourceName: "dashboard_merchant_payment"), #imageLiteral(resourceName: "dashboard_bank_counter"), #imageLiteral(resourceName: "dashboardDonation"), "dashboard_insurence"] as [Any]
        
        static var subDataArray = [""]
        static var subImgArray = [""]
    }
    
    
// Data Initilization for Book On OKDOLlar
    public struct BookOnOKDollar {
        
        enum BookOKDollarEnum {
            case goAuto
        }
 
        static var firstDataArray = ["Go-Auto Ticket", "1 Stop Mart Online", "Insurance"]
        static var firstImgArray = ["dashboard_go_autoticket.gif","onestopmart", "dashboard_insurence"]
        
        static var dataArrayNoMer = ["Insurance"]
        static var imgArrayNoMer  = ["dashboard_insurence"]  as [Any]
        
        static var subDataArray = [String]()
        static var subImgArray  = [Any]()

        static var goAutoTicketDataArr = ["Flights", "Hotel", "Intercity Bus"]
        static var goAutoTicketImgArr  = [#imageLiteral(resourceName: "dashboard_flight"),#imageLiteral(resourceName: "dashboard_hotel"),#imageLiteral(resourceName: "dashboard_intercity_bus")]
                
      static  func changeData(WithEnum type:BookOKDollarEnum) {
            switch type {
            case .goAuto:
                BookOnOKDollar.subDataArray = BookOnOKDollar.goAutoTicketDataArr
                BookOnOKDollar.subImgArray  = BookOnOKDollar.goAutoTicketImgArr
                break
            }
        }

    }
    
}

