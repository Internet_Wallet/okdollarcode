//
//  OKHomeKeys.swift
//  OK
//
//  Created by Ashish on 1/18/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

struct OKHomeKeys {
    
    var paysend : String!
    var addWithdraw : String!
    var bankDepositVerification : String!
    var bankCounterDeposit: String!
    var nearbyOKDollar: String!

}
