//
//  BookOnOKDollarViewController.swift
//  OK
//
//  Created by Ashish on 1/15/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BookOnOKDollarViewController: UIViewController {

    var containsValue: (Bool, Int) = (false, 0)
    @IBOutlet var titleHeader: UILabel!{
        didSet {
            titleHeader.font = UIFont(name: appFont, size: 13.0)
        }
    }
    @IBOutlet var mainCollectionView: UICollectionView!
    @IBOutlet var subCollectionView: UICollectionView!
    @IBOutlet var mainCollectionView_second: UICollectionView!
    
    @IBOutlet var sub_HeightConstraints: NSLayoutConstraint!
    @IBOutlet var mainCollectionheight: NSLayoutConstraint!
    @IBOutlet var mainCollectionheight_Second: NSLayoutConstraint!
    var delegate : TBHomeReloadingDelegate?
    
    var firstDataArray = ["Go-Auto Ticket", "1 Stop Mart Online", "Insurance"]
    //var firstImgArray = ["dashboard_go_autoticket.gif","taxi.gif","dashboard_insurence"]
     
    var dataArrayNoMer = ["1 Stop Mart Online"]
    var imgArrayNoMer  = ["onestopmart"]  as [Any]
    
    
    var hideSubCollection : Bool = true {
        didSet {
            if hideSubCollection {
                mainCollectionheight.constant = 110
                sub_HeightConstraints.constant = 0
                self.subCollectionView.isHidden = true
                mainCollectionheight_Second.constant = 0
                self.dHeight = 130.00
            } else {
                mainCollectionheight.constant = 110
                sub_HeightConstraints.constant = 105
                self.subCollectionView.isHidden = false
                mainCollectionheight_Second.constant = 0
                self.dHeight = 245.00 + 0
            }
        }
    }
    
    var lastSelectedString : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refBoolOKDollar = self
        self.lastSelectedString = ""
        containsValue = (false, 1)
        
        self.titleHeader.text = "Book On OK$".localized
        self.titleHeader.textColor = UIColor.init(hex: "0321AA")

        hideSubCollection = true
        
        self.mainCollectionView.register(UINib(nibName: TBHomeModel.cellDetails, bundle: nil), forCellWithReuseIdentifier: TBHomeModel.cellDetails)
        self.mainCollectionView_second.register(UINib(nibName: TBHomeModel.cellDetails, bundle: nil), forCellWithReuseIdentifier: TBHomeModel.cellDetails)
        self.subCollectionView.register(UINib(nibName: TBHomeModel.cellDetails, bundle: nil), forCellWithReuseIdentifier: TBHomeModel.cellDetails)

        self.mainCollectionView.delegate   = self
        self.mainCollectionView.dataSource = self
        
        self.subCollectionView.delegate   = self
        self.subCollectionView.dataSource = self
        
        self.mainCollectionView_second.delegate   = self
        self.mainCollectionView_second.dataSource = self

        NotificationCenter.default.addObserver(self, selector: #selector(BookOnOKDollarViewController.languageChangeObservation), name: NSNotification.Name(rawValue: languageKeyDashboard), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        appDel.floatingButtonControl?.window.isHidden = false
        self.titleHeader.font = UIFont(name: appFont, size: 13.0)
    }
    
    func reloadCollection() {
        DispatchQueue.main.async {
            if isMobileElectronicsShow {
                self.dataArrayNoMer.removeAll()
                self.imgArrayNoMer.removeAll()
                
                self.dataArrayNoMer = ["Mobile & Electronics","1 Stop Mart Online"]
                self.imgArrayNoMer  = ["mobile_shopping_new.gif","onestopmart"]  as [Any]
                
                self.mainCollectionheight.constant = 110
                self.mainCollectionheight_Second.constant = 110
                self.sub_HeightConstraints.constant = 0
                self.subCollectionView.isHidden = false
                self.dHeight = 245.00
            }else {
                self.dataArrayNoMer.removeAll()
                self.imgArrayNoMer.removeAll()
                
                self.dataArrayNoMer = ["1 Stop Mart Online"]
                self.imgArrayNoMer  = ["onestopmart"]  as [Any]
                
                self.mainCollectionheight.constant = 110
                self.mainCollectionheight_Second.constant = 110
                self.sub_HeightConstraints.constant = 0
                self.subCollectionView.isHidden = false
                self.dHeight = 245
            }
        //    self.mainCollectionView.reloadData()
            self.mainCollectionView_second.reloadData()
        }
    }
    
    @objc func languageChangeObservation() {
        
        self.titleHeader.text = "Book On OK$".localized
        self.titleHeader.font = UIFont(name: appFont, size: 13.0)

//            for (index, _) in dataArrayNoMer.enumerated() {
//                if dataArrayNoMer.count > index  {
//                    if  let cell = self.mainCollectionView_second.cellForItem(at: IndexPath.init(row: index, section: 0)) as? CollectionViewCell {
//                        cell.titleLabel.text = dataArrayNoMer[index] .localized
//                        cell.titleLabel.font = UIFont(name: appFont, size: 13.0)
//                    }
//                }
//            }
        
        for (index, _) in firstDataArray.enumerated() {
            if firstDataArray.count > index  {
                if  let cell = self.mainCollectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? CollectionViewCell {
                    cell.titleLabel.text = firstDataArray[index] .localized
                    cell.titleLabel.font = UIFont(name: appFont, size: 13.0)
                }
            }
        }
        
        for (index, _) in TBHomeModel.BookOnOKDollar.subDataArray.enumerated() {
            if TBHomeModel.BookOnOKDollar.subDataArray.count > index  {
                if  let cell = self.subCollectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? CollectionViewCell {
                    cell.titleLabel.text = TBHomeModel.BookOnOKDollar.subDataArray[index] .localized
                    cell.titleLabel.font = UIFont(name: appFont, size: 13.0)
                }
            }
        }


    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


extension BookOnOKDollarViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == mainCollectionView {
            return firstDataArray.count
        }
//        else if collectionView == mainCollectionView_second {
//            return dataArrayNoMer.count
//        }
        else {
            return TBHomeModel.BookOnOKDollar.subDataArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TBHomeModel.cellDetails, for: indexPath) as? CollectionViewCell
        if collectionView == mainCollectionView {
            cell?.wrapDataWithStaticImages(firstDataArray[indexPath.row], TBHomeModel.BookOnOKDollar.firstImgArray[indexPath.row])
        }
//        else  if collectionView == mainCollectionView_second {
//            cell?.wrapDataWithStaticImages(dataArrayNoMer[indexPath.row], imgArrayNoMer[indexPath.row])
//        }
        else {
            cell?.wrapDataWithStaticImages(TBHomeModel.BookOnOKDollar.subDataArray[indexPath.row], TBHomeModel.BookOnOKDollar.subImgArray[indexPath.row])
        }
        return cell!
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: screenWidth / 3.2, height: 105.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        for (index, _) in firstDataArray.enumerated() {
            if firstDataArray.count > index  {
                if  let cell = self.mainCollectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? CollectionViewCell {
                    cell.backgroundColor = .clear
                }
            }
        }

//            for (index, _) in dataArrayNoMer.enumerated() {
//                if dataArrayNoMer.count > index  {
//                    if  let cell = self.mainCollectionView_second.cellForItem(at: IndexPath.init(row: index, section: 0)) as? CollectionViewCell {
//                        cell.backgroundColor = .clear
//                    }
//                }
//        }
        
        if let cell = collectionView.cellForItem(at: indexPath) as? CollectionViewCell {
            
            if let text = cell.matchingText {
                
                switch text {
                    
                case "Go-Auto Ticket":
                    
                    if lastSelectedString == "Go-Auto Ticket" {
                        hideSubCollection = true
                        self.subCollectionView.reloadData()
                        if let delegate = delegate {
                            let index = IndexPath.init(row: 1, section: 0)
                            delegate.refreshContentHomeTab(index)
                        }
                        lastSelectedString = ""
                      //  self.mainCollectionView_second.reloadData()
                    } else {
                        cell.backgroundColor = UIColor.init(hex: "EBEBF1")
                     
                        hideSubCollection = false
                        TBHomeModel.BookOnOKDollar.changeData(WithEnum: TBHomeModel.BookOnOKDollar.BookOKDollarEnum.goAuto)
                        
                        UIView.animate(withDuration: 0.4, animations: {
                            self.view.layoutIfNeeded()
                        })
                        
                        self.subCollectionView.reloadData()
                        if let delegate = delegate {
                            let index = IndexPath.init(row: 1, section: 0)
                            delegate.refreshContentHomeTab(index)
                        }
                        lastSelectedString = "Go-Auto Ticket"
                        //self.mainCollectionView_second.reloadData()
                    }
                    break
                case "Flights":
                    TBHomeNavigations.main.navigate(.flight)
                    break
                case "Hotel":
                    TBHomeNavigations.main.navigate(.hotel)
                    break
                case "Intercity Bus":
                    TBHomeNavigations.main.navigate(.intercityBus)
                    break
//                case "Live Taxi":
//                    TBHomeNavigations.main.navigate(.LiveTaxi)
//                    break
                case "Train":
                    TBHomeNavigations.main.navigate(.train)
                    break
                case "Food Wallet":
                    TBHomeNavigations.main.navigate(.foodwallet)
                    break
                case "Mobile & Electronics":
                    TBHomeNavigations.main.navigate(.mobileElectronics)
                    break
                case "1 Stop Mart Online":
                    TBHomeNavigations.main.navigate(.onestopmart)
                    break
                case "OK$ Chat":
                    TBHomeNavigations.main.navigate(.okChat)
                case "Nearby OK$ Services":
                    TBHomeNavigations.main.navigate(.nearbyOKServices)
//                case "Promotions Tab":
//                    TBHomeNavigations.main.navigate(.promotions)
                case "Merchant Payment":
                    TBHomeNavigations.main.navigate(.merchantPayment)
                case "Donation":
                    TBHomeNavigations.main.navigate(.donation)
                case "Insurance":
                    TBHomeNavigations.main.navigate(.insurance)
                case "Offers":
                    TBHomeNavigations.main.navigate(.offers)
                default:
                    break
                }
            }
        }
    }
}

