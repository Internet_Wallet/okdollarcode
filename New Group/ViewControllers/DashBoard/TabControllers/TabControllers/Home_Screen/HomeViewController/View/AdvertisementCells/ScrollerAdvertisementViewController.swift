//
//  ScrollerAdvertisementViewController.swift
//  OK
//
//  Created by Ashish on 1/12/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import SDWebImage

class ScrollerAdvertisementViewController: OKBaseController {
    
    @IBOutlet var scroll: UIScrollView!
    var defaults = UserDefaults.standard
    var timer : Timer?
    var xOffSet: CGFloat  = 0
    var seconds : CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dHeight = 150.00
            self.timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.onTimer), userInfo: nil, repeats: true)
        if let date =  userDef.object(forKey: "calledInLastDay") as? Date  {
            if !self.compareDate(date1: date, date2: Date()) {
                self.apiForAdvertisement()
            } else {
                self.updateScrollFromDataBase()
            }
        } else {
            self.apiForAdvertisement()
        }
    }
    
    @objc func onTimer() {
        if timer == nil { return }
        if scroll.contentOffset.x < (self.scroll.contentSize.width - self.scroll.frame.width) {
            UIView.animate(withDuration: 1.0, animations: {
                self.scroll.contentOffset = CGPoint(x: self.scroll.contentOffset.x + self.scroll.frame.width, y: 0)
            })
        } else {
            UIView.animate(withDuration: 0, animations: {
                self.scroll.contentOffset = CGPoint(x: 0 , y: 0)
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        appDel.floatingButtonControl?.window.isHidden = false
    }
    
    func compareDate(date1:Date, date2:Date) -> Bool {
        let order = NSCalendar.current.compare(date1, to: date2, toGranularity: .day)
        switch order {
        case .orderedSame:
            return true
        default:
            return false
        }
    }
    
    func apiForAdvertisement() {
        if appDelegate.checkNetworkAvail() {
            let url = URL.init(string: "http://advertisement.api.okdollar.org/AdService.svc/GetAdvertisements")//"http://120.50.43.152:8002/AdService.svc/GetAdvertisements")
            let pram = Dictionary<String, Any>()
            TopupWeb.genericClassWithoutLoader(url: url!, param: pram as AnyObject, httpMethod: "GET", handle: { (response, success) in
                if success == false { return }
                if let dataDict = response as? Dictionary<String,AnyObject> {
                    if let dic = dataDict["Data"] as? String {
                        _ =   dic.replacingOccurrences(of: "\\", with: "")
                        if let arr = OKBaseController.convertToArrDictionary(text: dic) {
                            advImagesUrl.ImageURLArray.removeAll()
                            PaymentVerificationManager.deleteAllAdvertisement()
                            DispatchQueue.main.async {
                                for object in arr {
                                    if let obj = object as? Dictionary<String,Any> {
                                        var str = obj["ImagePath"] as? String
                                        str = str?.replacingOccurrences(of: "%2520", with: " ")
                                        str = str?.replacingOccurrences(of: "%20", with: " ")
                                        str = str?.replacingOccurrences(of: "%25", with: " ")
                                        let position = obj["Position"] as? String
                                        
                                        if position == "dashboard" {
                                        if let encoded = str?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed), let url = URL(string: encoded)
                                        {
                                            println_debug(url)
                                            advImagesUrl.ImageURLArray.append(url)
                                        }
                                    } else if position == "login" {
                                            if let encoded = str?.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed), let url = URL(string: encoded)
                                            {
                                                println_debug(url)
                                                advImagesUrl.loginImageArray.append(url)
                                            }
                                        }
                                        
                                        else {
                                            println_debug("No Images")
                                        }
                                    }
                                }
                                
                                userDef.set(Date(), forKey: "calledInLastDay")
                                self.updateScroll()
                            }
                        }
                    }
                }
            })
            
        }
    }

    func updateScrollFromDataBase() {
        for views in self.scroll.subviews {
            views.removeFromSuperview()
        }
        PaymentVerificationManager.getAllAdImageFromDB{ (status, array) in
            if status, let imageData = array {
                DispatchQueue.main.async(execute: {
                    for (index, image) in imageData.enumerated() {
                        var imageView = UIImageView()
                        var frame : CGRect = self.scroll.bounds
                        frame.origin.x = CGFloat(index) * screenWidth
                        imageView  = UIImageView.init(frame: frame)
                        imageView.contentMode = .scaleToFill
                        if let data = image.imageData {
                            imageView.image = UIImage(data: data)
                            advImagesUrl.AdImageData.append(data)
                        }
                        self.scroll.addSubview(imageView)
                        self.scroll.contentSize = CGSize.init(width: CGFloat(imageData.count) * screenWidth, height: self.scroll.bounds.size.height)
                    }
                    self.timer?.fire()
                })
            }
        }
        
    }
    

    
    func updateScroll() {
        for views in self.scroll.subviews {
            views.removeFromSuperview()
        }
        for (index, element) in advImagesUrl.ImageURLArray.enumerated() {
            var imageView = UIImageView()
            var frame : CGRect = self.scroll.bounds
            frame.origin.x = CGFloat(index) * screenWidth
            imageView  = UIImageView.init(frame: frame)
            imageView.contentMode = .scaleToFill

            PaymentVerificationManager.getAdImageFromDB(imageUrl: element.path) { (status, data) in
                if status == true, let imageData = data {
                    imageView.image = UIImage(data: imageData)
                    advImagesUrl.AdImageData.append(imageData)
                } else {
                    self.getImageFromURL(imageURL: element, imageView: imageView)
                }
            }
            self.scroll.addSubview(imageView)
            self.scroll.contentSize = CGSize.init(width: CGFloat(advImagesUrl.ImageURLArray.count) * screenWidth, height: self.scroll.bounds.size.height)
        }
        timer?.fire()
    }
}




