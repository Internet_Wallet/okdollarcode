//
//  RechargeBillPayViewController.swift
//  OK
//
//  Created by Ashish on 1/12/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class RechargeBillPayViewController: UIViewController {
    
    @IBOutlet var titleHeader : UILabel!{
        didSet{
            titleHeader.font = UIFont(name: appFont, size: 13.0)
        }
    }
    var containsValue: (Bool, Int) = (false, 0)
    
    @IBOutlet weak var mainCollectionView_first: UICollectionView!
    @IBOutlet weak var subCollectionView_first: UICollectionView!
    @IBOutlet weak var height_SubCollectionView_first: NSLayoutConstraint!
    @IBOutlet weak var maincollectionHeight_first: NSLayoutConstraint!
    
    @IBOutlet weak var maincollectionHeight_second: NSLayoutConstraint!
    @IBOutlet var mainCollectionView_second: UICollectionView!
    
    var delegate : TBHomeReloadingDelegate?
    
    var isOpen : Bool = true
    
    var tblHomeHeight: CGRect?
    
    @IBOutlet weak var topLayerView: UIView!
    
    var lastSelectedIndexString : String?
    
    var dataArrayDummyMainSecond = ["DTH", "Bill Splitter Tab", "Merchant Payment","Instant Pay","Tax Payment","Lottery"]
    var imgArrayDummyMainSecond = ["r_dth", #imageLiteral(resourceName: "dashboard_bill_splitter"), #imageLiteral(resourceName: "dashboard_merchant_payment"),"dashboard_instapay", "tax_Logo" ,"lottery"] as [Any]
    
    var advanceMerchantArrayDummy = ["Pay / Send", "Request Money", "Bill Splitter Tab","1 Stop Mart Online","Go-Auto Ticket", "Mobile & Electronics" ]
    var advanceMerchantArray = ["Pay / Send", "Request Money", "Bill Splitter Tab","1 Stop Mart Online","Go-Auto Ticket", "Mobile & Electronics" ]
    
    var advanceMerchantImageArrayDummy = [#imageLiteral(resourceName: "dashboard_pay_send"), #imageLiteral(resourceName: "dashboard_requestmoney"), #imageLiteral(resourceName: "dashboard_bill_splitter"), #imageLiteral(resourceName: "onestopmart"), "dashboard_go_autoticket.gif", "mobile_shopping_new.gif"] as [Any]
    var advanceMerchantImageArray = [#imageLiteral(resourceName: "dashboard_pay_send"), #imageLiteral(resourceName: "dashboard_requestmoney"), #imageLiteral(resourceName: "dashboard_bill_splitter"), #imageLiteral(resourceName: "onestopmart"), "dashboard_go_autoticket.gif", "mobile_shopping_new.gif"] as [Any]
    
    var advanceMerchantSecondArrayDummy = ["Send Money to Bank", "Buy & Sale Bonus Point Tab","Bonus Point Setup Tab", "Lucky draw & Coupon", "Earning Point Transfer Tab", "Redeem Bonus Point Tab", "Voting"]
    var advanceMerchantSecondArray = ["Send Money to Bank", "Buy & Sale Bonus Point Tab","Bonus Point Setup Tab", "Lucky draw & Coupon", "Earning Point Transfer Tab", "Redeem Bonus Point Tab", "Voting"]
    
    var advanceMerchantSecondImageArrayDummy = ["sendMoneyToBank_dashboard", "dashboard_buy_sale", "merchant_confi.png", #imageLiteral(resourceName: "dashboard_lucky_draw") , #imageLiteral(resourceName: "dashboardEPT"), #imageLiteral(resourceName: "dashboardRP"),  #imageLiteral(resourceName: "voting_dashboard")] as [Any]
    var advanceMerchantSecondImageArray = ["sendMoneyToBank_dashboard", "dashboard_buy_sale", "merchant_confi.png", #imageLiteral(resourceName: "dashboard_lucky_draw") , #imageLiteral(resourceName: "dashboardEPT"), #imageLiteral(resourceName: "dashboardRP"),  #imageLiteral(resourceName: "voting_dashboard")] as [Any]
    
    
    var dataArrayMainSecond = [String]()
    var imgArrayMainSecond = [Any]()
    var estimatedHeigh : CGFloat = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        refRecharge = self
        self.lastSelectedIndexString = ""
        
        if UserModel.shared.agentType == .advancemerchant {
            topLayerView.backgroundColor = .white
            self.titleHeader.text = ""
            self.dHeight = (screenHeight - 140) < 585.0 ? 585.0 : screenHeight - 140
            self.maincollectionHeight_first.constant = 220
            self.maincollectionHeight_second.constant = 330.0
            height_SubCollectionView_first.constant = 0.0
        }else {
            self.titleHeader.textColor = UIColor.init(hex: "0321AA")
            self.titleHeader.text = "Recharge & Bill Payment".localized
            self.maincollectionHeight_first.constant = 350.0
            self.maincollectionHeight_second.constant = 130.0
            self.estimatedHeigh = 510.0
            self.dHeight = estimatedHeigh
            height_SubCollectionView_first.constant = 0.0
        }

        self.mainCollectionView_first.register(UINib(nibName: TBHomeModel.cellDetails, bundle: nil), forCellWithReuseIdentifier: TBHomeModel.cellDetails)
        self.subCollectionView_first.register(UINib(nibName: TBHomeModel.cellDetails, bundle: nil), forCellWithReuseIdentifier: TBHomeModel.cellDetails)
        self.mainCollectionView_second.register(UINib(nibName: TBHomeModel.cellDetails, bundle: nil), forCellWithReuseIdentifier: TBHomeModel.cellDetails)
        self.mainCollectionView_first.delegate   = self
        self.mainCollectionView_first.dataSource = self
        
        self.mainCollectionView_second.delegate   = self
        self.mainCollectionView_second.dataSource = self
        
        self.subCollectionView_first.delegate   = self
        self.subCollectionView_first.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(RechargeBillPayViewController.languageChangeObservation), name: NSNotification.Name(rawValue: languageKeyDashboard), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserModel.shared.agentType == .advancemerchant {
            self.view.backgroundColor = .white
            topLayerView.backgroundColor = .white
        }
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        appDel.floatingButtonControl?.window.isHidden = false
        self.titleHeader.font = UIFont(name: appFont, size: 13.0)
    }
    
    func subCollectionReload() {
        subCollectionView_first.reloadData()
    }
    
    
    func reloadSection() {
        
        dataArrayMainSecond.removeAll()
        imgArrayMainSecond.removeAll()
        
        for (index , element) in dataArrayDummyMainSecond.enumerated() {
            if index == 0 && isDTHShow && isOverseasShow {
                dataArrayMainSecond.append(element)
                imgArrayMainSecond.append(imgArrayDummyMainSecond[index])
            }
            
            if index == 1 && isBillSplitterShow {
                dataArrayMainSecond.append(element)
                imgArrayMainSecond.append(imgArrayDummyMainSecond[index])
            }
            if index == 2 && isMarchantPaymentShow {
                dataArrayMainSecond.append(element)
                imgArrayMainSecond.append(imgArrayDummyMainSecond[index])
            }
            if index == 3 && isInstantPayShow {
                dataArrayMainSecond.append(element)
                imgArrayMainSecond.append(imgArrayDummyMainSecond[index])
            }
            if index == 4 {
                dataArrayMainSecond.append(element)
                imgArrayMainSecond.append(imgArrayDummyMainSecond[index])
            }
            if index == 5 && isLotteryShow{
                dataArrayMainSecond.append(element)
                imgArrayMainSecond.append(imgArrayDummyMainSecond[index])
            }
        }
        
        DispatchQueue.main.async {
            if self.dataArrayMainSecond.count < 4 {
                    self.maincollectionHeight_second.constant = 110.0
                }else {
                    self.maincollectionHeight_second.constant = 230.0
                }
                self.estimatedHeigh = self.maincollectionHeight_second.constant + 30 + 350.0
                self.dHeight = self.maincollectionHeight_second.constant + 30 + 350.0
            
            self.height_SubCollectionView_first.constant = 0.0
            self.mainCollectionView_second.reloadData()
            if isOverseasShow {
                self.mainCollectionView_first.reloadData()
            }
            if let delegate = self.delegate {
                let index = IndexPath.init(row: 1, section: 0)
                delegate.refreshContentHomeTab(index)
            }
        }
        
    }
    
    
    func advanceMerhantReload() {
        
        advanceMerchantArray.removeAll()
        advanceMerchantImageArray.removeAll()
        
        advanceMerchantSecondArray.removeAll()
        advanceMerchantSecondImageArray.removeAll()

        for (index, element) in advanceMerchantArrayDummy.enumerated() {
            if index == 0 {
                advanceMerchantArray.append(element)
                advanceMerchantImageArray.append(advanceMerchantImageArrayDummy[index])
            }else if index == 1 {
                advanceMerchantArray.append(element)
                advanceMerchantImageArray.append(advanceMerchantImageArrayDummy[index])
            }else if index == 2 && isBillSplitterShow {
                advanceMerchantArray.append(element)
                advanceMerchantImageArray.append(advanceMerchantImageArrayDummy[index])
            }else if index == 3 {
                advanceMerchantArray.append(element)
                advanceMerchantImageArray.append(advanceMerchantImageArrayDummy[index])
            }else if index == 4 {
                advanceMerchantArray.append(element)
                advanceMerchantImageArray.append(advanceMerchantImageArrayDummy[index])
            }else if index == 5 && isMobileElectronicsShow {
                advanceMerchantArray.append(element)
                advanceMerchantImageArray.append(advanceMerchantImageArrayDummy[index])
            }
        }
        
        for (index, element) in advanceMerchantSecondArrayDummy.enumerated() {
              if index == 0 {
                  advanceMerchantSecondArray.append(element)
                  advanceMerchantSecondImageArray.append(advanceMerchantSecondImageArrayDummy[index])
              }else if index == 1 && isBuySaleBonusPointShow {
                  advanceMerchantSecondArray.append(element)
                  advanceMerchantSecondImageArray.append(advanceMerchantSecondImageArrayDummy[index])
              }else if index == 2 {
                  advanceMerchantSecondArray.append(element)
                  advanceMerchantSecondImageArray.append(advanceMerchantSecondImageArrayDummy[index])
              }else if index == 3 && isLuckyDrawShow {
                  advanceMerchantSecondArray.append(element)
                  advanceMerchantSecondImageArray.append(advanceMerchantSecondImageArrayDummy[index])
              }else if index == 4 && isEarningPointShow {
                  advanceMerchantSecondArray.append(element)
                  advanceMerchantSecondImageArray.append(advanceMerchantSecondImageArrayDummy[index])
              }else if index == 5 {
                  advanceMerchantSecondArray.append(element)
                  advanceMerchantSecondImageArray.append(advanceMerchantSecondImageArrayDummy[index])
              }else if index == 6 && isVotingshow {
                  advanceMerchantSecondArray.append(element)
                  advanceMerchantSecondImageArray.append(advanceMerchantSecondImageArrayDummy[index])
              }
              else if index == 7{
                  advanceMerchantSecondArray.append(element)
                  advanceMerchantSecondImageArray.append(advanceMerchantSecondImageArrayDummy[index])
              }
          }
        
        if advanceMerchantArray.count == 4 {
            advanceMerchantArray.append(advanceMerchantSecondArray[0])
            advanceMerchantSecondArray.removeFirst()
          
            advanceMerchantImageArray.append(advanceMerchantSecondImageArray[0])
            advanceMerchantSecondImageArray.removeFirst()
            
          
            advanceMerchantArray.append(advanceMerchantSecondArray[0])
            advanceMerchantSecondArray.removeFirst()
            
            advanceMerchantImageArray.append(advanceMerchantSecondImageArray[0])
            advanceMerchantSecondImageArray.removeFirst()
            
        }else if advanceMerchantArray.count == 5 {
            advanceMerchantArray.append(advanceMerchantSecondArray[0])
            advanceMerchantSecondArray.removeFirst()
            advanceMerchantImageArray.append(advanceMerchantSecondImageArray[0])
            advanceMerchantSecondImageArray.removeFirst()
        }
        
        DispatchQueue.main.async {
            if !isBillSplitterShow || !isMobileElectronicsShow {
                if let cell = self.mainCollectionView_first.cellForItem(at: IndexPath(row: 2, section: 0)) as? CollectionViewCell {
                    cell.imageV.image = nil
                    cell.imageV.gifImage = nil
                    DispatchQueue.main.async {
                        cell.wrapDataWithStaticImages(self.advanceMerchantArray[2], self.advanceMerchantImageArray[2])
                        self.mainCollectionView_first.reloadItems(at: [IndexPath(row: 2, section: 0)])
                    }
                }
                if let cell = self.mainCollectionView_first.cellForItem(at: IndexPath(row: 3, section: 0)) as? CollectionViewCell {
                    cell.imageV.image = nil
                    cell.imageV.gifImage = nil
                    DispatchQueue.main.async {
                        cell.wrapDataWithStaticImages(self.advanceMerchantArray[3], self.advanceMerchantImageArray[3])
                        self.mainCollectionView_first.reloadItems(at: [IndexPath(row: 3, section: 0)])
                    }
                }
                if let cell = self.mainCollectionView_first.cellForItem(at: IndexPath(row: 4, section: 0)) as? CollectionViewCell {
                    cell.imageV.image = nil
                    cell.imageV.gifImage = nil
                    DispatchQueue.main.async {
                        cell.wrapDataWithStaticImages(self.advanceMerchantArray[4], self.advanceMerchantImageArray[4])
                        self.mainCollectionView_first.reloadItems(at: [IndexPath(row: 4, section: 0)])
                    }
                }
                if let cell = self.mainCollectionView_first.cellForItem(at: IndexPath(row: 5, section: 0)) as? CollectionViewCell {
                    cell.imageV.image = nil
                    cell.imageV.gifImage = nil
                    DispatchQueue.main.async {
                        cell.wrapDataWithStaticImages(self.advanceMerchantArray[5], self.advanceMerchantImageArray[5])
                        self.mainCollectionView_first.reloadItems(at: [IndexPath(row: 5, section: 0)])
                    }
                }
//                DispatchQueue.main.async {
//                    if self.advanceMerchantSecondArray.count < 4 {
//                        self.maincollectionHeight_second.constant = 110.0
//                    }else if self.advanceMerchantSecondArray.count < 7 {
//                        self.maincollectionHeight_second.constant = 220.0
//                    }else {
//                        self.maincollectionHeight_second.constant = 330.0
//                    }
//                    self.estimatedHeigh = self.maincollectionHeight_second.constant + 30
//                    self.dHeight = self.maincollectionHeight_second.constant + 30
//                    self.height_SubCollectionView_first.constant = 0.0
//                    if let delegate = self.delegate {
//                        let index = IndexPath.init(row: 1, section: 0)
//                        delegate.refreshContentHomeTab(index)
//                    }
//                }
            }
        }
        DispatchQueue.main.async {
             self.mainCollectionView_second.reloadData()
        }
        
    }
    
    @objc func languageChangeObservation() {
        if UserModel.shared.agentType == .advancemerchant {
            for (index, _) in advanceMerchantArray.enumerated() {
                if advanceMerchantArray.count > index  {
                    if  let cell = self.mainCollectionView_first.cellForItem(at: IndexPath.init(row: index, section: 0)) as? CollectionViewCell {
                        cell.titleLabel.text = advanceMerchantArray[index] .localized
                        cell.titleLabel.font = UIFont(name: appFont, size: 13.0)
                    }
                }
            }
            for (index, _) in advanceMerchantSecondArray.enumerated() {
                if advanceMerchantSecondArray.count > index  {
                    if  let cell = self.mainCollectionView_second.cellForItem(at: IndexPath.init(row: index, section: 0)) as? CollectionViewCell {
                        cell.titleLabel.text = advanceMerchantSecondArray[index] .localized
                        cell.titleLabel.font = UIFont(name: appFont, size: 13.0)
                    }
                }
            }
        } else {
            self.titleHeader.text = "Recharge & Bill Payment".localized
            if isOverseasShow == true {
                for (index, _) in TBHomeModel.RechargeBillPay.dataArrayMainOverSeas.enumerated() {
                    if TBHomeModel.RechargeBillPay.dataArrayMainOverSeas.count > index  {
                        if  let cell = self.mainCollectionView_first.cellForItem(at: IndexPath.init(row: index, section: 0)) as? CollectionViewCell {
                            if TBHomeModel.RechargeBillPay.dataArrayMainOverSeas[index].lowercased() != "Mobile Number".lowercased() {
                                cell.titleLabel.text = TBHomeModel.RechargeBillPay.dataArrayMainOverSeas[index] .localized
                                cell.titleLabel.font = UIFont(name: appFont, size: 13.0)
                            }
                        }
                    }
                }
            } else {
                for (index, _) in TBHomeModel.RechargeBillPay.dataArrayMain.enumerated() {
                    if TBHomeModel.RechargeBillPay.dataArrayMain.count > index  {
                        if  let cell = self.mainCollectionView_first.cellForItem(at: IndexPath.init(row: index, section: 0)) as? CollectionViewCell {
                            if TBHomeModel.RechargeBillPay.dataArrayMain[index].lowercased() != "Mobile Number".lowercased() {
                                cell.titleLabel.text = TBHomeModel.RechargeBillPay.dataArrayMain[index] .localized
                                cell.titleLabel.font = UIFont(name: appFont, size: 13.0)
                            }
                        }
                    }
                }
            }
            
            for (index, _) in dataArrayMainSecond.enumerated() {
                if dataArrayMainSecond.count > index  {
                    if  let cell = self.mainCollectionView_second.cellForItem(at: IndexPath.init(row: index, section: 0)) as? CollectionViewCell {
                        if dataArrayMainSecond[index].lowercased() != "Mobile Number".lowercased() {
                            cell.titleLabel.text = dataArrayMainSecond[index] .localized
                            cell.titleLabel.font = UIFont(name: appFont, size: 13.0)
                        }
                    }
                }
            }
        }
        
        if self.subCollectionView_first.isHidden == false {
            for (index, _) in TBHomeModel.RechargeBillPay.subDataArray.enumerated() {
                if TBHomeModel.RechargeBillPay.subDataArray.count > index  {
                    if  let cell = self.subCollectionView_first.cellForItem(at: IndexPath.init(row: index, section: 0)) as? CollectionViewCell {
                        cell.titleLabel.text = TBHomeModel.RechargeBillPay.subDataArray[index] .localized
                        cell.titleLabel.font = UIFont(name: appFont, size: 13.0)
                    }
                }
            }
        }
    }

}

extension RechargeBillPayViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == mainCollectionView_first {
            if UserModel.shared.agentType == .advancemerchant {
                return advanceMerchantArray.count
            }
            if isOverseasShow == true {
                return TBHomeModel.RechargeBillPay.dataArrayMainOverSeas.count
            }
            return TBHomeModel.RechargeBillPay.dataArrayMain.count
        } else if collectionView == mainCollectionView_second {
            if UserModel.shared.agentType == .advancemerchant {
                return advanceMerchantSecondArray.count
            }
            return dataArrayMainSecond.count
        } else {
            return TBHomeModel.RechargeBillPay.subDataArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.mainCollectionView_first {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String.init(describing: TBHomeModel.cellDetails), for: indexPath) as? CollectionViewCell
            if UserModel.shared.agentType == .advancemerchant {
                cell?.wrapDataWithStaticImages(advanceMerchantArray[indexPath.row], advanceMerchantImageArray[indexPath.row])
            }else {
                if isOverseasShow == true {
                    cell?.wrapDataWithStaticImages(TBHomeModel.RechargeBillPay.dataArrayMainOverSeas[indexPath.row], TBHomeModel.RechargeBillPay.imgArrayMainSeas[indexPath.row])
                } else {
                    cell?.wrapDataWithStaticImages(TBHomeModel.RechargeBillPay.dataArrayMain[indexPath.row], TBHomeModel.RechargeBillPay.imgArrayMain[indexPath.row])
                }
            }
            cell?.backgroundColor = .clear
            cell?.layoutIfNeeded()
            return cell!
        } else if collectionView == mainCollectionView_second {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String.init(describing: TBHomeModel.cellDetails), for: indexPath) as? CollectionViewCell
            if UserModel.shared.agentType == .advancemerchant {
                cell?.wrapDataWithStaticImages(advanceMerchantSecondArray[indexPath.row], advanceMerchantSecondImageArray[indexPath.row])
            } else {
                cell?.wrapDataWithStaticImages(dataArrayMainSecond[indexPath.row], imgArrayMainSecond[indexPath.row])
            }
            cell?.backgroundColor = .clear
            cell?.layoutIfNeeded()
            return cell!
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String.init(describing: TBHomeModel.cellDetails), for: indexPath) as? CollectionViewCell
            cell?.wrapDataWithStaticImages(TBHomeModel.RechargeBillPay.subDataArray[indexPath.row], TBHomeModel.RechargeBillPay.subImgArray[indexPath.row])
            cell?.backgroundColor = .clear
            cell?.layoutIfNeeded()
            return cell!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: screenWidth / 3.2, height: 110.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
            if collectionView == self.mainCollectionView_first {
                for (index, _) in TBHomeModel.RechargeBillPay.dataArrayMain.enumerated() {
                    if TBHomeModel.RechargeBillPay.dataArrayMain.count > index  {
                        if let cell = collectionView.cellForItem(at: IndexPath(row: index, section: 0)) as? CollectionViewCell {
                            cell.backgroundColor = .clear
                        }
                    }
                }
            }
            
            if let cell = collectionView.cellForItem(at: indexPath) as? CollectionViewCell {
                cell.backgroundColor = .clear
                
                if let text = cell.matchingText {
                    switch text {
                    case "International Top-Up":
                        TBHomeNavigations.main.navigate(.OverseasTopUp)
                    case "DTH":
                        TBHomeNavigations.main.navigate(.skynet)
                        break
                    case "Tax Payment":
                        TBHomeNavigations.main.navigate(.taxPaymentNew)
                        break
                    case "Lottery":
                        TBHomeNavigations.main.navigate(.lottery)
                        break
                    case "Electricity":
                        if collectionView == self.mainCollectionView_first {
                            if lastSelectedIndexString == "Electricity" {
                              self.dHeight = self.estimatedHeigh
                                
                                height_SubCollectionView_first.constant = 0.0
                                subCollectionView_first.isHidden = true
                                if let delegate = self.delegate {
                                    let index = IndexPath.init(row: 1, section: 0)
                                    delegate.refreshContentHomeTab(index)
                                }
                                self.lastSelectedIndexString = ""
                            } else {
                                cell.backgroundColor = UIColor.init(hex: "EBEBF1")
                                self.dHeight = self.estimatedHeigh + 120.00
                                
                                height_SubCollectionView_first.constant = 105.0
                                subCollectionView_first.isHidden = false
                                TBHomeModel.RechargeBillPay.switchData(.electricity)
                                
                                UIView.animate(withDuration: 0.4, animations: {
                                    self.view.layoutIfNeeded()
                                })
                                self.subCollectionView_first.reloadData()
                                if let delegate = self.delegate {
                                    let index = IndexPath.init(row: 1, section: 0)
                                    delegate.refreshContentHomeTab(index)
                                }
                                self.lastSelectedIndexString = "Electricity"}
                        } else {
                            TBHomeNavigations.main.navigate(.electricity)
                        }
                        break
                     case "Go-Auto Ticket":
                        if lastSelectedIndexString == "Go-Auto Ticket" {
                            height_SubCollectionView_first.constant = 0.0
                            self.dHeight = (screenHeight - 140) < 585.0 ? 585.0 : screenHeight - 140
                            subCollectionView_first.isHidden = true
                            if let delegate = self.delegate {
                                let index = IndexPath.init(row: 1, section: 0)
                                delegate.refreshContentHomeTab(index)
                            }
                            self.lastSelectedIndexString = ""
                        } else {
                            cell.backgroundColor = UIColor.init(hex: "EBEBF1")
                            self.dHeight = (screenHeight - 140) < 690.0 ? 690.0 : screenHeight - 140
                            height_SubCollectionView_first.constant = 105.0
                            subCollectionView_first.isHidden = false
                            TBHomeModel.RechargeBillPay.switchData(.goAuto)
                            
                            UIView.animate(withDuration: 0.4, animations: {
                                self.view.layoutIfNeeded()
                            })
                            self.subCollectionView_first.reloadData()
                            if let delegate = self.delegate {
                                let index = IndexPath.init(row: 1, section: 0)
                                delegate.refreshContentHomeTab(index)
                            }
                            self.lastSelectedIndexString = "Go-Auto Ticket"
                            
                        }
                        break
                    case "Sun King Payment":
                        TBHomeNavigations.main.navigate(.sunKingPayment)
                        break
                    case "Tax Payment":
                        TBHomeNavigations.main.navigate(.taxPayment)
                        break
                    case "Other Number":
                        TBHomeNavigations.main.navigate(.otherNumber)
                        break
                    
                    case "Mobile Number":
                        TBHomeNavigations.main.navigate(.mobileNumber)
                        break
                        
                    case "Re-Sale":
                        TBHomeNavigations.main.navigate(.resale)
                        break
                    
                    case "Landline Bill":
                        TBHomeNavigations.main.navigate(.landlineBill)
                        break
                    case "Postpaid Mobile":
                        TBHomeNavigations.main.navigate(.postpaidMobile)
                        break
                    case "Thai Mobile Recharge":
                        TBHomeNavigations.main.navigate(.ThaiMobRecharge)
                        break
                    case "China Mobile Recharge":
                        TBHomeNavigations.main.navigate(.ChinaMobRecharge)
                        break
                        
                    case "Buy OK$ Point":
                        TBHomeNavigations.main.navigate(.buyTopupPoint)
                    case "Skynet":
                        TBHomeNavigations.main.navigate(.skynet)
                        break
                    case "Airtel":
                        TBHomeNavigations.main.navigate(.airtel)
                        break
                    case "Dish Tv":
                        TBHomeNavigations.main.navigate(.dishTv)
                        break
                    case "Sun Direct":
                        TBHomeNavigations.main.navigate(.sunDirect)
                        break
                    case "Reliance":
                        TBHomeNavigations.main.navigate(.reliance)
                        break
                    case "Videocon":
                        TBHomeNavigations.main.navigate(.videocon)
                        break
                    case "Tata Sky":
                        TBHomeNavigations.main.navigate(.tataSky)
                        break
                    case "Bill Splitter Tab":
                        TBHomeNavigations.main.navigate(.BillSpliter)
                        break
                    case "Dish Home":
                        TBHomeNavigations.main.navigate(.dishHome)
                        break
                    case "Voucher":
                        TBHomeNavigations.main.navigate(.voucher)
                        break
                    case "Solar Home":
                        TBHomeNavigations.main.navigate(.solarElectricity)
                    case "Pay / Send":
                        TBHomeNavigations.main.navigate(.payto)
                    case "Request Money":
                        TBHomeNavigations.main.navigate(.ReqMoney)
//                    case "Live Taxi":
//                        TBHomeNavigations.main.navigate(.LiveTaxi)
                    case "Mobile & Electronics":
                        TBHomeNavigations.main.navigate(.mobileElectronics)
                    case "Buy & Sale Bonus Point Tab":
                        TBHomeNavigations.main.navigate(.BuyBonusPoint)
                    case "Bonus Point Setup Tab":
                        TBHomeNavigations.main.navigate(.loyalty)
                    case  "Lucky draw & Coupon":
                        TBHomeNavigations.main.navigate(.luckyDrawCoupon)
                    case "Earning Point Transfer Tab":
                        TBHomeNavigations.main.navigate(.earnPointTransfer)
                    case "Redeem Bonus Point Tab":
                        TBHomeNavigations.main.navigate(.redeemPoint)
                    case "Voting":
                         TBHomeNavigations.main.navigate(.voting)
                    case "Flights":
                        TBHomeNavigations.main.navigate(.flight)
                        break
                    case "Hotel":
                        TBHomeNavigations.main.navigate(.hotel)
                        break
                    case "1 Stop Mart Online":
                        TBHomeNavigations.main.navigate(.onestopmart)
                        break
                    case "Intercity Bus":
                        TBHomeNavigations.main.navigate(.intercityBus)
                    case "Send Money to Bank":
                        TBHomeNavigations.main.navigate(.sendMoneyToBank)
                    case "Merchant Payment":
                        TBHomeNavigations.main.navigate(.merchantPayment)
                    case "Instant Pay":
                        TBHomeNavigations.main.navigate(.instaPay)
                    case "Scan/Pay":
                        TBHomeNavigations.main.navigate(.scanPay)
                    default:
                        break
                    }
                }
        }
    }
}

