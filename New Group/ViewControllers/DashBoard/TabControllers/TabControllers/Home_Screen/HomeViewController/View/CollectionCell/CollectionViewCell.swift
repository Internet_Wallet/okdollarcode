//
//  CollectionViewCell.swift
//  OK
//
//  Created by Ashish on 1/11/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

enum ImageType {
    case staticImage, dynamicImage
}

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    let gifManager = SwiftyGifManager(memoryLimit:6)

    var uniqueID = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      
        self.titleLabel.font = UIFont(name: appFont, size: 13.0)
        
    }
    
    override func prepareForReuse() {
          super.prepareForReuse()
          self.imageV.image = nil
          self.imageV.gifImage = nil
      }
    
    func  wrapDataWithStaticImages(_ text: String, _ img: Any) {
        if text == "Mobile Number" {
            self.titleLabel.text = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "0")//UserModel.shared.formattedNumber
        } else {
            self.titleLabel.text = text.localized
        }
        
        if text == "Request Money", let _ = img as? UIImage {
            self.titleLabel.text = "Request Money Dashboard".localized
        }
        self.titleLabel.textColor = UIColor.init(hex: "0321AA")
        self.matchingText = text
        self.imageV.image = nil
            if let staticImage = img as? UIImage {
                self.imageV.image = staticImage
            } else if let dynamicImageString = img as? String {
                if dynamicImageString.hasSuffix("gif") {
                    DispatchQueue.main.async {
                        var imageName = dynamicImageString
                        imageName = String(imageName.dropLast(4))
                        self.imageV.loadGif(name: imageName)
                     }
                }
                else {
                    DispatchQueue.main.async {
                        self.imageV.image = UIImage(named: dynamicImageString)
                     }
                }
            }
        self.titleLabel.font = UIFont(name: appFont, size: 13.0)
    }

}
