//
//  CashIOPRViewController.swift
//  OK
//
//  Created by Ashish on 1/11/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class CashIOPRViewController: UIViewController {
    
    @IBOutlet var mainCollectionView   : UICollectionView!
    @IBOutlet var subCollectionView    : UICollectionView!
    @IBOutlet var sub_HeightConstrainst: NSLayoutConstraint!
    @IBOutlet var titleHeader : UILabel!
    
    private var mainCells = [CollectionViewCell]()
    private var subCells  = [CollectionViewCell]()
    
    var lastSelectedIndexText : String?
    
    var delegate : TBHomeReloadingDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lastSelectedIndexText = ""
        
        self.titleHeader.text = "Cash In|Out & Payment Request".localized
        self.titleHeader.textColor = UIColor.init(hex: "0321AA")
        self.dHeight = 260.00
        self.mainCollectionView.register(UINib(nibName: TBHomeModel.cellDetails, bundle: nil), forCellWithReuseIdentifier: TBHomeModel.cellDetails)
        self.subCollectionView.register(UINib(nibName: TBHomeModel.cellDetails, bundle: nil), forCellWithReuseIdentifier: TBHomeModel.cellDetails)
        
        subCollectionView.isHidden = true
        
        self.mainCollectionView.delegate = self
        self.mainCollectionView.dataSource = self
        
        self.subCollectionView.delegate = self
        self.subCollectionView.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(CashIOPRViewController.languageChangeObservation), name: NSNotification.Name(rawValue: languageKeyDashboard), object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        appDel.floatingButtonControl?.window.isHidden = false
    }
    
    @objc func languageChangeObservation() {
//        self.mainCollectionView.reloadData()
        var indexq = 0
        
        self.titleHeader.text = "Cash In|Out & Payment Request".localized
        self.titleHeader.font = UIFont(name: appFont, size: 13.0)

        for cell in self.mainCells {
            if TBHomeModel.CashPaymentRequest.dataArray.count > indexq  {
                cell.titleLabel.text = TBHomeModel.CashPaymentRequest.dataArray[indexq] .localized
                cell.titleLabel.font = UIFont(name: appFont, size: 13.0)
                indexq = indexq + 1
            }
        }
        
        self.subCollectionView.reloadData()
        var index = 0
        for cell in self.subCells {
            if TBHomeModel.CashPaymentRequest.subDataArray.count > index  {
                cell.titleLabel.text = TBHomeModel.CashPaymentRequest.subDataArray[index] .localized
                cell.titleLabel.font = UIFont(name: appFont, size: 13.0)
                index = index + 1
            }
        }
        
    }

    
}

extension CashIOPRViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == mainCollectionView {
            return TBHomeModel.CashPaymentRequest.dataArray.count
        } else {
            return TBHomeModel.CashPaymentRequest.subDataArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.mainCollectionView {
            
            if mainCells.count > 0 {
                if self.mainCells.contains(where: { $0.uniqueID == TBHomeModel.CashPaymentRequest.dataArray[indexPath.row]}) {
                    return self.mainCells[indexPath.row]
                }
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String.init(describing: TBHomeModel.cellDetails), for: indexPath) as? CollectionViewCell
            cell?.wrapDataWithStaticImages(TBHomeModel.CashPaymentRequest.dataArray[indexPath.row], TBHomeModel.CashPaymentRequest.imgArray[indexPath.row])
            cell?.layoutIfNeeded()
            self.mainCells.append(cell!)
            return cell!
        } else {
            if subCells.count > 0 {
                if  self.subCells.contains(where:  { $0.uniqueID == TBHomeModel.CashPaymentRequest.subDataArray[indexPath.row]} ) {
                    return self.subCells[indexPath.row]
                }
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String.init(describing: TBHomeModel.cellDetails), for: indexPath) as? CollectionViewCell
            cell?.wrapDataWithStaticImages(TBHomeModel.CashPaymentRequest.subDataArray[indexPath.row], TBHomeModel.CashPaymentRequest.subImgArray[indexPath.row])
            cell?.uniqueID = TBHomeModel.CashPaymentRequest.subDataArray[indexPath.row]
            cell?.layoutIfNeeded()
            self.subCells.append(cell!)
            return cell!
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: screenWidth / 3.2, height: 115.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        for cell in self.mainCells {
            cell.backgroundColor = .clear
        }

        
        if let cell = collectionView.cellForItem(at: indexPath) as? CollectionViewCell {
            if let text = cell.matchingText {
                switch text {
                case "Cash In Cash Out":
                    if lastSelectedIndexText == "Cash In Cash Out" {
                        self.dHeight = 140.00
                        self.subCollectionView.isHidden = true
                        if let delegate = self.delegate {
                            let index = IndexPath.init(row: 3, section: 0)
                            delegate.refreshContentHomeTab(index)
                        }
                        self.lastSelectedIndexText = ""
                    } else {
                        cell.backgroundColor = UIColor.init(hex: "EBEBF1")
                        self.dHeight = 240.00
                        subCollectionView.isHidden = false
                        TBHomeModel.CashPaymentRequest.switchDataType(.cash)
                        UIView.animate(withDuration: 0.4, animations: {
                            self.view.layoutIfNeeded()
                        })
                        
                        self.subCollectionView.reloadData()
                        if let delegate = self.delegate {
                            let index = IndexPath.init(row: 3, section: 0)
                            delegate.refreshContentHomeTab(index)
                        }
                        self.lastSelectedIndexText = "Cash In Cash Out"
                    }
                    break
                case "Request Cash In":
                    TBHomeNavigations.main.navigate(.cashIN)
                    break
                case "Request Cash Out":
                    TBHomeNavigations.main.navigate(.cashOUT)
                    break
                case "OK$ Chat":
                    TBHomeNavigations.main.navigate(.okChat)
                case "Nearby OK$ Services":
                    TBHomeNavigations.main.navigate(.nearbyOKServices)
                default :
                    break
                }
            }
        }
    }
}




