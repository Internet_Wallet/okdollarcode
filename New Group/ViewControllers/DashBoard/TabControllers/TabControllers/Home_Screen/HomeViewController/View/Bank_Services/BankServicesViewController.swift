//
//  LoyalityPromotionHomeViewController.swift
//  OK
//
//  Created by Ashish on 1/15/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class BankServicesViewController: UIViewController {

    @IBOutlet var titleHeader: UILabel!
    @IBOutlet var mainCollectionView: UICollectionView!
    @IBOutlet var subCollectionView: UICollectionView!
    
    // height constraints
    @IBOutlet weak var gapHeight: NSLayoutConstraint!
    @IBOutlet weak var headerHeight: NSLayoutConstraint!
    @IBOutlet weak var maincollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var subCollectionHeight: NSLayoutConstraint!
    
    var delegate : TBHomeReloadingDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleHeader.text = "Bank Services".localized
        self.titleHeader.textColor = UIColor.init(hex: "0321AA")
        self.maincollectionHeight.constant = 210.00
        self.subCollectionHeight.constant = 0.0
        self.setLayoutHeight()
        
        self.mainCollectionView.register(UINib(nibName: TBHomeModel.cellDetails, bundle: nil), forCellWithReuseIdentifier: TBHomeModel.cellDetails)
        self.mainCollectionView.delegate = self
        self.mainCollectionView.dataSource = self
        
        self.subCollectionView.register(UINib(nibName: TBHomeModel.cellDetails, bundle: nil), forCellWithReuseIdentifier: TBHomeModel.cellDetails)
        self.subCollectionView.delegate = self
        self.subCollectionView.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoyalityPromotionHomeViewController.languageChangeObservation), name: NSNotification.Name(rawValue: languageKeyDashboard), object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        appDel.floatingButtonControl?.window.isHidden = false
    }
    
    private func setLayoutHeight() {
        if subCollectionHeight.constant <= 1 {
            self.subCollectionView.isHidden = true
        } else {
            self.subCollectionView.isHidden = false
        }
        
        let fixedHeight = self.gapHeight.constant + self.headerHeight.constant
        self.dHeight = fixedHeight + self.subCollectionHeight.constant + self.maincollectionHeight.constant
    }
    
    @objc func languageChangeObservation() {
        
        self.titleHeader.text = "Bank Services".localized
        self.titleHeader.font = UIFont(name: appFont, size: 13.0)

        for (index, _) in TBHomeModel.BankServices.dataArray.enumerated() {
            if TBHomeModel.BankServices.dataArray.count > index  {
                if  let cell = self.mainCollectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? CollectionViewCell {
                    cell.titleLabel.text = TBHomeModel.BankServices.dataArray[index] .localized
                    cell.titleLabel.font = UIFont(name: appFont, size: 13.0)
                }
            }
        }
        
        for (index,_) in TBHomeModel.BankServices.subDataArray.enumerated() {
            if TBHomeModel.BankServices.subDataArray.count > index {
                if let cell = self.subCollectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? CollectionViewCell {
                    cell.titleLabel.text = TBHomeModel.BankServices.subDataArray[index] .localized
                    cell.titleLabel.font = UIFont(name: appFont, size: 13.0)
                }
            }
        }
    }
}

extension BankServicesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.mainCollectionView {
            return TBHomeModel.BankServices.dataArray.count
        } else {
            return TBHomeModel.BankServices.subDataArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.mainCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TBHomeModel.cellDetails, for: indexPath) as? CollectionViewCell
            cell?.wrapDataWithStaticImages(TBHomeModel.BankServices.dataArray[indexPath.row], TBHomeModel.BankServices.imgArray[indexPath.row])
            return cell!
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TBHomeModel.cellDetails, for: indexPath) as? CollectionViewCell
            cell?.wrapDataWithStaticImages(TBHomeModel.BankServices.subDataArray[indexPath.row], TBHomeModel.BankServices.subImgArray[indexPath.row])
            return cell!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: screenWidth / 3.2, height: 105.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            TBHomeNavigations.main.navigate(.addWithdraw)
        case 1:
            TBHomeNavigations.main.navigate(.merchantPayment)
        case 2:
            TBHomeNavigations.main.navigate(.bankCounterDeposit)
        case 3:
            TBHomeNavigations.main.navigate(.donation)
        case 4:
            TBHomeNavigations.main.navigate(.insurance)
        default:
            println_debug("default")
        }
    }
}
