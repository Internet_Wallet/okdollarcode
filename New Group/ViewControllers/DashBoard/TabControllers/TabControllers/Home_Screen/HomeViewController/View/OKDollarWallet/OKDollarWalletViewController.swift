//
//  OKDollarWalletViewController.swift
//  OK
//
//  Created by Ashish on 1/11/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class OKDollarWalletViewController: OKBaseController {
    //MARK: - Outlets
    @IBOutlet var titleLabel: UILabel!{
        didSet {
            titleLabel.font = UIFont(name: appFont, size: 13.0)
        }
    }
    @IBOutlet var collectionViewWallet: UICollectionView!
    @IBOutlet var heightConstraintsCV: NSLayoutConstraint!
    @IBOutlet var pageControl: UIPageControl!
    
   //  var nearByViewModel = NearByViewModel()
    
    //MARK: - Properties
    enum ScreenFor {
        case embeded, popDown
    }
    var height: CGFloat = 0.00
    let headerTitle: String = "" //OK$ Wallet
    var screenFor = ScreenFor.embeded
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageControl.currentPage = 0
        self.view.backgroundColor = UIColor.init(hex: "FEC400")
        self.titleLabel.text = headerTitle.localized
        self.collectionViewWallet.register(UINib.init(nibName: TBHomeModel.okWalletCellDetails, bundle: nil), forCellWithReuseIdentifier: TBHomeModel.okWalletCellDetails)
        self.collectionViewWallet.delegate = self
        self.collectionViewWallet.dataSource = self
        self.dHeight = 220.00
        switch screenFor {
        case .embeded:
            self.heightConstraintsCV.constant = 210.00
        case .popDown:
            self.heightConstraintsCV.constant = 70
        }
        pageControl.numberOfPages = TBHomeModel.OKDollarWallet.dataArray.count / 3
        NotificationCenter.default.addObserver(self, selector: #selector(OKDollarWalletViewController.languageChangeObservation), name: NSNotification.Name(rawValue: languageKeyDashboard), object: nil)
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        pageControl.backgroundColor = .clear
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        appDel.floatingButtonControl?.window.isHidden = false
        self.titleLabel.font = UIFont(name: appFont, size: 13.0)
    }
    
    @objc func languageChangeObservation() {
        self.titleLabel.text = headerTitle.localized
        self.titleLabel.font = UIFont(name: appFont, size: 13.0)

        for (index, _) in TBHomeModel.OKDollarWallet.dataArray.enumerated() {
            if TBHomeModel.OKDollarWallet.dataArray.count > index  {
                if  let cell = self.collectionViewWallet.cellForItem(at: IndexPath.init(row: index, section: 0)) as? OkWalletCollectionViewCell {
                    cell.titleLabel.text = TBHomeModel.OKDollarWallet.dataArray[index].localized
                    cell.titleLabel.font = UIFont(name: appFont, size: 13.0)
                }
            }
        }
    }
}

// MARK:- Collection View Delegate & Datasource
extension OKDollarWalletViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return TBHomeModel.OKDollarWallet.dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String.init(describing: TBHomeModel.okWalletCellDetails), for: indexPath) as? OkWalletCollectionViewCell
        switch screenFor {
        case .embeded:
            cell?.wrapDataWithStaticImages(TBHomeModel.OKDollarWallet.dataArray[indexPath.row], TBHomeModel.OKDollarWallet.imgArray[indexPath.row])
        case .popDown:
            cell?.wrapDataWithStaticImages(TBHomeModel.OKDollarWallet.dataArray[indexPath.row], TBHomeModel.OKDollarWallet.imgArray[indexPath.row], forTopDownView: true)
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if device.type == .iPhoneX || device.type == .iPhoneXS || device.type == .iPhoneXSMax || device.type == .iPhoneXR || device.type == .iPhone4 || device.type == .iPhone4S || device.type == .iPhone5 || device.type == .iPhone5S || device.type == .iPhoneSE {
            switch screenFor {
            case .embeded:
                return CGSize.init(width: screenWidth / 3.5, height: 105.0)
            case .popDown:
                return CGSize.init(width: screenWidth / 7.0, height: 50.0)
            }
        } else {
            switch screenFor {
            case .embeded:
                return CGSize.init(width: screenWidth / 3.2, height: 105.0)
            case .popDown:
                return CGSize.init(width: screenWidth / 7.0, height: 50.0)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let base = cell as? CollectionViewCell {
            base.titleLabel.text = TBHomeModel.OKDollarWallet.dataArray[indexPath.row].localized
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {        
        case 0:
            TBHomeNavigations.main.navigate(.payto)
        case 1:
            TBHomeNavigations.main.navigate(.transferTo)
        case 2:
            //C: Subh : new changes
            TBHomeNavigations.main.navigate(.nearMe)
            // TBHomeNavigations.main.navigate(.instaPay)
        case 3:
            TBHomeNavigations.main.navigate(.cashInPayto)
        case 4:
            TBHomeNavigations.main.navigate(.cashOutPayto)
        case 5:
            TBHomeNavigations.main.navigate(.ReqMoney)
        default:
            println_debug("default")
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = scrollView.contentOffset.x
        let w = scrollView.bounds.size.width
        let currentPage = Int(ceil(x/w))
        pageControl.currentPage = currentPage
        // Do whatever with currentPage.
    }
}


//extension OKDollarWalletViewController{
//    fileprivate func getQR(){
//        if appDelegate.checkNetworkAvail() {
//            progressViewObj.showProgressView()
//            nearByViewModel.getDataForQRScan( finished:{
//                progressViewObj.removeProgressView()
//                //checking if data is available
//                if let isValidObj = self.nearByViewModel.nearByQRModelObj{
//                    if let qrData = isValidObj.data{
//                        DispatchQueue.main.async {
//                            if let validImage = UitilityClass.getQRImage(stringQR:qrData,withSize: 10,logoName: "NearMeNew"){
//                                TBHomeNavigations.main.navigateToQRWithData(qrImage: validImage,isComingFromQR: false)
//                            }
//                        }
//                    }else{
//                        self.showErrorAlert(errMessage: isValidObj.msg ?? "")
//                    }
//                }else{
//                    self.showErrorAlert(errMessage: "Some error happen please try again".localized)
//                }
//            })
//        } else {
//            self.noInternetAlert()
//            progressViewObj.removeProgressView()
//        }
//    }
//}
//
//
//extension OKDollarWalletViewController: BioMetricLoginDelegate{
//    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String){
//        if isSuccessful{
//            self.getQR()
//        }else{
//            self.showErrorAlert(errMessage: "Login not successful so cannot generate QR".localized)
//        }
//    }
//}
