//
//  OkWalletCollectionViewCell.swift
//  OK
//
//  Created by iMac on 1/31/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class OkWalletCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var constraintWImgView: NSLayoutConstraint!
    @IBOutlet weak var constraintHImgView: NSLayoutConstraint!
    @IBOutlet weak var constraintTImgView: NSLayoutConstraint!
    
    let gifManager = SwiftyGifManager(memoryLimit:6)
    
    var uniqueID = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.titleLabel.font = UIFont(name: appFont, size: 13.0)
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageV.image = nil
        self.imageV.gifImage = nil
    }
    
    func wrapDataWithStaticImages(_ text: String, _ img: Any, forTopDownView: Bool = false) {
        
        if text == "Mobile Number" {
            self.titleLabel.text = UserModel.shared.mobileNo.replacingOccurrences(of: "0095", with: "0")//UserModel.shared.formattedNumber
        } else {
            self.titleLabel.text = text.localized
        }
        
        if text == "Request Money", let _ = img as? UIImage {
            self.titleLabel.text = "Request Money Dashboard".localized
        }
        self.titleLabel.textColor = UIColor.init(hex: "0321AA")
        
        imageV.backgroundColor = .white
        if forTopDownView {
            let imageSize: CGFloat = 40
            self.constraintHImgView.constant = imageSize
            self.constraintWImgView.constant = imageSize
            self.constraintTImgView.constant = 0
            self.layoutIfNeeded()
            imageV.layer.cornerRadius = imageSize / 2.0
            imageV.layer.masksToBounds = false
            imageV.clipsToBounds = true
            self.titleLabel.isHidden = true
            self.layoutIfNeeded()
        } else {
            imageV.frame = CGRect(x: imageV.frame.origin.x, y: imageV.frame.origin.y, width: imageV.frame.height, height: imageV.frame.height)
            imageV.layer.cornerRadius = imageV.frame.width / 2.0
            imageV.layer.masksToBounds = false
            imageV.clipsToBounds = true
        }
        self.matchingText = text
        
        if let staticImage = img as? UIImage {
            self.imageV.image = staticImage
        } else if let dynamicImageString = img as? String {
            if dynamicImageString.hasSuffix("gif") {
                DispatchQueue.main.async {
                    var imageName = dynamicImageString
                    imageName = String(imageName.dropLast(4))
                    self.imageV.loadGif(name: imageName)
                }
            } else {
                DispatchQueue.main.async {
                    self.imageV.image = nil
                    self.imageV.image = UIImage(named: dynamicImageString)
                }
               
            }
        }
        self.titleLabel.font = UIFont(name: appFont, size: 13.0)

    }
    
}
