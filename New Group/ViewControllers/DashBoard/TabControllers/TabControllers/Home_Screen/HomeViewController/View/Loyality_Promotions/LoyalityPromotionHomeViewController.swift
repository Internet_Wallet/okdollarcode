//
//  LoyalityPromotionHomeViewController.swift
//  OK
//
//  Created by Ashish on 1/15/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class LoyalityPromotionHomeViewController: UIViewController {

    @IBOutlet var titleHeader: UILabel!{
        didSet {
            titleHeader.font = UIFont(name: appFont, size: 13.0)
        }
    }
    @IBOutlet var mainCollectionView: UICollectionView!
    @IBOutlet var subCollectionView: UICollectionView!
    
    // height constraints
    @IBOutlet weak var gapHeight: NSLayoutConstraint!
    @IBOutlet weak var headerHeight: NSLayoutConstraint!
    @IBOutlet weak var maincollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var subCollectionHeight: NSLayoutConstraint!
    
    var delegate : TBHomeReloadingDelegate?
    
    var dataArrayPersonal = [String]()
    var dummyArrayPersonal = ["Buy & Sale Bonus Point Tab", "Lucky draw & Coupon", "Earning Point Transfer Tab", "Redeem Bonus Point Tab", "Voting", "Offers"]
    
    var imgArrayPersonal = [Any]()
    var dummyImgArrayPersonal = ["dashboard_buy_sale",#imageLiteral(resourceName: "dashboard_lucky_draw"), #imageLiteral(resourceName: "dashboardEPT"), #imageLiteral(resourceName: "dashboardRP"), #imageLiteral(resourceName: "voting_dashboard") , #imageLiteral(resourceName: "NoOffersFound") ] as [Any]
    
    var dataArray = [String]()
    var dummyDataArray = ["Buy & Sale Bonus Point Tab","Bonus Point Setup Tab", "Lucky draw & Coupon", "Earning Point Transfer Tab", "Redeem Bonus Point Tab", "Voting", "Offers"]
    
    var imgArray = [Any]()
    var dummyimgArray =  ["dashboard_buy_sale", "merchant_confi.png", #imageLiteral(resourceName: "dashboard_lucky_draw") , #imageLiteral(resourceName: "dashboardEPT"), #imageLiteral(resourceName: "dashboardRP"),  #imageLiteral(resourceName: "voting_dashboard") , #imageLiteral(resourceName: "NoOffersFound")] as [Any]
    
    var subDataArray = [""]
    var subImgArray = [""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refLoyality = self
        self.titleHeader.text = "Offers, Coupon & Bonus Points".localized
        self.titleHeader.textColor = UIColor.init(hex: "0321AA")
        
        if UserModel.shared.agentType == .user {
            if dummyArrayPersonal.count < 4 {
                self.maincollectionHeight.constant = 105
            }else if dummyArrayPersonal.count < 7 {
                self.maincollectionHeight.constant = 210
            }else {
                self.maincollectionHeight.constant = 315
            }
            dataArrayPersonal = dummyArrayPersonal
            imgArrayPersonal = dummyImgArrayPersonal
        }else {
            if dummyDataArray.count < 4 {
                self.maincollectionHeight.constant = 105
            }else if dummyDataArray.count < 7 {
                self.maincollectionHeight.constant = 210
            }else {
                self.maincollectionHeight.constant = 315
            }
            dataArray = dummyDataArray
            imgArray = dummyimgArray
        }
 
        self.subCollectionHeight.constant = 0.0
        self.setLayoutHeight()
        
        self.mainCollectionView.register(UINib(nibName: TBHomeModel.cellDetails, bundle: nil), forCellWithReuseIdentifier: TBHomeModel.cellDetails)
        self.mainCollectionView.delegate = self
        self.mainCollectionView.dataSource = self
        
        self.subCollectionView.register(UINib(nibName: TBHomeModel.cellDetails, bundle: nil), forCellWithReuseIdentifier: TBHomeModel.cellDetails)
        self.subCollectionView.delegate = self
        self.subCollectionView.dataSource = self
        self.reload()
        NotificationCenter.default.addObserver(self, selector: #selector(LoyalityPromotionHomeViewController.languageChangeObservation), name: NSNotification.Name(rawValue: languageKeyDashboard), object: nil)

    }
    
    func reload() {
      
        if UserModel.shared.agentType == .user {
            dataArrayPersonal.removeAll()
            imgArrayPersonal.removeAll()
            for (index , element) in dummyArrayPersonal.enumerated() {
                if index == 0 && isBuySaleBonusPointShow {
                    dataArrayPersonal.append(element)
                    imgArrayPersonal.append(dummyImgArrayPersonal[index])
                }
                if index == 1 {
                    dataArrayPersonal.append(element)
                    imgArrayPersonal.append(dummyImgArrayPersonal[index])
                }
                if index == 2 && isLuckyDrawShow {
                    dataArrayPersonal.append(element)
                    imgArrayPersonal.append(dummyImgArrayPersonal[index])
                }
                if index == 3 && isEarningPointShow {
                    dataArrayPersonal.append(element)
                    imgArrayPersonal.append(dummyImgArrayPersonal[index])
                }
                if index == 4 {
                    dataArrayPersonal.append(element)
                    imgArrayPersonal.append(dummyImgArrayPersonal[index])
                }
                if index == 5 && isVotingshow {
                    dataArrayPersonal.append(element)
                    imgArrayPersonal.append(dummyImgArrayPersonal[index])
                }
                if index == 6 && isOfferShow {
                    dataArrayPersonal.append(element)
                    imgArrayPersonal.append(dummyImgArrayPersonal[index])
                }
            }
        }else {
            dataArray.removeAll()
            imgArray.removeAll()
            for (index , element) in dummyDataArray.enumerated() {
                if index == 0 && isBuySaleBonusPointShow {
                    dataArray.append(element)
                    imgArray.append(dummyimgArray[index])
                }
                if index == 1 {
                    dataArray.append(element)
                    imgArray.append(dummyimgArray[index])
                }

                if index == 2 && isLuckyDrawShow {
                    dataArray.append(element)
                    imgArray.append(dummyimgArray[index])
                }
                if index == 3 && isEarningPointShow {
                    dataArray.append(element)
                    imgArray.append(dummyimgArray[index])
                }
                if index == 4 {
                    dataArray.append(element)
                    imgArray.append(dummyimgArray[index])
                }
                if index == 5 && isVotingshow {
                    dataArray.append(element)
                    imgArray.append(dummyimgArray[index])
                }
                if index == 6 && isOfferShow {
                    dataArray.append(element)
                    imgArray.append(dummyimgArray[index])
                }
            }
        }

        DispatchQueue.main.async {
             if UserModel.shared.agentType == .user {
                if self.dataArrayPersonal.count < 4 {
                       self.maincollectionHeight.constant = 105
                }else if self.dataArrayPersonal.count < 7 {
                       self.maincollectionHeight.constant = 210
                   }else {
                       self.maincollectionHeight.constant = 315
                   }
               }else {
                if self.dataArray.count < 4 {
                       self.maincollectionHeight.constant = 105
                }else if self.dataArray.count < 7 {
                       self.maincollectionHeight.constant = 210
                   }else {
                       self.maincollectionHeight.constant = 315
                   }
               }
              
            self.subCollectionHeight.constant = 0.0
            
            self.setLayoutHeight()
            self.mainCollectionView.reloadData()
            self.subCollectionView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.floatingButtonControl?.window.closeButton?.isHidden = true
        appDel.floatingButtonControl?.window.isHidden = false
        self.titleHeader.font = UIFont(name: appFont, size: 13.0)
    }
    
    private func setLayoutHeight() {
        if subCollectionHeight.constant <= 1 {
            self.subCollectionView.isHidden = true
        } else {
            self.subCollectionView.isHidden = false
        }
        
        let fixedHeight = self.gapHeight.constant + self.headerHeight.constant
        self.dHeight = fixedHeight + self.subCollectionHeight.constant + self.maincollectionHeight.constant
    }
    
    @objc func languageChangeObservation() {
        
        self.titleHeader.text = "Offers, Coupon & Bonus Points".localized
        self.titleHeader.font = UIFont(name: appFont, size: 13.0)

        if UserModel.shared.agentType == .user {
            for (index, _) in dataArrayPersonal.enumerated() {
                if dataArrayPersonal.count > index  {
                    if  let cell = self.mainCollectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? CollectionViewCell {
                        cell.titleLabel.text = dataArrayPersonal[index] .localized
                        cell.titleLabel.font = UIFont(name: appFont, size: 13.0)
                    }
                }
            }
        } else {
            for (index, _) in dataArray.enumerated() {
                if dataArray.count > index  {
                    if  let cell = self.mainCollectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? CollectionViewCell {
                        cell.titleLabel.text = dataArray[index] .localized
                        cell.titleLabel.font = UIFont(name: appFont, size: 13.0)
                    }
                }
            }
        }
        for (index,_) in subDataArray.enumerated() {
            if subDataArray.count > index {
                if let cell = self.subCollectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? CollectionViewCell {
                    cell.titleLabel.text = subDataArray[index] .localized
                    cell.titleLabel.font = UIFont(name: appFont, size: 13.0)
                }
            }
        }
    }
}

extension LoyalityPromotionHomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.mainCollectionView {
            if UserModel.shared.agentType == .user {
                return dataArrayPersonal.count
            }
            return dataArray.count
        } else {
            return subDataArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.mainCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TBHomeModel.cellDetails, for: indexPath) as? CollectionViewCell
            if UserModel.shared.agentType == .user {
                cell?.wrapDataWithStaticImages(dataArrayPersonal[indexPath.row], imgArrayPersonal[indexPath.row])
            } else {
                cell?.wrapDataWithStaticImages(dataArray[indexPath.row], imgArray[indexPath.row])
            }
            return cell!
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TBHomeModel.cellDetails, for: indexPath) as? CollectionViewCell
            cell?.wrapDataWithStaticImages(subDataArray[indexPath.row], subImgArray[indexPath.row])
            return cell!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: screenWidth / 3.2, height: 105.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if UserModel.shared.agentType == .user {
            for (index, _) in dataArrayPersonal.enumerated() {
                if dataArrayPersonal.count > index  {
                    if  let cell = self.mainCollectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? CollectionViewCell {
                        cell.backgroundColor = .clear
                    }
                }
            }
            
        } else {
            for (index, _) in dataArray.enumerated() {
                if dataArray.count > index  {
                    if  let cell = self.mainCollectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? CollectionViewCell {
                        cell.backgroundColor = .clear
                    }
                }
            }
        }
        
        if let cell = collectionView.cellForItem(at: indexPath) as? CollectionViewCell {
            if let text = cell.matchingText {
                switch text.lowercased() {
                case "Bonus Point Setup Tab".lowercased():
                    //                    if collectionView == mainCollectionView {
                    //                        if self.subCollectionView.isHidden {
                    //                            cell.backgroundColor = UIColor.init(hex: "EBEBF1")
                    //                            self.subCollectionHeight.constant = 210.00
                    //                            self.setLayoutHeight()
                    //                            self.delegate?.refreshContentHomeTab(indexPath)
                    //                        } else {
                    //                            self.subCollectionHeight.constant = 0.00
                    //                            self.setLayoutHeight()
                    //                            self.delegate?.refreshContentHomeTab(indexPath)
                    //                        }
                    //                    } else {
                    TBHomeNavigations.main.navigate(.loyalty)
                //                    }
                case "Earning Point Transfer Tab".lowercased():
                    TBHomeNavigations.main.navigate(.earnPointTransfer)
                case "Redeem Bonus Point Tab".lowercased():
                    TBHomeNavigations.main.navigate(.redeemPoint)
//                case "Promotions Tab".lowercased():
//                    TBHomeNavigations.main.navigate(.promotions)
                case "Lucky draw & Coupon".lowercased():
                    TBHomeNavigations.main.navigate(.luckyDrawCoupon)
                case "Voting".lowercased():
                    TBHomeNavigations.main.navigate(.voting)
                case "Buy & Sale Bonus Point Tab".lowercased():
                    TBHomeNavigations.main.navigate(.BuyBonusPoint)
                case "Sale Bonus Point Tab".lowercased():
                    TBHomeNavigations.main.navigate(.bonusPointResale)
                case "Offers".lowercased():
                    TBHomeNavigations.main.navigate(.offers)
                default:
                    break
                }
            }
        }
    }
}
