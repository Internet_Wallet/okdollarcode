//
//  TBHomeNavigations.swift
//  OK
//
//  Created by Ashish on 1/11/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class TBHomeNavigations: NSObject {
    
    var home: TBHomeViewController!
    var qrImage: UIImage?
    var isComingFromQR = false
    
    static let main = TBHomeNavigations()
    
    func startSyncing(_ home: TBHomeViewController) {
        self.home = home
    }
    
    func checkKYCShowviewStatus() -> Bool {
        if kycAppControlInfo.first?.kycApprovalStatus == 0 || kycAppControlInfo.first?.kycApprovalStatus == 1 {
            return true
        }else if (kycAppControlInfo.first?.kycApprovalStatus == -1 || kycAppControlInfo.first?.kycApprovalStatus == 2) && kycAppControlInfo.first?.kycSignupPriority == 0 {
            return false
        } else if (kycAppControlInfo.first?.kycApprovalStatus == -1 || kycAppControlInfo.first?.kycApprovalStatus == 2 || kycAppControlInfo.first?.kycSignupPriority == -1 ) {
            let remindMe = UserDefaults.standard.bool(forKey: "KYCRemindMeLater")
            guard let time = UserDefaults.standard.value(forKey: "KYCUpdateTime") as? Date else {
                UserDefaults.standard.set(Date(), forKey: "KYCUpdateTime")
                return false
            }
            let calendar = Calendar.current
            let oldDateComponents = calendar.dateComponents([.month, .day, .hour, .minute], from: time)
            let newDateComponents = calendar.dateComponents([.month, .day, .hour, .minute], from: Date())
            let difference = calendar.dateComponents([.minute], from: oldDateComponents, to: newDateComponents).minute!
            if remindMe {
                if difference > 1440 {
                    UserDefaults.standard.set(false, forKey: "KYCRemindMeLater")
                    return false
                }
            } else {
                if difference > 30 {
                    return false
                }
            }
        }
        return true
    }
    
    //Tushar
    //made this function because i wanted to pass data to my viewcontroller as there no function here to pass data
//    func navigateToQRWithData(qrImage: UIImage,isComingFromQR: Bool){
//        self.home.navigateToNearByNew(qrImage: qrImage, isComingFromTaxPayment: isComingFromQR)
//    }
    
    func navigateToQRWithData(isComingFromQR: Bool){
        self.home.navigateToNearByNew(isComingFromTaxPayment: isComingFromQR)
    }
    
    func navigate(_ navEnum : HomeNavigationActions) {
        let kycStatus = self.checkKYCShowviewStatus()
        if kycStatus == false {
            self.home.showKYCinfoView()
            return
        }
        switch navEnum {
        case .payto:
            self.home.navigateToPayto()
        case .transferTo:
            self.home.navigateTo_transferTo_Payto()
        case .cashInPayto:
            self.home.navigateTo_cashInOut_Payto(type: .cashInPayto)
        case .cashOutPayto:
            self.home.navigateTo_cashInOut_Payto(type: .cashOutPayto)
        case .addWithdraw:
            self.home.navigateToAddWithDraw()
        case .bankDepositVerification:
            self.home.navigateToBankDepositVerification()
        case .bankCounterDeposit:
            self.home.navigateToBankCounterDeposit()
        case .nearbyOKServices:
            self.home.navigateToNearByOKServices()
        case .okChat:
            self.home.navigateToOKChat()
        case .cashIN:
            self.home.navigateToCashIN()
        case .cashOUT:
            self.home.navigateToCashOUT()
        case .ReqMoney:
            self.home.navigateToRequestMoney()
        case .BillSpliter:
            self.home.navigateToBillSplitter()
        case .resale:
            self.home.navigateToReSale()
        case .mobileNumber:
            self.home.navigateToMobileNumber()
        case .otherNumber:
            self.home.navigateToOtherNumber()
        case .OverseasTopUp:
            self.home.navigateToOverseasRecharge()
        case .landlineBill:
            self.home.navigateToLandlineBill()
        case .postpaidMobile:
            self.home.navigateToPostpaidMobile()
        case .ThaiMobRecharge:
            self.home.navigateToThaiMobileRecharge()
        case .ChinaMobRecharge:
            self.home.navigateToChinaMobileRecharge()
        case .skynet:
            self.home.navigateToDth()
        case .airtel:
            self.home.navigateToDth()
        case .dishTv:
            self.home.navigateToDth()
        case .sunDirect:
            self.home.navigateToDth()
        case .reliance:
            self.home.navigateToDth()
        case .videocon:
            self.home.navigateToDth()
        case .tataSky:
            self.home.navigateToDth()
        case.dishHome:
            self.home.navigateToDth()
        case .electricity:
            self.home.navigateToElectricitybill()
        case .taxPayment:
            self.home.navigateToTaxPayment()
        case .lottery:
            self.home.navigateToLottery()
        case .loyalty:
            self.home.navigateToLoyalty()
        case .promotions:
            self.home.navigateToPromotions()
        case .luckyDrawCoupon:
            self.home.navigateToLuckyDrawCoupon()
        case .flight:
            self.home.navigateToFlights()
        case .hotel:
            self.home.navigateToHotel()
        case .intercityBus:
            self.home.navigateToIntercityBus()
//        case .LiveTaxi:
//
//            self.home.navigateToLiveTaxi()
        case .train:
            self.home.navigateTo_Train_Bus_Toll_Ferry(type: .bus)
        case .bus:
            self.home.navigateTo_Train_Bus_Toll_Ferry(type: .bus)
        case .toll:
            self.home.navigateTo_Train_Bus_Toll_Ferry(type: .toll)
        case .ferry:
            self.home.navigateTo_Train_Bus_Toll_Ferry(type: .ferry)
        case .voucher:
            self.home.navigateToVoucher()
        case .foodwallet:
            self.home.navigateToFoodWallet()
        case .merchantPayment:
            self.home.navigateToMerchantPayment()
        case .mobileElectronics:
            self.home.navigateToMobileAndElectronics()
        case .offers:
            self.home.navigateToOffers()
        case .voting:
            self.home.navigateToVoting()
        case .solarElectricity:
            self.home.navigateToSolarElectricity()
        case .donation:
            self.home.navigateToDonation()
        case .earnPointTransfer:
            self.home.navigateToEarnPointTransfer()
        case .redeemPoint:
            self.home.navigateToRedeem()
        case .sunKingPayment:
            self.home.navigateToSunKingorInsurancePayment(str: "SunKing")
        case .buyTopupPoint:
            self.home.navigateToBuyTopupBonusPoint()
        case .BuyBonusPoint:
            self.home.navigateToBuyBonusPoint()
        case .bonusPointResale:
            self.home.navigateToResaleBonusPoint()
        case .insurance:
            self.home.navigateToSunKingorInsurancePayment(str: "Insurance")
        case .onestopmart:
            self.home.navigateOneSTopMart()
        case .instaPay:
            self.home.navigateToInstaPay()
        case .sendMoneyToBank:
            self.home.navigateToSendMoneyToBank()
        case .taxPaymentNew:
            self.home.navigateToTaxPaymentNew()
        case .nearMe:
            self.home.navigateToNearMe()
        case .scanPay:
            self.home.navigateToScanPay()
        }
    }
    
    
    
}
