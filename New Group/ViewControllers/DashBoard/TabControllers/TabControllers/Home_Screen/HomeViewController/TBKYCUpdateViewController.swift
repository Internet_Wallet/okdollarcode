//
//  TBKYCUpdateViewController.swift
//  OK
//
//  Created by iMac on 4/24/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol ShowUpdateKYCProtocol: class {
    func showUpdateprofileVC()
}

class TBKYCUpdateViewController: OKBaseController {
    
    weak var delegate: ShowUpdateKYCProtocol?
    
    @IBOutlet weak var remindMeLaterBtn: UIButton! {
        didSet {
            remindMeLaterBtn.setTitle("Remind Me Later".localized, for: .normal)
        }
    }
    @IBOutlet weak var updateNowBtn: UIButton! {
        didSet {
            updateNowBtn.setTitle("Update Now".localized, for: .normal)
        }
    }
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            self.titleLabel.text = self.titleLabel.text?.localized
        }
    }
    @IBOutlet weak var infoLabel: UILabel! {
        didSet {
            self.infoLabel.text = self.infoLabel.text?.localized
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if kycAppControlInfo.first?.kycSignupPriority == 0 {
            self.remindMeLaterBtn.isHidden = true
        }
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        // Do any additional setup after loading the view.
    }

    
    @IBAction func updateNowAction(_ sender: UIButton) {
        UserDefaults.standard.set(Date(), forKey: "KYCUpdateTime")
        self.dismiss(animated: false, completion: nil)
        self.delegate?.showUpdateprofileVC()
    }
    
    @IBAction func remindMeLaterAction(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "KYCRemindMeLater")
        UserDefaults.standard.set(Date(), forKey: "KYCUpdateTime")
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        UserDefaults.standard.set(Date(), forKey: "KYCUpdateTime")
        self.dismiss(animated: true, completion: nil)
    }
}
