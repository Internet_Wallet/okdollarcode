//
//  AllNotificationsTableView.swift
//  OK
//
//  Created by iMac on 5/27/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol AllNotificationDeleteCell: class {
    func delete(cell: AllNotificationsTabCell)
}

class AllNotificationsView: UIView {
    
    var allRecords: [InboxMessages]?
    
    var arrlist = [String]()
    
    let validationmanger = PaymentVerificationManager()
    

    @IBOutlet weak var noRecordsView: UIView! {
        didSet {
            self.noRecordsView.isHidden = true
        }
    }
    
    @IBOutlet weak var noRecordsLabel: UILabel! {
        didSet {
            self.noRecordsLabel.font = UIFont(name: appFont, size: appFontSize)
            self.noRecordsLabel.text = "No Records Found!".localized
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    let refreshControl = UIRefreshControl()

    @IBOutlet weak var clearAllBtn: UIButton! {
        didSet {
            self.clearAllBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
            self.clearAllBtn.setTitle("Clear All".localized, for: .normal)
        }
    }
    
    func langChange() {
        self.noRecordsLabel.font = UIFont(name: appFont, size: appFontSize)
        self.noRecordsLabel.text = "No Records Found!".localized
        self.clearAllBtn.titleLabel?.font = UIFont(name: appFont, size: appButtonSize)
        self.clearAllBtn.setTitle("Clear All".localized, for: .normal)
    }
    
    func setUp() {
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
         if #available(iOS 10.0, *) {
                   tableView.refreshControl = refreshControl
               } else {
                   tableView.addSubview(refreshControl)
               }
               refreshControl.addTarget(self, action: #selector(refreshAllNotification(_:)), for: .valueChanged)
    }
    
    @objc private func refreshAllNotification(_ sender: Any) {
        callRecordsApiNew()
    }
    
    func callRecordsApiNew() {
        
        let currentStatus = UserDefaults.standard.bool(forKey: "Inboxdatabaseinserted")
     //   print("callRecordsApiNew=====\(currentStatus)")
        
        if currentStatus == true {
            
        PaymentVerificationManager.getInboxMessages({ (status, messages) in
      //  print("messages====\(messages)====\(messages?.count)")
       //     print("status====\(status)")

            DispatchQueue.main.async {
                if status {
                    self.allRecords?.removeAll()
                    self.allRecords = messages
                    //println_debug(self.allRecords)
                    if (self.allRecords?.count ?? 0) > 0 {
                        self.noRecordsView.isHidden = true
                        progressViewObj.removeProgressView()
                        self.tableView.reloadData()
                    } else {
                        self.noRecordsView.isHidden = false
                        progressViewObj.removeProgressView()
                    }
                    self.refreshControl.endRefreshing()
                } else {
                    self.noRecordsView.isHidden = false
                    progressViewObj.removeProgressView()
                    self.refreshControl.endRefreshing()
                }
            }
        })
            
        } else {
            
            self.callRecordsApi()
        }
    }
    
    func callRecordsApi() {
        
        
        self.getAllDetailsFromApi(timer: false, date: Date())


      /*  if let date = userDef.object(forKey: "InboxMessageTimerKey") as? Date {
            print("second date----\(date)")
            self.getAllDetailsFromApi(timer: true, date: date)

        } else {
            print("first date----\(Date())")
            self.getAllDetailsFromApi(timer: false, date: Date())
        } */
    }
    
    private func getDate() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        let daysToAdd = -7
        let currentDate = Date()
        var dateComponent = DateComponents()
        dateComponent.day = daysToAdd
        guard let prevDate = Calendar.current.date(byAdding: dateComponent, to: currentDate) else { return "" }
        return formatter.string(from: prevDate)
    }

//    private func getDateTest(date: Date) -> String {
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd hh:MM:ss.SSS"
//        let seconds = 1
//        var dateComponent = DateComponents()
//        dateComponent.second = seconds
//        guard let prevDate = Calendar.current.date(byAdding: dateComponent, to: date) else { return "" }
//        return formatter.string(from: prevDate)
//    }
    
    private func getDateTest(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = userDef.object(forKey: "InboxMessageCreatedDate") as? String
        let seconds = 1
        var dateComponent = DateComponents()
        dateComponent.second = seconds
       // let nowPlusOneSecond = Calendar.current.date(byAdding: .second, value: 1, to: date)!
        
        guard let prevDate = Calendar.current.date(byAdding: dateComponent, to: date) else { return "" }
        return formatter.string(from: prevDate)
    }
    
    private func getParams(timer: Bool, date: Date) -> Dictionary<String, String> {
        var params = [String: String]()
        params["id"] = ""
        //params["mobileNumber"] = String(UserModel.shared.mobileNo.dropFirst(2))
        params["mobileNumber"] = String(UserModel.shared.mobileNo.dropFirst(2))//"959975278302" //
        params["message"] = ""
        let dateTest = getDate()
        if timer {
            params["createdDate"] = self.getDateTest(date: date)
        } else {
            params["createdDate"] = dateTest
        }
        
        return params
    }
    
    private func getAllDetailsFromApi(timer: Bool, date: Date) {
        if appDelegate.checkNetworkAvail() {
            //PTLoader.shared.show()
            let web      = WebApiClass()
            web.delegate = self
            guard let url = URL.init(string: "https://notificationmanager.okdollar.asia/notification/smsmanager") else { return }
            let params = getParams(timer: timer, date: date)
            println_debug("REQUEST URL: \(url)")
            println_debug("params: \(params)")

            web.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", mScreen: "AllNotificationsTab")
        } else {
            println_debug("No Internet")
            self.refreshControl.endRefreshing()
        }
    }
    
    func deletefun() {
        
        print("deletefun=====")

        PaymentVerificationManager.deleteAllInboxMessages(id: "")
        allRecords?.removeAll()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        self.noRecordsView.isHidden = false
        UserDefaults.standard.set(false, forKey: "Inboxdatabaseinserted")

        
    }
    
    @IBAction func deleteAllAction(_ sender: UIButton) {
        PaymentVerificationManager.deleteAllInboxMessages(id: "")
        allRecords?.removeAll()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        self.noRecordsView.isHidden = false
    }
    
    fileprivate func calculateHeight(str: String) -> CGFloat {
        let height = str.heightWithConstrainedWidth(width: (screenWidth - 50.0), font: UIFont(name: appFont, size: 13.0) ?? UIFont.systemFont(ofSize: 13))
        return 67.0 + height
    }

}

extension AllNotificationsView: AllNotificationDeleteCell {
    func delete(cell: AllNotificationsTabCell) {
        DispatchQueue.main.async {
            guard let indexPath = self.tableView.indexPath(for: cell) else { return }
            PaymentVerificationManager.deleteAllInboxMessages(id: self.allRecords?[indexPath.row].id)
            self.allRecords?.remove(at: indexPath.row)
            //print("delete====\(self.allRecords?.count)")
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            if self.allRecords?.count == 0 {
                self.noRecordsView.isHidden = false
            }
        }
    }
}

extension AllNotificationsView: WebServiceResponseDelegate {
    func webResponse(withJson json: AnyObject, screen: String) {
        

        let now = Date()
        
        let formatter = DateFormatter()
        
        //formatter.timeZone = TimeZone.current
        formatter.calendar = Calendar(identifier: .gregorian)

        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        
        let dateString = formatter.string(from: now)
        
        userDef.set(dateString, forKey: "InboxMessageCreatedDate")
        
        print("DateDtring-======\(dateString)")
        
        guard let data = json as? Data else {
            println_debug("Data error")
            return
        }
        do {
            let decoder = JSONDecoder()
            if let castedData = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? String {
                guard let commentData = castedData.data(using: .utf8) else { return }
             
                var arrTempRecords = try decoder.decode([AllNotifyData].self, from: commentData)
                print("arrTempRecords====\(arrTempRecords)")
                arrTempRecords = arrTempRecords.filter{!($0.message?.contains(find: "Records Not Found") ?? true) }
                arrTempRecords = arrTempRecords.filter{!($0.message?.contains(find: "TECHNICAL FAILURE. PLEASE CALL") ?? true) }
                arrTempRecords = arrTempRecords.filter{!($0.message?.contains(find: "WRONG PASSWORD AND ATTEMPED") ?? true) }
                userDef.set(Date(), forKey: "InboxMessageTimerKey")
                
                 let currentStatus = UserDefaults.standard.bool(forKey: "Inboxdatabaseinserted")
                
                if currentStatus == false {
                    
                    PaymentVerificationManager.insertRecordsToInboxMessages(dataModel: arrTempRecords) { (status) in
                        
                        if status == true {
                               // print("DBInserted=====\(status)")

                                UserDefaults.standard.set(true, forKey: "Inboxdatabaseinserted")
                                self.callRecordsApiNew()
                                             
                        } else {
                                UserDefaults.standard.set(false, forKey: "Inboxdatabaseinserted")
                        }
                    }

                }  else {
                   // print("ElseDBInserted=====")
                    self.callRecordsApiNew()
                }

    
            }
        } catch (let error) {
            println_debug(error)
        }
    }
    
    func webErrorResponse(withErrorCode: Int, screenName: String) {
        progressViewObj.showProgressView()
    }
}


extension AllNotificationsView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allRecords?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.calculateHeight(str: self.allRecords?[indexPath.row].message ?? "")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationtabCell", for: indexPath) as? AllNotificationsTabCell
        cell?.delegate = self
      
        cell?.wrapData(model: allRecords?[indexPath.row])
        
        cell?.selectionStyle = .none
        
        return cell!
    }
}


class AllNotificationsTabCell: UITableViewCell {
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    weak var delegate: AllNotificationDeleteCell?
    
    func wrapData(model: InboxMessages?) {
//        if let valued = model{
//            println_debug(valued)
//        }
        let content = model?.message ?? ""
        
        if let index = (content.range(of: "Msg")?.upperBound) {
            let beforeEqualsToContainingSymbol = String(content.prefix(upTo: index))
            let stripped = String(beforeEqualsToContainingSymbol.dropLast(3)).replacingOccurrences(of: ".00", with: "")
             contentLabel.text = stripped
        } else {
            contentLabel.text = content
        }
      
      //  contentLabel.text = model?.message ?? ""
        if let date = model?.createdDate {
            dateLabel.text = date.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy HH:mm:ss")
        } else {
            dateLabel.text = ""
        }
    }
    
    @IBAction func deleteAction(_ sender: UIButton) {
        self.delegate?.delete(cell: self)
    }
    
}


struct AllNotifyData: Codable {
    let id: String?
    let message: String?
    var createdDate, mobileNo: String?
  //  var dateformae = NSDate()
  
    enum CodingKeys: String, CodingKey {
        case mobileNo = "mobileNumber"
        case id = "id"
        case message = "message"
        case createdDate = "createdDate"
    }
}
