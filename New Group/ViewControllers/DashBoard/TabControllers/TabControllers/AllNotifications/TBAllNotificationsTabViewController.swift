//
//  TBAllNotificationsTabVCViewController.swift
//  OK
//
//  Created by iMac on 5/27/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class TBAllNotificationsTabViewController: OKBaseController {

    @IBOutlet weak var mainView: AllNotificationsView!
     var vcTitle : String = ""
     var called : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = vcTitle.localized
        self.mainView.setUp()
        NotificationCenter.default.addObserver(self, selector: #selector(TBAllNotificationsTabViewController.languageChangeObservation), name: NSNotification.Name(rawValue: languageKeyDashboard), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        print("TBAllNotification viewDidAppear called in -------\(vcTitle)")
        print("TBAllNotification viewDidAppear called in -------\(called)")

        DispatchQueue.main.async {
          progressViewObj.showProgressView()
        }

        let currentStatus = UserDefaults.standard.bool(forKey: "Inboxdatabaseinserted")
        print("TBAllNotification status=====\(currentStatus)")
        
        if currentStatus == true {
            
            self.mainView.callRecordsApiNew()
            
        } else {
                UserDefaults.standard.set(false, forKey: "Inboxdatabaseinserted")
                self.mainView.deletefun()
            
               DispatchQueue.main.asyncAfter(deadline: .now() + 4.0, execute: {
                    progressViewObj.showProgressView()
                    self.mainView.callRecordsApi()
               });
        }
    }
    
    @objc func languageChangeObservation() {
        self.mainView.langChange()
    }

}
