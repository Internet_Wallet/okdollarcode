//
//  QRScanResultDetailViewController.swift
//  OK
//
//  Created by E J ANTONY on 02/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class QRScanResultDetailViewController: UIViewController {

    //MARK: - Outlet
    
    
    @IBOutlet weak var transID: ButtonWithImage! {
        didSet {
            transID.setTitle(transID.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var transType: ButtonWithImage! {
        didSet{
            transType.setTitle(transType.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var amount: ButtonWithImage! {
        didSet {
            amount.setTitle("Amount (MMK)".localized, for: .normal)
        }
    }
    
    @IBOutlet weak var senderNum: ButtonWithImage!  {
        didSet {
            senderNum.setTitle(senderNum.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var senderName: ButtonWithImage! {
        didSet {
            senderName.setTitle(senderName.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var receiverNum: ButtonWithImage! {
        didSet {
            receiverNum.setTitle(receiverNum.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var receiverName: ButtonWithImage! {
        didSet {
            receiverName.setTitle(receiverName.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var qrScanningTime: ButtonWithImage! {
        didSet {
            qrScanningTime.setTitle("QR/SMS Scanning Time".localized, for: .normal)
        }
    }
    @IBOutlet weak var qrGenerateTime: ButtonWithImage! {
        didSet {
            qrGenerateTime.setTitle("QR/SMS Generate Time".localized, for: .normal)
        }
    }
    @IBOutlet weak var age: ButtonWithImage! {
        didSet {
            age.setTitle(age.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var gender: ButtonWithImage! {
        didSet {
            gender.setTitle(gender.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var cashBack: ButtonWithImage! {
        didSet {
            cashBack.setTitle(cashBack.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var bonusPoints: ButtonWithImage! {
        didSet {
            bonusPoints.setTitle(bonusPoints.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var tableScanDetail: UITableView!
    @IBOutlet weak var constraintScrollContentHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    //MARK: - Properties
    let scanDetailCellID = "QRScanDetailTableViewCell"
    var modelArray = [PaymentVerification]()
    
    //MARK: - Views Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        tableScanDetail.tableFooterView = UIView()
        AppDelegate.AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeRight)
        modelArray = PaymentVerificationDBManager().getAllPaymentVerificationDataFromDB()
        self.tableScanDetail.reloadData()
        setUpNavigation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        let navBarHeight = navigationController?.navigationBar.frame.size.height ?? 44
        let heightToReduce = navBarHeight == 32 ? 56 : 44
        constraintScrollContentHeight.constant = scrollView.frame.size.height - CGFloat(heightToReduce) + 44
        self.view.layoutIfNeeded()
    }
}

//MARK: - UITableViewDelegate, UITableViewDataSource
extension QRScanResultDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if modelArray.count == 0 {
            let noDataLabel = UILabel(frame: tableView.bounds)
            noDataLabel.text = "No Records".localized
            noDataLabel.font = UIFont(name: appFont, size: 18)
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
        } else {
            tableView.backgroundView = nil
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let scanDetailCell = tableView.dequeueReusableCell(withIdentifier: scanDetailCellID, for: indexPath) as? QRScanDetailTableViewCell
        let detail = modelArray[indexPath.row]
        scanDetailCell!.configureCell(dataSet: detail)
        return scanDetailCell!
    }
}

// MARK: - Additional Methods
extension QRScanResultDetailViewController: NavigationSetUpProtocol {
    func setUpNavigation() {
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: titleViewWidth, height: self.navigationController?.navigationBar.frame.size.height ?? 44)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        
        let backButton = UIButton(type: .custom)
        backButton.setImage(#imageLiteral(resourceName: "backButton").withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        let ypos = (titleViewSize.height - 30 ) / 2 - 5
        backButton.frame = CGRect(x: 0, y: ypos, width: 30, height: 30)
        
        let label = MarqueeLabel(frame: CGRect(x: 0, y: 0, width: navTitleView.frame.size.width - 80, height: navTitleView.frame.size.height))
        label.type = .continuous
        label.speed = .duration(4)
        label.text = "OK$"
        label.textColor = UIColor.white
        if let navFont = UIFont(name: appFont, size: 23) {
            label.font = navFont
        }
        label.center = navTitleView.center
        label.textAlignment = .center
        
        navTitleView.addSubview(backButton)
        navTitleView.addSubview(label)
        self.navigationItem.titleView = navTitleView
    }
    
    /// It shows the home screen
    @objc func backAction() {
        AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        //self.dismiss(animated: true, completion: nil)
      self.navigationController?.popViewController(animated: true)
    }
}
