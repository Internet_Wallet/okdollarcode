//
//  TBNotificationViewController.swift
//  OK
//
//  Created by Ashish on 12/25/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol InBoxightBarShow: class {
    func showInBoxRightBar(sttaus: Bool, getIndex: Int)
}

class TBNotificationViewController: OKBaseController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var headerCollectionView: UICollectionView!
    
    @IBOutlet  var containerView: UIView!
    
     var selectedIndex: Int = 0
    
    var delegate: InBoxightBarShow?
    
     let navigationbarsentbutton = Notification.Name(rawValue:"inboxRightBarHidesent")
     let navigationbarbutton = Notification.Name(rawValue:"inboxRightBarHide")
    
    //prabu Search and Filter
       var inboxSearchBar = UISearchBar()
       var sideview = UIView()
        let fullview = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
       var compareStr = String()
       var indexToShowData = 0
    var searchActive: Bool = false
    var userInfo: Dictionary<String, Any> = [:]

    
    private var tabControllers = [UIViewController]()
    
    var navigation: UINavigationController?
    
    private var cellArray = [NotificationCollectionCell]() {
        didSet {
            if cellArray.count > 0, cellArray.count <= 1  {
                let cell = self.cellArray[0]
                cell.label.textColor = .black
                cell.imageView.isHighlighted = true
            }
        }
    }

    fileprivate let imagesStringArray = [#imageLiteral(resourceName: "tbNotif_notification"),#imageLiteral(resourceName: "tbNotif_update"),#imageLiteral(resourceName: "tbNotif_share")]
    fileprivate let highlightedImage  = [#imageLiteral(resourceName: "tbNotif_notification_hover"), #imageLiteral(resourceName: "tbNotif_update_hover"),#imageLiteral(resourceName: "tbNotif_share_hover")]
    fileprivate let titleArray = ["Notification", "App Update", "Share OK$"]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        
        if device.type == .iPhoneX || device.type == .iPhoneXR || device.type == .iPhoneXS || device.type == .iPhoneXSMax  || device.type == .iPhone11 || device.type == .iPhone11Pro || device.type == .iPhone11ProMax {
        
        if(appDel.currentLanguage == "uni")
               {
                       sideview = UIView(frame: CGRect(x: screenWidth - 185, y: 80, width: 220, height: 200))
               }
               else if(appDel.currentLanguage == "my")
               {
                       sideview = UIView(frame: CGRect(x: screenWidth - 185, y: 80, width: 220, height: 200))
               }
               else{
                       sideview = UIView(frame: CGRect(x: screenWidth - 185, y: 80, width: 220, height: 200))
               }
            
        } else {
            
            if(appDel.currentLanguage == "uni")
            {
                    sideview = UIView(frame: CGRect(x: screenWidth - 185, y: 40, width: 220, height: 250))
            }
            else if(appDel.currentLanguage == "my")
            {
                    sideview = UIView(frame: CGRect(x: screenWidth - 185, y: 40, width: 220, height: 250))
            }
            else{
                    sideview = UIView(frame: CGRect(x: screenWidth - 140, y: 40, width: 200, height: 250))
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Inbox".localized
        
        userDef.removeObject(forKey: "RequestMoney")
        userDef.removeObject(forKey: "RequestMoneyApproval")
        userDef.synchronize()
        
        sideview.isHidden = true
               sideview.layer.cornerRadius = 10;
               sideview.layer.masksToBounds = false;
               sideview.layer.borderWidth = 0.2;
               sideview.layer.shadowColor = UIColor.gray.cgColor
               sideview.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
               sideview.layer.shadowOpacity = 0.5
               sideview.layer.shadowRadius = 10
        
        
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.addCustomBackButton()
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
        
        self.tabControllers.removeAll()
        let story = UIStoryboard(name: "Notifications", bundle: nil)
        guard let notification = story.instantiateViewController(withIdentifier: String.init(describing: NotificationsViewController.self)) as? NotificationsViewController else { return }
        notification.navigation = navigation
        notification.delegate = self
        
        guard let appUpdate = story.instantiateViewController(withIdentifier: String.init(describing: AppUpdateNotifViewController.self)) as? AppUpdateNotifViewController else { return }
        guard let shareOk = story.instantiateViewController(withIdentifier: String.init(describing: ShareOKNotifViewController.self)) as? ShareOKNotifViewController else { return }
        
        tabControllers = [notification, appUpdate, shareOk]
        
        self.changeScreen(0)
        
        NotificationCenter.default.addObserver(self, selector: #selector(TBNotificationViewController.languageChangeObservation), name: NSNotification.Name(rawValue: languageKeyDashboard), object: nil)
        
//        NotificationCenter.default.addObserver(self, selector: #selector(TBNotificationViewController.hideinbox), name: NSNotification.Name(rawValue: "inboxRightBarHide"), object: nil)
//
//
         NotificationCenter.default.addObserver(self, selector: #selector(TBNotificationViewController.hideinbox), name: NSNotification.Name(rawValue: "inboxRightBarHidesent"), object: nil)
        
        
        let nc = NotificationCenter.default
        nc.addObserver(forName:navigationbarbutton, object:nil, queue:nil, using:navigationbarhide)
        nc.addObserver(forName:navigationbarsentbutton, object:nil, queue:nil, using:navigationbarhide)

    }
    
    @objc func languageChangeObservation() {
        for (index, _) in titleArray.enumerated() {
            if titleArray.count > index  {
                if  let cell = self.headerCollectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? NotificationCollectionCell {
                   // cell.label.font = UIFont(name: appFont, size: appFontSize)
                    cell.label.text = titleArray[index] .localized
                   // cell.label.font = UIFont(name: appFont, size: 13.0)
                }
            }
        }
    }
    
    @objc func hideinbox()
    {
//        DispatchQueue.main.async {
//            self.navigationItem.rightBarButtonItems = nil
//              self.fullview.isHidden = true
//              self.sideview.isHidden = true
//              self.inboxSearchBar.isHidden = true
//              self.navigationItem.titleView = nil
//              //self.navigationItem.rightBarButtonItem  = nil
//            //self.navigationItem.leftBarButtonItem   = nil
//              self.title = "Inbox".localized
//        }
    }
    
    func navigationbarhide(notification:Notification) -> Void {
        
        let tmpData = UserDefaults.standard.value(forKey: "SearchButtonHideShow") as! String

        println_debug("TBNotificationView navigationbarhide called---\(notification.userInfo?["HideSearch"] as? Bool ?? true)----\(tmpData)")
        
        if ((notification.userInfo?["HideSearch"] as? Bool) == true)
        {
            if (tmpData == "2") || (tmpData == "3") {
                self.inboxRightBar()
            } else {
                self.navigationItem.rightBarButtonItems = nil
                self.fullview.isHidden = true
                self.sideview.isHidden = true
                inboxSearchBar.isHidden = true
                self.navigationItem.titleView = nil
                //self.navigationItem.rightBarButtonItem  = nil
                //self.navigationItem.leftBarButtonItem   = nil
                self.title = "Inbox".localized
            }
            
            //                self.navigationItem.rightBarButtonItems = nil
            //                self.fullview.isHidden = true
            //                self.sideview.isHidden = true
            //                inboxSearchBar.isHidden = true
            //                self.navigationItem.titleView = nil
            //                //self.navigationItem.rightBarButtonItem  = nil
            //                //self.navigationItem.leftBarButtonItem   = nil
            //                self.title = "Inbox".localized
        }
        else
        {
             if (tmpData == "2") || (tmpData == "3") {
                self.inboxRightBar()
            }
        }
    }
    

    private func addCustomBackButton() {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "tabBarBack"), for: UIControl.State.normal)
        button.tintColor = UIColor.white
        
        button.sizeToFit()
        button.addTarget(self, action: #selector(dismissScreen), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        let changingCell = self.cellArray[0]
        cellArray.forEach { (cell) in
            if changingCell == cell {
                cell.label.textColor = .black
                cell.imageView.isHighlighted = true
            } else {
                cell.label.textColor = .lightGray
                cell.imageView.isHighlighted = false
            }
        }

        //self.tabControllers.removeAll()
    }
    
    //MARK:- Change Screen Function
    func changeScreen(_ tag: Int) {
        
        print("changeScreen======\(tag)")
        
        if tag != 0 {
            //Not using delegate param here ie getIndex
            // self.delegate?.showInBoxRightBar(sttaus: false, getIndex: 0)
            self.navigationItem.rightBarButtonItems = nil
            self.fullview.isHidden = true
            self.sideview.isHidden = true
            inboxSearchBar.isHidden = true
            self.navigationItem.titleView = nil
            //   self.navigationItem.rightBarButtonItem  = nil
            //  self.navigationItem.leftBarButtonItem   = nil
            self.title = "Inbox".localized
        }
        
        if tag == 2 {
            alertViewObj.wrapAlert(title: nil, body:"Do you want to share OK$ App?".localized, img: #imageLiteral(resourceName: "share_gray"))
            
            alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
                
            }
            alertViewObj.addAction(title: "App Link".localized, style: .cancel) {
                           self.shareAction()
                       }
            alertViewObj.showAlert(controller: self)
            
        } else {
            let previousIndex = selectedIndex
            
            selectedIndex = tag
            
            let previousVC = tabControllers[previousIndex]
            previousVC.willMove(toParent: nil)
            previousVC.view.removeFromSuperview()
            previousVC.removeFromParent()
            
            let selectedVC : UIViewController!
            
            selectedVC = tabControllers[selectedIndex]
            addChild(selectedVC)
            selectedVC.view.frame = containerView.bounds
            
            self.containerView.addSubview(selectedVC.view)
            selectedVC.didMove(toParent: self)
        }
    }
    
     func shareAction() {
        
        let textToShare: String = "Share OK$ application to your family & Friends"
        let link: String = "https://itunes.apple.com/us/app/ok-$/id1067828611?ls=1&mt=8"
        
        let url:NSURL = NSURL.init(string: link)!
        var activityItems = [Any]()
        activityItems.append(textToShare)
        activityItems.append(url)
        
        let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activityViewController.setValue("Hey this is an awsome app.You also download and enjoy the features", forKey: "subject")
        activityViewController.excludedActivityTypes = [.assignToContact, .print]
        DispatchQueue.main.async { self.present(activityViewController, animated: true, completion: nil) }

    }
    
    //MARK: Search Manage Delegate
        func setupSearchBar() {
            inboxSearchBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
          //  inboxSearchBar.showsCancelButton = true
            inboxSearchBar.delegate =  self
            
          //  inboxSearchBar.tintColor = UIColor.darkGray
            inboxSearchBar.barTintColor = UIColor.darkGray
            inboxSearchBar.searchBarStyle = UISearchBar.Style.minimal
            inboxSearchBar.placeholder = "Search".localized
            self.navigationItem.titleView = inboxSearchBar
            if let searchTextField = inboxSearchBar.value(forKey: "searchField") as? UITextField, let myFont = UIFont(name: appFont, size: 18) {
                if searchTextField.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                   
                    if appDel.currentLanguage == "my" || appDel.currentLanguage == "uni"{
                        searchTextField.font = myFont
                    }else{
                        //
                    }
                    //searchTextField.font = myFont
                    searchTextField.keyboardType = .asciiCapable
                    searchTextField.placeholder = "Search".localized
                    searchTextField.tintColor = UIColor.darkGray
                    searchTextField.backgroundColor  = .white
                }
            }
            let view = self.inboxSearchBar.subviews[0] as UIView
            let subViewsArray = view.subviews
            for subView: UIView in subViewsArray {
                if subView.isKind(of: UITextField.self) {
                    subView.tintColor = UIColor.gray
                }
            }
            
            inboxSearchBar.becomeFirstResponder()
        }
        
        func resignSearchBarKeypad() {
            inboxSearchBar.resignFirstResponder()
        }
        
        func hideSearchWithoutAnimation() {
         //   isHeaderSearchOpened = false
        //    inboxSearchBar.text = ""
            inboxSearchBar.resignFirstResponder()
            inboxSearchBar.isHidden = true
        }
        
        //MARK: inboxRightBar
             func inboxRightBar() {
                 
                 let searchImage = UIImage(named: "r_search")!
                 let clipImage = UIImage(named: "menu_white_payto")!
                 
                 let searchBtn: UIButton = UIButton(type: UIButton.ButtonType.custom)
                 searchBtn.setImage(searchImage, for: UIControl.State.normal)
                 searchBtn.addTarget(self, action: #selector(searchInBoxAction), for: .touchUpInside)
                 searchBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                 let searchBarBtn = UIBarButtonItem(customView: searchBtn)
                 
                 let clipBtn: UIButton = UIButton(type: UIButton.ButtonType.custom)
                 clipBtn.setImage(clipImage, for: UIControl.State.normal)
                 clipBtn.addTarget(self, action: #selector(moreInBoxAction), for: .touchUpInside)
                 clipBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                 let clipBarBtn = UIBarButtonItem(customView: clipBtn)
                 self.navigationItem.rightBarButtonItems = [clipBarBtn, searchBarBtn]
                // self.navigation?.navigationItem.rightBarButtonItems = [clipBarBtn, searchBarBtn]
                 
             }
         
       
              @objc func searchInBoxAction() {
                  sideview.isHidden = true
                  fullview.isHidden = true
                 inboxSearchBar.isHidden = false
                 //   self.navigationTitleViewSetup()
                 compareStr = "0"
                 self.setupSearchBar()
             }
             
             
             @objc func moreInBoxAction() {
                // let sideview = UIView(frame: CGRect(x: screenWidth - 140, y: 0, width: 140, height: 200))
                 
                 var locaArray = [String]()
                 inboxSearchBar.isHidden = true
                 self.navigationItem.titleView = nil
                 // self.navigationItem.rightBarButtonItem  = nil
                // self.navigationItem.leftBarButtonItem   = nil
                 self.title = "Inbox".localized
                 
                 // sideview = UIView(frame: CGRect(x: screenWidth - 250, y: 0, width: 250, height: 200))
                 sideview.isHidden = false
                 fullview.isHidden = false
                // locaArray = ["All", "Pending Approval", "Request Approved", "Cancelled"]
                 fullview.backgroundColor = .clear
                 self.view.addSubview(fullview)
                 self.view.bringSubviewToFront(fullview)
              
            
                 if indexToShowData == 2 {
                         
                      sideview.isHidden = false
                      fullview.isHidden = false
                     locaArray = ["All", "Pending Approval", "Request Approved", "Cancelled"]
                     fullview.backgroundColor = .clear
                     self.view.addSubview(fullview)
                     self.view.bringSubviewToFront(fullview)
                   
                 }
                else if indexToShowData == 3 {
                     
                     sideview.isHidden = false
                     fullview.isHidden = false
                     locaArray =  ["Pending", "Successful", "Reject", "Scheduled"]
                     
                     fullview.backgroundColor = .clear
                     self.view.addSubview(fullview)
                     self.view.bringSubviewToFront(fullview)
                 }
                 else {
                       fullview.isHidden = true
                       sideview.isHidden = true
                    
                 }
                 
                 if compareStr == "0"
                 {
                       sideview.isHidden = false
                        compareStr = "1"

                 }
                 else if compareStr == "1"
                 {
                     sideview.isHidden = true
                     compareStr = "0"
                 }
               
               if device.type == .iPhoneX || device.type == .iPhoneXR || device.type == .iPhoneXS || device.type == .iPhoneXSMax  || device.type == .iPhone11 || device.type == .iPhone11Pro || device.type == .iPhone11ProMax {
                
                 sideview.backgroundColor = .white
                 let myButton = UIButton(type: UIButton.ButtonType.custom) // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
                 myButton.backgroundColor = UIColor.white
                 myButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
                 myButton.setTitle(locaArray[0].localized, for: .normal)
                 myButton.tag = 0
                // myButton.titleLabel?.font = UIFont(name: appFont, size: 15)
                 myButton.setTitleColor(UIColor.black, for: .normal)
                 myButton.frame = CGRect(x: 10, y: 10, width: 250, height: 40)
                 myButton.addTarget(self, action: #selector(calllInBoxAction(_:)), for: .touchUpInside)
                 myButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
                 sideview.addSubview( myButton)
                 
                 let myButton1 = UIButton(type: UIButton.ButtonType.custom) // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
                 myButton1.backgroundColor = UIColor.white
                 myButton1.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
                 myButton1.setTitle(locaArray[1].localized, for: .normal)
                 myButton1.tag = 1
                // myButton1.titleLabel?.font = UIFont(name: appFont, size: 15)
                 myButton1.setTitleColor(UIColor.black, for: .normal)
                 myButton1.frame = CGRect(x: 10, y: 60, width: 250, height: 40)
                 myButton1.addTarget(self, action: #selector(calllInBoxAction(_:)), for: .touchUpInside)
                 myButton1.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
                 sideview.addSubview( myButton1)
                 
                 let myButton2 = UIButton(type: UIButton.ButtonType.custom) // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
                 myButton2.backgroundColor = UIColor.white
                 myButton2.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
                 myButton2.setTitle(locaArray[2].localized, for: .normal)
                 myButton2.tag = 2
               //  myButton2.titleLabel?.font = UIFont(name: appFont, size: 15)
                 myButton2.setTitleColor(UIColor.black, for: .normal)
                 myButton2.frame = CGRect(x: 10, y: 110, width: 250, height: 40)
                 myButton2.addTarget(self, action:#selector(calllInBoxAction(_:)), for: .touchUpInside)
                 myButton2.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
                 sideview.addSubview( myButton2)
                 
                 let myButton3 = UIButton(type: UIButton.ButtonType.custom) // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
                 myButton3.backgroundColor = UIColor.white
                 myButton3.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
                 myButton3.setTitle(locaArray[3].localized, for: .normal)
                 myButton3.tag = 3
                // myButton3.titleLabel?.font = UIFont(name: appFont, size: 15)
                 myButton3.setTitleColor(UIColor.black, for: .normal)
                 myButton3.frame = CGRect(x: 10, y: 160, width: 250, height: 40)
                 myButton3.addTarget(self, action: #selector(calllInBoxAction(_:)), for: .touchUpInside)
                 myButton3.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
                 sideview.addSubview( myButton3)
                
               } else {
                
                sideview.backgroundColor = .white
                let myButton = UIButton(type: UIButton.ButtonType.custom) // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
                myButton.backgroundColor = UIColor.white
                myButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
                myButton.setTitle(locaArray[0].localized, for: .normal)
                myButton.tag = 0
              //  myButton.titleLabel?.font = UIFont(name: appFont, size: 15)
                myButton.setTitleColor(UIColor.black, for: .normal)
                myButton.frame = CGRect(x: 10, y: 60, width: 250, height: 40)
                myButton.addTarget(self, action: #selector(calllInBoxAction(_:)), for: .touchUpInside)
                myButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
                sideview.addSubview( myButton)
                
                let myButton1 = UIButton(type: UIButton.ButtonType.custom) // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
                myButton1.backgroundColor = UIColor.white
                myButton1.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
                myButton1.setTitle(locaArray[1].localized, for: .normal)
                myButton1.tag = 1
               // myButton1.titleLabel?.font = UIFont(name: appFont, size: 15)
                myButton1.setTitleColor(UIColor.black, for: .normal)
                myButton1.frame = CGRect(x: 10, y: 110, width: 250, height: 40)
                myButton1.addTarget(self, action: #selector(calllInBoxAction(_:)), for: .touchUpInside)
                myButton1.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
                sideview.addSubview( myButton1)
                
                let myButton2 = UIButton(type: UIButton.ButtonType.custom) // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
                myButton2.backgroundColor = UIColor.white
                myButton2.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
                myButton2.setTitle(locaArray[2].localized, for: .normal)
                myButton2.tag = 2
               // myButton2.titleLabel?.font = UIFont(name: appFont, size: 15)
                myButton2.setTitleColor(UIColor.black, for: .normal)
                myButton2.frame = CGRect(x: 10, y: 160, width: 250, height: 40)
                myButton2.addTarget(self, action:#selector(calllInBoxAction(_:)), for: .touchUpInside)
                myButton2.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
                sideview.addSubview( myButton2)
                
                let myButton3 = UIButton(type: UIButton.ButtonType.custom) // if you want to set the type use like UIButton(type: .RoundedRect) or UIButton(type: .Custom)
                myButton3.backgroundColor = UIColor.white
                myButton.titleLabel?.font = UIFont(name: appFont, size: appFontSize)
                myButton3.setTitle(locaArray[3].localized, for: .normal)
                myButton3.tag = 3
              //  myButton3.titleLabel?.font = UIFont(name: appFont, size: 15)
                myButton3.setTitleColor(UIColor.black, for: .normal)
                myButton3.frame = CGRect(x: 10, y: 210, width: 250, height: 40)
                myButton3.addTarget(self, action: #selector(calllInBoxAction(_:)), for: .touchUpInside)
                myButton3.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
                sideview.addSubview( myButton3)
                
                }
                 
                 
                 self.view.addSubview(sideview)
                 self.view.bringSubviewToFront(sideview)
             }

             @objc func calllInBoxAction(_ sender: UIButton) {
                 if indexToShowData == 2 {
                     callNotificationForSideMenuDropDown(index: sender.tag)
                 }
               else
                 {
                     callNotificationForSideMenu(index: sender.tag)
                 }
                 
             }
             
             override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
                 let touch = touches.first
                 if touch?.view == self.sideview {
                     sideview.isHidden = false
                     fullview.isHidden = false
                       compareStr = "1"
                 }
                 else
                 {
                      fullview.isHidden = true
                      sideview.isHidden = true
                       compareStr = "0"
                 }
             }
    
    //MARK:- CollectionView Delegates & Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if cellArray.contains(where: {$0.label.text == titleArray[indexPath.row]}) {
            cellArray[indexPath.row].label.text = titleArray[indexPath.row].localized
            return cellArray[indexPath.row]
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String.init(describing: NotificationCollectionCell.self), for: indexPath) as? NotificationCollectionCell
        cell?.wrapData(imagesStringArray[indexPath.row], titleArray[indexPath.row], highlightedImage[indexPath.row])
        cellArray.append(cell!)
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = screenWidth / 3.2
        return CGSize.init(width: width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        let changingCell = self.cellArray[indexPath.row]
        cellArray.forEach { (cell) in
            if changingCell == cell {
                cell.label.textColor = .black
                cell.imageView.isHighlighted = true
            } else {
                cell.label.textColor = .lightGray
                cell.imageView.isHighlighted = false
            }
        }

        self.changeScreen(indexPath.row)
    }
    
}

extension TBNotificationViewController{
    
    fileprivate func callNotificationForSideMenuDropDown(index: Int){
        let passIndex:[String: Int] = ["Title": index+1]
        NotificationCenter.default.post(name: NSNotification.Name("DashboardVC"), object: nil, userInfo: passIndex)
        sideview.isHidden = true
        fullview.isHidden = true
        compareStr = "0"
    }
    
    fileprivate func callNotificationForSideMenu(index: Int){
        let passIndex:[String: Int] = ["Title2": index+1]
        NotificationCenter.default.post(name: NSNotification.Name("DashboardVCsent"), object: nil, userInfo: passIndex)
        sideview.isHidden = true
         fullview.isHidden = true
        compareStr = "0"
    }
    
    private func searchTextChangeNotification(searchText: String, status: Bool) {
        userInfo.removeAll()
        userInfo["searchText"] = searchText
        userInfo["searchStatus"] = searchActive
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DashboardSearchTextChange"), object: nil, userInfo: userInfo)
    }
    
  
}

extension TBNotificationViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if indexToShowData == 2 {
            inboxSearchBar.text = ""
        }
       else if indexToShowData == 3 {
            inboxSearchBar.text = ""
        }
        inboxSearchBar.isHidden = true
        self.navigationItem.titleView = nil
        //self.navigationItem.rightBarButtonItem  = nil
       //self.navigationItem.leftBarButtonItem   = nil
        self.title = "Inbox".localized
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        self.searchTextChangeNotification(searchText: "", status: false)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        self.searchTextChangeNotification(searchText: "", status: false)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchTextChangeNotification(searchText: searchText, status: true)
    }
}

class NotificationCollectionCell : UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!{
        didSet{
            label.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    
    func wrapData(_ img: UIImage, _ txt: String, _ highImg: UIImage) {
        self.imageView.image = img
        self.imageView.highlightedImage = highImg
        self.label.text = txt.localized
    }
}


extension TBNotificationViewController: InBoxightBarShow {
    func showInBoxRightBar(sttaus: Bool, getIndex: Int) {
        
        if sttaus {
                  indexToShowData = getIndex
                  self.inboxRightBar()
                  compareStr = "0"
              } else {
                  self.navigationItem.rightBarButtonItems = nil
                  self.fullview.isHidden = true
                  self.sideview.isHidden = true
                  inboxSearchBar.isHidden = true
                  self.navigationItem.titleView = nil
                  //self.navigationItem.rightBarButtonItem  = nil
                //self.navigationItem.leftBarButtonItem   = nil
                  self.title = "Inbox".localized
              }
        
    }
    
    
}
