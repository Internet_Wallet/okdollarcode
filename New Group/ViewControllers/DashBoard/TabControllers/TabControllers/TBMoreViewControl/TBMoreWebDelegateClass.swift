//
//  TBMoreWebDelegateClass.swift
//  OK
//
//  Created by Ashish on 3/17/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit


/*
 
 extension TBMoreViewController: WebServiceResponseDelegate {
 
 func webResponse(withJson json: AnyObject, screen: String){
 
 if screen == "addPromotion"{
 do{
 if let data = json as? Data{
 let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
 if let dic = dict as? Dictionary<String,AnyObject>{
 if dic["Msg"] as! String == "success"{
 DispatchQueue.main.async {
 guard let msg = dic["Data"] as? String else {
 return
 }
 if let promoDetail =  OKBaseController.convertToDictionary(text: msg ) {
 guard let arrDict = promoDetail["Promotion Details"] as? Array<Dictionary<String,Any>> else {
 return
 }
 for element in arrDict {
 self.dict["PromotionId"] = element["PromotionId"] as? String
 break
 }
 let story = UIStoryboard.init(name: "Setting", bundle: nil)
 let vc = story.instantiateViewController(withIdentifier: "SMAddPromotionViewController") as? SMAddPromotionViewController
 UserDefaults.standard.set(self.dict, forKey: "DictObject")
 UserDefaults.standard.synchronize()
 println_debug(self.dict)
 println_debug(vc?.accessAddressFromGEOLocation)
 vc?.accessAddressFromGEOLocation =  UserDefaults.standard.object(forKey: "DictObject") as! [String : String]
 self.navigationController?.pushViewController(vc!, animated: true)
 }
 self.removeProgressView()
 }
 }
 }
 
 }
 } catch{
 }
 }
 if screen == "getShopId"{
 do{
 if let data = json as? Data{
 let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
 if let dic = dict as? Dictionary<String,AnyObject>{
 if dic["Msg"] as! String == "success"{
 DispatchQueue.main.async{
 let msg  = dic["Data"] as! String
 self.dict["shopID"] = msg
 self.customAlert(msg: msg, title: "Response")
 UserDefaults.standard.set(msg, forKey: "shopID")
 UserDefaults.standard.synchronize()
 self.BroadCastGPSApiCall()
 self.removeProgressView()
 }
 }
 }
 }
 } catch{
 }
 }
 if screen == "getPromotionId"{
 }
 if screen == "BroadcastGPS"{
 self.addPromotionApiParsing(data: json, screen: screen)
 }
 
 
 
 if screen == "PromotionView"{
 self.addPromotionApiParsing(data: json, screen: screen)
 }
 if screen == "DeleteShop"{
 do{
 if let data = json as? Data{
 
 let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
 if let dic = dict as? Dictionary<String,AnyObject>{
 println_debug(dic)
 
 if dic["Msg"] as! String == "success"{
 
 DispatchQueue.main.async{
 UserDefaults.standard.set(nil, forKey: "shopID")
 UserDefaults.standard.synchronize()
 self.customAlert(msg: dic["Msg"] as! String, title: "Response")
 self.removeProgressView()
 
 }
 }
 }
 }
 } catch{
 }
 }
 }
 
 func addPromotionApiParsing(data: AnyObject,screen: String) {
 
 let responseString = String(data: data as! Data, encoding: .utf8)
 let xml            = SWXMLHash.parse(responseString!)
 println_debug(xml)
 self.enumerate(indexer: xml)
 
 
 
 if screen == "BroadcastGPS"{
 
 let Dvalue = self.responseDict["string"]
 let tempData = OKBaseController.convertToDictionary(text: Dvalue as! String)
 let dict = tempData!["status"] as AnyObject
 if Int(dict["code"] as! String) == 200{
 DispatchQueue.main.async {
 if dict["msg"] as! String == "success" || dict["msg"] as! String == "Records Not Found" || dict["msg"] as! String == "No Data found" {
 let msg = dict["msg"] as? String
 self.customAlert(msg: msg!, title: "Response")
 self.addPromotionApiCall(title: "Mohit Promo", description: "", contactName: UserModel.shared.name, mobileNo: UserModel.shared.mobileNo, Email: UserModel.shared.email, website: UserModel.shared.website, fbpage: UserModel.shared.fbEmailId)
 self.removeProgressView()
 }
 }
 }
 
 }
 else if screen == "ShopOnOff"{
 let Dvalue = self.responseDict["string"]
 let tempData = OKBaseController.convertToDictionary(text: Dvalue as! String)
 let dict = tempData!["status"] as AnyObject
 if Int(dict["code"] as! String) == 200{
 DispatchQueue.main.async {
 if dict["msg"] as! String == "success" || dict["msg"] as! String == "Records Not Found" || dict["msg"] as! String == "No Data found" {
 let msg = dict["msg"] as? String
 self.customAlert(msg: msg!, title: "Response")
 self.removeProgressView()
 }
 }
 }
 }
 }
 }
 */


extension TBMoreViewController : WebServiceResponseDelegate {
    
    
    func webResponse(withJson json: AnyObject, screen: String){
        
        println_debug("Screen :- \(screen)")
        
        if screen == "addPromotion"{
            
            do{
                
                if let data = json as? Data{
                    
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject>{
                        println_debug(dic)
                        
                        if dic["Code"] as! Int == 200{
                            
                            DispatchQueue.main.async{
                                
                                //                                let someOtherDateTime = Date()
                                //                                let dateAndTime = someOtherDateTime.toString(dateFormat: "MMM dd,yyyy HH:mm:ss")
                                //                                self.lblastUpdated.text = "Last Updated: ".appending(dateAndTime)
                                
                                //self.customAlert(msg: dic["Msg"] as? String ?? "", title: "")
                                
                                /*
                                 
                                 guard let msg = dic["Data"] as? String else {
                                 return
                                 }
                                 
                                 
                                 if let promoDetail =  OKBaseController.convertToDictionary(text: msg ) {
                                 println_debug(promoDetail)
                                 println_debug("Promotion det \(String(describing: promoDetail["PromotionId"]))")
                                 
                                 guard let arrDict = promoDetail["Promotion Details"] as? Array<Dictionary<String,Any>> else {
                                 return
                                 }
                                 
                                 for element in arrDict {
                                 
                                 self.customAlert(msg: dic["Msg"] as! String ?? "", title: "Response")
                                 break
                                 }
                                 }
                                 
                                 */
                                
                                
                                self.removeProgressView()
                                
                            }
                        }
                    }
                    
                }
            } catch{
                
            }
        }
        
        if screen == "getShopId"{
            
            do{
                
                if let data = json as? Data{
                    
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    
                    if let dic = dict as? Dictionary<String,AnyObject>{
                        
                        println_debug(dic)
                        
                        if dic["Msg"] as! String == "success"{
                            
                            DispatchQueue.main.async{
                                
                                let msg  = dic["Data"] as! String
                                
                                // CRNotifications.showNotification(type: .error, title: "Error!", message: msg, dismissDelay: 3)
                                
                                //self.customAlert(msg: msg, title: "")
                                self.accessAddressFromGEOLocation["shopID"] = msg
                                let businesstxtName = UserDefaults.standard.value(forKey: "txtValue") as? String
                                UserDefaults.standard.set(businesstxtName, forKey: "shopID")
                                userDef.set(true, forKey: "ShopOnOff")
                                userDef.synchronize()
                                UserDefaults.standard.synchronize()
                                self.BroadCastGPSApiCall()
                                self.removeProgressView()
                                
                            }
                        }
                            
                        else if dic["Code"] as? Int == 302{
                            DispatchQueue.main.async{
                                
                                alertViewObj.wrapAlert(title: "", body: (dic["Msg"] as? String)!, img:  #imageLiteral(resourceName: "setting@3x.png"))
                                alertViewObj.addAction(title: "Done".localized, style: .target, action:{
                                    self.navigationController?.popToRootViewController(animated: true)
                                    NotificationCenter.default.post(name: .goDashboardScreen, object: nil)
                                    
                                })
                                alertViewObj.showAlert(controller: self)
                            }
                        }
                            
                        else{
                            
                            DispatchQueue.main.async{/*self.customAlert(msg: dic["Data"] as! String , title: "");*/ self.removeProgressView()
                                
                            }
                        }
                    }
                }
            } catch{}
        }
        
        if screen == "mGetNewUpStatus" {
            
            do{
                
                if let data = json as? Data{
                    
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject>{
                        println_debug(dic)
                        if dic["Code"] as! Int == 200{
                            DispatchQueue.main.async{
                                guard let msg = dic["Data"] as? String else {
                                    return
                                }
                                if let promoDetail =  OKBaseController.convertToDictionary(text: msg ) {
                                    guard let strStatus = promoDetail["NewUpStatus"] as? String else {
                                        return
                                    }
                                    self.strNewUpStatus = strStatus
                                }
                                self.removeProgressView()
                            }
                        } else {
                            self.removeProgressView()
                        }
                    }
                }
            } catch{
            }
        }
        
        if screen == "ShopOnOff"{
            
            self.ParsingXML(data: json, screen: screen)
            
            /*
            do{
                
                if let data = json as? Data{
                    
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    
                    if let dic = dict as? Dictionary<String,AnyObject>{
                        println_debug(dic)
                        
                    }
                }
                
                self.removeProgressView()
                
                
            }catch{}
            
             let Dvalue = self.responseDict["string"]
             let tempData = OKBaseController.convertToDictionary(text: Dvalue as! String)
             let dict = tempData!["status"] as AnyObject
             if Int(dict["code"] as! String) == 200{
             DispatchQueue.main.async {
             if dict["msg"] as! String == "success" || dict["msg"] as! String == "Records Not Found" || dict["msg"] as! String == "No Data found" {
             let msg = dict["msg"] as? String
             self.customAlert(msg: msg!, title: "")
             self.removeProgressView()
             }
             }
             }
             */
        }
        
        
        if screen == "DeleteShop"{
            do{
                if let data = json as? Data{
                    
                    let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    if let dic = dict as? Dictionary<String,AnyObject>{
                        println_debug(dic)
                        
                        if dic["Msg"] as! String == "success"{
                            
                            DispatchQueue.main.async{
                                
                                self.strSubCatName = ""
                                
                                userDef.removeObject(forKey: "ShopOnOff")
                                UserDefaults.standard.removeObject(forKey: "BussinessDoneCurrentLocation")
                                UserDefaults.standard.removeObject(forKey: "txtValue")
                                UserDefaults.standard.removeObject(forKey: "shopID")
                                UserDefaults.standard.removeObject(forKey: "PromotionId")
                                UserDefaults.standard.removeObject(forKey: "lat")
                                UserDefaults.standard.removeObject(forKey: "long")
                                UserDefaults.standard.removeObject(forKey: "curAddress")
                                UserDefaults.standard.removeObject(forKey: "alerText")
                                UserDefaults.standard.synchronize()
                                userDef.synchronize()
                                // Update UI List
                                self.initDataArray()
                                self.appendSecondSectionCell()
                                self.tableView.reloadData()
                                //
                                
                                //self.customAlert(msg: dic["Msg"] as! String, title: "")
                                self.removeProgressView()
                                
                            }
                        }
                    }
                }
            } catch{
            }
        }
    }
    
    
    
    
    func ParsingXML(data: AnyObject,screen: String) {
        
        let responseString = String(data: data as! Data, encoding: .utf8)
        let xml            = SWXMLHash.parse(responseString!)
        println_debug(xml)
        self.enumerate(indexer: xml)
        
        
        
        if screen == "BroadcastGPS"{
            
            let Dvalue = self.responseDict["string"]
            let tempData = OKBaseController.convertToDictionary(text: Dvalue as! String)
            let dict = tempData!["status"] as AnyObject
            if Int(dict["code"] as! String) == 200{
                DispatchQueue.main.async {
                    if dict["msg"] as! String == "success" || dict["msg"] as! String == "Records Not Found" || dict["msg"] as! String == "No Data found" {
                        //let msg = dict["msg"] as? String
                        //self.customAlert(msg: msg!, title: "Response")
                        self.addPromotionApiCall(title: "No promotion set", description: "", contactName: UserModel.shared.name, mobileNo: UserModel.shared.mobileNo, Email: UserModel.shared.email, website: UserModel.shared.website, fbpage: UserModel.shared.fbEmailId)
                        self.removeProgressView()
                    }
                }
            }
            
        }
        else if screen == "ShopOnOff"{
            let Dvalue = self.responseDict["string"]
            let tempData = OKBaseController.convertToDictionary(text: Dvalue as! String)
            let dict = tempData!["status"] as AnyObject
            if Int(dict["code"] as! String) == 200{
                DispatchQueue.main.async {
                    if dict["msg"] as! String == "success" || dict["msg"] as! String == "Records Not Found" || dict["msg"] as! String == "No Data found" {
                        
                        if userDef.bool(forKey: "ShopOnOff"){
                            userDef.set(false, forKey: "ShopOnOff")
                        }
                        else{
                            userDef.set(true, forKey: "ShopOnOff")
                        }
                        userDef.synchronize()
                        self.initDataArray()
                        
                            self.reloadContent()
                         
                        //let msg = dict["msg"] as? String
                        //self.customAlert(msg: msg!, title: "")
                        self.removeProgressView()
                    }
                }
            }
        }
    }
}
