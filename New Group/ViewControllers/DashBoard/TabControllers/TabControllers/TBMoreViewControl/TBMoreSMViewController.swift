//  TBMoreSMViewController.swift
//  OK
//  Created by Ashish on 3/17/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreLocation

extension TBMoreViewController: SMViewOneDelegate,SMViewTwoDelegate,SMViewThreeDelegate {
    
    func enumerate(indexer: XMLIndexer){
        
        for child in indexer.children{
            self.responseDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
        
    }
    
    
    func doneClick(sender: UIButton?) {
        //        self.updateCurrentLocationToUI(alerText: "Demo")
    }
    
    func doneClick(sender: UIButton?, txtValue: String){
        self.dismiss(animated: true, completion: nil)
        UserDefaults.standard.setValue(txtValue, forKey: "txtValue")
        self.updateCurrentLocationToUI(alerText: txtValue)
        
    }
    
    
    
    
    func customAlert(msg:String, title:String) {
        
        alertViewObj.wrapAlert(title: title, body: msg, img: #imageLiteral(resourceName: "setting"))
        alertViewObj.addAction(title: "OK", style: .target){}
        alertViewObj.showAlert(controller: self)
        
    }
    
    
    func getCategoaryName(){
        
        //     UserModel.shared.businessSubCate = "5"
        
        let categoriesList = CategoriesManager.categoriesList
        println_debug(categoriesList)
        
        var i = 0
        
        while i < (categoriesList.count){
            
            let category = categoriesList[i]
            
            if (category.categoryCode.localizedCaseInsensitiveContains(UserModel.shared.businessCate)){
                
                strCatName = category.categoryName
                strCatImg = category.categoryEmoji
                
                //fetch subcat
                let subCatList = category.subCategoryList
                var subCat = 0
                
                while subCat < (subCatList.count){
                    
                    let subCategory = subCatList[subCat]
                    
                    if (subCategory.subCategoryCode.localizedCaseInsensitiveContains(UserModel.shared.businessSubCate)){
                        
                        strSubCatName = subCategory.subCategoryName
                        strSubCatImg = subCategory.subCategoryEmoji
                        break
                        
                    }
                    subCat = subCat + 1
                }
                
                break
            }
            i = i + 1
        }
        
        println_debug(strCatName)
        println_debug(strSubCatName)
        
    }
    
    func updateCurrentLocationToUI(alerText: String){
        
        geoLocManager.startUpdateLocation()
        guard appDelegate.checkNetworkAvail() else{
            self.showErrorAlert(errMessage: "Network Not Available")
            return
        }
        
        if let lat = geoLocManager.currentLatitude, let long = geoLocManager.currentLongitude {
            
            self.showProgressView()
            
            geoLocManager.getLocationNameByLatLong(lattitude: lat, longitude: long) {(isSuccess, currentAddress) in
                if isSuccess{
                    
                    DispatchQueue.main.async{
                        
                        if let curAddress = currentAddress as? String{
                            self.AfterBusinessNavigate(lat: lat, long: long, curadd: curAddress, alerText: alerText)
                        }else{
                            println_debug("Loading location.....")
                        }
                        self.removeProgressView()
                        geoLocManager.stopUpdateLocation()
                    }
                } else{
                    DispatchQueue.main.async {
                        println_debug("something went wrong when get current address from google api")
                        //                    if let curAddress = currentAddress as? String{
                        self.AfterBusinessNavigate(lat: UserModel.shared.lat, long: UserModel.shared.long, curadd: "\(UserModel.shared.address1),\(UserModel.shared.address2),\(UserModel.shared.address1)", alerText: alerText)
                        //                }
                        self.removeProgressView()
                        geoLocManager.stopUpdateLocation()
                    }
                    return
                }
            }
        }
    }
    
    
    func AfterBusinessNavigate(lat: String,long: String,curadd:String,alerText:String){

        //                        self.dict["lat"] = lat
        //                        self.dict["long"] = long
        //                        self.dict["curAddress"] = curAddress
        //                        self.dict["alerText"] = alerText
        
        UserDefaults.standard.set(lat, forKey: "lat")
        UserDefaults.standard.set(long, forKey: "long")
        // for distance purpose
       
        let userLocation = ["lat": GeoLocationManager.shared.currentLatitude, "long": GeoLocationManager.shared.currentLongitude]
        UserDefaults.standard.set(userLocation, forKey: "BussinessDoneCurrentLocation")
        UserDefaults.standard.set(curadd, forKey: "curAddress")
        UserDefaults.standard.set(alerText, forKey: "alerText")
        UserDefaults.standard.synchronize()
        self.tableView.reloadData()
        let story = UIStoryboard.init(name: "Setting", bundle: nil)
        let VC = story.instantiateViewController(withIdentifier: "SMAddPromotionViewController") as! SMAddPromotionViewController
        VC.accessAddressFromGEOLocation =  self.dict
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    
    
    
    
    func createBusiness() {
        
        //      let ScreenView = Bundle.main.loadNibNamed("SMViewOne", owner: self, options: nil)?[2] as! SMAddFiveView
        
        
        
        
        let viewController = UIStoryboard(name: "Setting", bundle: Bundle.main).instantiateViewController(withIdentifier: "customLayerVC") as? customLayerVC
        //        let labelTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.removeSubView))
        //        labelTapGesture.numberOfTapsRequired = 1
        //        viewController?.view.addGestureRecognizer(labelTapGesture)
        viewController?.modalPresentationStyle = .overCurrentContext
        
        println_debug(UserDefaults.standard.object(forKey: "shopID"))
        
        if (UserDefaults.standard.object(forKey: "shopID") as? String) != nil{
            self.strSubCatName = "Delete"
        }
        else{
            self.getCategoaryName()
            //    self.strSubCatName = "Ticketing"
        }
        if (self.strSubCatName == "Bus") || (self.strSubCatName == "Ticketing") {
            let ScreenView = Bundle.main.loadNibNamed("SMAddFiveView", owner: self, options: nil)?[3] as! SMViewThree
            ScreenView.delegate = self
            ScreenView.txt_busniessname.text = UserModel.shared.businessName
            ScreenView.parentVCThree = self
            ScreenView.frame = CGRect(x: 20, y:((screenHeight/2)-130) , width: screenWidth-40, height: 450)
            viewController?.view.addSubview(ScreenView)
            self.navigationController?.present(viewController!, animated: false , completion: nil)
        }else if (self.strSubCatName == "Taxi") {
            let ScreenView = Bundle.main.loadNibNamed("SMAddFiveView", owner: self, options: nil)?[2] as! SMViewOne
            ScreenView.delegate = self
            ScreenView.txt_busniessname.text = UserModel.shared.businessName
            ScreenView.parentVCOne = self
            ScreenView.frame = CGRect(x: 20, y:((screenHeight/2)-130) , width: screenWidth-40, height: 535)
            viewController?.view.addSubview(ScreenView)
            self.navigationController?.present(viewController!, animated: false , completion: nil)
        }else if (self.strSubCatName == "Delete") {
            let ScreenView = Bundle.main.loadNibNamed("SMAddFiveView", owner: self, options: nil)?[6] as! SMDeleteView
            ScreenView.delegate = self
            if (UserDefaults.standard.object(forKey: "shopID") as? String) != nil{
                //                ScreenView.switchStatus.setOn(true, animated: false)
                if userDef.bool(forKey: "ShopOnOff"){
                    ScreenView.switchStatus.setOn(true, animated: false)
                }
                else{
                    ScreenView.switchStatus.setOn(false, animated: false)
                }
            }
            else{
                ScreenView.switchStatus.setOn(false, animated: false)
            }
            //          ScreenView.shopNamelbl.text = self.DBDict["busniessname"] as? String
            ScreenView.shopNamelbl.text = UserDefaults.standard.value(forKey: "txtValue") as? String
            ScreenView.parentdeleteVC = self
            ScreenView.frame = CGRect(x: 20, y:((screenHeight/2)-130) , width: screenWidth-40, height: 230)
            
            viewController?.view.addSubview(ScreenView)
            self.navigationController?.present(viewController!, animated: false , completion: nil)
            // self.view.addSubview(viewController!.view)
            
        }
            
        else{
            
            let ScreenView = Bundle.main.loadNibNamed("SMAddFiveView", owner: self, options: nil)?[4] as! SMViewTwo
            ScreenView.delegate = self
            ScreenView.txt_busniessname.text = UserModel.shared.businessName
            ScreenView.parentVCTwo = self
            ScreenView.frame = CGRect(x: 20, y:((screenHeight/2)-130) , width: screenWidth-40, height: 260)
            
            viewController?.view.addSubview(ScreenView)
            // self.view.addSubview(viewController!.view)
            self.navigationController?.present(viewController!, animated: false , completion: nil)
        }
        
        // self.updateCurrentLocationToUI(alerText: alertViewObj.AlerTextReturnString())
    }
    
    
    
    //    @objc func removeSubView(VC: customLayerVC){
    ////        dismiss(animated: true, completion: nil)
    //
    //        VC.view.removeFromSuperview()
    //    }
    
    
    func getShopIdApiCall(){
        
        
        self.showProgressView()
        
        if appDelegate.checkNetworkAvail(){
            
            let web = WebApiClass()
            web.delegate = self
            
            
            //  https://www.okdollar.co/RestService.svc/AddShops?MobileNumber=00959400930281&Simid=DE429B3B-E5F2-44D9-BB07-A84B9333C28B&Lat=16.816783&Long=96.131835&CellID=&ShopName=tech&MSID=&OSType=1&OTP=DE429B3B-E5F2-44D9-BB07-A84B9333C28B&Country=Myanmar (Burma)&State=Yangon Region&TownShip=&City=Yangon
            
            
            //  "RestService.svc/AddShops?MobileNumber=%@&Simid=%@&Lat=%@&Long=%@&CellID=%@&ShopName=%@&MSID=%@&OSType=%@&OTP=%@&Country=%@&State=%@&TownShip=%@&City=%@"
            
            
            let sayHello =  UserDefaults.standard.object(forKey: "curAddress") as? String                 //self.dict["curAddress"]
            let result = sayHello?.split(separator: ",")
            println_debug(result!)
            
            //            println_debug(self.DBDict["busniessname"]! as! CVarArg)
            //            println_debug(self.dict["busniessname"]! as CVarArg)
            
            //            let urlS = String.init(format: Url.getShopId,UserModel.shared.mobileNo,UserModel.shared.simID,self.dict["lat"]!,self.dict["long"]!,"",self.DBDict["busniessname"]! as! CVarArg,"","1",uuid,result![2] as CVarArg,result![1] as CVarArg,"",result![1] as CVarArg)
            
            
            let urlS = String.init(format: Url.getShopId,UserModel.shared.mobileNo,UserModel.shared.simID,UserDefaults.standard.object(forKey: "lat") as! CVarArg,UserDefaults.standard.object(forKey: "long") as! CVarArg,"",self.dict["busniessname"]! as CVarArg,"","1",uuid,result![2] as CVarArg,result![1] as CVarArg,"",result![1] as CVarArg)
            
            let allowedCharacterSet = (CharacterSet(charactersIn: " ").inverted)
            
            if let escapedString = urlS.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet){
                
                let url =   getUrl(urlStr: escapedString, serverType: .serverApp)
                let param = [String:String]() as AnyObject
                web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "getShopId")
                
            }
            
        }
        else{
            
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection", img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
            
        }
        
    }
    
    
    
    
    func BroadCastGPSApiCall() {
        
        
        self.showProgressView()
        
        if appDelegate.checkNetworkAvail(){
            
            let web = WebApiClass()
            web.delegate = self
            
            
            //  https://www.okdollar.co/RestService.svc/BroadcastGPS?MobileNumber=00959404228737&simid=4DDC4F6F-3992-4FBA-8578-28E36FC7FEA5&Lat=16.816782&Long=96.131830&CellID=&Active=1&ShopId=45e8e9d3-b0a0-444e-8a47-9f274578bdf8&State=Yangon Region&TownShip=&City=Yangon
            
            
            let sayHello = UserDefaults.standard.object(forKey: "curAddress") as? String //self.dict["curAddress"]
            let result = sayHello?.split(separator: ",")
            println_debug(result!)
            
            
            
            let urlS = String.init(format: Url.BroadcastGPS,UserModel.shared.mobileNo,UserModel.shared.simID,UserDefaults.standard.object(forKey: "lat") as! CVarArg,UserDefaults.standard.object(forKey: "long") as! CVarArg,"","1",UserDefaults.standard.object(forKey: "shopID") as! CVarArg,result![2] as CVarArg,result![1] as CVarArg,result![1] as CVarArg)
            
            
            let allowedCharacterSet = (CharacterSet(charactersIn: " ").inverted)
            
            if let escapedString = urlS.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) {
                
                let url =   getUrl(urlStr: escapedString, serverType: .serverApp)
                let param = [String:String]() as AnyObject
                web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "BroadcastGPS")
                
            }
        }
        else{
            
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection", img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
            
        }
    }
    
    
    
    
    func addPromotionApiCall(title:String,description:String,contactName:String,mobileNo:String,Email:String,website:String,fbpage:String){
        
        self.showProgressView()
        
        if appDelegate.checkNetworkAvail(){
            let web = WebApiClass()
            web.delegate = self
            let urlS = String.init(format: Url.addPromotion, UserModel.shared.mobileNo,UserModel.shared.simID,"","1",uuid,title,description,website,mobileNo,fbpage,UserDefaults.standard.object(forKey: "shopID") as! CVarArg,"false",contactName,Email)
            let allowedCharacterSet = (CharacterSet(charactersIn: " ").inverted)
            if let escapedString = urlS.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) {
                let url =   getUrl(urlStr: escapedString, serverType: .serverApp)
                let param = [String:String]() as AnyObject
                web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "addPromotion")
            }
        }
        else{
            
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection", img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
            
        }
    }
    
    func DeleteApiCall() {
        self.showProgressView()
        if appDelegate.checkNetworkAvail(){
            let web = WebApiClass()
            web.delegate = self
            println_debug(Url.deleteShop)
            // println_debug(self.dict["shopID"] as! String)
            println_debug(self.dict["shopID"])
            let shopId = UserDefaults.standard.value(forKey: "shopID") as? String
            let urlS = String.init(format: Url.deleteShop,shopId!)
            let allowedCharacterSet = (CharacterSet(charactersIn: " ").inverted)
            if let escapedString = urlS.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) {
                let url =   getUrl(urlStr: escapedString, serverType: .serverApp)
                let param = [String:String]() as AnyObject
                web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "DeleteShop")
            }
        }
        else{
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection", img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
        }
    }
    
    func changeLocationState(state: Bool) {
        
        self.showProgressView()
        if appDelegate.checkNetworkAvail(){
            let web = WebApiClass()
            web.delegate = self
            let sayHello = UserDefaults.standard.object(forKey: "curAddress") as? String //self.dict["curAddress"]
            let result = sayHello?.split(separator: ",")
            var urlS = ""
            if result != nil{
                
                urlS = String.init(format: Url.BroadcastGPS,UserModel.shared.mobileNo,UserModel.shared.simID,UserDefaults.standard.object(forKey: "lat") as! CVarArg,UserDefaults.standard.object(forKey: "long") as! CVarArg,"",state as NSNumber,UserDefaults.standard.object(forKey: "shopID") as! CVarArg,result![2] as CVarArg,result![1] as CVarArg,result![1] as CVarArg)
            }else{
                urlS = String.init(format: Url.BroadcastGPS,UserModel.shared.mobileNo,UserModel.shared.simID,UserDefaults.standard.object(forKey: "lat") as! CVarArg,UserDefaults.standard.object(forKey: "long") as! CVarArg,"",state as NSNumber,UserDefaults.standard.object(forKey: "shopID") as! CVarArg,"","","")
            }
            let allowedCharacterSet = (CharacterSet(charactersIn: " ").inverted)
            if let escapedString = urlS.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) {
                let url =   getUrl(urlStr: escapedString, serverType: .serverApp)
                let param = [String:String]() as AnyObject
                web.genericClass(url: url, param: param, httpMethod: "GET", mScreen: "ShopOnOff")
            }
        }
        else{
            
            alertViewObj.wrapAlert(title: "", body: "No Internet Connection", img: #imageLiteral(resourceName: "alert-icon"))
            alertViewObj.addAction(title: "OK".localized, style: .target){}
            alertViewObj.showAlert(controller: self)
            
        }
    }
}

extension TBMoreViewController: SMAlertViewDelegate {
    
    func NoClick(sender: UIButton?) {
        println_debug("NoClick")
    }
    
    func YesClick(sender: UIButton?) {
        println_debug("YesClick")
        if (UserDefaults.standard.object(forKey: "shopID") as? String) != nil {
            self.createBusiness()
        }
    }
}

extension TBMoreViewController: SMDeleteViewDelegate {
    
    func SwitchClickDelegate(sender: UISwitch?) {
        if geoLocManager.ReturnDistanceDetails() {
            self.changeLocationState(state: (sender?.isOn)!)
        }else {
            sender?.setOn(false, animated: true)
            println_debug(geoLocManager.ReturnDistanceDetails())
            self.customAlert(msg: "Can't change status because location not same", title: "")
        }
    }
    
    func CloseClickDelegate(sender: UIButton?) {
        println_debug("CloseClickDelegate")
        self.reloadContent()
        dismiss(animated: false, completion: nil)
    }
    
    func DeleteClickDelegate(sender: UIButton?) {
        dismiss(animated: false, completion: nil)
        println_debug("DeleteClickDelegate")
        alertViewObj.wrapAlert(title: "", body: "Do you want to remove this business location ?".localized, img: #imageLiteral(resourceName: "share_location_sideMenu"))
        alertViewObj.addAction(title: "NO".localized, style: .target){}
        alertViewObj.addAction(title: "YES".localized, style: .target){
            
            if (UserDefaults.standard.object(forKey: "shopID") as? String) != nil{
                self.DeleteApiCall()
            }
            self.tableView.reloadData()
        }
        alertViewObj.showAlert(controller: self)
    }
}
