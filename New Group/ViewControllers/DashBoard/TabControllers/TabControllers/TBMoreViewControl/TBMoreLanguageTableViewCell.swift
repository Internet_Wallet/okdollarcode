//
//  TBMoreLanguageTableViewCell.swift
//  OK
//
//  Created by Ashish on 3/17/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol LanguageSelectionProtocol : class {
    func openLanguageSelection()
    func selectApplicationLanguage(stringTag: String?)
}

class TBMoreTableCell : UITableViewCell {
    
    @IBOutlet var cellImageView: UIImageView!
    @IBOutlet var cellLabel: UILabel!{
        didSet{
          //  cellLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet var cellCountLabel: UILabel!{
        didSet{
           // cellCountLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    var titleTag = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cellCountLabel.layer.cornerRadius = 15
        self.layoutIfNeeded()
        //self.cellLabel.font = UIFont(name: appFont, size: 17)
    }
    
    func wrapOtherSections(img: Any?, name: String) {
        self.cellLabel.font = UIFont(name: appFont, size: 17)
        if  name.contains("Auto payment is Active With") {
            if appDelegate.getSelectedLanguage() == "my" {
                self.cellLabel.text = "\((UserDefaults.standard.object(forKey: "txtValue") as? String) ?? "") \(name.localized)"
            } else if appDelegate.getSelectedLanguage() == "uni"{
                self.cellLabel.text = "\((UserDefaults.standard.object(forKey: "txtValue") as? String) ?? "") \(name.localized)"
            }
            else {
                self.cellLabel.text = "\(name.localized) \((UserDefaults.standard.object(forKey: "txtValue") as? String) ?? "")"
            }
        } else {
            self.cellLabel.text = appDel.getlocaLizationLanguage(key: name)
        }
        
        if let image = img as? UIImage {
            self.cellImageView.image = image
        } else if let imageStr = img as? String {
            self.cellImageView.image = UIImage(named: imageStr)
        }
        
        if name == "Received Money" {
            let countValue = UserDefaults.standard.integer(forKey: "ReceivedCount")
            if countValue > 0 {
                cellCountLabel.text = "\(countValue)"
                cellCountLabel.isHidden = false
            } else {
                cellCountLabel.text = ""
                cellCountLabel.isHidden = true
            }
        } else {
            cellCountLabel.isHidden = true
        }
    }
}


class TBMoreLanguageSelectionCell : UITableViewCell {
    
    weak var delegate : LanguageSelectionProtocol?
    
    @IBOutlet weak var firstFlag: UIImageView!
    @IBOutlet weak var secondFlag: UIImageView!
    @IBOutlet weak var thirdFlag: UIImageView!
    @IBOutlet weak var fourthFlag: UIImageView!
    
    @IBOutlet weak var arrowImage: UIImageView!
    
    @IBOutlet weak var selectedLanguageLbl: UILabel!
    @IBOutlet weak var firstLanguageLabel: UILabel!
    @IBOutlet weak var secondLanguageLabel: UILabel!
    @IBOutlet weak var thirdLanguageLabel: UILabel!
    @IBOutlet weak var fourthLanguageLabel: UILabel!
    @IBOutlet weak var chooseLanguageLbl: UILabel!{
        didSet{
            chooseLanguageLbl.font = UIFont (name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var chooseLanguageActionOutlet: UIButton!
    
    @IBAction func chooseLanguageAction(_ sender: UIButton) {
        if let delegate = self.delegate {
            
            if sender.isSelected == true
            {
                self.arrowImage.image = UIImage(named: "downarrow_blue")
                sender.isSelected = false
            }
            else
            {
                self.arrowImage.image = UIImage(named: "uparrow_blue")
                sender.isSelected = true
            }
            delegate.openLanguageSelection()
            
        }
    }
    
    @IBAction func firstLanguageAction(_ sender: UIButton) {
        let prevLanguage = selectedLanguageLbl.text
        selectedLanguageLbl.text = firstLanguageLabel.text
        firstLanguageLabel.text = prevLanguage
        self.arrowImage.image = UIImage(named: "downarrow_blue")
        chooseLanguageActionOutlet.isSelected = false
        
        if let delegate = self.delegate {
            delegate.selectApplicationLanguage(stringTag: selectedLanguageLbl.text)
        }
    }
    
    @IBAction func secondLanguageAction(_ sender: UIButton) {
        let prevLanguage = selectedLanguageLbl.text
        selectedLanguageLbl.text = secondLanguageLabel.text
        secondLanguageLabel.text = prevLanguage
        self.arrowImage.image = UIImage(named: "downarrow_blue")
        chooseLanguageActionOutlet.isSelected = false
        
        if let delegate = self.delegate {
            delegate.selectApplicationLanguage(stringTag: selectedLanguageLbl.text)
        }
    }
    
    @IBAction func thirdLanguageAction(_ sender: UIButton) {
        let prevLanguage = selectedLanguageLbl.text
        selectedLanguageLbl.text = thirdLanguageLabel.text
        thirdLanguageLabel.text = prevLanguage
        self.arrowImage.image = UIImage(named: "downarrow_blue")
        chooseLanguageActionOutlet.isSelected = false
        
        if let delegate = self.delegate {
            delegate.selectApplicationLanguage(stringTag: selectedLanguageLbl.text)
        }
    }
    
    @IBAction func fourthLanguageAction(_ sender: UIButton) {
        let prevLanguage = selectedLanguageLbl.text
        selectedLanguageLbl.text = fourthLanguageLabel.text
        fourthLanguageLabel.text = prevLanguage
        self.arrowImage.image = UIImage(named: "downarrow_blue")
        chooseLanguageActionOutlet.isSelected = false
        
        if let delegate = self.delegate {
            delegate.selectApplicationLanguage(stringTag: selectedLanguageLbl.text)
        }
    }
    
}





