//
//  OKWalletSizeViewController.swift
//  OK
//
//  Created by Kethan Kumar on 9/2/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

class OKWalletSizeViewController: OKBaseController, PTWebResponseDelegate {

    var balanceListArray = [Dictionary<String,Any>]()
    var balanceDict = Dictionary<String,Any>()
    
    @IBOutlet weak var maxWalletSizeLbl: UILabel!{
        didSet{
            maxWalletSizeLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var perDayTxnLbl: UILabel!{
        didSet{
            perDayTxnLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var monthlyTxtLbl: UILabel!{
        didSet{
            monthlyTxtLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var remPerDayTxtLbl: UILabel!{
        didSet{
            remPerDayTxtLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var remMonthlyTxnLbl: UILabel!{
        didSet{
            remMonthlyTxnLbl.font = UIFont(name: appFont, size: appFontSize)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dismissView()
        self.maxWalletSizeLbl.text = "Max Wallet Size".localized + "0 \("MMK".localized)"
        self.perDayTxnLbl.text = "Per day limit".localized +  "0 \("MMK".localized)"
        self.monthlyTxtLbl.text = "Monthly limit".localized + "0 \("MMK".localized)"
        self.remPerDayTxtLbl.text = "Per day limit remain".localized + "0 \("MMK".localized)"
        self.remMonthlyTxnLbl.text = "Monthly limit remain".localized + "0 \("MMK".localized)"
        // Do any additional setup after loading the view.
        self.getWalletSize()
    }
    
    private func dismissView() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(dismissSelf))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc private func dismissSelf() {
        self.dismiss(animated: false, completion: nil)
    }
    
    fileprivate func updateAllLabels() {
        DispatchQueue.main.async {
            self.maxWalletSizeLbl.text = "Max Wallet Size".localized +  "\((self.balanceDict["Maximumwalletsize"] as? String ?? "").replacingOccurrences(of: ".00", with: "").alignWithCurrencyFormat()) \("MMK".localized)"
            self.perDayTxnLbl.text = "Per day limit".localized + "\((self.balanceDict["Perdaylimitamount"] as? String ?? "").replacingOccurrences(of: ".00", with: "").alignWithCurrencyFormat()) \("MMK".localized)"
            self.monthlyTxtLbl.text = "Monthly limit".localized + "\((self.balanceDict["Monthlimitamount"] as? String ?? "").replacingOccurrences(of: ".00", with: "").alignWithCurrencyFormat()) \("MMK".localized)"
            self.remPerDayTxtLbl.text = "Per day limit remain".localized + "\((self.balanceDict["Perdayremaingamount"] as? String ?? "").replacingOccurrences(of: ".00", with: "").alignWithCurrencyFormat()) \("MMK".localized)"
            self.remMonthlyTxnLbl.text = "Monthly limit remain".localized + "\((self.balanceDict["Permonthremaingamount"] as? String ?? "").replacingOccurrences(of: ".00", with: "").alignWithCurrencyFormat()) \("MMK".localized)"
        }
    }
    
    //Kethan - changes
    fileprivate func getTheWalletSizeParams() -> Dictionary<String, Any> {
        var params = Dictionary<String, Any>()
        params["AppId"] = UserModel.shared.appID
        params["Limit"] = 0
        params["MobileNumber"] = UserModel.shared.mobileNo
        params["Msid"] = UserModel.shared.msid
        params["Offset"] = 0
        params["Ostype"] = 1
        params["Otp"] = ""
        params["Simid"] = ""
        return params
    }
    
    private func getWalletSize() {
        if appDelegate.checkNetworkAvail() {
            let web      = PayToWebApi()
            web.delegate = self
            
            let url = getUrl(urlStr: Url.okWalletSizeURL, serverType: .serverApp)
//             guard let url   = URL(string: "http://69.160.4.151:8001/RestService.svc/GetWalletsizeapiByMobilenumber") else { return }
            let pRam = self.getTheWalletSizeParams()
            web.genericClass(url: url, param: pRam as AnyObject, httpMethod: "POST", mScreen: "mWalletSize")
        }
    }
    
    //PTWebresponseDelegate
    func webSuccessResult(data: Any, screen: String) {
        if screen == "mWalletSize" {
            self.removeProgressView()
//            if let xmlString = data as? String {
//                let xml = SWXMLHash.parse(xmlString)
//                balanceDict.removeAll()
//                self.enumerateBalance(indexer: xml)
//                self.updateAllLabels()
//            }
            if let dataDict = data as? Dictionary<String, Any> {
                println_debug(dataDict)
                if let dataStr = dataDict["Data"] as? String {
                    if let dataDict = dataStr.parseJSONString as? Dictionary<String, Any> {
                        balanceDict["Monthlimitamount"] = dataDict["Monthlimitamount"] as? String ?? ""
                        balanceDict["Permonthremaingamount"] = dataDict["Permonthremaingamount"] as? String ?? ""
                        balanceDict["Permonthusedamount"] = dataDict["Permonthusedamount"] as? String ?? ""
                        balanceDict["Perdaylimitamount"] = dataDict["Perdaylimitamount"] as? String ?? ""
                        balanceDict["Perdayremaingamount"] = dataDict["Perdayremaingamount"] as? String ?? ""
                        balanceDict["Perdayusedamount"] = dataDict["Perdayusedamount"] as? String ?? ""
                        balanceDict["Maximumwalletsize"] = dataDict["Maximumwalletsize"] as? String ?? ""
                        self.updateAllLabels()
                    }
                }
            }
        }
    }
    
//    func enumerateBalance(indexer: XMLIndexer) {
//        for child in indexer.children {
//            balanceDict[child.element!.name] = child.element!.text
//            enumerateBalance(indexer: child)
//        }
//    }
    
    
    func webFailureResult(screen: String) {
        println_debug("Failure")
    }
}
