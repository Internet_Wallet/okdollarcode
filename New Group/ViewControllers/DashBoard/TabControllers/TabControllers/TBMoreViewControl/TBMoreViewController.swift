//  TBMoreViewController.swift
//  OK
//
//  Created by Ashish on 12/25/17.
//  Copyright © 2017 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import CoreLocation
import Rabbit_Swift


protocol LanguageChangeActionDashboardUpdate : class {
    func resetDashboardLanguage()
}

class TBMoreViewController:  OKBaseController, UITableViewDelegate, UITableViewDataSource,BioMetricLoginDelegate,LanguageSelectionProtocol, PTWebResponseDelegate {
    
    @IBOutlet var tableView: UITableView!
    
    var accessAddressFromGEOLocation = Dictionary <String,String>()
    var DBDict =  Dictionary<String,AnyObject>()
    var secondSection = [UITableViewCell]()
    
    let cellIndentifier = "TBMoreTableCell"
    
    var sideLabelArr = [String]()
    var sideImgsArr  = [Any]()
    var strCatName   = ""
    var strCatImg  = ""
    var strSubCatName = ""
    var strSubCatImg  = ""
    var strNewUpStatus  = ""
    var promotionStatusImageName = ""
    var dict = Dictionary<String,String>()
    var responseDict = Dictionary<String, Any>()
    
    weak var languageDelegate : LanguageChangeActionDashboardUpdate?
    
    @IBOutlet weak var profileImageView: UIImageView!
        {
        didSet
        {
            self.profileImageView.image = UIImage(named: "avatar")
        }
    }
    @IBOutlet weak var name: UILabel!{
        didSet {
            name.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var profileType: UILabel!{
        didSet {
            profileType.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var location: MarqueeLabel!
    {
        didSet {
            location.font = UIFont(name: appFont, size: 12)
        }
    }
    @IBOutlet weak var loginTime: UILabel!{
        didSet {
            loginTime.font = UIFont(name: appFont, size: 12)
        }
    }
    @IBOutlet weak var loginDate: UILabel!{
        didSet {
            loginDate.font = UIFont(name: appFont, size: 12)
        }
    }
    @IBOutlet weak var timeLogin: UILabel!{
        didSet {
            timeLogin.font = UIFont(name: appFont, size: 12)
        }
    }
    @IBOutlet weak var amount: UILabel!{
        didSet {
            amount.font = UIFont(name: appFont, size: 12)
        }
    }
    @IBOutlet weak var walletRefreshButton: UIButton!
    
    let formatter = DateFormatter()
    
    @IBOutlet weak var balanceCheckTimeLabel: UILabel!
    // footer
    @IBOutlet weak var versionNumber: UILabel!
    @IBOutlet weak var buildNumber: UILabel!
    
    var languageDisplay : [String: Any] = ["English" : #imageLiteral(resourceName: "uk"), "Burmese" :#imageLiteral(resourceName: "myanmar"), "Chinese" :#imageLiteral(resourceName: "china")]
    var languageSectionHeight : CGFloat = 60.00
    var lastLoginAddress : String {
        return userDef.value(forKey: "lastLoginKey") as? String ?? ""
    }
    
    var balTimeDict = Dictionary<String,Any>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(self.formatter.string(from: NSDate() as Date))
        geoLocManager.startUpdateLocation()
        self.versionNumber.text = String.init(format: "Version %@, Build (\(Bundle.main.buildVersionNumber))", Bundle.main.releaseVersionNumber)
        // self.buildNumber.text   = String.init(format: "Build (%@)", Bundle.main.buildVersionNumber)
        
        self.buildNumber.text   = String.init(format: "Build (\(Bundle.main.buildVersionNumber))")
        
        self.viewLayouting(onView: self.profileImageView)
        
        if isKeyPresentInUserDefaults(key: "DictObject"){
            DBDict = UserDefaults.standard.object(forKey: "DictObject") as! Dictionary<String,AnyObject>
        }
        let TapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.HandleTapGesture(_:)))
        
        profileImageView.isUserInteractionEnabled = true
        profileImageView.layer.masksToBounds = true
        profileImageView.clipsToBounds = true
        
        self.profileImageView.addGestureRecognizer(TapGesture)
        self.location.text = ""
        //        let myFont = UIFont(name: "Zawgyi-One", size: 20)
        //        self.location.font = myFont
        
        let amountBal = Double(UserLogin.shared.walletBal) ?? 0.00
        if amountBal < 500000.00 {
            self.amount.attributedText = self.walletBalance()
        } else {
            self.amount.attributedText = NSAttributedString.init(string: "XXXXXXXXXX".localized)
        }
        loginTime.text = loginTime.text?.localized
        formatter.dateFormat = "EEE, dd-MMM-yyyy hh:mm:ss a"
        formatter.locale = Locale.current
        formatter.calendar = Calendar(identifier: .gregorian)
        
        self.balanceCheckTimeLabel.text = formatter.string(from: NSDate() as Date)
        NotificationCenter.default.addObserver(self, selector: #selector(signOutAction), name: .loginTimeOut, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateProfileStatus), name: NSNotification.Name(rawValue: "UpdateTBMoreProfile"), object: nil)
        //this notification will work when coming from tax
        NotificationCenter.default.addObserver(self, selector: #selector(self.moveToDashboardFromRecent), name: NSNotification.Name(rawValue: "MoveToDashboardFromRecent"), object: nil)
    }
    
    deinit {
           NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "MoveToDashboardFromRecent"), object: nil)
       }
    
    @objc func moveToDashboardFromRecent() {
        NotificationCenter.default.post(name: .goDashboardScreen, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        GeoLocationManager.shared.startUpdateLocation()
        self.tableView.setContentOffset(CGPoint.zero, animated: true)
        // let currentLan = UserDefaults.standard.value(forKey: "currentLanguage") as? String
        //  self.selectApplicationLanguage(stringTag: currentLan)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.reloadContent()
    }
    
    func reloadContent(){
        DispatchQueue.main.async {
            //self.getUpdateProfileStatus()
            if UserDefaults.standard.string(forKey: "USERLASTLOGIN") == nil{
                UserDefaults.standard.set(self.formattedDateAndTime().time, forKey: "USERLASTLOGIN")
            }
            self.updateMoreProfile()
            self.updateCurrentAddress()
            self.initDataArray()
            self.appendSecondSectionCell()
            self.tableView.reloadData()
        }
    }
    
    @objc func updateProfileStatus() {
        if self.name.text == "XXXXXXXXXXX" {
            if sideLabelArr.contains("Logout") {
                self.updateMoreProfile()
                self.updateCurrentAddress()
            } else {
                sideLabelArr.append("Logout")
                sideImgsArr.append("logout_sidemenu_new")
                self.secondSection.removeAll()
                self.appendSecondSectionCell()
                self.tableView.reloadData()
                self.updateMoreProfile()
                self.updateCurrentAddress()
            }
        }
    }
    
    func initDataArray() {
        
        sideLabelArr.removeAll()
        sideImgsArr.removeAll()
        var promotionalText = ""
        
        if ((UserDefaults.standard.object(forKey: "shopID") as? String) != nil){
            if userDef.bool(forKey: "ShopOnOff") {
                self.promotionStatusImageName = "business_details"
                 promotionalText = "Auto payment is Active With"
            }else {
                self.promotionStatusImageName = "business_details_sidemenu_new"
                  promotionalText = "Share Your Business Details For Auto Payment."
            }
        }
        else{
            self.promotionStatusImageName = "business_details_sidemenu_new"
            promotionalText = "Share Your Business Details For Auto Payment."//"Auto payment"
        }
        
        
        
        
        if UserModel.shared.agentType == .advancemerchant {
            sideLabelArr = [promotionalText,"View Wallet Size","My QR Code","My Profile","Received Money","Favorite List","Recent","Reports", "Setting Menu","Help & Support",]
            sideImgsArr = [self.promotionStatusImageName,"blue_wallet", "qr_code_sidemenu_new","edit_profile_sidemenu_new", "recived_money_sidemenu_new","fav_list_sidemenu_new","recent_sidemenu_new","reports_sidemenu_new",  "settings_sidemenu_new", "help_support_sidemenu_new"] as [Any]
        }else {
            if (UserDefaults.standard.object(forKey: "shopID") as? String) != nil {
                sideLabelArr = [promotionalText, "My QR Code","My Profile","Received Money","Favorite List","Recent","Reports","Add Your Vehicle", "Setting Menu","Help & Support"]
                sideImgsArr = [self.promotionStatusImageName,"blue_wallet","qr_code_sidemenu_new", "edit_profile_sidemenu_new", "recived_money_sidemenu_new", "fav_list_sidemenu_new","recent_sidemenu_new", "reports_sidemenu_new", "add_vehicle_sidemenu_new", "settings_sidemenu_new", "help_support_sidemenu_new"] as [Any]
                
            }else {
                sideLabelArr = [promotionalText, "View Wallet Size", "My QR Code","My Profile","Received Money","Favorite List","Recent","Reports","Add Your Vehicle", "Setting Menu","Help & Support"]
                sideImgsArr = [self.promotionStatusImageName,"blue_wallet","qr_code_sidemenu_new","edit_profile_sidemenu_new", "recived_money_sidemenu_new","fav_list_sidemenu_new","recent_sidemenu_new","reports_sidemenu_new", "add_vehicle_sidemenu_new", "settings_sidemenu_new", "help_support_sidemenu_new"] as [Any]
            }
        }
        if !UserLogin.shared.loginSessionExpired {
            sideLabelArr.append("Logout")
            sideImgsArr.append("logout_sidemenu_new")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        GeoLocationManager.shared.stopUpdateLocation()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "UpdateTBMoreProfile"), object: nil)
    }
    
    private func navigateToReceivedMoneyReport() {
        guard let receivedMoneyViewController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.receivedMoneyViewController.nameAndID) as? ReceivedMoneyViewController else { return }
        let rootVC = UINavigationController(rootViewController: receivedMoneyViewController)
        rootVC.modalPresentationStyle = .fullScreen
        self.present(rootVC, animated: true, completion: nil)
        //self.navigationController?.pushViewController(receivedMoneyViewController, animated: true)
    }
    
    
    func formattedDateAndTime() -> (date: String, time: String) {
        let login = UserLogin.shared.lastLogin
        var date  = login.components(separatedBy: " ").first ?? ""
        var time  = login.components(separatedBy: " ").last ?? ""
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd hh:mm:ss.S a"
        
        dateFormatterGet.calendar = Calendar(identifier: .gregorian)
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEE, dd-MMM-yyyy"
        dateFormatterPrint.calendar = Calendar(identifier: .gregorian)
        
        let timeFormatterPrint = DateFormatter()
        timeFormatterPrint.dateFormat = "HH:mm:ss"//"HH:mm:ss"//
        timeFormatterPrint.calendar = Calendar(identifier: .gregorian)
        let lDate = dateFormatterGet.date(from: login)
        date = dateFormatterPrint.string(from: lDate ?? Date.init())
        
        time = timeFormatterPrint.string(from: lDate ?? Date.init())
        
        return (date,time)
    }
    
    private func updateMoreProfile() {
        self.updateView()
        let dateDetails = self.formattedDateAndTime()
        DispatchQueue.main.async {
             self.loginDate.text = dateDetails.date
            self.timeLogin.text = dateDetails.time//UserDefaults.standard.string(forKey: "USERLASTLOGIN")
          }
        if UserLogin.shared.loginSessionExpired
        {
            self.name.text = "XXXXXXXXXXX".localized
            self.profileType.text = "XXXXXXXXXXX".localized
            let amountBal = Double(UserLogin.shared.walletBal) ?? 0.00
            if amountBal < 500000.00 {
                DispatchQueue.main.async {
                  self.amount.attributedText = self.walletBalance()
                }
            } else {
                DispatchQueue.main.async {
                self.amount.attributedText = NSAttributedString.init(string: "XXXXXXXXXX".localized)
                }
            }
        } else {
            if ok_default_language == "my" {
                self.name.text = parabaik.uni(toZawgyi:UserModel.shared.name)
            }
            else {
                self.name.text = UserModel.shared.name
            }
            self.profileType.text = "(\(self.agentTypeString().localized))"
            self.amount.attributedText = self.walletBalance()
          }
        
    }
    
    func navigateToSettings() {
        guard let settingsViewController = UIStoryboard(name: "Setting", bundle: Bundle.main).instantiateViewController(withIdentifier: "SMSettingViewController") as? SMSettingViewController else { return }
        //        let navController = UINavigationController(rootViewController: settingsViewController)
        self.navigationController?.pushViewController(settingsViewController, animated: true)
    }
    
    func navigateToReportList() {
        guard let reportViewController = UIStoryboard(name: "Reports", bundle: Bundle.main).instantiateViewController(withIdentifier: ConstantsController.reportListViewController.nameAndID) as? ReportListViewController else { return }
        let navController = UINavigationController(rootViewController: reportViewController)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true, completion: nil)
    }
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    func updateView() {
        
        if UserLogin.shared.loginSessionExpired {
            DispatchQueue.main.async {
                self.profileImageView.image = UIImage(named: "avatar")
            }
        }else {
            let profileStr = UserModel.shared.proPic.replacingOccurrences(of: " ", with: "%20")
            if VillageManager.shared.data.registrationStatus == "1" {
                if profileStr != "" {
                    if let url = URL(string: profileStr){
                        self.downLoadAndCacheImage(url: url, handler: { (image) in
                            self.profileImageView.image = image
                            self.profileImageView.contentMode = .scaleAspectFill
                        })
                    }else {
                        if let encoded = profileStr.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed), let url = URL(string: encoded){
                            self.downLoadAndCacheImage(url: url, handler: { (image) in
                                self.profileImageView.image = image
                                self.profileImageView.contentMode = .scaleAspectFill
                            })
                        }
                    }
                }
            }else {
                if let url = URL(string: profileStr){
                    self.downLoadAndCacheImage(url: url, handler: { (image) in
                        self.profileImageView.image = image
                        self.profileImageView.contentMode = .scaleAspectFill
                    })
                }
            }
        }
    }
    
    fileprivate func setLanguage(language : String, cell : TBMoreLanguageSelectionCell) {
        switch language {
        case "en" :
            cell.selectedLanguageLbl.text = "English (အဂၤလိပ္ဘာသာ)"
            cell.firstLanguageLabel.text = "Myanmar (ျမန္မာဘာသာ)"
            cell.firstFlag.image = UIImage(named: "myanmarF")
            cell.secondLanguageLabel.text = "Chinese (တရုတ္ဘာသာ)"
            cell.secondFlag.image = UIImage(named: "chinaF")
            cell.thirdLanguageLabel.text = "Thai (ထိုင္းဘာသာ)"
            cell.thirdFlag.image = UIImage(named: "thaiF")
            cell.fourthLanguageLabel.text = "Unicode (ျမန္မာဘာသာ)"
            cell.fourthFlag.image = UIImage(named: "myanmarF")
        case "my":
            cell.selectedLanguageLbl.text = "Myanmar (ျမန္မာဘာသာ)"
            cell.firstLanguageLabel.text = "English (အဂၤလိပ္ဘာသာ)"
            cell.firstFlag.image = UIImage(named: "ukF")
            cell.secondLanguageLabel.text = "Chinese (တရုတ္ဘာသာ)"
            cell.secondFlag.image = UIImage(named: "chinaF")
            cell.thirdLanguageLabel.text = "Thai (ထိုင္းဘာသာ)"
            cell.thirdFlag.image = UIImage(named: "thaiF")
            cell.fourthLanguageLabel.text = "Unicode (ျမန္မာဘာသာ)"
            cell.fourthFlag.image = UIImage(named: "myanmarF")
        case "zh-Hans":
            cell.selectedLanguageLbl.text = "Chinese (တရုတ္ဘာသာ)"
            cell.firstLanguageLabel.text = "English (အဂၤလိပ္ဘာသာ)"
            cell.firstFlag.image = UIImage(named: "ukF")
            cell.secondLanguageLabel.text = "Myanmar (ျမန္မာဘာသာ)"
            cell.secondFlag.image = UIImage(named: "myanmarF")
            cell.thirdLanguageLabel.text = "Thai (ထိုင္းဘာသာ)"
            cell.thirdFlag.image = UIImage(named: "thaiF")
            cell.fourthLanguageLabel.text = "Unicode (ျမန္မာဘာသာ)"
            cell.fourthFlag.image = UIImage(named: "myanmarF")
        case "Sim Not Available":
            cell.selectedLanguageLbl.text = "Thai (ထိုင္းဘာသာ)"
            cell.firstLanguageLabel.text = "English (အဂၤလိပ္ဘာသာ)"
            cell.firstFlag.image = UIImage(named: "ukF")
            cell.secondLanguageLabel.text = "Myanmar (ျမန္မာဘာသာ)"
            cell.secondFlag.image = UIImage(named: "myanmarF")
            cell.thirdLanguageLabel.text = "Chinese (တရုတ္ဘာသာ)"
            cell.thirdFlag.image = UIImage(named: "chinaF")
            cell.fourthLanguageLabel.text = "Unicode (ျမန္မာဘာသာ)"
            cell.fourthFlag.image = UIImage(named: "myanmarF")
        case "uni":
            cell.selectedLanguageLbl.text = "Unicode (ျမန္မာဘာသာ)"
            cell.firstLanguageLabel.text = "English (အဂၤလိပ္ဘာသာ)"
            cell.firstFlag.image = UIImage(named: "ukF")
            cell.secondLanguageLabel.text = "Myanmar (ျမန္မာဘာသာ)"
            cell.secondFlag.image = UIImage(named: "myanmarF")
            cell.thirdLanguageLabel.text = "Chinese (တရုတ္ဘာသာ)"
            cell.thirdFlag.image = UIImage(named: "chinaF")
            cell.fourthLanguageLabel.text = "Thai (ထိုင္းဘာသာ)"
            cell.fourthFlag.image = UIImage(named: "thaiF")
        default :
            cell.selectedLanguageLbl.text = "Myanmar (ျမန္မာဘာသာ)"
            cell.firstLanguageLabel.text = "English (အဂၤလိပ္ဘာသာ)"
            cell.firstFlag.image = UIImage(named: "ukF")
            cell.secondLanguageLabel.text = "Chinese (တရုတ္ဘာသာ)"
            cell.secondFlag.image = UIImage(named: "chinaF")
            cell.thirdLanguageLabel.text = "Thai (ထိုင္းဘာသာ)"
            cell.thirdFlag.image = UIImage(named: "thaiF")
            cell.fourthLanguageLabel.text = "Unicode (ျမန္မာဘာသာ)"
            cell.fourthFlag.image = UIImage(named: "myanmarF")
        }
    }
    
    func appendSecondSectionCell() {
        
        self.secondSection.removeAll()
        var cell:TBMoreLanguageSelectionCell? = nil
        DispatchQueue.main.async {
               cell = self.tableView.dequeueReusableCell(withIdentifier: String(describing: TBMoreLanguageSelectionCell.self)) as? TBMoreLanguageSelectionCell
               let currentLangage = UserDefaults.standard.string(forKey: "currentLanguage") ?? ""
               self.setLanguage(language : currentLangage, cell : cell!)
               if let myFont = UIFont(name: appFont, size: 13)  {
                   cell?.chooseLanguageLbl.font = myFont
               }
               cell?.chooseLanguageLbl.text = "Choose Language".localized
               cell?.delegate = self
               self.secondSection.append(cell!)
        for (index, element) in self.sideLabelArr.enumerated() {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "TBMoreTableCell") as? TBMoreTableCell
                cell?.titleTag = self.sideLabelArr[index]
                let image = self.sideImgsArr[index]
                cell?.wrapOtherSections(img: image, name: element)
                self.secondSection.append(cell!)
           }
          self.tableView.reloadData()
        }
    }
    
    //MARK:- Cell Delegates Method
    func openLanguageSelection() {
        UIView.animate(withDuration: 0.3) {
            if self.languageSectionHeight == 60.00 {
                self.languageSectionHeight = 300.00
            } else {
                self.languageSectionHeight = 60.00
            }
            self.tableView.reloadData()
        }
    }
    
    func selectApplicationLanguage(stringTag: String?) {
        let languageArray = stringTag?.components(separatedBy: " (")
        if (languageArray?.count)! > 0 {
            var currentLanguage = ""
            let language = languageArray![0]
            switch language {
            case "English":
                currentLanguage = "en"
                appFont = "Zawgyi-One"
            case "Myanmar":
                currentLanguage = "my"
                appFont = "Zawgyi-One"
            case "Chinese":
                currentLanguage = "zh-Hans"
                appFont = "Zawgyi-One"
            case "Thai":
                currentLanguage = "th"
                appFont = "Zawgyi-One"
            case "Unicode":
                currentLanguage = "uni"
                appFont = "Myanmar3"
            default:
                currentLanguage = "my"
                appFont = "Zawgyi-One"
            }
            UserDefaults.standard.set(currentLanguage, forKey: "currentLanguage")
            appDel.setSeletedlocaLizationLanguage(language: currentLanguage)
            UserDefaults.standard.synchronize()
            
            if let delegate = self.languageDelegate {
                delegate.resetDashboardLanguage()
            }
            self.updateAddress()
            
            UIView.animate(withDuration: 0.3) {
                self.secondSection.removeAll()
                self.appendSecondSectionCell()
                self.languageSectionHeight = 60.00
                self.tableView.reloadData()
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return secondSection.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return secondSection[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     
        if indexPath.row == 0 {
            return languageSectionHeight
        } else {
            if  let cell = self.secondSection[indexPath.row] as? TBMoreTableCell {
                if  cell.titleTag.lowercased() == sideLabelArr[0].lowercased() {
                    let agent = UserModel.shared.agentType
                    if appDelegate.getSelectedLanguage() == "my" {
                        return (agent == .merchant || agent == .advancemerchant
                            || agent == .agent) ?  80.00 : 0.00
                    } else {
                        return (agent == .merchant || agent == .advancemerchant || agent == .agent) ?  70.00 : 0.00
                    }
                } else {
                    return 60.00
                }
            }else {
                return 30.00
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? TBMoreTableCell {
            self.performTask(withIndex: cell.titleTag)
        }
    }
    
    func biometricAuthentication(isSuccessful: Bool, inScreen screen: String) {
        
        if isSuccessful {
            if let validValue = balanceCheckTimeLabel.text?.components(separatedBy: " "){
                if validValue.indices.contains(2){
                    UserDefaults.standard.set(validValue[2], forKey: "USERLASTLOGIN")
                }
            }
            
            if sideLabelArr.contains("Logout") {
                self.updateMoreProfile()
                self.updateCurrentAddress()
            } else {
                sideLabelArr.append("Logout")
                sideImgsArr.append("logout_sidemenu_new")
                self.secondSection.removeAll()
                self.appendSecondSectionCell()
                self.tableView.reloadData()
                self.updateMoreProfile()
                self.updateCurrentAddress()
            }
            
            if screen == "Recent" {
                let viewController = UIStoryboard(name: "Recent", bundle: Bundle.main).instantiateViewController(withIdentifier: "recentRoot")
                viewController.modalPresentationStyle = .fullScreen
                self.present(viewController, animated: true, completion: nil)
            }
            
            if screen == "MyProfile" {
                if isSuccessful{
                    self.showEditProfile()
                }
            }else if screen == "ReportList" {
                if isSuccessful{
                    navigateToReportList()
                }
            }else if screen == "ReceivedMoneyReport" {
                if isSuccessful{
                    self.navigateToReceivedMoneyReport()
                }
            }else if screen == "AddYourVehicle" {
                if isSuccessful{
                    let viewController = UIStoryboard(name: "AddVehicle", bundle: Bundle.main).instantiateViewController(withIdentifier: "addVehicleRoot")
                    viewController.modalPresentationStyle = .fullScreen
                    self.present(viewController, animated: true, completion: nil)
                }
            }else if screen == "AmountTapped" {
                if isSuccessful{
                    self.getAmountBalanceFromServer()
                }
            }else if screen == "SettingsSideMenu" {
                if isSuccessful {
                    self.navigateToSettings()
                }
            }else {
                
            }
        }
    }
    
    fileprivate func showWalletSizeScreen() {
        let vcString = String.init(describing: OKWalletSizeViewController.self)
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: vcString)
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle   = .coverVertical
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    fileprivate func shareBusinessDetailsAction() {
        // Mohit code
        if UserDefaults.standard.object(forKey: "shopID") != nil {
            self.createBusiness()
        }else {
            self.customAlert1(msg: "Do you want to save this location as your business area ?".localized, title: "")
        }
    }
    
    func performTask(withIndex title: String) {
        let strCheck = sideLabelArr[0]
        
        switch title {
        case "View Wallet Size":
            alertViewObj.wrapAlert(title: "", body: "Coming Soon".localized, img: UIImage(named: "blue_wallet"))
            alertViewObj.addAction(title: "OK".localized, style: .cancel , action: {
                
            })
            alertViewObj.showAlert(controller: self)
        //self.showWalletSizeScreen()
        case "My Profile":
            if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "MyProfile", delegate: self)
            }else {
                self.showEditProfile()
            }
        case "My QR Code":
            let myQRCode = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: TBMyQRCode.self)) as? TBMyQRCode
            myQRCode?.navigation = self.navigationController
            //            myQRCode?.view.frame = CGRect.init(x: 0, y: 0, width: screenWidth, height: 548.00)
            myQRCode?.title = "My QR Code".localized
            myQRCode?.itemNavigation = self.navigationItem
            self.navigationController?.pushViewController(myQRCode!, animated: true)
            
        case "Ticket Gallery":
            let viewController = UIStoryboard(name: "TicketGallery", bundle: Bundle.main).instantiateViewController(withIdentifier: "GalleryNavigation")
            self.navigationController?.pushViewController(viewController, animated: true)
        case "Received Money":
            if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "ReceivedMoneyReport", delegate: self)
            } else {
                self.navigateToReceivedMoneyReport()
            }
        case "Favorite List":
            let vc = UIStoryboard.init(name: "Favorite", bundle: Bundle.main).instantiateViewController(withIdentifier: "favoriteNav")
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        case "Reports":
            //navigateToReportList()
            
            if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "ReportList", delegate: self)
            } else {
                navigateToReportList()
            }
        case "Recent":
            
            if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "Recent", delegate: self)
            } else {
                let viewController = UIStoryboard(name: "Recent", bundle: Bundle.main).instantiateViewController(withIdentifier: "recentRoot")
                viewController.modalPresentationStyle = .fullScreen
                self.present(viewController, animated: true, completion: nil)
            }
            
            
        case "Add Your Vehicle":
            if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "AddYourVehicle", delegate: self)
            } else {
                let viewController = UIStoryboard(name: "AddVehicle", bundle: Bundle.main).instantiateViewController(withIdentifier: "addVehicleRoot")
                viewController.modalPresentationStyle = .fullScreen
                self.present(viewController, animated: true, completion: nil)
            }
            
        case "Setting Menu":
            if UserLogin.shared.loginSessionExpired {
                OKPayment.main.authenticate(screenName: "SettingsSideMenu", delegate: self)
            } else {
                self.navigateToSettings()
            }
            
        case "Help & Support":
            guard appDel.checkNetworkAvail() else {
                PaytoConstants.alert.showErrorAlert(title: nil, body: PaytoConstants.messages.noInternet.localized)
                return
            }
            let viewController = UIStoryboard(name: "HelpSupport", bundle: Bundle.main).instantiateViewController(withIdentifier: "HelpNavigation")
            viewController.modalPresentationStyle = .fullScreen
            self.present(viewController, animated: true, completion: nil)
            
        case strCheck:
            self.shareBusinessDetailsAction()
            break
        case "Logout":
            self.showLogoutAlert()
        default:
            break
            
        }
        
    }
    
    fileprivate func showEditProfile(){
        //        //      self.updateView()
        //        //      println_debug("New UP Status : " + self.strNewUpStatus)
        //        //      let story = UIStoryboard.init(name: "MyProfile", bundle: nil)
        //        //      let vc = story.instantiateViewController(withIdentifier: "UpdateProfile") as! UpdateProfile
        //        //      vc.navigation = self.navigationController
        //        //      let navController = UpdateProfileNavController.init(rootViewController: vc)
        //        //      self.present(navController, animated: true, completion: nil)
        //
        //        ///*
        //         //New Profile
        if(UserModel.shared.accountType == "1")//personal user
        {
            // let story = UIStoryboard.init(name: "MyProfile", bundle: nil)
            //let vc = story.instantiateViewController(withIdentifier: "PersonalDetailsUP") as! PersonalDetailsUP
            //vc.navigation = self.navigationController
            //vc.statusView = "Personal"
            // let navController = UpdateProfileNavController.init(rootViewController: vc)
            // self.present(navController, animated: true, completion: nil)
            let story = UIStoryboard.init(name: "MyProfile", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "UpdateProfile") as! UpdateProfile
            vc.navigation = self.navigationController
            let navController = UpdateProfileNavController.init(rootViewController: vc)
            navController.modalPresentationStyle = .fullScreen
            self.present(navController, animated: true, completion: nil)
        } else {
            let story = UIStoryboard.init(name: "MyProfile", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "UpdateProfile") as! UpdateProfile
            vc.navigation = self.navigationController
            let navController = UpdateProfileNavController.init(rootViewController: vc)
            navController.modalPresentationStyle = .fullScreen
            self.present(navController, animated: true, completion: nil)
        }
        //         //*/
        //        /*
        //         if self.strNewUpStatus == "-1"
        //         {
        //         //New Profile
        //         if(UserModel.shared.accountType == "1")//personal user
        //         {
        //
        //         let navController = UpdateProfileNavController.init(rootViewController: vc)
        //         self.present(navController, animated: true, completion: nil)
        //         } else {
        //         let story = UIStoryboard.init(name: "MyProfile", bundle: nil)
        //         let vc = story.instantiateViewController(withIdentifier: "UpdateProfile") as! UpdateProfile
        //         //vc.statusView = "Update"
        //         vc.navigation = self.navigationController
        //         let navController = UpdateProfileNavController.init(rootViewController: vc)
        //         self.present(navController, animated: true, completion: nil)
        //         }
        //         }
        //         else
        //         {
        //         if UserModel.shared.registrationStatus == "-1" || UserModel.shared.registrationStatus == "0" {
        //         let story = UIStoryboard.init(name: "OldProfile", bundle: nil)
        //         let vc = story.instantiateViewController(withIdentifier: "OldProfileBase") as! OldProfileBase
        //         let navController = UpdateProfileNavController.init(rootViewController: vc)
        //         self.present(navController, animated: true, completion: nil)
        //         } else {
        //         let story = UIStoryboard.init(name: "OldProfile", bundle: nil)
        //         let vc = story.instantiateViewController(withIdentifier: "OldProfileBase") as! OldProfileBase
        //         let navController = UpdateProfileNavController.init(rootViewController: vc)
        //         self.present(navController, animated: true, completion: nil)
        //         }
        //
        //         }
        //         */
        
        //        if UserModel.shared.registrationStatus == "-1" || UserModel.shared.registrationStatus == "0" {
        //            let story = UIStoryboard.init(name: "OldProfile", bundle: nil)
        //            let vc = story.instantiateViewController(withIdentifier: "OldProfileBase") as! OldProfileBase
        //            let navController = UpdateProfileNavController.init(rootViewController: vc)
        //            self.present(navController, animated: true, completion: nil)
        //        } else {
        //            let story = UIStoryboard.init(name: "OldProfile", bundle: nil)
        //            let vc = story.instantiateViewController(withIdentifier: "OldProfileBase") as! OldProfileBase
        //            let navController = UpdateProfileNavController.init(rootViewController: vc)
        //            self.present(navController, animated: true, completion: nil)
        //        }
        
    }
    
    fileprivate func showLogoutAlert() {
        
        appDel.floatingButtonControl?.window.isHidden = true
        alertViewObj.wrapAlert(title: "", body: appDel.getlocaLizationLanguage(key: "You are about to log out.".localized), img: UIImage(named: "logout_st"))
        alertViewObj.addAction(title: appDel.getlocaLizationLanguage(key: "Cancel"), style: .cancel , action: {
            appDel.floatingButtonControl?.window.isHidden = false
        })
        alertViewObj.addAction(title: appDel.getlocaLizationLanguage(key: "Logout".localized), style: .target , action: {
            UserLogin.shared.loginSessionExpired = true
            //self.signOutAction()
        })
        alertViewObj.showAlert(controller: self)
        
    }
    
    @objc fileprivate func signOutAction() {
        DispatchQueue.main.async {
            if self.sideLabelArr.contains("Logout") {
                self.sideLabelArr.removeLast()
                self.sideImgsArr.removeLast()
                self.secondSection.removeAll()
                self.appendSecondSectionCell()
                self.tableView.reloadData()
            }
            self.updateMoreProfile()
            self.updateCurrentAddress()
        }
    }
    
    private func updateAddress() {
        loginTime.font = UIFont(name: appFont, size: appFontSize)
        loginTime.text = "Last Login".localized
        
        
        if UserLogin.shared.loginSessionExpired
        {
            self.name.text = "XXXXXXXXXXX".localized
            self.profileType.text = "XXXXXXXXXXX".localized
            let amountBal = Double(UserLogin.shared.walletBal) ?? 0.00
            if amountBal < 500000.00 {
                self.amount.attributedText = self.walletBalance()
            } else {
                self.amount.attributedText = NSAttributedString.init(string: "XXXXXXXXXX".localized)
            }
        } else {
            if ok_default_language == "my" {
                self.name.text = parabaik.uni(toZawgyi:UserModel.shared.name)
            }
            else {
                self.name.text = UserModel.shared.name
            }
            
            self.profileType.text =  "(\(self.agentTypeString().localized))"
            self.amount.attributedText = self.walletBalance()
        }
        
        let lat  = GeoLocationManager.shared.currentLatitude
        let long = GeoLocationManager.shared.currentLongitude
        
        if let lattitude = lat, let longitude = long {
            
            geoLocManager.getAddressFrom(lattitude: lattitude, longitude: longitude, language: appDel.currentLanguage, completionHandler: { (success, address) in
                DispatchQueue.main.async {
                    if success {
                        if let adrs = address {
                            if let dictionary = adrs as? Dictionary<String,Any> {
                                let street = dictionary.safeValueForKey("street") as? String ?? ""
                                let township = dictionary.safeValueForKey("township") as? String ?? ""
                                //let city = dictionary.safeValueForKey("city") as? String ?? ""
                                let region = dictionary.safeValueForKey("region") as? String ?? ""
                                if appDel.currentLanguage == "uni" {
                                    let curAddress = String.init(format: "%@, %@, %@", street, township, region)
                                    self.location.text = curAddress
                                    userDef.set(self.location.text.safelyWrappingString(), forKey: "lastLoginKey")
                                } else {
                                    let curAddress = Rabbit.uni2zg(String.init(format: "%@, %@, %@", street, township, region))
                                    self.location.text = curAddress
                                    userDef.set(self.location.text.safelyWrappingString(), forKey: "lastLoginKey")
                                }
                                
                            }
                        }
                    } else {
                        self.location.text = self.lastLoginAddress
                    }
                }
            })
        } else {
            self.location.text = lastLoginAddress
        }
    }
    
    private func updateCurrentAddress() {
        self.updateAddress()
    }
    
    fileprivate func sessionLogOut() {
        //OKSessionValidation.isMainAppSessionExpired = true
    }
    
    fileprivate func viewLayouting(onView v : UIView) {
        v.layer.masksToBounds = true
        v.layer.cornerRadius = v.frame.width / 2
        // border
        v.layer.borderColor = UIColor.init(hex: "F3C632").cgColor
        v.layer.borderWidth = 1.5
        // drop shadow
        v.layer.shadowColor = UIColor.lightGray.cgColor
        v.layer.shadowOpacity = 0.8
        v.layer.shadowRadius = 2.0
        v.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
    }
    
    
    fileprivate func getAmountBalanceFromServer()
    {
        
        if appDelegate.checkNetworkAvail() {
            let web      = PayToWebApi()
            web.delegate = self
            var ip = ""
            if let ipAdd = OKBaseController.getIPAddress() {
                ip = ipAdd
            }
            
            let urlStr   = String.init(format: Url.balanceCheckTimeUrl, "BALANCE", UserModel.shared.mobileNo, UserModel.shared.mobileNo, ok_password ?? "", "IPAY", ip, "iOS", "GPRS", UserLogin.shared.token)
            
            guard let encodedString = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
            
            let url = getUrl(urlStr: encodedString, serverType: .serverEstel)
            
            //let url = getUrl(urlStr: urlStr, serverType: .serverEstel)
            println_debug(url)
            let pRam = Dictionary<String,String>()
            web.genericClassXML(url: url, param: pRam as AnyObject, httpMethod: "POST", mScreen: "mBalanceScreen")
        }
        else {
            self.noInternetAlert()
        }
    }
    
    fileprivate func getprofiProfileStatus(){
        if appDelegate.checkNetworkAvail() {
            let web      = WebApiClass()
            web.delegate = self
            let urlStr   = Url.getNewUpStatus
            let ur = getUrl(urlStr: urlStr, serverType: .serverApp)
            println_debug(ur)
            web.genericClass(url: ur, param: self.loginInfoDictionary() as AnyObject, httpMethod: "POST", mScreen: "mGetNewUpStatus")
        }else {
            self.noInternetAlert()
        }
    }
    
    //MARK:- API Response
    
    //PTWebresponseDelegate
    func webSuccessResult(data: Any, screen: String) {
        println_debug(data)
        println_debug(screen)
        
        if let xmlString = data as? String {
            let xml = SWXMLHash.parse(xmlString)
            balTimeDict.removeAll()
            self.enumerateMore(indexer: xml)
            println_debug(balTimeDict)
            if balTimeDict["resultdescription"] as? String == "Invalid Secure Token" {
                DispatchQueue.main.async {
                    self.removeProgressView()
                    OKPayment.main.authenticate(screenName: "AmountTapped", delegate: self)
                }
            } else if balTimeDict["resultdescription"] as? String == "Wallet Not Found" {
                DispatchQueue.main.async {
                    self.removeProgressView()
                    self.showErrorAlert(errMessage: "Wallet Not Found".localized)
                }
            }
            else if (balTimeDict["resultdescription"] as? String)?.lowercased() == "Transaction Successful".lowercased() {
                DispatchQueue.main.async {
                    self.removeProgressView()
                    UserLogin.shared.walletBal = self.balTimeDict["walletbalance"] as? String ?? "0.0"
                    UserLogin.shared.kickBack = self.balTimeDict["udv1"] as? String ?? "0.0"
                    self.amount.attributedText = self.attributedString(amount: self.amountInFormat(self.balTimeDict["walletbalance"] as? String ?? "0.0"))
                    self.balanceCheckTimeLabel.text = self.formatter.string(from: NSDate() as Date)
                    self.showAmountCircularVIew()
                }
            } else {
                DispatchQueue.main.async {
                    self.removeProgressView()
                    if let message = self.balTimeDict["resultdescription"] as? String {
                        self.showErrorAlert(errMessage: message.localized)
                    }
                }
            }
        }
    }
    
    func enumerateMore(indexer: XMLIndexer) {
        for child in indexer.children {
            balTimeDict[child.element!.name] = child.element!.text
            enumerateMore(indexer: child)
        }
    }
    
    func webFailureResult(screen: String) {
        println_debug("Failure")
    }
    
    
    fileprivate func showAmountCircularVIew(){
        self.updateView()
        self.amount.attributedText = self.walletBalance()
        self.balanceCheckTimeLabel.text = self.formatter.string(from: NSDate() as Date)
        let vcString = String.init(describing: MainBalanceViewController.self)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: vcString)
        vc?.modalPresentationStyle = .overCurrentContext
        vc?.modalTransitionStyle   = .coverVertical
        self.navigationController?.present(vc!, animated: true, completion: nil)
    }
    
    @IBAction func makeCircularView_action(_ sender: UIButton) {
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "AmountTapped", delegate: self)
        } else {
            self.getAmountBalanceFromServer()
            //self.self.showAmountCircularVIew()
        }
    }
    
    @objc func HandleTapGesture(_:UIGestureRecognizer) {
        if UserLogin.shared.loginSessionExpired {
            OKPayment.main.authenticate(screenName: "MyProfile", delegate: self)
        } else {
            self.showEditProfile()
        }
    }
}

extension TBMoreViewController{
    
    func customAlert1(msg:String,title:String) {
        
        alertViewObj.addTextField(title: title, body: msg, img: #imageLiteral(resourceName: "share_location_sideMenu"))
        alertViewObj.addAction(title: "NO".localized, style: .cancel){}
        alertViewObj.addAction(title: "YES".localized, style: .target){
            
            
            let viewController = UIStoryboard(name: "Setting", bundle: Bundle.main).instantiateViewController(withIdentifier: "customLayerVC") as? customLayerVC
            viewController?.modalPresentationStyle = .overCurrentContext
            
            
            
            //      let ScreenView = Bundle.main.loadNibNamed("SMViewOne", owner: self, options: nil)?[2] as! SMAddFiveView
            
            println_debug(UserDefaults.standard.object(forKey: "shopID"))
            
            if (UserDefaults.standard.object(forKey: "shopID") as? String) != nil{
                self.strSubCatName = "Delete"
            }
            else{
                self.getCategoaryName()
                //            self.strSubCatName = "Ticketing"
            }
            
            
            
            if (self.strSubCatName == "Bus") || (self.strSubCatName == "Ticketing") {
                
                let ScreenView = Bundle.main.loadNibNamed("SMAddFiveView", owner: self, options: nil)?[3] as! SMViewThree
                let labelTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismiss))
                labelTapGesture.numberOfTapsRequired = 1
                ScreenView.addGestureRecognizer(labelTapGesture)
                ScreenView.delegate = self
                ScreenView.txt_busniessname.text = UserModel.shared.businessName
                ScreenView.parentVCThree = self
                ScreenView.frame = CGRect(x: 20, y:((screenHeight/2)-130) , width: screenWidth-40, height: 450)
                viewController?.view.addSubview(ScreenView)
                
            }
                
            else if (self.strSubCatName == "Taxi") {
                
                let ScreenView = Bundle.main.loadNibNamed("SMAddFiveView", owner: self, options: nil)?[2] as! SMViewOne
                let labelTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismiss))
                labelTapGesture.numberOfTapsRequired = 1
                ScreenView.addGestureRecognizer(labelTapGesture)
                ScreenView.delegate = self
                ScreenView.txt_busniessname.text = UserModel.shared.businessName
                ScreenView.parentVCOne = self
                ScreenView.frame = CGRect(x: 20, y:((screenHeight/2)-130) , width: screenWidth-40, height: 535)
                viewController?.view.addSubview(ScreenView)
                
            }
                
            else if (self.strSubCatName == "Delete") {
                
                let ScreenView = Bundle.main.loadNibNamed("SMAddFiveView", owner: self, options: nil)?[6] as! SMDeleteView
                let labelTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismiss))
                labelTapGesture.numberOfTapsRequired = 1
                ScreenView.addGestureRecognizer(labelTapGesture)
                ScreenView.delegate = self
                
                if userDef.bool(forKey: "ShopOnOff") {
                    ScreenView.switchStatus.setOn(true, animated: false)
                }else {
                    ScreenView.switchStatus.setOn(false, animated: false)
                }
                
                
                ScreenView.shopNamelbl.text = UserDefaults.standard.value(forKey: "txtValue") as? String
                ScreenView.parentdeleteVC = self
                ScreenView.frame = CGRect(x: 20, y:((screenHeight/2)-130) , width: screenWidth-40, height: 230)
                viewController?.view.addSubview(ScreenView)
                
            }
                
            else{
                
                let ScreenView = Bundle.main.loadNibNamed("SMAddFiveView", owner: self, options: nil)?[4] as! SMViewTwo
                let labelTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismiss))
                labelTapGesture.numberOfTapsRequired = 1
                ScreenView.addGestureRecognizer(labelTapGesture)
                ScreenView.delegate = self
                ScreenView.txt_busniessname.text = UserModel.shared.businessName
                ScreenView.parentVCTwo = self
                ScreenView.frame = CGRect(x: 20, y:((screenHeight/2)-130) , width: screenWidth-40, height: 260)
                viewController?.view.addSubview(ScreenView)
                
            }
            //viewController?.modalPresentationStyle = .fullScreen
            self.present(viewController!, animated:true, completion:nil)
            // self.updateCurrentLocationToUI(alerText: alertViewObj.AlerTextReturnString())
        }
        
        alertViewObj.showAlert(controller: self)
        
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismiss(sender:UITapGestureRecognizer) {
        
        sender.view?.isHidden = true
        sender.view?.removeFromSuperview()
        
    }
    
}


