//
//  QRScanDetailTableViewCell.swift
//  OK
//
//  Created by E J ANTONY on 02/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Foundation

class QRScanDetailTableViewCell: UITableViewCell {

    //MARK: - Outlet
    @IBOutlet weak var labelTransactionId: UILabel!
    @IBOutlet weak var labelAmount: UILabel!
    @IBOutlet weak var labelSenderNo: UILabel!
    @IBOutlet weak var labelSenderName: UILabel!
    @IBOutlet weak var labelReceiverNo: UILabel!
    @IBOutlet weak var labelReceiverName: UILabel!
    @IBOutlet weak var labelScanTime: UILabel!
    @IBOutlet weak var labelGenerateTime: UILabel!
    @IBOutlet weak var labelAge: UILabel!
    @IBOutlet weak var labelGender: UILabel!
    @IBOutlet weak var labelCashback: UILabel!
    @IBOutlet weak var labelBonus: UILabel!
    @IBOutlet weak var labelTransType: UILabel!
    
    //MARK: - Views Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func generateTime(dateStr: String) -> String {
        let time = dateStr.components(separatedBy: ":")
        if time[0].count > 0 {
            if Int(time[0]) ?? 0 >= 12 {
                let tm = Int(time[0]) ?? 0
                let hour = tm - 12
                let finalTime = String(hour) + ":" + time[1] + ":" +  time[2]
                return finalTime + " PM"
            }else {
                return dateStr + " AM"
            }
        }
        return ""
    }
     
     
     func generateDate(dateStr: String) -> String {

        let dateFormatterA = DateFormatter()
        dateFormatterA.dateFormat = "dd/MMM/yyyy HH:mm:ss"
        let date = (dateFormatterA.date(from: dateStr) ?? Date()) as Date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd-MMM-yyyy hh:mm:ss a"
        let currentDateString: String = dateFormatter.string(from: date)
        return  currentDateString
        
//          let dateFormat = DateFormatter()
//          //dateFormat.timeZone = .current
//        dateFormat.calendar = Calendar(identifier: .gregorian)
//
//          let str : String = dateStr
//        dateFormat.dateFormat = "dd/MMM/yyyy HH:mm:ss"
//
//        if str.contains("AM") || str.contains("PM") {
//            dateFormat.dateFormat = "dd/MMM/yyyy hh:mm:ss a"
//        }
//          guard let newDate = dateFormat.date(from: str) else {
//              return str
//          }
//          dateFormat.dateFormat = "EEE, dd-MMM-yyyy hh:mm:ss a"
//          let dateString = dateFormat.string(from: newDate)
//          return dateString
      }

    //MARK: - Methods
    func configureCell(dataSet: PaymentVerification) {
        self.labelTransactionId.text = dataSet.transactionId
        if dataSet.amount?.contains(find: ".") ?? false {
            let amout = dataSet.amount?.components(separatedBy: ".")
            if amout?[1] == "0" || amout?[1] == "00" || amout?[1] == "000" {
            self.labelAmount.text = amout?[0]
            }else {
            self.labelAmount.text = dataSet.amount
            }
        }else {
            var amountX = dataSet.amount ?? "0"
            if amountX.hasSuffix(" MMK") {
                amountX = String(amountX.dropLast(4))
            }
            self.labelAmount.text = amountX
        }
     
        if let transType = dataSet.transactionType, transType == "PAYTOID" || transType == "Hide No To Pay" || transType == "PAYWITHOUT ID"{
            self.labelSenderNo.text = "XXXXXXXXXX"
        } else {
            self.labelSenderNo.text =  dataSet.senderNumber
        }
        self.labelSenderName.text = dataSet.senderName
        self.labelReceiverNo.text = UserModel.shared.mobileNo
        self.labelReceiverName.text = UserModel.shared.name
        /*
        if let dateT = dataSet.generateTime {
            var date = ""
            var time = ""
            let dateTime = dateT.components(separatedBy: " ")
            if dateTime[0].count > 0 {
                println_debug(dateTime[0])
                let str = generateDate(dateStr: dateTime[0])
                date = str
            }
            if dateTime[1].count > 0 {
                println_debug(dateTime[1])
                let str = generateTime(dateStr: dateTime[1])
                time = str
            }
            self.labelGenerateTime.text = date + " " + time
        }else {
            self.labelGenerateTime.text = " "
        }
         */
        
        if let dateT = dataSet.generateTime {
            self.labelGenerateTime.text = generateDate(dateStr: dateT)
        }else {
            self.labelGenerateTime.text = " "
        }
        self.labelScanTime.text = dataSet.scanningTime
        
        self.labelAge.text = dataSet.age
        self.labelGender.text = dataSet.gender
        if let cashback = dataSet.cashback, cashback != "" {
            var cashBKAmount = cashback
            if cashBKAmount.hasSuffix(".00") {
                cashBKAmount = String(cashBKAmount.dropLast(3))
            }
            self.labelCashback.text = cashBKAmount
        } else {
            self.labelCashback.text = "-"
        }
        if let bonus = dataSet.bonusPoint, bonus != "" {
            var bonusAmount = bonus
            if bonusAmount.hasSuffix(".00") {
                bonusAmount = String(bonusAmount.dropLast(4))
            }
            self.labelBonus.text = bonusAmount
        } else {
            self.labelBonus.text = "-"
        }
        self.labelTransType.text = dataSet.transactionType
    }
}
