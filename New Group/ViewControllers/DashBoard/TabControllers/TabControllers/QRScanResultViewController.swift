//
//  QRScanResultViewController.swift
//  OK
//
//  Created by E J ANTONY on 02/07/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

protocol QRDelegate : class {
    func refreshControler()
}

class QRScanResultViewController: UIViewController {

    //MARK: - Outlet
    @IBOutlet weak var amountValue: UILabel!
    @IBOutlet weak var generateNumberValue: UILabel!
    @IBOutlet weak var generateNameValue: UILabel!
    @IBOutlet weak var scannerNumberValue: UILabel!
    @IBOutlet weak var scannerNameValue: UILabel!
    @IBOutlet weak var dateTimeValue: UILabel!
    @IBOutlet weak var transactionIdValue: UILabel!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonYes: UIButton!
    
    weak var delegate : QRDelegate?
    
    //MARK: - Properties
    var data: VerifyPaymentModel?
    
    //MARK: - Views Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Methods
    private func updateData() {
        
        if data?.amount?.contains(find: ".") ?? false {
            let amout = data?.amount?.components(separatedBy: ".")
            if amout?[1] == "0" || amout?[1] == "00" || amout?[1] == "000" {
                amountValue.text = wrapAmountWithCommaDecimal(key: (amout?[0] ?? "")) + " MMK"
            }else {
                amountValue.text =   wrapAmountWithCommaDecimal(key: (self.data?.amount ?? "")) + " MMK"
            }
        }else {
            amountValue.text = wrapAmountWithCommaDecimal(key: (self.data?.amount ?? "")) + " MMK"
        }
        
        if let transType = self.data?.transactionType, transType == "PAYTOID" || transType == "Hide No To Pay" {
            generateNumberValue.text = "XXXXXXXXXX"
        } else {
            var num = self.data?.senderNumber ?? ""
            if num.hasPrefix("0095") {
                num = "(+95)0" + num.dropFirst(4)
            }
            generateNumberValue.text = num
        }
        generateNameValue.text = self.data?.senderName ?? ""
        var scanNum = UserModel.shared.mobileNo
        if scanNum.hasPrefix("0095") {
            scanNum = "(+95)0" + scanNum.dropFirst(4)
        }
        scannerNumberValue.text = scanNum
        scannerNameValue.text = UserModel.shared.name
        let date = self.getDateFromString(dateStr: (self.data?.generateTime ?? ""))
        dateTimeValue.text = date
        transactionIdValue.text = self.data?.transactionId ?? ""
        
        
        //end
        self.view.layoutIfNeeded()
        self.viewHeight.constant = buttonYes.frame.maxY
        self.view.layoutIfNeeded()
    }
    
    
    func getDateFromString( dateStr : String) -> String {
        if dateStr != "" {
            let dateFormatterA = DateFormatter()
            dateFormatterA.dateFormat = "dd/MMM/yyyy HH:mm:ss"
            let date = (dateFormatterA.date(from: dateStr) ?? Date()) as Date
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEE, dd-MMM-yyyy"
            let currentDateString: String = dateFormatter.string(from: date)
            dateFormatter.dateFormat = "HH:mm:ss"
            let someDateTime: String = dateFormatter.string(from: date)
            return  currentDateString + " " + someDateTime
        }
        return dateStr
    }
    
    
    
    //MARKL: - Button Action Methods
    @IBAction func yesAction(sender: UIButton) {
        if let nav = self.navigationController {
            self.delegate?.refreshControler()
            nav.dismiss(animated: true, completion: nil)
        } else {
            self.delegate?.refreshControler()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
