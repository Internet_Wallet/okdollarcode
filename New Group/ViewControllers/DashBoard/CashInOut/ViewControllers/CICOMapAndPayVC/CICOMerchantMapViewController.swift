//
//  CICOMapLocationViewController.swift
//  OK
//
//  Created by Kethan on 8/22/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import GoogleMaps

class CICOMerchantMapViewController: CashInOutBaseViewController {
    //MARK: - Outlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var addressLabel1: UILabel!
    @IBOutlet weak var addressLabel2: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var callLabel: UILabel! {
        didSet {
            self.callLabel.text = self.callLabel.text?.localized
        }
    }
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var startRideButton: UIButton!
    @IBOutlet private weak var mapCenterPinImage: UIImageView! {
        didSet {
            self.mapCenterPinImage.isHidden = true
        }
    }
    @IBOutlet private weak var pinImageVerticalConstraint: NSLayoutConstraint!
    
    //MARK: - Properties
    var startLat = 0.0, startLong = 0.0, endLat = 0.0, endLong  = 0.0
    var currentSelectedCashDetails: CashTransferRequestStatus?
    var fromNotificationScreen: String?
    private lazy var modelCashInCashOut: CashInCashOutModel = {
        return CashInCashOutModel()
    }()
    var fromUser: Bool?
    var startMarker: GMSMarker?
    var locationManager = CLLocationManager()
    var needToShowFull = true
    var myTimer: Timer?
    var isTimeToUpdateLoc = false
    var infoView: UIView?
    var btnTitle = ""
    
    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.populateUIData()
        
        self.addBackButton(navTitle: "On Trip")
        self.setStartEndCoordinates()
        self.modelCashInCashOut.cashInCashOutDelegate = self
        self.setLocationMapDelegate()
        let camera = GMSCameraPosition.camera(withLatitude: startLat, longitude: startLong, zoom: 15.0)
        self.mapView.camera = camera
        self.mapView.setMinZoom(5, maxZoom: 18)
        self.mapView.settings.zoomGestures = true
        self.mapView.settings.myLocationButton = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(CICOPopToRoot), name: Notification.Name("CICOPopToRootVC"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(popCurrentScreen), name: Notification.Name("ShowCICOCancelAlert"), object: nil)
        self.myTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.updateBooleanForApi), userInfo: nil, repeats: true)
        btnTitle = self.startRideButton.currentTitle ?? ""
        NotificationCenter.default.addObserver(self, selector: #selector(dismissCurrentScreen), name: Notification.Name("CICOPopToroot"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showProcessAlert(_:)), name: Notification.Name("ShowCICOShowProcessAlert"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideCancelButton), name: Notification.Name("CICONotifyStartTrip"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ConfirmPop), name: Notification.Name("CICOConfirmPop"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func ConfirmPop() {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if let fromuser = fromUser, fromuser == true {
            if let status = currentSelectedCashDetails?.cashTransferMaster?.status {
                startRideButton.setTitle("Process".localized, for: .normal)
                btnTitle = startRideButton.currentTitle ?? ""
                if (status == 1) {
                    cancelButton.isHidden = true
                }
            }
            if let name = currentSelectedCashDetails?.agent?.businessName {
                self.title = name
            } else {
                self.title = currentSelectedCashDetails?.agent?.firstName ?? ""
            }
        } else {
            if let status = currentSelectedCashDetails?.cashTransferMaster?.status, let cashType = currentSelectedCashDetails?.cashTransferMaster?.cashType, let agentType = currentSelectedCashDetails?.agent?.agentType {
                if (status == 1) {
                    cancelButton.isHidden = true
                    startRideButton.setTitle("Stop & Proceed".localized, for: .normal)
                    btnTitle = startRideButton.currentTitle ?? ""
                } else if (status == 0 && agentType == 0) {
                    startRideButton.setTitle("Start Ride".localized, for: .normal)
                    btnTitle = startRideButton.currentTitle ?? ""
                } else if (status == 0 && cashType == 0 && agentType == 1) {
                    startRideButton.setTitle("Process".localized, for: .normal)
                    btnTitle = startRideButton.currentTitle ?? ""
                } else if (status == 0 && cashType == 1 && agentType == 1) {
                    startRideButton.setTitle("Process".localized, for: .normal)
                    btnTitle = startRideButton.currentTitle ?? ""
                } else if (status == 3) {
                    self.cancelButton.isHidden = true
                    startRideButton.setTitle("Waiting for user to pay".localized, for: .normal)
                    btnTitle = startRideButton.currentTitle ?? ""
                }
            }
            self.title = currentSelectedCashDetails?.customer?.firstName
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.showRoute()
    }
    
    //MARK: - Methods
    
    @objc func hideCancelButton() {
        DispatchQueue.main.async {
            self.cancelButton.isHidden = true
            self.showCICOToast(message: "Agent started".localized)
        }
    }
    private func setLocationMapDelegate() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        mapView.delegate = self
    }
    private func setStartEndCoordinates() {
        self.startLat = currentSelectedCashDetails?.cashTransferMaster?.startLocation?.latitude ?? 0.0
        self.startLong = currentSelectedCashDetails?.cashTransferMaster?.startLocation?.longitude ?? 0.0
        self.endLat = currentSelectedCashDetails?.cashTransferMaster?.rechargeDestination?.latitude ?? 0.0
        self.endLong = currentSelectedCashDetails?.cashTransferMaster?.rechargeDestination?.longitude ?? 0.0
        self.getDistanceAndDuration()
    }
    
    private func getDistanceAndDuration() {
        modelCashInCashOut.getDistanceAndTime(startLat: self.startLat, startLong: self.startLong, endLat: self.endLat, endLong: self.endLong)
    }
    
    private func addBackButton(navTitle: String) {
        let backButton = UIButton(type: .custom)
        backButton.setBackgroundImage(#imageLiteral(resourceName: "tabBarBack"), for: .normal)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        navigationItem.title = navTitle.localized
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white ]
        self.navigationController?.navigationBar.barTintColor = kYellowColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    @objc private func dismissCurrentScreen() {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc private func popCurrentScreen() {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "Transaction Cancelled".localized, body: "Request is cancelled by other user".localized, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                NotificationCenter.default.post(name: Notification.Name("ReloadCICOAllScreens"), object: nil, userInfo: nil)
                self.navigationController?.popViewController(animated: true)
            }
            alertViewObj.showAlert(controller: self)
        }
    }

    @objc func showProcessAlert(_ notification: Notification) {
        DispatchQueue.main.async {
            self.cancelButton.isHidden = true
            if self.currentSelectedCashDetails?.cashTransferMaster?.cashType == 0 {
            alertViewObj.wrapAlert(title: "Request to pay".localized, body: "\(self.currentSelectedCashDetails?.customer?.firstName ?? "") prompting you to end/process transaction.".localized, img: nil)
            } else {
                alertViewObj.wrapAlert(title: "Request to process transaction".localized, body: "Customer prompting you to end/process transaction.".localized, img: nil)
            }
            alertViewObj.addAction(title: "Cancel".localized, style: .cancel) {
                
            }
            alertViewObj.addAction(title: "Proceed".localized, style: .target) {
                self.stopAndproceedAction()
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    private func populateUIData() {
        if let customerAddress = currentSelectedCashDetails?.cashTransferMaster?.rechargeDestination?.source {
            let addressArray = customerAddress.components(separatedBy: ",")
            if (addressArray.count) > 0 {
                self.addressLabel1.text = addressArray[0]
            }
            self.addressLabel2.text = customerAddress
        }
        let width = UIScreen.main.bounds.width
        if width > 325 {
            self.addressLabel1.font = UIFont(name: appFont, size: 16.0) ?? UIFont.systemFont(ofSize: 16)
            self.addressLabel2.font = UIFont(name: appFont, size: 16.0) ?? UIFont.systemFont(ofSize: 16)
        } else {
            self.addressLabel1.font = UIFont(name: appFont, size: 14.0) ?? UIFont.systemFont(ofSize: 14)
            self.addressLabel2.font = UIFont(name: appFont, size: 14.0) ?? UIFont.systemFont(ofSize: 14)
        }
        let amount = Int(currentSelectedCashDetails?.cashTransferMaster?.amount ?? 0)
        self.amountLabel.text = String(amount) + " MMK".localized
    }
    
    private func showNotificationAlert() {
       // self.showErrorAlert(errMessage: "You are unauthorized to perform this action, please wait other user to complete the process.".localized)
        self.showCICOToast(message: "You are unauthorized to perform this action, please wait other user to complete the process.".localized)
    }
    
    private func showRoute() {
        let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(String(startLat)),\(String(startLong))&destination=\(String(endLat)),\(String(endLong))&mode=driving&sensor=true&key=\(googleAPIKey)"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if (error != nil) {
                println_debug("error")
            } else {
                do {
                    guard let dataRaw = data else { return }
                    let json = try JSONSerialization.jsonObject(with: dataRaw,
                                                                options:.allowFragments) as! [String: AnyObject]
                    let routes = json["routes"] as! NSArray
                    self.mapView.clear()
                    OperationQueue.main.addOperation({
                        for route in routes
                        {
                            if let routeOverviewPolyline = (route as! NSDictionary).value(forKey: "overview_polyline") as? NSDictionary {
                                let points = routeOverviewPolyline.object(forKey: "points")
                                let path = GMSMutablePath.init(fromEncodedPath: points! as! String)
                                let polyline = GMSPolyline.init(path: path)
                                polyline.strokeWidth = 3
                                if self.needToShowFull {
                                    let bounds = GMSCoordinateBounds(path: path!)
                                    self.mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))
                                    self.needToShowFull = false
                                }
                                polyline.map = self.mapView
                                self.getDistanceAndDuration()
                                self.showStartEndMarker()
                            }
                        }
                    })
                } catch let error as NSError{
                    println_debug("error:\(error)")
                }
            }
        }).resume()
    }
    
    private func updateNavigatorLocation(locationParam: CLLocation, curAddress: String) {
        let addressArray = curAddress.components(separatedBy: ",")
        var add = ""
        if (addressArray.count) > 0 {
            add = addressArray[0]
        }
        let currentLocation = CICOLocation(type: "POINT",
                                           coordinates: [locationParam.coordinate.longitude, locationParam.coordinate.latitude],
                                           angle: 0, time: nil, createdTime: nil, accuracy: 0,
                                           name: add, source: curAddress,
                                           latitude: locationParam.coordinate.latitude,
                                           longitude: locationParam.coordinate.longitude)
        if btnTitle == "Stop & Proceed".localized {
            let updateUserLocation = UpdateUserLocation(userID: UserDefaults.standard.value(forKey: "CICOUserID") as? String ?? "", userType: nil,
                                                        locationInfo: currentLocation, createdDate: currentSelectedCashDetails?.cashTransferMaster?.createdDate,
                                                        modifiedDate: currentSelectedCashDetails?.cashTransferMaster?.modifiedDate,
                                                        createdBy: currentSelectedCashDetails?.cashTransferMaster?.createdBy,
                                                        id: nil, transactionID: currentSelectedCashDetails?.cashTransferMaster?.transactionID)
            
            let locWhileTravel = UpdateLOCWhileTravelRequest(cashTransferMasterID: currentSelectedCashDetails?.cashTransferMaster?.id, customerID: currentSelectedCashDetails?.customer?.id, userLocation: updateUserLocation)
            modelCashInCashOut.updateLocationWhileTravel(request: locWhileTravel)
        } else {
            let updateLocReq = UpdateUserLocReq(userID: currentSelectedCashDetails?.agent?.id, userType: currentSelectedCashDetails?.agent?.agentType, location: currentLocation)
            modelCashInCashOut.updateUserLocation(request: updateLocReq)
        }
    }
    
    private func processAction() {
        stopAndproceedAction()
    }
    
    private func startRide() {
        guard let masterid = currentSelectedCashDetails?.cashTransferMaster?.id else { return }
        let agntLoc = currentSelectedCashDetails?.agent?.currentLocation
        let startTripReq = StartTripRequest(cashTransferMasterID: masterid, startLocation: agntLoc)
        modelCashInCashOut.startTripRequest(request: startTripReq)
    }
    
    private func stopAndproceedAction() {
        guard let masterid = currentSelectedCashDetails?.cashTransferMaster?.id else { return }
        let agntLoc = currentSelectedCashDetails?.agent?.currentLocation
        let endTripReq = EndTripRequest(cashMasterID: masterid, endLocation: agntLoc) 
        modelCashInCashOut.endTripRequest(request: endTripReq)
    }
    
    private func pushToPaymentScreen() {
        if let confirmVC = UIStoryboard(name: "CashInOutStoryboard", bundle: nil).instantiateViewController(withIdentifier: "CIOConfimationViewController_ID") as? CIOConfimationViewController {
            confirmVC.delegate = self
            confirmVC.currentSelectedCashDetails = self.currentSelectedCashDetails
            self.navigationController?.pushViewController(confirmVC, animated: true)
        }
    }
    
    private func getAgentMasterDetail() {
        if let reqID = currentSelectedCashDetails?.cashTransferMaster?.id {
            modelCashInCashOut.getMasterDetailRequest(id: "\"\(reqID)\"")
        }
    }
    
    private func showPayAlert() {
        //self.showErrorAlert(errMessage: "Error occured")
    }
    
    private func callToUserOrAgent(numberStr: String) {
        var number = wrapFormattedNumber(numberStr)
        if number.hasPrefix("00") {
            number = "+" + number
        }
        let phone = "tel://\(number)"
        if let url = URL.init(string: phone) {
            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        }
    }
    
    private func showCancelAlert(cancel: Bool) {
        NotificationCenter.default.post(name: Notification.Name("ReloadCICOAllScreens"), object: nil, userInfo: nil)
        DispatchQueue.main.async {
            var message = ""
            if cancel {
                message = "Request cancelled by requester"
                alertViewObj.wrapAlert(title: "Transaction Cancelled".localized, body: message.localized, img: #imageLiteral(resourceName: "succ"))
            } else {
                message = "This request is rejected by you"
                alertViewObj.wrapAlert(title: nil, body: message.localized, img: nil)
            }
            
            alertViewObj.addAction(title: "OK".localized, style: .cancel , action: {
                self.navigationController?.popToRootViewController(animated: true)
                NotificationCenter.default.post(name: Notification.Name("DismissCICOCancelAcceptScreen"), object: nil, userInfo: nil)
            })
            
            alertViewObj.showAlert(controller: self)
        }
    }
    
    private func showStartEndMarker() {
        showStartMarker()
        showEndMarker()
    }
    
    private func showStartMarker() {
        let startPosition = CLLocationCoordinate2D(latitude: startLat, longitude: startLong)
        startMarker = GMSMarker(position: startPosition)
        startMarker?.icon = #imageLiteral(resourceName: "cicoBikeInMap")
        startMarker?.map = mapView
        //startPos?.tracksViewChanges = true
        //startPos?.tracksInfoWindowChanges = true
        CATransaction.commit()
    }
    
    private func showEndMarker() {
        let endPosition = CLLocationCoordinate2D(latitude: endLat, longitude: endLong)
        let endPos = GMSMarker(position: endPosition)
        //endPos.icon = #imageLiteral(resourceName: "cicoLocationPole")
        endPos.map = mapView
        if let infoVw = self.infoView {
            endPos.iconView = infoVw
        } else {
            self.infoView = showInfoView()
            endPos.iconView = self.infoView
        }
       // endPos.infoWindowAnchor = CGPoint(x: 0.5, y: 0.5)
        //endPos.tracksViewChanges = true
        //endPos.tracksInfoWindowChanges = true
    }
    
    private func showInfoView() -> UIView {
        let infoVC = UIView(frame: CGRect(x: self.view.frame.size.width / 2 - 45, y: self.view.frame.size.height / 2, width: 70.0, height: 76.0))
        infoVC.backgroundColor = UIColor.clear
        let distanceLbl = UILabel(frame: CGRect(x: 0.0, y: 0, width: 70.0, height: 25.0))
        //distanceLbl.text = " Mi"
        distanceLbl.tag = 27
        distanceLbl.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        distanceLbl.textAlignment = .center
        distanceLbl.textColor = UIColor.white
        distanceLbl.font = UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: 17.0)
        infoVC.addSubview(distanceLbl)
        let durationLbl = UILabel(frame: CGRect(x: 0.0, y: 25.0, width: 70.0, height: 25.0))
        //durationLbl.text = " Min"
        durationLbl.textAlignment = .center
        durationLbl.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        durationLbl.tag = 28
        durationLbl.textColor = UIColor.white
        durationLbl.font = UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: 17.0)
        infoVC.addSubview(durationLbl)
        let image = UIImageView(frame: CGRect(x: 18.0, y: 50.0, width: 25.0, height: 25.0))
        image.image = #imageLiteral(resourceName: "ic_location_pin")
        infoVC.addSubview(image)
        if let routes = self.modelCashInCashOut.getTimeAndDistance?.routes {
            if routes.count > 0 {
                let legs = routes[0].legs
                if legs.count > 0 {
                    if let dist = legs[0].distance?.text {
                        distanceLbl.text = dist
                    }
                    if let dur = legs[0].duration?.text {
                        durationLbl.text = dur
                    }
                }
            }
        }
        return infoVC
    }
    
    func updateInfoview() {
        DispatchQueue.main.async {
            guard let legs = self.modelCashInCashOut.getTimeAndDistance?.routes?.first?.legs.first else { return }
            guard let infoVC = self.infoView else { return }
            for (_,infoSubView) in infoVC.subviews.enumerated() {
                if let label = infoSubView as? UILabel {
                    if label.tag == 27 {
                        if let array = legs.distance?.text?.components(separatedBy: " ") {
                            if array.count > 1, let str = array.last, str == "ft" {
                                label.text = "1.0 Mi"
                            } else {
                                label.text = legs.distance?.text ?? ""
                            }
                        } else {
                            label.text = legs.distance?.text ?? ""
                        }
                    } else if label.tag == 28 {
                        label.text = legs.duration?.text ?? ""
                    }
                }
            }
        }
    }
    
    //MARK: - Target Methods
    @objc func CICOPopToRoot() {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc override func backAction() {
        DispatchQueue.main.async {
            if let str = self.fromNotificationScreen, str.count > 0 {
                self.dismiss(animated: true, completion: nil)
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @objc func updateBooleanForApi() {
        isTimeToUpdateLoc = true
    }
    
    //MARK: - Button Action Methods
    @IBAction func callButtonAction(_ sender: UIButton) {
        if let fromuser = fromUser, fromuser == true {
            if let number = self.currentSelectedCashDetails?.agent?.phoneNumber.safelyWrappingString() {
                self.callToUserOrAgent(numberStr: number)
            }
        } else {
            if let number = self.currentSelectedCashDetails?.customer?.phoneNumber.safelyWrappingString() {
                self.callToUserOrAgent(numberStr: number)
            }
        }
    }
    
    @IBAction func cancelrequestAction(_ sender: UIButton) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "", message: "Do you want to cancel the request?".localized, preferredStyle: .alert)
            alert.addTextField { (textField) in
                textField.placeholder = "Reason for cancel".localized
                textField.text = ""
            }
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                if let customAlert = alert, let txtFldArr = customAlert.textFields, txtFldArr.count > 0 {
                    let cancelReason = txtFldArr[0].text ?? ""
                    if let modeldata = self.currentSelectedCashDetails {
                        let request = CancelCashTransferRequest(cancelledReason: cancelReason,
                                                                requestID: modeldata.cashTransferMaster?.requestID)
                        self.modelCashInCashOut.cancelCashTransfer(request: request)
                    }
                }
            }))
            alert.addAction(UIAlertAction(title: "Cancel".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func startRideAction(_ sender: UIButton) {
        if sender.titleLabel?.text == "Process".localized {
            self.processAction()
        } else if sender.titleLabel?.text == "Start Ride".localized {
            self.startRide()
        } else if sender.titleLabel?.text == "Waiting for user to pay".localized {
            self.showPayAlert()
        } else {
            self.stopAndproceedAction()
        }
    }
    
    deinit {
        self.myTimer?.invalidate()
        locationManager.stopUpdatingLocation()
        NotificationCenter.default.removeObserver(self)
    }
}

//MARK: - CLLocationManagerDelegate
extension CICOMerchantMapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status != .denied, status != .notDetermined, status != .restricted else {
            return
        }
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        startLat = location.coordinate.latitude
        startLong = location.coordinate.longitude
        if isTimeToUpdateLoc {
            geoLocManager.getLocationNameByLatLong(lattitude: String(location.coordinate.latitude), longitude: String(location.coordinate.longitude), isForCashinCashout : true) { (isSuccess, currentAddress) in
                guard isSuccess else {
                    println_debug("something went wrong when get current address from google api")
                    //need to show alert
                    return
                }
                if let curAddress = currentAddress as? String, curAddress.count > 0 {
                    currentAddressGlobal = curAddress
                    self.updateNavigatorLocation(locationParam: location, curAddress: curAddress)
                } else {
                    //need to show alert
                }
            }
            isTimeToUpdateLoc = false
        }
    }
}

// MARK: - GMSMapViewDelegate
extension CICOMerchantMapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        mapView.animate(toLocation: CLLocationCoordinate2D(latitude: startLat, longitude: startLong))
        return false
    }
}

//MARK: - CICOPopViewController
extension CICOMerchantMapViewController: CICOPopViewController {
    func popViewController() {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

// MARK: - CashInOutModel Delegate
extension CICOMerchantMapViewController: CashInOutModelDelegate {
    func apiResult(message: String?, statusCode: String?, type: CashInCashOutModel.CashInCashOutApiType) {
        switch type {
        case .updateUserLoc:
            if let lat = modelCashInCashOut.userLocBeforeStartRide?.latitude, let longit = modelCashInCashOut.userLocBeforeStartRide?.longitude {
                startLat = lat
                startLong = longit
                self.showRoute()
                self.getDistanceAndDuration()
            }
        case .updateUserLocTravel:
            if let lat = modelCashInCashOut.userLocAfterStartRide?.latitude, let longit = modelCashInCashOut.userLocAfterStartRide?.longitude {
                startLat = lat
                startLong = longit
                self.showRoute()
            }
        case .getDistTimeBtwnLoc:
            self.updateInfoview()
        default:
            break
        }
        if let fromuser = fromUser, fromuser == true {
            switch type {
            case .endTrip:
                NotificationCenter.default.post(name: Notification.Name("ReloadCICOAllScreens"), object: nil, userInfo: nil)
                println_debug("Trip ended successfully")
                DispatchQueue.main.async {
                    self.cancelButton.isHidden = true
                    self.showNotificationAlert()
                }
                locationManager.stopUpdatingLocation()
            case .cancelCashTransfer:
                self.showCancelAlert(cancel: true)
            default:
                break
            }
        } else {
            switch type {
            case .startTrip:
                DispatchQueue.main.async {
                    self.cancelButton.isHidden = true
                    self.navigationItem.title = self.currentSelectedCashDetails?.customer?.firstName ?? ""
                    self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white ]
                    self.startRideButton.setTitle("Stop & Proceed".localized, for: .normal)
                    self.btnTitle = self.startRideButton.currentTitle ?? ""
                }
                NotificationCenter.default.post(name: Notification.Name("ReloadCICOAllScreens"), object: nil, userInfo: nil)
                println_debug("Trip started successfully")
            case .endTrip:
                if let cashType = currentSelectedCashDetails?.cashTransferMaster?.cashType, cashType == 1 {
                    NotificationCenter.default.post(name: Notification.Name("ReloadCICOAllScreens"), object: nil, userInfo: nil)
                    DispatchQueue.main.async {
                        self.startRideButton.setTitle("Waiting for user to pay".localized, for: .normal)
                        self.cancelButton.isHidden = true
                        self.btnTitle = self.startRideButton.currentTitle ?? ""
                    }
                } else {
                    self.getAgentMasterDetail()
                }
                locationManager.stopUpdatingLocation()
                println_debug("Trip ended successfully")
            case .getMasterDetails:
                println_debug("Master Detail")
                DispatchQueue.main.async {
                    self.currentSelectedCashDetails = self.modelCashInCashOut.masterDetail
                    self.pushToPaymentScreen()
                }
            case .cancelCashTransfer:
                self.showCancelAlert(cancel: false)
            default:
                break
            }
        }
    }
    
    func handleCustomError(error: CustomError, type: CashInCashOutModel.CashInCashOutApiType) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error, \(error.rawValue)")
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
