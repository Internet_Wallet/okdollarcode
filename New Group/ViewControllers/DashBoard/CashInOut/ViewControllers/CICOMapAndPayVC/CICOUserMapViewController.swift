//
//  CICOUserMapViewController.swift
//  OK
//
//  Created by Kethan on 8/22/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import GoogleMaps
protocol CICOUserMapDelegate: class {
    func dismissCICOUserMap()
}

class CICOUserMapViewController: CashInOutBaseViewController {
    //MARK: - Outlets
    @IBOutlet weak var mapView : GMSMapView!
    @IBOutlet private weak var mapCenterPinImage: UIImageView! {
        didSet {
            self.mapCenterPinImage.isHidden = true
        }
    }
    @IBOutlet private weak var pinImageVerticalConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var addressLabel : UILabel!
    @IBOutlet weak var rateLabel : UILabel!
    @IBOutlet weak var reviewLabel : UILabel!
    @IBOutlet weak var durationLabel : UILabel!

    @IBOutlet weak var userNameLabel : UILabel!
    @IBOutlet weak var agentNameLabel : UILabel!
    @IBOutlet weak var requestTypeLabel : UILabel!
    
    @IBOutlet weak var userImageView : UIImageView!
    @IBOutlet weak var agebtImageView : UIImageView!
    @IBOutlet weak var callButton : UIButton!
    @IBOutlet weak var cancelButton : UIButton! {
        didSet {
            self.cancelButton.setTitle(self.cancelButton.titleLabel?.text?.localized, for: .normal)
        }
    }
    @IBOutlet weak var ratingStackView : UIStackView!
    weak var delegate: CICOUserMapDelegate?
    //MARK: - Properties
    private let locationManager = CLLocationManager()
    var startLat = 0.0
    var startLong = 0.0
    var endLat = 0.0
    var endLong  = 0.0
    var currentSelectedCashDetails: CashTransferRequestStatus?
    var currentPendingCashDetails: PendingCashTransferResponse? = nil
    private lazy var modelCashInCashOut: CashInCashOutModel = {
        return CashInCashOutModel()
    }()
    
    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if let currentModel = currentSelectedCashDetails {
            self.startLat = currentModel.cashTransferMaster?.startLocation?.latitude ?? 0.0
            self.startLong = currentModel.cashTransferMaster?.startLocation?.longitude ?? 0.0
            self.endLat = currentModel.cashTransferMaster?.rechargeDestination?.latitude ?? 0.0
            self.endLong = currentModel.cashTransferMaster?.rechargeDestination?.longitude ?? 0.0
        } else {
            if let currentModel = currentPendingCashDetails {
                self.startLat = Double(geoLocManager.currentLatitude) ?? 0.0
                self.startLong = Double(geoLocManager.currentLongitude) ?? 0.0
                self.endLat = currentModel.rechargeDestination?.latitude ?? 0.0
                self.endLong = currentModel.rechargeDestination?.longitude ?? 0.0
            }
        }
        self.mapView.settings.zoomGestures = true
        self.addBackButton(navTitle: "On Trip")
        self.updateUI()
        self.loadMapwithCurrentLocation()
        modelCashInCashOut.cashInCashOutDelegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(hideCancelButton), name: Notification.Name("CICONotifyStartTrip"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CICOPopToRoot), name: Notification.Name("CICOPopToRootVC"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dismissCurrentScreen), name: Notification.Name("ShowCICOCancelAlert"), object: nil)
    }
    
    @objc func CICOPopToRoot() {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        showRoute()
    }
    
    //MARK: - Methods
    @objc private func dismissCurrentScreen() {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "Transaction Cancelled".localized, body: "Request is cancelled by other user".localized, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                NotificationCenter.default.post(name: Notification.Name("ReloadCICOAllScreens"), object: nil, userInfo: nil)
                self.navigationController?.popViewController(animated: true)
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    private func updateUI() {
        if let currentModel = currentSelectedCashDetails {
            self.addressLabel.text = currentModel.cashTransferMaster?.rechargeDestination?.source ?? ""
            if let rating = currentModel.customer?.rating.safelyWrappingString() {
                self.rateLabel.text = String(format: "%.2f", rating)
            }
            self.reviewLabel.text = "2 reviews"
            self.durationLabel.text = "1 min"
        } else {
            if let currentModel = currentPendingCashDetails {
                self.addressLabel.text = currentModel.rechargeDestination?.source ?? ""
                self.reviewLabel.text = "2 reviews"
            }
        }
        let width = UIScreen.main.bounds.width
        if width > 325 {
            self.addressLabel.font = UIFont(name: appFont, size: 16.0) ?? UIFont.systemFont(ofSize: 16)
        } else {
            self.addressLabel.font = UIFont(name: appFont, size: 14.0) ?? UIFont.systemFont(ofSize: 14)
        }
        self.agentNameLabel.text = currentSelectedCashDetails?.agent?.firstName ?? ""
        self.userNameLabel.text = currentSelectedCashDetails?.customer?.firstName ?? ""
        self.requestTypeLabel.text = (currentSelectedCashDetails?.cashTransferMaster?.cashType == 0) ? "Cash In Service".localized : "Cash Out Service".localized
    }
    
    private func addBackButton(navTitle: String) {
        let backButton = UIButton(type: .custom)
        backButton.setBackgroundImage(#imageLiteral(resourceName: "tabBarBack"), for: .normal)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        navigationItem.title = navTitle.localized
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white ]
        self.navigationController?.navigationBar.barTintColor = kYellowColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }

    private func updateStars(starcount: Int) {
        for i in 0...starcount {
            let view = self.ratingStackView.viewWithTag(i)
            if let button = view as? UIButton {
                button.setImage(UIImage(named: "receiptStarFilled.png"), for: .normal)
            }
        }
    }
    
    @objc func hideCancelButton() {
        self.getMasterDetails()
        DispatchQueue.main.async {
            self.cancelButton.isHidden = true
            //self.delegate?.dismissCICOUserMap()
        }
    }
    
    private func getMasterDetails() {
        if let reqID = currentSelectedCashDetails?.cashTransferMaster?.id {
            modelCashInCashOut.getMasterDetailRequest(id: "\"\(reqID)\"")
        }
    }
    
    @objc override func backAction() {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    private func loadMapwithCurrentLocation() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        mapView.delegate = self
    }
    
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if #available(iOS 11.0, *) {
                self.mapView.padding = UIEdgeInsets(top: self.view.safeAreaInsets.top, left: 0,
                                                    bottom: self.mapView.safeAreaInsets.bottom, right: 0)
            } else {
                // Fallback on earlier versions
            }
            UIView.animate(withDuration: 0.25) {
                if #available(iOS 11.0, *) {
                    self.pinImageVerticalConstraint.constant = (self.mapView.safeAreaInsets.top * 0.5)
                } else {
                    // Fallback on earlier versions
                }
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func fetchNearbyPlaces(coordinate: CLLocationCoordinate2D) {
        mapView.clear()
        //
        //        dataProvider.fetchPlacesNearCoordinate(coordinate, radius:searchRadius, types: searchedTypes) { places in
        //            places.forEach {
        //                let marker = PlaceMarker(place: $0)
        //                marker.map = self.mapView
        //            }
        //        }
    }
    
    //MARK: - Button Action Methods
    @IBAction func callButtonAction(_ sender: UIButton) {
        if let modeldata = self.currentSelectedCashDetails, let phoneNumber = modeldata.agent?.phoneNumber {
            let phoneNumberStr = self.getPhoneNumberByFormat(phoneNumber: phoneNumber)
            if let url = URL(string: "tel://\(phoneNumberStr)") {
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        } else {
            if let modeldata = self.currentPendingCashDetails {
                DispatchQueue.main.async {
                    alertViewObj.wrapAlert(title: nil, body: "Your request is not accepted. Do you want to cancel the request?".localized, img: #imageLiteral(resourceName: "alert-icon"))
                    alertViewObj.addAction(title: "Cancel".localized, style: .cancel , action: {
                    })
                    alertViewObj.addAction(title: "OK".localized, style: .target , action: {
                        let request = CancelCashTransferRequest(cancelledReason: "Not accepted", requestID: modeldata.id)
                        self.modelCashInCashOut.cancelCashTransfer(request: request)
                    })
                    alertViewObj.showAlert(controller: self)
                }
            }
        }
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        guard let vc = UIStoryboard(name: "CashInOutStoryboard", bundle: nil).instantiateViewController(withIdentifier:"CICOCancelAlertViewController") as? CICOCancelAlertViewController else { return }
        vc.view.frame = CGRect.zero
        vc.modalPresentationStyle = .overCurrentContext
        vc.delegate = self
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    //MARK: - Target Methods
    @objc func showRoute() {
        
        let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(String(startLat)),\(String(startLong))&destination=\(String(endLat)),\(String(endLong))&mode=driving&sensor=true&key=\(googleAPIKey)"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if(error != nil){
                println_debug("error")
            } else {
                do {
                    guard let dataRaw = data else { return }
                    let json = try JSONSerialization.jsonObject(with: dataRaw, options:.allowFragments) as! [String: AnyObject]
                    let routes = json["routes"] as! NSArray
                    self.mapView.clear()
                    OperationQueue.main.addOperation({
                        for route in routes
                        {
                            let routeOverviewPolyline:NSDictionary = (route as! NSDictionary).value(forKey: "overview_polyline") as! NSDictionary
                            let points = routeOverviewPolyline.object(forKey: "points")
                            let path = GMSPath.init(fromEncodedPath: points! as! String)
                            let polyline = GMSPolyline.init(path: path)
                            polyline.strokeWidth = 3
                            //polyline.strokeColor = .black
                            let bounds = GMSCoordinateBounds(path: path!)
                            self.mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))
                            
                            polyline.map = self.mapView
                            
                        }
                    })
                } catch let error as NSError {
                    println_debug("error:\(error)")
                }
            }
        }).resume()
    }
    
    fileprivate func showMapView(masterDetail: CashTransferRequestStatus?) {
        if let userMapStatusVC = UIStoryboard(name: "CICOMapAndPay", bundle: nil).instantiateViewController(withIdentifier: "CICOUserMapStatusViewController_ID") as? CICOUserMapStatusViewController {
            userMapStatusVC.currentSelectedCashDetails = modelCashInCashOut.masterDetail
            self.navigationController?.pushViewController(userMapStatusVC, animated: true)
            return
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}



//MARK: - CLLocationManagerDelegate
extension CICOUserMapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status != .denied, status != .notDetermined, status != .restricted else {
            return
        }
        locationManager.startUpdatingLocation()
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 17.0, bearing: 0, viewingAngle: 0)
        locationManager.stopUpdatingLocation()
        //fetchNearbyPlaces(coordinate: location.coordinate)
    }
}

// MARK: - GMSMapViewDelegate
extension CICOUserMapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if (gesture) {
            mapCenterPinImage.fadeIn(0.25)
            mapView.selectedMarker = nil
        }
    }
    
    //    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
    //        guard let placeMarker = marker as? PlaceMarker else {
    //            return nil
    //        }
    //        guard let infoView = UIView.viewFromNibName("MarkerInfoView") as? MarkerInfoView else {
    //            return nil
    //        }
    //
    //        infoView.nameLabel.text = placeMarker.place.name
    //        if let photo = placeMarker.place.photo {
    //            infoView.placePhoto.image = photo
    //        } else {
    //            infoView.placePhoto.image = UIImage(named: "generic")
    //        }
    //
    //        return infoView
    //    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        mapCenterPinImage.fadeOut(0.25)
        return false
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        mapCenterPinImage.fadeIn(0.25)
        mapView.selectedMarker = nil
        return false
    }
}
extension CICOUserMapViewController: CICOCancelRequestProtocol {
    func cancelButtonAction(reason: String) {
        let cancelReason = reason
        if let modeldata = self.currentSelectedCashDetails {
            let request = CancelCashTransferRequest(cancelledReason: cancelReason,
                                                    requestID: modeldata.cashTransferMaster?.requestID)
            self.modelCashInCashOut.cancelCashTransfer(request: request)
        } else {
            if let modeldata = self.currentPendingCashDetails {
                let request = CancelCashTransferRequest(cancelledReason: cancelReason,
                                                        requestID: modeldata.id)
                self.modelCashInCashOut.cancelCashTransfer(request: request)
            }
        }
    }
}

// MARK: - CashInOutModel Delegate
extension CICOUserMapViewController: CashInOutModelDelegate {
    func apiResult(message: String?, statusCode: String?, type: CashInCashOutModel.CashInCashOutApiType) {
        switch type {
        case .getMasterDetails:
            DispatchQueue.main.async {
                self.showMapView(masterDetail: self.modelCashInCashOut.masterDetail)
            }
        default:
            DispatchQueue.main.async {
                alertViewObj.wrapAlert(title: nil, body: "Successfully cancelled your request".localized, img: #imageLiteral(resourceName: "succ"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel , action: {
                    //NotificationCenter.default.post(name: Notification.Name("ReloadCICOAllScreens"), object: nil, userInfo: nil)
                    self.navigationController?.popToRootViewController(animated: true)
                })
                alertViewObj.showAlert(controller: self)
            }
        }
    }
    
    func handleCustomError(error: CustomError, type: CashInCashOutModel.CashInCashOutApiType) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
