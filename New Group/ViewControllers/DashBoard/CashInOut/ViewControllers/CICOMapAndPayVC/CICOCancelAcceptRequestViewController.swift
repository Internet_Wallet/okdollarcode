//
//  CICOCancelAcceptRequestViewController.swift
//  OK
//
//  Created by Kethan on 8/22/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import GoogleMaps

class CICOCancelAcceptRequestViewController: CashInOutBaseViewController, UIGestureRecognizerDelegate {
    //MARK: - Outlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var secondsLabel: UILabel!
    @IBOutlet weak var secondsTxtLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton! {
        didSet {
            self.acceptButton.setTitle(self.acceptButton.titleLabel?.text, for: .normal)
        }
    }
    @IBOutlet weak var cancelButton: UIButton! {
        didSet {
            self.cancelButton.setTitle(self.cancelButton.titleLabel?.text, for: .normal)
        }
    }
    @IBOutlet weak var roundImage: UIImageView! {
        didSet {
            self.roundImage.layer.masksToBounds = true
            self.roundImage.layer.cornerRadius = self.roundImage.frame.size.width / 2.0
        }
    }
    @IBOutlet weak var acceptImageView: UIImageView!
    @IBOutlet weak var rejectImageView: UIImageView!
    @IBOutlet weak var constraintMidXRoundImage: NSLayoutConstraint!
    let marker = GMSMarker()
    //MARK: - Properties
    private let locationManager = CLLocationManager()
    var myTimer: Timer? = nil
    var countdown = 0
    var userInfoDict = Dictionary<AnyHashable, Any>()
    private lazy var modelCashInCashOut: CashInCashOutModel = {
        return CashInCashOutModel()
    }()
    
    @IBOutlet weak var multipleReqTableView: UITableView!
    var acceptRequestArray = [CashTransferRequestDetail?]()
    var selectedRequest = 0

    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        modelCashInCashOut.cashInCashOutDelegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(dismissCurrentScreen), name: Notification.Name("DismissCICOCancelAcceptScreen"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(navigateToCICOCancelAccept(_:)), name: Notification.Name("PushToCICOCancelAccept"), object: nil)
        self.getAgentDetails()
        self.animateImageView()
        mapView.settings.scrollGestures = false
        mapView.settings.zoomGestures = false
        mapView.settings.rotateGestures = false
        mapView.settings.tiltGestures = false
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        mapView.delegate = self
        multipleReqTableView.tableFooterView = UIView()
        self.loadMapwithCurrentLocation()
    }
    
     @objc func navigateToCICOCancelAccept(_ notification: Notification) {
        guard let userInfoDct = notification.userInfo else { return }
        self.userInfoDict = userInfoDct
        countdown = 20
        if self.acceptRequestArray.count < 3 {
            self.getAgentDetails()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
        countdown = 20
        self.startTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Methods
    fileprivate func animateImageView() {
        self.acceptImageView.animationImages = [#imageLiteral(resourceName: "cicoAcceptRequestLight"), #imageLiteral(resourceName: "cicoAcceptRequestDark")]
        self.rejectImageView.animationImages = [#imageLiteral(resourceName: "cicoRejectRequestLight"), #imageLiteral(resourceName: "cicoRejectRequestDark")]
        self.acceptImageView.animationDuration = 0.5
        self.rejectImageView.animationDuration = 0.5
        self.acceptImageView.startAnimating()
        self.rejectImageView.startAnimating()
    }
    
    deinit {
        self.acceptImageView.stopAnimating()
        self.rejectImageView.stopAnimating()
        NotificationCenter.default.removeObserver(self)
    }
    
    fileprivate func getAgentDetails() {
        println_debug(userInfoDict["value"] as? String ?? "No values in dict *****")
        if let dict = OKBaseController.convertToDictionary(text: userInfoDict["value"] as? String ?? "") {
            //            let request = PushReceivedRequest(pushNotificationKey: "NewCashTransferRequest", tripRequestId: dict["RequestId"] as? String ?? "", agentId: userInfoDict["userid"] as? String ?? "")
            //            modelCashInCashOut.acknowledgePushReceived(request: request)
            if let reqId = dict["RequestId"] as? String {
                modelCashInCashOut.getRequestDetail(id: "\"\(reqId)\"")
            }
        }
    }
    
    private func startTimer() {
        countDownTick()
        secondsLabel.text = "00 :\(countdown)"
    }
    
    func countDownTick() {
        myTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] _ in
            self?.countdown = (self?.countdown)! - 1
            if (self?.countdown == 0) {
                self?.myTimer!.invalidate()
                self?.myTimer=nil
                self?.insertRequestToDatabase()
            }
            if let count = self?.countdown, count < 10 {
                self?.secondsLabel.text = "00 : 0\(count)"
            } else {
                if self?.countdown != nil {
                    self?.secondsLabel.text = "00 : \(self?.countdown ?? 0)"
                }
            }
        }
        
    }
    
    fileprivate func insertRequestToDatabase() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func showPendingRequestScreen() {
        if let vc = UIStoryboard(name: "CashInOutStoryboard", bundle: nil).instantiateViewController(withIdentifier: cashInOutViewControllers.viewControllers.cioTransHomeVC) as? CIOTransactionHomeViewController {
            vc.fromCancelAcceptScreen = "CancelAcceptScreen"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    fileprivate func updateUI() {
        self.loadMapwithCurrentLocation()
    }
    
    //MARK:- MapView Delegate
    private func loadMapwithCurrentLocation() {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: modelCashInCashOut.cashTransferRequestDetail?.cashTransferRequest?.rechargeDestination?.latitude ?? 0.0, longitude: modelCashInCashOut.cashTransferRequestDetail?.cashTransferRequest?.rechargeDestination?.longitude ?? 0.0)
        println_debug("Lat Long")
        println_debug(modelCashInCashOut.cashTransferRequestDetail?.cashTransferRequest?.rechargeDestination?.latitude)
        println_debug(modelCashInCashOut.cashTransferRequestDetail?.cashTransferRequest?.rechargeDestination?.longitude)
        marker.icon = #imageLiteral(resourceName: "cicoLocation")
        marker.map = mapView
    }
    
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if #available(iOS 11.0, *) {
                self.mapView.padding = UIEdgeInsets(top: self.view.safeAreaInsets.top, left: 0,
                                                    bottom: self.mapView.safeAreaInsets.bottom, right: 0)
            } else {
                // Fallback on earlier versions
            }
            UIView.animate(withDuration: 0.25) {
                if #available(iOS 11.0, *) {
                    //self.pinImageVerticalConstraint.constant = (self.mapView.safeAreaInsets.top * 0.5)
                } else {
                    // Fallback on earlier versions
                }
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func dismissCurrentScreen() {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK: - Button Action Methods
    func acceptButtonAction() {
        if let detail = self.acceptRequestArray[selectedRequest] {
            let location = CICOLocation(type: "POINT", coordinates: [detail.cashTransferRequest?.rechargeDestination?.longitude ?? 0.0, detail.cashTransferRequest?.rechargeDestination?.latitude ?? 0.0], angle: nil, time: nil, createdTime: nil, accuracy: detail.cashTransferRequest?.rechargeDestination?.accuracy ?? 0, name: detail.cashTransferRequest?.rechargeDestination?.name ?? "", source: detail.cashTransferRequest?.rechargeDestination?.source ?? "", latitude: detail.cashTransferRequest?.rechargeDestination?.latitude ?? 0.0, longitude: detail.cashTransferRequest?.rechargeDestination?.longitude ?? 0.0)
            let request = CashTransferAcceptRequest(cashTransferRequestID: detail.cashTransferRequest?.id ?? "", cashTransferRequest: nil, agentID: detail.cashTransferRequest?.agentRequests?.first?.agentID ?? "", agentLocation: location, agentType: UserDefaults.standard.bool(forKey: "MerchantCashMobStationayStatus") ? 1 : 0)
            modelCashInCashOut.acceptCashTransferRequest(request: request)
        }
    }

    func cancelButtonAction() {
        if let requesterId = self.acceptRequestArray[selectedRequest]?.cashTransferRequest?.id {
            let request = CashTransferRejectRequest(cashTransferRequestID: requesterId)
            modelCashInCashOut.rejectCashTransferRequest(request: request)
        }
    }
    
    @IBAction func handlePan(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            println_debug("began")
        case .changed:
            //let velocity = recognizer.velocity(in: self.view)
            if self.view.frame.contains(recognizer.location(in: self.view)) {
                let translation = recognizer.translation(in: self.view)
                println_debug(translation.x)
                if let view = recognizer.view {
                    view.center = CGPoint(x: view.center.x + translation.x,
                                          y: view.center.y)
//                    constraintMidXRoundImage.constant = translation.x
//                    self.view.layoutIfNeeded()
                    if translation.x < -12 {
                        self.cancelButtonAction()
                        roundImage.isUserInteractionEnabled = false
                    } else if translation.x > 12 {
                        self.acceptButtonAction()
                        roundImage.isUserInteractionEnabled = false
                    }
                }
                recognizer.setTranslation(CGPoint.zero, in: self.view)
            }
        case .ended:
            println_debug("Ended")
//            roundImage.isUserInteractionEnabled = false
        default:
            break
        }
    }
}

// MARK: - CashInOutModel Delegate
extension CICOCancelAcceptRequestViewController: CashInOutModelDelegate {
    func apiResult(message: String?, statusCode: String?, type: CashInCashOutModel.CashInCashOutApiType) {
        PTLoader.shared.hide()
        DispatchQueue.main.async {
            if type == .getRequestDetails {
                DispatchQueue.main.async {
                    self.acceptRequestArray.append(self.modelCashInCashOut.cashTransferRequestDetail)
                    self.updateUI()
                    self.multipleReqTableView.reloadData()
                }
            } else if type == .acceptCashTransferReq {
                self.myTimer?.invalidate()
                self.myTimer = nil
                self.countdown = -1
                self.showPendingRequestScreen()
            } else if  type == .rejectCashTransferReq {
                self.myTimer?.invalidate()
                self.myTimer = nil
                self.countdown = -1
                DispatchQueue.main.async {
                    alertViewObj.wrapAlert(title: nil, body: "This request is rejected by you".localized, img: #imageLiteral(resourceName: "succ"))
                    alertViewObj.addAction(title: "OK".localized, style: .cancel , action: {
                        self.dismiss(animated: true, completion: nil)
                    })
                    alertViewObj.showAlert(controller: self)
                }
                
            }
        }
    }
    
    func handleCustomError(error: CustomError, type: CashInCashOutModel.CashInCashOutApiType) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}


//MARK: - CLLocationManagerDelegate
extension CICOCancelAcceptRequestViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status != .denied, status != .notDetermined, status != .restricted else {
            return
        }
        locationManager.startUpdatingLocation()
        mapView.isMyLocationEnabled = false
        mapView.settings.myLocationButton = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 17.0, bearing: 0, viewingAngle: 0)
    }
}

// MARK: - GMSMapViewDelegate
extension CICOCancelAcceptRequestViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if (gesture) {
            //mapCenterPinImage.fadeIn(0.25)
            mapView.selectedMarker = self.marker
        }
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
    }
    
        func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
            guard let placeMarker = marker as? PlaceMarker else {
                return nil
            }
//            guard let infoView = UIView.viewFromNibName("MarkerInfoView") as? MarkerInfoView else {
//                return nil
//            }
//
//            infoView.nameLabel.text = placeMarker.place.name
//            if let photo = placeMarker.place.photo {
//                infoView.placePhoto.image = photo
//            } else {
//                infoView.placePhoto.image = UIImage(named: "generic")
//            }
//
//            return infoView
            println_debug(placeMarker.place.address)
            return UIView()
        }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        //mapCenterPinImage.fadeOut(0.25)
        return false
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        //mapCenterPinImage.fadeIn(0.25)
        mapView.selectedMarker = nil
        return false
    }
}


extension CICOCancelAcceptRequestViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.acceptRequestArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CancelAcceptRequestCell", for: indexPath) as? CICOCancelAcceptTableViewCell
        cell?.wrapData(model: self.acceptRequestArray[indexPath.section])
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedRequest = indexPath.section
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5.0
    }
}


class CICOCancelAcceptTableViewCell: UITableViewCell {
    @IBOutlet weak var reqForLabelVal: UILabel!
    @IBOutlet weak var reqAmountLabelVal: UILabel!
    @IBOutlet weak var addressLabelVal: UILabel!
    
    override func awakeFromNib() {
        
    }
    
    func wrapData(model: CashTransferRequestDetail?)   {
        if let detail = model {
            if let cashType = detail.cashTransferRequest?.cashType {
                reqForLabelVal.text = (cashType == 1) ? "CASH OUT" : "CASH IN"
            }
            reqAmountLabelVal.text = String(detail.cashTransferRequest?.rechargeAmount ?? 0)
            addressLabelVal.text = detail.cashTransferRequest?.rechargeDestination?.source ?? ""
        }
    }
}
