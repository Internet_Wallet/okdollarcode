//
//  CICOUserMapStatusViewController.swift
//  OK
//
//  Created by Kethan on 8/22/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import GoogleMaps

class CICOUserMapStatusViewController: CashInOutBaseViewController {
    //MARK: - Outlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet private weak var mapCenterPinImage: UIImageView! {
        didSet {
            self.mapCenterPinImage.isHidden = true
        }
    }
    @IBOutlet private weak var pinImageVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var downUpButton: UIButton!
    @IBOutlet weak var addressLabel1: UILabel!
    @IBOutlet weak var addressLabel2: UILabel!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var offerAcceptedLabel: UILabel! {
        didSet {
            self.offerAcceptedLabel.text = self.offerAcceptedLabel.text?.localized
        }
    }
    @IBOutlet weak var agentStartedLabel: UILabel! {
        didSet {
            self.agentStartedLabel.text = self.agentStartedLabel.text?.localized
        }
    }
    @IBOutlet weak var arrivedLabel: UILabel! {
        didSet {
            self.arrivedLabel.text = self.arrivedLabel.text?.localized
        }
    }
    @IBOutlet weak var arrivedImageView: UIImageView!
    @IBOutlet weak var statusStackView: UIStackView! {
        didSet {
            self.statusStackView.isHidden = false
        }
    }
    //MARK: - Properties
    private lazy var modelCashInCashOut: CashInCashOutModel = {
        return CashInCashOutModel()
    }()
    var endPos: GMSMarker?
    var startMarker: GMSMarker?
    private let locationManager = CLLocationManager()
    var startLat = 0.0
    var startLong = 0.0
    var endLat = 0.0
    var endLong  = 0.0
    var currentSelectedCashDetails: CashTransferRequestStatus?
    var currentPendingCashDetails: PendingCashTransferResponse?
    var fromNotificationScreen: String?
    var infoView: UIView?
    
    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if let currentModel = currentSelectedCashDetails {
            self.startLat = currentModel.cashTransferMaster?.startLocation?.latitude ?? 0.0
            self.startLong = currentModel.cashTransferMaster?.startLocation?.longitude ?? 0.0
            self.endLat = currentModel.cashTransferMaster?.rechargeDestination?.latitude ?? 0.0
            self.endLong = currentModel.cashTransferMaster?.rechargeDestination?.longitude ?? 0.0
        } else {
            
            self.startLat = Double(geoLocManager.currentLatitude) ?? 0.0
            self.startLong = Double(geoLocManager.currentLongitude) ?? 0.0
            self.endLat = currentPendingCashDetails?.rechargeDestination?.latitude ?? 0.0
            self.endLong = currentPendingCashDetails?.rechargeDestination?.longitude ?? 0.0
        }
        self.updateUI()
        modelCashInCashOut.cashInCashOutDelegate = self
        self.getDistanceAndDuration()
        self.mapView.settings.zoomGestures = true
        self.addBackButton(navTitle: "On Trip")
        if let status = currentSelectedCashDetails?.cashTransferMaster?.status {
            if status == 3 || status == 4 {
                arrivedLabel.textColor = UIColor.init(hex: "727272")
                arrivedImageView.image = #imageLiteral(resourceName: "cicoArrivedDark")
            }
        }
        
        self.modelCashInCashOut.cashInCashOutDelegate = self
        self.setLocationMapDelegate()
        let camera = GMSCameraPosition.camera(withLatitude: startLat, longitude: startLong, zoom: 15.0)
        self.mapView.camera = camera
        self.mapView.setMinZoom(5, maxZoom: 18)
        self.mapView.settings.zoomGestures = true
        self.mapView.settings.myLocationButton = true
        NotificationCenter.default.addObserver(self, selector: #selector(updateUserMap(_:)), name: Notification.Name("PushToCICOMapUpdate"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CICOPopToRoot), name: Notification.Name("CICOPopToRootVC"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CICOPopToRoot), name: Notification.Name("CICOPopToroot"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateArraivedLabel), name: Notification.Name("ShowCICOConfirmationScreen"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dismissCurrentScreen), name: Notification.Name("ShowCICOCancelAlert"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ConfirmPop), name: Notification.Name("CICOConfirmPop"), object: nil)

    }
    
    @objc func CICOPopToRoot() {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc private func dismissCurrentScreen() {
        DispatchQueue.main.async {
            alertViewObj.wrapAlert(title: "Transaction Cancelled".localized, body: "Request is cancelled by other user".localized, img: nil)
            alertViewObj.addAction(title: "OK".localized, style: .cancel) {
                NotificationCenter.default.post(name: Notification.Name("ReloadCICOAllScreens"), object: nil, userInfo: nil)
                self.navigationController?.popViewController(animated: true)
            }
            alertViewObj.showAlert(controller: self)
        }
    }
    
    @objc func ConfirmPop() {
        DispatchQueue.main.async {
            if let navControllers = self.navigationController {
                let viewControllers: [UIViewController] = navControllers.viewControllers
                for aViewController in viewControllers {
                    if aViewController is CIOTransactionHomeViewController {
                        navControllers.popToViewController(aViewController, animated: true)
                    }
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if let name = currentSelectedCashDetails?.agent?.businessName {
            self.title = name
        } else {
            self.title = currentSelectedCashDetails?.agent?.firstName ?? ""
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.showRoute()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Methods
    deinit {
        locationManager.stopUpdatingLocation()
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func updateArraivedLabel() {
        DispatchQueue.main.async {
            if let cashType = self.currentSelectedCashDetails?.cashTransferMaster?.cashType, cashType == 0 {
                self.arrivedLabel.textColor = UIColor.init(hex: "727272")
                self.arrivedImageView.image = #imageLiteral(resourceName: "cicoArrivedDark")
                self.showCICOToast(message: "Receiver arrived. Please wait for receiver to complete payment".localized)
            }
        }
    }
    
    @objc func updateUserMap(_ notification: Notification) {
        guard let userInfoVal = notification.userInfo else { return }
        if let userInfoDict = OKBaseController.convertToDictionary(text: userInfoVal["value"] as? String ?? "") {
            if let locationDict = userInfoDict["UserLocation"] as? Dictionary<String, Any> {
                guard let lat = locationDict["Latitude"] as? Double , let long = locationDict["Longitude"] as? Double else { return }
                startLong = long
                startLat = lat
                self.showRoute()
                self.getDistanceAndDuration()
            }
        }
    }
    
    private func updateUI() {
        if let customerAddress = currentSelectedCashDetails?.cashTransferMaster?.rechargeDestination?.source {
            let addressArray = customerAddress.components(separatedBy: ",")
            if (addressArray.count) > 0 {
                self.addressLabel1.text = addressArray[0]
            }
            self.addressLabel2.text = customerAddress
        }
        let width = UIScreen.main.bounds.width
        if width > 325 {
            self.addressLabel1.font = UIFont(name: appFont, size: 16.0) ?? UIFont.systemFont(ofSize: 16)
            self.addressLabel2.font = UIFont(name: appFont, size: 16.0) ?? UIFont.systemFont(ofSize: 16)
        } else {
            self.addressLabel1.font = UIFont(name: appFont, size: 14.0) ?? UIFont.systemFont(ofSize: 14)
            self.addressLabel2.font = UIFont(name: appFont, size: 14.0) ?? UIFont.systemFont(ofSize: 14)
        }
    }
    
    func updateInfoview() {
        DispatchQueue.main.async {
            guard let legs = self.modelCashInCashOut.getTimeAndDistance?.routes?.first?.legs.first else { return }
            guard let infoVC = self.infoView else { return }
            for (_,infoSubView) in infoVC.subviews.enumerated() {
                if let label = infoSubView as? UILabel {
                    if label.tag == 27 {
                        if let array = legs.distance?.text?.components(separatedBy: " ") {
                            if array.count > 1, let str = array.last, str == "ft" {
                                label.text = "1.0 Mi"
                            } else {
                                label.text = legs.distance?.text ?? ""
                            }
                        } else {
                            label.text = legs.distance?.text ?? ""
                        }
                    } else if label.tag == 28 {
                        label.text = legs.duration?.text ?? ""
                    }
                }
            }
        }
    }
    
    private func addBackButton(navTitle: String) {
        let backButton = UIButton(type: .custom)
        backButton.setBackgroundImage(#imageLiteral(resourceName: "tabBarBack"), for: .normal)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        navigationItem.title = navTitle.localized
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white ]
        self.navigationController?.navigationBar.barTintColor = kYellowColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    private func setLocationMapDelegate() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        mapView.delegate = self
    }
    
    
    //MARK: - Target Methods
    @objc override func backAction() {
        DispatchQueue.main.async {
            if let str = self.fromNotificationScreen, str.count > 0 {
                self.dismiss(animated: true, completion: nil)
            } else {
                NotificationCenter.default.post(name: Notification.Name("ReloadCICOAllScreens"), object: nil, userInfo: nil)
                if let navControllers = self.navigationController {
                    let viewControllers: [UIViewController] = navControllers.viewControllers
                    for aViewController in viewControllers {
                        if aViewController is CIOTransactionHomeViewController {
                            navControllers.popToViewController(aViewController, animated: true)
                        }
                    }
                }
            }
        }
    }
    
    private func getDistanceAndDuration() {
        modelCashInCashOut.getDistanceAndTime(startLat: self.startLat, startLong: self.startLong, endLat: self.endLat, endLong: self.endLong)
    }
    
    private func showStartEndMarker() {
        showStartMarker()
        showEndMarker()
    }
    
    private func showStartMarker() {
        let startPosition = CLLocationCoordinate2D(latitude: startLat, longitude: startLong)
        startMarker = GMSMarker(position: startPosition)
        startMarker?.icon = #imageLiteral(resourceName: "ic_location_pin")
        startMarker?.map = mapView
        //startPos?.tracksViewChanges = true
        //startPos?.tracksInfoWindowChanges = true
        CATransaction.commit()
    }
    
    private func showEndMarker() {
        let endPosition = CLLocationCoordinate2D(latitude: endLat, longitude: endLong)
        let endPos = GMSMarker(position: endPosition)
        //endPos.icon = #imageLiteral(resourceName: "cicoBikeInMap")
        endPos.map = mapView
        if let infoVw = self.infoView {
            endPos.iconView = infoVw
        } else {
            self.infoView = showInfoView()
            endPos.iconView = self.infoView
        }
        endPos.infoWindowAnchor = CGPoint(x: 0.5, y: 0.5)
        //endPos.tracksViewChanges = true
        //endPos.tracksInfoWindowChanges = true
    }
    
    private func showInfoView() -> UIView {
        let infoVC = UIView(frame: CGRect(x: self.view.frame.size.width / 2 - 45, y: self.view.frame.size.height / 2, width: 70.0, height: 81.0))
        infoVC.backgroundColor = UIColor.clear
        let distanceLbl = UILabel(frame: CGRect(x: 0.0, y: 0, width: 70.0, height: 25.0))
        //distanceLbl.text = " Mi"
        distanceLbl.tag = 27
        distanceLbl.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        distanceLbl.textAlignment = .center
        distanceLbl.textColor = UIColor.white
        distanceLbl.font = UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: 17.0)
        infoVC.addSubview(distanceLbl)
        let durationLbl = UILabel(frame: CGRect(x: 0.0, y: 25.0, width: 70.0, height: 25.0))
        //durationLbl.text = " Min"
        durationLbl.textAlignment = .center
        durationLbl.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        durationLbl.tag = 28
        durationLbl.textColor = UIColor.white
        durationLbl.font = UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: 17.0)
        infoVC.addSubview(durationLbl)
        let image = UIImageView(frame: CGRect(x: 18.0, y: 50.0, width: 30.0, height: 30.0))
        image.image = #imageLiteral(resourceName: "cicoBikeInMap")
        infoVC.addSubview(image)
        if let routes = self.modelCashInCashOut.getTimeAndDistance?.routes {
            if routes.count > 0 {
                let legs = routes[0].legs
                if legs.count > 0 {
                    if let dist = legs[0].distance?.text {
                        distanceLbl.text = dist
                    }
                    if let dur = legs[0].duration?.text {
                        durationLbl.text = dur
                    }
                }
            }
        }
        return infoVC
    }
    
    private func updateMarker() {
        endPos?.position = CLLocationCoordinate2D(latitude: endLat, longitude: endLong)
        endPos?.map = self.mapView
        endPos?.icon = #imageLiteral(resourceName: "cicoBikeInMap")
    }
    
    
    private func showRoute() {
        let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(String(startLat)),\(String(startLong))&destination=\(String(endLat)),\(String(endLong))&mode=driving&sensor=true&key=\(googleAPIKey)"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if (error != nil) {
                println_debug("error")
            } else {
                do {
                    guard let dataRaw = data else { return }
                    let json = try JSONSerialization.jsonObject(with: dataRaw,
                                                                options:.allowFragments) as! [String: AnyObject]
                    let routes = json["routes"] as! NSArray
                    self.mapView.clear()
                    OperationQueue.main.addOperation({
                        for route in routes
                        {
                            if let routeOverviewPolyline = (route as! NSDictionary).value(forKey: "overview_polyline") as? NSDictionary {
                                let points = routeOverviewPolyline.object(forKey: "points")
                                let path = GMSMutablePath.init(fromEncodedPath: points! as! String)
                                let polyline = GMSPolyline.init(path: path)
                                polyline.strokeWidth = 3
                                let bounds = GMSCoordinateBounds(path: path!)
                                self.mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))
                                polyline.map = self.mapView
                            }
                        }
                        self.getDistanceAndDuration()
                        self.showStartEndMarker()
                    })
                } catch let error as NSError{
                    println_debug("error:\(error)")
                }
            }
        }).resume()
    }
    
    //MARK: - Button Action Methods
    @IBAction func hideAnsShowView(_ sender: UIButton) {
        if heightConstraint.constant == 35.0 {
            self.statusStackView.isHidden = false
            stackViewHeightConstraint.constant = 185.0
            heightConstraint.constant = 220.0
            self.downUpButton.setImage(#imageLiteral(resourceName: "cicoDownArrowDouble"), for: .normal)
        } else {
            self.statusStackView.isHidden = true
            stackViewHeightConstraint.constant = 0.0
            heightConstraint.constant = 35.0
            self.downUpButton.setImage(#imageLiteral(resourceName: "cicoUpArrowDouble"), for: .normal)
        }
    }
}

//MARK: - CLLocationManagerDelegate
extension CICOUserMapStatusViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status != .denied, status != .notDetermined, status != .restricted else {
            return
        }
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        startLat = location.coordinate.latitude
        startLong = location.coordinate.longitude
    }
}

//MARK: - GMSMapViewDelegate
extension CICOUserMapStatusViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        mapView.animate(toLocation: CLLocationCoordinate2D(latitude: startLat, longitude: startLong))
        return false
    }
}


// MARK: - CashInOutModel Delegate
extension CICOUserMapStatusViewController: CashInOutModelDelegate {
    func apiResult(message: String?, statusCode: String?, type: CashInCashOutModel.CashInCashOutApiType) {
        switch type {
        case .getDistTimeBtwnLoc:
            self.updateInfoview()
        default:
            break
        }
    }
    
    func handleCustomError(error: CustomError, type: CashInCashOutModel.CashInCashOutApiType) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error, \(error.rawValue)")
        }
    }
}
