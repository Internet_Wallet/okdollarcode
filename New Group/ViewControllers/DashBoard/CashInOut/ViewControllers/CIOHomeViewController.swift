//
//  CashInOutHomeViewController.swift
//  OK
//
//  Created by Kethan on 8/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import MapKit
import WebKit

class CIOHomeViewController: CashInOutBaseViewController, UIPopoverPresentationControllerDelegate, UITextFieldDelegate {
    
    //MARK: - Outlets
    @IBOutlet weak var searchBtn : UIButton! {
        didSet {
            self.searchBtn.isHidden = true
            self.searchBtn.setTitle(self.searchBtn.titleLabel?.text?.localized, for: .normal)
        }
    }
    
    @IBOutlet weak var commisionPercentageLabel: UILabel!{
        didSet {
            self.commisionPercentageLabel.text = "Commission Percent".localized
            self.commisionPercentageLabel.font = UIFont(name : appFont, size : 14.0)
        }
    }
    
    @IBOutlet weak var amtClearBtn : UIButton! {
        didSet {
            self.amtClearBtn.isHidden = true
        }
    }
    @IBOutlet weak var mmkLabel : UILabel! {
        didSet {
            self.mmkLabel.text = self.mmkLabel.text?.localized
        }
    }
    @IBOutlet weak var addressLabel1 : UILabel!
    {
      didSet
      {
        addressLabel1.text = "Please select your location".localized
        addressLabel1.font = UIFont(name : appFont, size : 14.0)
      }
  }
    @IBOutlet weak var addressLabel2 : UILabel!
      {
      didSet
      {
        addressLabel2.text = "Please select your location".localized
        addressLabel2.font = UIFont(name : appFont, size : 14.0)
      }
  }
    
    @IBOutlet weak var enterCashLabel : UILabel!
    
    @IBOutlet var amountTxt : UITextField! {
        didSet {
            self.amountTxt.placeholder = self.amountTxt.placeholder?.localized
            self.amountTxt.font = UIFont(name : appFont, size : 14.0)
        }
    }
    
    var navigationFromPayto: UINavigationController?
    
    @IBOutlet weak var labelPercentCommision: UILabel! {
        didSet {
            self.labelPercentCommision.attributedText = NSAttributedString(string: "Click here to view commission percentage".localized, attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue])
            self.labelPercentCommision.font = UIFont(name : appFont, size : 14.0)
        }
    }
    
    @IBOutlet var percentView: UIView!
    var webView = WKWebView()
    
    @IBOutlet weak var heightSearchBtn: NSLayoutConstraint!
    var headerNewTitle : String = userDef.value(forKey: "lastLoginKey") as? String ?? ""

    //MARK: - Properties
    let geoLocManager    = GeoLocationManager.shared
    fileprivate var currentAddressName = ""
    private lazy var modelCashInCashOut: CashInCashOutModel = {
        return CashInCashOutModel()
    }()
    var searchView: UIView {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 50))
        let btn = UIButton.init(frame: view.frame)
        btn.backgroundColor = kYellowColor
        btn.setTitle("Search".localized, for: .normal)
        btn.setTitleColor(kBlueColor, for: .normal)
        if let myFont = UIFont(name: appFont, size: 18) {
            btn.titleLabel?.font =  myFont
        }
        btn.addTarget(self, action: #selector(searchButtonAction(_:)), for: UIControl.Event.touchUpInside)
        view.addSubview(btn)
        return view
    }
    var screenName = "Cash In"
    
    //MARK: - Views life cycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        percentView.layer.borderColor = UIColor.clear.cgColor
        commisionPercentageLabel.layer.borderColor = UIColor.clear.cgColor
        initialize()
        setNavigation()
        self.setUpPercentView(height:409)
        NotificationCenter.default.addObserver(self, selector: #selector(backAction), name: NSNotification.Name(rawValue: "DismissCashIOView"), object: nil )
        NotificationCenter.default.addObserver(self, selector: #selector(dismissCurrentScreen), name: Notification.Name("CICOPopToroot"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.updateUserLocation()
        geoLocManager.startUpdateLocation()
        progressViewObj.showProgressView()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.startGeoLocation()
            self.modelCashInCashOut.updateAuthToken(fcmToken: UserDefaults.standard.string(forKey: "FCMToken") ?? "")
        }
    }
    
    
    func setUpPercentView(height: Int) {
       webView.frame =  CGRect(x: percentView.frame.origin.x, y: 0 , width: self.view.frame.width, height: CGFloat(height))
       webView.navigationDelegate = self
        self.percentView.addSubview(webView)
        
        var urlSuffix = Url.cashInOutCommisionUrl
        if let currentStr = UserDefaults.standard.string(forKey: "currentLanguage") {
            if currentStr == "my" {
                urlSuffix = Url.cashInOutCommisionUrlMY
            } else if currentStr == "uni" {
                 urlSuffix = Url.cashInOutCommisionUrlUN
            }
        }
        let url = getUrl(urlStr: urlSuffix, serverType: .cashInCashOutCommisionUrl)
        let urlReq = URLRequest(url: url)
        webView.load(urlReq)
        PTLoader.shared.show()
    }
    
    fileprivate func setWebViewHeight(height: Int){
        DispatchQueue.main.async {
            self.webView.frame.size.height = CGFloat(height)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Methods
    private func initialize() {
        self.amountTxt.placeholder = (screenName == "Cash In") ? "Enter Cash In Amount".localized : "Enter Cash Out Amount".localized
        self.addBackButton(navTitle: "")
        amountTxt.inputAccessoryView = self.searchView
        amountTxt.inputAccessoryView?.isHidden = true
        amountTxt.addTarget(self, action: #selector(CIOHomeViewController.textFieldDidChange(_:)),
                                  for: UIControl.Event.editingChanged)
        amountTxt.delegate = self
        self.addRightBarButton()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideMenuView))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc private func dismissCurrentScreen() {
        DispatchQueue.main.async {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    private func startGeoLocation() {
        self.getCurrentAddress { (curAddress) in
            PTLoader.shared.hide()
            if curAddress == "" {
                
            } else {
                DispatchQueue.main.async {
                    self.currentAddressName = curAddress
//                    let curAddressArr = curAddress.components(separatedBy: ",")
//                    if curAddressArr.count > 0 {
                        //self.addressLabel1.text = curAddressArr[0]
//                    } else {
                        //self.addressLabel1.text = curAddress
//                    }
                    //self.addressLabel2.text = curAddress
                }
                if curAddress.count > 0  {
                    currentAddressGlobal = curAddress
                    self.updateUserLocation()
                }
            }
        }
    }
    
    private func updateUserLocation() {
        if currentAddressGlobal != "" {
            let userId = UserDefaults.standard.value(forKey: "CICOUserID") as? String ?? ""
            let loc = self.getLocationDetailsForApi()
            let request = UpdateUserLocReq(userID: userId, userType: self.getAgentType(), location: loc)
            modelCashInCashOut.updateUserLocation(request: request)
        }
    }
    
    private func addRightBarButton() {
//        let moreButton = UIButton(type: .custom)
//        moreButton.setBackgroundImage(#imageLiteral(resourceName: "cicoMore"), for: .normal)
//        moreButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
//        moreButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
//        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: moreButton)
//        moreButton.addTarget(self, action: #selector(currentMenuAction), for: .touchUpInside)
        
        let moreButton = UIButton(type: .custom)
        moreButton.setBackgroundImage(UIImage(named: "chatMore") ,for: .normal)
        moreButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        moreButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: moreButton)
        moreButton.addTarget(self, action: #selector(currentMenuAction), for: .touchUpInside)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    private func addBackButton(navTitle: String) {
        let backButton = UIButton(type: .custom)
        backButton.setBackgroundImage(#imageLiteral(resourceName: "tabBarBack"), for: .normal)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        navigationItem.title = navTitle.localized
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white ]
        self.navigationController?.navigationBar.barTintColor = kYellowColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    func getDigitDisplay1(_ digit: String) -> String {
        var FormateStr: String
        let mystring = digit.replacingOccurrences(of: ",", with: "")
        let number = NSDecimalNumber(string: mystring)
        if screenName == "Cash In", number > 499, self.currentAddressName != "" {
            self.amountTxt.inputAccessoryView?.isHidden = false
            self.searchBtn.setTitleColor(kBlueColor, for: .normal)
            self.searchBtn.isHidden = false
            self.percentView.isHidden = true
            self.commisionPercentageLabel.isHidden = true
            self.searchBtn.backgroundColor = kYellowColor
            
        } else if screenName == "Cash Out", number > 999, self.currentAddressName != "" {
            self.amountTxt.inputAccessoryView?.isHidden = false
            self.searchBtn.setTitleColor(kBlueColor, for: .normal)
            self.searchBtn.isHidden = false
            self.percentView.isHidden = true
            self.commisionPercentageLabel.isHidden = true
            self.searchBtn.backgroundColor = kYellowColor
        } else {
            self.amountTxt.inputAccessoryView?.isHidden = true
            self.searchBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.searchBtn.isHidden = true
            self.percentView.isHidden = false
            self.commisionPercentageLabel.isHidden = false
            self.searchBtn.backgroundColor = UIColor.lightGray
            self.setWebViewHeight(height:250)
        }
        
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        formatter.usesGroupingSeparator = true
        FormateStr = formatter.string(from: number)!
        if FormateStr == "NaN"{
            FormateStr = ""
        }
        return FormateStr
    }
    
    //MARK: - Target methods
    @objc func hideMenuView() {
        //this will work for tap gesture on screen
        self.setWebViewHeight(height:409)
        self.view.endEditing(true)
    }
    
    @objc func currentMenuAction(_ sender : UIBarButtonItem) {
        let popoverContent = self.storyboard?.instantiateViewController(withIdentifier: "CIOTransactionListPopover_ID") as? CIOTransactionListPopover
        popoverContent?.delegate = self
        popoverContent?.modalPresentationStyle = .popover
        let popover = popoverContent?.popoverPresentationController
        popoverContent?.preferredContentSize = CGSize(width: 200.0, height: 45.0)
        popover?.delegate = self
        popover?.sourceView = self.view
        popover?.permittedArrowDirections = .init(rawValue: 0)
        popover?.sourceRect = CGRect(x: self.view.frame.size.width, y: 0, width: 0, height: 0)
        self.present(popoverContent!, animated: true, completion: nil)
    }
    
    @objc override func backAction() {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        DispatchQueue.main.async {
            self.setWebViewHeight(height:250)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text       = (textField.text! as NSString).replacingCharacters(in: range, with: string)

        if range.location == 0, string == "0" {
            return false
        }
        
        UitilityClass.updateTextColor(textField: amountTxt)
        if screenName == "Cash In" {
            let amount1Text = text.replacingOccurrences(of: ",", with: "")
            guard let enteredAmount = Double(amount1Text) else { return true }
            if enteredAmount > 9999999999.0 {
                self.showErrorAlert(errMessage: "Maximum amount 9,999,999,999".localized)
                return false
            }
        } else {
            let amount = self.walletAmount()
            let amountText = text.replacingOccurrences(of: ",", with: "")
            guard let enteredAmount = Double(amountText) else { return true }
            if  enteredAmount > amount {
                self.showErrorAlert(errMessage: "Your OK$ amount is less than your cash-in/cash-out amount".localized)
                return false
            }
        }
        return true
    }
    
    func setNavigation() {
        self.navigationController?.navigationBar.barTintColor        = kYellowColor
        self.navigationController?.navigationBar.tintColor           = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true)
        let titleViewSize = CGSize(width: UIScreen.main.bounds.size.width - 30, height: self.navigationController?.navigationBar.frame.size.height ?? 40)
        let navTitleView = UIView(frame: CGRect(x: 0, y: 0, width: titleViewSize.width, height: titleViewSize.height))
        let ypos = (titleViewSize.height - 30 ) / 2
        
        let label = MarqueeLabel()
        label.font =  UIFont(name: appFont, size: appFontSize)
        label.text = headerNewTitle + "         "
        label.layer.cornerRadius = 15
        
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.layer.borderColor = UIColor.white.cgColor
        label.layer.borderWidth = 1.0
        label.frame = CGRect(x: 0, y: ypos, width: self.view.frame.width * 0.75, height: 30)
        navTitleView.addSubview(label)
        
        self.navigationItem.titleView = navTitleView
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard let currentText = textField.text else { return }
        let dataWithoutComma = currentText.replacingOccurrences(of: ",", with: "")
        if dataWithoutComma.count > 0 {
            self.amtClearBtn.isHidden = false
            textField.placeholder = (screenName == "Cash In") ? "Cash In Amount".localized : "Cash Out Amount".localized
        } else {
            self.amtClearBtn.isHidden = true
            self.amountTxt.placeholder = (screenName == "Cash In") ? "Enter Cash In Amount".localized : "Enter Cash Out Amount".localized
        }
        
        amountTxt.text = self.getDigitDisplay1(dataWithoutComma)
    }
    
    @IBAction func amountClearAction(_ sender: UIButton) {
        self.amountTxt.text = ""
        self.amountTxt.placeholder = (screenName == "Cash In") ? "Enter Cash In Amount".localized : "Enter Cash Out Amount".localized
        self.searchBtn.isHidden = true
        self.amtClearBtn.isHidden = true
        self.percentView.isHidden = false
        self.commisionPercentageLabel.isHidden = false
        self.setWebViewHeight(height:250)
        self.amountTxt.inputAccessoryView?.isHidden = true
    }
    
    //MARK: - Button Action Methods
    @IBAction func searchButtonAction(_ sender : UIButton) {
        if screenName == "Cash In" {
            guard let amountStr = amountTxt.text?.replacingOccurrences(of: ",", with: ""), let amountInt =  Int(amountStr), amountInt >= 500 else { return }
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: cashInOutViewControllers.viewControllers.cioAgentListVC) as? CIOAgentListViewController {
                vc.screenName = screenName
                vc.amount = amountInt
                if let nav = navigationFromPayto {
                    nav.pushViewController(vc, animated: true)
                } else {
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        } else {
            let amount = self.walletAmount()
            guard let amountText = amountTxt.text?.replacingOccurrences(of: ",", with: ""), let enteredAmount = Double(amountText) else { return }
            guard let amountInt =  Int(amountText) else { return }
            if amount >= enteredAmount {
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: cashInOutViewControllers.viewControllers.cioAgentListVC) as? CIOAgentListViewController {
                    vc.screenName = screenName
                    vc.amount = amountInt
                    if let nav = navigationFromPayto {
                        nav.pushViewController(vc, animated: true)
                    } else {
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            } else {
                amountTxt.text = ""
              self.showErrorAlert(errMessage: "Amount should be less than wallet balance.".localized)
            }
        }
    }
    
    @IBAction func locationButtonAction(_ sender: UIButton) {
        if currentAddressName == "" {
            self.startGeoLocation()
        }
    }

    @IBAction func commisonPercentButtonAction(_ sender : UIButton) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: cashInOutViewControllers.viewControllers.ciocomPerVC) as? CIOComsnPercentViewController {
            if let nav = navigationFromPayto {
                nav.pushViewController(vc, animated: true)
            } else {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "DismissCashIOView") , object: nil)
    }
}

//MARK: - ViewAllTransactionProtocol
extension CIOHomeViewController : ViewAllTransactionProtocol {
    func showViewAllTransactionVC() {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: cashInOutViewControllers.viewControllers.cioTransHomeVC) as? CIOTransactionHomeViewController {
            if let nav = navigationFromPayto {
                nav.pushViewController(vc, animated: true)
            } else {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}

// MARK: - CashInOutModel Delegate
extension CIOHomeViewController: CashInOutModelDelegate {
    func apiResult(message: String?, statusCode: String?, type: CashInCashOutModel.CashInCashOutApiType) {
        if type == .getAuthToken {
            if currentAddressGlobal != "" {
                self.updateUserLocation()
            }
        }
    }
    
    func handleCustomError(error: CustomError, type: CashInCashOutModel.CashInCashOutApiType) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}


//MARK: - WKNavigationDelegate
extension CIOHomeViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        PTLoader.shared.hide()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        PTLoader.shared.hide()
    }
}
