//
//  CICOCancelAlertViewController.swift
//  OK
//
//  Created by Kethan Kumar on 10/8/19.
//  Copyright © 2019 Vinod Kumar. All rights reserved.
//

import UIKit

protocol CICOCancelRequestProtocol: class {
    func cancelButtonAction(reason: String)
}

class CICOCancelAlertViewController: UIViewController {
    weak var delegate: CICOCancelRequestProtocol?
    
    @IBOutlet weak var okButton: UIButton! {
        didSet {
            self.okButton.setTitle("OK".localized, for: .normal)
        }
    }
    @IBOutlet weak var cancelButton: UIButton! {
        didSet {
            self.cancelButton.setTitle("Cancel".localized, for: .normal)
        }
    }
    @IBOutlet weak var reasonTextfield: UITextField!
    @IBOutlet weak var centralX: NSLayoutConstraint!
    @IBOutlet weak var reasonTextLbl: UILabel! {
        didSet {
            self.reasonTextLbl.text = self.reasonTextLbl.text?.localized
        }
    }
    
    //MARK: - Views Method
    override func viewDidLoad() {
        super.viewDidLoad()
        reasonTextfield.placeholder = "Reason for cancel".localized
        self.reasonTextfield.text = ""
    }
    
    
    //MARK: - Button Action Method
    @IBAction func action_save(_ sender: UIButton) {
        self.delegate?.cancelButtonAction(reason: self.reasonTextfield.text ?? "")
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func action_skip(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
