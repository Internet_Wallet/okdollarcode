//
//  CIORatingViewController.swift
//  
//
//  Created by Kethan on 8/8/18.
//

import UIKit

class CIORatingViewController: CashInOutBaseViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var agentImageView: UIImageView! {
        didSet {
            self.agentImageView.layer.cornerRadius = self.agentImageView.frame.size.width / 2.0
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            self.nameLabel.text = self.nameLabel.text?.localized
        }
    }
    @IBOutlet weak var userTypeLabel: UILabel!
    @IBOutlet weak var ratingStackView: UIStackView!
    @IBOutlet weak var skipSubmitBtn: UIButton!
    
    //MARK: - Properties
    var destinationNumber: String?
    var fromUser: Bool?
    var delegate: PaymentRateDelegate?
    var rating: Int = 1
    var isPresented: Bool = false
    var currentSelectedCashDetails: CashTransferRequestStatus?
    private lazy var modelCashInCashOut: CashInCashOutModel = {
        return CashInCashOutModel()
    }()
    var userInfoDict: Dictionary<AnyHashable, Any>?
    private var ratingSelectedImage: UIImage?
    private var ratingUnselectedImage: UIImage?
    
    //MARK: - Views life cycel methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addBackButton(navTitle: "Rate Our Service")
        modelCashInCashOut.cashInCashOutDelegate = self
        skipSubmitBtn.setTitle("Skip".localized, for: .normal)
        updateUI()
    }
    
    private func updateUI() {
        if let _ = currentSelectedCashDetails {
            if let amIUser = fromUser {
                if amIUser {
                    ratingSelectedImage = #imageLiteral(resourceName: "cicoMerScreenRatingFilled")
                    ratingUnselectedImage = #imageLiteral(resourceName: "cicoMerScreenRatingUnfilled")
                    nameLabel.text = currentSelectedCashDetails?.agent?.firstName ?? ""
                    userTypeLabel.text = "Merchant".localized
                } else {
                    ratingSelectedImage = #imageLiteral(resourceName: "cicoMerScreenRatingFilled")
                    ratingUnselectedImage = #imageLiteral(resourceName: "cicoMerScreenRatingUnfilled")
                    nameLabel.text = currentSelectedCashDetails?.customer?.firstName ?? ""
                    //userTypeLabel.text = getUserType(userIndex: currentSelectedCashDetails?.customer?.agentType)
                    userTypeLabel.text = ""
                }
            }
        } else {
            guard let userInfo = userInfoDict else { return }
            println_debug(userInfo["value"] as? String ?? "No values in dict *****")
            if let reqID = userInfo["value"] as? String {
                modelCashInCashOut.getMasterDetailRequest(id: "\"\(reqID)\"")
            }
        }
    }
    
    func addBackButton(navTitle: String) {
        let backButton = UIButton(type: .custom)
        backButton.setBackgroundImage(#imageLiteral(resourceName: "tabBarBack"), for: .normal)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        backButton.addTarget(self, action: #selector(custBackAction), for: .touchUpInside)
        navigationItem.title = navTitle.localized
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white ]
        self.navigationController?.navigationBar.barTintColor = kYellowColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    @objc func custBackAction() {
        performRatingAction()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Methods
    func getUserType(userIndex: Int?) -> String {
        if let type = userIndex {
            switch type {
            case 2, 1, 8:
                return "Merchant".localized
            case 4, 6:
                return "Customer".localized
            default:
                break
            }
        }
        return ""
    }
    
    func rateCustomerAgent(ctMaster: CashTransferMaster?, amICustomer: Bool) {
        guard let ctMasterID = ctMaster?.id else { return }
        guard let ratedBy = ctMaster?.receiverID else { return }
        guard let ratedTo = ctMaster?.requesterID else { return }
        let ratingBy = amICustomer ? 0 : 1
        let ratingRequest = RateCustomerOrAgentRequest(cashTransferMasterID: ctMasterID,
                                                       ratedBy: ratedBy, ratedTo: ratedTo,
                                                       ratedValue: rating, ratingTypeBy: ratingBy)
        modelCashInCashOut.rateCustomerOrAgentRequest(request: ratingRequest)
    }
    
    func performRatingAction() {
        if let _ = currentSelectedCashDetails {
            guard let amICustomer = fromUser else { return }
            rateCustomerAgent(ctMaster: currentSelectedCashDetails?.cashTransferMaster, amICustomer: amICustomer)
        } else {
            rateCustomerAgent(ctMaster: modelCashInCashOut.masterDetail?.cashTransferMaster, amICustomer: true)
        }
    }
    
    //MARK: - Button Action Methods
    @IBAction func ratingButtonAction(_ sender: UIButton) {
        let tag = sender.tag
        rating = sender.tag
        for i in 1...tag {
            let view = self.ratingStackView.viewWithTag(i)
            if let button = view as? UIButton {
                button.setImage(ratingSelectedImage, for: .normal)
            }
        }
        if tag != 5 {
            for i in tag+1...5 {
                let view = self.ratingStackView.viewWithTag(i)
                if let button = view as? UIButton {
                    button.setImage(ratingUnselectedImage, for: .normal)
                }
            }
        }
        skipSubmitBtn.setTitle("Submit".localized, for: .normal)
    }
    
    @IBAction func submitAction(_sender: UIButton) {
        guard let currentBtnTitle = skipSubmitBtn.currentTitle else { return }
        switch currentBtnTitle {
        case "Submit".localized:
            performRatingAction()
        case "Skip".localized:
            performRatingAction()
        default:
            break
        }
    }
}

// MARK: - CashInOutModel Delegate
extension CIORatingViewController: CashInOutModelDelegate {
    func apiResult(message: String?, statusCode: String?, type: CashInCashOutModel.CashInCashOutApiType) {
        DispatchQueue.main.async {
            switch type {
            case .rateCusOrAgent:
                DispatchQueue.main.async {
                    if self.isPresented {
                        self.dismiss(animated: true, completion: nil)
                    } else {
                        self.navigationController?.popViewController(animated: true)
                    }
                    NotificationCenter.default.post(name: Notification.Name("CICOPopToroot"), object: nil, userInfo: nil)
                }
            case .getMasterDetails:
                DispatchQueue.main.async {
                    if let cashType = self.modelCashInCashOut.masterDetail?.cashTransferMaster?.cashType, cashType == 0 {
                        self.ratingSelectedImage = #imageLiteral(resourceName: "cicoMerScreenRatingFilled")
                        self.ratingUnselectedImage = #imageLiteral(resourceName: "cicoMerScreenRatingUnfilled")
                        self.nameLabel.text = self.modelCashInCashOut.masterDetail?.agent?.firstName ?? ""
                        self.userTypeLabel.text = "Merchant".localized
                    } else {
                        self.ratingSelectedImage = #imageLiteral(resourceName: "cicoMerScreenRatingFilled")
                        self.ratingUnselectedImage = #imageLiteral(resourceName: "cicoMerScreenRatingUnfilled")
                        self.nameLabel.text = self.modelCashInCashOut.masterDetail?.customer?.firstName ?? ""
                        //self.userTypeLabel.text = self.getUserType(userIndex: self.modelCashInCashOut.masterDetail?.agent?.agentType)
                        self.userTypeLabel.text = ""
                    }
                }
            default:
                break
            }
        }
    }
    
    func handleCustomError(error: CustomError, type: CashInCashOutModel.CashInCashOutApiType) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}
