//
//  CIOAgentListViewController.swift
//  OK
//
//  Created by Kethan on 8/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import GoogleMaps
import Foundation

protocol MapDelegate{
    func onClickToNavigateMap(indexPath: Int)
 }

class CIOAgentListViewController: CashInOutBaseViewController {
    private let mobileNumberAcceptableCharacters = "+0123456789"
    
    var isCheckUncheck = false
    
    //To Select De Select
    var tabDict = NSMutableDictionary()

    //MARK: - Outlets
    @IBOutlet weak var selectAllButton: UIButton!
    @IBOutlet weak var selectAllLabel : UILabel! {
        didSet {
            self.selectAllLabel.font  = UIFont(name: appFont, size: appFontSize)
            self.selectAllLabel.text = self.selectAllLabel.text?.localized
        }
    }
   
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.lightGray
        
        return refreshControl
    }()
    
    @IBOutlet weak var selectAllImageView : UIImageView! {
        didSet {
            self.selectAllImageView.image = UIImage(named:"sideMenuRadioUnselect")
          //  self.selectAllImageView.image = UIImage(named: "r_radio_btn")
        }
    }
    @IBOutlet weak var requestNowBtn : UIButton! {
        didSet {
            self.requestNowBtn.titleLabel?.font = UIFont(name: appFont, size: appTitleButton)
            self.requestNowBtn.setTitle(self.requestNowBtn.titleLabel?.text?.localized, for : .normal)
            self.requestNowBtn.isHidden = true
            self.requestNowBtn.backgroundColor = UIColor.init(hex: "EEC843")
            self.requestNowBtn.setTitleColor(UIColor.blue, for: .normal)
        }
    }
    @IBOutlet weak var filterView : UIView! {
        didSet {
            self.filterView.isHidden = true
        }
    }
    
    @IBOutlet weak var requestView : UIView! {
        didSet {
            self.requestView.isHidden = true
        }
    }
    @IBOutlet weak var indicatorView : UIActivityIndicatorView!
    @IBOutlet weak var agentListTableView : UITableView!
    
    @IBOutlet weak var filterHeaderLabel : UILabel! {
        didSet {
            self.filterHeaderLabel.font = UIFont(name: appFont, size: appFontSize)
            self.filterHeaderLabel.text = self.filterHeaderLabel.text?.localized
        }
    }
    @IBOutlet weak var noRecordLabel: UILabel! {
        didSet {
            self.noRecordLabel.font = UIFont(name: appFont, size: appFontSize)
            self.noRecordLabel.text = "No record found.".localized
        }
    }
    
    @IBOutlet weak var filetrHeaderView : UIView! {
        didSet {
            self.filetrHeaderView.layer.cornerRadius = 15.0
            self.filetrHeaderView.layer.borderWidth = 0.5
            self.filetrHeaderView.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    
    //FilterView
    @IBOutlet weak var filterByLabel : UILabel! {
        didSet {
            self.filterByLabel.font  =  UIFont(name:appFont,size:appFontSize)
            self.filterByLabel.text = self.filterByLabel.text?.localized
        }
    }
    @IBOutlet weak var mobilePaymentLabel : UILabel! {
        didSet {
            self.mobilePaymentLabel.font  =  UIFont(name:appFont,size:appFontSize)
            self.mobilePaymentLabel.text = self.mobilePaymentLabel.text?.localized
        }
    }
    @IBOutlet weak var stationaryServiceLabel : UILabel! {
        didSet {
            self.stationaryServiceLabel.font  =  UIFont(name:appFont,size:appFontSize)
            self.stationaryServiceLabel.text = self.stationaryServiceLabel.text?.localized
        }
    }
    @IBOutlet weak var filetrAgentLabel : UILabel! {
        didSet {
            self.filetrAgentLabel.font  =  UIFont(name:appFont,size:appFontSize)
            self.filetrAgentLabel.text = self.filetrAgentLabel.text?.localized
        }
    }
    @IBOutlet weak var serviceLabel : UILabel! {
        didSet {
            self.serviceLabel.font  =  UIFont(name:appFont,size:appFontSize)
            self.serviceLabel.text = self.serviceLabel.text?.localized
        }
    }
    @IBOutlet weak var openedLabel : UILabel! {
        didSet {
            self.openedLabel.font  =  UIFont(name:appFont,size:appFontSize)
            self.openedLabel.text = self.openedLabel.text?.localized
        }
    }
    @IBOutlet weak var closingSoonLabel : UILabel! {
        didSet {
            self.closingSoonLabel.font  =  UIFont(name:appFont,size:appFontSize)
            self.closingSoonLabel.text = self.closingSoonLabel.text?.localized
        }
    }
   
    @IBOutlet weak var typeOfAgentsLabel : UILabel! {
        didSet {
            self.typeOfAgentsLabel.font  =  UIFont(name:appFont,size:appFontSize)
            self.typeOfAgentsLabel.text = self.typeOfAgentsLabel.text?.localized
        }
    }
    @IBOutlet weak var agentFiletrLabel : UILabel! {
        didSet {
            self.agentFiletrLabel.font  =  UIFont(name:appFont,size:appFontSize)
            self.agentFiletrLabel.text = self.agentFiletrLabel.text?.localized
        }
    }
    @IBOutlet weak var brnachOfficeFiletrLabel : UILabel! {
        didSet {
            self.brnachOfficeFiletrLabel.font  =  UIFont(name:appFont,size:appFontSize)
            self.brnachOfficeFiletrLabel.text = self.brnachOfficeFiletrLabel.text?.localized
        }
    }
    @IBOutlet weak var OneStopFiletrLbl : UILabel! {
        didSet {
            self.OneStopFiletrLbl.font  =  UIFont(name:appFont,size:appFontSize)
            self.OneStopFiletrLbl.text = self.OneStopFiletrLbl.text?.localized
        }
    }
    @IBOutlet weak var bankFiletrLbl : UILabel! {
        didSet {
            self.bankFiletrLbl.font  =  UIFont(name:appFont,size:appFontSize)
            self.bankFiletrLbl.text = self.bankFiletrLbl.text?.localized
        }
    }
    @IBOutlet weak var noFilterLabel : UILabel! {
        didSet {
            self.noFilterLabel.font  =  UIFont(name:appFont,size:appFontSize)
            self.noFilterLabel.text = self.noFilterLabel.text?.localized
        }
    }
    
    @IBOutlet weak var filterDoneButton : UIButton! {
        didSet {
            self.filterDoneButton.titleLabel?.font = UIFont(name: appFont, size: appTitleButton)
            self.filterDoneButton.setTitle(self.filterDoneButton.titleLabel?.text?.localized, for : .normal)
        }
    }
    
    
    
    //RequestView
    @IBOutlet weak var requestingLabel : UILabel! {
        didSet {
            self.requestingLabel.font  =  UIFont(name:appFont,size:appFontSize)
            self.requestingLabel.text = "Requesting".localized
        }
    }
    @IBOutlet weak var cancelButton : UIButton! {
        didSet {
            self.cancelButton.titleLabel?.font = UIFont(name: appFont, size: appTitleButton)
            self.cancelButton.setTitle(self.cancelButton.titleLabel?.text?.localized, for : .normal)
        }
    }
    
    
    @IBOutlet weak var selectAllMainView: UIView!
    @IBOutlet weak var selectAllSubView: UIView!
    @IBOutlet weak var selectAllMainViewHeight: NSLayoutConstraint!
    
    var searchEnable = false
    
    var filterArray: [AvailableAgentsList] = []
    var localAgentListFiltered: [AvailableAgentsList] = []
    lazy var searchBar: UISearchBar = UISearchBar()

    var amount = 0
    var selectedCount = 0
    
    private var longPressGestureRecognizer:UILongPressGestureRecognizer!
    //MARK: - Properties
    var screenName = ""
    var selectedAgentArray = [Int]()
    var selectedFileterArray = [Int]()
    var tagArray = [Int]()
    var allFiltedString = ["isMobileAgent": false, "isStationaryAgent": false, "isFullDay": false, "isOpen": false, "isClosing": false, "noFilter" : false]
    var selectedAll = true
    private var isHittingApi = false
    private lazy var modelCashInCashOut: CashInCashOutModel = {
        return CashInCashOutModel()
    }()
    
   // *****************************************************************/
    
    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        noRecordLabel.isHidden = true
        modelCashInCashOut.cashInCashOutDelegate = self
        self.addCustomBackButton(navTitle: "Agent List")
        agentListTableView.tableFooterView = UIView()
        updateSelectAllView(show: false)
        getNearbyAgents()
        self.requestNowBtn.isHidden = true
        selectAllLabel.isHidden = true
        selectAllButton.isHidden = true
        selectAllImageView.isHidden = true
        self.configureLongPressGesture()
        NotificationCenter.default.addObserver(self, selector: #selector(dismissCurrentScreen), name: Notification.Name("DismissCICORequestScreen"), object: nil)
        self.agentListTableView.addSubview(self.refreshControl)
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        getNearbyAgents()
        refreshControl.endRefreshing()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func updateSelectAllView(show: Bool) {
        if show {
            selectAllMainView.isHidden = false
            selectAllSubView.isHidden = false
            selectAllButton.isHidden = false
            selectAllLabel.isHidden = false
            selectAllImageView.isHidden = false
            selectAllMainViewHeight.constant = 45.0
        } else {
            selectAllMainView.isHidden = true
            selectAllSubView.isHidden = true
            selectAllButton.isHidden = true
            selectAllLabel.isHidden = true
            selectAllImageView.isHidden = true
            selectAllMainViewHeight.constant = 0.0
        }
    }
    
    //MARK: - Methods
    private func getNearbyAgents() {
        isHittingApi = true
        let loc = self.getLocationDetailsForApi()
        let request = AvailableAgenListRequest(amount: amount, cashType: (screenName == "Cash In") ? 0 : 1,
                                               commission: 0, location: loc)
        modelCashInCashOut.getNearByMerchants(request: request)
    }
    
    private func addRightBarButton() {
        let moreButton = UIButton(type: .custom)
        moreButton.frame = CGRect(x: 0.0, y: 0.0, width: 35.0, height: 25.0)
        moreButton.setImage(UIImage(named: "search_white"), for: .normal)
        moreButton.imageView?.contentMode = .scaleAspectFit
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: moreButton)
        moreButton.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
    }
    
    @objc func dismissCurrentScreen() {
        DispatchQueue.main.async {
            self.requestView.isHidden = true
            self.indicatorView.stopAnimating()
        }
        self.showErrorAlert(errMessage: "Your request is rejected by receiver".localized)
    }
    
    private func sendRequestToSelectedAgents() {
        let loc = self.getLocationDetailsForApi()
        var agentArray = [AgentRequestIDs]()
        if searchEnable {
            for (index, agent) in self.filterArray.enumerated() {
                if self.selectedFileterArray.contains(index) {
                    agentArray.append(AgentRequestIDs(agentID: agent.id ?? ""))
                }
            }
        } else {
            for (index, agent) in self.modelCashInCashOut.agentsList.enumerated() {
                if self.selectedAgentArray.contains(index) {
                    agentArray.append(AgentRequestIDs(agentID: agent.id ?? ""))
                }
            }
        }
        let request = BookCashTransferRequest(agentRequests: agentArray, cashType: (screenName == "Cash In") ? 0 : 1,
                                              commisionPercent: 0, isDeleted: false,
                                              rechargeAmount: amount, rechargeDestination: loc)
        modelCashInCashOut.bookCashTransfer(request: request)
    }
    
    fileprivate func showPendingScreen() {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: cashInOutViewControllers.viewControllers.cioTransHomeVC) as? CIOTransactionHomeViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    fileprivate func showRequestView() {
        self.requestView.isHidden = false
        self.indicatorView.startAnimating()
    }
    
    @objc func searchAction() {
        if self.navigationItem.titleView == nil {
            searchBar.placeholder = "Search".localized
            searchBar.text = ""
            searchBar.delegate = self
            searchBar.tintColor = .black
            self.navigationItem.titleView = searchBar
            searchBar.becomeFirstResponder()
        } else {
            searchEnable = false
            self.navigationItem.titleView = nil
            self.agentListTableView.reloadData()
        }
    }
    
    //MARK: - Target methods
    @IBAction func filterAction(_ sender: UIButton) {
        if self.filterView.isHidden {
            self.filterView.isHidden = false
        } else {
            self.filterView.isHidden = true
        }
    }
    
    func configureLongPressGesture() {
        longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressGestureRecognizer.minimumPressDuration = 1.0
      //  self.agentListTableView.addGestureRecognizer(longPressGestureRecognizer)
    }
    
    
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
//        if gestureRecognizer.state == .began {
//            let touchPoint = gestureRecognizer.location(in: self.agentListTableView)
//            if let indexPath = agentListTableView.indexPathForRow(at: touchPoint) {
//                println_debug("Long press detected: indexpath - ")
//                println_debug(indexPath.row)
//                println_debug(" - ")
//                if searchEnable {
//                    if self.selectedFileterArray.contains(indexPath.row) {
//                        let index = self.selectedFileterArray.index(of: indexPath.row)
//                        self.selectedFileterArray.remove(at: index!)
//                        filterArray[indexPath.row].isSelected = false
//                    } else {
//                        self.selectedFileterArray.append(indexPath.row)
//                        filterArray[indexPath.row].isSelected = true
//                    }
//                } else {
//                    if self.selectedAgentArray.contains(indexPath.row) {
//                        let index = self.selectedAgentArray.index(of: indexPath.row)
//                        self.selectedAgentArray.remove(at: index!)
//                        modelCashInCashOut.agentsList[indexPath.row].isSelected = false
//                    } else {
//                        self.selectedAgentArray.append(indexPath.row)
//                        modelCashInCashOut.agentsList[indexPath.row].isSelected = true
//                    }
//                }
//                self.selectedCount = 1
//                agentListTableView.reloadRows(at: [indexPath], with: .none)
//                self.requestNowBtn.isHidden = false
//                longPressGestureRecognizer.isEnabled = false
//                self.updateSelectAllView(show: true)
////                self.view.layoutIfNeeded()
//                searchBar.resignFirstResponder()
//            }
//        }
    }
    
    //MARK: - Button Actions
    @IBAction func selectAllBtnAction(_ sender : UIButton){
       
        
        if selectedAll {
            isCheckUncheck = false
            self.updateSelectAllView(show: false)
            self.selectAllImageView.image = UIImage(named:"sideMenuRadioUnselect")
            //selectAllImageView.image = UIImage(named: "r_radio_btn")
            self.requestNowBtn.isHidden = false
            selectedAll = false
            self.selectedAgentArray.removeAll()
            self.selectedFileterArray.removeAll()
            for (index, _) in self.modelCashInCashOut.agentsList.enumerated() {
                self.modelCashInCashOut.agentsList[index].isSelected = false
            }
            for (index, _) in self.filterArray.enumerated() {
                self.filterArray[index].isSelected = false
            }
            selectedCount = 0
           // longPressGestureRecognizer.isEnabled = true
         //   self.updateSelectAllView(show: false)
        } else {
            isCheckUncheck = true
            self.updateSelectAllView(show: true)
            self.selectAllImageView.image = UIImage(named:"request_money_frquency_tick_mark")
           // selectAllImageView.image = UIImage(named: "cicoSelect")
            selectedAll = true
            self.requestNowBtn.isHidden = false
            if searchEnable {
                if self.filterArray.count > 0 {
                    self.selectedFileterArray = [Int](0...(self.filterArray.count - 1))
                    for (index, _) in self.filterArray.enumerated() {
                        self.filterArray[index].isSelected = true
                    }
                }
            } else {
                if self.modelCashInCashOut.agentsList.count > 0 {
                    self.selectedAgentArray = [Int](0...(self.modelCashInCashOut.agentsList.count - 1))
                    for (index, _) in self.modelCashInCashOut.agentsList.enumerated() {
                        self.modelCashInCashOut.agentsList[index].isSelected = true
                    }
                }
            }
        }
        DispatchQueue.main.async {
            self.agentListTableView.reloadData()
        }
    }
    
    @IBAction func requestNowAction(_ sender : UIButton){
        if searchEnable {
            if self.selectedFileterArray.count > 0 {
                self.sendRequestToSelectedAgents()
            } else {
                self.showErrorAlert(errMessage: "Please select atleast one profile.".localized)
            }
        } else {
            if self.selectedAgentArray.count > 0 {
                self.sendRequestToSelectedAgents()
            } else {
                self.showErrorAlert(errMessage: "Please select atleast one profile.".localized)
            }
        }
    }
    
    @IBAction func cancelButtonAction(_ sender : UIButton) {
        guard let vc = UIStoryboard(name: "CashInOutStoryboard", bundle: nil).instantiateViewController(withIdentifier:"CICOCancelAlertViewController") as? CICOCancelAlertViewController else { return }
        vc.view.frame = CGRect.zero
        vc.modalPresentationStyle = .overCurrentContext
        vc.delegate = self
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func requestNowCancelAction(_ sender : UIButton) {
        self.requestView.isHidden = true
        self.indicatorView.stopAnimating()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension CIOAgentListViewController: CICOCancelRequestProtocol {
    func cancelButtonAction(reason: String) {
        if let requesterId = self.modelCashInCashOut.bookCashTransferId {
            let request = CancelCashTransferRequest(cancelledReason: reason, requestID: requesterId)
            self.modelCashInCashOut.cancelCashTransfer(request: request)
        }
    }
}

//MARK: - Additional Methods
extension CIOAgentListViewController {
    //MARK: - Button Action Methods
    @IBAction func filterSelectionAction(_ sender : UIButton) {
        self.insertToArray(tag: sender.tag)
    }
    
    @IBAction func hideFilterViewAction(_ sender : UIButton) {
        self.filterView.isHidden = true
    }
    
    @IBAction func doneFilterAction(_ sender : UIButton) {
        selectedCount = 0
        longPressGestureRecognizer.isEnabled = true
        
        
        
        if tagArray.count > 0 {
            allFiltedString.removeAll()
            for index in tagArray {
                switch index {
                case 51:
                    allFiltedString["isMobileAgent"] = true
                case 52:
                    allFiltedString["isStationaryAgent"] = true
                case 53:
                    allFiltedString["isFullDay"] = true
                case 54:
                    allFiltedString["isOpen"] = true
                case 55:
                    allFiltedString["isClosing"] = true
                case 56:
                    allFiltedString["noFilter"] = true
                case 57:
                    passTypeToFilter(type: 11)
                    allFiltedString["isAgents"] = true
                case 58:
                    passTypeToFilter(type: 2)
                    allFiltedString["isBranchOffice"] = true
                case 59:
                    passTypeToFilter(type: 8)
                    allFiltedString["isOneStopMart"] = true
                case 60:
                    passTypeToFilter(type: 10)
                    allFiltedString["isBank"] = true
                    
                default:
                    break
                }
            }
        }
            self.filterView.isHidden = true
            searchEnable = false
          //  let loc = self.getLocationDetailsForApi()
            var filterString : CICOFilterUserAgent?
        
        //this will work for no filter
            if allFiltedString["noFilter"] == true {
                filterString = CICOFilterUserAgent(isMobileAgent: false, isStationaryAgent: false,
                                                   isFullDay: false, isOpen: false, isClosing: false)
                getListDetail(filter: filterString)
                
            } else  if allFiltedString["isAgents"] == true || allFiltedString["isBranchOffice"] == true || allFiltedString["isOneStopMart"] == true || allFiltedString["isBank"] == true {
              
                modelCashInCashOut.agentsList.removeAll()
                //this is been removed because to get new data and list will be filtered in the response block
                localAgentListFiltered.removeAll()
                
                
                if allFiltedString["isMobileAgent"] == true || allFiltedString["isStationaryAgent"] == true || allFiltedString["isFullDay"] == true || allFiltedString["isOpen"] == true || allFiltedString["isClosing"] == true{
                    filterString = CICOFilterUserAgent(isMobileAgent: allFiltedString["isMobileAgent"] ?? false,
                                                       isStationaryAgent: allFiltedString["isStationaryAgent"] ?? false,
                                                       isFullDay: allFiltedString["isFullDay"] ?? false,
                                                       isOpen: allFiltedString["isOpen"] ?? false,
                                                       isClosing: allFiltedString["isClosing"] ?? false)
                  
                    
                    getListDetail(filter: filterString)
                    
                }else{
                    for i in 0..<localAgentListFiltered.count{
                        modelCashInCashOut.agentsList.append(localAgentListFiltered[i])
                    }
                   self.agentListTableView.reloadData()
                }
                
               
            }else {
                //this will work when you click on above two section
                filterString = CICOFilterUserAgent(isMobileAgent: allFiltedString["isMobileAgent"] ?? false,
                                                   isStationaryAgent: allFiltedString["isStationaryAgent"] ?? false,
                                                   isFullDay: allFiltedString["isFullDay"] ?? false,
                                                   isOpen: allFiltedString["isOpen"] ?? false,
                                                   isClosing: allFiltedString["isClosing"] ?? false)
                
                getListDetail(filter: filterString)
            }
            
        
    }
    
    
    fileprivate func passTypeToFilter(type: Int){
        for i in 0..<modelCashInCashOut.agentsList.count{
            if let data = modelCashInCashOut.agentsList[i].type{
                if type == data{
                    localAgentListFiltered.append(modelCashInCashOut.agentsList[i])
                }
            }
        }
    }
    
    
    fileprivate func getListDetail(filter: CICOFilterUserAgent? = nil){
        let request = AvailableAgenListRequest(amount: amount, cashType: (screenName == "Cash In") ? 0 : 1, commission: 0,
                                               location: self.getLocationDetailsForApi(), filter: filter)
        modelCashInCashOut.getNearByMerchants(request: request)
        
    }
    
    
    //MARK: - Methods
    func insertToArray(tag : Int) {
        
        if tag == 56 {
            if let _ = tagArray.index(of: tag) {
                self.tagArray.removeAll()
                self.updateImageView(tag: tag, show: false, clearAll: true)
            } else {
                self.tagArray.removeAll()
                self.tagArray.append(tag)
                self.updateImageView(tag: tag, show: true, clearAll: true)
            }
        } else {
            if tagArray.contains(56) {
                if let index = tagArray.index(of: 56) {
                    tagArray.remove(at: index)
                    self.updateImageView(tag: 56, show: false)
                }
            }
            if let index = tagArray.index(of: tag) {
                self.tagArray.remove(at: index)
                self.updateImageView(tag: tag, show: false)
            } else {
                self.tagArray.append(tag)
                self.updateImageView(tag: tag, show: true)
            }
        }
    }
    
    private func updateImageView(tag : Int, show : Bool, clearAll: Bool = false) {
        for (_, subview) in self.filterView.subviews.enumerated() {
            for (_, subviewTop) in subview.subviews.enumerated() {
                if let stackView = subviewTop as? UIStackView {
                    for (_, subview1) in stackView.subviews.enumerated() {
                        for (_, subview2) in subview1.subviews.enumerated() {
                            if clearAll {
                                if let imageview = subview2 as? UIImageView {
                                    if show, imageview.tag == tag {
                                        //imageview.image = #imageLiteral(resourceName: "cicoBlueSelectMerchant")
                                        imageview.image = UIImage(named: "act_checkbox.png")
                                    } else {
                                        imageview.image = UIImage(named: "checkboxBlank.png")
                                       // imageview.image = #imageLiteral(resourceName: "cicoWhiteUnselect")
                                    }
                                }
                            } else {
                                if let imageview = subview2 as? UIImageView, imageview.tag == tag {
                                    if show {
                                        //imageview.image = #imageLiteral(resourceName: "cicoBlueSelectMerchant")
                                        imageview.image = UIImage(named: "act_checkbox.png")
                                    } else {
                                        imageview.image = UIImage(named: "checkboxBlank.png")
                                        //imageview.image = #imageLiteral(resourceName: "cicoWhiteUnselect")
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    fileprivate func cashTransferReqStatusApi() {
        if let userId = modelCashInCashOut.agentUserId {
            modelCashInCashOut.cashTransferRequestStatus(id: userId)
        }
    }
    
    fileprivate func showBranchDeatilScreen(index : Int) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier:
            "CICOBranchLocationViewController_ID") as? CICOBranchLocationViewController {
            vc.currentBranchDetails = self.modelCashInCashOut.agentsList[index]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    fileprivate func parseAgentList() {
//        var agents = false
//        if self.tagArray.contains(57) {
//            agents = true
//        }
//        var oneStopMart = false
//        if self.tagArray.contains(59) {
//            oneStopMart = true
//        }
//        var branch = false
//        if self.tagArray.contains(58) {
//            branch = true
//        }
//        var bank = false
//        if self.tagArray.contains(60) {
//            bank = true
//        }
//        self.modelCashInCashOut.agentsList = self.modelCashInCashOut.agentsList.filter({ (pair) -> Bool in
//            return true
//        })
        
    }
    
    fileprivate func calculateKilometer(weblatitude: Double,webLongitude: Double,currentLatitude: Double, currentLongitude: Double) -> String{
        let coordinate0 = CLLocation(latitude: weblatitude, longitude: webLongitude)
        let coordinate1 = CLLocation(latitude: currentLatitude, longitude: currentLongitude)
        let distance = coordinate0.distance(from: coordinate1) // result is in meters
        let km = (distance / 1000)
        let kmDouble = NSString(format: "%.2f", km)
        return kmDouble as String
    }
}

//MARK: - UITableViewDelegate, UITableViewDataSource
extension CIOAgentListViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.modelCashInCashOut.agentsList.count == 0 {
            let noDataLabel = UILabel(frame: tableView.bounds)
            noDataLabel.numberOfLines = 0
            var displayMsg = ""
            if isHittingApi {
                displayMsg = "Loading agent list...".localized
            } else {
                displayMsg = "No agents available now. Try after sometime".localized
            }
            noDataLabel.text = displayMsg
            noDataLabel.font = UIFont(name: appFont, size: 18)
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
        } else {
            tableView.backgroundView = nil
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchEnable {
            return filterArray.count
        }
        return self.modelCashInCashOut.agentsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "agentList",
                                                 for: indexPath) as? AgentListTableCell
        cell?.mapDelegate = self
        if searchEnable {
            var showTick = selectedAll
            
            if !selectedAll {
                if let _ = self.selectedFileterArray.index(of: indexPath.row) {
                    showTick = true
                } else {
                    showTick = false
                }
            }
            cell?.delegate = self
            cell?.wrapdata(agentDetails : self.filterArray[indexPath.row], selected: showTick, indexPath: indexPath.row)
            return cell!
        } else {
            var showTick = selectedAll
            
            if !selectedAll {
                if let _ = self.selectedAgentArray.index(of: indexPath.row) {
                    showTick = true
                } else {
                    showTick = false
                }
            }
            cell?.delegate = self
            cell?.wrapdata(agentDetails : self.modelCashInCashOut.agentsList[indexPath.row], selected: showTick, indexPath: indexPath.row)
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchBar.resignFirstResponder()
        
       self.updateSelectAllView(show: true)
        
        if searchEnable {
          //        if selectedCount == 1 {
                if self.selectedFileterArray.contains(indexPath.row) {
                    if let indexAtSelectedList = self.selectedFileterArray.index(of: indexPath.row) {
                        
                        //To Select DE select
                        tabDict.setValue(indexPath.row, forKey: String(indexPath.row))
                        
                        self.selectedFileterArray.remove(at: indexAtSelectedList)
                        
                        //this check is made in case the user clicks on select all and again click on any table cell and de select its so at that time we have to hide show
                        if isCheckUncheck{
                            selectedAll = false
                            self.selectAllImageView.image = UIImage(named:"sideMenuRadioUnselect")
                        }
                        
                        if selectedFileterArray.count == 0 {
                          //  self.updateSelectAllView(show: false)
                            selectedCount = 0
                           // longPressGestureRecognizer.isEnabled = true
                           // self.requestNowBtn.isHidden = true
                        }
                        filterArray[indexPath.row].isSelected = false
                    }
                } else {
                    
                    if tabDict.value(forKey: String(indexPath.row)) != nil {
                        tabDict.removeObject(forKey: String(indexPath.row))
                    }
                    
                    if tabDict.count == 0 && isCheckUncheck{
                        self.selectAllImageView.image = UIImage(named:"request_money_frquency_tick_mark")
                    }
                    
                    self.selectedFileterArray.append(indexPath.row)
                    filterArray[indexPath.row].isSelected = true
                }
                agentListTableView.reloadRows(at: [indexPath], with: .none)
          //  }
//            else {
//                self.showBranchDeatilScreen(index : indexPath.row)
//            }
        } else {
           // if selectedCount == 1 {
                if self.selectedAgentArray.contains(indexPath.row) {
                    if let indexAtSelectedList = self.selectedAgentArray.index(of: indexPath.row) {
                     tabDict.setValue(indexPath.row, forKey: String(indexPath.row))
                        self.selectedAgentArray.remove(at: indexAtSelectedList)
                     
                        if isCheckUncheck{
                            selectedAll = false
                            self.selectAllImageView.image = UIImage(named:"sideMenuRadioUnselect")
                        }
                        
                        if selectedAgentArray.count == 0 {
                           // self.updateSelectAllView(show: false)
                            selectedCount = 0
                            //longPressGestureRecognizer.isEnabled = true
                           // self.requestNowBtn.isHidden = true
                        }
                        modelCashInCashOut.agentsList[indexPath.row].isSelected = false
                    }
                } else {
                    if tabDict.value(forKey: String(indexPath.row)) != nil {
                        tabDict.removeObject(forKey: String(indexPath.row))
                    }
                    
                    if tabDict.count == 0 && isCheckUncheck{
                        self.selectAllImageView.image = UIImage(named:"request_money_frquency_tick_mark")
                    }
                    
                    self.selectedAgentArray.append(indexPath.row)
                    modelCashInCashOut.agentsList[indexPath.row].isSelected = true
                }
                agentListTableView.reloadRows(at: [indexPath], with: .none)
           // }
//            else {
//                self.showBranchDeatilScreen(index : indexPath.row)
//            }
        }
    }
}

//MARK: - SelectedAgentProtocol
extension CIOAgentListViewController: SelectedAgentProtocol {
    func selectedAgent(status: Bool, cell: AgentListTableCell) {
        if let indexPath = self.agentListTableView.indexPath(for: cell) {
            if let indexAtSelectedList = self.selectedAgentArray.index(of: indexPath.row) {
                if status == true {
                    self.selectedAgentArray.append(indexPath.row)
                    if selectedAgentArray.count == modelCashInCashOut.agentsList.count {
                        cell.checkBoxImage.image = #imageLiteral(resourceName: "cicoBlueSelectMerchant")
                        self.selectedAll = true
                        self.selectAllImageView.image = #imageLiteral(resourceName: "cicoWhiteSelect")
                    }
                } else {
                    self.selectedAgentArray.remove(at: indexAtSelectedList)
                    self.selectAllImageView.image = #imageLiteral(resourceName: "cicoWhiteUnselect")
                    self.selectedAll = false
                }
            } else {
                if status == true {
                    self.selectedAgentArray.append(indexPath.row)
                    if selectedAgentArray.count == modelCashInCashOut.agentsList.count {
                        cell.checkBoxImage.image = #imageLiteral(resourceName: "cicoBlueSelectMerchant")
                        self.selectedAll = true
                        self.selectAllImageView.image = #imageLiteral(resourceName: "cicoWhiteSelect")
                    }
                }
            }
            
            if self.selectedAgentArray.count > 0 {
                self.requestNowBtn.isHidden = false
            } else {
                self.requestNowBtn.isHidden = true
            }
        }
    }
}

// MARK: - CashInOutModel Delegate
extension CIOAgentListViewController: CashInOutModelDelegate {
    func apiResult(message: String?, statusCode: String?, type: CashInCashOutModel.CashInCashOutApiType) {
        switch type {
        case .getAgentList:
            isHittingApi = false
            
            DispatchQueue.main.async {
                if self.modelCashInCashOut.agentsList.count > 0 {
                    //self.selectedAgentArray = [Int](0...(self.modelCashInCashOut.agentsList.count - 1))
                    //if UserModel.shared.agentType == .merchant {
                    self.addRightBarButton()
                    //}
                    self.requestNowBtn.isHidden = false
                    self.selectAllLabel.isHidden = false
                    self.selectAllButton.isHidden = false
                    self.selectAllImageView.isHidden = false
                    self.selectedAll = false
                    self.requestNowBtn.isEnabled = true
                    self.requestNowBtn.backgroundColor = UIColor.init(hex: "EEC843")
                    self.requestNowBtn.setTitleColor(UIColor.blue, for: .normal)
//                    self.selectAllImageView.image = #imageLiteral(resourceName: "cicoWhiteSelect")
                }
//                if self.tagArray.count > 0 {
//                    if self.tagArray.contains(56) || self.tagArray.contains(57) || self.tagArray.contains(58) || self.tagArray.contains(59) {
//                        self.parseAgentList()
//                    }
//                }
                
                
                self.localAgentListFiltered.removeAll()
                
                if self.tagArray.count > 0 {
                    for index in self.tagArray {
                        switch index {
                        case 57:
                            self.passTypeToFilter(type: 11)
                        case 58:
                            self.passTypeToFilter(type: 2)
                        case 59:
                            self.passTypeToFilter(type: 8)
                        case 60:
                            self.passTypeToFilter(type: 10)
                            
                        default:
                            break
                        }
                    }
                }
                
                if self.localAgentListFiltered.count>0{
                    self.modelCashInCashOut.agentsList.removeAll()
                    for i in 0..<self.localAgentListFiltered.count{
                        self.modelCashInCashOut.agentsList.append(self.localAgentListFiltered[i])
                    }
                }
                
                //this will hold the distance in Km
                var localData = [String]()
                var sortedArray = [String]()
                localData.removeAll()
                sortedArray.removeAll()
                
              //  var localDic = NSMutableDictionary()
                for i in 0..<self.modelCashInCashOut.agentsList.count{
                    let data = self.modelCashInCashOut.agentsList[i]
                    localData.append(self.calculateKilometer(weblatitude: data.currentLocation?.latitude ?? 0.0, webLongitude: data.currentLocation?.longitude ?? 0.0, currentLatitude: Double(geoLocManager.currentLatitude ?? "0.0")!, currentLongitude: Double(geoLocManager.currentLongitude ?? "0.0")!))
                }
                
                sortedArray = localData.sorted()
                //making a copy of model class because i am holding the data in this object to later fetch the data from this object in to main model object
                let modelCopy = self.modelCashInCashOut.agentsList
                self.modelCashInCashOut.agentsList.removeAll()
                
                let sortDic = NSMutableDictionary()
                //comparing data and appending in main model to display data
                for j in 0..<sortedArray.count{
                    for k in 0..<localData.count{
                        if sortedArray[j] == localData[k]{
                            //this will work for the rest of the time
                            if sortDic.count>0{
                                //this check is made to avoid the duplicacy in the data
                                if sortDic.value(forKey: String(k)) == nil{
                                    sortDic.setValue(String(k), forKey: String(k))
                                    self.modelCashInCashOut.agentsList.append(modelCopy[k])
                                    break
                                }
                            }else{
                               //For the first Time
                                sortDic.setValue(String(k), forKey: String(k))
                                self.modelCashInCashOut.agentsList.append(modelCopy[k])
                                break
                            }
                        }
                    }
                }
                
               sortDic.removeAllObjects()
                PTLoader.shared.hide()
                self.agentListTableView.reloadData()
            }
        case .bookCashTransfer:
            if let sCode = statusCode, sCode == "403" {
                DispatchQueue.main.async {
                    alertViewObj.wrapAlert(title: nil, body: "You have transaction in processing state, please complete the transaction and try again".localized, img: nil)
                    alertViewObj.addAction(title: "OK".localized, style: .cancel , action: {
                        DispatchQueue.main.async {
                            self.indicatorView.stopAnimating()
                            self.requestView.isHidden = true
                        }
                        self.showPendingScreen()
                    })
                    alertViewObj.showAlert(controller: self)
                }
            } else if let sCode = statusCode, sCode == "409" {
                DispatchQueue.main.async {
                    alertViewObj.wrapAlert(title: nil, body: "You have pending transaction, please complete the transaction and try again".localized, img: nil)
                    alertViewObj.addAction(title: "OK".localized, style: .cancel , action: {
                        DispatchQueue.main.async {
                            self.indicatorView.stopAnimating()
                            self.requestView.isHidden = true
                        }
                        self.showPendingScreen()
                    })
                    alertViewObj.showAlert(controller: self)
                }
            } else {
                self.showRequestView()
                self.cashTransferReqStatusApi()
            }
        case .cashTransferReqStatus:
            println_debug(modelCashInCashOut.cashTransferReqStatusCode ?? 1000)
            println_debug(modelCashInCashOut.cashTransferReqStatus ?? 1001)
            if let status = modelCashInCashOut.cashTransferReqStatus, status == 0 {
                DispatchQueue.main.async {
                    self.indicatorView.stopAnimating()
                    self.requestView.isHidden = true
                }
                self.showPendingScreen()
            } else if let status = modelCashInCashOut.cashTransferReqStatus, status == -1 {
                self.cashTransferReqStatusApi()
            }  else if let status = modelCashInCashOut.cashTransferReqStatus, status == 1 {
                DispatchQueue.main.async {
                    self.indicatorView.stopAnimating()
                    self.requestView.isHidden = true
                }
                if let statuscode = modelCashInCashOut.cashTransferReqStatusCode, statuscode == 409 {
                    //self.showErrorAlert(errMessage: "No Driver Found".localized)
                } else if let statuscode = modelCashInCashOut.cashTransferReqStatusCode, statuscode == 405 {
                    //self.showErrorAlert(errMessage: "Your request is rejected by Agent".localized)
                } else if let statuscode = modelCashInCashOut.cashTransferReqStatusCode, statuscode == 417 {
                    self.showErrorAlert(errMessage: "You request is in queue.".localized)
                }
            }
        case .cancelCashTransfer:
            PTLoader.shared.hide()
            DispatchQueue.main.async {
                alertViewObj.wrapAlert(title: nil, body: "Request cancelled by requester".localized, img: #imageLiteral(resourceName: "succ"))
                alertViewObj.addAction(title: "OK".localized, style: .cancel , action: {
                    self.indicatorView.stopAnimating()
                    self.requestView.isHidden = true
                    self.navigationController?.popViewController(animated: true)
                })
                alertViewObj.showAlert(controller: self)
            }
        default:
            break
        }
    }
    
    func handleCustomError(error: CustomError, type: CashInCashOutModel.CashInCashOutApiType) {
        isHittingApi = false
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            if type == .bookCashTransfer {
                self.cashTransferReqStatusApi()
            } else if type == .cashTransferReqStatus {
                
            }
            println_debug("Abonded error")
        }
    }
}

//Searchbar delegate
extension CIOAgentListViewController : UISearchBarDelegate {
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.agentListTableView.reloadData()
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.agentListTableView.reloadData()
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.agentListTableView.reloadData()
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        
            if !(text == text.components(separatedBy: NSCharacterSet(charactersIn: SEARCHCHARSETOffer).inverted).joined(separator: "")) { return false }
            
            if range.location == 0 && text == " " {
                return false
            }
        
        return true
        
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
       
        if searchText.count > 0 {
            searchEnable = true
            let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: mobileNumberAcceptableCharacters).inverted
            let filteredSet = searchText.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
            
            if modelCashInCashOut.agentsList.count > 0 {
                if searchText == filteredSet {
                    filterArray = modelCashInCashOut.agentsList.filter { pair in
                        if let mobNum = pair.phoneNumber {
                            return mobNum.contains(find: searchText)
                        }
                        return  false
                    }
                } else {
                    filterArray = modelCashInCashOut.agentsList.filter { pair in
                        if let businessName = pair.businessName?.lowercased() {
                            return businessName.hasPrefix(searchText.lowercased())
                        } else if let fName = pair.firstName?.lowercased() {
                            return fName.contains(find: searchText.lowercased())
                        }
                        return  false
                    }
                }
            }
            
            if filterArray.count == 0{
                noRecordLabel.isHidden = false
            }else{
                noRecordLabel.isHidden = true
            }
            self.agentListTableView.reloadData()
        } else {
            searchEnable = false
            self.agentListTableView.reloadData()
        }
    }
    
}


//MARK: - SelectedAgent Protocol
protocol SelectedAgentProtocol : class {
    func selectedAgent(status: Bool, cell : AgentListTableCell)
}

//MARK: - UITableViewCell
class AgentListTableCell : UITableViewCell {
    //MARK: - Outlets
    @IBOutlet weak var checkBoxButton : UIButton!
    @IBOutlet weak var checkBoxImage : UIImageView!
    @IBOutlet weak var starImageView : UIImageView!
    @IBOutlet weak var starImageView2: UIImageView!
    @IBOutlet weak var starImageView3: UIImageView!
    @IBOutlet weak var starImageView4: UIImageView!
    @IBOutlet weak var starImageView5: UIImageView!
    
   
    
    
    @IBOutlet weak var agentNameLbl : UILabel! {
        didSet {
            self.agentNameLbl.font = UIFont(name: "Zawgyi-One", size: appFontSize)
            self.agentNameLbl.text = ""
        }
    }
    @IBOutlet weak var agentNumberLbl : UILabel! {
        didSet {
            self.agentNumberLbl.text = ""
        }
    }
    @IBOutlet weak var agentAddressLbl : UILabel! {
        didSet {
            self.agentAddressLbl.text = ""
        }
    }
    @IBOutlet weak var agnetStatusLabel : UILabel! {
        didSet {
            self.agnetStatusLabel.font = UIFont(name: appFont, size: appFontSize)
            self.agnetStatusLabel.text = ""
        }
    }
    @IBOutlet weak var agentRatingsLbl : UILabel! {
        didSet {
            self.agentRatingsLbl.text = ""
        }
    }
    @IBOutlet weak var agnetDurationLabel : UILabel! {
        didSet {
            self.agnetDurationLabel.text = ""
        }
    }
    @IBOutlet weak var callButton : UIButton!
    @IBOutlet weak var stationaryImageView : UIImageView! {
        didSet {
            self.stationaryImageView.isHidden = true
        }
    }
    @IBOutlet weak var mobileAgentImageView : UIImageView! {
        didSet {
            self.mobileAgentImageView.isHidden = true
        }
    }
    
    var agentModel: AvailableAgentsList?
    @IBOutlet weak var lineLabel: UILabel!
    @IBOutlet weak var bottomLineLabel: UILabel!
    //MARK: - Properties
    var checkSelected = false
    weak var delegate : SelectedAgentProtocol?
    let gifManager = SwiftyGifManager(memoryLimit:6)
    var mapDelegate: MapDelegate?
    
    //MARK: - View Methods
    override func awakeFromNib() {
        self.mobileAgentImageView.isHidden = true
        self.stationaryImageView.isHidden = true
    }
    
    private func callToUserOrAgent(numberStr: String) {
        var number = wrapFormattedNumber(numberStr)
        if number.hasPrefix("00") {
            number = "+" + number
        }
        let phone = "tel://\(number)"
        if let url = URL.init(string: phone) {
            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        }
    }
    
    //MARK: - Button Action Methods
    @IBAction func callButtonAction(_ sender: UIButton) {
        if let model = self.agentModel, let number = model.phoneNumber {
            self.callToUserOrAgent(numberStr: number)
        }
    }
    
    @IBAction func checkBoxButtonAction(_ sender: UIButton) {
        if checkSelected == false {
            self.checkBoxImage.image = #imageLiteral(resourceName: "cicoBlueSelectMerchant")
            self.checkSelected = true
        } else {
            self.checkBoxImage.image = #imageLiteral(resourceName: "cicoBlueUnselectMerchant")
            self.checkSelected = false
        }
        delegate?.selectedAgent(status: self.checkSelected, cell: self)
    }
    
    //MARK: - Methods
    //Added index path param to call in delegate
    func wrapdata(agentDetails: AvailableAgentsList, selected: Bool,indexPath: Int) {
        self.agentRatingsLbl.isHidden = true
        self.agentAddressLbl.text = ""
        self.agentModel = agentDetails
        let name = agentDetails.businessName.safelyWrappingString().capitalizedFirst()
        if name.count == 0 {
            self.agentNameLbl.text = agentDetails.firstName.safelyWrappingString().capitalizedFirst()
        } else {
            self.agentNameLbl.text = name.capitalizedFirst()
        }
        if let phNo = agentDetails.phoneNumber {
            if phNo.hasPrefix("0095") {
                self.agentNumberLbl.text = "+95" + "\(phNo.substring(from: 4))"
            } else {
                self.agentNumberLbl.text = phNo
            }
        }
        
        if agentDetails.isSelected {
           // self.checkBoxImage.image = UIImage(named: "cicoSelect")
            self.checkBoxImage.image = UIImage(named: "request_money_frquency_tick_mark")
            self.backgroundColor = UIColor.init(red: 220.0 / 255.0, green: 220.0 / 255.0, blue: 220.0 / 255.0, alpha: 1.0)
            self.lineLabel.backgroundColor = UIColor.white
            bottomLineLabel.backgroundColor = UIColor.darkGray
        } else {
            self.checkBoxImage.image = UIImage(named: "cicoCamera")
            self.backgroundColor = UIColor.white
            self.lineLabel.backgroundColor = UIColor.gray
            self.bottomLineLabel.backgroundColor = UIColor.darkGray
        }
        
        var adressString = ""
        if let line2 = agentDetails.line2, line2.count > 0 {
            adressString = "\(line2)"
        }
        if let line1 = agentDetails.line1, line1.count > 0 {
            adressString = "\(adressString), \(line1)"
        }
        if let cityName = agentDetails.cityName, cityName.count > 0 {
            adressString = "\(adressString), \(cityName)"
        }
        if let country = agentDetails.country, country.count > 0 {
            adressString = "\(adressString), \(country)"
        }
        if adressString.last == "," {
            adressString = String(adressString.dropLast())
        }
        if adressString != "" {
            self.agentAddressLbl.text = adressString
        }
        
        if let add = agentAddressLbl.text, add.count > 0 {
            
        } else {
            self.agentAddressLbl.text = "Address not found".localized
        }
        
        if agentDetails.isOpenAlways.safelyWrappingString() == "true" {
            self.agnetStatusLabel.text = "Open 24/7".localized
        } else {
            self.agnetStatusLabel.text = "Closed".localized
        }
     
        let coordinate0 = CLLocation(latitude: agentDetails.currentLocation?.latitude ?? 0.0, longitude: agentDetails.currentLocation?.longitude ?? 0.0)
        let coordinate1 = CLLocation(latitude: Double(geoLocManager.currentLatitude ?? "0.0")!, longitude: Double(geoLocManager.currentLongitude ?? "0.0")!)
        let distance = coordinate0.distance(from: coordinate1) // result is in meters
        let time = (distance / 500).rounded()
        let km = (distance / 1000)
        let kmDouble = NSString(format: "%.2f", km)

        if km < 0.51 {
            self.agnetDurationLabel.text = "1 min (\(kmDouble) Km)"
        } else {
            self.agnetDurationLabel.text = "\(Int(time)) min (\(kmDouble) Km)"
        }
                
        if let type = agentDetails.type{
            
            if type == 1 || type == 2 {
                if agentDetails.agentType == 1{
                    let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTappedNew(tapGestureRecognizer:)))
                    self.stationaryImageView.isUserInteractionEnabled = true
                    self.stationaryImageView.tag = indexPath
                    self.stationaryImageView.addGestureRecognizer(tapGestureRecognizer)
                    self.stationaryImageView.isHidden = false
                    self.mobileAgentImageView.clear()
                    let dynamicImageString = "bike_animation.gif"
                    let gif = UIImage(gifName: dynamicImageString)
                    self.stationaryImageView.setGifImage(gif, manager: gifManager)
                }else{
                    self.stationaryImageView.clear()
                    self.mobileAgentImageView.isHidden = false
                    //            let dynamicImageString = "bike_animation.gif"
                    //            let gif = UIImage(gifName: dynamicImageString)
                    //            self.mobileAgentImageView.setGifImage(gif, manager: gifManager)
                    self.mobileAgentImageView.image = #imageLiteral(resourceName: "cicoMerchantAgent")
                    let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
                    self.mobileAgentImageView.isUserInteractionEnabled = true
                    self.mobileAgentImageView.tag = indexPath
                    self.mobileAgentImageView.addGestureRecognizer(tapGestureRecognizer)
                }
                
                
            }else{
                
                self.mobileAgentImageView.clear()
                self.stationaryImageView.clear()
                self.mobileAgentImageView.isHidden = true
                self.stationaryImageView.isHidden = true
                
            }
        }

        
        if let rating = agentDetails.rating {
            if !(rating > 0.0){
                starImageView.isHidden = true
                starImageView2.isHidden = true
                starImageView3.isHidden = true
                starImageView4.isHidden = true
                starImageView5.isHidden = true
                agentRatingsLbl.isHidden = true
            }else{
                starImageView.isHidden = false
                starImageView2.isHidden = false
                starImageView3.isHidden = false
                starImageView4.isHidden = false
                starImageView5.isHidden = false
                agentRatingsLbl.isHidden = false
                
                if rating >= 4.50 {
                    // 5 star
                    starImageView.image = UIImage(named: "rate_yellow_star.png")
                    starImageView2.image = UIImage(named: "rate_yellow_star.png")
                    starImageView3.image = UIImage(named: "rate_yellow_star.png")
                    if rating == 4.50{
                        starImageView5.image = UIImage(named: "halfStar.png")
                        
                    }else{
                       starImageView5.image = UIImage(named: "rate_yellow_star.png")
                    }
                    
                    starImageView4.image = UIImage(named: "rate_yellow_star.png")
                }else if (rating >= 3.50) {
                    //4 star
                    starImageView.image = UIImage(named: "rate_yellow_star.png")
                    starImageView2.image = UIImage(named: "rate_yellow_star.png")
                    starImageView3.image = UIImage(named: "rate_yellow_star.png")
                    
                    if rating == 3.50{
                        starImageView4.image = UIImage(named: "halfStar.png")
                        
                    }else{
                        starImageView4.image = UIImage(named: "rate_yellow_star.png")
                    }
                    
                }else if (rating >= 2.50){
                    //3 star
                    starImageView.image = UIImage(named: "rate_yellow_star.png")
                    starImageView2.image = UIImage(named: "rate_yellow_star.png")
                    
                    if rating == 2.50{
                        starImageView3.image = UIImage(named: "halfStar.png")
                        
                    }else{
                        starImageView3.image = UIImage(named: "rate_yellow_star.png")
                    }
                    
                }
                else if (rating >= 1.50){
                    //2 star
                    starImageView.image = UIImage(named: "rate_yellow_star.png")
                    if rating == 1.50{
                        starImageView2.image = UIImage(named: "halfStar.png")
                        
                    }else{
                        starImageView2.image = UIImage(named: "rate_yellow_star.png")
                    }
                }
                else if (rating >= 0.50){
                    //1 star
                    
                    if rating == 0.50{
                        starImageView.image = UIImage(named: "halfStar.png")
                        
                    }else{
                        starImageView.image = UIImage(named: "rate_yellow_star.png")
                    }
                }
            }
        }

    }
}

extension AgentListTableCell{
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        mapDelegate?.onClickToNavigateMap(indexPath: mobileAgentImageView.tag)
        
        // Your action
    }
    
    @objc func imageTappedNew(tapGestureRecognizer: UITapGestureRecognizer)
    {
        mapDelegate?.onClickToNavigateMap(indexPath: stationaryImageView.tag)
        
    }
    
    
}

extension CIOAgentListViewController: MapDelegate{
   
    func onClickToNavigateMap(indexPath: Int){
        self.showBranchDeatilScreen(index : indexPath)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
