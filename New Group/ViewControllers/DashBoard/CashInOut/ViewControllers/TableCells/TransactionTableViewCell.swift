//
//  TransactionTableViewCell.swift
//  OK
//
//  Created by Kethan on 8/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var agentNameLabel : UILabel!
    @IBOutlet weak var agentAddressLabel : UILabel!
    @IBOutlet weak var agentRatingLabel : UILabel!
    @IBOutlet weak var serviceTypeLabel : UILabel!
    @IBOutlet weak var amountLabel : UILabel!
    @IBOutlet weak var statusLabel : UILabel!

    //MARK: - Views Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
