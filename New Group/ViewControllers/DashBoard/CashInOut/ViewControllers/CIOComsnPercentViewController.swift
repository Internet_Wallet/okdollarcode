//
//  CIOComsnPercentViewController.swift
//  OK
//
//  Created by Kethan on 8/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import WebKit

class CIOComsnPercentViewController: CashInOutBaseViewController {

    //MARK: - Properties
    private var webView: WKWebView!
    
    //MARK: - Views life cycle methods
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setTitle()
        
        // Remove all cache
        URLCache.shared.removeAllCachedResponses()
        
        // Delete any associated cookies
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }
        }
        
        var urlSuffix = Url.cashInOutCommisionUrl
        if let currentStr = UserDefaults.standard.string(forKey: "currentLanguage") {
            if currentStr == "my" {
                urlSuffix = Url.cashInOutCommisionUrlMY
            }
        }
        let url = getUrl(urlStr: urlSuffix, serverType: .cashInCashOutCommisionUrl)
        let urlReq = URLRequest(url: url)
        webView.load(urlReq)
        PTLoader.shared.show()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    	
    //MARK: - Methods
    func setTitle() {
        let backButton = UIButton(type: .custom)
        backButton.setBackgroundImage(#imageLiteral(resourceName: "tabBarBack"), for: .normal)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        navigationItem.title = "Commission Percent".localized
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white ]
    }
    
    deinit {
        navigationController?.isToolbarHidden = true
    }
}

//MARK: - WKNavigationDelegate
extension CIOComsnPercentViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        PTLoader.shared.hide()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        PTLoader.shared.hide()
    }
}
