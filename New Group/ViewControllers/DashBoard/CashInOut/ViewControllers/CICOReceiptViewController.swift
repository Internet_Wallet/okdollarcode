
//  CICOReceiptViewController.swift
//  OK
//
//  Created by Kethan on 8/10/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

class CICOReceiptViewController: CashInOutBaseViewController, MoreControllerActionDelegate, PaymentRateDelegate {
    //MARK: - Outlets
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var mobileNumber: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var keysenderAccountNumber: UILabel!
    @IBOutlet weak var valuesenderAccountNumber: UILabel!
    @IBOutlet weak var keysenderAmount: UILabel!
    @IBOutlet weak var valuesenderAmount: UILabel!
    @IBOutlet weak var keyTransactionID: UILabel!
    @IBOutlet weak var valueTransactionID: UILabel!
    @IBOutlet weak var keyTransactionType: UILabel!
    @IBOutlet weak var valueTransactionType: UILabel!
    @IBOutlet weak var valueDate: UILabel!
    @IBOutlet weak var valueTime: UILabel!
    @IBOutlet weak var heightDateConstraint: NSLayoutConstraint!
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var tabHome: UITabBarItem!
    @IBOutlet weak var tabContact: UITabBarItem!
    @IBOutlet weak var tabfav: UITabBarItem!
    @IBOutlet weak var cardView: CardDesignView!
    
    //MARK: - Properties
    var categoryValue: RecipetPlanCases?
    var tabBarAction: RecieptTabSelectionType?
    var planType: String?
    var currencyInt: String?
    var contactAdded: Bool = false
    var currentSelectedCashDetails: CashTransferRequestStatus?
    var genericPaymentResponse: CICOPaymentResponse?
    
    //MARK: - Properties
    private lazy var modelCashInCashOut: CashInCashOutModel = {
        return CashInCashOutModel()
    }()
    
    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            self.changeCancelImplementation()
        }
        self.uiUpdates()
        modelCashInCashOut.cashInCashOutDelegate = self
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: appFont, size: 11) ?? UIFont.systemFont(ofSize: 11)], for: .normal)
        UITabBar.appearance().unselectedItemTintColor = .lightGray
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Receipt".localized
        //        [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"AmericanTypewriter" size:20.0f]} forState:UIControlStateNormal];
        let color = UIColor.lightGray
        let attrFont = [NSAttributedString.Key.font: UIFont(name: appFont, size: 11.0) ?? UIFont.systemFont(ofSize: 11.0), NSAttributedString.Key.foregroundColor: color]
        
        self.tabHome.setTitleTextAttributes(attrFont, for: .normal)
        self.tabContact.setTitleTextAttributes(attrFont, for: .normal)
        self.tabfav.setTitleTextAttributes(attrFont, for: .normal)
        self.tabfav.title = "Feedback".localized
        self.tabHome.title = "Home Page".localized
        self.tabContact.title = "TabbarAddContact".localized
        
        //self.navigationController?.navigationBar.backgroundColor = MyNumberTopup.OperatorColorCode.okDefault
        //self.navigationController?.navigationBar.tintColor = MyNumberTopup.OperatorColorCode.okDefault
        navigationController?.navigationBar.barTintColor = kYellowColor
        self.navigationItem.setHidesBackButton(true, animated: true)
        NotificationCenter.default.addObserver(self, selector: #selector(CICOPopToRoot), name: Notification.Name("CICOPopToRootVC"), object: nil)
    }
    
    @objc func CICOPopToRoot() {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    //MARK:- Methods
    private func uiUpdates() {
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.keysenderAccountNumber.text = self.keysenderAccountNumber.text.safelyWrappingString().localized
        self.keysenderAmount.text = self.keysenderAmount.text.safelyWrappingString().localized
        self.keyTransactionID.text = self.keyTransactionID.text.safelyWrappingString().localized
        self.keyTransactionType.text = self.keyTransactionType.text.safelyWrappingString().localized
        self.valueDate.text = self.valueDate.text.safelyWrappingString().localized
        self.valueTime.text = self.valueTime.text.safelyWrappingString().localized
        
        guard let model = genericPaymentResponse else { return }
        guard let customerDet = currentSelectedCashDetails else { return }
        
        self.valueTransactionID.text = model.transId.safelyWrappingString()
   
        if let date = model.responsects {
            let dateFormat = date.components(separatedBy: " ").first?.dateValue(dateFormatIs: "dd-MMM-yyyy")
            let dateKey = (dateFormat?.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy"))!
            self.valueDate.text = dateKey
            
            let timeKey = date.components(separatedBy: " ").last ?? ""
            self.valueTime.text = timeKey
        }

        
//        let preciseTime  = model.responsects.safelyWrappingString().components(separatedBy: " ")
//        println_debug(preciseTime) //get the format and display date and time
//        self.valueDate.text = preciseTime.first.safelyWrappingString()
//        self.valueTime.text = preciseTime.last.safelyWrappingString()
        var paidAmount = 0.0
        if let cashType = customerDet.cashTransferMaster?.cashType, cashType == 0 {
            if let customerNumber = customerDet.customer?.phoneNumber {
                let mobNum = wrapFormattedNumber(customerNumber)
                self.mobileNumber.text = "(+95) \(mobNum)"
            }
            self.userNameLabel.text = customerDet.customer?.firstName ?? ""
            self.amount.text = customerDet.cashTransferMaster?.cashWithOutCommission.safelyWrappingString()
            paidAmount = Double(customerDet.cashTransferMaster?.cashWithOutCommission ?? 0) 
        } else {
            if let customerNumber = customerDet.agent?.phoneNumber {
                let mobNum = wrapFormattedNumber(customerNumber)
                self.mobileNumber.text = "(+95) \(mobNum)"
            }
            self.userNameLabel.text = customerDet.agent?.firstName ?? ""
            self.amount.text = customerDet.cashTransferMaster?.totalAmount.safelyWrappingString()
            paidAmount = (customerDet.cashTransferMaster?.totalAmount ?? 0.0)
        }
        self.valuesenderAccountNumber.text = "Pay To".localized
        let finalAmount = self.walletAmount() - paidAmount
        let displayAmount = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: "\(finalAmount)")
        self.valuesenderAmount.text = displayAmount + " MMK".localized
        self.valueTransactionType.text = (customerDet.cashTransferMaster?.cashType == 0) ? "Cash In".localized: "Cash Out".localized
        self.navigationController?.navigationBar.backgroundColor = MyNumberTopup.OperatorColorCode.okDefault
        self.navigationController?.navigationBar.tintColor = MyNumberTopup.OperatorColorCode.okDefault
    }
    
    private func addFav() {
        //guard let model = reciptModel else { return }
        guard let ratingScreen = UIStoryboard(name: "CashInOutStoryboard", bundle: nil).instantiateViewController(withIdentifier: "CIORatingViewController_ID") as? CIORatingViewController else { return }
        //ratingScreen.modalPresentationStyle = .overCurrentContext
        ratingScreen.delegate = self
        if let cashtype = currentSelectedCashDetails?.cashTransferMaster?.cashType, cashtype == 0 {
            ratingScreen.fromUser = false
        } else {
            ratingScreen.fromUser = true
        }
//        ratingScreen.isPresented = true
        navigationController?.navigationBar.barTintColor = kYellowColor
        ratingScreen.destinationNumber = currentSelectedCashDetails?.customer?.phoneNumber.safelyWrappingString() ?? ""
        ratingScreen.currentSelectedCashDetails = currentSelectedCashDetails
        self.navigationController?.pushViewController(ratingScreen, animated: true)
    }
    
    private func addContacts() {
        guard let modelRec = currentSelectedCashDetails else { return }
        if contactAdded {
            self.showErrorAlert(errMessage: "Contact Already Added".localized)
        }
        let store     = CNContactStore()
        let contact   = CNMutableContact()

        let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue : wrapFormattedNumber((modelRec.customer?.phoneNumber.safelyWrappingString())!) ))
        contact.phoneNumbers = [homePhone]
        contact.givenName = modelRec.customer?.firstName.safelyWrappingString() ?? ""
        let controller = CNContactViewController.init(forNewContact: contact)
        controller.contactStore = store
        controller.delegate     = self
        controller.title        = "Add Contact".localized
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.backgroundColor = UIColor.init(hex: "F3C632")
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func goToHome() {
        self.performRatingAction()
    }
    
    func rateCustomerAgent(ctMaster: CashTransferMaster?, amICustomer: Bool) {
        let rating: Int = 1
        guard let ctMasterID = ctMaster?.id else { return }
        guard let ratedBy = ctMaster?.receiverID else { return }
        guard let ratedTo = ctMaster?.requesterID else { return }
        let ratingBy = amICustomer ? 0 : 1
        let ratingRequest = RateCustomerOrAgentRequest(cashTransferMasterID: ctMasterID,
                                                       ratedBy: ratedBy, ratedTo: ratedTo,
                                                       ratedValue: rating, ratingTypeBy: ratingBy)
        modelCashInCashOut.rateCustomerOrAgentRequest(request: ratingRequest)
    }
    
    func performRatingAction() {
        if let cashType = currentSelectedCashDetails?.cashTransferMaster?.cashType, cashType == 0 {
            rateCustomerAgent(ctMaster: currentSelectedCashDetails?.cashTransferMaster, amICustomer: false)
        } else {
            rateCustomerAgent(ctMaster: currentSelectedCashDetails?.cashTransferMaster, amICustomer: true)
        }
    }
    
    private func more() {
        guard  let vc = UIStoryboard.init(name: "PayTo", bundle: nil).instantiateViewController(withIdentifier: "PTMoreViewController") as? PTMoreViewController else {return}
        addChild(vc)
        vc.isFromTopup = false
        vc.view.frame = self.view.bounds
        vc.didMove(toParent: self)
        vc.delegate = self
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        vc.view.layer.add(transition, forKey: nil)
        //view.addSubview(vc.view)
    }
    
    func showHomeViewController() {
        //self.navigationController?.popToRootViewController(animated: true)
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func actionFor(option: MorePaymentAction) {
        switch option {
        case .morePayment:
            break
            //self.showHomeViewController()
        case .repeatPayment:
            break
            //self.showHomeViewController()
        case .invoice:
            break
            //self.invoice()
        case .share:
            break
            //self.share()
        }
    }
    
    func ratingShow(_ rate: Int, commentText: String?) {
        
    }
    
    private func invoice() {
        guard let model = currentSelectedCashDetails else { return }
        guard let responseModel = genericPaymentResponse else { return }
        var pdfDictionary = Dictionary<String,Any>()
        pdfDictionary["senderAccName"]      = UserModel.shared.name
        
        var number = UserModel.shared.mobileNo
        _ = number.remove(at: String.Index.init(encodedOffset: 0))
        _ = number.remove(at: String.Index.init(encodedOffset: 0))
        number  = "+" + number
        var destination = ""
        if let destinationNum = model.customer?.phoneNumber.safelyWrappingString() {
            destination = destinationNum
            _ = destination.remove(at: String.Index.init(encodedOffset: 0))
            _ = destination.remove(at: String.Index.init(encodedOffset: 0))
            destination  = "+" + destination
        }
       
        
        pdfDictionary["invoiceTitle"]       = "Transaction Receipt".localized
        pdfDictionary["senderAccNo"]        = number
        pdfDictionary["mobileNumber"]       = destination
        pdfDictionary["transactionID"]      = responseModel.transId
        pdfDictionary["transactionType"]    = "PAYTO"
        pdfDictionary["transactionDate"]    = responseModel.responsects.safelyWrappingString()
        let pdfAmount = CodeSnippets.commaFormatWithDoublePrecision(forTheAmount: responseModel.amount.safelyWrappingString()) + " MMK"
        pdfDictionary["amount"]             = pdfAmount
        //pdfDictionary["amount"]             = responseModel.amount.safelyWrappingString()
        //pdfDictionary["cashback"]           = model.kickvalue.safelyWrappingString()
        pdfDictionary["transactionDate"]    = responseModel.responsects
        pdfDictionary["logoName"]           = "appIcon_Ok"
        
        
        let lat  = geoLocManager.currentLatitude ?? ""
        let long = geoLocManager.currentLongitude ?? ""
        
        let firstPart  = "00#" + "\(pdfDictionary["businessName"] ?? "")" + "-" + "\(pdfDictionary["receiverAccNo"] ?? "") " + "@" + pdfAmount + "&"
        let secondPart =  "\(pdfDictionary["CashInCashOut"] ?? "")" + "β" + "\(lat)" + "γ" + "\(long)" + "α" + "\(pdfDictionary["transactionID"] ?? "")" + "" + "`,,"
        
        let finalPart = String.init(format:"%@%@", firstPart,secondPart)
        
        guard let hashedQRKey = AESCrypt.encrypt(finalPart, password: "m2n1shlko@$p##d") else { return }
        
        let qrObject = PTQRGenerator.init()
        pdfDictionary["qrImage"] = qrObject.getQRImage(stringQR: hashedQRKey, withSize: 10)
        
        guard let pdfUrl = CodeSnippets.createInvoicePDF(dictValues: pdfDictionary, pdfFile: "OK$ CashIn CashOut Report") else {
            println_debug("Error - pdf not generated")
            return
        }
        
        guard let vc = UIStoryboard(name: "Topup", bundle: nil).instantiateViewController(withIdentifier: "SharePDFViewController") as? SharePDFViewController else { return }
        vc.url = pdfUrl
        navigationController?.navigationBar.barTintColor = kYellowColor
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func share() {
        let snapImage = self.cardView.snapshotOfCustomeView
        let vc = UIActivityViewController(activityItems: [snapImage], applicationActivities: [])
        DispatchQueue.main.async {self.present(vc, animated: true)}
    }
    
    private func screenShotMethod(_ view: UIView) -> UIImage? {
        //Create the UIImage
        UIGraphicsBeginImageContext(view.frame.size)
        
        if let context = UIGraphicsGetCurrentContext() {
            view.layer.render(in: context)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            if let imageFinal = image {
                UIImageWriteToSavedPhotosAlbum(imageFinal, nil, nil, nil)
            }
            return image
        }
        return nil
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

//MARK: - UITabBarDelegate
extension CICOReceiptViewController: UITabBarDelegate {
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        tabBarAction = RecieptTabSelectionType(rawValue: item.tag)
        guard let action = tabBarAction else { return }
        //guard let modelRec = reciptModel else { return }
        switch action {
        case .addFavorite:
            self.addFav()
        case .addContact:
            self.addContacts()
        case .home:
            self.goToHome()
        case .more:
            break
        }
    }
}

//MARK: - CNContactViewControllerDelegate
extension CICOReceiptViewController: CNContactViewControllerDelegate {
    
    func changeCancelImplementation() {
        let originalSelector = Selector(("editCancel:"))
        let swizzledSelector = #selector(CNContactViewController.cancelHack)
        if let originalMethod = class_getInstanceMethod(object_getClass(CNContactViewController()), originalSelector),
           let swizzledMethod = class_getInstanceMethod(object_getClass(CNContactViewController()), swizzledSelector) {
            method_exchangeImplementations(originalMethod, swizzledMethod)
        }
    }
    
    
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        if let _ = contact {
            contactAdded = true
        }
        viewController.delegate = nil
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - CashInOutModel Delegate
extension CICOReceiptViewController: CashInOutModelDelegate {
    func apiResult(message: String?, statusCode: String?, type: CashInCashOutModel.CashInCashOutApiType) {
        DispatchQueue.main.async {
            switch type {
            case .rateCusOrAgent:
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                    NotificationCenter.default.post(name: Notification.Name("CICOPopToroot"), object: nil, userInfo: nil)
                }
            default:
                break
            }
        }
    }
    
    func handleCustomError(error: CustomError, type: CashInCashOutModel.CashInCashOutApiType) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}
