//
//  CIOAllRequestViewController.swift
//  OK
//
//  Created by Kethan on 8/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class CIOMyRequestViewController: CashInOutBaseViewController {
    //MARK: - Outlets
    @IBOutlet weak var transactionRequestTableView : UITableView!
    @IBOutlet weak var noRecordsView : UIView! {
        didSet {
            self.noRecordsView.isHidden = false
        }
    }
    @IBOutlet weak var noRecordsLabel : UILabel! {
        didSet {
            self.noRecordsLabel.font = UIFont(name: appFont, size: appFontSize)
            self.noRecordsLabel.text = self.noRecordsLabel.text?.localized
        }
    }
    
    //MARK: - Properties
    private lazy var modelCashInCashOut: CashInCashOutModel = {
        return CashInCashOutModel()
    }()
    weak var delegate : CICOelectedRequestProcotol?
    
    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        modelCashInCashOut.cashInCashOutDelegate = self
        self.transactionRequestTableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getRequests()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Methods
    fileprivate func getRequests() {
        modelCashInCashOut.getPendingCashTransferList()
    }
}

//MARK: - UITableViewDelegate, UITableViewDataSource
extension CIOMyRequestViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelCashInCashOut.pendingList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "transReqCell", for: indexPath) as! CIOTransReqTableCell
        cell.wrapPendingData(modelData: modelCashInCashOut.pendingList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.selectedPendingRequest(pendingCashTranfReq: modelCashInCashOut.pendingList[indexPath.row])
    }
}

// MARK: - CashInOutModelDelegate
extension CIOMyRequestViewController: CashInOutModelDelegate {
    func apiResult(message: String?, statusCode: String?, type: CashInCashOutModel.CashInCashOutApiType) {
        PTLoader.shared.show()
        DispatchQueue.main.async {
            if self.modelCashInCashOut.pendingList.count > 0 {
                self.noRecordsView.isHidden = true
            }
            self.transactionRequestTableView.reloadData()
            PTLoader.shared.hide()
        }
    }
    
    func handleCustomError(error: CustomError, type: CashInCashOutModel.CashInCashOutApiType) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}
