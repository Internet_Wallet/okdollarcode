//
//  CIOTransactionHomeViewController.swift
//  OK
//
//  Created by Kethan on 8/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

//MARK: - CICOelectedRequestProcotol
protocol CICOelectedRequestProcotol : class {
    func selectedRequest(acceptedCashTransferList : AcceptedCashTransferList)
    func selectedPendingRequest(pendingCashTranfReq : PendingCashTransferResponse)
}

class CIOTransactionHomeViewController: CashInOutBaseViewController, UIPopoverPresentationControllerDelegate {

    //MARK: - Properties
    fileprivate var cicoTransactionHomeVC : PaytoScroller?
    fileprivate var controllerArray : [UIViewController] = []
    var fromCancelAcceptScreen : String?
    private var idToCheck = ""
    private lazy var modelCashInCashOut: CashInCashOutModel = {
        return CashInCashOutModel()
    }()
    var typeChange: (CICOTypeAndStatus, Bool)?
    
    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        addBackButton(navTitle: "Request")
        self.initializeViewControllers()
        if UserModel.shared.agentType == .merchant || UserModel.shared.agentType == .agent {
            self.addCustomMenuButton()
        }
        modelCashInCashOut.cashInCashOutDelegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Methods
    private func addBackButton(navTitle: String) {
        let backButton = UIButton(type: .custom)
        backButton.setBackgroundImage(#imageLiteral(resourceName: "tabBarBack"), for: .normal)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        navigationItem.title = navTitle.localized
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: appFont, size: 20) ?? UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.white ]
        self.navigationController?.navigationBar.barTintColor = kYellowColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    private func initializeViewControllers() {
//        let myRequestTabVC =  UIStoryboard(name: cashInOutViewControllers.cashinoutStoryBoard,
//                                           bundle: nil).instantiateViewController(withIdentifier: cashInOutViewControllers.viewControllers.cioMyrequestVC) as! CIOMyRequestViewController
//        myRequestTabVC.delegate = self
//        myRequestTabVC.title = "My Requests".localized
        
        let pendingReqTabVC = UIStoryboard(name: cashInOutViewControllers.cashinoutStoryBoard,
                                           bundle: nil).instantiateViewController(withIdentifier: cashInOutViewControllers.viewControllers.cioPendingrequestVC) as! CIOPendingRequestViewController
        pendingReqTabVC.delegate = self
        pendingReqTabVC.title = "Pending Requests".localized
        
        let successReqTabVC =  UIStoryboard(name: cashInOutViewControllers.cashinoutStoryBoard,
                                            bundle: nil).instantiateViewController(withIdentifier: cashInOutViewControllers.viewControllers.cioSuccessrequestVC) as! CIOSuccessRequestViewController
        successReqTabVC.title = "Success Requests".localized
        
        let allReqTabVC = UIStoryboard(name: cashInOutViewControllers.cashinoutStoryBoard,
                                       bundle: nil).instantiateViewController(withIdentifier: cashInOutViewControllers.viewControllers.cioAllrequestVC) as! CIOAllRequestViewController
        allReqTabVC.delegate = self
        allReqTabVC.title = "All Requests".localized
        
        controllerArray = [pendingReqTabVC, successReqTabVC, allReqTabVC]
        
        let parameters = [PaytoScrollerOptionMenuItemSeparatorWidth: 0,
                          PaytoScrollerOptionMenuItemSeparatorPercentageHeight: 0.1,
                          PaytoScrollerOptionScrollMenuBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectedMenuItemLabelColor: UIColor.black,
                          PaytoScrollerOptionViewBackgroundColor: UIColor.white,
                          PaytoScrollerOptionSelectionIndicatorColor: UIColor.yellow,
                          PaytoScrollerOptionUnselectedMenuItemLabelColor:UIColor.black,
                          PaytoScrollerOptionMenuHeight: 50,
                          PaytoScrollerOptionMenuItemWidthBasedOnTitleTextWidth: true,
                          PaytoScrollerOptionUseMenuLikeSegmentedControl: false,
                          PaytoScrollerOptionMenuItemFont: "Zawgyi-One",
                          PaytoScrollerOptionMenuItemWidth: (screenWidth / CGFloat(controllerArray.count)) - 10.0] as [String : Any]
        
        
        
//        if let navi = self.navigationController {
//            let yAxis = navi.navigationBar.frame.height + UIApplication.shared.statusBarFrame.height
//            cicoTransactionHomeVC = PaytoScroller(viewControllers: controllerArray,
//                                              frame: CGRect(x: 0.0, y: 0, width: self.view.frame.width,
//                                                            height: self.view.frame.height ),
//                                              options: parameters)
            
        cicoTransactionHomeVC = PaytoScroller(viewControllers: controllerArray, frame: self.view.bounds, options: parameters)

            cicoTransactionHomeVC?.currentPageIndex = 0
            cicoTransactionHomeVC?.delegate = self
            cicoTransactionHomeVC?.scrollingEnable(true)
            if let menu = cicoTransactionHomeVC {
                self.view.addSubview(menu.view)
            }
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cicoTransactionHomeVC?.selectedMenuItemLabelColor = UIColor.black
    }
    
    private func addCustomMenuButton() {
        let moreButton = UIButton(type: .custom)
        moreButton.setBackgroundImage(UIImage(named: "menu_white_payto"), for: .normal)
        moreButton.widthAnchor.constraint(equalToConstant: 35).isActive = true
        moreButton.heightAnchor.constraint(equalToConstant: 35).isActive = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: moreButton)
        moreButton.addTarget(self, action: #selector(moreButtonAction), for: .touchUpInside)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }

    fileprivate func isRequestUser(detail: CashTransferRequestStatus) -> Bool? {
        if let recID = detail.cashTransferMaster?.receiverID {
            if idToCheck == recID {
                return true
            }
        }
        if let reqID = detail.cashTransferMaster?.requesterID {
            if idToCheck == reqID {
                return false
            }
        }
        return nil
    }
    
    fileprivate func showMapView(masterDetail: CashTransferRequestStatus?) {
        if let msDetail = masterDetail {
            if let isRequestr = isRequestUser(detail: msDetail) {
                guard let status = msDetail.cashTransferMaster?.status, let agentType = msDetail.agent?.agentType else { return }
                if isRequestr {
                    self.navigateToUserMap(status: status, agentType: agentType, cashType: msDetail.cashTransferMaster?.cashType)
                } else {
                    navigateToMerchantMap(status: status, agentType: agentType, cashType: msDetail.cashTransferMaster?.cashType)
                }
            } else {
                println_debug("No requestor no receiver, please check")
            }
        } else {
            println_debug("No master detail, please check")
        }
    }
    
    fileprivate func showMapViewForPending(acceptedCashTransferList : PendingCashTransferResponse) {
        if let mapvc = UIStoryboard(name: "CICOMapAndPay", bundle: nil).instantiateViewController(withIdentifier: "CICOUserMapViewController_ID") as? CICOUserMapViewController {
            mapvc.currentPendingCashDetails = acceptedCashTransferList
            self.navigationController?.pushViewController(mapvc, animated: true)
        }
    }
    
    private func moveToRatingScreen(fromUser: Bool) {
        if let ratingVC = UIStoryboard(name: "CashInOutStoryboard", bundle: nil).instantiateViewController(withIdentifier: "CIORatingViewController_ID") as? CIORatingViewController {
            ratingVC.currentSelectedCashDetails = modelCashInCashOut.masterDetail
            ratingVC.fromUser = fromUser
            self.navigationController?.pushViewController(ratingVC, animated: true)
            return
        }
    }
    
    func navigateToMerchantMap(status: Int, agentType: Int, cashType: Int?) {
        switch cashType {
        case 0:
            switch status {
            case 0, 1:
                if let mapvc = UIStoryboard(name: "CICOMapAndPay", bundle: nil).instantiateViewController(withIdentifier: "CICOMerchantMapViewController_ID") as? CICOMerchantMapViewController {
                    mapvc.fromUser = false
                    mapvc.currentSelectedCashDetails = modelCashInCashOut.masterDetail
                    self.navigationController?.pushViewController(mapvc, animated: true)
                    return
                }
            case 3:
                if let userMapStatusVC = UIStoryboard(name: "CashInOutStoryboard", bundle: nil).instantiateViewController(withIdentifier: "CIOConfimationViewController_ID") as? CIOConfimationViewController {
                    userMapStatusVC.currentSelectedCashDetails = modelCashInCashOut.masterDetail
                    self.navigationController?.pushViewController(userMapStatusVC, animated: true)
                    return
                }
            case 5:
                moveToRatingScreen(fromUser: false)
            default:
                break
            }
        case 1:
            switch status {
            case 0, 1, 3:
                if let mapvc = UIStoryboard(name: "CICOMapAndPay", bundle: nil).instantiateViewController(withIdentifier: "CICOMerchantMapViewController_ID") as? CICOMerchantMapViewController {
                    mapvc.fromUser = false
                    mapvc.currentSelectedCashDetails = modelCashInCashOut.masterDetail
                    self.navigationController?.pushViewController(mapvc, animated: true)
                    return
                }
            case 5:
                moveToRatingScreen(fromUser: false)
            default:
                break
            }
        default:
            break
        }
        
    }
    
    func navigateToUserMap(status: Int, agentType: Int, cashType: Int?) {
        switch cashType {
        case 0:
            switch agentType {
            case 0: //  agent
                switch status {
                case 0:
                    if let mapvc = UIStoryboard(name: "CICOMapAndPay", bundle: nil).instantiateViewController(withIdentifier: "CICOUserMapViewController_ID") as? CICOUserMapViewController {
                        mapvc.delegate = self
                        mapvc.currentSelectedCashDetails = modelCashInCashOut.masterDetail
                        self.navigationController?.pushViewController(mapvc, animated: true)
                        return
                    }
                case 1, 3, 4:
                    if let userMapStatusVC = UIStoryboard(name: "CICOMapAndPay", bundle: nil).instantiateViewController(withIdentifier: "CICOUserMapStatusViewController_ID") as? CICOUserMapStatusViewController {
                        userMapStatusVC.currentSelectedCashDetails = modelCashInCashOut.masterDetail
                        self.navigationController?.pushViewController(userMapStatusVC, animated: true)
                        return
                    }
                case 5:
                    moveToRatingScreen(fromUser: true)
                default:
                    break
                }
            case 1: //stationary agent
                switch status {
                case 0, 1:
                    if let mapvc = UIStoryboard(name: "CICOMapAndPay", bundle: nil).instantiateViewController(withIdentifier: "CICOMerchantMapViewController_ID") as? CICOMerchantMapViewController {
                        mapvc.fromUser = true
                        mapvc.currentSelectedCashDetails = modelCashInCashOut.masterDetail
                        self.navigationController?.pushViewController(mapvc, animated: true)
                        return
                    }
                case 5:
                    moveToRatingScreen(fromUser: true)
                default:
                    break
                }
            default: break
                
            }
        case 1:
            
            switch agentType {
            case 0:
                switch status {
                case 0:
                    if let mapvc = UIStoryboard(name: "CICOMapAndPay", bundle: nil).instantiateViewController(withIdentifier: "CICOUserMapViewController_ID") as? CICOUserMapViewController {
                        mapvc.delegate = self
                        mapvc.currentSelectedCashDetails = modelCashInCashOut.masterDetail
                        self.navigationController?.pushViewController(mapvc, animated: true)
                        return
                    }
                case 1:
                    if let userMapStatusVC = UIStoryboard(name: "CICOMapAndPay", bundle: nil).instantiateViewController(withIdentifier: "CICOUserMapStatusViewController_ID") as? CICOUserMapStatusViewController {
                        userMapStatusVC.currentSelectedCashDetails = modelCashInCashOut.masterDetail
                        self.navigationController?.pushViewController(userMapStatusVC, animated: true)
                        return
                    }
                case 3:
                    if let userMapStatusVC = UIStoryboard(name: "CashInOutStoryboard", bundle: nil).instantiateViewController(withIdentifier: "CIOConfimationViewController_ID") as? CIOConfimationViewController {
                        userMapStatusVC.currentSelectedCashDetails = modelCashInCashOut.masterDetail
                        self.navigationController?.pushViewController(userMapStatusVC, animated: true)
                        return
                    }
                case 5:
                    moveToRatingScreen(fromUser: true)
                default:
                    break
                }
            case 1:
                switch status {
                case 0, 1:
                    if let mapvc = UIStoryboard(name: "CICOMapAndPay", bundle: nil).instantiateViewController(withIdentifier: "CICOMerchantMapViewController_ID") as? CICOMerchantMapViewController {
                        mapvc.fromUser = true
                        mapvc.currentSelectedCashDetails = modelCashInCashOut.masterDetail
                        self.navigationController?.pushViewController(mapvc, animated: true)
                        return
                    }
                case 3:
                    if let userMapStatusVC = UIStoryboard(name: "CashInOutStoryboard", bundle: nil).instantiateViewController(withIdentifier: "CIOConfimationViewController_ID") as? CIOConfimationViewController {
                        userMapStatusVC.currentSelectedCashDetails = modelCashInCashOut.masterDetail
                        self.navigationController?.pushViewController(userMapStatusVC, animated: true)
                        return
                    }
                case 5:
                    moveToRatingScreen(fromUser: false)
                default:
                    break
                    
                }
            default:
                break
            }
        default:
            break
        }
    }
    
    //MARK: - Target Methods
    @objc override func backAction() {
        DispatchQueue.main.async {
            if let str = self.fromCancelAcceptScreen, str.count > 0 {
                self.dismiss(animated: true, completion: nil)
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @objc func moreButtonAction() {
        let popoverContent = self.storyboard?.instantiateViewController(withIdentifier: "CIOAgentStatusPopover_ID") as? CIOAgentStatusPopover
        popoverContent?.delegate = self
        popoverContent?.modalPresentationStyle = .popover
        let popover = popoverContent?.popoverPresentationController
        popoverContent?.preferredContentSize = CGSize(width: 220.0, height: 180.0)
        popover?.delegate = self
        popover?.sourceView = self.view
        popover?.permittedArrowDirections = .init(rawValue: 0)
        popover?.sourceRect = CGRect(x: self.view.frame.size.width, y: 0, width: 0, height: 0)
        self.present(popoverContent!, animated: true, completion: nil)
    }
    
//    func showToastForStateChange(type: Int) {
//        switch
//    }
}

//MARK: - CICOelectedRequestProcotol
extension CIOTransactionHomeViewController: CICOelectedRequestProcotol {
    func selectedRequest(acceptedCashTransferList: AcceptedCashTransferList) {
        guard let statusCheck = acceptedCashTransferList.cashTransferMaster?.status, statusCheck != 6 else { return }
        if statusCheck == 2 {
            self.showCICOToast(message: "The selected transaction is cancelled".localized)
        } else {
            if let reqID = acceptedCashTransferList.cashTransferMaster?.id {
                idToCheck = acceptedCashTransferList.customer?.id ?? ""
                modelCashInCashOut.getMasterDetailRequest(id: "\"\(reqID)\"")
            }
        }
    }
    
    func selectedPendingRequest(pendingCashTranfReq: PendingCashTransferResponse) {
        self.showMapViewForPending(acceptedCashTransferList: pendingCashTranfReq)
    }
}

//MARK: - PaytoScrollerDelegate
extension CIOTransactionHomeViewController: PaytoScrollerDelegate {
    func willMove(toPage controller: UIViewController!, index: Int) {
    }
    
    func didMove(toPage controller: UIViewController!, index: Int) {
    }
}

//MARK:- CICOUserMapDelegate
extension CIOTransactionHomeViewController: CICOUserMapDelegate {
    func dismissCICOUserMap() {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
            self.showCICOToast(message: "Agent Started".localized)
        }
    }
}

//MARK: - AgentStatusChangeProtocol
extension CIOTransactionHomeViewController: AgentStatusChangeProtocol {
    func cashInOutStatusAction(cashIn: Bool, cashout: Bool, stationaryMobileStatus: Bool, typeAndStatus: (CICOTypeAndStatus, Bool)) {
        self.typeChange = typeAndStatus
        let agentType = self.getAgentType()
        let appversion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        let request = UpdateUserRequest(countryCode: UserModel.shared.countryCode.replacingOccurrences(of: "+", with: ""),
                                        mobileNumber: UserModel.shared.mobileNo,
                                        openingTime: UserModel.shared.openTime,
                                        closingTime: UserModel.shared.closeTime,
                                        okDollarBalance: Int(self.walletAmount()),
                                        latitude: UserModel.shared.lat,
                                        longitude: UserModel.shared.long, cellID: "0",
                                        cashIn: cashIn, cashOut: cashout,
                                        agentType: stationaryMobileStatus ? "1" : "0",
                                        type: agentType, isOpenAlways: true, appVersion: appversion,
                                        category: UserModel.shared.businessCate,
                                        subCategory: UserModel.shared.businessSubCate)
        modelCashInCashOut.updateUser(request: request)
    }
}

// MARK: - CashInOutModel Delegate
extension CIOTransactionHomeViewController: CashInOutModelDelegate {
    func apiResult(message: String?, statusCode: String?, type: CashInCashOutModel.CashInCashOutApiType) {
        DispatchQueue.main.async {
            switch type {
            case .getMasterDetails:
                self.showMapView(masterDetail: self.modelCashInCashOut.masterDetail)
            case .updateUser:
                var alertMessage = ""
                if let typeChang = self.typeChange {
                    switch typeChang.0 {
                    case .cashIn:
                        switch typeChang.1 {
                        case true:
                            alertMessage = "Cash-In service enabled".localized
                        case false:
                            alertMessage = "Cash-In service disabled".localized
                        }
                    case .cashOut:
                        switch typeChang.1 {
                        case true:
                            alertMessage = "Cash-Out service enabled".localized
                        case false:
                            alertMessage = "Cash-Out service disabled".localized
                        }
                    case .mobileStationary:
                        switch typeChang.1 {
                        case true:
                            alertMessage = "Delivery service enabled".localized
                        case false:
                            alertMessage = "Delivery service disabled".localized
                        }
                    }
                }
                self.showCICOToast(message: alertMessage)
            default:
                break
            }
        }
    }
    
    func handleCustomError(error: CustomError, type: CashInCashOutModel.CashInCashOutApiType) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}
