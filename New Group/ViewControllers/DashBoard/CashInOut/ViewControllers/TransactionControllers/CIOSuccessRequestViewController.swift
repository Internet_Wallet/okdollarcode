//
//  CIOAllRequestViewController.swift
//  OK
//
//  Created by Kethan on 8/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class CIOSuccessRequestViewController: CashInOutBaseViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var transactionRequestTableView : UITableView!
    @IBOutlet weak var noRecordsView : UIView! {
        didSet {
            self.noRecordsView.isHidden = false
        }
    }
    @IBOutlet weak var noRecordsLabel : UILabel! {
        didSet {
            self.noRecordsLabel.font = UIFont(name: appFont, size: appFontSize)
            self.noRecordsLabel.text = self.noRecordsLabel.text?.localized
        }
    }
    
    //MARK: - Properties
    private lazy var modelCashInCashOut: CashInCashOutModel = {
        return CashInCashOutModel()
    }()
    
    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        modelCashInCashOut.cashInCashOutDelegate = self
        self.transactionRequestTableView.tableFooterView = UIView()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadAllData), name: Notification.Name("ReloadCICOAllScreens"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        PTLoader.shared.show()
        self.getRequests()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Methods
    @objc func reloadAllData() {
        DispatchQueue.main.async {
            self.transactionRequestTableView.reloadData()
        }
        self.getRequests()
    }
    
    fileprivate func getRequests() {
        modelCashInCashOut.getSuccessCashTransferList()
    }
}

//MARK: - UITableViewDelegate, UITableViewDataSource
extension CIOSuccessRequestViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelCashInCashOut.successList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "transReqCell", for: indexPath) as! CIOTransReqTableCell
        cell.wrapData(modelData: modelCashInCashOut.successList[indexPath.row], type: "Success")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
}


// MARK: - CashInOutModel Delegate
extension CIOSuccessRequestViewController: CashInOutModelDelegate {
    func apiResult(message: String?, statusCode: String?, type: CashInCashOutModel.CashInCashOutApiType) {
        DispatchQueue.main.async {
            if self.modelCashInCashOut.successList.count > 0 {
                self.modelCashInCashOut.successList = self.modelCashInCashOut.successList.sorted(by: { (firstRec, secRec) -> Bool in
                    return (firstRec.cashTransferMaster?.createdDate ?? "") > (secRec.cashTransferMaster?.createdDate ?? "")
                })
                self.noRecordsView.isHidden = true
            }
            self.transactionRequestTableView.reloadData()
            PTLoader.shared.hide()
        }
    }
    
    func handleCustomError(error: CustomError, type: CashInCashOutModel.CashInCashOutApiType) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}
