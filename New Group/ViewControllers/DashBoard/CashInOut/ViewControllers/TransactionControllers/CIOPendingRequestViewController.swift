//
//  CIOAllRequestViewController.swift
//  OK
//
//  Created by Kethan on 8/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class CIOPendingRequestViewController: CashInOutBaseViewController {
    //MARK: - Outlets
    @IBOutlet weak var transactionRequestTableView : UITableView!
    @IBOutlet weak var noRecordsView : UIView! {
        didSet {
            self.noRecordsView.isHidden = false
        }
    }
    @IBOutlet weak var noRecordsLabel : UILabel! {
        didSet {
            self.noRecordsLabel.font = UIFont(name: appFont, size: appFontSize)
            self.noRecordsLabel.text = self.noRecordsLabel.text?.localized
        }
    }
    
    //MARK: - Properties
    private lazy var modelCashInCashOut: CashInCashOutModel = {
        return CashInCashOutModel()
    }()
    weak var delegate : CICOelectedRequestProcotol?
    enum StatusTemp {
        case accepted, running, ended
    }
    
    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        modelCashInCashOut.cashInCashOutDelegate = self
        self.transactionRequestTableView.tableFooterView = UIView()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadAllData), name: Notification.Name("ReloadCICOAllScreens"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        PTLoader.shared.show()
        self.getRequests()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    fileprivate func getRequests() {
        modelCashInCashOut.getAcceptedCashTransferList()
    }
    
    @objc func reloadAllData() {
        DispatchQueue.main.async {
            self.transactionRequestTableView.reloadData()
        }
        self.getRequests()
    }
    
    //MARK: - Methods
    private func actionToNavigate(statusTemp: StatusTemp, isMerchant: Bool) {
        if isMerchant {
            switch statusTemp {
            case .accepted:
                println_debug("HAVE TO DO")
            case .running:
                println_debug("HAVE TO DO")
            case .ended:
                println_debug("HAVE TO DO")
            }
        } else {
            switch statusTemp {
            case .accepted:
                if let vc = UIStoryboard(name: "CICOMapAndPay", bundle: Bundle.main).instantiateViewController(withIdentifier: "CICOUserMapViewController_ID") as? CICOUserMapViewController {
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            case .running:
                if let vc = UIStoryboard(name: "CICOMapAndPay", bundle: Bundle.main).instantiateViewController(withIdentifier: "CICOUserMapStatusViewController_ID") as? CICOUserMapStatusViewController {
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            default:
                break
            }
        }
    }
}

//MARK: - UITableViewDelegate, UITableViewDataSource
extension CIOPendingRequestViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelCashInCashOut.acceptedList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "transReqCell", for: indexPath) as! CIOTransReqTableCell
        cell.wrapData(modelData: modelCashInCashOut.acceptedList[indexPath.row], type: "Pending")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.selectedRequest(acceptedCashTransferList: modelCashInCashOut.acceptedList[indexPath.row])
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        actionToNavigate(statusTemp: .accepted, isMerchant: true)
//    }
}

//MARK: - CashInOutModelDelegate
extension CIOPendingRequestViewController: CashInOutModelDelegate {
    func apiResult(message: String?, statusCode: String?, type: CashInCashOutModel.CashInCashOutApiType) {
        DispatchQueue.main.async {
            if self.modelCashInCashOut.acceptedList.count > 0 {
                self.modelCashInCashOut.acceptedList = self.modelCashInCashOut.acceptedList.sorted(by: { (firstRec, secRec) -> Bool in
                    return (firstRec.cashTransferMaster?.createdDate ?? "") > (secRec.cashTransferMaster?.createdDate ?? "")
                })
                self.noRecordsView.isHidden = true
            }
            self.transactionRequestTableView.reloadData()
            PTLoader.shared.hide()
        }
    }
    
    func handleCustomError(error: CustomError, type: CashInCashOutModel.CashInCashOutApiType) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}
