//
//  CIOAllRequestViewController.swift
//  OK
//
//  Created by Kethan on 8/6/18.
//  Copyright © 2018 Ashish Kumar Singh. All rights reserved.
//

import UIKit

class CIOAllRequestViewController: CashInOutBaseViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var transactionRequestTableView : UITableView!
    @IBOutlet weak var noRecordsView : UIView! {
        didSet {
            self.noRecordsView.isHidden = false
        }
    }
    @IBOutlet weak var noRecordsLabel : UILabel! {
        didSet {
            self.noRecordsLabel.font = UIFont(name: appFont, size: appFontSize)
            self.noRecordsLabel.text = self.noRecordsLabel.text?.localized
        }
    }
    
    //MARK: - Properties
    private lazy var modelCashInCashOut: CashInCashOutModel = {
        return CashInCashOutModel()
    }()
    weak var delegate : CICOelectedRequestProcotol?
    
    //MARK: - Views life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        modelCashInCashOut.cashInCashOutDelegate = self
        self.transactionRequestTableView.tableFooterView = UIView()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadAllData), name: Notification.Name("ReloadCICOAllScreens"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        PTLoader.shared.show()
        self.getRequests()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Methods
    @objc func reloadAllData() {
        DispatchQueue.main.async {
            self.transactionRequestTableView.reloadData()
        }
        self.getRequests()
    }
    
    fileprivate func getRequests() {
        modelCashInCashOut.getAllCashTransferList()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

//MARK: - UITableViewDelegate, UITableViewDataSource
extension CIOAllRequestViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelCashInCashOut.allCashList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "transReqCell", for: indexPath) as! CIOTransReqTableCell
        cell.wrapData(modelData: modelCashInCashOut.allCashList[indexPath.row], type: "All")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.selectedRequest(acceptedCashTransferList: modelCashInCashOut.allCashList[indexPath.row])
    }
}

//MARK: - CIOTransReqTableCell
class CIOTransReqTableCell : UITableViewCell {
    //MARK: - Outlets
    @IBOutlet weak var agentNameLabel : UILabel!{
        didSet{
            agentNameLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var agentAddressLabel : UILabel!{
        didSet{
            agentAddressLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var agentRatingLabel : UILabel!{
        didSet{
            agentRatingLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var serviceTypeLabel : UILabel!{
        didSet{
            serviceTypeLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var requestStatusLabel : UILabel!{
        didSet{
            requestStatusLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var requestAmountLabel : UILabel!{
        didSet{
            requestAmountLabel.font = UIFont(name: appFont, size: appFontSize)
        }
    }
    @IBOutlet weak var ratingImageView : UIImageView!
    
    //MARK: - Views methods
    override func awakeFromNib() {
        let width = UIScreen.main.bounds.width
        if width > 325 {
            self.agentNameLabel.font = UIFont(name: appFont, size: 17.0) ?? UIFont.systemFont(ofSize: 17.0)
        } else {
            self.agentNameLabel.font = UIFont(name: appFont, size: 15.0) ?? UIFont.systemFont(ofSize: 15.0)
        }
    }
    
    //MARK: - Methods
    func wrapData(modelData : AcceptedCashTransferList, type: String) {
        self.agentNameLabel.text = ""
        var nameStr = ""
        if let name = modelData.customer?.firstName {
            nameStr = name
        }
        if let phone = modelData.customer?.phoneNumber {
            nameStr = "\(nameStr)-\(phone)-merchant"
        }
        self.agentNameLabel.text = nameStr
        
        self.agentAddressLabel.text = modelData.cashTransferMaster?.rechargeDestination?.source ?? ""
        if let rating = modelData.customer?.rating.safelyWrappingString() {
            if let ratingDouble  = Double(rating)?.rounded(toPlaces: 2) {
                self.agentRatingLabel.text = String(format: "%.2f", ratingDouble)
            }
        }
        self.serviceTypeLabel.text = (modelData.cashTransferMaster?.cashType == 0) ? "Cash In Service".localized : "Cash Out Service".localized
        if let status = modelData.cashTransferMaster?.status {
            switch status {
            case 0:
                self.requestStatusLabel.text = "Accepted".localized
            case 1:
                self.requestStatusLabel.text = "Running".localized
            case 2:
                self.requestStatusLabel.text = "Cancelled".localized
            case 3:
                self.requestStatusLabel.text = "Ended".localized
            case 4:
                self.requestStatusLabel.text = "PaymentReceiving".localized
            case 5:
                if type == "Success" || type == "All" {
                    if let modifiedDate = modelData.cashTransferMaster?.modifiedDate {
                        let date = modifiedDate.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                        if let stringDate = date?.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy h:mm a") {
                            self.requestStatusLabel.text = stringDate
                        }
                    } else {
                        self.requestStatusLabel.text = "Success".localized
                    }
                } else {
                    self.requestStatusLabel.text = "Closed".localized
                }
            case 6:
                if let modifiedDate = modelData.cashTransferMaster?.modifiedDate {
                    let date = modifiedDate.dateValue(dateFormatIs: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                    if let stringDate = date?.stringValue(dateFormatIs: "EEE, dd-MMM-yyyy h:mm a") {
                        self.requestStatusLabel.text = stringDate
                    }
                } else {
                    self.requestStatusLabel.text = "Success".localized
                }
            default:
                break
            }
        }
        let amount = Int(modelData.cashTransferMaster?.amount ?? 0)
        self.requestAmountLabel.text = String(amount) + " MMK".localized
    }
    
    func wrapPendingData(modelData: PendingCashTransferResponse) {
//        var nameStr = ""
//        if let name = modelData.customer?.firstName {
//            nameStr = name
//        }
//        if let phone = modelData.customer?.phoneNumber {
//            nameStr = "\(nameStr)-\(phone)-merchant"
//        }
//        var adressString = ""
//        if let line1 = modelData.customer?.line1 {
//            adressString = "\(line1)"
//        }
//        if let line2 = modelData.customer?.line2 {
//            adressString = "\(adressString), \(line2)"
//        }
//        if let cityName = modelData.customer?.cityName {
//            adressString = "\(adressString), \(cityName)"
//        }
//        if let country = modelData.customer?.country {
//            adressString = "\(adressString), \(country)"
//        }
        
//        if let rating = modelData.customer?.rating.safelyWrappingString() {
//            self.agentRatingLabel.text = String(format: "%.2f", rating)
//        }
        self.agentNameLabel.isHidden = true
        self.agentRatingLabel.isHidden = true
        self.ratingImageView.isHidden = true
        self.agentAddressLabel.text = modelData.rechargeDestination?.source ?? ""
        self.serviceTypeLabel.text = (modelData.cashType == 0) ? "Cash In Service".localized : "Cash Out Service".localized
        
        if let status = modelData.status {
            switch status {
            case 0:
                self.requestStatusLabel.text = "Accepted".localized
            case 1:
                self.requestStatusLabel.text = "Running".localized
            case 2:
                self.requestStatusLabel.text = "Cancelled".localized
            case 3:
                self.requestStatusLabel.text = "Ended".localized
            case 4:
                self.requestStatusLabel.text = "PaymentReceiving".localized
            case 5:
                self.requestStatusLabel.text = "Closed".localized
            case 6:
                self.requestStatusLabel.text = "RatingCompleted".localized
            default:
                break
            }
        }
        self.requestAmountLabel.text = String(modelData.rechargeAmount ?? 0) + " MMK"
    }
}

// MARK: - CashInOutModel Delegate
extension CIOAllRequestViewController: CashInOutModelDelegate {
    func apiResult(message: String?, statusCode: String?, type: CashInCashOutModel.CashInCashOutApiType) {
        DispatchQueue.main.async {
            if self.modelCashInCashOut.allCashList.count > 0 {
                self.modelCashInCashOut.allCashList = self.modelCashInCashOut.allCashList.sorted(by: { (firstRec, secRec) -> Bool in
                    return (firstRec.cashTransferMaster?.createdDate ?? "") > (secRec.cashTransferMaster?.createdDate ?? "")
                })
                self.noRecordsView.isHidden = true
            }
            self.transactionRequestTableView.reloadData()
            PTLoader.shared.hide()
        }
    }
    
    func handleCustomError(error: CustomError, type: CashInCashOutModel.CashInCashOutApiType) {
        switch error {
        case .noInternet:
            let alert = UIAlertController(title: "Alert".localized, message: error.rawValue.localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            println_debug("Abonded error")
        }
    }
}


extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
